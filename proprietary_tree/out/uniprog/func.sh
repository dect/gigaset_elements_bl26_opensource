#!/bin/bash
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


MKJFFS2="/usr/sbin/mkfs.jffs2"
SUMTOOL="/usr/sbin/sumtool"
DD="/bin/dd"
FILESIZE="stat -c %s"
NO_RECOVERY=0

function init_image
{
    echo "Creating empty file:"
    echo "   size         $1 kB"
    echo "   file:       '${OUTIMAGE}'"
    ${DD} if=/dev/zero of=${OUTDIR}/${OUTIMAGE} bs=1k count=${1} &>/dev/zero
    echo
}
function check_vmlinuz
{
    if [ ! -e "${OUTDIR}/${VMLINUZ}" ]; then
	echo -e "\n\nFile '${OUTDIR}/${VMLINUZ}' is missing!"
	exit 1
    fi
}
function check_recovery
{
    if [ ${NO_RECOVERY} -eq 1 ]; then
        return
    fi

    if [ ! -e "${OUTDIR}/${RECOVERY}" ]; then
	echo -e "\n\nFile '${OUTDIR}/${RECOVERY}' is missing!"
	echo -ne "\nDo you want to put empty region instead of recovery image? [y/N] "; read DECISION
	if [ "${DECISION^^}" = "Y" ]; then
	    NO_RECOVERY=1
	    echo
	else
	    exit 1
	fi
    fi
}
insert_bootloader() {
	insert_subimage			image452_service.bin		0		131072
}
insert_recovery() {
    if [ ${NO_RECOVERY} -eq 0 ]; then
        insert_subimage			${OUTDIR}/${RECOVERY}		131072		819200
    else
	echo -e "Skipping recovery image.\n"
    fi
}
insert_vmlinuz() {
    insert_subimage			${OUTDIR}/${VMLINUZ}		950272		2560000
}
function mk_jffs2
{

    echo "Creating JFFS2 file system:"
    echo "   from      '$1'"
    echo "   to        '$2'"
    echo "   size       $3"
    ${MKJFFS2} 	--squash \
		--little-endian \
		--compression-mode=size \
		--eraseblock=4096 \
		--pad=${3} \
		--root=${1} \
		--output=${2}
    echo
}
insert_jffs2() {
	create_jffs2
	insert_subimage			${MNT_DATA_JFFS2_FILE}		3510272		1138688
}
create_jffs2() {
    if [ -n "${MAKELEVEL}" ]; then
	mk_jffs2 ${MNT_DATA_FS}		${MNT_DATA_JFFS2_FILE}				1138688
	return 0
    fi
    if [ ! -e ${MNT_DATA_JFFS2_FILE} ]; then
	mk_jffs2 ${MNT_DATA_FS}		${MNT_DATA_JFFS2_FILE}				1138688
    else
	echo -e "Not creating new filesystem image.\nUsing old one: ${MNT_DATA_JFFS2_FILE}\n"
    fi	

}
function insert_subimage
{
    echo "Inserting image file:"
    echo "   file        '$1'"
    echo "   at offset    $2"
    echo "   size         $3"
    if [ ! -e $1 ]; then
	echo "File '$1' not exist!"
	exit 1
    fi
    ACTUAL_FILE_SIZE=`${FILESIZE} $1`
    if [ ${ACTUAL_FILE_SIZE} -gt $3 ]; then
	echo "File '$1' is too large!"
	exit 2
    fi
    ${DD} if=${1} of=${OUTDIR}/${OUTIMAGE} seek=${2} obs=1 conv=notrunc &>/dev/zero
    echo
}
create_sum() {
    pushd ${OUTDIR}
    sha256sum ${OUTIMAGE} > ${OUTIMAGE}.sum
    popd
}
function set_serial_port
{
    if [ -n "$PORT" ]; then
	return 0
    fi
    if [ -n "$PROGRAM_PORT" ]; then
	export PORT="${PROGRAM_PORT}"
	return 0
    fi
    echo
    echo "To skip this question, you can assign serial port to environment variable PROGRAM_PORT or PORT"
    echo
    echo "Please choose serial port:"

    select USER_CHOICE in `ls -1 -l /sys/class/tty/*/device/driver  | awk '{print $9}' | cut -d'/' -f 5`; do
	if [ "$USER_CHOICE" != "" ]; then
	    export PORT="/dev/${USER_CHOICE}"
	    return 0
	fi
	echo "Wrong choice!"
	echo
	continue
    done
    
} 
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.
