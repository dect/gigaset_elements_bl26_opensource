#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

# Wojciech Nizinski 
# Gigaset Elements GmbH 

echo "Executing $0..."

# give time to network up
echo "Sleeping 10 seconds..."
sleep 10

cd /tmp

. /etc/hlpfunc.sh
############################## CONFIG ###########################
URL=`get_recovery_url`

KERNEL_FILE_NAME="recovery.bin"      # Kernel image to flash
FS_FILE_NAME="recoveryfs.bin"        # JFFS2 image to flash

KERNEL_MTD_NAME="Linux1"	# string in  MTD device in /proc/mtd
FS_MTD_NAME="FS1"		# string in MTD device in /proc/mtd

TIMEOUT=60
#################################################################
BAS_TAG=`cat /proc/version | grep -w "Reef BS version" | cut -d \' -f 2`
BAS_HASH=`cat /proc/version | grep -w "Reef BS hash" | cut -d \' -f 2`
# no device id in plain http mode
USER_AGENT="Basestation/${BAS_TAG}"

echo "Recovery URL: '${URL}'"
echo "User-agent: '${USER_AGENT}'"


######################################
# Try to download file 10 times
#   $1 - complete URL
#   $2 - output file
#   r  - 0 if success
wget_file ()
{
    TRY=1
    
    echo
    while [ $TRY -lt 10 ]; do
        echo "${TRY}. try to download file '$2' from '$1'"    
        wget -T ${TIMEOUT} -U "${USER_AGENT}" $1 -O $2
        WGET_EXIT_CODE=$?
        echo "WGet exits with ${WGET_EXIT_CODE}"
        
        if [ ${WGET_EXIT_CODE} -eq 0 ]; then
            return 0
        fi
        TRY=$((TRY+1))
    done
    return 1
}

######################################3
# Try to download and verify file.
# SHA256 sums are expected on server as $2.sum
#
#   $1 - complete URL
#   $2 - output file
#   r  - 0 if success
#
download_file ()
{
    wget_file $1 $2
    if [ $? -ne 0 ]; then
        return 1
    fi
 
    wget_file $1.sum $2.sum
    if [ $? -ne 0 ]; then
        return 1
    fi
        
    echo
    echo "Downloaded sum file:"
    cat $2.sum
    echo
    echo "Calculating sum for file '$2' ..."
    sha256sum -s -c $2.sum
    if [ $? -ne 0 ]; then
        echo "SHA256 sum is incorrect"
        return 1
    fi
    echo "SHA256 sum is correct"
            
    return 0
}

#############################################
# erase and write image into flash partition
#  $1 - source file
#  $2 - name of partition (see names /proc/mtd)
#       can be a part of name
#  r - 0 if success
flash_image ()
{
    echo
    echo "Looking for MTD device '$2' ..."
    DST_MTD=`cat /proc/mtd | grep -w "$2" | cut -f 1 -d ':'`

    if [ -z "${DST_MTD}" ]; then
        echo "No MTD device found to flash!"
        return 1
    fi

    DST_MTD_DEV="/dev/${DST_MTD}"

    echo
    echo "Erasing MTD device '${DST_MTD_DEV}' ..."
    flash_eraseall ${DST_MTD_DEV}


    echo
    echo "Flashing MTD device '${DST_MTD_DEV}' ..."
    cp $1 ${DST_MTD_DEV}
    rm $1
    return 0
}




download_file "${URL}/${KERNEL_FILE_NAME}" "${KERNEL_FILE_NAME}"
if [ $? -ne 0 ]; then
    recovery_failed
fi

download_file "${URL}/${FS_FILE_NAME}" "${FS_FILE_NAME}"
if [ $? -ne 0 ]; then
    recovery_failed
fi


echo
echo "Bringing down network interface..."
ifconfig eth0 down


flash_image "${FS_FILE_NAME}" "${FS_MTD_NAME}"
if [ $? -ne 0 ]; then
    recovery_failed
fi

flash_image "${KERNEL_FILE_NAME}" "${KERNEL_MTD_NAME}"
if [ $? -ne 0 ]; then
    recovery_failed
fi


# TODO
# set boot from first system in uboot env or erase uboot env - better
#fw_setenv boot_from flash
#fw_setenv boot_from_image_no 1
flash_eraseall /dev/mtd9

echo "Rebooting system..."
reboot







  
    
