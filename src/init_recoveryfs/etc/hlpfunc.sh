#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


recovery_failed ()
{
    echo "RECOVERY FAIL: rebooting system..."
    reboot
    sleep 60
    exit 1
}

get_recovery_url ()
{ 
#    echo "http://reef.spox.org";
    echo "http://recovery.gigaset-elements.de"
} 

# Set led state 
#
# expected parameters:
#	$1 - LED name,
#	$2 - trigger (none, blinking),
#	$3 - led state (for "none" trigger).
set_led_state()
{
	case $2 in
		none)
			echo none > /sys/class/leds/$1/trigger
			echo $3 > /sys/class/leds/$1/brightness
			;;

		blinking)
			# don't restart led when mode "timer" is active
			cat /sys/class/leds/$1/trigger | grep -q [[]timer[]]
			if [ $? != 0 ]; then
				echo timer > /sys/class/leds/$1/trigger
				echo 1000 > /sys/class/leds/$1/delay_on
				echo 1000 > /sys/class/leds/$1/delay_off
			fi
			;;

		*)
			echo "Option $2 is not supported!"
			;;
	esac
}
