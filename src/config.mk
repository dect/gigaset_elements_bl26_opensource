TOP := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))
export TOP

# default build variant
export BUILD ?= hw

#configuration file for variant
export CONFIG ?= config.$(BUILD).mk

include $(TOP)/$(CONFIG)

# how many cores we have on build machine
export JOBS = $(shell cat /proc/cpuinfo  | grep processor | wc -l)


# output directory (common for all variants)
export OUT_COMMON ?= $(TOP)/../out

# output directory for build variant
export OUT_BUILD ?= $(OUT_COMMON)/$(BUILD)

# baseline dir and tag
OUT_BAS_PREFIX=$(TOP)/../
export BAS_TAG :=$(shell ${TOP}/../tools/make-baseline-version.sh gettag)
export BAS_VERSION :=001.000.026
export OUT_BAS_NAME :=HomeConnector_$(BAS_VERSION)
export OUT_BAS_DIR :=$(OUT_BAS_PREFIX)$(OUT_BAS_NAME)

# default OUT directory for all applications 
define OUT
$(OUT_BUILD)$(subst $(TOP),,$(CURDIR))
endef

# temporary rootfs for build variant
export TMP_ROOTFS ?= $(OUT_BUILD)/tmp_rootfs

# rootfs with initial stuff
export INIT_ROOTFS ?= $(TOP)/init_rootfs

# recovery rootfs with initial stuff
export INIT_RECOVERYFS ?= $(TOP)/init_recoveryfs

# main rootfs for hardware
export ROOTFS ?= $(TOP)/../rootfs

# nfsfs with initial stuff
export INIT_NFS ?= $(TOP)/init_nfs

# main nfs for hardware
export NFS ?= $(TOP)/../nfs

# /mnt/data fs with initial stuff
export INIT_DATAFS ?= $(TOP)/init_datafs

# main /mnt/data fs for hardware
export DATAFS ?= $(TOP)/../datafs

# magic file to signalize that configure for build variant was processed 
export LAST_INIT_REGEX := last_init.*
export LAST_INIT := last_init.$(BUILD)


# prefix for CUnit tests application 
export CUNIT_PREFIX := cunit_
export CUNIT_DIR := /cunit
export CUNIT_SUMMARY := $(OUT_COMMON)/cunit.txt
export CUNIT_ERRORS := $(shell cat $(CUNIT_SUMMARY) | grep FAILED | wc -l)

# output directory for Doxygen documentation
export DOXYGEN_DIR := $(OUT_COMMON)/doxygen


####################################################
# $(call copy_files,src,dst)
####################################################
define copy_files
	@echo "----------------------------------"
	@echo "Copying from \n\t$(1)\nto\n\t$(2)"
	@if [ -d $(1) ]; then \
	    cp -pr $(1) $(2); \
	else \
	    cp -p $(1) $(2); \
	fi
	@echo "----------------------------------"
endef

####################################################
# $(call update_depfile,depfile,objfile)
####################################################
define update_depfile
	@mv -f $(1) $(1).tmp
	@sed -e 's|.*:|$(2):|' < $(1).tmp > $(1)
	@rm -f $(1).tmp
endef

####################################################
# $(call install_on_root,file,where)
####################################################
ifeq ($(BUILD),hw)
define install_on_root
	@mkdir -p $(ROOTFS)$(2)
	$(call copy_files,$(1),$(ROOTFS)$(2))
endef
else
define install_on_root
	@mkdir -p $(TMP_ROOTFS)$(2)
	$(call copy_files,$(1),$(TMP_ROOTFS)$(2))
endef
endif

####################################################
# $(call install_on_nfs,file,where)
####################################################
ifeq ($(BUILD),hw)
define install_on_nfs
	@mkdir -p $(NFS)$(2)
	$(call copy_files,$(1),$(NFS)$(2))
endef
else
define install_on_nfs
	@mkdir -p $(TMP_ROOTFS)$(2)
	$(call copy_files,$(1),$(TMP_ROOTFS)$(2))
endef
endif


####################################################
ifeq ($(BUILD),hw)
# $(call install_test,file)
define install_test
	$(call install_on_root,$(1),$(CUNIT_DIR))
endef
else
# $(call install_test,file)
define install_test
	$(call install_on_root,$(1),$(CUNIT_DIR))
	@cd $(TMP_ROOTFS)$(CUNIT_DIR) && \
	$(TMP_ROOTFS)$(CUNIT_DIR)/$(notdir $(1)) >> $(CUNIT_SUMMARY)
endef
endif


# include user configuration
ifneq ($(wildcard $(TOP)/user.$(USER).mk), )
include $(TOP)/user.$(USER).mk
endif
