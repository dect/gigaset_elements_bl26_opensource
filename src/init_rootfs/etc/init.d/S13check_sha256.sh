#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.



start() {
	EXPECTED="edeaaff3f1774ad2888673770c6d64097e391bc362d7d6fb34982ddf0efd18cb"
	RESULT=`echo "abc" | sha256sum | cut -f 1 -d ' '`
	if [ "$EXPECTED" != "$RESULT" ]; then
	    echo -e "\n\n!!!! SHA 256 implementation broken, please correct this !!!! \n\n"
	    sleep 60
	    reboot
	fi
}




case "$1" in

start)
start
;;

*)
echo "Usage: $0 {start}"
exit 1

esac

exit 0
