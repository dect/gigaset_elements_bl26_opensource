#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


# Mount all filesystems


start() {
	# Mount ramfs and create links for the writable parts of the file system.
	#mount -t ramfs ramfs /mnt/ramfs
	
	#mkdir /mnt/ramfs/var
	
	#mkdir /mnt/ramfs/tmp
	
	#mount /proc
	
	#mount -t sysfs sysfs /sys
	
	#mount -t tmpfs -o size=64K,mode=0755 tmpfs /dev
	#mkdir /dev/pts
	mount -t devpts devpts /dev/pts
	
	mount -a
	mkdir /var/run
	mkdir /var/lock
	mkdir /tmp/tftp

	if [ "x${booted_from}" = "x2" ]; then
	    DATA_PART_NAME="FS2"
	else
	    DATA_PART_NAME="FS1"
	fi
	echo -n "Looking for MTD device '$DATA_PART_NAME' ... "
	DST_MTD_NUM=`cat /proc/mtd | grep -w "${DATA_PART_NAME}" | cut -f 1 -d ':' | cut -f 2 -d 'd'`
	if [ -z "${DST_MTD_NUM}" ]; then
	    echo "No MTD device found!"
	else
	    DST_MTD_DEV="/dev/mtdblock${DST_MTD_NUM}"
	    echo "${DST_MTD_DEV} found!"
	    mount ${DST_MTD_DEV} /mnt/data -t jffs2
	fi
	
	
	mkdir -p /mnt/data/cfg
	mkdir -p /mnt/data/cert
	mkdir -p /mnt/data/fw
	mkdir -p /mnt/data/fw/sensors
	
	# create symlinks to the sensor firmware packages
	SENSOR_FIRMWARE_FILES=`ls /mnt/data/fw/sensors`
	for FILE in ${SENSOR_FIRMWARE_FILES}
	do
		ln -s /mnt/data/fw/sensors/${FILE} /tmp/tftp/${FILE}
	done 
	
	
	# MDev is required for creation of /dev fs on NFS root filesytem
	/bin/grep nfsroot /proc/cmdline -q
	if [ $? -eq 0 ]; then
	    /bin/echo "$0: nfsroot detected, starting mdev..."
	    #echo /sbin/mdev > /proc/sys/kernel/hotplug
	    mdev -s
	fi
	
	cp /etc/ssl/certs/ca-bundle.pem.src /var/ca-bundle.pem
}

case "$1" in

start)
start
;;


*)
echo "Usage: $0 {start}"
exit 1

esac

exit 0
