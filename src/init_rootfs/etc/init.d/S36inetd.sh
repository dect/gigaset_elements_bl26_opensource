#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


# Start inetd/telnet only if system is unlocked

if [ "${system_locked}" == "false" ]; then
	echo "System unlocked - starting inetd"
	/usr/sbin/inetd -f /etc/inetd.conf &
fi

exit 0
