#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


# This step is introduced only for development stage.
# It will be removed from official release. 



start() {
	if [ x"$reef_stop" = xtrue ]; then
		touch /var/run/reef_stop
	fi
	
	if [ x"$factory_reset" = xtrue ]; then
	    echo -e "\n\nReseting to factory defaults!\n"
	    rm -rf /mnt/data/cfg/*
	    rm -rf /mnt/data/cert/*
	    rm -rf /mnt/data/cache/*
	    rm -rf /mnt/data/fw
	    rm -f /mnt/data/endnodes	    
	    # nvs.bin will be restored from Factory partition in S34dectdata.sh
	    rm -rf /mnt/data/nvs.bin
	fi
}


stop() {
	echo "Currently not supported"
}



case "$1" in

start)
start
;;

stop)
stop
;;

restart)
stop
start
;;

*)
echo "Usage: $0 {start|stop|restart}"
exit 1

esac

exit 0
