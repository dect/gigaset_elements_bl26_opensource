#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


#
# This script runs Factory Programming tool
#

start() {
	if [ x"$Giga94" = xtrue ]; then
		/usr/bin/uleapp -i
		/sbin/reboot -f
	fi
}


stop() {
	killall uleapp
}



case "$1" in
	start)
		start
		;;

	stop)
		echo REEF basestation is being stopped ...	
		stop
		echo Done.
		;;

	restart)
		stop
		start
		;;


*)
echo "Usage: $0 {start|stop|restart}"
exit 1

esac

exit 0