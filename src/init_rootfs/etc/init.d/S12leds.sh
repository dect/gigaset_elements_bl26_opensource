#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


# Turn Power LED ON/OFF

start() {
	echo 1 > /sys/class/leds/power/brightness
}


stop() {
	echo 0 > /sys/class/leds/power/brightness
}



case "$1" in

start)
start
;;

stop)
stop
;;

restart)
stop
start
;;

*)
echo "Usage: $0 {start|stop|restart}"
exit 1

esac

exit 0
