#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

DECT_DATA="/mnt/data/nvs.bin"
DECT_DATA_BAK="${DECT_DATA}.bak"

TEMP_DATA="/tmp/nvs.bin"
TEMP_SUM="/tmp/nvs.bin.sum"

MTDDEV="/dev/mtdblock8"
SUMLEN="128"
SUMOFFSET="3968"

do_dd() {
    DDOUT=`dd $@ 2>&1`
    if [ $? -ne 0 ]; then
	echo "dd error!"
	echo "   cmdline: $@"
	echo "${DDOUT}"
    fi
}

dump() {
    do_dd if=${MTDDEV} of=${TEMP_SUM} bs=1 count=${SUMLEN} skip=${SUMOFFSET}	#> /dev/zero	2>&1
    do_dd if=${MTDDEV} of=${TEMP_DATA} bs=4096 count=1 skip=1			#> /dev/zero	2>&1
    echo "Content of '${TEMP_DATA}':"
    hd -v ${TEMP_DATA}
    echo "Content of '${TEMP_SUM}':"
    hd -v ${TEMP_SUM}
}

# copy nvs.bin to nvs.bin.bak
make_local_backup() {
	if [ -e ${DECT_DATA} ]; then
	    # just for case:
	    cp ${DECT_DATA} ${DECT_DATA_BAK}
	fi
}

# copy /mnt/data/nvs.bin to factory partition
backup_to_factory() {
	if [ ! -e ${DECT_DATA} ]; then
	    echo "File '${DECT_DATA} doesn't exist!"
	    return 1
	fi
	echo "Copying ${DECT_DATA} to factory partition..."
	# copy file, to make unchangeable snapshot
	cp ${DECT_DATA} ${TEMP_DATA}
	# create sum
	sha256sum ${TEMP_DATA} > ${TEMP_SUM}
	# write zeroes to sum area of factory partition
	do_dd if=/dev/zero of=${MTDDEV} bs=1 count=${SUMLEN} seek=${SUMOFFSET}
	# write sum to factory partition.	
	do_dd if=${TEMP_SUM} of=${MTDDEV} bs=1 count=${SUMLEN} seek=${SUMOFFSET}
	# write data to factory partition. seek=1 skip one <bs> block at output
	do_dd if=${TEMP_DATA} of=${MTDDEV} bs=4096 count=1 seek=1
}

# loads factory data to /tmp and check sums, not modyfing nvs.bin
get_from_factory() {
	do_dd if=${MTDDEV} of=${TEMP_SUM} bs=1 count=${SUMLEN} skip=${SUMOFFSET}
	# remove padding zeroes
	cat ${TEMP_SUM} | tr -d '\0' > ${TEMP_SUM}.clean
	mv -f ${TEMP_SUM}.clean ${TEMP_SUM}
	# if file is zero sized
	if [ ! -s ${TEMP_SUM} ]; then
	    echo "Factory DECT data checksum failed, ignoring!"
	    return 1
	fi
	do_dd if=${MTDDEV} of=${TEMP_DATA} bs=4096 count=1 skip=1
	cd /tmp
	sha256sum -c ${TEMP_SUM} >/dev/zero 2>&1
	if [ $? -ne 0 ]; then
	    echo "Factory DECT data checksum failed, ignoring!"
	    return 1
	else
	    return 0
	fi
}


case "$1" in

    start)
	# put nvs.bin into factory partition once
	# check if factory data is already programed
	get_from_factory
	FACTORY_VALID=$?	# zero if valid
	
	if [ ${FACTORY_VALID} -eq 0 ]; then
	    # factory valid. Check if we need to restore nvs.bin
	    if [ ! -e ${DECT_DATA} ]; then
		echo
		echo "File '${DECT_DATA} not found, restoring factory data..."
		echo
		cp -f ${TEMP_DATA} ${DECT_DATA}
	    fi
	else
	    # factory is invalid. Try to save nvs.bin if exist
	    if [ -e ${DECT_DATA} ]; then
		echo
		echo "File '${DECT_DATA} found, factory prtition empty."
		echo
		backup_to_factory
	    fi
	fi
	make_local_backup
#	rm ${TEMP_DATA} > /dev/zero 2>&1
#	rm ${TEMP_SUM} > /dev/zero 2>&1
	;;

    backup_to_factory)
	backup_to_factory
	;;

    backup)
	make_local_backup
	;;
	
    dump)
	dump
	;;
    *)
	echo "Usage: $0 {start|backup|backup_to_factory|dump}"
	exit 1
	;;
esac

exit 0











