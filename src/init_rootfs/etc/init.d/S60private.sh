#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

# This script runs the script /mnt/data/private.sh if it exist. 

FILE="/mnt/data/private.sh"

if [ -f "${FILE}" ]; then
	${FILE} & 
fi