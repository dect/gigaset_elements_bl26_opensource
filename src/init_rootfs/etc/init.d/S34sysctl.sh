#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


# Tune the system

start() {
	# reduce TIME_WAIT from 60 to 5 seconds
	echo "5" > /proc/sys/net/ipv4/tcp_fin_timeout

	# increase local port numbers range
	echo "32768 65535" > /proc/sys/net/ipv4/ip_local_port_range

#tcp_tw_recycle - BOOLEAN
#    Enable fast recycling TIME-WAIT sockets. Default value is 0.
#    It should not be changed without advice/request of technical experts.

#tcp_tw_reuse - BOOLEAN
#    Allow to reuse TIME-WAIT sockets for new connections when it is
#    safe from protocol viewpoint. Default value is 0.
#    It  should not be changed without advice/request of technical experts.


#	echo "1" > /proc/sys/net/ipv4/tcp_tw_recycle
#	echo "1" > /proc/sys/net/ipv4/tcp_tw_reuse


# ----------------------------
# keep alive on default values:
# ----------------------------
# This means that the keepalive routines wait for two hours (7200 secs) before sending 
# the first keepalive probe, and then resend it every 75 seconds. 
# If no ACK response is received for nine consecutive times, the connection is marked as broken. 

# tcp_keepalive_time (seconds) (default 7200)
#	the interval between the last data packet sent (simple ACKs are not considered data) 
#	and the first keepalive probe; after the connection is marked to need keepalive, 
#	this counter is not used any further 

#	echo "10" > /proc/sys/net/ipv4/tcp_keepalive_time  


# tcp_keepalive_intvl (seconds) (default 75)
#	the interval between subsequential keepalive probes, 
#	regardless of what the connection has exchanged in the meantime 

#	echo "2" > /proc/sys/net/ipv4/tcp_keepalive_intvl   

# tcp_keepalive_probes - (number) (default 9)
#	the number of unacknowledged probes to send before considering the connection 
#	dead and notifying the application layer 
	
#	echo "2" > /proc/sys/net/ipv4/tcp_keepalive_probes


# How often flush JFFS2 write buffer
# (default 3000) in centiseconds, so 3000 means 30 seconds
# 50 = 0,5 second
echo 50 > /proc/sys/vm/dirty_expire_centisecs

# balance between flushing/keeping dirty pages (default 100), (0 - never flush = out of memory after some time)
echo 10000 > /proc/sys/vm/vfs_cache_pressure

# kernel debug infos on console
echo 8 > /proc/sys/kernel/printk

# number of second before reenabling messages (5)
echo 2 > /proc/sys/kernel/printk_ratelimit

#number of messagess accepted before rate limiting (15)
echo 50 > /proc/sys/kernel/printk_ratelimit_burst

}


case "$1" in

start)
start
;;

restart)

start
;;

*)
echo "Usage: $0 {start|restart}"
exit 1

esac

exit 0











