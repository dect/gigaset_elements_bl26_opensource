#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


# Start loopback interface

start() {
	ifup lo
}



stop() {
	ifdown lo
}



case "$1" in

start)
start
;;

stop)
stop
;;

restart)
stop
start
;;

*)
echo "Usage: $0 {start|stop|restart}"
exit 1

esac

exit 0











