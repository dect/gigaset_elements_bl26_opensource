#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


# Start network

start() {
	ifplugd -d 0 -i eth0 
}



stop() {
	ifplugd -k
}



case "$1" in

start)
start
;;

stop)
stop
;;

restart)
stop
start
;;

*)
echo "Usage: $0 {start|stop|restart}"
exit 1

esac

exit 0











