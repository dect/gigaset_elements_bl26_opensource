#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

#
# Start logging
#

case "$1" in
  start)
	SYSLOGHOST=`fw_printenv -n syslogip 2>/dev/null |grep  "^[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*"`
	if [ "$?" -eq 0 ]; then
	    echo -n "Starting local + remote '${SYSLOGHOST}' logging: "
	    syslogd -R ${SYSLOGHOST} -L -C64 -O /tmp/syslog
#	    sleep 1
	else
	    echo -n "Starting local logging: "
	    /sbin/syslogd -C64 -O /tmp/syslog
	fi
	/sbin/klogd
	echo "OK"
	;;
  stop)
	echo -n "Stopping logging: "
	killall syslogd
	killall klogd
	echo "OK"
	;;
  restart|reload)
	echo "Unsupported!"
	;;
  *)
	echo "Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $?
