#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


# This script runs REEF basestation.


OPENSSL_CONF=/mnt/data/cert/openssl.cnf

CERTDIR="/mnt/data/cert"
CERTFILE="${CERTDIR}/cert"
CSR=${CERTFILE}.csr
KEYPAIR=${CERTFILE}.key
PUB=${CERTFILE}.pub
CERT=${CERTFILE}.crt

gen_ssl_conf() {
	echo "[ distinguished_name ]"               > ${OPENSSL_CONF}
	echo "C     = DE"                           >> ${OPENSSL_CONF}
	echo "ST    = North Rhine-Westphalia"       >> ${OPENSSL_CONF}
	echo "L     = Dusseldorf"                   >> ${OPENSSL_CONF}
	echo "O     = Gigaset Elements GmbH"  >> ${OPENSSL_CONF}
	echo "CN    = ${deviceid}"                  >> ${OPENSSL_CONF}
}

gen_ssl_files() {

# Generate openssl configuration file
	gen_ssl_conf

# Make sure that cert directory exists
	if ! [ -d ${CERTDIR} ]; then
		mkdir -p ${CERTDIR}
	fi

# Generate key pair if necessary
	if [ -f ${KEYPAIR} ]; then
		echo "Key file ${KEYPAIR} already exists. Skipping generation."
	else
		echo "Generating key file"
		/usr/bin/coco --genkey --out ${KEYPAIR}
	fi
	
# Generate certificate request if necessary
	if [ -f ${CSR} ]; then
		echo "CSR file ${CSR} already exists. Skipping generation."
	else
		echo "Generating csr file"
		/usr/bin/coco --gencsr --conf ${OPENSSL_CONF} --in ${KEYPAIR} --out ${CSR}
	fi
}

# run $1 without output if required
silencium() {
	if [ x"$SILENCIUM" = xtrue ]; then
		echo "Running $1 in silent mode"
		$1 > /dev/null 2>&1 &
	else
		echo "Running $1 in normal mode"	
		$1 &
	fi
}


reef_pause() {
	killall sens
	killall tram
	killall heartbeat
	killall coco
	killall coma
	killall jkeymon
	killall qpa
	killall uleapp
	killall wdt
	killall tick
	watchdog -t 10 /dev/watchdog
}

reef_resume() {
	reboot
}

start() {
	if [ "$1" != "force" ]; then
		# check if BS is not blocked
		if [ -f /var/run/reef_stop ]; then
			echo REEF basestation is blocked
			exit 1
		fi 
	fi
	
	export DEVICE_ID=${deviceid}
	
	export BAS_TAG=`cat /proc/version | grep -w "Reef BS version" | cut -d \' -f 2`
	export BAS_HASH=`cat /proc/version | grep -w "Reef BS hash" | cut -d \' -f 2`
	# no device id in plain http mode
	export UA="Basestation/${BAS_TAG}"
	
	export MALLOC_DEBUG=1
	
	ULEOPT=`fw_printenv -n uleopt 2>/dev/null`
	TFTP_DIR=`fw_printenv -n tftp_dir 2>/dev/null`
	SILENCIUM=`fw_printenv -n silencium 2>/dev/null`
	
	if [ -x /bin/dmalloc ]; then
		echo "------------"
	    /bin/dmalloc
	    echo "------------"
	fi
	
	# generate keypairs and csr if needed
	gen_ssl_files
	
	# check databases
	/usr/bin/coin --a --v
	
	# set TFTP directory
	if [ ! -z ${TFTP_DIR} ]; then
	    export TFTP_DIR
	else
		export TFTP_DIR=/mnt/data/fw/sensors
	fi

    ULEAPP_ERR_LOG=/mnt/data/uleapp.err.log
    if [ -f ${ULEAPP_ERR_LOG} ]; then
        echo "BEGIN ${ULEAPP_ERR_LOG}"
        cat ${ULEAPP_ERR_LOG}
        echo "END ${ULEAPP_ERR_LOG}"
    fi
    
	# run REEF basestation
	echo REEF basestation with id $DEVICE_ID is being started ... 
	/usr/bin/jbus &
	/usr/bin/wdt &
	/usr/bin/coma &	
	silencium /usr/bin/coco
	/usr/bin/heartbeat & 
	/usr/bin/tram &
	/usr/bin/sens &
	/usr/bin/pyta &
	/usr/bin/jkeymon &	
	/usr/bin/qpa &
	/usr/bin/uleapp -6 -dmxce -v ${ULEOPT} &
	/usr/bin/tick &
	echo Done.
}


stop() {
	reef_pause
	killall pyta
	killall jbus
}



case "$1" in

forcestart)
	rm /var/run/reef_stop >/dev/zero 2>&1
	start force
	;;

start)
	start
	;;

stop)
	echo REEF basestation is being stopped ...	
	stop
	echo Done.
	;;

restart)
	stop
	start
	;;

pause)
	echo REEF basestation is being paused ...
	reef_pause	
	echo Done.
	;;

resume)
	echo REEF basestation is resuming operation ...
	reef_resume
	echo Done.
	;;

*)
echo "Usage: $0 {start|forcestart|stop|restart|pause|resume}"
exit 1

esac

exit 0











