#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

# This script runs the script specified in the U-Boot environment variable
# 'initscript' if it is defined

FILE=`fw_printenv -n initscript 2>/dev/null`
FILEPATH=/etc/init.d/${FILE}

if [ "$?" -eq 0 ]; then
    if [ -x ${FILEPATH} ]; then
        echo "Starting '${FILEPATH} $*' via UBoot initscript variable"
        ${FILEPATH} $*
    else
       echo "ERROR: UBoot initscript '${FILEPATH}' not found or not executable"
    fi
fi
