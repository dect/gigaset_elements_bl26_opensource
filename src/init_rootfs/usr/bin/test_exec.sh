#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

cd /

COUNTER=1
while [ true ]; do
    echo $COUNTER
    ls
    COUNTER=$((COUNTER+1))
done
