#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


usage()
{
    echo
    echo "Usage: $0 sensorId devId"
    echo
    echo "Can be used without panel/app interaction, but you probably want to listen on CloudRX while trying to delete the sensor"
    echo "in order to get its id"
    exit 0
}

if [ -z "$1" ]; then
    usage
fi


echo \{\"method\":\"POST\", \"uri\":\"https:\/\/api-bs.gigaset-elements.de\/api\/v1\/endnode\/$2\/$1\/sink\/ev\", \"payload\": \{\"payload\": \"deleted\"\}, \"clientId\": 138\} | sender 127.0.0.1 "CloudTX"




