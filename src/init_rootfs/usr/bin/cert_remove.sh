#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

. /usr/bin/cert_common.sh
rm ${CERT}
rm ${KEYPAIR}
rm ${CSR}
rm ${PUB}
