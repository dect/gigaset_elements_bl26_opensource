#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


LED=$1
SCENARIO=$2

# file with LED status
CFG=/var/run/led.${LED}


# read status file if exist
if [ -f ${CFG} ]; then
	CURRENT=`cat ${CFG}`
fi


# don't touch if LED is being used this SCENARIO
if [ "$CURRENT" = "$SCENARIO" ]; then
	exit 0
fi



# save new scenario for this led
echo ${SCENARIO} > ${CFG}


# apply scenario
case "$SCENARIO" in
	on)
		echo none > /sys/class/leds/${LED}/trigger
		echo 1 > /sys/class/leds/${LED}/brightness
		;;
	
	off)
		echo none > /sys/class/leds/${LED}/trigger
		echo 0 > /sys/class/leds/${LED}/brightness
		;;
	
	slow)
		echo timer > /sys/class/leds/${LED}/trigger
		echo 1000 > /sys/class/leds/${LED}/delay_on
		echo 1000 > /sys/class/leds/${LED}/delay_off
		;;


	fast)
		echo timer > /sys/class/leds/${LED}/trigger
		echo 250 > /sys/class/leds/${LED}/delay_on
		echo 250 > /sys/class/leds/${LED}/delay_off
		;;


	*)
		exit 1
		;;
esac

exit 0




