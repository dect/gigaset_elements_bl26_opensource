#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

if [ -z "$1" ]; then 
              echo usage: $0 your.machine.ip.address
              echo default port is 5600
              echo you have to start server on your machine first
              echo run nc -w 30 -p 5600 -l \> backup.file.name
              echo on your machine
              exit
fi

nc -w 2 $1 5600 < /mnt/data/nvs.bin


