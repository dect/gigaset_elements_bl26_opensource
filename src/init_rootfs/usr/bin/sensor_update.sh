#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

if [ -z "$1" ]; then 
              echo usage: $0 sensorId file
              exit
fi

echo \{\"cmd\":\"fwupdate\",\"devId\":\"$1\",\"file\":\"$2\"\} > /var/sensor_update.json


sender 127.0.0.1 ulecontrol /var/sensor_update.json


