#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

# $1 - destination dir to make copy of dump (i.e. flash filesystem)

NUM_OF_DUMPS_TO_KEEP=5
DATE=`date +%Y-%m-%d_%H%M%S`
WORKDIR="/tmp/sysdump"
DUMPFILE="/tmp/sysdump${DATE}.tar"

ARCHIVEDIR=$1
if [ -n "${ARCHIVEDIR}" ]; then
	ARCHIVEFILE="${ARCHIVEDIR}/sysdump${DATE}.tar"
	echo "Will copy to ${ARCHIVEFILE}"
fi

rm -rf ${WORKDIR}
mkdir -p ${WORKDIR}
mkdir -p ${WORKDIR}/mnt/data

echo "Copying /mnt/data..."
cp -r /mnt/data ${WORKDIR}/mnt

# do not archive recursively previous sysdumps
rm -f ${WORKDIR}/mnt/data/sysdump*.tar

echo "Process list..."
ps > ${WORKDIR}/ps.txt

echo "Syslog..."
logread > ${WORKDIR}/syslog.log

echo "Factory and env..."
dd if=/dev/mtdblock8 of=${WORKDIR}/mtdblock8.bin
dd if=/dev/mtdblock9 of=${WORKDIR}/mtdblock9.bin

echo "/proc..."
cat /proc/meminfo > ${WORKDIR}/meminfo.txt
cat /proc/slabinfo > ${WORKDIR}/slabinfo.txt
cat /proc/maps > ${WORKDIR}/maps.txt
uptime > ${WORKDIR}/uptime.txt
dmesg > ${WORKDIR}/dmesg.txt
arp -anv > ${WORKDIR}/arp.txt
cat /proc/version > ${WORKDIR}/version.txt
ifconfig > ${WORKDIR}/ifconfig.txt

echo
echo "Sysdump created in ${WORKDIR}"
echo
echo "Creating TAR archive..."
tar c -C ${WORKDIR} -f ${DUMPFILE} .
echo "You can use netcat to transfer it to PC:"
echo "# cat ${DUMPFILE} | nc 172.29.0.2 12345"

if [ -n "${ARCHIVEDIR}" ]; then
	echo
	echo "Copying ${DUMPFILE} to ${ARCHIVEFILE}"
	cp ${DUMPFILE} ${ARCHIVEFILE}
	echo
	echo "Delete old sysdumps on ${ARCHIVEDIR}, will preserve first ${NUM_OF_DUMPS_TO_KEEP} dumps"
	DUMPNO=1 
	ls -c -1 ${ARCHIVEDIR}/sysdump*.tar | while read FNAME; do
		if [ ${DUMPNO} -gt ${NUM_OF_DUMPS_TO_KEEP} ]; then
			echo "Removing ${FNAME}..."
			rm -f ${FNAME}
		fi 
		DUMPNO=$((DUMPNO+1))
	done
	echo
fi





