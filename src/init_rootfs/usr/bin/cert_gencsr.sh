#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

. /usr/bin/cert_common.sh
if [ -f ${CSR} ]; then
    echo "CSR file:"
    echo -e "\t${CSR}"
    echo " exists. Not generating new one"
    exit 0
fi
if [ ! -f ${KEYPAIR} ]; then
    echo "!!! Error"
    echo "${KEYPAIR} not exists!"
    exit 2
fi

if [ ! -x ${OSSL} ]; then
    cert_copy_openssl.sh
    if [ ! -x ${OSSL} ]; then
	echo "No openssl found!"
	exit 1
    fi
fi

echo "Generating CSR ..."

${OSSL} req -config /etc/ssl/openssl.cnf -new -nodes -key ${KEYPAIR} -out ${CSR} -passin pass:${PASSWORD} -passout pass:${PASSWORD} -sha1 -subj "/CN=${CN}"
EXITCODE=$?

echo "Openssl csr: ${EXITCODE}"
if [ $EXITCODE -ne 0 ]; then
    echo "!!! Error"
    echo "Openssl csr: ${EXITCODE}"
    exit 3
fi


if [ -f ${CSR} ]; then
    ${OSSL} req -config /etc/ssl/openssl.cnf -noout -verify -in ${CSR}
    #-text
else
    echo "!!! Error"
    echo "CSR verify failed!"
    exit 4
fi
