#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

. /usr/bin/cert_common.sh
echo ============== ${CERT} ================
${OSSL} x509 -noout -text -in ${CERT}
echo ============== ${KEYPAIR} ================
${OSSL} rsa -noout -text -in ${KEYPAIR}
echo ============== ${CSR} ================
${OSSL} req -config /etc/ssl/openssl.cnf -noout -text -in ${CSR}

echo ==============================
echo ==============================

echo ============== ${CERT} ================
${OSSL} x509 -noout -modulus -in ${CERT}
# | openssl md5
echo ============== ${KEYPAIR} ================
${OSSL} rsa -noout -modulus -in ${KEYPAIR}
# | openssl md5
echo ============== ${CSR} ================
${OSSL} req -config /etc/ssl/openssl.cnf -modulus -noout -in ${CSR}
# | openssl md5
echo =======================================
   
