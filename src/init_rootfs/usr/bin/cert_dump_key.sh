#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

. /usr/bin/cert_common.sh
${OSSL} rsa -noout -text -in ${KEYPAIR}
