#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

if [ -z "$1" ]; then
	echo Usage: $0 pid
	exit
fi
PID=$1

DEST_DIR="/tmp/maps/${PID}"
DEST_TAR="/tmp/maps_${PID}.tar"

echo "Removing old files..."
rm -f -r  ${DEST_DIR}
rm -f ${DEST_TAR}


echo "Dumping memory"
mkdir -p ${DEST_DIR}

cat /proc/${PID}/maps | cut -f 1 -d " " | tr "-" " " | while read START END; do
    sandbox dmem $START $END > ${DEST_DIR}/map_${PID}_${START}-${END}.bin
done

echo "Creating TAR '${DEST_TAR} ..."
tar -c -C ${DEST_DIR} -f ${DEST_TAR} .

