#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

DEVICEID=${deviceid}
CERTDIR="/mnt/data/cert"
#export OPENSSL_CONF=/etc/ssl/openssl.cnf
export RANDFILE="/mnt/data/cert/.rnd"
OSSL="/bin/openssl"
BASEURL="https://api-dev.gc.lovelysystems.com"

PASSWORD="alamakota"
CERTFILE="${CERTDIR}/cert"

CN="${DEVICEID}"

CSR=${CERTFILE}.csr
KEYPAIR=${CERTFILE}.key
PUB=${CERTFILE}.pub
CERT=${CERTFILE}.crt


