#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

if [ -z "$1" ]; then 
              echo usage: $0 sensorId
              exit
fi

echo \{\"cmd\":\"delete\",\"devId\":\"$1\"\} > /var/delete_sensor.json
sender 127.0.0.1 ulecontrol /var/delete_sensor.json


