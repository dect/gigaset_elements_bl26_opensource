#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


. fw_lib

DEVICE=$1
VERSION=$2

DIR=${FIRMWARE_DIR}/${DEVICE}/${VERSION}

echo "Installing firmware:"
echo " device: ${DEVICE}"
echo " version: ${VERSION}"

cd ${DIR}
./start	install ${VERSION}




