#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


. fw_lib

DEVICE_ID=$1

ID=$$.${RANDOM}

FIRMWARE_LIST_FILE=/tmp/${ID}.firmware_list
TMP_FILE=/tmp/${ID}.tmp



# lock the system
lock 

echo "Removing firmware for sensor: ${DEVICE_ID}"


# create list of directories with (possible) firmware
if [ -d ${FIRMWARE_DIR} ]; then
	DIRS=`ls -R ${FIRMWARE_DIR} | grep / | tr -d :`
fi

# find all files with firmware <-> sensors list
for DIR in ${DIRS}; do
	if [ -f ${DIR}/${SENSORS_LIST_FILE} ]; then
		echo ${DIR}/${SENSORS_LIST_FILE} >> ${FIRMWARE_LIST_FILE}
    fi
done


# create list with firmware <-> sensor list
if [ -f ${FIRMWARE_LIST_FILE} ]; then
	FIRMWARE_LIST=`cat ${FIRMWARE_LIST_FILE}`
fi


# remove given sensor from firmware lists 
for ITEM in ${FIRMWARE_LIST}; do
	cat ${ITEM} | grep -v -w -e ${DEVICE_ID} > ${TMP_FILE}
	cp ${TMP_FILE} ${ITEM}
	rm -f ${TMP_FILE}
done

# find useless firmware files and delete it
for ITEM in ${FIRMWARE_LIST}; do
	NOT_EMPTY=`cat ${ITEM} | grep -c ''`
	if [ "${NOT_EMPTY}" -eq 0 ]; then
		USELESS_DIR=`dirname ${ITEM}`		
		echo "Directory ${USELESS_DIR} is useless - it will be removed"
		rm -rf ${USELESS_DIR}
	fi			
done

# find empty directories and remove them
while true; do
	AGAIN=false	
	for DIR in ${DIRS}; do
		if [ -d ${DIR} ]; then
			NOT_EMPTY=`ls ${DIR} | grep -c ''`
			if [ "${NOT_EMPTY}" -eq 0 ]; then
				echo "Directory ${DIR} is empty - it will be removed" 
				rm -rf ${DIR}
				AGAIN=true
			fi
		fi
	done
	
	if [ "${AGAIN}" = false ]; then
		rm -f ${FIRMWARE_LIST_FILE}
		rm -f ${TMP_FILE}
		unlock		
		exit 0
	fi		 
done