#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

if [ -z "$1" ]; then 
              echo usage: $0 sensorId
              exit
fi

echo \{\"cmd\":\"gotoSleep\",\"devId\":\"$1\"\} > /var/sleep.json
./sender 127.0.0.1 ulecontrol /var/sleep.json


