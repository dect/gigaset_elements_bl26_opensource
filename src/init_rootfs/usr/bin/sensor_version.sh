#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

if [ -z "$1" ]; then 
              echo usage: $0 sensorId
              exit
fi

echo \{\"cmd\":\"verreq\",\"devId\":\"$1\"\} > /var/verreq.json
sender 127.0.0.1 ulecontrol /var/verreq.json


