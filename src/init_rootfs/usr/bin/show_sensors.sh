#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


if [ -f /mnt/data/endnodes ]; then
	cat /mnt/data/endnodes	
else
	echo "Endnodes database is empty!"	
fi