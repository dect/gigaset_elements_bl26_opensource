#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


. fw_lib


# lock the system
lock 


# create list of directories with (possible) firmware
if [ -d ${FIRMWARE_DIR} ]; then
	DIRS=`ls -R ${FIRMWARE_DIR} | grep / | tr -d :`
fi

# find all files with firmware <-> sensors list
for DIR in ${DIRS}; do
	if [ -f ${DIR}/${SENSORS_LIST_FILE} ]; then
		SENSOR_LIST=`cat ${DIR}/${SENSORS_LIST_FILE}`
		FIRMWARE_LIST=`ls ${DIR} | grep -v -w ${SENSORS_LIST_FILE}`
		for SENSOR in ${SENSOR_LIST}; do
			for FIRMWARE in ${FIRMWARE_LIST}; do
				announce ${SENSOR} ${DIR}/${FIRMWARE}
			done
		done
	fi
done

unlock