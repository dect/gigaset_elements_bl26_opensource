#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.



LAN_OK="/var/run/lan.ok"


# Send network status to JBus clients
#
# expected parameters:
#	$1 - network status (true, false)
send_lan_status()
{
    if [ "$1" = "true" ]; then
       touch ${LAN_OK}
    else
       rm ${LAN_OK}
    fi

	if [ ! -f /var/run/reef_stop ]; then
		echo -e "{\"link\":$1}" | /usr/bin/sender 127.0.0.1 network
	fi
}