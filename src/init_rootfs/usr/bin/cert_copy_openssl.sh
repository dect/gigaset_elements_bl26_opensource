#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

if [ -e /mnt/data/openssl ]; then
    echo "Copying Openssl..."
    cp /mnt/data/openssl /tmp
else
    echo "/mnt/data/openssl not found !"
fi
