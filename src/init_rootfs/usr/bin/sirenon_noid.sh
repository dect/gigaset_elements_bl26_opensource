#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


for SIREN in $(show_sensors.sh | grep 'is01' | cut -d: -f1 | cut -d\" -f2)
do
    sirenon.sh $SIREN
done
