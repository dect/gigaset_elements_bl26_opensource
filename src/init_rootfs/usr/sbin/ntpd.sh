#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.



start()
{
	ntpd_daemon.sh &
}

stop()
{
    killall -q ntpd
    killall -q ntpd_daemon.sh
}

case "$1" in
	once)
		ntpd -n -q -p de.pool.ntp.org
		;; 
		
	start)
		start
		;;
	
	stop)
		stop
		;;
	
	restart)
		stop
		start
		;;	
	
	
	*)
		echo "Usage: $0 (start|stop|restart|once)"
		;;
esac
