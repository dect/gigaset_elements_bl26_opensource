#!/bin/sh
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.


. /usr/bin/led_lan.sh

NTP_PEER="de.pool.ntp.org"

LOG() {
    echo "$@" | logger -s -t "ntpd.sh"
}

# sleep is provided by local loop, to do not leave background sleep process when this script will be killed
sleep_1d()
{
    LOG "Sleeping for 1 day..."
    MINUTES_LEFT=1440
    while [ ${MINUTES_LEFT} -gt 0 ]; do
	sleep 60
	MINUTES_LEFT=$((MINUTES_LEFT-1))
#	LOG "Minutes left: ${MINUTES_LEFT}"
    done
}


ntp_synchronize_time_once()
{
    killall -q ntpd 

	# start ntp and try to daemonize. NTPD will exit after time is set
	# if there is DNS problem, ntpd exits immediately without retrying, so make own retry loop
	EXIT=1
	while [ ${EXIT} -ne 0 ]; do
	    LOG "Starting ntpd to synchronize time"
	    OUT=`ntpd -q -p ${NTP_PEER} 2>&1`
	    EXIT=$?
	    if [ $EXIT -ne 0 ]; then
		LOG "${OUT}"
	    fi
	    sleep 10
	done
	send_lan_status true
}	





while [ true ]; do
	ntp_synchronize_time_once
	
	# sleep a week
	sleep_1d	#1
	sleep_1d	#2
	sleep_1d	#3
	sleep_1d	#4
	sleep_1d	#5
	sleep_1d	#6
	sleep_1d	#7
done
