#!/bin/sh
#
# STand-Alone ULEAPP: Kill background uleapp and restart it
#

ULEAPP_BIN="uleapp"

ULEAPP_OPTIONS="-dmxc -v -w -6"
ULEAPP_OPTIONS_GIGA94="-G"
ULEAPP_OPTIONS_USER=""

ARG_RMMOD=0
ARG_KILLREEF=0
ARG_ULEAPP_TFTP=0
ARG_USE_MENUMODE=0
ARG_GIGA94_FRESH=0

printUsage() {
    echo "Usage: ${0##*/} [OPTIONs] [ULEAPP-OPTIONS]"
    echo "Kill background uleapp and restart it with new arguments."
    echo "Uleapp is restarted in background mode unless -i option is given."
    echo "Additional uleapp options can be given at the end of the command-line"
    echo "Default options are: ${ULEAPP_OPTIONS}" 
    echo "OPTIONs"
    echo "  -t  TFTP uleapp from UBoot serverip and use that"
    echo "  -T  TFTP uleapp from UBoot serverip but do NOT relaunch it"
    echo "  -m  Unload kernel module"
    echo "  -k  Kill all S40reef apps. Then start jbus server."
    echo "  -i  Restart in menu/interactive mode"
    echo "  -0  Restart without default options"
    echo "  -g  Start in giga94 mode (${ULEAPP_OPTIONS_GIGA94})"
    echo "  -G  Delete nvs.bin, unload module, wait 5s, start in giga94 mode"
    echo "  -h  Print this help"
}

while [ ! -z $1 ]; do
    case $1 in
        -t) ARG_ULEAPP_TFTP=1 ;;
        -T) ARG_ULEAPP_TFTP=2 ;;
        -m) ARG_RMMOD=1 ;;
        -k) ARG_KILLREEF=1 ;;
        -i) ARG_USE_MENUMODE=1 ;;
        -0) echo "Launching uleapp without options" && ULEAPP_OPTIONS="" ;;
        -g) ULEAPP_OPTIONS="${ULEAPP_OPTIONS_GIGA94}" && ARG_USE_MENUMODE=1;;
        -G) ARG_GIGA94_FRESH=1 ;;
        -h) printUsage && exit 0 ;;
        *)  ULEAPP_OPTIONS_USER="${ULEAPP_OPTIONS_USER} $1" ;;
    esac
    shift
done

killall uleapp

ULEOPT=`fw_printenv -n uleopt 2>/dev/null`

# Simulating a fresh board without nvs.bin, being tested for the first time
if [ ${ARG_GIGA94_FRESH} -eq 1 ]; then
    rmmod rtxdect452
    rm -v /mnt/data/nvs.bin
    for i in 5 4 3 2 1; do
        echo $i
        sleep 1
    done
    ${ULEAPP_BIN} ${ULEAPP_OPTIONS_GIGA94} ${ULEAPP_OPTIONS_USER}
    exit
fi

if [ ${ARG_RMMOD} -eq 1 ]; then
    rmmod rtxdect452
fi

if [ ${ARG_KILLREEF} -eq 1 ]; then
    killall wdt
    /etc/init.d/S40reef.sh stop
    jbus &
fi

if [ ${ARG_ULEAPP_TFTP} -ne 0 ]; then
    HOST=`fw_printenv -n serverip 2>/dev/null | grep "^[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*"`
    echo "Downloading uleapp with TFTP from ${HOST}"
    tftp -g -r uleapp -l /tmp/uleapp ${HOST} && chmod +x /tmp/uleapp
    ULEAPP_BIN=/tmp/uleapp
    if [ ${ARG_ULEAPP_TFTP} -eq 2 ]; then
        ULEAPP_OPTIONS="-h"
    fi
fi

# Relaunch uleapp
ULEAPP_OPTIONS="${ULEAPP_OPTIONS} ${ULEAPP_OPTIONS_USER}"
echo "Launching: ${ULEAPP_BIN} ${ULEAPP_OPTIONS}"
if [ ${ARG_USE_MENUMODE} -eq 1 ]; then
    ${ULEAPP_BIN} ${ULEAPP_OPTIONS} ${ULEOPT} -i
else
    ${ULEAPP_BIN} ${ULEAPP_OPTIONS} ${ULEOPT} &
fi
