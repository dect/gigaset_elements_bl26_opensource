include config.mk

##############################
# Build configuration for HW #
############################## 
ifeq ($(BUILD),hw)

mrproper:
	make clean
	rm -rf $(OUT_BAS_DIR)
	rm -rf $(OUT_BAS_DIR)_fwupgrade
	rm -rf $(OUT_BAS_DIR)_winhost
	rm -rf $(OUT)
	rm -rf $(OUT_COMMON)
	rm -rf $(DOXYGEN_DIR)
	rm -rf $(ROOTFS)
	rm -rf $(TMP_ROOTFS)
	rm -rf $(DATAFS)


dirs:
	mkdir -p $(OUT)
#	mkdir -p $(OUT_BAS_DIR)
#	mkdir -p $(OUT_BAS_DIR)/hw
	mkdir -p $(DOXYGEN_DIR)
	mkdir -p $(DOXYGEN_DIR)/images
	mkdir -p $(ROOTFS)
	mkdir -p $(TMP_ROOTFS)
	mkdir -p $(GDB_DIR)
	mkdir -p $(ROOTFS)/proc
	mkdir -p $(ROOTFS)/sys
	mkdir -p $(ROOTFS)/mnt/ramfs
	mkdir -p $(ROOTFS)/usr/bin
	mkdir -p $(ROOTFS)/bin
	mkdir -p $(ROOTFS)/lib
	mkdir -p $(DATAFS)	
	cp -R $(INIT_DATAFS)/* $(DATAFS)


rootfs:
	mkdir -p $(ROOTFS)/mnt/data
	mkdir -p $(ROOTFS)/var
	mkdir -p $(ROOTFS)/tmp
	mkdir -p $(ROOTFS)/etc/network/if-pre-up.d
	mkdir -p $(ROOTFS)/etc/network/if-up.d
	mkdir -p $(ROOTFS)/etc/network/if-down.d
	mkdir -p $(ROOTFS)/etc/network/if-post-down.d
	cp -R $(INIT_ROOTFS)/* $(ROOTFS)
#   Initial date
configure:
	cd $(TOP)/../tools && ./update_libc.sh
	cp -f $(TOP)/opensource/busybox/.config-hw $(TOP)/opensource/busybox/.config
	cp -f $(TOP)/opensource/linux/.config-hw $(TOP)/opensource/linux/.config
	make OPENSOURCE=dmalloc configure
	make OPENSOURCE=dmalloc install	# needed by openssl install
	make OPENSOURCE=linux configure
	make OPENSOURCE=json-c configure
	make OPENSOURCE=busybox configure
	make OPENSOURCE=openssl configure
	make OPENSOURCE=openssl install
	make OPENSOURCE=curl configure


clean:
	make DIALOG=uart clean
	make DIALOG=cr16boot clean
	make DIALOG=loader clean
	make OPENSOURCE=dmalloc clean
	make OPENSOURCE=linux clean
	make OPENSOURCE=busybox clean
	make OPENSOURCE=openssl clean
	make OPENSOURCE=curl clean
	make OPENSOURCE=json-c clean
	make OPENSOURCE=u-boot-env-tools clean
	rm -rf $(OUT)
#	rm -rf $(DOXYGEN_DIR)
#	rm -rf $(DOXYGEN_DIR)/images
	rm -rf $(ROOTFS)
	rm -rf $(DATAFS)
	rm -rf $(TMP_ROOTFS)
	rm -rf $(GDB_DIR)
	make dirs

all: rootfs
	make OPENSOURCE=dmalloc install
	make OPENSOURCE=busybox install
	make OPENSOURCE=openssl install	
	make OPENSOURCE=curl install
	make OPENSOURCE=json-c install
	make OPENSOURCE=certs install

	make DIALOG=loader install 
	make DIALOG=cr16boot install
	make DIALOG=uart install
	make OPENSOURCE=u-boot-env-tools install # depends on cr16boot
	make OPENSOURCE=linux vmlinux
	make OPENSOURCE=linux modules
	make OPENSOURCE=linux modules_install
#	make OPENSOURCE=linux move_modules_to_jffs
ifdef TFTP_DIR
	cp $(OUT_BUILD)/opensource/linux/vmlinuz ${TFTP_DIR}
endif
	make prepare_for_baseline

kernel:
	make OPENSOURCE=linux vmlinux
	make OPENSOURCE=linux modules
	make OPENSOURCE=linux modules_install
ifdef TFTP_DIR
	cp $(OUT_BUILD)/opensource/linux/vmlinuz ${TFTP_DIR}
endif



program:
	@if [ !  -e $(OUT_BAS_DIR)/uniprog/uniprog.sh ]; then \
	    echo "\n\nPlease 'make all' first\n\n"; \
	    exit 1; \
	fi;
	cd $(OUT_BAS_DIR)/uniprog && \
	./uniprog.sh

start_shell:
	bash
 
prepare_for_baseline:
	mkdir -p $(OUT_BAS_DIR)/$(BUILD)/gdb
	cp -rf $(GDB_DIR) $(OUT_BAS_DIR)/$(BUILD)
	find $(OUT)/ -name "*.gdb" -exec cp {} $(OUT_BAS_DIR)/$(BUILD)/gdb \;
	find ./opensource/u-boot-env-tools -name "*.gdb" -exec cp {} $(OUT_BAS_DIR)/$(BUILD)/gdb \;
	find ./opensource/busybox -name "*.gdb" -exec cp {} $(OUT_BAS_DIR)/$(BUILD)/gdb \;
	find $(TOP)/../tools/cr16-tools/cr16-uclinux/runtime/usr/lib  -name "*.gdb" -exec cp {} $(OUT_BAS_DIR)/$(BUILD)/gdb \;
	cp -f $(TOP)/opensource/linux/vmlinux $(OUT_BAS_DIR)/$(BUILD)/gdb
	cp -f $(OUT_BUILD)/opensource/linux/vmlinuz	$(OUT_BAS_DIR)/reef_vmlinuz
	cd $(OUT_BAS_DIR) && sha256sum reef_vmlinuz > $(OUT_BAS_DIR)/reef_vmlinuz.sum
	@if [ ! -e $(OUT_BAS_DIR)/vmlinuz-recovery ]; then \
	    if [ -e $(OUT_COMMON)/hw_recovery/opensource/linux/vmlinuz ]; then \
		cp -f $(OUT_COMMON)/hw_recovery/opensource/linux/vmlinuz $(OUT_BAS_DIR)/reef_vmlinuz-recovery &>/dev/zero || true; \
	    fi \
	fi
	cp -rf $(TMP_ROOTFS)/include $(OUT_BAS_DIR)/$(BUILD)
	cp -rf $(OUT_COMMON)/uniprog $(OUT_BAS_DIR)
	cp -rf ../tools/mtd-utils $(OUT_BAS_DIR)

.PHONY: dirs mrproper baseline configure clean doxygen prepare_for_baseline baseline_tag

endif	# HW configuration
	
