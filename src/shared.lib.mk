


GDB_DIR := $(TOP)/../out/gdb



# libc library
LIBC_NO := 1
export LIBC_GDB = $(shell $(TOP)/../tools/cr16-tools/bin/$(CC) $(CFLAGS) -print-file-name=libc.gdb)

# posix threads
LIB_PTHREADS_NO := 2
export PTHREAD_GDB = $(shell $(TOP)/../tools/cr16-tools/bin/$(CC) $(CFLAGS) -print-file-name=libpthread.gdb)


# DECT ULE
LIB_ULE_NO := 3


# json-c
#export LIB_JSON_C_NO := 4
#export LIB_JSON_C_GDB = $(GDB_DIR)/lib$(LIB_JSON_C_NO).so.gdb


# JBus
export LIB_JBUS_NO := 4
export LIB_JBUS_GDB = $(GDB_DIR)/lib$(LIB_JBUS_NO).so.gdb


# COMA
export LIB_COMA_NO := 5
export LIB_COMA_GDB = $(GDB_DIR)/lib$(LIB_COMA_NO).so.gdb

