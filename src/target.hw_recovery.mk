include config.mk

##############################
# Build configuration for HW #
############################## 
ifeq ($(BUILD),hw_recovery)

mrproper:
	make clean
	make OPENSOURCE=linux clean
	make OPENSOURCE=busybox clean
	make OPENSOURCE=u-boot-env-tools clean
	rm -rf $(OUT_BAS_DIR)
	rm -rf $(OUT)
	rm -rf $(OUT_COMMON)
	rm -rf $(ROOTFS)
	rm -rf $(TMP_ROOTFS)


dirs:
	mkdir -p $(OUT)
	mkdir -p $(OUT_BAS_DIR)
	mkdir -p $(OUT_BAS_DIR)/hw_recovery
	mkdir -p $(ROOTFS)
	mkdir -p $(TMP_ROOTFS)
	mkdir -p $(ROOTFS)/proc
	mkdir -p $(ROOTFS)/sys
	mkdir -p $(ROOTFS)/var
	mkdir -p $(ROOTFS)/tmp
	mkdir -p $(ROOTFS)/usr/bin
	cp -R $(INIT_RECOVERYFS)/* $(ROOTFS)

configure:
	cp -f $(TOP)/opensource/busybox/.config-hw_recovery $(TOP)/opensource/busybox/.config
	cp -f $(TOP)/opensource/linux/.config-hw_recovery $(TOP)/opensource/linux/.config
	make OPENSOURCE=linux configure
	make OPENSOURCE=busybox configure

clean:
	make DIALOG=loader clean
	make DIALOG=cr16boot clean
	rm -rf $(OUT)
	rm -rf $(ROOTFS)
	rm -rf $(TMP_ROOTFS)
	make dirs

all:
	make DIALOG=loader install
	make DIALOG=cr16boot install
	make OPENSOURCE=u-boot-env-tools install
	make OPENSOURCE=busybox install
	make OPENSOURCE=linux vmlinux
ifdef TFTP_DIR
	cp $(OUT_BUILD)/opensource/linux/vmlinuz ${TFTP_DIR}/vmlinuz-recovery
endif

baseline: clean all
	mkdir -p $(OUT_BAS_DIR)/$(BUILD)/gdb
	git log -5 >$(OUT_BAS_DIR)/$(BUILD)/git_status.txt
	git status >>$(OUT_BAS_DIR)/$(BUILD)/git_status.txt
	date >$(OUT_BAS_DIR)/$(BUILD)/date.txt
	cp ./opensource/linux/reef_version.h $(OUT_BAS_DIR)/$(BUILD)
	find ./opensource/u-boot-env-tools -name "*.gdb" -exec cp {} $(OUT_BAS_DIR)/$(BUILD)/gdb \;
	find ./opensource/busybox -name "*.gdb" -exec cp {} $(OUT_BAS_DIR)/$(BUILD)/gdb \;
	cp -f $(OUT_BUILD)/opensource/linux/vmlinuz     $(OUT_BAS_DIR)/reef_vmlinuz-recovery
	cd $(OUT_BAS_DIR) && \
	sha256sum reef_vmlinuz-recovery > $(OUT_BAS_DIR)/reef_vmlinuz-recovery.sum

endif	# HW configuration
	
