ifeq ($(BUILD),hw)
    BUILD_THIS := 1
else ifeq ($(BUILD),hw_recovery)
    BUILD_THIS := 1
else
    BUILD_THIS := 0
endif

ifeq ($(BUILD_THIS),1)

CFLAGS := 
LDFLAGS :=  


install: all service
	mkdir -p $(OUT_COMMON)/uniprog
	cp cr16boot/image452.bin $(OUT_COMMON)/uniprog 
	cp cr16boot/image452_service.bin $(OUT_COMMON)/uniprog 


all:
	cd cr16boot && \
	make sc14452reef_32MB_config && \
	make all && \
	make image_452

service:
	cd cr16boot && \
	make sc14452reef_32MB_service_config && \
	make all && \
	make image_452_service


clean: 
	cd cr16boot && \
	make clean
	
endif	# HW configuration