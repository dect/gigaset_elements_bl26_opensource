ifeq ($(BUILD),hw)

CFLAGS := 
LDFLAGS :=  


install: all
	mkdir -p $(OUT_COMMON)/uniprog
	cp uart/host/uartfp $(OUT_COMMON)/uniprog/
	cp uart/target/452fp.bin $(OUT_COMMON)/uniprog/

all: 
	cd uart/host &&\
	make all
	cd uart/target &&\
	make all


clean: 
	cd uart/host &&\
	make clean

endif	# HW configuration