/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <vassilis.maniotis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 * See http://www.gnu.org/licenses/gpl-2.0.txt for more details.
 */
#ifndef SITEL_IO_H
#define SITEL_IO_H

typedef unsigned char      uint8;
typedef char               int8;
typedef unsigned short     uint16;
typedef short              int16;
typedef unsigned long int  uint32;
typedef long int           int32;

typedef unsigned char      bool;

typedef enum
    {
        FALSE  = 0,
        TRUE   = 1
    } BOOLEAN, BOOL;

#ifndef NULL
#define NULL (uint8*)(-1)
#endif

#define __data24


typedef unsigned char      BYTE;
typedef unsigned short     WORD;
typedef unsigned long int  DWORD;

#define __far

#if defined SC14450
   #include "sc14450.h"
#elif defined SC14452
   #include "sc14452.h"
#else
#error NO device IO include file
#endif

#endif
