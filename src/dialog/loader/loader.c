#include "asm.h"
#include "sitel_io.h"

#define AHB_CLK_5MHz	1
#define AHB_CLK_41MHz	2
#define AHB_CLK_82MHz	3

#define PLL1_CLK_164MHz 0
#define PLL1_CLK_82MHz  1


#define	SDRAM_START_PROGRAM 0xF00000

#if defined SC14450
	#define spi_mem_cs_low() SetWord(P0_RESET_DATA_REG, GPIO_10)
	#define spi_mem_cs_high() SetWord(P0_SET_DATA_REG, GPIO_10)
#elif defined SC14452
	#define spi_mem_cs_low() SetWord(P0_RESET_DATA_REG, GPIO_3)
	#define spi_mem_cs_high() SetWord(P0_SET_DATA_REG, GPIO_3)
#endif

#ifdef SC14452
#define SPI2_CTRL_REG SPI2_CTRL_REG0
#define __SPI2_CTRL_REG __SPI2_CTRL_REG0
#endif

#define CLK_1296	0x00
#define CLK_2592	0x01
#define CLK_5184	0x02  
#define CLK_1152	0x03  

#define MODE_0		0x00
#define MODE_1		0x01
#define MODE_2		0x02
#define MODE_3		0x03

#define MODE_8BITS  0x00
#define MODE_16BITS 0x01
#define MODE_32BITS 0x02

#define FLASH_MEMORY_SIZE		0x200000
#define	MAX_READY_WAIT_COUNT	1000000

#define	SPI_FLASH_WREN_CMD			0x06	/* Write Enable */
#define	SPI_FLASH_WRDI_CMD			0x04	/* Write Disable */
#define	SPI_FLASH_RDID_CMD			0x9F	/* Read Identification */
#define	SPI_FLASH_RDSR_CMD			0x05	/* Read Status Register */
#define	SPI_FLASH_WRSR_CMD			0x01	/* Write Status Register */
#define	SPI_FLASH_READ_CMD			0x03	/* Read Data Bytes */
#define	SPI_FLASH_READ_FAST_CMD		0x0B	/* Read Data Bytes in higher speed */
#define	SPI_FLASH_PP_CMD			0x02	/* Page Program */
#define	SPI_FLASH_SE_CMD			0xD8	/* Sector Erase */
#define	SPI_FLASH_BE_CMD			0xC7	/* Bulk Erase */
#define SPI_FLASH_DP_CMD			0xB9	/* Deep Power Down */
#define	SPI_FLASH_RES_CMD			0xAB	/* Release from Deep Power Down */
#define	SPI_FLASH_SE_32K_CMD		0x52	/* Sector Erase 32Kbytes*/
#define	SPI_FLASH_SE_4K_CMD			0x20	/* Sector Erase 4KBytes*/

/* Status Register Bits */

#define STATUS_BUSY		0x01
#define	STATUS_WEL		0x02
#define	STATUS_BP0		0x04
#define	STATUS_BP1		0x08
#define	STATUS_BP2		0x10
#define	STATUS_WP		0x80

#define ERR_OK					0
#define ERR_TIMOUT				-1
#define ERR_NOT_ERASED				-2
#define ERR_PROTECTED				-3
#define ERR_INVAL				-4
#define ERR_ALIGN				-5
#define ERR_UNKNOWN_FLASH_VENDOR		-6
#define ERR_UNKNOWN_FLASH_TYPE			-7
#define ERR_PROG_ERROR				-8

void init_sdram (unsigned char ahb_clk) 
{
	/* configure GPIOs as SDRAM interface */
#if defined( CONFIG_RAM_SIZE_32MB )
	SetWord( P2_07_MODE_REG,0x031c );	/* P2_07 is AD13 */
#endif
	SetWord( P0_15_MODE_REG,0x0330 );	/* P0_15 is ACS4 */
	SetWord( P1_03_MODE_REG,0x031d );	/* P1_03 is SD_A10 */
	SetWord( P1_12_MODE_REG,0x031e );	/* P1_12 is SD_CLK */
	SetWord( P1_11_MODE_REG,0x031d );	/* P1_11 is SD_CKE */
	SetWord( P1_04_MODE_REG,0x031d );	/* P1_04 is SD_WEN */
	SetWord( P1_05_MODE_REG,0x031d );	/* P1_05 is SD_BA0 */
	SetWord( P1_06_MODE_REG,0x031d );	/* P1_06 is SD_BA1 */
	SetWord( P1_07_MODE_REG,0x031d );	/* P1_07 is SD_LDQM */
	SetWord( P1_08_MODE_REG,0x031d );	/* P1_08 is SD_UDQM */
	SetWord( P1_09_MODE_REG,0x031d );	/* P1_09 is SD_RAS */
	SetWord( P1_10_MODE_REG,0x031d );	/* P1_10 is SD_CAS */

	/* sdram controller configuration */
	/* base address is 0x20000 */
	SetDword( EBI_ACS4_LOW_REG,0x00020000 );
#if defined( CONFIG_RAM_SIZE_32MB )
	/* sdram size is 32MB */
	SetDword( EBI_ACS4_CTRL_REG,0x0000000a );
	SetDword( EBI_SDCONR_REG,0x00001188 );
#else
	/* sdram size is 16MB */
	SetDword( EBI_ACS4_CTRL_REG,0x00000009 );
	SetDword( EBI_SDCONR_REG,0x00001168 );
#endif
	
	if (ahb_clk == AHB_CLK_82MHz)
		SetDword( EBI_SDREFR_REG,0x0011050c );
	else if (ahb_clk == AHB_CLK_41MHz)
		SetDword( EBI_SDREFR_REG,0x00110286 );
	else if (ahb_clk == AHB_CLK_5MHz)
		SetDword( EBI_SDREFR_REG,0x00110050 );
	
	SetDword( EBI_SDEXN_MODE_REG,0x00000022 );
	
	SetDword( EBI_SDCTLR_REG,0x00003049 );
	while(GetWord(EBI_SDCTLR_REG)& 1);
}

#if defined SC14450

void init_pll(void)
{

	unsigned i,temp;

	/* APB clock is HCLK/2 */
	SetWord(CLK_AMBA_REG,0x0010);
	/* AHB clock is HCLK */
	SetWord(CLK_AMBA_REG,0x0011);

#ifdef CLOCK_12MHz
	/* Xtal is 12.288 MHz PLL at 165.888 MHz VD = 3*9 XD = /2 */
	SetWord(CLK_PLL1_DIV_REG, 0x0019);
	/* we setup the PLL2 to give the 25MHz for the ethernet */
	/* for 12.288 MHz Xtal we use as XD/VD values 58/0 */ 
	SetWord(CLK_PLL2_DIV_REG,0x003a);	

	/* HF mode enable, VCO on , PLL charge pump on */
	SetWord(CLK_PLL1_CTRL_REG,0x0052);	
	SetWord(CLK_PLL2_CTRL_REG,0x0012);	

	/* wait500usec */
	for(i = 0; i < 0x100; ++i)
		temp = *(volatile unsigned*)0xff4000;

	SetWord(CLK_PLL2_CTRL_REG, 0x001a);	
	/* system clock in PLL mode */
	SetWord(CLK_PLL1_CTRL_REG,0x005a);
#else
	/* Xtal is 12.288 MHz PLL at 165.888 MHz VD = 3*9 XD = /2 */
	SetWord(CLK_PLL1_DIV_REG, 0x001c);
	/* we setup the PLL2 to give the 25MHz for the ethernet */
	/* for 12.288 MHz Xtal we use as XD/VD values 58/0 */ 
	SetWord(CLK_PLL2_DIV_REG,0x803e);	
//	SetWord(CLK_PLL2_DIV_REG,0x201b);	

	/* HF mode enable, VCO on , PLL charge pump on */
	SetWord(CLK_PLL1_CTRL_REG,0x0052);	
	SetWord(CLK_PLL2_CTRL_REG,0x0016);	
//	SetWord(CLK_PLL2_CTRL_REG,0x0012);	

	/* wait500usec */
	for(i = 0; i < 0x100; ++i)
		temp = *(volatile unsigned*)0xff4000;

	SetWord(CLK_PLL2_CTRL_REG, 0x001e);	
//	SetWord(CLK_PLL2_CTRL_REG, 0x001a);	
	/* system clock in PLL mode */
	SetWord(CLK_PLL1_CTRL_REG,0x005a);


#endif

	SetWord(CLK_DSP_REG,0x99);
	SetWord(CLK_CODEC_DIV_REG,0x00C8);
	SetWord(CLK_CODEC_REG,0x2D55);
	SetWord(CLK_AMBA_REG,0x01f1);

	/* Peripherial clock connected to PLL/2 at 82.944 MHz divide by 72 for 1.152 MHz */
	SetWord(CLK_PER_DIV_REG,0x00c8);	

	/* Codec clock connected to PLL/2 at 82.944 MHz divide by 72 for 1.152 MHz */
	SetWord(CLK_CODEC_DIV_REG,0x00c8);	

	/* Serial clock is 10.368MHz = 82.944MHz/8 */
	/* SPI clock is 20.736MHz = (82.944MHz / 2) / 2 */
	SetWord(CLK_PER10_DIV_REG,0x0048);	// PER20 /= 2, PER10 /= 1

	/* JTAG OWI clock is driven by XTAL */
	SetWord(CLK_AUX_REG,0x0020);

	/* Disable XTAL startup capacitences */
	SetWord(CLK_XTAL_CTRL_REG,0x0022);

	/* Change XTAL power from VDD_RF to LDO_XTAL */
	SetWord( CLK_XTAL_CTRL_REG,0x0023);	
	/* wait 200 usec */
	for(i = 0; i < 0x400; ++i)
		temp = *(volatile unsigned*)0xff4000;

	SetWord( CLK_XTAL_CTRL_REG,0x0021);	

}

#elif defined SC14452

void pll_1_on(unsigned char pll_clk)  /* PLL1 on, Used by CR16C, PROCEDURE FOR 10.368 Mhz */
{    
	volatile WORD i;

	// PLL CLOCK SWITCHING PROCEDURE

	if (pll_clk == PLL1_CLK_164MHz)
		SetWord(CLK_PLL1_DIV_REG, 0x1c);	   // x16
	else
		SetWord(CLK_PLL1_DIV_REG, 0x1d);           // x8
		
	SetWord(CLK_PLL1_CTRL_REG, (PLL1_HF_SEL | PLL1_VCO_ON | PLL1_CP_ON));

	if (pll_clk == PLL1_CLK_164MHz)
		SetBits(CLK_GLOBAL_REG, SW_XTAL_PLL1_RATE, 0x1); // XTAL - PLL1 rate = 16
	else 
		SetBits(CLK_GLOBAL_REG, SW_XTAL_PLL1_RATE, 0x0); // XTAL - PLL1 rate = 8

	for(i=0;i<50;i++);			   // Wait for approx. 200 us when in XTAL mode and HCLK_DIV=2.

	SetBits(CLK_AMBA_REG, SW_PCLK_DIV, 2);     // Divide by 2 (PCLK max = 41 MHz). Must be done in two steps.
	SetBits(CLK_AMBA_REG, SW_HCLK_DIV, 4);     // Divide by 4 (HCLK=40 MHz) before switching to PLL mode (SW workaround).
	SetBits(CLK_AMBA_REG, SW_HCLK_DIV, 4);     // Same as above .. just for delay until the div register really updates... (NM/07-08-2008).

	SetBits(CLK_GLOBAL_REG, SW_SWITCH_CLK, 0); // Go.

	while (GetWord (CLK_AUX1_REG) & SWITCH_TO_PLL); //Wait until all clocks switch to pll domain.

	SetBits(CLK_AMBA_REG, SW_HCLK_DIV, 2);     // Divide by 2 (HCLK=80 MHz), so put it back again in the HCLK max. frequency setting.
 }


void pll_2_on(void)  /* PLL2 on, in Can be used by QSPIC and EMAC, PROCEDURE FOR 10 Mhz */ 
{
    volatile WORD i;

    SetWord(CLK_PLL2_DIV_REG , PLL2_DIV5 | 62);             // VD=299, XD=62
    SetWord(CLK_PLL2_CTRL_REG, PLL2_VCO_ON | PLL2_CP_ON );  // VCO_ON | CP_ON 
    
    for(i=0;i<500;i++);                                     // Wait for a while.

}

#endif

// SPI Functions


void init_SPI2(void)
{ 
#if defined SC14450
  
	/* Set PPA MATRIX FOR SPI2 */
	SetPort(P0_12_MODE_REG, PORT_OUTPUT,  PID_SPI2_DOUT);   /* P0_12 as SPI_DO  */
	SetPort(P0_13_MODE_REG, PORT_INPUT,   PID_SPI2_DIN);    /* P0_13 as SPI_DI  */
	SetPort(P0_14_MODE_REG, PORT_OUTPUT,  PID_SPI2_CLK);    /* P0_14 as SPI_CLK */
	SetPort(P0_10_MODE_REG, PORT_OUTPUT,  PID_port);        /* P0_15 as SPI_EN  */  
	/* Set SPI SETTINGS */

#elif defined SC14452
	SetWord(P0_DATA_REG, GPIO_3 | GPIO_6 | GPIO_12 | GPIO_13 | GPIO_14);  // Note: QSPI.WPn=P0[11]=0
	SetPort(P0_03_MODE_REG, PORT_OUTPUT,  PID_port);	              // 452: Chip Select
	SetPort(P0_06_MODE_REG, PORT_OUTPUT,  PID_SPI2_CLK);	              // 452: use SPI clk the P0[6] (instead of P0[14])
	SetPort(P0_11_MODE_REG, PORT_OUTPUT,  PID_port);	              // 452: Write protect
	SetPort(P0_12_MODE_REG, PORT_OUTPUT,  PID_SPI2_DOUT);	              // Set PPA
	SetPort(P0_13_MODE_REG, PORT_PULL_UP, PID_SPI2_DIN);
	SetPort(P0_14_MODE_REG, PORT_OUTPUT,  PID_port);	              // 452: QSPI HOLDn pin (keep always high)

	// set SPI2_CLK = 20.736 MHz...
	if (GetWord(CLK_GLOBAL_REG) & SW_XTAL_PLL1_RATE)
		SetBits(CLK_GPIO2_REG, SW_SPI2_DIV, 8);
	else
		SetBits(CLK_GPIO2_REG, SW_SPI2_DIV, 4);

	// Stop spi2 clock
	SetBits(CLK_GPIO3_REG, SW_SPI2_EN,  0);  
	  
	// Enable spi2 clk
	SetBits(CLK_GPIO3_REG, SW_SPI2_EN,  1); 
#endif

	SetBits(SPI2_CTRL_REG, SPI_WORD, 0x00);                 // Set SPI 8bits Mode,  Serial Flash Device
	SetBits(SPI2_CTRL_REG, SPI_SMN,  0x00);                 // Set SPI IN MASTER MODE
	SetBits(SPI2_CTRL_REG, SPI_POL,  0x01);  
	SetBits(SPI2_CTRL_REG, SPI_PHA,  0x01);					// MODE 3: SPI2_POL=1  and PI2_PHA=1
	SetBits(SPI2_CTRL_REG, SPI_MINT, 0x01);                 // Disable SPI2_INT to ICU, timer0 is used
	SetBits(SPI2_CTRL_REG, SPI_ON, 1);                      // Enable SPI2 

	SetBits(SPI2_CTRL_REG, SPI_CLK,  CLK_5184);                 // SPI_CLK = XTAL/(PER20_DIV/4) = 2.592 MHz

}

bool putByte_SPI2(uint32 x)
/******************************************************************************
 *  Write a single item to serial flash via SPI
 *****************************************************************************/
{ 
   SetWord(SPI2_RX_TX_REG0, (uint8) x);
  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==0));
  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==1)) 
  {
    SetWord(SPI2_CLEAR_INT_REG, 0x01);
  }
  return TRUE;
}

bool getByte_SPI2(uint32* x  )
/******************************************************************************
*  Read a single item from serial flash via SPI
*****************************************************************************/
{
  SetWord(SPI2_RX_TX_REG0, (uint16) 0xffff);
  
  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==0));
  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==1)) 
  {
    SetWord(SPI2_CLEAR_INT_REG, 0x01);
  }
   *(uint8*)x=(uint8)GetWord(SPI2_RX_TX_REG0); 
  return TRUE;
} 


// SPI Flash Functions

unsigned char SpiFlashReadStatus( void ) 
{
	unsigned long temp;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_RDSR_CMD);
	getByte_SPI2(&temp);
	spi_mem_cs_high();

	return (unsigned char)temp;
}


int SpiFlashWaitTillReady (void)
{
  int count;
  unsigned char status;
  for (count = 0; count < MAX_READY_WAIT_COUNT; count++)
  {
    status = SpiFlashReadStatus();
    if (!(status & STATUS_BUSY))
      return 0;
  }
  return 1;
}

int SpiFlashSetWriteEbable(void)
{
	if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_WREN_CMD);
	spi_mem_cs_high();

	return ERR_OK;

}


unsigned long SpiFlashID( void ) 
{
	unsigned long ret,i ;
	
	if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;

	if (SpiFlashSetWriteEbable())
		return ERR_TIMOUT;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_RDID_CMD);
	getByte_SPI2(&i);
	ret = ((i&0xff)<<16);
	getByte_SPI2(&i);
	ret |= ((i&0xff)<<8);
	getByte_SPI2(&i);
	ret |= (i&0xff);
	spi_mem_cs_high();

	return ret;
}

int SpiFlashRead(unsigned long source_addr,unsigned char* destination_buffer,unsigned long len)
{

	if (!len)
		return 0;
	if ((source_addr+len) > FLASH_MEMORY_SIZE)
		return ERR_INVAL;

	if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_READ_CMD);
	putByte_SPI2((source_addr&0xffffff)>>16);
	putByte_SPI2((source_addr&0xffff)>>8);
	putByte_SPI2(source_addr&0xff);
	while(len-- >0)
	{
		getByte_SPI2((uint32*)destination_buffer);
		destination_buffer++;
	}
	spi_mem_cs_high();

	return ERR_OK;
}


int main(void)
{
	unsigned long temp;
	void (*loader_function)( void);

	/* Freeze Watchdog */
	SetWord(SET_FREEZE_REG,FRZ_WDOG);

#if defined SC14450

	SetBits(BAT_CTRL_REG,REG_ON,1);
	SetBits(BAT_CTRL_REG,DC_HYST,0);
	SetBits(BAT_CTRL2_REG,CHARGE_LEVEL,0x1e);
	SetBits(BAT_CTRL2_REG,NTC_DISABLE,1);
	SetBits(BAT_CTRL2_REG,CHARGE_CUR,7);
	

	init_pll();
	SetPort(P2_04_MODE_REG, PORT_OUTPUT,  PID_ACS1);        /* P2_04 is ACS1 */
	SetPort(P2_05_MODE_REG, PORT_OUTPUT,  PID_ACS2);        /* P2_05 is ACS2 */
	SetPort(P2_06_MODE_REG, PORT_OUTPUT,  PID_ACS3);        /* P2_06 is ACS3 */


	for (temp = 0;temp<0x800;temp+=4) {
		*( volatile unsigned int*)(0x9800+temp) = 0;
	}
	// 1st region is SDRAM from 0x000f0000 to 0x00FEEFFF (14.93MB)
	SetWord(CACHE_LEN0_REG,0x00ef);	// length in 64KB blocks
	SetWord(CACHE_START0_REG,0x000f);	// start offset in 64KB blocks


// 16KB split cache: 8KB instruction, 8KB data
// 16byte burst prefetch to match line length
// 4KB trace buffer (cyclic mode)
	SetWord(CACHE_CTRL_REG,0x0006);

	init_sdram(AHB_CLK_82MHz);
#elif defined SC14452
		// Disable additional capacitors.
	SetBits (CLK_XTAL_CTRL_REG, XTAL_EXTRA_CV, 0);

//	SetWord(SUPPLY_CTRL_REG,0x412e);
//	SetWord(BANDGAP_REG,0x0d);


	SetPort(P2_10_MODE_REG, PORT_OUTPUT,  PID_ACS3);        /* P2_10 is ACS3 */

	
#ifdef JTAG_OK
	// Change the jowi clk divider
//	SetBits (CLK_JOWI_REG, SW_OWICLK_C_DIV,16);
	
	// Start PLL 1 and switch
	pll_1_on(PLL1_CLK_164MHz);

	for (temp = 0;temp<0x800;temp+=4) {
		*( volatile unsigned int*)(0x9800+temp) = 0;
	}
	// 1st region is SDRAM from 0x000f0000 to 0x00FEEFFF (14.93MB)
	SetWord(CACHE_LEN0_REG,0x00ef);	// length in 64KB blocks
	SetWord(CACHE_START0_REG,0x000f);	// start offset in 64KB blocks


// 16KB split cache: 8KB instruction, 8KB data
// 16byte burst prefetch to match line length
// 4KB trace buffer (cyclic mode)
	SetWord(CACHE_CTRL_REG,0x0016);


	// Init SDRAM
	init_sdram(AHB_CLK_82MHz);
#else		


	// Start PLL 1 and switch
	pll_1_on(PLL1_CLK_82MHz);
	
	// Init SDRAM
	init_sdram(AHB_CLK_41MHz);
#endif
	
	// Start PLL 2 
	pll_2_on();
	
	// Enable UART clock
	if (GetWord(CLK_GLOBAL_REG) & SW_XTAL_PLL1_RATE)
		SetWord(CLK_GPIO6_REG, SW_UART_EN | 0x10);
	else
		SetWord(CLK_GPIO6_REG, SW_UART_EN | 0x8);
#ifdef LOW_CURRENT	
// joaquin  for RF sensitive  19Apr2011
	//SetWord(GPRG_R0_REG,0xFFC0);  //     0xFF7000
	//SetWord(GPRG_R1_REG,0xF000);  //     0xFF7002
// joaquin end
#endif
#endif
	SetWord(GPRG_R0_REG,0xFC00);  //     0xFF7000
	SetWord(GPRG_R1_REG,0xF000);  //     0xFF7002

	init_SPI2();

//	while(1);

	temp = SpiFlashID();
	SpiFlashRead(0x1000,(unsigned char*) SDRAM_START_PROGRAM,0x20000);

	loader_function = (void (*)())(SDRAM_START_PROGRAM>>1&0xffffff);
	loader_function();

	return 0;
}



//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:50.268884890
//@@ -513,12 +513,13 @@
// 		SetWord(CLK_GPIO6_REG, SW_UART_EN | 0x8);
// #ifdef LOW_CURRENT	
// // joaquin  for RF sensitive  19Apr2011
//-	SetWord(GPRG_R0_REG,0xFFC0);  //     0xFF7000
//-	SetWord(GPRG_R1_REG,0xF000);  //     0xFF7002
//+	//SetWord(GPRG_R0_REG,0xFFC0);  //     0xFF7000
//+	//SetWord(GPRG_R1_REG,0xF000);  //     0xFF7002
// // joaquin end
// #endif
// #endif
//-
//+	SetWord(GPRG_R0_REG,0xFC00);  //     0xFF7000
//+	SetWord(GPRG_R1_REG,0xF000);  //     0xFF7002
// 
// 	init_SPI2();
// 
