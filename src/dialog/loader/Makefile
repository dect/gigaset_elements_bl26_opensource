CROSS_COMPILE = cr16-elf-
export	CROSS_COMPILE

include config.mk

OBJS  =	start.o 

all:		sc14452loader
		
sc14450loader: 	sc14450loader.elf
		$(OBJCOPY) ${OBJCFLAGS} -O binary sc14450loader.elf sc14450loader.bin

sc14450loader.elf:	depend $(OBJS) $(LDSCRIPT)
		$(CC) $(CFLAGS) -DSC14450 -c -o loader.o loader.c
		$(LD) $(LDFLAGS) $(LDFLAGS_EXTRA) $(OBJS) loader.o $(LIBGCC) $(EXTERN_LIB) -Map=sc14450loader.map -o sc14450loader.elf 


sc14450loader12MHz: sc14450loader12MHz.elf
		$(OBJCOPY) ${OBJCFLAGS} -O binary sc14450loader12MHz.elf sc14450loader12MHz.bin

sc14450loader12MHz.elf:	depend $(OBJS) $(LDSCRIPT) 
		$(CC) $(CFLAGS) -DSC14450 -DCLOCK_12MHz -c -o loader.o loader.c
		$(LD) $(LDFLAGS) $(LDFLAGS_EXTRA) $(OBJS) loader.o $(LIBGCC) $(EXTERN_LIB) -Map=sc14450loader12MHz.map -o sc14450loader12MHz.elf 


sc14452loader: 	sc14452loader.elf
	$(OBJCOPY) ${OBJCFLAGS} -O binary sc14452loader.elf sc14452loader.bin

sc14452loader.elf:	depend $(OBJS) $(LDSCRIPT)
	$(CC) $(CFLAGS) -DSC14452 -UCONFIG_RAM_SIZE_32MB -DJTAG_OK \
		-c -o loader.o loader.c
	$(LD) $(LDFLAGS) $(LDFLAGS_EXTRA) $(OBJS) loader.o $(LIBGCC) \
		$(EXTERN_LIB) -Map=sc14452loader.map -o sc14452loader.elf

sc14452loader_lds: 	sc14452loader_lds.elf
	$(OBJCOPY) ${OBJCFLAGS} -O binary sc14452loader.elf sc14452loader.bin

sc14452loader_lds.elf:	depend $(OBJS) $(LDSCRIPT)
	$(CC) $(CFLAGS) -DLOW_CURRENT -DSC14452 -UCONFIG_RAM_SIZE_32MB -DJTAG_OK \
		-c -o loader.o loader.c
	$(LD) $(LDFLAGS) $(LDFLAGS_EXTRA) $(OBJS) loader.o $(LIBGCC) \
		$(EXTERN_LIB) -Map=sc14452loader.map -o sc14452loader.elf


sc14452loader32MB: 	sc14452loader32MB.elf
	$(OBJCOPY) ${OBJCFLAGS} -O binary sc14452loader.elf sc14452loader.bin

sc14452loader32MB.elf:	depend $(OBJS) $(LDSCRIPT)
	$(CC) $(CFLAGS) -DSC14452 -DCONFIG_RAM_SIZE_32MB -DJTAG_OK \
		-c -o loader.o loader.c
	$(LD) $(LDFLAGS) $(LDFLAGS_EXTRA) $(OBJS) loader.o $(LIBGCC) \
		$(EXTERN_LIB) -Map=sc14452loader.map -o sc14452loader.elf


sc14452loader32MB_lds: 	sc14452loader32MB_lds.elf
	$(OBJCOPY) ${OBJCFLAGS} -O binary sc14452loader.elf sc14452loader.bin

sc14452loader32MB_lds.elf:	depend $(OBJS) $(LDSCRIPT)
	$(CC) $(CFLAGS) -DLOW_CURRENT -DSC14452 -DCONFIG_RAM_SIZE_32MB -DJTAG_OK \
		-c -o loader.o loader.c
	$(LD) $(LDFLAGS) $(LDFLAGS_EXTRA) $(OBJS) loader.o $(LIBGCC) \
		$(EXTERN_LIB) -Map=sc14452loader.map -o sc14452loader.elf


depend dep:
	@for dir in $(SUBDIRS) ; do $(MAKE) -C $$dir .depend ; done

clean:
	@find  -name "sc14450loader" -o -name "*.elf" -o -name "*.o" -o -name "*.bin" -o -name "*.bak" -o -name "*~"\
	-o -name "*.o"  -o -name "*.a" -o -name ".depend" -o -name "*.map" -name "tmp"| xargs rm -f

help:
	@echo  'How to build the loader for each board:'
	@echo  '  make sc14450loader	  	--> SC14450-based board with 10.368kHz (protruding) crystal'
	@echo  '  make sc14450loader12MHz 	--> SC14450-based board with 12.288kHz (flat) crystal'
	@echo  '  make sc14452loader	  	--> SC14452-based board with 16MB RAM'
	@echo  '  make sc14452loader32MB  	--> SC14452-based board with 32MB RAM'
	@echo  '  make sc14452loader_lds	--> SC14452-based board with 16MB RAM I/O configured for low current strength'
	@echo  '  make sc14452loader32MB_lds	--> SC14452-based board with 32MB RAM I/O configured for low current strength'
	@echo  ''

