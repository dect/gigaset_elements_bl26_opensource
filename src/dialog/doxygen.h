// File created by Gigaset Elements GmbH
// All rights reserved.
/**
 * @addtogroup uboot U-Boot
 * @{
 *
 * @section uboot U-Boot
 *
 * @subsection ubootEnv U-Boot Environment Variables
 *
 * The following special environment variables are used:
 *
 * - <code>reef_stop</code>: used in <code>/etc/init.d/S40reef.sh</code> to conditionally start
 *   the Reef basestation modules. If the value is <code>true</code>, the modules are NOT
 *   startes after boot. No value or any other value will start Reef normally.
 * - <code>initscript</code>: used in <code>/etc/init.d/S50ubootscript.sh</code> to start an
 *   additional script after boot has finished. The value should be the name of a script
 *   in the <code>/etc/init.d</code> folder
 * - <code>syslogip</code>: used in <code>/etc/network/if-up.d/up</code>. If the variable
 *   has a value that is an IP address, that address will be used for the syslog daemon.
 *
 * @}
 */
