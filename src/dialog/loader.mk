ifeq ($(BUILD),hw)
    BUILD_THIS := 1
else ifeq ($(BUILD),hw_recovery)
    BUILD_THIS := 1
else
    BUILD_THIS := 0
endif

ifeq ($(BUILD_THIS),1)

CFLAGS := 
LDFLAGS :=  

install: all


all: 
	cd loader &&\
	make sc14452loader32MB


clean: 
	cd loader &&\
	make clean

endif	# HW configuration