/*
 *  armboot - Startup Code for ARM720 CPU-core
 *
 *  Copyright (c) 2001	Marius Gr�ger <mag@sysgo.de>
 *  Copyright (c) 2002	Alex Z�pke <azu@sysgo.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 *************************************************************************
 *
 * Jump vector table as in table 3.1 in [1]
 *
 *************************************************************************
 */

	
	.extern	_STACK_START
	.extern	_ISTACK_START

	.extern __bss_start
	.extern __bss_end
	.extern _main	

	.global	_start
	.globl	__dispatch_table


/*
 * Note: armboot_end is defined by the (board-dependent) linker script
 */


_start:

	di				/* Disable maskable interupts */
	movd	$0, (r1,r0)
	lprd	(r1,r0), psr

	movd	$_STACK_START, (sp)
	
	movd	$_ISTACK_START, (r1,r0)
	lprd	(r1,r0), isp
	
	cinv	[i]
	cinv	[d]
	
	movw	$0x100, r0
	lpr	r0, cfg
	
	/* Clear the .bss */
	movd	$__bss_start,(r1, r0)	/* Start address of bbs */
	movd    $__bss_end,(r3, r2)	/* End address of bbs */
	subd	(r1, r0), (r3, r2)	/* Size of bbs */
	lshd	$-2, (r3, r2)		/* Convert from 'size (bytes)'	*/
					/* to 'erase blocks (32 bits)'	*/
	subd	(r5, r4), (r5, r4)	/* Create a zeroing block */
1:	
	stord	(r5, r4), 0x0(r1, r0)	/* Zero this block of memory */
	addd	$4, (r1, r0)		/* Move to next block */
	cmpd	$0, (r3, r2)		/* Has the bbs all been zeroed */
	subd	$1, (r3, r2)		/* --remaining_erase_blocks */
	bne	1b		

	bal	(ra), _main





