#include "sitel_io.h"

#define UART_TIMEOUT_FACTOR	1000	/* 1000 */
#define MIN(a,b)	((a)<(b)?(a):(b))
#if defined SC14450
	#define spi_mem_cs_low() SetWord(P0_RESET_DATA_REG, GPIO_10)
	#define spi_mem_cs_high() SetWord(P0_SET_DATA_REG, GPIO_10)
#elif defined SC14452
	#define spi_mem_cs_low() SetWord(P0_RESET_DATA_REG, GPIO_3)
	#define spi_mem_cs_high() SetWord(P0_SET_DATA_REG, GPIO_3)
#endif


#if ((defined SC14452) && (defined REEF))
    #define BLUE    GPIO_8
    #define GREEN   GPIO_11
    #define YELLOW  GPIO_12

    #define LED(led,state)          { vLEDControl (led,state); }
    #define LED_OFF                 { LED(BLUE,0); LED(GREEN,0); LED(YELLOW,0); }
    #define LED_ERROR               { LED_OFF; while (1) { LED(YELLOW,1); DELAY(100); LED(YELLOW,0); DELAY(100); }; }
    #define LED_OK                  { LED_OFF; while (1) { LED(GREEN,1);  DELAY(100); LED(GREEN,0);  DELAY(500); }; }
    #define LED_SHOW_STATE(x)       { LED(GREEN,(x)&1); LED(BLUE,(x)&2); LED(YELLOW,(x)&4); }
#else
    #define LED(led,state)
    #define LED_OFF
    #define LED_ERROR
    #define LED_OK
    #define LED_SHOW_STATE(x)
#endif

#ifdef SC14452
#define SPI2_CTRL_REG SPI2_CTRL_REG0
#define __SPI2_CTRL_REG __SPI2_CTRL_REG0
#endif


#define CLK_1296	0x00
#define CLK_2592	0x01
#define CLK_5184	0x02  
#define CLK_1152	0x03  

#define MODE_0		0x00
#define MODE_1		0x01
#define MODE_2		0x02
#define MODE_3		0x03

#define MODE_8BITS  0x00
#define MODE_16BITS 0x01
#define MODE_32BITS 0x02

#define PHYS_FLASH_SIZE 	0x800000
#define FLASH_MEMORY_SIZE	0x800000
#define FLASH_PAGE_SIZE		256
#define SEC_SIZE_4K			0x1000//0
#define SEC_SIZE_32K		0x8000//1
#define SEC_SIZE_64K		0x10000//2

#define PHYS_FLASH_SECTOR_SIZE SEC_SIZE_4K

#define	MAX_READY_WAIT_COUNT	1000000

#define	SPI_FLASH_WREN_CMD			0x06	/* Write Enable */
#define	SPI_FLASH_WRDI_CMD			0x04	/* Write Disable */
#define	SPI_FLASH_RDID_CMD			0x9F	/* Read Identification */
#define	SPI_FLASH_RDSR_CMD			0x05	/* Read Status Register */
#define	SPI_FLASH_WRSR_CMD			0x01	/* Write Status Register */
#define	SPI_FLASH_READ_CMD			0x03	/* Read Data Bytes */
#define	SPI_FLASH_READ_FAST_CMD		0x0B	/* Read Data Bytes in higher speed */
#define	SPI_FLASH_PP_CMD			0x02	/* Page Program */
#define	SPI_FLASH_SE_CMD			0xD8	/* Sector Erase */
#define	SPI_FLASH_BE_CMD			0xC7	/* Bulk Erase */
#define SPI_FLASH_DP_CMD			0xB9	/* Deep Power Down */
#define	SPI_FLASH_RES_CMD			0xAB	/* Release from Deep Power Down */
#define	SPI_FLASH_SE_32K_CMD		0x52	/* Sector Erase 32Kbytes*/
#define	SPI_FLASH_SE_4K_CMD			0x20	/* Sector Erase 4KBytes*/

/* Status Register Bits */

#define STATUS_BUSY		0x01
#define	STATUS_WEL		0x02
#define	STATUS_BP0		0x04
#define	STATUS_BP1		0x08
#define	STATUS_BP2		0x10
#define	STATUS_WP		0x80

#define ERR_OK						0
#define ERR_TIMOUT					-1
#define ERR_NOT_ERASED				-2
#define ERR_PROTECTED				-3
#define ERR_INVAL					-4
#define ERR_ALIGN					-5
#define ERR_UNKNOWN_FLASH_VENDOR	-6
#define ERR_UNKNOWN_FLASH_TYPE		-7
#define ERR_PROG_ERROR				-8

#define ERROR                  0x00	// 000
#define IDLE                   0x01	// 001
#define WAITING_LEN            0x02	// 010
#define REC_CODE               0x03	// 011
#define CHK_CRC                0x04	// 100
#define WAIT_ACK               0x05	// 101

#define SOH   0x01              
#define STX   0x02              
#define ACK   0x06              
#define NAK   0x15              

#define PROG_SOH 0x03
#define PROG_STX 0x05
#define PROG_ACK 0x07
#define PROG_NAK 0x16
#define PROG_COM 0x17

#define DOWNLOAD_POS 		0x100000
#define COMPARE_BUFF 		0x800000
#define MAX_COMPARE_BLOCK	0x100000
uint8 RecChar;
uint32 max_download_size;


///< delay macro in ms
//#define DELAY(x)	{ for(ui1=0; ui1<(5*(x)); ui1++) for(ui2=0; ui2<800; ui2++); }
#define DELAY(x)	{ delay_ms(x); }

/**
 * @brief       Simple and not accurate delay function
 * @author      Wojciech Nizinski
 * @param ms    miliseconds
 */
static void delay_ms(uint32 ms)
{
    volatile uint32 ui1;
    for (ui1=0; ui1<(5*(ms)); ui1++) 
      __asm__ __volatile__
      (\
        "push $2,r6                       /* store 2 registers starting from r6 */      \n"\
        "subd (r7, r6), (r7, r6)          /* zero counter (r6,r7) */                    \n"\
        "addd $1500, (r7, r6)             /* init counter with 1500 */                  \n"\
        "1:                                                                             \n"\
        "subd $1, (r7,r6)                 /* decrement counter by 1 */                  \n"\
        "cmpd $0, (r7,r6)                 /* compare with zero */                       \n"\
        "bne 1b                           /* loop if not zero */                        \n"\
        "pop $2,r6                        /* restore 2 registers starting from r6 */    \n"\
      );
}

#if ((defined SC14452) && (defined REEF))
static void vLEDControl (uint16 port_id, uint8 state)
{
    if (state==0)
    {
        SetBits(P2_RESET_DATA_REG, port_id,  1);
    }
    else
    {
        SetBits(P2_SET_DATA_REG, port_id,  1);    
    }
}
#endif

void init_sdram (void) 
{
	/* configure GPIOs as SDRAM interface */
	SetWord( P0_15_MODE_REG,0x0330 );	/* P0_15 is ACS4 */
	SetWord( P1_03_MODE_REG,0x031d );	/* P1_03 is SD_A10 */
	SetWord( P1_12_MODE_REG,0x031e );	/* P1_12 is SD_CLK */
	SetWord( P1_11_MODE_REG,0x031d );	/* P1_11 is SD_CKE */
	SetWord( P1_04_MODE_REG,0x031d );	/* P1_04 is SD_WEN */
	SetWord( P1_05_MODE_REG,0x031d );	/* P1_05 is SD_BA0 */
	SetWord( P1_06_MODE_REG,0x031d );	/* P1_06 is SD_BA1 */
	SetWord( P1_07_MODE_REG,0x031d );	/* P1_07 is SD_LDQM */
	SetWord( P1_08_MODE_REG,0x031d );	/* P1_08 is SD_UDQM */
	SetWord( P1_09_MODE_REG,0x031d );	/* P1_09 is SD_RAS */
	SetWord( P1_10_MODE_REG,0x031d );	/* P1_10 is SD_CAS */

	/* sdram controller configuration */
	/* base address is 0x20000 */
	SetDword( EBI_ACS4_LOW_REG,0x00020000 );
	/* sdram size is 16MB */
	SetDword( EBI_ACS4_CTRL_REG,0x00000009 );
	SetDword( EBI_SDCONR_REG,0x00001168 );
	

	SetDword( EBI_SDREFR_REG,0x0011050c );
	
	SetDword( EBI_SDEXN_MODE_REG,0x00000022 );
	
	SetDword( EBI_SDCTLR_REG,0x00005049 );
	while(GetWord(EBI_SDCTLR_REG)& 1);
}

#if defined SC14450

void init_pll(void)
{

	unsigned i,temp;

	/* APB clock is HCLK/2 */
	SetWord(CLK_AMBA_REG,0x0010);
	/* AHB clock is HCLK */
	SetWord(CLK_AMBA_REG,0x0011);

	/* Xtal is 12.288 MHz PLL at 165.888 MHz VD = 3*9 XD = /2 */
	SetWord(CLK_PLL1_DIV_REG, 0x001c);
	/* we setup the PLL2 to give the 25MHz for the ethernet */
	/* for 12.288 MHz Xtal we use as XD/VD values 58/0 */ 
	SetWord(CLK_PLL2_DIV_REG,0x803e);	
//	SetWord(CLK_PLL2_DIV_REG,0x201b);	

	/* HF mode enable, VCO on , PLL charge pump on */
	SetWord(CLK_PLL1_CTRL_REG,0x0052);	
	SetWord(CLK_PLL2_CTRL_REG,0x0016);	
//	SetWord(CLK_PLL2_CTRL_REG,0x0012);	

	/* wait500usec */
	for(i = 0; i < 0x100; ++i)
		temp = *(volatile unsigned*)0xff4000;

	SetWord(CLK_PLL2_CTRL_REG, 0x001e);	
//	SetWord(CLK_PLL2_CTRL_REG, 0x001a);	
	/* system clock in PLL mode */
	SetWord(CLK_PLL1_CTRL_REG,0x005a);

	SetWord(CLK_DSP_REG,0x99);
	SetWord(CLK_CODEC_DIV_REG,0x00C8);
	SetWord(CLK_CODEC_REG,0x2D55);
	SetWord(CLK_AMBA_REG,0x01f1);

	/* Peripherial clock connected to PLL/2 at 82.944 MHz divide by 72 for 1.152 MHz */
	SetWord(CLK_PER_DIV_REG,0x00c8);	

	/* Codec clock connected to PLL/2 at 82.944 MHz divide by 72 for 1.152 MHz */
	SetWord(CLK_CODEC_DIV_REG,0x00c8);	

	/* Serial clock is 10.368MHz = 82.944MHz/8 */
	/* SPI clock is 20.736MHz = (82.944MHz / 2) / 2 */
	SetWord(CLK_PER10_DIV_REG,0x0048);	// PER20 /= 2, PER10 /= 1

	/* JTAG OWI clock is driven by XTAL */
	SetWord(CLK_AUX_REG,0x0020);

	/* Disable XTAL startup capacitences */
	SetWord(CLK_XTAL_CTRL_REG,0x0022);

	/* Change XTAL power from VDD_RF to LDO_XTAL */
	SetWord( CLK_XTAL_CTRL_REG,0x0023);	
	/* wait 200 usec */
	for(i = 0; i < 0x400; ++i)
		temp = *(volatile unsigned*)0xff4000;

	SetWord( CLK_XTAL_CTRL_REG,0x0021);	

}

#elif defined SC14452

void pll_1_on( void)  /* PLL1 on, Used by CR16C, PROCEDURE FOR 10.368 Mhz */
{    
	volatile uint16 i;
	// PLL CLOCK SWITCHING PROCEDURE
	SetWord(CLK_PLL1_DIV_REG, 0x1c);	   // x16

	SetWord(CLK_PLL1_CTRL_REG, (PLL1_HF_SEL | PLL1_VCO_ON | PLL1_CP_ON));
	SetBits(CLK_GLOBAL_REG, SW_XTAL_PLL1_RATE, 0x1); // XTAL - PLL1 rate = 16

	for(i=0;i<50;i++);			   // Wait for approx. 200 us when in XTAL mode and HCLK_DIV=2.

	SetBits(CLK_AMBA_REG, SW_PCLK_DIV, 2);     // Divide by 2 (PCLK max = 41 MHz). Must be done in two steps.
	SetBits(CLK_AMBA_REG, SW_HCLK_DIV, 4);     // Divide by 4 (HCLK=40 MHz) before switching to PLL mode (SW workaround).
	SetBits(CLK_AMBA_REG, SW_HCLK_DIV, 4);     // Same as above .. just for delay until the div register really updates... (NM/07-08-2008).

	SetBits(CLK_GLOBAL_REG, SW_SWITCH_CLK, 0); // Go.

	while (GetWord (CLK_AUX1_REG) & SWITCH_TO_PLL); //Wait until all clocks switch to pll domain.

	SetBits(CLK_AMBA_REG, SW_HCLK_DIV, 2);     // Divide by 2 (HCLK=80 MHz), so put it back again in the HCLK max. frequency setting.
 }

void pll_2_on(void)  /* PLL2 on, in Can be used by QSPIC and EMAC, PROCEDURE FOR 10 Mhz */ 
{
    volatile uint16 i;

    SetWord(CLK_PLL2_DIV_REG , PLL2_DIV5 | 62);             // VD=299, XD=62
    SetWord(CLK_PLL2_CTRL_REG, PLL2_VCO_ON | PLL2_CP_ON );  // VCO_ON | CP_ON 
    
    for(i=0;i<500;i++);                                     // Wait for a while.

}

#endif


void init_SPI2(void)
{ 
#if defined SC14450
  
	/* Set PPA MATRIX FOR SPI2 */
	SetPort(P0_12_MODE_REG, PORT_OUTPUT,  PID_SPI2_DOUT);   /* P0_12 as SPI_DO  */
	SetPort(P0_13_MODE_REG, PORT_INPUT,   PID_SPI2_DIN);    /* P0_13 as SPI_DI  */
	SetPort(P0_14_MODE_REG, PORT_OUTPUT,  PID_SPI2_CLK);    /* P0_14 as SPI_CLK */
	SetPort(P0_10_MODE_REG, PORT_OUTPUT,  PID_port);        /* P0_15 as SPI_EN  */  
	/* Set SPI SETTINGS */

#elif defined SC14452
	SetWord(P0_DATA_REG, GPIO_3 | GPIO_6 | GPIO_12 | GPIO_13 | GPIO_14);  // Note: QSPI.WPn=P0[11]=0
	SetPort(P0_03_MODE_REG, PORT_OUTPUT,  PID_port);	              // 452: Chip Select
	SetPort(P0_06_MODE_REG, PORT_OUTPUT,  PID_SPI2_CLK);	              // 452: use SPI clk the P0[6] (instead of P0[14])
	SetPort(P0_11_MODE_REG, PORT_OUTPUT,  PID_port);	              // 452: Write protect
	SetPort(P0_12_MODE_REG, PORT_OUTPUT,  PID_SPI2_DOUT);	              // Set PPA
	SetPort(P0_13_MODE_REG, PORT_PULL_UP, PID_SPI2_DIN);
	SetPort(P0_14_MODE_REG, PORT_OUTPUT,  PID_port);	              // 452: QSPI HOLDn pin (keep always high)

	// set SPI2_CLK = 20.736 MHz...
	if (GetWord(CLK_GLOBAL_REG) & SW_XTAL_PLL1_RATE)
		SetBits(CLK_GPIO2_REG, SW_SPI2_DIV, 8);
	else
		SetBits(CLK_GPIO2_REG, SW_SPI2_DIV, 4);

	// Stop spi2 clock
	SetBits(CLK_GPIO3_REG, SW_SPI2_EN,  0);  
	  
	// Enable spi2 clk
	SetBits(CLK_GPIO3_REG, SW_SPI2_EN,  1); 
#endif

	SetBits(SPI2_CTRL_REG, SPI_WORD, 0x00);                 // Set SPI 8bits Mode,  Serial Flash Device
	SetBits(SPI2_CTRL_REG, SPI_SMN,  0x00);                 // Set SPI IN MASTER MODE
	SetBits(SPI2_CTRL_REG, SPI_POL,  0x01);  
	SetBits(SPI2_CTRL_REG, SPI_PHA,  0x01);					// MODE 3: SPI2_POL=1  and PI2_PHA=1
	SetBits(SPI2_CTRL_REG, SPI_MINT, 0x01);                 // Disable SPI2_INT to ICU, timer0 is used
	SetBits(SPI2_CTRL_REG, SPI_ON, 1);                      // Enable SPI2 

	SetBits(SPI2_CTRL_REG, SPI_CLK,  CLK_5184);                 // SPI_CLK = XTAL/(PER20_DIV/4) = 2.592 MHz

}

bool putByte_SPI2(uint32 x)
/******************************************************************************
 *  Write a single item to serial flash via SPI
 *****************************************************************************/
{ 
  SetWord(SPI2_RX_TX_REG0, (uint8) x);
  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==0));
  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==1)) 
  {
	  SetWord(SPI2_CLEAR_INT_REG, 0x01);
  }
  return TRUE;
}

bool getByte_SPI2(uint32* x  )
/******************************************************************************
*  Read a single item from serial flash via SPI
*****************************************************************************/
{
  SetWord(SPI2_RX_TX_REG0, (uint16) 0xffff);
  
  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==0));
  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==1)) 
  {
    SetWord(SPI2_CLEAR_INT_REG, 0x01);
  }
   *(uint8*)x=(uint8)GetWord(SPI2_RX_TX_REG0); 
  return TRUE;
} 

uint8 SpiFlashReadStatus( void ) 
{
	uint32 temp;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_RDSR_CMD);
	getByte_SPI2(&temp);
	spi_mem_cs_high();

	return (uint8)temp;
}

int SpiFlashWaitTillReady (void)
{
  int count;
  uint8 status;
  for (count = 0; count < MAX_READY_WAIT_COUNT; count++)
  {
    status = SpiFlashReadStatus();
    if (!(status & STATUS_BUSY))
      return 0;
  }
  return 1;
}

int SpiFlashSetWriteEbable(void)
{
	if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_WREN_CMD);
	spi_mem_cs_high();

	return ERR_OK;

}

uint32 SpiFlashID( void ) 
{
	unsigned long ret,i ;
	
	if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;

	if (SpiFlashSetWriteEbable())
		return ERR_TIMOUT;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_RDID_CMD);
	getByte_SPI2(&i);
	ret = ((i&0xff)<<16);
	getByte_SPI2(&i);
	ret |= ((i&0xff)<<8);
	getByte_SPI2(&i);
	ret |= (i&0xff);
	spi_mem_cs_high();

	return ret;
}

int SpiFlashRead(uint32 source_addr,uint8* destination_buffer,uint32 len)
{

	if (!len)
		return 0;
	if ((source_addr+len) > FLASH_MEMORY_SIZE)
		return ERR_INVAL;

	if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_READ_CMD);
	putByte_SPI2((source_addr&0xffffff)>>16);
	putByte_SPI2((source_addr&0xffff)>>8);
	putByte_SPI2(source_addr&0xff);
	while(len-- >0)
	{
		getByte_SPI2((uint32*)destination_buffer);
		destination_buffer++;
	}
	spi_mem_cs_high();

	return ERR_OK;
}

int SpiFlashProgramPage(uint32 dest_addr,uint8* data, uint32 len)
{
	uint16 index;
	if (!len)
		return ERR_OK;
	if (len > FLASH_PAGE_SIZE)
		return ERR_INVAL;
    
  if (SpiFlashSetWriteEbable())
		return ERR_TIMOUT;  
  if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_PP_CMD);
	putByte_SPI2((dest_addr&0xffffff)>>16);
	putByte_SPI2((dest_addr&0xffff)>>8);
	putByte_SPI2(dest_addr&0xff);
	for (index =0; index < len; index++)
		putByte_SPI2(data[index]);
	spi_mem_cs_high();

	return ERR_OK;

}

int SpiFlashProgram(uint32 dest_addr,uint8* data, uint32 len)
{
	
	uint32 index,page_offset,page_size;
	int retvalue = 0;

	if (!len)
		return ERR_OK;
	if ((dest_addr+len) > PHYS_FLASH_SIZE)
		return ERR_INVAL;
	
	page_offset = dest_addr % FLASH_PAGE_SIZE;

	if ((page_offset + len) <= FLASH_PAGE_SIZE)  
	{
		retvalue = SpiFlashProgramPage(dest_addr,data,len);
		return retvalue;
	}
	else
	{
		page_size = FLASH_PAGE_SIZE - page_offset;
		retvalue = SpiFlashProgramPage(dest_addr,data,page_size);

		if (retvalue != 0)
			return retvalue;

		for (index = page_size; index < len; index+=page_size)
		{
			page_size = len - index;
			if (page_size > FLASH_PAGE_SIZE)
				page_size = FLASH_PAGE_SIZE;

			retvalue = SpiFlashProgramPage(dest_addr + index,&data[index],page_size);

			if (retvalue != 0)
				return retvalue;
		}
	}
	return ERR_OK;
}

int SpiFlashEraseSector(uint32 sector_address)
{
  
	uint8 status;

	if (SpiFlashSetWriteEbable())
		return ERR_TIMOUT;

	status = SpiFlashReadStatus();
	if (( status & 0x3c) != 0) {
	// write protected
		spi_mem_cs_low();
		putByte_SPI2(SPI_FLASH_WRSR_CMD);
		putByte_SPI2(0);
		spi_mem_cs_high();
		
		if (SpiFlashSetWriteEbable())
			return ERR_TIMOUT;
 
	}

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_SE_4K_CMD);
	putByte_SPI2((sector_address&0xffffff)>>16);
	putByte_SPI2((sector_address&0xffff)>>8);
	putByte_SPI2(sector_address&0xff);
	spi_mem_cs_high();  
	
	return ERR_OK;			
}


uint8 ReceiveChar(void)
{
   uint32 timeout;

   SetWord(UART_CLEAR_RX_INT_REG, 0x01);
   for (timeout = 850*UART_TIMEOUT_FACTOR; timeout > 0;  timeout --)
   {
       if ( GetBits(UART_CTRL_REG,RI) == 1)
       {                                   /* We really got a Byte from the other side */
           RecChar = GetWord(UART_RX_TX_REG);  
           return (TRUE);
       }
   }
   return ( FALSE );
}

void SendChar(unsigned char tra_byte)  /* (V0.5) WORD changed to BYTE for byte oriented UART. */
{
   uint32 timeout;
    DELAY(100);
   SetWord(UART_RX_TX_REG, tra_byte);               /* Transmit character */
   SetWord(UART_CLEAR_TX_INT_REG,0x01);            /* clear TI this will start transmission */

   for ( timeout = 150*UART_TIMEOUT_FACTOR; timeout > 0; timeout --)  /* Wait till really send */
   {
       if( GetBits(UART_CTRL_REG,TI) == 1 )
       {
           break;                                   /* Byte moved to the shift register */
       }
   }
}


int main(void)
{
	uint32 temp;
	uint32 length=0;
	uint32 i;
	uint16 sect;
	uint8 state;      
	uint8 crc;        
	uint8 *ptr_code;
	uint8 *temp_ptr;  

	/* Freeze Watchdog */
	SetWord(SET_FREEZE_REG,FRZ_WDOG);

#if defined SC14450
	SetBits(BAT_CTRL_REG,REG_ON,1);
	SetBits(BAT_CTRL_REG,DC_HYST,0);
	SetBits(BAT_CTRL2_REG,CHARGE_LEVEL,0x1e);
	SetBits(BAT_CTRL2_REG,NTC_DISABLE,1);
	SetBits(BAT_CTRL2_REG,CHARGE_CUR,7);

	init_pll();

	SetPort(P2_04_MODE_REG, PORT_OUTPUT,  PID_ACS1);        /* P2_04 is ACS1 */
	SetPort(P2_05_MODE_REG, PORT_OUTPUT,  PID_ACS2);        /* P2_05 is ACS2 */
	SetPort(P2_06_MODE_REG, PORT_OUTPUT,  PID_ACS3);        /* P2_06 is ACS3 */
	
#elif defined SC14452
	// Disable additional capacitors.
	SetBits (CLK_XTAL_CTRL_REG, XTAL_EXTRA_CV, 0);
	// Start PLL 1 and switch
	pll_1_on();
	
	// Start PLL 2 
	pll_2_on();
	
	// Enable UART clock
	if (GetWord(CLK_GLOBAL_REG) & SW_XTAL_PLL1_RATE)
		SetWord(CLK_GPIO6_REG, SW_UART_EN | 0x10);
	else
		SetWord(CLK_GPIO6_REG, SW_UART_EN | 0x8);
	
#endif	
	
	init_sdram();	
	init_SPI2();

		
	SetWord(UART_CTRL_REG,0);
        /*   Config PPA for UART. */
	SetPort(P0_00_MODE_REG, PORT_OUTPUT,    PID_UTX);
	SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_URX);
	
	SetBits( UART_CTRL_REG, BAUDRATE, CLK_1152);

	SetBits(UART_CTRL_REG, UART_REN, 1); // enable receive
	SetBits(UART_CTRL_REG, UART_TEN, 1); // enable transmit

	max_download_size = FLASH_MEMORY_SIZE;
	
	state = IDLE;

#if defined SC14452
	SetPort(P2_08_MODE_REG, PORT_OUTPUT, PID_port); // Blue led
	SetPort(P2_11_MODE_REG, PORT_OUTPUT, PID_port); // Green led
	SetPort(P2_12_MODE_REG, PORT_OUTPUT, PID_port); // Yellow led
#endif
	
	LED(YELLOW,1);
    
	while(1)
	{
		LED_SHOW_STATE(state);
		switch(state)
		{
			case ERROR:
			    LED_ERROR; // unfinished loop state
			    break;
			    
			case IDLE:
			{
				while(1)
				{

					SendChar(PROG_STX);
					if (ReceiveChar())
					{
						if (RecChar==PROG_SOH)
						{
							state = WAITING_LEN;
							break;
						}
					}
				}
				break;
			}
			
			case WAITING_LEN:
			{
				if (ReceiveChar())
				{
					length = RecChar;
					if (ReceiveChar())
					{
						length += RecChar<<8;
						if (ReceiveChar())
						{
							length += RecChar<<16;
							if (ReceiveChar())
							{
								length += RecChar<<24;
								if (length > max_download_size || length == 0)
								{
									SendChar(PROG_NAK);
								}
								else
								{
									SendChar(PROG_ACK);
									state = REC_CODE;
									break;
								}
							}
						}
					}
				}
				state = ERROR;
				break;
			}
			
			case REC_CODE:
			{
				ptr_code = (uint8*)DOWNLOAD_POS;
				for (i=0;i<length;i++,ptr_code++)
				{
					if (!ReceiveChar())
					{
						state = ERROR;
						break;
					}
					else
					{
						*ptr_code = RecChar;
					}
					LED (GREEN,(RecChar&2));
					if ((i & 0xFFFF) % 0x8000 == 0)
					{
						LED (BLUE, 0);
					}
					if ((i & 0xFFFF) % 0x8300 == 0)
					{
						LED (BLUE, 1);
					}
				}
				if (state == REC_CODE)
				{
					state = CHK_CRC;
					LED(GREEN,0);
				}
				break;
			}

			case CHK_CRC:
			{
				ptr_code = (uint8*)DOWNLOAD_POS;

				crc = 0;
				for ( i = 0 ; i < length ; i ++, ptr_code ++)
				{
					crc ^= *ptr_code;
				}
				SendChar(crc);
				state = WAIT_ACK;
				break;

			}
			case WAIT_ACK:
			{
				if ( ReceiveChar() && RecChar == PROG_ACK ) 
				{
# if 0
					// cheat to simulate full 8MB programming without waiting for serial data :)
					length = FLASH_MEMORY_SIZE;
					ptr_code = (uint8*)DOWNLOAD_POS;
					for ( i = 0 ; i < length ; i ++, ptr_code ++)
					{
						*ptr_code = 0xA5;
					}
#endif
					temp = SpiFlashID();
					for (sect=0; sect<((length/PHYS_FLASH_SECTOR_SIZE)+1); sect++)
					{
						LED(BLUE,(sect&1));
						SpiFlashEraseSector(sect * PHYS_FLASH_SECTOR_SIZE );
					}
					LED(BLUE,1);
					SpiFlashProgram(0, (uint8*)DOWNLOAD_POS, length);

					if (!(SpiFlashWaitTillReady()))
					{
						LED_OFF;
						LED(BLUE,1);
						LED(GREEN,1);
						uint32 length_left = length;
						uint32 current_read_length;
						uint8 * temp_ptr_code;
						ptr_code = (uint8*)DOWNLOAD_POS;
						while (length_left>0)
						{
							current_read_length = MIN (length_left, MAX_COMPARE_BLOCK);
							SpiFlashRead(0,(uint8*) COMPARE_BUFF, current_read_length);

							temp_ptr = (uint8*)COMPARE_BUFF;

							temp_ptr_code = ptr_code;
							for (i=0; i<current_read_length; i++)
							{
								if((*temp_ptr_code++) != (*temp_ptr_code++) )
								{
									state = ERROR;
									break;
								}
							}
							length_left -= current_read_length;
							ptr_code 	+= current_read_length;
						}
						SendChar(PROG_COM);
						LED_OK
					}
				}
				state = ERROR;
				break;
			}
			
			default:
			{
				state = IDLE;
				break;
			}
		}
	}
	
	return 0;
}



//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:50.268884890
//@@ -1,717 +1,830 @@
// #include "sitel_io.h"
// 
//-#if defined SC14450
//-	#define spi_mem_cs_low() SetWord(P0_RESET_DATA_REG, GPIO_10)
//-	#define spi_mem_cs_high() SetWord(P0_SET_DATA_REG, GPIO_10)
//-#elif defined SC14452
//-	#define spi_mem_cs_low() SetWord(P0_RESET_DATA_REG, GPIO_3)
//-	#define spi_mem_cs_high() SetWord(P0_SET_DATA_REG, GPIO_3)
//-#endif
//-
//-#ifdef SC14452
//-#define SPI2_CTRL_REG SPI2_CTRL_REG0
//-#define __SPI2_CTRL_REG __SPI2_CTRL_REG0
//-#endif
//-
//-
//-#define CLK_1296	0x00
//-#define CLK_2592	0x01
//-#define CLK_5184	0x02  
//-#define CLK_1152	0x03  
//-
//-#define MODE_0		0x00
//-#define MODE_1		0x01
//-#define MODE_2		0x02
//-#define MODE_3		0x03
//-
//-#define MODE_8BITS  0x00
//-#define MODE_16BITS 0x01
//-#define MODE_32BITS 0x02
//-
//-#define PHYS_FLASH_SIZE 	0x400000
//-#define FLASH_MEMORY_SIZE	0x400000
//-#define FLASH_PAGE_SIZE		256
//-#define SEC_SIZE_4K			0x1000//0
//-#define SEC_SIZE_32K		0x8000//1
//-#define SEC_SIZE_64K		0x10000//2
//-
//-#define PHYS_FLASH_SECTOR_SIZE SEC_SIZE_4K
//-
//+#define UART_TIMEOUT_FACTOR	1000	/* 1000 */
//+#define MIN(a,b)	((a)<(b)?(a):(b))
//+#if defined SC14450
//+	#define spi_mem_cs_low() SetWord(P0_RESET_DATA_REG, GPIO_10)
//+	#define spi_mem_cs_high() SetWord(P0_SET_DATA_REG, GPIO_10)
//+#elif defined SC14452
//+	#define spi_mem_cs_low() SetWord(P0_RESET_DATA_REG, GPIO_3)
//+	#define spi_mem_cs_high() SetWord(P0_SET_DATA_REG, GPIO_3)
//+#endif
//+
//+
//+#if ((defined SC14452) && (defined REEF))
//+    #define BLUE    GPIO_8
//+    #define GREEN   GPIO_11
//+    #define YELLOW  GPIO_12
//+
//+    #define LED(led,state)          { vLEDControl (led,state); }
//+    #define LED_OFF                 { LED(BLUE,0); LED(GREEN,0); LED(YELLOW,0); }
//+    #define LED_ERROR               { LED_OFF; while (1) { LED(YELLOW,1); DELAY(100); LED(YELLOW,0); DELAY(100); }; }
//+    #define LED_OK                  { LED_OFF; while (1) { LED(GREEN,1);  DELAY(100); LED(GREEN,0);  DELAY(500); }; }
//+    #define LED_SHOW_STATE(x)       { LED(GREEN,(x)&1); LED(BLUE,(x)&2); LED(YELLOW,(x)&4); }
//+#else
//+    #define LED(led,state)
//+    #define LED_OFF
//+    #define LED_ERROR
//+    #define LED_OK
//+    #define LED_SHOW_STATE(x)
//+#endif
//+
//+#ifdef SC14452
//+#define SPI2_CTRL_REG SPI2_CTRL_REG0
//+#define __SPI2_CTRL_REG __SPI2_CTRL_REG0
//+#endif
//+
//+
//+#define CLK_1296	0x00
//+#define CLK_2592	0x01
//+#define CLK_5184	0x02  
//+#define CLK_1152	0x03  
//+
//+#define MODE_0		0x00
//+#define MODE_1		0x01
//+#define MODE_2		0x02
//+#define MODE_3		0x03
//+
//+#define MODE_8BITS  0x00
//+#define MODE_16BITS 0x01
//+#define MODE_32BITS 0x02
//+
//+#define PHYS_FLASH_SIZE 	0x800000
//+#define FLASH_MEMORY_SIZE	0x800000
//+#define FLASH_PAGE_SIZE		256
//+#define SEC_SIZE_4K			0x1000//0
//+#define SEC_SIZE_32K		0x8000//1
//+#define SEC_SIZE_64K		0x10000//2
//+
//+#define PHYS_FLASH_SECTOR_SIZE SEC_SIZE_4K
//+
// #define	MAX_READY_WAIT_COUNT	1000000
//-
//-#define	SPI_FLASH_WREN_CMD			0x06	/* Write Enable */
//-#define	SPI_FLASH_WRDI_CMD			0x04	/* Write Disable */
//-#define	SPI_FLASH_RDID_CMD			0x9F	/* Read Identification */
//-#define	SPI_FLASH_RDSR_CMD			0x05	/* Read Status Register */
//-#define	SPI_FLASH_WRSR_CMD			0x01	/* Write Status Register */
//-#define	SPI_FLASH_READ_CMD			0x03	/* Read Data Bytes */
//-#define	SPI_FLASH_READ_FAST_CMD		0x0B	/* Read Data Bytes in higher speed */
//-#define	SPI_FLASH_PP_CMD			0x02	/* Page Program */
//-#define	SPI_FLASH_SE_CMD			0xD8	/* Sector Erase */
//-#define	SPI_FLASH_BE_CMD			0xC7	/* Bulk Erase */
//-#define SPI_FLASH_DP_CMD			0xB9	/* Deep Power Down */
//-#define	SPI_FLASH_RES_CMD			0xAB	/* Release from Deep Power Down */
//-#define	SPI_FLASH_SE_32K_CMD		0x52	/* Sector Erase 32Kbytes*/
//-#define	SPI_FLASH_SE_4K_CMD			0x20	/* Sector Erase 4KBytes*/
//-
//-/* Status Register Bits */
//-
//-#define STATUS_BUSY		0x01
//-#define	STATUS_WEL		0x02
//-#define	STATUS_BP0		0x04
//-#define	STATUS_BP1		0x08
//-#define	STATUS_BP2		0x10
//-#define	STATUS_WP		0x80
//-
//-#define ERR_OK						0
//-#define ERR_TIMOUT					-1
//-#define ERR_NOT_ERASED				-2
//-#define ERR_PROTECTED				-3
//-#define ERR_INVAL					-4
//-#define ERR_ALIGN					-5
//-#define ERR_UNKNOWN_FLASH_VENDOR	-6
//-#define ERR_UNKNOWN_FLASH_TYPE		-7
//-#define ERR_PROG_ERROR				-8
//-
//-#define DUMMY                  0x00
//-#define IDLE                   0x01
//-#define WAITING_LEN            0x02
//-#define REC_CODE               0x03
//-#define CHK_CRC                0x04
//-#define WAIT_ACK               0x05
//-
//-#define SOH   0x01              
//-#define STX   0x02              
//-#define ACK   0x06              
//-#define NAK   0x15              
//-
//-#define PROG_SOH 0x03
//-#define PROG_STX 0x05
//-#define PROG_ACK 0x07
//-#define PROG_NAK 0x16
//-#define PROG_COM 0x17
//-
//-#define DOWNLOAD_POS 0x100000
//-#define COMPARE_BUFF 0x800000
//-uint8 RecChar;
//-uint32 max_download_size;
//-
//-
//-void init_sdram (void) 
//-{
//-	/* configure GPIOs as SDRAM interface */
//-	SetWord( P0_15_MODE_REG,0x0330 );	/* P0_15 is ACS4 */
//-	SetWord( P1_03_MODE_REG,0x031d );	/* P1_03 is SD_A10 */
//-	SetWord( P1_12_MODE_REG,0x031e );	/* P1_12 is SD_CLK */
//-	SetWord( P1_11_MODE_REG,0x031d );	/* P1_11 is SD_CKE */
//-	SetWord( P1_04_MODE_REG,0x031d );	/* P1_04 is SD_WEN */
//-	SetWord( P1_05_MODE_REG,0x031d );	/* P1_05 is SD_BA0 */
//-	SetWord( P1_06_MODE_REG,0x031d );	/* P1_06 is SD_BA1 */
//-	SetWord( P1_07_MODE_REG,0x031d );	/* P1_07 is SD_LDQM */
//-	SetWord( P1_08_MODE_REG,0x031d );	/* P1_08 is SD_UDQM */
//-	SetWord( P1_09_MODE_REG,0x031d );	/* P1_09 is SD_RAS */
//-	SetWord( P1_10_MODE_REG,0x031d );	/* P1_10 is SD_CAS */
//-
//-	/* sdram controller configuration */
//-	/* base address is 0x20000 */
//-	SetDword( EBI_ACS4_LOW_REG,0x00020000 );
//-	/* sdram size is 16MB */
//-	SetDword( EBI_ACS4_CTRL_REG,0x00000009 );
//-	SetDword( EBI_SDCONR_REG,0x00001168 );
//-	
//-
//-	SetDword( EBI_SDREFR_REG,0x0011050c );
//-	
//-	SetDword( EBI_SDEXN_MODE_REG,0x00000022 );
//-	
//-	SetDword( EBI_SDCTLR_REG,0x00005049 );
//-	while(GetWord(EBI_SDCTLR_REG)& 1);
//-}
//-
//-#if defined SC14450
//-
//-void init_pll(void)
//-{
//-
//-	unsigned i,temp;
//-
//-	/* APB clock is HCLK/2 */
//-	SetWord(CLK_AMBA_REG,0x0010);
//-	/* AHB clock is HCLK */
//-	SetWord(CLK_AMBA_REG,0x0011);
//-
//-	/* Xtal is 12.288 MHz PLL at 165.888 MHz VD = 3*9 XD = /2 */
//-	SetWord(CLK_PLL1_DIV_REG, 0x001c);
//-	/* we setup the PLL2 to give the 25MHz for the ethernet */
//-	/* for 12.288 MHz Xtal we use as XD/VD values 58/0 */ 
//-	SetWord(CLK_PLL2_DIV_REG,0x803e);	
//-//	SetWord(CLK_PLL2_DIV_REG,0x201b);	
//-
//-	/* HF mode enable, VCO on , PLL charge pump on */
//-	SetWord(CLK_PLL1_CTRL_REG,0x0052);	
//-	SetWord(CLK_PLL2_CTRL_REG,0x0016);	
//-//	SetWord(CLK_PLL2_CTRL_REG,0x0012);	
//-
//-	/* wait500usec */
//-	for(i = 0; i < 0x100; ++i)
//-		temp = *(volatile unsigned*)0xff4000;
//-
//-	SetWord(CLK_PLL2_CTRL_REG, 0x001e);	
//-//	SetWord(CLK_PLL2_CTRL_REG, 0x001a);	
//-	/* system clock in PLL mode */
//-	SetWord(CLK_PLL1_CTRL_REG,0x005a);
//-
//-	SetWord(CLK_DSP_REG,0x99);
//-	SetWord(CLK_CODEC_DIV_REG,0x00C8);
//-	SetWord(CLK_CODEC_REG,0x2D55);
//-	SetWord(CLK_AMBA_REG,0x01f1);
//-
//-	/* Peripherial clock connected to PLL/2 at 82.944 MHz divide by 72 for 1.152 MHz */
//-	SetWord(CLK_PER_DIV_REG,0x00c8);	
//-
//-	/* Codec clock connected to PLL/2 at 82.944 MHz divide by 72 for 1.152 MHz */
//-	SetWord(CLK_CODEC_DIV_REG,0x00c8);	
//-
//-	/* Serial clock is 10.368MHz = 82.944MHz/8 */
//-	/* SPI clock is 20.736MHz = (82.944MHz / 2) / 2 */
//-	SetWord(CLK_PER10_DIV_REG,0x0048);	// PER20 /= 2, PER10 /= 1
//-
//-	/* JTAG OWI clock is driven by XTAL */
//-	SetWord(CLK_AUX_REG,0x0020);
//-
//-	/* Disable XTAL startup capacitences */
//-	SetWord(CLK_XTAL_CTRL_REG,0x0022);
//-
//-	/* Change XTAL power from VDD_RF to LDO_XTAL */
//-	SetWord( CLK_XTAL_CTRL_REG,0x0023);	
//-	/* wait 200 usec */
//-	for(i = 0; i < 0x400; ++i)
//-		temp = *(volatile unsigned*)0xff4000;
//-
//-	SetWord( CLK_XTAL_CTRL_REG,0x0021);	
//-
//-}
//-
//-#elif defined SC14452
//-
//-void pll_1_on( void)  /* PLL1 on, Used by CR16C, PROCEDURE FOR 10.368 Mhz */
//-{    
//-	volatile uint16 i;
//-	// PLL CLOCK SWITCHING PROCEDURE
//-	SetWord(CLK_PLL1_DIV_REG, 0x1c);	   // x16
//-
//-	SetWord(CLK_PLL1_CTRL_REG, (PLL1_HF_SEL | PLL1_VCO_ON | PLL1_CP_ON));
//-	SetBits(CLK_GLOBAL_REG, SW_XTAL_PLL1_RATE, 0x1); // XTAL - PLL1 rate = 16
//-
//-	for(i=0;i<50;i++);			   // Wait for approx. 200 us when in XTAL mode and HCLK_DIV=2.
//-
//-	SetBits(CLK_AMBA_REG, SW_PCLK_DIV, 2);     // Divide by 2 (PCLK max = 41 MHz). Must be done in two steps.
//-	SetBits(CLK_AMBA_REG, SW_HCLK_DIV, 4);     // Divide by 4 (HCLK=40 MHz) before switching to PLL mode (SW workaround).
//-	SetBits(CLK_AMBA_REG, SW_HCLK_DIV, 4);     // Same as above .. just for delay until the div register really updates... (NM/07-08-2008).
//-
//-	SetBits(CLK_GLOBAL_REG, SW_SWITCH_CLK, 0); // Go.
//-
//-	while (GetWord (CLK_AUX1_REG) & SWITCH_TO_PLL); //Wait until all clocks switch to pll domain.
//-
//-	SetBits(CLK_AMBA_REG, SW_HCLK_DIV, 2);     // Divide by 2 (HCLK=80 MHz), so put it back again in the HCLK max. frequency setting.
//- }
//-
//-void pll_2_on(void)  /* PLL2 on, in Can be used by QSPIC and EMAC, PROCEDURE FOR 10 Mhz */ 
//-{
//-    volatile uint16 i;
//-
//-    SetWord(CLK_PLL2_DIV_REG , PLL2_DIV5 | 62);             // VD=299, XD=62
//-    SetWord(CLK_PLL2_CTRL_REG, PLL2_VCO_ON | PLL2_CP_ON );  // VCO_ON | CP_ON 
//-    
//-    for(i=0;i<500;i++);                                     // Wait for a while.
//-
//-}
//-
//+
//+#define	SPI_FLASH_WREN_CMD			0x06	/* Write Enable */
//+#define	SPI_FLASH_WRDI_CMD			0x04	/* Write Disable */
//+#define	SPI_FLASH_RDID_CMD			0x9F	/* Read Identification */
//+#define	SPI_FLASH_RDSR_CMD			0x05	/* Read Status Register */
//+#define	SPI_FLASH_WRSR_CMD			0x01	/* Write Status Register */
//+#define	SPI_FLASH_READ_CMD			0x03	/* Read Data Bytes */
//+#define	SPI_FLASH_READ_FAST_CMD		0x0B	/* Read Data Bytes in higher speed */
//+#define	SPI_FLASH_PP_CMD			0x02	/* Page Program */
//+#define	SPI_FLASH_SE_CMD			0xD8	/* Sector Erase */
//+#define	SPI_FLASH_BE_CMD			0xC7	/* Bulk Erase */
//+#define SPI_FLASH_DP_CMD			0xB9	/* Deep Power Down */
//+#define	SPI_FLASH_RES_CMD			0xAB	/* Release from Deep Power Down */
//+#define	SPI_FLASH_SE_32K_CMD		0x52	/* Sector Erase 32Kbytes*/
//+#define	SPI_FLASH_SE_4K_CMD			0x20	/* Sector Erase 4KBytes*/
//+
//+/* Status Register Bits */
//+
//+#define STATUS_BUSY		0x01
//+#define	STATUS_WEL		0x02
//+#define	STATUS_BP0		0x04
//+#define	STATUS_BP1		0x08
//+#define	STATUS_BP2		0x10
//+#define	STATUS_WP		0x80
//+
//+#define ERR_OK						0
//+#define ERR_TIMOUT					-1
//+#define ERR_NOT_ERASED				-2
//+#define ERR_PROTECTED				-3
//+#define ERR_INVAL					-4
//+#define ERR_ALIGN					-5
//+#define ERR_UNKNOWN_FLASH_VENDOR	-6
//+#define ERR_UNKNOWN_FLASH_TYPE		-7
//+#define ERR_PROG_ERROR				-8
//+
//+#define ERROR                  0x00	// 000
//+#define IDLE                   0x01	// 001
//+#define WAITING_LEN            0x02	// 010
//+#define REC_CODE               0x03	// 011
//+#define CHK_CRC                0x04	// 100
//+#define WAIT_ACK               0x05	// 101
//+
//+#define SOH   0x01              
//+#define STX   0x02              
//+#define ACK   0x06              
//+#define NAK   0x15              
//+
//+#define PROG_SOH 0x03
//+#define PROG_STX 0x05
//+#define PROG_ACK 0x07
//+#define PROG_NAK 0x16
//+#define PROG_COM 0x17
//+
//+#define DOWNLOAD_POS 		0x100000
//+#define COMPARE_BUFF 		0x800000
//+#define MAX_COMPARE_BLOCK	0x100000
//+uint8 RecChar;
//+uint32 max_download_size;
//+
//+
//+///< delay macro in ms
//+//#define DELAY(x)	{ for(ui1=0; ui1<(5*(x)); ui1++) for(ui2=0; ui2<800; ui2++); }
//+#define DELAY(x)	{ delay_ms(x); }
//+
//+/**
//+ * @brief       Simple and not accurate delay function
//+ * @author      Wojciech Nizinski
//+ * @param ms    miliseconds
//+ */
//+static void delay_ms(uint32 ms)
//+{
//+    volatile uint32 ui1;
//+    for (ui1=0; ui1<(5*(ms)); ui1++) 
//+      __asm__ __volatile__
//+      (\
//+        "push $2,r6                       /* store 2 registers starting from r6 */      \n"\
//+        "subd (r7, r6), (r7, r6)          /* zero counter (r6,r7) */                    \n"\
//+        "addd $1500, (r7, r6)             /* init counter with 1500 */                  \n"\
//+        "1:                                                                             \n"\
//+        "subd $1, (r7,r6)                 /* decrement counter by 1 */                  \n"\
//+        "cmpd $0, (r7,r6)                 /* compare with zero */                       \n"\
//+        "bne 1b                           /* loop if not zero */                        \n"\
//+        "pop $2,r6                        /* restore 2 registers starting from r6 */    \n"\
//+      );
//+}
//+
//+#if ((defined SC14452) && (defined REEF))
//+static void vLEDControl (uint16 port_id, uint8 state)
//+{
//+    if (state==0)
//+    {
//+        SetBits(P2_RESET_DATA_REG, port_id,  1);
//+    }
//+    else
//+    {
//+        SetBits(P2_SET_DATA_REG, port_id,  1);    
//+    }
//+}
//+#endif
//+
//+void init_sdram (void) 
//+{
//+	/* configure GPIOs as SDRAM interface */
//+	SetWord( P0_15_MODE_REG,0x0330 );	/* P0_15 is ACS4 */
//+	SetWord( P1_03_MODE_REG,0x031d );	/* P1_03 is SD_A10 */
//+	SetWord( P1_12_MODE_REG,0x031e );	/* P1_12 is SD_CLK */
//+	SetWord( P1_11_MODE_REG,0x031d );	/* P1_11 is SD_CKE */
//+	SetWord( P1_04_MODE_REG,0x031d );	/* P1_04 is SD_WEN */
//+	SetWord( P1_05_MODE_REG,0x031d );	/* P1_05 is SD_BA0 */
//+	SetWord( P1_06_MODE_REG,0x031d );	/* P1_06 is SD_BA1 */
//+	SetWord( P1_07_MODE_REG,0x031d );	/* P1_07 is SD_LDQM */
//+	SetWord( P1_08_MODE_REG,0x031d );	/* P1_08 is SD_UDQM */
//+	SetWord( P1_09_MODE_REG,0x031d );	/* P1_09 is SD_RAS */
//+	SetWord( P1_10_MODE_REG,0x031d );	/* P1_10 is SD_CAS */
//+
//+	/* sdram controller configuration */
//+	/* base address is 0x20000 */
//+	SetDword( EBI_ACS4_LOW_REG,0x00020000 );
//+	/* sdram size is 16MB */
//+	SetDword( EBI_ACS4_CTRL_REG,0x00000009 );
//+	SetDword( EBI_SDCONR_REG,0x00001168 );
//+	
//+
//+	SetDword( EBI_SDREFR_REG,0x0011050c );
//+	
//+	SetDword( EBI_SDEXN_MODE_REG,0x00000022 );
//+	
//+	SetDword( EBI_SDCTLR_REG,0x00005049 );
//+	while(GetWord(EBI_SDCTLR_REG)& 1);
//+}
//+
//+#if defined SC14450
//+
//+void init_pll(void)
//+{
//+
//+	unsigned i,temp;
//+
//+	/* APB clock is HCLK/2 */
//+	SetWord(CLK_AMBA_REG,0x0010);
//+	/* AHB clock is HCLK */
//+	SetWord(CLK_AMBA_REG,0x0011);
//+
//+	/* Xtal is 12.288 MHz PLL at 165.888 MHz VD = 3*9 XD = /2 */
//+	SetWord(CLK_PLL1_DIV_REG, 0x001c);
//+	/* we setup the PLL2 to give the 25MHz for the ethernet */
//+	/* for 12.288 MHz Xtal we use as XD/VD values 58/0 */ 
//+	SetWord(CLK_PLL2_DIV_REG,0x803e);	
//+//	SetWord(CLK_PLL2_DIV_REG,0x201b);	
//+
//+	/* HF mode enable, VCO on , PLL charge pump on */
//+	SetWord(CLK_PLL1_CTRL_REG,0x0052);	
//+	SetWord(CLK_PLL2_CTRL_REG,0x0016);	
//+//	SetWord(CLK_PLL2_CTRL_REG,0x0012);	
//+
//+	/* wait500usec */
//+	for(i = 0; i < 0x100; ++i)
//+		temp = *(volatile unsigned*)0xff4000;
//+
//+	SetWord(CLK_PLL2_CTRL_REG, 0x001e);	
//+//	SetWord(CLK_PLL2_CTRL_REG, 0x001a);	
//+	/* system clock in PLL mode */
//+	SetWord(CLK_PLL1_CTRL_REG,0x005a);
//+
//+	SetWord(CLK_DSP_REG,0x99);
//+	SetWord(CLK_CODEC_DIV_REG,0x00C8);
//+	SetWord(CLK_CODEC_REG,0x2D55);
//+	SetWord(CLK_AMBA_REG,0x01f1);
//+
//+	/* Peripherial clock connected to PLL/2 at 82.944 MHz divide by 72 for 1.152 MHz */
//+	SetWord(CLK_PER_DIV_REG,0x00c8);	
//+
//+	/* Codec clock connected to PLL/2 at 82.944 MHz divide by 72 for 1.152 MHz */
//+	SetWord(CLK_CODEC_DIV_REG,0x00c8);	
//+
//+	/* Serial clock is 10.368MHz = 82.944MHz/8 */
//+	/* SPI clock is 20.736MHz = (82.944MHz / 2) / 2 */
//+	SetWord(CLK_PER10_DIV_REG,0x0048);	// PER20 /= 2, PER10 /= 1
//+
//+	/* JTAG OWI clock is driven by XTAL */
//+	SetWord(CLK_AUX_REG,0x0020);
//+
//+	/* Disable XTAL startup capacitences */
//+	SetWord(CLK_XTAL_CTRL_REG,0x0022);
//+
//+	/* Change XTAL power from VDD_RF to LDO_XTAL */
//+	SetWord( CLK_XTAL_CTRL_REG,0x0023);	
//+	/* wait 200 usec */
//+	for(i = 0; i < 0x400; ++i)
//+		temp = *(volatile unsigned*)0xff4000;
//+
//+	SetWord( CLK_XTAL_CTRL_REG,0x0021);	
//+
//+}
//+
//+#elif defined SC14452
//+
//+void pll_1_on( void)  /* PLL1 on, Used by CR16C, PROCEDURE FOR 10.368 Mhz */
//+{    
//+	volatile uint16 i;
//+	// PLL CLOCK SWITCHING PROCEDURE
//+	SetWord(CLK_PLL1_DIV_REG, 0x1c);	   // x16
//+
//+	SetWord(CLK_PLL1_CTRL_REG, (PLL1_HF_SEL | PLL1_VCO_ON | PLL1_CP_ON));
//+	SetBits(CLK_GLOBAL_REG, SW_XTAL_PLL1_RATE, 0x1); // XTAL - PLL1 rate = 16
//+
//+	for(i=0;i<50;i++);			   // Wait for approx. 200 us when in XTAL mode and HCLK_DIV=2.
//+
//+	SetBits(CLK_AMBA_REG, SW_PCLK_DIV, 2);     // Divide by 2 (PCLK max = 41 MHz). Must be done in two steps.
//+	SetBits(CLK_AMBA_REG, SW_HCLK_DIV, 4);     // Divide by 4 (HCLK=40 MHz) before switching to PLL mode (SW workaround).
//+	SetBits(CLK_AMBA_REG, SW_HCLK_DIV, 4);     // Same as above .. just for delay until the div register really updates... (NM/07-08-2008).
//+
//+	SetBits(CLK_GLOBAL_REG, SW_SWITCH_CLK, 0); // Go.
//+
//+	while (GetWord (CLK_AUX1_REG) & SWITCH_TO_PLL); //Wait until all clocks switch to pll domain.
//+
//+	SetBits(CLK_AMBA_REG, SW_HCLK_DIV, 2);     // Divide by 2 (HCLK=80 MHz), so put it back again in the HCLK max. frequency setting.
//+ }
//+
//+void pll_2_on(void)  /* PLL2 on, in Can be used by QSPIC and EMAC, PROCEDURE FOR 10 Mhz */ 
//+{
//+    volatile uint16 i;
//+
//+    SetWord(CLK_PLL2_DIV_REG , PLL2_DIV5 | 62);             // VD=299, XD=62
//+    SetWord(CLK_PLL2_CTRL_REG, PLL2_VCO_ON | PLL2_CP_ON );  // VCO_ON | CP_ON 
//+    
//+    for(i=0;i<500;i++);                                     // Wait for a while.
//+
//+}
//+
// #endif
//-
//-
//-void init_SPI2(void)
//-{ 
//-#if defined SC14450
//-  
//-	/* Set PPA MATRIX FOR SPI2 */
//-	SetPort(P0_12_MODE_REG, PORT_OUTPUT,  PID_SPI2_DOUT);   /* P0_12 as SPI_DO  */
//-	SetPort(P0_13_MODE_REG, PORT_INPUT,   PID_SPI2_DIN);    /* P0_13 as SPI_DI  */
//-	SetPort(P0_14_MODE_REG, PORT_OUTPUT,  PID_SPI2_CLK);    /* P0_14 as SPI_CLK */
//-	SetPort(P0_10_MODE_REG, PORT_OUTPUT,  PID_port);        /* P0_15 as SPI_EN  */  
//-	/* Set SPI SETTINGS */
//-
//-#elif defined SC14452
//-	SetWord(P0_DATA_REG, GPIO_3 | GPIO_6 | GPIO_12 | GPIO_13 | GPIO_14);  // Note: QSPI.WPn=P0[11]=0
//-	SetPort(P0_03_MODE_REG, PORT_OUTPUT,  PID_port);	              // 452: Chip Select
//-	SetPort(P0_06_MODE_REG, PORT_OUTPUT,  PID_SPI2_CLK);	              // 452: use SPI clk the P0[6] (instead of P0[14])
//-	SetPort(P0_11_MODE_REG, PORT_OUTPUT,  PID_port);	              // 452: Write protect
//-	SetPort(P0_12_MODE_REG, PORT_OUTPUT,  PID_SPI2_DOUT);	              // Set PPA
//-	SetPort(P0_13_MODE_REG, PORT_PULL_UP, PID_SPI2_DIN);
//-	SetPort(P0_14_MODE_REG, PORT_OUTPUT,  PID_port);	              // 452: QSPI HOLDn pin (keep always high)
//-
//-	// set SPI2_CLK = 20.736 MHz...
//-	if (GetWord(CLK_GLOBAL_REG) & SW_XTAL_PLL1_RATE)
//-		SetBits(CLK_GPIO2_REG, SW_SPI2_DIV, 8);
//-	else
//-		SetBits(CLK_GPIO2_REG, SW_SPI2_DIV, 4);
//-
//-	// Stop spi2 clock
//-	SetBits(CLK_GPIO3_REG, SW_SPI2_EN,  0);  
//-	  
//-	// Enable spi2 clk
//-	SetBits(CLK_GPIO3_REG, SW_SPI2_EN,  1); 
//-#endif
//-
//-	SetBits(SPI2_CTRL_REG, SPI_WORD, 0x00);                 // Set SPI 8bits Mode,  Serial Flash Device
//-	SetBits(SPI2_CTRL_REG, SPI_SMN,  0x00);                 // Set SPI IN MASTER MODE
//-	SetBits(SPI2_CTRL_REG, SPI_POL,  0x01);  
//-	SetBits(SPI2_CTRL_REG, SPI_PHA,  0x01);					// MODE 3: SPI2_POL=1  and PI2_PHA=1
//-	SetBits(SPI2_CTRL_REG, SPI_MINT, 0x01);                 // Disable SPI2_INT to ICU, timer0 is used
//-	SetBits(SPI2_CTRL_REG, SPI_ON, 1);                      // Enable SPI2 
//-
//-	SetBits(SPI2_CTRL_REG, SPI_CLK,  CLK_5184);                 // SPI_CLK = XTAL/(PER20_DIV/4) = 2.592 MHz
//-
//-}
//-
//-bool putByte_SPI2(uint32 x)
//-/******************************************************************************
//- *  Write a single item to serial flash via SPI
//- *****************************************************************************/
//-{ 
//-  SetWord(SPI2_RX_TX_REG0, (uint8) x);
//-  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==0));
//-  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==1)) 
//-  {
//-	  SetWord(SPI2_CLEAR_INT_REG, 0x01);
//-  }
//-  return TRUE;
//-}
//-
//-bool getByte_SPI2(uint32* x  )
//-/******************************************************************************
//-*  Read a single item from serial flash via SPI
//-*****************************************************************************/
//-{
//-  SetWord(SPI2_RX_TX_REG0, (uint16) 0xffff);
//-  
//-  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==0));
//-  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==1)) 
//-  {
//-    SetWord(SPI2_CLEAR_INT_REG, 0x01);
//-  }
//-   *(uint8*)x=(uint8)GetWord(SPI2_RX_TX_REG0); 
//-  return TRUE;
//-} 
//-
//-uint8 SpiFlashReadStatus( void ) 
//-{
//-	uint32 temp;
//-
//-	spi_mem_cs_low();
//-	putByte_SPI2(SPI_FLASH_RDSR_CMD);
//-	getByte_SPI2(&temp);
//-	spi_mem_cs_high();
//-
//-	return (uint8)temp;
//-}
//-
//-int SpiFlashWaitTillReady (void)
//-{
//-  int count;
//-  uint8 status;
//-  for (count = 0; count < MAX_READY_WAIT_COUNT; count++)
//-  {
//-    status = SpiFlashReadStatus();
//-    if (!(status & STATUS_BUSY))
//-      return 0;
//-  }
//-  return 1;
//-}
//-
//-int SpiFlashSetWriteEbable(void)
//-{
//-	if (SpiFlashWaitTillReady())
//-		return ERR_TIMOUT;
//-
//-	spi_mem_cs_low();
//-	putByte_SPI2(SPI_FLASH_WREN_CMD);
//-	spi_mem_cs_high();
//-
//-	return ERR_OK;
//-
//-}
//-
//-uint32 SpiFlashID( void ) 
//-{
//-	unsigned long ret,i ;
//-	
//-	if (SpiFlashWaitTillReady())
//-		return ERR_TIMOUT;
//-
//-	if (SpiFlashSetWriteEbable())
//-		return ERR_TIMOUT;
//-
//-	spi_mem_cs_low();
//-	putByte_SPI2(SPI_FLASH_RDID_CMD);
//-	getByte_SPI2(&i);
//-	ret = ((i&0xff)<<16);
//-	getByte_SPI2(&i);
//-	ret |= ((i&0xff)<<8);
//-	getByte_SPI2(&i);
//-	ret |= (i&0xff);
//-	spi_mem_cs_high();
//-
//-	return ret;
//-}
//-
//-int SpiFlashRead(uint32 source_addr,uint8* destination_buffer,uint32 len)
//-{
//-
//-	if (!len)
//-		return 0;
//-	if ((source_addr+len) > FLASH_MEMORY_SIZE)
//-		return ERR_INVAL;
//-
//-	if (SpiFlashWaitTillReady())
//-		return ERR_TIMOUT;
//-
//-	spi_mem_cs_low();
//-	putByte_SPI2(SPI_FLASH_READ_CMD);
//-	putByte_SPI2((source_addr&0xffffff)>>16);
//-	putByte_SPI2((source_addr&0xffff)>>8);
//-	putByte_SPI2(source_addr&0xff);
//-	while(len-- >0)
//-	{
//-		getByte_SPI2((uint32*)destination_buffer);
//-		destination_buffer++;
//-	}
//-	spi_mem_cs_high();
//-
//-	return ERR_OK;
//-}
//-
//-int SpiFlashProgramPage(uint32 dest_addr,uint8* data, uint32 len)
//-{
//-	uint16 index;
//-	if (!len)
//-		return ERR_OK;
//-	if (len > FLASH_PAGE_SIZE)
//-		return ERR_INVAL;
//-    
//-  if (SpiFlashSetWriteEbable())
//-		return ERR_TIMOUT;  
//-  if (SpiFlashWaitTillReady())
//-		return ERR_TIMOUT;
//-
//-	spi_mem_cs_low();
//-	putByte_SPI2(SPI_FLASH_PP_CMD);
//-	putByte_SPI2((dest_addr&0xffffff)>>16);
//-	putByte_SPI2((dest_addr&0xffff)>>8);
//-	putByte_SPI2(dest_addr&0xff);
//-	for (index =0; index < len; index++)
//-		putByte_SPI2(data[index]);
//-	spi_mem_cs_high();
//-
//-	return ERR_OK;
//-
//-}
//-
//-int SpiFlashProgram(uint32 dest_addr,uint8* data, uint32 len)
//-{
//-	
//-	uint32 index,page_offset,page_size;
//-	int retvalue = 0;
//-
//-	if (!len)
//-		return ERR_OK;
//-	if ((dest_addr+len) > PHYS_FLASH_SIZE)
//-		return ERR_INVAL;
//-	
//-	page_offset = dest_addr % FLASH_PAGE_SIZE;
//-
//-	if ((page_offset + len) <= FLASH_PAGE_SIZE)  
//-	{
//-		retvalue = SpiFlashProgramPage(dest_addr,data,len);
//-		return retvalue;
//-	}
//-	else
//-	{
//-		page_size = FLASH_PAGE_SIZE - page_offset;
//-		retvalue = SpiFlashProgramPage(dest_addr,data,page_size);
//-
//-		if (retvalue != 0)
//-			return retvalue;
//-
//-		for (index = page_size; index < len; index+=page_size)
//-		{
//-			page_size = len - index;
//-			if (page_size > FLASH_PAGE_SIZE)
//-				page_size = FLASH_PAGE_SIZE;
//-
//-			retvalue = SpiFlashProgramPage(dest_addr + index,&data[index],page_size);
//-
//-			if (retvalue != 0)
//-				return retvalue;
//-		}
//-	}
//-	return ERR_OK;
//-}
//-
//-int SpiFlashEraseSector(uint32 sector_address)
//-{
//-  
//-	uint8 status;
//-
//-	if (SpiFlashSetWriteEbable())
//-		return ERR_TIMOUT;
//-
//-	status = SpiFlashReadStatus();
//-	if (( status & 0x3c) != 0) {
//-	// write protected
//-		spi_mem_cs_low();
//-		putByte_SPI2(SPI_FLASH_WRSR_CMD);
//-		putByte_SPI2(0);
//-		spi_mem_cs_high();
//-		
//-		if (SpiFlashSetWriteEbable())
//-			return ERR_TIMOUT;
//- 
//-	}
//-
//-	spi_mem_cs_low();
//-	putByte_SPI2(SPI_FLASH_SE_4K_CMD);
//-	putByte_SPI2((sector_address&0xffffff)>>16);
//-	putByte_SPI2((sector_address&0xffff)>>8);
//-	putByte_SPI2(sector_address&0xff);
//-	spi_mem_cs_high();  
//-	
//-	return ERR_OK;			
//-}
//-
//-
//-uint8 ReceiveChar(void)
//-{
//-   uint32 timeout;
//-
//-   SetWord(UART_CLEAR_RX_INT_REG,0x01);
//-   for (timeout = 850000; timeout > 0;  timeout --)
//-   {
//-       if ( GetBits(UART_CTRL_REG,RI) == 1)
//-       {                                   /* We really got a Byte from the other side */
//-           RecChar = GetWord(UART_RX_TX_REG);  
//-           return ( TRUE);
//-       }
//-   }
//-   return ( FALSE );
//-}
//-
//-void SendChar(unsigned char tra_byte)  /* (V0.5) WORD changed to BYTE for byte oriented UART. */
//-{
//-   uint32 timeout;
//- 
//-   SetWord(UART_RX_TX_REG,tra_byte);               /* Transmit character */
//-   SetWord(UART_CLEAR_TX_INT_REG,0x01);            /* clear TI this will start transmission */
//-
//-   for ( timeout = 150000; timeout > 0; timeout --)  /* Wait till really send */
//-   {
//-       if( GetBits(UART_CTRL_REG,TI) == 1 )
//-       {
//-           break;                                   /* Byte moved to the shift register */
//-       }
//-   }
//-
//-}
//-
//+
//+
//+void init_SPI2(void)
//+{ 
//+#if defined SC14450
//+  
//+	/* Set PPA MATRIX FOR SPI2 */
//+	SetPort(P0_12_MODE_REG, PORT_OUTPUT,  PID_SPI2_DOUT);   /* P0_12 as SPI_DO  */
//+	SetPort(P0_13_MODE_REG, PORT_INPUT,   PID_SPI2_DIN);    /* P0_13 as SPI_DI  */
//+	SetPort(P0_14_MODE_REG, PORT_OUTPUT,  PID_SPI2_CLK);    /* P0_14 as SPI_CLK */
//+	SetPort(P0_10_MODE_REG, PORT_OUTPUT,  PID_port);        /* P0_15 as SPI_EN  */  
//+	/* Set SPI SETTINGS */
//+
//+#elif defined SC14452
//+	SetWord(P0_DATA_REG, GPIO_3 | GPIO_6 | GPIO_12 | GPIO_13 | GPIO_14);  // Note: QSPI.WPn=P0[11]=0
//+	SetPort(P0_03_MODE_REG, PORT_OUTPUT,  PID_port);	              // 452: Chip Select
//+	SetPort(P0_06_MODE_REG, PORT_OUTPUT,  PID_SPI2_CLK);	              // 452: use SPI clk the P0[6] (instead of P0[14])
//+	SetPort(P0_11_MODE_REG, PORT_OUTPUT,  PID_port);	              // 452: Write protect
//+	SetPort(P0_12_MODE_REG, PORT_OUTPUT,  PID_SPI2_DOUT);	              // Set PPA
//+	SetPort(P0_13_MODE_REG, PORT_PULL_UP, PID_SPI2_DIN);
//+	SetPort(P0_14_MODE_REG, PORT_OUTPUT,  PID_port);	              // 452: QSPI HOLDn pin (keep always high)
//+
//+	// set SPI2_CLK = 20.736 MHz...
//+	if (GetWord(CLK_GLOBAL_REG) & SW_XTAL_PLL1_RATE)
//+		SetBits(CLK_GPIO2_REG, SW_SPI2_DIV, 8);
//+	else
//+		SetBits(CLK_GPIO2_REG, SW_SPI2_DIV, 4);
//+
//+	// Stop spi2 clock
//+	SetBits(CLK_GPIO3_REG, SW_SPI2_EN,  0);  
//+	  
//+	// Enable spi2 clk
//+	SetBits(CLK_GPIO3_REG, SW_SPI2_EN,  1); 
//+#endif
//+
//+	SetBits(SPI2_CTRL_REG, SPI_WORD, 0x00);                 // Set SPI 8bits Mode,  Serial Flash Device
//+	SetBits(SPI2_CTRL_REG, SPI_SMN,  0x00);                 // Set SPI IN MASTER MODE
//+	SetBits(SPI2_CTRL_REG, SPI_POL,  0x01);  
//+	SetBits(SPI2_CTRL_REG, SPI_PHA,  0x01);					// MODE 3: SPI2_POL=1  and PI2_PHA=1
//+	SetBits(SPI2_CTRL_REG, SPI_MINT, 0x01);                 // Disable SPI2_INT to ICU, timer0 is used
//+	SetBits(SPI2_CTRL_REG, SPI_ON, 1);                      // Enable SPI2 
//+
//+	SetBits(SPI2_CTRL_REG, SPI_CLK,  CLK_5184);                 // SPI_CLK = XTAL/(PER20_DIV/4) = 2.592 MHz
//+
//+}
//+
//+bool putByte_SPI2(uint32 x)
//+/******************************************************************************
//+ *  Write a single item to serial flash via SPI
//+ *****************************************************************************/
//+{ 
//+  SetWord(SPI2_RX_TX_REG0, (uint8) x);
//+  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==0));
//+  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==1)) 
//+  {
//+	  SetWord(SPI2_CLEAR_INT_REG, 0x01);
//+  }
//+  return TRUE;
//+}
//+
//+bool getByte_SPI2(uint32* x  )
//+/******************************************************************************
//+*  Read a single item from serial flash via SPI
//+*****************************************************************************/
//+{
//+  SetWord(SPI2_RX_TX_REG0, (uint16) 0xffff);
//+  
//+  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==0));
//+  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==1)) 
//+  {
//+    SetWord(SPI2_CLEAR_INT_REG, 0x01);
//+  }
//+   *(uint8*)x=(uint8)GetWord(SPI2_RX_TX_REG0); 
//+  return TRUE;
//+} 
//+
//+uint8 SpiFlashReadStatus( void ) 
//+{
//+	uint32 temp;
//+
//+	spi_mem_cs_low();
//+	putByte_SPI2(SPI_FLASH_RDSR_CMD);
//+	getByte_SPI2(&temp);
//+	spi_mem_cs_high();
//+
//+	return (uint8)temp;
//+}
//+
//+int SpiFlashWaitTillReady (void)
//+{
//+  int count;
//+  uint8 status;
//+  for (count = 0; count < MAX_READY_WAIT_COUNT; count++)
//+  {
//+    status = SpiFlashReadStatus();
//+    if (!(status & STATUS_BUSY))
//+      return 0;
//+  }
//+  return 1;
//+}
//+
//+int SpiFlashSetWriteEbable(void)
//+{
//+	if (SpiFlashWaitTillReady())
//+		return ERR_TIMOUT;
//+
//+	spi_mem_cs_low();
//+	putByte_SPI2(SPI_FLASH_WREN_CMD);
//+	spi_mem_cs_high();
//+
//+	return ERR_OK;
//+
//+}
//+
//+uint32 SpiFlashID( void ) 
//+{
//+	unsigned long ret,i ;
//+	
//+	if (SpiFlashWaitTillReady())
//+		return ERR_TIMOUT;
//+
//+	if (SpiFlashSetWriteEbable())
//+		return ERR_TIMOUT;
//+
//+	spi_mem_cs_low();
//+	putByte_SPI2(SPI_FLASH_RDID_CMD);
//+	getByte_SPI2(&i);
//+	ret = ((i&0xff)<<16);
//+	getByte_SPI2(&i);
//+	ret |= ((i&0xff)<<8);
//+	getByte_SPI2(&i);
//+	ret |= (i&0xff);
//+	spi_mem_cs_high();
//+
//+	return ret;
//+}
//+
//+int SpiFlashRead(uint32 source_addr,uint8* destination_buffer,uint32 len)
//+{
//+
//+	if (!len)
//+		return 0;
//+	if ((source_addr+len) > FLASH_MEMORY_SIZE)
//+		return ERR_INVAL;
//+
//+	if (SpiFlashWaitTillReady())
//+		return ERR_TIMOUT;
//+
//+	spi_mem_cs_low();
//+	putByte_SPI2(SPI_FLASH_READ_CMD);
//+	putByte_SPI2((source_addr&0xffffff)>>16);
//+	putByte_SPI2((source_addr&0xffff)>>8);
//+	putByte_SPI2(source_addr&0xff);
//+	while(len-- >0)
//+	{
//+		getByte_SPI2((uint32*)destination_buffer);
//+		destination_buffer++;
//+	}
//+	spi_mem_cs_high();
//+
//+	return ERR_OK;
//+}
//+
//+int SpiFlashProgramPage(uint32 dest_addr,uint8* data, uint32 len)
//+{
//+	uint16 index;
//+	if (!len)
//+		return ERR_OK;
//+	if (len > FLASH_PAGE_SIZE)
//+		return ERR_INVAL;
//+    
//+  if (SpiFlashSetWriteEbable())
//+		return ERR_TIMOUT;  
//+  if (SpiFlashWaitTillReady())
//+		return ERR_TIMOUT;
//+
//+	spi_mem_cs_low();
//+	putByte_SPI2(SPI_FLASH_PP_CMD);
//+	putByte_SPI2((dest_addr&0xffffff)>>16);
//+	putByte_SPI2((dest_addr&0xffff)>>8);
//+	putByte_SPI2(dest_addr&0xff);
//+	for (index =0; index < len; index++)
//+		putByte_SPI2(data[index]);
//+	spi_mem_cs_high();
//+
//+	return ERR_OK;
//+
//+}
//+
//+int SpiFlashProgram(uint32 dest_addr,uint8* data, uint32 len)
//+{
//+	
//+	uint32 index,page_offset,page_size;
//+	int retvalue = 0;
//+
//+	if (!len)
//+		return ERR_OK;
//+	if ((dest_addr+len) > PHYS_FLASH_SIZE)
//+		return ERR_INVAL;
//+	
//+	page_offset = dest_addr % FLASH_PAGE_SIZE;
//+
//+	if ((page_offset + len) <= FLASH_PAGE_SIZE)  
//+	{
//+		retvalue = SpiFlashProgramPage(dest_addr,data,len);
//+		return retvalue;
//+	}
//+	else
//+	{
//+		page_size = FLASH_PAGE_SIZE - page_offset;
//+		retvalue = SpiFlashProgramPage(dest_addr,data,page_size);
//+
//+		if (retvalue != 0)
//+			return retvalue;
//+
//+		for (index = page_size; index < len; index+=page_size)
//+		{
//+			page_size = len - index;
//+			if (page_size > FLASH_PAGE_SIZE)
//+				page_size = FLASH_PAGE_SIZE;
//+
//+			retvalue = SpiFlashProgramPage(dest_addr + index,&data[index],page_size);
//+
//+			if (retvalue != 0)
//+				return retvalue;
//+		}
//+	}
//+	return ERR_OK;
//+}
//+
//+int SpiFlashEraseSector(uint32 sector_address)
//+{
//+  
//+	uint8 status;
//+
//+	if (SpiFlashSetWriteEbable())
//+		return ERR_TIMOUT;
//+
//+	status = SpiFlashReadStatus();
//+	if (( status & 0x3c) != 0) {
//+	// write protected
//+		spi_mem_cs_low();
//+		putByte_SPI2(SPI_FLASH_WRSR_CMD);
//+		putByte_SPI2(0);
//+		spi_mem_cs_high();
//+		
//+		if (SpiFlashSetWriteEbable())
//+			return ERR_TIMOUT;
//+ 
//+	}
//+
//+	spi_mem_cs_low();
//+	putByte_SPI2(SPI_FLASH_SE_4K_CMD);
//+	putByte_SPI2((sector_address&0xffffff)>>16);
//+	putByte_SPI2((sector_address&0xffff)>>8);
//+	putByte_SPI2(sector_address&0xff);
//+	spi_mem_cs_high();  
//+	
//+	return ERR_OK;			
//+}
//+
//+
//+uint8 ReceiveChar(void)
//+{
//+   uint32 timeout;
//+
//+   SetWord(UART_CLEAR_RX_INT_REG, 0x01);
//+   for (timeout = 850*UART_TIMEOUT_FACTOR; timeout > 0;  timeout --)
//+   {
//+       if ( GetBits(UART_CTRL_REG,RI) == 1)
//+       {                                   /* We really got a Byte from the other side */
//+           RecChar = GetWord(UART_RX_TX_REG);  
//+           return (TRUE);
//+       }
//+   }
//+   return ( FALSE );
//+}
//+
//+void SendChar(unsigned char tra_byte)  /* (V0.5) WORD changed to BYTE for byte oriented UART. */
//+{
//+   uint32 timeout;
//+    DELAY(100);
//+   SetWord(UART_RX_TX_REG, tra_byte);               /* Transmit character */
//+   SetWord(UART_CLEAR_TX_INT_REG,0x01);            /* clear TI this will start transmission */
//+
//+   for ( timeout = 150*UART_TIMEOUT_FACTOR; timeout > 0; timeout --)  /* Wait till really send */
//+   {
//+       if( GetBits(UART_CTRL_REG,TI) == 1 )
//+       {
//+           break;                                   /* Byte moved to the shift register */
//+       }
//+   }
//+}
//+
// 
// int main(void)
// {
//-	uint32 temp;
//-	uint32 length=0;
//-	uint32 i;
//-	uint16 sect;
//-	uint8 state;      
//-	uint8 crc;        
//-	uint8 *ptr_code;
//-	uint8 *temp_ptr;  
//+	uint32 temp;
//+	uint32 length=0;
//+	uint32 i;
//+	uint16 sect;
//+	uint8 state;      
//+	uint8 crc;        
//+	uint8 *ptr_code;
//+	uint8 *temp_ptr;  
// 
// 	/* Freeze Watchdog */
//-	SetWord(SET_FREEZE_REG,FRZ_WDOG);
//-
//-#if defined SC14450
//-	SetBits(BAT_CTRL_REG,REG_ON,1);
//-	SetBits(BAT_CTRL_REG,DC_HYST,0);
//-	SetBits(BAT_CTRL2_REG,CHARGE_LEVEL,0x1e);
//-	SetBits(BAT_CTRL2_REG,NTC_DISABLE,1);
//-	SetBits(BAT_CTRL2_REG,CHARGE_CUR,7);
//-
//-	init_pll();
//-
//-	SetPort(P2_04_MODE_REG, PORT_OUTPUT,  PID_ACS1);        /* P2_04 is ACS1 */
//-	SetPort(P2_05_MODE_REG, PORT_OUTPUT,  PID_ACS2);        /* P2_05 is ACS2 */
//-	SetPort(P2_06_MODE_REG, PORT_OUTPUT,  PID_ACS3);        /* P2_06 is ACS3 */
//-	
//-#elif defined SC14452
//-	// Disable additional capacitors.
//-	SetBits (CLK_XTAL_CTRL_REG, XTAL_EXTRA_CV, 0);
//-	// Start PLL 1 and switch
//-	pll_1_on();
//-	
//-	// Start PLL 2 
//-	pll_2_on();
//-	
//-	// Enable UART clock
//-	if (GetWord(CLK_GLOBAL_REG) & SW_XTAL_PLL1_RATE)
//-		SetWord(CLK_GPIO6_REG, SW_UART_EN | 0x10);
//-	else
//-		SetWord(CLK_GPIO6_REG, SW_UART_EN | 0x8);
//-	
//-#endif	
//-	
//-	init_sdram();	
//-	init_SPI2();
//-
//-		
//-	SetWord(UART_CTRL_REG,0);
//-	/* TODO niziak: temporary disabled */
//-//	SetBits( UART_CTRL_REG, BAUDRATE, CLK_1152);
//-	SetBits(UART_CTRL_REG, UART_REN, 1); // enable receive
//-	SetBits(UART_CTRL_REG, UART_TEN, 1); // enable transmit
//-
//-	max_download_size = FLASH_MEMORY_SIZE;
//-	
//-	state = IDLE;
//-	
//-		/*   Config PPA for UART. */
//-	SetPort(P0_00_MODE_REG, PORT_OUTPUT,    PID_UTX);
//-	SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_URX);
//-
//-
//-	while(1)
//-	{
//-		switch(state)
//-		{
//-			case IDLE:
//-			{
//-				while(1)
//-				{
//-					SendChar(PROG_STX);
//-					if (ReceiveChar())
//-					{
//-						if (RecChar==PROG_SOH)
//-						{
//-							state = WAITING_LEN;
//-							break;
//-						}
//-					}
//-				}
//-				break;
//-			}
//-			case WAITING_LEN:
//-			{
//-				if (ReceiveChar())
//-				{
//-					length = RecChar;
//-					if (ReceiveChar())
//-					{
//-						length += RecChar<<8;
//-						if (ReceiveChar())
//-						{
//-							length += RecChar<<16;
//-							if (ReceiveChar())
//-							{
//-								length += RecChar<<24;
//-								if (length > max_download_size || length == 0)
//-								{
//-									SendChar(PROG_NAK);
//-								}
//-								else
//-								{
//-									SendChar(PROG_ACK);
//-									state = REC_CODE;
//-									break;
//-								}
//-							}
//-						}
//-					}
//-				}
//-				state = IDLE;
//-				break;
//-			}
//-			case REC_CODE:
//-			{
//-				ptr_code = (uint8*)DOWNLOAD_POS;
//-				for (i=0;i<length;i++,ptr_code++)
//-				{
//-					if (!ReceiveChar())
//-					{
//-						state = IDLE;
//-						break;
//-					}
//-					else
//-					{
//-						*ptr_code = RecChar;
//-					}
//-				}
//-				if (state == REC_CODE)
//-				{
//-					state = CHK_CRC;
//-				}
//-				break;
//-			}
//-			case CHK_CRC:
//-			{
//-				ptr_code = (uint8*)DOWNLOAD_POS;
//-
//-				crc = 0;
//-				for ( i = 0 ; i < length ; i ++, ptr_code ++)
//-				{
//-					crc ^= *ptr_code;
//-				}
//-
//-				SendChar(crc);
//-				state = WAIT_ACK;
//-				break;
//-
//-			}
//-			case WAIT_ACK:
//-			{
//-				if ( ReceiveChar() && RecChar == PROG_ACK ) 
//-				{
//-					temp = SpiFlashID();
//-					for (sect=0;sect<((length/PHYS_FLASH_SECTOR_SIZE)+1);sect++)
//-					{
//-						SpiFlashEraseSector(sect * PHYS_FLASH_SECTOR_SIZE );
//-					}
//-					SpiFlashProgram(0,(uint8*)DOWNLOAD_POS,length);
//-					
//-					if (!(SpiFlashWaitTillReady()))
//-					{
//-						SpiFlashRead(0,(uint8*) COMPARE_BUFF,length);
//-						temp_ptr = (uint8*)COMPARE_BUFF;
//-						ptr_code = (uint8*)DOWNLOAD_POS;
//-						
//-						for (i=0;i<length;i++)
//-						{
//-							if((*ptr_code++)!=(*temp_ptr++) )
//-							{
//-								state = IDLE;
//-								break;
//-							}
//-						}
//-						SendChar(PROG_COM);
//-					}
//-				}
//-				state = IDLE;
//-				break;
//-			}
//-			default:
//-			{
//-				state = IDLE;
//-				break;
//-			}
//-		}
//-	}
//+	SetWord(SET_FREEZE_REG,FRZ_WDOG);
//+
//+#if defined SC14450
//+	SetBits(BAT_CTRL_REG,REG_ON,1);
//+	SetBits(BAT_CTRL_REG,DC_HYST,0);
//+	SetBits(BAT_CTRL2_REG,CHARGE_LEVEL,0x1e);
//+	SetBits(BAT_CTRL2_REG,NTC_DISABLE,1);
//+	SetBits(BAT_CTRL2_REG,CHARGE_CUR,7);
//+
//+	init_pll();
//+
//+	SetPort(P2_04_MODE_REG, PORT_OUTPUT,  PID_ACS1);        /* P2_04 is ACS1 */
//+	SetPort(P2_05_MODE_REG, PORT_OUTPUT,  PID_ACS2);        /* P2_05 is ACS2 */
//+	SetPort(P2_06_MODE_REG, PORT_OUTPUT,  PID_ACS3);        /* P2_06 is ACS3 */
//+	
//+#elif defined SC14452
//+	// Disable additional capacitors.
//+	SetBits (CLK_XTAL_CTRL_REG, XTAL_EXTRA_CV, 0);
//+	// Start PLL 1 and switch
//+	pll_1_on();
//+	
//+	// Start PLL 2 
//+	pll_2_on();
//+	
//+	// Enable UART clock
//+	if (GetWord(CLK_GLOBAL_REG) & SW_XTAL_PLL1_RATE)
//+		SetWord(CLK_GPIO6_REG, SW_UART_EN | 0x10);
//+	else
//+		SetWord(CLK_GPIO6_REG, SW_UART_EN | 0x8);
//+	
//+#endif	
//+	
//+	init_sdram();	
//+	init_SPI2();
//+
//+		
//+	SetWord(UART_CTRL_REG,0);
//+        /*   Config PPA for UART. */
//+	SetPort(P0_00_MODE_REG, PORT_OUTPUT,    PID_UTX);
//+	SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_URX);
//+	
//+	SetBits( UART_CTRL_REG, BAUDRATE, CLK_1152);
//+
//+	SetBits(UART_CTRL_REG, UART_REN, 1); // enable receive
//+	SetBits(UART_CTRL_REG, UART_TEN, 1); // enable transmit
//+
//+	max_download_size = FLASH_MEMORY_SIZE;
//+	
//+	state = IDLE;
//+
//+#if defined SC14452
//+	SetPort(P2_08_MODE_REG, PORT_OUTPUT, PID_port); // Blue led
//+	SetPort(P2_11_MODE_REG, PORT_OUTPUT, PID_port); // Green led
//+	SetPort(P2_12_MODE_REG, PORT_OUTPUT, PID_port); // Yellow led
//+#endif
//+	
//+	LED(YELLOW,1);
//+    
//+	while(1)
//+	{
//+		LED_SHOW_STATE(state);
//+		switch(state)
//+		{
//+			case ERROR:
//+			    LED_ERROR; // unfinished loop state
//+			    break;
//+			    
//+			case IDLE:
//+			{
//+				while(1)
//+				{
//+
//+					SendChar(PROG_STX);
//+					if (ReceiveChar())
//+					{
//+						if (RecChar==PROG_SOH)
//+						{
//+							state = WAITING_LEN;
//+							break;
//+						}
//+					}
//+				}
//+				break;
//+			}
//+			
//+			case WAITING_LEN:
//+			{
//+				if (ReceiveChar())
//+				{
//+					length = RecChar;
//+					if (ReceiveChar())
//+					{
//+						length += RecChar<<8;
//+						if (ReceiveChar())
//+						{
//+							length += RecChar<<16;
//+							if (ReceiveChar())
//+							{
//+								length += RecChar<<24;
//+								if (length > max_download_size || length == 0)
//+								{
//+									SendChar(PROG_NAK);
//+								}
//+								else
//+								{
//+									SendChar(PROG_ACK);
//+									state = REC_CODE;
//+									break;
//+								}
//+							}
//+						}
//+					}
//+				}
//+				state = ERROR;
//+				break;
//+			}
//+			
//+			case REC_CODE:
//+			{
//+				ptr_code = (uint8*)DOWNLOAD_POS;
//+				for (i=0;i<length;i++,ptr_code++)
//+				{
//+					if (!ReceiveChar())
//+					{
//+						state = ERROR;
//+						break;
//+					}
//+					else
//+					{
//+						*ptr_code = RecChar;
//+					}
//+					LED (GREEN,(RecChar&2));
//+					if ((i & 0xFFFF) % 0x8000 == 0)
//+					{
//+						LED (BLUE, 0);
//+					}
//+					if ((i & 0xFFFF) % 0x8300 == 0)
//+					{
//+						LED (BLUE, 1);
//+					}
//+				}
//+				if (state == REC_CODE)
//+				{
//+					state = CHK_CRC;
//+					LED(GREEN,0);
//+				}
//+				break;
//+			}
//+
//+			case CHK_CRC:
//+			{
//+				ptr_code = (uint8*)DOWNLOAD_POS;
//+
//+				crc = 0;
//+				for ( i = 0 ; i < length ; i ++, ptr_code ++)
//+				{
//+					crc ^= *ptr_code;
//+				}
//+				SendChar(crc);
//+				state = WAIT_ACK;
//+				break;
//+
//+			}
//+			case WAIT_ACK:
//+			{
//+				if ( ReceiveChar() && RecChar == PROG_ACK ) 
//+				{
//+# if 0
//+					// cheat to simulate full 8MB programming without waiting for serial data :)
//+					length = FLASH_MEMORY_SIZE;
//+					ptr_code = (uint8*)DOWNLOAD_POS;
//+					for ( i = 0 ; i < length ; i ++, ptr_code ++)
//+					{
//+						*ptr_code = 0xA5;
//+					}
//+#endif
//+					temp = SpiFlashID();
//+					for (sect=0; sect<((length/PHYS_FLASH_SECTOR_SIZE)+1); sect++)
//+					{
//+						LED(BLUE,(sect&1));
//+						SpiFlashEraseSector(sect * PHYS_FLASH_SECTOR_SIZE );
//+					}
//+					LED(BLUE,1);
//+					SpiFlashProgram(0, (uint8*)DOWNLOAD_POS, length);
//+
//+					if (!(SpiFlashWaitTillReady()))
//+					{
//+						LED_OFF;
//+						LED(BLUE,1);
//+						LED(GREEN,1);
//+						uint32 length_left = length;
//+						uint32 current_read_length;
//+						uint8 * temp_ptr_code;
//+						ptr_code = (uint8*)DOWNLOAD_POS;
//+						while (length_left>0)
//+						{
//+							current_read_length = MIN (length_left, MAX_COMPARE_BLOCK);
//+							SpiFlashRead(0,(uint8*) COMPARE_BUFF, current_read_length);
//+
//+							temp_ptr = (uint8*)COMPARE_BUFF;
//+
//+							temp_ptr_code = ptr_code;
//+							for (i=0; i<current_read_length; i++)
//+							{
//+								if((*temp_ptr_code++) != (*temp_ptr_code++) )
//+								{
//+									state = ERROR;
//+									break;
//+								}
//+							}
//+							length_left -= current_read_length;
//+							ptr_code 	+= current_read_length;
//+						}
//+						SendChar(PROG_COM);
//+						LED_OK
//+					}
//+				}
//+				state = ERROR;
//+				break;
//+			}
//+			
//+			default:
//+			{
//+				state = IDLE;
//+				break;
//+			}
//+		}
//+	}
// 	
// 	return 0;
// }
