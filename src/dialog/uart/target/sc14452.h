/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <vassilis.maniotis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 * See http://www.gnu.org/licenses/gpl-2.0.txt for more details.
 */

#ifndef _IO14452_INCLUDED
#define _IO14452_INCLUDED

#ifdef __IAR_SYSTEMS_ICC__
	#define MODEL               1
	#define __far
#else
	#define MODEL               4
#endif

/*========================== USER DEFINED CONSTANTS =========================*/

#define MAX_DOG_TIME        0x00FF

/*==============================================================
 * SC14452 Programmable Peripheral Assigment PID values
 ===============================================================*/

#define SetPort(PORT, IO_CONFIG, PID) SetWord(PORT, IO_CONFIG <<8 | PID); 

/*----------------------------*/
/*  IO configurations         */
/*----------------------------*/
#define PORT_INPUT          0
#define PORT_PULL_UP        1
#define PORT_PULL_DOWN      2
#define PORT_OUTPUT         3

/*----------------------------*/
/*  PIDs                      */
/*----------------------------*/
#define PID_port            0	
#define PID_CLK100          1	
#define PID_PID_TDOD        2
#define PID_BXTAL           3	
#define PID_PWM0            4	
#define PID_PWM1            5	
#define PID_ECZ1            6	
#define PID_ECZ2            7	
#define PID_PLL2_CLK        8	
#define PID_WTF_IN1         9	
#define PID_WTF_IN2         10	
#define PID_UTX             11	
#define PID_URX             12	
#define PID_SDA1            13	
#define PID_SCL1            14	
#define PID_SDA2            15	
#define PID_SCL2            16	
#define PID_SPI1_DOUT       17	
#define PID_SPI1_DIN        18	
#define PID_SPI1_CLK        19	
#define PID_SPI1_EN         20	
#define PID_SPI2_DOUT       21	
#define PID_SPI2_DIN        22	
#define PID_SPI2_CLK        23	
#define PID_PCM_DO          24	
#define PID_PCM_CLK         25	
#define PID_PCM_FSC         26	
#define PID_PCM_DI          27	
#define PID_AD13            28	
#define PID_AD14            28	
#define PID_AD15            28	
#define PID_AD16            28	
#define PID_AD17            28	
#define PID_AD18            28	
#define PID_AD19            28	
#define PID_AD20            28	
#define PID_AD21            28	
#define PID_AD22            28	
#define PID_AD23            28	
#define PID_SDCKE           29
#define PID_SDCLK           30	
#define PID_SF_ADV          31	
#define PID_BE0n            32	
#define PID_BE1n            33	
#define PID_READY           34	
#define PID_INT0n           35	
#define PID_INT1n           36	
#define PID_INT2n           37	
#define PID_INT3n           38	
#define PID_INT4n           39	
#define PID_INT5n           40	
#define PID_INT6n           41	
#define PID_INT7n           42	
#define PID_INT8n           43	
#define PID_ACS0            44	
#define PID_ACS1            45	
#define PID_ACS2            46	
#define PID_ACS3            47	
#define PID_ACS4            48	
#define PID_PD1             49	
#define PID_PD2             50	
#define PID_PD3             51	
#define PID_PD4             52	
#define PID_PD5             53	
#define PID_PAOUTp          63	
#define PID_PAOUTn          63	
#define PID_ADC0            63	
#define PID_ADC1            63	
#define PID_ADC2            63	

/*======================= Start of automatic generated code =================*/

/*
 * This file is generated from Meta file "Output/SC14452.meta"
 * With version number: 
 * On Oct 10 2008 15:23:01
 */




#define NMI_INT             (MODEL*01)  /* Non maskable interrupt from DIP <VNMI> and Watchdog timer */
#define SVC_TRAP            (MODEL*05)  /* Supervisor Call Trap */
#define DVZ_TRAP            (MODEL*06)  /* Divide by zero Trap */
#define FLG_TRAP            (MODEL*07)  /* Flag Trap */
#define BPT_TRAP            (MODEL*08)  /* Breakpoint Trap */
#define TRC_TRAP            (MODEL*09)  /* Trace Trap */
#define UND_TRAP            (MODEL*10)  /* Undefined Instruction Trap */
#define IAD_TRAP            (MODEL*12)  /* Illegal address Trap */
#define DBG_TRAP            (MODEL*14)  /* Debug Trap */
#define ISE_INT             (MODEL*15)  /* In System Emulator Interrupt caused by ISE pin or SDI */
#define ACCESS12_INT        (MODEL*16)  /* Access bus 1 and 2 interrupt */
#define KEYB_INT            (MODEL*17)  /* Keyboard Interrupt or vectored level/edge interrupt */
#define EMAC_INT            (MODEL*18)  /* EMAC interrupt */
#define CT_CLASSD_INT       (MODEL*19)  /* Capture timer 1,2 interrupt or CLASSD clipping interrupt */
#define UART_RI_INT         (MODEL*20)  /* UART RI interrupt */
#define UART_TI_INT         (MODEL*21)  /* UART TI interrupt */
#define SPI1_ADC_INT        (MODEL*22)  /* SPI 1 or ADC Interrupt */
#define TIM0_INT            (MODEL*23)  /* Timer 0 interrupt */
#define TIM1_INT            (MODEL*24)  /* Timer 1 interrupt */
#define CLK100_INT          (MODEL*25)  /* CLK100 interrupt  */
#define DIP_INT             (MODEL*26)  /* DIP Interrupt */
#define CRYPTO_INT          (MODEL*27)  /* CRYPTO Interrupt */
#define SPI2_INT            (MODEL*28)  /* SPI 2 interrupt */
#define DSP1_INT            (MODEL*29)  /* Gen2DSP 1 Interrupt */
#define DSP2_INT            (MODEL*30)  /* Gen2DSP 2 Interrupt */




#define RAM_START                               (0x000000)  /*  */
#define RAM_END                                 (0x005FFF)  /*  */
#define RAM_ADM_START                           (0x008000)  /*  */
#define RAM_ADM_END                             (0x009FFF)  /*  */
#define SHARED_RAM1_START                       (0x010000)  /*  */
#define SHARED_RAM1_END                         (0x013FFF)  /*  */
#define SHARED_RAM2_START                       (0x018000)  /*  */
#define SHARED_RAM2_END                         (0x01CFFF)  /*  */
#define PROGRAM_START                           (0x0F0000)  /*  */
#define PROGRAM_END                             (0xFEEFFF)  /*  */
#define BOOT_ROM_START                          (0xFEF000)  /*  */
#define BOOT_ROM_END                            (0xFEF7FF)  /*  */
#define EBI_SDCONR_REG                          (0xFF0000)  /* SDRAM Configuration register */
#define EBI_SDTMG0R_REG                         (0xFF0004)  /* SDRAM Timing register 0 */
#define EBI_SDTMG1R_REG                         (0xFF0008)  /* SDRAM Timing register 1 */
#define EBI_SDCTLR_REG                          (0xFF000C)  /* SDRAM Control register */
#define EBI_SDREFR_REG                          (0xFF0010)  /* SDRAM Refresh register */
#define EBI_ACS0_LOW_REG                        (0xFF0014)  /* Chip select 0 base address register */
#define EBI_ACS1_LOW_REG                        (0xFF0018)  /* Chip select 1 base address register */
#define EBI_ACS2_LOW_REG                        (0xFF001C)  /* Chip select 2 base address register */
#define EBI_ACS3_LOW_REG                        (0xFF0020)  /* Chip select 3 base address register */
#define EBI_ACS4_LOW_REG                        (0xFF0024)  /* Chip select 4 base address register */
#define EBI_ACS0_CTRL_REG                       (0xFF0054)  /* Chip select 0 control register */
#define EBI_ACS1_CTRL_REG                       (0xFF0058)  /* Chip select 1 control register */
#define EBI_ACS2_CTRL_REG                       (0xFF005C)  /* Chip select 2 control register */
#define EBI_ACS3_CTRL_REG                       (0xFF0060)  /* Chip select 3 control register */
#define EBI_ACS4_CTRL_REG                       (0xFF0064)  /* Chip select 4 control register */
#define EBI_SMTMGR_SET0_REG                     (0xFF0094)  /* Static memory Timing register Set 0 */
#define EBI_SMTMGR_SET1_REG                     (0xFF0098)  /* Static memory Timing register Set 1 */
#define EBI_SMTMGR_SET2_REG                     (0xFF009C)  /* Static memory Timing register Set 2 */
#define EBI_FLASH_TRPDR_REG                     (0xFF00A0)  /* FLASH Timing Register */
#define EBI_SMCTLR_REG                          (0xFF00A4)  /* Static memory Control register */
#define EBI_SDEXN_MODE_REG                      (0xFF00AC)  /* Mobile SDRAM Extende mode register */
#define CRYPTO_CTRL_REG                         (0xFF0800)  /* Crypto control register */
#define CRYPTO_START_REG                        (0xFF0804)  /* Start calculation */
#define CRYPTO_FETCH_ADDR_REG                   (0xFF0808)  /* DMA Fetch address */
#define CRYPTO_LEN_REG                          (0xFF080C)  /* The length of input block in bytes */
#define CRYPTO_DEST_ADDR_REG                    (0xFF0810)  /* DMA Destination address */
#define CRYPTO_STATUS_REG                       (0xFF0814)  /* Status register */
#define CRYPTO_CLRIRQ_REG                       (0xFF0818)  /* Clear interrupt request */
#define CRYPTO_MREG0_REG                        (0xFF081C)  /* Mode depended register 0 */
#define CRYPTO_MREG1_REG                        (0xFF0820)  /* Mode depended register 1 */
#define CRYPTO_MREG2_REG                        (0xFF0824)  /* Mode depended register 2 */
#define CRYPTO_MREG3_REG                        (0xFF0828)  /* Mode depended register 3 */
#define CRYPTO_MREG4_REG                        (0xFF082C)  /* Mode depended register 4 */
#define CRYPTO_MREG5_REG                        (0xFF0830)  /* Mode depended register 5 */
#define CRYPTO_MREG6_REG                        (0xFF0834)  /* Mode depended register 6 */
#define CRYPTO_MREG7_REG                        (0xFF0838)  /* Mode depended register 7 */
#define CRYPTO_KEYS_START                       (0xFF0900)  /*  */
#define CRYPTO_KEYS_END                         (0xFF09FF)  /*  */
#define QSPIC_CTRLBUS_REG                       (0xFF0E00)  /* SPI Bus Control register */
#define QSPIC_CTRLMODE_REG                      (0xFF0E04)  /* QSPIC Mode Control register */
#define QSPIC_RECVDATA_REG                      (0xFF0E08)  /* Received Data */
#define QSPIC_BURSTCMDA_REG                     (0xFF0E0C)  /* The way of reading in Auto Mode */
#define QSPIC_BURSTCMDB_REG                     (0xFF0E10)  /* The way of reading in Auto Mode */
#define QSPIC_STATUS_REG                        (0xFF0E14)  /* QSPIC Status register */
#define QSPIC_WRITEDATA_REG                     (0xFF0E18)  /* Write Data to SPI Bus */
#define QSPIC_READDATA_REG                      (0xFF0E1C)  /* Read Data from SPI Bus */
#define QSPIC_DUMMYDATA_REG                     (0xFF0E20)  /* Send Dummy clocks to SPI Bus */
#define QSPIC_AUTO_START                        (0xFF1000)  /*  */
#define QSPIC_AUTO_END                          (0xFF1FFF)  /*  */
#define EMAC_MACR0_CONFIG_REG                   (0xFF2000)  /* MAC Configuration Register */
#define EMAC_MACR1_FRAME_FILTER_REG             (0xFF2004)  /* MAC Frame Filter */
#define EMAC_MACR4_MII_ADDR_REG                 (0xFF2010)  /* MII Address Register */
#define EMAC_MACR5_MII_DATA_REG                 (0xFF2014)  /* MII Data Register */
#define EMAC_MACR6_FLOW_CTRL_REG                (0xFF2018)  /* Flow Control Register */
#define EMAC_MACR7_VLAN_TAG_REG                 (0xFF201C)  /* VLAN Tag Register */
#define EMAC_MACR8_CORE_VER_REG                 (0xFF2020)  /* Version Register */
#define EMAC_MACR14_INT_REG                     (0xFF2038)  /* Interrupt Register */
#define EMAC_MACR15_INT_MSK_REG                 (0xFF203C)  /* Interrupt Mask Register */
#define EMAC_MACR16_MAC_ADDR0_HIGH_REG          (0xFF2040)  /* MAC Address0 High Register */
#define EMAC_MACR17_MAC_ADDR0_LOW_REG           (0xFF2044)  /* MAC Address0 Low Register */
#define EMAC_MACR54_MII_STATUS_REG              (0xFF20D8)  /* MII/RMII Status Register */
#define EMAC_MMC_CNTRL_REG                      (0xFF2100)  /* MMC Control */
#define EMAC_MMC_INTR_RX_REG                    (0xFF2104)  /* MMC Receive Interrupt */
#define EMAC_MMC_INTR_TX_REG                    (0xFF2108)  /* MMC Transmit Interrupt */
#define EMAC_MMC_INTR_MASK_RX_REG               (0xFF210C)  /* MMC Receive Interrupt Mask */
#define EMAC_MMC_INTR_MASK_TX_REG               (0xFF2110)  /* MMC Transmit Interrupt Mask */
#define EMAC_MMC_TXUNICASTFRAMES_GB_REG         (0xFF213C)  /* Number of good and bad unicast frames transmitted. */
#define EMAC_MMC_TXUNDERFLOWERROR_REG           (0xFF2148)  /* Number of frames aborted due to frame underflow error */
#define EMAC_MMC_TXSINGLECOL_G_REG              (0xFF214C)  /* Number of successfully transmitted frames after a single collision in Half-duplex mode */
#define EMAC_MMC_TXMULTICOL_G_REG               (0xFF2150)  /* Number of successfully transmitted frames after more than a single collision in Half-duplex mode */
#define EMAC_MMC_TXLATECOL_REG                  (0xFF2158)  /* Number of frames aborted due to late collision error */
#define EMAC_MMC_TXCARRIERERROR_REG             (0xFF2160)  /* Number of frames aborted due to carrier sense error (no carrier or loss of carrier). */
#define EMAC_MMC_TXOCTETCOUNT_G_REG             (0xFF2164)  /* Number of bytes transmitted, exclusive of preamble, in good frames only. */
#define EMAC_MMC_TXFRAMECOUNT_G_REG             (0xFF2168)  /* Number of good frames transmitted */
#define EMAC_MMC_TXVLANFRAMES_G_REG             (0xFF2174)  /* Number of good VLAN frames transmitted, exclusive of retried frames. */
#define EMAC_MMC_RXFRAMECOUNT_GB_REG            (0xFF2180)  /* Number of good and bad frames received */
#define EMAC_MMC_RXOCTETCOUNT_G_REG             (0xFF2188)  /* Number of bytes received, exclusive of preamble, only in good frames. */
#define EMAC_MMC_RXBROADCASTFRAMES_G_REG        (0xFF218C)  /* Number of good broadcast frames received */
#define EMAC_MMC_RXMULTICASTFRAMES_G_REG        (0xFF2190)  /* Number of good multicast frames received */
#define EMAC_MMC_RXJABBERERROR_REG              (0xFF21A0)  /* Number of giant frames received with length (including CRC) greater than 1,518 bytes (1,522 bytes for VLAN tagged) and with CRC error. */
#define EMAC_MMC_RXUNDERSIZE_G_REG              (0xFF21A4)  /* Number of frames received with length less than 64 bytes, without any errors. */
#define EMAC_MMC_RXOVERSIZE_G_REG               (0xFF21A8)  /* Number of frames received with length greater than the maxsize (1,518 or 1,522 for VLAN tagged frames), without errors */
#define EMAC_MMC_RXUNICASTFRAMES_G_REG          (0xFF21C4)  /* Number of good unicast frames received */
#define EMAC_MMC_RXFIFOOVERFLOW_REG             (0xFF21D4)  /* Number of missed received frames due to FIFO overflow.  */
#define EMAC_MMC_RXVLANFRAMES_GB_REG            (0xFF21D8)  /* Number of good and bad VLAN frames received */
#define EMAC_DMAR0_BUS_MODE_REG                 (0xFF3000)  /* Bus Mode Register */
#define EMAC_DMAR1_TX_POLL_DEMAND_REG           (0xFF3004)  /* Transmit Poll Demand Register */
#define EMAC_DMAR2_RX_POLL_DEMAND_REG           (0xFF3008)  /* Receive Poll Demand Register */
#define EMAC_DMAR3_RX_DESCRIPTOR_LIST_ADDR_REG  (0xFF300C)  /* Receive Descriptor List Address Register */
#define EMAC_DMAR4_TX_DESCRIPTOR_LIST_ADDR_REG  (0xFF3010)  /* Transmit Descriptor List Address Register */
#define EMAC_DMAR5_STATUS_REG                   (0xFF3014)  /* Status Register */
#define EMAC_DMAR6_OPERATION_MODE_REG           (0xFF3018)  /* Operation Mode Register */
#define EMAC_DMAR7_INT_ENABLE_REG               (0xFF301C)  /* Interrupt Enable Register */
#define EMAC_DMAR8_MISSFRAME_BUFOVR_CNT_REG     (0xFF3020)  /* Missed Frame and Buffer Overflow Counter Register */
#define EMAC_DMAR18_CUR_HOST_TX_DESCRIPTOR_REG  (0xFF3048)  /* Current Host Transmit Descriptor Register */
#define EMAC_DMAR19_CUR_HOST_RX_DESCRIPTOR_REG  (0xFF304C)  /* Current Host Receive Descriptor Register */
#define EMAC_DMAR20_CUR_HOST_TX_BUF_ADDR_REG    (0xFF3050)  /* Current Host Transmit Buffer Address Register */
#define EMAC_DMAR21_CUR_HOST_RX_BUF_ADDR_REG    (0xFF3054)  /* Current Host Receive Buffer Address Register */
#define EMAC_RXFIFO_LOW_START                   (0xFF3400)  /*  */
#define EMAC_RXFIFO_HIGH_START                  (0xFF3500)  /*  */
#define EMAC_TXFIFO_LOW_START                   (0xFF3600)  /*  */
#define EMAC_TXFIFO_HIGH_START                  (0xFF3700)  /*  */
#define CLK_GLOBAL_REG                          (0xFF4000)  /* Selection between PLL and XTAL mode */
#define CLK_AMBA_REG                            (0xFF4002)  /* HCLK, PCLK, divider and clock gates */
#define CLK_CODEC1_REG                          (0xFF4004)  /* Codec clock register 1 */
#define CLK_CODEC2_REG                          (0xFF4006)  /* Codec clock register 2 */
#define CLK_CODEC3_REG                          (0xFF4008)  /* Codec clock register 3 */
#define CLK_SPU1_REG                            (0xFF400A)  /* SPU clock register 1 */
#define CLK_SPU2_REG                            (0xFF400C)  /* SPU clock register 2 */
#define CLK_CLASSD1_REG                         (0xFF400E)  /* CLASSD clock register 1 */
#define CLK_CLASSD2_REG                         (0xFF4010)  /* CLASSD clock register 2 */
#define CLK_TIM1_REG                            (0xFF4012)  /* TIM clock register 1 */
#define CLK_TIM2_REG                            (0xFF4014)  /* TIM clock register 2 */
#define CLK_BMC1_REG                            (0xFF4016)  /* BMC clock register 1 */
#define CLK_BMC2_REG                            (0xFF4018)  /* BMC clock register 2 */
#define CLK_BMC3_REG                            (0xFF401A)  /* BMC clock register 3 */
#define CLK_GPIO1_REG                           (0xFF401C)  /* GPIO peripherals clock register 1 */
#define CLK_GPIO2_REG                           (0xFF401E)  /* GPIO peripherals clock register 2 */
#define CLK_GPIO3_REG                           (0xFF4020)  /* GPIO peripherals clock register 3 */
#define CLK_GPIO4_REG                           (0xFF4022)  /* GPIO peripherals clock register 4 */
#define CLK_GPIO5_REG                           (0xFF4024)  /* GPIO peripherals clock register 5 */
#define CLK_GPIO6_REG                           (0xFF4026)  /* GPIO peripherals clock register 6 */
#define CLK_GPIO7_REG                           (0xFF4028)  /* GPIO peripherals clock register 7 */
#define CLK_GPIO8_REG                           (0xFF402A)  /* GPIO peripherals clock register 8 */
#define CLK_AUX1_REG                            (0xFF402C)  /* AUX clock register 1 */
#define CLK_AUX2_REG                            (0xFF402E)  /* AUX clock register 2 */
#define CLK_JOWI_REG                            (0xFF4030)  /* JOWI clock register */
#define CLK_CLK100A_REG                         (0xFF4032)  /* CLK100 clock division register A */
#define CLK_CLK100B_REG                         (0xFF4034)  /* CLK100 clock division register B */
#define CLK_DCDC1_REG                           (0xFF4036)  /* DCDC clock regsiter 1 */
#define CLK_DCDC2_REG                           (0xFF4038)  /* DCDC clock register 2 */
#define CLK_PLL1_CTRL_REG                       (0xFF403A)  /* PLL1 control register */
#define CLK_PLL1_DIV_REG                        (0xFF403C)  /* PLL1 divider regsiter */
#define CLK_PLL2_CTRL_REG                       (0xFF403E)  /* PLL2 control register */
#define CLK_PLL2_DIV_REG                        (0xFF4040)  /* PLL2 divider regsiter */
#define CLK_FREQ_TRIM_REG                       (0xFF4042)  /* XTAL frequency triming register */
#define CLK_XTAL_CTRL_REG                       (0xFF4044)  /* XTAL control register */
#define CLK_XDIV0_REG                           (0xFF4046)  /* XDIV enable register 0 */
#define CLK_XDIV1_REG                           (0xFF4048)  /* XDIV enable register 1 */
#define CLK_XDIV_VAL_REG                        (0xFF404A)  /* XDIV divider register */
#define CLK_CDC_CORRECT_REG                     (0xFF404C)  /* CDC clock register */
#define DMA0_A_STARTL_REG                       (0xFF4400)  /* Start address Low A of DMA channel 0 */
#define DMA0_A_STARTH_REG                       (0xFF4402)  /* Start address Low A of DMA channel 0 */
#define DMA0_B_STARTL_REG                       (0xFF4404)  /* Start address Low B of DMA channel 0 */
#define DMA0_B_STARTH_REG                       (0xFF4406)  /* Start address High B of DMA channel 0 */
#define DMA0_INT_REG                            (0xFF4408)  /* DMA receive interrupt register channel 0 */
#define DMA0_LEN_REG                            (0xFF440A)  /* DMA receive length register channel 0 */
#define DMA0_CTRL_REG                           (0xFF440C)  /* Control register for the DMA channel 0 */
#define DMA0_IDX_REG                            (0xFF440E)  /* Internal Index register for the DMA channel 0 */
#define DMA1_A_STARTL_REG                       (0xFF4410)  /* Start address Low A of DMA channel 1 */
#define DMA1_A_STARTH_REG                       (0xFF4412)  /* Start address High A of DMA channel 1 */
#define DMA1_B_STARTL_REG                       (0xFF4414)  /* Start address Low B of DMA channel 1 */
#define DMA1_B_STARTH_REG                       (0xFF4416)  /* Start address High B of DMA channel 1 */
#define DMA1_INT_REG                            (0xFF4418)  /* DMA receive interrupt register channel 1 */
#define DMA1_LEN_REG                            (0xFF441A)  /* DMA receive length register channel 1 */
#define DMA1_CTRL_REG                           (0xFF441C)  /* Control register for the DMA channel 1 */
#define DMA1_IDX_REG                            (0xFF441E)  /* Internal Index register for the DMA channel 1 */
#define DMA2_A_STARTL_REG                       (0xFF4420)  /* Start address Low A of DMA channel 2 */
#define DMA2_A_STARTH_REG                       (0xFF4422)  /* Start address High A of DMA channel 2 */
#define DMA2_B_STARTL_REG                       (0xFF4424)  /* Start address Low B of DMA channel 2 */
#define DMA2_B_STARTH_REG                       (0xFF4426)  /* Start address High B of DMA channel 2 */
#define DMA2_INT_REG                            (0xFF4428)  /* DMA receive interrupt register channel 2 */
#define DMA2_LEN_REG                            (0xFF442A)  /* DMA receive length register channel 2 */
#define DMA2_CTRL_REG                           (0xFF442C)  /* Control register for the DMA channel 2 */
#define DMA2_IDX_REG                            (0xFF442E)  /* Internal Index register for the DMA channel 2 */
#define DMA3_A_STARTL_REG                       (0xFF4430)  /* Start address Low A of DMA channel 3 */
#define DMA3_A_STARTH_REG                       (0xFF4432)  /* Start address High A of DMA channel 3 */
#define DMA3_B_STARTL_REG                       (0xFF4434)  /* Start address Low B of DMA channel 3 */
#define DMA3_B_STARTH_REG                       (0xFF4436)  /* Start address High B of DMA channel 3 */
#define DMA3_INT_REG                            (0xFF4438)  /* DMA receive interrupt register channel 3 */
#define DMA3_LEN_REG                            (0xFF443A)  /* DMA receive length register channel 3 */
#define DMA3_CTRL_REG                           (0xFF443C)  /* Control register for the DMA channel 3 */
#define DMA3_IDX_REG                            (0xFF443E)  /* Internal Index register for the DMA channel 3 */
#define TEST_ENV_REG                            (0xFF4800)  /* CR16C+ boot mode control register */
#define TEST_CTRL_REG                           (0xFF4802)  /* Test control register */
#define TEST_CTRL2_REG                          (0xFF4804)  /* Test control register 2 */
#define BANDGAP_REG                             (0xFF4810)  /* Bandgap register */
#define SUPPLY_CTRL_REG                         (0xFF4812)  /* Power Management control register */
#define P0_DATA_REG                             (0xFF4830)  /* P0 Data input /out register */
#define P0_SET_DATA_REG                         (0xFF4832)  /* P0 Set port pins register */
#define P0_RESET_DATA_REG                       (0xFF4834)  /* P0 Reset port pins register */
#define P0_00_MODE_REG                          (0xFF4840)  /* P0y Mode Register */
#define P0_01_MODE_REG                          (0xFF4842)  /* P0y Mode Register */
#define P0_02_MODE_REG                          (0xFF4844)  /* P0y Mode Register */
#define P0_03_MODE_REG                          (0xFF4846)  /* P0y Mode Register */
#define P0_04_MODE_REG                          (0xFF4848)  /* P0y Mode Register */
#define P0_05_MODE_REG                          (0xFF484A)  /* P0y Mode Register */
#define P0_06_MODE_REG                          (0xFF484C)  /* P0y Mode Register */
#define P0_07_MODE_REG                          (0xFF484E)  /* P0y Mode Register */
#define P0_08_MODE_REG                          (0xFF4850)  /* P0y Mode Register */
#define P0_09_MODE_REG                          (0xFF4852)  /* P0y Mode Register */
#define P0_10_MODE_REG                          (0xFF4854)  /* P0y Mode Register */
#define P0_11_MODE_REG                          (0xFF4856)  /* P0y Mode Register */
#define P0_12_MODE_REG                          (0xFF4858)  /* P0y Mode Register */
#define P0_13_MODE_REG                          (0xFF485A)  /* P0y Mode Register */
#define P0_14_MODE_REG                          (0xFF485C)  /* P0y Mode Register */
#define P0_15_MODE_REG                          (0xFF485E)  /* P0y Mode Register */
#define P1_DATA_REG                             (0xFF4860)  /* P1 Data input /out register */
#define P1_SET_DATA_REG                         (0xFF4862)  /* P1 Set port pins register */
#define P1_RESET_DATA_REG                       (0xFF4864)  /* P1 Reset port pins register */
#define P1_00_MODE_REG                          (0xFF4870)  /* P1y Mode Register */
#define P1_01_MODE_REG                          (0xFF4872)  /* P1y Mode Register */
#define P1_02_MODE_REG                          (0xFF4874)  /* P1y Mode Register */
#define P1_03_MODE_REG                          (0xFF4876)  /* P1y Mode Register */
#define P1_04_MODE_REG                          (0xFF4878)  /* P1y Mode Register */
#define P1_05_MODE_REG                          (0xFF487A)  /* P1y Mode Register */
#define P1_06_MODE_REG                          (0xFF487C)  /* P1y Mode Register */
#define P1_07_MODE_REG                          (0xFF487E)  /* P1y Mode Register */
#define P1_08_MODE_REG                          (0xFF4880)  /* P1y Mode Register */
#define P1_09_MODE_REG                          (0xFF4882)  /* P1y Mode Register */
#define P1_10_MODE_REG                          (0xFF4884)  /* P1y Mode Register */
#define P1_11_MODE_REG                          (0xFF4886)  /* P1y Mode Register */
#define P1_12_MODE_REG                          (0xFF4888)  /* P1y Mode Register */
#define P1_13_MODE_REG                          (0xFF488A)  /* P1y Mode Register */
#define P1_14_MODE_REG                          (0xFF488C)  /* P1y Mode Register */
#define P1_15_MODE_REG                          (0xFF488E)  /* P1y Mode Register */
#define P2_DATA_REG                             (0xFF4890)  /* P2 Data input /out register */
#define P2_SET_DATA_REG                         (0xFF4892)  /* P2 Set port pins register */
#define P2_RESET_DATA_REG                       (0xFF4894)  /* P2 Reset port pins register */
#define P2_00_MODE_REG                          (0xFF48A0)  /* P2y Mode Register */
#define P2_01_MODE_REG                          (0xFF48A2)  /* P2y Mode Register */
#define P2_02_MODE_REG                          (0xFF48A4)  /* P2y Mode Register */
#define P2_03_MODE_REG                          (0xFF48A6)  /* P2y Mode Register */
#define P2_04_MODE_REG                          (0xFF48A8)  /* P2y Mode Register */
#define P2_05_MODE_REG                          (0xFF48AA)  /* P2y Mode Register */
#define P2_06_MODE_REG                          (0xFF48AC)  /* P2y Mode Register */
#define P2_07_MODE_REG                          (0xFF48AE)  /* P2y Mode Register */
#define P2_08_MODE_REG                          (0xFF48B0)  /* P2y Mode Register */
#define P2_09_MODE_REG                          (0xFF48B2)  /* P2y Mode Register */
#define P2_10_MODE_REG                          (0xFF48B4)  /* P2y Mode Register */
#define P2_11_MODE_REG                          (0xFF48B6)  /* P2y Mode Register */
#define P2_12_MODE_REG                          (0xFF48B8)  /* P2y Mode Register */
#define P2_13_MODE_REG                          (0xFF48BA)  /* P2y Mode Register */
#define P2_14_MODE_REG                          (0xFF48BC)  /* P2y Mode Register */
#define P2_15_MODE_REG                          (0xFF48BE)  /* P2y Mode Register */
#define P3_DATA_REG                             (0xFF48C0)  /* P3 Data input /out register */
#define P3_SET_DATA_REG                         (0xFF48C2)  /* P3 Set port pins register */
#define P3_RESET_DATA_REG                       (0xFF48C4)  /* P3 Reset port pins register */
#define P3_00_MODE_REG                          (0xFF48D0)  /* P3y Mode Register */
#define P3_01_MODE_REG                          (0xFF48D2)  /* P3y Mode Register */
#define P3_02_MODE_REG                          (0xFF48D4)  /* P3y Mode Register */
#define P3_03_MODE_REG                          (0xFF48D6)  /* P3y Mode Register */
#define P3_04_MODE_REG                          (0xFF48D8)  /* P3y Mode Register */
#define P3_05_MODE_REG                          (0xFF48DA)  /* P3y Mode Register */
#define P3_06_MODE_REG                          (0xFF48DC)  /* P3y Mode Register */
#define P3_07_MODE_REG                          (0xFF48DE)  /* P3y Mode Register */
#define P3_08_MODE_REG                          (0xFF48E0)  /* P3y Mode Register */
#define UART_CTRL_REG                           (0xFF4900)  /* UART control register */
#define UART_RX_TX_REG                          (0xFF4902)  /* UART data transmit/receive register */
#define UART_CLEAR_TX_INT_REG                   (0xFF4904)  /* UART clear transmit interrupt */
#define UART_CLEAR_RX_INT_REG                   (0xFF4906)  /* UART clear receive interrupt */
#define UART_ERROR_REG                          (0xFF4908)  /* UART Parity error register */
#define ACCESS1_IN_OUT_REG                      (0xFF4920)  /* ACCESS bus 1 receive/transmit register */
#define ACCESS1_CTRL_REG                        (0xFF4922)  /* ACCESS bus 1 Control register */
#define ACCESS1_CLEAR_INT_REG                   (0xFF4924)  /* Clear ACCESS bus 1 interrupt */
#define ACCESS2_IN_OUT_REG                      (0xFF4930)  /* ACCESS bus 2 receive/transmit register */
#define ACCESS2_CTRL_REG                        (0xFF4932)  /* ACCESS bus 2 Control register */
#define ACCESS2_CLEAR_INT_REG                   (0xFF4934)  /* Clear ACCESS bus 2 interrupt */
#define SPI1_CTRL_REG0                          (0xFF4940)  /* SPI 1 control register 0 */
#define SPI1_RX_TX_REG0                         (0xFF4942)  /* SPI 1 RX/TX register 0 */
#define SPI1_RX_TX_REG1                         (0xFF4944)  /* SPI 1 RX/TX register 1 */
#define SPI1_CLEAR_INT_REG                      (0xFF4946)  /* SPI 1 clear interrupt register */
#define SPI1_CTRL_REG1                          (0xFF4948)  /* SPI 1 control register 1 */
#define SPI2_CTRL_REG0                          (0xFF4950)  /* SPI 2 control register 0 */
#define SPI2_RX_TX_REG0                         (0xFF4952)  /* SPI 2 RX/TX register 0 */
#define SPI2_RX_TX_REG1                         (0xFF4954)  /* SPI 2 RX/TX register 1 */
#define SPI2_CLEAR_INT_REG                      (0xFF4956)  /* SPI 2 clear interrupt register */
#define SPI2_CTRL_REG1                          (0xFF4958)  /* SPI 2 control register 1 */
#define ADC_CTRL_REG                            (0xFF4960)  /* ADC control register  */
#define ADC_CTRL1_REG                           (0xFF4962)  /* ADC control register 1  */
#define ADC_CLEAR_INT_REG                       (0xFF4964)  /* Clears ADC interrupt if set in ADC_CTRL_REG  */
#define ADC0_REG                                (0xFF4966)  /* ADC0 value */
#define ADC1_REG                                (0xFF4968)  /* ADC1 value */
#define TIMER_CTRL_REG                          (0xFF4970)  /* Timers control registers */
#define TIMER0_ON_REG                           (0xFF4972)  /* Timers 0 on control registers */
#define TIMER0_RELOAD_M_REG                     (0xFF4974)  /*  */
#define TIMER0_RELOAD_N_REG                     (0xFF4976)  /*  */
#define TIMER1_RELOAD_M_REG                     (0xFF4978)  /*  */
#define TIMER1_RELOAD_N_REG                     (0xFF497A)  /*  */
#define TONE_CTRL1_REG                          (0xFF4990)  /* Capture timer control register 1 */
#define TONE_COUNTER1_REG                       (0xFF4992)  /* Capture timer Counter 1 */
#define TONE_LATCH1_REG                         (0xFF4994)  /* Capture timer Latch 1 */
#define TONE_CLEAR_INT1_REG                     (0xFF4996)  /* Clears CT1 interrupt and TONE_LATCH1_REG */
#define TONE_CTRL2_REG                          (0xFF4998)  /* Capture timer control register 2  */
#define TONE_COUNTER2_REG                       (0xFF499A)  /* Capture timer Counter 2 */
#define TONE_LATCH2_REG                         (0xFF499C)  /* Capture timer Latch 2 */
#define TONE_CLEAR_INT2_REG                     (0xFF499E)  /* Clears CT2 interrupt and TONE_LATCH2_REG */
#define KEY_GP_INT_REG                          (0xFF49B0)  /* General purpose interrupts for KEYB_INT */
#define KEY_BOARD_INT_REG                       (0xFF49B2)  /* Keyboard interrupt enable register */
#define KEY_DEBOUNCE_REG                        (0xFF49B4)  /* Keyboard debounce and auto key generation timer */
#define KEY_STATUS_REG                          (0xFF49B6)  /* Keyboard interrupt status. */
#define WATCHDOG_REG                            (0xFF4C00)  /* Watchdog preset value. */
#define SET_FREEZE_REG                          (0xFF5000)  /* Freeze watchdog, timer1 and DIP during debugging */
#define RESET_FREEZE_REG                        (0xFF5002)  /* Release watchdog, timer1 and DIP during debugging after setting in freeze mode */
#define DEBUG_REG                               (0xFF5004)  /* DEBUG_REG for boot program control and debug control */
#define CACHE_CTRL_REG                          (0xFF5006)  /* Cache control register */
#define CACHE_LEN0_REG                          (0xFF5008)  /* Cache length register 0 */
#define CACHE_START0_REG                        (0xFF500A)  /* Cache start register 0, can be used for iCache and dCache */
#define CACHE_LEN1_REG                          (0xFF500C)  /* Cache length register 1 */
#define CACHE_START1_REG                        (0xFF500E)  /* Cache start register 1, can be used for iCache and dCache */
#define CACHE_STATUS_REG                        (0xFF5010)  /* Cache status register */
#define TRACE_CTRL_REG                          (0xFF5020)  /* Trace control register */
#define TRACE_STATUS_REG                        (0xFF5022)  /* Trace status register */
#define TRACE_START0_REG                        (0xFF5024)  /* Trace start register 0 */
#define TRACE_LEN0_REG                          (0xFF5026)  /* Trace length register 0 */
#define TRACE_START1_REG                        (0xFF5028)  /* Trace start register 1 */
#define TRACE_LEN1_REG                          (0xFF502A)  /* Trace length register 1 */
#define TRACE_TIMERL_REG                        (0xFF502C)  /* Trace timer bits 15-0 */
#define TRACE_TIMERH_REG                        (0xFF502E)  /* Trace timer bits 31-16 */
#define SET_INT_PENDING_REG                     (0xFF5400)  /* Set interrupt pending register */
#define RESET_INT_PENDING_REG                   (0xFF5402)  /* Reset interrupt pending register */
#define INT0_PRIORITY_REG                       (0xFF5404)  /* Interrupt priority register 0 */
#define INT1_PRIORITY_REG                       (0xFF5406)  /* Interrupt priority register 1 */
#define INT2_PRIORITY_REG                       (0xFF5408)  /* Interrupt priority register 2 */
#define INT3_PRIORITY_REG                       (0xFF540A)  /* Interrupt priority register 2 */
#define PC_START_REG                            (0xFF540C)  /* CR16C+ startup address */
#define CODEC_MIC_REG                           (0xFF5800)  /* Codec microphone control register  */
#define CODEC_LSR_REG                           (0xFF5802)  /* Codec loudspeaker control register  */
#define CODEC_VREF_REG                          (0xFF5804)  /* Codec vref control register  */
#define CODEC_TONE_REG                          (0xFF5806)  /* Codec CID control register  */
#define CODEC_ADDA_REG                          (0xFF5808)  /* Codec ad/da control register  */
#define CODEC_OFFSET1_REG                       (0xFF580A)  /* Codec offset error and compensation register  */
#define CODEC_TEST_CTRL_REG                     (0xFF580C)  /* Codec test control register codec  */
#define CODEC_OFFSET2_REG                       (0xFF580E)  /* Codec offset compensation register  */
#define CLASSD_CTRL_REG                         (0xFF5C00)  /* Class D control register */
#define CLASSD_CLEAR_INT_REG                    (0xFF5C02)  /* Class D Clear interrupt register */
#define CLASSD_BUZZER_REG                       (0xFF5C04)  /* Class D buzzer register */
#define CLASSD_TEST_REG                         (0xFF5C06)  /* Class D test register */
#define CLASSD_NR_REG                           (0xFF5C08)  /* Class D noise reduction register */
#define DIP_STACK_REG                           (0xFF6000)  /* DIP Stack pointer. (read only). The DIP stack is 4 deep  */
#define DIP_PC_REG                              (0xFF6002)  /* DIP program counter */
#define DIP_STATUS_REG                          (0xFF6004)  /* DIP Status register, */
#define DIP_CTRL_REG                            (0xFF6006)  /* DIP Control register1  */
#define DIP_STATUS1_REG                         (0xFF6008)  /* DIP Status register1, */
#define DIP_CTRL1_REG                           (0xFF600A)  /* DIP Control register, clears DIP_INT if read */
#define DIP_SLOT_NUMBER_REG                     (0xFF600C)  /* DIP slot number register, returns the current slot number */
#define DIP_CTRL2_REG                           (0xFF600E)  /* DIP Control register 2 (debug status information) */
#define DIP_USB_PHASE_REG                       (0xFF6010)  /* Phase between SLOTZERO and USB SOF */
#define DIP_MOD_SEL_REG                         (0xFF6012)  /* DIP Modulo counters selection */
#define DIP_MOD_VAL_REG                         (0xFF6014)  /* DIP Modulo values selection */
#define DIP_DC01_REG                            (0xFF6016)  /* DIP MultiFrame control */
#define DIP_DC23_REG                            (0xFF6018)  /* DIP MultiFrame control */
#define BMC_CTRL_REG                            (0xFF6400)  /* BMC control register */
#define BMC_CTRL2_REG                           (0xFF6402)  /* BMC control register 2 */
#define BMC_TX_SFIELDL_REG                      (0xFF6404)  /* BMC Tx S field register Low */
#define BMC_TX_SFIELDH_REG                      (0xFF6406)  /* BMC Tx S field register High */
#define BMC_RX_SFIELDL_REG                      (0xFF6408)  /* BMC Rx S field register Low */
#define BMC_RX_SFIELDH_REG                      (0xFF640A)  /* BMC Rx S field register High */
#define CCU_CRC_LOW_REG                         (0xFF6C00)  /* CCU MSB result */
#define CCU_CRC_HIGH_REG                        (0xFF6C02)  /* CCU LSB result */
#define CCU_IN_REG                              (0xFF6C04)  /* CCU Input */
#define CCU_MODE_REG                            (0xFF6C06)  /* CCU mode */
#define GPRG_R0_REG                             (0xFF7000)  /* General Programming Register 0 */
#define GPRG_R1_REG                             (0xFF7002)  /* General Programming Register 1 */
#define CHIP_ID1_REG                            (0xFFFBF8)  /* Chip identification register 1 */
#define CHIP_ID2_REG                            (0xFFFBF9)  /* Chip identification register 2 */
#define CHIP_ID3_REG                            (0xFFFBFA)  /* Chip identification register 3 */
#define CHIP_MEM_SIZE_REG                       (0xFFFBFB)  /* Chip memory size register */
#define CHIP_REVISION_REG                       (0xFFFBFC)  /* Chip revision registers (Corresponds to Chip Marking) */
#define CHIP_TEST1_REG                          (0xFFFBFD)  /* Chip test register */
#define CHIP_TEST2_REG                          (0xFFFBFE)  /* Chip test register */
#define INT_ACK_CR16_START                      (0xFFFC00)  /*  */
#define INT_ACK_CR16_END                        (0xFFFFFF)  /*  */
#define DIP_RAM_START                           (0x1000000)  /*  */
#define DIP_RAM_END                             (0x10001FF)  /*  */
#define DIP_RAM_2_START                         (0x1000200)  /*  */
#define DIP_RAM_2_END                           (0x10003FF)  /*  */
#define SHARED_ROM1_START                       (0x1010000)  /*  */
#define SHARED_ROM1_END                         (0x10107FF)  /*  */
#define DSP_MAIN_SYNC0_REG                      (0x101FF80)  /* DSP main counter outputs selection register 0 */
#define DSP_MAIN_SYNC1_REG                      (0x101FF82)  /* DSP main counter outputs selection register 1 */
#define DSP_MAIN_CNT_REG                        (0x101FF84)  /* DSP main counter and reload register */
#define DSP_ADC1S_REG                           (0x101FF86)  /* ADC1 Input 2's Complement register */
#define DSP_ADC0S_REG                           (0x101FF88)  /* ADC0 Input 2's Complement register */
#define DSP_CLASSD_REG                          (0x101FF8A)  /* CLASSD Output output data register */
#define DSP_CODEC_MIC_GAIN_REG                  (0x101FF8C)  /* CODEC MIC GAIN register */
#define DSP_CODEC_OUT_REG                       (0x101FF8E)  /* CODEC DATA register */
#define DSP_CODEC_IN_REG                        (0x101FF90)  /* CODEC DATA register */
#define DSP_RAM_OUT0_REG                        (0x101FF92)  /* Shared RAM 1 or 2 output register 0 */
#define DSP_RAM_OUT1_REG                        (0x101FF94)  /* Shared RAM 1 or 2 output register 1 */
#define DSP_RAM_OUT2_REG                        (0x101FF96)  /* Shared RAM 1 or 2 output register 2 */
#define DSP_RAM_OUT3_REG                        (0x101FF98)  /* Shared RAM 1 or 2 output register 3 */
#define DSP_RAM_IN0_REG                         (0x101FF9A)  /* Shared RAM 1 or 2 input register 0 */
#define DSP_RAM_IN1_REG                         (0x101FF9C)  /* Shared RAM 1 or 2 input register 1 */
#define DSP_RAM_IN2_REG                         (0x101FF9E)  /* Shared RAM 1 or 2 input register 2 */
#define DSP_RAM_IN3_REG                         (0x101FFA0)  /* Shared RAM 1 or 2 input register 3 */
#define DSP_ZCROSS1_OUT_REG                     (0x101FFA2)  /* ZERO CROSSING 1 output register */
#define DSP_ZCROSS2_OUT_REG                     (0x101FFA4)  /* ZERO CROSSING 2 output register */
#define DSP_PCM_OUT0_REG                        (0x101FFA6)  /* PCM channel 0 output register */
#define DSP_PCM_OUT1_REG                        (0x101FFA8)  /* PCM channel 1 output register */
#define DSP_PCM_OUT2_REG                        (0x101FFAA)  /* PCM channel 2 output register */
#define DSP_PCM_OUT3_REG                        (0x101FFAC)  /* PCM channel 3 output register */
#define DSP_PCM_IN0_REG                         (0x101FFAE)  /* PCM channel 0 input register */
#define DSP_PCM_IN1_REG                         (0x101FFB0)  /* PCM channel 1 input register */
#define DSP_PCM_IN2_REG                         (0x101FFB2)  /* PCM channel 2 input register */
#define DSP_PCM_IN3_REG                         (0x101FFB4)  /* PCM channel 3 input register */
#define DSP_PCM_CTRL_REG                        (0x101FFB6)  /* PCM Control register */
#define DSP_PHASE_INFO_REG                      (0x101FFB8)  /* Phase information between PCM FSC 8/16/32 and main counter 8/16/32 kHz */
#define DSP_VQI_REG                             (0x101FFBA)  /* BMC VQI register */
#define DSP_MAIN_CTRL_REG                       (0x101FFBC)  /* DSP Main counter control and preset value */
#define DSP_CLASSD_BUZZOFF_REG                  (0x101FFBE)  /* CLASSD Buzzer on/off controller, bit 15 */
#define DSP1_CTRL_REG                           (0x101FFD0)  /* DSP1 control register */
#define DSP1_PC_REG                             (0x101FFD2)  /* DSP1 Programma counter */
#define DSP1_PC_START_REG                       (0x101FFD4)  /* DSP1 Programma counter start */
#define DSP1_IRQ_START_REG                      (0x101FFD6)  /* DSP1 Interrupt vector start */
#define DSP1_INT_REG                            (0x101FFD8)  /* DSP1 to CR16C+ interrupt vector */
#define DSP1_INT_MASK_REG                       (0x101FFDA)  /* DSP1 to CR16C+ interrupt vector maks */
#define DSP1_INT_PRIO1_REG                      (0x101FFDC)  /* DSP1 Interrupt source mux 1 register */
#define DSP1_INT_PRIO2_REG                      (0x101FFDE)  /* DSP1 Interrupt source mux 2 register */
#define DSP1_OVERFLOW_REG                       (0x101FFE0)  /* DSP1 to CR16C+ overflow register */
#define DSP1_JTBL_START_REG                     (0x101FFE2)  /* DSP1 jump table start address */
#define DBG1_IREG                               (0x101FFF0)  /* DSP1 JTAG instruction register */
#define DBG1_INOUT_REG_LSW                      (0x101FFF4)  /* Reserved */
#define DBG1_INOUT_REG_MSW                      (0x101FFF6)  /*  */
#define SHARED_ROM2_START                       (0x1020000)  /*  */
#define SHARED_ROM2_END                         (0x102FF4F)  /*  */
#define DSP2_CTRL_REG                           (0x102FFD0)  /* DSP2 control register */
#define DSP2_PC_REG                             (0x102FFD2)  /* DSP2 Programma counter */
#define DSP2_PC_START_REG                       (0x102FFD4)  /* DSP2 Programma counter start */
#define DSP2_IRQ_START_REG                      (0x102FFD6)  /* DSP2 Interrupt vector start */
#define DSP2_INT_REG                            (0x102FFD8)  /* DSP2 to CR16C+ interrupt vector */
#define DSP2_INT_MASK_REG                       (0x102FFDA)  /* DSP2 to CR16C+ interrupt vector maks */
#define DSP2_INT_PRIO1_REG                      (0x102FFDC)  /* DSP2 Interrupt source mux 1 register */
#define DSP2_INT_PRIO2_REG                      (0x102FFDE)  /* DSP2 Interrupt source mux 2 register */
#define DSP2_OVERFLOW_REG                       (0x102FFE0)  /* DSP2 to CR16C+ overflow register */
#define DSP2_JTBL_START_REG                     (0x102FFE2)  /* DSP2 jump table start address */
#define DBG2_IREG                               (0x102FFF0)  /* DSP2 JTAG instruction register */
#define DBG2_INOUT_REG_LSW                      (0x102FFF4)  /* Reserved */
#define DBG2_INOUT_REG_MSW                      (0x102FFF6)  /*  */
#define DSP_MC_RAM1_START                       (0x1030000)  /*  */
#define DSP_MC_RAM1_END                         (0x1034FFF)  /*  */
#define DSP_MC_RAM2_START                       (0x1038000)  /*  */
#define DSP_MC_RAM2_END                         (0x103BFFF)  /*  */
#define DSP_MC_ROM1_START                       (0x1040000)  /*  */
#define DSP_MC_ROM1_END                         (0x1047FFF)  /*  */
#define DSP_MC_ROM2_START                       (0x1060000)  /*  */
#define DSP_MC_ROM2_END                         (0x107BFFF)  /*  */




/*====================================================*/
struct __ACCESS1_IN_OUT_REG
/*====================================================*/
{
    WORD BITFLD_ACCESS_DATA                        : 8;
};

#define ACCESS_DATA                        (0x00FF)


/*====================================================*/
struct __ACCESS2_IN_OUT_REG
/*====================================================*/
{
    WORD BITFLD_ACCESS_DATA                        : 8;
};

#define ACCESS_DATA                        (0x00FF)


/*====================================================*/
struct __ACCESS1_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_EN_ACCESS_BUS                      : 1;
    WORD BITFLD_ACKn                               : 1;
    WORD BITFLD_SCL_VAL                            : 1;
    WORD BITFLD_SDA_VAL                            : 1;
    WORD BITFLD_ACCESS_INT                         : 1;
    WORD BITFLD_EN_ACCESS_INT                      : 1;
    WORD BITFLD_SCL_OD                             : 1;
    WORD BITFLD_SDA_OD                             : 1;
    WORD BITFLD_SCK_NUM                            : 1;
    WORD BITFLD_SCK_SEL                            : 2;
};

#define EN_ACCESS_BUS                      (0x0001)
#define ACKn                               (0x0002)
#define SCL_VAL                            (0x0004)
#define SDA_VAL                            (0x0008)
#define ACCESS_INT                         (0x0010)
#define EN_ACCESS_INT                      (0x0020)
#define SCL_OD                             (0x0040)
#define SDA_OD                             (0x0080)
#define SCK_NUM                            (0x0100)
#define SCK_SEL                            (0x0600)


/*====================================================*/
struct __ACCESS2_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_EN_ACCESS_BUS                      : 1;
    WORD BITFLD_ACKn                               : 1;
    WORD BITFLD_SCL_VAL                            : 1;
    WORD BITFLD_SDA_VAL                            : 1;
    WORD BITFLD_ACCESS_INT                         : 1;
    WORD BITFLD_EN_ACCESS_INT                      : 1;
    WORD BITFLD_SCL_OD                             : 1;
    WORD BITFLD_SDA_OD                             : 1;
    WORD BITFLD_SCK_NUM                            : 1;
    WORD BITFLD_SCK_SEL                            : 2;
};

#define EN_ACCESS_BUS                      (0x0001)
#define ACKn                               (0x0002)
#define SCL_VAL                            (0x0004)
#define SDA_VAL                            (0x0008)
#define ACCESS_INT                         (0x0010)
#define EN_ACCESS_INT                      (0x0020)
#define SCL_OD                             (0x0040)
#define SDA_OD                             (0x0080)
#define SCK_NUM                            (0x0100)
#define SCK_SEL                            (0x0600)


/*====================================================*/
struct __ADC_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_ADC_START                          : 1;
    WORD BITFLD_ADC_AUTO                           : 1;
    WORD BITFLD_ADC_ALT                            : 1;
    WORD BITFLD_ADC_IN_3_0                         : 4;
    WORD BITFLD_ADC_TEST                           : 4;
    WORD BITFLD_ADC_INT                            : 1;
    WORD BITFLD_ADC_MINT                           : 1;
    WORD BITFLD_ADC0_PR_DIS                        : 1;
    WORD BITFLD_ADC1_PR_DIS                        : 1;
    WORD BITFLD_ADC2_PR_DIS                        : 1;
};

#define ADC_START                          (0x0001)
#define ADC_AUTO                           (0x0002)
#define ADC_ALT                            (0x0004)
#define ADC_IN_3_0                         (0x0078)
#define ADC_TEST                           (0x0780)
#define ADC_INT                            (0x0800)
#define ADC_MINT                           (0x1000)
#define ADC0_PR_DIS                        (0x2000)
#define ADC1_PR_DIS                        (0x4000)
#define ADC2_PR_DIS                        (0x8000)


/*====================================================*/
struct __ADC_CTRL1_REG
/*====================================================*/
{
    WORD BITFLD_ADC_IN_3_0_1                       : 4;
};

#define ADC_IN_3_0_1                       (0x000F)


/*====================================================*/
struct __ADC0_REG
/*====================================================*/
{
    WORD BITFLD_ADC0_VAL                           : 10;
};

#define ADC0_VAL                           (0x03FF)


/*====================================================*/
struct __ADC1_REG
/*====================================================*/
{
    WORD BITFLD_ADC1_VAL                           : 10;
};

#define ADC1_VAL                           (0x03FF)


/*====================================================*/
struct __BANDGAP_REG
/*====================================================*/
{
    WORD BITFLD_BANDGAP_VI                         : 4;
    WORD BITFLD_BANDGAP_VIT                        : 2;
    WORD BITFLD_BANDGAP_I                          : 3;
};

#define BANDGAP_VI                         (0x000F)
#define BANDGAP_VIT                        (0x0030)
#define BANDGAP_I                          (0x01C0)


/*====================================================*/
struct __SUPPLY_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_REG_ON                             : 1;
    WORD BITFLD_LDO1_ON                            : 1;
    WORD BITFLD_DC_ON                              : 1;
    WORD BITFLD_LDO1_LEVEL                         : 3;
    WORD BITFLD_DC_MODE_0                          : 1;
    WORD BITFLD_DC_MODE_1                          : 1;
    WORD BITFLD_DC_VOUT                            : 3;
    WORD BITFLD_DC_HYST                            : 1;
    WORD BITFLD_DC_IMAX                            : 1;
    WORD BITFLD_DC_FREQ                            : 2;
    WORD BITFLD_DC_CLK_SEL                         : 1;
};

#define REG_ON                             (0x0001)
#define LDO1_ON                            (0x0002)
#define DC_ON                              (0x0004)
#define LDO1_LEVEL                         (0x0038)
#define DC_MODE_0                          (0x0040)
#define DC_MODE_1                          (0x0080)
#define DC_VOUT                            (0x0700)
#define DC_HYST                            (0x0800)
#define DC_IMAX                            (0x1000)
#define DC_FREQ                            (0x6000)
#define DC_CLK_SEL                         (0x8000)


/*====================================================*/
struct __BMC_CTRL_REG
/*====================================================*/
{
    WORD                                           : 4;
    WORD BITFLD_GAUSS_REF                          : 1;
    WORD BITFLD_RSSI_RANGE                         : 1;
    WORD BITFLD_RSSI_TDO                           : 1;
    WORD                                           : 1;
    WORD BITFLD_SIO_PD                             : 1;
    WORD                                           : 4;
    WORD BITFLD_BCNT_INH                           : 1;
};

#define GAUSS_REF                          (0x0010)
#define RSSI_RANGE                         (0x0020)
#define RSSI_TDO                           (0x0040)
#define SIO_PD                             (0x0100)
#define BCNT_INH                           (0x2000)


/*====================================================*/
struct __BMC_CTRL2_REG
/*====================================================*/
{
    WORD BITFLD_DAC_TEST                           : 3;
    WORD BITFLD_RCV_CTL                            : 5;
};

#define DAC_TEST                           (0x0007)
#define RCV_CTL                            (0x00F8)


/*====================================================*/
struct __BMC_RX_SFIELDH_REG
/*====================================================*/
{
    WORD BITFLD_RX_SFIELDH                         : 8;
};

#define RX_SFIELDH                         (0x00FF)


/*====================================================*/
struct __CACHE_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_CACHE_SIZE                         : 3;
    WORD                                           : 1;
    WORD BITFLD_CACHE_MODE                         : 2;
    WORD BITFLD_CACHE_LOCK                         : 1;
    WORD                                           : 1;
    WORD BITFLD_ICACHE_B_SIZE                      : 1;
    WORD                                           : 1;
    WORD BITFLD_DCACHE_B_SIZE                      : 1;
};

#define CACHE_SIZE                         (0x0007)
#define CACHE_MODE                         (0x0030)
#define CACHE_LOCK                         (0x0040)
#define ICACHE_B_SIZE                      (0x0100)
#define DCACHE_B_SIZE                      (0x0400)


/*====================================================*/
struct __CACHE_LEN0_REG
/*====================================================*/
{
    WORD BITFLD_CACHE_LEN0                         : 9;
};

#define CACHE_LEN0                         (0x01FF)


/*====================================================*/
struct __CACHE_START0_REG
/*====================================================*/
{
    WORD BITFLD_CACHE_START0                       : 9;
};

#define CACHE_START0                       (0x01FF)


/*====================================================*/
struct __CACHE_LEN1_REG
/*====================================================*/
{
    WORD BITFLD_CACHE_LEN1                         : 9;
};

#define CACHE_LEN1                         (0x01FF)


/*====================================================*/
struct __CACHE_START1_REG
/*====================================================*/
{
    WORD BITFLD_CACHE_START1                       : 9;
};

#define CACHE_START1                       (0x01FF)


/*====================================================*/
struct __CACHE_STATUS_REG
/*====================================================*/
{
    WORD                                           : 10;
    WORD BITFLD_ICACHE_HIT                         : 1;
    WORD BITFLD_DCACHE_HIT                         : 1;
    WORD BITFLD_CACHE_TOUCH_TOGGLE                 : 1;
    WORD BITFLD_CACHE_TOUCH                        : 1;
};

#define ICACHE_HIT                         (0x0400)
#define DCACHE_HIT                         (0x0800)
#define CACHE_TOUCH_TOGGLE                 (0x1000)
#define CACHE_TOUCH                        (0x2000)


/*====================================================*/
struct __CCU_MODE_REG
/*====================================================*/
{
    WORD BITFLD_CCU_MODE                           : 2;
    WORD BITFLD_CCU_BIT_SWAP                       : 1;
    WORD BITFLD_CCU_BYTE_SWAP                      : 1;
};

#define CCU_MODE                           (0x0003)
#define CCU_BIT_SWAP                       (0x0004)
#define CCU_BYTE_SWAP                      (0x0008)


/*====================================================*/
struct __GPRG_R0_REG
/*====================================================*/
{
    WORD BITFLD_EBI_CS_POL                         : 5;
    WORD                                           : 3;
    WORD BITFLD_EMAC_REF_CLK_OE                    : 1;
};

#define EBI_CS_POL                         (0x001F)
#define EMAC_REF_CLK_OE                    (0x0100)


/*====================================================*/
struct __GPRG_R1_REG
/*====================================================*/
{
    WORD BITFLD_QSPI_A_HADDR                       : 12;
};

#define QSPI_A_HADDR                       (0x0FFF)


/*====================================================*/
struct __CHIP_ID1_REG
/*====================================================*/
{
    WORD BITFLD_CHIP_ID1                           : 8;
};

#define CHIP_ID1                           (0x00FF)


/*====================================================*/
struct __CHIP_ID2_REG
/*====================================================*/
{
    WORD BITFLD_CHIP_ID2                           : 8;
};

#define CHIP_ID2                           (0x00FF)


/*====================================================*/
struct __CHIP_ID3_REG
/*====================================================*/
{
    WORD BITFLD_CHIP_ID3                           : 8;
};

#define CHIP_ID3                           (0x00FF)


/*====================================================*/
struct __CHIP_MEM_SIZE_REG
/*====================================================*/
{
    WORD BITFLD_MEM_SIZE_ID                        : 8;
};

#define MEM_SIZE_ID                        (0x00FF)


/*====================================================*/
struct __CHIP_REVISION_REG
/*====================================================*/
{
    WORD                                           : 4;
    WORD BITFLD_REVISION_ID                        : 4;
};

#define REVISION_ID                        (0x00F0)


/*====================================================*/
struct __CHIP_TEST1_REG
/*====================================================*/
{
    WORD BITFLD_CHIP_TEST1                         : 8;
};

#define CHIP_TEST1                         (0x00FF)


/*====================================================*/
struct __CHIP_TEST2_REG
/*====================================================*/
{
    WORD BITFLD_CHIP_TEST2                         : 8;
};

#define CHIP_TEST2                         (0x00FF)


/*====================================================*/
struct __CLASSD_BUZZER_REG
/*====================================================*/
{
    WORD BITFLD_CLASSD_BUZ_GAIN                    : 4;
    WORD BITFLD_CLASSD_BUZ_MODE                    : 1;
};

#define CLASSD_BUZ_GAIN                    (0x000F)
#define CLASSD_BUZ_MODE                    (0x0010)


/*====================================================*/
struct __CLASSD_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_CLASSD_PD                          : 1;
    WORD BITFLD_CLASSD_PROT                        : 1;
    WORD BITFLD_CLASSD_VOUT                        : 2;
    WORD BITFLD_CLASSD_CLIP                        : 3;
    WORD BITFLD_CLASSD_INT_BIT                     : 1;
    WORD BITFLD_CLASSD_DITH_D                      : 2;
    WORD BITFLD_CLASSD_DITH_A                      : 2;
    WORD BITFLD_CLASSD_MODE                        : 1;
    WORD BITFLD_CLASSD_MINT                        : 1;
    WORD BITFLD_CLASSD_MOPEN                       : 1;
    WORD BITFLD_CLASSD_POPEN                       : 1;
};

#define CLASSD_PD                          (0x0001)
#define CLASSD_PROT                        (0x0002)
#define CLASSD_VOUT                        (0x000C)
#define CLASSD_CLIP                        (0x0070)
#define CLASSD_INT_BIT                     (0x0080)
#define CLASSD_DITH_D                      (0x0300)
#define CLASSD_DITH_A                      (0x0C00)
#define CLASSD_MODE                        (0x1000)
#define CLASSD_MINT                        (0x2000)
#define CLASSD_MOPEN                       (0x4000)
#define CLASSD_POPEN                       (0x8000)


/*====================================================*/
struct __CLASSD_TEST_REG
/*====================================================*/
{
    WORD BITFLD_CLASSD_SWITCH                      : 1;
    WORD BITFLD_CLASSD_RST_D                       : 1;
    WORD BITFLD_CLASSD_RST_A                       : 1;
    WORD BITFLD_CLASSD_FORCE                       : 1;
    WORD BITFLD_CLASSD_ANA_TEST                    : 3;
};

#define CLASSD_SWITCH                      (0x0001)
#define CLASSD_RST_D                       (0x0002)
#define CLASSD_RST_A                       (0x0004)
#define CLASSD_FORCE                       (0x0008)
#define CLASSD_ANA_TEST                    (0x0070)


/*====================================================*/
struct __CLASSD_NR_REG
/*====================================================*/
{
    WORD BITFLD_CLASSD_NR_ACTIVE                   : 1;
    WORD BITFLD_CLASSD_NR_LVL                      : 3;
    WORD BITFLD_CLASSD_NR_TON                      : 4;
    WORD BITFLD_CLASSD_NR_HYST                     : 2;
    WORD BITFLD_CLASSD_NR_ZERO                     : 2;
};

#define CLASSD_NR_ACTIVE                   (0x0001)
#define CLASSD_NR_LVL                      (0x000E)
#define CLASSD_NR_TON                      (0x00F0)
#define CLASSD_NR_HYST                     (0x0300)
#define CLASSD_NR_ZERO                     (0x0C00)


/*====================================================*/
struct __CLK_GLOBAL_REG
/*====================================================*/
{
    WORD BITFLD_SW_XTAL_PLL1_RATE                  : 1;
    WORD                                           : 4;
    WORD BITFLD_SW_SWITCH_CLK                      : 1;
    WORD BITFLD_SW_DRAMODE                         : 1;
    WORD BITFLD_SW_CODEC_CORRECTION_PRG            : 8;
};

#define SW_XTAL_PLL1_RATE                  (0x0001)
#define SW_SWITCH_CLK                      (0x0020)
#define SW_DRAMODE                         (0x0040)
#define SW_CODEC_CORRECTION_PRG            (0x7F80)


/*====================================================*/
struct __CLK_AMBA_REG
/*====================================================*/
{
    WORD BITFLD_SW_HCLK_DIV                        : 4;
    WORD BITFLD_SW_PCLK_DIV                        : 3;
    WORD BITFLD_SW_SRAM1_EN                        : 1;
    WORD BITFLD_SW_SRAM2_EN                        : 1;
    WORD BITFLD_SW_MCRAM1_EN                       : 1;
    WORD BITFLD_SW_MCRAM2_EN                       : 1;
    WORD BITFLD_AHB_CLK_DIV                        : 2;
};

#define SW_HCLK_DIV                        (0x000F)
#define SW_PCLK_DIV                        (0x0070)
#define SW_SRAM1_EN                        (0x0080)
#define SW_SRAM2_EN                        (0x0100)
#define SW_MCRAM1_EN                       (0x0200)
#define SW_MCRAM2_EN                       (0x0400)
#define AHB_CLK_DIV                        (0x1800)


/*====================================================*/
struct __CLK_CODEC1_REG
/*====================================================*/
{
    WORD BITFLD_SW_ADCDC_DIV                       : 8;
    WORD BITFLD_SW_ADCDC_EN                        : 1;
};

#define SW_ADCDC_DIV                       (0x00FF)
#define SW_ADCDC_EN                        (0x0100)


/*====================================================*/
struct __CLK_CODEC2_REG
/*====================================================*/
{
    WORD BITFLD_SW_DALSRCDC_DIV                    : 8;
    WORD BITFLD_SW_DALSRCDC_EN                     : 1;
};

#define SW_DALSRCDC_DIV                    (0x00FF)
#define SW_DALSRCDC_EN                     (0x0100)


/*====================================================*/
struct __CLK_CODEC3_REG
/*====================================================*/
{
    WORD BITFLD_SW_DACLASSDCDC_DIV                 : 8;
    WORD BITFLD_SW_DACLASSDCDC_EN                  : 1;
};

#define SW_DACLASSDCDC_DIV                 (0x00FF)
#define SW_DACLASSDCDC_EN                  (0x0100)


/*====================================================*/
struct __CLK_SPU1_REG
/*====================================================*/
{
    WORD BITFLD_SW_DSP2_DIV                        : 5;
    WORD BITFLD_SW_DSP1_DIV                        : 5;
    WORD BITFLD_SW_MAINCDC_EN                      : 1;
    WORD BITFLD_SW_CLK_DSP2_EN                     : 1;
    WORD BITFLD_SW_CLK_DSP1_EN                     : 1;
    WORD BITFLD_SW_PCMCDC_EN                       : 1;
    WORD BITFLD_SW_DSP2_MEM_ACC_FORCE              : 1;
    WORD BITFLD_SW_DSP1_MEM_ACC_FORCE              : 1;
};

#define SW_DSP2_DIV                        (0x001F)
#define SW_DSP1_DIV                        (0x03E0)
#define SW_MAINCDC_EN                      (0x0400)
#define SW_CLK_DSP2_EN                     (0x0800)
#define SW_CLK_DSP1_EN                     (0x1000)
#define SW_PCMCDC_EN                       (0x2000)
#define SW_DSP2_MEM_ACC_FORCE              (0x4000)
#define SW_DSP1_MEM_ACC_FORCE              (0x8000)


/*====================================================*/
struct __CLK_SPU2_REG
/*====================================================*/
{
    WORD BITFLD_SW_MAINCDC_DIV                     : 8;
    WORD BITFLD_SW_PCMCDC_DIV                      : 8;
};

#define SW_MAINCDC_DIV                     (0x00FF)
#define SW_PCMCDC_DIV                      (0xFF00)


/*====================================================*/
struct __CLK_CLASSD1_REG
/*====================================================*/
{
    WORD BITFLD_SW_CLASSD_DIV                      : 7;
    WORD BITFLD_SW_CLASSD_EN                       : 1;
};

#define SW_CLASSD_DIV                      (0x007F)
#define SW_CLASSD_EN                       (0x0080)


/*====================================================*/
struct __CLK_CLASSD2_REG
/*====================================================*/
{
    WORD BITFLD_SW_CT_DIV                          : 11;
    WORD BITFLD_SW_CT_EN                           : 1;
};

#define SW_CT_DIV                          (0x07FF)
#define SW_CT_EN                           (0x0800)


/*====================================================*/
struct __CLK_TIM1_REG
/*====================================================*/
{
    WORD BITFLD_SW_TC1MCCLK_DIV                    : 8;
    WORD BITFLD_SW_TCKCLK_DIV                      : 2;
    WORD BITFLD_SW_TC1MC_EN                        : 1;
    WORD BITFLD_SW_TCK_EN                          : 1;
};

#define SW_TC1MCCLK_DIV                    (0x00FF)
#define SW_TCKCLK_DIV                      (0x0300)
#define SW_TC1MC_EN                        (0x0400)
#define SW_TCK_EN                          (0x0800)


/*====================================================*/
struct __CLK_TIM2_REG
/*====================================================*/
{
    WORD BITFLD_SW_TIMCLKGC_DIV                    : 2;
    WORD BITFLD_SW_TIMCLK_DIV                      : 5;
};

#define SW_TIMCLKGC_DIV                    (0x0003)
#define SW_TIMCLK_DIV                      (0x007C)


/*====================================================*/
struct __CLK_BMC1_REG
/*====================================================*/
{
    WORD BITFLD_SW_CLK10SWC_EN                     : 1;
    WORD                                           : 4;
    WORD BITFLD_SW_CLK10SWNC_EN                    : 1;
};

#define SW_CLK10SWC_EN                     (0x0001)
#define SW_CLK10SWNC_EN                    (0x0020)


/*====================================================*/
struct __CLK_BMC2_REG
/*====================================================*/
{
    WORD                                           : 8;
    WORD BITFLD_SW_CLK1GC_EN                       : 1;
};

#define SW_CLK1GC_EN                       (0x0100)


/*====================================================*/
struct __CLK_BMC3_REG
/*====================================================*/
{
    WORD                                           : 10;
    WORD BITFLD_SW_CLK1SWGC_EN                     : 1;
};

#define SW_CLK1SWGC_EN                     (0x0400)


/*====================================================*/
struct __CLK_GPIO1_REG
/*====================================================*/
{
    WORD BITFLD_SW_ADC_DIV                         : 10;
    WORD BITFLD_SW_SPI_DIV                         : 6;
};

#define SW_ADC_DIV                         (0x03FF)
#define SW_SPI_DIV                         (0xFC00)


/*====================================================*/
struct __CLK_GPIO2_REG
/*====================================================*/
{
    WORD BITFLD_SW_ADC10SA_DIV                     : 10;
    WORD BITFLD_SW_SPI2_DIV                        : 6;
};

#define SW_ADC10SA_DIV                     (0x03FF)
#define SW_SPI2_DIV                        (0xFC00)


/*====================================================*/
struct __CLK_GPIO3_REG
/*====================================================*/
{
    WORD BITFLD_SW_TMR0_DIV                        : 11;
    WORD BITFLD_SW_TMR0_EN                         : 1;
    WORD BITFLD_SW_SPI_EN                          : 1;
    WORD BITFLD_SW_ADC_EN                          : 1;
    WORD BITFLD_SW_SPI2_EN                         : 1;
};

#define SW_TMR0_DIV                        (0x07FF)
#define SW_TMR0_EN                         (0x0800)
#define SW_SPI_EN                          (0x1000)
#define SW_ADC_EN                          (0x2000)
#define SW_SPI2_EN                         (0x4000)


/*====================================================*/
struct __CLK_GPIO4_REG
/*====================================================*/
{
    WORD BITFLD_SW_TMR1_DIV                        : 11;
    WORD BITFLD_SW_TMR1_EN                         : 1;
};

#define SW_TMR1_DIV                        (0x07FF)
#define SW_TMR1_EN                         (0x0800)


/*====================================================*/
struct __CLK_GPIO5_REG
/*====================================================*/
{
    WORD BITFLD_SW_AB2_DIV                         : 5;
    WORD BITFLD_SW_AB2_EN                          : 1;
    WORD BITFLD_SW_AB1_DIV                         : 5;
    WORD BITFLD_SW_AB1_EN                          : 1;
};

#define SW_AB2_DIV                         (0x001F)
#define SW_AB2_EN                          (0x0020)
#define SW_AB1_DIV                         (0x07C0)
#define SW_AB1_EN                          (0x0800)


/*====================================================*/
struct __CLK_GPIO6_REG
/*====================================================*/
{
    WORD BITFLD_SW_UART_DIV                        : 6;
    WORD BITFLD_SW_UART_EN                         : 1;
};

#define SW_UART_DIV                        (0x003F)
#define SW_UART_EN                         (0x0040)


/*====================================================*/
struct __CLK_GPIO7_REG
/*====================================================*/
{
    WORD BITFLD_SW_IRDACLK_DIV                     : 5;
    WORD BITFLD_SW_IRDACLK_EN                      : 1;
};

#define SW_IRDACLK_DIV                     (0x001F)
#define SW_IRDACLK_EN                      (0x0020)


/*====================================================*/
struct __CLK_GPIO8_REG
/*====================================================*/
{
    WORD BITFLD_SW_KBRD_DIV                        : 8;
    WORD BITFLD_SW_PER10_DIV                       : 5;
};

#define SW_KBRD_DIV                        (0x00FF)
#define SW_PER10_DIV                       (0x1F00)


/*====================================================*/
struct __CLK_AUX1_REG
/*====================================================*/
{
    WORD BITFLD_SW_MWRGC_EN                        : 1;
    WORD BITFLD_SW_DCLK_DIV                        : 2;
    WORD BITFLD_SW_DCLK_EN                         : 1;
    WORD BITFLD_SW_DCS_SEL                         : 1;
    WORD BITFLD_SWITCH_TO_XTAL                     : 1;
    WORD BITFLD_SWITCH_TO_PLL                      : 1;
};

#define SW_MWRGC_EN                        (0x0001)
#define SW_DCLK_DIV                        (0x0006)
#define SW_DCLK_EN                         (0x0008)
#define SW_DCS_SEL                         (0x0010)
#define SWITCH_TO_XTAL                     (0x0020)
#define SWITCH_TO_PLL                      (0x0040)


/*====================================================*/
struct __CLK_AUX2_REG
/*====================================================*/
{
    WORD BITFLD_SW_BXTAL_DIV                       : 5;
    WORD BITFLD_SW_BXTAL_EN                        : 1;
    WORD BITFLD_SW_ETH_SEL_SPEED                   : 1;
    WORD BITFLD_SW_RMII_CLK_INT                    : 1;
    WORD BITFLD_SW_RMII_EN                         : 1;
    WORD BITFLD_SW_QSPIC_SEL_SOURCE                : 1;
    WORD BITFLD_SW_QSPIC_EN                        : 1;
    WORD BITFLD_SW_CRYPTO_EN                       : 1;
    WORD BITFLD_SW_EMAC_EN                         : 1;
    WORD BITFLD_SW_QSPIC_CLK_DIV                   : 1;
};

#define SW_BXTAL_DIV                       (0x001F)
#define SW_BXTAL_EN                        (0x0020)
#define SW_ETH_SEL_SPEED                   (0x0040)
#define SW_RMII_CLK_INT                    (0x0080)
#define SW_RMII_EN                         (0x0100)
#define SW_QSPIC_SEL_SOURCE                (0x0200)
#define SW_QSPIC_EN                        (0x0400)
#define SW_CRYPTO_EN                       (0x0800)
#define SW_EMAC_EN                         (0x1000)
#define SW_QSPIC_CLK_DIV                   (0x2000)


/*====================================================*/
struct __CLK_JOWI_REG
/*====================================================*/
{
    WORD BITFLD_SW_OWICLK_C_DIV                    : 8;
    WORD BITFLD_SW_OWICLK_C_EN                     : 1;
};

#define SW_OWICLK_C_DIV                    (0x00FF)
#define SW_OWICLK_C_EN                     (0x0100)


/*====================================================*/
struct __CLK_CLK100B_REG
/*====================================================*/
{
    WORD BITFLD_SW_CLK100_DIVH                     : 5;
};

#define SW_CLK100_DIVH                     (0x001F)


/*====================================================*/
struct __CLK_DCDC1_REG
/*====================================================*/
{
    WORD BITFLD_SW_DC_EN                           : 1;
};

#define SW_DC_EN                           (0x0001)


/*====================================================*/
struct __CLK_PLL1_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_PLL1_TESTMODE_SEL                  : 1;
    WORD BITFLD_PLL1_CP_ON                         : 1;
    WORD BITFLD_PLL1_OUT_DIV                       : 1;
    WORD                                           : 1;
    WORD BITFLD_PLL1_VCO_ON                        : 1;
    WORD                                           : 1;
    WORD BITFLD_PLL1_HF_SEL                        : 1;
};

#define PLL1_TESTMODE_SEL                  (0x0001)
#define PLL1_CP_ON                         (0x0002)
#define PLL1_OUT_DIV                       (0x0004)
#define PLL1_VCO_ON                        (0x0010)
#define PLL1_HF_SEL                        (0x0040)


/*====================================================*/
struct __CLK_PLL1_DIV_REG
/*====================================================*/
{
    WORD BITFLD_PLL1_XD                            : 2;
    WORD BITFLD_PLL1_VD                            : 3;
};

#define PLL1_XD                            (0x0003)
#define PLL1_VD                            (0x001C)


/*====================================================*/
struct __CLK_PLL2_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_PLL2_TESTMODE_SEL                  : 1;
    WORD BITFLD_PLL2_CP_ON                         : 1;
    WORD BITFLD_PLL2_PLL_OUT_DIV                   : 1;
    WORD                                           : 1;
    WORD BITFLD_PLL2_VCO_ON                        : 1;
    WORD BITFLD_PLL2_LPF_SEL                       : 1;
    WORD BITFLD_PLL2_HF_SEL                        : 1;
};

#define PLL2_TESTMODE_SEL                  (0x0001)
#define PLL2_CP_ON                         (0x0002)
#define PLL2_PLL_OUT_DIV                   (0x0004)
#define PLL2_VCO_ON                        (0x0010)
#define PLL2_LPF_SEL                       (0x0020)
#define PLL2_HF_SEL                        (0x0040)


/*====================================================*/
struct __CLK_PLL2_DIV_REG
/*====================================================*/
{
    WORD BITFLD_PLL2_XD                            : 7;
    WORD BITFLD_PLL2_DIV2                          : 1;
    WORD BITFLD_PLL2_DIV1                          : 1;
    WORD BITFLD_PLL2_VD                            : 4;
    WORD BITFLD_PLL2_DIV4                          : 1;
    WORD BITFLD_PLL2_DIV3                          : 1;
    WORD BITFLD_PLL2_DIV5                          : 1;
};

#define PLL2_XD                            (0x007F)
#define PLL2_DIV2                          (0x0080)
#define PLL2_DIV1                          (0x0100)
#define PLL2_VD                            (0x1E00)
#define PLL2_DIV4                          (0x2000)
#define PLL2_DIV3                          (0x4000)
#define PLL2_DIV5                          (0x8000)


/*====================================================*/
struct __CLK_FREQ_TRIM_REG
/*====================================================*/
{
    WORD BITFLD_FINE_ADJ                           : 5;
    WORD BITFLD_COARSE_ADJ                         : 3;
    WORD BITFLD_CL_SEL                             : 1;
    WORD BITFLD_OSC_OK                             : 1;
};

#define FINE_ADJ                           (0x001F)
#define COARSE_ADJ                         (0x00E0)
#define CL_SEL                             (0x0100)
#define OSC_OK                             (0x0200)


/*====================================================*/
struct __CLK_XTAL_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_LDO_XTAL_ON                        : 1;
    WORD BITFLD_XTAL_SUPPLY                        : 1;
    WORD BITFLD_AVD_XTAL_OK                        : 1;
    WORD BITFLD_LDO_RFCLK_ON                       : 2;
    WORD BITFLD_RFCLK_SUPPLY                       : 1;
    WORD BITFLD_LDO_RFCLK_OK                       : 1;
    WORD BITFLD_XTAL_EXTRA_CV                      : 1;
};

#define LDO_XTAL_ON                        (0x0001)
#define XTAL_SUPPLY                        (0x0002)
#define AVD_XTAL_OK                        (0x0004)
#define LDO_RFCLK_ON                       (0x0018)
#define RFCLK_SUPPLY                       (0x0020)
#define LDO_RFCLK_OK                       (0x0040)
#define XTAL_EXTRA_CV                      (0x0080)


/*====================================================*/
struct __CLK_XDIV0_REG
/*====================================================*/
{
    WORD BITFLD_CLASSD_XDIV_EN                     : 1;
    WORD BITFLD_CODEC_XDIV_EN                      : 1;
    WORD BITFLD_SPU_XDIV_EN                        : 1;
    WORD BITFLD_TIM_XDIV_EN                        : 1;
    WORD BITFLD_BMC_XDIV_EN                        : 1;
    WORD BITFLD_MWR_XDIV_EN                        : 1;
    WORD BITFLD_DSC_XDIV_EN                        : 1;
    WORD BITFLD_RISCUTIL_XDIV_EN                   : 1;
    WORD BITFLD_JOWI_XDIV_EN                       : 1;
    WORD BITFLD_DCDC_XDIV_EN                       : 1;
    WORD BITFLD_BXTAL_XDIV_EN                      : 1;
    WORD BITFLD_MWRGC_XDIV_EN                      : 1;
};

#define CLASSD_XDIV_EN                     (0x0001)
#define CODEC_XDIV_EN                      (0x0002)
#define SPU_XDIV_EN                        (0x0004)
#define TIM_XDIV_EN                        (0x0008)
#define BMC_XDIV_EN                        (0x0010)
#define MWR_XDIV_EN                        (0x0020)
#define DSC_XDIV_EN                        (0x0040)
#define RISCUTIL_XDIV_EN                   (0x0080)
#define JOWI_XDIV_EN                       (0x0100)
#define DCDC_XDIV_EN                       (0x0200)
#define BXTAL_XDIV_EN                      (0x0400)
#define MWRGC_XDIV_EN                      (0x0800)


/*====================================================*/
struct __CLK_XDIV1_REG
/*====================================================*/
{
    WORD BITFLD_ADC_XDIV_EN                        : 1;
    WORD BITFLD_ADC10SA_XDIV_EN                    : 1;
    WORD BITFLD_TMR0_XDIV_EN                       : 1;
    WORD BITFLD_TMR1_XDIV_EN                       : 1;
    WORD BITFLD_AB1_XDIV_EN                        : 1;
    WORD BITFLD_AB2_XDIV_EN                        : 1;
    WORD BITFLD_UART_XDIV_EN                       : 1;
    WORD BITFLD_IRDA_XDIV_EN                       : 1;
    WORD BITFLD_SPI_XDIV_EN                        : 1;
    WORD BITFLD_SPI2_XDIV_EN                       : 1;
    WORD BITFLD_CT_XDIV_EN                         : 1;
    WORD BITFLD_PER10_XDIV_EN                      : 1;
    WORD BITFLD_KBRD_XDIV_EN                       : 1;
    WORD BITFLD_AHB_XDIV_EN                        : 1;
};

#define ADC_XDIV_EN                        (0x0001)
#define ADC10SA_XDIV_EN                    (0x0002)
#define TMR0_XDIV_EN                       (0x0004)
#define TMR1_XDIV_EN                       (0x0008)
#define AB1_XDIV_EN                        (0x0010)
#define AB2_XDIV_EN                        (0x0020)
#define UART_XDIV_EN                       (0x0040)
#define IRDA_XDIV_EN                       (0x0080)
#define SPI_XDIV_EN                        (0x0100)
#define SPI2_XDIV_EN                       (0x0200)
#define CT_XDIV_EN                         (0x0400)
#define PER10_XDIV_EN                      (0x0800)
#define KBRD_XDIV_EN                       (0x1000)
#define AHB_XDIV_EN                        (0x2000)


/*====================================================*/
struct __CLK_XDIV_VAL_REG
/*====================================================*/
{
    WORD BITFLD_XDIV_VAL                           : 5;
};

#define XDIV_VAL                           (0x001F)


/*====================================================*/
struct __CLK_CDC_CORRECT_REG
/*====================================================*/
{
    WORD BITFLD_SW_ADCDC_COR                       : 2;
    WORD BITFLD_SW_DALSR_COR                       : 2;
    WORD BITFLD_SW_DACLASSD_COR                    : 2;
    WORD BITFLD_SW_PCMCDC_COR                      : 2;
    WORD BITFLD_SW_MAINCDC_COR                     : 2;
};

#define SW_ADCDC_COR                       (0x0003)
#define SW_DALSR_COR                       (0x000C)
#define SW_DACLASSD_COR                    (0x0030)
#define SW_PCMCDC_COR                      (0x00C0)
#define SW_MAINCDC_COR                     (0x0300)


/*====================================================*/
struct __CODEC_ADDA_REG
/*====================================================*/
{
    WORD BITFLD_DA_PD                              : 1;
    WORD BITFLD_AD_PD                              : 1;
    WORD BITFLD_DA_CADJ                            : 2;
    WORD BITFLD_AD_CADJ                            : 2;
    WORD BITFLD_DA_DITH_OFF                        : 1;
    WORD BITFLD_AD_DITH_OFF                        : 1;
    WORD BITFLD_DA_HBW                             : 1;
    WORD BITFLD_LPF_PD                             : 1;
    WORD BITFLD_LPF_BW                             : 3;
    WORD BITFLD_ADC_VREF_LSR                       : 2;
    WORD BITFLD_AUTO_SYNC                          : 1;
};

#define DA_PD                              (0x0001)
#define AD_PD                              (0x0002)
#define DA_CADJ                            (0x000C)
#define AD_CADJ                            (0x0030)
#define DA_DITH_OFF                        (0x0040)
#define AD_DITH_OFF                        (0x0080)
#define DA_HBW                             (0x0100)
#define LPF_PD                             (0x0200)
#define LPF_BW                             (0x1C00)
#define ADC_VREF_LSR                       (0x6000)
#define AUTO_SYNC                          (0x8000)


/*====================================================*/
struct __CODEC_LSR_REG
/*====================================================*/
{
    WORD BITFLD_LSRP_MODE                          : 2;
    WORD BITFLD_LSRP_PD                            : 1;
    WORD BITFLD_LSRN_MODE                          : 2;
    WORD BITFLD_LSRN_PD                            : 1;
    WORD BITFLD_LSRATT                             : 3;
    WORD BITFLD_LSREN_SE                           : 1;
};

#define LSRP_MODE                          (0x0003)
#define LSRP_PD                            (0x0004)
#define LSRN_MODE                          (0x0018)
#define LSRN_PD                            (0x0020)
#define LSRATT                             (0x01C0)
#define LSREN_SE                           (0x0200)


/*====================================================*/
struct __CODEC_MIC_REG
/*====================================================*/
{
    WORD BITFLD_MIC_MODE                           : 2;
    WORD BITFLD_MIC_PD                             : 1;
    WORD BITFLD_MIC_MUTE                           : 1;
    WORD BITFLD_MIC_GAIN                           : 4;
    WORD BITFLD_MIC_OFFCOM_SG                      : 1;
    WORD BITFLD_MIC_OFFCOM_ON                      : 1;
    WORD BITFLD_DSP_CTRL                           : 1;
    WORD BITFLD_MICHN_ON                           : 1;
    WORD BITFLD_MICHP_ON                           : 1;
    WORD BITFLD_MIC_CADJ                           : 1;
};

#define MIC_MODE                           (0x0003)
#define MIC_PD                             (0x0004)
#define MIC_MUTE                           (0x0008)
#define MIC_GAIN                           (0x00F0)
#define MIC_OFFCOM_SG                      (0x0100)
#define MIC_OFFCOM_ON                      (0x0200)
#define DSP_CTRL                           (0x0400)
#define MICHN_ON                           (0x0800)
#define MICHP_ON                           (0x1000)
#define MIC_CADJ                           (0x2000)


/*====================================================*/
struct __CODEC_TEST_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_TCR                                : 12;
    WORD BITFLD_COR_ON                             : 1;
    WORD BITFLD_COR_STAT                           : 1;
};

#define TCR                                (0x0FFF)
#define COR_ON                             (0x1000)
#define COR_STAT                           (0x2000)


/*====================================================*/
struct __CODEC_TONE_REG
/*====================================================*/
{
    WORD BITFLD_CID_PD                             : 1;
    WORD BITFLD_CID_PR_DIS                         : 1;
    WORD BITFLD_RNG_CMP_PD                         : 1;
};

#define CID_PD                             (0x0001)
#define CID_PR_DIS                         (0x0002)
#define RNG_CMP_PD                         (0x0004)


/*====================================================*/
struct __CODEC_VREF_REG
/*====================================================*/
{
    WORD BITFLD_VREF_PD                            : 1;
    WORD                                           : 1;
    WORD BITFLD_VREF_FILT_CADJ                     : 16;
    WORD BITFLD_VREF_INIT                          : 1;
    WORD BITFLD_AMP1V5_PD                          : 1;
    WORD BITFLD_VREF_BG_PD                         : 1;
    WORD BITFLD_BIAS_PD                            : 1;
    WORD BITFLD_AGND_LSR_PD                        : 1;
    WORD BITFLD_REFINT_PD                          : 1;
};

#define VREF_PD                            (0x0001)
#define VREF_FILT_CADJ                     (0x80000000)
#define VREF_INIT                          (0x0010)
#define AMP1V5_PD                          (0x0020)
#define VREF_BG_PD                         (0x0040)
#define BIAS_PD                            (0x0080)
#define AGND_LSR_PD                        (0x0100)
#define REFINT_PD                          (0x0200)


/*====================================================*/
struct __CRYPTO_CLRIRQ_REG
/*====================================================*/
{
    WORD BITFLD_CRYPTO_CLRIRQ                      : 1;
};

#define CRYPTO_CLRIRQ                      (0x0001)


/*====================================================*/
struct __CRYPTO_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_CRYPTO_ALG                         : 2;
    WORD BITFLD_CRYPTO_ALG_MD                      : 2;
    WORD BITFLD_CRYPTO_OUT_MD                      : 1;
    WORD BITFLD_CRYPTO_AES_KEY_SZ                  : 2;
    WORD BITFLD_CRYPTO_ENCDEC                      : 1;
    WORD BITFLD_CRYPTO_IRQ_EN                      : 1;
};

#define CRYPTO_ALG                         (0x0003)
#define CRYPTO_ALG_MD                      (0x000C)
#define CRYPTO_OUT_MD                      (0x0010)
#define CRYPTO_AES_KEY_SZ                  (0x0060)
#define CRYPTO_ENCDEC                      (0x0080)
#define CRYPTO_IRQ_EN                      (0x0100)


/*====================================================*/
struct __CRYPTO_START_REG
/*====================================================*/
{
    WORD BITFLD_CRYPTO_START                       : 1;
};

#define CRYPTO_START                       (0x0001)


/*====================================================*/
struct __CRYPTO_STATUS_REG
/*====================================================*/
{
    WORD BITFLD_CRYPTO_INACTIVE                    : 1;
};

#define CRYPTO_INACTIVE                    (0x0001)


/*====================================================*/
struct __DEBUG_REG
/*====================================================*/
{
    WORD BITFLD_CLK100_EDGE                        : 1;
    WORD BITFLD_CLK100_NEG                         : 1;
    WORD BITFLD_CLK100_POS                         : 1;
    WORD BITFLD_CLK100_SRC                         : 1;
    WORD BITFLD_ENV_B01                            : 1;
    WORD                                           : 2;
    WORD BITFLD_SW_RESET                           : 1;
};

#define CLK100_EDGE                        (0x0001)
#define CLK100_NEG                         (0x0002)
#define CLK100_POS                         (0x0004)
#define CLK100_SRC                         (0x0008)
#define ENV_B01                            (0x0010)
#define SW_RESET                           (0x0080)


/*====================================================*/
struct __DIP_STACK_REG
/*====================================================*/
{
    WORD BITFLD_DIP_STACK                          : 8;
};

#define DIP_STACK                          (0x00FF)


/*====================================================*/
struct __DIP_PC_REG
/*====================================================*/
{
    WORD BITFLD_DIP_PC                             : 8;
    WORD BITFLD_DIP_BANK                           : 1;
};

#define DIP_PC                             (0x00FF)
#define DIP_BANK                           (0x0100)


/*====================================================*/
struct __DIP_STATUS_REG
/*====================================================*/
{
    WORD BITFLD_DIP_INT_VEC                        : 4;
    WORD BITFLD_PD1_INT                            : 1;
    WORD BITFLD_DIP_BRK_INT                        : 1;
    WORD BITFLD_PRESCALER                          : 1;
    WORD BITFLD_URST                               : 1;
};

#define DIP_INT_VEC                        (0x000F)
#define PD1_INT                            (0x0010)
#define DIP_BRK_INT                        (0x0020)
#define PRESCALER                          (0x0040)
#define URST                               (0x0080)


/*====================================================*/
struct __DIP_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_DIP_INT_VEC                        : 4;
    WORD BITFLD_PD1_INT                            : 1;
    WORD BITFLD_DIP_BRK_INT                        : 1;
    WORD BITFLD_PRESCALER                          : 1;
    WORD BITFLD_URST                               : 1;
};

#define DIP_INT_VEC                        (0x000F)
#define PD1_INT                            (0x0010)
#define DIP_BRK_INT                        (0x0020)
#define PRESCALER                          (0x0040)
#define URST                               (0x0080)


/*====================================================*/
struct __DIP_CTRL1_REG
/*====================================================*/
{
    WORD BITFLD_DIP_VNMI_VEC                       : 4;
};

#define DIP_VNMI_VEC                       (0x000F)


/*====================================================*/
struct __DIP_STATUS1_REG
/*====================================================*/
{
    WORD BITFLD_DIP_VNMI_VEC                       : 4;
};

#define DIP_VNMI_VEC                       (0x000F)


/*====================================================*/
struct __DIP_CTRL2_REG
/*====================================================*/
{
    WORD BITFLD_DBUF                               : 1;
    WORD BITFLD_PRE_ACT                            : 1;
    WORD BITFLD_BRK_PRE_OVR                        : 1;
    WORD                                           : 1;
    WORD BITFLD_PD1_INT                            : 1;
    WORD BITFLD_DIP_BRK                            : 1;
    WORD                                           : 1;
    WORD BITFLD_EN_8DIV9                           : 1;
    WORD BITFLD_SLOTCNT_RES                        : 1;
    WORD BITFLD_FR_BMC                             : 1;
    WORD BITFLD_MFR_DSC                            : 1;
};

#define DBUF                               (0x0001)
#define PRE_ACT                            (0x0002)
#define BRK_PRE_OVR                        (0x0004)
#define PD1_INT                            (0x0010)
#define DIP_BRK                            (0x0020)
#define EN_8DIV9                           (0x0080)
#define SLOTCNT_RES                        (0x0100)
#define FR_BMC                             (0x0200)
#define MFR_DSC                            (0x0400)


/*====================================================*/
struct __DIP_MOD_SEL_REG
/*====================================================*/
{
    WORD BITFLD_ARMOD0                             : 1;
    WORD BITFLD_ARMOD1                             : 1;
    WORD BITFLD_ARMOD2                             : 1;
    WORD BITFLD_ARMOD3                             : 1;
    WORD BITFLD_AWMOD0                             : 1;
    WORD BITFLD_AWMOD1                             : 1;
    WORD BITFLD_AWMOD2                             : 1;
    WORD BITFLD_AWMOD3                             : 1;
};

#define ARMOD0                             (0x0001)
#define ARMOD1                             (0x0002)
#define ARMOD2                             (0x0004)
#define ARMOD3                             (0x0008)
#define AWMOD0                             (0x0010)
#define AWMOD1                             (0x0020)
#define AWMOD2                             (0x0040)
#define AWMOD3                             (0x0080)


/*====================================================*/
struct __DIP_MOD_VAL_REG
/*====================================================*/
{
    WORD BITFLD_MODUL00                            : 8;
    WORD BITFLD_MODULO1                            : 8;
};

#define MODUL00                            (0x00FF)
#define MODULO1                            (0xFF00)


/*====================================================*/
struct __DIP_DC01_REG
/*====================================================*/
{
    WORD BITFLD_FRAME                              : 4;
    WORD BITFLD_MFRAME_LOW                         : 12;
};

#define FRAME                              (0x000F)
#define MFRAME_LOW                         (0xFFF0)


/*====================================================*/
struct __DIP_DC23_REG
/*====================================================*/
{
    WORD BITFLD_MFRAME_HIGH                        : 12;
    WORD BITFLD_Reserved                           : 4;
};

#define MFRAME_HIGH                        (0x0FFF)
#define Reserved                           (0xF000)


/*====================================================*/
struct __DIP_SLOT_NUMBER_REG
/*====================================================*/
{
    WORD BITFLD_SLOT_CNTER                         : 5;
};

#define SLOT_CNTER                         (0x001F)


/*====================================================*/
struct __DMA0_A_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_A_STARTH                      : 9;
};

#define DMAx_A_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA1_A_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_A_STARTH                      : 9;
};

#define DMAx_A_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA2_A_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_A_STARTH                      : 9;
};

#define DMAx_A_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA3_A_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_A_STARTH                      : 9;
};

#define DMAx_A_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA0_B_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_B_STARTH                      : 9;
};

#define DMAx_B_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA1_B_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_B_STARTH                      : 9;
};

#define DMAx_B_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA2_B_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_B_STARTH                      : 9;
};

#define DMAx_B_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA3_B_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_B_STARTH                      : 9;
};

#define DMAx_B_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA0_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_DMA_ON                             : 1;
    WORD BITFLD_BW                                 : 2;
    WORD BITFLD_DINT_MODE                          : 1;
    WORD BITFLD_DREQ_MODE                          : 1;
    WORD BITFLD_BINC                               : 1;
    WORD BITFLD_AINC                               : 1;
    WORD BITFLD_CIRCULAR                           : 1;
    WORD BITFLD_DMA_PRIO                           : 2;
    WORD BITFLD_MAX_BURST                          : 2;
    WORD BITFLD_MEM_INIT                           : 1;
    WORD BITFLD_INIT_VAL                           : 1;
};

#define DMA_ON                             (0x0001)
#define BW                                 (0x0006)
#define DINT_MODE                          (0x0008)
#define DREQ_MODE                          (0x0010)
#define BINC                               (0x0020)
#define AINC                               (0x0040)
#define CIRCULAR                           (0x0080)
#define DMA_PRIO                           (0x0300)
#define MAX_BURST                          (0x0C00)
#define MEM_INIT                           (0x1000)
#define INIT_VAL                           (0x2000)


/*====================================================*/
struct __DMA1_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_DMA_ON                             : 1;
    WORD BITFLD_BW                                 : 2;
    WORD BITFLD_DINT_MODE                          : 1;
    WORD BITFLD_DREQ_MODE                          : 1;
    WORD BITFLD_BINC                               : 1;
    WORD BITFLD_AINC                               : 1;
    WORD BITFLD_CIRCULAR                           : 1;
    WORD BITFLD_DMA_PRIO                           : 2;
    WORD BITFLD_MAX_BURST                          : 2;
    WORD BITFLD_MEM_INIT                           : 1;
    WORD BITFLD_INIT_VAL                           : 1;
};

#define DMA_ON                             (0x0001)
#define BW                                 (0x0006)
#define DINT_MODE                          (0x0008)
#define DREQ_MODE                          (0x0010)
#define BINC                               (0x0020)
#define AINC                               (0x0040)
#define CIRCULAR                           (0x0080)
#define DMA_PRIO                           (0x0300)
#define MAX_BURST                          (0x0C00)
#define MEM_INIT                           (0x1000)
#define INIT_VAL                           (0x2000)


/*====================================================*/
struct __DMA2_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_DMA_ON                             : 1;
    WORD BITFLD_BW                                 : 2;
    WORD BITFLD_DINT_MODE                          : 1;
    WORD BITFLD_DREQ_MODE                          : 1;
    WORD BITFLD_BINC                               : 1;
    WORD BITFLD_AINC                               : 1;
    WORD BITFLD_CIRCULAR                           : 1;
    WORD BITFLD_DMA_PRIO                           : 2;
    WORD BITFLD_MAX_BURST                          : 2;
    WORD BITFLD_MEM_INIT                           : 1;
    WORD BITFLD_INIT_VAL                           : 1;
};

#define DMA_ON                             (0x0001)
#define BW                                 (0x0006)
#define DINT_MODE                          (0x0008)
#define DREQ_MODE                          (0x0010)
#define BINC                               (0x0020)
#define AINC                               (0x0040)
#define CIRCULAR                           (0x0080)
#define DMA_PRIO                           (0x0300)
#define MAX_BURST                          (0x0C00)
#define MEM_INIT                           (0x1000)
#define INIT_VAL                           (0x2000)


/*====================================================*/
struct __DMA3_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_DMA_ON                             : 1;
    WORD BITFLD_BW                                 : 2;
    WORD BITFLD_DINT_MODE                          : 1;
    WORD BITFLD_DREQ_MODE                          : 1;
    WORD BITFLD_BINC                               : 1;
    WORD BITFLD_AINC                               : 1;
    WORD BITFLD_CIRCULAR                           : 1;
    WORD BITFLD_DMA_PRIO                           : 2;
    WORD BITFLD_MAX_BURST                          : 2;
    WORD BITFLD_MEM_INIT                           : 1;
    WORD BITFLD_INIT_VAL                           : 1;
};

#define DMA_ON                             (0x0001)
#define BW                                 (0x0006)
#define DINT_MODE                          (0x0008)
#define DREQ_MODE                          (0x0010)
#define BINC                               (0x0020)
#define AINC                               (0x0040)
#define CIRCULAR                           (0x0080)
#define DMA_PRIO                           (0x0300)
#define MAX_BURST                          (0x0C00)
#define MEM_INIT                           (0x1000)
#define INIT_VAL                           (0x2000)


/*====================================================*/
struct __DSP_CLASSD_BUZZOFF_REG
/*====================================================*/
{
    WORD                                           : 15;
    WORD BITFLD_BUZZOFF                            : 1;
};

#define BUZZOFF                            (0x8000)


/*====================================================*/
struct __DSP_CODEC_MIC_GAIN_REG
/*====================================================*/
{
    WORD                                           : 12;
    WORD BITFLD_DSP_MIC_GAIN                       : 4;
};

#define DSP_MIC_GAIN                       (0xF000)


/*====================================================*/
struct __DSP1_CTRL_REG
/*====================================================*/
{
    WORD                                           : 2;
    WORD BITFLD_DSP_EN                             : 1;
    WORD BITFLD_DSP_CLK_EN                         : 1;
    WORD BITFLD_DSP_CR16_INT                       : 1;
    WORD                                           : 3;
    WORD BITFLD_DBG_EN                             : 1;
};

#define DSP_EN                             (0x0004)
#define DSP_CLK_EN                         (0x0008)
#define DSP_CR16_INT                       (0x0010)
#define DBG_EN                             (0x0100)


/*====================================================*/
struct __DSP2_CTRL_REG
/*====================================================*/
{
    WORD                                           : 2;
    WORD BITFLD_DSP_EN                             : 1;
    WORD BITFLD_DSP_CLK_EN                         : 1;
    WORD BITFLD_DSP_CR16_INT                       : 1;
    WORD                                           : 3;
    WORD BITFLD_DBG_EN                             : 1;
};

#define DSP_EN                             (0x0004)
#define DSP_CLK_EN                         (0x0008)
#define DSP_CR16_INT                       (0x0010)
#define DBG_EN                             (0x0100)


/*====================================================*/
struct __DSP1_INT_PRIO1_REG
/*====================================================*/
{
    WORD BITFLD_DSP_INT2_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT1_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT0_PRIO                      : 3;
};

#define DSP_INT2_PRIO                      (0x0007)
#define DSP_INT1_PRIO                      (0x0070)
#define DSP_INT0_PRIO                      (0x0700)


/*====================================================*/
struct __DSP2_INT_PRIO1_REG
/*====================================================*/
{
    WORD BITFLD_DSP_INT2_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT1_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT0_PRIO                      : 3;
};

#define DSP_INT2_PRIO                      (0x0007)
#define DSP_INT1_PRIO                      (0x0070)
#define DSP_INT0_PRIO                      (0x0700)


/*====================================================*/
struct __DSP1_INT_PRIO2_REG
/*====================================================*/
{
    WORD BITFLD_DSP_INT5_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT4_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT3_PRIO                      : 3;
};

#define DSP_INT5_PRIO                      (0x0007)
#define DSP_INT4_PRIO                      (0x0070)
#define DSP_INT3_PRIO                      (0x0700)


/*====================================================*/
struct __DSP2_INT_PRIO2_REG
/*====================================================*/
{
    WORD BITFLD_DSP_INT5_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT4_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT3_PRIO                      : 3;
};

#define DSP_INT5_PRIO                      (0x0007)
#define DSP_INT4_PRIO                      (0x0070)
#define DSP_INT3_PRIO                      (0x0700)


/*====================================================*/
struct __DSP1_IRQ_START_REG
/*====================================================*/
{
    WORD                                           : 4;
    WORD BITFLD_DSPx_IRQ_START                     : 12;
};

#define DSPx_IRQ_START                     (0xFFF0)


/*====================================================*/
struct __DSP2_IRQ_START_REG
/*====================================================*/
{
    WORD                                           : 4;
    WORD BITFLD_DSPx_IRQ_START                     : 12;
};

#define DSPx_IRQ_START                     (0xFFF0)


/*====================================================*/
struct __DSP_MAIN_SYNC0_REG
/*====================================================*/
{
    WORD BITFLD_RAMIN0_SYNC                        : 2;
    WORD BITFLD_RAMIN1_SYNC                        : 2;
    WORD BITFLD_RAMIN2_SYNC                        : 2;
    WORD BITFLD_RAMIN3_SYNC                        : 2;
    WORD BITFLD_RAMOUT0_SYNC                       : 2;
    WORD BITFLD_RAMOUT1_SYNC                       : 2;
    WORD BITFLD_RAMOUT2_SYNC                       : 2;
    WORD BITFLD_RAMOUT3_SYNC                       : 2;
};

#define RAMIN0_SYNC                        (0x0003)
#define RAMIN1_SYNC                        (0x000C)
#define RAMIN2_SYNC                        (0x0030)
#define RAMIN3_SYNC                        (0x00C0)
#define RAMOUT0_SYNC                       (0x0300)
#define RAMOUT1_SYNC                       (0x0C00)
#define RAMOUT2_SYNC                       (0x3000)
#define RAMOUT3_SYNC                       (0xC000)


/*====================================================*/
struct __DSP_MAIN_SYNC1_REG
/*====================================================*/
{
    WORD BITFLD_DSP_SYNC0                          : 2;
    WORD BITFLD_DSP_SYNC1                          : 2;
    WORD BITFLD_DSP_SYNC2                          : 2;
    WORD BITFLD_AD_SYNC                            : 2;
    WORD BITFLD_DA_CLASSD_SYNC                     : 2;
    WORD BITFLD_DA_LSR_SYNC                        : 2;
    WORD BITFLD_ADC_SYNC                           : 2;
    WORD BITFLD_PCM_SYNC                           : 2;
};

#define DSP_SYNC0                          (0x0003)
#define DSP_SYNC1                          (0x000C)
#define DSP_SYNC2                          (0x0030)
#define AD_SYNC                            (0x00C0)
#define DA_CLASSD_SYNC                     (0x0300)
#define DA_LSR_SYNC                        (0x0C00)
#define ADC_SYNC                           (0x3000)
#define PCM_SYNC                           (0xC000)


/*====================================================*/
struct __DSP_MAIN_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_DSP_MAIN_PRESET                    : 8;
    WORD BITFLD_DSP_MAIN_CTRL                      : 2;
};

#define DSP_MAIN_PRESET                    (0x00FF)
#define DSP_MAIN_CTRL                      (0x0300)


/*====================================================*/
struct __DSP_MAIN_CNT_REG
/*====================================================*/
{
    WORD BITFLD_DSP_MAIN_CNT                       : 8;
    WORD BITFLD_DSP_MAIN_REL                       : 8;
};

#define DSP_MAIN_CNT                       (0x00FF)
#define DSP_MAIN_REL                       (0xFF00)


/*====================================================*/
struct __DSP1_OVERFLOW_REG
/*====================================================*/
{
    WORD BITFLD_INT_OVERFLOW                       : 6;
    WORD BITFLD_WTF_OVERFLOW                       : 1;
    WORD BITFLD_IRQ_OVERFLOW                       : 1;
    WORD BITFLD_M_INT_OVERFLOW                     : 6;
    WORD BITFLD_M_WTF_OVERFLOW                     : 1;
    WORD BITFLD_M_IRQ_OVERFLOW                     : 1;
};

#define INT_OVERFLOW                       (0x003F)
#define WTF_OVERFLOW                       (0x0040)
#define IRQ_OVERFLOW                       (0x0080)
#define M_INT_OVERFLOW                     (0x3F00)
#define M_WTF_OVERFLOW                     (0x4000)
#define M_IRQ_OVERFLOW                     (0x8000)


/*====================================================*/
struct __DSP2_OVERFLOW_REG
/*====================================================*/
{
    WORD BITFLD_INT_OVERFLOW                       : 6;
    WORD BITFLD_WTF_OVERFLOW                       : 1;
    WORD BITFLD_IRQ_OVERFLOW                       : 1;
    WORD BITFLD_M_INT_OVERFLOW                     : 6;
    WORD BITFLD_M_WTF_OVERFLOW                     : 1;
    WORD BITFLD_M_IRQ_OVERFLOW                     : 1;
};

#define INT_OVERFLOW                       (0x003F)
#define WTF_OVERFLOW                       (0x0040)
#define IRQ_OVERFLOW                       (0x0080)
#define M_INT_OVERFLOW                     (0x3F00)
#define M_WTF_OVERFLOW                     (0x4000)
#define M_IRQ_OVERFLOW                     (0x8000)


/*====================================================*/
struct __DSP1_JTBL_START_REG
/*====================================================*/
{
    WORD                                           : 9;
    WORD BITFLD_DSPx_JTBL                          : 7;
};

#define DSPx_JTBL                          (0xFE00)


/*====================================================*/
struct __DSP2_JTBL_START_REG
/*====================================================*/
{
    WORD                                           : 9;
    WORD BITFLD_DSPx_JTBL                          : 7;
};

#define DSPx_JTBL                          (0xFE00)


/*====================================================*/
struct __DSP_PCM_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_PCM_EN                             : 1;
    WORD BITFLD_PCM_MASTER                         : 1;
    WORD BITFLD_DSP_PCM_SYNC                       : 1;
    WORD BITFLD_PCM_FSC0LEN                        : 2;
    WORD BITFLD_PCM_FSC0DEL                        : 1;
    WORD BITFLD_PCM_PPOD                           : 1;
    WORD BITFLD_PCM_CLKINV                         : 1;
};

#define PCM_EN                             (0x0001)
#define PCM_MASTER                         (0x0002)
#define DSP_PCM_SYNC                       (0x0004)
#define PCM_FSC0LEN                        (0x0018)
#define PCM_FSC0DEL                        (0x0020)
#define PCM_PPOD                           (0x0040)
#define PCM_CLKINV                         (0x0080)


/*====================================================*/
struct __DSP_PHASE_INFO_REG
/*====================================================*/
{
    WORD BITFLD_PHASE_INFO                         : 8;
};

#define PHASE_INFO                         (0x00FF)


/*====================================================*/
struct __DSP_RAM_OUT0_REG
/*====================================================*/
{
    WORD BITFLD_DSP_RAM_OUTx                       : 8;
};

#define DSP_RAM_OUTx                       (0x00FF)


/*====================================================*/
struct __DSP_RAM_OUT1_REG
/*====================================================*/
{
    WORD BITFLD_DSP_RAM_OUTx                       : 8;
};

#define DSP_RAM_OUTx                       (0x00FF)


/*====================================================*/
struct __DSP_RAM_OUT2_REG
/*====================================================*/
{
    WORD BITFLD_DSP_RAM_OUTx                       : 8;
};

#define DSP_RAM_OUTx                       (0x00FF)


/*====================================================*/
struct __DSP_RAM_OUT3_REG
/*====================================================*/
{
    WORD BITFLD_DSP_RAM_OUTx                       : 8;
};

#define DSP_RAM_OUTx                       (0x00FF)


/*====================================================*/
struct __DSP_RAM_IN0_REG
/*====================================================*/
{
    WORD BITFLD_DSP_RAM_INx                        : 8;
};

#define DSP_RAM_INx                        (0x00FF)


/*====================================================*/
struct __DSP_RAM_IN1_REG
/*====================================================*/
{
    WORD BITFLD_DSP_RAM_INx                        : 8;
};

#define DSP_RAM_INx                        (0x00FF)


/*====================================================*/
struct __DSP_RAM_IN2_REG
/*====================================================*/
{
    WORD BITFLD_DSP_RAM_INx                        : 8;
};

#define DSP_RAM_INx                        (0x00FF)


/*====================================================*/
struct __DSP_RAM_IN3_REG
/*====================================================*/
{
    WORD BITFLD_DSP_RAM_INx                        : 8;
};

#define DSP_RAM_INx                        (0x00FF)


/*====================================================*/
struct __DSP_VQI_REG
/*====================================================*/
{
    WORD BITFLD_BVQI_ON                            : 4;
};

#define BVQI_ON                            (0x000F)


/*====================================================*/
struct __DSP_ZCROSS1_OUT_REG
/*====================================================*/
{
    WORD                                           : 15;
    WORD BITFLD_DSP_ZCROSSx                        : 1;
};

#define DSP_ZCROSSx                        (0x8000)


/*====================================================*/
struct __DSP_ZCROSS2_OUT_REG
/*====================================================*/
{
    WORD                                           : 15;
    WORD BITFLD_DSP_ZCROSSx                        : 1;
};

#define DSP_ZCROSSx                        (0x8000)


/*====================================================*/
struct __EBI_SDCONR_REG
/*====================================================*/
{
    WORD                                           : 3;
    WORD BITFLD_S_BANK_ADDR_WIDTH                  : 2;
    WORD BITFLD_S_ROW_ADDR_WIDTH                   : 4;
    WORD BITFLD_S_COL_ADDR_WIDTH                   : 4;
    WORD BITFLD_S_DATA_WIDTH                       : 2;
};

#define S_BANK_ADDR_WIDTH                  (0x0018)
#define S_ROW_ADDR_WIDTH                   (0x01E0)
#define S_COL_ADDR_WIDTH                   (0x1E00)
#define S_DATA_WIDTH                       (0x6000)


/*====================================================*/
struct __EBI_SDTMG0R_REG
/*====================================================*/
{
    WORD BITFLD_CAS_LATENCY                        : 2;
    WORD BITFLD_T_RAS_MIN                          : 4;
    WORD BITFLD_T_RCD                              : 3;
    WORD BITFLD_T_RP                               : 3;
    WORD BITFLD_T_WRSD                             : 2;
    WORD BITFLD_T_RCAR                             : 4;
    WORD BITFLD_T_XSR                              : 4;
    WORD BITFLD_T_RCSD                             : 4;
    WORD BITFLD_CAS_LATENCY_H                      : 1;
    WORD BITFLD_T_XSR_H                            : 5;
};

#define CAS_LATENCY                        (0x0003)
#define T_RAS_MIN                          (0x003C)
#define T_RCD                              (0x01C0)
#define T_RP                               (0x0E00)
#define T_WRSD                             (0x3000)
#define T_RCAR                             (0x3C000)
#define T_XSR                              (0x3C0000)
#define T_RCSD                             (0x3C00000)
#define CAS_LATENCY_H                      (0x4000000)
#define T_XSR_H                            (0xF8000000)


/*====================================================*/
struct __EBI_SDTMG1R_REG
/*====================================================*/
{
    WORD BITFLD_T_INIT                             : 16;
    WORD BITFLD_NUM_INIT_REF                       : 4;
};

#define T_INIT                             (0xFFFF)
#define NUM_INIT_REF                       (0xF0000)


/*====================================================*/
struct __EBI_SDCTLR_REG
/*====================================================*/
{
    WORD BITFLD_INITIALIZE                         : 1;
    WORD BITFLD_SR_OR_DP_MODE                      : 1;
    WORD BITFLD_POWER_DOWN_MODE                    : 1;
    WORD BITFLD_PRECHARGE_ALGO                     : 1;
    WORD BITFLD_FULL_REFRESH_BEFORE_SR             : 1;
    WORD BITFLD_FULL_REFRESH_AFTER_SR              : 1;
    WORD BITFLD_READ_PIPE                          : 3;
    WORD BITFLD_SET_MODE_REG                       : 1;
    WORD                                           : 1;
    WORD BITFLD_SELF_REFRESH_STATUS                : 1;
    WORD BITFLD_NUM_OPEN_BANKS                     : 5;
    WORD BITFLD_S_RD_READY_MODE                    : 1;
};

#define INITIALIZE                         (0x0001)
#define SR_OR_DP_MODE                      (0x0002)
#define POWER_DOWN_MODE                    (0x0004)
#define PRECHARGE_ALGO                     (0x0008)
#define FULL_REFRESH_BEFORE_SR             (0x0010)
#define FULL_REFRESH_AFTER_SR              (0x0020)
#define READ_PIPE                          (0x01C0)
#define SET_MODE_REG                       (0x0200)
#define SELF_REFRESH_STATUS                (0x0800)
#define NUM_OPEN_BANKS                     (0x1F000)
#define S_RD_READY_MODE                    (0x20000)


/*====================================================*/
struct __EBI_SDREFR_REG
/*====================================================*/
{
    WORD BITFLD_T_REF                              : 16;
    WORD BITFLD_READ_PIPE_CLK                      : 3;
    WORD BITFLD_ACS3_IOEXP                         : 1;
    WORD BITFLD_READ_PIPE_MUX                      : 3;
    WORD BITFLD_GPO                                : 1;
    WORD BITFLD_GPI                                : 8;
};

#define T_REF                              (0xFFFF)
#define READ_PIPE_CLK                      (0x70000)
#define ACS3_IOEXP                         (0x80000)
#define READ_PIPE_MUX                      (0x700000)
#define GPO                                (0x800000)
#define GPI                                (0xFF000000)


/*====================================================*/
struct __EBI_ACS0_LOW_REG
/*====================================================*/
{
    WORD                                           : 11;
    WORD BITFLD_EBI_RES                            : 5;
    WORD BITFLD_CS_BASE                            : 16;
};

#define EBI_RES                            (0xF800)
#define CS_BASE                            (0xFFFF0000)


/*====================================================*/
struct __EBI_ACS1_LOW_REG
/*====================================================*/
{
    WORD                                           : 11;
    WORD BITFLD_EBI_RES                            : 5;
    WORD BITFLD_CS_BASE                            : 16;
};

#define EBI_RES                            (0xF800)
#define CS_BASE                            (0xFFFF0000)


/*====================================================*/
struct __EBI_ACS2_LOW_REG
/*====================================================*/
{
    WORD                                           : 11;
    WORD BITFLD_EBI_RES                            : 5;
    WORD BITFLD_CS_BASE                            : 16;
};

#define EBI_RES                            (0xF800)
#define CS_BASE                            (0xFFFF0000)


/*====================================================*/
struct __EBI_ACS3_LOW_REG
/*====================================================*/
{
    WORD                                           : 11;
    WORD BITFLD_EBI_RES                            : 5;
    WORD BITFLD_CS_BASE                            : 16;
};

#define EBI_RES                            (0xF800)
#define CS_BASE                            (0xFFFF0000)


/*====================================================*/
struct __EBI_ACS4_LOW_REG
/*====================================================*/
{
    WORD                                           : 11;
    WORD BITFLD_EBI_RES                            : 5;
    WORD BITFLD_CS_BASE                            : 16;
};

#define EBI_RES                            (0xF800)
#define CS_BASE                            (0xFFFF0000)


/*====================================================*/
struct __EBI_ACS0_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_MEM_SIZE                           : 5;
    WORD BITFLD_MEM_TYPE                           : 3;
    WORD BITFLD_REG_SELECT                         : 3;
};

#define MEM_SIZE                           (0x001F)
#define MEM_TYPE                           (0x00E0)
#define REG_SELECT                         (0x0700)


/*====================================================*/
struct __EBI_ACS1_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_MEM_SIZE                           : 5;
    WORD BITFLD_MEM_TYPE                           : 3;
    WORD BITFLD_REG_SELECT                         : 3;
};

#define MEM_SIZE                           (0x001F)
#define MEM_TYPE                           (0x00E0)
#define REG_SELECT                         (0x0700)


/*====================================================*/
struct __EBI_ACS2_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_MEM_SIZE                           : 5;
    WORD BITFLD_MEM_TYPE                           : 3;
    WORD BITFLD_REG_SELECT                         : 3;
};

#define MEM_SIZE                           (0x001F)
#define MEM_TYPE                           (0x00E0)
#define REG_SELECT                         (0x0700)


/*====================================================*/
struct __EBI_ACS3_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_MEM_SIZE                           : 5;
    WORD BITFLD_MEM_TYPE                           : 3;
    WORD BITFLD_REG_SELECT                         : 3;
};

#define MEM_SIZE                           (0x001F)
#define MEM_TYPE                           (0x00E0)
#define REG_SELECT                         (0x0700)


/*====================================================*/
struct __EBI_ACS4_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_MEM_SIZE                           : 5;
    WORD BITFLD_MEM_TYPE                           : 3;
    WORD BITFLD_REG_SELECT                         : 3;
};

#define MEM_SIZE                           (0x001F)
#define MEM_TYPE                           (0x00E0)
#define REG_SELECT                         (0x0700)


/*====================================================*/
struct __EBI_SMTMGR_SET0_REG
/*====================================================*/
{
    WORD BITFLD_T_RC                               : 6;
    WORD BITFLD_T_AS                               : 2;
    WORD BITFLD_T_WR                               : 2;
    WORD BITFLD_T_WP                               : 6;
    WORD BITFLD_T_BTA                              : 3;
    WORD BITFLD_T_PRC                              : 4;
    WORD BITFLD_PAGE_MODE                          : 1;
    WORD BITFLD_PAGE_SIZE                          : 2;
    WORD BITFLD_READY_MODE                         : 1;
    WORD BITFLD_LOW_FREQ_SYNC                      : 1;
    WORD BITFLD_SM_READ_PIPE                       : 2;
};

#define T_RC                               (0x003F)
#define T_AS                               (0x00C0)
#define T_WR                               (0x0300)
#define T_WP                               (0xFC00)
#define T_BTA                              (0x70000)
#define T_PRC                              (0x780000)
#define PAGE_MODE                          (0x800000)
#define PAGE_SIZE                          (0x3000000)
#define READY_MODE                         (0x4000000)
#define LOW_FREQ_SYNC                      (0x8000000)
#define SM_READ_PIPE                       (0x30000000)


/*====================================================*/
struct __EBI_SMTMGR_SET1_REG
/*====================================================*/
{
    WORD BITFLD_T_RC                               : 6;
    WORD BITFLD_T_AS                               : 2;
    WORD BITFLD_T_WR                               : 2;
    WORD BITFLD_T_WP                               : 6;
    WORD BITFLD_T_BTA                              : 3;
    WORD BITFLD_T_PRC                              : 4;
    WORD BITFLD_PAGE_MODE                          : 1;
    WORD BITFLD_PAGE_SIZE                          : 2;
    WORD BITFLD_READY_MODE                         : 1;
    WORD BITFLD_LOW_FREQ_SYNC                      : 1;
    WORD BITFLD_SM_READ_PIPE                       : 2;
};

#define T_RC                               (0x003F)
#define T_AS                               (0x00C0)
#define T_WR                               (0x0300)
#define T_WP                               (0xFC00)
#define T_BTA                              (0x70000)
#define T_PRC                              (0x780000)
#define PAGE_MODE                          (0x800000)
#define PAGE_SIZE                          (0x3000000)
#define READY_MODE                         (0x4000000)
#define LOW_FREQ_SYNC                      (0x8000000)
#define SM_READ_PIPE                       (0x30000000)


/*====================================================*/
struct __EBI_SMTMGR_SET2_REG
/*====================================================*/
{
    WORD BITFLD_T_RC                               : 6;
    WORD BITFLD_T_AS                               : 2;
    WORD BITFLD_T_WR                               : 2;
    WORD BITFLD_T_WP                               : 6;
    WORD BITFLD_T_BTA                              : 3;
    WORD BITFLD_T_PRC                              : 4;
    WORD BITFLD_PAGE_MODE                          : 1;
    WORD BITFLD_PAGE_SIZE                          : 2;
    WORD BITFLD_READY_MODE                         : 1;
    WORD BITFLD_LOW_FREQ_SYNC                      : 1;
    WORD BITFLD_SM_READ_PIPE                       : 2;
};

#define T_RC                               (0x003F)
#define T_AS                               (0x00C0)
#define T_WR                               (0x0300)
#define T_WP                               (0xFC00)
#define T_BTA                              (0x70000)
#define T_PRC                              (0x780000)
#define PAGE_MODE                          (0x800000)
#define PAGE_SIZE                          (0x3000000)
#define READY_MODE                         (0x4000000)
#define LOW_FREQ_SYNC                      (0x8000000)
#define SM_READ_PIPE                       (0x30000000)


/*====================================================*/
struct __EBI_FLASH_TRPDR_REG
/*====================================================*/
{
    WORD BITFLD_T_RPD                              : 12;
};

#define T_RPD                              (0x0FFF)


/*====================================================*/
struct __EBI_SMCTLR_REG
/*====================================================*/
{
    WORD BITFLD_sSM_RP_N                           : 1;
    WORD BITFLD_WP_N                               : 3;
    WORD                                           : 3;
    WORD BITFLD_SM_DATA_WIDTH_SET0                 : 3;
    WORD BITFLD_SM_DATA_WIDTH_SET1                 : 3;
    WORD BITFLD_SM_DATA_WIDTH_SET2                 : 3;
};

#define sSM_RP_N                           (0x0001)
#define WP_N                               (0x000E)
#define SM_DATA_WIDTH_SET0                 (0x0380)
#define SM_DATA_WIDTH_SET1                 (0x1C00)
#define SM_DATA_WIDTH_SET2                 (0xE000)


/*====================================================*/
struct __INT0_PRIORITY_REG
/*====================================================*/
{
    WORD BITFLD_SPI2_INT_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP1_INT_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP2_INT_PRIO                      : 3;
};

#define SPI2_INT_PRIO                      (0x0007)
#define DSP1_INT_PRIO                      (0x0070)
#define DSP2_INT_PRIO                      (0x0700)


/*====================================================*/
struct __INT1_PRIORITY_REG
/*====================================================*/
{
    WORD BITFLD_TIM1_INT_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_CLK100_INT_PRIO                    : 3;
    WORD                                           : 1;
    WORD BITFLD_DIP_INT_PRIO                       : 3;
    WORD                                           : 1;
    WORD BITFLD_CRYPTO_INT_PRIO                    : 3;
};

#define TIM1_INT_PRIO                      (0x0007)
#define CLK100_INT_PRIO                    (0x0070)
#define DIP_INT_PRIO                       (0x0700)
#define CRYPTO_INT_PRIO                    (0x7000)


/*====================================================*/
struct __INT2_PRIORITY_REG
/*====================================================*/
{
    WORD BITFLD_UART_RI_INT_PRIO                   : 3;
    WORD                                           : 1;
    WORD BITFLD_UART_TI_INT_PRIO                   : 3;
    WORD                                           : 1;
    WORD BITFLD_SPI1_ADC_INT_PRIO                  : 3;
    WORD                                           : 1;
    WORD BITFLD_TIM0_INT_PRIO                      : 3;
};

#define UART_RI_INT_PRIO                   (0x0007)
#define UART_TI_INT_PRIO                   (0x0070)
#define SPI1_ADC_INT_PRIO                  (0x0700)
#define TIM0_INT_PRIO                      (0x7000)


/*====================================================*/
struct __INT3_PRIORITY_REG
/*====================================================*/
{
    WORD BITFLD_ACCESS_INT_PRIO                    : 3;
    WORD                                           : 1;
    WORD BITFLD_KEYB_INT_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_EMAC_INT_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_CT_CLASSD_INT_PRIO                 : 3;
};

#define ACCESS_INT_PRIO                    (0x0007)
#define KEYB_INT_PRIO                      (0x0070)
#define EMAC_INT_PRIO                      (0x0700)
#define CT_CLASSD_INT_PRIO                 (0x7000)


/*====================================================*/
struct __KEY_GP_INT_REG
/*====================================================*/
{
    WORD BITFLD_INT6_CTRL                          : 3;
    WORD BITFLD_INT7_CTRL                          : 3;
    WORD BITFLD_INT8_CTRL                          : 3;
};

#define INT6_CTRL                          (0x0007)
#define INT7_CTRL                          (0x0038)
#define INT8_CTRL                          (0x01C0)


/*====================================================*/
struct __KEY_BOARD_INT_REG
/*====================================================*/
{
    WORD BITFLD_INT0_EN                            : 1;
    WORD BITFLD_INT1_EN                            : 1;
    WORD BITFLD_INT2_EN                            : 1;
    WORD BITFLD_INT3_EN                            : 1;
    WORD BITFLD_INT4_EN                            : 1;
    WORD BITFLD_INT5_EN                            : 1;
    WORD                                           : 4;
    WORD BITFLD_KEY_LEVEL                          : 1;
    WORD BITFLD_KEY_REL                            : 1;
};

#define INT0_EN                            (0x0001)
#define INT1_EN                            (0x0002)
#define INT2_EN                            (0x0004)
#define INT3_EN                            (0x0008)
#define INT4_EN                            (0x0010)
#define INT5_EN                            (0x0020)
#define KEY_LEVEL                          (0x0400)
#define KEY_REL                            (0x0800)


/*====================================================*/
struct __KEY_DEBOUNCE_REG
/*====================================================*/
{
    WORD BITFLD_DEBOUNCE                           : 6;
    WORD BITFLD_KEY_REPEAT                         : 6;
};

#define DEBOUNCE                           (0x003F)
#define KEY_REPEAT                         (0x0FC0)


/*====================================================*/
struct __KEY_STATUS_REG
/*====================================================*/
{
    WORD BITFLD_KEY_STATUS                         : 4;
};

#define KEY_STATUS                         (0x000F)


/*====================================================*/
struct __PC_START_REG
/*====================================================*/
{
    WORD BITFLD_PC_START10                         : 2;
    WORD BITFLD_PC_START                           : 14;
};

#define PC_START10                         (0x0003)
#define PC_START                           (0xFFFC)


/*====================================================*/
struct __P0_00_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_01_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_02_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_03_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_04_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_05_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_06_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_07_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_08_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_09_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_10_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_11_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_12_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_13_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_14_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_15_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_00_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_01_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_02_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_03_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_04_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_05_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_06_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_07_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_08_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_09_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_10_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_11_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_12_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_13_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_14_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_15_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_00_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_01_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_02_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_03_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_04_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_05_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_06_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_07_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_08_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_09_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_10_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_11_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_12_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_13_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_14_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_15_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P3_SET_DATA_REG
/*====================================================*/
{
    WORD BITFLD_P3_SET                             : 9;
};

#define P3_SET                             (0x01FF)


/*====================================================*/
struct __P3_RESET_DATA_REG
/*====================================================*/
{
    WORD BITFLD_P3_RESET                           : 9;
};

#define P3_RESET                           (0x01FF)


/*====================================================*/
struct __P3_00_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P3_01_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P3_02_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P3_03_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P3_04_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P3_05_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P3_06_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P3_07_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P3_08_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __QSPIC_BURSTCMDA_REG
/*====================================================*/
{
    WORD BITFLD_QSPIC_INST	                        : 8;
    WORD BITFLD_QSPIC_INST_WB	                     : 8;
    WORD BITFLD_QSPIC_EXT_BYTE	                    : 8;
    WORD BITFLD_QSPIC_INST_TX_MD	                  : 2;
    WORD BITFLD_QSPIC_ADR_TX_MD	                   : 2;
    WORD BITFLD_QSPIC_EXT_TX_MD	                   : 2;
    WORD BITFLD_QSPIC_DMY_TX_MD	                   : 2;
};

#define QSPIC_INST	                        (0x00FF)
#define QSPIC_INST_WB	                     (0xFF00)
#define QSPIC_EXT_BYTE	                    (0xFF0000)
#define QSPIC_INST_TX_MD	                  (0x3000000)
#define QSPIC_ADR_TX_MD	                   (0xC000000)
#define QSPIC_EXT_TX_MD	                   (0x30000000)
#define QSPIC_DMY_TX_MD	                   (0xC0000000)


/*====================================================*/
struct __QSPIC_BURSTCMDB_REG
/*====================================================*/
{
    WORD BITFLD_QSPIC_DAT_RX_MD                    : 2;
    WORD BITFLD_QSPIC_EXT_BYTE_EN                  : 1;
    WORD BITFLD_QSPIC_EXT_HF_DS                    : 1;
    WORD BITFLD_QSPIC_DMY_NUM                      : 2;
    WORD BITFLD_QSPIC_INST_MD                      : 1;
    WORD BITFLD_QSPIC_WRAP_MD                      : 1;
    WORD BITFLD_QSPIC_WRAP_LEN                     : 2;
    WORD BITFLD_QSPIC_WRAP_SIZE                    : 2;
    WORD BITFLD_QSPIC_CS_HIGH_MIN                  : 3;
};

#define QSPIC_DAT_RX_MD                    (0x0003)
#define QSPIC_EXT_BYTE_EN                  (0x0004)
#define QSPIC_EXT_HF_DS                    (0x0008)
#define QSPIC_DMY_NUM                      (0x0030)
#define QSPIC_INST_MD                      (0x0040)
#define QSPIC_WRAP_MD                      (0x0080)
#define QSPIC_WRAP_LEN                     (0x0300)
#define QSPIC_WRAP_SIZE                    (0x0C00)
#define QSPIC_CS_HIGH_MIN                  (0x7000)


/*====================================================*/
struct __QSPIC_CTRLBUS_REG
/*====================================================*/
{
    WORD BITFLD_QSPIC_SET_SINGLE                   : 1;
    WORD BITFLD_QSPIC_SET_DUAL                     : 1;
    WORD BITFLD_QSPIC_SET_QUAD                     : 1;
    WORD BITFLD_QSPIC_EN_CS                        : 1;
    WORD BITFLD_QSPIC_DIS_CS                       : 1;
};

#define QSPIC_SET_SINGLE                   (0x0001)
#define QSPIC_SET_DUAL                     (0x0002)
#define QSPIC_SET_QUAD                     (0x0004)
#define QSPIC_EN_CS                        (0x0008)
#define QSPIC_DIS_CS                       (0x0010)


/*====================================================*/
struct __QSPIC_CTRLMODE_REG
/*====================================================*/
{
    WORD BITFLD_QSPIC_AUTO_MD                      : 1;
    WORD BITFLD_QSPIC_CLK_MD                       : 1;
    WORD BITFLD_QSPIC_IO2_OEN                      : 1;
    WORD BITFLD_QSPIC_IO3_OEN                      : 1;
    WORD BITFLD_QSPIC_IO2_DAT                      : 1;
    WORD BITFLD_QSPIC_IO3_DAT                      : 1;
    WORD BITFLD_QSPIC_HRDY_MD                      : 1;
    WORD BITFLD_QSPIC_RXD_NEG                      : 1;
};

#define QSPIC_AUTO_MD                      (0x0001)
#define QSPIC_CLK_MD                       (0x0002)
#define QSPIC_IO2_OEN                      (0x0004)
#define QSPIC_IO3_OEN                      (0x0008)
#define QSPIC_IO2_DAT                      (0x0010)
#define QSPIC_IO3_DAT                      (0x0020)
#define QSPIC_HRDY_MD                      (0x0040)
#define QSPIC_RXD_NEG                      (0x0080)


/*====================================================*/
struct __QSPIC_STATUS_REG
/*====================================================*/
{
    WORD BITFLD_QSPIC_BUSY                         : 1;
};

#define QSPIC_BUSY                         (0x0001)


/*====================================================*/
struct __EMAC_MACR0_CONFIG_REG
/*====================================================*/
{
    WORD                                           : 2;
    WORD BITFLD_RE                                 : 1;
    WORD BITFLD_TE                                 : 1;
    WORD BITFLD_DC                                 : 1;
    WORD BITFLD_BL                                 : 2;
    WORD BITFLD_ACS                                : 1;
    WORD BITFLD_LUD                                : 1;
    WORD BITFLD_DR                                 : 1;
    WORD BITFLD_IPC                                : 1;
    WORD BITFLD_DM                                 : 1;
    WORD BITFLD_LM                                 : 1;
    WORD BITFLD_DO                                 : 1;
    WORD BITFLD_FES                                : 1;
    WORD                                           : 1;
    WORD BITFLD_DCRS                               : 1;
    WORD BITFLD_IFG                                : 3;
    WORD BITFLD_JE                                 : 1;
    WORD                                           : 1;
    WORD BITFLD_JD                                 : 1;
    WORD BITFLD_WD                                 : 1;
    WORD BITFLD_TC                                 : 1;
};

#define RE                                 (0x0004)
#define TE                                 (0x0008)
#define DC                                 (0x0010)
#define BL                                 (0x0060)
#define ACS                                (0x0080)
#define LUD                                (0x0100)
#define DR                                 (0x0200)
#define IPC                                (0x0400)
#define DM                                 (0x0800)
#define LM                                 (0x1000)
#define DO                                 (0x2000)
#define FES                                (0x4000)
#define DCRS                               (0x10000)
#define IFG                                (0xE0000)
#define JE                                 (0x100000)
#define JD                                 (0x400000)
#define WD                                 (0x800000)
#define TC                                 (0x1000000)


/*====================================================*/
struct __EMAC_MACR1_FRAME_FILTER_REG
/*====================================================*/
{
    WORD BITFLD_PRM                                : 1;
    WORD                                           : 2;
    WORD BITFLD_DAIF                               : 1;
    WORD BITFLD_PM                                 : 1;
    WORD BITFLD_DBF                                : 1;
    WORD BITFLD_PCF                                : 2;
    WORD BITFLD_SAIF                               : 1;
    WORD BITFLD_SAF                                : 1;
    DWORD                                          : 21;
    WORD BITFLD_RA                                 : 1;
};

#define PRM                                (0x0001)
#define DAIF                               (0x0008)
#define PM                                 (0x0010)
#define DBF                                (0x0020)
#define PCF                                (0x00C0)
#define SAIF                               (0x0100)
#define SAF                                (0x0200)
#define RA                                 (0x80000000)


/*====================================================*/
struct __EMAC_MACR4_MII_ADDR_REG
/*====================================================*/
{
    WORD BITFLD_GB                                 : 1;
    WORD BITFLD_GW                                 : 1;
    WORD BITFLD_CR                                 : 3;
    WORD                                           : 1;
    WORD BITFLD_GR                                 : 5;
    WORD BITFLD_PA                                 : 5;
};

#define GB                                 (0x0001)
#define GW                                 (0x0002)
#define CR                                 (0x001C)
#define GR                                 (0x07C0)
#define PA                                 (0xF800)


/*====================================================*/
struct __EMAC_MACR6_FLOW_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_FCB_BPA                            : 1;
    WORD BITFLD_TFE                                : 1;
    WORD BITFLD_RFE                                : 1;
    WORD BITFLD_UP                                 : 1;
    WORD BITFLD_PLT                                : 2;
    WORD                                           : 1;
    WORD BITFLD_DZPQ                               : 1;
    WORD                                           : 8;
    WORD BITFLD_PT                                 : 16;
};

#define FCB_BPA                            (0x0001)
#define TFE                                (0x0002)
#define RFE                                (0x0004)
#define UP                                 (0x0008)
#define PLT                                (0x0030)
#define DZPQ                               (0x0080)
#define PT                                 (0xFFFF0000)


/*====================================================*/
struct __EMAC_MACR7_VLAN_TAG_REG
/*====================================================*/
{
    WORD BITFLD_VL                                 : 16;
    WORD BITFLD_ETV                                : 1;
};

#define VL                                 (0xFFFF)
#define ETV                                (0x10000)


/*====================================================*/
struct __EMAC_MACR8_CORE_VER_REG
/*====================================================*/
{
    WORD BITFLD_Version_low                        : 8;
    WORD BITFLD_Version_high                       : 8;
};

#define Version_low                        (0x00FF)
#define Version_high                       (0xFF00)


/*====================================================*/
struct __EMAC_MACR14_INT_REG
/*====================================================*/
{
    WORD BITFLD_RMI                                : 1;
    WORD                                           : 3;
    WORD BITFLD_MI                                 : 1;
    WORD BITFLD_MRI                                : 1;
    WORD BITFLD_MTI                                : 1;
    WORD BITFLD_MRCOI                              : 1;
    WORD                                           : 1;
    WORD BITFLD_TSI                                : 1;
};

#define RMI                                (0x0001)
#define MI                                 (0x0010)
#define MRI                                (0x0020)
#define MTI                                (0x0040)
#define MRCOI                              (0x0080)
#define TSI                                (0x0200)


/*====================================================*/
struct __EMAC_MACR15_INT_MSK_REG
/*====================================================*/
{
    WORD BITFLD_RMI                                : 1;
    WORD BITFLD_PLSI                               : 1;
    WORD BITFLD_PANCI                              : 1;
    WORD                                           : 6;
    WORD BITFLD_TSI                                : 1;
};

#define RMI                                (0x0001)
#define PLSI                               (0x0002)
#define PANCI                              (0x0004)
#define TSI                                (0x0200)


/*====================================================*/
struct __EMAC_DMAR0_BUS_MODE_REG
/*====================================================*/
{
    WORD BITFLD_SWR                                : 1;
    WORD BITFLD_DA                                 : 1;
    WORD BITFLD_DSL                                : 5;
    WORD                                           : 1;
    WORD BITFLD_PBL                                : 6;
    WORD BITFLD_RXPR                               : 2;
    WORD BITFLD_FB                                 : 1;
    WORD BITFLD_RPBL                               : 6;
    WORD BITFLD_USP                                : 1;
    WORD BITFLD_FPBL                               : 1;
    WORD BITFLD_AAL                                : 1;
};

#define SWR                                (0x0001)
#define DA                                 (0x0002)
#define DSL                                (0x007C)
#define PBL                                (0x3F00)
#define RXPR                               (0xC000)
#define FB                                 (0x10000)
#define RPBL                               (0x7E0000)
#define USP                                (0x800000)
#define FPBL                               (0x1000000)
#define AAL                                (0x2000000)


/*====================================================*/
struct __EMAC_DMAR5_STATUS_REG
/*====================================================*/
{
    WORD BITFLD_TXINT                              : 1;
    WORD BITFLD_TPS                                : 1;
    WORD BITFLD_TU                                 : 1;
    WORD BITFLD_TJT                                : 1;
    WORD BITFLD_OVF                                : 1;
    WORD BITFLD_UNF                                : 1;
    WORD BITFLD_RXINT                              : 1;
    WORD BITFLD_RU                                 : 1;
    WORD BITFLD_RPS                                : 1;
    WORD BITFLD_RWT                                : 1;
    WORD BITFLD_ETI                                : 1;
    WORD                                           : 2;
    WORD BITFLD_FBI                                : 1;
    WORD BITFLD_ERI                                : 1;
    WORD BITFLD_AIS                                : 1;
    WORD BITFLD_NIS                                : 1;
    WORD BITFLD_RS                                 : 3;
    WORD BITFLD_TS                                 : 3;
    WORD BITFLD_EB                                 : 3;
    WORD BITFLD_GLI                                : 1;
    WORD BITFLD_GMI                                : 1;
    WORD                                           : 1;
    WORD BITFLD_TTI                                : 1;
};

#define TXINT                              (0x0001)
#define TPS                                (0x0002)
#define TU                                 (0x0004)
#define TJT                                (0x0008)
#define OVF                                (0x0010)
#define UNF                                (0x0020)
#define RXINT                              (0x0040)
#define RU                                 (0x0080)
#define RPS                                (0x0100)
#define RWT                                (0x0200)
#define ETI                                (0x0400)
#define FBI                                (0x2000)
#define ERI                                (0x4000)
#define AIS                                (0x8000)
#define NIS                                (0x10000)
#define RS                                 (0xE0000)
#define TS                                 (0x700000)
#define EB                                 (0x3800000)
#define GLI                                (0x4000000)
#define GMI                                (0x8000000)
#define TTI                                (0x20000000)


/*====================================================*/
struct __EMAC_DMAR6_OPERATION_MODE_REG
/*====================================================*/
{
    WORD                                           : 1;
    WORD BITFLD_SR                                 : 1;
    WORD BITFLD_OSF                                : 1;
    WORD BITFLD_RTC                                : 2;
    WORD                                           : 1;
    WORD BITFLD_FUF                                : 1;
    WORD BITFLD_FEF                                : 1;
    WORD BITFLD_EFC                                : 1;
    WORD BITFLD_RFA                                : 2;
    WORD BITFLD_RFD                                : 2;
    WORD BITFLD_ST                                 : 1;
    WORD BITFLD_TTC                                : 3;
    WORD                                           : 3;
    WORD BITFLD_FTF                                : 1;
    WORD BITFLD_TSF                                : 1;
    WORD                                           : 2;
    WORD BITFLD_DFF                                : 1;
    WORD BITFLD_RSF                                : 1;
    WORD BITFLD_DT                                 : 1;
};

#define SR                                 (0x0002)
#define OSF                                (0x0004)
#define RTC                                (0x0018)
#define FUF                                (0x0040)
#define FEF                                (0x0080)
#define EFC                                (0x0100)
#define RFA                                (0x0600)
#define RFD                                (0x1800)
#define ST                                 (0x2000)
#define TTC                                (0x1C000)
#define FTF                                (0x100000)
#define TSF                                (0x200000)
#define DFF                                (0x1000000)
#define RSF                                (0x2000000)
#define DT                                 (0x4000000)


/*====================================================*/
struct __EMAC_DMAR7_INT_ENABLE_REG
/*====================================================*/
{
    WORD BITFLD_TIE                                : 1;
    WORD BITFLD_TSE                                : 1;
    WORD BITFLD_TUE                                : 1;
    WORD BITFLD_TJE                                : 1;
    WORD BITFLD_OVE                                : 1;
    WORD BITFLD_UNE                                : 1;
    WORD BITFLD_RIE                                : 1;
    WORD BITFLD_RUE                                : 1;
    WORD BITFLD_RSE                                : 1;
    WORD BITFLD_RWE                                : 1;
    WORD BITFLD_ETE                                : 1;
    WORD                                           : 2;
    WORD BITFLD_FBE                                : 1;
    WORD BITFLD_ERE                                : 1;
    WORD BITFLD_AIE                                : 1;
    WORD BITFLD_NIE                                : 1;
};

#define TIE                                (0x0001)
#define TSE                                (0x0002)
#define TUE                                (0x0004)
#define TJE                                (0x0008)
#define OVE                                (0x0010)
#define UNE                                (0x0020)
#define RIE                                (0x0040)
#define RUE                                (0x0080)
#define RSE                                (0x0100)
#define RWE                                (0x0200)
#define ETE                                (0x0400)
#define FBE                                (0x2000)
#define ERE                                (0x4000)
#define AIE                                (0x8000)
#define NIE                                (0x10000)


/*====================================================*/
struct __RESET_FREEZE_REG
/*====================================================*/
{
    WORD BITFLD_FRZ_DIP                            : 1;
    WORD BITFLD_FRZ_TIM0                           : 1;
    WORD BITFLD_FRZ_TIM1                           : 1;
    WORD BITFLD_FRZ_WDOG                           : 1;
    WORD BITFLD_FRZ_DMA0                           : 1;
    WORD BITFLD_FRZ_DMA1                           : 1;
    WORD BITFLD_FRZ_DMA2                           : 1;
    WORD BITFLD_FRZ_DMA3                           : 1;
};

#define FRZ_DIP                            (0x0001)
#define FRZ_TIM0                           (0x0002)
#define FRZ_TIM1                           (0x0004)
#define FRZ_WDOG                           (0x0008)
#define FRZ_DMA0                           (0x0010)
#define FRZ_DMA1                           (0x0020)
#define FRZ_DMA2                           (0x0040)
#define FRZ_DMA3                           (0x0080)


/*====================================================*/
struct __RESET_INT_PENDING_REG
/*====================================================*/
{
    WORD BITFLD_ACCESS_INT_PEND                    : 1;
    WORD BITFLD_KEYB_INT_PEND                      : 1;
    WORD BITFLD_EMAC_INT_PEND                      : 1;
    WORD BITFLD_CT_CLASSD_INT_PEND                 : 1;
    WORD BITFLD_UART_RI_INT_PEND                   : 1;
    WORD BITFLD_UART_TI_INT_PEND                   : 1;
    WORD BITFLD_SPI1_ADC_INT_PEND                  : 1;
    WORD BITFLD_TIM0_INT_PEND                      : 1;
    WORD BITFLD_TIM1_INT_PEND                      : 1;
    WORD BITFLD_CLK100_INT_PEND                    : 1;
    WORD BITFLD_DIP_INT_PEND                       : 1;
    WORD BITFLD_CRYPTO_INT_PEND                    : 1;
    WORD BITFLD_SPI2_INT_PEND                      : 1;
    WORD BITFLD_DSP1_INT_PEND                      : 1;
    WORD BITFLD_DSP2_INT_PEND                      : 1;
};

#define ACCESS_INT_PEND                    (0x0001)
#define KEYB_INT_PEND                      (0x0002)
#define EMAC_INT_PEND                      (0x0004)
#define CT_CLASSD_INT_PEND                 (0x0008)
#define UART_RI_INT_PEND                   (0x0010)
#define UART_TI_INT_PEND                   (0x0020)
#define SPI1_ADC_INT_PEND                  (0x0040)
#define TIM0_INT_PEND                      (0x0080)
#define TIM1_INT_PEND                      (0x0100)
#define CLK100_INT_PEND                    (0x0200)
#define DIP_INT_PEND                       (0x0400)
#define CRYPTO_INT_PEND                    (0x0800)
#define SPI2_INT_PEND                      (0x1000)
#define DSP1_INT_PEND                      (0x2000)
#define DSP2_INT_PEND                      (0x4000)


/*====================================================*/
struct __SET_FREEZE_REG
/*====================================================*/
{
    WORD BITFLD_FRZ_DIP                            : 1;
    WORD BITFLD_FRZ_TIM0                           : 1;
    WORD BITFLD_FRZ_TIM1                           : 1;
    WORD BITFLD_FRZ_WDOG                           : 1;
    WORD BITFLD_FRZ_DMA0                           : 1;
    WORD BITFLD_FRZ_DMA1                           : 1;
    WORD BITFLD_FRZ_DMA2                           : 1;
    WORD BITFLD_FRZ_DMA3                           : 1;
};

#define FRZ_DIP                            (0x0001)
#define FRZ_TIM0                           (0x0002)
#define FRZ_TIM1                           (0x0004)
#define FRZ_WDOG                           (0x0008)
#define FRZ_DMA0                           (0x0010)
#define FRZ_DMA1                           (0x0020)
#define FRZ_DMA2                           (0x0040)
#define FRZ_DMA3                           (0x0080)


/*====================================================*/
struct __SET_INT_PENDING_REG
/*====================================================*/
{
    WORD BITFLD_ACCESS_INT_PEND                    : 1;
    WORD BITFLD_KEYB_INT_PEND                      : 1;
    WORD BITFLD_EMAC_INT_PEND                      : 1;
    WORD BITFLD_CT_CLASSD_INT_PEND                 : 1;
    WORD BITFLD_UART_RI_INT_PEND                   : 1;
    WORD BITFLD_UART_TI_INT_PEND                   : 1;
    WORD BITFLD_SPI1_ADC_INT_PEND                  : 1;
    WORD BITFLD_TIM0_INT_PEND                      : 1;
    WORD BITFLD_TIM1_INT_PEND                      : 1;
    WORD BITFLD_CLK100_INT_PEND                    : 1;
    WORD BITFLD_DIP_INT_PEND                       : 1;
    WORD BITFLD_CRYPTO_INT_PEND                    : 1;
    WORD BITFLD_SPI2_INT_PEND                      : 1;
    WORD BITFLD_DSP1_INT_PEND                      : 1;
    WORD BITFLD_DSP2_INT_PEND                      : 1;
};

#define ACCESS_INT_PEND                    (0x0001)
#define KEYB_INT_PEND                      (0x0002)
#define EMAC_INT_PEND                      (0x0004)
#define CT_CLASSD_INT_PEND                 (0x0008)
#define UART_RI_INT_PEND                   (0x0010)
#define UART_TI_INT_PEND                   (0x0020)
#define SPI1_ADC_INT_PEND                  (0x0040)
#define TIM0_INT_PEND                      (0x0080)
#define TIM1_INT_PEND                      (0x0100)
#define CLK100_INT_PEND                    (0x0200)
#define DIP_INT_PEND                       (0x0400)
#define CRYPTO_INT_PEND                    (0x0800)
#define SPI2_INT_PEND                      (0x1000)
#define DSP1_INT_PEND                      (0x2000)
#define DSP2_INT_PEND                      (0x4000)


/*====================================================*/
struct __SPI1_CTRL_REG0
/*====================================================*/
{
    WORD BITFLD_SPI_ON                             : 1;
    WORD BITFLD_SPI_PHA                            : 1;
    WORD BITFLD_SPI_POL                            : 1;
    WORD BITFLD_SPI_CLK                            : 2;
    WORD BITFLD_SPI_DO                             : 1;
    WORD BITFLD_SPI_SMN                            : 1;
    WORD BITFLD_SPI_WORD                           : 2;
    WORD BITFLD_SPI_RST                            : 1;
    WORD BITFLD_SPI_FORCE_DO                       : 1;
    WORD BITFLD_SPI_TXH                            : 1;
    WORD BITFLD_SPI_DI                             : 1;
    WORD BITFLD_SPI_INT_BIT                        : 1;
    WORD BITFLD_SPI_MINT                           : 1;
    WORD BITFLD_SPI_EN_CTRL                        : 1;
};

#define SPI_ON                             (0x0001)
#define SPI_PHA                            (0x0002)
#define SPI_POL                            (0x0004)
#define SPI_CLK                            (0x0018)
#define SPI_DO                             (0x0020)
#define SPI_SMN                            (0x0040)
#define SPI_WORD                           (0x0180)
#define SPI_RST                            (0x0200)
#define SPI_FORCE_DO                       (0x0400)
#define SPI_TXH                            (0x0800)
#define SPI_DI                             (0x1000)
#define SPI_INT_BIT                        (0x2000)
#define SPI_MINT                           (0x4000)
#define SPI_EN_CTRL                        (0x8000)


/*====================================================*/
struct __SPI2_CTRL_REG0
/*====================================================*/
{
    WORD BITFLD_SPI_ON                             : 1;
    WORD BITFLD_SPI_PHA                            : 1;
    WORD BITFLD_SPI_POL                            : 1;
    WORD BITFLD_SPI_CLK                            : 2;
    WORD BITFLD_SPI_DO                             : 1;
    WORD BITFLD_SPI_SMN                            : 1;
    WORD BITFLD_SPI_WORD                           : 2;
    WORD BITFLD_SPI_RST                            : 1;
    WORD BITFLD_SPI_FORCE_DO                       : 1;
    WORD BITFLD_SPI_TXH                            : 1;
    WORD BITFLD_SPI_DI                             : 1;
    WORD BITFLD_SPI_INT_BIT                        : 1;
    WORD BITFLD_SPI_MINT                           : 1;
    WORD BITFLD_SPI_EN_CTRL                        : 1;
};

#define SPI_ON                             (0x0001)
#define SPI_PHA                            (0x0002)
#define SPI_POL                            (0x0004)
#define SPI_CLK                            (0x0018)
#define SPI_DO                             (0x0020)
#define SPI_SMN                            (0x0040)
#define SPI_WORD                           (0x0180)
#define SPI_RST                            (0x0200)
#define SPI_FORCE_DO                       (0x0400)
#define SPI_TXH                            (0x0800)
#define SPI_DI                             (0x1000)
#define SPI_INT_BIT                        (0x2000)
#define SPI_MINT                           (0x4000)
#define SPI_EN_CTRL                        (0x8000)


/*====================================================*/
struct __SPI1_CTRL_REG1
/*====================================================*/
{
    WORD BITFLD_SPI_FIFO_MODE                      : 1;
    WORD BITFLD_SPI_PRIORITY                       : 1;
    WORD BITFLD_SPI_BUSY                           : 1;
};

#define SPI_FIFO_MODE                      (0x0001)
#define SPI_PRIORITY                       (0x0004)
#define SPI_BUSY                           (0x0008)


/*====================================================*/
struct __SPI2_CTRL_REG1
/*====================================================*/
{
    WORD BITFLD_SPI_FIFO_MODE                      : 1;
    WORD BITFLD_SPI_PRIORITY                       : 1;
    WORD BITFLD_SPI_BUSY                           : 1;
};

#define SPI_FIFO_MODE                      (0x0001)
#define SPI_PRIORITY                       (0x0004)
#define SPI_BUSY                           (0x0008)


/*====================================================*/
struct __TEST_ENV_REG
/*====================================================*/
{
    WORD BITFLD_BOOT                               : 1;
    WORD BITFLD_AD1                                : 1;
    WORD BITFLD_AD2                                : 1;
    WORD BITFLD_AD3                                : 1;
    WORD BITFLD_ENV_SDI                            : 1;
    WORD BITFLD_ENV_REG7_5                         : 3;
};

#define BOOT                               (0x0001)
#define AD1                                (0x0002)
#define AD2                                (0x0004)
#define AD3                                (0x0008)
#define ENV_SDI                            (0x0010)
#define ENV_REG7_5                         (0x00E0)


/*====================================================*/
struct __TIMER_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_TIM0_CTRL                          : 1;
    WORD BITFLD_TIM1_CTRL                          : 1;
    WORD                                           : 2;
    WORD BITFLD_WDOG_CTRL                          : 1;
    WORD BITFLD_TIM1_MODE                          : 1;
    WORD BITFLD_CLK_DIV8                           : 1;
};

#define TIM0_CTRL                          (0x0001)
#define TIM1_CTRL                          (0x0002)
#define WDOG_CTRL                          (0x0010)
#define TIM1_MODE                          (0x0020)
#define CLK_DIV8                           (0x0040)


/*====================================================*/
struct __TONE_CTRL1_REG
/*====================================================*/
{
    WORD BITFLD_GATESRC1                           : 2;
    WORD BITFLD_CLKSRC1                            : 2;
    WORD BITFLD_TIMER_RELOAD1                      : 4;
    WORD BITFLD_CT1_INT                            : 1;
    WORD BITFLD_MCT1_INT                           : 1;
    WORD BITFLD_GATE_EDGE1                         : 1;
};

#define GATESRC1                           (0x0003)
#define CLKSRC1                            (0x000C)
#define TIMER_RELOAD1                      (0x00F0)
#define CT1_INT                            (0x0100)
#define MCT1_INT                           (0x0200)
#define GATE_EDGE1                         (0x0400)


/*====================================================*/
struct __TONE_CTRL2_REG
/*====================================================*/
{
    WORD BITFLD_GATESRC2                           : 2;
    WORD BITFLD_CLKSRC2                            : 2;
    WORD BITFLD_TIMER_RELOAD2                      : 4;
    WORD BITFLD_CT2_INT                            : 1;
    WORD BITFLD_MCT2_INT                           : 1;
    WORD BITFLD_GATE_EDGE2                         : 1;
};

#define GATESRC2                           (0x0003)
#define CLKSRC2                            (0x000C)
#define TIMER_RELOAD2                      (0x00F0)
#define CT2_INT                            (0x0100)
#define MCT2_INT                           (0x0200)
#define GATE_EDGE2                         (0x0400)


/*====================================================*/
struct __TRACE_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_TRACE_SIZE                         : 3;
    WORD BITFLD_TRACE_COND                         : 1;
    WORD BITFLD_EXT_EVENTS_ON                      : 1;
    WORD BITFLD_DIP_EVENTS_ON                      : 1;
    WORD BITFLD_DSP_EVENTS_ON                      : 1;
    WORD BITFLD_BUS_EVENTS_ON                      : 1;
    WORD BITFLD_D_TRACE_EV_ON                      : 1;
    WORD BITFLD_I_TRACE_EV_ON                      : 1;
    WORD BITFLD_D_TRACE_ON                         : 1;
    WORD BITFLD_I_TRACE_ON                         : 1;
    WORD BITFLD_TRACE_COND_EXT                     : 3;
    WORD BITFLD_TRACE_MODE                         : 1;
};

#define TRACE_SIZE                         (0x0007)
#define TRACE_COND                         (0x0008)
#define EXT_EVENTS_ON                      (0x0010)
#define DIP_EVENTS_ON                      (0x0020)
#define DSP_EVENTS_ON                      (0x0040)
#define BUS_EVENTS_ON                      (0x0080)
#define D_TRACE_EV_ON                      (0x0100)
#define I_TRACE_EV_ON                      (0x0200)
#define D_TRACE_ON                         (0x0400)
#define I_TRACE_ON                         (0x0800)
#define TRACE_COND_EXT                     (0x7000)
#define TRACE_MODE                         (0x8000)


/*====================================================*/
struct __TRACE_STATUS_REG
/*====================================================*/
{
    WORD BITFLD_TRACE_IDX                          : 11;
    WORD                                           : 1;
    WORD BITFLD_TRACE_TOUCH_TOGGLE                 : 1;
    WORD BITFLD_TRACE_TOUCH                        : 1;
    WORD BITFLD_TRACE_EOB                          : 1;
    WORD BITFLD_TRACE_BUSY                         : 1;
};

#define TRACE_IDX                          (0x07FF)
#define TRACE_TOUCH_TOGGLE                 (0x1000)
#define TRACE_TOUCH                        (0x2000)
#define TRACE_EOB                          (0x4000)
#define TRACE_BUSY                         (0x8000)


/*====================================================*/
struct __TRACE_START0_REG
/*====================================================*/
{
    WORD BITFLD_TRACE_START0                       : 15;
};

#define TRACE_START0                       (0x7FFF)


/*====================================================*/
struct __TRACE_LEN0_REG
/*====================================================*/
{
    WORD BITFLD_TRACE_LEN0                         : 15;
};

#define TRACE_LEN0                         (0x7FFF)


/*====================================================*/
struct __TRACE_START1_REG
/*====================================================*/
{
    WORD BITFLD_TRACE_START1                       : 15;
};

#define TRACE_START1                       (0x7FFF)


/*====================================================*/
struct __TRACE_LEN1_REG
/*====================================================*/
{
    WORD BITFLD_TRACE_LEN1                         : 15;
};

#define TRACE_LEN1                         (0x7FFF)


/*====================================================*/
struct __UART_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_UART_REN                           : 1;
    WORD BITFLD_UART_TEN                           : 1;
    WORD BITFLD_BAUDRATE                           : 3;
    WORD BITFLD_TI                                 : 1;
    WORD BITFLD_RI                                 : 1;
    WORD BITFLD_UART_MODE                          : 1;
    WORD BITFLD_IRDA_EN                            : 1;
    WORD BITFLD_INV_URX                            : 1;
    WORD BITFLD_INV_UTX                            : 1;
};

#define UART_REN                           (0x0001)
#define UART_TEN                           (0x0002)
#define BAUDRATE                           (0x001C)
#define TI                                 (0x0020)
#define RI                                 (0x0040)
#define UART_MODE                          (0x0080)
#define IRDA_EN                            (0x0100)
#define INV_URX                            (0x0200)
#define INV_UTX                            (0x0400)


/*====================================================*/
struct __UART_RX_TX_REG
/*====================================================*/
{
    WORD BITFLD_UART_DATA                          : 8;
};

#define UART_DATA                          (0x00FF)


/*====================================================*/
struct __UART_ERROR_REG
/*====================================================*/
{
    WORD BITFLD_PAR_STATUS                         : 1;
    WORD BITFLD_DMA_PARITY_ERROR                   : 1;
};

#define PAR_STATUS                         (0x0001)
#define DMA_PARITY_ERROR                   (0x0002)


/*====================================================*/
struct __WATCHDOG_REG
/*====================================================*/
{
    WORD BITFLD_WDOG_VAL                           : 8;
};

#define WDOG_VAL                           (0x00FF)




#ifdef __IAR_SYSTEMS_ICC__

/*
 * Extern declarations of all peripheral registers above 16 Mbyte
 * Note that such a pointer has to be declared somewhere,
 * before it can be used.in the application.
 */

extern const __data24 uint16* dsp_main_sync0_reg;
extern const __data24 uint16* dsp_main_sync1_reg; 
extern const __data24 uint16* dsp_main_cnt_reg;
extern const __data24 uint16* dsp_adcos;
extern const __data24 uint16* dsp_adcis;
extern const __data24 uint16* dsp_classd_reg;
extern const __data24 uint16* dsp_codec_mic_gain_reg;
extern const __data24 uint16* dsp_codec_out_reg;
extern const __data24 uint16* dsp_codec_in_reg;
extern const __data24 uint16* dsp_ram_out0_reg;
extern const __data24 uint16* dsp_ram_out1_reg;
extern const __data24 uint16* dsp_ram_out2_reg;
extern const __data24 uint16* dsp_ram_out3_reg;
extern const __data24 uint16* dsp_ram_in0_reg;
extern const __data24 uint16* dsp_ram_in1_reg;
extern const __data24 uint16* dsp_ram_in2_reg;
extern const __data24 uint16* dsp_ram_in3_reg;
extern const __data24 uint16* dsp_zcross1_out_reg;
extern const __data24 uint16* dsp_zcross2_out_reg;
extern const __data24 uint16* dsp_pcm_out0_reg;     
extern const __data24 uint16* dsp_pcm_out1_reg;    
extern const __data24 uint16* dsp_pcm_out2_reg;    
extern const __data24 uint16* dsp_pcm_out3_reg;    
extern const __data24 uint16* dsp_pcm_out4_reg;    
extern const __data24 uint16* dsp_pcm_out5_reg;    
extern const __data24 uint16* dsp_pcm_in0_reg;     
extern const __data24 uint16* dsp_pcm_in1_reg;    
extern const __data24 uint16* dsp_pcm_in2_reg;    
extern const __data24 uint16* dsp_pcm_in3_reg;    
extern const __data24 uint16* dsp_pcm_in4_reg;    
extern const __data24 uint16* dsp_pcm_in5_reg;    
extern const __data24 uint16* dsp_pcm_ctrl_reg;    
extern const __data24 uint16* dsp_phase_info_reg;    
extern const __data24 uint16* dsp_vqi_reg;    
extern const __data24 uint16* dsp_main_ctrl_reg;
extern const __data24 uint16* dsp_classd_buzzoff_reg;       
extern const __data24 uint16* dsp_ctrl_reg;       
extern const __data24 uint16* dsp_pc_reg;    
extern const __data24 uint16* dsp_pc_start_reg;    
extern const __data24 uint16* dsp_irq_start_reg;  
extern const __data24 uint16* dsp_int_reg;        
extern const __data24 uint16* dsp_int_mask_reg;   
extern const __data24 uint16* dsp_int_prio1_reg;   
extern const __data24 uint16* dsp_int_prio2_reg;   
extern const __data24 uint16* dsp_overflow_reg;   
extern const __data24 uint16* dbg_ireg;   
extern const __data24 uint16* dbg_inout_reg_lsw;   
extern const __data24 uint16* dbg_inout_reg_msw;   

#endif
/*
 * Missing bit field definitions for GPIO registers
 * This is not done in the datasheet to avoid very long tables
 * that contain no extra information.
 */


#define __GPIO_DATA_REG        __GPIO_BITFIELD_REG
#define __GPIO_DIR_REG         __GPIO_BITFIELD_REG
#define __GPIO_PUPD_REG        __GPIO_BITFIELD_REG
#define __GPIO_INT_EN_REG      __GPIO_BITFIELD_REG

/*====================================================*/
struct __GPIO_BITFIELD_REG
/*====================================================*/
{
    WORD BITFLD_GPIO_0                        : 1;
    WORD BITFLD_GPIO_1                        : 1;
    WORD BITFLD_GPIO_2                        : 1;
    WORD BITFLD_GPIO_3                        : 1;
    WORD BITFLD_GPIO_4                        : 1;
    WORD BITFLD_GPIO_5                        : 1;
    WORD BITFLD_GPIO_6                        : 1;
    WORD BITFLD_GPIO_7                        : 1;
    WORD BITFLD_GPIO_8                        : 1;
    WORD BITFLD_GPIO_9                        : 1;
    WORD BITFLD_GPIO_10                       : 1;
    WORD BITFLD_GPIO_11                       : 1;
    WORD BITFLD_GPIO_12                       : 1;
    WORD BITFLD_GPIO_13                       : 1;
    WORD BITFLD_GPIO_14                       : 1;
    WORD BITFLD_GPIO_15                       : 1;
};

#define GPIO_0                        (0x0001)
#define GPIO_1                        (0x0002)
#define GPIO_2                        (0x0004)
#define GPIO_3                        (0x0008)
#define GPIO_4                        (0x0010)
#define GPIO_5                        (0x0020)
#define GPIO_6                        (0x0040)
#define GPIO_7                        (0x0080)
#define GPIO_8                        (0x0100)
#define GPIO_9                        (0x0200)
#define GPIO_10                       (0x0400)
#define GPIO_11                       (0x0800)
#define GPIO_12                       (0x1000)
#define GPIO_13                       (0x2000)
#define GPIO_14                       (0x4000)
#define GPIO_15                       (0x8000)

#define SHIF(a) ((a)&0x0001?0: (a)&0x0002?1: (a)&0x0004?2: (a)&0x0008?3:\
                 (a)&0x0010?4: (a)&0x0020?5: (a)&0x0040?6: (a)&0x0080?7:\
                 (a)&0x0100?8: (a)&0x0200?9: (a)&0x0400?10:(a)&0x0800?11:\
                 (a)&0x1000?12:(a)&0x2000?13:(a)&0x4000?14: 15)

#define DSHIF(a)((a)&0x00000001?0: (a)&0x00000002?1: (a)&0x00000004?2: (a)&0x00000008?3:\
                 (a)&0x00000010?4: (a)&0x00000020?5: (a)&0x00000040?6: (a)&0x00000080?7:\
                 (a)&0x00000100?8: (a)&0x00000200?9: (a)&0x00000400?10:(a)&0x00000800?11:\
                 (a)&0x00001000?12:(a)&0x00002000?13:(a)&0x00004000?14:(a)&0x00008000?15:\
                 (a)&0x00010000?16:(a)&0x00020000?17:(a)&0x00040000?18:(a)&0x00080000?19:\
                 (a)&0x00100000?20:(a)&0x00200000?21:(a)&0x00400000?22:(a)&0x00800000?23:\
                 (a)&0x01000000?24:(a)&0x02000000?25:(a)&0x04000000?26:(a)&0x08000000?27:\
                 (a)&0x10000000?28:(a)&0x20000000?29:(a)&0x40000000?30: 31)

#ifdef IO_BY_FUNCTION_CALLS
    WORD fc_get_word(BYTE* a);
    WORD fc_get_bits(BYTE* a, WORD msk);
    void fc_set_word(BYTE* a, WORD v);
    void fc_set_bits(BYTE* a, WORD msh, WORD v);

    void fc_set_PSR_I_bit(void);
    void fc_reset_PSR_I_bit(void);
    void fc_enable_interrupt(void);
    void fc_disable_interrupt(void);
    void fc_wait(void);

    #define GetByte(a)  	fc_get_byte((BYTE*)a)
    #define GetWord(a)  	fc_get_word((BYTE*)a)
    #define GetBits(a,f)	fc_get_bits((BYTE*)a,f) 
    #define SetByte(a,d)	fc_set_byte((BYTE*)a,d)    
    #define SetWord(a,d)	fc_set_word((BYTE*)a,d)
    #define SetBits(a,f,d)	fc_set_bits((BYTE*)a,f,d)
#else
    #define GetByte(a)  	(* ( __far volatile BYTE*)(a) )
    #define GetWord(a)  	(* ( __far volatile WORD*)(a) )
    #define GetDword(a)  	(* ( __far volatile DWORD*)(a) )
    #define GetBits(a,f)	(( ( __far volatile struct __##a *)(a))->BITFLD_##f )
    #define GetDbits(a,f)	(( ( __far volatile struct __##a *)(a))->BITFLD_##f ) 
    #define SetByte(a,d)	(* ( __far volatile BYTE*)(a)=(d) )
    #define SetWord(a,d)	(* ( __far volatile WORD*)(a)=(d) )
    #define SetDword(a,d)	(* ( __far volatile DWORD*)(a)=(d) )
    #define SetBits(a,f,d)	( SetWord( a, (GetWord(a)&((WORD)~f)) | ((d)<<SHIF(f)) ))
    #define SetDbits(a,f,d)	( SetDword(a, (GetDword(a)&(~(DWORD)f)) | (((DWORD)d)<<DSHIF(f)) ))
#endif
#endif
