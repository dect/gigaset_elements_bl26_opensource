AS	= $(CROSS_COMPILE)as
LD	= $(CROSS_COMPILE)ld
CC	= $(CROSS_COMPILE)gcc
CPP	= $(CC) -E
AR	= $(CROSS_COMPILE)ar
NM	= $(CROSS_COMPILE)nm
STRIP	= $(CROSS_COMPILE)strip
OBJCOPY = $(CROSS_COMPILE)objcopy
OBJDUMP = $(CROSS_COMPILE)objdump
RANLIB	= $(CROSS_COMPILE)RANLIB

RELFLAGS= $(PLATFORM_RELFLAGS)
DBGFLAGS= #-DDEBUG
OPTFLAGS= -O2
LDSCRIPT := link.lds
CR16_ARCH_FLAGS = -mcr16cplus -mdata-model=far -mint32 

CPPFLAGS := $(DBGFLAGS) $(OPTFLAGS) $(RELFLAGS)	$(CR16_ARCH_FLAGS) -DTEXT_BASE=$(0x00000000)

CFLAGS   := $(CPPFLAGS) -Wall -Wstrict-prototypes -DREEF
AFLAGS   := -xassembler-with-cpp $(CPPFLAGS)

LIBGCC   := $(shell $(CC) -msoft-float -print-libgcc-file-name)
LDFLAGS  += -nostdlib -Bstatic -T $(LDSCRIPT) 

%.s:	%.S
	$(CPP) $(AFLAGS) -o $@ $<
%.o:	%.S
	$(CC) $(AFLAGS) -c -o $@ $<
%.o:	%.c
	$(CC) $(CFLAGS) -c -o $@ $<

