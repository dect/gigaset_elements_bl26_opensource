/****************************************************************************
* This file contains the RS232 bus API used by the HDLC protocol.
*
*
****************************************************************************/
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <termios.h>
#include <signal.h>
/****************************************************************************
*                     Definitions
****************************************************************************/
#define TRACE_STATE 1

/* Serial communication configuration */

#define BAUDRATE B9600//B115200            
//#define MODEMDEVICE "/dev/ttyUSB0"    // instead this use 4th parameter

#define DUMMY               0x00
#define IDLE                0x01
#define WAIT_ACK            0x02
#define WAIT_CRC			0x03
#define PROG_IDLE			0x04
#define PROG_WAIT_ACK		0x05
#define PROG_WAIT_CRC		0x06
#define CHANGE_UART_BPS		0x07
#define PROG_WAIT_COMP		0x08

#define SOH   0x01              
#define STX   0x02              
#define ACK   0x06              
#define NAK   0x15              

#define PROG_SOH 0x03
#define PROG_STX 0x05
#define PROG_ACK 0x07
#define PROG_NAK 0x16
#define PROG_COM 0x17


/****************************************************************************
*                     Enumerations/Type definitions/Structs
****************************************************************************/

/****************************************************************************
*                     Variable declarations
****************************************************************************/
int hComPortHandle = 0;
struct termios oldtio, newtio;//used for storing old and new serial port configurations

/****************************************************************************
*                      Function Prototypes
****************************************************************************/
/*Defined here*/

/****************************************************************************
*                      Function declarations
****************************************************************************/
int Uart_Open(char *serialPort)
{
	/* 
	  Open modem device for reading and writing and not as controlling tty
	  because we don't want to get killed if linenoise sends CTRL-C.
	*/
	hComPortHandle = open(serialPort, O_RDWR | O_NOCTTY /*| O_NONBLOCK*/);
	if (hComPortHandle <0) {
		perror(serialPort);
		return hComPortHandle; 
	}
	tcgetattr(hComPortHandle,&oldtio); /* save current serial port settings */
	memset(&newtio,0, sizeof(newtio)); /* clear struct for new port settings */


	/* 
	BAUDRATE: Set bps rate. You could also use cfsetispeed and cfsetospeed.
	CRTSCTS : output hardware flow control (only used if the cable has
			all necessary lines. See sect. 7 of Serial-HOWTO)
	CS8     : 8n1 (8bit,no parity,1 stopbit)
	CLOCAL  : local connection, no modem contol
	CREAD   : enable receiving characters
	*/
	// newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
	newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;

	/*
	IGNPAR  : ignore bytes with parity errors
	ICRNL   : map CR to NL (otherwise a CR input on the other computer
			will not terminate input)
	otherwise make device raw (no other input processing)
	*/
	newtio.c_iflag = IGNPAR;// | ICRNL;

	/*
	Raw output.
	*/
	newtio.c_oflag = 0;

	/* set input mode (non-canonical, no echo,...) */
	newtio.c_lflag = 0;//ICANON;

	/* 
	initialize all control characters 
	default values can be found in /usr/include/termios.h, and are given
	in the comments, but we don't need them here
	*/
	newtio.c_cc[VINTR]    = 0;     /* Ctrl-c */ 
	newtio.c_cc[VQUIT]    = 0;     /* Ctrl-\ */
	newtio.c_cc[VERASE]   = 0;     /* del */
	newtio.c_cc[VKILL]    = 0;     /* @ */
	newtio.c_cc[VEOF]     = 0;//4;//4;     /* Ctrl-d */
	newtio.c_cc[VTIME]    = 0;     /* inter-character timer unused */
	newtio.c_cc[VMIN]     = 1;     /* blocking read until 1 character arrives */
	newtio.c_cc[VSWTC]    = 0;     /* '\0' */
	newtio.c_cc[VSTART]   = 0;     /* Ctrl-q */ 
	newtio.c_cc[VSTOP]    = 0;     /* Ctrl-s */
	newtio.c_cc[VSUSP]    = 0;     /* Ctrl-z */
	newtio.c_cc[VEOL]     = 0;     /* '\0' */
	newtio.c_cc[VREPRINT] = 0;     /* Ctrl-r */
	newtio.c_cc[VDISCARD] = 0;     /* Ctrl-u */
	newtio.c_cc[VWERASE]  = 0;     /* Ctrl-w */
	newtio.c_cc[VLNEXT]   = 0;     /* Ctrl-v */
	newtio.c_cc[VEOL2]    = 0;     /* '\0' */
	/* 
	now clean the modem line and activate the settings for the port
	*/
	tcflush(hComPortHandle, TCIFLUSH);
	tcsetattr(hComPortHandle,TCSANOW,&newtio);
    

	return 0;
}

void Uart_Flush(void)
{
  	tcflush(hComPortHandle, TCIFLUSH);
}


int Uart_SetBaudRate(speed_t speed)
{
  cfsetispeed(&newtio, speed);
  cfsetospeed(&newtio, speed);
  if (tcsetattr(hComPortHandle, TCSADRAIN, &newtio))
  {
    perror("UartSetBaudRate:tcsetattr");
    return errno;
  }
  Uart_Flush();
  return 0;
}



/****************************************************************************
*  FUNCTION: 	Rs232_Read(unsigned char *Buf, rsuint16 Len)
*   INPUTS  :         Buf:  specifies the address of the buffer where to data that 
*                       	should be read from the bus. 
*  RETURNS :  	-1, on Error
*			The number of bytes read, on success
*  DESCRIPTION: This function is used for reading from the RS232 bus. NOTE: IT IS EXPECTED TO BLOCK !!
****************************************************************************/
int Uart_Read_byte_blocking(unsigned char *Buf)
{
	int result;
#ifdef TRACE_STATE					
	fprintf(stderr,"Uart RX");					
#endif					
	
	result = read( hComPortHandle, Buf, 1);
#ifdef TRACE_STATE					
	fprintf(stderr,"[%d]=0x%02X,...\n",result, *Buf);					
#endif					
	return result;
}	

/****************************************************************************
*  FUNCTION: 	Rs232_Write(unsigned char *Buf, rsuint16 Len)
*   INPUTS  :         Buf:  specifies the address of the buffer where to data that 
*                       	should be written to the bus. 
*  RETURNS :  	-1, on Error
*			The number of bytes written, on success
*  DESCRIPTION: This function is used for writing to the RS232 bus. NOTE: IT IS EXPECTED TO BLOCK !!
****************************************************************************/
int Uart_Write_byte_blocking(unsigned char *Buf, unsigned Len)
{
	int result;
	usleep(10000);
#ifdef TRACE_STATE					
	fprintf(stderr,"Uart TX[%d]=0x%02X,...\n",Len, *Buf);					
#endif					
	result = write( hComPortHandle, Buf, Len);
	return result;
}			


int Uart_Close(void)
{
	if (hComPortHandle != 0)
	{
		tcsetattr( hComPortHandle, TCSANOW, &oldtio ) ;
		close(hComPortHandle);
	}
	return 0;
}

unsigned char ProgrHeader[3];
unsigned char ImageHeader[5];
volatile unsigned char RecChar;
unsigned char TxChar;
unsigned char *ImageBuff = NULL;
unsigned char *ProgrCode = NULL;
FILE* fp = NULL;


static void die_gracefully( int sig )
{
	
	if (ImageBuff != NULL)
		free(ImageBuff);
	if (ProgrCode != NULL)
		free(ProgrCode);
			
	Uart_Close();

	fprintf( stderr, "exiting due to signal %d...\n", sig ) ;

	exit( EXIT_FAILURE ) ;
}

int main(int argc, char** argv)
{

	unsigned short state;
	unsigned char ProgrCrc,ImageCrc;	
	unsigned long i,ProgrLen,ImageLen;


	
	
	if (argc != 4 )
	{
		fprintf(stderr,"Wrong number of arguments.\n\nUsage: uartfp ProgrammerFile BinaryImageFile SerialPort\n");
		exit (EXIT_SUCCESS);
	}
	
	signal( SIGINT, die_gracefully ) ;
	signal( SIGTERM, die_gracefully ) ;
	signal( SIGABRT, die_gracefully ) ;


/* Read Flash Programmer binary file */

	fp = fopen(argv[1],"rb");
	if (!fp) {
		fprintf (stderr, "Can't open %s: %s\n",argv[1], strerror(errno));
		exit (EXIT_FAILURE);
	}
	
	fseek (fp, 0, SEEK_END);
    ProgrLen = ftell (fp);
    fseek (fp, 0, SEEK_SET);
	fprintf(stderr,"Programmer File Size %d\n",(int)ProgrLen);
	
	ProgrCode = malloc(ProgrLen);
	fread(ProgrCode,1,ProgrLen,fp);
	fclose(fp);

	ProgrCrc = 0;
	for (i=0;i<ProgrLen;i++)
		ProgrCrc ^=ProgrCode[i];
		
	fprintf(stderr,"Programmer File CRC 0x%02X\n",ProgrCrc);	

	ProgrHeader[0]=SOH;
	ProgrHeader[1]=ProgrLen;
	ProgrHeader[2]=ProgrLen>>8;
	
	
/* Read binary image file that is to program to the flash */
	
	fp = fopen(argv[2],"rb");
	if (!fp) {
		fprintf (stderr, "Can't open %s: %s\n",argv[1], strerror(errno));
		free(ProgrCode);
		exit (EXIT_FAILURE);
	}
	
	fseek (fp, 0, SEEK_END);
    ImageLen = ftell (fp);
    fseek (fp, 0, SEEK_SET);
	fprintf(stderr,"Image File Size %d\n",(int)ImageLen);
	
	ImageBuff = malloc(ImageLen);
	fread(ImageBuff,1,ImageLen,fp);
	fclose(fp);
	
	ImageCrc = 0;
	for (i=0;i<ImageLen;i++)
		ImageCrc ^=ImageBuff[i];
		
	fprintf(stderr,"Image File CRC 0x%02X\n",ImageCrc);	
	
	ImageHeader[0]=PROG_SOH;
	ImageHeader[1]=ImageLen;
	ImageHeader[2]=ImageLen>>8;	
	ImageHeader[3]=ImageLen>>16;	
	ImageHeader[4]=ImageLen>>24;	
	

	Uart_Open(argv[3]);
	state = IDLE;
	
	while(1)
	{
#ifdef TRACE_STATE					
	    fprintf(stderr,"\n--- ");
	    switch (state)
	    {
		case DUMMY:		fprintf(stderr, "DUMMY"); 			break;
		case IDLE:		fprintf(stderr, "IDLE"); 			break;
		case WAIT_ACK:		fprintf(stderr, "WAIT_ACK"); 			break;
		case WAIT_CRC:		fprintf(stderr, "WAIT_CRC"); 			break;
		case PROG_IDLE:		fprintf(stderr, "PROG_IDLE"); 			break;
		case PROG_WAIT_ACK:	fprintf(stderr, "PROG_WAIT_ACK"); 		break;
		case PROG_WAIT_CRC:	fprintf(stderr, "PROG_WAIT_CRC"); 		break;
		case CHANGE_UART_BPS:	fprintf(stderr, "CHANGE_UART_BPS"); 		break;
		case PROG_WAIT_COMP:	fprintf(stderr, "PROG_WAIT_COMP"); 		break;
		default:		fprintf(stderr, "DUMMY"); 			break;
	    }
	    fprintf(stderr," ---\n");	    
#endif					
	
		switch (state)
		{
			case IDLE:
			{
				if ( Uart_Read_byte_blocking((unsigned char*)&RecChar) > 0)
				{
#ifdef TRACE_STATE					
					fprintf(stderr,"state IDLE rec: 0x%02X\n",RecChar);					
#endif					
					if (RecChar == STX)
					{
						Uart_Write_byte_blocking(ProgrHeader,3);
						fprintf(stderr,"Downloading programmer & image...\n");					
						state = WAIT_ACK;
						break;
					}
				}
				break;
			}
			case WAIT_ACK:
			{
				if ( Uart_Read_byte_blocking((unsigned char*)&RecChar) > 0)
				{
#ifdef TRACE_STATE					
					fprintf(stderr,"state WAIT_ACK rec: 0x%02X\n",RecChar);					
#endif					
					if (RecChar == ACK)
					{
						printf("ACK received, flashing loader (size %ld)...\n",ProgrLen);
						Uart_Write_byte_blocking(ProgrCode,ProgrLen);
						state = WAIT_CRC;
						break;
					}
					if (RecChar == PROG_STX)
					{
					    printf("Already in loader mode, start programming...\n");
					    state = PROG_IDLE;
					    break;
					}
				}
				state = IDLE;
				break;
			}
			case WAIT_CRC:
			{
				if ( Uart_Read_byte_blocking((unsigned char*)&RecChar) > 0)
				{
					printf("CRC expected %02X, received %02X ", ProgrCrc, RecChar);
					if (RecChar == ProgrCrc)
					{
						printf("CRC OK\n");
						TxChar = ACK;
						Uart_Write_byte_blocking(&TxChar,1);
						state = CHANGE_UART_BPS;
						break;
					}
					printf("CRC NOT OK !!!\n");
				}
				state = IDLE;
				break;
			}
			case CHANGE_UART_BPS:
			{
				Uart_Flush();
				usleep(200000); // 200 ms delay for USB adapters, to transmit all data
				Uart_SetBaudRate(B115200);
				state = PROG_IDLE;
				break;
			}
			case PROG_IDLE:
			{
				if ( Uart_Read_byte_blocking((unsigned char*)&RecChar) > 0)
				{
					if (RecChar == PROG_STX)
					{
						printf("Writing image header...\n");
						Uart_Write_byte_blocking(ImageHeader,5);
						state = PROG_WAIT_ACK;
						break;
					}
				}
				break;
			}
			case PROG_WAIT_ACK:
			{
				if ( Uart_Read_byte_blocking((unsigned char*)&RecChar) > 0)
				{
					if (RecChar == PROG_ACK)
					{
						printf("Writing image (size %ld)...\n", ImageLen);
						Uart_Write_byte_blocking(ImageBuff,ImageLen);
						state = PROG_WAIT_CRC;
						break;
					}
				}
				state = PROG_IDLE;
				break;
			}
			case PROG_WAIT_CRC:
			{
				if ( Uart_Read_byte_blocking((unsigned char*)&RecChar) > 0)
				{
					printf("PROG CRC expected %02X, received %02X ", ImageCrc, RecChar);
					if (RecChar == ImageCrc)
					{
						printf("CRC OK\n");
						TxChar = PROG_ACK;
						Uart_Write_byte_blocking(&TxChar,1);
						fprintf(stderr,"Programming image ...\n");					
						state = PROG_WAIT_COMP;
						break;
					}
					printf("CRC NOT OK !!!\n");
				}
				state = PROG_IDLE;
				break;
			}
			case PROG_WAIT_COMP:
			{
				if ( Uart_Read_byte_blocking((unsigned char*)&RecChar) > 0)
				{
					if (RecChar == PROG_COM)
					{	
							fprintf(stderr,"Flash programmed successfully\n");
							free(ProgrCode);							
							free(ImageBuff);
							Uart_Close();
							exit (EXIT_SUCCESS);
					}
				}
				state = PROG_IDLE;
				break;
			}
		}
	}
	
	free(ProgrCode);							
	free(ImageBuff);
	Uart_Close();
	exit (EXIT_SUCCESS);
}

//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:50.272218205
//@@ -25,7 +25,7 @@
// /* Serial communication configuration */
// 
// #define BAUDRATE B9600//B115200            
//-#define MODEMDEVICE "/dev/ttyUSB0"
//+//#define MODEMDEVICE "/dev/ttyUSB0"    // instead this use 4th parameter
// 
// #define DUMMY               0x00
// #define IDLE                0x01
//@@ -67,15 +67,15 @@
// /****************************************************************************
// *                      Function declarations
// ****************************************************************************/
//-int Uart_Open(void)
//+int Uart_Open(char *serialPort)
// {
// 	/* 
// 	  Open modem device for reading and writing and not as controlling tty
// 	  because we don't want to get killed if linenoise sends CTRL-C.
// 	*/
//-	hComPortHandle = open(MODEMDEVICE, O_RDWR | O_NOCTTY /*| O_NONBLOCK*/); 
//+	hComPortHandle = open(serialPort, O_RDWR | O_NOCTTY /*| O_NONBLOCK*/);
// 	if (hComPortHandle <0) {
//-		perror(MODEMDEVICE); 
//+		perror(serialPort);
// 		return hComPortHandle; 
// 	}
// 	tcgetattr(hComPortHandle,&oldtio); /* save current serial port settings */
//@@ -173,8 +173,14 @@
// int Uart_Read_byte_blocking(unsigned char *Buf)
// {
// 	int result;
//+#ifdef TRACE_STATE					
//+	fprintf(stderr,"Uart RX");					
//+#endif					
// 	
// 	result = read( hComPortHandle, Buf, 1);
//+#ifdef TRACE_STATE					
//+	fprintf(stderr,"[%d]=0x%02X,...\n",result, *Buf);					
//+#endif					
// 	return result;
// }	
// 
//@@ -189,10 +195,15 @@
// int Uart_Write_byte_blocking(unsigned char *Buf, unsigned Len)
// {
// 	int result;
//-	
//+	usleep(10000);
//+#ifdef TRACE_STATE					
//+	fprintf(stderr,"Uart TX[%d]=0x%02X,...\n",Len, *Buf);					
//+#endif					
// 	result = write( hComPortHandle, Buf, Len);
// 	return result;
// }			
//+
//+
// int Uart_Close(void)
// {
// 	if (hComPortHandle != 0)
//@@ -211,6 +222,7 @@
// unsigned char *ProgrCode = NULL;
// FILE* fp = NULL;
// 
//+
// static void die_gracefully( int sig )
// {
// 	
//@@ -236,9 +248,9 @@
// 
// 	
// 	
//-	if (argc != 3 )
//+	if (argc != 4 )
// 	{
//-		fprintf(stderr,"Wrong number of arguments.\n\nUsage: uartfp ProgrammerFile BinaryImageFile\n");
//+		fprintf(stderr,"Wrong number of arguments.\n\nUsage: uartfp ProgrammerFile BinaryImageFile SerialPort\n");
// 		exit (EXIT_SUCCESS);
// 	}
// 	
//@@ -306,11 +318,29 @@
// 	ImageHeader[4]=ImageLen>>24;	
// 	
// 
//-	Uart_Open();
//+	Uart_Open(argv[3]);
// 	state = IDLE;
// 	
// 	while(1)
// 	{
//+#ifdef TRACE_STATE					
//+	    fprintf(stderr,"\n--- ");
//+	    switch (state)
//+	    {
//+		case DUMMY:		fprintf(stderr, "DUMMY"); 			break;
//+		case IDLE:		fprintf(stderr, "IDLE"); 			break;
//+		case WAIT_ACK:		fprintf(stderr, "WAIT_ACK"); 			break;
//+		case WAIT_CRC:		fprintf(stderr, "WAIT_CRC"); 			break;
//+		case PROG_IDLE:		fprintf(stderr, "PROG_IDLE"); 			break;
//+		case PROG_WAIT_ACK:	fprintf(stderr, "PROG_WAIT_ACK"); 		break;
//+		case PROG_WAIT_CRC:	fprintf(stderr, "PROG_WAIT_CRC"); 		break;
//+		case CHANGE_UART_BPS:	fprintf(stderr, "CHANGE_UART_BPS"); 		break;
//+		case PROG_WAIT_COMP:	fprintf(stderr, "PROG_WAIT_COMP"); 		break;
//+		default:		fprintf(stderr, "DUMMY"); 			break;
//+	    }
//+	    fprintf(stderr," ---\n");	    
//+#endif					
//+	
// 		switch (state)
// 		{
// 			case IDLE:
//@@ -339,7 +369,7 @@
// #endif					
// 					if (RecChar == ACK)
// 					{
//-						printf("ACK received, flashing loader...\n");
//+						printf("ACK received, flashing loader (size %ld)...\n",ProgrLen);
// 						Uart_Write_byte_blocking(ProgrCode,ProgrLen);
// 						state = WAIT_CRC;
// 						break;
//@@ -358,9 +388,7 @@
// 			{
// 				if ( Uart_Read_byte_blocking((unsigned char*)&RecChar) > 0)
// 				{
//-#ifdef TRACE_STATE					
//-					fprintf(stderr,"state WAIT_CRC rec: 0x%02X\n",RecChar);					
//-#endif					
//+					printf("CRC expected %02X, received %02X ", ProgrCrc, RecChar);
// 					if (RecChar == ProgrCrc)
// 					{
// 						printf("CRC OK\n");
//@@ -369,18 +397,16 @@
// 						state = CHANGE_UART_BPS;
// 						break;
// 					}
//+					printf("CRC NOT OK !!!\n");
// 				}
// 				state = IDLE;
// 				break;
// 			}
// 			case CHANGE_UART_BPS:
// 			{
//-#ifdef TRACE_STATE					
//-					fprintf(stderr,"state CHANGE_UART_BPS \n");					
//-#endif					
// 				Uart_Flush();
//-				/* TODO niziak: temporary disabled */
//-				//Uart_SetBaudRate(B115200);
//+				usleep(200000); // 200 ms delay for USB adapters, to transmit all data
//+				Uart_SetBaudRate(B115200);
// 				state = PROG_IDLE;
// 				break;
// 			}
//@@ -388,9 +414,6 @@
// 			{
// 				if ( Uart_Read_byte_blocking((unsigned char*)&RecChar) > 0)
// 				{
//-#ifdef TRACE_STATE					
//-					fprintf(stderr,"state PROG_IDLE rec: 0x%02X\n",RecChar);					
//-#endif					
// 					if (RecChar == PROG_STX)
// 					{
// 						printf("Writing image header...\n");
//@@ -405,12 +428,9 @@
// 			{
// 				if ( Uart_Read_byte_blocking((unsigned char*)&RecChar) > 0)
// 				{
//-#ifdef TRACE_STATE					
//-					fprintf(stderr,"state PROG_WAIT_ACK rec: 0x%02X\n",RecChar);					
//-#endif										
// 					if (RecChar == PROG_ACK)
// 					{
//-						printf("Writing image...\n");
//+						printf("Writing image (size %ld)...\n", ImageLen);
// 						Uart_Write_byte_blocking(ImageBuff,ImageLen);
// 						state = PROG_WAIT_CRC;
// 						break;
//@@ -423,17 +443,17 @@
// 			{
// 				if ( Uart_Read_byte_blocking((unsigned char*)&RecChar) > 0)
// 				{
//-#ifdef TRACE_STATE					
//-					fprintf(stderr,"state PROG_WAIT_CRC rec: 0x%02X\n",RecChar);					
//-#endif										
//+					printf("PROG CRC expected %02X, received %02X ", ImageCrc, RecChar);
// 					if (RecChar == ImageCrc)
// 					{
//+						printf("CRC OK\n");
// 						TxChar = PROG_ACK;
// 						Uart_Write_byte_blocking(&TxChar,1);
// 						fprintf(stderr,"Programming image ...\n");					
// 						state = PROG_WAIT_COMP;
// 						break;
// 					}
//+					printf("CRC NOT OK !!!\n");
// 				}
// 				state = PROG_IDLE;
// 				break;
//@@ -442,9 +462,6 @@
// 			{
// 				if ( Uart_Read_byte_blocking((unsigned char*)&RecChar) > 0)
// 				{
//-#ifdef TRACE_STATE					
//-					fprintf(stderr,"state PROG_WAIT_COMP rec: 0x%02X\n",RecChar);					
//-#endif										
// 					if (RecChar == PROG_COM)
// 					{	
// 							fprintf(stderr,"Flash programmed successfully\n");
