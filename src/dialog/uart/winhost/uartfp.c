/*
 * uartfp.c
 *
 *  Created on: 5 Jan 2010
 *      Author: maniotis
 */
/****************************************************************************
* This file contains the RS232 bus API used by the HDLC protocol.
*
*
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <windows.h>


/****************************************************************************
*                     Definitions
****************************************************************************/
#define TRACE_STATE

/* Serial communication configuration */


#define DUMMY               0x00
#define IDLE                0x01
#define WAIT_ACK            0x02
#define WAIT_CRC			0x03
#define PROG_IDLE			0x04
#define PROG_WAIT_ACK		0x05
#define PROG_WAIT_CRC		0x06
#define CHANGE_UART_BPS		0x07
#define PROG_WAIT_COMP		0x08

#define SOH   0x01
#define STX   0x02
#define ACK   0x06
#define NAK   0x15

#define PROG_SOH 0x03
#define PROG_STX 0x05
#define PROG_ACK 0x07
#define PROG_NAK 0x16
#define PROG_COM 0x17


HANDLE Cport[32];

char comports[32][10]={"\\\\.\\COM1",  "\\\\.\\COM2",  "\\\\.\\COM3",  "\\\\.\\COM4",
                       "\\\\.\\COM5",  "\\\\.\\COM6",  "\\\\.\\COM7",  "\\\\.\\COM8",
                       "\\\\.\\COM9",  "\\\\.\\COM10", "\\\\.\\COM11", "\\\\.\\COM12",
                       "\\\\.\\COM13", "\\\\.\\COM14", "\\\\.\\COM15", "\\\\.\\COM16",
                       "\\\\.\\COM17", "\\\\.\\COM18", "\\\\.\\COM19", "\\\\.\\COM20",
                       "\\\\.\\COM21", "\\\\.\\COM22", "\\\\.\\COM23", "\\\\.\\COM24",
                       "\\\\.\\COM25", "\\\\.\\COM26", "\\\\.\\COM27", "\\\\.\\COM28",
                       "\\\\.\\COM29", "\\\\.\\COM30", "\\\\.\\COM31", "\\\\.\\COM32"};

char baudr[64];

int comportnum;

int OpenComport(int comport_number, int baudrate)
{
	DCB port_settings;
	COMMTIMEOUTS Cptimeouts;


  if((comport_number>31)||(comport_number<0))
  {
    printf("illegal comport number\n");
    return(1);
  }

  switch(baudrate)
  {
    case     110 : strcpy(baudr, "baud=110 data=8 parity=N stop=1");
                   break;
    case     300 : strcpy(baudr, "baud=300 data=8 parity=N stop=1");
                   break;
    case     600 : strcpy(baudr, "baud=600 data=8 parity=N stop=1");
                   break;
    case    1200 : strcpy(baudr, "baud=1200 data=8 parity=N stop=1");
                   break;
    case    2400 : strcpy(baudr, "baud=2400 data=8 parity=N stop=1");
                   break;
    case    4800 : strcpy(baudr, "baud=4800 data=8 parity=N stop=1");
                   break;
    case    9600 : strcpy(baudr, "baud=9600 data=8 parity=N stop=1");
                   break;
    case   19200 : strcpy(baudr, "baud=19200 data=8 parity=N stop=1");
                   break;
    case   38400 : strcpy(baudr, "baud=38400 data=8 parity=N stop=1");
                   break;
    case   57600 : strcpy(baudr, "baud=57600 data=8 parity=N stop=1");
                   break;
    case  115200 : strcpy(baudr, "baud=115200 data=8 parity=N stop=1");
                   break;
    case  128000 : strcpy(baudr, "baud=128000 data=8 parity=N stop=1");
                   break;
    case  256000 : strcpy(baudr, "baud=256000 data=8 parity=N stop=1");
                   break;
    default      : printf("invalid baudrate\n");
                   return(1);
                   break;
  }

  Cport[comport_number] = CreateFileA(comports[comport_number],
                      GENERIC_READ|GENERIC_WRITE,
                      0,                          /* no share  */
                      NULL,                       /* no security */
                      OPEN_EXISTING,
                      0,                          /* no threads */
                      NULL);                      /* no templates */

  if(Cport[comport_number]==INVALID_HANDLE_VALUE)
  {
    printf("unable to open comport\n");
    return(1);
  }
   
  
  memset(&port_settings, 0, sizeof(port_settings));  /* clear the new struct  */
  port_settings.DCBlength = sizeof(port_settings);

  if(!BuildCommDCBA(baudr, &port_settings))
  {
    printf("unable to set comport dcb settings\n");
    CloseHandle(Cport[comport_number]);
    return(1);
  }

  if(!SetCommState(Cport[comport_number], &port_settings))
  {
    printf("unable to set comport cfg settings\n");
    CloseHandle(Cport[comport_number]);
    return(1);
  }

  

  Cptimeouts.ReadIntervalTimeout         = MAXDWORD;
  Cptimeouts.ReadTotalTimeoutMultiplier  = MAXDWORD;
  Cptimeouts.ReadTotalTimeoutConstant    = 100000;
  Cptimeouts.WriteTotalTimeoutMultiplier = 0;
  Cptimeouts.WriteTotalTimeoutConstant   = 0;

  if(!SetCommTimeouts(Cport[comport_number], &Cptimeouts))
  {
    printf("unable to set comport time-out settings\n");
    CloseHandle(Cport[comport_number]);
    return(1);
  }

  return(0);
}

int ComportChangeBaudrate(int comport_number, int baudrate)
{
	DCB port_settings;
  if((comport_number>31)||(comport_number<0))
  {
    printf("illegal comport number\n");
    return(1);
  }

  switch(baudrate)
  {
    case     110 : strcpy(baudr, "baud=110 data=8 parity=N stop=1");
                   break;
    case     300 : strcpy(baudr, "baud=300 data=8 parity=N stop=1");
                   break;
    case     600 : strcpy(baudr, "baud=600 data=8 parity=N stop=1");
                   break;
    case    1200 : strcpy(baudr, "baud=1200 data=8 parity=N stop=1");
                   break;
    case    2400 : strcpy(baudr, "baud=2400 data=8 parity=N stop=1");
                   break;
    case    4800 : strcpy(baudr, "baud=4800 data=8 parity=N stop=1");
                   break;
    case    9600 : strcpy(baudr, "baud=9600 data=8 parity=N stop=1");
                   break;
    case   19200 : strcpy(baudr, "baud=19200 data=8 parity=N stop=1");
                   break;
    case   38400 : strcpy(baudr, "baud=38400 data=8 parity=N stop=1");
                   break;
    case   57600 : strcpy(baudr, "baud=57600 data=8 parity=N stop=1");
                   break;
    case  115200 : strcpy(baudr, "baud=115200 data=8 parity=N stop=1");
                   break;
    case  128000 : strcpy(baudr, "baud=128000 data=8 parity=N stop=1");
                   break;
    case  256000 : strcpy(baudr, "baud=256000 data=8 parity=N stop=1");
                   break;
    default      : printf("invalid baudrate\n");
                   return(1);
                   break;
  }

  if(Cport[comport_number]==INVALID_HANDLE_VALUE)
  {
    printf("unable to open comport\n");
    return(1);
  }

  
  memset(&port_settings, 0, sizeof(port_settings));  /* clear the new struct  */
  port_settings.DCBlength = sizeof(port_settings);

  if(!BuildCommDCBA(baudr, &port_settings))
  {
    printf("unable to set comport dcb settings\n");
    CloseHandle(Cport[comport_number]);
    return(1);
  }

  if(!SetCommState(Cport[comport_number], &port_settings))
  {
    printf("unable to set comport cfg settings\n");
    CloseHandle(Cport[comport_number]);
    return(1);
  }
  return(0);
}



int PollComport(int comport_number, unsigned char *buf, int size)
{
  int n;

  if(size>4096)  size = 4096;


/* added the void pointer cast, otherwise gcc will complain about */
/* "warning: dereferencing type-punned pointer will break strict aliasing rules" */

  ReadFile(Cport[comport_number], buf, size, (LPDWORD)((void *)&n), NULL);
  return(n);
}


int SendByte(int comport_number, unsigned char byte)
{
  int n;

  WriteFile(Cport[comport_number], &byte, 1, (LPDWORD)((void *)&n), NULL);

  if(n<0)  return(1);

  return(0);
}


int SendBuf(int comport_number, unsigned char *buf, int size)
{
  int n;

  if(WriteFile(Cport[comport_number], buf, size, (LPDWORD)((void *)&n), NULL))
  {
    return(n);
  }

  return(-1);
}


void CloseComport(int comport_number)
{
  CloseHandle(Cport[comport_number]);
}



unsigned char ProgrHeader[3];
unsigned char ImageHeader[5];
volatile unsigned char RecChar;
unsigned char TxChar;
unsigned char *ImageBuff = NULL;
unsigned char *ProgrCode = NULL;
FILE* fp = NULL;

static void die_gracefully( int sig )
{

	if (ImageBuff != NULL)
		free(ImageBuff);
	if (ProgrCode != NULL)
		free(ProgrCode);

	CloseComport(comportnum);

	printf("exiting due to signal %d...\n", sig ) ;

	exit( EXIT_FAILURE ) ;
}


int main(int argc, char** argv)
{

	unsigned short state;
	unsigned char ProgrCrc,ImageCrc;
	unsigned long i,ProgrLen,ImageLen;
	unsigned char *tempptr;

	if (argc != 4 )
	{
		printf("Wrong number of arguments.\n\nUsage: uartfp ComPort ProgrammerFile BinaryImageFile\n");
		exit (EXIT_SUCCESS);
	}

	signal( SIGINT, die_gracefully ) ;
	signal( SIGTERM, die_gracefully ) ;
	signal( SIGABRT, die_gracefully ) ;

	comportnum=strtoul(argv[1],(char**)&tempptr,10);
//comportnum=31;
	if ((comportnum < 1) || (comportnum>31))
	{
		printf("Invalid com port. Port set to COM1\n");
		comportnum=1;
	}
	comportnum--;

/* Read Flash Programmer binary file */

	fp = fopen(argv[2],"rb");
	if (!fp) {
		printf ( "Can't open %s: %s\n",argv[1], strerror(errno));
		exit (EXIT_FAILURE);
	}

	fseek (fp, 0, SEEK_END);
    ProgrLen = ftell (fp);
    fseek (fp, 0, SEEK_SET);
#ifdef TRACE_STATE
	printf("Programmer File Size %d\n",(int)ProgrLen);
#endif
	ProgrCode = malloc(ProgrLen);
	fread(ProgrCode,1,ProgrLen,fp);
	fclose(fp);

	ProgrCrc = 0;
	for (i=0;i<ProgrLen;i++)
		ProgrCrc ^=ProgrCode[i];
#ifdef TRACE_STATE
	printf("Programmer File CRC 0x%02X\n",ProgrCrc);
#endif
	ProgrHeader[0]=SOH;
	ProgrHeader[1]=ProgrLen;
	ProgrHeader[2]=ProgrLen>>8;


/* Read binary image file that is to program to the flash */

	fp = fopen(argv[3],"rb");
	if (!fp) {
		printf ("Can't open %s: %s\n",argv[1], strerror(errno));
		free(ProgrCode);
		exit (EXIT_FAILURE);
	}

	fseek (fp, 0, SEEK_END);
    ImageLen = ftell (fp);
    fseek (fp, 0, SEEK_SET);
#ifdef TRACE_STATE
	printf("Image File Size %d\n",(int)ImageLen);
#endif
	ImageBuff = malloc(ImageLen);
	fread(ImageBuff,1,ImageLen,fp);
	fclose(fp);

	ImageCrc = 0;
	for (i=0;i<ImageLen;i++)
		ImageCrc ^=ImageBuff[i];
#ifdef TRACE_STATE
	printf("Image File CRC 0x%02X\n",ImageCrc);
#endif
	ImageHeader[0]=PROG_SOH;
	ImageHeader[1]=ImageLen;
	ImageHeader[2]=ImageLen>>8;
	ImageHeader[3]=ImageLen>>16;
	ImageHeader[4]=ImageLen>>24;


	OpenComport(comportnum,9600);
	state = IDLE;

	while(1)
	{
		switch (state)
		{
			case IDLE:
			{
				if ( PollComport(comportnum,(unsigned char*)&RecChar,1) > 0)
				{
#ifdef TRACE_STATE
					printf("state IDLE rec: 0x%02X\n",RecChar);
#endif
					if (RecChar == STX)
					{
						SendBuf(comportnum,ProgrHeader,3);
						printf("Downloading programmer ...\n");
						state = WAIT_ACK;
						break;
					}
				}
				break;
			}
			case WAIT_ACK:
			{
				if ( PollComport(comportnum,(unsigned char*)&RecChar,1) > 0)
				{
#ifdef TRACE_STATE
					printf("state WAIT_ACK rec: 0x%02X\n",RecChar);
#endif
					if (RecChar == ACK)
					{
						SendBuf(comportnum,ProgrCode,ProgrLen);
						state = WAIT_CRC;
						break;
					}
				}
				state = IDLE;
				break;
			}
			case WAIT_CRC:
			{
				if ( PollComport(comportnum,(unsigned char*)&RecChar,1) > 0)
				{
#ifdef TRACE_STATE
					printf("state WAIT_CRC rec: 0x%02X\n",RecChar);
#endif
					if (RecChar == ProgrCrc)
					{
						TxChar = ACK;
						SendBuf(comportnum,&TxChar,1);
						state = CHANGE_UART_BPS;
						break;
					}
				}
				state = IDLE;
				break;
			}
			case CHANGE_UART_BPS:
			{
#ifdef TRACE_STATE
					printf("state CHANGE_UART_BPS \n");
#endif

			//	ComportChangeBaudrate(comportnum,115200);
				CloseComport(comportnum);
				OpenComport(comportnum,115200);

				state = PROG_IDLE;
				break;
			}
			case PROG_IDLE:
			{
				if ( PollComport(comportnum,(unsigned char*)&RecChar,1) > 0)
				{
#ifdef TRACE_STATE
					printf("state PROG_IDLE rec: 0x%02X\n",RecChar);
#endif
					if (RecChar == PROG_STX)
					{
						SendBuf(comportnum,ImageHeader,5);
						printf("Downloading image ...\n");
						state = PROG_WAIT_ACK;
						break;
					}
				}
				break;
			}
			case PROG_WAIT_ACK:
			{
				if ( PollComport(comportnum,(unsigned char*)&RecChar,1) > 0)
				{
#ifdef TRACE_STATE
					printf("state PROG_WAIT_ACK rec: 0x%02X\n",RecChar);
#endif
					if (RecChar == PROG_ACK)
					{
						SendBuf(comportnum,ImageBuff,ImageLen);
						state = PROG_WAIT_CRC;
						break;
					}
				}
				state = PROG_IDLE;
				break;
			}
			case PROG_WAIT_CRC:
			{
				if ( PollComport(comportnum,(unsigned char*)&RecChar,1) > 0)
				{
#ifdef TRACE_STATE
					printf("state PROG_WAIT_CRC rec: 0x%02X\n",RecChar);
#endif
					if (RecChar == ImageCrc)
					{
						TxChar = PROG_ACK;
						SendBuf(comportnum,&TxChar,1);
						printf("Programming image ...\n");
						state = PROG_WAIT_COMP;
						break;
					}
				}
				state = PROG_IDLE;
				break;
			}
			case PROG_WAIT_COMP:
			{
				if ( PollComport(comportnum,(unsigned char*)&RecChar,1) > 0)
				{
#ifdef TRACE_STATE
					printf("state PROG_WAIT_COMP rec: 0x%02X\n",RecChar);
#endif
					if (RecChar == PROG_COM)
					{
							printf("Flash programmed successfully\n");
							free(ProgrCode);
							free(ImageBuff);
							CloseComport(comportnum);
							exit (EXIT_SUCCESS);
					}
				}
				state = PROG_IDLE;
				break;
			}
		}
	}

	free(ProgrCode);
	free(ImageBuff);
	CloseComport(comportnum);
	exit (EXIT_SUCCESS);
}

