@REM Unlock REEF Home Connector device via Serial Port

@REM Please set this value to serial port available on your machine (COM4 in this example)
@set serialPort=4

@REM Please don't change nothing more
:program
@echo.
@echo ^/--------------------------------------------------------------------------------------^\
@echo ^|                                                                                      ^|
@echo ^|                     Unlocking REEF Home Connector via COM %serialPort%                          ^|
@echo ^|                                                                                      ^|
@echo ^|--------------------------------------------------------------------------------------^|
@echo ^|                                                                                      ^|
@echo ^| You have to initialize 'Unlock' on your REEF Home Connector device, please:          ^|
@echo ^| 1. Press and hold 'Program' button on your program adapter,                          ^|
@echo ^| 2. Press for a short while 'Reset' button,                                           ^|
@echo ^| 3. Release 'Program' button,                                                         ^|
@echo ^| 3. Press 'Enter' key.                                                                ^|
@echo ^|                                                                                      ^|
@echo ^\--------------------------------------------------------------------------------------^/

@echo.
@pause
@echo.
@uart %serialPort% 452fp.bin image452_service.bin

@echo.
@echo  ____                   
@echo ^|  _ ^\  ___  _ __   ___ 
@echo ^| ^| ^| ^|/ _ \^| '_ \ / _ \
@echo ^| ^|_^| ^| (_) ^| ^| ^| ^|  __/
@echo ^|____/ \___/^|_^| ^|_^|\___^|
@echo.

@set choice=n
@set /p choice="Would you like to unlock one more device (y/N)?"
@if "%choice%"=="y" goto program
@if "%choice%"=="Y" goto program
