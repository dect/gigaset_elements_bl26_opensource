/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <vassilis.maniotis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 * See http://www.gnu.org/licenses/gpl-2.0.txt for more details.
 */

#ifndef _SPI_FLASH_H
#define _SPI_FLASH_H

/*========================== Include files ==================================*/

/*========================== Local macro definitions & typedefs =============*/

/* Flash Command Definitions */

#define FLASH_PAGE_SIZE		256
#define SEC_SIZE_4K			0x1000//0
#define SEC_SIZE_32K		0x8000//1
#define SEC_SIZE_64K		0x10000//2
#define FLASH_MEMORY_SIZE	0x200000
#define	MAX_READY_WAIT_COUNT	1000000

#define	SPI_FLASH_WREN_CMD			0x06	/* Write Enable */
#define	SPI_FLASH_WRDI_CMD			0x04	/* Write Disable */
#define	SPI_FLASH_RDID_CMD			0x9F	/* Read Identification */
#define	SPI_FLASH_RDSR_CMD			0x05	/* Read Status Register */
#define	SPI_FLASH_WRSR_CMD			0x01	/* Write Status Register */
#define	SPI_FLASH_READ_CMD			0x03	/* Read Data Bytes */
#define	SPI_FLASH_READ_FAST_CMD		0x0B	/* Read Data Bytes in higher speed */
#define	SPI_FLASH_PP_CMD			0x02	/* Page Program */
#define	SPI_FLASH_SE_CMD			0xD8	/* Sector Erase */
#define	SPI_FLASH_BE_CMD			0xC7	/* Bulk Erase */
#define SPI_FLASH_DP_CMD			0xB9	/* Deep Power Down */
#define	SPI_FLASH_RES_CMD			0xAB	/* Release from Deep Power Down */
#define	SPI_FLASH_SE_32K_CMD		0x52	/* Sector Erase 32Kbytes*/
#define	SPI_FLASH_SE_4K_CMD			0x20	/* Sector Erase 4KBytes*/
#define	SPI_FLASH_AAI_CMD			0xAD	/* Address Auto Increment */
#define	SPI_FLASH_EBSY_CMD			0x70	/* Enable SO BUSY during AAI */
#define	SPI_FLASH_DBSY_CMD			0x80	/* Disable SO BUSY during AAI */

/* Status Register Bits */

#define STATUS_BUSY		0x01
#define	STATUS_WEL		0x02
#define	STATUS_BP0		0x04
#define	STATUS_BP1		0x08
#define	STATUS_BP2		0x10
#define	STATUS_WP		0x80

/* Manufacturer and device ID */
/* ST devices */
#define	ST_MANID			0x20
#define	ST_ID_M25P16		0x2015
#define	ST_ID_M25P32		0x2016

/* Spansion devices */
#define	SPANSION_MANID			0x01
#define	SPANSION_ID_S25FL064A	0x0216
/* Winbond devices */
#define	WINBOND_MANID			0xEF
#define	WINBOND_ID_W25X16		0x3015
#define	WINBOND_ID_W25X32		0x3016
#define	WINBOND_ID_W25X64		0x3017
#define	WINBOND_ID_W25Q80		0x4014
#define	WINBOND_ID_W25Q16		0x4015
#define	WINBOND_ID_W25Q32		0x4016
#define	WINBOND_ID_W25Q64		0x4017
#define	WINBOND_ID_W25Q128		0x4018

/* MXIC devices */
#define	MXIC_MANID			0xC2
#define	MXIC_ID_MX25L1605D	0x2015
#define	MXIC_ID_MX25L3205D	0x2016
#define	MXIC_ID_MX25L6405D	0x2017

/* SST devices */
#define	SST_MANID				0xBF
#define SST_ID_SST25VF32		0x254A
#define SST_ID_SST25VF064		0x254B

/*EON devices */
#define EON_MANID				0x1C
#define EON_ID_EN25Q32A			0x3016
#define EON_ID_EN25Q64			0x3017

/*========================== Global function prototypes =====================*/

unsigned long SpiFlashID( void ); 
unsigned char SpiFlashReadStatus( void );
int SpiFlashWriteStatus( unsigned char ); 
int SpiFlashSetWriteEbable( void);
int SpiFlashSetWriteDisable( void);
int SpiFlashEraseChip( void );
int SpiFlashEraseSector( unsigned long ,unsigned int );
int SpiFlashRead( unsigned long ,unsigned char* ,unsigned long );
int SpiFlashReadFast(unsigned long ,unsigned char* ,unsigned long );
void SpiFlashSectorProtect(unsigned long );
int SpiFlashSectorProtectDetect( unsigned long );
int SpiFlashProgram(unsigned long ,unsigned char* , unsigned long );
int SpiFlashProgramPage(unsigned long ,unsigned char* , unsigned );
int SpiFlashWaitTillReady (void);
int SpiFlashCheckErase(unsigned long , unsigned long );




 

#endif /* SPI_FLASH_H */

//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:49.715554554
//@@ -96,7 +96,7 @@
// int SpiFlashSetWriteEbable( void);
// int SpiFlashSetWriteDisable( void);
// int SpiFlashEraseChip( void );
//-int SpiFlashEraseSector( unsigned long ,unsigned short );
//+int SpiFlashEraseSector( unsigned long ,unsigned int );
// int SpiFlashRead( unsigned long ,unsigned char* ,unsigned long );
// int SpiFlashReadFast(unsigned long ,unsigned char* ,unsigned long );
// void SpiFlashSectorProtect(unsigned long );
