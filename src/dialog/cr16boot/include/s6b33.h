// This is the header file for the experimental lcd driver for SC1445x developoment board
// 			Supported displays:
// LCD:  (dotmatrix display) ---- using lcd driver ST7529

/*
	Driver interface description
	LCD		BOARD
	---------------------------
	RS		->        AD7
	DB[0..7]	->	DAB[0..7]
*/
#ifndef LCD_STS6B33_H
#define LCD_STS6B33_H
/***************       Definitions section             ***************/
/* HW dependent definitions */


#define CONFIG_32M_SDRAM_PATCH
#define OVER_16M_SDRAM_ACCESS_PATCH_FLAG
#define PROTECT_OVER_16M_SDRAM_ACEESS
#define RESTORE_OVER_16M_SDRAM_ACEESS

/* Registers */
//Since RS -> AD7, data are accessed at 0x80 and instructions are accessed at 0x00
#define LCD_BASE 0x01080000
#define LCD_DR (LCD_BASE+0x80)
#define LCD_IR (LCD_BASE+0x00)
#define LCD_LATCH 0x01090000

extern unsigned int lcd_backlight;
#define LCD_SET_BACKLIGHT_ON	{lcd_backlight=1;}
#define LCD_SET_BACKLIGHT_OFF	{lcd_backlight=0;}

#define FASTBUSTIMING

//Kernel MACROs

#define LCD_WRITE_DATA(x)	{(*(volatile unsigned char *)(LCD_DR)=(x));} /* write data to the display RAM (one byte)*/
#define LCD_WRITE_INST(x)	{(*(volatile unsigned char *)(LCD_IR)=(x));}
#define LCD_READ_DATA(x)	{(x=*(volatile unsigned char *)(LCD_DR));} /* read data from the display RAM (one byte)*/

extern unsigned int lcd_backlight;
#define LCD_SET_BACKLIGHT_ON	{lcd_backlight=1;}
#define LCD_SET_BACKLIGHT_OFF	{lcd_backlight=0;}

#define DDRAM_X_OFFSET 16
#define DDRAM_Y_OFFSET 46
#define X_OFFSET 2

	#define lcd_out_ctl2(X) \
	({\
			register short __r4 __asm__ ("r4") = (short)(X) ; \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1080000:l,(r1,r0) \n\t"\
    			"storw	r4,0x0:(r1,r0) \n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
			    :"=r" (__r4) \
			    : "0" (__r4) \
			    : "r0","r1","r2","r3"); \
			 \
		})

#define lcd_out_ctl(X) lcd_out_ctl2((X))



#define lcd_out_dat2(X) \
		({\
			register short __r4 __asm__ ("r4") = (short)(X) ; \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1080080:l,(r1,r0) \n\t"\
    			"storw	r4,0x0:(r1,r0) \n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
			    :"=r" (__r4) \
			    : "0" (__r4) \
			    : "r0","r1","r2","r3"); \
			 \
		})

#define lcd_out_dat(X) {lcd_out_dat2((X));}


#define lcd_in_dat2 \
	({\
			register short __r4 __asm__ ("r4") ; \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1080080:l,(r1,r0) \n\t"\
    			"loadw	0x0:s(r1,r0),r4 \n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
			    :"=r" (__r4) \
			    : "0" (__r4) \
			    : "r0","r1","r2","r3"); \
			    __r4;\
		})

#define lcd_in_dat(X) {X=lcd_in_dat2;}




#define lcd_out2b3_dat2(X,Y) \
		({\
			volatile register short __r4 __asm__ ("r4") = (short)(X); \
			volatile register short __r5 __asm__ ("r5") = (short)(Y); \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1080080:l,(r1,r0) \n\t"\
    			"storw	r4,0x0:(r1,r0) \n\t"\
    			"storw	$0x9,0xff0064 \n\t"\
    			"storw	r5,0x0:(r1,r0) \n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
			    :"=r" (__r4),"=r" (__r5)\
			    : "0" (__r4),"1" (__r5) \
			    : "r0","r1","r2","r3"); \
			 \
		})


#define lcd_out_2b3_dat(X,Y,Z) { lcd_out2b3_dat2(    ((X&0xF8)|((Y>>5)&0x7)),\
													(((Y<<3)&0xE0)|((Z>>3)&0x1F)) );}

//Critical !! the "storw	$0x9,0xff0064 \n\t"\ is required for proper timing of the
//CS

	//#define lcd_out_ctl(X)   {LCD_WRITE_INST(X);}
	//#define lcd_out_dat(X)   {LCD_WRITE_DATA(X);}
/* Display settings */

//#define B3P3_MODE 	//When defined 3bytes-2pixels mode is used. If not defined 2B3P_MODE is used


/* LCD screen and bitmap image array constants */
#define SCRN_TOP			0x0


#define SCRN_BOTTOM			79
#define SCRN_LEFT			0x0
#define SCRN_RIGHT			147
#define DDRAM_SIZE			((SCRN_BOTTOM+1)*(SCRN_RIGHT+1)*2/3)
#define X_BYTES				(SCRN_RIGHT+1)
#define Y_BYTES	      		((SCRN_BOTTOM+1))
#define COL_MIN				0x00

//Processor specific commands
//#define LCD_BACKLIGHT_LEVEL(X)  (TIMER2_DUTY_REG=X|X<<8)

/***      Data types definitions       ***/
struct lcd_sc14450_conf_struct{
	// unsigned short flags;
	unsigned char refresh_tmout;
} ;
//lcd_sc14450_conf_struct Flags
// #ifdef AUTO_REFRESH
	// #define	REFRESH_TIMER_ON  0x0001
// #endif

//IOCTL Commands
typedef enum IOCTL_Cmd_enum {
	LCD_init,
	LCD_out_ctl,
	LCD_out_data,
	LCD_clear_ram,
	LCD_clear_rect,
	LCD_invert_rect,
	LCD_draw_border,
	LCD_clear_border,
	LCD_text,
	LCD_label,
	LCD_glyph,
	LCD_graphic,
	LCD_animate_bitmap,
	LCD_update,
	LCD_contrast_up,
	LCD_contrast_down,
	LCD_sleepmode_enter,
	LCD_sleepmode_exit,
	LCD_backlight_level,
	LCD_contrast_level,
	LCD_refresh_tmout,
	LCD_show_cursor,
 	LCD_fonts_width_get,
	LCD_IOCTL_MAX//this is a delimiter
} IOCTL_Cmd;


/****************************************************************************/
/**************           HW INDEPENDENT DEFINITIONS                    ***************/
/****************************************************************************/
/*
 * Macros to help debugging
 */
//#define LCD_SC14450_DEBUG
#undef PDEBUG             /* undef it, just in case */
#ifdef LCD_SC14450_DEBUG
#  ifdef __KERNEL__
     /* This one if debugging is on, and kernel space */
#    define PDEBUG(fmt, args...) printk( KERN_DEBUG "lcd_sc14450: " fmt, ## args)
#  else
     /* This one for user space */
#    define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif

#undef PDEBUGG
#define PDEBUGG(fmt, args...) /* nothing: it's a placeholder */

#define LCD_SC14450_MAJOR 251
#ifndef LCD_SC14450_MAJOR
#define LCD_SC14450_MAJOR 0   /* dynamic major by default */
#endif

#ifndef LCD_SC14450_NR_DEVS
#define LCD_SC14450_NR_DEVS 1
#endif

/* LCD function prototype list */
void lcd_init(void);
void lcd_clear_ram(void);
void lcd_clear_rect(uchar left,  uchar top, uchar right, uchar bottom);
void lcd_invert_rect(uchar left, uchar top, uchar right, uchar bottom);
void lcd_horz_line(uchar left, uchar right, uchar row);
void lcd_vert_line(uchar top, uchar bottom, uchar column);
void lcd_clr_horz_line(uchar left, uchar right, uchar row);
void lcd_clr_vert_line(uchar top, uchar bottom, uchar column);
void lcd_draw_border(uchar left, uchar top, uchar right, uchar bottom);
void lcd_clear_border(uchar left, uchar top, uchar right, uchar bottom);
void lcd_glyph(uchar left, uchar top, uchar width, uchar height, uchar *glyph, uchar store_width);
void lcd_animated_glyph(uchar left, uchar top, uchar width, uchar num_of_bitmaps, uchar *glyph1, uchar *glyph2, uchar *glyph3);
void lcd_text(uchar left, uchar top, uchar font, char *str);
void lcd_update(uchar top, uchar bottom);
void lcd_cursor_set(uchar left, uchar top, uchar font, uchar ShowCursor, uchar blinking_rate);
/**/
void lcd_contrast_level(uchar level);
void lcd_contrast_up(void);
void lcd_contrast_down(void);
void lcd_sleepmode_enter(void);
void lcd_sleepmode_exit(void);
void lcd_scroll_init(char *buf);
 uchar lcd_font_width_get(uchar font_width);

#endif
