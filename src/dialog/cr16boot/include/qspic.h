#ifndef _QSPIC_H_
#define _QSPIC_H_

#define SST_ID      0xBF
#define WINBOND_ID  0xEF
#define MACRONIX_ID 0xC2
#define SPANSION_ID 0x01

#define SINGLE_SLOW_READ_FREQ 0x0
#define SINGLE_FAST_READ_FREQ 0x1
#define DUAL_READ_FREQ        0x2
#define QUAD_READ_FREQ        0x3

struct FlashDescriptor {
	unsigned char     ManID;       // Jedec ID : Manufacturer Identifier
	unsigned char     TypeID;      // Jedec ID : Memory Type Identifier
	unsigned char     CapID;       // Jedec ID : Memory Capacity Identifier
	unsigned char     Freqs[4];    // Contains the allowed frequencies (MHz)
	unsigned int      Capacity;    // Memory capacity in Megabits
	unsigned long int MaxAddr;     // The maximum available address
	char              *DeviceStr;  // The name of the device
};
// The maximum allowed frequencies.
// Freqs[0] : Standard SPI (slow speed)
// Freqs[1] : Standard SPI (fast speed)
// Freqs[2] : Dual SPI
// Freqs[3] : Quad SPI

              
struct flash_dev {
	// Takes the descriptor number. 
	unsigned char desc_num;

	// Takes the maximum frequencies that can be used during auto and manual mode. (Mhz)
	unsigned char auto_max_clk;	
	unsigned char manual_max_clk;

	// Initialize the internal state of the flash memory.
	// Returns the size of flash memory in Mbits.
	int           (*init_dev)     (void);
	
	// Controls the auto mode of the qspic controller
	void          (*start_auto)   (void);
	void          (*stop_auto)    (void);
	
	// Functions to read data from the flash memory. Use this only during the manual mode
	void          (*read_byte)    (unsigned long int Address, unsigned long int BSize, unsigned char     *BBuf);
	void          (*read_half)    (unsigned long int Address, unsigned long int HSize, unsigned int      *HBuf);
	void          (*read_word)    (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf);
	
	// Functions to erase the flash memory. Returns the number of bytes that has been erased.
	void          (*erase_chip)   (void);
	long int      (*erase_sector) (unsigned long int Address);
	long int      (*erase_block)  (unsigned long int Address);
	long int      (*erase_parms)  (unsigned long int Address);
	
	// Write a page to the flash memory. Returns the number of bytes tha has been written.
	int           (*write_page)   (unsigned long int Address, unsigned long int *WPage);
};

unsigned long int qspic_flash_to_sys_addr(unsigned long int FlashAddress);

void qspic_set_auto_page( unsigned long int FlashAddress);

char qspic_find_flash_desc (unsigned char *JedecID, unsigned char *desc_num);

char qspic_autofind_flash_type (unsigned long *flash_type);

struct flash_dev * qspic_dev_init (unsigned char flash_type);

void qspic_init (void);

#define NUM_OF_FLASH_DESC 10

extern const struct FlashDescriptor FlashDes[NUM_OF_FLASH_DESC];

#endif
