
#ifndef TFT_LCD_H
#define TFT_LCD_H



#define LCD_CTRL_ADDR CONFIG_TFT_LCD_BASE 		//0x1300000
#define LCD_DATA_ADDR CONFIG_TFT_LCD_BASE+0x80	//0x1300080

#define lcd_ctrl_write(x) *(volatile unsigned char*)(LCD_CTRL_ADDR)=x
#define lcd_ctrl_read	*(volatile unsigned char*)(LCD_CTRL_ADDR)

#define lcd_data_write(x) *(volatile unsigned char*)(LCD_DATA_ADDR)=x
#define lcd_data_read	*(volatile unsigned char*)(LCD_DATA_ADDR)


#define LCD_NOP						lcd_ctrl_write(0x00)			
#define LCD_RESET					lcd_ctrl_write(0x01)
#define LCD_READ_DISPLAY_IDENT		lcd_ctrl_write(0x04)	
#define LCD_GET_RED_CHANNEL			lcd_ctrl_write(0x06)			
#define LCD_GET_GREEN_CHANNEL		lcd_ctrl_write(0x07			
#define LCD_GET_BLUE_CHANNEL		lcd_ctrl_write(0x08)

#define LCD_READ_STATUS				lcd_ctrl_write(0x09)


#define LCD_GET_PIXEL_FORMAT		lcd_ctrl_write(0x0c)			
#define LCD_GET_POWER_MODE			lcd_ctrl_write(0x0a)			
#define LCD_GET_ADDRESS_MODE		lcd_ctrl_write(0x0b)			
#define LCD_GET_DISPLAY_MODE		lcd_ctrl_write(0x0d)			
#define LCD_GET_SIGNAL_MODE			lcd_ctrl_write(0x0e)
#define LCD_GET_DIAGNOSTIC_RESULT	lcd_ctrl_write(0x0f)			


#define LCD_ENTER_SLEEP_MODE		lcd_ctrl_write(0x10)			
#define LCD_EXIT_SLEEP_MODE			lcd_ctrl_write(0x11)			
#define LCD_ENTER_PARTIAL_MODE		lcd_ctrl_write(0x12)			
#define LCD_ENTER_NORMAL_MODE		lcd_ctrl_write(0x13)			
#define LCD_EXIT_INVERT_MODE		lcd_ctrl_write(0x20)			
#define LCD_ENTER_INVERT_MODE		lcd_ctrl_write(0x21)			

#define LCD_SET_GAMMA_CURVE			lcd_ctrl_write(0x26)			

#define LCD_SET_DISPLAY_OFF			lcd_ctrl_write(0x28)			
#define LCD_SET_DISPLAY_ON			lcd_ctrl_write(0x29)			
#define LCD_SET_COLUMN_ADDRESS		lcd_ctrl_write(0x2a)			
#define LCD_SET_PAGE_ADDRESS		lcd_ctrl_write(0x2b)			

#define LCD_WRITE_MEMORY_START		lcd_ctrl_write(0x2c)			
#define LCD_WRITE_LUT				lcd_ctrl_write(0x2d)			
#define LCD_READ_MEMORY_START		lcd_ctrl_write(0x2e)			
#define LCD_SET_PARTIAL_AREA		lcd_ctrl_write(0x30)			
#define LCD_SET_SCROLL_AREA			lcd_ctrl_write(0x33)			
#define LCD_SET_TEAR_OFF			lcd_ctrl_write(0x34)			
#define LCD_SET_TEAR_ON				lcd_ctrl_write(0x35)			

#define LCD_SET_ADDRESS_MODE		lcd_ctrl_write(0x36)			
#define LCD_SET_SCROLL_START		lcd_ctrl_write(0x37)			
#define LCD_EXIT_IDLE_MODE			lcd_ctrl_write(0x38)			
#define LCD_ENTER_IDLE_MODE			lcd_ctrl_write(0x39)			
#define LCD_SET_PIXEL_FORMAT		lcd_ctrl_write(0x3a)			
#define LCD_WRITE_MEMORY_CONTINUE	lcd_ctrl_write(0x3c)			
#define LCD_READ_MEMORY_CONTINUE	lcd_ctrl_write(0x3e)			
#define LCD_SET_TEAR_SCANLINE		lcd_ctrl_write(0x44)
#define LCD_GET_SCANLINE			lcd_ctrl_write(0x45)
#define LCD_READ_ID1				lcd_ctrl_write(0xda)			
#define LCD_READ_ID2				lcd_ctrl_write(0xdb)			
#define LCD_READ_ID3				lcd_ctrl_write(0xdc)			

#define LCD_RGB_SIGNAL_CTRL			lcd_ctrl_write(0xb0)			
#define LCD_FRAME_RATE_NORMAL_CTRL	lcd_ctrl_write(0xb1)			
#define LCD_FRAME_RATE_IDLE_CTRL	lcd_ctrl_write(0xb2)			
#define LCD_FRAME_RATE_PARTIAL_CTRL	lcd_ctrl_write(0xb3)			
#define LCD_DISPLAY_INVERSION_CTRL	lcd_ctrl_write(0xb4)			
#define LCD_RGB_INTERFACE_BLANKING	lcd_ctrl_write(0xb5)			
#define LCD_DISPALY_FUNC_SET_5		lcd_ctrl_write(0xb6)
#define LCD_SOURCE_DRIVER_DIR_CTRL	lcd_ctrl_write(0xb7)	
#define LCD_GATE_DRIVER_DIR_CTRL	lcd_ctrl_write(0xb8)

#define LCD_POWER_CTRL1				lcd_ctrl_write(0xc0)
#define LCD_POWER_CTRL2				lcd_ctrl_write(0xc1)
#define LCD_POWER_CTRL3				lcd_ctrl_write(0xc2)
#define LCD_POWER_CTRL4				lcd_ctrl_write(0xc3)
#define LCD_POWER_CTRL5				lcd_ctrl_write(0xc4)

#define LCD_VCOM_CTRL1				lcd_ctrl_write(0xc5)
#define LCD_VCOM_CTRL2				lcd_ctrl_write(0xc6)

#define LCD_VCOM_OFFSET_CTRL		lcd_ctrl_write(0xc7)

#define LCD_WRITE_ID4				lcd_ctrl_write(0xd3)
#define LCD_NV_MEM_FUN_CTRL1		lcd_ctrl_write(0xd9)
#define LCD_NV_MEM_FUN_CTRL2		lcd_ctrl_write(0xde)

#define LCD_POS_GAMMA_CORRECTION	lcd_ctrl_write(0xe0)
#define LCD_NEG_GAMMA_CORRECTION	lcd_ctrl_write(0xe1)

#define LCD_GAMM_R_SEL				lcd_ctrl_write(0xf2)












// initializes I/O pins connected to LCD
void lcdInitHW(void);
// initializes the LCD display (gets it ready for use)
void lcdInit(void);

// moves the cursor/position to Home (upper left corner)
void lcdHome(void);

// clears the LCD display
void lcdClear(void);

// moves the cursor/position to the row,col requested
// ** this may not be accurate for all displays
void lcdGotoXY(unsigned short row, unsigned short col);

void lcdCLS(void);

#endif
