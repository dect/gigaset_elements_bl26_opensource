#ifndef _MACRONIX_H_
#define _MACRONIX_H

//--------------------------------------------------------------------------------------
//                              Macronix Instructions
//--------------------------------------------------------------------------------------

#define MACRONIX_INST_WREN     0x06    // Write Enable
#define MACRONIX_INST_WRDI     0x04    // Write Disable
#define MACRONIX_INST_RDID     0x9f    // Read Idendification 
#define MACRONIX_INST_RDSR     0x05    // Read Status Register
#define MACRONIX_INST_WRSR     0x01    // Write Status Register
#define MACRONIX_INST_READ     0x03    // Read Data
#define MACRONIX_INST_FREAD    0x0b    // Fast Read Data
#define MACRONIX_INST_DREAD    0xbb    // Dual I/O Read Data
#define MACRONIX_INST_QREAD    0xeb    // Quad I/O Read Data
#define MACRONIX_INST_QPP      0x38    // Quag Page Program
#define MACRONIX_INST_SE       0x20    // Sector Erase
#define MACRONIX_INST_BE       0xd8    // Block Erase
#define MACRONIX_INST_CE       0xc7    // Chip Erase
#define MACRONIX_INST_PP       0x02    // Page Program
#define MACRONIX_INST_CP       0xad    // Continuously program mode
#define MACRONIX_INST_DP       0xb9    // Deep Power Down
#define MACRONIX_INST_RDP      0xab    // Release from deep poewr down
#define MACRONIX_INST_RES      0xab    // Read Enectronic  ID
#define MACRONIX_INST_REMS     0x90    // Read electronic manufacture and device ID
#define MACRONIX_INST_REMS2    0xef    // Read electronic manufacture and device ID (Dual Mode)
#define MACRONIX_INST_REMS4    0xdf    // Read electronic manufacture and device ID (Quad Mode)
#define MACRONIX_INST_ENSO     0xb1    // Enter Secured OTP
#define MACRONIX_INST_EXSO     0xc1    // Exit secured OTP
#define MACRONIX_INST_RDSCUR   0x2b    // Read security register
#define MACRONIX_INST_WRSCUR   0x2f    // Write security register
#define MACRONIX_INST_ESRY     0x70    // Enable SO to output RY/BY#
#define MACRONIX_INST_DSRY     0x80    // Disable SO to output RY/BY#

//--------------------------------------------------------------------------------------
//                              Macronix Status Register
//--------------------------------------------------------------------------------------

#define MACRONIX_STATUS_WIP  (0x1 << 0) // Write In Progess
#define MACRONIX_STATUS_WEL  (0x1 << 1) // Write Enable Latch
#define MACRONIX_STATUS_BP0  (0x1 << 2) // Level of Protected Block bit 0
#define MACRONIX_STATUS_BP1  (0x1 << 3) // Level of Protected Block bit 1
#define MACRONIX_STATUS_BP2  (0x1 << 4) // Level of Protected Block bit 2
#define MACRONIX_STATUS_BP3  (0x1 << 5) // Level of Protected Block bit 3
#define MACRONIX_STATUS_QE   (0x1 << 6) // Quad Enable
#define MACRONIX_STATUS_SRWD (0x1 << 7) // Status Register Write Disable

//--------------------------------------------------------------------------------------
//                              Macronix Security Register Bits
//--------------------------------------------------------------------------------------
#define MACRONIX_SECURITY_OTPID (0x1 << 0) // Secured OTP Indicator bit
#define MACRONIX_SECURITY_LDSO  (0x1 << 1) // Lock Down Secured OTP 
#define MACRONIX_SECURITY_CP    (0x1 << 4) // Continuously Program Mode

//--------------------------------------------------------------------------------------
//                              Functions related to Status Register
//--------------------------------------------------------------------------------------

void MACRONIX_WaitEndOfWrite (void);
unsigned char MACRONIX_ReadStatus (void);
void MACRONIX_WriteStatus (unsigned char w_status);

//--------------------------------------------------------------------------------------
//                                   Read ID Functions
//-------------------------------------------------------------------------------------- 

void MACRONIX_ReadJEDECID (unsigned char *JIDBBuf);

//--------------------------------------------------------------------------------------
//                    Functions to Control the mode of the flash memory
//-------------------------------------------------------------------------------------- 

void MACRONIX_SetQuadEnable (void);
void MACRONIX_ClearQuadEnable (void);
void MACRONIX_EscapePE (void);
void MACRONIX_ReleasePDM(void);

//--------------------------------------------------------------------------------------
//                                   Erase Functions
//--------------------------------------------------------------------------------------

void MACRONIX_EraseChip (void);
long int MACRONIX_EraseSector (unsigned long int Address);
long int MACRONIX_EraseBlock (unsigned long int Address);

//--------------------------------------------------------------------------------------
//                                   Write Data Functions
//--------------------------------------------------------------------------------------

int MACRONIX_WritePageSingle (unsigned long int Address, unsigned long int *WPage);
int MACRONIX_WritePageQuad (unsigned long int Address, unsigned long int *WPage);
long int MACRONIX_WriteCP (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf);

//--------------------------------------------------------------------------------------
//                           Read Data functions during Manual Mode
//--------------------------------------------------------------------------------------

void MACRONIX_ReadDataByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf);
void MACRONIX_ReadFastByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf);
void MACRONIX_ReadQuadIOByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf);
void MACRONIX_ReadQuadIOHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf);
void MACRONIX_ReadQuadIOWord (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf);
void MACRONIX_ReadDualIOByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf);
void MACRONIX_ReadDualIOHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf);
void MACRONIX_ReadDualIOWord (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf);

//--------------------------------------------------------------------------------------
//                Configurations for the Read sequence during Auto Mode
//--------------------------------------------------------------------------------------

void MACRONIX_AutoCfg_QuadIO_NoPE(void);
void MACRONIX_AutoCfg_QuadIO_PE(void);
void MACRONIX_AutoCfg_DualIO (void);
void MACRONIX_AutoCfg_FastRead(void);
void MACRONIX_AutoCfg_Read (void);



int MACRONIX_InitFlashDevice(void);
void MACRONIX_StartAutoMode(void);
void MACRONIX_StopAutoMode(void);
unsigned char MACRONIX_GetDescNum (void);
#endif
