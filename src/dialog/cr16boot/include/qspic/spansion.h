#ifndef _SPANSION_H_
#define _SPANSION_H_

//--------------------------------------------------------------------------------------
//                              Spansion Instructions
//--------------------------------------------------------------------------------------

#define SPANSION_INST_WEn      0x06	// Write Enable
#define SPANSION_INST_WDis     0x04	// Write Disable
#define SPANSION_INST_RSTAT    0x05	// Read Status Register
#define SPANSION_INST_WRR      0x01	// Write Status and configuration Register
#define SPANSION_INST_CLSR     0x30	// Reset the Erase and Program Fail Flag (SR5 and SR6, and restore normal operation)
#define SPANSION_INST_RCR      0x35	// Read and Verify Configuration Register (CFG)
#define SPANSION_INST_READ     0x03	// Read Data bytes
#define SPANSION_INST_FREAD    0x0b	// Read Data bytes at Fast speed
#define SPANSION_INST_FRDDATA  0x3b	// Fast Read Dual Output
#define SPANSION_INST_FRQDATA  0x6b	// Fast Read Quad Output
#define SPANSION_INST_FRDIO    0xbb	// Fast Read Dual IO
#define SPANSION_INST_FRQIO    0xeb	// Fast Read Quad IO
#define SPANSION_INST_PPROG    0x02	// Page Program
#define SPANSION_INST_QPPROG   0x32	// Quad Page Program
#define SPANSION_INST_P4E      0x20	// 4KB Parameter Sector Erase
#define SPANSION_INST_P8E      0x40	// 8KB Parameter Sector Erase
#define SPANSION_INST_SE       0xd8	// 64KB Sector Erase
#define SPANSION_INST_BE       0xc7	// Bulk Erase
#define SPANSION_INST_POWDN    0xb9	// Deep Power Down
#define SPANSION_INST_PDID     0xab	// Release Power Down / Read Electronic Signature
#define SPANSION_INST_JID      0x9f	// JEDEC ID
#define SPANSION_INST_OTPP     0x42	// Program one byte of data in the OTP memory space
#define SPANSION_INST_OTPR     0x4b	// Read data in the OTP memory space


//--------------------------------------------------------------------------------------
//                              Spansion Status Register
//--------------------------------------------------------------------------------------

#define SPANSION_STATUS_WIP   (0x1 << 0) // Write In Progress
#define SPANSION_STATUS_WEL   (0x1 << 1) // Write Enable Latch
#define SPANSION_STATUS_BP0   (0x1 << 2) // Level of Protected Block bit 0
#define SPANSION_STATUS_BP1   (0x1 << 3) // Level of Protected Block bit 1
#define SPANSION_STATUS_BP2   (0x1 << 4) // Level of Protected Block bit 2
#define SPANSION_STATUS_E_ERR (0x1 << 5) // Erase Error Occurred
#define SPANSION_STATUS_P_ERR (0x1 << 6) // Programming Error Occured
#define SPANSION_STATUS_SRWD  (0x1 << 7) // Status Register Write Disable


//--------------------------------------------------------------------------------------
//                              Spansion Configuration Register
//--------------------------------------------------------------------------------------

#define SPANSION_CONF_FREEZE  (0x1 << 0) // Lock BP 2-0 bits in Status Register
#define SPANSION_CONF_QUAD    (0x1 << 1) // Puts the Device into Quad I/O operation
#define SPANSION_CONF_TBPARAM (0x1 << 2) // Configures Parameter Sector location
#define SPANSION_CONF_BPNV    (0x1 << 3) // Configures BP 2-0 bits in Status Register
#define SPANSION_CONF_LOCK    (0x1 << 4) // Permanently Locks BP2-2 and configuration Bits
#define SPANSION_CONF_TBPROT  (0x1 << 5) // Configures Start of Block Protection

//--------------------------------------------------------------------------------------
//                 Functions related to Status and Configuration Registers
//--------------------------------------------------------------------------------------
void SPANSION_WaitEndOfWrite(void);
unsigned char SPANSION_ReadStatus (void);
void SPANSION_ClearStatus (void);
unsigned char SPANSION_ReadConf (void);
void SPANSION_WriteRegs (unsigned char w_status, unsigned char w_conf) ;

//--------------------------------------------------------------------------------------
//                                   Read ID Functions
//-------------------------------------------------------------------------------------- 
void SPANSION_ReadJEDECID (unsigned char *JIDBBuf);

//--------------------------------------------------------------------------------------
//                    Functions to Control the mode of the flash memory
//-------------------------------------------------------------------------------------- 
void SPANSION_SetNormalMode (void);
void SPANSION_SetQuadEnable (void);
void SPANSION_ClearQuadEnable (void);
void SPANSION_ResetQuadModeBits(void);
void SPANSION_ResetDualModeBits(void);

//--------------------------------------------------------------------------------------
//                                   Erase Functions
//--------------------------------------------------------------------------------------
void SPANSION_EraseChip (void);
long int SPANSION_EraseSector (unsigned long int Address);
long int SPANSION_EraseParamSector4KB (unsigned long int Address);
long int SPANSION_EraseParamSector8KB (unsigned long int Address);

//--------------------------------------------------------------------------------------
//                                   Write Data Functions
//--------------------------------------------------------------------------------------
int SPANSION_WritePageSingle (unsigned long int Address, unsigned long int *WPage);
int SPANSION_WritePageQuad (unsigned long int Address, unsigned long int *WPage);

//--------------------------------------------------------------------------------------
//                           Read Data functions during Manual Mode
//--------------------------------------------------------------------------------------
void SPANSION_ReadDataByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf);
void SPANSION_ReadFastByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf);
void SPANSION_ReadQuadOutputByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf);
void SPANSION_ReadQuadOutputHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf);
void SPANSION_ReadQuadOutputWord (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf);
void SPANSION_ReadDualOutputByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf);
void SPANSION_ReadDualOutputHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf);
void SPANSION_ReadDualOutputWord (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf);

//--------------------------------------------------------------------------------------
//                Configurations for the Read sequence during Auto Mode
//--------------------------------------------------------------------------------------
void SPANSION_AutoCfg_QuadOutput (void);
void SPANSION_AutoCfg_QuadIO (void);
void SPANSION_AutoCfg_DualOutput (void);
void SPANSION_AutoCfg_DualIO (void);
void SPANSION_AutoCfg_FastRead (void);
void SPANSION_AutoCfg_Read (void);


int SPANSION_InitFlashDevice (void);
void SPANSION_StartAutoMode(void);
void SPANSION_StopAutoMode(void);
#endif
