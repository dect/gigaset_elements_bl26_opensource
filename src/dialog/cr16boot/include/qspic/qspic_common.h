#ifndef _QSPIC_COMMON_H_
#define _QSPIC_COMMON_H_

#include "qspic.h"

extern struct flash_dev qspicFDev;

void qspic_clock_en (void);
void qspic_clock_dis(void);
void qspic_clksrc_hclk(void);
void qspic_clksrc_pll2 (void);
void qspic_clk_div2 (void);
void qspic_clk_div1 (void);
void qspic_pads_init (void);
void qspic_auto_mode (void);
void qspic_manual_mode (void);
void qspic_enable_quad_pads (void);
void qspic_disable_quad_pads (void);
void qspi_clk_set_PLL2_half (void);
void qspi_clk_set_PLL2_full (void);
void qspi_clk_set_HCLK_half (void);
void qspi_clk_set_HCLK_full (void);
void qspic_set_closer_freq  (unsigned char max_flash_freq);                                     
void qspic_delay(void);

#endif
