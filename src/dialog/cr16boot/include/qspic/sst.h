#ifndef _SST_H_
#define _SST_H_

//--------------------------------------------------------------------------------------
//                              SST Instructions
//--------------------------------------------------------------------------------------

#define SST_INST_NOP      0x00     // No Operation
#define SST_INST_RSTEN    0x66     // Reset Enable
#define SST_INST_RST      0x99     // Reset Memory
#define SST_INST_EQIO     0x38     // Enable Quad I/O
#define SST_INST_RSTQIO   0xff     // Reset Quad I/O
#define SST_INST_RDATA    0x03     // Read Data (clock up to 25Mhz)
#define SST_INST_FRDATA   0x0b     // Fast Read Data for single and quad SPI bus (clock up to 80 MHz)
#define SST_INST_SETBRST  0xc0     // Set Burst Length
#define SST_INST_RDBURST  0x0c     // Fast Read Data Burst with wrap
#define SST_INST_RDPI     0x08     // Jump to address within 256 byte page indexed by n
#define SST_INST_RDI      0x09     // Jump to address within block indexed by n
#define SST_INST_RDBI     0x10     // Jump to block indexed by n
#define SST_INST_JID      0x9f     // Jedec ID read (single SPI)
#define SST_INST_QJID     0xaf     // Jedec ID read (quad SPI)
#define SST_INST_SERASE   0x20     // Sector Erase 4KB
#define SST_INST_BERASE   0xd8     // Block Erase 8KB, 32KB or 64KB
#define SST_INST_CERASE   0xc7     // Chip Erase
#define SST_INST_PPROG    0x02     // Program 1 to 256 Data Bytes
#define SST_INST_ERSUS    0xb0     // Erase/Program suspends
#define SST_INST_ERRES    0x30     // Erase/Program resumes
#define SST_INST_RSID     0x88     // Read Security ID
#define SST_INST_PSID     0xa5     // Program user security ID area
#define SST_INST_LSID     0x85     // LockOut Security ID Programming
#define SST_INST_RDSR     0x05     // Read Status register
#define SST_INST_WEn      0x06     // Write Enable
#define SST_INST_WDis     0x04     // Write Disable
#define SST_INST_RBPR     0x72     // Read Block Protection Register
#define SST_INST_WBPR     0x42     // Write Block Protection Register
#define SST_INST_LBPR     0x8d     // Lock Down Block Protection Register

//--------------------------------------------------------------------------------------
//                              SST Status Bits
//--------------------------------------------------------------------------------------
#define SST_STATUS_WEL  (0x1 << 1) // Write Enable Latch
#define SST_STATUS_WSE  (0x1 << 2) // Write Suspend Erase Status
#define SST_STATUS_WSP  (0x1 << 3) // Write Suspend Program Status
#define SST_STATUS_WPLD (0x1 << 4) // Write Protection Lock Down Status
#define SST_STATUS_SEC  (0x1 << 5) // Security ID Status
#define SST_STATUS_BUSY (0x1 << 7) // Write Operetion Status



//--------------------------------------------------------------------------------------
//                              Functions related to Status Register
//--------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------
//                                   Read ID Functions
//-------------------------------------------------------------------------------------- 
void SST_ReadSingleJID (unsigned char *JIDBuf);
void SST_ReadQuadJID (unsigned char *JIDBuf);

//--------------------------------------------------------------------------------------
//                    Functions to Control the mode of the flash memory
//-------------------------------------------------------------------------------------- 
void SST_SetQuadEnable (void);
void SST_ClearQuadEnable (void);
void SST_SetBurst (unsigned char BSize);
void SST_Unlock (void);


//--------------------------------------------------------------------------------------
//                                   Erase Functions
//--------------------------------------------------------------------------------------
void SST_EraseChip (void);
long int SST_EraseSector (unsigned long int Address);
long int SST_EraseBlock (unsigned long int Address);

//--------------------------------------------------------------------------------------
//                                   Write Data Functions
//--------------------------------------------------------------------------------------
int SST_WritePageQuad (unsigned long int Address, unsigned long int *WPage);

//--------------------------------------------------------------------------------------
//                           Read Data functions during Manual Mode
//--------------------------------------------------------------------------------------
void SST_ReadFastSingleByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf );
void SST_ReadFastQuadByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf);
void SST_ReadFastSingleHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf);
void SST_ReadFastQuadHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf);
void SST_ReadFastSingleWord  (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf);
void SST_ReadFastQuadWord (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf);

//--------------------------------------------------------------------------------------
//                Configurations for the Read sequence during Auto Mode
//--------------------------------------------------------------------------------------
void SST_AutoCfg_4BurstWrap(void);
void SST_AutoCfg_8BurstWrap(void);

int SST_InitFlashDevice(void);
void SST_StartAutoMode(void);
void SST_StopAutoMode(void);
#endif
