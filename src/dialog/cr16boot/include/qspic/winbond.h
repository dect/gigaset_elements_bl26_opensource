#ifndef _WINBOND_H_
#define _WINBOND_H
//--------------------------------------------------------------------------------------
//                              Winbond Instructions
//--------------------------------------------------------------------------------------

#define WND_INST_WEn      0x06     // Write Enable
#define WND_INST_WDis     0x04     // Write Disable
#define WND_INST_RSTAT1   0x05     // Read Status Register 1
#define WND_INST_RSTAT2   0x35     // Read Status Register 2
#define WND_INST_WSTAT    0x01     // Write Status Register
#define WND_INST_PPROG    0x02     // Page Program
#define WND_INST_QPPROG   0x32     // Quad Page Program
#define WND_INST_BERASE64 0xd8     // Block Erase 64KB
#define WND_INST_BERASE32 0x52     // Block Erase 32KB
#define WND_INST_SERASE   0x20     // Sector Erase 4KB
#define WND_INST_CERASE   0xc7     // Chip Erase
#define WND_INST_ERSUS    0x75     // Erase Suspend
#define WND_INST_ERRES    0x7a     // Erase Resume
#define WND_INST_POWDN    0xb9     // Power Down
#define WND_INST_HIGHP    0xa3     // High Performance Mode
#define WND_INST_MRSTQIO  0xff     // Mode bit Reset Quad I/O
#define WND_INST_MRSTDIO  0xffff   // Mode bit Reset Dual I/O
#define WND_INST_PDID     0xab     // Release Power Down / HPM / Device ID
#define WND_INST_RUID     0x4b     // Read Unique ID
#define WND_INST_JID      0x9f     // JEDEC ID
#define WND_INST_RDATA    0x03     // Read Data
#define WND_INST_FRDATA   0x0b     // Fast Read Data
#define WND_INST_FRDDATA  0x3b     // Fast Read Dual Output
#define WND_INST_FRDIO    0xbb     // Fast Read Dual IO
#define WND_INST_FRQDATA  0x6b     // Fast Read Quad Output
#define WND_INST_FRQIO    0xeb     // Fast Read Quad IO

//--------------------------------------------------------------------------------------
//                              Winbond Status Register 1
//--------------------------------------------------------------------------------------

#define WND_STATUS_BUSSY   (0x1 << 0) // Erase or Write i progress
#define WND_STATUS_WEL     (0x1 << 1) // Write Enable Latch
#define WND_STATUS_BP0     (0x1 << 2) // Level of Protected Block bit 0
#define WND_STATUS_BP1     (0x1 << 3) // Level of Protected Block bit 1
#define WND_STATUS_BP2     (0x1 << 4) // Level of Protected Block bit 2
#define WND_STATUS_TB      (0x1 << 5) // Top / Bottom Write Protect
#define WND_STATUS_SEC     (0x1 << 6) // Sector Protect
#define WND_STATUS_SRP0    (0x1 << 7) // Status Register Protect 0


//--------------------------------------------------------------------------------------
//                              Winbond Status Register 2
//--------------------------------------------------------------------------------------

#define WND_STATUS_SRP1  (0x1 << 0) // Status Register Protect 1
#define WND_STATUS_QE    (0x1 << 1) // Quad Enable


//--------------------------------------------------------------------------------------
//                              Functions related to Status Register
//--------------------------------------------------------------------------------------

void WND_WaitEndOfWrite (void);
unsigned char WND_ReadStatus1 (void);
unsigned char WND_ReadStatus2 (void);
void WND_WriteStatus (unsigned char w_status1, unsigned char w_status2);

//--------------------------------------------------------------------------------------
//                                   Read ID Functions
//-------------------------------------------------------------------------------------- 

void WND_ReadUIDByte (unsigned char *UIDBBuf);
void WND_ReadUIDHalf (unsigned int *UIDHBuf);
void WND_ReadUIDWord (unsigned long int *UIDWBuf);
void WND_ReadJEDECID (unsigned char *JIDBuf);

//--------------------------------------------------------------------------------------
//                    Functions to Control the mode of the flash memory
//-------------------------------------------------------------------------------------- 

void WND_SetHighPerf (void);
void WND_SetQuadEnable (void);
void WND_ClearQuadEnable (void);
void WND_ResetQuadModeBits(void);
void WND_ResetDualModeBits(void);
void WND_ReleasePDM_HPM(void);

//--------------------------------------------------------------------------------------
//                                   Erase Functions
//--------------------------------------------------------------------------------------

void WND_EraseChip (void);
long int WND_EraseSector (unsigned long int Address);
long int WND_EraseBlock32KB (unsigned long int Address);
long int WND_EraseBlock64KB (unsigned long int Address);

//--------------------------------------------------------------------------------------
//                                   Write Data Functions
//--------------------------------------------------------------------------------------

int WND_WritePage (unsigned long int Address, unsigned long int *WPage);
int WND_WritePageQuad (unsigned long int Address, unsigned long int *WPage);

//--------------------------------------------------------------------------------------
//                           Read Data functions during Manual Mode
//--------------------------------------------------------------------------------------

void WND_ReadDataByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf);
void WND_ReadFastByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf);
void WND_ReadQuadOutputByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf);
void WND_ReadQuadOutputHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf);
void WND_ReadQuadOutputWord (unsigned long int RAddress, unsigned long int WSize, unsigned long int *WBuf);
void WND_ReadDualOutputByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf);
void WND_ReadDualOutputHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf);
void WND_ReadDualOutputWord (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf);

//--------------------------------------------------------------------------------------
//                Configurations for the Read sequence during Auto Mode
//--------------------------------------------------------------------------------------

void WND_AutoCfg_QuadOutput (void);
void WND_AutoCfg_QuadIO (void);
void WND_AutoCfg_DualOutput (void);
void WND_AutoCfg_DualIO (void);
void WND_AutoCfg_FastRead (void);
void WND_AutoCfg_Read(void);

int WND_InitFlashDevice(void);
void WND_StartAutoMode(void);
void WND_StopAutoMode(void);
#endif
