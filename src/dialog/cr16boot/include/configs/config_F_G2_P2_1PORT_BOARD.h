/*
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 * Gary Jennejohn <gj@denx.de>
 *
 * Configuation settings for the SAMSUNG board.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __CONFIG_H
#define __CONFIG_H

#include "sitel_io.h"

/*
 * If we are developing, we might want to start armboot from ram
 * so we MUST NOT initialize critical regs like mem-timing ...
 */

/*
 * High Level Configuration Options
 * (easy to change)
 */
#define CONFIG_SC14452			1	/* This is an SC14450 SoC	*/
#define CONFIG_F_G2_P2_1PORT_BOARD		1	/* on a Board */

//#undef CONFIG_USE_IRQ			/* we don't need IRQ/FIQ stuff */
#define CONFIG_USE_IRQ			/* we don't need IRQ/FIQ stuff */

/*
 * Size of malloc() pool
 */
#define CONFIG_MALLOC_SIZE	(CFG_ENV_SIZE + 128*1024)

/*
 * Hardware drivers : none
 */

/*
 * select serial console configuration
 */

/* allow to overwrite serial */
#define CONFIG_ENV_OVERWRITE

//#define CONFIG_COMMANDS	(CONFIG_CMD_DFL | CFG_CMD_JFFS2)

/* this must be included AFTER the definition of CONFIG_COMMANDS (if any) */
#include <cmd_confdefs.h>

#define CONFIG_BAUDRATE		115200
#define CONFIG_BOOTDELAY	3
#define CONFIG_BOOTARGS    	"noinitrd root=/dev/ram0 rw init=/linuxrc earlyprintk=serial console=ttyS0"
#define CONFIG_ETHADDR		00:02:03:04:05:06
#define CONFIG_NETMASK      255.255.255.0
#define CONFIG_IPADDR		192.168.1.10
#define CONFIG_SERVERIP		192.168.1.34
#define CONFIG_BOOTFILE		"vmlinuz"
#define CONFIG_BOOT_FROM	"flash"
#define CONFIG_BOOTCOMMAND	"bootm 20000"
#define CONFIG_BOARDREVISION "F_G2_P2_1PORT_BOARD"

//#if (CONFIG_COMMANDS & CFG_CMD_KGDB)
//#define CONFIG_KGDB_BAUDRATE	115200		/* speed to run kgdb serial port */
/* what's this ? it's not used anywhere */
//#define CONFIG_KGDB_SER_INDEX	1		/* which serial port to use */
//#endif
/*
 * Miscellaneous configurable options
 */
#define	CFG_LONGHELP				/* undef to save memory		*/
#define	CFG_PROMPT		"SiTel # "	/* Monitor Command Prompt	*/
#define	CFG_CBSIZE		256		/* Console I/O Buffer Size	*/
#define	CFG_PBSIZE (CFG_CBSIZE+sizeof(CFG_PROMPT)+16) /* Print Buffer Size */
#define	CFG_MAXARGS		16		/* max number of command args	*/
#define CFG_BARGSIZE		CFG_CBSIZE	/* Boot Argument Buffer Size	*/


#define CFG_MEMTEST_START	0x20000	/* memtest works on	*/
#define CFG_MEMTEST_END		0x00FEF000	/* 16 MB in DRAM - 7 for test,last is config and bootloader data*/

#undef  CFG_CLKS_IN_HZ		/* everything, incl board info, in Hz */

#define	CFG_LOAD_ADDR		0xa00000	/* default load address	*/

#ifndef CR16CP_CLK
#error config_sc14452dk_rA_char.h: CR16CP_CLK should have been defined 
#endif
//vm
#if (CR16CP_CLK == 82944000)
#define	CFG_HZ			1152000     /* 115.2 KHz*/
#else
#error config_sc14452dk_rA_char.h: CR16CP_CLK has an invalid value
#endif

/* valid baudrates */
#define CFG_BAUDRATE_TABLE	{ 9600, 19200, 57600, 115200,230400 }

#ifndef __ASSEMBLY__
/*-----------------------------------------------------------------------
 * Board specific extension for bd_info
 *
 * This structure is embedded in the global bd_info (bd_t) structure
 * and can be used by the board specific code (eg board/...)
 */

struct bd_info_ext
{
    /* helper variable for board environment handling
     *
     * env_crc_valid == 0    =>   uninitialised
     * env_crc_valid  > 0    =>   environment crc in flash is valid
     * env_crc_valid  < 0    =>   environment crc in flash is invalid
     */
     int	env_crc_valid;
};
#endif

/*-----------------------------------------------------------------------
 * Stack sizes
 *
 * The stack sizes are set up in start.S using the settings below
 */
#define CONFIG_STACKSIZE	(128*1024)	/* regular stack */
#ifdef CONFIG_USE_IRQ
#define CONFIG_STACKSIZE_IRQ	(4*1024)	/* IRQ stack */
#define CONFIG_STACKSIZE_FIQ	(4*1024)	/* FIQ stack */
#endif

/*-----------------------------------------------------------------------
 * Physical Memory Map
 */

#define CONFIG_NR_DRAM_BANKS    1          /* we have 1 bank of SDRAM*/
#define PHYS_SDRAM_1            0x00020000 /* SDRAM Bank #1 */
#define PHYS_SDRAM_1_SIZE       0x00FCF000 /* 16MB */
#define PHYS_SDRAM_2            0x00000000 /* There isn't a second bank */
#define PHYS_SDRAM_2_SIZE       0x00000000 /* No size for second bank */



/* Device drivers */
#define F_G2_P2_1PORT_BOARD



#define CONFIG_LCD_BASE	0x1300000
/* char lcd */
/* graph lcd support */
#define CONFIG_GRAPH_LCD
#define LCD_192_WIDE
#define DISPLAY_LCD_MESG

#define CONFIG_GRAPH_LCD_BASE	0x1300000
/* Ethernet support */
/* latch */
#define CONFIG_KEYB_LATCH_BASE	0x1400000

#define CONFIG_LED_BASE	        0x1500000
/*-----------------------------------------------------------------------
 * FLASH and environment organization
 */

//#define CONFIG_FLASH_IS_QUAD_SPI
#define CONFIG_FLASH_IS_SPI

 
#define CONFIG_NR_FLASH_BANKS	1	   /* we have 1 bank of FLASH */
#define PHYS_FLASH_1		0x00000000 /* Flash Bank #1 */
#define PHYS_FLASH_SIZE		0x00400000 /* 4 MB  */
#define PHYS_FLASH_SECTOR_SIZE	0x00001000 //0x00010000 /* 4 KB */

#define CFG_FLASH_BASE		PHYS_FLASH_1

#define CFG_MAX_FLASH_BANKS	1		/* max number of memory banks */
#define CFG_MAX_FLASH_SECT	(1024)	/* max number of sectors on one chip */

/* timeout values are in ticks */
#define CFG_FLASH_ERASE_TOUT	(5*CFG_HZ) /* Timeout for Flash Erase */
#define CFG_FLASH_WRITE_TOUT	(5*CFG_HZ) /* Timeout for Flash Write */

#define CFG_ENV_ADDR			 0x003FE000	/* Addr of Environment Sector (Fist Flash Bank 134 Sec)	*/
#define CFG_ENV_SIZE           0x2000	/* Total Size of Environment Sector */

#define CFG_HWCFG_ADDR			 0x003FC000	/* Addr of HW configuration area	*/
#define CFG_HWCFG_SIZE           0x2000	/* Total Size of HW configuration area */


#define CFG_MIN_IMAGE_SIZE	0x80000
#define CFG_MAX_IMAGE_SIZE  0x3d0000 //0x300000
#define CFG_CONFIG_PARAMETERS_ADDR	0x20000



/* Flash banks JFFS2 should use */
#define CFG_JFFS2_FIRST_BANK    0
#define CFG_JFFS2_FIRST_SECTOR	800
#define CFG_JFFS2_NUM_BANKS     1


#endif	/* __CONFIG_H */
