// File created by Gigaset Elements GmbH
// All rights reserved.
/** @file config_sc14452reef_32MB.h
    @addtogroup gcr16boot CR16 Boot
 */
/*
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 * Gary Jennejohn <gj@denx.de>
 *
 * Configuation settings for the SAMSUNG board.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __SC14452REEF_32MB_CONFIG_SERVICE_H
#define __SC14452REEF_32MB_CONFIG_SERVICE_H

#include "config_sc14452reef_32MB.h"

#undef CONFIG_SYSTEM_LOCK_ENV
#undef CONFIG_SYSTEM_LOCK_DEF

#define CONFIG_SYSTEM_LOCK_ENV          "system_locked" ///< (env|export)env variable
#define CONFIG_SYSTEM_LOCK_DEF          "false"                  ///< (true|false)default value. When "true", serial console is disabled. Can be unlocked by ethernet frame.

#define SERVICE_UBOOT



#endif	/* __SC14452REEF_32MB_CONFIG_SERVICE_H */
