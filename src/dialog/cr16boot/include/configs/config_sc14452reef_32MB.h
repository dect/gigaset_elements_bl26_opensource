// File created by Gigaset Elements GmbH
// All rights reserved.
/** @file config_sc14452reef_32MB.h
    @addtogroup gcr16boot CR16 Boot
 */
/*
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 * Gary Jennejohn <gj@denx.de>
 *
 * Configuation settings for the SAMSUNG board.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __SC14452REEF_32MB_CONFIG_H
#define __SC14452REEF_32MB_CONFIG_H

#include "sitel_io.h"
#include "config_sc14452reef.h"

#undef CONFIG_BOARDREVISION
#define CONFIG_BOARDREVISION "RevB"

#undef CFG_MEMTEST_START
#undef CFG_MEMTEST_END
#define CFG_MEMTEST_START	0x20000	/* memtest works on	*/
#define CFG_MEMTEST_END		0x00FEF000	/* 16MB in DRAM last is config and bootloader data*/


/*-----------------------------------------------------------------------
 * Physical Memory Map
 */

#undef CONFIG_NR_DRAM_BANKS
#undef PHYS_SDRAM_1
#undef PHYS_SDRAM_1_SIZE
#undef PHYS_SDRAM_2
#undef PHYS_SDRAM_2_SIZE

#define CONFIG_NR_DRAM_BANKS    2          /* we have 2 banks of SDRAM*/
#define PHYS_SDRAM_1            0x00020000 /* SDRAM Bank #1 	   131 072             */
#define PHYS_SDRAM_1_SIZE       0x00FCF000 /* 16MB - hole 		16 576 512 = 15,8 MB   */
//#define PHYS_SDRAM_2            0x010A0000 /* SDRAM Bank #2		17 432 576 = 16,6 MB   */
//#define PHYS_SDRAM_2_SIZE       0x00F60000 /* 16MB - hole       16 121 856 = 15,36 MB  */
#define PHYS_SDRAM_2            0x01080000 /* SDRAM Bank #2		17 432 576 = 16,6 MB   */
#define PHYS_SDRAM_2_SIZE       0x00F80000 /* 16MB - hole       16 121 856 = 15,36 MB  */

// 32MB is enabled in loader
#if 0
#undef PROTECT_OVER_16M_SDRAM_ACEESS
#undef RESTORE_OVER_16M_SDRAM_ACEESS
#define PROTECT_OVER_16M_SDRAM_ACEESS	 SetWord(EBI_ACS4_CTRL_REG,9);			// ext memory 16MB
#define RESTORE_OVER_16M_SDRAM_ACEESS	 {	SetWord( P2_07_MODE_REG,0x031c );	/* P2_07 is AD13 */\
											SetWord(EBI_ACS4_CTRL_REG,0xa);		/* ext memory 32MB*/\
											SetDword( EBI_SDCONR_REG,0x00001188 ); }

#endif

#endif	/* __SC14452REEF_32MB_CONFIG_H */
