/** @file config_sc14452reef.h
    @addtogroup gcr16boot_config CR16 Boot config
    @ingroup gcr16boot
 */
/*
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 * Gary Jennejohn <gj@denx.de>
 *
 * Configuation settings for the SAMSUNG board.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __SC14452REEF_CONFIG_H
#define __SC14452REEF_CONFIG_H

#include "sitel_io.h"

/*
 * If we are developing, we might want to start armboot from ram
 * so we MUST NOT initialize critical regs like mem-timing ...
 */

/*
 * High Level Configuration Options
 * (easy to change)
 */
#define CONFIG_SC14452			1	/* This is an SC14450 SoC	*/
#define CONFIG_SC14452DK		1	/* on a Board */

//#undef CONFIG_USE_IRQ			/* we don't need IRQ/FIQ stuff */
#define CONFIG_USE_IRQ

/*
 * Size of malloc() pool
 */
#define CONFIG_MALLOC_SIZE	(CFG_ENV_SIZE + 128*1024)

/*
 * Hardware drivers : none
 */

/*
 * select serial console configuration
 */

/* allow to overwrite serial */
#define CONFIG_ENV_OVERWRITE

//#define CONFIG_COMMANDS	(CONFIG_CMD_DFL | CFG_CMD_JFFS2)
#define CONFIG_COMMANDS	(CONFIG_CMD_DFL | CFG_CMD_DHCP)

/* this must be included AFTER the definition of CONFIG_COMMANDS (if any) */
#include <cmd_confdefs.h>
 /**
  * @addtogroup gcr16boot_config CR16 Boot config
  * @ingroup gcr16boot
  * @{
  */
#define CONFIG_BAUDRATE		115200
#define CONFIG_BOOTDELAY	1						///< (env) how long wait for keypress (in seconds)
#define CONFIG_BOOTARGS    	"noinitrd root=/dev/ram0 rw init=/linuxrc earlyprintk=serial console=ttyS0" ///< (env) linux kernel default command line
#define CONFIG_ETHADDR		"02:4e:ef:"__TIME__		///< (env) in format aa:bb:cc:11:22:33 without quotation marks
#define CONFIG_NETMASK      255.255.255.0			///< (env) device IP addres for TFTP operations
#define CONFIG_IPADDR		192.168.1.10			///< (env) device IP addres for TFTP operations
#define CONFIG_SERVERIP		192.168.1.34			///< (env) TFTP server address
#define CONFIG_BOOTFILE		"vmlinuz"				///< (env) TFTP default file name

#define CONFIG_BOOT_FROM			"flash"			///< (env) "flash" or "tftp-eth0"
#define CONFIG_BOOTCOMMAND			"bootm E8000"	///< (env) default boot command - used when no CFG_ALT_BOOT_POS feature
#define CONFIG_BOARDREVISION		"RevB"
#define CONFIG_BOOT_RETRY_TIME		60				///< in seconds
//#define CONFIG_REEF_STOP			"true"			///< (env) if defined, "reef_stop=<val>" is set in default environment
#define CONFIG_GIGA94_SUPPORT       1               ///< Giga94 support enabled
//#define DEBUG						1				///< prints debug info when _defined_

//===============================================================
//= feature: system lock
#define CONFIG_SYSTEM_LOCK_ENV		"system_locked"	///< (env|export)env variable
#define CONFIG_SYSTEM_LOCK_DEF		"true"			///< (true|false)default value. When "true", serial console is disabled. Can be unlocked by ethernet frame.


//===============================================================
//= feature: alternative boot positions
#define CFG_ALT_BOOT_POS	 		1			///< (feat) (0|1) support for more than one boot position
#if CFG_ALT_BOOT_POS
#define CONFIG_1ST_BOOT_POS			E8000		///< (env) first image location (hex without quotation marks)
#define CONFIG_2ND_BOOT_POS			46F000		///< (env) second image location
#define CONFIG_BOOT_FROM_IMAGE_NO	1			///< (env) which image should be booted first "1" or "2" or "R" (recovery)
#define CFG_RECOVERY				1			///< (feat) (0|1) support for recovery image
	#define CONFIG_REC_BOOT_POS		20000		///< (env) recovery image location
	#define CFG_FORCE_RECOVERY		1			///< (feat) (0|1) boot recovery image when both images fails

#define CONFIG_DEVICEID				"deviceid"	///< (export) if defined, device id will be exported as hex string
	#define FACTORY_DATA_OFFSET			0x7F6000
	#define DEVICE_ID_OFFSET			6			///< 16 bytes device id starts after mac address
	#define CONFIG_DEVICEID_OFFSET		(FACTORY_DATA_OFFSET+DEVICE_ID_OFFSET)	///< location in flash of device id
	#define CONFIG_DEVICEID_SIZE		16			///< length of device id (in bytes)
#endif
#define CONFIG_BOOTED_FROM			"booted_from"	///< (export) if defined, "booted_form=(1|2|R)" will be exported to Linux
#define CONFIG_MAC_OFFSET			0x7F6000			///< (feat) location of factory mac address
#define CONFIG_MAC_SIZE				6

//===============================================================


//===============================================================
//= feature: longpress button detection
#define CFG_BUTTON_DETECTION		1			///< (feat) (0|1) enable button driven behavior
#if (CFG_BUTTON_DETECTION)
/** (export) if defined CONFIG_FACTORY_RESET variable will be exported to linux environment but not stored in u-boot's env settings */
	#define CONFIG_FACTORY_RESET		"factory_reset"
	#define CFG_LONGPRESS_RESET			(10*10)	/// (feat) 10 seconds in 100ms ticks
	#define CFG_LONGPRESS_RECOVERY		(30*10)	/// (feat) 30 seconds in 100ms ticks
	#define CFG_LONGPRESS_MAXIMUM_WAIT	(32*10)	/// (feat) when button is pressed wait maximum 6 seconds
#endif
//===============================================================
/**
 * @}
 */

/* not used in Giggaset's REEF */
//#define CONFIG_BOOTCOMMAND_PROD		"bootm 46F000"
//#define CONFIG_DUAL_IMAGE_BOOT
//#define CONFIG_BOOT_POS				E8000	///< address in hex
//#define CONFIG_UPGRADE_POS			46F000	///< address in hex
//#define CONFIG_PHONE_PORT    1
//CONFIG_PREBOOT		///< command to execute when uboot starts main_loop

//#if (CONFIG_COMMANDS & CFG_CMD_KGDB)
//#define CONFIG_KGDB_BAUDRATE	115200		/* speed to run kgdb serial port */
/* what's this ? it's not used anywhere */
//#define CONFIG_KGDB_SER_INDEX	1		/* which serial port to use */
//#endif
/*
 * Miscellaneous configurable options
 */
#define	CFG_LONGHELP				/* undef to save memory		*/
#define	CFG_PROMPT		"Gigaset # "	/* Monitor Command Prompt	*/
#define	CFG_CBSIZE		256		/* Console I/O Buffer Size	*/
#define	CFG_PBSIZE		(CFG_CBSIZE+sizeof(CFG_PROMPT)+16) /* Print Buffer Size */
#define	CFG_MAXARGS		16		/* max number of command args	*/
#define CFG_BARGSIZE	CFG_CBSIZE	/* Boot Argument Buffer Size	*/


#define CFG_MEMTEST_START	0x20000	/* memtest works on	*/
#define CFG_MEMTEST_END		0x00FEF000	/* 16 MB in DRAM - 7 for test,last is config and bootloader data*/

#undef  CFG_CLKS_IN_HZ		/* everything, incl board info, in Hz */

#define	CFG_LOAD_ADDR		0xa00000	/* default load address	*/

#ifndef CR16CP_CLK
#error config_sc14452dk_rA_char.h: CR16CP_CLK should have been defined 
#endif
//vm
#if (CR16CP_CLK == 82944000)
#define	CFG_HZ			1152000     /* 115.2 KHz*/
#else
#error config_sc14452dk_rA_char.h: CR16CP_CLK has an invalid value
#endif

/* valid baudrates */
#define CFG_BAUDRATE_TABLE	{ 9600, 19200, 57600, 115200,230400 }

#ifndef __ASSEMBLY__
/*-----------------------------------------------------------------------
 * Board specific extension for bd_info
 *
 * This structure is embedded in the global bd_info (bd_t) structure
 * and can be used by the board specific code (eg board/...)
 */

struct bd_info_ext
{
    /* helper variable for board environment handling
     *
     * env_crc_valid == 0    =>   uninitialised
     * env_crc_valid  > 0    =>   environment crc in flash is valid
     * env_crc_valid  < 0    =>   environment crc in flash is invalid
     */
     int	env_crc_valid;

 #ifdef CONFIG_FACTORY_RESET
	 unsigned char ucFactoryResetFlag;
 #endif
 #ifdef CONFIG_DEVICEID
	 unsigned char aucDeviceId[CONFIG_DEVICEID_SIZE];
 #endif
 #ifdef CONFIG_BOOTED_FROM
	 char cBootedFrom;
 #endif
 #ifdef CFG_LONGPRESS_RECOVERY
	 unsigned char ucRecoveryModeFlag;
 #endif
 #ifdef	CONFIG_SYSTEM_LOCK_ENV
	 unsigned char ucSystemLocked;
 #endif
#ifdef CONFIG_GIGA94_SUPPORT
	 unsigned char ucGiga94On;
	 unsigned char ucEthLoopbackTestResult;
#endif
};
#endif

/*-----------------------------------------------------------------------
 * Stack sizes
 *
 * The stack sizes are set up in start.S using the settings below
 */
#define CONFIG_STACKSIZE	(128*1024)	/* regular stack */
#ifdef CONFIG_USE_IRQ
#define CONFIG_STACKSIZE_IRQ	(4*1024)	/* IRQ stack */
#define CONFIG_STACKSIZE_FIQ	(4*1024)	/* FIQ stack */
#endif

/*-----------------------------------------------------------------------
 * Physical Memory Map
 */

#define CONFIG_NR_DRAM_BANKS    1          /* we have 1 bank of SDRAM*/
#define PHYS_SDRAM_1            0x00020000 /* SDRAM Bank #1 */
#define PHYS_SDRAM_1_SIZE       0x00FCF000 /* 16MB */
#define PHYS_SDRAM_2            0x00000000 /* There isn't a second bank */
#define PHYS_SDRAM_2_SIZE       0x00000000 /* No size for second bank */

#define PROTECT_OVER_16M_SDRAM_ACEESS
#define RESTORE_OVER_16M_SDRAM_ACEESS


/* Device drivers */
#define SC14452DK_ule
#define NO_DISPLAY
#define ETH_PHY_ADDR 0
#define REEF
/*-----------------------------------------------------------------------
 * FLASH and environment organization
 */

//#define CONFIG_FLASH_IS_QUAD_SPI
#define CONFIG_FLASH_IS_SPI

 
#define CONFIG_NR_FLASH_BANKS	1	   /* we have 1 bank of FLASH */
#define PHYS_FLASH_1		0x00000000 /* Flash Bank #1 */
#define PHYS_FLASH_SIZE_4MB		0x00400000 /* 4 MB  */
//for 8Mbyte
#define PHYS_FLASH_SIZE_8MB		0x00800000 /* 8 MB  */
//for 8Mbyte
//#define PHYS_FLASH_SIZE		0x00800000 /* 4 MB  */

#define PHYS_FLASH_SECTOR_SIZE	0x00001000 //0x00010000 /* 4 KB */

#define CFG_FLASH_BASE		PHYS_FLASH_1

#define CFG_MAX_FLASH_BANKS	1		/* max number of memory banks */
#define CFG_MAX_FLASH_SECT_4MB	(1024)	/* max number of sectors on one chip */
//for 8Mbyte
#define CFG_MAX_FLASH_SECT_8MB	(2048)	/* max number of sectors on one chip */

/* timeout values are in ticks */
#define CFG_FLASH_ERASE_TOUT	(5*CFG_HZ) /* Timeout for Flash Erase */
#define CFG_FLASH_WRITE_TOUT	(5*CFG_HZ) /* Timeout for Flash Write */
#define CFG_ENV_ADDR_4MB			 0x003FE000	/* Addr of Environment Sector (Fist Flash Bank 134 Sec)	*/
//for 8Mbyte
#define CFG_ENV_ADDR_8MB			 0x007FE000	/* Addr of Environment Sector (Fist Flash Bank 134 Sec)	*/
#define CFG_ENV_SIZE           		 0x2000	/* Total Size of Environment Sector */
#define CFG_HWCFG_ADDR_4MB			 0x003FC000	/* Addr of HW configuration area	*/
//for 8Mbyte
#define CFG_HWCFG_ADDR_8MB			 0x007F6000	/* Addr of HW configuration area	*/
#define CFG_HWCFG_SIZE          	 0x8000	/* Total Size of HW configuration area */

extern unsigned long CFG_ENV_ADDR;
extern unsigned long CFG_HWCFG_ADDR;
extern unsigned long PHYS_FLASH_SIZE;

#define CFG_MIN_IMAGE_SIZE			0x80000
#define CFG_MAX_IMAGE_SIZE  		0x3d0000 //0x300000
#define CFG_CONFIG_PARAMETERS_ADDR	0x20000



/* Flash banks JFFS2 should use */
#define CFG_JFFS2_FIRST_BANK    0
#define CFG_JFFS2_FIRST_SECTOR	800
#define CFG_JFFS2_NUM_BANKS     1

/* REEF board LEDs */
#define BLUE    GPIO_8
#define GREEN   GPIO_11
#define YELLOW  GPIO_12
extern void vLEDControl (uint16 port_id, uint8 state);


#endif	/* __SC14452REEF_CONFIG_H */

//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:49.748887707
//@@ -1,3 +1,7 @@
//+/** @file config_sc14452reef.h
//+    @addtogroup gcr16boot_config CR16 Boot config
//+    @ingroup gcr16boot
//+ */
// /*
//  * (C) Copyright 2002
//  * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
//@@ -25,8 +29,8 @@
//  * MA 02111-1307 USA
//  */
// 
//-#ifndef __CONFIG_H
//-#define __CONFIG_H
//+#ifndef __SC14452REEF_CONFIG_H
//+#define __SC14452REEF_CONFIG_H
// 
// #include "sitel_io.h"
// 
//@@ -43,7 +47,7 @@
// #define CONFIG_SC14452DK		1	/* on a Board */
// 
// //#undef CONFIG_USE_IRQ			/* we don't need IRQ/FIQ stuff */
//-#define CONFIG_USE_IRQ			/* we don't need IRQ/FIQ stuff */
//+#define CONFIG_USE_IRQ
// 
// /*
//  * Size of malloc() pool
//@@ -62,22 +66,84 @@
// #define CONFIG_ENV_OVERWRITE
// 
// //#define CONFIG_COMMANDS	(CONFIG_CMD_DFL | CFG_CMD_JFFS2)
//+#define CONFIG_COMMANDS	(CONFIG_CMD_DFL | CFG_CMD_DHCP)
// 
// /* this must be included AFTER the definition of CONFIG_COMMANDS (if any) */
// #include <cmd_confdefs.h>
//-
//+ /**
//+  * @addtogroup gcr16boot_config CR16 Boot config
//+  * @ingroup gcr16boot
//+  * @{
//+  */
// #define CONFIG_BAUDRATE		115200
//-#define CONFIG_BOOTDELAY	3
//-#define CONFIG_BOOTARGS    	"noinitrd root=/dev/ram0 rw init=/linuxrc earlyprintk=serial console=ttyS0"
//-#define CONFIG_ETHADDR		00:02:03:04:05:06
//-#define CONFIG_NETMASK      255.255.255.0
//-#define CONFIG_IPADDR		192.168.1.10
//-#define CONFIG_SERVERIP		192.168.1.34
//-#define CONFIG_BOOTFILE		"vmlinuz"
//-#define CONFIG_BOOT_FROM	"flash"
//-#define CONFIG_BOOTCOMMAND	"bootm 20000"
//-#define CONFIG_BOARDREVISION "RevB"
//-#define CONFIG_PHONE_PORT    1
//+#define CONFIG_BOOTDELAY	1						///< (env) how long wait for keypress (in seconds)
//+#define CONFIG_BOOTARGS    	"noinitrd root=/dev/ram0 rw init=/linuxrc earlyprintk=serial console=ttyS0" ///< (env) linux kernel default command line
//+#define CONFIG_ETHADDR		"02:4e:ef:"__TIME__		///< (env) in format aa:bb:cc:11:22:33 without quotation marks
//+#define CONFIG_NETMASK      255.255.255.0			///< (env) device IP addres for TFTP operations
//+#define CONFIG_IPADDR		192.168.1.10			///< (env) device IP addres for TFTP operations
//+#define CONFIG_SERVERIP		192.168.1.34			///< (env) TFTP server address
//+#define CONFIG_BOOTFILE		"vmlinuz"				///< (env) TFTP default file name
//+
//+#define CONFIG_BOOT_FROM			"flash"			///< (env) "flash" or "tftp-eth0"
//+#define CONFIG_BOOTCOMMAND			"bootm E8000"	///< (env) default boot command - used when no CFG_ALT_BOOT_POS feature
//+#define CONFIG_BOARDREVISION		"RevB"
//+#define CONFIG_BOOT_RETRY_TIME		60				///< in seconds
//+//#define CONFIG_REEF_STOP			"true"			///< (env) if defined, "reef_stop=<val>" is set in default environment
//+#define CONFIG_GIGA94_SUPPORT       1               ///< Giga94 support enabled
//+//#define DEBUG						1				///< prints debug info when _defined_
//+
//+//===============================================================
//+//= feature: system lock
//+#define CONFIG_SYSTEM_LOCK_ENV		"system_locked"	///< (env|export)env variable
//+#define CONFIG_SYSTEM_LOCK_DEF		"true"			///< (true|false)default value. When "true", serial console is disabled. Can be unlocked by ethernet frame.
//+
//+
//+//===============================================================
//+//= feature: alternative boot positions
//+#define CFG_ALT_BOOT_POS	 		1			///< (feat) (0|1) support for more than one boot position
//+#if CFG_ALT_BOOT_POS
//+#define CONFIG_1ST_BOOT_POS			E8000		///< (env) first image location (hex without quotation marks)
//+#define CONFIG_2ND_BOOT_POS			46F000		///< (env) second image location
//+#define CONFIG_BOOT_FROM_IMAGE_NO	1			///< (env) which image should be booted first "1" or "2" or "R" (recovery)
//+#define CFG_RECOVERY				1			///< (feat) (0|1) support for recovery image
//+	#define CONFIG_REC_BOOT_POS		20000		///< (env) recovery image location
//+	#define CFG_FORCE_RECOVERY		1			///< (feat) (0|1) boot recovery image when both images fails
//+
//+#define CONFIG_DEVICEID				"deviceid"	///< (export) if defined, device id will be exported as hex string
//+	#define FACTORY_DATA_OFFSET			0x7F6000
//+	#define DEVICE_ID_OFFSET			6			///< 16 bytes device id starts after mac address
//+	#define CONFIG_DEVICEID_OFFSET		(FACTORY_DATA_OFFSET+DEVICE_ID_OFFSET)	///< location in flash of device id
//+	#define CONFIG_DEVICEID_SIZE		16			///< length of device id (in bytes)
//+#endif
//+#define CONFIG_BOOTED_FROM			"booted_from"	///< (export) if defined, "booted_form=(1|2|R)" will be exported to Linux
//+#define CONFIG_MAC_OFFSET			0x7F6000			///< (feat) location of factory mac address
//+#define CONFIG_MAC_SIZE				6
//+
//+//===============================================================
//+
//+
//+//===============================================================
//+//= feature: longpress button detection
//+#define CFG_BUTTON_DETECTION		1			///< (feat) (0|1) enable button driven behavior
//+#if (CFG_BUTTON_DETECTION)
//+/** (export) if defined CONFIG_FACTORY_RESET variable will be exported to linux environment but not stored in u-boot's env settings */
//+	#define CONFIG_FACTORY_RESET		"factory_reset"
//+	#define CFG_LONGPRESS_RESET			(10*10)	/// (feat) 10 seconds in 100ms ticks
//+	#define CFG_LONGPRESS_RECOVERY		(30*10)	/// (feat) 30 seconds in 100ms ticks
//+	#define CFG_LONGPRESS_MAXIMUM_WAIT	(32*10)	/// (feat) when button is pressed wait maximum 6 seconds
//+#endif
//+//===============================================================
//+/**
//+ * @}
//+ */
//+
//+/* not used in Giggaset's REEF */
//+//#define CONFIG_BOOTCOMMAND_PROD		"bootm 46F000"
//+//#define CONFIG_DUAL_IMAGE_BOOT
//+//#define CONFIG_BOOT_POS				E8000	///< address in hex
//+//#define CONFIG_UPGRADE_POS			46F000	///< address in hex
//+//#define CONFIG_PHONE_PORT    1
//+//CONFIG_PREBOOT		///< command to execute when uboot starts main_loop
// 
// //#if (CONFIG_COMMANDS & CFG_CMD_KGDB)
// //#define CONFIG_KGDB_BAUDRATE	115200		/* speed to run kgdb serial port */
//@@ -88,11 +154,11 @@
//  * Miscellaneous configurable options
//  */
// #define	CFG_LONGHELP				/* undef to save memory		*/
//-#define	CFG_PROMPT		"SiTel # "	/* Monitor Command Prompt	*/
//+#define	CFG_PROMPT		"Gigaset # "	/* Monitor Command Prompt	*/
// #define	CFG_CBSIZE		256		/* Console I/O Buffer Size	*/
//-#define	CFG_PBSIZE (CFG_CBSIZE+sizeof(CFG_PROMPT)+16) /* Print Buffer Size */
//+#define	CFG_PBSIZE		(CFG_CBSIZE+sizeof(CFG_PROMPT)+16) /* Print Buffer Size */
// #define	CFG_MAXARGS		16		/* max number of command args	*/
//-#define CFG_BARGSIZE		CFG_CBSIZE	/* Boot Argument Buffer Size	*/
//+#define CFG_BARGSIZE	CFG_CBSIZE	/* Boot Argument Buffer Size	*/
// 
// 
// #define CFG_MEMTEST_START	0x20000	/* memtest works on	*/
//@@ -132,6 +198,26 @@
//      * env_crc_valid  < 0    =>   environment crc in flash is invalid
//      */
//      int	env_crc_valid;
//+
//+ #ifdef CONFIG_FACTORY_RESET
//+	 unsigned char ucFactoryResetFlag;
//+ #endif
//+ #ifdef CONFIG_DEVICEID
//+	 unsigned char aucDeviceId[CONFIG_DEVICEID_SIZE];
//+ #endif
//+ #ifdef CONFIG_BOOTED_FROM
//+	 char cBootedFrom;
//+ #endif
//+ #ifdef CFG_LONGPRESS_RECOVERY
//+	 unsigned char ucRecoveryModeFlag;
//+ #endif
//+ #ifdef	CONFIG_SYSTEM_LOCK_ENV
//+	 unsigned char ucSystemLocked;
//+ #endif
//+#ifdef CONFIG_GIGA94_SUPPORT
//+	 unsigned char ucGiga94On;
//+	 unsigned char ucEthLoopbackTestResult;
//+#endif
// };
// #endif
// 
//@@ -156,6 +242,8 @@
// #define PHYS_SDRAM_2            0x00000000 /* There isn't a second bank */
// #define PHYS_SDRAM_2_SIZE       0x00000000 /* No size for second bank */
// 
//+#define PROTECT_OVER_16M_SDRAM_ACEESS
//+#define RESTORE_OVER_16M_SDRAM_ACEESS
// 
// 
// /* Device drivers */
//@@ -173,7 +261,9 @@
//  
// #define CONFIG_NR_FLASH_BANKS	1	   /* we have 1 bank of FLASH */
// #define PHYS_FLASH_1		0x00000000 /* Flash Bank #1 */
//-#define PHYS_FLASH_SIZE		0x00400000 /* 4 MB  */
//+#define PHYS_FLASH_SIZE_4MB		0x00400000 /* 4 MB  */
//+//for 8Mbyte
//+#define PHYS_FLASH_SIZE_8MB		0x00800000 /* 8 MB  */
// //for 8Mbyte
// //#define PHYS_FLASH_SIZE		0x00800000 /* 4 MB  */
// 
//@@ -182,25 +272,28 @@
// #define CFG_FLASH_BASE		PHYS_FLASH_1
// 
// #define CFG_MAX_FLASH_BANKS	1		/* max number of memory banks */
//-#define CFG_MAX_FLASH_SECT	(1024)	/* max number of sectors on one chip */
//+#define CFG_MAX_FLASH_SECT_4MB	(1024)	/* max number of sectors on one chip */
// //for 8Mbyte
//-//#define CFG_MAX_FLASH_SECT	(2048)	/* max number of sectors on one chip */
//+#define CFG_MAX_FLASH_SECT_8MB	(2048)	/* max number of sectors on one chip */
// 
// /* timeout values are in ticks */
// #define CFG_FLASH_ERASE_TOUT	(5*CFG_HZ) /* Timeout for Flash Erase */
// #define CFG_FLASH_WRITE_TOUT	(5*CFG_HZ) /* Timeout for Flash Write */
//-#define CFG_ENV_ADDR			 0x003FE000	/* Addr of Environment Sector (Fist Flash Bank 134 Sec)	*/
//+#define CFG_ENV_ADDR_4MB			 0x003FE000	/* Addr of Environment Sector (Fist Flash Bank 134 Sec)	*/
// //for 8Mbyte
//-//#define CFG_ENV_ADDR			 0x007FE000	/* Addr of Environment Sector (Fist Flash Bank 134 Sec)	*/
//-#define CFG_ENV_SIZE           0x2000	/* Total Size of Environment Sector */
//-#define CFG_HWCFG_ADDR			 0x003FC000	/* Addr of HW configuration area	*/
//+#define CFG_ENV_ADDR_8MB			 0x007FE000	/* Addr of Environment Sector (Fist Flash Bank 134 Sec)	*/
//+#define CFG_ENV_SIZE           		 0x2000	/* Total Size of Environment Sector */
//+#define CFG_HWCFG_ADDR_4MB			 0x003FC000	/* Addr of HW configuration area	*/
// //for 8Mbyte
//-//#define CFG_HWCFG_ADDR			 0x007FC000	/* Addr of HW configuration area	*/
//-#define CFG_HWCFG_SIZE           0x2000	/* Total Size of HW configuration area */
//+#define CFG_HWCFG_ADDR_8MB			 0x007F6000	/* Addr of HW configuration area	*/
//+#define CFG_HWCFG_SIZE          	 0x8000	/* Total Size of HW configuration area */
// 
//+extern unsigned long CFG_ENV_ADDR;
//+extern unsigned long CFG_HWCFG_ADDR;
//+extern unsigned long PHYS_FLASH_SIZE;
// 
//-#define CFG_MIN_IMAGE_SIZE	0x80000
//-#define CFG_MAX_IMAGE_SIZE  0x3d0000 //0x300000
//+#define CFG_MIN_IMAGE_SIZE			0x80000
//+#define CFG_MAX_IMAGE_SIZE  		0x3d0000 //0x300000
// #define CFG_CONFIG_PARAMETERS_ADDR	0x20000
// 
// 
//@@ -210,5 +303,12 @@
// #define CFG_JFFS2_FIRST_SECTOR	800
// #define CFG_JFFS2_NUM_BANKS     1
// 
//+/* REEF board LEDs */
//+#define BLUE    GPIO_8
//+#define GREEN   GPIO_11
//+#define YELLOW  GPIO_12
//+extern void vLEDControl (uint16 port_id, uint8 state);
//+
//+
//+#endif	/* __SC14452REEF_CONFIG_H */
// 
//-#endif	/* __CONFIG_H */
