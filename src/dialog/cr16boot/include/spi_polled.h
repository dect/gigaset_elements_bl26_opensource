/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <vassilis.maniotis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 * See http://www.gnu.org/licenses/gpl-2.0.txt for more details.
 */

#ifndef SPI_POLLED_H
#define SPI_POLLED_H

/*========================== Include files ==================================*/


/*========================== Local macro definitions & typedefs =============*/
#define CLK_1296    0x00
#define CLK_2592    0x01
#define CLK_5184    0x02  
#define CLK_1152    0x03  

#define MODE_0     0x00
#define MODE_1     0x01
#define MODE_2     0x02
#define MODE_3     0x03

#define MODE_8BITS  0x00
#define MODE_16BITS 0x01
#define MODE_32BITS 0x02

#define MASTER      0x00
#define SLAVE       0x01

#define READY       0x00
#define BUSY        0x01

#define SPI_AD_INT_PEND   (0x0040)
#define SPI_BUSY          (0x0001)  /* Set P0_0 as handshake SPI_EN Pin */

/*========================== Global variabeles ==============================*/


/*========================== Global function prototypes =====================*/
void init_SPI2(uint8 freq, uint8 mode, uint8 bits);
void reset_SPI2(void);
bool getByte_SPI2( uint32 *byte );
bool putByte_SPI2( uint32 byte );
bool putData_SPI2(uint32 *data, uint32 cnt);
bool getData_SPI2(uint32 *data, uint32 cnt);

void masterHandshake_SPI(void);

#endif /* SPI_IRQ_H */

