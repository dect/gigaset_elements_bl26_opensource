
#ifndef ILI9325_H
#define ILI9325_H



#define LCD_CTRL_ADDR CONFIG_LCD_BASE 		//0x1300000
#define LCD_DATA_ADDR CONFIG_LCD_BASE+0x80	//0x1300080

#define lcd_ctrl_write(x) *(volatile unsigned short*)(LCD_CTRL_ADDR)=x
#define lcd_ctrl_read	*(volatile unsigned short*)(LCD_CTRL_ADDR)

#define lcd_data_write(x) *(volatile unsigned short*)(LCD_DATA_ADDR)=x
#define lcd_data_read	*(volatile unsigned short*)(LCD_DATA_ADDR)


#define LCD_OSC_START						lcd_ctrl_write(0x00)			
#define LCD_DRIVER_OUTPUT_CTRL1				lcd_ctrl_write(0x01)
#define LCD_DRIVING_CTRL					lcd_ctrl_write(0x02)	
#define LCD_ENTRY_MODE						lcd_ctrl_write(0x03)			
#define LCD_RESIZE_CTRL						lcd_ctrl_write(0x04)			
#define LCD_DISPALY_CTRL1					lcd_ctrl_write(0x07)
#define LCD_DISPALY_CTRL2					lcd_ctrl_write(0x08)
#define LCD_DISPALY_CTRL3					lcd_ctrl_write(0x09)
#define LCD_DISPALY_CTRL4					lcd_ctrl_write(0x0a)
#define LCD_RGB_DISPLAY_INTERFACE_CTRL1		lcd_ctrl_write(0x0c)			
#define LCD_FRAME_MAKER_POS					lcd_ctrl_write(0x0d)			
#define LCD_RGB_DISPLAY_INTERFACE_CTRL2		lcd_ctrl_write(0x0f)

#define LCD_POWER_CTRL1						lcd_ctrl_write(0x10)			
#define LCD_POWER_CTRL2						lcd_ctrl_write(0x11)			
#define LCD_POWER_CTRL3						lcd_ctrl_write(0x12)			
#define LCD_POWER_CTRL4						lcd_ctrl_write(0x13)

#define LCD_HOR_GRAM_ADDR_SET				lcd_ctrl_write(0x20)			
#define LCD_VER_GRAM_ADDR_SET				lcd_ctrl_write(0x21)			
#define LCD_WRITE_DATA_GRAM					lcd_ctrl_write(0x22)			
#define LCD_POWER_CTRL7						lcd_ctrl_write(0x29)			
#define LCD_FRAME_RATE_COLOR_CTRL			lcd_ctrl_write(0x2b)

#define LCD_GAMMA_CTRL1						lcd_ctrl_write(0x30)			
#define LCD_GAMMA_CTRL2						lcd_ctrl_write(0x31)			
#define LCD_GAMMA_CTRL3						lcd_ctrl_write(0x32)			
#define LCD_GAMMA_CTRL4						lcd_ctrl_write(0x35)			
#define LCD_GAMMA_CTRL5						lcd_ctrl_write(0x36)			
#define LCD_GAMMA_CTRL6						lcd_ctrl_write(0x37)			
#define LCD_GAMMA_CTRL7						lcd_ctrl_write(0x38)			
#define LCD_GAMMA_CTRL8						lcd_ctrl_write(0x39)			
#define LCD_GAMMA_CTRL9						lcd_ctrl_write(0x3c)			
#define LCD_GAMMA_CTRL10					lcd_ctrl_write(0x3d)

#define LCD_HOR_ADDR_START_POS				lcd_ctrl_write(0x50)			
#define LCD_HOR_ADDR_END_POS				lcd_ctrl_write(0x51)			
#define LCD_VER_ADDR_START_POS				lcd_ctrl_write(0x52)			
#define LCD_VER_ADDR_END_POS				lcd_ctrl_write(0x53)

#define LCD_DRIVER_OUTPUT_CTRL2				lcd_ctrl_write(0x60)
#define LCD_BASE_IMAGE_DISPLAY_CTRL			lcd_ctrl_write(0x61)
#define LCD_VER_SCROLL_CTRL					lcd_ctrl_write(0x6a)

#define LCD_BASE_IMAGE_1_DISPLAY_POS		lcd_ctrl_write(0x80)
#define LCD_BASE_IMAGE_1_AREA_START_LINE	lcd_ctrl_write(0x81)
#define LCD_BASE_IMAGE_1_AREA_END_LINE		lcd_ctrl_write(0x82)
#define LCD_BASE_IMAGE_2_DISPLAY_POS		lcd_ctrl_write(0x83)
#define LCD_BASE_IMAGE_2_AREA_START_LINE	lcd_ctrl_write(0x84)
#define LCD_BASE_IMAGE_2_AREA_END_LINE		lcd_ctrl_write(0x85)

#define LCD_PANEL_INERFACE_CTRL1			lcd_ctrl_write(0x90)
#define LCD_PANEL_INERFACE_CTRL2			lcd_ctrl_write(0x92)
#define LCD_PANEL_INERFACE_CTRL3			lcd_ctrl_write(0x95)

#define LCD_OTP_VCM_PROGRAMMING_CTRL		lcd_ctrl_write(0xa1)
#define LCD_OTP_VCM_STATUS_ENABLE			lcd_ctrl_write(0xa2)
#define LCD_OTP_PROGRAMMING_ID_KEY			lcd_ctrl_write(0xa5)


// initializes I/O pins connected to LCD
void lcdInitHW(void);
// initializes the LCD display (gets it ready for use)
void lcdInit(void);

// moves the cursor/position to Home (upper left corner)
void lcdHome(void);

// clears the LCD display
void lcdClear(void);

// moves the cursor/position to the row,col requested
// ** this may not be accurate for all displays
void lcdGotoXY(unsigned short row, unsigned short col);

void lcdCLS(void);

#endif //ILI9325
