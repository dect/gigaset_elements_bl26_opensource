
#ifndef GRAPH_LCD_H
#define GRAPH_LCD_H


typedef struct FONT_DEF 
{
	unsigned char Width;     		/* Character width for storage         */
	unsigned char Height;  		/* Character height for storage        */
	unsigned char *FontTable;      /* Font table start address in memory  */
} FONT_DEF;

extern FONT_DEF Font_System3x6;
extern FONT_DEF Font_System5x8;
extern FONT_DEF Font_System7x8;
extern FONT_DEF Font_Courrier8x12;

extern unsigned char FontSystem3x6[];
extern unsigned char FontSystem5x8[];
extern unsigned char FontSystem7x8[];
extern unsigned char FontCourrier8x12[];



#define LCD_CTRL_ADDR CONFIG_GRAPH_LCD_BASE 		//0x1300000
#define LCD_DATA_ADDR CONFIG_GRAPH_LCD_BASE+0x80	//0x1300080

#define lcd_ctrl_write(x) *(volatile unsigned char*)(LCD_CTRL_ADDR)=x
#define lcd_status	*(volatile unsigned char*)(LCD_CTRL_ADDR)

#define lcd_data_write(x) *(volatile unsigned char*)(LCD_DATA_ADDR)=x
#define lcd_data_read	*(volatile unsigned char*)(LCD_DATA_ADDR)

#define G_LCD_DISPLAY_ON		lcd_ctrl_write(0xaf)
#define G_LCD_DISPLAY_OFF		lcd_ctrl_write(0xae)
#define G_LCD_ADC_SEL_NORMAL	lcd_ctrl_write(0xa0)
#define G_LCD_ADC_SEL_REV		lcd_ctrl_write(0xa1)
#define G_LCD_DISPLAY_NORM		lcd_ctrl_write(0xa6)
#define G_LCD_DISPLAY_REV		lcd_ctrl_write(0xa7)
#define G_LCD_ENT_DISPLAY_ON	lcd_ctrl_write(0xa4)
#define G_LCD_ENT_DISPLAY_OFF	lcd_ctrl_write(0xa5)
#define G_LCD_SET_BIAS_19		lcd_ctrl_write(0xa2)
#define G_LCD_SET_BIAS_17		lcd_ctrl_write(0xa3)
#define G_LCD_RESET				lcd_ctrl_write(0xe2)
#define G_LCD_SET_RMW			lcd_ctrl_write(0xe0)
#define G_LCD_RESET_RMW			lcd_ctrl_write(0xee)
#define G_LCD_SHL_NORMAL		lcd_ctrl_write(0xc0)
#define G_LCD_SHL_FLIPPED		lcd_ctrl_write(0xc8)
#define G_LCD_VOLUME_MODE_SET	lcd_ctrl_write(0x81)
#define G_LCD_NOP				lcd_ctrl_write(0xe3)
#define G_LCD_OSC_ON			lcd_ctrl_write(0xab)
#define G_LCD_RST_REVDRV		lcd_ctrl_write(0xe4)
#define G_LCD_PWR_SAVE_RESET	lcd_ctrl_write(0xe1)
#define G_LCD_SET_LINE			0x40
#define G_LCD_SET_PAGE			0xb0
#define G_LCD_SET_COL_LOW		0x00
#define G_LCD_SET_COL_HIGH		0x10
#define G_LCD_POWER_CTRL_SET	0x28
#define G_LCD_POWER_VC_SET		0x04
#define G_LCD_POWER_VR_SET		0x02
#define G_LCD_POWER_VF_SET		0x01
#define G_LCD_REG_RATIO			0x20

// initializes I/O pins connected to LCD
void lcdInitHW(void);
// initializes the LCD display (gets it ready for use)
void lcdInit(void);

// moves the cursor/position to Home (upper left corner)
void lcdHome(void);

// clears the LCD display
void lcdClear(void);

// moves the cursor/position to the row,col requested
// ** this may not be accurate for all displays
void lcdGotoXY(unsigned short row, unsigned short col);

// prints a series of bytes/characters to the display
void lcdPrintData(char* data, unsigned char nBytes,FONT_DEF *fontused);

void lcdCLS(void);

void graph_lcd_disp_line(unsigned short line,char* data);


#endif
