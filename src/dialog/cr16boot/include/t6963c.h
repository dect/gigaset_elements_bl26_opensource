// display properties
#define LCD_NUMBER_OF_LINES				128
#define LCD_PIXELS_PER_LINE				240
#define LCD_FONT_WIDTH					8
//
#define LCD_GRAPHIC_AREA					(LCD_PIXELS_PER_LINE / LCD_FONT_WIDTH)
#define LCD_TEXT_AREA						(LCD_PIXELS_PER_LINE / LCD_FONT_WIDTH)
#define LCD_GRAPHIC_SIZE					(LCD_GRAPHIC_AREA * LCD_NUMBER_OF_LINES)
#define LCD_TEXT_SIZE						(LCD_TEXT_AREA * (LCD_NUMBER_OF_LINES/8))


#define LCD_TEXT_HOME						0
#define LCD_GRAPHIC_HOME					(LCD_TEXT_HOME + LCD_TEXT_SIZE)
#define LCD_OFFSET_REGISTER					2
#define LCD_EXTERNAL_CG_HOME				(LCD_OFFSET_REGISTER << 11)

#define LCD_CTRL_ADDR CONFIG_T6963C_LCD_BASE + 0x80 		//0x1300080
#define LCD_DATA_ADDR CONFIG_T6963C_LCD_BASE				//0x1300000

#define LCD_SET_CURSOR_POINTER				0x21
#define LCD_SET_OFFSET_REGISTER				0x22
#define LCD_SET_ADDRESS_POINTER				0x24
#define LCD_SET_TEXT_HOME_ADDRESS			0x40
#define LCD_SET_TEXT_AREA					0x41
#define LCD_SET_GRAPHIC_HOME_ADDRESS		0x42
#define LCD_SET_GRAPHIC_AREA				0x43

#define LCD_MODE_SET						0x80
#define LCD_MODE_OR							0x80
#define LCD_MODE_EXOR						0x81
#define LCD_MODE_AND						0x83
#define LCD_MODE_TEXT_ATTR					0x84
#define LCD_MODE_INT_CG_ROM					0x80
#define LCD_MODE_EXT_CG_RAM					0x88

#define LCD_DISPLAY_OFF						0x90
#define LCD_CURSOR_ON_BLINK_OFF				0x92
#define LCD_CURSOR_ON_BLINK_ON				0x93
#define LCD_TEXT_ON_GRAPHIC_OFF				0x94
#define LCD_TEXT_OFF_GRAPHIC_ON				0x98
#define LCD_TEXT_ON_GRAPHIC_ON				0x9C

#define LCD_CURSOR_1_LINE					0xA0
#define LCD_CURSOR_2_LINE					0xA1
#define LCD_CURSOR_3_LINE					0xA2
#define LCD_CURSOR_4_LINE					0xA3
#define LCD_CURSOR_5_LINE					0xA4
#define LCD_CURSOR_6_LINE					0xA5
#define LCD_CURSOR_7_LINE					0xA6
#define LCD_CURSOR_8_LINE					0xA7

#define LCD_SET_DATA_AUTO_WRITE				0xB0
#define LCD_SET_DATA_AUTO_READ				0xB1
#define LCD_AUTO_RESET						0xB2
#define LCD_DATA_WRITE_AND_INCREMENT		0xC0
#define LCD_DATA_READ_AND_INCREMENT			0xC1
#define LCD_DATA_WRITE_AND_DECREMENT		0xC2
#define LCD_DATA_READ_AND_DECREMENT			0xC3
#define LCD_DATA_WRITE_AND_NONVARIALBE		0xC4
#define LCD_DATA_READ_AND_NONVARIABLE		0xC5
#define LCD_SCREEN_PEEK						0xE0
#define LCD_SCREEN_COPY						0xE8
#define LCD_BIT_RESET						0xF0
#define LCD_BIT_SET							0xF8

void LCD_InitalizeInterface(void);
unsigned char LCD_ChceckStatus(void);
void LCD_WriteCommand(unsigned char);
void LCD_WriteData(unsigned char);
unsigned char LCD_ReadData(void);
void LCD_ClearText(void);
void LCD_ClearCG(void);
void LCD_ClearGraphic(void);
void LCD_WriteChar(char ch);
void LCD_WriteString(char * str);
//void LCD_WriteStringPgm(prog_char * str);
void LCD_TextGoTo(unsigned char, unsigned char);
void LCD_DefineCharacter(unsigned char, unsigned char *);
void LCD_Initalize(void);
void LCD_SetPixel(unsigned char, unsigned char, unsigned char);
void LCD_WriteDisplayData(unsigned char);

