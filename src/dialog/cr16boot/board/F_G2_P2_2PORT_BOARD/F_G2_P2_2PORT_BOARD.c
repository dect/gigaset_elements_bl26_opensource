/*
 * (C) Copyright 2002
 * Kyle Harris, Nexus Technologies, Inc. kharris@nexus-tech.net
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */


#include "armboot.h"
#include "sitel_io.h"
#include "spi_polled.h"
#include "qspic.h"
/* ------------------------------------------------------------------------- */

/*
 * Miscelaneous platform dependent initialisations
 */
void lcd_reset (void);



int 
/**********************************************************/
board_post_init(bd_t *bd)
/**********************************************************/
{
   return 0;
}

int 
/**********************************************************/
board_init(bd_t *bd)
/**********************************************************/
{
	unsigned i,j;
	
	/* arch number of Sitel SC14450 DK */
	bd->bi_arch_number = 452;

	/* vm must change */ 
	/* adress of boot parameters */
	bd->bi_boot_params = CFG_CONFIG_PARAMETERS_ADDR;//0xa0000100;

	/*   Config PPA for UART. */
	SetPort(P0_00_MODE_REG, PORT_OUTPUT,    PID_UTX);
	SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_URX);


	init_SPI2(CLK_5184, MODE_3, MODE_8BITS);

	SetWord(EBI_SMTMGR_SET1_REG,0x0d44);
	SetWord(EBI_SMTMGR_SET2_REG, 0xe278);	
// enable timer1 clock
	if (GetWord(CLK_GLOBAL_REG) & SW_XTAL_PLL1_RATE)
		SetBits(CLK_GPIO4_REG, SW_TMR1_DIV, 144);
	else
		SetBits(CLK_GPIO4_REG, SW_TMR1_DIV, 72);
	
	SetBits(CLK_GPIO4_REG, SW_TMR1_EN, 1);


#ifdef F_G2_P2_2PORT_BOARD



// setup lcd interface 
	SetPort(P1_01_MODE_REG, PORT_OUTPUT, PID_ACS3);        /* P2_06 is ACS3 */
	SetDword(EBI_ACS3_CTRL_REG,0x221);						/* use time setting 1, memory is sram, size is 64K */
	SetDword(EBI_ACS3_LOW_REG,CONFIG_LCD_BASE);				/* ACS3 base address is 0x1300000 */  



//reset lmx
	SetPort(P1_00_MODE_REG, PORT_OUTPUT,   PID_port);

	SetWord(P1_SET_DATA_REG,1);
	for (i=0;i<0x800;i++)
		j=*(volatile unsigned*)0;
	SetWord(P1_RESET_DATA_REG,1);
	for (i=0;i<0x800;i++)
		j=*(volatile unsigned*)0;
	SetPort(P1_00_MODE_REG, PORT_INPUT,   PID_port);
	SetWord(P1_RESET_DATA_REG,1);
// set 3wire i/f port pins
	SetPort(P2_13_MODE_REG, PORT_OUTPUT,   PID_LE);
	SetPort(P2_14_MODE_REG, PORT_OUTPUT,   PID_SK);
	SetPort(P2_15_MODE_REG, PORT_OUTPUT,   PID_SIO);




#else
#error "board revision is undefined"
#endif



	SetWord(BAT_CTRL_REG,0x412e);
//	SetWord(BANDGAP_REG,0x0d);



	return 1;

}

int 
/**********************************************************/
dram_init(bd_t *bd)
/**********************************************************/
{
  	bd->bi_dram[0].start = PHYS_SDRAM_1;
	bd->bi_dram[0].size  = PHYS_SDRAM_1_SIZE;
	
	
	bd->bi_dram[1].start = PHYS_SDRAM_2;
	bd->bi_dram[1].size = PHYS_SDRAM_2_SIZE;
	
	return PHYS_SDRAM_1_SIZE+PHYS_SDRAM_2_SIZE;
 
}

void lcd_reset (void)
{
	SetWord(P2_RESET_DATA_REG, 8);
	udelay(50000);
	SetWord(P2_SET_DATA_REG, 8);
	udelay(50000);
}

