/*
 * (C) Copyright 2002
 * Kyle Harris, Nexus Technologies, Inc. kharris@nexus-tech.net
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */


#include "armboot.h"
#include "sitel_io.h"
#include "spi_polled.h"
/* ------------------------------------------------------------------------- */

/*
 * Miscelaneous platform dependent initialisations
 */

void lcd_reset (void);

void led_check(void)
{
	unsigned i;

	*(volatile unsigned short*)CONFIG_KEYB_LATCH_BASE = 0;
	udelay(50000);
	*(volatile unsigned short*)CONFIG_KEYB_LATCH_BASE = 0xffff;
	udelay(50000);
	*(volatile unsigned short*)CONFIG_KEYB_LATCH_BASE = 0;
	udelay(50000);
	for (i=0;i<14;i++){
		*(volatile unsigned short*)CONFIG_KEYB_LATCH_BASE = 1<<i;
		udelay(50000);
	}
	*(volatile unsigned short*)CONFIG_KEYB_LATCH_BASE = 0;
	udelay(50000);
	*(volatile unsigned short*)CONFIG_KEYB_LATCH_BASE = 0xffff;
	udelay(50000);
	*(volatile unsigned short*)CONFIG_KEYB_LATCH_BASE = 0;

}


int 
/**********************************************************/
board_post_init(bd_t *bd)
/**********************************************************/
{
	led_check();
   return 0;
}

int 
/**********************************************************/
board_init(bd_t *bd)
/**********************************************************/
{
	
	/* arch number of Sitel SC14450 DK */
	bd->bi_arch_number = 450;

	/* vm must change */ 
	/* adress of boot parameters */
	bd->bi_boot_params = CFG_CONFIG_PARAMETERS_ADDR;//0xa0000100;

		/*   Config PPA for UART. */
	SetPort(P0_00_MODE_REG, PORT_OUTPUT,    PID_UTX);
	SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_URX);

	init_SPI2(CLK_5184, MODE_3, MODE_8BITS);

	SetWord(EBI_SMTMGR_SET1_REG,0x0d44);
	SetWord(EBI_SMTMGR_SET2_REG, 0xe278);	

//setup LED interface
	SetPort(P2_07_MODE_REG, PORT_OUTPUT,  PID_ACS0);		/* P2_07 is ACS0 */
	SetDword(EBI_ACS0_CTRL_REG,0x221);						/* use time setting 2 */
	SetDword(EBI_ACS0_LOW_REG,CONFIG_LED_BASE);		        /* ACS0 base address is 0x1500000 */

//vm setup ethernet interface 
	SetPort(P2_04_MODE_REG, PORT_OUTPUT,  PID_ACS1);        /* P2_04 is ACS1 */
	SetDword(EBI_ACS1_CTRL_REG,0x121);						/* use time setting 1, memory is sram, size is 64K */
	SetDword(EBI_ACS1_LOW_REG,CONFIG_ETHERNET_BASE);		/* ACS1 base address is 0x1200000 */  

	SetPort(P1_15_MODE_REG, PORT_PULL_DOWN,  PID_INT7n);	/* P1_15 is int7 for ethernet interrupt */
	SetWord(KEY_GP_INT_REG , 0x20);							/* Enable positive level int7 */


//setup latch interface
	SetPort(P2_09_MODE_REG, PORT_OUTPUT,  PID_ACS2);		/* P1_14 is ACS2 */
	SetDword(EBI_ACS2_CTRL_REG,0x121);						/* use time setting 2, memory is sram, size is 64K */
	SetDword(EBI_ACS2_LOW_REG,CONFIG_KEYB_LATCH_BASE);		/* ACS2 base address is 0x1400000 */  

//vm setup lcd interface 
	SetPort(P2_06_MODE_REG, PORT_OUTPUT, PID_ACS3);        /* P2_06 is ACS3 */
	SetDword(EBI_ACS3_CTRL_REG,0x221);						/* use time setting 1, memory is sram, size is 64K */
	SetDword(EBI_ACS3_LOW_REG,CONFIG_LCD_BASE);				/* ACS3 base address is 0x1300000 */  
	
	SetPort(P1_13_MODE_REG,PORT_OUTPUT,PID_port);			/* P1_13 is LCD RESET */
	SetWord(P1_SET_DATA_REG, GPIO_13);
	
	return 1;

}

int 
/**********************************************************/
dram_init(bd_t *bd)
/**********************************************************/
{
  	bd->bi_dram[0].start = PHYS_SDRAM_1;
	bd->bi_dram[0].size  = PHYS_SDRAM_1_SIZE;
	
	
	bd->bi_dram[1].start = PHYS_SDRAM_2;
	bd->bi_dram[1].size = PHYS_SDRAM_2_SIZE;
	
	return PHYS_SDRAM_1_SIZE+PHYS_SDRAM_2_SIZE;
 
}

void lcd_reset (void)
{
	SetWord(P1_RESET_DATA_REG, GPIO_13);
	udelay(50000);
	SetWord(P1_SET_DATA_REG, GPIO_13);
	udelay(50000);

}
