#include "sitel_io.h"
#include "qspic.h"

struct flash_dev qspicFDev;



// Enable Quad SPI Clock
void qspic_clock_en (void) {
	SetWord(CLK_AUX2_REG, GetWord(CLK_AUX2_REG) | SW_QSPIC_EN);
}


// Disable Quad SPI Clock
void qspic_clock_dis(void) {
	SetWord(CLK_AUX2_REG, GetWord(CLK_AUX2_REG) & ~(SW_QSPIC_EN));
}


// The the source of spi clock is the hclk.
void qspic_clksrc_hclk(void) {
	SetWord(CLK_AUX2_REG, GetWord(CLK_AUX2_REG) | SW_QSPIC_SEL_SPEED);
}

// The source of the spi clock is the PLL2.
void qspic_clksrc_pll2 (void) {
	SetWord(CLK_AUX2_REG, GetWord(CLK_AUX2_REG) & ~(SW_QSPIC_SEL_SPEED));
}


// The divider for the spi clock is equal to 2.
void qspic_clk_div2 (void) {
	SetWord(CLK_AUX2_REG, GetWord(CLK_AUX2_REG) | 0x2000);
}


// The divider for the spi clock is equal to 1.
void qspic_clk_div1 (void) {
	SetWord(CLK_AUX2_REG, GetWord(CLK_AUX2_REG) & ~(0x2000));
}

// Configure the Programmable Peripheral Matrix to enable the Quad SPI Controller.
void qspic_pads_init (void) {
	SetWord (P0_03_MODE_REG, 0x338);
	SetWord (P0_06_MODE_REG, 0x338);
	SetWord (P0_11_MODE_REG, 56);
	SetWord (P0_12_MODE_REG, 56);
	SetWord (P0_13_MODE_REG, 56);
	SetWord (P0_14_MODE_REG, 56);
}

// Auto Mode.
void qspic_auto_mode (void) {
	SetDword(QSPIC_CTRLMODE_REG , QSPIC_AUTO_MD | GetDword (QSPIC_CTRLMODE_REG));
}

// Manual Mode
void qspic_manual_mode (void) {
	SetDword(QSPIC_CTRLMODE_REG , ~QSPIC_AUTO_MD & GetDword (QSPIC_CTRLMODE_REG));
}

// Enable the pads for quad operation
void qspic_enable_quad_pads (void) {
	SetDword(QSPIC_CTRLMODE_REG , (GetDword (QSPIC_CTRLMODE_REG) & ~(QSPIC_IO3_OEN | QSPIC_IO2_OEN)));
}

// Diable the quad pads operation
void qspic_disable_quad_pads (void) {
	SetDword(QSPIC_CTRLMODE_REG ,(GetDword (QSPIC_CTRLMODE_REG) | (QSPIC_IO3_OEN | QSPIC_IO2_OEN | QSPIC_IO3_DAT | QSPIC_IO2_DAT)));
}


// Set Clock frequency equal to PLL2/2 (25Mhz)
void qspi_clk_set_PLL2_half (void) {
	qspic_clock_dis();
	qspic_clksrc_pll2();
	qspic_clk_div2();
	qspic_clock_en();
}

// Set Clock frequency equal to PLL2 (50Mhz)
void qspi_clk_set_PLL2_full (void) {
	qspic_clock_dis();
	qspic_clksrc_pll2();
	qspic_clk_div1();
	qspic_clock_en();
}

// Set Clock frequency equal to PLL1/2 (41.5Mhz)
void qspi_clk_set_HCLK_half (void) {
	qspic_clock_dis();
	qspic_clksrc_hclk();
	qspic_clk_div2();
	qspic_clock_en();
}

// Set Clock frequency equal to PLL1 (83Mhz)
void qspi_clk_set_HCLK_full (void) {
	qspic_clock_dis();
	qspic_clksrc_hclk();
	qspic_clk_div1();
	qspic_clock_en();
}

// This functions suppose that the two plls are enabled and the generated frequencies are :
// PLL1 : 165.888 MHz
// PLL2 : 50 MHz
void qspic_set_closer_freq  (unsigned char max_flash_freq) {
	unsigned char hclk_freq  = 166;
	unsigned char pll2_freq  = 50;
	unsigned char hclk_div;
	
	unsigned char hclk_bgt_flash, half_hclk_bgt_flash, pll2_bgt_flash;
	
	hclk_div = GetWord (CLK_AMBA_REG) & SW_HCLK_DIV;
	
	if (hclk_div == 0) 
		hclk_div = 16;
	
	hclk_freq = hclk_freq / hclk_div;
	

	if (hclk_freq > max_flash_freq )
		hclk_bgt_flash = 1;
	else 
		hclk_bgt_flash = 0;
	
	if ((hclk_freq / 2) > max_flash_freq)
		half_hclk_bgt_flash = 1;
	else
		half_hclk_bgt_flash = 0;
	

	if (pll2_freq > max_flash_freq)
		pll2_bgt_flash = 1;
	else
		pll2_bgt_flash = 0;
	
	
	if (!hclk_bgt_flash)
		qspi_clk_set_HCLK_full();
	else if (!pll2_bgt_flash)
		qspi_clk_set_PLL2_full();
	else if (!half_hclk_bgt_flash)
		qspi_clk_set_HCLK_half();
	else
		qspi_clk_set_PLL2_half();
}

void qspic_delay(void) {
	unsigned long int i;
	for (i=0;i<200; i++);
}
