/*
 *-----------------------------------------------------------------------------
 *                                                                             
 *               @@@@@@@      *   @@@@@@@@@           *                                     
 *              @       @             @               *                            
 *              @             *       @      ****     *                                 
 *               @@@@@@@      *       @     *    *    *                              
 *        ___---        @     *       @     ******    *                                 
 *  ___---      @       @     *       @     *         *                             
 *   -_          @@@@@@@  _   *       @      ****     *                               
 *     -_                 _ -                                                     
 *       -_          _ -                                                       
 *         -_   _ -        s   e   m   i   c   o   n   d   u   c   t   o   r 
 *           -                                                                    
 *                                                                              
 * (C) Copyright SiTel Semiconductor BV, unpublished work.
 * This computer program includes Confidential, Proprietary Information and
 * is a Trade Secret of SiTel Semiconductor BV.
 * All use, disclosure, and/or reproduction is prohibited unless authorized
 * in writing. All Rights Reserved.
 *
 *-----------------------------------------------------------------------------
 * Filename :   spi_flash.c
 * Purpose  :   Driver for serial flash memories with SPI interface
 * Created  :   31/10/2007
 * By       :   Vassilis Maniotis
 * Remarks  :   -
 *
 *
 *-----------------------------------------------------------------------------
 */

/*========================== Include files ==================================*/

//#include <sys/asm.h>
#include <armboot.h>
#include "sitel_io.h"
#include "spi_polled.h"
#include "spi_flash.h"
#include <flash.h>
/*========================== Local macro definitions ========================*/

#define spi_mem_cs_low() SetWord(P0_RESET_DATA_REG, GPIO_3)
#define spi_mem_cs_high() SetWord(P0_SET_DATA_REG, GPIO_3)

/*========================== Global definitions =============================*/
/*========================== Local data definitions =========================*/
/*========================== Local function prototypes ======================*/
/*========================== Function definitions ===========================*/

unsigned long SpiFlashID( void ) 
{
	unsigned long ret,i ;
	
	if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;

	if (SpiFlashSetWriteEbable())
		return ERR_TIMOUT;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_RDID_CMD);
	getByte_SPI2(&i);
	ret = ((i&0xff)<<16);
	getByte_SPI2(&i);
	ret |= ((i&0xff)<<8);
	getByte_SPI2(&i);
	ret |= (i&0xff);
	spi_mem_cs_high();

	return ret;
}

unsigned char SpiFlashReadStatus( void ) 
{
	unsigned long temp;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_RDSR_CMD);
	getByte_SPI2(&temp);
	spi_mem_cs_high();

	return (unsigned char)temp;
}

int SpiFlashWriteStatus( unsigned char writevalue ) 
{
	if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;
	SpiFlashSetWriteEbable();
	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_WRSR_CMD);
	putByte_SPI2(writevalue);
	spi_mem_cs_high();
	return ERR_OK;
}

int SpiFlashSetWriteEbable(void)
{
	if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_WREN_CMD);
	spi_mem_cs_high();

	return ERR_OK;

}

int SpiFlashSetWriteDisable(void)
{
	if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_WRDI_CMD);
	spi_mem_cs_high();

	return ERR_OK;
 
}

int SpiFlashEraseChip(void)
{
	unsigned char status;

	if (SpiFlashSetWriteEbable())
		return ERR_TIMOUT;
	status = SpiFlashReadStatus();
	if (( status & 0x3c) != 0) {
	// write protected
		spi_mem_cs_low();
		putByte_SPI2(SPI_FLASH_WRSR_CMD);
		putByte_SPI2(0);
		spi_mem_cs_high();
		
		if (SpiFlashSetWriteEbable())
			return ERR_TIMOUT;
 
	}

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_WRSR_CMD);
	putByte_SPI2(0);
	spi_mem_cs_high();   

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_BE_CMD);
	spi_mem_cs_high();  
	
	return ERR_OK;
}

int SpiFlashEraseSector(unsigned long sector_address,unsigned short sector_size)
{
  
	unsigned char status,cmd;

	if (SpiFlashSetWriteEbable())
		return ERR_TIMOUT;

	status = SpiFlashReadStatus();
	if (( status & 0x3c) != 0) {
	// write protected
		spi_mem_cs_low();
		putByte_SPI2(SPI_FLASH_WRSR_CMD);
		putByte_SPI2(0);
		spi_mem_cs_high();
		
		if (SpiFlashSetWriteEbable())
			return ERR_TIMOUT;
 
	}

	switch (sector_size)
	{
		case SEC_SIZE_4K:
			cmd=SPI_FLASH_SE_4K_CMD;
			break;
		case SEC_SIZE_32K:
			cmd=SPI_FLASH_SE_32K_CMD;
			break;
		case SEC_SIZE_64K:
		default:
			cmd=SPI_FLASH_SE_CMD;
			break;
	}

	spi_mem_cs_low();
	putByte_SPI2(cmd);
	putByte_SPI2((sector_address&0xffffff)>>16);
	putByte_SPI2((sector_address&0xffff)>>8);
	putByte_SPI2(sector_address&0xff);
	spi_mem_cs_high();  
	
	return ERR_OK;			
}


int SpiFlashRead(unsigned long source_addr,unsigned char* destination_buffer,unsigned long len)
{

	if (!len)
		return 0;
	if ((source_addr+len) > PHYS_FLASH_SIZE)
		return ERR_INVAL;

	if (SpiFlashSetWriteEbable())
		return ERR_TIMOUT;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_READ_CMD);
	putByte_SPI2((source_addr&0xffffff)>>16);
	putByte_SPI2((source_addr&0xffff)>>8);
	putByte_SPI2(source_addr&0xff);
	while(len-- >0)
	{
		getByte_SPI2(destination_buffer);
		destination_buffer++;
	}
	spi_mem_cs_high();

	return ERR_OK;
}

int SpiFlashReadFast(unsigned long source_addr,unsigned char* destination_buffer,unsigned long len)
{

	if (!len)
		return 0;
	if ((source_addr+len) > PHYS_FLASH_SIZE)
		return ERR_INVAL;

	if (SpiFlashSetWriteEbable())
		return ERR_TIMOUT;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_READ_FAST_CMD);
	putByte_SPI2((source_addr&0xffffff)>>16);
	putByte_SPI2((source_addr&0xffff)>>8);
	putByte_SPI2(source_addr&0xff);
	while(len-- >0)
	{
		getByte_SPI2(destination_buffer);
		destination_buffer++;
	}
	spi_mem_cs_high();

	return ERR_OK;
}

void SpiFlashSectorProtect(unsigned long sector )
{
}

int SpiFlashSectorProtectDetect( unsigned long sector )
{
  unsigned char status;
  status = SpiFlashReadStatus();
}


int SpiFlashProgram(unsigned long dest_addr,unsigned char* data, unsigned long len)
{
	
	unsigned long index,page_offset,page_size;
	unsigned long word_count;
	int retvalue = 0;

	if (!len)
		return ERR_OK;
	if ((dest_addr+len) > PHYS_FLASH_SIZE)
		return ERR_INVAL;
#ifdef CFG_SST_SERIAL_FLASH
#if 0 // byte program
//	SetWord( P0_06_MODE_REG,0x0300 );
//	SetWord( P0_SET_DATA_REG,0x40);

	if (SpiFlashSetWriteEbable())
		return ERR_TIMOUT;  

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_WRSR_CMD);
	putByte_SPI2(0);
	spi_mem_cs_high();  

	if (SpiFlashSetWriteEbable())
		return ERR_TIMOUT;  
	
	for (index =0;index<len;index++)
	{
		if (SpiFlashSetWriteEbable())
			return ERR_TIMOUT;  

		if (SpiFlashWaitTillReady())
			return ERR_TIMOUT;

		spi_mem_cs_low();
		putByte_SPI2(SPI_FLASH_PP_CMD);
		putByte_SPI2((dest_addr&0xffffff)>>16);
		putByte_SPI2((dest_addr&0xffff)>>8);
		putByte_SPI2(dest_addr&0xff);
		putByte_SPI2(data[index]);
		spi_mem_cs_high();
		dest_addr++;
	}
//	SetWord( P0_RESET_DATA_REG,0x40);
#endif
#if 1	// AAI
//	SetWord( P0_06_MODE_REG,0x0300 );
//	SetWord( P0_SET_DATA_REG,0x40);


	if (SpiFlashSetWriteEbable())
		return ERR_TIMOUT;  

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_WRSR_CMD);
	putByte_SPI2(0);
	spi_mem_cs_high();  

	if (SpiFlashSetWriteEbable())
		return ERR_TIMOUT;

	if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;
	
	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_DBSY_CMD);
	spi_mem_cs_high();

	if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;
	
	word_count = len/2 + len%2;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_AAI_CMD);
	putByte_SPI2((dest_addr&0xffffff)>>16);
	putByte_SPI2((dest_addr&0xffff)>>8);
	putByte_SPI2(dest_addr&0xff);
	putByte_SPI2(*data++);
	putByte_SPI2(*data++);
	spi_mem_cs_high();

	for (index =1;index<word_count;index++)
	{

		if (SpiFlashWaitTillReady())
			return ERR_TIMOUT;

		spi_mem_cs_low();
		putByte_SPI2(SPI_FLASH_AAI_CMD);
		putByte_SPI2(*data++);
		putByte_SPI2(*data++);
		spi_mem_cs_high();

	}
	if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_WRDI_CMD);
	spi_mem_cs_high();


	if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;

//	SetWord( P0_RESET_DATA_REG,0x40);

#endif

#else
	
	page_offset = dest_addr % FLASH_PAGE_SIZE;

	if ((page_offset + len) <= FLASH_PAGE_SIZE)  
	{
		retvalue = SpiFlashProgramPage(dest_addr,data,len);
		return retvalue;
	}
	else
	{
		page_size = FLASH_PAGE_SIZE - page_offset;
		retvalue = SpiFlashProgramPage(dest_addr,data,page_size);

		if (retvalue != 0)
			return retvalue;

		for (index = page_size; index < len; index+=page_size)
		{
			page_size = len - index;
			if (page_size > FLASH_PAGE_SIZE)
				page_size = FLASH_PAGE_SIZE;

			retvalue = SpiFlashProgramPage(dest_addr + index,&data[index],page_size);

			if (retvalue != 0)
				return retvalue;

		}

	}
#endif

	return ERR_OK;


}

int SpiFlashProgramPage(unsigned long dest_addr,unsigned char* data, unsigned len)
{
	unsigned short index;
	if (!len)
		return ERR_OK;
	if (len > FLASH_PAGE_SIZE)
		return ERR_INVAL;
    
  if (SpiFlashSetWriteEbable())
		return ERR_TIMOUT;  
  if (SpiFlashWaitTillReady())
		return ERR_TIMOUT;

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_PP_CMD);
	putByte_SPI2((dest_addr&0xffffff)>>16);
	putByte_SPI2((dest_addr&0xffff)>>8);
	putByte_SPI2(dest_addr&0xff);
	for (index =0; index < len; index++)
		putByte_SPI2(data[index]);
	spi_mem_cs_high();

	return ERR_OK;

}

int SpiFlashWaitTillReady (void)
{
  int count;
  unsigned char status;
  for (count = 0; count < MAX_READY_WAIT_COUNT; count++)
  {
    status = SpiFlashReadStatus();
    if (!(status & STATUS_BUSY))
      return 0;
  }
  return 1;
}
  
int SpiFlashCheckErase(unsigned long dest_addr, unsigned long len)
{
	unsigned char temp;

	if (!len)
		return ERR_OK;
	if ((dest_addr+len) > PHYS_FLASH_SIZE)
		return ERR_INVAL;

	if (SpiFlashSetWriteEbable())
		return ERR_TIMOUT;  

	spi_mem_cs_low();
	putByte_SPI2(SPI_FLASH_READ_CMD);
	putByte_SPI2((dest_addr&0xffffff)>>16);
	putByte_SPI2((dest_addr&0xffff)>>8);
	putByte_SPI2(dest_addr&0xff);
	while(len-- >0)
	{
		getByte_SPI2(&temp);
		if (temp != 0xff) {
			spi_mem_cs_high();
			return ERR_NOT_ERASED;
		}
	}
	spi_mem_cs_high();
	return ERR_OK;
}

