/*
 * (C) Copyright 2002
 * Kyle Harris, Nexus Technologies, Inc. kharris@nexus-tech.net
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */


#include "armboot.h"
#include "sitel_io.h"
#include "spi_polled.h"
#include "qspic.h"

#include "nt75451.h"
/* ------------------------------------------------------------------------- */

/*
 * Miscelaneous platform dependent initialisations
 */



extern void close_switch(void);
extern int kbd_configure(void);
extern unsigned char kbd_getc(void);

int 
/**********************************************************/
board_post_init(bd_t *bd)
/**********************************************************/
{

	SetPort(P0_05_MODE_REG, PORT_OUTPUT,  PID_port);					/* P0_06 is SPI_CLK  */

	SetWord(P0_SET_DATA_REG,0x20);
	udelay(50000);
	SetWord(P0_RESET_DATA_REG,0x20);
	udelay(50000);
	SetWord(P0_SET_DATA_REG,0x20);
	udelay(50000);
	SetWord(P0_RESET_DATA_REG,0x20);
	udelay(50000);
	SetWord(P0_SET_DATA_REG,0x20);

   return 0;
}




int 
/**********************************************************/
board_init(bd_t *bd)
/**********************************************************/
{
	unsigned int temp;
	
	/* arch number of Sitel SC14450 DK */
	bd->bi_arch_number = 452;

	/* vm must change */ 
	/* adress of boot parameters */
	bd->bi_boot_params = CFG_CONFIG_PARAMETERS_ADDR;//0xa0000100;

	/*   Config PPA for UART. */
	SetPort(P0_00_MODE_REG, PORT_OUTPUT,    PID_UTX);
	SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_URX);


#ifdef CONFIG_FLASH_IS_QUAD_SPI
	qspic_init();
#else
	init_SPI2(CLK_5184, MODE_3, MODE_8BITS);
#endif

// enable timer1 clock
	if (GetWord(CLK_GLOBAL_REG) & SW_XTAL_PLL1_RATE)
		SetBits(CLK_GPIO4_REG, SW_TMR1_DIV, 144);
	else
		SetBits(CLK_GPIO4_REG, SW_TMR1_DIV, 72);
	
	SetBits(CLK_GPIO4_REG, SW_TMR1_EN, 1);

	SetWord(BAT_CTRL_REG,0x412c);
	// 8mA
	//SetWord(GPRG_R0_REG,0x2980);
	//SetWord(GPRG_R1_REG,0x5000);//changed by: IP, 02 June 2010//SetWord(GPRG_R1_REG,0xa000);

	// 4mA
	SetWord(GPRG_R0_REG,0xFD80);  
	
	
	SetWord(GPRG_R1_REG,0xF000);//changed by: IP, 02 June 2010//SetWord(GPRG_R1_REG,0xa000);

//	SetWord(BANDGAP_REG,0x0d);

	close_switch();

	kbd_configure();

	return 1;

}

int 
/**********************************************************/
dram_init(bd_t *bd)
/**********************************************************/
{
  	bd->bi_dram[0].start = PHYS_SDRAM_1;
	bd->bi_dram[0].size  = PHYS_SDRAM_1_SIZE;
	
	
	bd->bi_dram[1].start = PHYS_SDRAM_2;
	bd->bi_dram[1].size = PHYS_SDRAM_2_SIZE;
	
	return PHYS_SDRAM_1_SIZE+PHYS_SDRAM_2_SIZE;
 
}


int check_kernel_image (bd_t *bd,ulong base_address)
{
	int file_size = 0;
	int i;
	const char *p;
	ulong addr, checksum, my_checksum ;
	image_header_t* hdr ;

	/* check if the file we just downloaded looks alright */
	addr = base_address ;
	hdr = (image_header_t*)addr ;
	if( SWAP32( hdr->ih_magic )  != IH_MAGIC )
		return 0;

	file_size = simple_strtol( getenv( bd, "filesize" ), NULL, 16 ) ;
	/* assume that valid images cannot be less than 500KB...
	   they also shouldn't exceed the size of the size of the flash
	   partition reserved for the image */
	if( file_size < CFG_MIN_IMAGE_SIZE )
		return 0;
	if( file_size > CFG_MAX_IMAGE_SIZE )
		return 0;

	/* finally, check the header CRC and the image CRC */
	checksum = hdr->ih_hcrc ;
	hdr->ih_hcrc = 0 ;
	my_checksum = crc32( 0, (char*)hdr, sizeof( image_header_t ) ) ;
	if( my_checksum != SWAP32( checksum ) )
		return 0;

	hdr->ih_hcrc = checksum ;
	print_image_hdr( hdr ) ;
	my_checksum = crc32( 0, (char*)hdr + sizeof( image_header_t ),
						SWAP32( hdr->ih_size ) ) ;
	checksum = hdr->ih_dcrc ;
	if( my_checksum != SWAP32( checksum ) )
		return 0;

	return 1;
}





int is_valid_ip(char *ip_str)
{
	char *e;
	ulong i,val;
	for (i=0; i<4; ++i) {
		val = ip_str ? simple_strtoul(ip_str, &e, 10) : 0;
		if (val > 255)
			return 0;
		if (ip_str) ip_str = (*e) ? e+1 : e;
	}
	return 1;
}

extern ulong get_timer_masked(void);
extern ulong get_timer(ulong);

unsigned char get_con(void)
{
	volatile ulong tmo;
	unsigned char c;

	while (1)
	{
		tmo = 11520;
		tmo += get_timer(0);

		while(get_timer_masked() < tmo)
		{
			if (tstc())
			{
				c =  getc();
				return c;
			}
		}
		c = kbd_getc();
		if (c!=0)
			return c;
	}
}

#define PASSTIMEOUT 1000	// 10 seconds

unsigned char get_con_timed(void)
{
	volatile ulong tmo;
	unsigned char c;
	volatile overall_wait=0;
	
	while (1) {
		tmo = 11520;
		tmo += get_timer(0);
		while ( get_timer_masked() < tmo ) {
			if ( tstc() ) {
				c =  getc();
				return c;
			}
		}
		c = kbd_getc();
		if ( c != 0 ) return c;
		
		if ( ++overall_wait > PASSTIMEOUT ) return 0;
	}
}


void edit_ip_str(bd_t *bd, char* ip_str,uchar x_pos,uchar y_pos,uchar m_font)
{
	int i,f_len,dot_num;
	char c;
	uchar m_cur_val_disp = 1;
	i=0;
	f_len=0;
	dot_num=0;

	while (1)
	{
		c = get_con();
		if (c>='0' && c<='9')
		{
			if (m_cur_val_disp == 1)
			{
				m_cur_val_disp=0;
				strcpy(ip_str,"                  \0");
				lcd_text(0,20,m_font,&ip_str[0]);
				lcd_update(1,31);
			}

			if (f_len<3)
			{
				lcd_cursor_set(i*6, 20, m_font, 0, 0);
				ip_str[i]=c;
				lcd_text(0,20,m_font,&ip_str[0]);
				lcd_update(1,31);
				lcd_cursor_set((i+1)*6, 20, m_font, 1, 0);
				f_len++;
				i++;
			}
		}

		if (c=='.')
		{
			if (f_len>0)
			{
				if (dot_num < 3)
				{
					dot_num++;
					f_len=0;
					lcd_cursor_set(i*6, 20, m_font, 0, 0);
					ip_str[i]=c;
					lcd_text(0,20,m_font,&ip_str[0]);
					lcd_update(1,31);
					lcd_cursor_set((i+1)*6, 20, m_font, 1, 0);
					i++;
				}
			}
		}

		if (c==0x7f)
		{
			if (i>0)
			{
				lcd_cursor_set(i*6, 20, m_font, 0, 0);
				i--;
				ip_str[i]=0x20;
				if (f_len == 0)
				{
					dot_num--;
					if(dot_num < 0)
						dot_num=0;
				}
				else
				{
					f_len--;
					if(f_len < 0)
						f_len=0;
				}
				lcd_text(0,20,m_font,&ip_str[0]);
				lcd_update(1,31);
				lcd_cursor_set(i*6, 20, m_font, 1, 0);
			}
		}
		if (c=='\r')
		{
	//		printf("disp %d f_len %d dot_num %d \n",m_cur_val_disp,f_len,dot_num);

			if ((m_cur_val_disp == 1) || ((f_len > 0) && (dot_num == 3)))
			{
				if (is_valid_ip(ip_str))
					break;
				else
				{
					lcd_cursor_set(i*6, 20, m_font, 0, 0);
					lcd_text(0,20,m_font,"Invalid value !!\0");
					lcd_update(1,31);

					m_cur_val_disp = 1;
					i=0;
					f_len=0;
					dot_num=0;
				}
			}
		}
	}
}

/* oth 100625 string scanner of recovery mode */
int mstrncmp(const char *cs, const char *ct, size_t count) 
{
	register signed char __res = 0;

	while (count--) {
		if ( (__res = *cs++ - *ct++) != 0 ) break;  // ±ÛÀÚ°¡ ´Ù¸£¸é ¹Ù·Î non zero¸¦ returnÇÑ´Ù
	}

	return __res;
}

unsigned long block_per_10;

void boot_l_v1_board(bd_t *bd)
{
	char txt_str[30];
	char pass[30];
	char c;
	char *p;
	int i,len;
	uchar m_font = 0;
	volatile unsigned char match;
	ulong file_size;

	SetWord(P0_RESET_DATA_REG,0x20);

	lcd_clear_ram();

	lcd_text(0,0,m_font,"BootC Started\0");
	lcd_text(0,10,m_font,"Enter Password\0");
	lcd_update(1,31);

	for (i=0;i<100;i++)	udelay(10000);

	for (i=0;i<30;i++) txt_str[i]=0;

	p = getenv(bd, "passwd");
	strcpy(pass, p);
	len = strlen(pass);
	if ( len < 4 ) {
		strcpy(pass, "1234");
		len = 4;
	}

	lcd_cursor_set(0, 20, m_font, 1, 0);

	while (1) {
		match = 1;
		for (i=0; i<len; i++) {
/*
			if ( i == 0 ) {	// timeout
				c = get_con_timed();
				if ( c == 0 ) {
					match = 2;
					break;
				}
			} else
*/
				c = get_con();
			
			lcd_cursor_set(i*6, 20, m_font, 0, 0);
			txt_str[i]='*';
			lcd_text(0,20,m_font,&txt_str[0]);
			lcd_update(1,31);
			lcd_cursor_set((i+1)*6, 20, m_font, 1, 0);
			if (c != pass[i]) match = 0;
		}
	
		if ( match == 1  ||  match == 2 ) break;

		lcd_cursor_set((i+1)*6, 20, m_font, 0, 0);
		lcd_text(0,20,m_font,"              \0");
		lcd_text(0,20,m_font,"WRONG !!!!\0");
		lcd_update(1,31);
		strcpy(txt_str,"                \0");
	}

	if ( match == 1 ) {
		lcd_clear_ram();
		p = getenv(bd, "ipaddr");
		strcpy(txt_str,"IP? ");
		lcd_text(0,0,m_font,&txt_str[0]);
		strcpy(txt_str, p);
		lcd_text(0,20,m_font,&txt_str[0]);
		lcd_update(1,31);
		edit_ip_str(bd, &txt_str[0],0,20,m_font);
		setenv(bd,"ipaddr",&txt_str[0]);

		lcd_clear_ram();
		p = getenv(bd, "netmask");
		strcpy(txt_str,"Netmask? ");
		lcd_text(0,0,m_font,&txt_str[0]);
		strcpy(txt_str, p);
		lcd_text(0,20,m_font,&txt_str[0]);
		lcd_update(1,31);
		edit_ip_str(bd, &txt_str[0],0,20,m_font);
		setenv(bd,"netmask",&txt_str[0]);

		lcd_clear_ram();
		p = getenv(bd, "gatewayip");
		strcpy(txt_str,"GW? ");
		lcd_text(0,0,m_font,&txt_str[0]);
		strcpy(txt_str, p);
		lcd_text(0,20,m_font,&txt_str[0]);
		lcd_update(1,31);
		edit_ip_str(bd, &txt_str[0],0,20,m_font);
		setenv(bd,"gatewayip",&txt_str[0]);

		lcd_clear_ram();
		p = getenv(bd, "serverip");
		strcpy(txt_str,"Server? ");
		lcd_text(0,0,m_font,&txt_str[0]);
		strcpy(txt_str, p);
		lcd_text(0,20,m_font,&txt_str[0]);
		lcd_update(1,31);
		edit_ip_str(bd, &txt_str[0],0,20,m_font);
		setenv(bd,"serverip",&txt_str[0]);
	}


	p = getenv(bd, "filesize");
	file_size = simple_strtol(p, NULL, 16);
	if(( file_size < CFG_MIN_IMAGE_SIZE ) || ( file_size > CFG_MAX_IMAGE_SIZE ))
		file_size = 2500000;

	block_per_10 = (file_size /512)/10;

	// oth 100625 recovery success ¿©ºÎ¿Í °ü°è¾øÀÌ preprov¿¡¼­ setÇÑ force_recovery¸¦ clearÇÔÀ¸·Î½á ´ÙÀ½ µ¿ÀÛ¿¡¼­´Â recovery mode¿¡¼­ ºüÁ®³ª¿Àµµ·Ï ÇÔ
	strcpy(txt_str, "0");
	setenv(bd, "force_recovery", &txt_str[0]);
	run_command("saveenv", bd, 0);

	while(1) {
		lcd_clear_ram();
		//	run_command("saveenv", bd, 0);
		lcd_text(0,0,m_font,"Downloading kernel\0");
		//	lcd_text(0,20,m_font,"----------  00%\0");
		lcd_text(0,20,m_font,"Searching Server ...\0");
		lcd_update(1,31);



		// Start: add for downloading vmlinuz filename oth 100625
		
		char *odm_ptr;
		char tftpbootstr[50];
		char filename[32];
		char *verfiledata;

		#define VER_MAX_FILESIZE 	0x400 
		#define VARIABLE_NAME 		"ker_img" 
		#define END_OF_FILE 		(0xa00000+VER_MAX_FILESIZE)

		strcpy(tftpbootstr, "tftpboot a00000 ");
		odm_ptr = getenv(bd, "odm_vendor");
		if ( odm_ptr == NULL  ||  odm_ptr[0] == 'L' ) { 
			strcat(tftpbootstr, "LIP_8002_VER_ALL"); 
			printf("Trying to get the vmlinuz filename with version filename [LIP_8002_VER_ALL]\n"); 
		} else { 
			strcat(tftpbootstr, "IP_8802_VER_ALL"); 
			printf("Trying to get the vmlinuz filename with version filename [IP_8802_VER_ALL]\n"); 
        }
		
		verfiledata = 0xa00000;
		for (i=0x0; i<VER_MAX_FILESIZE; i++) *(verfiledata+i) = '\0';
		
		if ( run_command(tftpbootstr, bd, 0) >= 0 ) {
ScanFilenameInVersionFile:
			while ( mstrncmp(verfiledata, VARIABLE_NAME, sizeof(VARIABLE_NAME)-1) && (verfiledata++ < (END_OF_FILE-sizeof(VARIABLE_NAME)-2)) ) ; 
		
			if ( verfiledata >= (END_OF_FILE-sizeof(VARIABLE_NAME)-1) ) { 
				printf ("Unable to find variable ker_img within the version file. Default filename [vmlinuz]\n"); 
				strcpy (filename, "vmlinuz"); 
			} else if ( *(verfiledata-1) == '#') {
				verfiledata++;
				goto ScanFilenameInVersionFile;
			} else {
				verfiledata += sizeof(VARIABLE_NAME)-1;
				if ( *verfiledata != ' '  &&  *verfiledata != '\t' ) goto ScanFilenameInVersionFile;
				while ( (*verfiledata == ' ' || *verfiledata == '\t')  &&  verfiledata < END_OF_FILE ) verfiledata++; 
		
        		if ( verfiledata == END_OF_FILE || *verfiledata == '#' || *verfiledata == '\0' || *verfiledata == '\n' || *verfiledata == '\r') { 
					printf ("Find ker_img but Do not define filename. Default filename [vmlinuz]\n"); 
					strcpy (filename, "vmlinuz");
				} else { 
					int j = 0; 
					while ( *(verfiledata+j) != '\0' && *(verfiledata+j) != '\n' && *(verfiledata+j) != '\r' &&
							*(verfiledata+j) != ' '  && *(verfiledata+j) != '\t' && (j<31) ) filename[j] = *(verfiledata + j++);
					filename[j] = '\0'; 
					if ( j == 31 ) { 
						printf ("Find ker_img and filename but Invalid filename. Default filename [vmlinuz]\n"); 
						strcpy (filename, "vmlinuz"); 
					} else {
						printf ("Find ker_img and valid filename. [%s]\n", filename); 
					}
				}
			}
		} else {
			printf("Unable to download version file. Default filename [vmlinuz]\n");
			strcpy (filename, "vmlinuz");
		}
		
		printf("Trying to download the vmlinuz [%s]\n", filename); 
		strcpy(tftpbootstr, "tftpboot a00000 ");
		strcat(tftpbootstr, filename); 
		
		// End: add for downloading vmlinuz filename oth 100625



		//if(run_command("tftpboot a00000 vmlinuz\0", bd, 0))
		if ( run_command(tftpbootstr, bd, 0) ) {
			if (check_kernel_image(bd,0xa00000)) {
				break;
			} else {
				lcd_text(0,20,m_font,"Bad Kernel Image\0");
				lcd_update(1,31);
				for (i=0;i<200;i++)	udelay(10000);
			}
		}
	}

//	run_command("upgrade vmlinuz a00000 20000\0", bd, 0);

	strcpy(txt_str, "ecp.w a00000 20000 ");

	/* copy data length (in 16-bit words) */
	p = getenv(bd, "filesize");
	file_size = simple_strtol(p, NULL, 16);
	file_size = file_size/2 + file_size%2;
	sprintf(pass, "%x", file_size);
	strcat(txt_str,pass);

	lcd_text(0,20,m_font,"Writing Flash ...   \0");
	lcd_update(1,31);

	while(1) {
		if(run_command(&txt_str[0], bd, 0)) break;
	}

	strcpy(txt_str,"1");
	setenv(bd,"recovery",&txt_str[0]);

	//strcpy(txt_str,"0");
	//setenv(bd,"force_recovery",&txt_str[0]);
	run_command("saveenv", bd, 0);

	lcd_clear_ram();
	lcd_text(0,0,m_font,"Completed.\0");
	lcd_text(0,20,m_font,"Rebooting Phone.\0");
	lcd_update(1,31);

	for (i=0;i<200;i++) udelay(10000);

	run_command("reset", bd, 0);

}



void disp_tftp_progress(unsigned block)
{
	uchar m_font = 0;

	lcd_clear_ram();

	lcd_text(0,0,m_font,"Downloading kernel\0");
//	lcd_text(0,20,m_font,"                    \0");

	if (block < block_per_10)
		lcd_text(0,20,m_font,"----------  00%\0");
	else if (block < 2*block_per_10)
		lcd_text(0,20,m_font,"*---------  10%\0");
	else if (block < 3*block_per_10)
		lcd_text(0,20,m_font,"**--------  20%\0");
	else if (block < 4*block_per_10)
		lcd_text(0,20,m_font,"***-------  30%\0");
	else if (block < 5*block_per_10)
		lcd_text(0,20,m_font,"****------  40%\0");
	else if (block < 6*block_per_10)
		lcd_text(0,20,m_font,"*****-----  50%\0");
	else if (block < 7*block_per_10)
		lcd_text(0,20,m_font,"******----  60%\0");
	else if (block < 8*block_per_10)
		lcd_text(0,20,m_font,"*******---  70%\0");
	else if (block < 9*block_per_10)
		lcd_text(0,20,m_font,"********--  80%\0");
	else if (block < 10*block_per_10)
		lcd_text(0,20,m_font,"*********-  90%\0");
	else
		lcd_text(0,20,m_font,"********** 100%\0");

	lcd_update(1,31);

}


