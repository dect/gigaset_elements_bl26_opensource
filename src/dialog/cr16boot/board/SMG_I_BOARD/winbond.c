#include "sitel_io.h"
#include "qspic.h"
#include "qspic/qspic_common.h"
#include "qspic/winbond.h"

//--------------------------------------------------------------------------------------
//                              Functions related to Status Register
//--------------------------------------------------------------------------------------

// Wait until the end of write/erase operation
void WND_WaitEndOfWrite(void)
{
	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	
	
	// Read Status 1
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_RSTAT1);
	
	while ( GetByte(QSPIC_READDATA_REG) & WND_STATUS_BUSSY);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Read Status Register 1
unsigned char WND_ReadStatus1 (void)
{
	unsigned char r_status;
	
	// Single Mode, CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	

	// Read Status 
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_RSTAT1);

	r_status = GetByte(QSPIC_READDATA_REG);

	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	return (r_status);
}

// Read Status Register 2
unsigned char WND_ReadStatus2 (void)
{
	unsigned char r_status;
	
	// Single Mode, CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	

	// Read Status 
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_RSTAT2);

	r_status = GetByte(QSPIC_READDATA_REG);

	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	return (r_status);
}


// Write Status and Configuration Registers
void WND_WriteStatus (unsigned char w_status1, unsigned char w_status2) {
	// Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);
	
	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_WEn);
	
	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Write Status Registers
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_WSTAT);
	SetByte(QSPIC_WRITEDATA_REG, w_status1);
	SetByte(QSPIC_WRITEDATA_REG, w_status2);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);

	// Wait end of write
	WND_WaitEndOfWrite();
}
//--------------------------------------------------------------------------------------
//                                   Read ID Functions
//-------------------------------------------------------------------------------------- 

// Read Unique ID -  Byte Buffer
void WND_ReadUIDByte(unsigned char *UIDBBuf)
{
	// Single Mode, CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Unique ID Instruction
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_RUID);
	
	// 4-Dummy Bytes
	SetByte(QSPIC_DUMMYDATA_REG,0x0);
	SetByte(QSPIC_DUMMYDATA_REG,0x0);
	SetByte(QSPIC_DUMMYDATA_REG,0x0);
	SetByte(QSPIC_DUMMYDATA_REG,0x0);
	
	// 8-Read Bytes - Unique ID
	UIDBBuf[0] = GetByte(QSPIC_READDATA_REG);
	UIDBBuf[1] = GetByte(QSPIC_READDATA_REG);
	UIDBBuf[2] = GetByte(QSPIC_READDATA_REG);
	UIDBBuf[3] = GetByte(QSPIC_READDATA_REG);
	UIDBBuf[4] = GetByte(QSPIC_READDATA_REG);
	UIDBBuf[5] = GetByte(QSPIC_READDATA_REG);
	UIDBBuf[6] = GetByte(QSPIC_READDATA_REG);
	UIDBBuf[7] = GetByte(QSPIC_READDATA_REG);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
}

// Read Unique ID -  Half Word Buffer
void WND_ReadUIDHalf(unsigned int *UIDHBuf)
{
	// Single Mode, CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Unique ID Instruction
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_RUID);
	
	// 2-Dummy Halfs
	SetWord(QSPIC_DUMMYDATA_REG, 0x0);
	SetWord(QSPIC_DUMMYDATA_REG, 0x0);
	
	// 4-Read Halfs - Unique ID
	UIDHBuf[0] = GetWord(QSPIC_READDATA_REG);
	UIDHBuf[1] = GetWord(QSPIC_READDATA_REG);
	UIDHBuf[2] = GetWord(QSPIC_READDATA_REG);
	UIDHBuf[3] = GetWord(QSPIC_READDATA_REG);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Read Unique ID -  Word Buffer
void WND_ReadUIDWord (unsigned long int *UIDWBuf)
{
	// Single Mode, CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);

	// Read Unique ID Instruction
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_RUID);

	// 1-Dummy Word
	SetDword(QSPIC_DUMMYDATA_REG, 0x0);
	
	// 2-Read Words - Unique ID
	UIDWBuf[0] = GetDword(QSPIC_READDATA_REG);
	UIDWBuf[1] = GetDword(QSPIC_READDATA_REG);

	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Read Jedec ID -  Byte Buffer
void WND_ReadJEDECID (unsigned char *JIDBuf)
{
	// Single Mode, CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);

	// Read Unique ID Instruction
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_JID);
	
	// Manufacture ID 
	JIDBuf[0] = GetByte(QSPIC_READDATA_REG);

	// Device type
	JIDBuf[1] = GetByte(QSPIC_READDATA_REG);
		
	// Device ID
	JIDBuf[2] = GetByte(QSPIC_READDATA_REG);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
}

//--------------------------------------------------------------------------------------
//                    Functions to Control the mode of the flash memory
//-------------------------------------------------------------------------------------- 

// Set High Performance - To operate in high frequencies.
void WND_SetHighPerf (void)
{
	// Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	
	
	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_WEn);
		
	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Write High Performance Mode
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_HIGHP);

	// 3 - Dummy Bytes
	SetWord(QSPIC_DUMMYDATA_REG, 0x0);
	SetByte(QSPIC_DUMMYDATA_REG,0x0);

	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Set Quad Enable - Sets the quad enable bit in status register to enable the quad spi instructions.
void WND_SetQuadEnable (void)
{
	unsigned char r_status1;
	unsigned char r_status2;
	
	// Read status Register 1
	r_status1 = WND_ReadStatus1();
	
	// Read Status Register 2
	r_status2 = WND_ReadStatus2();
	
	WND_WriteStatus(r_status1, r_status2 | WND_STATUS_QE);

	// IO3 hi-z, IO2 hi-z 	
	qspic_enable_quad_pads();
}


// Clear Quad Enable. Clear the quad enable bit in status register.
void WND_ClearQuadEnable (void)
{
	unsigned char r_status1;
	unsigned char r_status2;
	
	// Read status Register 1
	r_status1 = WND_ReadStatus1();
	
	// Read Status Register 2
	r_status2 = WND_ReadStatus2();
	
	WND_WriteStatus(r_status1, r_status2 & ~WND_STATUS_QE);

	// IO3 output = 1, IO2 output = 1 
	qspic_disable_quad_pads();
}


// Exit from Performance enhanced mode when a Quad I/O read is used.
void WND_ResetQuadModeBits(void) {
	// Set Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);

	// Reset Mode Bit
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_MRSTQIO);
}

// Exit from Performance enhanced mode when a Dual I/O read is used.
void WND_ResetDualModeBits(void) {
	// Set Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);

	// Reset Mode Bit
	SetWord(QSPIC_WRITEDATA_REG, WND_INST_MRSTDIO);
}

// Release From Deep Power Down 
void WND_ReleasePDM_HPM(void) {
	// Set Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);

	// Release Power Down
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_PDID);
}

//--------------------------------------------------------------------------------------
//                                   Erase Functions
//--------------------------------------------------------------------------------------

// Erase all chip.
void WND_EraseChip (void)
{
	// Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_WEn);

	// Chip Erase
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_CERASE);
	
	// Wait end of Erase
	WND_WaitEndOfWrite();
}

// Erase 4KB sector.
long int WND_EraseSector (unsigned long int Address)
{

	if (Address > FlashDes[qspicFDev.desc_num].MaxAddr)
		return (-1);
		
	// Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_WEn);

	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Sector 4KB Erase
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_SERASE);
	
	// Sector Address
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	// Wait end of Erase
	WND_WaitEndOfWrite();
	
	return (4*1024);
}

// Erase 32KB block (8 Sectors).
long int WND_EraseBlock32KB (unsigned long int Address)
{
	if (Address > FlashDes[qspicFDev.desc_num].MaxAddr)
		return (-1);
		
	// Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_WEn);

	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Block 32KB Erase
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_BERASE32);
	
	// Block Address
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
		
	// Wait end of Erase
	WND_WaitEndOfWrite();
	
	return ((long int)32*1024);
}


// Erase 64KB block (16 sectors).
long int WND_EraseBlock64KB (unsigned long int Address)
{
	if (Address > FlashDes[qspicFDev.desc_num].MaxAddr)
		return (-1);
		
	// Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_WEn);

	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Block 64KB Erase
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_BERASE64);
	
	// Block Address
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
		
	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);	
	
	// Read Status 1
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_RSTAT1);
	
	while ( GetByte(QSPIC_READDATA_REG) & 0x1);

	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	return ((long int) 64*1024);
}
//--------------------------------------------------------------------------------------
//                                   Write Data Functions
//--------------------------------------------------------------------------------------


// Write a page (256 bytes) using single spi mode.
int WND_WritePage (unsigned long int Address, unsigned long int *WPage)
{
	unsigned int i;

	if (Address > FlashDes[qspicFDev.desc_num].MaxAddr)
		return (-1);
	
	// Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_WEn);
	
	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Program Page 
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_PPROG);
	
	// Write Address
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	for (i=0; i<64; i++) 
		SetDword(QSPIC_WRITEDATA_REG, WPage[i]);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
		
	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);	
	
	// Read Status 1
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_RSTAT1);
	
	while ( GetByte(QSPIC_READDATA_REG) & 0x1);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	return (256);
}

// Write a page (256  byte) using the quad spi mode.
int WND_WritePageQuad (unsigned long int Address, unsigned long int *WPage)
{
	unsigned int i;
   
	if (Address > FlashDes[qspicFDev.desc_num].MaxAddr)
		return (-1);
		
	// Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_WEn);
		
	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);	
	
	// Program Page 
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_QPPROG);
	
	// Write Address
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Quad Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);	
	
	for (i=0; i<64; i++) 
		SetDword(QSPIC_WRITEDATA_REG, WPage[i]);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	
	
	// Read Status 1
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_RSTAT1);
	
	while ( GetByte(QSPIC_READDATA_REG) & 0x1);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	return (256);
}

//--------------------------------------------------------------------------------------
//                           Read Data functions during Manual Mode
//--------------------------------------------------------------------------------------

// Slow Byte Read
void WND_ReadDataByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf)
{
	unsigned long int i;

	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_RDATA);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address  & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Read Data
	for (i=0; i<BSize; i++) 
		BBuf[i] = GetByte(QSPIC_READDATA_REG);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Fast Byte Read 
void WND_ReadFastByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf)
{
	unsigned long int i;

	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_FRDATA);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);
	
	// Read Data
	for (i=0; i<BSize; i++) 
    		BBuf[i] = GetByte(QSPIC_READDATA_REG);
	
    // CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Quad Output Byte Read
void WND_ReadQuadOutputByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf)
{
	int i;
	
	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_FRQDATA);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);

	// Set Quad mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);
	
	// Read Data
	for (i=0; i<BSize; i++) 
		BBuf[i] = GetByte(QSPIC_READDATA_REG);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Quad Output Half Read
void WND_ReadQuadOutputHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf)
{
	unsigned long int i;

	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_FRQDATA);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);

	// Set Quad mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);
	
	// Read Data
	for (i=0; i<HSize; i++) 
		HBuf[i] = GetWord(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Quad Output Word Read
void WND_ReadQuadOutputWord (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf)
{
	unsigned long int i;
	
	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_FRQDATA);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);

	// Set Quad mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);
	
	// Read Data
	for (i=0; i<WSize; i++) 
		WBuf[i] = GetDword(QSPIC_READDATA_REG);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Dual Output Byte Read
void WND_ReadDualOutputByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf)
{
	unsigned long int i;
	
	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_FRDDATA);
	SetWord(QSPIC_WRITEDATA_REG,  ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);

	// Set Dual mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_DUAL);
	
	// Read Data
	for (i=0; i<BSize; i++) 
		BBuf[i] = GetByte(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Dual Output Half Read
void WND_ReadDualOutputHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf)
{
	int i;
	
	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_FRDDATA);
	SetWord(QSPIC_WRITEDATA_REG,  ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);

	// Set Dual mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_DUAL);
	
	// Read Data
	for (i=0; i<HSize; i++) 
		HBuf[i] = GetWord(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Dual Output Word Read
void WND_ReadDualOutputWord (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf)
{
	int i;

	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, WND_INST_FRDDATA);
	SetWord(QSPIC_WRITEDATA_REG,  ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);

	// Set Dual mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_DUAL);
	
	// Read Data
	for (i=0; i<WSize; i++) 
		WBuf[i] = GetDword(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

//--------------------------------------------------------------------------------------
//                Configurations for the Read sequence during Auto Mode
//--------------------------------------------------------------------------------------


void WND_AutoCfg_QuadOutput (void)
{
	//-------------------------------------------------------------------
	//                      Fast Read Quad Output
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : 6b - Fast Read Quad Output
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : 00 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  0 - SPI 
	// Extra Byte Trasmit Mode        [29:28] :  0 - SPI 
	// Dummy Bytes Trasmit Mode       [31:30] :  2 - Quad
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  2 - Quad
	// Extra Byte Enable                  [2] :  0 - Extra Byte Disabled
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  3 - 4 Dummy Bytes
	// Command mode                       [6] :  0 - Always Send Command Byte
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  3 - 3 CLKs	
	

	SetDword (QSPIC_BURSTCMDA_REG, 0x8000006b);
	SetDword (QSPIC_BURSTCMDB_REG, 0x00003032);
}


void WND_AutoCfg_QuadIO (void) {	
	//-------------------------------------------------------------------
	//                      Fast Read Quad I/O
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : eb - Fast Read Quad I/O
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : a0 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  2 - Quad 
	// Extra Byte Trasmit Mode        [29:28] :  2 - Quad
	// Dummy Bytes Trasmit Mode       [31:30] :  2 - Quad
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  2 - Quad
	// Extra Byte Enable                  [2] :  1 - Extra Byte Enabled
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  2 - 2 Dummy Bytes
	// Command mode                       [6] :  1 - Send Command Byte only in the first access.
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  0 - 0 CLKs	

	SetDword (QSPIC_BURSTCMDA_REG, 0xa8a000eb);
	SetDword (QSPIC_BURSTCMDB_REG, 0x00000066);
}
	

void WND_AutoCfg_DualOutput (void) {
	//-------------------------------------------------------------------
	//                      Fast Read Dual Output
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : 3b - Fast Read Dual Output
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : 00 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  0 - SPI
	// Extra Byte Trasmit Mode        [29:28] :  0 - SPI
	// Dummy Bytes Trasmit Mode       [31:30] :  1 - Dual
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  1 - Dual
	// Extra Byte Enable                  [2] :  0 - Extra Byte Disable
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  2 - 2 Dummy Bytes
	// Command mode                       [6] :  0 - Always Send Command Byte
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  3 - 3 CLKs	
	
	SetDword (QSPIC_BURSTCMDA_REG, 0x4000003b);
	SetDword (QSPIC_BURSTCMDB_REG, 0x00003021);
}


void WND_AutoCfg_DualIO (void) {
	//-------------------------------------------------------------------
	//                      Fast Read Dual I/O
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : bb - Fast Read Dual I/O
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : a0 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  1 - Dual
	// Extra Byte Trasmit Mode        [29:28] :  1 - Dual
	// Dummy Bytes Trasmit Mode       [31:30] :  1 - Dual
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  1 - Dual
	// Extra Byte Enable                  [2] :  1 - Extra Byte Enabled
	// Extra Half Disable Out             [3] :  1 - Disable output during the transmition of bits [3:0] of Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  0 - 0 Dummy Bytes
	// Command mode                       [6] :  1 - Send Command Byte only in the first access.
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  3 - 3 CLKs	
			
	SetDword (QSPIC_BURSTCMDA_REG, 0x54a000bb);
	SetDword (QSPIC_BURSTCMDB_REG, 0x0000304d);
}


void WND_AutoCfg_FastRead (void) {
	//-------------------------------------------------------------------
	//                          Fast Read
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : 0b - Fast Read
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : 00 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  0 - SPI
	// Extra Byte Trasmit Mode        [29:28] :  0 - SPI
	// Dummy Bytes Trasmit Mode       [31:30] :  0 - SPI
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  0 - SPI
	// Extra Byte Enable                  [2] :  0 - Extra Byte Disable
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  1 - 1 Dummy Byte
	// Command mode                       [6] :  0 - Always Send Command Byte
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  3 - 3 CLKs	
	
	SetDword (QSPIC_BURSTCMDA_REG, 0x0000000b);
	SetDword (QSPIC_BURSTCMDB_REG, 0x00003010);
}


void WND_AutoCfg_Read(void) {
	//-------------------------------------------------------------------
	//                          Read
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : 03 - Read
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : 00 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  0 - SPI
	// Extra Byte Trasmit Mode        [29:28] :  0 - SPI
	// Dummy Bytes Trasmit Mode       [31:30] :  0 - SPI
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  0 - SPI
	// Extra Byte Enable                  [2] :  0 - Extra Byte Disable
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  0 - 0 Dummy Byte
	// Command mode                       [6] :  0 - Always Send Command Byte
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  3 - 3 CLKs	
	
	SetDword (QSPIC_BURSTCMDA_REG, 0x00000003);
	SetDword (QSPIC_BURSTCMDB_REG, 0x00003000);
}



//--------------------------------------------------------------------------------------
//                           
//--------------------------------------------------------------------------------------

int WND_InitFlashDevice(void) {
	unsigned char WNDDescNum;
	unsigned char WNDJedecID[3];
	
	// IO3 = 1, IO2 = 1
	qspic_disable_quad_pads();
	
	// Escape from Performance Enchanced Mode
	WND_ResetQuadModeBits();
	WND_ResetDualModeBits();
	
	// Release From Deeep Power Down 
	WND_ReleasePDM_HPM();
	
	// Wait to exit from power down mode.
	qspic_delay();
	
	// Read Jedec ID
	WND_ReadJEDECID(WNDJedecID);

	// Check if the flash memory is the expected
	
	if (qspic_find_flash_desc (WNDJedecID, &WNDDescNum) == -1)
		return(-1);
	
	if (FlashDes[WNDDescNum].ManID != WINBOND_ID)
		return (-1);

	// Save the Flash Memory Desrciptor number
	qspicFDev.desc_num = WNDDescNum;
	
	// Initialize the Status registers. 
	// Disable Write Protection and Enable Quad mode
	WND_WriteStatus(0x0,WND_STATUS_QE);
	
	// IO3 hi-z, IO2 hi-z 
	qspic_enable_quad_pads();
	
	// Enable high frequencies
	WND_SetHighPerf();
	
	// The selected read instruction during the Auto mode
	WND_AutoCfg_QuadIO();

	// Install handlers
	qspicFDev.start_auto   = WND_StartAutoMode;		     
	qspicFDev.stop_auto    = WND_StopAutoMode;		     
	qspicFDev.read_byte    = WND_ReadQuadOutputByte;	     
	qspicFDev.read_half    = WND_ReadQuadOutputHalf;	     
	qspicFDev.read_word    = WND_ReadQuadOutputWord;	     
	qspicFDev.erase_chip   = WND_EraseChip; 		     
	qspicFDev.erase_sector = WND_EraseSector;	  // 4KB Sector Erase		     
	qspicFDev.erase_block  = WND_EraseBlock64KB;	  // 64KB Block Erase
	qspicFDev.erase_parms  = WND_EraseSector;	  // 4KB Sector Erase
	qspicFDev.write_page   = WND_WritePageQuad;		     

	qspicFDev.auto_max_clk   = FlashDes[qspicFDev.desc_num].Freqs[QUAD_READ_FREQ];
	qspicFDev.manual_max_clk = FlashDes[qspicFDev.desc_num].Freqs[QUAD_READ_FREQ];
	
	// Returns the size of the memory in Mbits
	return (FlashDes[qspicFDev.desc_num].Capacity);
}

void WND_StartAutoMode(void) {
	qspic_auto_mode();
	qspic_set_closer_freq(qspicFDev.auto_max_clk);
}

void WND_StopAutoMode(void) {
	qspic_manual_mode();
	WND_ResetQuadModeBits();
	qspic_set_closer_freq(qspicFDev.manual_max_clk);
}

