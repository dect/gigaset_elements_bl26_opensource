#include "sitel_io.h"
#include "qspic.h"
#include "qspic/qspic_common.h"
#include "qspic/spansion.h"

//--------------------------------------------------------------------------------------
//                  Functions related to Status and Configuration Registers
//--------------------------------------------------------------------------------------
// Wait until the end of write operation
void SPANSION_WaitEndOfWrite(void)
{
	// Single Mode, CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	

	// Read Status 
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_RSTAT);

	// Wait end of write operation
	while ( GetByte(QSPIC_READDATA_REG) & SPANSION_STATUS_WIP);

	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Read Status Register
unsigned char SPANSION_ReadStatus (void)
{
	unsigned char r_status;
	
	// Single Mode, CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	

	// Read Status 
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_RSTAT);

	r_status = GetByte(QSPIC_READDATA_REG);

	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	return (r_status);
}

// Clear Status register (P_ERR and E_ERR bits)
void SPANSION_ClearStatus (void)
{
	// Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Clear Status 
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_CLSR);
}

// Read Configuratin Register
unsigned char SPANSION_ReadConf (void)
{
	unsigned char r_conf;
	
	// Single Mode, CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	

	// Read Status 
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_RCR);

	r_conf = GetByte(QSPIC_READDATA_REG);

	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	return (r_conf);
}

// Write Status and Configuration Registers
void SPANSION_WriteRegs (unsigned char w_status, unsigned char w_conf) {
	
	// Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);
	
	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_WEn);
	
	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Write Status an configuration Registers
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_WRR);
	SetByte(QSPIC_WRITEDATA_REG, w_status);
	SetByte(QSPIC_WRITEDATA_REG, w_conf);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);

	// Wait end of write
	SPANSION_WaitEndOfWrite();
}


//--------------------------------------------------------------------------------------
//                                   Read ID Functions
//-------------------------------------------------------------------------------------- 

// Read Jedec ID
void SPANSION_ReadJEDECID (unsigned char *JIDBBuf)
{

	unsigned int i;
	
	// Single Mode, CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);

	// Read Jedec ID Instruction
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_JID);
	
	// 1 byte Manufacture ID, 2 bytes ID Memory Type, 4 bytes extended device ID
		
	for (i=0; i<7; i++)
		JIDBBuf[i] = GetByte(QSPIC_READDATA_REG);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);	
}

//--------------------------------------------------------------------------------------
//                    Functions to Control the mode of the flash memory
//-------------------------------------------------------------------------------------- 

// Release From Deep Power Down 
void SPANSION_SetNormalMode (void)
{
	// Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);
	
	// Release from Deep Power Down instruction
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_PDID);
}

// Set Quad Enable Bit
void SPANSION_SetQuadEnable (void)
{
	unsigned char r_status;
	unsigned char r_conf;
	
	// Read Status Register	
	r_status = SPANSION_ReadStatus();
	
	// Read Configuration Register
	r_conf  = SPANSION_ReadConf();

	// Write Status and configuration Registers - Set Quad Enable Bit
	SPANSION_WriteRegs(r_status, r_conf | SPANSION_CONF_QUAD);
	
	//IO3 hi-z, IO2 hi-z 
	qspic_enable_quad_pads();
}

// Clear Quad Enable Bit
void SPANSION_ClearQuadEnable (void)
{
	unsigned char r_status;
	unsigned char r_conf;
	
	// Read Status Register	
	r_status = SPANSION_ReadStatus();
	
	// Read Configuration Register
	r_conf  = SPANSION_ReadConf();

	// Write Status and configuration Registers - Clear Quad Enable Bit
	SPANSION_WriteRegs(r_status, r_conf & ~SPANSION_CONF_QUAD);

	//IO3 output = 1, IO2 output = 1 
	qspic_disable_quad_pads();
}

// Exit from Performance enhanced mode when a Quad I/O read is used.
void SPANSION_ResetQuadModeBits(void) {
	// Set SPI Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);

	// Reset Mode Bit
	SetByte(QSPIC_WRITEDATA_REG, 0xff);
}


// Exit from Performance enhanced mode when a Dual I/O read is used.
void SPANSION_ResetDualModeBits(void) {
	// Set Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);

	// Reset Mode Bit
	SetWord(QSPIC_WRITEDATA_REG, 0xffff);
}


//--------------------------------------------------------------------------------------
//                                   Erase Functions
//--------------------------------------------------------------------------------------

// Erase Chip
void SPANSION_EraseChip (void)
{
	// Set SPI Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_WEn);

	// Chip Erase
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_BE);
	
	// Wait end of Erase
	SPANSION_WaitEndOfWrite();
}

// Uniform 64KB sector erase
long int SPANSION_EraseSector (unsigned long int Address)
{
	
	if (Address > FlashDes[qspicFDev.desc_num].MaxAddr)
		return (-1);
	
	// Set SPI Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_WEn);

	// CS Low
	SetDword(QSPIC_CTRLBUS_REG,QSPIC_EN_CS);
	
	// Sector 4KB Erase
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_SE);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
		
	// Wait end of Erase
	SPANSION_WaitEndOfWrite();
	
	// Check Erase Error Bit
	if (SPANSION_ReadStatus() & SPANSION_STATUS_E_ERR) {
		SPANSION_ClearStatus();
		return (-2);
	} else
		return ((long int)64*1024);
}

// 4KB Erase for parameter sectors only
long int SPANSION_EraseParamSector4KB (unsigned long int Address)
{
	// Set SPI Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_WEn);

	// CS Low
	SetDword(QSPIC_CTRLBUS_REG,QSPIC_EN_CS);
	
	// Sector 4KB Erase
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_P4E);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
		
	// Wait end of Erase
	SPANSION_WaitEndOfWrite();
	
	// Check Erase Error Bit
	if (SPANSION_ReadStatus() & SPANSION_STATUS_E_ERR) {
		SPANSION_ClearStatus();
		return (-2);
	} else
		return (4*1024);
}

// 8KB Erase for parameter sectors only
long int SPANSION_EraseParamSector8KB (unsigned long int Address)
{
	// Set SPI Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_WEn);

	// CS Low
	SetDword(QSPIC_CTRLBUS_REG,QSPIC_EN_CS);
	
	// Sector 4KB Erase
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_P8E);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
		
	// Wait end of Erase
	SPANSION_WaitEndOfWrite();
	
	// Check Erase Error Bit
	if (SPANSION_ReadStatus() & SPANSION_STATUS_E_ERR) {
		SPANSION_ClearStatus();
		return (-2);
	} else
		return (8*1024);
}

//--------------------------------------------------------------------------------------
//                                   Write Data Functions
//--------------------------------------------------------------------------------------

// Write a page (256 bytes) using single mode
int SPANSION_WritePageSingle (unsigned long int Address, unsigned long int *WPage)
{
	unsigned int i;

	if (Address > FlashDes[qspicFDev.desc_num].MaxAddr)
		return (-1);
	
	// Set SPI Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_WEn);
	
	// CS Low
	SetDword(QSPIC_CTRLBUS_REG,QSPIC_EN_CS);
	
	// Program Page 
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_PPROG);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	for (i=0; i<64; i=i+1) 
		SetDword(QSPIC_WRITEDATA_REG, WPage[i]);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
		
	// Wait end of Write
	SPANSION_WaitEndOfWrite();
	
	// Check Program Error Bit
	if (SPANSION_ReadStatus() & SPANSION_STATUS_P_ERR) {
		SPANSION_ClearStatus();
		return (-2);
	} else
		return (256);
}

// Write a page (256 bytes) using quad mode
int SPANSION_WritePageQuad (unsigned long int Address, unsigned long int *WPage)
{
	unsigned int i;
	
	if (Address > FlashDes[qspicFDev.desc_num].MaxAddr)
		return (-1);
	
	// Set SPI Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_WEn);
	
	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);	
	
	// Program Page 
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_QPPROG);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Set Quad Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);	
	
	for (i=0; i<64; i++)
		SetDword(QSPIC_WRITEDATA_REG, WPage[i]);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	// Wait end of Write
	SPANSION_WaitEndOfWrite();
	
	// Check Program Error Bit
	if (SPANSION_ReadStatus() & SPANSION_STATUS_P_ERR) {
		SPANSION_ClearStatus();
		return (-2);
	} else
		return (256);
}

//--------------------------------------------------------------------------------------
//                           Read Data functions during Manual Mode
//--------------------------------------------------------------------------------------

// Slow Byte Read
void SPANSION_ReadDataByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf)
{
	unsigned long int i;

	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_READ);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address  & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Read Data
	for (i=0; i<BSize; i++) 
		BBuf[i] = GetByte(QSPIC_READDATA_REG);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Fast Byte Read
void SPANSION_ReadFastByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf)
{
	unsigned long int i;

	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_FREAD);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);
	
	// Read Data
	for (i=0; i<BSize; i++) 
    	BBuf[i] = GetByte(QSPIC_READDATA_REG);
	
    // CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Quad Output Byte Read
void SPANSION_ReadQuadOutputByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf)
{
	unsigned long int i;
	
	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_FRQDATA);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);

	// Set Quad mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);
	
	// Read Data
	for (i=0; i<BSize; i++) 
		BBuf[i] = GetByte(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Quad Output Half Read
void SPANSION_ReadQuadOutputHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf)
{
	unsigned long int i;

	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_FRQDATA);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);

	// Set Quad mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);
	
	// Read Data
	for (i=0; i<HSize; i=i+1) 
		HBuf[i] = GetWord(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Quad Output Word Read
void SPANSION_ReadQuadOutputWord (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf)
{
	unsigned long int i;
	
	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_FRQDATA);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);

	// Set Quad mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);
	
	// Read Data
	for (i=0; i<WSize; i++) 
		WBuf[i] = GetDword(QSPIC_READDATA_REG);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Dual Output Byte Read
void SPANSION_ReadDualOutputByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf)
{
	unsigned long int i;
	
	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_FRDDATA);
	SetWord(QSPIC_WRITEDATA_REG,  ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);

	// Set Dual mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_DUAL);
	
	// Read Data
	for (i=0; i<BSize; i++) 
		BBuf[i] = GetByte(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Dual Output Half Read
void SPANSION_ReadDualOutputHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf)
{
	unsigned long int i;
	
	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_FRDDATA);
	SetWord(QSPIC_WRITEDATA_REG,  ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);

	// Set Dual mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_DUAL);
	
	// Read Data
	for (i=0; i<HSize; i++) 
		HBuf[i] = GetWord(QSPIC_READDATA_REG);
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Dual Output Word Read
void SPANSION_ReadDualOutputWord (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf)
{
	unsigned long int i;

	// Single Mode, CS Low	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, SPANSION_INST_FRDDATA);
	SetWord(QSPIC_WRITEDATA_REG,  ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);

	// Set Dual mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_DUAL);
	
	// Read Data
	for (i=0; i<WSize; i++) 
		WBuf[i] = GetDword(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}



//--------------------------------------------------------------------------------------
//                Configurations for the Read sequence during Auto Mode
//--------------------------------------------------------------------------------------

void SPANSION_AutoCfg_QuadOutput(void)
{

	//-------------------------------------------------------------------
	//                      Fast Read Quad Output
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : 6b - Fast Read Quad Output
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : 00 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  0 - SPI 
	// Extra Byte Trasmit Mode        [29:28] :  0 - SPI 
	// Dummy Bytes Trasmit Mode       [31:30] :  2 - Quad
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  2 - Quad
	// Extra Byte Enable                  [2] :  0 - Extra Byte Disabled
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  3 - 4 Dummy Bytes
	// Command mode                       [6] :  0 - Always Send Command Byte
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  3 - 3 CLKs	
	

	SetDword (QSPIC_BURSTCMDA_REG, 0x8000006b);
	SetDword (QSPIC_BURSTCMDB_REG, 0x00003032);
}

void SPANSION_AutoCfg_QuadIO(void)
{
	//-------------------------------------------------------------------
	//                      Fast Read Quad I/O
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : eb - Fast Read Quad I/O
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : a0 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  2 - Quad 
	// Extra Byte Trasmit Mode        [29:28] :  2 - Quad
	// Dummy Bytes Trasmit Mode       [31:30] :  2 - Quad
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  2 - Quad
	// Extra Byte Enable                  [2] :  1 - Extra Byte Enabled
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  2 - 2 Dummy Bytes
	// Command mode                       [6] :  1 - Send Command Byte only in the first access.
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  0 - 0 CLKs	

	SetDword (QSPIC_BURSTCMDA_REG, 0xa8a000eb);
	SetDword (QSPIC_BURSTCMDB_REG, 0x00000066);
}

	
void SPANSION_AutoCfg_DualOutput(void)
{	
	//-------------------------------------------------------------------
	//                      Fast Read Dual Output
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : 3b - Fast Read Dual Output
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : 00 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  0 - SPI
	// Extra Byte Trasmit Mode        [29:28] :  0 - SPI
	// Dummy Bytes Trasmit Mode       [31:30] :  1 - Dual
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  1 - Dual
	// Extra Byte Enable                  [2] :  0 - Extra Byte Disable
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  2 - 2 Dummy Bytes
	// Command mode                       [6] :  0 - Always Send Command Byte
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  3 - 3 CLKs	
	
	SetDword (QSPIC_BURSTCMDA_REG, 0x4000003b);
	SetDword (QSPIC_BURSTCMDB_REG, 0x00003021);
}

void SPANSION_AutoCfg_DualIO(void)
{
	//-------------------------------------------------------------------
	//                      Fast Read Dual I/O
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : bb - Fast Read Dual I/O
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : a0 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  1 - Dual
	// Extra Byte Trasmit Mode        [29:28] :  1 - Dual
	// Dummy Bytes Trasmit Mode       [31:30] :  1 - Dual
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  1 - Dual
	// Extra Byte Enable                  [2] :  1 - Extra Byte Enabled
	// Extra Half Disable Out             [3] :  1 - Disable output during the transmition of bits [3:0] of Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  0 - 0 Dummy Bytes
	// Command mode                       [6] :  1 - Send Command Byte only in the first access.
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  3 - 3 CLKs	
			
	SetDword (QSPIC_BURSTCMDA_REG, 0x54a000bb);
	SetDword (QSPIC_BURSTCMDB_REG, 0x0000304d);
	
}	

void SPANSION_AutoCfg_FastRead(void)
{
	//-------------------------------------------------------------------
	//                          Fast Read
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : 0b - Fast Read
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : 00 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  0 - SPI
	// Extra Byte Trasmit Mode        [29:28] :  0 - SPI
	// Dummy Bytes Trasmit Mode       [31:30] :  0 - SPI
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  0 - SPI
	// Extra Byte Enable                  [2] :  0 - Extra Byte Disable
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  1 - 1 Dummy Byte
	// Command mode                       [6] :  0 - Always Send Command Byte
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  3 - 3 CLKs	
	
	SetDword (QSPIC_BURSTCMDA_REG, 0x0000000b);
	SetDword (QSPIC_BURSTCMDB_REG, 0x00003010);
}	

void SPANSION_AutoCfg_Read(void)
{	
	//-------------------------------------------------------------------
	//                          Read
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : 03 - Read
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : 00 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  0 - SPI
	// Extra Byte Trasmit Mode        [29:28] :  0 - SPI
	// Dummy Bytes Trasmit Mode       [31:30] :  0 - SPI
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  0 - SPI
	// Extra Byte Enable                  [2] :  0 - Extra Byte Disable
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  0 - 0 Dummy Byte
	// Command mode                       [6] :  0 - Always Send Command Byte
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  3 - 3 CLKs	
	
	
	SetDword (QSPIC_BURSTCMDA_REG, 0x00000003);
	SetDword (QSPIC_BURSTCMDB_REG, 0x00003000);
}

//--------------------------------------------------------------------------------------
//                           
//--------------------------------------------------------------------------------------

int SPANSION_InitFlashDevice(void) {
	unsigned char SPANSIONDescNum;
	unsigned char SPANSIONJedecID[7];

	// IO3 = 1, IO2 = 1
	qspic_disable_quad_pads();
	
	// Escape from Performance Enchanced Mode
	SPANSION_ResetQuadModeBits();
	SPANSION_ResetDualModeBits();
	
	// Release From Deeep Power Down
	SPANSION_SetNormalMode();
	
	// Wait to exit from power down mode.
	qspic_delay();
	
	// Read Jedec ID
	SPANSION_ReadJEDECID(SPANSIONJedecID);
	
	// Check if the flash memory is the expected
	
	if (qspic_find_flash_desc (SPANSIONJedecID, &SPANSIONDescNum) == -1)
		return(-1);
	
	if (FlashDes[SPANSIONDescNum].ManID != SPANSION_ID)
		return (-1);
	
	// Save the Flash Memory Desrciptor number
	qspicFDev.desc_num = SPANSIONDescNum;
	
	// Initialize the Status and configuration registers. 
	// Disable Write Protection and Enable Quad mode
	SPANSION_WriteRegs(0x0, SPANSION_CONF_QUAD);
	
	// IO3 hi-z, IO2 hi-z 
	qspic_enable_quad_pads();
	
	// The selected read instruction during the Auto mode
	SPANSION_AutoCfg_QuadIO();
	
	// Install handlers
	qspicFDev.start_auto   = SPANSION_StartAutoMode;		     
	qspicFDev.stop_auto    = SPANSION_StopAutoMode; 		     
	qspicFDev.read_byte    = SPANSION_ReadQuadOutputByte;		     
	qspicFDev.read_half    = SPANSION_ReadQuadOutputHalf;		     
	qspicFDev.read_word    = SPANSION_ReadQuadOutputWord;		     
	qspicFDev.erase_chip   = SPANSION_EraseChip;			     
	qspicFDev.erase_sector = SPANSION_EraseSector;         // 64KB Sector Erase	     
	qspicFDev.erase_block  = SPANSION_EraseSector;         // 64KB Sector Erase
	qspicFDev.erase_parms  = SPANSION_EraseParamSector4KB; // 4KB Special Parameter Sector Erase
	qspicFDev.write_page   = SPANSION_WritePageQuad;
	
	qspicFDev.auto_max_clk   = FlashDes[qspicFDev.desc_num].Freqs[QUAD_READ_FREQ];
	qspicFDev.manual_max_clk = FlashDes[qspicFDev.desc_num].Freqs[QUAD_READ_FREQ];
	
	// Returns the size of the memory in Mbits
	return (FlashDes[qspicFDev.desc_num].Capacity);	
}

void SPANSION_StartAutoMode(void) {
	qspic_auto_mode();
	qspic_set_closer_freq(qspicFDev.auto_max_clk);
}

void SPANSION_StopAutoMode(void) {
	qspic_manual_mode();
	SPANSION_ResetQuadModeBits();
	qspic_set_closer_freq(qspicFDev.manual_max_clk);
}
