/*
 * (C) Copyright 2002
 * Kyle Harris, Nexus Technologies, Inc. kharris@nexus-tech.net
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */


#include "armboot.h"
#include "sitel_io.h"
#include "spi_polled.h"
#include "qspic.h"

#include "nt75451.h"
/* ------------------------------------------------------------------------- */

/*
 * Miscelaneous platform dependent initialisations
 */



extern void close_switch(void);
extern int kbd_configure(void);
extern unsigned char kbd_getc(void);

int 
/**********************************************************/
board_post_init(bd_t *bd)
/**********************************************************/
{

	SetPort(P0_05_MODE_REG, PORT_OUTPUT,  PID_port);					/* P0_06 is SPI_CLK  */

	SetWord(P0_SET_DATA_REG,0x20);
	udelay(50000);
	SetWord(P0_RESET_DATA_REG,0x20);
	udelay(50000);
	SetWord(P0_SET_DATA_REG,0x20);
	udelay(50000);
	SetWord(P0_RESET_DATA_REG,0x20);
	udelay(50000);
	SetWord(P0_SET_DATA_REG,0x20);

   return 0;
}

int 
/**********************************************************/
board_init(bd_t *bd)
/**********************************************************/
{
	unsigned int temp;
	
	/* arch number of Sitel SC14450 DK */
	bd->bi_arch_number = 452;

	/* vm must change */ 
	/* adress of boot parameters */
	bd->bi_boot_params = CFG_CONFIG_PARAMETERS_ADDR;//0xa0000100;

	/*   Config PPA for UART. */
	SetPort(P0_00_MODE_REG, PORT_OUTPUT,    PID_UTX);
	SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_URX);


#ifdef CONFIG_FLASH_IS_QUAD_SPI
	qspic_init();
#else
	init_SPI2(CLK_5184, MODE_3, MODE_8BITS);
#endif

// enable timer1 clock
	if (GetWord(CLK_GLOBAL_REG) & SW_XTAL_PLL1_RATE)
		SetBits(CLK_GPIO4_REG, SW_TMR1_DIV, 144);
	else
		SetBits(CLK_GPIO4_REG, SW_TMR1_DIV, 72);
	
	SetBits(CLK_GPIO4_REG, SW_TMR1_EN, 1);

	SetWord(BAT_CTRL_REG,0x412c);
//	SetWord(GPRG_R0_REG,0x2980);
//	SetWord(GPRG_R1_REG,0xa000);

//	close_switch();

//	kbd_configure();

	return 1;

}

int 
/**********************************************************/
dram_init(bd_t *bd)
/**********************************************************/
{
  	bd->bi_dram[0].start = PHYS_SDRAM_1;
	bd->bi_dram[0].size  = PHYS_SDRAM_1_SIZE;
	
	
	bd->bi_dram[1].start = PHYS_SDRAM_2;
	bd->bi_dram[1].size = PHYS_SDRAM_2_SIZE;
	
	return PHYS_SDRAM_1_SIZE+PHYS_SDRAM_2_SIZE;
 
}


