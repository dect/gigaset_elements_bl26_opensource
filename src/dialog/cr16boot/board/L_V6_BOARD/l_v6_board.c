/*
 * (C) Copyright 2002
 * Kyle Harris, Nexus Technologies, Inc. kharris@nexus-tech.net
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */


#include "armboot.h"
#include "sitel_io.h"
#include "spi_polled.h"
#include "qspic.h"

#include "nt75451.h"
/* ------------------------------------------------------------------------- */

/*
 * Miscelaneous platform dependent initialisations
 */



extern void close_switch(void);

int 
/**********************************************************/
board_post_init(bd_t *bd)
/**********************************************************/
{

   return 0;
}




int 
/**********************************************************/
board_init(bd_t *bd)
/**********************************************************/
{
	unsigned int temp,i,j;
	
	/* arch number of Sitel SC14450 DK */
	bd->bi_arch_number = 452;

	/* vm must change */ 
	/* adress of boot parameters */
	bd->bi_boot_params = CFG_CONFIG_PARAMETERS_ADDR;//0xa0000100;

	/*   Config PPA for UART. */
	SetPort(P0_00_MODE_REG, PORT_OUTPUT,    PID_UTX);
	SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_URX);


#ifdef CONFIG_FLASH_IS_QUAD_SPI
	qspic_init();
#else
	init_SPI2(CLK_5184, MODE_3, MODE_8BITS);
#endif

// enable timer1 clock
	if (GetWord(CLK_GLOBAL_REG) & SW_XTAL_PLL1_RATE)
		SetBits(CLK_GPIO4_REG, SW_TMR1_DIV, 144);
	else
		SetBits(CLK_GPIO4_REG, SW_TMR1_DIV, 72);
	
	SetBits(CLK_GPIO4_REG, SW_TMR1_EN, 1);
	

//reset lmx
	SetPort(P2_03_MODE_REG, PORT_OUTPUT,   PID_port);

	SetWord(P2_SET_DATA_REG,8);
	for (i=0;i<0x800;i++)
		j=*(volatile unsigned*)0;
	SetWord(P2_RESET_DATA_REG,8);
	for (i=0;i<0x800;i++)
		j=*(volatile unsigned*)0;
	SetPort(P2_03_MODE_REG, PORT_INPUT,   PID_port);


	SetWord(P2_RESET_DATA_REG,8);
	SetPort(P2_13_MODE_REG, PORT_OUTPUT,   PID_LE);
	SetPort(P2_14_MODE_REG, PORT_OUTPUT,   PID_SK);
	SetPort(P2_15_MODE_REG, PORT_OUTPUT,   PID_SIO);
	
	
	/* Set PPA MATRIX FOR SPI1 */
	SetPort(P1_02_MODE_REG, PORT_OUTPUT,  PID_SPI1_CLK);	/* P1_02 is SPI_CLK  */
	SetPort(P1_13_MODE_REG, PORT_INPUT ,  PID_SPI1_DIN);    /* P1_13 is SPI_DI  */
	SetPort(P2_04_MODE_REG, PORT_OUTPUT,  PID_SPI1_DOUT);	/* P2_04 is SPI_DO */
	SetPort(P0_07_MODE_REG, PORT_OUTPUT,  PID_port);	/* P0_07 as SPI_EN  */

	SetPort(P0_09_MODE_REG, PORT_OUTPUT, PID_SCL1);  	// Phy I2C CLK
	SetPort(P0_10_MODE_REG, PORT_OUTPUT, PID_SDA1);		// Phy I2C DATA
	
	SetPort(P2_10_MODE_REG, PORT_OUTPUT,  PID_PCM_FSC);       /* P2_10 is PCM_FSC */
	SetPort(P1_15_MODE_REG, PORT_INPUT,  PID_PCM_DI);         /* P1_15 is PCM_DI */
	SetPort(P1_00_MODE_REG, PORT_OUTPUT,  PID_PCM_DO);         /* P1_00 is PCM_DO */
	SetPort(P2_09_MODE_REG, PORT_OUTPUT,  PID_PCM_CLK);        /* P2_09 is PCM_CLK */


	

	SetWord(BAT_CTRL_REG,0x412c);
	// 8mA
	//SetWord(GPRG_R0_REG,0x2980);
	//SetWord(GPRG_R1_REG,0x5000);//changed by: IP, 02 June 2010//SetWord(GPRG_R1_REG,0xa000);

	// 4mA
	SetWord(GPRG_R0_REG,0xFD80);  
	
	
	SetWord(GPRG_R1_REG,0xF000);//changed by: IP, 02 June 2010//SetWord(GPRG_R1_REG,0xa000);

//	SetWord(BANDGAP_REG,0x0d);

	close_switch();


	return 1;

}

int 
/**********************************************************/
dram_init(bd_t *bd)
/**********************************************************/
{
  	bd->bi_dram[0].start = PHYS_SDRAM_1;
	bd->bi_dram[0].size  = PHYS_SDRAM_1_SIZE;
	
	
	bd->bi_dram[1].start = PHYS_SDRAM_2;
	bd->bi_dram[1].size = PHYS_SDRAM_2_SIZE;
	
	return PHYS_SDRAM_1_SIZE+PHYS_SDRAM_2_SIZE;
 
}



