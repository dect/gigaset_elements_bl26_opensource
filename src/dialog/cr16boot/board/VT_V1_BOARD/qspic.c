#include "sitel_io.h"
#include "qspic.h"
#include "qspic/qspic_common.h"
#include "qspic/sst.h"
#include "qspic/winbond.h"
#include "qspic/macronix.h"
#include "qspic/spansion.h"

const struct FlashDescriptor FlashDes[NUM_OF_FLASH_DESC] = {{SST_ID,      0x26, 0x01, {25, 80,  0, 80}, 16, 0x1fffff, "SST26VF016"},
                                                            {SST_ID,      0x26, 0x02, {25, 80,  0, 80}, 32, 0x3fffff, "SST26VF032"},
                                                            {SST_ID,      0x26, 0x03, {25, 80,  0, 80}, 64, 0x7fffff, "SST26VF064"},
							    {WINBOND_ID,  0x40, 0x14, {50, 80, 80, 80},  8, 0x0fffff, "W25Q80"    },
							    {WINBOND_ID,  0x40, 0x15, {50, 80, 80, 80}, 16, 0x1fffff, "W25Q16"    },
							    {WINBOND_ID,  0x40, 0x16, {50, 80, 80, 80}, 32, 0x3fffff, "W25Q32"    },
							    {MACRONIX_ID, 0x24, 0x15, {33, 86, 75, 75}, 16, 0x1fffff, "MX25L1635D"},   
							    {MACRONIX_ID, 0x24, 0x16, {33, 86, 75, 75}, 32, 0x3fffff, "MX25L3235D"},
							    {SPANSION_ID, 0x02, 0x15, {40, 104,80, 80}, 32, 0x3fffff, "S25FL032P" },
							    {SPANSION_ID, 0x02, 0x16, {40, 104,80, 80}, 64, 0x7fffff, "S25FL064P" }};


void qspic_set_auto_page( unsigned long int FlashAddress) {
	SetWord (GPRG_R1_REG, (GetWord (GPRG_R1_REG) & ~0x0fff) | (((unsigned long int) FlashAddress >> 12) & 0x0fff));
}

unsigned long int qspic_flash_to_sys_addr(unsigned long int FlashAddress) {
	return (QSPIC_AUTO_START + (FlashAddress & 0x00000fff));
}

char qspic_find_flash_desc (unsigned char *JedecID, unsigned char *desc_num) {
	unsigned char i;
	
	*desc_num=0;
	
	for (i=0; i< NUM_OF_FLASH_DESC; i++)
		if ((JedecID[0] == FlashDes[i].ManID) && 
		    (JedecID[1] == FlashDes[i].TypeID) &&
		    (JedecID[2] == FlashDes[i].CapID)) {
		    	*desc_num = i;
			return(1);
		}
		
	return(-1);
}

char qspic_autofind_flash_type (unsigned long *flash_type) {
	unsigned char JedecID[3];
	unsigned char desc_num;

	unsigned long ret;
	
	
	// IO3 = 1, IO2 = 1
	qspic_disable_quad_pads();
	
	// Set Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);

	// Reset Mode Bit - Quad Mode
	SetByte(QSPIC_WRITEDATA_REG, 0xFF);
	
	// Reset Mode Bit - Dual Mode
	SetWord(QSPIC_WRITEDATA_REG, 0xFFFF);
	
	// Release Power Down
	SetByte(QSPIC_WRITEDATA_REG, 0xAB);
	
	// Wait to exit from power down mode.
	qspic_delay();
	
	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Read Jedec ID Instruction
	SetByte(QSPIC_WRITEDATA_REG, 0x9f);
	
	//1 byte Manufacturer, 2 byte device ID 
	JedecID[0] = GetByte(QSPIC_READDATA_REG);
	JedecID[1] = GetByte(QSPIC_READDATA_REG);
	JedecID[2] = GetByte(QSPIC_READDATA_REG);

	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
//vm
ret = JedecID[2] | (JedecID[1]<<8) | (JedecID[0] <<16);
	
	if (qspic_find_flash_desc(JedecID, &desc_num) == -1)
		return (-1);
	
	*flash_type = ret;//FlashDes[desc_num].ManID;

	return(1);
}


struct flash_dev *qspic_dev_init (unsigned char flash_type) {
	switch (flash_type) {
		case SST_ID :
			qspicFDev.init_dev     = SST_InitFlashDevice;
			break;
			
		case WINBOND_ID :
			qspicFDev.init_dev     = WND_InitFlashDevice;
			break;
			
		case MACRONIX_ID :
			qspicFDev.init_dev     = MACRONIX_InitFlashDevice;
 			break;
			
		case SPANSION_ID :
			qspicFDev.init_dev     = SPANSION_InitFlashDevice;		     
			break;

		default :
			return ((struct flash_dev *) NULL);
	}


	if  (qspicFDev.init_dev()==-1)
		return((struct flash_dev *) NULL);
	else
		return(&qspicFDev);
}

void qspic_init (void) {
	// Program the Peripherals pin assignment matrix to enable the quad spi controller interface 
	qspic_pads_init();
	
	// Sets the qspi bus clock equal to hclk/2 and enables the qspic clock.
	// This is a safe selection because the hclk is always active.
	qspi_clk_set_HCLK_half();
	
	SetDword (QSPIC_CTRLMODE_REG, QSPIC_CLK_MD  | QSPIC_IO2_OEN | QSPIC_IO3_OEN | 
	                              QSPIC_IO2_DAT | QSPIC_IO3_DAT | QSPIC_RXD_NEG  );
	
	SetDword (QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_DIS_CS);
}
