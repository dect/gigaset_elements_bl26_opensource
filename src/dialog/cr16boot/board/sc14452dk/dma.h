#ifndef _DMA_H_
#define _DMA_H_

//void dma_irq_enable (unsigned char channel);

//void dma_irq_disable (unsigned char channel);

//void dma_install_handler (void (*handler) (void), unsigned char channel);

void dma_wait (unsigned char channel);


void dma_memcopy(unsigned long int src_addr, unsigned long int dest_src, 
                 unsigned int len,           unsigned char data_width, 
                 unsigned char max_burst,    unsigned char channel);
		 
void dma_meminit (unsigned long int dest_addr, unsigned int len,
                  unsigned char data_width,    unsigned char max_burst,
		  unsigned char init_value,    unsigned char channel);

void dma_port_write(unsigned long int src_addr, unsigned long int dest_src, 
                    unsigned int len,           unsigned char data_width, 
                    unsigned char max_burst,    unsigned char channel);

void dma_port_read(unsigned long int src_addr, unsigned long int dest_src, 
                 unsigned int len,           unsigned char data_width, 
                 unsigned char max_burst,    unsigned char channel);

#endif
