#include "sitel_io.h"
#include "qspic.h"
#include "qspic/qspic_common.h"
#include "qspic/macronix.h"

//--------------------------------------------------------------------------------------
//                              Functions related to Status Register
//--------------------------------------------------------------------------------------

// Wait until the end of write operation
void MACRONIX_WaitEndOfWrite(void)
{
	// Single Mode, CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	

	// Read Status 
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_RDSR);

	// Wait end of write operation
	while ( GetByte(QSPIC_READDATA_REG) & MACRONIX_STATUS_WIP);

	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Read Status Register
unsigned char MACRONIX_ReadStatus (void)
{
	unsigned char r_status;
	
	// Single Mode, CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	

	// Read Status 
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_RDSR);

	r_status = GetByte(QSPIC_READDATA_REG);

	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	return (r_status);
}

// Write Status Register
void MACRONIX_WriteStatus (unsigned char w_status) {
	
	// Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);
	
	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_WREN);
	
	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Write Status Register 
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_WRSR);
	SetByte(QSPIC_WRITEDATA_REG, w_status);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);

	// Wait end of write
	MACRONIX_WaitEndOfWrite();
}

//--------------------------------------------------------------------------------------
//                                   Read ID Functions
//-------------------------------------------------------------------------------------- 

// Read Jedec ID
void MACRONIX_ReadJEDECID (unsigned char *JIDBBuf)
{
	// Single Mode, CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Unique ID Instruction
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_RDID);
	
	//1 byte Manufacturer, 2 byte device ID 
	JIDBBuf[0] = GetByte(QSPIC_READDATA_REG);
	JIDBBuf[1] = GetByte(QSPIC_READDATA_REG);
	JIDBBuf[2] = GetByte(QSPIC_READDATA_REG);

	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

//--------------------------------------------------------------------------------------
//                    Functions to Control the mode of the flash memory
//-------------------------------------------------------------------------------------- 


// Sets the Quad Enable bit
void MACRONIX_SetQuadEnable (void) {

	unsigned char r_status;
	
	// Read Status Register	
	r_status = MACRONIX_ReadStatus();

	// Write Status Register - Set Quad Enable Bit
	MACRONIX_WriteStatus(r_status | MACRONIX_STATUS_QE);

	// IO3 hi-z, IO2 hi-z 
	qspic_enable_quad_pads();
}


// Clears the Quad Enable bit
void MACRONIX_ClearQuadEnable (void)
{
	unsigned char r_status;

	// Read Status Register	
	r_status = MACRONIX_ReadStatus();

	// Write Status Register - Clear Quad Enable Bit
	MACRONIX_WriteStatus(r_status & ~MACRONIX_STATUS_QE);

	//IO3 output = 1, IO2 output = 1 
	qspic_disable_quad_pads();
}

// Escape the high preformance mode
void MACRONIX_EscapePE (void) {
	// Quad Mode, CS low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD | QSPIC_EN_CS);

	// Reset Mode Bit
	SetDword(QSPIC_WRITEDATA_REG, 0xffffffff);
	SetWord(QSPIC_DUMMYDATA_REG, 0x0000);

	// CS high
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Release from Power Down Mode
void MACRONIX_ReleasePDM(void) {
	// Set Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);

	// Releas Power Down Mode
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_RDP);
}

//--------------------------------------------------------------------------------------
//                                   Erase Functions
//--------------------------------------------------------------------------------------

// Erase Chip
void MACRONIX_EraseChip (void)
{
	// Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_WREN);

	// Chip Erase
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_CE);
	
	// Wait end of Erase
	MACRONIX_WaitEndOfWrite();
}

// Uniform 4KB sector erase
long int MACRONIX_EraseSector (unsigned long int Address)
{
	if (Address > FlashDes[qspicFDev.desc_num].MaxAddr)
		return (-1);
		
	// Set SPI Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_WREN);

	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Sector 4KB Erase
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_SE);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	// Wait end of Erase
	MACRONIX_WaitEndOfWrite();
	
	return (4*1024);
}

// 64KB block erase
long int MACRONIX_EraseBlock (unsigned long int Address)
{
	if (Address > FlashDes[qspicFDev.desc_num].MaxAddr)
		return (-1);
		
	// Set SPI Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_WREN);

	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Sector 4KB Erase
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_BE);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	// Wait end of Erase
	MACRONIX_WaitEndOfWrite();
	
	return ((long int) 64 * 1024);
}

//--------------------------------------------------------------------------------------
//                                   Write Data Functions
//--------------------------------------------------------------------------------------

// Write a page (256 bytes) using single mode access
int MACRONIX_WritePageSingle (unsigned long int Address, unsigned long int *WPage)
{
	unsigned int i;

	if (Address > FlashDes[qspicFDev.desc_num].MaxAddr)
		return (-1);
	
	// Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_WREN);
	
	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Program Page 
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_PP);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	for (i=0; i<64; i=i+1) 
		SetDword(QSPIC_WRITEDATA_REG, WPage[i]);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
		
	// Wait end of Write
	MACRONIX_WaitEndOfWrite();
	
	return (256);
}

// Write a page (256 bytes) using quad mode access
int MACRONIX_WritePageQuad (unsigned long int Address, unsigned long int *WPage)
{
	unsigned int i;
    
	if (Address > FlashDes[qspicFDev.desc_num].MaxAddr)
		return (-1);
	
	// Single Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_WREN);
	
	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);	
	
	// Program Page 
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_QPP);
	
	// Set Quad Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);
	
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
	
	for (i=0; i<64; i++)
		SetDword(QSPIC_WRITEDATA_REG, WPage[i]);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	// Wait end of Write
	MACRONIX_WaitEndOfWrite ();
	
	return (256);
}

// Write in Continiusly Mode
long int MACRONIX_WriteCP (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf)
{
	unsigned long int i;
	
	if (Address > FlashDes[qspicFDev.desc_num].MaxAddr)
		return (-1);
	
	// Set SPI Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_WREN);
	
	for (i=0; i < HSize; i++) {
	
		// CS Low
		SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
			
		// Continuously Program
		SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_CP);
		
		if (i==0) {
			SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
			SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
		}
		
		SetWord(QSPIC_WRITEDATA_REG, HBuf[i]);
	
		// CS High
		SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
		
		// Wait end of Write
		MACRONIX_WaitEndOfWrite();
	}
		
	// Write Disable
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_WRDI);

	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);	
		
	// Read Security Register - Check CP
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_RDSCUR);
	
	while ( GetByte(QSPIC_READDATA_REG) & MACRONIX_SECURITY_CP);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	return  (2*HSize);
}

//--------------------------------------------------------------------------------------
//                           Read Data functions during Manual Mode
//--------------------------------------------------------------------------------------

// Slow Byte Read
void MACRONIX_ReadDataByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf)
{
	unsigned long int i;

	// Single Mode, CS Low 
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_READ);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address  & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Read Data
	for (i=0; i<BSize; i++) 
		BBuf[i] = GetByte(QSPIC_READDATA_REG);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Fast Byte Read
void MACRONIX_ReadFastByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf)
{
	unsigned long int i;

	// Single Mode, CS Low 
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_FREAD);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);
	
	// Read Data
	for (i=0; i<BSize; i=i+1) 
    	BBuf[i] = GetByte(QSPIC_READDATA_REG);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Quad IO Byte Read
void MACRONIX_ReadQuadIOByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf)
{
	unsigned long int i;
		
	// Single Mode, CS Low 
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_QREAD);

	// Set Quad mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);
	
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// 3 Dummy Bytes -> 6 dummy clocks
	SetByte(QSPIC_WRITEDATA_REG,0x0);       // Escape the performance enhance mode
	SetWord(QSPIC_DUMMYDATA_REG,0x0);

	
	// Read Data
	for (i=0; i<BSize; i++) 
		BBuf[i] = GetByte(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Quad IO Half Read
void MACRONIX_ReadQuadIOHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf)
{
	unsigned long int i;

	// Single Mode, CS Low 
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_QREAD);

	// Set Quad mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// 3 Dummy Bytes -> 6 dummy clocks
	SetByte(QSPIC_WRITEDATA_REG,0x0);       // Escape the performance enhance mode
	SetWord(QSPIC_DUMMYDATA_REG,0x0);

	// Read Data
	for (i=0; i<HSize; i++) 
		HBuf[i] = GetWord(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Quad IO Word Read
void MACRONIX_ReadQuadIOWord (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf)
{
	unsigned long int i;

	// Single Mode, CS Low 
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_QREAD);

	// Set Quad mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// 3 Dummy Bytes -> 6 dummy clocks
	SetByte(QSPIC_WRITEDATA_REG,0x0);       // Escape the performance enhance mode
	SetWord(QSPIC_DUMMYDATA_REG,0x0);
	
	// Read Data
	for (i=0; i<WSize; i++) 
		WBuf[i] = GetDword(QSPIC_READDATA_REG);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Dual IO Byte Read
void MACRONIX_ReadDualIOByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf)
{
	unsigned long int i;
		
	// Single Mode, CS Low 
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_DREAD);

	// Set Dual mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_DUAL);
	SetWord(QSPIC_WRITEDATA_REG,  ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);
	
	// Read Data
	for (i=0; i<BSize; i++) 
		BBuf[i] = GetByte(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Dual IO Half Read
void MACRONIX_ReadDualIOHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf)
{
	unsigned long int i;
		
	// Single Mode, CS Low 
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_DREAD);

	// Set Dual mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_DUAL);
	
	SetWord(QSPIC_WRITEDATA_REG,  ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);
	
	// Read Data
	for (i=0; i<HSize; i++) 
		HBuf[i] = GetWord(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Dual IO Word Read
void MACRONIX_ReadDualIOWord (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf)
{
	unsigned long int i;

	// Single Mode, CS Low 
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, MACRONIX_INST_DREAD);

	// Set Dual mode	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_DUAL);
	
	SetWord(QSPIC_WRITEDATA_REG,  ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);

	// Read Data
	for (i=0; i<WSize; i++) 
		WBuf[i] = GetDword(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}


//--------------------------------------------------------------------------------------
//                Configurations for the Read sequence during Auto Mode
//--------------------------------------------------------------------------------------


void MACRONIX_AutoCfg_QuadIO_NoPE(void)
{
	//-------------------------------------------------------------------
	//       Read Quad I/O - Escape the performance enhance mode
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : eb - Read Quad I/O
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : 00 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  2 - Quad 
	// Extra Byte Trasmit Mode        [29:28] :  2 - Quad
	// Dummy Bytes Trasmit Mode       [31:30] :  2 - Quad
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  2 - Quad
	// Extra Byte Enable                  [2] :  1 - Extra Byte Enabled
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  2 - 2 Dummy Bytes
	// Command mode                       [6] :  0 - Send Command Byte always.
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  0 - 0 CLKs	

	SetDword (QSPIC_BURSTCMDA_REG, 0xa80000eb);
	SetDword (QSPIC_BURSTCMDB_REG, 0x00000026);
}

void MACRONIX_AutoCfg_QuadIO_PE(void)
{	
	//-------------------------------------------------------------------
	//       Read Quad I/O - Enable the performance enhance mode
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : eb - Read Quad I/O
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : a5 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  2 - Quad 
	// Extra Byte Trasmit Mode        [29:28] :  2 - Quad
	// Dummy Bytes Trasmit Mode       [31:30] :  2 - Quad
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  2 - Quad
	// Extra Byte Enable                  [2] :  1 - Extra Byte Enabled
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  2 - 2 Dummy Bytes
	// Command mode                       [6] :  1 - Send Command Byte only in the first access.
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  0 - 0 CLKs
	
	SetDword (QSPIC_BURSTCMDA_REG, 0xa8a500eb);
	SetDword (QSPIC_BURSTCMDB_REG, 0x00000066);
}	


void MACRONIX_AutoCfg_DualIO (void) {
	//-------------------------------------------------------------------
	//                     Read Dual I/O
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : bb - Fast Read Dual I/O
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : 00 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  1 - Dual
	// Extra Byte Trasmit Mode        [29:28] :  1 - Dual
	// Dummy Bytes Trasmit Mode       [31:30] :  1 - Dual
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  1 - Dual
	// Extra Byte Enable                  [2] :  0 - Extra Byte Enabled
	// Extra Half Disable Out             [3] :  0 - Disable output during the transmition of bits [3:0] of Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  1 - 1 Dummy Bytes
	// Command mode                       [6] :  0 - Send Command Byte always.
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  3 - 3 CLKs	
	
	SetDword (QSPIC_BURSTCMDA_REG, 0x540000bb);
	SetDword (QSPIC_BURSTCMDB_REG, 0x00003011);
}

void MACRONIX_AutoCfg_FastRead(void) {

	//-------------------------------------------------------------------
	//                          Fast Read
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : 0b - Fast Read
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : 00 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  0 - SPI
	// Extra Byte Trasmit Mode        [29:28] :  0 - SPI
	// Dummy Bytes Trasmit Mode       [31:30] :  0 - SPI
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  0 - SPI
	// Extra Byte Enable                  [2] :  0 - Extra Byte Disable
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  1 - 1 Dummy Byte
	// Command mode                       [6] :  0 - Always Send Command Byte
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  3 - 3 CLKs	
	

	SetDword (QSPIC_BURSTCMDA_REG, 0x0000000b);
	SetDword (QSPIC_BURSTCMDB_REG, 0x00003010);
}

void MACRONIX_AutoCfg_Read (void) {	

	//-------------------------------------------------------------------
	//                          Read
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : 03 - Read
	// Command Value (WrapBurst)       [15:8] : 00
	// Extra byte                     [23:16] : 00 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  0 - SPI 
	// Address Trasmit Mode           [27:26] :  0 - SPI
	// Extra Byte Trasmit Mode        [29:28] :  0 - SPI
	// Dummy Bytes Trasmit Mode       [31:30] :  0 - SPI
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  0 - SPI
	// Extra Byte Enable                  [2] :  0 - Extra Byte Disable
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  0 - 0 Dummy Byte
	// Command mode                       [6] :  0 - Always Send Command Byte
	// Wrap mode                          [7] :  0 - Disable Wrap mode
	// Wrap Length                      [9:8] :  0 - Don't care
	// Wrap Size                      [11:10] :  0 - Don't care
	// CS High Min Num of CLKs        [14:12] :  3 - 3 CLKs	
	

	SetDword (QSPIC_BURSTCMDA_REG, 0x00000003);
	SetDword (QSPIC_BURSTCMDB_REG, 0x00003000);

}

//--------------------------------------------------------------------------------------
//                           
//--------------------------------------------------------------------------------------

int MACRONIX_InitFlashDevice(void) {
	unsigned char MACRONIXDescNum;
	unsigned char MACRONIXJedecID[3];
	
	// IO3 hi-z, IO2 hi-z 
	qspic_enable_quad_pads();
	
	// Escape from Performance Enchanced Mode
	MACRONIX_EscapePE();

	// IO3 = 1, IO2 = 1
	qspic_disable_quad_pads();
		
	// Release From Deeep Power Down
	MACRONIX_ReleasePDM();
	
	// Wait to exit from power down mode.
	qspic_delay();
	
	// Read Jedec ID
	MACRONIX_ReadJEDECID(MACRONIXJedecID);
	
	// Check if the flash memory is the expected
	
	if (qspic_find_flash_desc (MACRONIXJedecID, &MACRONIXDescNum) == -1)
		return(-1);
	
	if (FlashDes[MACRONIXDescNum].ManID != MACRONIX_ID)
		return (-1);
	
	// Save the Flash Memory Desrciptor number
	qspicFDev.desc_num = MACRONIXDescNum;
	
	// Initialize the Status register. Disable write protection and Enable Quad mode
	MACRONIX_WriteStatus(MACRONIX_STATUS_QE);
	
	// IO3 hi-z, IO2 hi-z 
	qspic_enable_quad_pads();
	
	// The selected read instruction during the Auto mode
	MACRONIX_AutoCfg_QuadIO_PE();
	
	// Install handlers
	qspicFDev.start_auto   = MACRONIX_StartAutoMode;	     
	qspicFDev.stop_auto    = MACRONIX_StopAutoMode; 	     
	qspicFDev.read_byte    = MACRONIX_ReadQuadIOByte;	     
	qspicFDev.read_half    = MACRONIX_ReadQuadIOHalf;	     
	qspicFDev.read_word    = MACRONIX_ReadQuadIOWord;	     
	qspicFDev.erase_chip   = MACRONIX_EraseChip;		     
	qspicFDev.erase_sector = MACRONIX_EraseSector;    // 4KB Sector Erase		     
	qspicFDev.erase_block  = MACRONIX_EraseBlock;	  // 64KB Sector Erase  	     
	qspicFDev.erase_parms  = MACRONIX_EraseSector;    // 4KB Sector Erase
	qspicFDev.write_page   = MACRONIX_WritePageQuad;	
	
	qspicFDev.auto_max_clk   = FlashDes[qspicFDev.desc_num].Freqs[QUAD_READ_FREQ];
	qspicFDev.manual_max_clk = FlashDes[qspicFDev.desc_num].Freqs[QUAD_READ_FREQ];
	
	// Returns the size of the memory in Mbits
	return (FlashDes[qspicFDev.desc_num].Capacity);
	
}

void MACRONIX_StartAutoMode(void) {
	// Set the qspic controller in Auto Mode
	qspic_auto_mode();
	qspic_set_closer_freq(qspicFDev.auto_max_clk);
}

void MACRONIX_StopAutoMode(void) {
	// Set the qspic controller in Manual Mode
	qspic_manual_mode();
	
	// Escape for Performance Enchanced Mode
	MACRONIX_EscapePE();
	qspic_set_closer_freq(qspicFDev.manual_max_clk);
}

