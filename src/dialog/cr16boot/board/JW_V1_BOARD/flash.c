/*
 * (C) Copyright 2002
 * Lineo, Inc. <www.lineo.com>
 * Bernhard Kuhn <bkuhn@lineo.com>
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Alex Zuepke <azu@sysgo.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <armboot.h>
#include <flash.h>
#include "spi_flash.h"
#include "qspic.h"



flash_info_t    flash_info[CFG_MAX_FLASH_BANKS];

#ifndef PHYS_FLASH_2
#warning PHYS_FLASH_2 was NOT defined!!! Assuming 1 FLASH AREA...
#define PHYS_FLASH_2 0xD0000000 
#endif

#define FLASH_BASE_ADDR(x)	((x == 0 ? PHYS_FLASH_1 : PHYS_FLASH_2))

#if 0 

#define READY 1
#define ERR   2
#define TMO   4
#endif




/*-----------------------------------------------------------------------
 */
void flash_identification (flash_info_t * info)
{
    volatile short ManID1 , DevID1 ;

#ifdef CONFIG_FLASH_IS_QUAD_SPI
	qspic_autofind_flash_type (&info->flash_id); 

#else
	info->flash_id = SpiFlashID();
#endif
    
    
 
	ManID1 = (info->flash_id >> 16);  
	DevID1 = (info->flash_id & 0xffff);  

	if (ManID1 == ST_MANID){
		printf ("ST: ");
		if (DevID1 == ST_ID_M25P16)
			printf ("M25P16 (16-megabit)\n");
        else if (DevID1 == ST_ID_M25P32)
        	printf ("M25P32 (32-megabit)\n");
    } else if (ManID1 == SPANSION_MANID){
		printf ("SPANSION: ");
		if (DevID1 == SPANSION_ID_S25FL064A)
			printf ("S25FL064A (64-megabit)\n");
    } else if (ManID1 == WINBOND_MANID){
		printf ("WINBOND: ");
		if (DevID1 == WINBOND_ID_W25X16)
			printf ("W25X16 (16-megabit)\n");
        else if (DevID1 == WINBOND_ID_W25X32)
        	printf ("W25X32 (32-megabit)\n");
		else if (DevID1 == WINBOND_ID_W25X64)
			printf ("W25X64 (64-megabit)\n");
		else if (DevID1 == WINBOND_ID_W25Q16)
			printf ("W25Q16 (16-megabit)\n");
		else if (DevID1 == WINBOND_ID_W25Q32)
			printf ("W25Q32 (32-megabit)\n");
		else if (DevID1 == WINBOND_ID_W25Q64)
			printf ("W25Q64 (64-megabit)\n");

		
    } else	if (ManID1 == SST_MANID){
		printf ("SST: ");
		if(DevID1 == SST_ID_SST25VF32)
			printf("25VF32 (32-megabit)\n");
	}
	else if (ManID1 == MXIC_MANID)
    {
		printf ("MXIC: ");
		if (DevID1 == MXIC_ID_MX25L1605D)
			printf ("MX25L1605D (16-megabit)\n");
        else if (DevID1 == MXIC_ID_MX25L3205D)
        	printf ("MX25L3205D (32-megabit)\n");
		else if (DevID1 == MXIC_ID_MX25L6405D)
			printf ("MX25L6405D (64-megabit)\n");
    }
	else if (ManID1 == EON_MANID)
	{
		printf("EON: ");
		if(DevID1 == EON_ID_EN25Q32A)
			printf("EN25Q32A (32-megabit)\n");
		else if(DevID1 == EON_ID_EN25Q64)
			printf("EN25Q64 (64-megabit)\n");
	}
    //Foxconn add end
    else {
		printf ("Unknown: detected manID=%x, devID=%x,......... \n", ManID1, DevID1);
		info->flash_id = FLASH_UNKNOWN;
	}
}


unsigned long flash_init (bd_t *bd)
{
	int i, j;

	for (i = 0; i < CFG_MAX_FLASH_BANKS; i++) {
		bd->bi_flash[i].start = flash_info[i].base   = FLASH_BASE_ADDR(i);
		bd->bi_flash[i].size  = flash_info[i].size   = PHYS_FLASH_SIZE;
		flash_info[i].sector_count  = CFG_MAX_FLASH_SECT;

		for (j = 0; j < CFG_MAX_FLASH_SECT; j++) {

			flash_info[i].start[j]   = flash_info[i].base + j*PHYS_FLASH_SECTOR_SIZE;
			flash_info[i].protect[j] = 0 ; //ProtectFlashSectorDetect(flash_info[i].base, j) ;
				
		}

		// mg - 
		flash_identification (&flash_info[i]);
#ifdef CONFIG_FLASH_IS_QUAD_SPI
		flash_info[i].pqspidev = qspic_dev_init (flash_info[i].flash_id >> 16);
#endif
	
	}

	  /* Protect monitor and environment sectors
     */
    flash_protect(FLAG_PROTECT_SET,
		  CFG_FLASH_BASE,
		  CFG_FLASH_BASE+0x20000-1,//CFG_FLASH_BASE + _armboot_end - _armboot_start,
		  &flash_info[0]);

    flash_protect(FLAG_PROTECT_SET,
		  CFG_ENV_ADDR,
		  CFG_ENV_ADDR + CFG_ENV_SIZE - 1,
		  &flash_info[0]);

	flash_protect(FLAG_PROTECT_SET,
		  CFG_HWCFG_ADDR,
		  CFG_HWCFG_ADDR + CFG_HWCFG_SIZE - 1,
		  &flash_info[0]);


	return 1;
}

/*-----------------------------------------------------------------------
 */

void flash_print_info (flash_info_t * info)
{
	int i;
	
	 volatile short ManID1 , DevID1 ;

	ManID1 = (info->flash_id >> 16);
	DevID1 = (info->flash_id & 0xffff);

	printf("Man: %4x, Dev: %4x\n", ManID1, DevID1);


	if (ManID1 == ST_MANID){
		printf ("ST: ");
		if (DevID1 == ST_ID_M25P16)
			printf ("M25P16 (16-megabit)\n");
        else if (DevID1 == ST_ID_M25P32)
        	printf ("M25P32 (32-megabit)\n");
        else
        {
        	printf ("Unknown Chip Type\n");
        	return;
        }

    } else if (ManID1 == SPANSION_MANID){
		printf ("SPANSION: ");
		if (DevID1 == SPANSION_ID_S25FL064A)
			printf ("S25FL064A (64-megabit)\n");
        else
        {
        	printf ("Unknown Chip Type\n");
        	return;
        }

    } else if (ManID1 == WINBOND_MANID){
		printf ("WINBOND: ");
		if (DevID1 == WINBOND_ID_W25X16)
			printf ("W25X16 (16-megabit)\n");
        else if (DevID1 == WINBOND_ID_W25X32)
        	printf ("W25X32 (32-megabit)\n");
		else if (DevID1 == WINBOND_ID_W25X64)
			printf ("W25X64 (64-megabit)\n");
		else if (DevID1 == WINBOND_ID_W25Q16)
			printf ("W25Q16 (16-megabit)\n");
		else if (DevID1 == WINBOND_ID_W25Q32)
			printf ("W25Q32 (32-megabit)\n");
		else if (DevID1 == WINBOND_ID_W25Q64)
			printf ("W25Q64 (64-megabit)\n");

        else
        {
        	printf ("Unknown Chip Type\n");
        	return;
        }


    } else	if (ManID1 == SST_MANID){
		printf ("SST: ");
		if(DevID1 == SST_ID_SST25VF32)
			printf("25VF32 (32-megabit)\n");
        else
        {
        	printf ("Unknown Chip Type\n");
        	return;
        }

	}
	else if (ManID1 == MXIC_MANID)
    {
		printf ("MXIC: ");
		if (DevID1 == MXIC_ID_MX25L1605D)
			printf ("MX25L1605D (16-megabit)\n");
        else if (DevID1 == MXIC_ID_MX25L3205D)
        	printf ("MX25L3205D (32-megabit)\n");
		else if (DevID1 == MXIC_ID_MX25L6405D)
			printf ("MX25L6405D (64-megabit)\n");
        else
        {
        	printf ("Unknown Chip Type\n");
        	return;
        }

    }
	else if (ManID1 == EON_MANID)
	{
		printf("EON: ");
		if(DevID1 == EON_ID_EN25Q32A)
			printf("EN25Q32A (32-megabit)\n");
		else if(DevID1 == EON_ID_EN25Q64)
			printf("EN25Q64 (64-megabit)\n");
        else
        {
        	printf ("Unknown Chip Type\n");
        	return;
        }

	}
    else
    {
    	printf ("Unknown Chip Type\n");
    	return;
    }



#if 0
	printf("Man: %4x, Dev: %4x\n", (info->flash_id >> 16), (info->flash_id & 0xffff));

	switch ((info->flash_id >> 16)) {
	case (ST_MANID):
		printf ("ST: ");
		break;
	case (SPANSION_MANID):
		printf ("SPANSION: ");
		break;
	case (WINBOND_MANID):
		printf ("WINBOND: ");
		break;
	case (SST_MANID):
		printf ("SST: ");
		break;
	case (MXIC_MANID):
		printf ("MXIC: ");
		break;
	case (EON_MANID):
		printf ("EON: ");
		break;

	default:
		printf ("Unknown Vendor ");
		break;
	}
                                                                                                                                     
	switch (info->flash_id & 0xffff) {
	case (ST_ID_M25P16):
		printf ("M25P16 (16-megabit)\n");
		break;
    case(ST_ID_M25P32):
    	printf ("M25P32 (32-megabit)\n");
		break;
	case (SPANSION_ID_S25FL064A):
		printf ("S25FL064A (64-megabit)\n");
		break;
	case (WINBOND_ID_W25X16):
		printf ("W25X16 (16-megabit)\n");
		break;
    case (WINBOND_ID_W25X32):
    	printf ("W25X32 (32-megabit)\n");
		break;
	case (WINBOND_ID_W25X64):
		printf ("W25X64 (64-megabit)\n");
		break;
	case (WINBOND_ID_W25Q16):
		printf ("W25Q16 (16-megabit)\n");
		break;
	case (WINBOND_ID_W25Q32):
		printf ("W25Q32 (32-megabit)\n");
		break;
	case (WINBOND_ID_W25Q64):
		printf ("W25Q64 (64-megabit)\n");
		break;
	case ( SST_ID_SST25VF32):
			printf("25VF32 (32-megabit)\n");
		break;
	case (MXIC_ID_MX25L6405D):
			printf ("MX25L6405D (64-megabit)\n");	
		break;
	case (EON_ID_EN25Q32A):
			printf ("EN25Q32A (32-megabit)\n");
		break;
	case (EON_ID_EN25Q64):
			printf ("EN25Q64 (64-megabit)\n");
		break;



	default:
		printf ("Unknown Chip Type\n");
		goto Done;
		break;
	}
#endif

	printf ("  Size: %ld MB in %ld Sectors\n",
		info->size >> 20, info->sector_count);

	printf ("  Sector Start Addresses:");
	for (i = 0; i < info->sector_count; i++) {
		if ((i % 5) == 0) {
			printf ("\n   ");
		}
		printf (" %08lX%s", info->start[i],
			info->protect[i] ? " (RO) " : " (RW) ");
	}
	printf ("\n");

  Done:
	return;
}

/*-----------------------------------------------------------------------
 */

int flash_erase (flash_info_t * info, int s_first, int s_last)
{
	int iflag, prot, sect;
	int rc = ERR_OK;

	/* first look for protection bits */

	if (info->flash_id == FLASH_UNKNOWN)
		return ERR_UNKNOWN_FLASH_TYPE;

	if ((s_first < 0) || (s_first > s_last)) {
		return ERR_INVAL;
	}

	switch ((info->flash_id >> 16)) {
	case (ST_MANID):
	case (SPANSION_MANID):
	case (WINBOND_MANID):
	case (SST_MANID):
    case (MXIC_MANID):
	case (EON_MANID):
		break;
	default:
		return ERR_UNKNOWN_FLASH_VENDOR;
		break;
	}
    
	prot = 0;
	for (sect = s_first; sect <= s_last; ++sect) {
		if (info->protect[sect]) {
			prot++;
		}
	}
	if (prot)
		return ERR_PROTECTED;

	/*
	 * Disable interrupts which might cause a timeout
	 * here. Remember that our exception vectors are
	 * at address 0 in the flash, and we don't want a
	 * (ticker) exception to happen while the flash
	 * chip is in programming mode.
	 */
	iflag = disable_interrupts ();

	/* Start erase on unprotected sectors */
	for (sect = s_first; sect <= s_last && !ctrlc (); sect++)
	{
		printf ("Erasing sector %2d ... ", sect);

		/* arm simple, non interrupt dependent timer */
		reset_timer_masked ();

		if (info->protect[sect] == 0)
		{	/* not protected */
#ifdef CONFIG_FLASH_IS_QUAD_SPI
			if (info->pqspidev->erase_sector(sect * PHYS_FLASH_SECTOR_SIZE  ) != PHYS_FLASH_SECTOR_SIZE) {

#else
			if (SpiFlashEraseSector(sect * PHYS_FLASH_SECTOR_SIZE , PHYS_FLASH_SECTOR_SIZE) != 0) {
#endif
				rc = ERR_PROG_ERROR;
				goto outahere;
			}
			printf ("ok.\n");
		} else {			/* it was protected */
			printf ("protected!\n");
		}
	}

	if (ctrlc ())
		printf ("User Interrupt!\n");

outahere:
	/* allow flash to settle - wait 10 ms */
	udelay_masked (10000);

	if (iflag)
		enable_interrupts ();

	return rc;
}

/*-----------------------------------------------------------------------
 * Copy memory to flash
 */
#ifdef CONFIG_FLASH_IS_QUAD_SPI
int QSpiFlashProgram(unsigned long dest_addr,unsigned char* data, unsigned long len)
{
	
	unsigned long index,page_offset,page_size;
	unsigned long word_count;
	int retvalue = 0;

	if (!len)
		return ERR_OK;
	if ((dest_addr+len) > PHYS_FLASH_SIZE)
		return ERR_INVAL;
	
	page_offset = dest_addr % FLASH_PAGE_SIZE;

	if ((page_offset + len) <= FLASH_PAGE_SIZE)  
	{
	//	retvalue = flash_info[0].pqspidev->write_page(dest_addr,data,len);
		retvalue = flash_info[0].pqspidev->write_page(dest_addr,data);
	//	return retvalue;
		if (retvalue != FLASH_PAGE_SIZE)
			return ERR_PROG_ERROR;
	}
	else
	{
		page_size = FLASH_PAGE_SIZE - page_offset;
		//retvalue = flash_info[0].pqspidev->write_page(dest_addr,data,page_size);
		retvalue = flash_info[0].pqspidev->write_page(dest_addr,data);

		if (retvalue != FLASH_PAGE_SIZE)
			return ERR_PROG_ERROR;

		for (index = page_size; index < len; index+=page_size)
		{
			page_size = len - index;
			if (page_size > FLASH_PAGE_SIZE)
				page_size = FLASH_PAGE_SIZE;

		//	retvalue = flash_info[0].pqspidev->write_page(dest_addr + index,&data[index],page_size);
				retvalue = flash_info[0].pqspidev->write_page(dest_addr + index,&data[index]);

			if (retvalue != FLASH_PAGE_SIZE)
				return ERR_PROG_ERROR;

		}

	}

	return ERR_OK;

}

#endif


int write_buff (flash_info_t * info, uchar * src, ulong addr, ulong cnt)
{
#if 1
	ulong wp, data, prog_size, sector;
        int iflag,retval;

	if (addr & 1) {
		printf ("unaligned destination not supported\n");
		return ERR_ALIGN;
	};

	if ((int) src & 1) {
		printf ("unaligned source not supported\n");
		return ERR_ALIGN;
	};

	wp = addr;

        /*
         * Disable interrupts which might cause a timeout
         * here. Remember that our exception vectors are
         * at address 0 in the flash, and we don't want a
         * (ticker) exception to happen while the flash
         * chip is in programming mode.
         */
        iflag = disable_interrupts ();
//vm
#ifdef CONFIG_FLASH_IS_QUAD_SPI
 retval = QSpiFlashProgram(addr,src, cnt);
#else
 retval = SpiFlashProgram(addr,src, cnt);
#endif
	if (iflag)
		enable_interrupts ();

	//return ERR_OK;
	return retval;
#endif
}
