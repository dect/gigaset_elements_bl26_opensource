#include "sitel_io.h"
#include "qspic.h"
#include "qspic/qspic_common.h"
#include "qspic/sst.h"

//--------------------------------------------------------------------------------------
//                              Functions related to Status Register
//--------------------------------------------------------------------------------------
// Wait until the end of write/erase operation
void SST_WaitEndOfWrite(void)
{
	// Quad Mode, CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD | QSPIC_EN_CS);	

	// Read Status 
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_RDSR);

	// Wait end of write operation
	while ( GetByte(QSPIC_READDATA_REG) & SST_STATUS_BUSY);

	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Read Status Register
unsigned char SST_ReadStatus (void)
{
	unsigned char r_status;
	
	// Quad Mode, CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD | QSPIC_EN_CS);	

	// Read Status 
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_RDSR);

	r_status = GetByte(QSPIC_READDATA_REG);

	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	return (r_status);
}
//--------------------------------------------------------------------------------------
//                                   Read ID Functions
//-------------------------------------------------------------------------------------- 

// Read Jedec ID (Single Mode)
void SST_ReadSingleJID (unsigned char *JIDBuf)
{
	// Single Mode, Cs Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS );
	
	// Read Single J-ID Instruction
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_JID);
	
	// Manufacture ID 
	JIDBuf[0] = GetByte(QSPIC_READDATA_REG);

	// Device type
	JIDBuf[1] = GetByte(QSPIC_READDATA_REG);
		
	// Device ID
	JIDBuf[2] = GetByte(QSPIC_READDATA_REG);

	// CS high
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Read Jedec ID (Quad Mode)
void SST_ReadQuadJID (unsigned char *JIDBuf)
{	
	// Quad Mode, Cs Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD | QSPIC_EN_CS);
	
	// Read Quad J-ID instruction
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_QJID);
	
	// Manufacture ID 
	JIDBuf[0] = GetByte(QSPIC_READDATA_REG);

	// Device type
	JIDBuf[1] = GetByte(QSPIC_READDATA_REG);
		
	// Device ID
	JIDBuf[2] = GetByte(QSPIC_READDATA_REG);

	// CS high
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

//--------------------------------------------------------------------------------------
//                    Functions to Control the mode of the flash memory
//-------------------------------------------------------------------------------------- 

// Enable Quad Mode
void SST_SetQuadEnable (void)
{
	// Single mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE);
	
	// Send Quad Enable Command
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_EQIO);
	
	// IO3 hi-z, IO2 hi-z
	qspic_enable_quad_pads();
}

// Disable Quad Mode
void SST_ClearQuadEnable (void)
{
	// Quad mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);
	
	// Quad Disable Command
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_RSTQIO);
	
	// IO3 output = 1, IO2 output = 1
	qspic_disable_quad_pads();
}

// Sets the length of a wrapping burst.
void SST_SetBurst (unsigned char BSize)
{
	// BSize = 0 :  8 bytes
	// BSize = 1 : 16 bytes
	// BSize = 2 : 32 bytes
	// BSize = 3 : 64 bytes
	
	// Quad mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);

	// SetBurst Command + burst size
	SetWord(QSPIC_WRITEDATA_REG, SST_INST_SETBRST | (BSize << 8));
}


// Unlocks (Read and Write) operations over all memory space.
void SST_Unlock (void)
{	
	// Set Quad Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);
	
	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_WEn);
	
	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Write Block Protection Register
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_WBPR);
	
	// 6 zeros to unlock read and write operations for sst26vf016 (6*8 =48 bits)
	SetDword(QSPIC_WRITEDATA_REG, 0x0);
	SetWord(QSPIC_WRITEDATA_REG,  0x0);
	
	// more 4 zeros to unlock read and write operations for the sst26vf032 (48 + 4 * 8 = 80 bits)
	if (FlashDes[qspicFDev.desc_num].Capacity > 16)
		SetDword(QSPIC_WRITEDATA_REG, 0x0);
	
	// more 8 zeros to unlock read and write operations for the sst26vf064 (80 + 8 * 8 = 144 bits)
	if (FlashDes[qspicFDev.desc_num].Capacity == 64) {
		SetDword(QSPIC_WRITEDATA_REG, 0x0);
		SetDword(QSPIC_WRITEDATA_REG, 0x0);
	}
			
	// CS High	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

//--------------------------------------------------------------------------------------
//                                   Erase Functions
//--------------------------------------------------------------------------------------

void SST_EraseChip (void)
{
	// Set Quad Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_WEn);

	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Chip Erase
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_CERASE);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	// Wait end of Erase
	SST_WaitEndOfWrite();
}

// Uniform 4KB sector erase
long int SST_EraseSector (unsigned long int Address)
{
	if (Address > FlashDes[qspicFDev.desc_num].MaxAddr)
		return (-1);
		
	// Set Quad Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_WEn);

	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Sector 4KB Erase
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_SERASE);
	
	// Erase Address
	SetWord(QSPIC_WRITEDATA_REG,  ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	// Wait end of Erase
	SST_WaitEndOfWrite();
	
	return (4*1024);
}

// 8 KB , 32 KB, 64 KB Blocks erase (The address defines the size of the block. See to the datasheet)
long int SST_EraseBlock (unsigned long int Address)
{
	long int block_size;
	
	if (FlashDes[qspicFDev.desc_num].Capacity == 16 ){        // SST26VF016
		if ((Address >= 0x000000 && Address <= 0x007fff) |
		    (Address >= 0x1f8000 && Address <= 0x1fffff)  )
			block_size = (long int) 8*1024;
		else if ((Address >= 0x008000 && Address <= 0x00ffff) |
		         (Address >= 0x1f0000 && Address <= 0x1f7fff)  )
		    block_size = (long int) 32*1024;
		else if (Address >= 0x010000 && Address <= 0x1effff)
			block_size = (long int) 64*1024;
		else
			return (-1);
	} else if (FlashDes[qspicFDev.desc_num].Capacity == 32) {  // SST26VF032
		if ((Address >= 0x000000 && Address <= 0x007fff) |
		    (Address >= 0x3f8000 && Address <= 0x3fffff)  )
			block_size = (long int) 8*1024;
		else if ((Address >= 0x008000 && Address <= 0x00ffff) |
		         (Address >= 0x3f0000 && Address <= 0x3f7fff)  )
		    block_size = (long int) 32*1024;
		else if (Address >= 0x010000 && Address <= 0x3effff)
			block_size = (long int) 64*1024;
		else
			return (-1);
	} else if (FlashDes[qspicFDev.desc_num].Capacity == 64)   // SST26VF064
		if ((Address >= 0x000000 && Address <= 0x007fff) |
		    (Address >= 0x7f8000 && Address <= 0x7fffff)  )
			block_size = (long int) 8*1024;
		else if ((Address >= 0x008000 && Address <= 0x00ffff) |
		         (Address >= 0x7f0000 && Address <= 0x7f7fff)  )
		    block_size = (long int) 32*1024;
		else if (Address >= 0x010000 && Address <= 0x7effff)
			block_size = (long int) 64*1024 ;
		else
			return (-1);
	else
		return (-1);	
	
	// Set Quad Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_WEn);

	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Block Erase
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_BERASE);
	
	// Erase Address
	SetWord(QSPIC_WRITEDATA_REG,  ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	// Wait end of Erase
	SST_WaitEndOfWrite();
	
	return (block_size);
}
//--------------------------------------------------------------------------------------
//                                   Write Data Functions
//--------------------------------------------------------------------------------------

// Write a page (256 bytes)
int SST_WritePageQuad (unsigned long int Address, unsigned long int *WPage)
{
	unsigned int i;

	if (Address > FlashDes[qspicFDev.desc_num].MaxAddr)
		return (-1);

	
	// Set Quad Mode
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD);	

	// Write Enable
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_WEn);
	
	// CS Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_EN_CS);
	
	// Program Page 
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_PPROG);
	
	// Write Address
	SetWord(QSPIC_WRITEDATA_REG,  ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Write Data (one page)
	for (i=0; i<64; i++)
		SetDword(QSPIC_WRITEDATA_REG, WPage[i]);
	
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
	
	// Wait end of Write
	SST_WaitEndOfWrite();
	
	return (256);
}

//--------------------------------------------------------------------------------------
//                           Read Data functions during Manual Mode
//--------------------------------------------------------------------------------------

// Fast Byte Read (Single mode)
void SST_ReadFastSingleByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf )
{
	unsigned long int i;

	// Single Mode, Cs Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_FRDATA);
	
	// Read Address
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address  & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG,0x0);

	// Read Data
	for (i=0; i<BSize; i++) 
		BBuf[i] = GetByte(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Fast Byte Read (Quad mode)
void SST_ReadFastQuadByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf)
{
	unsigned long int i;

	// Quad Mode, Cs Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD | QSPIC_EN_CS);	
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_FRDATA);
	
	// Read Address
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address  & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);

	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG, 0x0);

	// Read Data
	for (i=0; i<BSize; i++) 
		BBuf[i] = GetByte(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Fast Half Read (Single mode)
void SST_ReadFastSingleHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf)
{
	unsigned long int i;

	
	// Single Mode, Cs Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);	

	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_FRDATA);
	
	// Read Address
	SetWord(QSPIC_WRITEDATA_REG,((Address >> 16) & 0x00ff) | (Address  & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
	
	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG, 0x0);

	// Read Data
	for (i=0; i<HSize; i++) 
		HBuf[i] = GetWord(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);		
}

// Fast Half Read (Quad mode)
void SST_ReadFastQuadHalf (unsigned long int Address, unsigned long int HSize, unsigned int *HBuf)
{
	unsigned long int i;

	// Quad Mode, Cs Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_FRDATA);
	
	// Read Address
	SetWord(QSPIC_WRITEDATA_REG,((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
	
	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG, 0x0);

	// Read Data
	for (i=0; i<HSize; i++) 
		HBuf[i] = GetWord(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Fast Word Read (Single mode)
void SST_ReadFastSingleWord  (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf)
{
	unsigned long int i;

	// Single Mode, Cs Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_FRDATA);
	
	// Read Address
	SetWord(QSPIC_WRITEDATA_REG, ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
	
	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG, 0x0);

	// Read Data
	for (i=0; i<WSize; i++) 
		WBuf[i] = GetDword(QSPIC_READDATA_REG);
		
	// CS High
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

// Fast Word Read (Quad mode)
void SST_ReadFastQuadWord (unsigned long int Address, unsigned long int WSize, unsigned long int *WBuf)
{
	unsigned long int i;

	// Quad Mode, Cs Low
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_SET_QUAD | QSPIC_EN_CS);
	
	// Read Data Instruction
	SetByte(QSPIC_WRITEDATA_REG, SST_INST_FRDATA);
	
	// Read Address
	SetWord(QSPIC_WRITEDATA_REG,  ((Address >> 16) & 0x00ff) | (Address & 0xff00));
	SetByte(QSPIC_WRITEDATA_REG, Address & 0xff);
	
	// Dummy Byte
	SetByte(QSPIC_DUMMYDATA_REG, 0x0);

	// Read Data
	for (i=0; i<WSize; i++) 
		WBuf[i] = GetDword(QSPIC_READDATA_REG);
	
	// CS High	
	SetDword(QSPIC_CTRLBUS_REG, QSPIC_DIS_CS);
}

//--------------------------------------------------------------------------------------
//                Configurations for the Read sequence during Auto Mode
//--------------------------------------------------------------------------------------

void SST_AutoCfg_4BurstWrap(void)
{
	//-------------------------------------------------------------------
	//                      Fast Read Quad - mem burst 4 words
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : 0b - Fast Read 
	// Command Value (WrapBurst)       [15:8] : 0c - Burst Read
	// Extra byte                     [23:16] : 00 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  2 - Quad
	// Address Trasmit Mode           [27:26] :  2 - Quad
	// Extra Byte Trasmit Mode        [29:28] :  2 - Quad
	// Dummy Bytes Trasmit Mode       [31:30] :  2 - Quad
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  2 - Quad
	// Extra Byte Enable                  [2] :  0 - Extra Byte Disabled
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  1 - 1 Dummy Bytes
	// Command mode                       [6] :  0 - Always Send Command Byte
	// Wrap mode                          [7] :  1 - Enable Wrap mode
	// Wrap Length                      [9:8] :  0 - 4 beat wrapping burst
	// Wrap Size                      [11:10] :  2 - word data size (32 bits)
	// CS High Min Num of CLKs        [14:12] :  3 - 3 CLKs	
	
	
	// Set Burst Size 4 "words" x 4 "bytes/word"  = 16 bytes
	SST_SetBurst(1);

	SetDword(QSPIC_BURSTCMDA_REG,  0xaa000c0b);
	SetDword(QSPIC_BURSTCMDB_REG,  0x00003892);
}

void SST_AutoCfg_8BurstWrap(void)
{
	//-------------------------------------------------------------------
	//                      Fast Read Quad - mem burst 8 words
	//-------------------------------------------------------------------
	// BustCmdA
	//-------------------------------------------------------------------
	// Command Value (IncBurst, Single) [7:0] : 0b - Fast Read 
	// Command Value (WrapBurst)       [15:8] : 0c - Burst Read
	// Extra byte                     [23:16] : 00 - Extra Byte After Address
	// Command Trasmit Mode           [25:24] :  2 - Quad
	// Address Trasmit Mode           [27:26] :  2 - Quad
	// Extra Byte Trasmit Mode        [29:28] :  2 - Quad
	// Dummy Bytes Trasmit Mode       [31:30] :  2 - Quad
	//-------------------------------------------------------------------
	// BustCmdB
	//-------------------------------------------------------------------
	// Read Data Receive Mode           [1:0] :  2 - Quad
	// Extra Byte Enable                  [2] :  0 - Extra Byte Disabled
	// Extra Half Disable Out             [3] :  0 - Send the complete Extra Byte (if [2] ==1)
	// Num Of Dummy Bytes               [5:4] :  1 - 1 Dummy Bytes
	// Command mode                       [6] :  0 - Always Send Command Byte
	// Wrap mode                          [7] :  1 - Enable Wrap mode
	// Wrap Length                      [9:8] :  1 - 8 beat wrapping burst
	// Wrap Size                      [11:10] :  2 - word data size (32 bits)
	// CS High Min Num of CLKs        [14:12] :  3 - 3 CLKs	
	

	// Set Burst Size 8 "words" x 4 "bytes/word"  = 32 bytes
	SST_SetBurst(2);
	
	SetDword(QSPIC_BURSTCMDA_REG,  0xaa000c0b);
	SetDword(QSPIC_BURSTCMDB_REG,  0x00003992);
}

//--------------------------------------------------------------------------------------
//                           
//--------------------------------------------------------------------------------------

int SST_InitFlashDevice(void) {
	unsigned char SSTDescNum;
	unsigned char SSTJedecID[3];

	// IO3 output = 1, IO2 output = 1
	qspic_disable_quad_pads();
	
	// Enable Quad Mode
	SST_SetQuadEnable();
	
	// Read Jedec ID
	SST_ReadQuadJID (SSTJedecID);
	
	// Check if the flash memory is the expected
	
	if (qspic_find_flash_desc (SSTJedecID, &SSTDescNum) == -1)
		return(-1);
	
	if (FlashDes[SSTDescNum].ManID != SST_ID)
		return (-1);
	
	// Save the Flash Memory Desrciptor number
	qspicFDev.desc_num = SSTDescNum;
	
	// Unlocks (Read and Write) operations over all memory space.
	SST_Unlock();
	
	// The selected read instruction during the Auto mode
	SST_AutoCfg_4BurstWrap();
	
	// Install handlers
	qspicFDev.start_auto   = SST_StartAutoMode;
	qspicFDev.stop_auto    = SST_StopAutoMode;
	qspicFDev.read_byte    = SST_ReadFastQuadByte;
	qspicFDev.read_half    = SST_ReadFastQuadHalf;
	qspicFDev.read_word    = SST_ReadFastQuadWord;
	qspicFDev.erase_chip   = SST_EraseChip;
	qspicFDev.erase_sector = SST_EraseSector;	// 4KB Sector Erase
	qspicFDev.erase_block  = SST_EraseBlock;	// 8 KB , 32 KB, 64 KB Blocks Erase (address depended).
	qspicFDev.erase_parms  = SST_EraseSector;	// 4KB Sector Erase
	qspicFDev.write_page   = SST_WritePageQuad;
	
	qspicFDev.auto_max_clk   = FlashDes[qspicFDev.desc_num].Freqs[QUAD_READ_FREQ];
	qspicFDev.manual_max_clk = FlashDes[qspicFDev.desc_num].Freqs[QUAD_READ_FREQ];
	
	// Returns the size of the memory in Mbits
	return (FlashDes[qspicFDev.desc_num].Capacity);
}

void SST_StartAutoMode(void) {
	qspic_auto_mode();
	qspic_set_closer_freq(qspicFDev.auto_max_clk);
}

void SST_StopAutoMode(void) {
	qspic_manual_mode();
	qspic_set_closer_freq(qspicFDev.manual_max_clk);
}


