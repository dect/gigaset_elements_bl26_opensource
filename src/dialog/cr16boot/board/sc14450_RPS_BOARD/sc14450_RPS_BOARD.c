/*
 * (C) Copyright 2002
 * Kyle Harris, Nexus Technologies, Inc. kharris@nexus-tech.net
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */


#include "armboot.h"
#include "sitel_io.h"
#include "spi_polled.h"
/* ------------------------------------------------------------------------- */
//void cvm_access(void);


void init_spi1(void)
{

	unsigned i;

	SetPort(P0_06_MODE_REG, PORT_OUTPUT,  PID_port);					/* P0_06 is SPI_CLK  */

	for (i=0;i<7;i++)
	{
		SetWord(P0_SET_DATA_REG,0x40);
		SetWord(P0_RESET_DATA_REG,0x40);
	}

	/* Set PPA MATRIX FOR SPI1 */
	SetPort(P0_06_MODE_REG, PORT_OUTPUT,  PID_SPI1_CLK);					/* P0_06 is SPI_CLK  */
	SetPort(P2_05_MODE_REG, PORT_INPUT /*PORT_PULL_UP*/,   PID_SPI1_DIN);    /* P2_05 is SPI_DI  */
	SetPort(P2_08_MODE_REG, PORT_OUTPUT,  PID_SPI1_DOUT);					/* P2_08 is SPI_DO */
	SetPort(P0_07_MODE_REG, PORT_OUTPUT,  PID_port);						/* P0_07 as SPI_EN  */

	/* Set SPI SETTINGS */

	SetBits(CLK_PER10_DIV_REG,PER20_DIV,0);

	SetWord(SPI1_CTRL_REG , 0);				/* clear all fields*/

	SetBits(SPI1_CTRL_REG, SPI_POL,  1);  
	SetBits(SPI1_CTRL_REG, SPI_PHA,  1);  
	
	SetBits(SPI1_CTRL_REG, SPI_DO, 1);                 
	
	SetBits(SPI1_CTRL_REG, SPI_ON, 1);                      // Enable SPI2 


	SetWord(P0_SET_DATA_REG,0x80);
	
	SetPort(P1_13_MODE_REG, PORT_INPUT,  PID_port);


}


/*
 * Miscelaneous platform dependent initialisations
 */

void led_check(void)
{
	unsigned i;

	*(volatile unsigned short*)CONFIG_KEYB_LATCH_BASE = 0;
	udelay(50000);
	*(volatile unsigned short*)CONFIG_KEYB_LATCH_BASE = 0xffff;
	udelay(50000);
	*(volatile unsigned short*)CONFIG_KEYB_LATCH_BASE = 0;
	udelay(50000);
	for (i=0;i<14;i++){
		*(volatile unsigned short*)CONFIG_KEYB_LATCH_BASE = 1<<i;
		udelay(50000);
	}
	*(volatile unsigned short*)CONFIG_KEYB_LATCH_BASE = 0;
	udelay(50000);
	*(volatile unsigned short*)CONFIG_KEYB_LATCH_BASE = 0xffff;
	udelay(50000);
	*(volatile unsigned short*)CONFIG_KEYB_LATCH_BASE = 0;
}

int 
/**********************************************************/
board_post_init(bd_t *bd)
/**********************************************************/
{

	init_spi1();
//	led_check();

	return 0;
}

int 
/**********************************************************/
board_init(bd_t *bd)
/**********************************************************/
{
	
	/* arch number of Sitel SC14450 DK */
	bd->bi_arch_number = 450;

	/* vm must change */ 
	/* adress of boot parameters */
	bd->bi_boot_params = CFG_CONFIG_PARAMETERS_ADDR;//0xa0000100;

		/*   Config PPA for UART. */

		/*   Config PPA for UART. */
	SetPort(P0_00_MODE_REG, PORT_OUTPUT,    PID_port);
	SetPort(P0_01_MODE_REG, PORT_OUTPUT,   PID_port);


	SetPort(P2_10_MODE_REG, PORT_OUTPUT,    PID_UTX);
	SetPort(P2_07_MODE_REG, PORT_PULL_UP,   PID_URX);


	init_SPI2(CLK_5184, MODE_3, MODE_8BITS);

	SetWord(EBI_SMTMGR_SET1_REG,0x0d44);
	SetWord(EBI_SMTMGR_SET2_REG,0x0568);	


//vm setup ethernet interface 
	SetPort(P2_04_MODE_REG, PORT_OUTPUT,  PID_ACS1);        /* P2_04 is ACS1 */
	SetDword(EBI_ACS1_CTRL_REG,0x121);						/* use time setting 1, memory is sram, size is 64K */
	SetDword(EBI_ACS1_LOW_REG,CONFIG_ETHERNET_BASE);		/* ACS1 base address is 0x1200000 */  

	SetPort(P1_15_MODE_REG, PORT_PULL_DOWN,  PID_INT7n);		/* P1_15 is int7 for ethernet interrupt */
	SetWord(KEY_GP_INT_REG , 0x20);							/* Enable positive level int7 */


//setup latch interface
	SetPort(P1_14_MODE_REG, PORT_OUTPUT,  PID_ACS2);		/* P1_14 is ACS2 */
	SetDword(EBI_ACS2_CTRL_REG,0x121);						/* use time setting 2, memory is sram, size is 64K */
	SetDword(EBI_ACS2_LOW_REG,CONFIG_KEYB_LATCH_BASE);		/* ACS2 base address is 0x1400000 */  

//vm setup lcd interface 
	SetPort(P2_06_MODE_REG, PORT_OUTPUT, PID_ACS3);        /* P2_06 is ACS3 */
	SetDword(EBI_ACS3_CTRL_REG,0x221);						/* use time setting 1, memory is sram, size is 64K */
	SetDword(EBI_ACS3_LOW_REG,CONFIG_LCD_BASE);				/* ACS3 base address is 0x1300000 */  

	
	return 1;

}

int 
/**********************************************************/
dram_init(bd_t *bd)
/**********************************************************/
{
  	bd->bi_dram[0].start = PHYS_SDRAM_1;
	bd->bi_dram[0].size  = PHYS_SDRAM_1_SIZE;
	
	
	bd->bi_dram[1].start = PHYS_SDRAM_2;
	bd->bi_dram[1].size = PHYS_SDRAM_2_SIZE;
	
	return PHYS_SDRAM_1_SIZE+PHYS_SDRAM_2_SIZE;
 
}

