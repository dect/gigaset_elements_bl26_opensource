#include "sitel_io.h"
#include "dma.h"

#define DMA_CHANNEL_SPACE (DMA1_A_STARTL_REG - DMA0_A_STARTL_REG)
/*
// Enable dma interrupt
void dma_irq_enable (unsigned char channel) {
	if (channel == 0) {
		SetBits (INT2_PRIORITY_REG, SPI1_AD_INT_PRIO, 1);
	} else if (channel == 1) {
		SetBits (INT3_PRIORITY_REG, KEYB_INT_PRIO, 1);
	} else if (channel == 2) {
		SetBits (INT2_PRIORITY_REG, UART_RI_INT_PRIO ,1);
	} else if (channel == 3) {
		SetBits (INT2_PRIORITY_REG, UART_TI_INT_PRIO ,1);
	}
}

// Disable dma interrupt
void dma_irq_disable (unsigned char channel) {
	if (channel == 0) {
		SetBits (INT2_PRIORITY_REG, SPI1_AD_INT_PRIO, 0);
	} else if (channel == 1) {
		SetBits (INT3_PRIORITY_REG, KEYB_INT_PRIO, 0);
	} else if (channel == 2) {
		SetBits (INT2_PRIORITY_REG, UART_RI_INT_PRIO ,0);
	} else if (channel == 3) {
		SetBits (INT2_PRIORITY_REG, UART_TI_INT_PRIO ,0);
	}
}

// Install interrupt handler for a dma channel
void dma_install_handler (void (*handler) (void), unsigned char channel) {
	if (channel == 0) {
		irq_install_handler (handler,  SPI1_AD_INT_IRQ_VECTOR);
	} else if (channel == 1) {
		irq_install_handler (handler,  KEYB_INT_IRQ_VECTOR);
	} else if (channel == 2) {
		irq_install_handler (handler,  UART_RI_INT_IRQ_VECTOR);
	} else if (channel == 3) {
		irq_install_handler (handler,  UART_TI_INT_IRQ_VECTOR);
	}

}
*/
void dma_wait (unsigned char channel) {
	while (GetWord(DMA0_CTRL_REG + channel* DMA_CHANNEL_SPACE) & DMA_ON);       // wait until finished
}


void dma_memcopy(unsigned long int src_addr, unsigned long int dest_src, 
                 unsigned int len,           unsigned char data_width, 
                 unsigned char max_burst,    unsigned char channel) {
	
	
	unsigned long int offset = channel * DMA_CHANNEL_SPACE;
	
	SetWord(DMA0_A_STARTL_REG + offset, (src_addr) & 0xFFFF );
	SetWord(DMA0_A_STARTH_REG + offset, ((unsigned long int)(src_addr) >> 16) & 0xFFFF);
	SetWord(DMA0_B_STARTL_REG + offset, (dest_src) & 0xFFFF );
	SetWord(DMA0_B_STARTH_REG + offset, ((unsigned long int)(dest_src) >> 16) & 0xFFFF);

	SetWord(DMA0_LEN_REG + offset, len);
	SetWord(DMA0_INT_REG + offset, 0);
	
	SetWord(DMA0_CTRL_REG + offset, 0x0000);
	SetBits(DMA0_CTRL_REG + offset, MAX_BURST, max_burst);  // 8-beat burst
        SetBits(DMA0_CTRL_REG + offset, AINC,   1);             // increment the source
        SetBits(DMA0_CTRL_REG + offset, BINC,   1);             // increment the destination
        SetBits(DMA0_CTRL_REG + offset, BW,     data_width);    // set bus width to 32 bits
        SetBits(DMA0_CTRL_REG + offset, DMA_ON, 1);             // enable the DMA
}

void dma_meminit (unsigned long int dest_addr, unsigned int len,
                  unsigned char data_width,    unsigned char max_burst,
		  unsigned char init_value,    unsigned char channel) {

	unsigned long int offset = channel * DMA_CHANNEL_SPACE;
	
	SetWord(DMA0_B_STARTH_REG + offset, ((unsigned long int)(dest_addr) >> 16) & 0xFFFF);
        SetWord(DMA0_B_STARTL_REG + offset, (dest_addr) & 0xFFFF );
	
        SetWord(DMA0_LEN_REG + offset,    len);
	
	SetWord(DMA0_CTRL_REG + offset, 0x0000);
	SetBits(DMA0_CTRL_REG + offset, MEM_INIT, 1);           // initialize a memory block (bypass the read access.)
	SetBits(DMA0_CTRL_REG + offset, INIT_VAL, init_value);  // Set the init value for the memory block.
	SetBits(DMA0_CTRL_REG + offset, MAX_BURST, max_burst);  // set access mode
        SetBits(DMA0_CTRL_REG + offset, BINC,   1);             // increment the destination
        SetBits(DMA0_CTRL_REG + offset, BW,     data_width);    // set bus width 
        SetBits(DMA0_CTRL_REG + offset, DMA_ON, 1);             // enable the DMA
}

void dma_port_write(unsigned long int src_addr, unsigned long int dest_src, 
                    unsigned int len,           unsigned char data_width, 
                    unsigned char max_burst,    unsigned char channel) {
	
	
	unsigned long int offset = channel * DMA_CHANNEL_SPACE;
	
	SetWord(DMA0_A_STARTL_REG + offset, (src_addr) & 0xFFFF );
	SetWord(DMA0_A_STARTH_REG + offset, ((unsigned long int)(src_addr) >> 16) & 0xFFFF);
	SetWord(DMA0_B_STARTL_REG + offset, (dest_src) & 0xFFFF );
	SetWord(DMA0_B_STARTH_REG + offset, ((unsigned long int)(dest_src) >> 16) & 0xFFFF);

	SetWord(DMA0_LEN_REG + offset, len);
	SetWord(DMA0_INT_REG + offset, 0);
	
	SetWord(DMA0_CTRL_REG + offset, 0x0000);
	SetBits(DMA0_CTRL_REG + offset, MAX_BURST, max_burst);  // 8-beat burst
        SetBits(DMA0_CTRL_REG + offset, AINC,   1);             // increment the source
        SetBits(DMA0_CTRL_REG + offset, BW,     data_width);    // set bus width to 32 bits
        SetBits(DMA0_CTRL_REG + offset, DMA_ON, 1);             // enable the DMA
}

void dma_port_read(unsigned long int src_addr, unsigned long int dest_src, 
                 unsigned int len,           unsigned char data_width, 
                 unsigned char max_burst,    unsigned char channel) {
	
	
	unsigned long int offset = channel * DMA_CHANNEL_SPACE;
	
	SetWord(DMA0_A_STARTL_REG + offset, (src_addr) & 0xFFFF );
	SetWord(DMA0_A_STARTH_REG + offset, ((unsigned long int)(src_addr) >> 16) & 0xFFFF);
	SetWord(DMA0_B_STARTL_REG + offset, (dest_src) & 0xFFFF );
	SetWord(DMA0_B_STARTH_REG + offset, ((unsigned long int)(dest_src) >> 16) & 0xFFFF);

	SetWord(DMA0_LEN_REG + offset, len);
	SetWord(DMA0_INT_REG + offset, 0);
	
	SetWord(DMA0_CTRL_REG + offset, 0x0000);
	SetBits(DMA0_CTRL_REG + offset, MAX_BURST, max_burst);  // 8-beat burst
        SetBits(DMA0_CTRL_REG + offset, BINC,   1);             // increment the destination
        SetBits(DMA0_CTRL_REG + offset, BW,     data_width);    // set bus width to 32 bits
        SetBits(DMA0_CTRL_REG + offset, DMA_ON, 1);             // enable the DMA
	
}
