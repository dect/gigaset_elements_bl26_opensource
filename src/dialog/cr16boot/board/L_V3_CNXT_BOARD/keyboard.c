#include "armboot.h"


#define __REG8(x)	(*((volatile unsigned char*)x))
#define __REG(x)	(*((volatile unsigned short*)x))
#define __REG32(x)	(*((volatile unsigned int*)x))

#define P0_DATA_REG_PTR 		__REG(P0_DATA_REG)
#define P1_DATA_REG_PTR 		__REG(P1_DATA_REG)
#define P2_DATA_REG_PTR 		__REG(P2_DATA_REG)
#define P0_SET_DATA_REG_PTR 	__REG(P0_SET_DATA_REG)
#define P1_SET_DATA_REG_PTR 	__REG(P1_SET_DATA_REG)
#define P2_SET_DATA_REG_PTR 	__REG(P2_SET_DATA_REG)
#define P0_RESET_DATA_REG_PTR 	__REG(P0_RESET_DATA_REG)
#define P1_RESET_DATA_REG_PTR 	__REG(P1_RESET_DATA_REG)
#define P2_RESET_DATA_REG_PTR 	__REG(P2_RESET_DATA_REG)
#define P0_00_MODE_REG_PTR 		__REG(P0_00_MODE_REG)
#define P1_00_MODE_REG_PTR 		__REG(P1_00_MODE_REG)
#define P2_00_MODE_REG_PTR 		__REG(P2_00_MODE_REG)

volatile unsigned short *gpio_data_reg[3]={&P0_DATA_REG_PTR, &P1_DATA_REG_PTR, &P2_DATA_REG_PTR};
volatile unsigned short *gpio_set_reg[3]={&P0_SET_DATA_REG_PTR, &P1_SET_DATA_REG_PTR, &P2_SET_DATA_REG_PTR};
volatile unsigned short *gpio_reset_reg[3]={&P0_RESET_DATA_REG_PTR, &P1_RESET_DATA_REG_PTR, &P2_RESET_DATA_REG_PTR};
volatile unsigned short *gpio_mode_reg[3]={&P0_00_MODE_REG_PTR, &P1_00_MODE_REG_PTR, &P2_00_MODE_REG_PTR};

#define GPIO_PUPD_IN_NONE	       (0x0 << 8)	/* Input, no resistors selected */
#define GPIO_PUPD_IN_PULLUP	       (0x1 << 8)	/* Input, pull-up selected */
#define GPIO_PUPD_IN_PULLDOWN          (0x2 << 8)	/* Input, Pull-down selected */
#define GPIO_PUPD_OUT_NONE	       (0x3 << 8)	/* Output, no resistors selected*/


//LED Definitions
#define TOTAL_LEDS			1//the total number of LEDS
#define LED_D(X)  			(1<<(X-1))
#define LED_MSG				LED_D(1)

//Keypad Layout
#define KBD_COLUMNS 8
#define KBD_ROWS 5

//Extra keys
#define EXTRA_KEYS			1 //the hook switch

/***      Flags definitions     ***/
//key_scan FSM state
#define KBD_IDLE_STATE				(0x01)
#define KBD_PRESS_DEBOUNCE_STATE	(0x02)
#define KBD_PRESS_STATE				(0x03)
#define KBD_REPEAT_SHORT_STATE		(0x04)
#define KBD_WAIT_RELEASE_STATE		(0x05)

//key_scan flags
#define SAME_KEY	(0x01)
//extra_key_scan flags
#define EXTRA_KEY_PRESSED	(0x01)
//the total amount of keys that sohuld be concurrently scanned .
#define MAX_MULT_KEYS 2


/***      Data types definitions       ***/
typedef struct {
	/* Common in INT and POLLING modes*/
	unsigned char flags; //POLLING , INT, etc
	unsigned char rows;//the number of keypad rows
	unsigned char cols;//the number of keypad columns
	unsigned char repeat_tmouts_long; //tmouts before generating the first KBD_SC1445x_REPEAT_MSG (in polling timer units)
	unsigned char repeat_tmouts_short;//tmouts before generating the subsequent KBD_SC1445x_REPEAT_MSG (in polling timer units)
	unsigned char mult_keys; //the number of simultaneous keypad  events that should be scanned
	unsigned char extra_key_debounce_tmouts; //debounce tmouts for extra_key switch (in polling timer units)
	unsigned char led_toggle_tmouts; //led toggle tmouts (in polling timer units)
	/* Only for polling mode */
	unsigned char debounce_tmouts; //debounce tmouts for keypad buttons(in polling timer units)
	unsigned char polling_timer; // the multiples of the linux system timer (e.g 10ms) that will be used for polling
	/* Only for INT mode*/
	unsigned char int_debounce_timer; //debounce timer in ms, range: 0-63. This is the generation period of debounce interrupts
	unsigned char int_repeat_timer; //repeat timer in ms, range: 0-63. This is the generation period of repeat.release interrupts
	unsigned char irq; //the irq used for keyboard int
	unsigned short buffersize;	//the size of the circular buffer used for storing msgs
} keypad_sc1445x_conf_struct;

typedef struct {
	unsigned short state; //PRESS, RELEASE, REPEAT etc
	unsigned short flags;
	unsigned char pre_col;
	unsigned char pre_row;
	unsigned char tmout; //the time key is pressed
}key_scan_struct;

typedef struct {
	unsigned short state; //PRESS, RELEASE, DEBOUNCE etc
	unsigned short flags;
	unsigned char tmout; //the time key is pressed
}extra_key_scan_struct;

typedef struct {
	unsigned short masks[2];//the led toggle masks are stored here
	unsigned char index;
	unsigned char tmout; //the time remaing for next toggle
}led_toggle_struct;

typedef struct {
	/* Registers */
	volatile unsigned short *kbd_pio_stat_reg;
	unsigned short kbd_pio_id;
}keypad_rw_struct;


typedef struct  {
	unsigned char msg_code;
	unsigned short key_id;
}kbd_msg;
typedef struct  {
	char buf_len;
	char *buf;
}kbd_msg_buf;
//Used for debugging
typedef struct kbd_debug_drv {
	kbd_msg *buffer;       /* begin of buf, end of buf */
	unsigned char free_space;
	unsigned short buffersize;
    char rp, wp;
	unsigned char ver_major;
	unsigned char ver_minor;
}kbd_debug_drv_struct;
#define KBD_SC1445x_INT     	(0x02)  //check keyboard using int

#define KBD_SC1445x_FLAGS_DEFAULT 0//KBD_SC1445x_LEVEL_HIGH

/***      Msgs definitions       ***/
//key_scan msgs
#define KBD_SC1445x_RELEASE_MSG  (0x01)
#define KBD_SC1445x_PRESS_MSG    (0x02)
#define KBD_SC1445x_REPEAT_MSG	 (0x03)

//extra_key msg definitions
#define KBD_SC1445x_HOOK_ON_MSG				(0x04)
#define KBD_SC1445x_HOOK_OFF_MSG			(0x05)
//#ifdef CONFIG_RPS_BOARD
#define KBD_SC1445x_CALL_END_PRESSED_MSG	(0x06)
#define KBD_SC1445x_CALL_END_RELEASED_MSG	(0x07)
//#elif defined CONFIG_F_G2_P2_2PORT_BOARD
#define KBD_SC1445x_HS_MIC_PRESSED_MSG		(0x08)
#define KBD_SC1445x_HS_MIC_RELEASED_MSG		(0x09)
//#endif

#define HOOK_DUMMY_KEY_ID					(0xff)
#define CALL_END_DUMMY_KEY_ID				(0xff)

	//extra_key msgs
	#define KBD_SC1445x_EXTRA_KEY_RELEASE_MSG(X)	KBD_SC1445x_HOOK_ON_MSG
	#define KBD_SC1445x_EXTRA_KEY_PRESS_MSG(X)		KBD_SC1445x_HOOK_OFF_MSG
	//extra_key dummy key_id
	#define EXTRA_KEY_DUMMY_KEY_ID(X)				HOOK_DUMMY_KEY_ID

/***      Buffers definitions       ***/
#define KBD_SC1445x_BUFFER_SIZE 64
/***      Ioctl cmds      ***/



#define SCAN_ROUTINE_COLUMN_PORT_TRISTATE //SCAN_ROUTINE_NORMAL
//#define SET_LED_GPIO(X) {set_led_gpio(X);};
#define SET_COL_GPIO(X) {set_col_gpio(X);};
#define SET_COL_LATCH(X) {set_col_latch(X);};



unsigned int gpio_leds_mask=0;
unsigned int lcd_backlight=1;


#define COL_WR_BUF   (*(volatile unsigned short *)(CONFIG_KEYB_LATCH_BASE))
#define ETH_RST_BLOFF_COL (0x4000)


#define SET_LEDS(X) //SET_LED_GPIO(X)
//#define SET_COLUMNS(X) SET_COL_GPIO(X)
// SET_COL_LATCH(X)
#define SET_COLUMNS(X) {PROTECT_OVER_16M_SDRAM_ACEESS;COL_WR_BUF=X|ETH_RST_BLOFF_COL|((gpio_leds_mask)<<8)|((lcd_backlight<<13));RESTORE_OVER_16M_SDRAM_ACEESS;volatile char c; for (c=0;c<10;c++);};
//#define SET_COLUMNS(X) {COL_WR_BUF=X|ETH_RST_BLOFF_COL|((gpio_leds_mask)<<8)|((lcd_backlight<<13));}
#define EXTRA_KEY_CHECK(X) ((P0_DATA_REG & ((0x0001)<<2)))	//P002

#define LCDRESET {PROTECT_OVER_16M_SDRAM_ACEESS;COL_WR_BUF=0xFF|((gpio_leds_mask)<<8)|((lcd_backlight<<13));RESTORE_OVER_16M_SDRAM_ACEESS;}
#define LCDSET {PROTECT_OVER_16M_SDRAM_ACEESS;COL_WR_BUF=0xFF|ETH_RST_BLOFF_COL|((gpio_leds_mask)<<8)|((lcd_backlight<<13));RESTORE_OVER_16M_SDRAM_ACEESS;}

#define KEY_ID(key_scan_index) ((1 << key_scan[key_scan_index].pre_col) << 5 | (key_scan[key_scan_index].pre_row))

/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
	unsigned char row_config[KBD_ROWS][3] = {
		{1, 13, 4},//P113, INT4
		{2, 3,  5},//P203, INT5
		{2, 4,  0},//P204, INT0
		{2, 5,  2},//P205, INT2
		{2, 8,  3},//P205, INT2
		};

/*For GPIO_X_YY com_config[i]={X, YY}*/
unsigned char col_config[KBD_COLUMNS][2] = {
				{2, 2},//P202
				{2, 3},//P203
				{2, 4},//P204
				{2, 11},//P211
				{2, 12},//P212
				{0, 7},//P007
		};

		//Leds configuration
unsigned char led_config[TOTAL_LEDS][2] = {
			{0, 5},//P002
		};

//keypad_sc1445x_conf_struct kbd_conf;

keypad_sc1445x_conf_struct kbd_conf = {
	/* Set keypad default configuration */
	.flags= KBD_SC1445x_FLAGS_DEFAULT,//IF NOT INT then POLLING//KBD_SC1445x_LEVEL_HIGH
	.cols=KBD_COLUMNS,
	.rows=KBD_ROWS,//1-5
	.mult_keys=2,
	.repeat_tmouts_long=1,//100,
	.repeat_tmouts_short=1,//50,
	.polling_timer=1,
	.extra_key_debounce_tmouts=10,
	.led_toggle_tmouts=200,
	//.int_debounce_timer=60;
	//.int_repeat_timer=60;
	/*Driver internals*/
//	.buffer=0;
	.buffersize=KBD_SC1445x_BUFFER_SIZE,
	.irq=2, //CHANGE THIS TO KBD_INT defined in irq.h
	.debounce_tmouts=1,
};


volatile unsigned char  dummy_read;
unsigned char total_active_keys=0;
unsigned char scan_col[KBD_COLUMNS];
static keypad_rw_struct keypad_row[KBD_ROWS];//stores info about the pios that should be read for getting kbd press events
unsigned short col_mask=0;
//unsigned short led_mask=LED_LINE1|LED_LINE2|LED_GP1|LED_GP2|LED_AUDIO_FN1|LED_AUDIO_FN2;	//this var is never initialized again in the driver when operating in POLLING mode. In this mode it is used for controlling leds.
unsigned short led_mask=0;	//this var is never initialized again in the driver when operating in POLLING mode. In this mode it is used for controlling leds.
//unsigned short led_mask=LED_LINE3|LED_LINE2|LED_LINE1|LED_HOOK|LED_GP|LED_TRNSFR|LED_CONFR|LED_HOLD|LED_DND|LED_REDIAL|LED_MWI|LED_HNDSET|LED_HDSET|LED_SPKR|;
static key_scan_struct key_scan[MAX_MULT_KEYS];
static extra_key_scan_struct extra_key_scan[EXTRA_KEYS];//used for storing fsm state
static led_toggle_struct led_toggle;

kbd_msg pressed_keys[MAX_MULT_KEYS];

void lcd_reset()
{
	LCDRESET
}
void lcd_set()
{
	LCDSET
}

void set_col_gpio(unsigned short mask){
	unsigned char i;
	for (i=0; i< KBD_COLUMNS; i++){
		if (mask & (1<<i)){//set the pio high
			__REG(gpio_set_reg[col_config[i][0]])  = 1 << col_config[i][1];
		}
		else{//set the pio low
			__REG(gpio_reset_reg[col_config[i][0]])= 1 << col_config[i][1];
		}
	}
}


void set_col_latch(unsigned short mask){
	PROTECT_OVER_16M_SDRAM_ACEESS;
	COL_WR_BUF=mask|ETH_RST_BLOFF_COL|((gpio_leds_mask)<<8)|((lcd_backlight<<13));
	RESTORE_OVER_16M_SDRAM_ACEESS;
}

/*******************************************************
	Description: 	Configures hw for controlling the attached keypad
	Input:		The hw configuration stored in a kbd_conf struct
	Return:		In interrupt mod:, the value that should be written to KEY_BOARD_INT_REG, to enable the intrs.
				0: otherwise
	Notes:		The cols, rows are configured based on the ifdefs KBD_ROWS, KBD_COLUMNS and not the rows, cols fields of the kbd_conf
 ********************************************************/
unsigned short hw_init(){
	unsigned char i;
	unsigned short key_board_int_reg=0;
	unsigned short gpio_pupd;//stores gpio pull up/down configuration


//	PROTECT_OVER_16M_SDRAM_ACEESS;

	/************** COLUMNS CONFIGURATION ***************/
	/* Columns configuration remains the same. Since it is memory mapped, it should be configured on chip initialization */
	i=0;
	/* Disable kbd intr*/
	/* Init vars used to store register configurations  */
	key_board_int_reg=gpio_pupd=0;

	/* Set key press sense logic */
	gpio_pupd = GPIO_PUPD_IN_PULLUP;
	//if(kbd_conf->flags & KBD_SC1445x_LEVEL_HIGH){
		key_board_int_reg |= (0x0400);
	//	gpio_pupd = GPIO_PUPD_IN_PULLDOWN;
		SET_COLUMNS(col_mask);
	//}

	/*************** EXTRA KEY CONFIGURATION ************/
	/* Set the PID and the pull-up or pull-down registores if needed */
	__REG(P2_11_MODE_REG)=gpio_pupd; //HOOK Input, registors according to LEVEL logic
//	gpio_pupd = GPIO_PUPD_IN_PULLUP;

	/************** LEDS CONFIGURATION ***************/
	/* Columns configuration remains the same. Since it is memory mapped, it should be configured on chip initialization */

	/*************** ROWS CONFIGURATION **************/
	for(i=0; i<KBD_ROWS; i++){
		/* Define the register and the bit from where the status of the button should be read */
		keypad_row[i].kbd_pio_stat_reg=gpio_data_reg[row_config[i][0]];
		keypad_row[i].kbd_pio_id=(0x0001)<<row_config[i][1];
		//printk(KERN_ERR "P%d_00_MODE_REG = 0x%x \n", row_config[i][0], gpio_mode_reg[row_config[i][0]]);
		/* Set the PID and the pull-up or pull-down registores if needed */
		/*
		printk(KERN_ERR "Row= %d, &P%d_0%d_MODE_REG = 0x%x, val=0x%x \n",
				i, row_config[i][0], row_config[i][1], gpio_mode_reg[row_config[i][0]]+row_config[i][1], gpio_pupd);
		printk(KERN_ERR "gpio_mode_reg[row_config[i][0]]= 0x%x, 2*row_config[i][1]=0x%x \n",
				gpio_mode_reg[row_config[i][0]], 2*row_config[i][1]);
		printk(KERN_ERR "keypad_row[i].kbd_pio_stat_reg= 0x%x, keypad_row[i].kbd_pio_id= 0x%x \n",
				keypad_row[i].kbd_pio_stat_reg, keypad_row[i].kbd_pio_id);
		*/
		__REG(gpio_mode_reg[row_config[i][0]]+row_config[i][1])=gpio_pupd;//Input, registors according to LEVEL logic

	}
}


/*******************************************************
	Description: 	Configures driver based on configuration stored in kbd_init struct.
	Notes:		Should be called atomic.
				Kbd int should be disabled before calling this function, and enabled afterwards if needed (by checking kbd_conf->flags)
 ********************************************************/
int kbd_configure(void){
	int result=0;
	unsigned char i;

	//reset key_scan struct (kbd fsm)
	for(i=0;i<MAX_MULT_KEYS;i++){
		key_scan[i].state=KBD_IDLE_STATE;
		key_scan[i].pre_row=key_scan[i].pre_col=0x00;
		key_scan[i].tmout=0x00;
	}
	//reset extra_key_scan struct
	for(i=0; i<EXTRA_KEYS; i++){
		extra_key_scan[i].state=KBD_IDLE_STATE;
		extra_key_scan[i].flags=0;//extra_key is released
		extra_key_scan[i].tmout=0x00;
	}

	//init other vars
	col_mask=0;
	for(i=0;i<kbd_conf.cols;i++){
		col_mask |= 1<<i;//create a mask for all columns
	}

	//reset led toggle struct
	led_toggle.masks[0]=0;
	led_toggle.masks[1]=0;
	led_toggle.index=0;
	led_toggle.tmout=0;
	for(i=0;i<TOTAL_LEDS;i++){
		led_toggle.masks[0]|=1<<i;
		led_toggle.masks[1]|=1<<i;
	}


	for (i=0;i<kbd_conf.mult_keys;i++)
	{
		pressed_keys[i].key_id=0;
		pressed_keys[i].msg_code=0;
	}


	/*Initialize hw */
	hw_init();


}

#if 0
unsigned char scan_keypad(char first_active_row){
	unsigned char col, row,i;
	row=col=0;

	col=0;
	while( col<kbd_conf.cols ){
		for(i=0; i<kbd_conf.cols; i++){
			__REG(gpio_mode_reg[col_config[i][0]]+col_config[i][1])= GPIO_PUPD_IN_PULLUP; //All Columns Input pull up
		}
		__REG(gpio_mode_reg[col_config[col][0]]+col_config[col][1])= 0x300; //Column Output 0
		__REG(gpio_reset_reg[col_config[col][0]])= 1 << col_config[col][1];
		for (i=0; i<2; i++){
			dummy_read=*(volatile unsigned char*)0x140000;//cpu_relax();
		}
		scan_col[col]=0;
		for(row=first_active_row;row<kbd_conf.rows;row++){
			if(!(*keypad_row[row].kbd_pio_stat_reg & keypad_row[row].kbd_pio_id)){
				scan_col[col] |= 1<<row;//shift
			}
		}
		scan_col[col]&=(1<<kbd_conf.rows)-1;//mask out the unused rows
		col++;
	}
	for(i=0; i<kbd_conf.cols; i++){
		__REG(gpio_mode_reg[col_config[i][0]]+col_config[i][1])= GPIO_PUPD_IN_PULLUP; //All Columns Input pull up
	}
	return 0;
}

#endif
#if 1
unsigned char scan_keypad(char first_active_row){
	unsigned char col, row, i;
	row=col=0;

//	COL_WR_BUF=col_mask;//enable all columns
	memset(scan_col, 0, kbd_conf.cols);
	for(row=first_active_row;row<kbd_conf.rows;row++){
		SET_COLUMNS(col_mask);//enable all columns
		if(*keypad_row[row].kbd_pio_stat_reg & keypad_row[row].kbd_pio_id)
		{
			for(col=0;col<kbd_conf.cols;col++){
				SET_COLUMNS(col_mask & ~(1<<col));//disable columns one by one
				//Perform a dummy read to shift the bus fifo
				for (i=0; i<4; i++){
					dummy_read=COL_WR_BUF;//cpu_relax();
				}
				if(!(*keypad_row[row].kbd_pio_stat_reg & keypad_row[row].kbd_pio_id))
				{

					scan_col[col] |= 1<<row;//shift
				}
			}

		}
	}
	return 0;
}

#endif

#if 0
unsigned char scan_keypad(char first_active_row){
	unsigned char col, row, i;

	row=col=0;

	SET_COLUMNS(col_mask);//enable all columns
	while( col<kbd_conf.cols ){
		SET_COLUMNS(col_mask & ~(1<<col));//disable columns one by one

//		udelay(100);
		scan_col[col]=0;
		for(row=first_active_row;row<kbd_conf.rows;row++){
			if(!(*keypad_row[row].kbd_pio_stat_reg & keypad_row[row].kbd_pio_id)){
				scan_col[col] |= 1<<row;//shift
			}
		}
		scan_col[col]&=(1<<kbd_conf.rows)-1;//mask out the unused rows
		col++;
	}
	SET_COLUMNS(col_mask);//enable all columns
	return 0;
}

#endif



#if 0
unsigned char scan_keypad(char first_active_row){
	unsigned char col, row, i;

	row=col=0;

	while( col<kbd_conf.cols ){

		for(i=0; i<KBD_ROWS; i++){
			__REG(gpio_reset_reg[row_config[i][0]])= 1<<row_config[i][0]; //Rows Output 0
		}
		for(i=0; i<KBD_ROWS; i++){
			__REG(gpio_mode_reg[row_config[i][0]]+row_config[i][1])= 0x300; //Rows Output 0
		}
		SET_COLUMNS(0);// All columns 0
		for(i=0; i<KBD_ROWS; i++){
			__REG(gpio_mode_reg[row_config[i][0]]+row_config[i][1])= 0x200; //Rows Input
		}
		SET_COLUMNS(1<<col);//enable columns one by one
		//udelay(100);
		scan_col[col]=0;
		for(row=first_active_row;row<kbd_conf.rows;row++){
			if((*keypad_row[row].kbd_pio_stat_reg & keypad_row[row].kbd_pio_id)){
				scan_col[col] |= 1<<row;//shift
			}
		}
		scan_col[col]&=(1<<kbd_conf.rows)-1;//mask out the unused rows
		col++;
	}
	SET_COLUMNS(0x00);
	return 0;
}

#endif


unsigned short kbd_send_msg(unsigned char index,unsigned char msg_id, unsigned short key_id){
//	kbd_msg m;
//	m.msg_code=msg_id;
//	m.key_id=key_id;

	pressed_keys[index].key_id = key_id;
	pressed_keys[index].msg_code=msg_id;

	//printk("kbd_send_msg input: %x, %x\n", msg_id, key_id );
#if 0
	char row,col;
	row=col=0;//key_scan[key_scan_index].pre_row;
	while(!(key_id & 1<<row++));row--;
	key_id>>=5;
	while(!(key_id & 1<<col++));col--;
	switch(msg_id){
		case KBD_SC1445x_RELEASE_MSG:
			printf("KBD_RELEASE: col= %d, row= %d\n", col, row);
			break;
		case KBD_SC1445x_PRESS_MSG:
			printf("KBD_PRESS: col= %d, row= %d\n", col, row);
			break;
		case KBD_SC1445x_REPEAT_MSG:
			printf("KBD_REPEAT: col= %d, row= %d\n", col, row);
			break;
		case KBD_SC1445x_HOOK_ON_MSG:
			printf("HOOK_ON\n");
			break;
		case KBD_SC1445x_HOOK_OFF_MSG:
			printf("HOOK_OFF\n");
			break;
		case KBD_SC1445x_CALL_END_PRESSED_MSG:
			printf("CALL_END_PRESSED\n");
			break;
		case KBD_SC1445x_CALL_END_RELEASED_MSG:
			printf("CALL_END_RELEASED\n");
			break;
        default:
        	printf("err! KBD msgid unknown = %x\n", msg_id);
            break;
	}
#endif

	return 0;

}




/*******************************************************
	Description: 	The keypad fsm that generates the PRESS, REPEAT, RELEASE msgs based on keypad activity

 ********************************************************/
void check_kbd(){
	unsigned char i, j, k;

	/* Mask out the previously scanned key presses */
	for(i=0;i<kbd_conf.mult_keys;i++){
		key_scan[i].flags &= ~SAME_KEY;
		if( key_scan[i].pre_row && (key_scan[i].pre_row & scan_col[key_scan[i].pre_col]) ){
			scan_col[key_scan[i].pre_col] &= ~key_scan[i].pre_row;//mask out already stored key press
			key_scan[i].flags |= SAME_KEY;
		}
	}
	/* Run fsm for all  key_scans */
	i=0;
	while(i<kbd_conf.mult_keys){
		if(key_scan[i].tmout)
			key_scan[i].tmout--;
		switch(key_scan[i].state){
			case KBD_IDLE_STATE:
				/* Search for a key press*/
				j=0;
				while( j<kbd_conf.cols && !scan_col[j] )
					j++;
				if(j<kbd_conf.cols ){
					/* Find the first row that has this col active and mask it out*/
					key_scan[i].pre_col=j;
					k=0x01;
					while(!(scan_col[j] & k)) k<<=1;
					key_scan[i].pre_row=k;
					scan_col[j] &= ~k;
					if(kbd_conf.debounce_tmouts){
						key_scan[i].tmout=kbd_conf.debounce_tmouts;
						key_scan[i].state=KBD_PRESS_DEBOUNCE_STATE;
					}
					else{
						key_scan[i].state=KBD_PRESS_STATE;
						/*Send KBD_SC1445x_PRESS_MSG*/
						kbd_send_msg(i,KBD_SC1445x_PRESS_MSG, KEY_ID(i));
					//	printf("P %d\n",KEY_ID(i));
					//	pressed_keys[i]=KEY_ID(i);

					}
					total_active_keys++;
				}
				i++;
				break;
			case KBD_PRESS_DEBOUNCE_STATE:
				if(!key_scan[i].tmout){//check only if debounce timer has expired
					if(key_scan[i].flags & SAME_KEY){
						key_scan[i].state=KBD_PRESS_STATE;
						/*Send KBD_SC1445x_PRESS_MSG*/
						kbd_send_msg(i,KBD_SC1445x_PRESS_MSG, KEY_ID(i));
				//		printf("P1 %d\n",KEY_ID(i));
				//		pressed_keys[i]=KEY_ID(i);
						if(kbd_conf.repeat_tmouts_long){
							key_scan[i].tmout=kbd_conf.repeat_tmouts_long;
							key_scan[i].state=KBD_PRESS_STATE;
						}
					}
					else{//rerun the fsm (without sending release msg)
						key_scan[i].state=KBD_IDLE_STATE;
						key_scan[i].pre_row=key_scan[i].pre_col=0;
						total_active_keys--;
						break;
					}
				}
				i++;
				break;
			case KBD_PRESS_STATE:
				if(key_scan[i].flags & SAME_KEY){
					if(!key_scan[i].tmout){
						if(kbd_conf.repeat_tmouts_long){
							/*Send KBD_SC1445x_REPEAT_MSG*/
							kbd_send_msg(i,KBD_SC1445x_REPEAT_MSG, KEY_ID(i));
//							printf("PP %d\n",KEY_ID(i));
//							pressed_keys[i]=KEY_ID(i);
							if(kbd_conf.repeat_tmouts_short){
								key_scan[i].tmout=kbd_conf.repeat_tmouts_short;
								key_scan[i].state=KBD_REPEAT_SHORT_STATE;
							}
							else{
								key_scan[i].state=KBD_WAIT_RELEASE_STATE;
							}
						}
						else{
							key_scan[i].state=KBD_WAIT_RELEASE_STATE;
						}
					}
				}
				else{//rerun the fsm
					key_scan[i].state=KBD_WAIT_RELEASE_STATE;
					break;
				}
				i++;
				break;
			case KBD_REPEAT_SHORT_STATE:
				if(key_scan[i].flags & SAME_KEY){
					if(!key_scan[i].tmout){
						/*Send KBD_SC1445x_REPEAT_MSG*/
						kbd_send_msg(i,KBD_SC1445x_REPEAT_MSG, KEY_ID(i));
//						printf("PP %d\n",KEY_ID(i));
//						pressed_keys[i]=KEY_ID(i);
						key_scan[i].tmout=kbd_conf.repeat_tmouts_short;
					}
				}
				else{//rerun the fsm
					key_scan[i].state=KBD_WAIT_RELEASE_STATE;
					break;
				}
				i++;
				break;
			case KBD_WAIT_RELEASE_STATE:
				if(!(key_scan[i].flags & SAME_KEY)){
					/*Send KBD_SC1445x_RELEASE_MSG*/
					kbd_send_msg(i,KBD_SC1445x_RELEASE_MSG, KEY_ID(i));
//					printf("R %d\n",KEY_ID(i));
					key_scan[i].state=KBD_IDLE_STATE;
					key_scan[i].tmout=0;
					key_scan[i].pre_row=key_scan[i].pre_col=0;
					i++;
					total_active_keys--;
				}
				break;
		}
	}
}

unsigned char kbd_getc(void)
{
	unsigned char i;
	volatile unsigned char ret;

	volatile unsigned short temp;

	ret = 0;

	scan_keypad(0);
	check_kbd();


	if (total_active_keys == 2)
	{

		if ( ((pressed_keys[0].msg_code==KBD_SC1445x_PRESS_MSG) && (pressed_keys[1].msg_code==KBD_SC1445x_REPEAT_MSG)) ||
				((pressed_keys[1].msg_code==KBD_SC1445x_PRESS_MSG) && (pressed_keys[0].msg_code==KBD_SC1445x_REPEAT_MSG))||
				((pressed_keys[1].msg_code==KBD_SC1445x_PRESS_MSG) && (pressed_keys[0].msg_code==KBD_SC1445x_PRESS_MSG)))
		{
	//		printf("2 key %d %d %d %d\n",pressed_keys[0].key_id,pressed_keys[0].msg_code,pressed_keys[1].key_id,pressed_keys[1].msg_code);
			if (((pressed_keys[0].key_id==2052)&&(pressed_keys[1].key_id==65)) || ((pressed_keys[0].key_id==65)&&(pressed_keys[1].key_id==2052)))
			{
	//			printf("break\n");
				ret = 3;
			}
		}
	}
	else
	{
		if ((pressed_keys[0].msg_code==KBD_SC1445x_PRESS_MSG) || (pressed_keys[1].msg_code==KBD_SC1445x_PRESS_MSG))
		{

			//printf("1 key %d %d %d %d\n",pressed_keys[0].key_id,pressed_keys[0].msg_code,pressed_keys[1].key_id,pressed_keys[1].msg_code);

			switch (pressed_keys[0].key_id)
			{
			case 130:
				ret= 0x30;
				break;
			case 33:
				ret= 0x31;
				break;
			case 65:
				ret= 0x32;
				break;
			case 129:
				ret= 0x33;
				break;
			case 257:
				ret= 0x34;
				break;
			case 513:
				ret= 0x35;
				break;
			case 1025:
				ret= 0x36;
				break;
			case 2049:
				ret= 0x37;
				break;
			case 4097:
				ret= 0x38;
				break;
			case 34:
				ret= 0x39;
				break;
			//case 4100:
			case 72:
				ret= '\r';
				break;
			//case 66:
			case 258:
				ret= 0x2e;
				break;
			//case 1028:
			case 136:
				ret= 0x7f;
				break;
			default:
				ret= 0;
				break;
			}
		}

	}
//	if (ret == 3 )
//		printf("break\n");


	return ret;
}





