/*
 * (C) Copyright 2002
 * Kyle Harris, Nexus Technologies, Inc. kharris@nexus-tech.net
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */


#include "armboot.h"
#include "sitel_io.h"
#include "spi_polled.h"
#include "qspic.h"

// for Gigaset board
#define BLUE_MODE_REG	P2_08_MODE_REG
#define GREEN_MODE_REG  P2_11_MODE_REG
#define YELLOW_MODE_REG	P2_12_MODE_REG


void vLEDControl (uint16 port_id, uint8 state)
{
    if (state==0)
    {
        SetBits(P2_RESET_DATA_REG, port_id,  1);
    }
    else
    {
        SetBits(P2_SET_DATA_REG, port_id,  1);
    }
}

/* ------------------------------------------------------------------------- */

/*
 * Miscelaneous platform dependent initialisations
 */

void init_spi1(void)
{

	unsigned i;

	SetPort(P1_15_MODE_REG, PORT_OUTPUT,  PID_port);						/* P0_07 as SPI_EN  */

	SetWord(P1_RESET_DATA_REG,0x8000);

	SetPort(P1_02_MODE_REG, PORT_OUTPUT,  PID_port);					/* P0_06 is SPI_CLK  */

	for (i=0;i<15;i++)
	{
		SetWord(P1_SET_DATA_REG,0x4);
		SetWord(P1_RESET_DATA_REG,0x4);
	}
	
	SetBits(CLK_GPIO1_REG,SW_SPI_DIV,4);
	SetBits(CLK_GPIO3_REG,SW_SPI_EN,1);


	/* Set PPA MATRIX FOR SPI1 */
	SetPort(P1_02_MODE_REG, PORT_OUTPUT,  PID_SPI1_CLK);					/* P0_06 is SPI_CLK  */
	SetPort(P1_13_MODE_REG, PORT_INPUT ,   PID_SPI1_DIN);    /* P2_05 is SPI_DI  */
	SetPort(P2_04_MODE_REG, PORT_OUTPUT,  PID_SPI1_DOUT);					/* P2_08 is SPI_DO */
	SetPort(P0_07_MODE_REG, PORT_OUTPUT,  PID_port);						/* P0_07 as SPI_EN  */

	/* Set SPI SETTINGS */

	SetWord(SPI1_CTRL_REG , 0);				/* clear all fields*/
	SetBits(SPI1_CTRL_REG, SPI_POL,  1);  
	SetBits(SPI1_CTRL_REG, SPI_DO, 1);                 
	SetBits(SPI1_CTRL_REG, SPI_ON, 1);                      // Enable SPI2 
	SetBits(SPI1_CTRL_REG, SPI_CLK,  3);     // SPI_CLK = XTAL/(PER20_DIV/4) = 2.592 MHz

	SetWord(P1_SET_DATA_REG,0x8000);

}



int 
/**********************************************************/
board_post_init(bd_t *bd)
/**********************************************************/
{
    init_spi1();
    int i;
    // read device id from flash
#ifdef CONFIG_DEVICEID
#ifdef CONFIG_FLASH_IS_SPI
    SpiFlashRead (CONFIG_DEVICEID_OFFSET , &(bd->bi_ext.aucDeviceId[0]) , CONFIG_DEVICEID_SIZE );
	printf("Factory device ID=");

	for (i=CONFIG_DEVICEID_SIZE; i>0; i--)
	{
		printf("%02X", bd->bi_ext.aucDeviceId[i-1]);
	}
	printf("\n");
#endif
#endif
#ifdef CONFIG_MAC_OFFSET
#ifdef CONFIG_FLASH_IS_SPI
	unsigned char aucFactoryMAC[CONFIG_MAC_SIZE];
	unsigned char aucFactoryMACString[CONFIG_MAC_SIZE*2+CONFIG_MAC_SIZE+1];
	memset (aucFactoryMACString, 0, sizeof(aucFactoryMACString));

	SpiFlashRead (CONFIG_MAC_OFFSET , &(aucFactoryMAC[0]) , CONFIG_MAC_SIZE );

	sprintf(&(aucFactoryMACString[0]), "%02X:%02X:%02X:%02X:%02X:%02X",
			aucFactoryMAC[0], aucFactoryMAC[1], aucFactoryMAC[2],
			aucFactoryMAC[3], aucFactoryMAC[4], aucFactoryMAC[5]);
	printf("Factory MAC=%s\n", aucFactoryMACString);
#ifdef DEBUG
	printf("Board MAC=");
	for (i=0; i<CONFIG_MAC_SIZE; i++)
	{
		printf("%02X", bd->bi_enetaddr[i]);
	}
	printf("\n");
#endif
	if (memcmp( bd->bi_enetaddr, aucFactoryMAC, CONFIG_MAC_SIZE) != 0)
	{
		printf("Copying factory MAC to environment...\n");
		setenv(bd,"ethaddr",&aucFactoryMACString[0]);
		run_command("saveenv", bd, 0);
	}
#endif
#endif //CONFIG_MAC_OFFSET
	SetPort(BLUE_MODE_REG,   PORT_OUTPUT, PID_port); // Blue led
	SetPort(GREEN_MODE_REG,  PORT_OUTPUT, PID_port); // Green led
	SetPort(YELLOW_MODE_REG, PORT_OUTPUT, PID_port); // Yellow led
    // turn On green led
    vLEDControl (GREEN, 1);
    return 0;
}

int 
/**********************************************************/
board_init(bd_t *bd)
/**********************************************************/
{
	unsigned i,j;
	
	/* arch number of Sitel SC14450 DK */
	bd->bi_arch_number = 452;

	/* vm must change */ 
	/* adress of boot parameters */
	bd->bi_boot_params = CFG_CONFIG_PARAMETERS_ADDR;

	/*   Config PPA for UART. */
#if defined(CONFIG_GIGA94_SUPPORT)
	/* If BOOT_MODE P2[5] is low and KEY P2[9] is not low then configure UART TX as GPIO (no UART TX) */
	if((GetWord(P2_DATA_REG)&GPIO_5)==0 && (GetWord(P2_DATA_REG)&GPIO_9)!=0){
		SetPort(P0_00_MODE_REG, PORT_INPUT,    PID_port);
		SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_port);
	} else {
		SetPort(P0_00_MODE_REG, PORT_OUTPUT,    PID_UTX);
		SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_URX);
	}
#endif

#ifdef CONFIG_FLASH_IS_QUAD_SPI
	qspic_init();
#else
	init_SPI2(CLK_5184, MODE_3, MODE_8BITS);
#endif

// enable timer1 clock
	if (GetWord(CLK_GLOBAL_REG) & SW_XTAL_PLL1_RATE)
		SetBits(CLK_GPIO4_REG, SW_TMR1_DIV, 144);
	else
		SetBits(CLK_GPIO4_REG, SW_TMR1_DIV, 72);
	
	SetBits(CLK_GPIO4_REG, SW_TMR1_EN, 1);


#ifdef SC14452DK_ule


//Enable 2.5v regulator for lmx
	SetPort(P0_04_MODE_REG, PORT_OUTPUT,   PID_port);
	SetWord(P0_SET_DATA_REG,0x10);

//reset lmx
	SetPort(P2_03_MODE_REG, PORT_OUTPUT,   PID_port);

	SetWord(P2_SET_DATA_REG,8);
	for (i=0;i<0x800;i++)
		j=*(volatile unsigned*)0;
	SetWord(P2_RESET_DATA_REG,8);
	for (i=0;i<0x800;i++)
		j=*(volatile unsigned*)0;
	SetPort(P2_03_MODE_REG, PORT_INPUT,   PID_port);


	SetWord(P2_RESET_DATA_REG,8);
	SetPort(P2_13_MODE_REG, PORT_OUTPUT,   PID_LE);
	SetPort(P2_14_MODE_REG, PORT_OUTPUT,   PID_SK);
	SetPort(P2_15_MODE_REG, PORT_OUTPUT,   PID_SIO);


	SetPort(P0_05_MODE_REG, PORT_OUTPUT,   PID_port); //ethernet phy reset pin
	SetWord(P0_SET_DATA_REG,0x20);


#else
#error "board revision is undefined"
#endif


	SetWord(BAT_CTRL_REG,0x412e);
	SetWord(BANDGAP_REG,0x0a);

	RESTORE_OVER_16M_SDRAM_ACEESS
	return 1;

}

int 
/**********************************************************/
dram_init(bd_t *bd)
/**********************************************************/
{
  	bd->bi_dram[0].start = PHYS_SDRAM_1;
	bd->bi_dram[0].size  = PHYS_SDRAM_1_SIZE;
	
	
	bd->bi_dram[1].start = PHYS_SDRAM_2;
	bd->bi_dram[1].size = PHYS_SDRAM_2_SIZE;
	
	return PHYS_SDRAM_1_SIZE+PHYS_SDRAM_2_SIZE;
 
}
//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:49.852220480
//@@ -30,6 +30,25 @@
// #include "sitel_io.h"
// #include "spi_polled.h"
// #include "qspic.h"
//+
//+// for Gigaset board
//+#define BLUE_MODE_REG	P2_08_MODE_REG
//+#define GREEN_MODE_REG  P2_11_MODE_REG
//+#define YELLOW_MODE_REG	P2_12_MODE_REG
//+
//+
//+void vLEDControl (uint16 port_id, uint8 state)
//+{
//+    if (state==0)
//+    {
//+        SetBits(P2_RESET_DATA_REG, port_id,  1);
//+    }
//+    else
//+    {
//+        SetBits(P2_SET_DATA_REG, port_id,  1);
//+    }
//+}
//+
// /* ------------------------------------------------------------------------- */
// 
// /*
//@@ -82,8 +101,55 @@
// board_post_init(bd_t *bd)
// /**********************************************************/
// {
//-	init_spi1();
//-   return 0;
//+    init_spi1();
//+    int i;
//+    // read device id from flash
//+#ifdef CONFIG_DEVICEID
//+#ifdef CONFIG_FLASH_IS_SPI
//+    SpiFlashRead (CONFIG_DEVICEID_OFFSET , &(bd->bi_ext.aucDeviceId[0]) , CONFIG_DEVICEID_SIZE );
//+	printf("Factory device ID=");
//+
//+	for (i=CONFIG_DEVICEID_SIZE; i>0; i--)
//+	{
//+		printf("%02X", bd->bi_ext.aucDeviceId[i-1]);
//+	}
//+	printf("\n");
//+#endif
//+#endif
//+#ifdef CONFIG_MAC_OFFSET
//+#ifdef CONFIG_FLASH_IS_SPI
//+	unsigned char aucFactoryMAC[CONFIG_MAC_SIZE];
//+	unsigned char aucFactoryMACString[CONFIG_MAC_SIZE*2+CONFIG_MAC_SIZE+1];
//+	memset (aucFactoryMACString, 0, sizeof(aucFactoryMACString));
//+
//+	SpiFlashRead (CONFIG_MAC_OFFSET , &(aucFactoryMAC[0]) , CONFIG_MAC_SIZE );
//+
//+	sprintf(&(aucFactoryMACString[0]), "%02X:%02X:%02X:%02X:%02X:%02X",
//+			aucFactoryMAC[0], aucFactoryMAC[1], aucFactoryMAC[2],
//+			aucFactoryMAC[3], aucFactoryMAC[4], aucFactoryMAC[5]);
//+	printf("Factory MAC=%s\n", aucFactoryMACString);
//+#ifdef DEBUG
//+	printf("Board MAC=");
//+	for (i=0; i<CONFIG_MAC_SIZE; i++)
//+	{
//+		printf("%02X", bd->bi_enetaddr[i]);
//+	}
//+	printf("\n");
//+#endif
//+	if (memcmp( bd->bi_enetaddr, aucFactoryMAC, CONFIG_MAC_SIZE) != 0)
//+	{
//+		printf("Copying factory MAC to environment...\n");
//+		setenv(bd,"ethaddr",&aucFactoryMACString[0]);
//+		run_command("saveenv", bd, 0);
//+	}
//+#endif
//+#endif //CONFIG_MAC_OFFSET
//+	SetPort(BLUE_MODE_REG,   PORT_OUTPUT, PID_port); // Blue led
//+	SetPort(GREEN_MODE_REG,  PORT_OUTPUT, PID_port); // Green led
//+	SetPort(YELLOW_MODE_REG, PORT_OUTPUT, PID_port); // Yellow led
//+    // turn On green led
//+    vLEDControl (GREEN, 1);
//+    return 0;
// }
// 
// int 
//@@ -98,12 +164,19 @@
// 
// 	/* vm must change */ 
// 	/* adress of boot parameters */
//-	bd->bi_boot_params = CFG_CONFIG_PARAMETERS_ADDR;//0xa0000100;
//+	bd->bi_boot_params = CFG_CONFIG_PARAMETERS_ADDR;
// 
// 	/*   Config PPA for UART. */
//-	SetPort(P0_00_MODE_REG, PORT_OUTPUT,    PID_UTX);
//-	SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_URX);
//-
//+#if defined(CONFIG_GIGA94_SUPPORT)
//+	/* If BOOT_MODE P2[5] is low and KEY P2[9] is not low then configure UART TX as GPIO (no UART TX) */
//+	if((GetWord(P2_DATA_REG)&GPIO_5)==0 && (GetWord(P2_DATA_REG)&GPIO_9)!=0){
//+		SetPort(P0_00_MODE_REG, PORT_INPUT,    PID_port);
//+		SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_port);
//+	} else {
//+		SetPort(P0_00_MODE_REG, PORT_OUTPUT,    PID_UTX);
//+		SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_URX);
//+	}
//+#endif
// 
// #ifdef CONFIG_FLASH_IS_QUAD_SPI
// 	qspic_init();
//@@ -157,7 +230,7 @@
// 	SetWord(BAT_CTRL_REG,0x412e);
// 	SetWord(BANDGAP_REG,0x0a);
// 
//-
//+	RESTORE_OVER_16M_SDRAM_ACEESS
// 	return 1;
// 
// }
//@@ -177,4 +250,3 @@
// 	return PHYS_SDRAM_1_SIZE+PHYS_SDRAM_2_SIZE;
//  
// }
//-
