/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <vassilis.maniotis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 * See http://www.gnu.org/licenses/gpl-2.0.txt for more details.
 */

/*========================== Include files ==================================*/
//#include <intrinsics.h>
#include "sitel_io.h"
#include "spi_polled.h"

/*========================== Local macro definitions ========================*/

/*========================== Global definitions =============================*/
#define READ               0
#define WRITE              1
#define DUMMY       0xffff//0x0000

/*========================== Local data definitions =========================*/
uint8   g_bits2;

/*========================== Local function prototypes ======================*/

/*========================== Function definitions ===========================*/

void init_SPI2(uint8 freq, uint8 mode, uint8 bits)
/*==============================================================================
* Inputs         frequenty
* Returns        nothing.
*
* Description    Initializes the SPI for transmission at freq clock, mode 0,1,2,3
*                8,16 or 32 bits.
*                the following clock frequenties can be selected: 
*
*                 CLK_1296    
*                 CLK_2592    
*                 CLK_5184      
*                 CLK_1152      
*                 
*                 Following Modes can be selected:
*                 MODE_0     
*                 MODE_1     
*                 MODE_2     
*                 MODE_3     
*                 
*                 Following Data Types Can be selected
*                 MODE_8BITS  
*                 MODE_16BITS 
*                 MODE_32BITS 
*==============================================================================
*/
{ 
	g_bits2 = bits;

	/* Disable Interrupts */
	// _disable_interrupt_();    
#if 1
	/* Set PPA MATRIX FOR SPI2 */
	SetPort(P0_12_MODE_REG, PORT_OUTPUT,  PID_SPI2_DOUT);   /* P0_12 as SPI_DO  */
	SetPort(P0_13_MODE_REG, PORT_INPUT,   PID_SPI2_DIN);    /* P0_13 as SPI_DI  */
	SetPort(P0_06_MODE_REG, PORT_OUTPUT,  PID_SPI2_CLK);    /* P0_14 as SPI_CLK */
	//vm
	SetPort(P0_03_MODE_REG, PORT_OUTPUT,  PID_port);        /* P0_03 as SPI_EN  */  
	/* Set SPI SETTINGS */
	if( !(GetWord(P2_DATA_REG) & GPIO_5) ){
		SetBits(CLK_GPIO2_REG,SW_SPI2_DIV,3);
	}else{
		SetBits(CLK_GPIO2_REG,SW_SPI2_DIV,8);
	}
	SetBits(CLK_GPIO3_REG,SW_SPI2_EN,1);


	SetBits(SPI2_CTRL_REG, SPI_WORD, bits);                 // Set SPI 8bits Mode,  Serial Flash Device
	SetBits(SPI2_CTRL_REG, SPI_SMN,  0x00);                 // Set SPI IN MASTER MODE
	SetBits(SPI2_CTRL_REG, SPI_POL,  (mode & 0x01));  
	SetBits(SPI2_CTRL_REG, SPI_PHA,  (mode >> 1) & 0x01);   // MODE 3: SPI2_POL=1  and PI2_PHA=1
	SetBits(SPI2_CTRL_REG, SPI_MINT, 0x1);                  // Disable SPI2_INT to ICU, timer0 is used
	SetBits(SPI2_CTRL_REG, SPI_ON, 1);                      // Enable SPI2 

	SetBits(SPI2_CTRL_REG, SPI_CLK,  freq);                  // SPI_CLK = XTAL/(PER20_DIV/4) = 2.592 MHz

//	SetBits(CLK_PER10_DIV_REG,PER20_DIV,1);
#endif
#if 0
	 SetWord(P0_DATA_REG, GPIO_3 | GPIO_6 | GPIO_12 | GPIO_13 | GPIO_14);  // Note: QSPI.WPn=P0[11]=0
    SetPort(P0_03_MODE_REG, PORT_OUTPUT,  PID_port);        // 452: Chip Select
    SetPort(P0_06_MODE_REG, PORT_OUTPUT,  PID_SPI2_CLK);    // 452: use SPI clk the P0[6] (instead of P0[14])
    SetPort(P0_11_MODE_REG, PORT_OUTPUT,  PID_SPI2_DOUT);   // 452: Write protect
    SetPort(P0_12_MODE_REG, PORT_OUTPUT,  PID_SPI2_DOUT);   // Set PPA
    SetPort(P0_13_MODE_REG, PORT_PULL_UP, PID_SPI2_DIN);
    SetPort(P0_14_MODE_REG, PORT_OUTPUT,  PID_port);        // 452: QSPI HOLDn pin (keep always high)

    /*---------------------------------------------------*/
    /*   Config SPI2 interface                           */
    /*---------------------------------------------------*/
    SetWord(SPI2_CTRL_REG, 0);                              // Reset SPI2
    
    /*** 450 configuration
    SetBits(CLK_PER10_DIV_REG, PER20_DIV, 1);               // SPI Clock Xtal/1 because PLL is off
    SetBits(SPI2_CTRL_REG, SPI_CLK,  1);                    // SPI_CLK = XTAL/(PER20_DIV/4) = 2.592 MHz
    ***/
    SetBits(CLK_GPIO2_REG, SW_SPI2_DIV, 4);  // set SPI2_CLK = XTAL/4 = 2.592 MHz...
    SetBits(CLK_GPIO3_REG, SW_SPI2_EN,  1);  //...and enable it...
                                             //...(Note: the clock will not be driven out until SPI2_CTRL_REG.SPI_ON==1)

    SetBits(SPI2_CTRL_REG, SPI_WORD, 0x00);                 // Set SPI 8bits Mode,  Serial Flash Device
    SetBits(SPI2_CTRL_REG, SPI_SMN,  0x00);                 // Set SPI IN MASTER MODE
    SetBits(SPI2_CTRL_REG, SPI_POL,  0x01);  
    SetBits(SPI2_CTRL_REG, SPI_PHA,  0x01);                 // MODE 3: SPI2_POL=1  and PI2_PHA=1
    SetBits(SPI2_CTRL_REG, SPI_MINT, 0x1);                  // 450: Disable SPI2_INT to ICU, timer0 is used
                                                            // 452: Use the SPI INT integation (timer0 is not used!)
    SetBits(SPI2_CTRL_REG, SPI_ON,   1);                    // Enable SPI2 (NOTE: this will also enable the generation of SPI2 clock)
#endif
}

bool putByte_SPI2(uint32 x)
/******************************************************************************
 *  Write a single item to serial flash via SPI
 *****************************************************************************/
{ 
  switch(g_bits2)
  {
  case MODE_8BITS:
    SetWord(SPI2_RX_TX_REG0, (uint8) x);
    break;
  case MODE_16BITS:
    SetWord(SPI2_RX_TX_REG0, (uint16) x);
    break;
  case MODE_32BITS:
    SetWord(SPI2_RX_TX_REG1, (uint16)(x >> 16) );
    SetWord(SPI2_RX_TX_REG0, (uint16) x); 
  }
  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==0));
  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==1)) 
  {
    SetWord(SPI2_CLEAR_INT_REG, 0x01);
  }
  return TRUE;
}

bool getByte_SPI2(uint32* x  )
/******************************************************************************
*  Read a single item from serial flash via SPI
*****************************************************************************/
{
  SetWord(SPI2_RX_TX_REG0, (uint16) DUMMY);
  
  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==0));
  while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==1)) 
  {
    SetWord(SPI2_CLEAR_INT_REG, 0x01);
  }
  switch(g_bits2)
  {
  case MODE_8BITS:
    *(uint8*)x=(uint8)GetWord(SPI2_RX_TX_REG0); 
    break;
  case MODE_16BITS:
    *(uint16*)x=GetWord(SPI2_RX_TX_REG0); 
    break;
  case MODE_32BITS:
    *(uint16*)x=GetWord(SPI2_RX_TX_REG1);
    *(uint32*)x=GetWord(SPI2_RX_TX_REG0 | (*x<<16) ); 
  }
  return TRUE;
} 

bool putData_SPI2(uint32* x, uint32 cnt)
/******************************************************************************
 *  Write a uint8s to serial flash via SPI
 *****************************************************************************/
{
   int i;
  for(i=0; i<cnt; i++)
  {
    switch(g_bits2)
    {
    case MODE_8BITS:
      SetWord(SPI2_RX_TX_REG0, ((uint8*)x)[i]);
      break;
    case MODE_16BITS:
      SetWord(SPI2_RX_TX_REG0, ((uint16*)x)[i]);
      break;
    case MODE_32BITS:
      SetWord(SPI2_RX_TX_REG1, ( x[i] >> 16) );
      SetWord(SPI2_RX_TX_REG0, ( x[i] ) ); 
    }
    while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==0));
    while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==1)) 
    {
      SetWord(SPI2_CLEAR_INT_REG, 0x01);
    }
  }
  return TRUE;
}   
   
bool getData_SPI2(uint32* x, uint32 cnt)
/******************************************************************************
*  Read a uint8s from serial flash via SPI
*****************************************************************************/
{
  int i;
  for(i=0; i<cnt; i++)
  {
    SetWord(SPI2_RX_TX_REG0, (WORD) DUMMY);
    
    while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==0));
    while (GetBits(SPI2_CTRL_REG, SPI_INT_BIT==1)) 
    {
      SetWord(SPI2_CLEAR_INT_REG, 0x01);
    }
    switch(g_bits2)
    {
    case MODE_8BITS:
      ((uint8*)x)[i]=(uint8)GetWord(SPI2_RX_TX_REG0); 
      break;
    case MODE_16BITS:
      ((uint16*)x)[i]=GetWord(SPI2_RX_TX_REG0); 
      break;
    case MODE_32BITS:
      ((uint16*)x)[i]=GetWord(SPI2_RX_TX_REG1);
      ((uint32*)x)[i]=GetWord(SPI2_RX_TX_REG0 | ( x[i]<<16) ); 
    }
  }
  return TRUE;
} 

void masterHandshake_SPI(void)
/*==============================================================================
* Inputs         None.
* Returns        None.
*
* Description    SPI Master Handshake
*                
*==============================================================================
*/
{
   while( (P0_DATA_REG & SPI_BUSY ) != 0);
}

//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:49.852220480
//@@ -67,8 +67,11 @@
// 	//vm
// 	SetPort(P0_03_MODE_REG, PORT_OUTPUT,  PID_port);        /* P0_03 as SPI_EN  */  
// 	/* Set SPI SETTINGS */
//-
//-	SetBits(CLK_GPIO2_REG,SW_SPI2_DIV,8);
//+	if( !(GetWord(P2_DATA_REG) & GPIO_5) ){
//+		SetBits(CLK_GPIO2_REG,SW_SPI2_DIV,3);
//+	}else{
//+		SetBits(CLK_GPIO2_REG,SW_SPI2_DIV,8);
//+	}
// 	SetBits(CLK_GPIO3_REG,SW_SPI2_EN,1);
// 
// 
