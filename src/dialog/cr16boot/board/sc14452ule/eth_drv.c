#include <armboot.h>
#include <command.h>
#include <net.h>
#include <malloc.h>
#include "dma.h"

#include "asm.h"


#if DEBUG & 0
#define PRINTK(args...) printf(args)
#else
//#define PRINTK(args...) printf(args)
#define PRINTK(args...)
#endif
char isloopbackok=1;
char ethloopresult =0;
extern char ethloopresult;
unsigned char golden_eth_data[6]={255,255,255,255,255,255};
/* timeout for tx/rx in s */
#define TOUT 5
//#define CONFIG_IPBS_CVM_10MBPS
//-----------------------------------------------------------------------
//              Definitions for memory allocation
//-----------------------------------------------------------------------

#define TX_DESCRIPTORS_NUM        3
#define RX_DESCRIPTORS_NUM        8

#define TX_BUF_GAP  0      // space between two sequential tx buffers (bytes)
#define TX_BUF_SIZE 1024   // the size of one tx buffer (bytes)

#define RX_BUF_GAP  0      // space between two sequential rx buffers (bytes)
#define RX_BUF_SIZE 1024   // the size of one rx buffer (bytes)

#define TX_DES_BASE  0x0020000
#define RX_DES_BASE  0x0021000

#define TX_BUF_BASE  0x0022000
#define RX_BUF_BASE  0x0026000

#define TX_FRAME_BUFF 0x0036000
#define RX_FRAME_BUFF 0x0038000

//-----------------------------------------------------------------------
//              Definitions for the SMI interface (MDC, MDIO)
//-----------------------------------------------------------------------
#define MD_PHY_ADDR(x) DWORD_SHIFT((x & 0x1f),11)
#define MD_REG_ADDR(x) DWORD_SHIFT((x & 0x1f),6)
#define MD_CLK(x)      DWORD_SHIFT((x & 0x7),2)
#define MD_WRITE       DWORD_SHIFT(1,1)
#define MD_BUSY        0x1

#define EMAC_MDC_DIV_42  0x0
#define EMAC_MDC_DIV_62  0x1
#define EMAC_MDC_DIV_16  0x2
#define EMAC_MDC_DIV_26  0x3
#define EMAC_MDC_DIV_102 0x4
#define EMAC_MDC_DIV_122 0x5

//-----------------------------------------------------------------------
//              Mathematics
//-----------------------------------------------------------------------

#define DWORD_SHIFT(DAT,BITS) ((unsigned long int) (DAT) << (BITS))

#define NUM_OF_DWORDS(x) (((x % 4) == 0) ? (x >> 2) : ((x >>2 ) + 1))

//-----------------------------------------------------------------------
//              Emac peripheral ID
//-----------------------------------------------------------------------

#define PID_EMAC_PIN  57

//-----------------------------------------------------------------------
//  The dma channel used in order to accelerate the process of memory copy.
//-----------------------------------------------------------------------

#define EMAC_USE_DMA_CHANNEL 0

//-----------------------------------------------------------------------
//              Structures and variables related with the emac.
//-----------------------------------------------------------------------

/***
 *** Tx/Rx descriptors
 ***/
struct s_descriptor {
   volatile unsigned long int DES0;
   volatile unsigned long int DES1;
   volatile unsigned long int DES2;
   volatile unsigned long int DES3;   
};

struct emac_dev {
		struct s_descriptor  *tx_descriptor;
		unsigned long int    *tx_buf_base;
		unsigned long int    tx_buf_size;
		unsigned char        num_of_tx_des;		
		unsigned char        tx_ptr;
		struct s_descriptor  *rx_descriptor;
		unsigned long int    *rx_buf_base;
		unsigned long int    rx_buf_size;
		unsigned char        num_of_rx_des;
		unsigned char        rx_ptr;
};

struct emac_dev emacDev;

char TxEnd;

#define MAX_RX_FRAMES 4
#define RX_FRAME_SIZE 2000   // Multiple of 4 only!!!!

unsigned char *RxFrameBuf=NULL;

int RxFrameLen[MAX_RX_FRAMES];

char RxFrames=0;

typedef enum _speed_mode {
	s10_full = 0,
	s10_half,
	s100_full,
	s100_half,
}speed_mode;

int emac_init_speed(unsigned short speed);

/*----------------------------------------------------------------------------*/
/*                               Clock Enable                                 */
/*----------------------------------------------------------------------------*/
void emac_clk_enable(void) {
	SetBits(CLK_AUX2_REG, SW_EMAC_EN, 1);	
}

/*----------------------------------------------------------------------------*/
/*                               Clock Disable                                */
/*----------------------------------------------------------------------------*/
void emac_clk_disable(void) {
	SetBits(CLK_AUX2_REG, SW_EMAC_EN, 0);
}

/*----------------------------------------------------------------------------*/
/*                   Sets the interface speed to 10Mpbs                       */
/*----------------------------------------------------------------------------*/
void emac_10mbps(void) {
	SetBits(CLK_AUX2_REG, SW_ETH_SEL_SPEED, 0);
}

/*----------------------------------------------------------------------------*/
/*                   Sets the interface speed to 100Mpbs                      */
/*----------------------------------------------------------------------------*/
void emac_100mbps(void) {
	SetBits(CLK_AUX2_REG, SW_ETH_SEL_SPEED, 1);
}

/*----------------------------------------------------------------------------*/
/*                           Init PADs for RMII mode                          */
/*----------------------------------------------------------------------------*/

void emac_rmii_pads (void)
{
	SetPort(P3_02_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // port3(2) -> emac ref_clk

	SetPort(P3_03_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxEn output
	SetPort(P3_07_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxD[0] output
	SetPort(P3_08_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxD[1] output
	SetPort(P3_04_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxDV input
	SetPort(P3_05_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxD[0] input
	SetPort(P3_06_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxD[1] input

}

/*----------------------------------------------------------------------------*/
/*                           Init PADs for MII mode                          */
/*----------------------------------------------------------------------------*/

void emac_mii_pads (void)
{
	SetPort(P0_00_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // COL input
	SetPort(P0_01_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // CSR input
	SetPort(P0_02_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RXER input

	SetPort(P0_04_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // TXCLK
	SetPort(P0_05_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RXCLK

	SetPort(P3_03_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxEn output
	SetPort(P3_07_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxD[0] output
	SetPort(P3_08_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxD[1] output
	SetPort(P0_07_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxD[2] output
	SetPort(P0_08_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxD[3] output

	SetPort(P3_04_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxDV input
	SetPort(P3_05_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxD[0] input
	SetPort(P3_06_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxD[1] input
	SetPort(P0_09_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxD[2] input
	SetPort(P0_10_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxD[3] input
}

/*----------------------------------------------------------------------------*/
/*                           Init MD PADs                                     */
/*----------------------------------------------------------------------------*/

void emac_md_pads(void)
{
	SetPort(P3_00_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);    // MDC output
	SetPort(P3_01_MODE_REG, PORT_INPUT,  PID_EMAC_PIN);    // MDIO
}


/*----------------------------------------------------------------------------*/
/*                   Set RMII mode and Internal Clocks                        */
/*----------------------------------------------------------------------------*/
void emac_rmii_clk_int(void)
{
	SetBits(CLK_AUX2_REG, SW_RMII_EN,1);        // Set RMII Mode
	SetBits(CLK_AUX2_REG, SW_RMII_CLK_INT, 1);  // Internal source for RMII ref clock
	SetBits(GPRG_R0_REG, 0x0100, 0);            // REF_CLK output
}


/*----------------------------------------------------------------------------*/
/*                   Set RMII mode and External Clocks                        */
/*----------------------------------------------------------------------------*/
void emac_rmii_clk_ext (void)
{
	SetBits(CLK_AUX2_REG, SW_RMII_EN,1);        // Set RMII Mode
	SetBits(GPRG_R0_REG, 0x0100, 1);            // REF_CLK input
	SetBits(CLK_AUX2_REG, SW_RMII_CLK_INT, 0);  // External source for RMII ref clock
}


/*----------------------------------------------------------------------------*/
/*                   Set MII mode and External  Clocks                        */
/*----------------------------------------------------------------------------*/

void emac_mii_clk_ext(void)
{
	SetBits(CLK_AUX2_REG, SW_RMII_EN,0);                  // Set MII Mode
	SetPort(P0_04_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // TXCLK input
	SetPort(P0_05_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RXCLK input
}

/*----------------------------------------------------------------------------*/
/*                          MD Interface - Read Function                      */
/*----------------------------------------------------------------------------*/
unsigned int emac_md_read (unsigned char phy_address, unsigned char reg_address, unsigned char mdc_div)
{
	// Check the BUSY bit before write to the EMAC_MACR4_MII_ADDR_REG
	while (GetDword (EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);

	// Write the read request
	SetDword (EMAC_MACR4_MII_ADDR_REG, MD_PHY_ADDR(phy_address) | 
	                                   MD_REG_ADDR(reg_address) |
					   MD_CLK(mdc_div) |
					   MD_BUSY);
		
	// Wait Until to receive the requested data
	while (GetDword (EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);
		
	return (GetDword (EMAC_MACR5_MII_DATA_REG));
}



/*----------------------------------------------------------------------------*/
/*                         MD Interface - Write Function                      */
/*----------------------------------------------------------------------------*/
void emac_md_write (unsigned char phy_address, unsigned char reg_address, unsigned char mdc_div, unsigned int wdata)
{
	// Check the BUSY bit before write to the EMAC_MACR4_MII_ADDR_REG & EMAC_MACR5_MII_ADDR_REG
	while (GetDword (EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);
	
	SetDword (EMAC_MACR5_MII_DATA_REG, wdata);

	SetDword (EMAC_MACR4_MII_ADDR_REG, MD_PHY_ADDR(phy_address) | 
	                                   MD_REG_ADDR(reg_address) |
	                                   MD_CLK(mdc_div) |
	                                   MD_WRITE  |
	                                   MD_BUSY      );

	// Wait Until  the end of write operation
	while (GetDword(EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);

}


/*----------------------------------------------------------------------------*/
/*                     Initialize the external switch                         */
/*----------------------------------------------------------------------------*/




/*----------------------------------------------------------------------------*/
/*                   Initialize the external PHY KSZ8041RNL                   */
/*----------------------------------------------------------------------------*/



#ifndef ETH_PHY_ADDR
#define ETH_PHY_ADDR 1
#endif


speed_mode KSZ8041RNL_init(void) 
{
	unsigned int temp;
	int i = 0, cnt = 0;
	int opmode = 0;
	int ret = s100_full;

	/* 
		unsigned int temp;	
		printf("------------ Read registers --------------------- \n");						
	
		for (temp =0; temp<=0x1F; temp++) 
		{ 
			printf("register %2x %s %x\n", temp, "=", emac_md_read(ETH_PHY_ADDR, temp, EMAC_MDC_DIV_42));
		}
	*/
#ifdef REEF
	//emac_md_write(ETH_PHY_ADDR , 18, EMAC_MDC_DIV_42, 0x40E0);  // Set correct strap pins
	//emac_md_write(ETH_PHY_ADDR ,  0, EMAC_MDC_DIV_42, 0x8000); // SW-Reset

    SetPort(P0_05_MODE_REG, PORT_OUTPUT, PID_port);
    SetWord(P0_SET_DATA_REG,0x20);
    udelay(50000);
    SetWord(P0_RESET_DATA_REG,0x20);
    udelay(50000);
    SetWord(P0_SET_DATA_REG,0x20);
    udelay(50000);
	//emac_md_write(ETH_PHY_ADDR , 0, EMAC_MDC_DIV_42, 0x1000);		// auto-neg enable

	temp = emac_md_read(ETH_PHY_ADDR, 0x1F, EMAC_MDC_DIV_42);

	i= (isloopbackok==2)?40:150;

	if (isloopbackok==2) DoFlashCopy();

	while (((temp&0x1000)==0) && (cnt < i)) {
		temp = emac_md_read(ETH_PHY_ADDR, 0x1F, EMAC_MDC_DIV_42);
		//printf("register 0x1F=[%x] cnt=%d tmp1=[%d] \n", temp, cnt, (temp&0x1000)==0);
		cnt++;
		udelay(50000);
	}
	if ((cnt==i)&&(isloopbackok==2)){
		printf("ETH LOOPBACK:FAIL TIME\n");
		ethloopresult=0;
		NetLoopbackOK();
		isloopbackok=0;
	}
#else
	
	SetPort(P1_13_MODE_REG, PORT_PULL_UP, PID_port);	// ensure ETH_RSTn = High 

	emac_md_write (1, 0, EMAC_MDC_DIV_42, 0x1000);		// auto-neg enable

	temp = emac_md_read(1, 0x1F, EMAC_MDC_DIV_42);

	while ((!((temp&0x80)>>7)) && (cnt < 10000)) {
		temp = emac_md_read(1, 0x1F, EMAC_MDC_DIV_42);
		//printf("register 0x1F=[%x]  tmp1=[%d], tmp2=[%d]\n", temp, !((temp&0x80)>>8), !((temp&0x80)>>7));
		cnt++;
	}
#endif
	printf("register 0x1F=[%x] cnt=%d tmp1=[%d] \n", temp, cnt, (temp&0x1000)==0);
	opmode = (temp&0x1c)>>2;
	switch (opmode)
	{
		case 0:
			printf("Eth speed: Still auto negotiating \n");
			break;
		case 1:
			emac_init_speed(10);
			ret = s10_half;
			printf("Eth speed: 10Mbps half duplex \n");
			break;
		case 2:
			emac_init_speed(100);
			ret = s100_half;
			printf("Eth speed: 100Mbps half duplex \n");
			break;
		case 3:
			printf("Eth speed: Unknown. Reverting to 100Mbps full duplex  \n");
			break;
		case 4:
			printf("Eth speed: Unknown. Reverting to 100Mbps full duplex  \n");
			break;
		case 5:
			emac_init_speed(10);
			ret = s10_full;
			printf("Eth speed: 10Mbps full duplex \n");
			break;
		case 6:
			emac_init_speed(100);
			ret = s100_full;
			printf("Eth speed: 100Mbps full duplex \n");
			break;
		case 7:
			printf("Eth speed: Unknown. Reverting to 100Mbps full duplex  \n");
			break;
		default: 
			printf("Eth speed: Unknown. Reverting to 100Mbps full duplex  \n"); 
			break;
	}
	/*
		for (temp=0; temp<=0x1F; temp++) 
		{ 
			printf("register %2x %s %x\n", temp, "=", emac_md_read(ETH_PHY_ADDR, temp, EMAC_MDC_DIV_42));
		}
	*/

	return ret;
}	

/*----------------------------------------------------------------------------*/
/*                           Init Tx Descriptor                               */
/*----------------------------------------------------------------------------*/

void emac_init_tx_desc (struct s_descriptor *tx_des, unsigned char num_of_tx_des, unsigned long int *tx_buf_base,
                        unsigned int tx_buf_size,    unsigned char tx_buf_gap)
{
	unsigned char i;
	long int temp;

    
	emacDev.tx_descriptor = tx_des;
	emacDev.num_of_tx_des = num_of_tx_des;
	emacDev.tx_buf_base   = tx_buf_base;
	emacDev.tx_buf_size   = tx_buf_size & 0x7ff;

	temp = DWORD_SHIFT(0x1 , 31) | // [31]    Interrupt Completion
	       DWORD_SHIFT(0x0 , 30) | // [30]    Last Segment
	       DWORD_SHIFT(0x0 , 29) | // [29]    First Segment
	       DWORD_SHIFT(0x0 , 27) | // [28:27] Checksum Insertion Control
	       DWORD_SHIFT(0x0 , 26) | // [26]    Disable CRC
	       DWORD_SHIFT(0x0 , 25) | // [25]    Transmit End Of Ring
	       DWORD_SHIFT(0x0 , 24) | // [24]    Second address Chained
	       DWORD_SHIFT(0x0 , 23) | // [23]    Disable padding
	       DWORD_SHIFT(0x0 , 11) | // [21:11] Trasmit Buffer 2 Size
	                   0x0;        // [10:0]  Trasmit Buffer 1 Size
	   
	for (i=0; i<emacDev.num_of_tx_des; i++) {
		emacDev.tx_descriptor[i].DES0 = 0x00000000; // The software is the owner of the descriptor
		emacDev.tx_descriptor[i].DES1 = temp;
	
		emacDev.tx_descriptor[i].DES2 = ((unsigned long int) emacDev.tx_buf_base) +  2*i   *(emacDev.tx_buf_size + tx_buf_gap);
		emacDev.tx_descriptor[i].DES3 = ((unsigned long int) emacDev.tx_buf_base) + (2*i+1)*(emacDev.tx_buf_size + tx_buf_gap);
	}
    
	// Set Transmit End of Ring in the Last descriptor
	emacDev.tx_descriptor[emacDev.num_of_tx_des-1].DES1 = emacDev.tx_descriptor[emacDev.num_of_tx_des-1].DES1 | DWORD_SHIFT(0x1,25); // [25] Transmit End Of Ring;

	emacDev.tx_ptr=0;
}


/*----------------------------------------------------------------------------*/
/*                           Init Rx Descriptor                               */
/*----------------------------------------------------------------------------*/

void emac_init_rx_desc (struct s_descriptor *rx_des, unsigned char num_of_rx_des, unsigned long int *rx_buf_base,
                        unsigned int rx_buf_size,    unsigned char rx_buf_gap)
{
	unsigned char i;
	long int temp;

	emacDev.rx_descriptor = rx_des;
	emacDev.num_of_rx_des = num_of_rx_des;
	emacDev.rx_buf_base   = rx_buf_base;
	emacDev.rx_buf_size   = rx_buf_size & 0x7ff;
	
	temp = DWORD_SHIFT(                0x0 , 31) | // [31]    Disable Interrupt On Completion
	       DWORD_SHIFT(                0x0 , 25) | // [25]    Receive End Of Ring
	       DWORD_SHIFT(                0x0 , 24) | // [24]    Second address Chained
	       DWORD_SHIFT(emacDev.rx_buf_size , 11) | // [21:11] Trasmit Buffer 2 Size
	                   emacDev.rx_buf_size;        // [10:0]  Trasmit Buffer 1 Size

	for (i=0; i < emacDev.num_of_rx_des; i++) {
		emacDev.rx_descriptor[i].DES0 = 0x00000000;    	
		emacDev.rx_descriptor[i].DES1 = temp;

		emacDev.rx_descriptor[i].DES2 = ((unsigned long int) emacDev.rx_buf_base) +  2*i   *(emacDev.rx_buf_size + rx_buf_gap);
		emacDev.rx_descriptor[i].DES3 = ((unsigned long int) emacDev.rx_buf_base) + (2*i+1)*(emacDev.rx_buf_size + rx_buf_gap);
	}    

	// Set Receive End of Ring in the Last descriptor
	emacDev.rx_descriptor[emacDev.num_of_rx_des-1].DES1 = emacDev.rx_descriptor[emacDev.num_of_rx_des-1].DES1 | DWORD_SHIFT(0x1,25); // [25] Receive End Of Ring;

	emacDev.rx_ptr=0;
}

/*----------------------------------------------------------------------------*/
/*                       Prepare Rx Descriptors                               */
/*----------------------------------------------------------------------------*/
void emac_rx_des_prepare(void) 
{
	unsigned char i;

	for (i=0; i<emacDev.num_of_rx_des; i++) 
		emacDev.rx_descriptor[i].DES0 = 0x80000000; // Set OWN bit. The descriptor is owned by the DMA of the EMAC.
	
}

/*----------------------------------------------------------------------------*/
/*                                   Init Emac                                */
/*----------------------------------------------------------------------------*/

void emac_cfg (unsigned char *mac_address, speed_mode sp_mode)
{
	long int temp;
	unsigned char eth_sp = 1, duplex = 1;
	
	/***
	 *** Init: (see EthMAC datasheet paragraph 3.3.1,pp.46)
	 ***/
	 
	/* a) DMA Register0 = ...
	 *    Bus Mode Register
	 *    Details:
	 *    -> DMA Register0.DA = 1; // set round-robin arbitration with priority at Rx during collisions
	 *    -> Set fixed bursts
	 *       DMA Register0, bit FB 
	 *       and set maximum burst length (is necessary !!!!!):
	 *       Register0[13:8]=PBL field
	 */
	temp = DWORD_SHIFT( 0x1  , 25) |  // [25]    Address - Aligned Beats
	       DWORD_SHIFT( 0x0  , 24) |  // [24]    4xPBL Mode
	       DWORD_SHIFT( 0x1  , 23) |  // [23]    Use Separate PBL
	       DWORD_SHIFT( 0x04 , 17) |  // [22:17] RxDMA PBL
	       DWORD_SHIFT( 0x1  , 16) |  // [16]    Fixed Burst
	       DWORD_SHIFT( 0x1  , 14) |  // [15:14] Rx:Tx priority ratio
	       DWORD_SHIFT( 0x10 ,  8) |  // [13:8]  Programmable Burst Length
	       DWORD_SHIFT( 0x0  ,  2) |  // [6:2]   DSL : Descriptor Skip Length
	       DWORD_SHIFT( 0x1  ,  1) |  // [1]	  DMA Arbitration scheme
	                    0x0;          // [0]    Software Reset
     
	SetDword (EMAC_DMAR0_BUS_MODE_REG, temp); 
  
	/* b) DMA Register7 = ...
	 *    Interrupt Enable Register
	 */
	temp = DWORD_SHIFT( 0x1 , 16) | // [16] Normal Interrupt Summary Enable
	       DWORD_SHIFT( 0x1 , 15) | // [15] Abnormal Interrupt Summary Enable
	       DWORD_SHIFT( 0x1 , 14) | // [14] Early Receive Interrupt Enable
	       DWORD_SHIFT( 0x1 , 13) | // [13] Fatal Bus Error Enable
	       DWORD_SHIFT( 0x1 , 10) | // [10] Early Transmit Interrupt Enable
	       DWORD_SHIFT( 0x1 ,  9) | // [ 9] Receive Watchdog Timeout Enable
	       DWORD_SHIFT( 0x1 ,  8) | // [ 8] Receive Stopped Enable
	       DWORD_SHIFT( 0x1 ,  7) | // [ 7] Receive Buffer Unavailable Enable
	       DWORD_SHIFT( 0x1 ,  6) | // [ 6] Receive Interrupt Enable
	       DWORD_SHIFT( 0x1 ,  5) | // [ 5] Underflow Interrupt Enable
	       DWORD_SHIFT( 0x1 ,  4) | // [ 4] Overflow Interrupt Enable
	       DWORD_SHIFT( 0x1 ,  3) | // [ 3] Transmit Jabber Timeout Enable
	       DWORD_SHIFT( 0x1 ,  2) | // [ 2] Transmit Buffer Unavailable Enable
	       DWORD_SHIFT( 0x1 ,  1) | // [ 1] Transmit Stopped Enable
	                 0x1;        // [ 0] Transmit Interrupt Enable

	SetDword (EMAC_DMAR7_INT_ENABLE_REG,temp);	    
	        
	/* c) Prepare Tx/Rx buffers
	 *    and update DMA Register3 ( Receive Descript List Address Register) 
	 *               DMA Register4 ( Transmit Descriptor List Address Regsiter)
	 */
	SetDword(EMAC_DMAR3_RX_DESCRIPTOR_LIST_ADDR_REG, (unsigned long int) emacDev.rx_descriptor);
	SetDword(EMAC_DMAR4_TX_DESCRIPTOR_LIST_ADDR_REG, (unsigned long int) emacDev.tx_descriptor); 
	 
	 
	/* d) set filtering options at
	 *    MAC Register1 (Frame Filter)
	 *    MAC Register2 (Reserved - Hash Disabled)
	 *    MAC Register3 (Reserved - Hash Disabled)
	 *    MAC Register 16 & 17 (MAC Address 0)
	 */
     
	temp = DWORD_SHIFT(  0x0 , 31) | // [31]  Receive All
	       DWORD_SHIFT(  0x0 ,  9) | // [ 9]  Source Address Filter Enable
	       DWORD_SHIFT(  0x0 ,  8) | // [ 8]  SA Inverse Filtering
	       DWORD_SHIFT(  0x3 ,  6) | // [7:6] Pass Control Frames ( Forwards control frames that pass address filter)
	       DWORD_SHIFT(  0x0 ,  5) | // [ 5] Disable Broadcast Frames
	       DWORD_SHIFT(  0x0 ,  4) | // [ 4] Pass All Multicast
	       DWORD_SHIFT(  0x0 ,  3) | // [ 3] DA Inverse Filtering
                         0x0;         // [ 0] Promiscuous Mode
     
	SetDword(EMAC_MACR1_FRAME_FILTER_REG, temp);
	 
	SetDword(EMAC_MACR16_MAC_ADDR0_HIGH_REG, DWORD_SHIFT(mac_address[5], 8)| mac_address[4]);
	
	SetDword(EMAC_MACR17_MAC_ADDR0_LOW_REG, DWORD_SHIFT(mac_address[3],24) | 
                                            DWORD_SHIFT(mac_address[2],16) | 
	                                        DWORD_SHIFT(mac_address[1], 8) | mac_address[0]);
      
	/* e) configure and enable Tx/Rx operating modes
	 *    MAC Register0
	 *    (PS and DM bits are set based on the auto-negotion result read from the PHY)
	 */
	if (sp_mode == s10_full) {
		eth_sp = 0;
		duplex = 1;
	}else if (sp_mode == s10_half) {
		eth_sp = 0;
		duplex = 0;
	}else if (sp_mode == s100_full) {
		eth_sp = 1;
		duplex = 1;
	}else if (sp_mode == s100_half) {
		eth_sp = 1;
		duplex = 0;
	}
		 
	temp =	DWORD_SHIFT( 0x0 , 31) | // [31]	Transmit Configuration in RGMII/SGMII
			DWORD_SHIFT( 0x1 , 23) | // [23]	Watchdog Disable
			DWORD_SHIFT( 0x0 , 22) | // [22]	Jabber Disable
			DWORD_SHIFT( 0x0 , 21) | // [21]	Frame Burst Enable
			DWORD_SHIFT( 0x0 , 20) | // [20]	Jombo Frame Enable
			DWORD_SHIFT( 0x0 , 17) | // [19:17] Inter Frame Gap ( 96 bit time)
			DWORD_SHIFT( 0x0 , 16) | // [16]	Disable Carrier Sense During Transmission
			DWORD_SHIFT( 0x1 , 15) | // [15]	Port Select ( 10/100Mbps)
			DWORD_SHIFT( eth_sp , 14) | // [14]	Speed (100Mbps)
		    DWORD_SHIFT( 0x1 , 13) | // [13]	Disable Receive Own
		    DWORD_SHIFT( 0x0 , 12) | // [12]	Loop Back Mode
			DWORD_SHIFT( duplex , 11) | // [11]	Duplex Mode
			DWORD_SHIFT( 0x1 , 10) | // [10]	Checksum offload
			DWORD_SHIFT( 0x0 ,  9) | // [ 9]	Disable Retry
			DWORD_SHIFT( 0x1 ,  8) | // [ 8]	Link Up/Down
			DWORD_SHIFT( 0x1 ,  7) | // [ 7]	Automatic Pad/CRC Stripping
			DWORD_SHIFT( 0x0 ,  5) | // [6:5]	Back-Off Limit
			DWORD_SHIFT( 0x0 ,  4) | // [ 4]	Deferral Check
			DWORD_SHIFT( 0x1 ,  3) | // [ 3]	Transmitter Enable
			DWORD_SHIFT( 0x1 ,  2) ; // [ 2]	Receiver Enable

	SetDword(EMAC_MACR0_CONFIG_REG, temp);
    
	/* f) Set The Own bit to the Rx Descriptors
	 *
	 *
	 */
     
	emac_rx_des_prepare();
     
	/* g) Start Tx/Rx 
	 *    Operation Mode
	 *    DMA Register6, bits 13 and 1
	 *    Note: if bit ST (13) = 1 then EthMAC polls the Tx descriptor
	 */
    
	temp = DWORD_SHIFT( 0x0 , 26) | // [26]	Disable Dropping of TCP/IP checksum Error Frames 
	       DWORD_SHIFT( 0x0 , 25) | // [25]	Receive Store and Forward 
	       DWORD_SHIFT( 0x0 , 24) | // [24]	Disable Flushing of Received Frames
	       DWORD_SHIFT( 0x0 , 21) | // [21]	Transmit Store and Forward
	       DWORD_SHIFT( 0x0 , 20) | // [20]	Flush Transmit FIFO
	       DWORD_SHIFT( 0x0 , 14) | // [16:14] Transmit Threshold Control (64 bytes)
	       DWORD_SHIFT( 0x1 , 13) | // [13]	Start/Stop Transmission Command
	       DWORD_SHIFT( 0x0 ,  7) | // [ 7]	Forward Error Frames
	       DWORD_SHIFT( 0x0 ,  6) | // [ 6]	Forward Undersized Good Frames
	       DWORD_SHIFT( 0x1 ,  3) | // [4:3]	Receive Threshold Control (32 bytes)
	       DWORD_SHIFT( 0x0 ,  2) | // [ 2]	Operate on Second Frame
	       DWORD_SHIFT( 0x1 ,  1) ; // [ 1]	Start/Stop Receive
   
	SetDword(EMAC_DMAR6_OPERATION_MODE_REG, temp);
}

/*----------------------------------------------------------------------------*/
/*                        Emac Soft Reset                                     */
/*----------------------------------------------------------------------------*/

void emac_soft_rst(void)
{
	// Set Software reset bit. The new PHY configuration will be read.
	SetDword (EMAC_DMAR0_BUS_MODE_REG, 0x1); 

	// Wait End of Reset
	while (GetDword (EMAC_DMAR0_BUS_MODE_REG) & 0x1); 
}

/*----------------------------------------------------------------------------*/
/*                  The Emac start using the tx descriptors                   */
/*----------------------------------------------------------------------------*/

void emac_read_tx_des(void)
{
    SetDword(EMAC_DMAR1_TX_POLL_DEMAND_REG, 0x0);
}

/*----------------------------------------------------------------------------*/
/*                  The Emac start using the rx descriptors                   */
/*----------------------------------------------------------------------------*/

void emac_read_rx_des(void)
{
    SetDword(EMAC_DMAR2_RX_POLL_DEMAND_REG, 0x0);
}


/*----------------------------------------------------------------------------*/
/*                        Wait End of Frame Reception                         */
/*----------------------------------------------------------------------------*/

void emac_wait_frame_reception(void) {
    // Polling
    while (!(GetDword (EMAC_DMAR5_STATUS_REG) & (DWORD_SHIFT(0x1,6))));

    // Clear Receive Interrupt
    SetDword (EMAC_DMAR5_STATUS_REG, DWORD_SHIFT(0x1,6));
}


/*----------------------------------------------------------------------------*/
/*                        Wait End of Frame Transmission                       */
/*----------------------------------------------------------------------------*/

void emac_wait_frame_transmission(void) {
    // Polling
    while (!(GetDword (EMAC_DMAR5_STATUS_REG) & 0x1));

    // Clear Receive Interrupt
    SetDword (EMAC_DMAR5_STATUS_REG, 0x1);
}

    
/*-----------------------------------------------------------------------------------------*/
/* Reads the Length of the received frame and checks if the descriptor structure is valid. */
/*-----------------------------------------------------------------------------------------*/
long int emac_get_rx_len(void) 
{
	unsigned char i = 0;
	unsigned long int frame_buf_pos = 0;
	unsigned long int frame_len = 0;
	long int des0;
    
	while (i<emacDev.num_of_rx_des) {
	
		des0 = emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES0;

		// Check Own bit
		if (des0 & DWORD_SHIFT(0x1,31))
			return (-1);
    
		// Check First Descriptor bit
		if (i==0 && !(des0 & DWORD_SHIFT(0x1,9)))
			return (-1);
	
		// Check Last Descriptor bit
		if (des0 & DWORD_SHIFT(0x1,8)) {
		
			frame_len = (des0 >> 16) & 0x3fff;
			
			// Check Error Summary
			if (des0  & DWORD_SHIFT(0x1,15)) {
				emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES0 = 0x80000000;

				printf("Eth Rx Error\n");

				return(-1);
			}
			
			if ( (frame_len < frame_buf_pos) || 
			    ((frame_len - frame_buf_pos) > 2*emacDev.rx_buf_size))
				return (-1);

				
			return (frame_len);
		} else
			frame_buf_pos += 2*emacDev.rx_buf_size;

		i++;
	}
    return (-1);
}

/*----------------------------------------------------------------------------*/
/*  Copy the received data to a new buffer and release the Rx Descriptors     */
/*----------------------------------------------------------------------------*/
unsigned long int emac_get_rx_data(unsigned long int *frame_buf) 
{
	unsigned char i = 0;
    
	long int des0;
	unsigned long int frame_buf_pos = 0;
	unsigned long int frame_len = 0;
	unsigned int dma_len =0;
    
	while (i<emacDev.num_of_rx_des) {
    
		des0 = emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES0;
	
	
		// Check Last Descriptor bit
		if (des0 & DWORD_SHIFT(0x1,8)) {
		
			frame_len = (des0 >> 16) & 0x3fff;
			
			// Copy the data from buffer 1 to the frame buffer using the dma engine.      
			if ((frame_len - frame_buf_pos) >= emacDev.rx_buf_size)
				dma_len = emacDev.rx_buf_size;
			else
				dma_len = (frame_len - frame_buf_pos);	
			
			        	
			dma_memcopy ( emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES2,
			             (unsigned long int) frame_buf + frame_buf_pos,
			             NUM_OF_DWORDS(dma_len), 2, 1, EMAC_USE_DMA_CHANNEL);
        	
			dma_wait(EMAC_USE_DMA_CHANNEL);
			
			frame_buf_pos += dma_len;
			
			if (frame_len > frame_buf_pos) {
				// Copy the data from buffer 2 to the frame buffer using the dma engine. 
        		
				dma_len = (frame_len - frame_buf_pos);
        		
				dma_memcopy ( emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES3,
				             (unsigned long int) frame_buf + frame_buf_pos,
				             NUM_OF_DWORDS(dma_len), 2, 1, EMAC_USE_DMA_CHANNEL);
        	
				dma_wait(EMAC_USE_DMA_CHANNEL);
        	
				frame_buf_pos += dma_len;
			}

		// Set Own bit
		emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES0 = 0x80000000;

			emacDev.rx_ptr = (emacDev.rx_ptr + i + 1) % emacDev.num_of_rx_des;
	
			return (frame_len);
		} else {
			// Copy the data from buffer 1 to the frame buffer using the dma engine. 
        	
			dma_len = emacDev.rx_buf_size;
        	
			dma_memcopy ( emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES2,
			             (unsigned long int) frame_buf + frame_buf_pos,
			             NUM_OF_DWORDS(dma_len), 2, 1, EMAC_USE_DMA_CHANNEL);
        	
			dma_wait(EMAC_USE_DMA_CHANNEL);
		
			frame_buf_pos += dma_len;
        	
			// Copy the data from buffer 2 to the frame buffer using the dma engine. 
			dma_memcopy ( emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES3,
			              (unsigned long int) frame_buf + frame_buf_pos,
			              NUM_OF_DWORDS(dma_len), 2, 1, EMAC_USE_DMA_CHANNEL);
        	
			dma_wait(EMAC_USE_DMA_CHANNEL);
        	
			frame_buf_pos += dma_len;
		
			// Set Own bit
			emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES0 = 0x80000000;

        	
			i++;
		}
	}

	return (frame_buf_pos);

}

/*----------------------------------------------------------------------------*/
/*            Calculates the number of the free tx descriptors                */
/*----------------------------------------------------------------------------*/
unsigned char emac_tx_des_calc (void) {
	unsigned char i; 
	
	for (i=0; i<emacDev.num_of_tx_des; i++)
		if (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES0 & 0x80000000)
			return (i);
	
	return (emacDev.num_of_tx_des);
}

/*----------------------------------------------------------------------------*/
/*            Calculates the total size of the free tx buffers                */
/*----------------------------------------------------------------------------*/
unsigned long int emac_tx_buf_calc (void) {
	return (2* emac_tx_des_calc() * emacDev.tx_buf_size);
}



/*----------------------------------------------------------------------------*/
/*            Write a Frame to the Tx Descriptors                             */
/*----------------------------------------------------------------------------*/
unsigned long int emac_send_tx_data(unsigned long int frame_size, unsigned long int *frame_buf)
{
	unsigned char i=0,k;
	unsigned long int frame_buf_pos = 0;
	unsigned int dma_len =0;

	while (i<emacDev.num_of_tx_des) {
		
		if (i==0)
			// This is the first Descriptor. Sets the First Segment bit and clears the Last Segment bit
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 | DWORD_SHIFT(0x1,29) ) & (~DWORD_SHIFT(0x1,30));
		else
			// Clear the First Segment bit and the Last Segment bit
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & ~(DWORD_SHIFT(0x1,29) | DWORD_SHIFT(0x1,30));		
		
		// Copy the data from frame buffer to the buffer 1 using the dma engine.
		if ((frame_size - frame_buf_pos) >= emacDev.tx_buf_size)
			dma_len = emacDev.tx_buf_size;
		else
			dma_len = frame_size - frame_buf_pos;
       	
		dma_memcopy ((unsigned long int) frame_buf + frame_buf_pos,
		              emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES2,
		              NUM_OF_DWORDS(dma_len), 2, 1, EMAC_USE_DMA_CHANNEL);
        	
		dma_wait(EMAC_USE_DMA_CHANNEL);

		// Write the size of buffer 1
		emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & (~0x7ff)) | dma_len;
			
		frame_buf_pos += dma_len;
	
		// Copy the data from frame buffer to the buffer 2 using the dma engine if is necessary.
		if (frame_size > frame_buf_pos) {
       			if ((frame_size -  frame_buf_pos) >= emacDev.tx_buf_size)
				dma_len = emacDev.tx_buf_size;
			else
				dma_len = frame_size - frame_buf_pos;
			
			dma_memcopy ((unsigned long int) frame_buf + frame_buf_pos,
			              emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES3,
			              NUM_OF_DWORDS(dma_len), 2, 1, EMAC_USE_DMA_CHANNEL);
        	
			dma_wait(EMAC_USE_DMA_CHANNEL);

			// Write the size of buffer 2
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & (~0x3ff800)) | DWORD_SHIFT (dma_len, 11);
				
			frame_buf_pos += dma_len;
		} else 
			// Clear the size of buffer 2
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & (~0x3ff800);
		
		if (frame_size == frame_buf_pos) {
			// Set the Last Segment bit to the Last descriptor
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 | DWORD_SHIFT(0x1,30);
	
			// -------------------- Set Own Bit ------------------------------------//
			for (k=0; k<=i; k++) {
				emacDev.tx_descriptor[(emacDev.tx_ptr + k) % emacDev.num_of_tx_des].DES0 = 0x80000000;
			}
			
			// -------------------- Update TxPtr Value -----------------------------//
			emacDev.tx_ptr = (emacDev.tx_ptr + i + 1) % emacDev.num_of_tx_des;	
			
			return (frame_buf_pos);
		}
		i++;
	}
	
	return (frame_buf_pos);
	
}


void emac_rxevent(void) {
	long int len;
	
	while ( (len = emac_get_rx_len()) > 0) {
		
		if (len>=RX_FRAME_SIZE) {
			printf("EMAC452: packet too big\n");
			return;
		}
		if (RxFrames >= MAX_RX_FRAMES) {
			return;
		}
		RxFrameLen[RxFrames] = (int ) emac_get_rx_data((unsigned long int *) &RxFrameBuf[RxFrames * RX_FRAME_SIZE]);
		RxFrames++;		
	}
}

void emac_rxoverflow(void)
{
	printf("losing packets in rx\n");
	
	// Take all the good frames
	emac_rxevent();
	
	// Clear all the rx descriptors 
	emac_rx_des_prepare();
	
	// Force the emac to read again the rx descriptors
	emac_read_rx_des();
	
}


void emac_txevent(void) {
	TxEnd = 1;
}

void emac_poll(void) {
	long int status;
	
	while ((status = (GetDword (EMAC_DMAR5_STATUS_REG) & 0x1ffff)) !=0) {

		// Receive Overflow
		if (status & DWORD_SHIFT(0x1,4)) {
			emac_rxoverflow();
			SetDword (EMAC_DMAR5_STATUS_REG, DWORD_SHIFT(0x1,4));	
		}

		// End of frame transmission
		if (status & 0x1) {
			emac_txevent();
			SetDword (EMAC_DMAR5_STATUS_REG,0x1);
		}
	
		// End of frame reception
		if (status & DWORD_SHIFT(0x1,6)) {
			SetDword (EMAC_DMAR5_STATUS_REG, DWORD_SHIFT(0x1,6));
			emac_rxevent();
		}

		// Clear all other interrupts
		SetDword (EMAC_DMAR5_STATUS_REG, DWORD_SHIFT(0x1,16) |
						 DWORD_SHIFT(0x1,15) |
						 DWORD_SHIFT(0x1,14) | 
		                                 DWORD_SHIFT(0x1,13) |
						 DWORD_SHIFT(0x1,10) |
						 DWORD_SHIFT(0x1,9)  |
						 DWORD_SHIFT(0x1,8)  |
						 DWORD_SHIFT(0x1,7)  |
						 DWORD_SHIFT(0x1,5)  |
						 DWORD_SHIFT(0x1,3)  |
						 DWORD_SHIFT(0x1,2)  |
						 DWORD_SHIFT(0x1,1)   );
	}
	
}

int emac_init_speed(unsigned short speed)
{
	emac_clk_disable();

	if (speed == 10)
		emac_10mbps();
	else
		emac_100mbps();
	// Enable EMAC clocks
	emac_clk_enable();
	// Software reset to read the PHY configuration
	emac_soft_rst();

	return 0;
}


int eth_init(bd_t *bd)
{
	struct s_descriptor *tx_descriptor;
	struct s_descriptor *rx_descriptor;
	unsigned long int *tx_buf_base;
	unsigned long int *rx_buf_base;
	speed_mode ret = s100_full;


	// allocate memory for the rx and tx descriptors
	// rx_descriptor : sizeof (s_descriptor) * TX_DESCRIPTORS_NUM
	// tx_descriptor : sizeof (s_descriptor) * RX_DESCRIPTORS_NUM
	// WARNING!!! The descriptors (rx and tx) must be aligned
	// as double words (32 bits)

	tx_descriptor = (struct s_descriptor*)TX_DES_BASE;//(struct s_descriptor *) malloc ( sizeof (struct s_descriptor) * TX_DESCRIPTORS_NUM); // TX_DES_BASE
	rx_descriptor = (struct s_descriptor*)RX_DES_BASE;//(struct s_descriptor *) malloc ( sizeof (struct s_descriptor) * RX_DESCRIPTORS_NUM); // RX_DES_BASE

//	tx_descriptor = (struct s_descriptor *) malloc ( sizeof (struct s_descriptor) * TX_DESCRIPTORS_NUM); // TX_DES_BASE
//	rx_descriptor = (struct s_descriptor *) malloc ( sizeof (struct s_descriptor) * RX_DESCRIPTORS_NUM); // RX_DES_BASE


	// allocate memory for the rx and tx buffers
	// tx_buf_base : 2*TX_DESCRIPTORS_NUM*(TX_BUF_SIZE+TX_BUF_GAP)
	// rx_buf_base : 2*RX_DESCRIPTORS_NUM*(RX_BUF_SIZE+RX_BUF_GAP)
	// WARNING!!! must be aligned as double words (32 bits)

	tx_buf_base = (unsigned long int*)TX_BUF_BASE;//(unsigned long int *) malloc ( 2*TX_DESCRIPTORS_NUM*(TX_BUF_SIZE+TX_BUF_GAP)); //TX_BUF_BASE
	rx_buf_base = (unsigned long int*)RX_BUF_BASE;//(unsigned long int *) malloc ( 2*RX_DESCRIPTORS_NUM*(RX_BUF_SIZE+RX_BUF_GAP)); //RX_BUF_BASE

//	tx_buf_base = (unsigned long int *) malloc ( 2*TX_DESCRIPTORS_NUM*(TX_BUF_SIZE+TX_BUF_GAP)); //TX_BUF_BASE
//	rx_buf_base = (unsigned long int *) malloc ( 2*RX_DESCRIPTORS_NUM*(RX_BUF_SIZE+RX_BUF_GAP)); //RX_BUF_BASE
	
	// allocate memory for the rx frames
	RxFrameBuf = (unsigned char*)RX_FRAME_BUFF;//(unsigned long int *) malloc (MAX_RX_FRAMES * RX_FRAME_SIZE);
//	RxFrameBuf = (unsigned long int *) malloc (MAX_RX_FRAMES * RX_FRAME_SIZE);

		
	// Init the descriptors structures
	emac_init_tx_desc (tx_descriptor, TX_DESCRIPTORS_NUM, tx_buf_base, TX_BUF_SIZE, TX_BUF_GAP);
	emac_init_rx_desc (rx_descriptor, RX_DESCRIPTORS_NUM, rx_buf_base, RX_BUF_SIZE, RX_BUF_GAP);

	// Configure the pads for RMII interface
	emac_rmii_pads();
	
	// Set 100mbps operation

#ifdef CONFIG_IPBS_CVM_10MBPS
	emac_10mbps();																			//------ PGR testing ULPBS
#else
	emac_100mbps();																			//------ PGR testing ULPBS
#endif

	// Configure the clock for RMII interface and external clock
	emac_rmii_clk_ext();
	
	// Configure the pads for MDC interface
	emac_md_pads();
	
	// Enable EMAC clocks
	emac_clk_enable();

	// Software reset to read the PHY configuration
	emac_soft_rst();
	
	// Configure the emac
	emac_cfg(bd->bi_enetaddr, s100_full);

	ret = KSZ8041RNL_init();																	// ----- PGR  testing for ULPBS

	emac_cfg(bd->bi_enetaddr, ret);

	return 0;
}

void eth_halt()
{
	unsigned temp;
	temp = GetDword (EMAC_DMAR6_OPERATION_MODE_REG);
	SetDword(EMAC_DMAR6_OPERATION_MODE_REG,temp & ~0x2002);

	udelay(2000);
}

void ethisloopback(){
	isloopbackok=2;
}

int eth_rx()
{
	int j, tmo;
    int ok;
	PRINTK("### eth_rx\n");																

	tmo = get_timer (0) + TOUT * CFG_HZ;
	PRINTK("RxFrameBuf %d %d %d \n",RxFrameBuf[0],RxFrameBuf[1],RxFrameBuf[2]);
	while(1) {
		if (isloopbackok) {
			emac_poll();
		}else{
			return 0;
		}

		if (RxFrames > 0) {
			PRINTK("RxFrames %02x \n",RxFrames);
			for(j=0; j<RxFrames; j++) {
				NetReceive(&RxFrameBuf[j*RX_FRAME_SIZE], RxFrameLen[j]);
			}

			if(isloopbackok==2){
				PRINTK("RxFrameBuf %d %d %d %d %d %d %d %d %d \n",RxFrameBuf[0],RxFrameBuf[1],RxFrameBuf[2],
						RxFrameBuf[3],RxFrameBuf[4],RxFrameBuf[5],RxFrameBuf[6],RxFrameBuf[7],RxFrameBuf[8]);

				ok=1;
				for(j=0; j<6; j++) {
					PRINTK("RxFrameBuf[j] %d %d \n",RxFrameBuf[j],golden_eth_data[j]);
					if(RxFrameBuf[j]!=golden_eth_data[j]) ok=0;
				}
				if (ok){
					printf("ETH LOOPBACK:PASS\n");
					ethloopresult=1;
				}else{
					printf("ETH LOOPBACK:FAIL COMP\n");
					ethloopresult=0;
				}
				isloopbackok=0;
				NetLoopbackOK();
			}

			RxFrames = 0;


			return 1;
		}
		if (get_timer (0) >= tmo) {
			printf("timeout during rx\n");
			return 0;
		}
	}
	return 0;
}

int eth_send(volatile void *packet, int length)
{
	int tmo;

//	SetPort(P0_04_MODE_REG, PORT_OUTPUT, PID_port); 							//---- PGR testULPBS
		
	PRINTK("### eth_send\n");											
	
//	SetWord(P0_SET_DATA_REG, 0x10); 											//---- PGR testULPBS
	  
    
	TxEnd = -1;

	emac_send_tx_data(length, (unsigned long int *) packet);
	
	emac_read_tx_des();
	
	tmo = get_timer (0) + TOUT * CFG_HZ;
	while(1) {
		emac_poll();
		if (TxEnd != -1) {
			PRINTK("Packet sucesfully sent\n");
//			SetWord(P0_RESET_DATA_REG, 0x10); 											//---- PGR testULPBS
			return 0;
		}
		
		if (get_timer (0) >= tmo) {
			printf("transmission error (timoeut)\n");
			return 0;
		}

	}
	return 0;
}
//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:49.852220480
//@@ -7,12 +7,16 @@
// #include "asm.h"
// 
// 
//-#if DEBUG & 1												
//+#if DEBUG & 0
// #define PRINTK(args...) printf(args)
// #else
//+//#define PRINTK(args...) printf(args)
// #define PRINTK(args...)
// #endif
//-
//+char isloopbackok=1;
//+char ethloopresult =0;
//+extern char ethloopresult;
//+unsigned char golden_eth_data[6]={255,255,255,255,255,255};
// /* timeout for tx/rx in s */
// #define TOUT 5
// //#define CONFIG_IPBS_CVM_10MBPS
//@@ -331,16 +335,26 @@
// 
// 	temp = emac_md_read(ETH_PHY_ADDR, 0x1F, EMAC_MDC_DIV_42);
// 
//-	while (((temp&0x1000)==0) && (cnt < 10000)) {
//+	i= (isloopbackok==2)?40:150;
//+
//+	if (isloopbackok==2) DoFlashCopy();
//+
//+	while (((temp&0x1000)==0) && (cnt < i)) {
// 		temp = emac_md_read(ETH_PHY_ADDR, 0x1F, EMAC_MDC_DIV_42);
// 		//printf("register 0x1F=[%x] cnt=%d tmp1=[%d] \n", temp, cnt, (temp&0x1000)==0);
// 		cnt++;
// 		udelay(50000);
// 	}
//+	if ((cnt==i)&&(isloopbackok==2)){
//+		printf("ETH LOOPBACK:FAIL TIME\n");
//+		ethloopresult=0;
//+		NetLoopbackOK();
//+		isloopbackok=0;
//+	}
// #else
// 	
// 	SetPort(P1_13_MODE_REG, PORT_PULL_UP, PID_port);	// ensure ETH_RSTn = High 
//-	
//+
// 	emac_md_write (1, 0, EMAC_MDC_DIV_42, 0x1000);		// auto-neg enable
// 
// 	temp = emac_md_read(1, 0x1F, EMAC_MDC_DIV_42);
//@@ -1056,8 +1070,8 @@
// 	// WARNING!!! The descriptors (rx and tx) must be aligned
// 	// as double words (32 bits)
// 
//-	tx_descriptor = TX_DES_BASE;//(struct s_descriptor *) malloc ( sizeof (struct s_descriptor) * TX_DESCRIPTORS_NUM); // TX_DES_BASE
//-	rx_descriptor = RX_DES_BASE;//(struct s_descriptor *) malloc ( sizeof (struct s_descriptor) * RX_DESCRIPTORS_NUM); // RX_DES_BASE
//+	tx_descriptor = (struct s_descriptor*)TX_DES_BASE;//(struct s_descriptor *) malloc ( sizeof (struct s_descriptor) * TX_DESCRIPTORS_NUM); // TX_DES_BASE
//+	rx_descriptor = (struct s_descriptor*)RX_DES_BASE;//(struct s_descriptor *) malloc ( sizeof (struct s_descriptor) * RX_DESCRIPTORS_NUM); // RX_DES_BASE
// 
// //	tx_descriptor = (struct s_descriptor *) malloc ( sizeof (struct s_descriptor) * TX_DESCRIPTORS_NUM); // TX_DES_BASE
// //	rx_descriptor = (struct s_descriptor *) malloc ( sizeof (struct s_descriptor) * RX_DESCRIPTORS_NUM); // RX_DES_BASE
//@@ -1068,14 +1082,14 @@
// 	// rx_buf_base : 2*RX_DESCRIPTORS_NUM*(RX_BUF_SIZE+RX_BUF_GAP)
// 	// WARNING!!! must be aligned as double words (32 bits)
// 
//-	tx_buf_base = TX_BUF_BASE;//(unsigned long int *) malloc ( 2*TX_DESCRIPTORS_NUM*(TX_BUF_SIZE+TX_BUF_GAP)); //TX_BUF_BASE
//-	rx_buf_base = RX_BUF_BASE;//(unsigned long int *) malloc ( 2*RX_DESCRIPTORS_NUM*(RX_BUF_SIZE+RX_BUF_GAP)); //RX_BUF_BASE
//+	tx_buf_base = (unsigned long int*)TX_BUF_BASE;//(unsigned long int *) malloc ( 2*TX_DESCRIPTORS_NUM*(TX_BUF_SIZE+TX_BUF_GAP)); //TX_BUF_BASE
//+	rx_buf_base = (unsigned long int*)RX_BUF_BASE;//(unsigned long int *) malloc ( 2*RX_DESCRIPTORS_NUM*(RX_BUF_SIZE+RX_BUF_GAP)); //RX_BUF_BASE
// 
// //	tx_buf_base = (unsigned long int *) malloc ( 2*TX_DESCRIPTORS_NUM*(TX_BUF_SIZE+TX_BUF_GAP)); //TX_BUF_BASE
// //	rx_buf_base = (unsigned long int *) malloc ( 2*RX_DESCRIPTORS_NUM*(RX_BUF_SIZE+RX_BUF_GAP)); //RX_BUF_BASE
// 	
// 	// allocate memory for the rx frames
//-	RxFrameBuf = RX_FRAME_BUFF;//(unsigned long int *) malloc (MAX_RX_FRAMES * RX_FRAME_SIZE);
//+	RxFrameBuf = (unsigned char*)RX_FRAME_BUFF;//(unsigned long int *) malloc (MAX_RX_FRAMES * RX_FRAME_SIZE);
// //	RxFrameBuf = (unsigned long int *) malloc (MAX_RX_FRAMES * RX_FRAME_SIZE);
// 
// 		
//@@ -1125,21 +1139,54 @@
// 	udelay(2000);
// }
// 
//+void ethisloopback(){
//+	isloopbackok=2;
//+}
//+
// int eth_rx()
// {
// 	int j, tmo;
//-
//+    int ok;
// 	PRINTK("### eth_rx\n");																
// 
// 	tmo = get_timer (0) + TOUT * CFG_HZ;
//+	PRINTK("RxFrameBuf %d %d %d \n",RxFrameBuf[0],RxFrameBuf[1],RxFrameBuf[2]);
// 	while(1) {
//-		emac_poll();
//+		if (isloopbackok) {
//+			emac_poll();
//+		}else{
//+			return 0;
//+		}
//+
// 		if (RxFrames > 0) {
// 			PRINTK("RxFrames %02x \n",RxFrames);
// 			for(j=0; j<RxFrames; j++) {
// 				NetReceive(&RxFrameBuf[j*RX_FRAME_SIZE], RxFrameLen[j]);
// 			}
//+
//+			if(isloopbackok==2){
//+				PRINTK("RxFrameBuf %d %d %d %d %d %d %d %d %d \n",RxFrameBuf[0],RxFrameBuf[1],RxFrameBuf[2],
//+						RxFrameBuf[3],RxFrameBuf[4],RxFrameBuf[5],RxFrameBuf[6],RxFrameBuf[7],RxFrameBuf[8]);
//+
//+				ok=1;
//+				for(j=0; j<6; j++) {
//+					PRINTK("RxFrameBuf[j] %d %d \n",RxFrameBuf[j],golden_eth_data[j]);
//+					if(RxFrameBuf[j]!=golden_eth_data[j]) ok=0;
//+				}
//+				if (ok){
//+					printf("ETH LOOPBACK:PASS\n");
//+					ethloopresult=1;
//+				}else{
//+					printf("ETH LOOPBACK:FAIL COMP\n");
//+					ethloopresult=0;
//+				}
//+				isloopbackok=0;
//+				NetLoopbackOK();
//+			}
//+
// 			RxFrames = 0;
//+
//+
// 			return 1;
// 		}
// 		if (get_timer (0) >= tmo) {
