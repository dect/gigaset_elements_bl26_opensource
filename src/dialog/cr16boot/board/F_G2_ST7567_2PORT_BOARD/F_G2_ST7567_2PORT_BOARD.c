/*
 * (C) Copyright 2002
 * Kyle Harris, Nexus Technologies, Inc. kharris@nexus-tech.net
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */


#include "armboot.h"
#include "sitel_io.h"
#include "spi_polled.h"
#include "qspic.h"
/* ------------------------------------------------------------------------- */



int 
/**********************************************************/
board_post_init(bd_t *bd)
/**********************************************************/
{
   return 0;
}

int 
/**********************************************************/
board_init(bd_t *bd)
/**********************************************************/
{
	unsigned i,j;
	
	/* arch number of Sitel SC14450 DK */
	bd->bi_arch_number = 452;

	/* vm must change */ 
	/* adress of boot parameters */
	bd->bi_boot_params = CFG_CONFIG_PARAMETERS_ADDR;//0xa0000100;

	/*   Config PPA for UART. */
	SetPort(P0_00_MODE_REG, PORT_OUTPUT,    PID_UTX);
	SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_URX);


	init_SPI2(CLK_5184, MODE_3, MODE_8BITS);

	SetWord(EBI_SMTMGR_SET1_REG,0x0d44);
	SetWord(EBI_SMTMGR_SET2_REG, 0xe278);	
// enable timer1 clock
	if (GetWord(CLK_GLOBAL_REG) & SW_XTAL_PLL1_RATE)
		SetBits(CLK_GPIO4_REG, SW_TMR1_DIV, 144);
	else
		SetBits(CLK_GPIO4_REG, SW_TMR1_DIV, 72);
	
	SetBits(CLK_GPIO4_REG, SW_TMR1_EN, 1);


#ifdef CONFIG_F_G2_ST7567_2PORT_BOARD



		SetWord(BAT_CTRL_REG,0x412e);
//	SetWord(BANDGAP_REG,0x0d);



#else
#error "board revision is undefined"
#endif






	return 1;

}

int 
/**********************************************************/
dram_init(bd_t *bd)
/**********************************************************/
{
  	bd->bi_dram[0].start = PHYS_SDRAM_1;
	bd->bi_dram[0].size  = PHYS_SDRAM_1_SIZE;
	
	
	bd->bi_dram[1].start = PHYS_SDRAM_2;
	bd->bi_dram[1].size = PHYS_SDRAM_2_SIZE;
	
	return PHYS_SDRAM_1_SIZE+PHYS_SDRAM_2_SIZE;
 
}


