// File created by Gigaset Elements GmbH
// All rights reserved.

#include "armboot.h"
//#include "sitel_io.h"
//#include "spi_polled.h"
//#include "qspic.h"
#include "net.h"



/**
 * Turn on Giga94 mode then is it necessary
 */
void test_giga94_status(bd_t *bd)
{
    // P2[5] is connected to GND
    SetPort(P2_05_MODE_REG, PORT_PULL_UP, PID_port);
    if( !(GetWord(P2_DATA_REG) & GPIO_5) )
    {
        // set flag that Giga94 is running
        bd->bi_ext.ucGiga94On = 1;


        // it is possible to unlock system when Giga94 is on - when paging key (P2[9]) is connected to the GND
        SetPort(P2_09_MODE_REG, PORT_PULL_UP, PID_port);
        if( !(GetWord(P2_DATA_REG) & GPIO_9) )
            bd->bi_ext.ucSystemLocked = 0;
        else
            bd->bi_ext.ucSystemLocked = 1;

    }
}





