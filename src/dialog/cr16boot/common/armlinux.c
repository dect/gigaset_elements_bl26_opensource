/*
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * Copyright (C) 2001  Erik Mouw (J.A.K.Mouw@its.tudelft.nl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "armboot.h"
#include "command.h"
#include "cmd_boot.h"
#include "image.h"
#include "malloc.h"
#include "zlib.h"

#include <asm/setup.h>

#ifdef CONFIG_CHAR_LCD
#include "char_lcd.h"
#endif

#ifdef CONFIG_GRAPH_LCD
#include "graph_lcd.h"
#endif

#ifdef CONFIG_NT75451_LCD
#include "nt75451.h"
#endif

#ifdef CONFIG_ST7567_LCD
#include "st7567.h"
#endif
#ifdef CONFIG_S6B33_LCD
#include "s6b33.h"
#endif

#define tag_size(type)  ((sizeof(struct tag_header) + sizeof(struct type)) >> 2)
#define tag_next(t)     ((struct tag *)((u32 *)(t) + (t)->hdr.size))

static void setup_start_tag(bd_t *bd);
static void setup_memory_tags(bd_t *bd);
static void setup_commandline_tag(bd_t *bd, char *commandline);
static void setup_commandline_arguments(bd_t *bd);
#if 1
static void setup_ramdisk_tag(bd_t *bd);
#endif
static void setup_initrd_tag(bd_t *bd, ulong initrd_start, ulong initrd_end);
static void setup_end_tag(bd_t *bd);

extern image_header_t header;           /* from cmd_bootm.c */

#undef DEBUG

static struct tag *params;

void boot_linux(cmd_tbl_t *cmdtp,
		bd_t *bd, int flag,
		int argc, char *argv[],
		ulong addr,
		ulong *len_ptr,
		int   verify)
{
    ulong len = 0, checksum;
    ulong initrd_start, initrd_end;
    ulong data;
    char *commandline = getenv(bd, "bootargs");
    void (*theKernel)(int zero, int arch);
    image_header_t *hdr = &header;

    /*
     * Check if there is an initrd image
     */
    if (argc >= 3) {
	addr = simple_strtoul(argv[2], NULL, 16);
	
	printf ("## Loading Ramdisk Image at %08lx ...\n", addr);
	
	/* Copy header so we can blank CRC field for re-calculation */
	memcpy (&header, (char *)addr, sizeof(image_header_t));
	
	if (SWAP32(hdr->ih_magic) != IH_MAGIC) {
	    printf ("Bad Magic Number\n");
	    do_reset (cmdtp, bd, flag, argc, argv);
	}
	
	data = (ulong)&header;
	len  = sizeof(image_header_t);
	
	checksum = SWAP32(hdr->ih_hcrc);
	hdr->ih_hcrc = 0;
	
	if (crc32 (0, (char *)data, len) != checksum) {
	    printf ("Bad Header Checksum\n");
	    do_reset (cmdtp, bd, flag, argc, argv);
	}
	
	print_image_hdr (hdr);
	
	data = addr + sizeof(image_header_t);
	len  = SWAP32(hdr->ih_size);
	
	if (verify) {
	    ulong csum = 0;

	    printf ("   Verifying Checksum ... ");
	    csum = crc32 (0, (char *)data, len);
	    if (csum != SWAP32(hdr->ih_dcrc)) {
		printf ("Bad Data CRC\n");
		do_reset (cmdtp, bd, flag, argc, argv);
	    }
	    printf ("OK\n");
	}
	
	if ((hdr->ih_os   != IH_OS_LINUX)	||
	    (hdr->ih_arch != IH_CPU_ARM)	||
	    (hdr->ih_type != IH_TYPE_RAMDISK)	) {
	    printf ("No Linux ARM Ramdisk Image\n");
	    do_reset (cmdtp, bd, flag, argc, argv);
	}
	
	/*
	 * Now check if we have a multifile image
	 */
    } else if ((hdr->ih_type==IH_TYPE_MULTI) && (len_ptr[1])) {
	ulong tail    = SWAP32(len_ptr[0]) % 4;
	int i;
	
	/* skip kernel length and terminator */
	data = (ulong)(&len_ptr[2]);
	/* skip any additional image length fields */
	for (i=1; len_ptr[i]; ++i)
	  data += 4;
	/* add kernel length, and align */
	data += SWAP32(len_ptr[0]);
	if (tail) {
	    data += 4 - tail;
	}
	
	len   = SWAP32(len_ptr[1]);
	
    } else {
	/*
	 * no initrd image
	 */
	data = 0;
    }
    
#ifdef	DEBUG
    if (!data) {
	printf ("No initrd\n");
    }
#endif
    
    if (data) {
	initrd_start = data;
	initrd_end   = initrd_start + len;
	printf ("   Loading Ramdisk to %08lx, end %08lx ... ",
		initrd_start, initrd_end);
	memmove ((void *)initrd_start, (void *)data, len);
	printf ("OK\n");
    } else {
	initrd_start = 0;
	initrd_end = 0;
    }
    //vm
    theKernel = (void (*)(int, int))(SWAP32(hdr->ih_ep)>>1&0xffffff);
   
#ifdef DEBUG
    printf ("## Transferring control to Linux (at address %08lx) ...\n",
	    (ulong)theKernel);
#endif

	setup_commandline_arguments(bd);
#if 0
    setup_start_tag(bd);
 //   setup_memory_tags(bd);
    setup_commandline_tag(bd, commandline);
//    setup_initrd_tag(bd, initrd_start, initrd_end);
#if 0
    setup_ramdisk_tag(bd);
#endif
    setup_end_tag(bd);
#endif

    /* we assume that the kernel is in place */
    printf("\nStarting kernel ...\n\n");

#ifdef SC14452
	DISP_MESG(0," SC14452DK Boot ");
#else
	DISP_MESG(0," SC14450DK Boot ");
#endif
	DISP_MESG(1,"Starting kernel ");
    //mg copy configuration data at proper position
    //mg temp ....  
  
    //memcpy ((char *) 0x10ffc000, (char *)0x30000, 0x2f70);

    cleanup_before_linux(bd);

// vm for speed test 
//	SetPort(P2_10_MODE_REG, PORT_OUTPUT,  PID_port);        /* P2_10 is GPIO */
//	SetWord(P2_SET_DATA_REG,0x400);
//	SetWord(P2_RESET_DATA_REG,0x400);


    theKernel(0, bd->bi_arch_number);
}


static void setup_start_tag(bd_t *bd)
{
    params = (struct tag *)bd->bi_boot_params;
    
    params->hdr.tag = ATAG_CORE;
    params->hdr.size = tag_size(tag_core);
    
    params->u.core.flags = 0;
    params->u.core.pagesize = 0;
    params->u.core.rootdev = 0;

#if 1
    printf( "\nstart_tag @%p:\n", params ) ;
    printf( "params->hdr.tag = %ld\n"
            "params->hdr.size = %ld\n"
            "params->u.core.flags = 0x%x\n"
            "params->u.core.pagesize = %ld\n"
            "params->u.core.rootdev = %d\n", params->hdr.tag, 
	    params->hdr.size,
            params->u.core.flags, params->u.core.pagesize, 
	    params->u.core.rootdev ) ;
#endif

    params = tag_next(params);
}


static void setup_memory_tags(bd_t *bd)
{
    int i;
    
    for(i = 0; i < CONFIG_NR_DRAM_BANKS; i++) {
	params->hdr.tag = ATAG_MEM;
	params->hdr.size = tag_size(tag_mem32);
	
	params->u.mem.start = bd->bi_dram[i].start;
	params->u.mem.size = bd->bi_dram[i].size;

#if 1
	printf( "\nmemory tag #%d @%p\n", i, params ) ;
	printf( "params->hdr.tag = %d\n"
		"params->hdr.size = %d\n"
		"params->u.mem.start = 0x%08x\n"
		"params->u.mem.size = 0x%x\n", params->hdr.tag,
		params->hdr.size, params->u.mem.start, params->u.mem.size ) ;
#endif
	    
	params = tag_next(params);
    }
}


static void setup_commandline_tag(bd_t *bd, char *commandline)
{
    char *p;
    
    /* eat leading white space */
    for(p = commandline; *p == ' '; p++)
      ;
    
    /* skip non-existent command lines so the kernel will still
     * use its default command line.
     */
    if(*p == '\0')
      return;
    
    params->hdr.tag = ATAG_CMDLINE;
    params->hdr.size = (sizeof(struct tag_header) + strlen(p) + 1 + 4) >> 2;
    
    strcpy(params->u.cmdline.cmdline, p);

#if 1
    printf( "\ncommandline tag @%p\n", params ) ;
    printf( "params->hdr.tag = %d\n"
            "params->hdr.size = %d\n"
            "params->u.cmdline.cmdline = '%s'\n", params->hdr.tag,
            params->hdr.size,
            params->u.cmdline.cmdline ?
                     params->u.cmdline.cmdline[0] ?  params->u.cmdline.cmdline
                                                  :  "<empty>"
                                      :  "NULL" ) ;
#endif    
    params = tag_next(params);
}


static void setup_initrd_tag(bd_t *bd, ulong initrd_start, ulong initrd_end)
{
    /* an ATAG_INITRD node tells the kernel where the compressed
     * ramdisk can be found. ATAG_RDIMG is a better name, actually.
     */
#if 0
    params->hdr.tag = ATAG_INITRD;
#else
    /* use ATAG_INITRD2 instead of ATAG_INITRD...
     * ATAG_INITRD2 is supposed to pass the physical address, while
     * ATAG_INITRD is supposed to pass the virtual addres, which we don't
     * know
     */
    params->hdr.tag = ATAG_INITRD2 ;
#endif
    params->hdr.size = tag_size(tag_initrd);
    
    params->u.initrd.start = initrd_start;
    params->u.initrd.size = initrd_end - initrd_start;
   
#if 1
    printf( "\ninitrd tag @%p\n", params ) ;
    printf( "params->hdr.tag = %d\n"
            "params->hdr.size = %d\n"
            "params->u.initrd.start = 0x%08x\n"
            "params->u.initrd.size = 0x%x\n", params->hdr.tag,
            params->hdr.size, params->u.initrd.start, params->u.initrd.size ) ;
#endif

    params = tag_next(params);
}


#if 1
static void setup_ramdisk_tag(bd_t *bd)
{
    /* an ATAG_RAMDISK node tells the kernel how large the
     * decompressed ramdisk will become.
     */
    params->hdr.tag = ATAG_RAMDISK;
    params->hdr.size = tag_size(tag_ramdisk);
    
    params->u.ramdisk.start = 0;
    params->u.ramdisk.size = -1;
    params->u.ramdisk.flags = 1;	/* automatically load ramdisk */
   
#if 1
    printf( "\nramdisk tag @%p\n", params ) ;
    printf( "params->hdr.tag = %d\n"
            "params->hdr.size = %d\n"
            "params->u.ramdisk.start = 0x%08x\n"
            "params->u.ramdisk.size = %d\n"
            "params->u.ramdisk.flags = 0x%x\n", params->hdr.tag,
            params->hdr.size, params->u.ramdisk.start, params->u.ramdisk.size,
            params->u.ramdisk.flags ) ;
#endif
 
    params = tag_next(params);
}
#endif

static void setup_end_tag(bd_t *bd)
{
    params->hdr.tag = ATAG_NONE;
    params->hdr.size = 0;

#if 1
    printf( "\nend tag @%p\n", params ) ;
    printf( "params->hdr.tag = %d\n"
            "params->hdr.size = %d\n\n", params->hdr.tag, params->hdr.size ) ;
#endif
}

static void setup_commandline_arguments(bd_t *bd)
{

//	char *commandline = getenv(bd, "bootargs");
	char *commandline = getenv(bd, "ethaddr");
	char *p;
    
    /* eat leading white space */
    for(p = commandline; *p == ' '; p++)
      ;
    
    /* skip non-existent command lines so the kernel will still
     * use its default command line.
     */
    if(*p == '\0')
      return;
	strcpy((char*)bd->bi_boot_params,"ethaddr=");
	strcat(bd->bi_boot_params, p);

	p=getenv(bd, "eth2addr");
	if (p!=NULL) {
		strcat(bd->bi_boot_params," eth2addr=");
		strcat(bd->bi_boot_params,p);
	}
	
	p=getenv(bd, "board_rev");
	if (p!=NULL) {
		strcat(bd->bi_boot_params," board_rev=");
		strcat(bd->bi_boot_params,p);
	}

#ifdef CONFIG_PHONE_PORT
	p=getenv(bd, "phone_port");
	if (p!=NULL) {
		strcat(bd->bi_boot_params," phone_port=");
		strcat(bd->bi_boot_params,p);
	}
#endif
    p=getenv(bd, "reef_stop");
    if (p!=NULL) {
        strcat(bd->bi_boot_params," reef_stop=");
        strcat(bd->bi_boot_params,p);
    }

    p=getenv(bd, "boot_count");
    if (p!=NULL) {
        strcat(bd->bi_boot_params," boot_count=");
        strcat(bd->bi_boot_params,p);
    }

    p=getenv(bd, "jbus_logcfg");
    if (p!=NULL) {
        strcat(bd->bi_boot_params," jbus_logcfg=");
        strcat(bd->bi_boot_params,p);
    }

    p=getenv(bd, "watchdog");
    if (p!=NULL) {
        strcat(bd->bi_boot_params," watchdog=");
        strcat(bd->bi_boot_params,p);
    }

    p=getenv(bd, "showEvents");
    if (p!=NULL) {
        strcat(bd->bi_boot_params," showEvents=");
        strcat(bd->bi_boot_params,p);
    }

#ifdef CONFIG_GIGA94_SUPPORT
    if(bd->bi_ext.ucGiga94On!=1)
    {
        strcat(bd->bi_boot_params," Giga94=false");
    }
    else
    {
        strcat(bd->bi_boot_params," Giga94=true");

        if(bd->bi_ext.ucEthLoopbackTestResult==1)
        {
            strcat(bd->bi_boot_params," ethloopbacktest=1");
        }
        else
        {
            strcat(bd->bi_boot_params," ethloopbacktest=0");
        }
    }
#endif

#ifdef CONFIG_SYSTEM_LOCK_ENV
    if(!bd->bi_ext.ucSystemLocked)
    {
        strcat(bd->bi_boot_params," system_locked=false");
    }
#endif

#ifdef CONFIG_FACTORY_RESET
	if (bd->bi_ext.ucFactoryResetFlag==1) {
		strcat(bd->bi_boot_params," "CONFIG_FACTORY_RESET"=true");
	}
#endif

#ifdef CONFIG_DEVICEID
	unsigned char aucHexByte[2+1];	// 2 chars per byte +1 nt
	int i;
	strcat(bd->bi_boot_params," "CONFIG_DEVICEID"=");
	for (i=CONFIG_DEVICEID_SIZE; i>0; i--)
	{
		sprintf(aucHexByte,"%02X", bd->bi_ext.aucDeviceId[i-1]);
		strcat(bd->bi_boot_params, aucHexByte);
	}
#endif
#ifdef CONFIG_BOOTED_FROM
	unsigned char aucBuf[2];
	if (bd->bi_ext.cBootedFrom) {
		aucBuf[0] = bd->bi_ext.cBootedFrom;
		aucBuf[1] = '\0';
		strcat(bd->bi_boot_params, " "CONFIG_BOOTED_FROM"=");
		strcat(bd->bi_boot_params, aucBuf);
	}

#endif
#ifdef L_V1_BOARD
	p=getenv(bd, "ipaddr");
	if (p!=NULL) {
		strcat(bd->bi_boot_params," ipaddr=");
		strcat(bd->bi_boot_params,p);
	}

	p=getenv(bd, "netmask");
	if (p!=NULL) {
		strcat(bd->bi_boot_params," netmask=");
		strcat(bd->bi_boot_params,p);
	}

	p=getenv(bd, "gatewayip");
	if (p!=NULL) {
		strcat(bd->bi_boot_params," gatewayip=");
		strcat(bd->bi_boot_params,p);
	}

	p=getenv(bd, "serverip");
	if (p!=NULL) {
		strcat(bd->bi_boot_params," serverip=");
		strcat(bd->bi_boot_params,p);
	}

	p=getenv(bd, "recovery");
	if (p!=NULL) {
		strcat(bd->bi_boot_params," recovery=");
		strcat(bd->bi_boot_params,p);
	}
#endif

 /*   
    strcpy(bd->bi_boot_params, p);
	strcat(bd->bi_boot_params," ethaddr=");
	p=getenv(bd, "ethaddr");
	strcat(bd->bi_boot_params,p);
	strcat(bd->bi_boot_params," ipaddr=");
	p=getenv(bd, "ipaddr");
	strcat(bd->bi_boot_params,p);

*/
	printf("commandline arguments position = %08x\n",bd->bi_boot_params);

     printf( "commandline = '%s'\n", bd->bi_boot_params) ;

}

//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:49.652221564
//@@ -398,7 +398,7 @@
//      */
//     if(*p == '\0')
//       return;
//-	strcpy(bd->bi_boot_params,"ethaddr=");
//+	strcpy((char*)bd->bi_boot_params,"ethaddr=");
// 	strcat(bd->bi_boot_params, p);
// 
// 	p=getenv(bd, "eth2addr");
//@@ -413,12 +413,96 @@
// 		strcat(bd->bi_boot_params,p);
// 	}
// 
//+#ifdef CONFIG_PHONE_PORT
// 	p=getenv(bd, "phone_port");
// 	if (p!=NULL) {
// 		strcat(bd->bi_boot_params," phone_port=");
// 		strcat(bd->bi_boot_params,p);
// 	}
//+#endif
//+    p=getenv(bd, "reef_stop");
//+    if (p!=NULL) {
//+        strcat(bd->bi_boot_params," reef_stop=");
//+        strcat(bd->bi_boot_params,p);
//+    }
// 
//+    p=getenv(bd, "boot_count");
//+    if (p!=NULL) {
//+        strcat(bd->bi_boot_params," boot_count=");
//+        strcat(bd->bi_boot_params,p);
//+    }
//+
//+    p=getenv(bd, "jbus_logcfg");
//+    if (p!=NULL) {
//+        strcat(bd->bi_boot_params," jbus_logcfg=");
//+        strcat(bd->bi_boot_params,p);
//+    }
//+
//+    p=getenv(bd, "watchdog");
//+    if (p!=NULL) {
//+        strcat(bd->bi_boot_params," watchdog=");
//+        strcat(bd->bi_boot_params,p);
//+    }
//+
//+    p=getenv(bd, "showEvents");
//+    if (p!=NULL) {
//+        strcat(bd->bi_boot_params," showEvents=");
//+        strcat(bd->bi_boot_params,p);
//+    }
//+
//+#ifdef CONFIG_GIGA94_SUPPORT
//+    if(bd->bi_ext.ucGiga94On!=1)
//+    {
//+        strcat(bd->bi_boot_params," Giga94=false");
//+    }
//+    else
//+    {
//+        strcat(bd->bi_boot_params," Giga94=true");
//+
//+        if(bd->bi_ext.ucEthLoopbackTestResult==1)
//+        {
//+            strcat(bd->bi_boot_params," ethloopbacktest=1");
//+        }
//+        else
//+        {
//+            strcat(bd->bi_boot_params," ethloopbacktest=0");
//+        }
//+    }
//+#endif
//+
//+#ifdef CONFIG_SYSTEM_LOCK_ENV
//+    if(!bd->bi_ext.ucSystemLocked)
//+    {
//+        strcat(bd->bi_boot_params," system_locked=false");
//+    }
//+#endif
//+
//+#ifdef CONFIG_FACTORY_RESET
//+	if (bd->bi_ext.ucFactoryResetFlag==1) {
//+		strcat(bd->bi_boot_params," "CONFIG_FACTORY_RESET"=true");
//+	}
//+#endif
//+
//+#ifdef CONFIG_DEVICEID
//+	unsigned char aucHexByte[2+1];	// 2 chars per byte +1 nt
//+	int i;
//+	strcat(bd->bi_boot_params," "CONFIG_DEVICEID"=");
//+	for (i=CONFIG_DEVICEID_SIZE; i>0; i--)
//+	{
//+		sprintf(aucHexByte,"%02X", bd->bi_ext.aucDeviceId[i-1]);
//+		strcat(bd->bi_boot_params, aucHexByte);
//+	}
//+#endif
//+#ifdef CONFIG_BOOTED_FROM
//+	unsigned char aucBuf[2];
//+	if (bd->bi_ext.cBootedFrom) {
//+		aucBuf[0] = bd->bi_ext.cBootedFrom;
//+		aucBuf[1] = '\0';
//+		strcat(bd->bi_boot_params, " "CONFIG_BOOTED_FROM"=");
//+		strcat(bd->bi_boot_params, aucBuf);
//+	}
//+
//+#endif
// #ifdef L_V1_BOARD
// 	p=getenv(bd, "ipaddr");
// 	if (p!=NULL) {
