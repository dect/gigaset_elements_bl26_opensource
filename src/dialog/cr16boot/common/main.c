/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <armboot.h>
#include <command.h>
#include <ptregs.h>
#include <cmd_nvedit.h>
#include <cmd_bootm.h>
#include <net.h>
#include <linux/string.h>

static char * delete_char (char *buffer, char *p, int *colp, int *np, int plen);
static int parse_line (char *, char *[]);
#if (CONFIG_BOOTDELAY >= 0)
static int abortboot(bd_t *, int);
#endif
#undef DEBUG_PARSER
#define DEBUG_BOOTKEYS

char        console_buffer[CFG_CBSIZE];		/* console I/O buffer	*/

static char erase_seq[] = "\b \b";		/* erase sequence	*/
static char   tab_seq[] = "        ";		/* used to expand TABs	*/

#ifdef CONFIG_BOOT_RETRY_TIME
static unsigned long endtime = 0;  /* must be set, default is instant timeout */
static int      retry_time = -1; /* -1 so can call readline before main_loop */
#endif

#define get_ticks(x)	get_timer_masked()
#define get_tbclk(x)	1152 /*TIMER1_CFG_HZ*/
#define	endtick(seconds) (get_ticks() + (unsigned long)(seconds) * get_tbclk()*1000)

#ifndef CONFIG_BOOT_RETRY_MIN
#define CONFIG_BOOT_RETRY_MIN CONFIG_BOOT_RETRY_TIME
#endif

void boot_450(bd_t *bd);

extern void boot_l_v1_board(bd_t *bd);


/***************************************************************************
 * Watch for 'delay' seconds for autoboot stop or autoboot delay string.
 * returns: 0 -  no key string, allow autoboot
 *          1 - got key string, abort
 */
#if (CONFIG_BOOTDELAY >= 0)
# if defined(CONFIG_AUTOBOOT_KEYED)
#error CONFIG_AUTOBOOT_KEYED
static __inline__ int abortboot(bd_t *bd, int bootdelay)
{
	int abort = 0;
	char ch;
	uint64_t etime = endtick(bootdelay);
	char* delaykey = getenv (bd, "bootdelaykey");
	char* delaymatch;
	char* stopkey = getenv (bd, "bootstopkey");
	char* stopmatch;

#  ifdef CONFIG_AUTOBOOT_PROMPT
	printf (CONFIG_AUTOBOOT_PROMPT, bootdelay);
#  endif

#  ifdef CONFIG_AUTOBOOT_DELAY_STR
	if (delaykey == NULL)
		delaykey = CONFIG_AUTOBOOT_DELAY_STR;
#  endif
#  ifdef CONFIG_AUTOBOOT_STOP_STR
	if (stopkey == NULL)
		stopkey = CONFIG_AUTOBOOT_STOP_STR;
#  endif

#  if DEBUG_BOOTKEYS
	printf("delay key:<%s>\n",delaykey?delaykey:"NULL");
	printf("stop key:<%s>\n",stopkey?stopkey:"NULL");
#  endif
	delaymatch = delaykey;
	stopmatch = stopkey;

	/* In order to keep up with incoming data, check timeout only
	 * when catch up.
	 */
	while (!abort && get_ticks() <= etime) {
		/* abort if matched all of one of the keys */
		if (delaymatch && !*delaymatch) {
#  if DEBUG_BOOTKEYS
			printf("got delaykey\n");
#  endif
			abort = 1;
		}
		if (stopmatch && !*stopmatch) {
#  if DEBUG_BOOTKEYS
			printf("got stopkey\n");
#  endif
#  ifdef CONFIG_BOOT_RETRY_TIME
			/* don't retry auto boot */
			retry_time = -1;
#  endif
			abort = 1;
		}
		if (tstc()) {
			ch = getc();	/* get the incoming char */

			if (stopmatch) {
				if (*stopmatch != ch)
					stopmatch = stopkey;	/* start over */
				if (*stopmatch == ch)
					++stopmatch;	/* matched 1 more */
			}

			if (delaymatch) {
				if (*delaymatch != ch)
					delaymatch = delaykey;	/* start over */
				if (*delaymatch == ch)
					++delaymatch;	/* matched 1 more */
			}
		}
	}
#  if DEBUG_BOOTKEYS
	if (!abort)
		printf("key timeout\n");
#  endif

	return abort;
}

# else	/* !defined(CONFIG_AUTOBOOT_KEYED) */

extern unsigned char kbd_getc(void);


#if (CFG_BUTTON_DETECTION)
/// how long button was pressed (in 100ms ticks)
static u16 button_pressed_100ms=0;

/** Increment button_pressed_100ms counter when button is pressed
 *
 * Function should be called every 100ms from any looping routines
 *
 * @return 1 if button is still pressed, otherwise 0
 */
static u8 ucTickCheckButton100ms(void)
{
	if((GetWord(P2_DATA_REG)&GPIO_9)==0)
	{
		button_pressed_100ms++;
	    vLEDControl (GREEN, 0); // turn Off green led
		return 1;
	}
	return 0;
}
#endif // CFG_BUTTON_DETECTION

static __inline__ int abortboot(bd_t *bd, int bootdelay)
{
	int abort = 0;
	#if defined(L_V1_BOARD) || defined(L_V2_BOARD)  || defined(L_V3_BOARD) || defined(L_V4_BOARD) || defined(L_V5_BOARD) \
	|| defined (L_V2_CNXT_BOARD)|| defined (L_V3_CNXT_BOARD)|| defined(L_V4_CNXT_BOARD) || defined(L_V5_CNXT_BOARD)
	printf("Hit Enter to stop autoboot: %2d ", bootdelay);
	#else
	printf("Hit any key to stop autoboot: %2d ", bootdelay);
	#endif

#if defined CONFIG_ZERO_BOOTDELAY_CHECK
        /*
         * Check if key already pressed
         * Don't check if bootdelay < 0
         */
	if (bootdelay >= 0) {
		if (tstc()) {	/* we got a key press	*/
			#if defined(L_V1_BOARD) || defined(L_V2_BOARD) || defined(L_V3_BOARD)|| defined(L_V4_BOARD) || defined(L_V5_BOARD)\
			|| defined (L_V2_CNXT_BOARD)|| defined (L_V3_CNXT_BOARD)|| defined(L_V4_CNXT_BOARD) || defined(L_V5_CNXT_BOARD)
						ch = getc();  /* consume input	*/
			if (ch==0xD)
			{
				printf ("\b\b\b 0\n");
				return 1; 	/* don't auto boot	*/
			}
			#else
			(void) getc();  /* consume input	*/
			printf ("\b\b\b 0\n");
			return 1; 	/* don't auto boot	*/
			#endif

		}
	}
#endif

#if defined(L_V1_BOARD) || defined(L_V2_BOARD) || defined(L_V3_BOARD)|| defined(L_V4_BOARD) || defined(L_V5_BOARD)\
	|| defined (L_V2_CNXT_BOARD)|| defined (L_V3_CNXT_BOARD)|| defined(L_V4_CNXT_BOARD) || defined(L_V5_CNXT_BOARD)

	while (bootdelay > 0) {
		int i;
		unsigned char c;

		--bootdelay;
		/* delay 100 * 10ms */
		for (i=0; !abort && i<100; ++i) {
			if (tstc())
			{	/* we got a key press	*/
				c=getc(); /* consume input	*/
				if (c==0xD)
				{
					abort  = 1;	/* don't auto boot	*/
					bootdelay = 0;	/* no more delay*/
					break;
				}

			}
			udelay (10000);
			c= kbd_getc();
			if (c==3) {	/* we got a key press	*/
				abort  = 2;	/* don't auto boot	*/
				bootdelay = 0;	/* no more delay	*/
				break;
			}

		}

		printf ("\b\b\b%2d ", bootdelay);
	}


#else
	while (bootdelay > 0) {
		int i;

		--bootdelay;
		/* delay 100 * 10ms */
		for (i=0; !abort && i<100; ++i) {
			#if (CFG_BUTTON_DETECTION)
				if (i % 10 == 0) (void)ucTickCheckButton100ms();
			#endif
			if (tstc()) {	/* we got a key press	*/
				abort  = 1;	/* don't auto boot	*/
				bootdelay = 0;	/* no more delay	*/
				(void) getc();  /* consume input	*/
				break;
			}
			udelay (10000);
		}
		printf ("\b\b\b%2d ", bootdelay);
	}
#endif

	putc ('\n');

//#ifdef CONFIG_SC14450
#if 0
	if (abort) {
		printf ("------------------ Flash organization ------------------\n");
		printf ("Start\t\t\tSize\t\tComment\n");
		printf ("0x00000000\t\t0x1000\t\tLoader\n");
		printf ("0x00001000\t\t0x1F000\t\tBootloader\n");
		printf ("0x00020000\t\t0x300000\tLinux image\n");
		printf ("0x00320000\t\t0xDC000\t\tFile system\n");
		printf ("0x003FC000\t\t0x2000\t\tManufacture Data\n");
    printf ("0x003FE000\t\t0x2000\t\tEnviroment Variables\n");
		printf ("--------------------------------------------------------\n\n");
	}
#endif
//#endif
	return abort;
}
# endif	/* CONFIG_AUTOBOOT_KEYED */
#endif	/* CONFIG_BOOTDELAY >= 0  */


/****************************************************************************/
void main_loop(bd_t *bd)
{
	static char lastcommand[CFG_CBSIZE] = { 0, };
	int len;
	int rc = 1;
	int flag;


#if (CONFIG_BOOTDELAY >= 0)
	char *s,*sp;
	int bootdelay,abort;
#if defined(L_V1_BOARD) || defined(L_V2_BOARD) || defined(L_V3_BOARD)|| defined(L_V4_BOARD) || defined(L_V5_BOARD)\
	|| defined (L_V2_CNXT_BOARD)|| defined (L_V3_CNXT_BOARD)|| defined(L_V4_CNXT_BOARD) || defined(L_V5_CNXT_BOARD)
	int force_rec;
#endif

#endif
#ifdef CONFIG_PREBOOT
	char *p;
#endif

	char* boot_from = NULL ;

#ifdef CONFIG_PREBOOT
#error CONFIG_PREBOOT
	if ((p = getenv ("preboot")) != NULL) {
# ifdef CONFIG_AUTOBOOT_KEYED
		int prev = disable_ctrlc(1);	/* disable Control C checking */
# endif
		run_command (p, bd, 0);
# ifdef CONFIG_AUTOBOOT_KEYED
		disable_ctrlc(prev);	/* restore Control C checking */
# endif
	}
#endif /* CONFIG_PREBOOT */

#if (CONFIG_BOOTDELAY >= 0)
	//mg
	s = getenv (bd, "bootdelay");
	bootdelay = s ? (int)simple_strtol(s, NULL, 10) : 0;

	PRINTF ("### main_loop entered:\n\n");

# ifdef CONFIG_BOOT_RETRY_TIME
//# error CONFIG_BOOT_RETRY_TIME

	s = getenv (bd, "bootretry");
	if (s != NULL)
		retry_time = (int)simple_strtoul(s, NULL, 10);
	else
		retry_time =  CONFIG_BOOT_RETRY_TIME;
	if (retry_time >= 0 && retry_time < CONFIG_BOOT_RETRY_MIN)
		retry_time = CONFIG_BOOT_RETRY_MIN;
# endif	/* CONFIG_BOOT_RETRY_TIME */
	//mg
	s = getenv (bd, "bootcmd");
#ifdef	CONFIG_BOOTCOMMAND_PROD
	sp = getenv (bd, "bootcmd_prod");
#endif


//	SetPort(P2_05_MO222DE_REG, PORT_PULL_DOWN, PID_port);
//	//if((GetWord(P2_DATA_REG)&0x00100000)==0){ //P2[5] is pulled to GND
//	if((GetWord(P2_DATA_REG) & GPIO_5)==0){ //P2[5] is pulled to GND
//	//if(0){
//		printf ("Ethernet loopback test: Start\n");
//		NetIsLoopback();
//		run_command( "tftpboot", bd, 0 ) ;
//		eth_rx();
//		if(ethloopresult){
//			setenv(bd,"ethloopbacktest","1");
//		}else{
//			setenv(bd,"ethloopbacktest","0");
//		}
//		run_command("saveenv", bd, 0);
//		printf ("Ethernet loopback test: End \n");
//	}

#ifdef CONFIG_GIGA94_SUPPORT
    if( bd->bi_ext.ucGiga94On )
    {
    	abort = 0;
    }else{
    	abort = abortboot (bd, bootdelay);
    }
#else
    abort = abortboot (bd, bootdelay);
#endif


#if (CFG_BUTTON_DETECTION)
	SetPort(P2_09_MODE_REG, PORT_PULL_UP, PID_port);
	PRINTF ("P2_DATA_REG: 0x%08X\n", GetWord(P2_DATA_REG));
	#if defined DEBUG
	if((GetWord(P2_DATA_REG)&GPIO_9)==0) { //P2[9] is pulled to GND
			PRINTF ("P2[9] pressed!\n");
	//		run_command( sp, bd, 0 ) ;
	}
	#endif
#endif // CFG_BUTTON_DETECTION

#if (CFG_BUTTON_DETECTION)
    while ((ucTickCheckButton100ms() != 0)
            && (button_pressed_100ms < CFG_LONGPRESS_MAXIMUM_WAIT))
    {
        if (button_pressed_100ms >= CFG_LONGPRESS_RESET)
        {
            vLEDControl(GREEN, 1);
        }
        udelay(100000);
    }

    // turn On green led
    vLEDControl(GREEN, 1);

	PRINTF("Button pressed for %d x 100ms\n", button_pressed_100ms);
#ifdef CFG_LONGPRESS_RECOVERY
	if (button_pressed_100ms >= CFG_LONGPRESS_RECOVERY)
	{
		printf("Will start recovery image\n");
		bd->bi_ext.ucRecoveryModeFlag=1;
	}
	else
#endif
#ifdef CONFIG_FACTORY_RESET
	if (button_pressed_100ms >= CFG_LONGPRESS_RESET)
	{
		printf("Will start Linux with env. 'factory_reset=true'\n");
		bd->bi_ext.ucFactoryResetFlag=1;
	}
#endif // CONFIG_FACTORY_RESET
#endif // CFG_BUTTON_DETECTION
#if defined(L_V1_BOARD) || defined(L_V2_BOARD) || defined(L_V3_BOARD)|| defined(L_V4_BOARD) || defined(L_V5_BOARD)\
	|| defined (L_V2_CNXT_BOARD)|| defined (L_V3_CNXT_BOARD)|| defined(L_V4_CNXT_BOARD) || defined(L_V5_CNXT_BOARD)
	s=getenv (bd,"force_recovery");
	force_rec = s ? (int)simple_strtol(s, NULL, 10) : 0;
	if (force_rec == 1)
		abort = 2;
#endif

//	if (bootdelay >= 0 && s && !abortboot (bd, bootdelay)) {
	if (bootdelay >= 0 && s && !abort) {
# ifdef CONFIG_AUTOBOOT_KEYED
		int prev = disable_ctrlc(1);	/* disable Control C checking */
# endif
		//run_command (s, bd, 0);

		boot_450(bd);

#if defined(L_V1_BOARD) || defined(L_V2_BOARD) || defined(L_V3_BOARD) || defined(L_V4_BOARD) || defined(L_V5_BOARD)\
		|| defined (L_V2_CNXT_BOARD)|| defined (L_V3_CNXT_BOARD)|| defined(L_V4_CNXT_BOARD) || defined(L_V5_CNXT_BOARD)
		boot_l_v1_board(bd);
#endif


# ifdef CONFIG_AUTOBOOT_KEYED
		disable_ctrlc(prev);	/* restore Control C checking */
# endif
	}
#endif	/* CONFIG_BOOTDELAY */
#if defined(L_V1_BOARD) || defined(L_V2_BOARD) || defined(L_V3_BOARD) || defined(L_V4_BOARD) || defined(L_V5_BOARD)\
	|| defined (L_V2_CNXT_BOARD)|| defined (L_V3_CNXT_BOARD)|| defined(L_V4_CNXT_BOARD) || defined(L_V5_CNXT_BOARD)
	if (abort == 2)
		boot_l_v1_board(bd);
#endif

	/*
	 * Main Loop for Monitor Command Processing
	 */
	for (;;) {
#ifdef CONFIG_BOOT_RETRY_TIME
//#error CONFIG_BOOT_RETRY_TIME
		if (rc >= 0) {
			/* Saw enough of a valid command to
			 * restart the timeout.
			 */
			reset_cmd_timeout();
		}
#endif
		len = readline (CFG_PROMPT);

		flag = 0;	/* assume no special flags for now */
		if (len > 0)
			strcpy (lastcommand, console_buffer);
		else if (len == 0)
			flag |= CMD_FLAG_REPEAT;
#ifdef CONFIG_BOOT_RETRY_TIME
		else if (len == -2) {
			/* -2 means timed out, retry autoboot
			 */
			printf("\nTimed out waiting for command\n");
			return;		/* retry autoboot */
		}
#endif

		if (len == -1)
			printf ("<INTERRUPT>\n");
		else
			rc = run_command (lastcommand, bd, flag);
			rc = 0xff;

		if (rc <= 0) {
			/* invalid command or not repeatable, forget it */
			lastcommand[0] = 0;
		}
	}
}

/***************************************************************************
 * reset command line timeout to retry_time seconds
 */
#ifdef CONFIG_BOOT_RETRY_TIME
void reset_cmd_timeout(void)
{
	endtime = endtick(retry_time);
}
#endif

/****************************************************************************/

/*
 * Prompt for input and read a line.
 * If  CONFIG_BOOT_RETRY_TIME is defined and retry_time >= 0,
 * time out when time goes past endtime (timebase time in ticks).
 * Return:	number of read characters
 *		-1 if break
 *		-2 if timed out
 */
int readline (const char *const prompt)
{
	char   *p = console_buffer;
	int	n = 0;				/* buffer index		*/
	int	plen = strlen (prompt);		/* prompt length	*/
	int	col;				/* output column cnt	*/
	char	c;

	/* print prompt */
	if (prompt)
		puts (prompt);
	col = plen;

	for (;;) {
#ifdef CONFIG_BOOT_RETRY_TIME
		while (!tstc()) {	/* while no incoming data */
			if (retry_time >= 0 && get_ticks() > endtime)
				return (-2);	/* timed out */
		}
#endif
		c = getc();

		/*
		 * Special character handling
		 */
		switch (c) {
		case '\r':				/* Enter		*/
		case '\n':
			*p = '\0';
			puts ("\r\n");
			return (p - console_buffer);

		case 0x03:				/* ^C - break		*/
			console_buffer[0] = '\0';	/* discard input */
			return (-1);

		case 0x15:				/* ^U - erase line	*/
			while (col > plen) {
				puts (erase_seq);
				--col;
			}
			p = console_buffer;
			n = 0;
			continue;

		case 0x17:				/* ^W - erase word 	*/
			p=delete_char(console_buffer, p, &col, &n, plen);
			while ((n > 0) && (*p != ' ')) {
				p=delete_char(console_buffer, p, &col, &n, plen);
			}
			continue;

		case 0x08:				/* ^H  - backspace	*/
		case 0x7F:				/* DEL - backspace	*/
			p=delete_char(console_buffer, p, &col, &n, plen);
			continue;

		default:
			/*
			 * Must be a normal character then
			 */
			if (n < CFG_CBSIZE-2) {
				if (c == '\t') {	/* expand TABs		*/
					puts (tab_seq+(col&07));
					col += 8 - (col&07);
				} else {
					++col;		/* echo input		*/
					putc (c);
				}
				*p++ = c;
				++n;
			} else {			/* Buffer full		*/
				putc ('\a');
			}
		}
	}
}

/****************************************************************************/

static char * delete_char (char *buffer, char *p, int *colp, int *np, int plen)
{
	char *s;

	if (*np == 0) {
		return (p);
	}

	if (*(--p) == '\t') {			/* will retype the whole line	*/
		while (*colp > plen) {
			puts (erase_seq);
			(*colp)--;
		}
		for (s=buffer; s<p; ++s) {
			if (*s == '\t') {
				puts (tab_seq+((*colp) & 07));
				*colp += 8 - ((*colp) & 07);
			} else {
				++(*colp);
				putc (*s);
			}
		}
	} else {
		puts (erase_seq);
		(*colp)--;
	}
	(*np)--;
	return (p);
}

/****************************************************************************/

int parse_line (char *line, char *argv[])
{
	int nargs = 0;

#ifdef DEBUG_PARSER
	printf ("parse_line: \"%s\"\n", line);
#endif
	while (nargs < CFG_MAXARGS) {

		/* skip any white space */
		while ((*line == ' ') || (*line == '\t')) {
			++line;
		}

		if (*line == '\0') {	/* end of line, no more args	*/
			argv[nargs] = NULL;
#ifdef DEBUG_PARSER
		printf ("parse_line: nargs=%d\n", nargs);
#endif
			return (nargs);
		}

		argv[nargs++] = line;	/* begin of argument string	*/

		/* find end of string */
		while (*line && (*line != ' ') && (*line != '\t')) {
			++line;
		}

		if (*line == '\0') {	/* end of line, no more args	*/
			argv[nargs] = NULL;
#ifdef DEBUG_PARSER
		printf ("parse_line: nargs=%d\n", nargs);
#endif
			return (nargs);
		}

		*line++ = '\0';		/* terminate current arg	 */
	}

	printf ("** Too many args (max. %d) **\n", CFG_MAXARGS);

#ifdef DEBUG_PARSER
	printf ("parse_line: nargs=%d\n", nargs);
#endif
	return (nargs);
}

/****************************************************************************/

static void process_macros (bd_t *bd, const char *input, char *output)
{
	char c, prev;
	const char *varname_start = 0;
	int inputcnt  = strlen (input);
	int outputcnt = CFG_CBSIZE;
	int state = 0;	/* 0 = waiting for '$'	*/
			/* 1 = waiting for '('	*/
			/* 2 = waiting for ')'	*/

#ifdef DEBUG_PARSER
	char *output_start = output;

	printf ("[PROCESS_MACROS] INPUT len %d: \"%s\"\n", strlen(input), input);
#endif

	prev = '\0';			/* previous character	*/

	while (inputcnt && outputcnt) {
	    c = *input++;
	    inputcnt--;

	    /* remove one level of escape characters */
	    if ((c == '\\') && (prev != '\\')) {
		if (inputcnt-- == 0)
			break;
		prev = c;
	    	c = *input++;
	    }

	    switch (state) {
	    case 0:			/* Waiting for (unescaped) $	*/
		if ((c == '$') && (prev != '\\')) {
			state++;
		} else {
			*(output++) = c;
			outputcnt--;
		}
		break;
	    case 1:			/* Waiting for (	*/
		if (c == '(') {
			state++;
			varname_start = input;
		} else {
			state = 0;
			*(output++) = '$';
			outputcnt--;

			if (outputcnt) {
				*(output++) = c;
				outputcnt--;
			}
		}
		break;
	    case 2:			/* Waiting for )	*/
		if (c == ')') {
			int i;
			char envname[CFG_CBSIZE], *envval;
			int envcnt = input-varname_start-1; /* Varname # of chars */

			/* Get the varname */
			for (i = 0; i < envcnt; i++) {
				envname[i] = varname_start[i];
			}
			envname[i] = 0;

			/* Get its value */
			envval = getenv (bd, envname);

			/* Copy into the line if it exists */
			if (envval != NULL)
				while ((*envval) && outputcnt) {
					*(output++) = *(envval++);
					outputcnt--;
				}
			/* Look for another '$' */
			state = 0;
		}
		break;
	    }

	    prev = c;
	}

	if (outputcnt)
		*output = 0;

#ifdef DEBUG_PARSER
	printf ("[PROCESS_MACROS] OUTPUT len %d: \"%s\"\n",
		strlen(output_start), output_start);
#endif
}

/****************************************************************************
 * returns:
 *	1  - command executed, repeatable
 *	0  - command executed but not repeatable, interrupted commands are
 *	     always considered not repeatable
 *	-1 - not executed (unrecognized, bootd recursion or too many args)
 *           (If cmd is NULL or "" or longer than CFG_CBSIZE-1 it is
 *           considered unrecognized)
 *
 * WARNING:
 *
 * We must create a temporary copy of the command since the command we get
 * may be the result from getenv(), which returns a pointer directly to
 * the environment data, which may change magicly when the command we run
 * creates or modifies environment variables (like "bootp" does).
 */

int run_command (const char *cmd, bd_t *bd, int flag)
{
	cmd_tbl_t *cmdtp;
	char cmdbuf[CFG_CBSIZE];	/* working copy of cmd		*/
	char *token;			/* start of token in cmdbuf	*/
	char *sep;			/* end of token (separator) in cmdbuf */
	char finaltoken[CFG_CBSIZE];
	char *str = cmdbuf;
	char *argv[CFG_MAXARGS + 1];	/* NULL terminated	*/
	int argc;
	int repeatable = 1;

#ifdef DEBUG_PARSER
	printf ("[RUN_COMMAND] cmd[%p]=\"%s\"\n", cmd, cmd ? cmd : "NULL");
#endif

	clear_ctrlc();		/* forget any previous Control C */

	if (!cmd || !*cmd || strlen(cmd) > CFG_CBSIZE - 1)
		return -1;	/* empty command or too long, do nothing */

	strcpy (cmdbuf, cmd);

	/* Process separators and check for invalid
	 * repeatable commands
	 */

#ifdef DEBUG_PARSER
	printf ("[PROCESS_SEPARATORS] %s\n", cmd);
#endif
	while (*str) {

		/*
		 * Find separator, or string end
		 * Allow simple escape of ';' by writing "\;"
		 */
		for (sep = str; *sep; sep++) {
			if ((*sep == ';') &&	/* separator		*/
			    ( sep != str) &&	/* past string start	*/
			    (*(sep-1) != '\\'))	/* and NOT escaped	*/
				break;
		}

		/*
		 * Limit the token to data between separators
		 */
		token = str;
		if (*sep) {
			str = sep + 1;	/* start of command for next pass */
			*sep = '\0';
		}
		else
			str = sep;	/* no more commands for next pass */
#ifdef DEBUG_PARSER
		printf ("token: \"%s\"\n", token);
#endif

		/* find macros in this token and replace them */
		process_macros (bd, token, finaltoken);

		/* Extract arguments */
		argc = parse_line (finaltoken, argv);

		/* Look up command in command table */
		if ((cmdtp = find_cmd(argv[0])) == NULL) {
			printf ("Unknown command '%s' - try 'help'\n", argv[0]);
			return -1;	/* give up after bad command */
		}

		/* found - check max args */
		if (argc > cmdtp->maxargs) {
			printf ("Usage:\n%s\n", cmdtp->usage);
			return -1;
		}

#if (CONFIG_COMMANDS & CFG_CMD_BOOTD)
		/* avoid "bootd" recursion */
		if (cmdtp->cmd == do_bootd) {
#ifdef DEBUG_PARSER
			printf ("[%s]\n", finaltoken);
#endif
			if (flag & CMD_FLAG_BOOTD) {
				printf ("'bootd' recursion detected\n");
				return -1;
			}
			else
				flag |= CMD_FLAG_BOOTD;
		}
#endif	/* CFG_CMD_BOOTD */

		/* OK - call function to do the command */
		(cmdtp->cmd) (cmdtp, bd, flag, argc, argv);

		repeatable &= cmdtp->repeatable;

		/* Did the user stop this? */
		if (had_ctrlc ())
			return 0;	/* if stopped then not repeatable */
	}

	return repeatable;
}

/****************************************************************************/

#if (CONFIG_COMMANDS & CFG_CMD_RUN)
int do_run (cmd_tbl_t * cmdtp, bd_t * bd, int flag, int argc, char *argv[])
{
	int i;
	int rc = 0;

	if (argc < 2) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		return 1;
	}

	for (i=1; rc == 0 && i<argc; ++i) {
		rc = run_command (getenv (bd, argv[i]), bd, flag);
	}

	return rc;
}
#endif

#define XMK_STR1(x)	#x
#define MK_STR1(x)	XMK_STR1(x)

#ifdef CONFIG_DUAL_IMAGE_BOOT
void  swap_boot_address(bd_t *bd)
{

	int i;
	char boot_pos[20];
	char upgrade_pos[20];
	int val1,val2;

	char *s1,*s2;

	memset (boot_pos, 0, sizeof(boot_pos));
	memset (upgrade_pos, 0, sizeof(upgrade_pos));
	s1 = getenv (bd, "boot_pos");
	if (s1!=NULL)
	{
		val1 = (int)simple_strtol(s1, NULL, 16);

		s2 = getenv (bd, "upgrade_pos");
		if (s2!=NULL)
		{
			val2 = (int)simple_strtol(s2, NULL, 16);

			PRINTF("swap_boot_address %08X %08X \n",val1, val2);

			if (val1==val2)
			{
				printf("same boot & upgrade position\n");
				strcpy(boot_pos,MK_STR1(CONFIG_BOOT_POS));
				strcpy(upgrade_pos,MK_STR1(CONFIG_UPGRADE_POS));
			}
			else
			{
				strcpy(boot_pos,s2);
				strcpy(upgrade_pos,s1);
			}

			setenv(bd,"boot_pos",&boot_pos[0]);
			setenv(bd,"upgrade_pos",&upgrade_pos[0]);
			run_command("saveenv", bd, 0);

		}
		else
		{
			printf("No upgrade position defined \n");
		}

	}
	else
	{
		printf("No boot position defined \n");
	}

}
#endif // CONFIG_DUAL_IMAGE_BOOT


#if CFG_ALT_BOOT_POS

#define	BOOT_FROM_FIRST				'1'
#define BOOT_FROM_SECOND			'2'
#define BOOT_FROM_RECOVERY			'R'
#define BOOT_FROM_DEFAULT			'D'

/**
 * Function checks string pointed by *ppcBootFromImage and tries to boot from one of addresses:
 *  - for "1" reads env variable "1st_boot_pos"
 *  - for "2" reads env variable "2nd_boot_pos"
 *  - for "R" reads env variable "rec_boot_pos"
 *
 * @param bd	uboot context
 * @param ppcBootFromImage	one letter string (null terminated) with id of system to boot
 */
static void TryToBootFromImage (bd_t *bd, char *pcBootFromImage)
{
	char *pcBootFromAddr;
	char acDualBootCommand[32];

	memset(acDualBootCommand, 0 ,sizeof(acDualBootCommand));
	strcpy(acDualBootCommand, "bootm ");

	{
		switch (*pcBootFromImage)
		{
			default:
			case BOOT_FROM_FIRST:
				*pcBootFromImage = BOOT_FROM_FIRST;
				// boot from first image "1st_boot_pos"
				pcBootFromAddr = getenv (bd, "1st_boot_pos");
				break;

			case BOOT_FROM_SECOND:
				// boot from second image "2nd_boot_pos"
				pcBootFromAddr = getenv (bd, "2nd_boot_pos");
				break;
#if CFG_RECOVERY
			case BOOT_FROM_RECOVERY:
				// boot from recovery image "rec_boot_pos"
				pcBootFromAddr = getenv (bd, "rec_boot_pos");
				break;
#endif
		} // switch
	}

	bd->bi_ext.cBootedFrom = *pcBootFromImage;
	printf( "\nBooting from '%s'\n", pcBootFromAddr);
	strcat(acDualBootCommand, pcBootFromAddr); // acDualBootCommand="bootm <boot_pos>"
	run_command( acDualBootCommand, bd, 0 ) ;
	printf ("Boot failed!\n");
}



/** Entry describing single transition if previous boot fails */
typedef	struct
{
	unsigned char	ucDefault;
	unsigned char	ucOld;
	unsigned char	ucNew;
	unsigned long 	ulLEDPin;
} BOOT_FROM_TRANS_DEF;

/** Array of alternative boot options if previous boot fails
 *
 * Green LED: default firmware
 * Blue LED: alternate firmware
 * Yellow LED: recovery firmware
 *
 */

BOOT_FROM_TRANS_DEF	atdBootTrans[] =
{//     default             old --> new				LED
		{ '1',				'1',	'2',			BLUE	},
#if ((CFG_FORCE_RECOVERY) & (CFG_RECOVERY))
		{ '1',				'2',	'R',			YELLOW	},
#else
		{ '1',				'2',	'1',			GREEN	},
#endif

#if ((CFG_FORCE_RECOVERY) & (CFG_RECOVERY))
		{ '2',				'1',	'R',			YELLOW	},
#else
		{ '2',				'1',	'2',			GREEN	},
#endif
		{ '2',				'2',	'1',			BLUE	},


#if CFG_RECOVERY
		{ '1',				'R',	'D',			GREEN	},
		{ '2',				'R',	'D',			GREEN	},
#endif
};
/**
 * Function changes from previous boot position identifier to next one in cyclic manner:
 * "1" --> "2" --> "R" --> "1" ...
 *
 * @param ppcBootFromImage	pointer to char
 */
static void SwapBootIndex (bd_t *bd, char *pcBootFromImage)
{
	char *pcTemp = getenv (bd, "boot_from_image_no");
	char cDefaultBootFromImage;
	if (NULL == pcTemp)
	{
		cDefaultBootFromImage = BOOT_FROM_FIRST;
	}
	else
	{
		cDefaultBootFromImage = *pcTemp;
	}
	printf("\nChange default boot from '%c'", *pcBootFromImage);
	vLEDControl (GREEN,  0);
	vLEDControl (BLUE,   0);
	vLEDControl (YELLOW, 0);

	int i;
	for (i=0; i<sizeof(atdBootTrans)/sizeof(atdBootTrans[0]); i++)
	{
		if (cDefaultBootFromImage == atdBootTrans[i].ucDefault)
		{
			if (*pcBootFromImage == atdBootTrans[i].ucOld)
			{
				*pcBootFromImage = atdBootTrans[i].ucNew;
				vLEDControl (atdBootTrans[i].ulLEDPin, 1);
				break;
			}
		}
	}

	// If boot from default, use default firmware location
	if (*pcBootFromImage == BOOT_FROM_DEFAULT)
	{
		*pcBootFromImage = cDefaultBootFromImage;
	}
	printf(" to '%c' \n", *pcBootFromImage);
}
#endif //CFG_ALT_BOOT_POS


/**
 *
 * @param bd
 */
void  boot_450(bd_t *bd)
{

	char *s1,*s2;
	char* boot_from = NULL ;

    char *my_tftp_boot_cmd = NULL ;

    printf("\n....AutoBoot for SiTel 450 DK....\n");

    // continue according to the value of the boot_from env. variable

	boot_from = getenv( bd, "boot_from" ) ;
	if( boot_from ) {
		//----- boot from tftp ------
		if( ( strcmp( boot_from, "tftp-eth0" ) == 0 ) ) {
			my_tftp_boot_cmd = getenv( bd, "script_tftp");
			if (my_tftp_boot_cmd == NULL) { /* no command available! */
				printf( "\nA command for booting via TFTP was not found! "
				"Setting default as:\n\t"
#ifdef CONFIG_SDRAM_SIZE_32MB
				"tftpboot 0x01a00000 vmlinuz; bootm 0x01a00000\n"
#else
				"tftpboot 0x00a00000 vmlinuz; bootm 0x00a00000\n"
#endif
				"You can change it by issuing:\n"
				"\tsetenv script_tftp tftpboot 0xaddress\\; bootm 0xaddress"
				"\n\tsaveenv\n\n\n");
#ifdef CONFIG_SDRAM_SIZE_32MB
				setenv(bd, "script_tftp", "tftpboot 0x01a00000 vmlinuz; bootm 0x01a00000");
#else
				setenv(bd, "script_tftp", "tftpboot 0x00a00000 vmlinuz; bootm 0x00a00000");
#endif
				run_command("saveenv", bd, 0);
				my_tftp_boot_cmd = getenv( bd, "script_tftp");
			}

	        printf( "\nBooting via TFTP...\nBoot command is: %s\n\n", my_tftp_boot_cmd) ;
			printf( "You can change it by issuing:\n"
					"\tsetenv script_tftp "
					"tftpboot 0xaddress vmlinuz\\; bootm 0xaddress"
					"\n\tsaveenv\n");

			run_command("run script_tftp", bd, 0);
		}

		//----- boot from flash ------

		else if( strcmp( boot_from, "flash" ) == 0 ) {
			printf( "\nBooting from flash memory...\n\n" ) ;
#ifdef CONFIG_DUAL_IMAGE_BOOT
			char acDualBootCommand[32];
			// boot form <boot_pos>
			memset(acDualBootCommand,0 ,sizeof(acDualBootCommand));
			strcpy(acDualBootCommand,"bootm ");
			s1 = getenv (bd, "boot_pos");
			if (s1!=NULL)
			{
				strcat(acDualBootCommand,s1); // acDualBootCommand="bootm <boot_pos>"

				run_command( acDualBootCommand, bd, 0 ) ; // run "bootm <boot_pos>"

				// if we reach this point, boot from <boot_pos> failed,
				// try to boot from <upgrade_pos>
				memset(acDualBootCommand,0 ,sizeof(acDualBootCommand));
				strcpy(acDualBootCommand,"bootm ");
				s2 = getenv (bd, "upgrade_pos");
				if(s2!=NULL)
				{
					strcat(acDualBootCommand,s2);
					swap_boot_address(bd);

					run_command( acDualBootCommand, bd, 0 ) ;
				}
			}
#elif CFG_ALT_BOOT_POS
			// REEF dual system boot
			char *pcTemp = getenv (bd, "boot_from_image_no");
			char cBootFromImage;
			if (NULL == pcTemp)
			{
				cBootFromImage = BOOT_FROM_FIRST;
			}
			else
			{
				cBootFromImage = *pcTemp;
			}

#ifdef	CFG_LONGPRESS_RECOVERY
#if CFG_RECOVERY
			if (bd->bi_ext.ucRecoveryModeFlag==1)
			{
				cBootFromImage = BOOT_FROM_RECOVERY;
			}
#endif
#endif
			while (1)
			{
				TryToBootFromImage (bd, &cBootFromImage);
				SwapBootIndex (bd, &cBootFromImage);
				udelay(1000000);
			}

#else
			s1 = getenv (bd, "bootcmd");
			run_command( s1, bd, 0 ) ;
#endif
		}
		else {
			printf( "\nEnvironment variable 'boot_from' set to an invalid value ('%s')...\n"
			"Please set it to:\n"
			"\t'tftp-eth0'  to boot from a PC usign Ethernet 0 or\n"
			"\t'flash'      to boot from FLASH\n", boot_from ) ;
		}
	}
	else {
		printf( "\nEnvironment variable boot_from not set!\n"
		"Please set it to:\n"
		"\t'tftp-eth0'  to boot from a PC usign Ethernet 0 or\n"
		"\t'flash'      to boot from FLASH\n\n"
		"AutoBoot stops...\n\n" ) ;
	}
}

//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:49.652221564
//@@ -27,6 +27,7 @@
// #include <cmd_nvedit.h>
// #include <cmd_bootm.h>
// #include <net.h>
//+#include <linux/string.h>
// 
// static char * delete_char (char *buffer, char *p, int *colp, int *np, int plen);
// static int parse_line (char *, char *[]);
//@@ -42,11 +43,13 @@
// static char   tab_seq[] = "        ";		/* used to expand TABs	*/
// 
// #ifdef CONFIG_BOOT_RETRY_TIME
//-static uint64_t endtime = 0;  /* must be set, default is instant timeout */
//+static unsigned long endtime = 0;  /* must be set, default is instant timeout */
// static int      retry_time = -1; /* -1 so can call readline before main_loop */
// #endif
// 
//-#define	endtick(seconds) (get_ticks() + (uint64_t)(seconds) * get_tbclk())
//+#define get_ticks(x)	get_timer_masked()
//+#define get_tbclk(x)	1152 /*TIMER1_CFG_HZ*/
//+#define	endtick(seconds) (get_ticks() + (unsigned long)(seconds) * get_tbclk()*1000)
// 
// #ifndef CONFIG_BOOT_RETRY_MIN
// #define CONFIG_BOOT_RETRY_MIN CONFIG_BOOT_RETRY_TIME
//@@ -56,6 +59,7 @@
// 
// extern void boot_l_v1_board(bd_t *bd);
// 
//+
// /***************************************************************************
//  * Watch for 'delay' seconds for autoboot stop or autoboot delay string.
//  * returns: 0 -  no key string, allow autoboot
//@@ -145,6 +149,29 @@
// 
// extern unsigned char kbd_getc(void);
// 
//+
//+#if (CFG_BUTTON_DETECTION)
//+/// how long button was pressed (in 100ms ticks)
//+static u16 button_pressed_100ms=0;
//+
//+/** Increment button_pressed_100ms counter when button is pressed
//+ *
//+ * Function should be called every 100ms from any looping routines
//+ *
//+ * @return 1 if button is still pressed, otherwise 0
//+ */
//+static u8 ucTickCheckButton100ms(void)
//+{
//+	if((GetWord(P2_DATA_REG)&GPIO_9)==0)
//+	{
//+		button_pressed_100ms++;
//+	    vLEDControl (GREEN, 0); // turn Off green led
//+		return 1;
//+	}
//+	return 0;
//+}
//+#endif // CFG_BUTTON_DETECTION
//+
// static __inline__ int abortboot(bd_t *bd, int bootdelay)
// {
// 	int abort = 0;
//@@ -222,6 +249,9 @@
// 		--bootdelay;
// 		/* delay 100 * 10ms */
// 		for (i=0; !abort && i<100; ++i) {
//+			#if (CFG_BUTTON_DETECTION)
//+				if (i % 10 == 0) (void)ucTickCheckButton100ms();
//+			#endif
// 			if (tstc()) {	/* we got a key press	*/
// 				abort  = 1;	/* don't auto boot	*/
// 				bootdelay = 0;	/* no more delay	*/
//@@ -256,6 +286,7 @@
// # endif	/* CONFIG_AUTOBOOT_KEYED */
// #endif	/* CONFIG_BOOTDELAY >= 0  */
// 
//+
// /****************************************************************************/
// void main_loop(bd_t *bd)
// {
//@@ -264,11 +295,10 @@
// 	int rc = 1;
// 	int flag;
// 
//+
// #if (CONFIG_BOOTDELAY >= 0)
//-	char *s;
//+	char *s,*sp;
// 	int bootdelay,abort;
//-	char cmd1[]="bootm 20000";
//-
// #if defined(L_V1_BOARD) || defined(L_V2_BOARD) || defined(L_V3_BOARD)|| defined(L_V4_BOARD) || defined(L_V5_BOARD)\
// 	|| defined (L_V2_CNXT_BOARD)|| defined (L_V3_CNXT_BOARD)|| defined(L_V4_CNXT_BOARD) || defined(L_V5_CNXT_BOARD)
// 	int force_rec;
//@@ -298,14 +328,11 @@
// 	//mg
// 	s = getenv (bd, "bootdelay");
// 	bootdelay = s ? (int)simple_strtol(s, NULL, 10) : 0;
//-//	bootdelay = 3;
// 
//-#if 0
//-	printf ("### main_loop entered:\n\n");
//-#endif
//+	PRINTF ("### main_loop entered:\n\n");
// 
// # ifdef CONFIG_BOOT_RETRY_TIME
//-# error CONFIG_BOOT_RETRY_TIME
//+//# error CONFIG_BOOT_RETRY_TIME
// 
// 	s = getenv (bd, "bootretry");
// 	if (s != NULL)
//@@ -317,10 +344,82 @@
// # endif	/* CONFIG_BOOT_RETRY_TIME */
// 	//mg
// 	s = getenv (bd, "bootcmd");
//-	//s = cmd1;
//+#ifdef	CONFIG_BOOTCOMMAND_PROD
//+	sp = getenv (bd, "bootcmd_prod");
//+#endif
//+
// 
//+//	SetPort(P2_05_MO222DE_REG, PORT_PULL_DOWN, PID_port);
//+//	//if((GetWord(P2_DATA_REG)&0x00100000)==0){ //P2[5] is pulled to GND
//+//	if((GetWord(P2_DATA_REG) & GPIO_5)==0){ //P2[5] is pulled to GND
//+//	//if(0){
//+//		printf ("Ethernet loopback test: Start\n");
//+//		NetIsLoopback();
//+//		run_command( "tftpboot", bd, 0 ) ;
//+//		eth_rx();
//+//		if(ethloopresult){
//+//			setenv(bd,"ethloopbacktest","1");
//+//		}else{
//+//			setenv(bd,"ethloopbacktest","0");
//+//		}
//+//		run_command("saveenv", bd, 0);
//+//		printf ("Ethernet loopback test: End \n");
//+//	}
//+
//+#ifdef CONFIG_GIGA94_SUPPORT
//+    if( bd->bi_ext.ucGiga94On )
//+    {
//+    	abort = 0;
//+    }else{
//+    	abort = abortboot (bd, bootdelay);
//+    }
//+#else
//+    abort = abortboot (bd, bootdelay);
//+#endif
// 
//-	abort = abortboot (bd, bootdelay);
//+
//+#if (CFG_BUTTON_DETECTION)
//+	SetPort(P2_09_MODE_REG, PORT_PULL_UP, PID_port);
//+	PRINTF ("P2_DATA_REG: 0x%08X\n", GetWord(P2_DATA_REG));
//+	#if defined DEBUG
//+	if((GetWord(P2_DATA_REG)&GPIO_9)==0) { //P2[9] is pulled to GND
//+			PRINTF ("P2[9] pressed!\n");
//+	//		run_command( sp, bd, 0 ) ;
//+	}
//+	#endif
//+#endif // CFG_BUTTON_DETECTION
//+
//+#if (CFG_BUTTON_DETECTION)
//+    while ((ucTickCheckButton100ms() != 0)
//+            && (button_pressed_100ms < CFG_LONGPRESS_MAXIMUM_WAIT))
//+    {
//+        if (button_pressed_100ms >= CFG_LONGPRESS_RESET)
//+        {
//+            vLEDControl(GREEN, 1);
//+        }
//+        udelay(100000);
//+    }
//+
//+    // turn On green led
//+    vLEDControl(GREEN, 1);
//+
//+	PRINTF("Button pressed for %d x 100ms\n", button_pressed_100ms);
//+#ifdef CFG_LONGPRESS_RECOVERY
//+	if (button_pressed_100ms >= CFG_LONGPRESS_RECOVERY)
//+	{
//+		printf("Will start recovery image\n");
//+		bd->bi_ext.ucRecoveryModeFlag=1;
//+	}
//+	else
//+#endif
//+#ifdef CONFIG_FACTORY_RESET
//+	if (button_pressed_100ms >= CFG_LONGPRESS_RESET)
//+	{
//+		printf("Will start Linux with env. 'factory_reset=true'\n");
//+		bd->bi_ext.ucFactoryResetFlag=1;
//+	}
//+#endif // CONFIG_FACTORY_RESET
//+#endif // CFG_BUTTON_DETECTION
// #if defined(L_V1_BOARD) || defined(L_V2_BOARD) || defined(L_V3_BOARD)|| defined(L_V4_BOARD) || defined(L_V5_BOARD)\
// 	|| defined (L_V2_CNXT_BOARD)|| defined (L_V3_CNXT_BOARD)|| defined(L_V4_CNXT_BOARD) || defined(L_V5_CNXT_BOARD)
// 	s=getenv (bd,"force_recovery");
//@@ -360,7 +459,7 @@
// 	 */
// 	for (;;) {
// #ifdef CONFIG_BOOT_RETRY_TIME
//-#error CONFIG_BOOT_RETRY_TIME
//+//#error CONFIG_BOOT_RETRY_TIME
// 		if (rc >= 0) {
// 			/* Saw enough of a valid command to
// 			 * restart the timeout.
//@@ -816,7 +915,7 @@
// #define XMK_STR1(x)	#x
// #define MK_STR1(x)	XMK_STR1(x)
// 
//-
//+#ifdef CONFIG_DUAL_IMAGE_BOOT
// void  swap_boot_address(bd_t *bd)
// {
// 
//@@ -827,12 +926,8 @@
// 
// 	char *s1,*s2;
// 
//-	for (i=0;i<20;i++)
//-	{
//-		boot_pos[i] = 0;
//-		upgrade_pos[i] = 0;
//-	}
//-
//+	memset (boot_pos, 0, sizeof(boot_pos));
//+	memset (upgrade_pos, 0, sizeof(upgrade_pos));
// 	s1 = getenv (bd, "boot_pos");
// 	if (s1!=NULL)
// 	{
//@@ -843,7 +938,7 @@
// 		{
// 			val2 = (int)simple_strtol(s2, NULL, 16);
// 
//-//			printf("%x %x \n",val1,val2);
//+			PRINTF("swap_boot_address %08X %08X \n",val1, val2);
// 
// 			if (val1==val2)
// 			{
//@@ -874,29 +969,162 @@
// 	}
// 
// }
//+#endif // CONFIG_DUAL_IMAGE_BOOT
// 
// 
//+#if CFG_ALT_BOOT_POS
// 
//-void  boot_450(bd_t *bd)
//+#define	BOOT_FROM_FIRST				'1'
//+#define BOOT_FROM_SECOND			'2'
//+#define BOOT_FROM_RECOVERY			'R'
//+#define BOOT_FROM_DEFAULT			'D'
//+
//+/**
//+ * Function checks string pointed by *ppcBootFromImage and tries to boot from one of addresses:
//+ *  - for "1" reads env variable "1st_boot_pos"
//+ *  - for "2" reads env variable "2nd_boot_pos"
//+ *  - for "R" reads env variable "rec_boot_pos"
//+ *
//+ * @param bd	uboot context
//+ * @param ppcBootFromImage	one letter string (null terminated) with id of system to boot
//+ */
//+static void TryToBootFromImage (bd_t *bd, char *pcBootFromImage)
// {
//+	char *pcBootFromAddr;
//+	char acDualBootCommand[32];
// 
//-	char *s1,*s2,*s3,*s4,*s5,*s6,*s8;
//-	char* boot_from = NULL ;
//+	memset(acDualBootCommand, 0 ,sizeof(acDualBootCommand));
//+	strcpy(acDualBootCommand, "bootm ");
// 
//-    char *my_tftp_boot_cmd = NULL ;
//+	{
//+		switch (*pcBootFromImage)
//+		{
//+			default:
//+			case BOOT_FROM_FIRST:
//+				*pcBootFromImage = BOOT_FROM_FIRST;
//+				// boot from first image "1st_boot_pos"
//+				pcBootFromAddr = getenv (bd, "1st_boot_pos");
//+				break;
// 
//-    char cmd1[] = "setenv serverip 192.168.1.2";
//-    char cmd2[] = "savenv";
//-    char cmd3[] = "printenv";
//-    char cmd4[] = "tftpboot uClinux.img.gz ";
//-#ifdef CONFIG_SDRAM_SIZE_32MB
//-    char cmd5[] = "bootm 0x1a00000" ;
//+			case BOOT_FROM_SECOND:
//+				// boot from second image "2nd_boot_pos"
//+				pcBootFromAddr = getenv (bd, "2nd_boot_pos");
//+				break;
//+#if CFG_RECOVERY
//+			case BOOT_FROM_RECOVERY:
//+				// boot from recovery image "rec_boot_pos"
//+				pcBootFromAddr = getenv (bd, "rec_boot_pos");
//+				break;
//+#endif
//+		} // switch
//+	}
//+
//+	bd->bi_ext.cBootedFrom = *pcBootFromImage;
//+	printf( "\nBooting from '%s'\n", pcBootFromAddr);
//+	strcat(acDualBootCommand, pcBootFromAddr); // acDualBootCommand="bootm <boot_pos>"
//+	run_command( acDualBootCommand, bd, 0 ) ;
//+	printf ("Boot failed!\n");
//+}
//+
//+
//+
//+/** Entry describing single transition if previous boot fails */
//+typedef	struct
//+{
//+	unsigned char	ucDefault;
//+	unsigned char	ucOld;
//+	unsigned char	ucNew;
//+	unsigned long 	ulLEDPin;
//+} BOOT_FROM_TRANS_DEF;
//+
//+/** Array of alternative boot options if previous boot fails
//+ *
//+ * Green LED: default firmware
//+ * Blue LED: alternate firmware
//+ * Yellow LED: recovery firmware
//+ *
//+ */
//+
//+BOOT_FROM_TRANS_DEF	atdBootTrans[] =
//+{//     default             old --> new				LED
//+		{ '1',				'1',	'2',			BLUE	},
//+#if ((CFG_FORCE_RECOVERY) & (CFG_RECOVERY))
//+		{ '1',				'2',	'R',			YELLOW	},
// #else
//-    char cmd5[] = "bootm 0xa00000" ;
//+		{ '1',				'2',	'1',			GREEN	},
// #endif
//-    char cmd6[] = "bootm 0x20000" ;
//+
//+#if ((CFG_FORCE_RECOVERY) & (CFG_RECOVERY))
//+		{ '2',				'1',	'R',			YELLOW	},
//+#else
//+		{ '2',				'1',	'2',			GREEN	},
//+#endif
//+		{ '2',				'2',	'1',			BLUE	},
//+
//+
//+#if CFG_RECOVERY
//+		{ '1',				'R',	'D',			GREEN	},
//+		{ '2',				'R',	'D',			GREEN	},
//+#endif
//+};
//+/**
//+ * Function changes from previous boot position identifier to next one in cyclic manner:
//+ * "1" --> "2" --> "R" --> "1" ...
//+ *
//+ * @param ppcBootFromImage	pointer to char
//+ */
//+static void SwapBootIndex (bd_t *bd, char *pcBootFromImage)
//+{
//+	char *pcTemp = getenv (bd, "boot_from_image_no");
//+	char cDefaultBootFromImage;
//+	if (NULL == pcTemp)
//+	{
//+		cDefaultBootFromImage = BOOT_FROM_FIRST;
//+	}
//+	else
//+	{
//+		cDefaultBootFromImage = *pcTemp;
//+	}
//+	printf("\nChange default boot from '%c'", *pcBootFromImage);
//+	vLEDControl (GREEN,  0);
//+	vLEDControl (BLUE,   0);
//+	vLEDControl (YELLOW, 0);
//+
// 	int i;
//-	char dual_boot[20];
//+	for (i=0; i<sizeof(atdBootTrans)/sizeof(atdBootTrans[0]); i++)
//+	{
//+		if (cDefaultBootFromImage == atdBootTrans[i].ucDefault)
//+		{
//+			if (*pcBootFromImage == atdBootTrans[i].ucOld)
//+			{
//+				*pcBootFromImage = atdBootTrans[i].ucNew;
//+				vLEDControl (atdBootTrans[i].ulLEDPin, 1);
//+				break;
//+			}
//+		}
//+	}
//+
//+	// If boot from default, use default firmware location
//+	if (*pcBootFromImage == BOOT_FROM_DEFAULT)
//+	{
//+		*pcBootFromImage = cDefaultBootFromImage;
//+	}
//+	printf(" to '%c' \n", *pcBootFromImage);
//+}
//+#endif //CFG_ALT_BOOT_POS
//+
//+
//+/**
//+ *
//+ * @param bd
//+ */
//+void  boot_450(bd_t *bd)
//+{
//+
//+	char *s1,*s2;
//+	char* boot_from = NULL ;
//+
//+    char *my_tftp_boot_cmd = NULL ;
// 
//     printf("\n....AutoBoot for SiTel 450 DK....\n");
// 
//@@ -904,6 +1132,7 @@
// 
// 	boot_from = getenv( bd, "boot_from" ) ;
// 	if( boot_from ) {
//+		//----- boot from tftp ------
// 		if( ( strcmp( boot_from, "tftp-eth0" ) == 0 ) ) {
// 			my_tftp_boot_cmd = getenv( bd, "script_tftp");
// 			if (my_tftp_boot_cmd == NULL) { /* no command available! */
//@@ -934,35 +1163,66 @@
// 
// 			run_command("run script_tftp", bd, 0);
// 		}
//+
//+		//----- boot from flash ------
//+
// 		else if( strcmp( boot_from, "flash" ) == 0 ) {
// 			printf( "\nBooting from flash memory...\n\n" ) ;
// #ifdef CONFIG_DUAL_IMAGE_BOOT
//-
//-			for (i=0;i<20;i++) dual_boot[i]=0;
//-
//-			strcpy(dual_boot,"bootm ");
//+			char acDualBootCommand[32];
//+			// boot form <boot_pos>
//+			memset(acDualBootCommand,0 ,sizeof(acDualBootCommand));
//+			strcpy(acDualBootCommand,"bootm ");
// 			s1 = getenv (bd, "boot_pos");
// 			if (s1!=NULL)
// 			{
//-				strcat(dual_boot,s1);
//+				strcat(acDualBootCommand,s1); // acDualBootCommand="bootm <boot_pos>"
// 
//-				 run_command( dual_boot, bd, 0 ) ;
//+				run_command( acDualBootCommand, bd, 0 ) ; // run "bootm <boot_pos>"
// 
//-				for (i=0;i<20;i++) dual_boot[i]=0;
//-
//-				strcpy(dual_boot,"bootm ");
//+				// if we reach this point, boot from <boot_pos> failed,
//+				// try to boot from <upgrade_pos>
//+				memset(acDualBootCommand,0 ,sizeof(acDualBootCommand));
//+				strcpy(acDualBootCommand,"bootm ");
// 				s2 = getenv (bd, "upgrade_pos");
// 				if(s2!=NULL)
// 				{
//-					strcat(dual_boot,s2);
//+					strcat(acDualBootCommand,s2);
// 					swap_boot_address(bd);
// 
//-				 run_command( dual_boot, bd, 0 ) ;
//+					run_command( acDualBootCommand, bd, 0 ) ;
// 				}
// 			}
//+#elif CFG_ALT_BOOT_POS
//+			// REEF dual system boot
//+			char *pcTemp = getenv (bd, "boot_from_image_no");
//+			char cBootFromImage;
//+			if (NULL == pcTemp)
//+			{
//+				cBootFromImage = BOOT_FROM_FIRST;
//+			}
//+			else
//+			{
//+				cBootFromImage = *pcTemp;
//+			}
//+
//+#ifdef	CFG_LONGPRESS_RECOVERY
//+#if CFG_RECOVERY
//+			if (bd->bi_ext.ucRecoveryModeFlag==1)
//+			{
//+				cBootFromImage = BOOT_FROM_RECOVERY;
//+			}
//+#endif
//+#endif
//+			while (1)
//+			{
//+				TryToBootFromImage (bd, &cBootFromImage);
//+				SwapBootIndex (bd, &cBootFromImage);
//+				udelay(1000000);
//+			}
//+
// #else
// 			s1 = getenv (bd, "bootcmd");
//-//			s1 = cmd7 ;
// 			run_command( s1, bd, 0 ) ;
// #endif
// 		}
