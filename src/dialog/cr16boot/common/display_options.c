/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include "version.h"
#include "armboot.h"



const char version_string[] = 
	SITELBOOT_VERSION " (" __DATE__ " - " __TIME__ ")";

void display_banner(bd_t *bd)
{
    printf ("\n\n\n\n\n");
    printf ("    ,;:.                                        	\n");
    printf ("  ;;;;;;;                                   :,		\n");
    printf (" ;;:    .                                   ;;		\n");
    printf (" ;;        ,   `;;;.  `:;;,   .;;:   `;;:  :;;:,	\n");
    printf (",;`       `;  :;;:;;, :;,:;: ,;:.;  .;;,;; ;;;;	\n");
    printf (":;        `;  ;:   ;,     :; ;;     ;:   ;. ;;		\n");
    printf (":;    ;;;.`; ,;    ;,  .:;;; ,;;.  `;....;: ;;		\n");
    printf (",;`   ..;.`; :;    ;, ;;,.:;  .;;; `;;;;;;: ;;		\n");
    printf (" ;;     ;.`; ,;`   ;,.;   :;    `;; ;`      ;;		\n");
    printf (" :;;   `;.`;  ;;  ,;,`;`  :; `   ;; ;;   .  ;;		\n");
    printf ("  ,;;;;;;``;  `;;;;;, ;;;;:; ;;;;;   ;;;;;  ,;;;	\n");
    printf ("    `..            :,  ``      .`     `.`     .	\n");
    printf ("                  `:.                           	\n");
    printf ("              ::.,::                            	\n");
    printf ("              `::::								\n");

    printf ("\n\n%s\n\n", version_string);

#ifdef SC14452
    printf ("Runing on SC14452 SoC \n\n" );
#else
    printf ("Runing on SC14450 SoC \n\n" );
#endif
    printf("SiTelboot \n\t\tcode: %08lx -> %08lx\n\t\tdata: %08lx -> %08lx\n",
		    _armboot_start,_armboot_text_end,_armboot_data_start, _armboot_end);
#ifdef CONFIG_USE_IRQ
//    printf("IRQ Stack: %08lx\n", IRQ_STACK_START);
#endif
}

static void pretty_print_size(ulong size)
{
    if (size > 0x100000)
      printf("%ld MB", size / 0x100000);
    else
      printf("%ld KB", size / 0x400);
}

void display_dram_config(bd_t *bd)
{
    int i;
   
    printf("DRAM Configuration:\n");
   
    for(i=0; i<CONFIG_NR_DRAM_BANKS; i++)
    {
	printf("Bank #%d: %08lx ", i, bd->bi_dram[i].start);
	pretty_print_size(bd->bi_dram[i].size);
	printf("\n");
    }
}
/*
void display_flash_config(bd_t *bd, ulong size)
{
    printf("Flash: ");
    pretty_print_size(size);
    printf("\n");
}
*/

void display_flash_config(bd_t *bd)
{
    int i;

    printf("FLASH Configuration:\n");

    for(i=0; i<CONFIG_NR_FLASH_BANKS; i++)
    {
    	printf("Bank #%d: %08lx ", i, bd->bi_flash[i].start);
    	pretty_print_size(bd->bi_flash[i].size);
    	printf("\n");
    }
}
//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:49.652221564
//@@ -31,22 +31,22 @@
// 
// void display_banner(bd_t *bd)
// {
//-
//     printf ("\n\n\n\n\n");
//-    printf ("                 @@@           @@                  @@\n");
//-    printf ("                @@@@@          @@   @@             @@\n");
//-    printf ("                @@@@@          @@                  @@\n");
//-    printf ("      .@@@@@.  @@@@@      @@@ @@   @@     @@@@    @@     @@@       @@@\n");
//-    printf ("     @@@@@@   @@@@@@@    @@   @@@   @@        @@   @@   @@   @@   @@   @@\n");
//-    printf ("    @@@@@    @@@@@@@@    @@    @@   @@    @@@@@@   @@   @@   @@   @@   @@\n");
//-    printf ("   @@@@@@     @@@@@@@    @@    @@   @@   @@   @@   @@   @@   @@   @@   @@\n");
//-    printf ("   @@@@@@@@     @@@@@    @@   @@@   @@   @@   @@   @@   @@   @@   @@   @@\n");
//-    printf ("   @@@@@@@@@@@    @@@     @@@@ @@   @@    @@@@@    @@     @@@       @@@@@\n");
//-    printf ("    @@@@@@@@@@@  @@@@                                                  @@\n");
//-    printf ("    @@@@@@@@@@@@@@@@                                                  @@\n");
//-    printf ("     @@@@@    S  E  M  I  C  O  N  D  U  C  T  O  R     @@@@@\n");
//-
//-
//+    printf ("    ,;:.                                        	\n");
//+    printf ("  ;;;;;;;                                   :,		\n");
//+    printf (" ;;:    .                                   ;;		\n");
//+    printf (" ;;        ,   `;;;.  `:;;,   .;;:   `;;:  :;;:,	\n");
//+    printf (",;`       `;  :;;:;;, :;,:;: ,;:.;  .;;,;; ;;;;	\n");
//+    printf (":;        `;  ;:   ;,     :; ;;     ;:   ;. ;;		\n");
//+    printf (":;    ;;;.`; ,;    ;,  .:;;; ,;;.  `;....;: ;;		\n");
//+    printf (",;`   ..;.`; :;    ;, ;;,.:;  .;;; `;;;;;;: ;;		\n");
//+    printf (" ;;     ;.`; ,;`   ;,.;   :;    `;; ;`      ;;		\n");
//+    printf (" :;;   `;.`;  ;;  ,;,`;`  :; `   ;; ;;   .  ;;		\n");
//+    printf ("  ,;;;;;;``;  `;;;;;, ;;;;:; ;;;;;   ;;;;;  ,;;;	\n");
//+    printf ("    `..            :,  ``      .`     `.`     .	\n");
//+    printf ("                  `:.                           	\n");
//+    printf ("              ::.,::                            	\n");
//+    printf ("              `::::								\n");
// 
//     printf ("\n\n%s\n\n", version_string);
// 
