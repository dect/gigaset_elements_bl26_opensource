// File created by Gigaset Elements GmbH
// All rights reserved.

#include "armboot.h"
//#include "sitel_io.h"
//#include "spi_polled.h"
//#include "qspic.h"
#include "net.h"


#ifdef CONFIG_SYSTEM_LOCK_ENV

/*
 * Unlock timeout - unlock is not possible for values lower than 10.
 */
#define UNLOCK_TIMEOUT		10

static void UnlockHandler(uchar *pkt, unsigned dest, unsigned src, unsigned len)
{

	int i;

	if(dest!=0 && src!=0) {
		return;
	}

	PRINTF("DestPort: %d\n", dest);
	PRINTF("SrcPort: %d\n", src);
	PRINTF("Len: %d\n", len);

#ifdef DEBUG
	for(i=0;i<len;i++)
		printf("%x ", pkt[i]);
	printf("\n");
#endif

}


static void UnlockTimeout(void)
{
	PRINTF("Unlock Timeout\n");
	NetState = NETLOOP_FAIL;
}



/**
 * Sets ucSystemLocked flag to 0 if "false"
 */
void set_system_locked(bd_t *bd)
{
	const char *env_value;

#ifdef SERVICE_UBOOT
	bd->bi_ext.ucSystemLocked = 0;
#else
	if (strcmp(CONFIG_SYSTEM_LOCK_DEF, "false")==0)
	{
		bd->bi_ext.ucSystemLocked = 0;	// set default state
	}
	else
	{
		bd->bi_ext.ucSystemLocked = 1;
	}
	// try to set SystemLocked state from environment
	if ((env_value = (char*)getenv(bd, (uchar*)CONFIG_SYSTEM_LOCK_ENV)) != 0)
	{
		if (strcmp(env_value, "true")==0)
		{
			bd->bi_ext.ucSystemLocked = 1;
		}
		else
		{
			bd->bi_ext.ucSystemLocked = 0;
		}
	}
#endif
}




void unlock_system(bd_t *bd)
{
	if(!bd->bi_ext.ucSystemLocked)
	{
		PRINTF("System already unlocked\n");
		return;
	}
	//PRINTF("Unlock procedure: Start\n");

#if (CFG_BUTTON_DETECTION)
	SetPort(P2_09_MODE_REG, PORT_PULL_UP, PID_port);
	if((GetWord(P2_DATA_REG)&GPIO_9)==0) { //P2[9] is pulled to GND

		//PRINTF("Key pressed\n");
		NetSetTimeout(UNLOCK_TIMEOUT * CFG_HZ, UnlockTimeout);
		NetSetHandler(UnlockHandler);

		if(NetLoop(bd, UNLOCK))
		{
			bd->bi_ext.ucSystemLocked = 0;
			serial_unlock();
			printf("\nUnlocked\n");
		}
	}
#endif



	//PRINTF("Unlock procedure: End\n");
}







#endif

