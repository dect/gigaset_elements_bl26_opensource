/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * Boot support
 */
#include <armboot.h>
#include <command.h>
#include <cmd_net.h>
#include <net.h>
#include <malloc.h>

#if (CONFIG_COMMANDS & CFG_CMD_NET)

#ifdef CONFIG_CHAR_LCD
#include "char_lcd.h"
#endif

#ifdef CONFIG_GRAPH_LCD
#include "graph_lcd.h"
#endif



# if (CONFIG_COMMANDS & CFG_CMD_AUTOSCRIPT)
# include <cmd_autoscript.h>
# endif

extern int do_bootm (cmd_tbl_t *, bd_t *, int, int, char *[]);

static int netboot_common (int, cmd_tbl_t *, bd_t *, int , char *[]);

int do_bootp (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
	return netboot_common (BOOTP, cmdtp, bd, argc, argv);
}

int do_tftpb (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
#ifdef SC14452
	DISP_MESG(0," SC14452DK Boot ");
#else
	DISP_MESG(0," SC14450DK Boot ");
#endif
	DISP_MESG(1,"TFTP boot ...   ");

	return netboot_common (TFTP, cmdtp, bd, argc, argv);
}

int do_rarpb (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
	return netboot_common (RARP, cmdtp, bd, argc, argv);
}

#if (CONFIG_COMMANDS & CFG_CMD_DHCP)
int do_dhcp (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
	return netboot_common(DHCP, cmdtp, bd, argc, argv);
}
#endif	/* CFG_CMD_DHCP */

#if (CONFIG_COMMANDS & CFG_CMD_MEMORY)
extern int do_mem_ecp   (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
#ifdef CONFIG_DUAL_IMAGE_BOOT
extern void  swap_boot_address(bd_t *bd);
#endif

int do_tftpupgrade (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
	int my_argc = 0;
	char *my_argv[4] = {NULL, NULL, NULL, NULL};
	char* s1;
	int file_size = 0;
	int i;
	const char *p;
	ulong addr, checksum, my_checksum ;
	image_header_t* hdr ;
	const int min_image_size = CFG_MIN_IMAGE_SIZE;//0x80000 ;
	const int max_image_size = CFG_MAX_IMAGE_SIZE;//0x2B0000 ;

	for (i = 0; i < 4; i++) {
		my_argv[i] = malloc(64);
		if (my_argv[i] == NULL) {
			printf("Could not allocate memory!\n");
			goto _exit;
		}
	}

	printf("*** 1st step - TFTP image to SDRAM\n");
	my_argc = 3;
	/* copy command name */
	strcpy(my_argv[0], "tftpboot");

#ifdef CONFIG_DUAL_IMAGE_BOOT
	sprintf(my_argv[1],"%#0.8x",CFG_LOAD_ADDR);
#else

	/* copy load address */
	if (strlen(argv[2]) > 64) {
		printf("String %s too big!\n", argv[2]);
		goto _exit;	
	}
	strncpy(my_argv[1], argv[2], 64);
#endif


	/* copy file name */
	if (strlen(argv[1]) > 64) {
		printf("String %s too big!\n", argv[1]);
		goto _exit;	
	}
	strncpy(my_argv[2], argv[1], 64);

	/* call TFTP first */
	printf("*** Executing command: %s %s %s\n",
		my_argv[0], my_argv[1], my_argv[2]);
	netboot_common (TFTP, cmdtp, bd, my_argc, my_argv);

	/* check if the file we just downloaded looks alright */
	addr = simple_strtoul( my_argv[1], NULL, 16 ) ;
	hdr = (image_header_t*)addr ;
	if( SWAP32( hdr->ih_magic )  != IH_MAGIC ) {
		printf( "Bad Magic Number in image (%08lx) @%08lx\n",
					SWAP32( hdr->ih_magic ), addr ) ;
		goto _exit ;
	}
	else {
		printf( "Image Magic Number is correct\n" ) ;
	}
	file_size = simple_strtol( getenv( bd, "filesize" ), NULL, 16 ) ;
	/* assume that valid images cannot be less than 500KB...
	   they also shouldn't exceed the size of the size of the flash
	   partition reserved for the image */
	if( file_size < min_image_size ) {
		printf( "The image is suspiciously small (%d bytes)...\n"
			"Aborting upgrade...\n", file_size ) ;
		goto _exit ;
	}
	if( file_size > max_image_size ) {
		printf( "The image is too big (%d bytes) to fit in the reserved"
			" flash partition (%d bytes)\n", file_size,
			max_image_size ) ;
		goto _exit ;
	}
	/* finally, check the header CRC and the image CRC */
	checksum = hdr->ih_hcrc ;
	hdr->ih_hcrc = 0 ;
	my_checksum = crc32( 0, (uchar*)hdr, sizeof( image_header_t ) ) ;
	if( my_checksum != SWAP32( checksum ) ) {
		printf( "Bad header CRC!\nAborting upgrade...\n" ) ;
		goto _exit ;
	}
	hdr->ih_hcrc = checksum ;
	printf( "Image header:\n" ) ;
	print_image_hdr( hdr ) ;
	printf( "Checking image CRC...\n" ) ;
	my_checksum = crc32( 0, (uchar*)hdr + sizeof( image_header_t ),
						SWAP32( hdr->ih_size ) ) ;
	checksum = hdr->ih_dcrc ;
	if( my_checksum != SWAP32( checksum ) ) {
		printf( "Bad image CRC!\nAborting upgrade...\n" ) ;
		goto _exit ;
	}
	printf( "Image looks OK, continuing.\n" ) ;

	printf("\n*** 2nd step - write image to FLASH\n");
	my_argc = 4;
	strcpy(my_argv[0], "ecp.w");

	

#ifdef CONFIG_DUAL_IMAGE_BOOT
	/* copy load address */
	sprintf(my_argv[1],"%#0.8x",CFG_LOAD_ADDR);
	/* copy dest address */
	s1 = getenv (bd, "upgrade_pos");
	if(s1==NULL)
	{
		printf("No upgrade postiotion in environment\n");
		goto _exit;
	}

	strncpy(my_argv[2], s1, 64);
#else	
	/* copy load address */
	strncpy(my_argv[1], argv[2], 64);

	/* copy dest address */
	if (strlen(argv[3]) > 64) {
		printf("String %s too big!\n", argv[3]);
		goto _exit;	
	}
	strncpy(my_argv[2], argv[3], 64);
#endif

	/* copy data length (in 16-bit words) */
	p = getenv(bd, "filesize");
	file_size = simple_strtol(p, NULL, 16);
	printf("*** Will write %d (0x%s) bytes to FLASH... ***\n", 
		file_size, p);
	file_size = file_size/2 + file_size%2;
	printf("    ...which results into %d 16-bit words\n", file_size);
	sprintf(my_argv[3], "%x", file_size);

	/* then call ecp */
	printf("*** Executing command: %s %s %s %s\n",
		my_argv[0], my_argv[1], my_argv[2], my_argv[3]);
	do_mem_ecp(cmdtp, bd, flag, my_argc, my_argv);

#ifdef CONFIG_DUAL_IMAGE_BOOT
	swap_boot_address(bd);
#endif
_exit:
	for (i = 0; i < 4; i++)
		if (my_argv[i] != NULL)
			free(my_argv[i]);

	return 0;
}
#endif /* CFG_CMD_MEMORY */

static void netboot_update_env(void)
{
    char tmp[16] ;

    if (NetOurGatewayIP) {
	ip_to_string (NetOurGatewayIP, tmp);
	setenv(Net_bd, "gatewayip", tmp);
    }

    if (NetOurSubnetMask) {
	ip_to_string (NetOurSubnetMask, tmp);
	setenv(Net_bd, "netmask", tmp);
    }

    if (NetOurHostName[0])
	setenv(Net_bd, "hostname", NetOurHostName);

    if (NetOurRootPath[0])
	setenv(Net_bd, "rootpath", NetOurRootPath);

    if (NetOurIP) {
	ip_to_string (NetOurIP, tmp);
	setenv(Net_bd, "ipaddr", tmp);
    }

    if (NetServerIP) {
	ip_to_string (NetServerIP, tmp);
	setenv(Net_bd, "serverip", tmp);
    }

    if (NetOurDNSIP) {
	ip_to_string (NetOurDNSIP, tmp);
	setenv(Net_bd, "dnsip", tmp);
    }
}

static int
netboot_common (int proto, cmd_tbl_t *cmdtp, bd_t *bd, int argc, char *argv[])
{
	char *s;
	int rc = 0;
   
	switch (argc) {
	case 1:
		break;

	case 2:	/* only one arg - accept two forms:
		 * just load address, or just boot file name.
		 * The latter form must be written "filename" here.
		 */
		if (argv[1][0] == '"') {	/* just boot filename */
			copy_filename (BootFile, (uchar*)argv[1], sizeof(BootFile));
		} else {			/* load address	*/
			load_addr = simple_strtoul(argv[1], NULL, 16);
		}
		break;

	case 3:	load_addr = simple_strtoul(argv[1], NULL, 16);
		copy_filename (BootFile, (uchar*)argv[2], sizeof(BootFile));

		break;

	default: printf ("Usage:\n%s\n", cmdtp->usage);
		return 1;
	}

	if (NetLoop(bd, proto) == 0)
		return 0;

	/* NetLoop ok, update environment */
	netboot_update_env();

	/* Loading ok, check if we should attempt an auto-start */
	if (((s = getenv(Net_bd, "autostart")) != NULL) && (strcmp(s,"yes") == 0)) {
		char *local_args[2];
		local_args[0] = argv[0];
		local_args[1] = NULL;

		printf ("Automatic boot of image at addr 0x%08lX ...\n",
			load_addr);

		rc = do_bootm (cmdtp, bd, 0, 1, local_args);
	}

#ifdef CONFIG_AUTOSCRIPT
	if (rc == 0 && 
	    ((s = getenv(Net_bd, "autoscript")) != NULL) && (strcmp(s,"yes") == 0)) {
		printf("Running autoscript at addr 0x%08lX ...\n", load_addr);
		rc = autoscript (bd, load_addr);
	}
#endif

	return rc;
}

#endif	/* CFG_CMD_NET */
//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:49.652221564
//@@ -81,7 +81,9 @@
// 
// #if (CONFIG_COMMANDS & CFG_CMD_MEMORY)
// extern int do_mem_ecp   (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[]);
//+#ifdef CONFIG_DUAL_IMAGE_BOOT
// extern void  swap_boot_address(bd_t *bd);
//+#endif
// 
// int do_tftpupgrade (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
// {
//@@ -163,7 +165,7 @@
// 	/* finally, check the header CRC and the image CRC */
// 	checksum = hdr->ih_hcrc ;
// 	hdr->ih_hcrc = 0 ;
//-	my_checksum = crc32( 0, (char*)hdr, sizeof( image_header_t ) ) ;
//+	my_checksum = crc32( 0, (uchar*)hdr, sizeof( image_header_t ) ) ;
// 	if( my_checksum != SWAP32( checksum ) ) {
// 		printf( "Bad header CRC!\nAborting upgrade...\n" ) ;
// 		goto _exit ;
//@@ -172,7 +174,7 @@
// 	printf( "Image header:\n" ) ;
// 	print_image_hdr( hdr ) ;
// 	printf( "Checking image CRC...\n" ) ;
//-	my_checksum = crc32( 0, (char*)hdr + sizeof( image_header_t ),
//+	my_checksum = crc32( 0, (uchar*)hdr + sizeof( image_header_t ),
// 						SWAP32( hdr->ih_size ) ) ;
// 	checksum = hdr->ih_dcrc ;
// 	if( my_checksum != SWAP32( checksum ) ) {
//@@ -288,14 +290,14 @@
// 		 * The latter form must be written "filename" here.
// 		 */
// 		if (argv[1][0] == '"') {	/* just boot filename */
//-			copy_filename (BootFile, argv[1], sizeof(BootFile));
//+			copy_filename (BootFile, (uchar*)argv[1], sizeof(BootFile));
// 		} else {			/* load address	*/
// 			load_addr = simple_strtoul(argv[1], NULL, 16);
// 		}
// 		break;
// 
// 	case 3:	load_addr = simple_strtoul(argv[1], NULL, 16);
//-		copy_filename (BootFile, argv[2], sizeof(BootFile));
//+		copy_filename (BootFile, (uchar*)argv[2], sizeof(BootFile));
// 
// 		break;
// 
