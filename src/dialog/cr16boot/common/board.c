/*
 * (C) Copyright 2002
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include "armboot.h"
#include "command.h"
#include "devices.h"
#include "version.h"


#ifdef CONFIG_DRIVER_CS8900
extern void cs8900_get_enetaddr(uchar *addr);
#endif

#ifdef CONFIG_EPXA1DB_MAC_ADDR
extern void epxa1db_set_mac_addr(bd_t* bd);
#endif

extern int board_post_init(bd_t *bd);
/*
#ifdef CONFIG_CHAR_LCD
extern void lcdInit(void);
#endif
#ifdef CONFIG_GRAPH_LCD
extern void lcdInit(void);
#endif
*/

#ifdef CONFIG_CHAR_LCD
#include "char_lcd.h"
#endif

#ifdef CONFIG_GRAPH_LCD
#include "graph_lcd.h"
#endif


/*
 * Begin and End of memory area for malloc(), and current "brk"
 */
static  ulong   mem_malloc_start = 0;
static  ulong   mem_malloc_end   = 0;
static  ulong   mem_malloc_brk   = 0;

static void mem_malloc_init (ulong dest_addr)
{
    mem_malloc_start  = dest_addr;
    mem_malloc_end    = dest_addr + CONFIG_MALLOC_SIZE;
    mem_malloc_brk    = mem_malloc_start;

    memset ((void *)mem_malloc_start, 0, mem_malloc_end - mem_malloc_start);
}

void *sbrk (ptrdiff_t increment)
{
    ulong old = mem_malloc_brk;
    ulong new = old + increment;

    if ((new < mem_malloc_start) ||
	(new > mem_malloc_end) ) {
	return (NULL);
    }
    mem_malloc_brk = new;
    return ((void *)old);
}



/*
 * Breath some life into the board...
 *
 * Initialize an SMC for serial comms, and carry out some hardware
 * tests.
 *
 * The first part of initialization is running from Flash memory;
 * its main purpose is to initialize the RAM so that we
 * can relocate the monitor code to RAM.
 */
void start_armboot(void)
{
    bd_t bd;
    ulong size;

    /* set up bd strucuture */
    memset(&bd, 0, sizeof(bd));
    bd.bi_baudrate = CONFIG_BAUDRATE;

    /* basic cpu dependent setup */
    cpu_init(&bd);

    /* basic board dependent setup */
    board_init(&bd);

    /* initialize environment */
	#if ((defined VT_MS20_BOARD) || (defined VT_V1_BOARD))
		size = flash_init(&bd);
	#endif
	/* configure available FLASH banks */
	size = flash_init(&bd);

	/* read env sector */
    env_init(&bd);

#ifdef CONFIG_SYSTEM_LOCK_ENV
    /* set system lock state */
    set_system_locked(&bd);
#endif

#ifdef CONFIG_GIGA94_SUPPORT
    test_giga94_status(&bd);
#endif

    /* serial communications setup */
    serial_init(&bd);

    display_banner(&bd);

    /* set up execptions */
    interrupt_init(&bd);

    /* configure available RAM banks */
    dram_init(&bd);
    display_dram_config(&bd);

    /* configure available FLASH banks */
    size = flash_init(&bd);
    display_flash_config(&bd);

    /* armboot_end is defined in the board-specific linker script */
    mem_malloc_init(_armboot_real_end);

    /* initialize environment */
    env_relocate(&bd);


    /* enable exceptions */
    enable_interrupts();

//vm
#ifndef NO_DISPLAY
	lcdInit();
#endif

	board_post_init(&bd);

/*
 * FIXME: this should probably be rationalised into a standard call for
 * each board, e.g. enet_mac_init() - but this'll do for now.
 */
/*
#ifdef CONFIG_DRIVER_CS8900
    if (!getenv(&bd,"ethaddr") ) {
	cs8900_get_enetaddr(bd.bi_enetaddr);
    }
#endif

#ifdef CONFIG_EPXA1DB_MAC_ADDR
    epxa1db_set_mac_addr(&bd);
#endif

#ifdef BOARD_POST_INIT
    (&bd);
#endifboard_post_init
 */

#ifdef CONFIG_SYSTEM_LOCK_ENV
	unlock_system(&bd);
#endif

#ifdef SERVICE_UBOOT
	printf("\nThis is SERVICE U-Boot!\n\n");
#endif

    /* main_loop() can return to retry autoboot, if so just run it again. */
    for (;;) {
    	main_loop(&bd);
    }

    /* NOTREACHED - no way out of command loop except booting */
}

void hang(void)
{
	DISP_MESG(0," #FATAL ERROR#  ");
	DISP_MESG(1,"RESET THE BOARD ");
	puts ("### ERROR ### Please RESET the board ###\n");
	for (;;);
}
//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:49.652221564
//@@ -39,6 +39,7 @@
// extern void epxa1db_set_mac_addr(bd_t* bd);
// #endif
// 
//+extern int board_post_init(bd_t *bd);
// /*
// #ifdef CONFIG_CHAR_LCD
// extern void lcdInit(void);
//@@ -117,8 +118,21 @@
// 	#if ((defined VT_MS20_BOARD) || (defined VT_V1_BOARD))
// 		size = flash_init(&bd);
// 	#endif
//+	/* configure available FLASH banks */
//+	size = flash_init(&bd);
//+
//+	/* read env sector */
//     env_init(&bd);
// 
//+#ifdef CONFIG_SYSTEM_LOCK_ENV
//+    /* set system lock state */
//+    set_system_locked(&bd);
//+#endif
//+
//+#ifdef CONFIG_GIGA94_SUPPORT
//+    test_giga94_status(&bd);
//+#endif
//+
//     /* serial communications setup */
//     serial_init(&bd);
// 
//@@ -171,9 +185,18 @@
//     (&bd);
// #endifboard_post_init
//  */
//+
//+#ifdef CONFIG_SYSTEM_LOCK_ENV
//+	unlock_system(&bd);
//+#endif
//+
//+#ifdef SERVICE_UBOOT
//+	printf("\nThis is SERVICE U-Boot!\n\n");
//+#endif
//+
//     /* main_loop() can return to retry autoboot, if so just run it again. */
//     for (;;) {
//-	main_loop(&bd);
//+    	main_loop(&bd);
//     }
// 
//     /* NOTREACHED - no way out of command loop except booting */
