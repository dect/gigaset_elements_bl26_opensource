/*
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Alex Zuepke <azu@sysgo.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * CPU specific code
 */

#include "armboot.h"
#include "command.h"
#include "clps7111.h"
#include "asm.h"

void test_cache(void);

void cpu_init(bd_t *bd)
{

    /*
     * setup up stack if necessary
     */
}




void cleanup_before_linux(bd_t *bd)
{
    /*
     * this function is called just before we call linux
     * it prepares the processor for linux
     *
     * we turn off caches etc ...
     * and we set the CPU-speed to 73 MHz - see start.S for details
     */
    unsigned long i;

    disable_interrupts();

	icache_disable();
	dcache_disable();

	/* clear cache entries */

	SetWord(CACHE_CTRL_REG,0x0000);

	for (i=0; i<0x800;i+=4)
		*(unsigned long*)(0x9800 + i) = 0;

}




void do_reset (cmd_tbl_t *cmdtp, bd_t *bd, int flag, int argc, char *argv[])
{
    extern void reset_cpu(ulong addr);
	unsigned i;

	disable_interrupts();

	icache_disable();
	dcache_disable();

	/* clear cache entries */

	SetWord(CACHE_CTRL_REG,0x0000);

	for (i=0; i<0x800;i+=4)
		*(unsigned long*)(0x9800 + i) = 0;



	SetBits(DEBUG_REG,SW_RESET,1);

	printf("Reset Failed %d.....\n",i);

    /*NOTREACHED*/
}

void icache_enable(void)
{
	unsigned short temp;
	_spr_("cfg",temp);
	temp |= 0x10;	 //set bit 4
	_lpr_("cfg",temp);

}

void icache_disable(void)
{
	unsigned short temp;
	_spr_("cfg",temp);
	temp &= 0xFFEF;	 //reset bit 4
	_lpr_("cfg",temp);

}

int icache_status(void)
{
	unsigned short temp;
	_spr_("cfg",temp);

    return (temp & 0x10) != 0;
}

void dcache_enable(void)
{
   unsigned short temp;
  _spr_("cfg",temp);
  temp |= 0x04;	 //set bit 2
  _lpr_("cfg",temp);
}

void dcache_disable(void)
{
  unsigned short temp;
  _spr_("cfg",temp);
  temp &= 0xFFFB;	 //reset bit 2
  _lpr_("cfg",temp);

}

int dcache_status(void)
{
	unsigned short temp;
	_spr_("cfg",temp);

    return (temp & 0x04) != 0;
}

