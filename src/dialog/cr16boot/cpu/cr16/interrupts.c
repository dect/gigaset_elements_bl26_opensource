/*
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Alex Zuepke <azu@sysgo.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include "armboot.h"
#include "sitel_io.h"
#include "asm.h"


/* we always count down the max. */
#define TIMER_LOAD_VAL 0xffff

#define TIMER1_CFG_HZ 1152

/* macro to read the 16 bit timer */
#define READ_TIMER GetWord(TIMER1_RELOAD_N_REG)


#ifdef CONFIG_USE_IRQ
/* enable IRQ/FIQ interrupts */
void enable_interrupts (void)
{
	_ei_();
}


/* 
 * disable IRQ/FIQ interrupts
 * returns true if interrupts had been enabled before we disabled them
 */
int disable_interrupts (void)
{
	
	unsigned short temp;
	_spr_("psr",temp);


    return (temp & 0x200) == 0;
}
#else
void enable_interrupts (void)
{
    return;
}
int disable_interrupts (void)
{
    return 0;
}
#endif



void bad_mode(void)
{
    panic("Resetting CPU ...\n");
    SetBits(DEBUG_REG,SW_RESET,1);
}

#if 0
void show_regs(struct pt_regs * regs)
{
    unsigned long flags;
    
}


void do_undefined_instruction(struct pt_regs *pt_regs)
{
/*
    printf("undefined instruction\n");
    show_regs(pt_regs);
    bad_mode();
	*/
}

void do_software_interrupt(struct pt_regs *pt_regs)
{
	/*
    printf("software interrupt\n");
    show_regs(pt_regs);
    bad_mode();
	*/
}

void do_prefetch_abort(struct pt_regs *pt_regs)
{
	/*
    printf("prefetch abort\n");
    show_regs(pt_regs);
    bad_mode();
	*/
}

void do_data_abort(struct pt_regs *pt_regs)
{
	/*
    printf("data abort\n");
    show_regs(pt_regs);
    bad_mode();
	*/
}

void do_not_used(struct pt_regs *pt_regs)
{
	/*
    printf("not used\n");
    show_regs(pt_regs);
    bad_mode();
	*/
}

void do_fiq(struct pt_regs *pt_regs)
{
	/*
    printf("fast interrupt request\n");
    show_regs(pt_regs);
    bad_mode();
	*/
}

void do_irq(struct pt_regs *pt_regs)
{
	/*
    printf("interrupt request\n");
    show_regs(pt_regs);
    bad_mode();
	*/
}
#endif

static ulong timestamp;
static ulong lastdec;

extern void interrupt_init (bd_t *bd)
{
  unsigned short temp;
  
  temp = GetWord(TIMER_CTRL_REG);
  temp &=~(CLK_CTRL1|TIM1_CTRL|TIM1_MODE);
  temp |=(TIM1_CTRL|TIM1_MODE);
  SetWord(TIMER_CTRL_REG ,temp);
  
  SetWord(TIMER1_RELOAD_M_REG , TIMER_LOAD_VAL);
  SetWord(TIMER1_RELOAD_N_REG , TIMER_LOAD_VAL);
  // clear any pending interrupt interrupts
  SetWord(RESET_INT_PENDING_REG , TIM1_INT_PEND);

   lastdec = READ_TIMER;//TIMER_LOAD_VAL;
   timestamp = 0;
}

/*
 * timer without interrupts
 */

void reset_timer(void)
{
    reset_timer_masked();
}

ulong get_timer (ulong base)
{
    return get_timer_masked() - base;
}

void set_timer (ulong t)
{
    timestamp = t;
}

void udelay(unsigned long usec)
{
    ulong tmo;

  //  tmo = usec / 1000;
    tmo = usec*TIMER1_CFG_HZ;
    tmo /= 1000;

    tmo += get_timer(0);

    while(get_timer_masked() < tmo)
      /*NOP*/;
}

void reset_timer_masked(void)
{
    /* reset time */
    lastdec = READ_TIMER;
    timestamp = 0;
}

ulong get_timer_masked(void)
{
    ulong now = READ_TIMER;

    if (lastdec >= now)
    {
        /* normal mode */
        timestamp += lastdec - now;
    } else {
        /* we have an overflow ... */
        timestamp += lastdec + TIMER_LOAD_VAL - now;
    }
    lastdec = now;

    return timestamp;
}

void udelay_masked(unsigned long usec)
{
    ulong tmo;

   // tmo = usec / 1000;
    tmo = usec * TIMER1_CFG_HZ;
    tmo /= 1000;

    reset_timer_masked();

    while(get_timer_masked() < tmo)
      /*NOP*/;
}

//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:49.648888248
//@@ -80,12 +80,14 @@
//     SetBits(DEBUG_REG,SW_RESET,1);
// }
// 
//+#if 0
// void show_regs(struct pt_regs * regs)
// {
//     unsigned long flags;
//     
// }
// 
//+
// void do_undefined_instruction(struct pt_regs *pt_regs)
// {
// /*
//@@ -148,6 +150,7 @@
//     bad_mode();
// 	*/
// }
//+#endif
// 
// static ulong timestamp;
// static ulong lastdec;
