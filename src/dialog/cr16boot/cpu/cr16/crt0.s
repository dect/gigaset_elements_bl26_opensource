 ##############################################################################
 # crt0.s -- CR16 default start-up routine                                #
 #                                                                            #
 # Copyright (c) 2007 National Semiconductor Corporation                      #
 #                                                                            #
 # The authors hereby grant permission to use, copy, modify, distribute,      #
 # and license this software and its documentation for any purpose, provided  #
 # that existing copyright notices are retained in all copies and that this   #
 # notice is included verbatim in any distributions. No written agreement,    #
 # license, or royalty fee is required for any of the authorized uses.        #
 # Modifications to this software may be copyrighted by their authors         #
 # and need not follow the licensing terms described here, provided that      #
 # the new terms are clearly indicated on the first page of each file where   #
 # they apply.                                                                #
 #                                                                            #
 # This is the  start routine of your CR16 program.                           #
 # It is linked with your application automatically. You can use              #
 # this routine as a template and modify it to your needs, yet this           #
 # file must be supplied for the compiler.                                    #
 # It is assumed that the following symbols are defined in your linker        #
 # script: __STACK_START, __ISTACK_START                                      #
 ##############################################################################

	.text
#ifdef __CR16CP__
	.align 4
#else
	.align 2
#endif
	.extern	_main
	.extern	_atexit
	.extern	_exit
	.extern	_init
	.extern	_fini

	.extern	__STACK_START
	.extern	__ISTACK_START

	.global	_start
	.globl	__dispatch_table

_start:

 #----------------------------------------------------------------------------#
 # Initialize the stack pointers. The constants __STACK_START and             #
 # __ISTACK_START should be defined in the linker script.                     #

	movd	$__STACK_START, (sp)

	movd	$__ISTACK_START, (r1,r0)
	lprd	(r1,r0), isp

 #----------------------------------------------------------------------------#
 # Initialize the default sections according to the linker script.            #

	bal 	(ra), __init_bss_data

 #----------------------------------------------------------------------#
 # Set the Extended Dispatch bit in the CFG register. This is the       #
 # default configuration for CR16C.                                     #

	spr	cfg, r0			# Set dispatch table width
	orw	$0x100, r0
	lpr	r0, cfg

 #----------------------------------------------------------------------------#
 # Initialize the intbase (pointer to the dispatch table).                    #

	movd	$__dispatch_table, (r1,r0)
	lprd    (r1,r0), intbase

 #----------------------------------------------------------------------------#
 # Handle global and static constructurs execution and setup                  #
 # destructors to be called from exit.                                        #

	bal		(ra), _init
	movd	$_fini@c, (r3,r2)
	bal     (ra), _atexit

 #----------------------------------------------------------------------------#
 # Jump to the main function in your application.                             #

#ifdef __INT32__
	movd    $0, (r3,r2) # Number of arguments
	movd    $0, (r5,r4) # conatins pointer to argument string.
#else
	movw    $0, r2      # Number of arguments
	movd    $0, (r4,r3) # conatins pointer to argument string.
#endif
	bal		(ra), _main

 #----------------------------------------------------------------------------#
 # Upon returning from the main function (if it isn't an infinite loop),      #
 # jump to the exit function. The exit function is located in the             #
 # library 'libc.a'.                                                          #

#ifdef __INT32__
	movd	(r1,r0), (r3,r2) # _main return value gets forwarded.
#else
	movw	r0, r2		# _main return value gets forwarded.
#endif
	br		_exit		# returns control to the functional simulator.


#if 0
 #----------------------------------------------------------------------------#
 # memcpy optimized for CR16C, using LOADMP and STORMP instructions.          #

	.global fast_memcpy
# Assumptions:
#   R0 - R6 do not need to be preserved.
#   R3, R2 Contain a 32 bit pointer to the destination
#   R5, R4 Contain a 32 bit pointer to the source
#   R6 is the number of bytes to be copied

_fast_memcpy:  
  PUSH $7, r7             # back-up R7 to R12. R12 is 2 words
  MOVD (r5, r4), (r1, r0) # Source pointer in R1,R0 (loadmp)
  MOVD (r3, r2), (r7, r6) # Destination pointer in R7,R6 (stormp)
  LOADD 14(sp),(r12)      # Length

  CMPW $64, r12
  BGT block8

# If more than 64 bytes are to be transferred, we move a block with consecutive instructions to avoid the
# cost of test and branch.

block64:  
  LOADMP $8
  STORMP $8
  
  LOADMP $8
  STORMP $8
  
  LOADMP $8
  STORMP $8
  
  LOADMP $8
  STORMP $8
  
  ADDW $-64, r12
  CMPW $64, r12
  BLE block64

# The rest is transferred in 8-byte chunks. Since the cost of test and branch is not overwhelming, we only transfer 
# 8-byte blocks to get the counter below 8.

block8:   
  CMPW $8, r12
  BGT block4
          
b8again:  
  LOADMP $4
  STORMP $4
  
  ADDW $-8, r12
  CMPW $8, r12
  BLE b8again

 # Maybe this section is unnecessary, but we would like the function to be fast for lengths of 4-7 bytes as well...
block4:   
  CMPW $4, r12
  BGT block1

  LOADMP $2
  STORMP $2

  ADDW $-4, r12

# Transfer the rest byte for byte 
block1:   
  BEQ0W r12, continue
  LOADB 00(r1, r0), r2
  STORB r2,00(r7, r6)
  ADDD $1, (r1, r0)
  ADDD $1, (r7, r6)
  ADDW $-1, r12
  BR block1

# Restore and return.
continue: 
  POPRET $7, r7

#endif

