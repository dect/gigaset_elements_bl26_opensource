//to enable and disable caches
#include "asm.h"

#define ICACHE_ENABLE_BIT	1<<4
#define DCACHE_ENABLE_BIT	1<<2


void disable_icache(void)
{
  unsigned short temp;
  _spr_("cfg",temp);
  temp &= ~ICACHE_ENABLE_BIT;	 //reset bit 4
  _lpr_("cfg",temp);
}

void enable_icache(void)
{
   unsigned short temp;
  _spr_("cfg",temp);
  temp |= ICACHE_ENABLE_BIT;	 //set bit 4
  _lpr_("cfg",temp);
}

void enable_dcache(void)
{
   unsigned short temp;
  _spr_("cfg",temp);
  temp |= DCACHE_ENABLE_BIT;	 //set bit 2
  _lpr_("cfg",temp);
}

void disable_dcache(void)
{
  unsigned short temp;
  _spr_("cfg",temp);
  temp &= ~DCACHE_ENABLE_BIT;	 //reset bit 2
  _lpr_("cfg",temp);

}

//to lock cache (prevent load new code/data in cache)

void lock_icache(void)
{
   unsigned short temp;
  _spr_("cfg",temp);
  temp |= 0x20;	 //set bit 5
  _lpr_("cfg",temp);
}

void unlock_icache(void)
{
  unsigned short temp;
  _spr_("cfg",temp);
  temp &= 0xFFDF;	 //reset bit 5
  _lpr_("cfg",temp);
}

void lock_dcache(void)
{
  unsigned short temp;
  _spr_("cfg",temp);
  temp |= 0x08;	 //set bit 3
  _lpr_("cfg",temp);
}

void unlock_dcache(void)
{
  unsigned short temp;
  _spr_("cfg",temp);
  temp &= 0xFFF7;	 //reset bit 3
  _lpr_("cfg",temp);
}


void init_cache(unsigned short size)
{
  unsigned short i,len;
  volatile unsigned short temp;
 
 // int size = 6; /* init cache size */
 
  disable_icache();
  disable_dcache();

  if( size == 6 )
  {
    i   = 0x9800;
    len = 64*8;
  }
  else if( size == 5 )
  {
    i   = 0x9C00;
    len = 32*8;
  }
  else if( size == 4 )
  {
    i   = 0x9e00;
    len = 16*8;
  }
  else
  {
    i   = 0xa000;
    len = 0;
  }
  
  /* use DMA to speed up the init cache part - fill cache adm ram */
  *(volatile unsigned short*)0xFF4402 = 0;  //DMA0_A_STARTH_REG
  *(volatile unsigned short*)0xFF4400 = i;  //DMA0_A_STARTL_REG
  *(volatile unsigned short*)0xFF4406 = 0;  //DMA0_B_STARTH_REG
  *(volatile unsigned short*)0xFF4404 = i;  //DMA0_B_STARTL_REG
  *(volatile unsigned short*)0xFF440A = len -1;  //DMA0_LEN_REG
  *(volatile unsigned long *)(unsigned long)i =0;
  
  
  temp =  *(volatile unsigned short*)0xFF440C;   //DMA0_CTRL_REG
  temp &= 0xFFD9;
  temp |= 0x0024;  // increment the destination , set bus width to 32 bits
  *(volatile unsigned short*)0xFF440C = temp;
  
 
  temp =  *(volatile unsigned short*)0xFF440C;   //DMA0_CTRL_REG
  temp |= 0x0001;  // enable the DMA
  *(volatile unsigned short*)0xFF440C = temp;
  
  while ((*(volatile unsigned short*)0xFF440C & 1)); // wait until finished  
    
   
  // ConfigureCache
  *(volatile unsigned short*)0xFF500A = 1;      //CACHE_START0_REG
  *(volatile unsigned short*)0xFF5008 = 0x100;   //CACHE_LEN0_REG
  
  *(volatile unsigned short*)0xFF5006 = size;  // CACHE_CTRL_REG 
                                               // lock per cache
                                               // disable prefetch
                                               // disable 32 bytes burst mode
                                               // disable TEST1 mode
                                               // cache mode unified
  
  /* enable instruction cache */
   enable_icache();

  /* enable data cache */
  enable_dcache();
  }
