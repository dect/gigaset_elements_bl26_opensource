/*
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Alex Zuepke <azu@sysgo.de>
 *
 * Copyright (C) 1999 2000 2001 Erik Mouw (J.A.K.Mouw@its.tudelft.nl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "armboot.h"
#include "sitel_io.h"


void serial_setbrg(bd_t *bd, int baudrate)
{
    unsigned char baud = 1;
   
    if (baudrate == 9600)   baud = 0;
    else if (baudrate == 19200)  baud = 1;
    else if (baudrate == 57600)  baud = 2;
    else if (baudrate == 115200) baud = 3;
	else if (baudrate == 230400) baud = 4;
    else hang();

	SetBits( UART_CTRL_REG, BAUDRATE, baud );
 
}

  
/*
 * Initialise the serial port with the given baudrate. The settings
 * are always 8 data bits, no parity, 1 stop bit, 1 start bit.
 *
 */
void serial_init(bd_t *bd)
{
	const char *baudrate;

	if ((baudrate = (char*)getenv(bd, (uchar*)"baudrate")) != 0)
		bd->bi_baudrate = simple_strtoul(baudrate, NULL, 10);
	else
		bd->bi_baudrate = 115200;


	serial_setbrg(bd, bd->bi_baudrate);

	SetBits( RESET_INT_PENDING_REG, UART_RI_INT_PEND, 0 );
	SetBits( RESET_INT_PENDING_REG, UART_TI_INT_PEND, 0 );
	SetBits( UART_CTRL_REG, UART_TEN, 1 );
	SetBits( UART_CTRL_REG, UART_REN, 1 );


	if(bd->bi_ext.ucSystemLocked) {
		//printf("\nSystem locked\n");
		serial_lock();
	}
	else {
		//printf("System unlocked\n");
		serial_unlock();
	}
}
 
    

/*
 * Output a single byte to the serial port.
 */
void serial_putc(const char c)
{

  /* If \n, also do \r */
    if(c == '\n')
      serial_putc('\r');
 
/*clear buffer*/
	 while( GetBits(UART_CTRL_REG,TI)  ) 
{
	SetWord(UART_CLEAR_TX_INT_REG, 0x01);	/* clear TI this will start transmission */
}
	    
	  SetWord(UART_RX_TX_REG, c);			/* Transmit character */
	  

	while( GetBits(UART_CTRL_REG,TI) != 1 ) /* wait till previous transmition is completed*/ 
	   ; 	  

  
  
}

/*
  * Test whether a character is in the RX buffer
 */
unsigned char serial_tstc(void)
{
    return (unsigned char)(GetBits(UART_CTRL_REG,RI));
}

/*
 * Read a single byte from the serial port. Returns 1 on success, 0
 * otherwise. When the function is succesfull, the character read is
 * written into its argument c.
 */
unsigned char serial_getc(void)
{
	unsigned char ret;
	 while ( GetBits(UART_CTRL_REG,RI) != 1) /* wait till reception is completed*/
			;
      ret = GetWord(UART_RX_TX_REG); /* We really got a Byte from the other side */
 
      SetWord(UART_CLEAR_RX_INT_REG, 0x01);  
  
      return ret;
}


void serial_lock(void)
{
	/* Configure UART RX/TX as GPIO */
	SetPort(P0_00_MODE_REG, PORT_INPUT,    PID_port);
	SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_port);
}

void serial_unlock(void)
{
	/* Configure UART RX/TX as console input/output */
	SetPort(P0_00_MODE_REG, PORT_OUTPUT,    PID_UTX);
	SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_URX);
}
//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:49.648888248
//@@ -54,7 +54,7 @@
// {
// 	const char *baudrate;
// 
//-	if ((baudrate = getenv(bd, "baudrate")) != 0)
//+	if ((baudrate = (char*)getenv(bd, (uchar*)"baudrate")) != 0)
// 		bd->bi_baudrate = simple_strtoul(baudrate, NULL, 10);
// 	else
// 		bd->bi_baudrate = 115200;
//@@ -66,6 +66,16 @@
// 	SetBits( RESET_INT_PENDING_REG, UART_TI_INT_PEND, 0 );
// 	SetBits( UART_CTRL_REG, UART_TEN, 1 );
// 	SetBits( UART_CTRL_REG, UART_REN, 1 );
//+
//+
//+	if(bd->bi_ext.ucSystemLocked) {
//+		//printf("\nSystem locked\n");
//+		serial_lock();
//+	}
//+	else {
//+		//printf("System unlocked\n");
//+		serial_unlock();
//+	}
// }
//  
//     
//@@ -120,3 +130,18 @@
//   
//       return ret;
// }
//+
//+
//+void serial_lock(void)
//+{
//+	/* Configure UART RX/TX as GPIO */
//+	SetPort(P0_00_MODE_REG, PORT_INPUT,    PID_port);
//+	SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_port);
//+}
//+
//+void serial_unlock(void)
//+{
//+	/* Configure UART RX/TX as console input/output */
//+	SetPort(P0_00_MODE_REG, PORT_OUTPUT,    PID_UTX);
//+	SetPort(P0_01_MODE_REG, PORT_PULL_UP,   PID_URX);
//+}
