/* A stripped-down version of sk_buff
 */
 
#ifndef PK_BUFF_H
#define PK_BUFF_H

#include "malloc.h"


struct pk_buff {
	void *list_next; 	/* used by netif_rx */
        unsigned char* buf ;    /* the allocated memory starts from here */
        unsigned char* data ;   /* the current start of data start from here */
        int real_len ;          /* the total amount of memory allocated */
        int len ;               /* the size of the currently used data */
        
        unsigned short protocol ;
} ;


struct pk_buff* alloc_pkb( int size )
{
        struct pk_buff* skb = NULL ;
        
        skb = malloc( sizeof( struct pk_buff ) ) ;
        if( skb ) {
                skb->buf = malloc( size ) ;
                if( skb->buf ) {
                        skb->data = skb->buf ;
                        skb->real_len = size ;
                        skb->len = 0 ;
                }
                else {
                        free( skb ) ;
                        skb = NULL ;
                }
        }
        
        return skb ;
}

void free_pkb( struct pk_buff* skb )
{
        if( skb ) {
                if( skb->buf )
                        free( skb->buf ) ;

                free( skb ) ;
        }
}

inline unsigned char* pkb_pull( struct pk_buff* skb, int len )
{
        if( len > skb->len )
                return NULL ;
        
        skb->len -= len ;
        skb->data += len ;
        
        return skb->data ;
}

inline unsigned char* pkb_put( struct pk_buff* skb, int len )
{
        if( skb->len + len <= skb->real_len ) {
                skb->len += len ;
                return skb->data + len ;
        }
        
        return NULL ;
}

inline unsigned char* pkb_push( struct pk_buff* skb, unsigned int len )
{
        unsigned char* tmp = skb->data - len ;

        if( tmp >= skb->buf ) {
                skb->data = tmp ;
                skb->len += len ;
                return skb->data;
        }
        
        return NULL ;
}


#endif  /* PK_BUFF_H */
