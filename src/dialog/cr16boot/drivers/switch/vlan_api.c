/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
 /*============================================================================
 * Module Name: vlan_api.c
 * Purpose:
 * Author:
 * Date:
 * Notes:
 *
 *
 *=============================================================================
 */


/* INCLUDE FILE DECLARATIONS */
#include "switch.h"
#ifdef CONFIG_DRIVER_AX88783


/* NAMING CONSTANT DECLARATIONS */

/* GLOBAL VARIABLES DECLARATIONS */

/* LOCAL VARIABLES DECLARATIONS */

/* LOCAL SUBPROGRAM DECLARATIONS */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadTagBaseVlanConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_CONFIG
NapaReadTagBaseVlanConfig(SWITCH *pSwitch)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);

	if(TmpLong & L22PSR_1Q_ON)
		return ENABLE;
	else
		return DISABLE;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadVlanConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaWriteTagBaseVlanConfig(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);

	if(Set)
		TmpLong |= L22PSR_1Q_ON;
	else
		TmpLong &= ~L22PSR_1Q_ON;

	NapaWriteRegister(pSwitch, L2PSR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadTagBaseVlanConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_CONFIG
NapaReadPortBaseVlanConfig(SWITCH *pSwitch)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);

	if(TmpLong & L2PSR_VALN_ON)
		return ENABLE;
	else
		return DISABLE;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadVlanConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaWritePortBaseVlanConfig(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);

	if(Set)
		TmpLong |= L2PSR_VALN_ON;
	else
		TmpLong &= ~L2PSR_VALN_ON;

	NapaWriteRegister(pSwitch, L2PSR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigPortbaseMember()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_STATUS
NapaConfigPortbaseMember(SWITCH *pSwitch, PORTMAP_STURE *pPortBaseMember, unsigned char Group)
{
	unsigned char  Map = 0;
	unsigned long  TmpLong;
	NAPA_STATUS    Status = NAPA_STATUS_SUCCESS;

	SDBG(SDBG_VLAN, ("===> NapaConfigPortbaseMember\n\r"));
	
	if(Group > 2) {
		Status = NAPA_STATUS_NOT_SUPPORT;
		return Status;
	}
	
	TmpLong = NapaReadRegister(pSwitch, QDCR);

	if(pPortBaseMember->Port0)
		Map |= 1;
	if(pPortBaseMember->Port1)
		Map |= 2;
	if(pPortBaseMember->Port2)
		Map |= 4;
	
	TmpLong |= QDCR_VLAN_PORT_MAP(Group, Map);

	NapaWriteRegister(pSwitch, QDCR, TmpLong);

	SDBG(SDBG_VLAN, ("<=== NapaConfigPortbaseMember\n\r"));

	return Status;
} /* End of NapaConfigPortbaseMember() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadDefaultVlanConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaReadDefaultVlanConfig(SWITCH *pSwitch, PORT Port, TAG_VLAN_STURE *pVlan)
{
	unsigned long TmpLong;
	unsigned short Offset;

	SDBG(SDBG_VLAN, ("===> NapaReadDefaultVlanConfig"));

	Offset = P0QSR + Port * 0x40;
	TmpLong = NapaReadRegister(pSwitch, Offset);

	SDBG(SDBG_VLAN, ("Register0x%x = 0x%lx", Offset, TmpLong));

	pVlan->VlanID = QSR_GET_PVID(TmpLong);
	pVlan->Priority = QSR_GET_PRIORITY(TmpLong);

	SDBG(SDBG_VLAN, ("<=== NapaReadDefaultVlanConfig"));
} /* End of NapaReadDefaultVlanConfig() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaWriteDefaultVlanConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaWriteDefaultVlanConfig(SWITCH *pSwitch, PORT Port, TAG_VLAN_STURE *pVlan)
{
	unsigned long TmpLong;
	unsigned short Offset;

	SDBG(SDBG_VLAN, ("===> NapaWriteDefaultVlanConfig"));

	Offset = P0QSR + Port * 0x40;

	TmpLong = NapaReadRegister(pSwitch, Offset);

	TmpLong &= QSR_PVID_MASK;
	TmpLong &= QSR_PRIORITY_MASK;

	TmpLong |= pVlan->VlanID | QSR_SET_PRIORITY(pVlan->Priority);

	NapaWriteRegister(pSwitch, Offset, TmpLong);

	SDBG(SDBG_VLAN, ("Write Register0x%x = 0x%lx", Offset, TmpLong));

	SDBG(SDBG_VLAN, ("<=== NapaWriteDefaultVlanConfig"));
} /* End of NapaWriteDefaultVlanConfig() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadTagVlanConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaReadTagVlanConfig(SWITCH *pSwitch, TAG_VLAN_STURE *pVlan)
{
	unsigned long TmpLong;
	unsigned short Offset;

	SDBG(SDBG_VLAN, ("===> NapaReadTagVlanConfig"));

	Offset = pVlan->Entry * 4 + VENTRY0;
	TmpLong = NapaReadRegister(pSwitch, Offset);
	SDBG(SDBG_VLAN, ("Register0x%x = 0x%lx", Offset, TmpLong));

	if(TmpLong & VENTRY_VALID)
		pVlan->Valid = 1;
	else
		pVlan->Valid = 0;

	if(TmpLong & VENTRY_MEMBER_PORT0)
		pVlan->MemberPort0 = 1;
	else
		pVlan->MemberPort0 = 0;

	if(TmpLong & VENTRY_MEMBER_PORT1)
		pVlan->MemberPort1 = 1;
	else
		pVlan->MemberPort1 = 0;

	if(TmpLong & VENTRY_MEMBER_PORT2)
		pVlan->MemberPort2 = 1;
	else
		pVlan->MemberPort2 = 0;

	if(TmpLong & VENTRY_OUTPUT_TAG_PORT0)
		pVlan->OutputTagPort0 = 1;
	else
		pVlan->OutputTagPort0 = 0;

	if(TmpLong & VENTRY_OUTPUT_TAG_PORT1)
		pVlan->OutputTagPort1 = 1;
	else
		pVlan->OutputTagPort1 = 0;

	if(TmpLong & VENTRY_OUTPUT_TAG_PORT2)
		pVlan->OutputTagPort2 = 1;
	else
		pVlan->OutputTagPort2 = 0;

	pVlan->VlanID = VENTRY_VLANID(TmpLong);

	SDBG(SDBG_VLAN, ("pVlan->Entry = %d", pVlan->Entry));
	SDBG(SDBG_VLAN, ("pVlan->VlanID = %d", pVlan->VlanID));
	SDBG(SDBG_VLAN, ("pVlan->Valid = %d", pVlan->Valid));
	SDBG(SDBG_VLAN, ("pVlan->VENTRY_MEMBER_PORT0 = %d", pVlan->MemberPort0));
	SDBG(SDBG_VLAN, ("pVlan->VENTRY_MEMBER_PORT1 = %d", pVlan->MemberPort1));
	SDBG(SDBG_VLAN, ("pVlan->VENTRY_MEMBER_PORT2 = %d", pVlan->MemberPort2));
	SDBG(SDBG_VLAN, ("pVlan->VENTRY_OUTPUT_TAG_PORT0 = %d", pVlan->OutputTagPort0));
	SDBG(SDBG_VLAN, ("pVlan->VENTRY_OUTPUT_TAG_PORT1 = %d", pVlan->OutputTagPort1));
	SDBG(SDBG_VLAN, ("pVlan->VENTRY_OUTPUT_TAG_PORT2 = %d", pVlan->OutputTagPort2));

	SDBG(SDBG_VLAN, ("<=== NapaReadTagVlanConfig"));
} /* End of NapaReadTagVlanConfig() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadTagVlanConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaWriteTagVlanConfig(SWITCH *pSwitch, TAG_VLAN_STURE *pVlan)
{
	unsigned long TmpLong = 0;
	unsigned short Offset;

	SDBG(SDBG_VLAN, ("===> NapaWriteTagVlanConfig"));

	Offset = pVlan->Entry * 4 + VENTRY0;

	if(pVlan->MemberPort0)
		TmpLong |= VENTRY_MEMBER_PORT0;
	if(pVlan->MemberPort1)
		TmpLong |= VENTRY_MEMBER_PORT1;
	if(pVlan->MemberPort2)
		TmpLong |= VENTRY_MEMBER_PORT2;

	if(pVlan->OutputTagPort0)
		TmpLong |= VENTRY_OUTPUT_TAG_PORT0;
	if(pVlan->OutputTagPort1)
		TmpLong |= VENTRY_OUTPUT_TAG_PORT1;
	if(pVlan->OutputTagPort2)
		TmpLong |= VENTRY_OUTPUT_TAG_PORT2;

	if(pVlan->Valid)
		TmpLong |= VENTRY_VALID;

	TmpLong |= (unsigned long)pVlan->VlanID << 6;

	SDBG(SDBG_VLAN, ("Write Register0x%x = 0x%lx", Offset, TmpLong));
	NapaWriteRegister(pSwitch, Offset, TmpLong);

	SDBG(SDBG_VLAN, ("<=== NapaWriteTagVlanConfig"));
} /* End of NapaReadTagVlanConfig() */
#endif