/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
 /*============================================================================
 * Module Name: qos_api.c
 * Purpose:
 * Author:
 * Date:
 * Notes:
 *
 *
 *=============================================================================
 */

/* INCLUDE FILE DECLARATIONS */
#include "switch.h"
#ifdef CONFIG_DRIVER_AX88783


/* NAMING CONSTANT DECLARATIONS */

/* GLOBAL VARIABLES DECLARATIONS */

/* LOCAL VARIABLES DECLARATIONS */

/* LOCAL SUBPROGRAM DECLARATIONS */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWrite8021pConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWrite8021pConfig(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	if(Set)
		TmpLong |= L2PSR_1P_ON;
	else
		TmpLong &= ~L2PSR_1P_ON;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWrite8021pConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWriteTosConfig(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	if(Set)
		TmpLong |= L2PSR_TOS_ON;
	else
		TmpLong &= ~L2PSR_TOS_ON;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWrite8021pConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWriteCosConfig(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	if(Set)
		TmpLong |= L2PSR_COS_ON;
	else
		TmpLong &= ~L2PSR_COS_ON;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadRead8021pMapTable()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadRead8021pMapTable(SWITCH *pSwitch, QOS_1P_MAP *pMap)
{
	unsigned long TmpLong;
	unsigned short Offset;
	unsigned char i;

	Offset = pMap->Port * 0x40 + P0QMTBL;
	TmpLong = NapaReadRegister(pSwitch, Offset);

	for(i = 0; i < 8; i++)
	{
		pMap->Priority[i] = (unsigned char)(TmpLong >> (2 * i)) & QMTBL_PRIORITY_MASK;
	}
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWrite8021pMapTable()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWrite8021pMapTable(SWITCH *pSwitch, QOS_1P_MAP *pMap)
{
	unsigned long TmpLong;
	unsigned short Offset;
	unsigned char i;

	Offset = pMap->Port * 0x40 + P0QMTBL;
	TmpLong = 0;

	for(i = 0; i < 8; i++)
	{
		TmpLong |= (unsigned long)pMap->Priority[i] << (2 * i);
	}

	NapaWriteRegister(pSwitch, Offset, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadReadCosMapTable()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadReadTosMapTable(SWITCH *pSwitch, QOS_COSTOS_MAP *pMap)
{
	unsigned long TmpLong;
	unsigned char i;
	
	TmpLong = NapaReadRegister(pSwitch, PMTR);

	for(i = 0; i < 8; i++)
	{
		pMap->Priority[i] = (unsigned char)(TmpLong >> (i * 2)) & PMTR_PRIORITY_MASK;
	}
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWriteCosMapTable()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWriteTosMapTable(SWITCH *pSwitch, QOS_COSTOS_MAP *pMap)
{
	unsigned long TmpLong;
	unsigned char i;
	
	TmpLong = NapaReadRegister(pSwitch, PMTR) & ~PMTR_TOS_MASK;

	for(i = 0; i < 8; i++)
	{
		TmpLong |= (unsigned long)pMap->Priority[i] << (i * 2);
	}

	NapaWriteRegister(pSwitch, PMTR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadReadCosMapTable()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadReadCosMapTable(SWITCH *pSwitch, QOS_COSTOS_MAP *pMap)
{
	unsigned long TmpLong;
	unsigned char i;
	
	TmpLong = NapaReadRegister(pSwitch, PMTR);

	for(i = 0; i < 8; i++)
	{
		pMap->Priority[i] = (unsigned char)(TmpLong >> (16 + i * 2)) & PMTR_PRIORITY_MASK;
	}
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWriteCosMapTable()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWriteCosMapTable(SWITCH *pSwitch, QOS_COSTOS_MAP *pMap)
{
	unsigned long TmpLong;
	unsigned char i;
	
	TmpLong = NapaReadRegister(pSwitch, PMTR) & ~PMTR_COS_MASK;

	for(i = 0; i < 8; i++)
	{
		TmpLong |= (unsigned long)pMap->Priority[i] << (16 + i * 2);
	}

	NapaWriteRegister(pSwitch, PMTR, TmpLong);
}
#endif