/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
 /*============================================================================
 * Module Name: access_port.c
 * Purpose:
 * Author:
 * Date:
 * Notes:
 * $Log: access_port.c,v $
 * no message
 *
 *
 *=============================================================================
 */


#include <armboot.h>
#ifdef CONFIG_DRIVER_AX88783

#include <command.h>
#include <net.h>
#include <malloc.h>


/* INCLUDE FILE DECLARATIONS */
#include "access_port.h"
#include "switch.h"



/* NAMING CONSTANT DECLARATIONS */

/* GLOBAL VARIABLES DECLARATIONS */

/* wor around udelay resetting OCR */

/* LOCAL VARIABLES DECLARATIONS */
typedef void (*Write_Reg)(unsigned long value, void *membase);
typedef unsigned long(*Read_Reg)(void *membase);

//#define swap_addr_bit(x) (x&~0x180)|((x&0x100)>>1)|((x&0x80)<<1)

#define swap_addr_bit(x) (x)


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaAccessPortInit()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaAccessPortInit(unsigned char mode)
{
#if 0
	switch(mode) {
	case CR_CHIP_MODE_8BIT:
		pWrite_Reg = Write_Register_8bit;
		pRead_Reg = Read_Register_8bit;
		printf("NapaAccessPortInit(): 8-bit mode\n");
		break;
	case CR_CHIP_MODE_16BIT:
		pWrite_Reg = Write_Register_16bit;
		pRead_Reg = Read_Register_16bit;
		printf("NapaAccessPortInit(): 16-bit mode\n");
		break;
	case CR_CHIP_MODE_32BIT:
		pWrite_Reg = Write_Register_32bit;
		pRead_Reg = Read_Register_32bit;
		printf("NapaAccessPortInit(): 32-bit mode\n");
		break;
	default:
		printf("NapaAccessPortInit(): unknown mode %hu\n", mode);
		break;
	}
#endif

} /* End of NapaAccessPortInit() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadRegister()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned long
NapaReadRegister(SWITCH *pSwitch, unsigned short RegAddr)
{
	unsigned long	TmpLong,uw,lw;
	unsigned short temp = 2* RegAddr;
//	TmpLong = pRead_Reg((unsigned char *)pSwitch->pMemBase + temp);

	unsigned templ,temph;

	templ = swap_addr_bit((unsigned)pSwitch->pMemBase + temp);
	temph = swap_addr_bit((unsigned)pSwitch->pMemBase + temp+4);

//	printf("Read Addr: %X %X\n",templ,temph);

	lw = *(volatile unsigned short*)templ;
	uw = *(volatile unsigned short*)temph; 

	TmpLong = ((uw<<16)& 0xffff0000) | (lw&0xffff);



//	SDBG(SDBG_ACCESS, ("Read register 0x%x : 0x%lx\n\r", temp, TmpLong));

	return TmpLong;

} /* End of NapaReadRegister() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaWriteRegister()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaWriteRegister(SWITCH *pSwitch, unsigned short RegAddr, unsigned long Value)
{
	unsigned short temp = 2* RegAddr;
//	pWrite_Reg(Value, (unsigned char *)pSwitch->pMemBase + temp);
	unsigned templ,temph;

	templ = swap_addr_bit((unsigned)pSwitch->pMemBase + temp);
	temph = swap_addr_bit((unsigned)pSwitch->pMemBase + temp+4);

//	printf("Write Addr: %X %X\n",templ,temph);

	*(volatile unsigned short*)templ = Value;
	*(volatile unsigned short*)temph = Value>>16;

//	SDBG(SDBG_ACCESS, ("Write register 0x%x : 0x%lx\n\r", temp, Value));

} /* End of NapaWriteRegister() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaDelayMs()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaDelayMs(unsigned long Time)
{

	unsigned long i;
	for (i=0;i<Time;i++)
		udelay(1000);
//	mdelay(Time);

} /* End of NapaDelayMs() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaGetCurrentTime()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned long
NapaGetCurrentTime(void)
{

	return 0;//jiffies; //vm ?????????????

} /* End of NapaGetCurrentTime() */
#endif
