/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
 /*============================================================================
 * Module Name: stp_api.c
 * Purpose:
 * Author:
 * Date:
 * Notes:
 * $Log: stp_api.c,v $
 * no message
 *
 *
 *=============================================================================
 */

/* INCLUDE FILE DECLARATIONS */
#include "switch.h"
#ifdef CONFIG_DRIVER_AX88783


/* NAMING CONSTANT DECLARATIONS */

/* GLOBAL VARIABLES DECLARATIONS */

/* LOCAL VARIABLES DECLARATIONS */

/* LOCAL SUBPROGRAM DECLARATIONS */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigSTP()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigSTP(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	SDBG(SDBG_STP, ("=====> NapaConfigSTP\n\r"));

	TmpLong = NapaReadRegister(pSwitch, L2PSR);

	if(Set == ENABLE)
		TmpLong |= ENABLE_STP_SUPPOR;
	else
		TmpLong &= ~ENABLE_STP_SUPPOR;

	NapaWriteRegister(pSwitch, L2PSR, TmpLong);

	SDBG(SDBG_STP, ("<===== NapaConfigSTP\n\r"));
} /* End of NapaConfigSTP() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigPortState()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigPortState(SWITCH *pSwitch, PORT Port, PORT_STATE State)
{
	unsigned long TmpLong;

	SDBG(SDBG_STP, ("=====> NapaConfigPortState\n\r"));

	TmpLong = NapaReadRegister(pSwitch, QDCR);

	TmpLong |= SET_STATE(Port, State);

	NapaWriteRegister(pSwitch, QDCR, TmpLong);

	SDBG(SDBG_STP, ("<===== NapaConfigPortState\n\r"));
} /* End of NapaConfigPortState() */

#endif