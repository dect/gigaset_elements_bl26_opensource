/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
 /*============================================================================
 * Module Name: smac_api.c
 * Purpose:
 * Author:
 * Date:
 * Notes:
 * $Log: smac_api.c,v $
 * no message
 *
 *
 *=============================================================================
 */

/* INCLUDE FILE DECLARATIONS */
#include "switch.h"
#ifdef CONFIG_DRIVER_AX88783


/* NAMING CONSTANT DECLARATIONS */

/* GLOBAL VARIABLES DECLARATIONS */

/* LOCAL VARIABLES DECLARATIONS */

/* LOCAL SUBPROGRAM DECLARATIONS */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadStaticMac()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_STATUS
NapaReadStaticMac(SWITCH *pSwitch, unsigned char Entry, SMAC_STURE *pSMac)
{
	unsigned short Offset;
	NAPA_STATUS    Status = NAPA_STATUS_SUCCESS;
	unsigned long  TmpLong;

	while(1)
	{
		if(Entry >= MAX_STATIC_MAC) {
			Status = NAPA_STATUS_NOT_SUPPORT;
			break;
		}

		Offset = Entry * 8 + SMAC0DATA;

		TmpLong = NapaReadRegister(pSwitch, Offset);

		pSMac->Mac[0] = (unsigned char)TmpLong;
		pSMac->Mac[1] = (unsigned char)(TmpLong >> 8);
		pSMac->Mac[2] = (unsigned char)(TmpLong >> 16);
		pSMac->Mac[3] = (unsigned char)(TmpLong >> 24);

		Offset += 4;

		TmpLong = NapaReadRegister(pSwitch, Offset);

		pSMac->Mac[4] = (unsigned char)TmpLong;
		pSMac->Mac[5] = (unsigned char)(TmpLong >> 8);

		pSMac->SrcPort = SMACCR_GET_SRC_PORT(TmpLong);
		pSMac->SnifferSA = (TmpLong & SMACCR_SNIFFER_SA) == ENABLE ? ENABLE : 0;
		pSMac->SnifferDA = (TmpLong & SMACCR_SNIFFER_DA) == ENABLE ? ENABLE : 0;
		pSMac->SnifferPair = (TmpLong & SMACCR_SNIFFER_PAIR) == ENABLE ? ENABLE : 0;
		pSMac->X1SAMatch = (TmpLong & SMACCR_1X_SA_MATCH) == ENABLE ? ENABLE : 0;
		pSMac->FilterPair = (TmpLong & SMACCR_FILTER_PAIR) == ENABLE ? ENABLE : 0;
		pSMac->FilterSA = (TmpLong & SMACCR_FILTER_SA) == ENABLE ? ENABLE : 0;
		pSMac->FilterDA = (TmpLong & SMACCR_FILTER_DA) == ENABLE ? ENABLE : 0;
		pSMac->ForwardingOn = (TmpLong & SMACCR_FORWARDING_ON) == ENABLE ? ENABLE : 0;

		break;
	}

	return Status;

} /* End of NapaReadStaticMac() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadStaticMac()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_STATUS
NapaWriteStaticMac(SWITCH *pSwitch, unsigned char Entry, SMAC_STURE *pSMac)
{
	unsigned short Offset;
	NAPA_STATUS    Status = NAPA_STATUS_SUCCESS;
	unsigned long  TmpLong;

	while(1)
	{
		if(Entry >= MAX_STATIC_MAC) {
			Status = NAPA_STATUS_NOT_SUPPORT;
			break;
		}

		Offset = Entry * 8 + SMAC0DATA;

		TmpLong = 	((unsigned long)pSMac->Mac[0]) |
					((unsigned long)pSMac->Mac[1] << 8) |
					((unsigned long)pSMac->Mac[2] << 16) |
					((unsigned long)pSMac->Mac[3] << 24);

		NapaWriteRegister(pSwitch, Offset, TmpLong);

		Offset += 4;

		TmpLong = 	((unsigned long)pSMac->Mac[4]) |
					((unsigned long)pSMac->Mac[5] << 8);

		TmpLong |= SMACCR_SRC_PORT((unsigned long)pSMac->SrcPort);
		TmpLong |= pSMac->SnifferSA == ENABLE ? SMACCR_SNIFFER_SA : 0;
		TmpLong |= pSMac->SnifferDA == ENABLE ? SMACCR_SNIFFER_DA : 0;
		TmpLong |= pSMac->SnifferPair == ENABLE ? SMACCR_SNIFFER_PAIR : 0;
		TmpLong |= pSMac->X1SAMatch == ENABLE ? SMACCR_1X_SA_MATCH : 0;
		TmpLong |= pSMac->FilterPair == ENABLE ? SMACCR_FILTER_PAIR : 0;
		TmpLong |= pSMac->FilterSA == ENABLE ? SMACCR_FILTER_SA : 0;
		TmpLong |= pSMac->FilterDA == ENABLE ? SMACCR_FILTER_DA : 0;
		TmpLong |= pSMac->ForwardingOn == ENABLE ? SMACCR_FORWARDING_ON : 0;

		NapaWriteRegister(pSwitch, Offset, TmpLong);

		break;
	}

	return Status;

} /* End of NapaReadStaticMac() */
#endif