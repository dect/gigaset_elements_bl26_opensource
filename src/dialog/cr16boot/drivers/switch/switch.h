/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
/*=============================================================================
 * Module Name:switch.h
 * Purpose:
 * Author:
 * Date:
 * Notes:
 * $Log: switch.h,v $
 * Revision 0.0.0.1
 * no message
 *
 *
 *=============================================================================
 */

#ifndef __SWITCH_H__
#define __SWITCH_H__

/* INCLUDE FILE DECLARATIONS */
#include "access_port.h"

/* NAMING CONSTANT AND TYPE DECLARATIONS */
typedef unsigned char               PORT;
#define Port_0                      0
#define Port_1                      1
#define Port_2                      2

//Return Value definition
typedef int							NAPA_STATUS;
#define NAPA_STATUS_NO_DATA         1
#define NAPA_STATUS_SUCCESS         0
#define NAPA_STATUS_FAILURE         -1
#define NAPA_STATUS_NOT_SUPPORT     -2

typedef unsigned char               NAPA_CONFIG;
#define ENABLE                      1
#define DISABLE                     0

//
// RMON counter related definition
//
typedef struct _RMON_STURE {
	unsigned long        RxPkt;
	unsigned long        RxGoodPkt;
	unsigned long        RxByteLow;
	unsigned long        RxByteHigh;
	unsigned long        RxBroadcastPkt;
	unsigned long        RxMulticastPkt;
	unsigned long        RxPauseFrame;
	unsigned long        RxLenLess64;
	unsigned long        RxLen64;
	unsigned long        RxLenLess128;
	unsigned long        RxLenLess256;
	unsigned long        RxLenLess512;
	unsigned long        RxLenLess1024;
	unsigned long        RxLenLessMaximum;
	unsigned long        RxLenLargeMaximum;
	unsigned long        RxCrcErrPkt;
	unsigned long        RxAlignErrPkt;
	unsigned long        RxFragErrPkt;
	unsigned long        RxOverfolwPkt;
	unsigned long        RxFilterPkt;
	unsigned long        TxPkt;
	unsigned long        TxGoodPkt;
	unsigned long        TxByteLow;
	unsigned long        TxByteHigh;
	unsigned long        TxBroadcastPkt;
	unsigned long        TxMulticastPkt;
	unsigned long        TxPauseFrame;
	unsigned long        TxCollision;
	unsigned long        TxOneCollisionPkt;
	unsigned long        TxMultiCollisionPkt;
	unsigned long        TxExcessiveCollision;
	unsigned long        TxLateCollision;
} RMON_STURE, *PRMON_STURE;

#define MAX_RMONR_NUM               (sizeof(RMON_STURE) / sizeof(unsigned long))

//
//EEPROM related definition
//
#define EEPROM_MODE_93C46           1
#define EEPROM_MODE_93C56           2
#define EEPROM_MODE_93C66           3
#define EEPROM_MODE_93C76           4
#define EEPROM_MODE_93C86           5

#define EEPROM_MAX_NUM(Mode)        ((1024 * (1 << (Mode - 1)) / 8) / 5)
#define EEPROM_93C46_MAX_NUM        25
#define EEPROM_93C56_MAX_NUM        51
#define EEPROM_93C66_MAX_NUM        102
#define EEPROM_93C76_MAX_NUM        205
#define EEPROM_93C86_MAX_NUM        409

#define EEPROM_93C46_OPCODE(x)      (x << 15)
#define EEPROM_93C56_OPCODE(x)      (x << 17)
#define EEPROM_93C66_OPCODE(x)      (x << 17)
#define EEPROM_93C76_OPCODE(x)      (x << 19)
#define EEPROM_93C86_OPCODE(x)      (x << 19)

#define EEPROM_93C46_STARTBIT       (1 << 17)
#define EEPROM_93C56_STARTBIT       (1 << 19)
#define EEPROM_93C66_STARTBIT       (1 << 19)
#define EEPROM_93C76_STARTBIT       (1 << 21)
#define EEPROM_93C86_STARTBIT       (1 << 21)

#define EEPROM_READ                 0x02
#define EEPROM_EWEN                 0x00
#define EEPROM_ERASE                0x03
#define EEPROM_WRITE                0x01
#define EEPROM_ERALL                0x00
#define EEPROM_WRAL                 0x00
#define EEPROM_EWDS                 0x00

#define EEPROM_EWEN_93C46_ADDR      (0x03 << 5)
#define EEPROM_EWEN_93C56_ADDR      (0x03 << 7)
#define EEPROM_EWEN_93C66_ADDR      (0x03 << 7)
#define EEPROM_EWEN_93C76_ADDR      (0x03 << 9)
#define EEPROM_EWEN_93C86_ADDR      (0x03 << 9)

#define EEPROM_ERALL_93C46_ADDR     (0x02 << 5)
#define EEPROM_ERALL_93C56_ADDR     (0x02 << 7)
#define EEPROM_ERALL_93C66_ADDR     (0x02 << 7)
#define EEPROM_ERALL_93C76_ADDR     (0x02 << 9)
#define EEPROM_ERALL_93C86_ADDR     (0x02 << 9)

#define EEPROM_EWDS_93C46_ADDR      (0x00 << 5)
#define EEPROM_EWDS_93C56_ADDR      (0x00 << 7)
#define EEPROM_EWDS_93C66_ADDR      (0x00 << 7)
#define EEPROM_EWDS_93C76_ADDR      (0x00 << 9)
#define EEPROM_EWDS_93C86_ADDR      (0x00 << 9)

typedef struct _EEPROM_STURE {
	unsigned short       Num;
	unsigned short       Addr[EEPROM_93C86_MAX_NUM];
	unsigned long        Data[EEPROM_93C86_MAX_NUM];
} EEPROM_STURE;

//
//Forwarding table related definition
//
typedef struct _FORWARDING_TABLE_STRUE {
	unsigned char        Mac[6];
	unsigned char        IsStatic;
	PORT                 SrcPort;
	unsigned char        FilterDa;
	unsigned char        FilterSa;
	unsigned char        Page;
} FORWARDING_TABLE_STRUE;

#define FORWARDING_LINEAR_LEARNING	0
#define FORWARDING_HASH_LEARNING	1

//
//Tag Vlan definition
//
typedef struct
{
	unsigned short		VlanID;
	unsigned char		Priority;
	unsigned char		Entry;
	unsigned char		Valid;
	unsigned char		MemberPort0;
	unsigned char		MemberPort1;
	unsigned char		MemberPort2;
	unsigned char		OutputTagPort0;
	unsigned char		OutputTagPort1;
	unsigned char		OutputTagPort2;
} TAG_VLAN_STURE;

//
//Port map definition
//
typedef struct _PORTMAP_STURE {
    NAPA_CONFIG        Port0;
    NAPA_CONFIG        Port1;
    NAPA_CONFIG        Port2;
} PORTMAP_STURE;

//-------------------------------------------------------------------------
// STATIC MAC related definition
//-------------------------------------------------------------------------
typedef struct _SMAC_STURE {
    unsigned char      Mac[6];
	PORT               SrcPort;
    NAPA_CONFIG        SnifferSA;
    NAPA_CONFIG        SnifferDA;
    NAPA_CONFIG        SnifferPair;
    NAPA_CONFIG        X1SAMatch;
    NAPA_CONFIG        FilterPair;
    NAPA_CONFIG        FilterSA;
    NAPA_CONFIG        FilterDA;
    NAPA_CONFIG        ForwardingOn;
} SMAC_STURE;

#define MAX_STATIC_MAC              16

//
//Ethernet related definition
//
#define	MAX_ETHERNET_FRAME_SIZE     1518
#define	MIN_ETHERNET_FRAME_SIZE     60
#define	ETHERNET_HEADER_SIZE        14
#define	ETH_ADDR_LEN                6
#define	MAX_MULTICAST_LIST          8

//
//Receive filter definition
//
#define RX_NORMAL                   0
#define RX_PROMISCUOUS              1

//
//Spanning Tree related definition
//
typedef unsigned char               PORT_STATE;
#define PORT_STATE_DISABLE          0
#define PORT_STATE_LISTENING        1
#define PORT_STATE_LEARNING         2
#define PORT_STATE_FORWARDING       3
#define PORT_STATE_BLOCKING         PORT_STATE_DISABLE

#define ENABLE_STP_SUPPOR       (1 << 23) // Bits in Layer 2 Global Configuration Register

#define SET_STATE(Port, State)  ((unsigned long)State << (Port * 2))

//
// PHY related definition
//
typedef struct _LINK_STURE {
	unsigned char      Link;
	unsigned char      Speed;
	unsigned char      Duplex;
	unsigned char      FlowCtrl;
} LINK_STURE;

typedef struct _PHY_STURE {
	unsigned char      PhyID;
	unsigned char      PhyAddr;
	unsigned short     Value;
} PHY_STURE;

typedef struct _LED_STURE {
	unsigned char      Led0_Mode;
	unsigned char      Led1_Mode;
	unsigned char      Led2_Mode;
} LED_STURE;

#define PHY_MODE_MII                1
#define PHY_MODE_RMII               2

//
//802.1P Priority Mapping definition
//
typedef struct
{
	PORT			Port;
	unsigned char	Priority[8];
} QOS_1P_MAP;

//
//COS/TOS Mapping definition
//
typedef struct
{
	unsigned char	Priority[8];
} QOS_COSTOS_MAP;

//

// DMA related definition

//

typedef struct _DMA_DESC {

	unsigned long      status;

	unsigned long      length;

	unsigned long      buf_addr;

	unsigned long      next_desc;

} DMA_DESC, *PDMA_DESC;

//-------------------------------------------------------------------------
// Control/Status Registers (CSR)
//-------------------------------------------------------------------------
enum {
  CR          = 0x0000,		/* 0x00 Chip ID and Reset Register */
        #define CR_CHIP_MODE_8BIT              (0)
        #define CR_CHIP_MODE_16BIT             (1)
        #define CR_CHIP_MODE_32BIT             (2)
        #define CR_CHIP_MODE_SPI               (3)
        #define CR_CHIP_MODE_PCI               (4)
        #define CR_CHIP_MODE_MASK              (0x07)
        #define CR_CPI_RESET                   (1 << 26)
        #define CR_CPO_RESET                   (1 << 27)
        #define CR_CHIP_RESET                  (1 << 31)
  PCR         = 0x0004,		/* 0x04 PHY0/PHY1 Configuration Register */
        #define PCR_PHY0_RESET_CLEAR           (1)
        #define PCR_PHY1_RESET_CLEAR           (0x00010000)
  PSR         = 0x0008,		/* 0x08 PHY0/PHY1 Status Register */
  GMCR        = 0x000C,		/* 0x0C Global MAC Configuration Register */
        #define GMCR_MPL(Length)               (Longth & 0x7FF)
        #define GMCR_NO_ABORT                  (1 << 13)
        #define GMCR_MAX_STORM(Mode)           ((Mode & 0x03) << 14)
        #define GMCR_SUPER_MAC                 (1 << 21)
        #define GMCR_PTO                       (1 << 22)
        #define GMCR_CSJ                       (1 << 23)
        #define GMCR_GEN_CRC                   (1 << 26)
        #define GMCR_INT_ACTIVE_HIGH           (1 << 27)
        #define GMCR_INT_ACTIVE_LOW            (0)
        #define GMCR_INS_SRC_FROM_CPU          (1 << 28)
        #define GMCR_INS_SRC_TO_CPU            (1 << 29)
        #define GMCR_PCI_FIFO_ON               (1 << 31)
  L2PSR       = 0x0010,		/* 0x10 Layer 2 Protocol Setup Register */
        #define L2PSR_DIFF_SEL                 (1 << 0)
        #define L2PSR_COS_ON                   (1 << 1)
        #define L2PSR_TOS_ON                   (1 << 2)
        #define L2PSR_DSCP_ON                  (1 << 3)
        #define L2PSR_FILTER_MAC               (1 << 4)
        #define L2PSR_STOP_LEARNING            (1 << 5)
        #define L2PSR_ARP_TO_CPU               (1 << 6)
        #define L2PSR_HASH_MODE_LINEAR         (0)
        #define L2PSR_HASH_MODE_HASH           (1 << 7)
        #define L2PSR_CPIO_ON                  (1 << 9)
        #define L2PSR_MC_OVER_VLAN             (1 << 10)
        #define L2PSR_CTRL_PKT_TO_CPU          (1 << 12)
        #define L2PSR_IGMP_ON                  (1 << 13)
        #define L2PSR_IGMP_FORWARD_TO_CPU      (1 << 14)
        #define L2PSR_IGMP_COPY_TO_CPU         (0)
        #define L2PSR_QINQ_ON                  (1 << 15)
        #define L2PSR_1X_ON                    (1 << 16)
        #define L2PSR_GMRP_ON                  (1 << 17)
        #define L2PSR_GVRP_ON                  (1 << 18)
        #define L2PSR_GARP_ON                  (1 << 19)
        #define L2PSR_VALN_ON                  (1 << 20)
        #define L2PSR_1P_ON                    (1 << 21)
        #define L22PSR_1Q_ON                    (1 << 22)
        #define L2PSR_1D_ON                    (1 << 23)
  L2FCR       = 0x0014,		/* 0x14 Layer 2 Forwarding Control Register */
        #define L2FCR_LEARNING_ON(Port)        (1 << (12 + Port))
        #define L2FCR_AGING_TIME(Time)         ((Time) << 16)
        #define L2FCR_ONE_SA_RESET(Port)       (1 << (26 + Port))
        #define L2FCR_ONE_SA_ON                (1 << (29 + Port))
  FTDATA      = 0x0018,		/* 0x18 Layer 2 Forwarding Table Entry Read/Write Register 0 */
  FTCMD       = 0x001C,		/* 0x1C Layer 2 Forwarding Table Entry Read/Write Register 1 */
        #define FTCMD_WRITE_FT                 (1 << 31)
        #define FTCMD_READ_FT                  (1 << 30)
        #define FTCMD_FLUSH_FT                 (1 << 29)
        #define FTCMD_SER_BYPORT               (1 << 28)
        #define FTCMD_CONTI_FT                 (1 << 27)
        #define FTCMD_FT_PAGE0                 (0)
        #define FTCMD_FT_PAGE1                 (1 << 26)
        #define FTCMD_FT_PAGE(x)               (x << 26)
        #define FTCMD_FT_END                   (1 << 25)
        #define FTCMD_FT_VALID                 (1 << 24)
        #define FTCMD_FT_PORT1                 (1 << 23)
        #define FTCMD_FT_PORT0                 (1 << 22)
        #define FTCMD_FT_PORT(x)               (x << 22)
        #define FTCMD_FT_FLUSH_DOWN            (1 << 21)
        #define FTCMD_FT_STATIC                (1 << 20)
        #define FTCMD_FT_FILTER_DA             (1 << 19)
        #define FTCMD_FT_FILTER_SA             (1 << 18)
  QDCR        = 0x0020,		/* 0x20 Global 1Q/1D Configuration Register */
        #define QDCR_PORT_STATE(Port, State)   (State << (Port * 2))
        #define QDCR_VLAN_FILTER_ON(Port)      (1 << (8 + Port))
        #define QDCR_VLAN_PORT_MAP(Group, Map) (Map << (16 + Group * 4))
  SCR0        = 0x0024,		/* 0x24 Sniffer Configuration Register 0 */
  SCR1        = 0x0028,		/* 0x28 Sniffer Configuration Register 1 */
  SCR2        = 0x002C,		/* 0x2C Sniffer Configuration Register 2 */
  PMTR        = 0x0030,		/* 0x30 TOS and COS -> Priority Mapping Table */
        #define PMTR_PRIORITY_MASK             (0x03)
        #define PMTR_TOS_MASK                  (0x0000FFFF)
        #define PMTR_COS_MASK                  (0xFFFF0000)
  DQSR        = 0x0034,		/* 0x34 1Q-in-1Q Setup Register */
  PRCR        = 0x0038,		/* 0x38 Port Pair and RMON collision Configuration Register */
  MDCR        = 0x003C,		/* 0x3C MDIO Read/Write Control Register */
        #define MDCR_WRITE                     (1 << 31)
        #define MDCR_READ                      (1 << 30)
        #define MDCR_VALID                     (1 << 29)
        #define MDCR_PHY_ID(x)                 (x << 24)
        #define MDCR_PHY_REG(x)                (x << 16)
        #define MDCR_VALUE_MASK                (0xFFFF)
  TOCR        = 0x0040,		/* 0x40 TCPIP Offload Control Register */
        #define TOCR_INJECT_UDP_CKECKSUM       (1 << 0)
        #define TOCR_INJECT_TCP_CKECKSUM       (1 << 1)
        #define TOCR_INJECT_IGMP_CKECKSUM      (1 << 2)
        #define TOCR_INJECT_ICMP_CKECKSUM      (1 << 3)
        #define TOCR_INJECT_IP_CKECKSUM        (1 << 4)
        #define TOCR_INJECT_PPPOE_CKECKSUM     (1 << 5)
        #define TOCR_DROP_ERR_UDP_CHECKSUM     (1 << 8)
        #define TOCR_DROP_ERR_TCP_CHECKSUM     (1 << 9)
        #define TOCR_DROP_ERR_IGMP_CHECKSUM    (1 << 10)
        #define TOCR_DROP_ERR_ICMP_CHECKSUM    (1 << 11)
        #define TOCR_DROP_ERR_IP_CHECKSUM      (1 << 12)
        #define TOCR_DROP_ERR_PPPOE_CHECKSUM   (1 << 13)
  DATAPORT    = 0x004C,		/* 0x4C CPIO Control Register 2 */
        #define CPI_INSERT_PORT_ON             (1 << 15)
        #define CPI_INSERT_PORT_NUM(x)         ((unsigned short)(x) << 13)
		#define CPO_PORT_NUM(x)                ((unsigned char)((x) >> 15))
  SMAC0DATA   = 0x0050,		/* 0x50 Security Mac'x' Data Register */
  SMAC0CR     = 0x0054,		/* 0x54 Security Mac'x' Control Register */
        #define SMACCR_SRC_PORT(Port)          ((Port & 0x03) << 16)
        #define SMACCR_GET_SRC_PORT(x)         ((unsigned char)((x >> 16) & 0x03))
        #define SMACCR_SNIFFER_SA              (1 << 18)
        #define SMACCR_SNIFFER_DA              (1 << 19)
        #define SMACCR_SNIFFER_PAIR            (1 << 20)
        #define SMACCR_1X_SA_MATCH             (1 << 27)
        #define SMACCR_FILTER_PAIR             (1 << 28)
        #define SMACCR_FILTER_SA               (1 << 29)
        #define SMACCR_FILTER_DA               (1 << 30)
        #define SMACCR_FORWARDING_ON           (1 << 31)
  SMAC1DATA   = 0x0058,		/* 0x58 Security Mac'x' Data Register */
  SMAC1CR     = 0x005C,		/* 0x5C Security Mac'x' Control Register */
  SMAC2DATA   = 0x0060,		/* 0x60 Security Mac'x' Data Register */
  SMAC2CR     = 0x0064,		/* 0x64 Security Mac'x' Control Register */
  SMAC3DATA   = 0x0068,		/* 0x68 Security Mac'x' Data Register */
  SMAC3CR     = 0x006C,		/* 0x6C Security Mac'x' Control Register */
  SMAC4DATA   = 0x0070,		/* 0x70 Security Mac'x' Data Register */
  SMAC4CR     = 0x0074,		/* 0x74 Security Mac'x' Control Register */
  SMAC5DATA   = 0x0078,         /* 0x78 Security Mac'x' Data Register */
  SMAC5CR     = 0x007C,         /* 0x7C Security Mac'x' Control Register */
  SMAC6DATA   = 0x0080,         /* 0x80 Security Mac'x' Data Register */
  SMAC6CR     = 0x0084,		/* 0x84 Security Mac'x' Control Register */
  SMAC7DATA   = 0x0088,		/* 0x88 Security Mac'x' Data Register */
  SMAC7CR     = 0x008C,		/* 0x8C Security Mac'x' Control Register */
  VENTRY0     = 0x0090,		/* 0x90 VLAN Enrty */
        #define VENTRY_MEMBER_PORT0            (1)
        #define VENTRY_MEMBER_PORT1            (1 << 1)
        #define VENTRY_MEMBER_PORT2            (1 << 2)
        #define VENTRY_OUTPUT_TAG_PORT0        (1 << 3)
        #define VENTRY_OUTPUT_TAG_PORT1        (1 << 4)
        #define VENTRY_OUTPUT_TAG_PORT2        (1 << 5)
        #define VENTRY_VLANID_MASK             (0xFFFC003F)
        #define VENTRY_VLANID(Reg)             ((unsigned short)((Reg >> 6) & 0xFFF))
        #define VENTRY_VALID                   (1 << 18)
  VENTRY1     = 0x0094,		/* 0x94 VLAN Enrty */
  VENTRY2     = 0x0098,		/* 0x98 VLAN Enrty */
  VENTRY3     = 0x009C,		/* 0x9c VLAN Enrty */
  VENTRY4     = 0x00A0,		/* 0xA0 VLAN Enrty */
  VENTRY5     = 0x00A4,		/* 0xA4 VLAN Enrty */
  VENTRY6     = 0x00A8,		/* 0xA8 VLAN Enrty */
  VENTRY7     = 0x00AC,		/* 0xAC VLAN Enrty */
  VENTRY8     = 0x00B0,		/* 0xB0 VLAN Enrty */
  VENTRY9     = 0x00B4,		/* 0xB4 VLAN Enrty */
  VENTRY10    = 0x00B8,		/* 0xB8 VLAN Enrty */
  VENTRY11    = 0x00BC,		/* 0xBC VLAN Enrty */
  VENTRY12    = 0x00C0,		/* 0xC0 VLAN Enrty */
  VENTRY13    = 0x00C4,		/* 0xC4 VLAN Enrty */
  VENTRY14    = 0x00C8,		/* 0xC8 VLAN Enrty */
  VENTRY15    = 0x00CC,		/* 0xCC VLAN Enrty */
  BORDER      = 0X00D0,		/* 0xD0 Byte Order Control Register */
        #define BORDER_BIG                     (0)
        #define BORDER_LITTLE                  (1)
        #define BORDER_PCI                     (1 << 2)
  CSCR        = 0x00D4,		/* 0xD4 CPIO Start Command Register */
        #define CSCR_CPI_START                 (1 << 15)
        #define CSCR_CPO_START                 (1 << 31)
  MULTCR      = 0x00D8,		/* 0xD8 Multicast Control Register */
  LEDCR       = 0x00DC,		/* 0xDC LED COntrol Register */
        #define LEDCR_LED0(x)                  (x)
        #define LEDCR_LED1(x)                  (x << 8)
        #define LEDCR_LED2(x)                  (x << 16)
        #define LEDCR_PORT_LED_ON(Port)        (1 << (24 + Port))
        #define PHY_LED_SPEED                  (1 << 0)
        #define PHY_LED_DUPLEX                 (1 << 1)
        #define PHY_LED_LINK                   (1 << 2)
        #define PHY_LED_RX                     (1 << 3)
        #define PHY_LED_TX                     (1 << 4)
        #define PHY_LED_COLLISION              (1 << 5)
  RMONCR      = 0x00E0,		/* 0xE0 RMON Control Register */
        #define RMONCR_CLEAR                   (1 << 4)
        #define RMONCR_PORT(Port)              (Port << 13)
        #define RMONCR_RMON_ADDR(x)            (x << 8)
        #define RMONCR_READ                    (1 << 15)
  RMONDR      = 0x00E4,		/* 0xE4 RMON Data Register */
  DSCPTBL0    = 0x00E8,		/* 0xE8 DSCP Table Register0 */
  DSCPTBL1    = 0x00EC,		/* 0xEC DSCP Table Register1 */
  DSCPTBL2    = 0x00F0,		/* 0xF0 DSCP Table Register2 */
  DSCPTBL3    = 0x00F4,		/* 0xF4 DSCP Table Register3 */
  IMSR        = 0x00F8,		/* 0xF8 Interrupt Mask and Status Register */
        #define IMSR_INT_AGAIN                 (1 << 0)
        #define IMSR_INT_NEW_LEARN             (1 << 1)
        #define IMSR_INT_CPI_ERR               (1 << 2)
        #define IMSR_INT_CPO_EMPTY             (1 << 3)
        #define IMSR_INT_CPI_FULL              (1 << 4)
        #define IMSR_INT_CPI_CLEAR             (1 << 5)
        #define IMSR_INT_LINK_CHANGE           (1 << 6)
        #define IMSR_INT_PCI_RX                (1 << 7)
        #define IMSR_INT_PCI_TX                (1 << 8)
        #define IMSR_INT_PCI_RX_HALT           (1 << 9)
        #define IMSR_INT_PCI_TX_HALT           (1 << 10)
        #define IMSR_INT_TIMER                 (1 << 11)

        #define IMSR_MASK_AGAIN                (1 << 16)
        #define IMSR_MASK_NEW_LEARN            (1 << 17)
        #define IMSR_MASK_CPI_ERR              (1 << 18)
        #define IMSR_MASK_CPO_EMPTY            (1 << 19)
        #define IMSR_MASK_CPI_FULL             (1 << 20)
        #define IMSR_MASK_CPI_CLEAR            (1 << 21)
        #define IMSR_MASK_LINK_CHANGE          (1 << 22)
        #define IMSR_MASK_PCI_RX               (1 << 23)
        #define IMSR_MASK_PCI_TX               (1 << 24)
        #define IMSR_MASK_PCI_RX_HALT          (1 << 25) 
        #define IMSR_MASK_PCI_TX_HALT          (1 << 26)
        #define IMSR_MASK_TIMER                (1 << 27)
        #define IMSR_MASK_ALL                  0xFFFF0000
  PCDR        = 0x00FC,		/* 0xFC Page Count Debug Register */
  SUDPTCR     = 0x0100,		/* 0x100 Sniffer User Define Packet Type Configuration Register */
  WOLCR       = 0x0104,		/* 0x104 Wake-ON-Lan Configuration Register */
        #define WOLCR_PORT0_LINK_CHANGE        (1 << 0)
        #define WOLCR_PORT0_MAGIC_PACKET       (1 << 1)
        #define WOLCR_PORT0_WAKEUP_FRAME       (1 << 2)
        #define WOLCR_PORT0_CASCADE0           (1 << 3)
        #define WOLCR_PORT0_CASCADE1           (1 << 4)
        #define WOLCR_PORT1_LINK_CHANGE        (1 << 5)
        #define WOLCR_PORT1_MAGIC_PACKET       (1 << 6)
        #define WOLCR_PORT1_WAKEUP_FRAME       (1 << 7)
        #define WOLCR_PORT1_CASCADE0           (1 << 8)
        #define WOLCR_PORT1_CASCADE1           (1 << 9)
        #define WOLCR_WAKEUP_START             (1 << 16)
        #define WOLCR_WAKEUP_CLEAR             (1 << 18)
        #define WOLCR_PORT0_WAKEUP             (1 << 29)
        #define WOLCR_PORT1_WAKEUP             (1 << 30)
  WOLSR       = 0x0108,		/* 0x108 Wake-ON-Lan Setup Register */
        #define WOLSR_PORT0_OFFSET0(x)         (x)
        #define WOLSR_PORT0_OFFSET0_MASK       (0x0000000F)
        #define WOLSR_PORT0_OFFSET1(x)         (x << 4)
        #define WOLSR_PORT0_OFFSET1_MASK       (0x000000F0)
        #define WOLSR_PORT0_OFFSET2(x)         (x << 8)
        #define WOLSR_PORT0_OFFSET2_MASK       (0x00000F00)
        #define WOLSR_PORT0_MASK0_ON           (1 << 12)
        #define WOLSR_PORT0_MASK1_ON           (1 << 13)
        #define WOLSR_PORT0_MASK2_ON           (1 << 14)
        #define WOLSR_PORT0_DA_MATCH           (1 << 15)

        #define WOLSR_PORT1_OFFSET0(x)         (x << 16)
        #define WOLSR_PORT1_OFFSET0_MASK       (0x000F0000)
        #define WOLSR_PORT1_OFFSET1(x)         (x << 20)
        #define WOLSR_PORT1_OFFSET1_MASK       (0x00F00000)
        #define WOLSR_PORT1_OFFSET2(x)         (x << 24)
        #define WOLSR_PORT1_OFFSET2_MASK       (0x0F000000)
        #define WOLSR_PORT1_MASK0_ON           (1 << 28)
        #define WOLSR_PORT1_MASK1_ON           (1 << 29)
        #define WOLSR_PORT1_MASK2_ON           (1 << 30)
        #define WOLSR_PORT1_DA_MATCH           (1 << 31)
  PORT0MASK0  = 0x010C,		/* 0x10C - 0x114 Port0 Wake-Up Frame Mask */
  PORT0MASK1  = 0x0110,		/* 0x110 - 0x114 Port0 Wake-Up Frame Mask */
  PORT0MASK2  = 0x0114,		/* 0x114 - 0x114 Port0 Wake-Up Frame Mask */
  PORT0CRC0   = 0x0118,		/* 0x118 - 0x120 Port0 Wake-Up Frame CRC */
  PORT0CRC1   = 0x011C,		/* 0x11C - 0x120 Port0 Wake-Up Frame CRC */
  PORT0CRC2   = 0x0120,		/* 0x120 - 0x120 Port0 Wake-Up Frame CRC */
  PORT1MASK0  = 0x0124,		/* 0x124 - 0x12C Port0 Wake-Up Frame Mask */
  PORT1MASK1  = 0x0128,		/* 0x128 - 0x12C Port0 Wake-Up Frame Mask */
  PORT1MASK2  = 0x012C,		/* 0x12C - 0x12C Port0 Wake-Up Frame Mask */
  PORT1CRC0   = 0x0130,		/* 0x130 - 0x138 Port0 Wake-Up Frame CRC */
  PORT1CRC1   = 0x0134,		/* 0x134 - 0x138 Port0 Wake-Up Frame CRC */
  PORT1CRC2   = 0x0138,		/* 0x138 - 0x138 Port0 Wake-Up Frame CRC */
  POLLCR      = 0X0140,         /* 0x140 Auto-polling control Registers */
        #define POLLCR_PORT0_PHYID_MASK        (0xFFFFFFE0)
        #define POLLCR_PORT0_PHYID(x)          (x)
        #define POLLCR_PORT1_PHYID_MASK        (0xFFFFE0FF)
        #define POLLCR_PORT1_PHYID(x)          (x << 8)
        #define POLLCR_PORT2_PHYID_MASK        (0xFFE0FFFF)
        #define POLLCR_PORT2_PHYID(x)          (x << 16)
        #define POLLCR_PORT0_AUTO_POOLING      0x11000000
        #define POLLCR_PORT0_AUTO_POOLING_MASK 0xEEFFFFFF
        #define POLLCR_PORT1_AUTO_POOLING      0x22000000
        #define POLLCR_PORT1_AUTO_POOLING_MASK 0xDDFFFFFF
        #define POLLCR_PORT2_AUTO_POOLING      0x44000000
        #define POLLCR_PORT2_AUTO_POOLING_MASK 0xBBFFFFFF
  EECR        = 0x0144,         /* 0x144 EEPROM Control Register */
        #define EECR_CHK                       (1 << 24)
        #define EECR_MODE(x)                   (x << 27)
        #define EECR_VALID                     (1 << 30)
        #define EECR_REQ                       (1 << 31)
        #define EECR_ADDR(x)                   (x << 8)
  BLCR        = 0x0148,   /* Boot Loader Control Register */
        #define BLCR_DONE                      (1 << 30)
        #define BLCR_RELOAD                    (1 << 31)
  IGENTRY0    = 0x0150,   /* 0x150 IGMP Entry 0 */
  IGENTRY1    = 0x0154,   /* 0x154 IGMP Entry 1 */
  IGENTRY2    = 0x0158,   /* 0x158 IGMP Entry 2 */
  IGENTRY3    = 0x015C,   /* 0x15C IGMP Entry 3 */
  IGENTRY4    = 0x0160,   /* 0x160 IGMP Entry 4 */
  IGENTRY5    = 0x0164,   /* 0x164 IGMP Entry 5 */
  IGENTRY6    = 0x0168,   /* 0x168 IGMP Entry 6 */
  IGENTRY7    = 0x016C,   /* 0x16C IGMP Entry 7 */
  MULTIFILTER = 0x01B0,   /* 0x1B0 Multicast Filters*/
  MIICR       = 0x01F0,   /* 0x1F0 MII/RMII/RevMII Configuration Register */
        #define MIICR_PORT0_MII_CLK_GEN        (1 << 0)
        #define MIICR_PORT1_MII_CLK_GEN        (1 << 1)
        #define MIICR_PORT2_MII_CLK_GEN        (1 << 2)
        #define MIICR_PORT0_PHY_RMII           (1 << 4)
        #define MIICR_PORT1_PHY_RMII           (1 << 5)
        #define MIICR_PORT2_PHY_RMII           (1 << 6)
  CSLEEPCR    = 0x01F4,   /* 0x1F4 Clear Sleep Mode Control Register */
  ENDIANCR    = 0x01F8,   /* 0x1F8 Endian Control Register */
  TIMERCR     = 0x01FC,   /* 0x1FC Timer Control Register */
  P0MCR       = 0x0200,   /* 0x200 Port 0 MAC Configuration Register */
        #define MCR_MAC_ON                     (1 << 0)
        #define MCR_SPEED100                   (1 << 3)
        #define MCR_DUPLEX_FULL                (1 << 4)
        #define MCR_FLOWCTRL_ON                (1 << 7)
        #define MCR_CRC_CHECK                  (1 << 8)
        #define MCR_DA_MATCH                   (1 << 18)
        #define MCR_UniRxStart                 (MCR_FLOWCTRL_ON | MCR_CRC_CHECK | MCR_DA_MATCH)
        #define MCR_PromisRxStart              (MCR_FLOWCTRL_ON | MCR_CRC_CHECK)
        #define MCR_MAC_STOP                   (0)
  P0QMTBL     = 0x0204,   /* 0x204 Port 0 RX/TX QoS Mapping Table */
        #define QMTBL_PRIORITY_MASK            (0x03)
  P0QSR       = 0x0208,   /* 0x208 Port 0 1Q per Port Setting Register */
        #define QSR_GET_PVID(x)                ((unsigned short)((x) & 0xFFF))
        #define QSR_PVID_MASK                  (0xFFFFF000)
        #define QSR_PRIORITY_MASK              (0xFFFF1FFF)
        #define QSR_SET_PRIORITY(Pri)          (((unsigned long)(Pri)) << 13)
        #define QSR_GET_PRIORITY(Reg)          ((unsigned char)(((Reg) >> 13) & 0x07))
  P0RQRL0     = 0x020C,   /* 0x20C Port 0 RX per Queue Rate Limit 0 */
  P0RQRL1     = 0x0210,   /* 0x210 Port 0 RX per Queue Rate Limit 1 */
  P0TQRL0     = 0x0214,   /* 0x214 Port 0 TX per Queue Rate Limit 0 */
  P0TQRL1     = 0x0218,   /* 0x218 Port 0 TX per Queue Rate Limit 1 */
  P0RL        = 0x021C,   /* 0x21C Port 0 RX/TX per Port Rate Limit /FlowControl High/Low Watermark */
  P0CTSR      = 0x0220,   /* 0x220 Port 0 Ingress/Egress Cycle Time Setup Register */
  P0WATERMARK = 0x0224,   /* 0x224 Flow Control High/Low Watermark Register */
  P0BMU       = 0x0228,   /* 0x228 BMU Weighting Configuration Register */
  P0RXDROP    = 0x022C,   /* 0x22C RX Checksum Drop Counter Register */
  P0MAC0      = 0x0230,   /* 0x230 Default MAC Register */
  P0MAC1      = 0x0234,   /* 0x234 Default MAC Register */
  P1MCR       = 0x0240,   /* 0x240 Port 1 MAC Configuration Register */
  P1QMTBL     = 0x0244,   /* 0x244 Port 1 RX/TX QoS Mapping Table */
  P1QSR       = 0x0248,   /* 0x248 Port 1 1Q per Port Setting Register */
  P1RQRL0     = 0x024C,   /* 0x24C Port 1 RX per Queue Rate Limit 0 */
  P1RQRL1     = 0x0250,   /* 0x250 Port 1 RX per Queue Rate Limit 1 */
  P1TQRL0     = 0x0254,   /* 0x254 Port 1 TX per Queue Rate Limit 0 */
  P1TQRL1     = 0x0258,   /* 0x258 Port 1 TX per Queue Rate Limit 1 */
  P1RL        = 0x025C,   /* 0x25C Port 1 RX/TX per Port Rate Limit /FlowControl High/Low Watermark */
  P1CTSR      = 0x0260,   /* 0x260 Port 1 Ingress/Egress Cycle Time Setup Register */
  P1WATERMARK = 0x0264,   /* 0x264 Flow Control High/Low Watermark Register */
  P1BMU       = 0x0268,   /* 0x268 BMU Weighting Configuration Register */
  P1RXDROP    = 0x026C,   /* 0x26C RX Checksum Drop Counter Register */
  P1MAC0      = 0x0270,   /* 0x270 Default MAC Register */
  P1MAC1      = 0x0274,   /* 0x274 Default MAC Register */
  P2MCR       = 0x0280,   /* 0x280 Port 2 MAC Configuration Register */
  P2QMTBL     = 0x0284,   /* 0x284 Port 2 RX/TX QoS Mapping Table */
  P2QSR       = 0x0288,   /* 0x288 Port 2 1Q per Port Setting Register */
  P2RQRL0     = 0x028C,   /* 0x28C Port 2 RX per Queue Rate Limit 0 */
  P2RQRL1     = 0x0290,   /* 0x290 Port 2 RX per Queue Rate Limit 1 */
  P2TQRL0     = 0x0294,   /* 0x294 Port 2 TX per Queue Rate Limit 0 */
  P2TQRL1     = 0x0298,   /* 0x298 Port 2 TX per Queue Rate Limit 1 */
  P2RL        = 0x029C,   /* 0x29C Port 2 RX/TX per Port Rate Limit /FlowControl High/Low Watermark */
  P2CTSR      = 0x02A0,   /* 0x2A0 Port 2 Ingress/Egress Cycle Time Setup Register */
  P2WATERMARK =	0x02A4,   /* 0x2A4 Flow Control High/Low Watermark Register */
  P2BMU       =	0x02A8,   /* 0x2A8 BMU Weighting Configuration Register */
  P2RXDROP    =	0x02AC,   /* 0x2AC RX Checksum Drop Counter Register */
  P2MAC0      =	0x02B0,   /* 0x2B0 Default MAC Register */
  P2MAC1      =	0x02B4,   /* 0x2B4 Default MAC Register */
        #define DMA_OWN                        (1 << 31)
  DMARXCR     = 0x02C0,   /* 0x2C0 RX DMA Control Register */
  DMARXDESCPA = 0x02C4,   /* 0x2C4 First RX Descriptor Bus Address */
  DMATXCR     =	0x02C8,   /* 0x2C8 TX DMA Control Register */
  DMATXDESCPA =	0x02CC,   /* 0x2CC First TX Descriptor Bus Address */
  DMAHWTX     = 0x02D0,   /* 0x2D0 Current Transmit desc address */
  DMAHWRX     = 0x02D4    /* 0x2D0 Current receive desc address */
};


/* EXPORTED SUBPROGRAM SPECIFICATIONS */

//
// Initialize APIs
//
void NapaGetMacAddress(SWITCH *pSwitch, PORT Port, unsigned char *pMac);
void NapaSetMacAddress(SWITCH *pSwitch, PORT Port, unsigned char *pMac);
unsigned char NapaGetChipMode(SWITCH *pSwitch);
void NapaChipReset(SWITCH *pSwitch);
void NapaLocalBusCpiReset(SWITCH *pSwitch);
void NapaLocalBusCpoReset(SWITCH *pSwitch);
void NapaConfigLocalBusCpio(SWITCH *pSwitch, NAPA_CONFIG Set);
void NapaSetRxFilter(SWITCH *pSwitch, PORT Port, unsigned char Filter);
void NapaConfigPciFifo(SWITCH *pSwitch, NAPA_CONFIG Set);
void NapaPciSetTxDesc(SWITCH *pSwitch, unsigned long PhyAddr);
void NapaPciSetRxDesc(SWITCH *pSwitch, unsigned long PhyAddr);
void NapaPciTxStart(SWITCH *pSwitch);
void NapaPciRxStart(SWITCH *pSwitch);
void NapaConfigTcpOffload(SWITCH *pSwitch, NAPA_CONFIG Inject, NAPA_CONFIG Drop);
void NapaConfigIpOffload(SWITCH *pSwitch, NAPA_CONFIG Inject, NAPA_CONFIG Drop);
void NapaConfigUdpOffload(SWITCH *pSwitch, NAPA_CONFIG Inject, NAPA_CONFIG Drop);
void NapaConfigIcmpOffload(SWITCH *pSwitch, NAPA_CONFIG Inject, NAPA_CONFIG Drop);
void NapaConfigIgmpOffload(SWITCH *pSwitch, NAPA_CONFIG Inject, NAPA_CONFIG Drop);
void NapaConfigPPPoEOffload(SWITCH *pSwitch, NAPA_CONFIG Inject, NAPA_CONFIG Drop);
void NapaSetPktOrder(SWITCH *pSwitch, unsigned char Order);
void NapaSetMultiFilter(SWITCH *pSwitch, PORT Port,int Num, unsigned char *Mac);
void NapaCleanMultiFilter(SWITCH *pSwitch, unsigned short Offset);
// End of Initialize APIs

//
// Interrupt APIs
//
void NapaDisableInterrupt(SWITCH *pSwitch);
void NapaEnableInterrupt(SWITCH *pSwitch);
void NapaConfigInterruptLevel(SWITCH *pSwitch, unsigned char Level);
unsigned long NapaReadInterruptMask(SWITCH *pSwitch);
void NapaSetInterruptMask(SWITCH *pSwitch, unsigned long Mask);
void NapaAddInterruptMask(SWITCH *pSwitch, unsigned long Mask);
void NapaRemoveInterruptMask(SWITCH *pSwitch, unsigned long Mask);
unsigned short NapaReadInterrupt(SWITCH *pSwitch);
void NapaDisableAndAckInterrupt(SWITCH *pSwitch, unsigned short Isr);
// End of Interrupt APIs

//
// Tx/Rx APIs
//
void NapaLocalBusConfigInsPort(SWITCH *pSwitch, NAPA_CONFIG Insert);
NAPA_STATUS NapaLocalBusSendPacket2Port(SWITCH *pSwitch, unsigned short Len, unsigned char *pBuf, PORT Port);
NAPA_STATUS NapaLocalBusSendPacket(SWITCH *pSwitch, unsigned short Len, unsigned char *pBuf);
NAPA_STATUS NapaLocalBusGetRxPktLen(SWITCH *pSwitch, unsigned short *Len, PORT *Port);
void NapaLocalBusGetRxPkt(SWITCH *pSwitch, unsigned char *pBuf, unsigned short PktLen);
// End of Tx/Rx APIs

//
// PHY APIs
//
unsigned char NapaGetPhyId(SWITCH *pSwitch, PORT Port);
void NapaSetPhyId(SWITCH *pSwitch, PORT Port, unsigned char Phyid);
void NapaSetPhyMode(SWITCH *pSwitch, PORT Port, unsigned char Mode);
void NapaEnablePhy(SWITCH *pSwitch, PORT Port);
void NapaConfigPhyPolling(SWITCH *pSwitch, PORT Port, NAPA_CONFIG Set);
void NapaSetLedMode(SWITCH *pSwitch, PORT Port, LED_STURE *pLed);
void NapaGetLinkStatus(SWITCH *pSwitch, PORT Port, LINK_STURE *pLinkStatus);
void NapaSetLinkStatus(SWITCH *pSwitch, PORT Port, LINK_STURE *pLinkStatus);
void NapaMdioRead(SWITCH *pSwitch, PHY_STURE *pPhy);
void NapaMdioWrite(SWITCH *pSwitch, PHY_STURE *pPhy);
// End of PHY APIs

//
// Wake up APIs
//
void NapaConfigWakeUpFunc(SWITCH *pSwitch, NAPA_CONFIG Set);
void NapaClearWakeUp(SWITCH *pSwitch);
void NapaConfigWakeUpFrame(SWITCH *pSwitch, PORT Port, NAPA_CONFIG Set);
void NapaConfigLinkChange(SWITCH *pSwitch, PORT Port, NAPA_CONFIG Set);
void NapaConfigMagicPacket(SWITCH *pSwitch, PORT Port, NAPA_CONFIG Set);
void NapaConfigWakeUpDaMatch(SWITCH *pSwitch, PORT Port, NAPA_CONFIG Set);
void NapaConfigWakeUpMask(SWITCH *pSwitch, PORT Port, unsigned char MaskNum, NAPA_CONFIG Set);
void NapaConfigWakeUpOffset(SWITCH *pSwitch, PORT Port, unsigned char OffsetNum, unsigned char Offset);
void NapaConfigWakeUpCascade(SWITCH *pSwitch, PORT Port, unsigned char CascadeNum, NAPA_CONFIG Set);
void NapaConfigWakeUpMaskData(SWITCH *pSwitch, PORT Port, unsigned char MaskNum, unsigned long Value);
void NapaConfigWakeUpCrcData(SWITCH *pSwitch, PORT Port, unsigned char CrcNum,unsigned long Value);
// End of Wake up APIs

//
// RMON APIs
//
void NapaReadRmonCounter(SWITCH *pSwitch, PORT Port, RMON_STURE *pCounter);
void NapaClearRmonCounter(SWITCH *pSwitch);
// End of RMON APIs

//
// EEPROM APIs
//
void NapaReloadEEProm(SWITCH *pSwitch);
NAPA_STATUS NapaReadEEPromData(SWITCH *pSwitch, unsigned char Mode, EEPROM_STURE *pData);
NAPA_STATUS NapaWriteEEPromData(SWITCH *pSwitch, unsigned char Mode, EEPROM_STURE *pData);
NAPA_STATUS NapaEreaseEEProm(SWITCH *pSwitch, unsigned char Mode);
//End of EEPROM APIs

//Forwarding Table APIs
NAPA_STATUS NapaReadForwardingEntry(SWITCH *pSwitch, FORWARDING_TABLE_STRUE *pMac);
NAPA_STATUS NapaReadNextForwardingEntry(SWITCH *pSwitch, FORWARDING_TABLE_STRUE *pMac);
void NapaFlushForwardingEntry(SWITCH *pSwitch);
void NapaWriteForwardingEntry(SWITCH *pSwitch, FORWARDING_TABLE_STRUE *pMac);
NAPA_CONFIG NapaReadForwardingLearnConfig(SWITCH *pSwitch);
void NapaConfigForwardingLearnStop(SWITCH *pSwitch, NAPA_CONFIG Set);
unsigned short NapaReadAgingConfig(SWITCH *pSwitch);
void NapaWriteAgingConfig(SWITCH *pSwitch, unsigned long ConfigTime);
void NapaWriteLearningMethod(SWITCH *pSwitch, NAPA_CONFIG Method);
NAPA_CONFIG NapaReadLearningMethod(SWITCH *pSwitch);
//End of Forwarding Table APIs

//
//VLAN APIs
//
NAPA_CONFIG NapaReadTagBaseVlanConfig(SWITCH *pSwitch);
void NapaWriteTagBaseVlanConfig(SWITCH *pSwitch, NAPA_CONFIG Set);
NAPA_CONFIG NapaReadPortBaseVlanConfig(SWITCH *pSwitch);
void NapaWritePortBaseVlanConfig(SWITCH *pSwitch, NAPA_CONFIG Set);
NAPA_STATUS NapaConfigPortbaseMember(SWITCH *pSwitch, PORTMAP_STURE *pPortBaseMember, unsigned char Group);
void NapaReadDefaultVlanConfig(SWITCH *pSwitch, PORT Port, TAG_VLAN_STURE *pVlan);
void NapaWriteDefaultVlanConfig(SWITCH *pSwitch, PORT Port, TAG_VLAN_STURE *pVlan);
void NapaReadTagVlanConfig(SWITCH *pSwitch, TAG_VLAN_STURE *pVlan);
void NapaWriteTagVlanConfig(SWITCH *pSwitch, TAG_VLAN_STURE *pVlan);
//End of VLAN APIs

//
//QOS APIs
//
void NapadWrite8021pConfig(SWITCH *pSwitch, NAPA_CONFIG Set);
void NapadWriteTosConfig(SWITCH *pSwitch, NAPA_CONFIG Set);
void NapadWriteCosConfig(SWITCH *pSwitch, NAPA_CONFIG Set);
void NapadRead8021pMapTable(SWITCH *pSwitch, QOS_1P_MAP *pMap);
void NapadWrite8021pMapTable(SWITCH *pSwitch, QOS_1P_MAP *pMap);
void NapadReadTosMapTable(SWITCH *pSwitch, QOS_COSTOS_MAP *pMap);
void NapadWriteTosMapTable(SWITCH *pSwitch, QOS_COSTOS_MAP *pMap);
void NapadReadCosMapTable(SWITCH *pSwitch, QOS_COSTOS_MAP *pMap);
void NapadWriteCosMapTable(SWITCH *pSwitch, QOS_COSTOS_MAP *pMap);
//End of QOS APIs

//
// STP APIs
//
void NapaConfigSTP(SWITCH *pSwitch, NAPA_CONFIG Set);
void NapaConfigPortState(SWITCH *pSwitch, PORT Port, PORT_STATE State);
//End of STO APIs

//
// SMAC APIs
//
NAPA_STATUS NapaReadStaticMac(SWITCH *pSwitch, unsigned char Entry, SMAC_STURE *pSMac);
NAPA_STATUS NapaWriteStaticMac(SWITCH *pSwitch, unsigned char Entry, SMAC_STURE *pSMac);
// End of SMAC APIs
#endif
