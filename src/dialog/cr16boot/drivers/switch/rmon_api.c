/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
 /*============================================================================
 * Module Name: rmon_api.c
 * Purpose:
 * Author:
 * Date:
 * Notes:
 * $Log: rmon_api.c,v $
 * no message
 *
 *
 *=============================================================================
 */

/* INCLUDE FILE DECLARATIONS */
#include "switch.h"
#ifdef CONFIG_DRIVER_AX88783


/* NAMING CONSTANT DECLARATIONS */

/* GLOBAL VARIABLES DECLARATIONS */

/* LOCAL VARIABLES DECLARATIONS */

/* LOCAL SUBPROGRAM DECLARATIONS */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadRmonCounter()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaReadRmonCounter(SWITCH *pSwitch, PORT Port, RMON_STURE *pCounter)
{
	unsigned long TmpLong;
	unsigned long Counter;
	unsigned long *pDataPtr = (unsigned long *)pCounter;

	SDBG(SDBG_RMON, ("===> NapaReadRmonCounter\n\r"));

	for (Counter = 0; Counter < MAX_RMONR_NUM; Counter++)
	{
		TmpLong = RMONCR_PORT((unsigned long)Port) | RMONCR_RMON_ADDR(Counter) | RMONCR_READ;
		NapaWriteRegister(pSwitch, RMONCR, TmpLong);
		TmpLong = NapaReadRegister(pSwitch, RMONDR);
		*pDataPtr = TmpLong;
		pDataPtr++;
	}

	SDBG(SDBG_RMON, ("<=== NapaReadRmonCounter\n\r"));

} /* End of NapaReadRmonCounter() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaClearRmonCounter()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaClearRmonCounter(SWITCH *pSwitch)
{
	unsigned long TmpLong;

	SDBG(SDBG_RMON, ("===> NapaClearRmonCounter\n\r"));

	TmpLong = RMONCR_CLEAR;
	NapaWriteRegister(pSwitch, RMONCR, TmpLong);

	SDBG(SDBG_RMON, ("<=== NapaClearRmonCounter\n\r"));

} /* End of NapaClearRmonCounter() */

#endif