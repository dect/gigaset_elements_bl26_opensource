
#include "armboot.h"
#include "nt75451.h"
#include "fonts.h"

#ifdef CONFIG_NT75451_LCD

#if (defined CONFIG_CHAR_LCD) || (defined CONFIG_GRAPH_LCD) || (defined CONFIG_ST7567_LCD)
#error "NT75451 LCD already defined"
#endif



#define X_BYTES				128
#define Y_BYTES	      		4

#define COL_MIN				0x04//0x00
//#define COL_MAX				0x83

	#define SCRN_TOP			0
	#define SCRN_BOTTOM			31
	#define SCRN_LEFT			0//0x04
	#define SCRN_RIGHT			127//0x83


#define PAGE_MIN			0
#define PAGE_MAX			(SCRN_BOTTOM/8)

#define MAX_SCROLL_TEXT_SIZE 4000

typedef struct {
	char left;
	char top;
	char font;
	char visible_size;
	int  size ;
	char text[MAX_SCROLL_TEXT_SIZE];
	int ScrollPos;
	int enable;
}scroll_struct;
scroll_struct m_scroll_struct;
typedef struct {
	uchar left;
	uchar top;
	uchar font;
	uchar blinking_rate;
	uchar state;
	uchar enabled;
	uchar reserved[2];
}cursor_struct;

typedef struct {
	uchar left;
	uchar top;
	uchar width;
 	uchar blinking_rate;
	uchar state;
	uchar enabled;
	uchar bitmaps;
	uchar reserved;
	uchar  bitmap[4][128];
}animation_struct;

const unsigned char l_mask_array[8] ={0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
uchar l_display_array[Y_BYTES][X_BYTES];

cursor_struct m_cursor;


unsigned char  sitel_logo_glyph_table[] = {

		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0xF0,0xFC,0x1E,0x06,0x03,0x03,
		0x03,0x83,0x83,0x87,0x8E,0x9C,0x88,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,
		0xFF,0x0E,0x1C,0x70,0xE0,0x80,0x00,0x00,
		0xFF,0xFF,0x00,0x00,0xC0,0xE0,0x70,0x30,
		0x30,0x30,0x70,0xE0,0xC0,0x00,0x00,0xF0,
		0xF0,0x60,0x30,0x30,0x30,0xFE,0xFE,0x30,
		0x30,0x00,0xC0,0xE0,0x70,0x30,0x30,0x30,
		0x70,0xE0,0x80,0x00,0x00,0xFF,0xFF,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x3F,0x3F,0x30,0x30,0x30,0x30,0x30,0x30,
		0x00,0x00,0x03,0x0F,0x1C,0x18,0x38,0x30,
		0x30,0x31,0x31,0x31,0x19,0x1F,0x0F,0x00,
		0x03,0x03,0x03,0x03,0x03,0x00,0x00,0x3F,
		0x3F,0x00,0x00,0x00,0x01,0x03,0x0E,0x1C,
		0x3F,0x3F,0x00,0x00,0x0F,0x1F,0x38,0x30,
		0x30,0x30,0x38,0x1F,0x0F,0x00,0x00,0x3F,
		0x3F,0x00,0x00,0x00,0x00,0x1F,0x3F,0x30,
		0x30,0x00,0x0F,0x1F,0x3B,0x33,0x33,0x33,
		0x33,0x1B,0x0B,0x00,0x00,0x3F,0x3F,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};



void putByte_SPI1(uint8 x)
{ 
  SetWord(SPI1_RX_TX_REG0,  x);
  while (GetBits(SPI1_CTRL_REG, SPI_INT_BIT==0));
  while (GetBits(SPI1_CTRL_REG, SPI_INT_BIT==1)) 
  {
    SetWord(SPI1_CLEAR_INT_REG, 0x01);
  }
}


void lcd_ctrl_write(uint8 x)
{
	SetWord(P1_RESET_DATA_REG,GPIO_15);		//LCD cs low
	SetWord(P2_RESET_DATA_REG,GPIO_6);		//LCD A0 high
	putByte_SPI1(x);
	SetWord(P1_SET_DATA_REG,GPIO_15);		//LCD cs high
}

void lcd_data_write( uint8 x)
{
	SetWord(P1_RESET_DATA_REG,GPIO_15);		//LCD cs low
	SetWord(P2_SET_DATA_REG,GPIO_6);		//LCD A0 low
	putByte_SPI1(x);
	SetWord(P1_SET_DATA_REG,GPIO_15);		//LCD cs high

}

void lcd_reset( void )
{
	SetWord(P2_RESET_DATA_REG,GPIO_10);
	udelay(50000);
	udelay(50000);
	SetWord(P2_SET_DATA_REG,GPIO_10);
}
void lcdInitHW(void)
{

	SetPort(P2_10_MODE_REG, PORT_OUTPUT,   PID_port);		//LCD reset
	SetPort(P2_06_MODE_REG, PORT_OUTPUT,   PID_port);		//LCD A0	
	SetPort(P1_15_MODE_REG, PORT_OUTPUT,   PID_port);		//LCD SPI Enable	
	SetPort(P2_04_MODE_REG, PORT_OUTPUT,   PID_SPI1_DOUT);	//LCD SPI Do	
	SetPort(P1_02_MODE_REG, PORT_OUTPUT,   PID_SPI1_CLK);	//LCD SPI Clk
	
		
	SetBits(CLK_GPIO1_REG,SW_SPI_DIV,4);
	SetBits(CLK_GPIO3_REG,SW_SPI_EN,1);

	SetBits(SPI1_CTRL_REG, SPI_WORD, 0);        // Set SPI 8bits Mode
	SetBits(SPI1_CTRL_REG, SPI_SMN,  0x00);     // Set SPI IN MASTER MODE
	SetBits(SPI1_CTRL_REG, SPI_POL,  1);  
	SetBits(SPI1_CTRL_REG, SPI_PHA,  1);   		// MODE 3: POL=1  and PHA=1
	SetBits(SPI1_CTRL_REG, SPI_MINT, 0x1);      // Disable SPI2_INT to ICU, timer0 is used
	SetBits(SPI1_CTRL_REG, SPI_ON, 1);          // Enable SPI1 

	SetBits(SPI1_CTRL_REG, SPI_CLK,  3);     // SPI_CLK = XTAL/(PER20_DIV/4) = 2.592 MHz
		
					
}
/*************************************************************/
/********************* PUBLIC FUNCTIONS **********************/
/*************************************************************/


void lcd_print_screen(unsigned char *screen)
{
	unsigned char p;
	unsigned char i;

	for(p=0; p<Y_BYTES; p++)
	{
		lcd_ctrl_write(G_LCD_SET_PAGE|p);	/* set page */
		lcd_ctrl_write(G_LCD_SET_COL_HIGH|((COL_MIN & 0xF0)>>4));	/* set column 0 */
		lcd_ctrl_write(G_LCD_SET_COL_LOW|(COL_MIN & 0x0F));
		for(i=0; i<X_BYTES; i++)
		{
			lcd_data_write(screen[p*X_BYTES+i]);				/* print the data */
		}
	}
}

/*******************************************************
	Description:	Erases lcd RAM
	Note:
 ********************************************************/
void lcd_clear_ram(void)
{
	uchar p;
	uchar i;
	if (m_cursor.enabled){
		m_cursor.enabled=0;
	}
  	for(p=0; p<Y_BYTES; p++)
	{
		lcd_ctrl_write(G_LCD_SET_PAGE|p);	/* set page */
		lcd_ctrl_write(G_LCD_SET_COL_HIGH|((COL_MIN & 0xF0)>>4));	/* set column 0 */
		lcd_ctrl_write(G_LCD_SET_COL_LOW|(COL_MIN & 0x0F));
		for(i=0; i<X_BYTES; i++)
		{
			lcd_data_write(0);				/* clear the data */
			l_display_array[p][i]=0; /*Clear buffer*/
		}
	}
}

/*
**
** Inverts the display memory starting at the left/top and going to
** the right/bottom. No runtime error checking is performed. It is
** assumed that left is less than right and that top is less than
** bottom
**
*/

void lcd_invert_rect(uchar left,  uchar top,
			         uchar right, uchar bottom)
{
	uchar bit_pos;
	uchar x;
	uchar byte_offset;
	uchar y_bits;
	uchar remaining_bits;
	uchar mask;

	bit_pos = top & 0x07;					/* get starting bit offset into byte */

	for(x = left; x <= right; x++)
  	{
		byte_offset = top >> 3;				/* get byte offset into y direction */
		y_bits = (bottom - top) + 1;		/* get length in the x direction to write */
		remaining_bits = 8 - bit_pos;		/* number of bits left in byte */
		mask = l_mask_array[bit_pos];		/* get mask for this bit */

		while(y_bits)						/* while there are still bits to write */
    	{
			if((remaining_bits == 8) && (y_bits > 7))
			{
				/* here if we are byte aligned and have at least 1 byte to write */
				/* do the entire byte at once instead of bit by bit */
				while(y_bits > 7)			/* while there are at least 8 more bits to do */
				{
					l_display_array[byte_offset][x] ^= 0xFF;
					byte_offset++;
					y_bits -= 8;
				}
      		}
      		else
      		{
				/* here if not byte aligned or an entire byte does not need written */
				/* thus do bit by bit */
				l_display_array[byte_offset][x] ^= mask;
				if(l_mask_array[0] & 0x80)
				{
					mask >>= 1;
				}
				else
				{
					mask <<= 1;
				}
				y_bits--;
				remaining_bits--;
				if(remaining_bits == 0)
				{
					/* might have bust gotton byte aligned */
					/* so reset for beginning of a byte */
					remaining_bits = 8;
					byte_offset++;
					mask = l_mask_array[0];
				}
			}
		}
	}
}
#if 0

/*
**
** 	Clears the display memory starting at the left/top  and going to
**  the right/bottom . No runtime error checking is performed. It is
**  assumed that left is less than right and that top is less than
**  bottom
**
*/

void lcd_clear_rect(uchar left,  uchar top,
			        uchar right, uchar bottom)
{
	uchar bit_pos;
	uchar x;
	uchar byte_offset;
	uchar y_bits;
	uchar remaining_bits;
	uchar mask;

	bit_pos = top & 0x07;					/* get starting bit offset into byte */

	for(x = left; x <= right; x++)
	{
		byte_offset = top >> 3;				/* get byte offset into y direction */
		y_bits = (bottom - top) + 1;		/* get length in the y direction to write */
		remaining_bits = 8 - bit_pos;		/* number of bits left in byte */
		mask = l_mask_array[bit_pos];		/* get mask for this bit */

		while(y_bits)						/* while there are still bits to write */
		{
			if((remaining_bits == 8) && (y_bits > 7))
			{
				/* here if we are byte aligned and have at least 1 byte to write */
				/* do the entire byte at once instead of bit by bit */
				while(y_bits > 7)			/* while there are at least 8 more bits to do */
				{
					l_display_array[byte_offset][x] = 0x00;
					byte_offset++;
					y_bits -= 8;
				}
			}
			else
			{
				/* here if not byte aligned or an entire byte does not need written */
				/* thus do bit by bit */
				l_display_array[byte_offset][x] &= ~mask;
				if(l_mask_array[0] & 0x80)
				{
					mask >>= 1;
				}
				else
				{
					mask <<= 1;
				}
				y_bits--;
				remaining_bits--;
				if(remaining_bits == 0)
				{
					/* might have bust gotton byte aligned */
					/* so reset for beginning of a byte */
					remaining_bits = 8;
					byte_offset++;
					mask = l_mask_array[0];
				}
			}
		}
	}
}


/*
**
** Draws a line into the display memory starting at left going to
** right, on the given row. No runtime error checking is performed.
** It is assumed that left is less than right.
**
*/

void lcd_horz_line(uchar left, uchar right, uchar row)
{
	uchar bit_pos;
	uchar byte_offset;
	uchar mask;
	uchar col;

  	bit_pos = row & 0x07;			/* get the bit offset into a byte */
  	byte_offset = row >> 3;		    /* get the byte offset into x array */
  	mask = l_mask_array[bit_pos]; 	/* get the mask for this bit */

  	for(col = left; col <= right; col++)
  	{
    	l_display_array[byte_offset][col] |= mask;
  	}
}

/*
**
** Draws a vertical line into display memory starting at the top
** going to the bottom in the given column. No runtime error checking
** is performed. It is assumed that top is less than bottom and that
** the column is in range.
**
*/

void lcd_vert_line(uchar top, uchar bottom, uchar column)
{
	uchar bit_pos;
	uchar byte_offset;
	uchar y_bits;
	uchar remaining_bits;
	uchar mask;

	bit_pos = top & 0x07;		   /* get starting bit offset into byte */

	byte_offset = top >> 3;		   /* get byte offset into y direction */
	y_bits = (bottom - top) + 1;   /* get length in the x direction to write */
	remaining_bits = 8 - bit_pos;  /* number of bits left in byte */
	mask = l_mask_array[bit_pos];  /* get mask for this bit */

	while(y_bits)				   /* while there are still bits to write */
	{
		if((remaining_bits == 8) && (y_bits > 7))
		{
			/* here if we are byte aligned and have at least 1 byte to write */
			/* do the entire byte at once instead of bit by bit */
			while(y_bits > 7)			/* while there are at least 8 more bits to do */
			{
				l_display_array[byte_offset][column] = 0xFF;
				byte_offset++;
				y_bits -= 8;
			}
		}
		else
		{
      		/* we are not byte aligned or an entire byte does not need written */
      		/* do each individual bit                                          */
			l_display_array[byte_offset][column] |= mask;
			if(l_mask_array[0] & 0x80)
			{
				mask >>= 1;
			}
			else
			{
				mask <<= 1;
			}
			y_bits--;
			remaining_bits--;
			if(remaining_bits == 0)
			{
				/* might have bust gotton byte aligned */
				/* so reset for beginning of a byte */
				remaining_bits = 8;
				byte_offset++;
				mask = l_mask_array[0];
			}
		}
	}
}

/*
**
** Clears a line into the display memory starting at left going to
** right, on the given row. No runtime error checking is performed.
** It is assumed that left is less than right.
**
*/

void lcd_clr_horz_line(uchar left, uchar right,
		               uchar row)
{
	uchar bit_pos;
	uchar byte_offset;
	uchar mask;
	uchar col;

  	bit_pos = row & 0x07;			/* get the bit offset into a byte */
  	byte_offset = row >> 3;		    /* get the byte offset into x array */
  	mask = l_mask_array[bit_pos]; 	/* get the mask for this bit */

  	for(col = left; col <= right; col++)
  	{
    	l_display_array[byte_offset][col] &= ~mask;
  	}
}


/*
**
** Clears a vertical line into display memory starting at the top
** going to the bottom in the given column. No runtime error checking
** is performed. It is assumed that top is less than bottom and that
** the column is in range.
**
*/

void lcd_clr_vert_line(uchar top, uchar bottom, uchar column)
{
	uchar bit_pos;
	uchar byte_offset;
	uchar y_bits;
	uchar remaining_bits;
	uchar mask;

	bit_pos = top & 0x07;		   /* get starting bit offset into byte */

	byte_offset = top >> 3;		   /* get byte offset into y direction */
	y_bits = (bottom - top) + 1;   /* get length in the x direction to write */
	remaining_bits = 8 - bit_pos;  /* number of bits left in byte */
	mask = l_mask_array[bit_pos];  /* get mask for this bit */

	while(y_bits)				   /* while there are still bits to write */
	{
		if((remaining_bits == 8) && (y_bits > 7))
		{
			/* here if we are byte aligned and have at least 1 byte to write */
			/* do the entire byte at once instead of bit by bit */
			while(y_bits > 7)			/* while there are at least 8 more bits to do */
			{
				l_display_array[byte_offset][column] = 0x00;
				byte_offset++;
				y_bits -= 8;
			}
		}
		else
		{
      		/* we are not byte aligned or an entire byte does not need written */
      		/* do each individual bit                                          */
			l_display_array[byte_offset][column] &= ~mask;
			if(l_mask_array[0] & 0x80)
			{
				mask >>= 1;
			}
			else
			{
				mask <<= 1;
			}
			y_bits--;
			remaining_bits--;
			if(remaining_bits == 0)
			{
				/* might have bust gotton byte aligned */
				/* so reset for beginning of a byte */
				remaining_bits = 8;
				byte_offset++;
				mask = l_mask_array[0];
			}
		}
	}
}

/*
**
** 	Draws a box in display memory starting at the left/top and going
**  to the right/bottom. No runtime error checking is performed.
**  It is assumed that left is less than right and that top is less
**  than bottom.
**
*/

void lcd_draw_border(uchar left, uchar top, uchar right, uchar bottom)
{
  	/* to draw a box requires two vertical lines */
   	lcd_vert_line(top,bottom,left);
   	lcd_vert_line(top,bottom,right);
  	/* and two horizonal lines */
   	lcd_horz_line(left,right,top);
   	lcd_horz_line(left,right,bottom);
return ;
	lcd_vert_line(top+1,top+1,left+1);	//+1
	lcd_vert_line(top+2,bottom-2,left);
	lcd_vert_line(bottom-1, bottom-1,left+1); //-1

	lcd_vert_line(top+1,top+1,right-1);//+1
	lcd_vert_line(top+2,bottom-2,right);
	lcd_vert_line(bottom-1,bottom-1,right-1);//-1

	lcd_horz_line(left+2,right-2,top);
	lcd_horz_line(left+2,right-2,bottom);


}

/*
**
** Clears a box in display memory starting at the Top left and going
** to the bottom right. No runtime error checking is performed and
** it is assumed that Left is less than Right and that Top is less
** than Bottom.
**
*/

void lcd_clear_border(uchar left, uchar top, uchar right, uchar bottom)
{
  	/* to undraw the box undraw the two vertical lines */
  	lcd_clr_vert_line(top,bottom,left);
  	lcd_clr_vert_line(top,bottom,right);

  	/* and the two horizonal lines that comprise it */
  	lcd_clr_horz_line(left,right,top);
    lcd_clr_horz_line(left,right,bottom);
}
#endif //
/*
**
** Writes a glyph to the display at location x,y
**
** Arguments are:
**    column     - x corrdinate of the left part of glyph
**    row        - y coordinate of the top part of glyph
**    width  	 - size in pixels of the width of the glyph
**    height 	 - size in pixels of the height of the glyph
**    glyph      - an uchar pointer to the glyph pixels
**                 to write assumed to be of length "width"
**
*/

void lcd_glyph(uchar left, uchar top,
			   uchar width, uchar height,
			   uchar *glyph, uchar store_width)
{
	uchar bit_pos;
	uchar byte_offset;
	uchar y_bits;
	uchar remaining_bits;
	uchar mask;
	uchar char_mask;
	uchar x;
	uchar *glyph_scan;
	uchar glyph_offset;

  	bit_pos = top & 0x07;		/* get the bit offset into a byte */

	glyph_offset = 0;			/* start at left side of the glyph rasters */
    char_mask = 0x80;			/* initial character glyph mask */

  	for (x = left; x < (left + width); x++)
  	{
    	byte_offset = top >> 3;        	/* get the byte offset into y direction */
		y_bits = height;				/* get length in y direction to write */
		remaining_bits = 8 - bit_pos;	/* number of bits left in byte */
		mask = l_mask_array[bit_pos];	/* get mask for this bit */
		glyph_scan = glyph + glyph_offset;	 /* point to base of the glyph */

    	/* boundary checking here to account for the possibility of  */
    	/* write past the bottom of the screen.                        */
    	while((y_bits) && (byte_offset < Y_BYTES)) /* while there are bits still to write */
    	{
			/* check if the character pixel is set or not */
			if(*glyph_scan & char_mask)
			{
				l_display_array[byte_offset][x] |= mask;	/* set image pixel */
			}
			else
			{
      			l_display_array[byte_offset][x] &= ~mask;	/* clear the image pixel */
			}

			if(l_mask_array[0] & 0x80)
			{
				mask >>= 1;
			}
			else
			{
				mask <<= 1;
			}

			y_bits--;
			remaining_bits--;
      		if(remaining_bits == 0)
      		{
				/* just crossed over a byte boundry, reset byte counts */
				remaining_bits = 8;
				byte_offset++;
				mask = l_mask_array[0];
      		}

			/* bump the glyph scan to next raster */
			glyph_scan += store_width;
		}

		/* shift over to next glyph bit */
		char_mask >>= 1;
		if(char_mask == 0)				/* reset for next byte in raster */
		{
			char_mask = 0x80;
			glyph_offset++;
	    }
	}
}



/*
**
**	Prints the given string at location x,y in the specified font.
**  Prints each character given via calls to lcd_glyph. The entry string
**  is null terminated and non 0x20->0x7e characters are ignored.
**
**  Arguments are:
**      left       coordinate of left start of string.
**      top        coordinate of top of string.
**      font       font number to use for display
**      str   	   text string to display
**
*/

void lcd_text(uchar left, uchar top, uchar font, char *str)
{
  	uchar x = left;
  	uchar glyph;
  	uchar width;
	uchar height;
	uchar store_width;
	uchar  *glyph_ptr;
if (str==NULL) {printf("INVALID STRING ....\n");}
//printk("LEFT %d TOP %d FONT %d\n", left, top, font);
//printk("TEXT STRING %s \n",str);
  	while(*str != 0x00)
  	{
    	glyph = (uchar)*str;

		/* check to make sure the symbol is a legal one */
		/* if not then just replace it with the default character */
		if((glyph < fonts[font].glyph_beg) || (glyph > fonts[font].glyph_end))
		{
			glyph = fonts[font].glyph_def;
		}

    	/* make zero based index into the font data arrays */
    	glyph -= fonts[font].glyph_beg;
    	width = fonts[font].fixed_width;	/* check if it is a fixed width */
		if(width == 0)
		{
			width=fonts[font].width_table[glyph];	/* get the variable width instead */
		}

		height = fonts[font].glyph_height;
		store_width = fonts[font].store_width;

		glyph_ptr = fonts[font].glyph_table + ((unsigned int)glyph * (unsigned int)store_width * (unsigned int)height);

		/* range check / limit things here */
		if(x > SCRN_RIGHT)
		{
			x = SCRN_RIGHT;
		}
		if((x + width) > SCRN_RIGHT+1)
		{
			width = SCRN_RIGHT - x + 1;
		}
		if(top > SCRN_BOTTOM)
		{
			top = SCRN_BOTTOM;
		}
		if((top + height) > SCRN_BOTTOM+1)
		{
			height = SCRN_BOTTOM - top + 1;
		}

		lcd_glyph(x,top,width,height,glyph_ptr,store_width);  /* plug symbol into buffer */

		x += width;							/* move right for next character */
		str++;								/* point to next character in string */
	}
}


/*
**
** Updates area of the display. Writes data from display
** RAM to the lcd display controller.
**
** Arguments Used:
**    top     top line of area to update.
**    bottom  bottom line of area to update.
**
*/

void lcd_update(uchar top, uchar bottom)
{
	uchar x;
	uchar y;
	uchar yt;
	uchar yb;
	uchar *colptr;
	//YANNIS
	top =0;
	bottom=SCRN_BOTTOM;

	yt = top >> 3;				/* setup bytes of range */
	yb = bottom >> 3;

	for(y = yt; y <= yb; y++)
	{
		/* setup the page number for the y direction */
		lcd_ctrl_write(G_LCD_SET_PAGE|y);	/* set page */
		/* setup column of update to left side */
		lcd_ctrl_write(G_LCD_SET_COL_HIGH|((COL_MIN & 0xF0)>>4));	/* set column 0 */
		lcd_ctrl_write(G_LCD_SET_COL_LOW|(COL_MIN & 0x0F));

		colptr = &l_display_array[y][0];
		for (x=0; x < X_BYTES; x++)
		{
			lcd_data_write(*colptr++);
		}
	}
}



void lcd_cursor_set(uchar left, uchar top, uchar font, uchar ShowCursor, uchar blinking_rate)
{
	uchar width;
//	STOP_TIMER(cursor_timer);
 	width=fonts[font].fixed_width;
	if (width==0) width=8;
	if (m_cursor.enabled)
	{
		m_cursor.enabled=0;
   		if(m_cursor.state)
		{
//			printk("Invert cursor position [%d] [%d] \n", m_cursor.left, width);
			lcd_invert_rect(m_cursor.left, m_cursor.top, m_cursor.left + width - 1, m_cursor.top + fonts[m_cursor.font].glyph_height - 1);
			lcd_update(SCRN_TOP, SCRN_BOTTOM);
			m_cursor.state = 0;
  		}//else printk("Cursor is in cleared state \n");
	}//else printk("new text ... \n");
 	if(ShowCursor == 0)	return;
 	m_cursor.left = left;
	m_cursor.top = top;
	m_cursor.font = font;
	m_cursor.blinking_rate = blinking_rate;
  	lcd_invert_rect(m_cursor.left, m_cursor.top, m_cursor.left + width - 1, m_cursor.top + fonts[m_cursor.font].glyph_height - 1);
 	lcd_update(SCRN_TOP, SCRN_BOTTOM);
//   	START_TIMER(cursor_timer, blinking_rate / 2);
	m_cursor.state = 1;
	m_cursor.enabled = 1;
}


uchar lcd_font_width_get(uchar font_width)
{
 	return fonts[font_width].fixed_width;
}



void lcdInit()
{

	unsigned i,j;
	
	lcdInitHW();
	
	lcd_reset();

	printf("\n128x32 graphics lcd configuration\n");

	G_LCD_SET_BIAS_19;
//	G_LCD_ADC_SEL_NORMAL;
	G_LCD_ADC_SEL_REV;
//	G_LCD_SHL_FLIPPED;
	// initial display line 0 page set command
	lcd_ctrl_write(G_LCD_SET_LINE);
	lcd_ctrl_write(G_LCD_POWER_CTRL_SET | G_LCD_POWER_VC_SET);
	udelay(50000);
	udelay(50000);
	lcd_ctrl_write(G_LCD_POWER_CTRL_SET | G_LCD_POWER_VC_SET | G_LCD_POWER_VR_SET);
	udelay(50000);
	udelay(50000);
	lcd_ctrl_write(G_LCD_POWER_CTRL_SET | G_LCD_POWER_VC_SET | G_LCD_POWER_VR_SET | G_LCD_POWER_VF_SET);
	udelay(50000);
	udelay(50000);
	lcd_ctrl_write(G_LCD_REG_RATIO | 0x03);
	G_LCD_VOLUME_MODE_SET;
	lcd_ctrl_write(0x10);
	G_LCD_DISPLAY_ON;
	lcd_ctrl_write(G_LCD_SET_PAGE);
	lcd_ctrl_write(G_LCD_SET_COL_HIGH);
	lcd_ctrl_write(G_LCD_SET_COL_LOW |4);
	udelay(50000);
	udelay(50000);
	


	lcd_print_screen(sitel_logo_glyph_table);

	// display sitel logo

/*
	for (j=0;j<4;j++){

		lcd_ctrl_write(G_LCD_SET_PAGE|j);
		lcd_ctrl_write(G_LCD_SET_COL_HIGH);
		lcd_ctrl_write(G_LCD_SET_COL_LOW |4);
		for (i=0;i<128;i++) {
			lcd_data_write(sitel_logo_glyph_table[128*j+i]);
		}
	}
*/
}



// clear screen
void lcdClearScreen(void)
{
	unsigned char i,j;
	//clear all pixels
	for (j=0;j<4;j++){
		lcd_ctrl_write(G_LCD_SET_PAGE|j);
		lcd_ctrl_write(G_LCD_SET_COL_HIGH);
		lcd_ctrl_write(G_LCD_SET_COL_LOW |4);
		for (i=0;i<128;i++) {
			lcd_data_write(0);
		}
	}
	lcdGotoXY(0,0);
}


void lcdGotoXY(unsigned short x, unsigned short y)
{
	unsigned short page;

	//lcd columns are from 4 to 131
	x+=4;
	page = y>>3;

	lcd_ctrl_write(G_LCD_SET_PAGE|page);
	//set col high
	lcd_ctrl_write(G_LCD_SET_COL_HIGH|((x&0xff)>>4));
	//set col low
	lcd_ctrl_write(G_LCD_SET_COL_LOW |(x&0xf));

}



void graph_lcd_disp_line(unsigned short line,char* data)
{
	unsigned i,j;


//	lcdGotoXY(0,(line+6)*8);
	lcdGotoXY(0,(line+0)*8);
	for (i=0;i<128;i++) {
		lcd_data_write(0);
	}
//	lcdGotoXY(0,(line+6)*8);
	lcdGotoXY(0,(line+0)*8);

//	lcdPrintData(data,16,&Font_System5x8);
}


#endif //CONFIG_NT75451_LCD
