
#include "armboot.h"
#include "char_lcd.h"

#ifdef CONFIG_CHAR_LCD

#ifdef CONFIG_GRAPH_LCD
#error "128x64 Graphics LCD already defined"
#endif
//#if 0
//#ifdef NEW_LCD_IF
//#define SET_LCD_WRITE()	SetDbits (EBI_SDREFR_REG ,ACS3_IOEXP,1)
//#define SET_LCD_READ()	SetDbits (EBI_SDREFR_REG ,ACS3_IOEXP,0)
//#else
//#define SET_LCD_WRITE()	SetWord (P2_RESET_DATA_REG , GPIO_10)
//#define SET_LCD_READ()	SetWord (P2_SET_DATA_REG , GPIO_10)
//#endif
//#endif



#ifdef CONFIG_SDRAM_SIZE_32MB

#define PROTECT_OVER_16M_SDRAM_ACEESS	{ SetWord(EBI_ACS4_CTRL_REG,9);}
#define RESTORE_OVER_16M_SDRAM_ACEESS	{ SetWord(EBI_ACS4_CTRL_REG,0xa);}

#else

#define PROTECT_OVER_16M_SDRAM_ACEESS	
#define RESTORE_OVER_16M_SDRAM_ACEESS	

#endif



// custom LCD characters
static unsigned char LcdCustomChar[] =
{
	0x00, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x1F, 0x00, // 0. 0/5 full progress block
	0x00, 0x1F, 0x10, 0x10, 0x10, 0x10, 0x1F, 0x00, // 1. 1/5 full progress block
	0x00, 0x1F, 0x18, 0x18, 0x18, 0x18, 0x1F, 0x00, // 2. 2/5 full progress block
	0x00, 0x1F, 0x1C, 0x1C, 0x1C, 0x1C, 0x1F, 0x00, // 3. 3/5 full progress block
	0x00, 0x1F, 0x1E, 0x1E, 0x1E, 0x1E, 0x1F, 0x00, // 4. 4/5 full progress block
	0x00, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x00, // 5. 5/5 full progress block
	0x03, 0x07, 0x0F, 0x1F, 0x0F, 0x07, 0x03, 0x00, // 6. rewind arrow
	0x00, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x00, // 7. stop block
	0x1B, 0x1B, 0x1B, 0x1B, 0x1B, 0x1B, 0x1B, 0x00, // 8. pause bars
	0x18, 0x1C, 0x1E, 0x1F, 0x1E, 0x1C, 0x18, 0x00, // 9. fast-forward arrow
	0x00, 0x04, 0x04, 0x0E, 0x0E, 0x1F, 0x1F, 0x00, // 10. scroll up arrow
	0x00, 0x1F, 0x1F, 0x0E, 0x0E, 0x04, 0x04, 0x00, // 11. scroll down arrow
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 12. blank character
	0x00, 0x0E, 0x19, 0x15, 0x13, 0x0E, 0x00, 0x00, // 13. animated play icon frame 0
	0x00, 0x0E, 0x15, 0x15, 0x15, 0x0E, 0x00, 0x00, // 14. animated play icon frame 1
	0x00, 0x0E, 0x13, 0x15, 0x19, 0x0E, 0x00, 0x00, // 15. animated play icon frame 2
	0x00, 0x0E, 0x11, 0x1F, 0x11, 0x0E, 0x00, 0x00, // 16. animated play icon frame 3
};



void lcdBusyWait(void)
{
	unsigned i;
	unsigned char data;
	udelay(100);
	// wait until LCD busy bit goes to zero
	// do a read from control register
	// memory bus read
	SET_LCD_READ();
	i=0;
//	while( (*((volatile unsigned char *) (LCD_CTRL_ADDR))) & (1<<LCD_BUSY) );
//	while( ((*((volatile unsigned char *) (LCD_CTRL_ADDR))) & (1<<LCD_BUSY))&&(i++<100000) );
	while (1)
	{
PROTECT_OVER_16M_SDRAM_ACEESS	
	data = *((volatile unsigned char *) (LCD_CTRL_ADDR));
RESTORE_OVER_16M_SDRAM_ACEESS
if ((data & (1<<LCD_BUSY))==0)
	break;	
i++;
if (i>100000)
	break;	
	
	}

}

void lcdControlWrite(unsigned char data) 
{
	udelay(100);
	// write the control byte to the display controller
	lcdBusyWait();              // wait until LCD not busy
	SET_LCD_WRITE();
PROTECT_OVER_16M_SDRAM_ACEESS
	*((volatile unsigned char *) (LCD_CTRL_ADDR)) = data;
RESTORE_OVER_16M_SDRAM_ACEESS	
}


unsigned char lcdControlRead(void)
{

	// read the control byte from the display controller
	unsigned char data;
	udelay(100);
	lcdBusyWait();              // wait until LCD not busy
PROTECT_OVER_16M_SDRAM_ACEESS	
	data = *((volatile unsigned char *) (LCD_CTRL_ADDR));
RESTORE_OVER_16M_SDRAM_ACEESS	
	return data;
}

void lcdDataWrite(unsigned char data) 
{
	udelay(100);
	lcdBusyWait();              // wait until LCD not busy
	SET_LCD_WRITE();
PROTECT_OVER_16M_SDRAM_ACEESS	
	*((volatile unsigned char *) (LCD_DATA_ADDR)) = data;
RESTORE_OVER_16M_SDRAM_ACEESS	
}
unsigned char lcdDataRead(void)
{
	// read a data byte from the display
	unsigned char data;
	lcdBusyWait();              // wait until LCD not busy
PROTECT_OVER_16M_SDRAM_ACEESS	
	data = *((volatile unsigned char *) (LCD_DATA_ADDR));
RESTORE_OVER_16M_SDRAM_ACEESS	
	return data;
}
void lcdInitHW(void)
{
	unsigned char temp;
	//setup lcd cs and memory access timing
//open charge pump
	SetWord(CP_CTRL_REG,0x0003);
	

}
/*************************************************************/
/********************* PUBLIC FUNCTIONS **********************/
/*************************************************************/

void lcdInit()
{
	char logo1[]="     SiTel      ";
#ifdef SC14452
	char logo2[]=" SC14452DK Boot ";
#else
	char logo2[]=" SC14450DK Boot ";
#endif
	// initialize hardware

	printf("\n2x16 char lcd configuration\n");

	lcdInitHW();
	udelay(100000);
	udelay(100000);
	udelay(100000);
	
		SET_LCD_WRITE();
	// LCD function set
//	lcdControlWrite(LCD_FUNCTION_DEFAULT);
PROTECT_OVER_16M_SDRAM_ACEESS
	*((volatile unsigned char *) (LCD_CTRL_ADDR)) = LCD_FUNCTION_DEFAULT;	
RESTORE_OVER_16M_SDRAM_ACEESS	
	udelay(10000);
PROTECT_OVER_16M_SDRAM_ACEESS	
	*((volatile unsigned char *) (LCD_CTRL_ADDR)) = LCD_FUNCTION_DEFAULT;	
RESTORE_OVER_16M_SDRAM_ACEESS	
	udelay(2000);
PROTECT_OVER_16M_SDRAM_ACEESS	
	*((volatile unsigned char *) (LCD_CTRL_ADDR)) = LCD_FUNCTION_DEFAULT;	
RESTORE_OVER_16M_SDRAM_ACEESS	
	udelay(2000);

	//display off
	lcdOff();
//	*((volatile unsigned char *) (LCD_CTRL_ADDR)) = (1<<LCD_ON_CTRL );	
	udelay(2000);
	//display clear
	lcdClear();
//	lcdControlWrite(1<<LCD_CLR);
//	*((volatile unsigned char *) (LCD_CTRL_ADDR)) = (1<<LCD_CLR);
	// set entry mode
	udelay(4000);
	lcdControlWrite(1<<LCD_ENTRY_MODE | 1<<LCD_ENTRY_INC);
//	*((volatile unsigned char *) (LCD_CTRL_ADDR)) =  (1<<LCD_ENTRY_MODE | 1<<LCD_ENTRY_INC);
	udelay(2000);
	// set display to on
//	lcdControlWrite(1<<LCD_ON_CTRL | 1<<LCD_ON_DISPLAY | 1<<LCD_ON_BLINK | 1<<LCD_ON_CURSOR);
	lcdControlWrite(1<<LCD_ON_CTRL | 1<<LCD_ON_DISPLAY );
//	*((volatile unsigned char *) (LCD_CTRL_ADDR)) =  (1<<LCD_ON_CTRL | 1<<LCD_ON_DISPLAY | 1<<LCD_ON_BLINK | 1<<LCD_ON_CURSOR);
	udelay(2000);
	// move cursor to home
	lcdControlWrite(1<<LCD_HOME);
//	*((volatile unsigned char *) (LCD_CTRL_ADDR)) =  (1<<LCD_HOME);
	udelay(2000);
	// set data address to 0
	lcdControlWrite(1<<LCD_DDRAM | 0x00);
//	*((volatile unsigned char *) (LCD_CTRL_ADDR)) =  (1<<LCD_DDRAM | 0x00);
	udelay(2000);

  
	lcdGotoXY(0,0);
	lcdPrintData(&logo1[0],16);
	lcdGotoXY(0,1);
	lcdPrintData(&logo2[0],16);

}
void lcdOn(void)
{
	// turn LCD On
	lcdControlWrite(1<<LCD_ON_CTRL | 1<<LCD_ON_DISPLAY);
	
}
void lcdOff(void)
{
	// turn LCD Off
	lcdControlWrite(1<<LCD_ON_CTRL );
}
void lcdReset()
{
}

void lcdCursorLeft(void)
{
	// move cursor to the left
	lcdControlWrite(1<<LCD_MOVE );
}
void lcdCursorRight(void)
{
	// move cursor to the right
	lcdControlWrite(1<<LCD_MOVE | 1<<LCD_MOVE_RIGHT );
}
void lcdCursorOff(void)
{	
	//turn cursor off
	lcdControlWrite(1<<LCD_ON_CTRL | 1<<LCD_ON_DISPLAY);
}
void lcdCursorOn(void)
{
	lcdControlWrite(1<<LCD_ON_CTRL | 1<<LCD_ON_DISPLAY | 1<<LCD_ON_CURSOR);
}
void lcdCursorBlink(void)
{
	lcdControlWrite(1<<LCD_ON_CTRL | 1<<LCD_ON_DISPLAY | 1<<LCD_ON_CURSOR | 1<<LCD_ON_BLINK);
}
void lcdCursorBlinkOff(void)
{
	lcdControlWrite(1<<LCD_ON_CTRL | 1<<LCD_ON_DISPLAY | 1<<LCD_ON_CURSOR);
}

void lcdHome(void)
{
	// move cursor to home
	lcdControlWrite(1<<LCD_HOME);
}

void lcdClear(void)
{
	// clear LCD
	lcdControlWrite(1<<LCD_CLR);
}
void lcdGotoXY(unsigned char x, unsigned char y)
{
	unsigned char DDRAMAddr;
	// remap lines into proper order
	switch(y)
	{
		case 0: DDRAMAddr = LCD_LINE0_DDRAMADDR+x; break;
		case 1: DDRAMAddr = LCD_LINE1_DDRAMADDR+x; break;
		case 2: DDRAMAddr = LCD_LINE2_DDRAMADDR+x; break;
		case 3: DDRAMAddr = LCD_LINE3_DDRAMADDR+x; break;
		default: DDRAMAddr = LCD_LINE0_DDRAMADDR+x;
	}
	// set data address
	lcdControlWrite(1<<LCD_DDRAM | DDRAMAddr);
}

void lcdLoadCustomChar(unsigned char* lcdCustomCharArray, unsigned char romCharNum, unsigned char lcdCharNum)
{
	unsigned char i;
	unsigned char saveDDRAMAddr;

	// backup the current cursor position
	saveDDRAMAddr = lcdControlRead() & 0x7F;
	
	// multiply the character index by 8
	lcdCharNum = (lcdCharNum<<3);   // each character occupies 8 bytes
	romCharNum = (romCharNum<<3);   // each character occupies 8 bytes
	
	// copy the 8 bytes into CG (character generator) RAM
	for(i=0; i<8; i++)
	{
		// set CG RAM address
		lcdControlWrite((1<<LCD_CGRAM) | (lcdCharNum+i));
		// write character data
		lcdDataWrite(lcdCustomCharArray+romCharNum+i );
	}

	// restore the previous cursor position
	lcdControlWrite(1<<LCD_DDRAM | saveDDRAMAddr);

}

void lcdPrintData(char* data, unsigned char nBytes)
{
	unsigned char i;

	// check to make sure we have a good pointer
	if (!data) return;

	// print data
	for(i=0; i<nBytes; i++)
	{
		lcdDataWrite(data[i]);
	}
}


void lcd_disp_line(unsigned short line,char* data)
{
	lcdGotoXY(0,line);
	lcdPrintData(data,16);
}

void lcdProgressBar(unsigned short progress, unsigned short maxprogress, unsigned char length)
{
	unsigned char i;
	unsigned long pixelprogress;
	unsigned char c;
	 
	// draw a progress bar displaying (progress / maxprogress)
	// starting from the current cursor position
	// with a total length of "length" characters
	// ***note, LCD chars 0-5 must be programmed as the bar characters
	// char 0 = empty ... char 5 = full

	// total pixel length of bargraph equals length*PROGRESSPIXELS_PER_CHAR;
	// pixel length of bar itself is
	pixelprogress = ((progress*(length*PROGRESSPIXELS_PER_CHAR))/maxprogress);
     
	// print exactly "length" characters
	for(i=0; i<length; i++)
	{
		// check if this is a full block, or partial or empty
		// (unsigned short) cast is needed to avoid sign comparison warning
		if( ((i*(unsigned short)PROGRESSPIXELS_PER_CHAR)+5) > pixelprogress )
		{
			// this is a partial or empty block
			if( ((i*(unsigned short)PROGRESSPIXELS_PER_CHAR)) > pixelprogress )
			{
				// this is an empty block
				// use space character?
				c = 0;
			}
			else
			{
				// this is a partial block
				c = pixelprogress % PROGRESSPIXELS_PER_CHAR;
			}
		}
		else
		{
			// this is a full block
			c = 5;
		}
		     
		// write character to display
		lcdDataWrite(c);
	}
 
}

#endif //CONFIG_CHAR_LCD
