/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
 /*============================================================================
 * Module Name: driver.c
 * Purpose:
 * Author:
 * Date:
 * Notes:
 * $Log: driver.c,v $
 * no message
 *
 *
 *=============================================================================
 */



/* INCLUDE FILE DECLARATIONS */
#include <armboot.h>

#ifdef CONFIG_DRIVER_AX88783

#include <command.h>
#include <net.h>
#include <malloc.h>

#include "ax_driver.h"



extern int eth_init(bd_t *bd);
extern void eth_halt(void);
extern int eth_rx(void);
extern int eth_send(volatile void *packet, int length);



#define DEBUG 0

#if DEBUG & 1
#define DEBUG_FUNCTION() do { printf("%s\n", __FUNCTION__); } while (0)
#define DEBUG_LINE() do { printf("%d\n", __LINE__); } while (0)
#else
#define DEBUG_FUNCTION() do {} while(0)
#define DEBUG_LINE() do {} while(0)
#endif

#if DEBUG & 1
#define PRINTK(args...) printf(args)
#else
#define PRINTK(args...)
#endif

#define TOUT 5

#define ETHER_ADDR_LEN 6





SWITCH ax_switch;
unsigned char	phyID[2];
LINK_STURE		LinkStatus[2];
LED_STURE		Led;
unsigned char dev_addr[6];
#define NB 5

static unsigned char *pbuf = NULL;
static int plen[NB];
static int nrx = 0;

static int media0 = 0;
static int media1 = 0;


/* LOCAL SUBPROGRAM DECLARATIONS */

/*
 * ----------------------------------------------------------------------------
 * Function Name: mdio_read()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static int
mdio_read(int phy_id, int location)
{
	PHY_STURE		PhyData;

	PhyData.PhyID = (u8)phy_id;
	PhyData.PhyAddr = (u8)location;

	NapaMdioRead(&ax_switch, &PhyData);

	return PhyData.Value;
} /* End of mdio_read() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: mdio_write()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void
mdio_write(int phy_id, int location, int value)
{
	PHY_STURE		PhyData;

	PhyData.PhyID = (u8)phy_id;
	PhyData.PhyAddr = (u8)location;
	PhyData.Value = value;

	NapaMdioWrite(&ax_switch, &PhyData);

} /* End of mdio_write() */

/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_pci_start_xmit()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
//static void
void
ax_rx(void)
{
	NAPA_STATUS		Status;
	unsigned short	PktLen;
	PORT			Port;

	
	unsigned i,j;

	while(1)
	{
		Status = NapaLocalBusGetRxPktLen(&ax_switch, &PktLen, &Port);
	
		if(Status == NAPA_STATUS_FAILURE)
		{
			printf("AX failed read packet\n");
			NapaLocalBusCpoReset(&ax_switch);
			break;
		}

		if (nrx >= NB) {
			printf("losing packets in rx\n");
			NapaLocalBusCpoReset(&ax_switch);
			break;
		}
		plen[nrx] = PktLen;
		NapaLocalBusGetRxPkt(&ax_switch, &pbuf[nrx*2000], PktLen);
		nrx++;

		break;
	}

}



/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_link_change()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
ax_check_link(void)
{

	NapaGetLinkStatus(&ax_switch, Port_0, &LinkStatus[0]);
//	printf("port0, Link %s. Speed %d Mbps, %s duplex, %s.\n",
//		LinkStatus[0].Link == 1 ? "Up" : "Down",
//		LinkStatus[0].Speed == 1 ? 100 : 10,
//		LinkStatus[0].Duplex == 1 ? "full" : "half",
//		LinkStatus[0].FlowCtrl == 1 ? "FlowCtrl Enabled" : "FlowCtrl Disabled");

	NapaGetLinkStatus(&ax_switch, Port_1, &LinkStatus[1]);
//	printf("port1, Link %s. Speed %d Mbps, %s duplex, %s.\n",
//		LinkStatus[1].Link == 1 ? "Up" : "Down",
//		LinkStatus[1].Speed == 1 ? 100 : 10,
//		LinkStatus[1].Duplex == 1 ? "full" : "half",
//		LinkStatus[1].FlowCtrl == 1 ? "FlowCtrl Enabled" : "FlowCtrl Disabled");


}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_link()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */

#if 0

static void
ax_link(void)
{
	u16 BMCR, BMSR;
	u8  Port;
	u8  CurrLink;
	u8  CurrSpeed;
	u8  CurrDuplex;

	for(Port = 0; Port < 2; Port++)
	{
		BMSR = mdio_read(phyID[Port], MII_BMSR);
		BMCR = mdio_read(phyID[Port], MII_BMCR);
		CurrLink = (BMSR & BMSR_LSTATUS) == 0 ? LINK_DOWN : LINK_UP;
		CurrSpeed = (BMCR & BMCR_SPEED100) == 0 ? 0 : 1;
		CurrDuplex = (BMCR & BMCR_FULLDPLX) == 0 ? 0 : 1;
		
		if(CurrLink != LinkStatus[Port].Link ||
		   CurrSpeed != LinkStatus[Port].Speed ||
		   CurrDuplex != LinkStatus[Port].Duplex)
		{
			LinkStatus[Port].Link = CurrLink;
			LinkStatus[Port].Speed = CurrSpeed;
			LinkStatus[Port].Duplex = CurrDuplex;
			LinkStatus[Port].FlowCtrl = 1;
			
			NapaSetLinkStatus(&ax_switch, Port, &LinkStatus[Port]);

			printf("port%d, Link %s. Speed %d Mbps, %s duplex, %s.\n",
				    Port,
	   				LinkStatus[Port].Link == 1 ? "Up" : "Down",
					LinkStatus[Port].Speed == 1 ? 100 : 10,
	 				LinkStatus[Port].Duplex == 1 ? "full" : "half",
	  				LinkStatus[Port].FlowCtrl == 1 ?
					"FlowCtrl Enabled" : "FlowCtrl Disabled");
			if(Port == 0)
				PRINTK("0x200 = 0x%lx\n",
					   NapaReadRegister(&ax_switch, 0x200));
			else
				PRINTK("0x240 = 0x%lx\n",
					   NapaReadRegister(&ax_switch, 0x240));
		}
	}
/*
	if(LinkStatus[0].Link == LINK_DOWN &&
		  LinkStatus[1].Link == LINK_DOWN)
	{
		netif_carrier_off(dev);
	}
	else
	{
		netif_carrier_on(dev);
	}
*/
}


#endif

/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_poll()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void
ax_poll (void)
{
	unsigned short Isr;



	Isr = NapaReadInterrupt(&ax_switch);

	

	while (Isr) {
	//	printf ("ISR = %X\n",Isr);


		NapaDisableAndAckInterrupt(&ax_switch, Isr);

		if (Isr & IMSR_INT_LINK_CHANGE) {
			PRINTK("Link change interrupt\n");
			ax_check_link();
		}

		if (Isr & IMSR_INT_CPO_EMPTY) {
			PRINTK("Rx interrupt\n");
			ax_rx();
		}

		if(Isr & IMSR_INT_CPI_ERR) {
			PRINTK("CPI error interrupt\n");
			NapaLocalBusCpiReset(&ax_switch);
		}

		if (Isr & IMSR_INT_CPI_FULL) {
			PRINTK("Tx buffer full interrupt\n");
//			netif_stop_queue(dev);
		}

		if (Isr & IMSR_INT_CPI_CLEAR) {
			PRINTK("Tx buffer clean interrupt\n");
//			netif_wake_queue(dev);
		}

		Isr = NapaReadInterrupt(&ax_switch);
	}

	NapaEnableInterrupt(&ax_switch);

}



/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_media_init()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void
ax_media_init(int media, u8 phyID)
{
	int advertise;

	advertise = mdio_read(phyID, MII_ADVERTISE) & ~(ADVERTISE_ALL);
	if(media == 0)
	{
		advertise |= (ADVERTISE_ALL | 0x400);
	}
	else if(media == 1)
	{
		advertise |= (ADVERTISE_100FULL | 0x400);
	}
	else if(media == 2)
	{
		advertise |= ADVERTISE_100HALF;
	}
	else if(media == 3)
	{
		advertise |= (ADVERTISE_10FULL | 0x400);
	}
	else if(media == 4)
	{
		advertise |= ADVERTISE_10HALF;
	}
	mdio_write(phyID, MII_ADVERTISE, advertise);
}



/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_phy_init()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void
ax_phy_init(void)
{
	
	int Data16;
	
	DEBUG_FUNCTION();

	NapaEnablePhy(&ax_switch, Port_0);
	NapaEnablePhy(&ax_switch, Port_1);

	NapaDelayMs(1000);

	PRINTK("Port 0 Phy ID %d\n", phyID[Port_0]);
	PRINTK("Port 1 Phy ID %d\n", phyID[Port_1]);
	NapaSetPhyId(&ax_switch, Port_0, phyID[Port_0]);
	NapaSetPhyId(&ax_switch, Port_1, phyID[Port_1]);

#if(PORT0_DEFAULT_PHY_INTERFACE == PHY_MODE_RMII)
	PRINTK("Port 0 Phy interface : RMII\n");
	NapaSetPhyMode(&ax_switch, Port_0, PHY_MODE_RMII);
#else
	PRINTK("Port 0 Phy interface : MII\n");
	NapaSetPhyMode(&ax_switch, Port_0, PHY_MODE_MII);
#endif

#if(PORT1_DEFAULT_PHY_INTERFACE == PHY_MODE_RMII)
	PRINTK("Port 1 Phy interface : RMII\n");
	NapaSetPhyMode(&ax_switch, Port_1, PHY_MODE_RMII);
#else
	PRINTK("Port 1 Phy interface : MII\n");
	NapaSetPhyMode(&ax_switch, Port_1, PHY_MODE_MII);
#endif

	NapaConfigPhyPolling(&ax_switch, Port_0, ENABLE);
	NapaConfigPhyPolling(&ax_switch, Port_1, ENABLE);


	Led.Led0_Mode = DEFAULT_PHY_LED0;
	Led.Led1_Mode = DEFAULT_PHY_LED1;
	Led.Led2_Mode = DEFAULT_PHY_LED2;
	NapaSetLedMode(&ax_switch, Port_0, &Led);
	NapaSetLedMode(&ax_switch, Port_1, &Led);

	// Speed up PHY link speed
	Data16 = mdio_read( phyID[Port_0], 16);
	mdio_write(phyID[Port_0], 16, (Data16 | 0x1000));

	Data16 = mdio_read(phyID[Port_1], 16);
	mdio_write(phyID[Port_1], 16, (Data16 | 0x1000));

	Data16 = mdio_read(phyID[Port_0], 24);
	mdio_write(phyID[Port_0], 24, (Data16 | 4));

	Data16 = mdio_read(phyID[Port_1], 24);
	mdio_write(phyID[Port_1], 24, (Data16 | 4));

	ax_media_init(media0, phyID[Port_0]);
	ax_media_init(media1, phyID[Port_1]);
	mdio_write(phyID[Port_0], MII_BMCR, (BMCR_ANRESTART | BMCR_ANENABLE));
	mdio_write(phyID[Port_1], MII_BMCR, (BMCR_ANRESTART | BMCR_ANENABLE));
 

} /* End of ax_phy_init() */

/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_close()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static int
ax_close(void)
{
	DEBUG_FUNCTION();
	
	NapaDisableInterrupt(&ax_switch);

	NapaChipReset(&ax_switch);

	
	return 0;
} /* End of ax_open() */



/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_init()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static int ax_init(void)
{
	unsigned char BusMode;

	unsigned  i,j;

	DEBUG_FUNCTION();

	ax_switch.pMemBase = CONFIG_ETHERNET_BASE;
	phyID[Port_0] = PORT0_DEFAULT_PHY_ID;
	phyID[Port_1] = PORT1_DEFAULT_PHY_ID;

	LinkStatus[0].Link   = LINK_DOWN;
	LinkStatus[0].Speed   = 1;
	LinkStatus[0].Duplex   = 1;

	LinkStatus[1].Link   = LINK_DOWN;
	LinkStatus[1].Speed   = 1;
	LinkStatus[1].Duplex   = 1;

		

	BusMode = NapaGetChipMode(&ax_switch);
//	NapaAccessPortInit(BusMode);

	NapaChipReset(&ax_switch);

	NapaDelayMs(1000);
	NapaDisableInterrupt(&ax_switch);
	NapaConfigInterruptLevel(&ax_switch, 0);
	NapaSetInterruptMask(&ax_switch, IMSR_MASK_AGAIN | IMSR_MASK_NEW_LEARN);

	NapaConfigLocalBusCpio(&ax_switch, ENABLE);

	NapaSetMacAddress(&ax_switch, Port_0, dev_addr);
	NapaSetMacAddress(&ax_switch, Port_1, dev_addr);
	NapaSetPktOrder(&ax_switch, BORDER_LITTLE);

	
	ax_phy_init();

	NapaLocalBusCpiReset(&ax_switch);
	NapaLocalBusCpoReset(&ax_switch);
	NapaEnableInterrupt(&ax_switch);
	

	NapaDelayMs(2000);

//	ax_check_link();

	return 0;

} /* End of ax_probe() */




int eth_init(bd_t *bd) {

	unsigned  i,j;

	DEBUG_FUNCTION();

	if (!pbuf) {
		pbuf = malloc(NB*2000);
		if (!pbuf) {
			printf("Cannot allocate rx buffers\n");
			return -1;
		}
	}

	for (i=0;i<6;i++)
		dev_addr[i]=bd->bi_enetaddr[i];	

	printf("AX88783 - %s ESA: %02x:%02x:%02x:%02x:%02x:%02x\n",
	       "environment",
	       dev_addr[0],
	       dev_addr[1],
	       dev_addr[2],
	       dev_addr[3],
	       dev_addr[4],
	       dev_addr[5] );

	ax_init();

	return 0;
}

void eth_halt(void) {

	PRINTK("### eth_halt\n");

//	ax_close();
}

int eth_rx(void) {
	int j, tmo;

	PRINTK("### eth_rx\n");

	tmo = get_timer (0) + TOUT * CFG_HZ;
	while(1) {
		ax_poll();
		if (nrx > 0) {
			PRINTK("nrx %02x \n",nrx);
			for(j=0; j<nrx; j++) {
				NetReceive(&pbuf[j*2000], plen[j]);
			}
			nrx = 0;
			return 1;
		}
		if (get_timer (0) >= tmo) {
			printf("timeout during rx\n");
			return 0;
		}
	}
	return 0;
}

int eth_send(volatile void *packet, int length) {
	int tmo;
	NAPA_STATUS		Status;

	PRINTK("### eth_send len = %x\n",length);

	ax_check_link();
	
	if(LinkStatus[0].Link == LINK_DOWN &&
		  LinkStatus[1].Link == LINK_DOWN)
	{
		printf(" No Cable (timoeut)\n");
	
	}
	else
	{
		NapaDisableInterrupt(&ax_switch);
		Status = NapaLocalBusSendPacket(&ax_switch, (unsigned short)length, (unsigned char*)packet);
		NapaEnableInterrupt(&ax_switch);
		if(Status != NAPA_STATUS_SUCCESS) {
			printf("transmission error \n");

		}

	}

	return 0;

}


#endif
