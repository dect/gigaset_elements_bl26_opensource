/*
 * lcd_st7586s.c -- lcd char driver
 *Based on the code from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.   
 *
 * 
 */
#include "armboot.h"
#include "lcd_st7586s.h"
#include "fonts.h"

#ifdef CONFIG_ST7586S_LCD


/***************       Definitions section             ***************/
#define BUFFER_SIZE 50 /*Buffer size to support the ioctl with the largest data transfer*/	
#define MAX_IOCTL_ARGS 1024 /*Maximum number of args to pass with ioctl*/
#define LCD_DEFAULT_BACKLIGHT_LEVEL 0x7F
#define MAX_SCROLL_TEXT_SIZE 4000

/***      Data types definitions       ***/
#define uchar unsigned char
//Driver structs

typedef struct {
	char left;
	char top;
	char font;
	char visible_size;
	int  size ;
	char text[MAX_SCROLL_TEXT_SIZE];
	int ScrollPos;
	int enable;
}scroll_struct;
scroll_struct m_scroll_struct;
typedef struct {
	uchar left;
	uchar top;
	uchar font;
	uchar blinking_rate;
	uchar state;
	uchar enabled;
	uchar reserved[2];
}cursor_struct;

typedef struct {
	uchar left;
	uchar top;
	uchar width;
 	uchar blinking_rate;
	uchar state;   
	uchar enabled;   
	uchar bitmaps;   
	uchar reserved;   
	uchar  bitmap[4][128]; 
}animation_struct;

/***      Flags definitions     ***/
/***      Hw specific definitions       ***/

/***   Macros   ***/
//#define	START_TIMER(TIMER, TMOUT) {TIMER.expires = jiffies + TMOUT;add_timer(&TIMER);}
//#define	STOP_TIMER(TIMER) {del_timer(&TIMER);}
/***      Function Prototypes       ***/
/** Low level **/

/* LCD function prototype list */
void lcdInit(void);
void lcd_clear_ram(void);
void lcd_print_screen(unsigned char *screen);
void lcd_clear_rect(uchar left,  uchar top, uchar right, uchar bottom);
void lcd_invert_rect(uchar left, uchar top, uchar right, uchar bottom);
void lcd_horz_line(uchar left, uchar right, uchar row);
void lcd_vert_line(uchar top, uchar bottom, uchar column);
void lcd_clr_horz_line(uchar left, uchar right, uchar row);
void lcd_clr_vert_line(uchar top, uchar bottom, uchar column); 
void lcd_draw_border(uchar left, uchar top, uchar right, uchar bottom);
void lcd_clear_border(uchar left, uchar top, uchar right, uchar bottom);
void lcd_glyph(uchar left, uchar top, uchar width, uchar height, uchar *glyph, uchar store_width);
void lcd_animated_glyph(uchar left, uchar top, uchar width, uchar num_of_bitmaps, uchar *glyph1, uchar *glyph2, uchar *glyph3);
void lcd_text(uchar left, uchar top, uchar font, char *str);
void lcd_update(uchar top, uchar bottom);
void lcd_cursor_set(uchar left, uchar top, uchar font, uchar ShowCursor, uchar blinking_rate);
/**/
void lcd_contrast_level(unsigned short level);
void lcd_contrast_up(void);
void lcd_contrast_down(void);
void lcd_sleepmode_enter(void);
void lcd_sleepmode_exit(void);
void lcd_scroll_init(char *buf);
 uchar lcd_font_width_get(uchar font_width);
//Timers
static void InitializeTimer(struct timer_list *timer_ptr, void (*timer_function)(unsigned long param));
//static void lcd_timer_refresh_fn(unsigned long param);
static void scroll_timer_refresh(unsigned long param);
static void cursor_timer_refresh(unsigned long param);
static void animation_timer_refresh(unsigned long param);

const unsigned char l_mask_array[8] ={0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
uchar l_display_array[Y_BYTES][X_BYTES];
//const unsigned char l_col_array[3] ={0xE0,0x1C,0x03};
#ifdef INVERSE_DISPLAY
const unsigned char l_col_array[3] ={0x03,0x1C,0xE0};
#else
const unsigned char l_col_array[3] ={0xE0,0x1C,0x03};
#endif

cursor_struct m_cursor;

#define MAX_ANIMATED_BITMAPS 8
animation_struct m_animation[MAX_ANIMATED_BITMAPS];


#define mdelay(x) udelay(x*1000)
 
//HW Patch

/***************          Main body             ***************/
unsigned char test_screen[X_BYTES*Y_BYTES];
unsigned short scr_top_border, scr_bottom_border, scr_left_border, scr_right_border, scr_left_dim, scr_right_dim;	
unsigned short scr_left_offset, scr_right_offset;
/****************************************************************************************************/
/************                                        LOW LEVEL DRIVER FUNTIONS                                                            *********/
/****************************************************************************************************/

/******************************************************* 
	Description: 	Initializes hw and driver internal state
	Note:			
 ********************************************************/
void lcd_init_col_lines(unsigned short screen_top, unsigned short screen_bottom, unsigned short screen_left, unsigned short screen_right){
	unsigned short tmp;

	//Setup the line\column borders

	if( (screen_bottom<=screen_top) || (screen_right<=screen_left)) 
		return;
	
	//Check if dimensions not alligned to 2pixels or 3 pixels
	scr_top_border =  screen_top;
	scr_bottom_border = screen_bottom;
	scr_left_border = screen_left;
	scr_right_border = screen_right;

#if 1
	scr_left_dim =  screen_left/3;
	scr_left_offset =  screen_left%3;
	if(screen_right%3){
		scr_right_dim =  screen_right/3;
		scr_right_offset =  3-screen_right%3;
	}
	else{
		scr_right_dim =  screen_right/3;
		scr_right_offset =  0;
	}
#else
	scr_right_dim =  screen_right;
#endif
	
#ifdef INVERSE_DISPLAY
	tmp = scr_top_border;
	scr_top_border = 160 - scr_bottom_border;
	scr_bottom_border = 160 - tmp;
	tmp = scr_left_dim;
	scr_left_dim = 127 - scr_right_dim;
	scr_right_dim = 127 - tmp;
#endif

	lcd_out_ctl( 0x002B ); //Row Address Set
	lcd_out_dat( 0x00); //
	lcd_out_dat( scr_top_border); //Start Line
	lcd_out_dat( 0x00); //
	lcd_out_dat( scr_bottom_border); //End Line 
	lcd_out_ctl( 0x002A ); //Column Address Set
	lcd_out_dat( 0x00); //
	lcd_out_dat( scr_left_dim); //Start Column=0
	lcd_out_dat( 0x00); //
	lcd_out_dat( scr_right_dim); //End Column =127 //columns represent 8-bit words = 3 pixels
	
	lcd_out_ctl( 0x0037 ); //Start Line
	lcd_out_dat( 0x00 ); //Start Line
	lcd_out_ctl( 0x00B1 ); //First COM
	lcd_out_dat( 0 ); //First COM

	lcd_out_ctl( 0x00B0 ); //Duty Setting
	lcd_out_dat( (scr_bottom_border - scr_top_border - 1) ); 
}

void lcdInit(){
	unsigned char i;
	int j;
	/*Processor specific commands*/

	PROTECT_OVER_16M_SDRAM_ACEESS
	


	//setup reset
	SetWord(P2_RESET_DATA_REG,8);		//P2_3 low
	udelay(10000);
	SetWord(P2_SET_DATA_REG,8);		//P2_3 low
	udelay(50000);
	udelay(50000);
	udelay(50000);
	RESTORE_OVER_16M_SDRAM_ACEESS 	 	
	
//===============================
	//Copied from datasheet
	lcd_out_ctl( 0x00D7 ); //Disable Auto Read
	lcd_out_dat( 0x009F ); 

	lcd_out_ctl( 0x00E0 ); //EnableOTP Read
	lcd_out_dat( 0x0000 ); 
	mdelay( 10 );

	lcd_out_ctl( 0x00E3 ); //OTP Up-Load
	mdelay( 20 );

	lcd_out_ctl( 0x00E1 ); //OTP Control Out
	lcd_out_ctl( 0x0011 ); //Sleep Out
	lcd_out_ctl( 0x0028 ); //Display OFF
	mdelay( 50 );

	lcd_out_ctl( 0x00C0 ); //Vop=B9h
	//lcd_out_dat( 0x00B9 ); 
	lcd_out_dat( 0x0002 ); 
	lcd_out_dat( 0x0001 ); 

	lcd_out_ctl( 0x00C3 ); //BIAS=1/9
	lcd_out_dat( 0x0005 ); 

	lcd_out_ctl( 0x00C4 ); //Booster=x8
//	lcd_out_dat( 0x0007 ); 
	lcd_out_dat( 0x0001 ); 

	lcd_out_ctl( 0x00D0 ); //Enable Analog Circuit
	lcd_out_dat( 0x001D ); 

	lcd_out_ctl( 0x00B5 ); //N-Line=0
	lcd_out_dat( 0x0000 ); 

	lcd_out_ctl( 0x0038 ); //Gray Scale Mode
//	lcd_out_ctl( 0x0039 ); //Monochrome Mode
	lcd_out_ctl( 0x003A ); //Enable DDRAM interface
	lcd_out_dat( 0x0002 ); 

	lcd_out_ctl( 0x0036 ); //Scan Direction Setting
#ifdef INVERSE_DISPLAY	
	lcd_out_dat( 0x00C0 ); 
#else
	lcd_out_dat( 0x0000 ); 
#endif

	lcd_out_ctl( 0x00B0 ); //Duty Setting
	lcd_out_dat( 0x009F ); 

//	lcd_out_ctl( 0x0013 ); //Partial Display Off

/*	lcd_out_ctl( 0x00B4 ); //Partial Display
	lcd_out_dat( 0x00A0 ); 

	lcd_out_ctl( 0x0030 ); //Partial Display Area-COM0~COM119
	lcd_out_dat( 0x0000 ); 
	lcd_out_dat( 0x0000 ); 
	lcd_out_dat( 0x0000 ); 
	lcd_out_dat( 0x0077 ); 
*/

	lcd_out_ctl( 0x0020 ); //Display Inversion OFF

	lcd_out_ctl( 0x002A ); //Column Address Setting
	lcd_out_dat( 0x0000 ); //SEG0->SEG384
	lcd_out_dat( 0x0000 ); 
	lcd_out_dat( 0x0000 ); 
	lcd_out_dat( 0x007F ); 

	lcd_out_ctl( 0x002B ); //Row Address Setting
	lcd_out_dat( 0x0000 ); //COM0->COM160
	lcd_out_dat( 0x0000 ); 
	lcd_out_dat( 0x0000 ); 
	lcd_out_dat( 0x009F ); 

	lcd_out_ctl( 0x002C ); //Write
	for(j=0;j<384*160/3;j++)
		lcd_out_dat( 0x00 ); 

	//lcd_out_ctl( 0x0023 ); //All Pixel ON
	//mdelay(5);
	//lcd_out_ctl( 0x0022 ); //All Pixel OFF

	lcd_out_ctl( 0x0029 ); //Display ON

	mdelay(100);
	lcd_out_ctl( 0x0029 ); //Display On

//================================
	lcd_clear_ram();

lcd_text(1, 1, 0, "Booting...");
lcd_update(SCRN_TOP, SCRN_BOTTOM);

#if 1	
	/*This code is not part of the hw init sequence*/
 	m_cursor.state =0;
	m_cursor.enabled =0;
	for (i=0;i<MAX_ANIMATED_BITMAPS;i++)	
	{
		m_animation[i].state = 0;
		m_animation[i].enabled = 0;
		m_animation[i].bitmaps = 0;
	}
 
//lcd_cursor_set(1, 40, 0, 1, 5);
#endif	
}

void lcd_print_screen(unsigned char *screen)
{
	unsigned char x, y, z;
	unsigned char width=128, y_offset = 10;

//	lcd_init_col_lines(0, height-1, 0, width-1 );
	lcd_init_col_lines(SCRN_TOP, SCRN_BOTTOM, SCRN_LEFT, SCRN_RIGHT );
	lcd_out_ctl( 0x002C ); //Enter memory write mode
	//printk("screen=%x \n", screen);
	z=40;//(scr_right_border-width)/2;
	for( y = 0; y <SCRN_BOTTOM; y++)
	{
		for (x=0; x < z; x++)
		{	
			lcd_out_dat(0x00);
		}
		//colptr = screen + (width * (y>>3));
		for (x=0; x < width; x++)
		{		
		//	printk("&screen[(%d * (%d>>3)) + x] = 0x%x \n", width, y, &screen[(width * (y>>3)) + x]);
			if((screen[(width * ((y+y_offset)>>3)) + x])& (0x1<<((y+y_offset)%8))){
//			if(*(colptr+x) & (0x1<<(y%8))){
				lcd_out_dat(0xff);
//				printk("*");
			}
			else{
				lcd_out_dat(0x00);
//				printk("-");
			}
		}
		for (x=z+width; x < scr_right_border + 1; x++)
		{	
			lcd_out_dat(0x00);
		}
	}		
}

 /******************************************************* 
	Description:	Erases lcd RAM
	Note:			
 ********************************************************/
void lcd_clear_ram(void)
{
	ushort p, i;
	
	
	if (m_cursor.enabled){
		m_cursor.enabled=0;
//		STOP_TIMER(cursor_timer)
	}
	lcd_init_col_lines(SCRN_TOP, SCRN_BOTTOM, SCRN_LEFT, SCRN_RIGHT );
	lcd_out_ctl( 0x002C ); //Enter memory write mode
  	for(p=0; p<Y_BYTES; p++)
	{		
		for(i=0; i<X_BYTES; i++)
		{
			l_display_array[p][i]=0; /*Clear buffer*/
			{
				lcd_out_dat(0x00);				/* clear the data */
			}
		}
	}
}

/*
**
** 	Clears the display memory starting at the left/top  and going to
**  the right/bottom . No runtime error checking is performed. It is 
**  assumed that left is less than right and that top is less than 
**  bottom
**
*/

void lcd_clear_rect(uchar left,  uchar top,    
			        uchar right, uchar bottom)
{
	uchar bit_pos;
	uchar mask;

	int y;
	int x;

	for(y=top;y<bottom;y++)
	{
	x = left/3;
	bit_pos = left%3;		   /* get starting bit offset into byte */
//	mask = l_mask_array[bit_pos];  /* get mask for this bit */
	mask = l_col_array[bit_pos];  /* get mask for this bit */
   	if(bit_pos == 2)
		l_display_array[y][x] &= ~mask;
   	if(bit_pos == 1)
	{
		l_display_array[y][x] &= ~mask;
		mask = l_col_array[bit_pos+1];  /* get mask for this bit */
		l_display_array[y][x] &= ~mask;
	}
   	if(bit_pos == 0)
    	l_display_array[y][x] = 0x00;

  	for(x = left/3+1; x < right/3; x++)
  	{
    	l_display_array[y][x] = 0x00;
  	}

	bit_pos = right%3;		   /* get ending bit offset into byte */
	mask = l_col_array[bit_pos];  /* get mask for this bit */
   	if(bit_pos == 2)
		l_display_array[y][x] = 0x00;
   	if(bit_pos == 0)
		l_display_array[y][x] &= ~mask;
   	if(bit_pos == 1)
	{
		l_display_array[y][x] &= ~mask;
		mask = l_col_array[bit_pos-1];  /* get mask for this bit */
		l_display_array[y][x] &= ~mask;
	}
	}
}

/*
**
** Inverts the display memory starting at the left/top and going to
** the right/bottom. No runtime error checking is performed. It is 
** assumed that left is less than right and that top is less than 
** bottom 
** 
*/

void lcd_invert_rect(uchar left,  uchar top,    
			         uchar right, uchar bottom)
{
	uchar bit_pos;
	uchar mask;

	int y;
	int x;

	for(y=top;y<bottom;y++)
	{
	x = left/3;
	bit_pos = left%3;		   /* get starting bit offset into byte */
//	mask = l_mask_array[bit_pos];  /* get mask for this bit */
	mask = l_col_array[bit_pos];  /* get mask for this bit */
   	if(bit_pos == 2)
		l_display_array[y][x] ^= mask;
   	if(bit_pos == 1)
	{
		l_display_array[y][x] ^= mask;
		mask = l_col_array[bit_pos+1];  /* get mask for this bit */
		l_display_array[y][x] ^= mask;
	}
   	if(bit_pos == 0)
    	l_display_array[y][x] ^= 0xFF;

  	for(x = left/3+1; x < right/3; x++)
  	{
    	l_display_array[y][x] ^= 0xFF;
    	//l_display_array[y][byte_offset] = 0xFF;
  	}

	bit_pos = right%3;		   /* get ending bit offset into byte */
	mask = l_col_array[bit_pos];  /* get mask for this bit */
   	if(bit_pos == 2)
		l_display_array[y][x] ^= 0xFF;
   	if(bit_pos == 0)
		l_display_array[y][x] ^= mask;
   	if(bit_pos == 1)
	{
		l_display_array[y][x] ^= mask;
		mask = l_col_array[bit_pos-1];  /* get mask for this bit */
		l_display_array[y][x] ^= mask;
	}
	}
}

/*
**
** Draws a line into the display memory starting at left going to
** right, on the given row. No runtime error checking is performed.  
** It is assumed that left is less than right.
**
*/

void lcd_horz_line(uchar left, uchar right, uchar row)
{
	uchar bit_pos;
	uchar mask;
	int y;
	int x;

	y = row;
	x = left/3;

	bit_pos = left%3;		   /* get starting bit offset into byte */
//	mask = l_mask_array[bit_pos];  /* get mask for this bit */
	mask = l_col_array[bit_pos];  /* get mask for this bit */
   	if(bit_pos == 2)
		l_display_array[y][x] |= mask;
   	if(bit_pos == 1)
	{
		l_display_array[y][x] |= mask;
		mask = l_col_array[bit_pos+1];  /* get mask for this bit */
		l_display_array[y][x] |= mask;
	}
   	if(bit_pos == 0)
    	l_display_array[y][x] = 0xFF;

  	for(x = left/3+1; x < right/3; x++)
  	{
    	l_display_array[y][x] = 0xFF;
    	//l_display_array[y][byte_offset] = 0xFF;
  	}

	bit_pos = right%3;		   /* get ending bit offset into byte */
	mask = l_col_array[bit_pos];  /* get mask for this bit */
   	if(bit_pos == 2)
		l_display_array[y][x] = 0xFF;
   	if(bit_pos == 0)
		l_display_array[y][x] |= mask;
   	if(bit_pos == 1)
	{
		l_display_array[y][x] |= mask;
		mask = l_col_array[bit_pos-1];  /* get mask for this bit */
		l_display_array[y][x] |= mask;
	}
}

/*
**
** Draws a vertical line into display memory starting at the top
** going to the bottom in the given column. No runtime error checking 
** is performed. It is assumed that top is less than bottom and that 
** the column is in range.
**
*/

void lcd_vert_line(uchar top, uchar bottom, uchar column)
{
	uchar bit_pos;
	uchar byte_offset;
	uchar mask;
	int y;
	
	byte_offset = column/3;		   /* get starting bit offset into byte */
	bit_pos = column%3;		   /* get starting bit offset into byte */
//	mask = l_mask_array[bit_pos];  /* get mask for this bit */
	mask = l_col_array[bit_pos];  /* get mask for this bit */

  	for(y = top; y < bottom; y++)
  	{
    	l_display_array[y][byte_offset] |= mask;
  	}
}

/*
**
** Clears a line into the display memory starting at left going to
** right, on the given row. No runtime error checking is performed.  
** It is assumed that left is less than right.
**
*/

void lcd_clr_horz_line(uchar left, uchar right,
		               uchar row)
{
	uchar bit_pos;
	uchar mask;
	
	int y;
	int x;

	y = row;
	x = left/3;

	bit_pos = left%3;		   /* get starting bit offset into byte */
//	mask = l_mask_array[bit_pos];  /* get mask for this bit */
	mask = l_col_array[bit_pos];  /* get mask for this bit */
   	if(bit_pos == 2)
		l_display_array[y][x] &= ~mask;
   	if(bit_pos == 1)
	{
		l_display_array[y][x] &= ~mask;
		mask = l_col_array[bit_pos+1];  /* get mask for this bit */
		l_display_array[y][x] &= ~mask;
	}
   	if(bit_pos == 0)
    	l_display_array[y][x] = 0x00;

  	for(x = left/3+1; x < right/3; x++)
  	{
    	l_display_array[y][x] = 0x00;
  	}

	bit_pos = right%3;		   /* get ending bit offset into byte */
	mask = l_col_array[bit_pos];  /* get mask for this bit */
   	if(bit_pos == 2)
		l_display_array[y][x] = 0x00;
   	if(bit_pos == 0)
		l_display_array[y][x] &= ~mask;
   	if(bit_pos == 1)
	{
		l_display_array[y][x] &= ~mask;
		mask = l_col_array[bit_pos-1];  /* get mask for this bit */
		l_display_array[y][x] &= ~mask;
	}
}


/*
**
** Clears a vertical line into display memory starting at the top
** going to the bottom in the given column. No runtime error checking 
** is performed. It is assumed that top is less than bottom and that 
** the column is in range.
**
*/

void lcd_clr_vert_line(uchar top, uchar bottom, uchar column)
{
	uchar bit_pos;
	uchar byte_offset;
	uchar mask;
	
	int y;
	
	byte_offset = column/3;		   /* get starting bit offset into byte */
	bit_pos = column%3;		   /* get starting bit offset into byte */
//	mask = l_mask_array[bit_pos];  /* get mask for this bit */
	mask = l_col_array[bit_pos];  /* get mask for this bit */

  	for(y = top; y < bottom; y++)
  	{
    	l_display_array[y][byte_offset] &= ~mask;
  	}
}

/*
**
** 	Draws a box in display memory starting at the left/top and going
**  to the right/bottom. No runtime error checking is performed.
**  It is assumed that left is less than right and that top is less 
**  than bottom.
** 
*/

void lcd_draw_border(uchar left, uchar top, uchar right, uchar bottom)
{
	
  	/* to draw a box requires two vertical lines */
   	lcd_vert_line(top,bottom,left);
   	lcd_vert_line(top,bottom,right);
  	/* and two horizonal lines */
   	lcd_horz_line(left,right,top);
   	lcd_horz_line(left,right,bottom);
}

/*
**
** Clears a box in display memory starting at the Top left and going
** to the bottom right. No runtime error checking is performed and
** it is assumed that Left is less than Right and that Top is less 
** than Bottom.
**
*/

void lcd_clear_border(uchar left, uchar top, uchar right, uchar bottom)
{
	
  	/* to undraw the box undraw the two vertical lines */
  	lcd_clr_vert_line(top,bottom,left);
  	lcd_clr_vert_line(top,bottom,right);

  	/* and the two horizonal lines that comprise it */
  	lcd_clr_horz_line(left,right,top);
    lcd_clr_horz_line(left,right,bottom);
}

/*
**
** Writes a glyph to the display at location x,y
**
** Arguments are:
**    column     - x corrdinate of the left part of glyph          
**    row        - y coordinate of the top part of glyph       
**    width  	 - size in pixels of the width of the glyph    
**    height 	 - size in pixels of the height of the glyph   
**    glyph      - an uchar pointer to the glyph pixels 
**                 to write assumed to be of length "width"
**
*/

void lcd_glyph(uchar left, uchar top,
			   uchar width, uchar height,
			   uchar *glyph, uchar store_width)
{
	uchar bit_pos;
	uchar mask;
	uchar char_mask;
	uchar x;
	uchar *glyph_scan;
	uchar glyph_offset;
	uchar remaining_bits;

	int y;
	int xl;

	y = top;
	xl = left;

	glyph_scan = glyph;	 /* point to base of the glyph */
  	for (y = top; y < (top + height); y++)
  	{
		glyph_offset = 0;
		char_mask = 0x80;			/* initial character glyph mask */
		x = xl;
		remaining_bits = 8;	/* number of bits left in byte */

    	/* boundary checking here to account for the possibility of  */
    	/* write past the bottom of the screen.                        */
    	while(remaining_bits--) /* while there are bits still to write */
    	{
			bit_pos = x%3;		   /* get starting bit offset into byte */
			mask = l_col_array[bit_pos];	/* get mask for this bit */

			/* check if the character pixel is set or not */
			if(*(glyph_scan+glyph_offset) & char_mask)
			{
				l_display_array[y][x/3] |= mask;	/* set image pixel */
			}
			else
			{
      			l_display_array[y][x/3] &= ~mask;	/* clear the image pixel */
			}
			x++;

			/* shift over to next glyph bit */
			char_mask >>= 1;
			if(char_mask == 0)				/* reset for next byte in raster */
			{
				char_mask = 0x80;
				glyph_offset++;
			}
		}
		/* bump the glyph scan to next raster */
		glyph_scan += store_width;
	}
}


void lcd_animated_glyph(uchar left, uchar top,
				uchar width, uchar num_of_bitmaps, uchar *glyph1, uchar *glyph2, uchar *glyph3)
{	 
	int i;
	//STOP_TIMER(animation_timer);

 	// if width == 0, stop animation and clear glyph region
	if(width == 0) {
 		for (i=0;i<MAX_ANIMATED_BITMAPS;i++) {
			if (m_animation[i].enabled && (m_animation[i].left == left) && (m_animation[i].top == top)) {

				lcd_clear_rect(left, top, left + m_animation[i].width, top + m_animation[i].width);
 
				m_animation[i].enabled = 0;
				m_animation[i].state = 0;

 				lcd_update(top, top + m_animation[i].width);
 				break;
			}
		}
 
		for (i=0;i<MAX_ANIMATED_BITMAPS;i++) {
			if (m_animation[i].enabled) {
				//START_TIMER(animation_timer, 100 );
				return;
			}
		}

		return;
	}

	for (i=0;i<MAX_ANIMATED_BITMAPS;i++) {
		if (m_animation[i].enabled && (m_animation[i].left == left) && (m_animation[i].top == top)) {
			//START_TIMER(animation_timer, 100 );
			return ;
		}
	}

	for (i=0;i<MAX_ANIMATED_BITMAPS;i++)
	{
	  if (!m_animation[i].enabled)
		  break;
	}
	if (i==MAX_ANIMATED_BITMAPS) {
 //		START_TIMER(animation_timer, 100 );
		return ;
	}

	m_animation[i].enabled = 1;
	m_animation[i].left =left; 
	m_animation[i].top =top; 
	m_animation[i].width =width; 
	m_animation[i].blinking_rate =200; 
	m_animation[i].state =0; 
	m_animation[i].bitmaps =num_of_bitmaps; 
	memcpy(m_animation[i].bitmap[0],glyph1,width*width/8);
	memcpy(m_animation[i].bitmap[1],glyph2,width*width/8);
	memcpy(m_animation[i].bitmap[2],glyph3,width*width/8);

	lcd_glyph(left,top,width,width,&m_animation[i].bitmap[0][0],width/8);  /* plug symbol into buffer */
 //	START_TIMER(animation_timer, 100 );
	lcd_update(SCRN_TOP, SCRN_BOTTOM);
 }


/*
**
**	Prints the given string at location x,y in the specified font.
**  Prints each character given via calls to lcd_glyph. The entry string
**  is null terminated and non 0x20->0x7e characters are ignored.
**
**  Arguments are:                                                   
**      left       coordinate of left start of string.                
**      top        coordinate of top of string.
**      font       font number to use for display                
**      str   	   text string to display
**
*/

void lcd_text(uchar left, uchar top, uchar font, char *str)
{
  	ushort x = left;
  	uchar glyph;
  	uchar width;
	uchar height;
	uchar store_width;
	uchar  *glyph_ptr;

	
if (str==NULL) {printk("INVALID STRING ....");}
//printk("LEFT %d TOP %d FONT %d\n", left, top, font);
//printk("TEXT STRING %s \n",str);
  	while(*str != 0x00)
  	{
    	glyph = (uchar)*str;

		/* check to make sure the symbol is a legal one */
		/* if not then just replace it with the default character */
		if((glyph < fonts[font].glyph_beg) || (glyph > fonts[font].glyph_end))
		{
			glyph = fonts[font].glyph_def;
		}

    	/* make zero based index into the font data arrays */
    	glyph -= fonts[font].glyph_beg;
    	width = fonts[font].fixed_width;	/* check if it is a fixed width */
		if(width == 0)
		{
			width=fonts[font].width_table[glyph];	/* get the variable width instead */
		}

		height = fonts[font].glyph_height;
		store_width = fonts[font].store_width;

		glyph_ptr = fonts[font].glyph_table + ((unsigned int)glyph * (unsigned int)store_width * (unsigned int)height);

		/* range check / limit things here */
		if(x > SCRN_RIGHT)
		{
			x = SCRN_RIGHT;
		}
		if((x + width) > SCRN_RIGHT+1)
		{
			width = SCRN_RIGHT - x + 1;
		}
		if(top > SCRN_BOTTOM)
		{
			top = SCRN_BOTTOM;
		}
		if((top + height) > SCRN_BOTTOM+1)
		{
			height = SCRN_BOTTOM - top + 1;
		}

		lcd_glyph(x,top,width,height,glyph_ptr,store_width);  /* plug symbol into buffer */

		x += width;							/* move right for next character */
		str++;								/* point to next character in string */
	}
}

 
/*
**
** Updates area of the display. Writes data from display 
** RAM to the lcd display controller.
** 
** Arguments Used:                                      
**    top     top line of area to update.         
**    bottom  bottom line of area to update.
**
*/

int protection_flag = 0;
void lcd_update(uchar top, uchar bottom)
{
	ushort x, y;
	uchar *colptr;
	
	
	if (protection_flag) return;
	protection_flag = 1;
	top = 0;//????
	bottom = bottom;//SCRN_BOTTOM;//???

	lcd_init_col_lines(SCRN_TOP, SCRN_BOTTOM, SCRN_LEFT, SCRN_RIGHT  );
	lcd_out_ctl( 0x002C ); //Enter memory write mode
	for( y = top; y <= bottom; y++)
	{
		colptr = &l_display_array[y][0];
		for (x=0; x < X_BYTES; x++)
		{		
			lcd_out_dat(*(colptr+x));
		}
	}	

	protection_flag=0;
}


void lcd_cursor_set(uchar left, uchar top, uchar font, uchar ShowCursor, uchar blinking_rate)
{
	uchar width;
	
	//STOP_TIMER(cursor_timer);
 	width=fonts[font].fixed_width;
	if (width==0) width=8;
	if (m_cursor.enabled)
	{
		m_cursor.enabled=0;
   		if(m_cursor.state) 
		{
//			printk("Invert cursor position [%d] [%d] \n", m_cursor.left, width);
			lcd_invert_rect(m_cursor.left, m_cursor.top, m_cursor.left + width - 1, m_cursor.top + fonts[m_cursor.font].glyph_height - 1);
			lcd_update(SCRN_TOP, SCRN_BOTTOM);
			m_cursor.state = 0;
  		}//else printk("Cursor is in cleared state \n");
	}//else printk("new text ... \n");
 	if(ShowCursor == 0)	return;
 	m_cursor.left = left;
	m_cursor.top = top;
	m_cursor.font = font;
	m_cursor.blinking_rate = blinking_rate;
  	lcd_invert_rect(m_cursor.left, m_cursor.top, m_cursor.left + width - 1, m_cursor.top + fonts[m_cursor.font].glyph_height - 1);
 	lcd_update(SCRN_TOP, SCRN_BOTTOM);
 //  	START_TIMER(cursor_timer, blinking_rate / 2);
	m_cursor.state = 1;
	m_cursor.enabled = 1;
}


void lcd_scroll_init(char *buf)
{
	char visible_buffer[50];
	char scroll_enable = buf[4];
	
  	if (m_scroll_struct.top == buf[1])
	{
  	//	STOP_TIMER(scroll_timer);
	}

	if ((scroll_enable == 0 )|| ( buf[3] >strlen(&buf[5])))
	{
  		memset(visible_buffer, 0 , 50);
		strncpy(visible_buffer, &buf[5], buf[3]);
 		lcd_text(buf[0], buf[1], buf[2], visible_buffer);
  		return;
	}
 
 
//	if(scroll_enable == -1)
//	{
 //		STOP_TIMER(scroll_timer);
//		return;
//	}
 	if( buf[3] <strlen(&buf[5]))
	{
	//	STOP_TIMER(scroll_timer);
		m_scroll_struct.left = buf[0];
		m_scroll_struct.top = buf[1];
		m_scroll_struct.font = buf[2];
		m_scroll_struct.visible_size = buf[3];
		strcpy(m_scroll_struct.text, &buf[5]);
		m_scroll_struct.size = strlen(m_scroll_struct.text);
		m_scroll_struct.ScrollPos = 0;

		memset(visible_buffer, 0 , 50);
		strncpy(visible_buffer, m_scroll_struct.text, m_scroll_struct.visible_size);
		lcd_text(m_scroll_struct.left, m_scroll_struct.top, m_scroll_struct.font, visible_buffer);

		 
		//if(m_scroll_struct.visible_size < m_scroll_struct.size) 
		{
			// add a space at the end of the text
			//strcat(m_scroll_struct.text, " ");
			//m_scroll_struct.size++;
			m_scroll_struct.enable =1;
  	//		START_TIMER(scroll_timer, 100);
			 
			 
		}
	}else{
  		memset(visible_buffer, 0 , 50);
		strncpy(visible_buffer, &buf[5], buf[3]);
 		lcd_text(buf[0], buf[1], buf[2], visible_buffer);
 	//	STOP_TIMER(scroll_timer);
	}
	 
}
uchar lcd_font_width_get(uchar font_width)
{
 	return fonts[font_width].fixed_width;
} 

/******************************************************* 
	Description: 	Sets the lcd contrast
	Input:		level: Brightness level (5-410). We map 0-255 to 0-511 by doubling the input level.				
 ********************************************************/
void lcd_contrast_level(unsigned short level){
	
	lcd_out_ctl(0x00C0);
	lcd_out_dat(level & 0xFF);
	lcd_out_dat(level>>8);
}
/******************************************************* 
	Description: 	
	Input:			
 ********************************************************/
 
void lcd_contrast_up(void){	

	
	lcd_out_ctl(0x00C1);
	
}
/******************************************************* 
	Description: 	
	Input:			
 ********************************************************/
void lcd_contrast_down(void){

	
	lcd_out_ctl(0x00C2);
	
}	
/******************************************************* 
	Description: 	
	Input:			
 ********************************************************/
void lcd_sleepmode_enter(void){	
	
	lcd_out_ctl(0x0010);
}
/******************************************************* 
	Description: 	
	Input:			
 ********************************************************/
void lcd_sleepmode_exit(void){
	
	lcd_out_ctl(0x0011);
}	
/****************************************************************************************************/
/************                                       HIGH LEVEL DRIVER FUNTIONS                                                            *********/
/****************************************************************************************************/

 /*static void lcd_timer_refresh_fn(unsigned long param){
	lcd_update(SCRN_TOP, SCRN_BOTTOM);
	START_TIMER(lcd_timer, lcd_conf->refresh_tmout);	
}*/
 
 static void scroll_timer_refresh(unsigned long param)
 {
 	char visible_buffer[50];
	int i;
 
	m_scroll_struct.text[m_scroll_struct.size] = m_scroll_struct.text[0];
  
	for(i=0 ; i < m_scroll_struct.size ; i++)
		m_scroll_struct.text[i] = m_scroll_struct.text[i+1];

	m_scroll_struct.text[m_scroll_struct.size] = '\0';

	memcpy(visible_buffer, m_scroll_struct.text, m_scroll_struct.visible_size);
	visible_buffer[(int)m_scroll_struct.visible_size] = '\0';
	lcd_text(m_scroll_struct.left, m_scroll_struct.top, m_scroll_struct.font, visible_buffer);
 
	lcd_update(SCRN_TOP, SCRN_BOTTOM);
 //	START_TIMER(scroll_timer, 100);
}

 static void animation_timer_refresh(unsigned long param) {
	int i;
	int found =0; 

	for (i=0;i<MAX_ANIMATED_BITMAPS;i++)
	{
			if (m_animation[i].enabled)
			{
				m_animation[i].state++;
				if (m_animation[i].state==m_animation[i].bitmaps)
					m_animation[i].state = 0;

				lcd_glyph(m_animation[i].left,m_animation[i].top,
					m_animation[i].width,
					m_animation[i].width,	
					&m_animation[i].bitmap[m_animation[i].state][0],
					m_animation[i].width/8);  /* plug symbol into buffer */
				found =1; 
			}
	}
	lcd_update(SCRN_TOP, SCRN_BOTTOM);
	if (found){
	//	START_TIMER(animation_timer, 100 );
	}
  } 
static void cursor_timer_refresh(unsigned long param) {
 	uchar width;
 	 
	if (!m_cursor.enabled) {printk("timer not enabled ...\n");return;}
 
	width= fonts[m_cursor.font].fixed_width;
 	if (width==0)  
  		width=8;//fonts[m_cursor.font].width_table[glyph];
 
	lcd_invert_rect(m_cursor.left, m_cursor.top,m_cursor.left+ width - 1, m_cursor.top + fonts[m_cursor.font].glyph_height - 1);
	if (m_cursor.state==1) m_cursor.state=0;
	else m_cursor.state=1;

  	lcd_update(SCRN_TOP, SCRN_BOTTOM);
//	START_TIMER(cursor_timer, m_cursor.blinking_rate / 2);		 
}

void lcdGotoXY(unsigned short x, unsigned short y)
{
        unsigned short page;

        //lcd columns are from 4 to 131^M
/*        x+=4;
        page = y>>3;

        lcd_ctrl_write(G_LCD_SET_PAGE|page);
        //set col high
        lcd_ctrl_write(G_LCD_SET_COL_HIGH|((x&0xff)>>4));
        //set col low
        lcd_ctrl_write(G_LCD_SET_COL_LOW |(x&0xf));
*/
}



void graph_lcd_disp_line(unsigned short line,char* data)
{
        unsigned i,j;

	lcd_horz_line(0, SCRN_RIGHT, line);

/*
//      lcdGotoXY(0,(line+6)*8);
        lcdGotoXY(0,(line+0)*8);
        for (i=0;i<128;i++) {
                lcd_data_write(0);
        }
//      lcdGotoXY(0,(line+6)*8);
        lcdGotoXY(0,(line+0)*8);

//      lcdPrintData(data,16,&Font_System5x8);
*/
}

#endif
