/*
 * lcd_st7529.c -- lcd char driver
 *Based on the code from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.
 *
 *
 */

/***************       Include headers section       ***************/

#include "armboot.h"
#include "st7032.h"
#include "fonts.h"

#ifdef CONFIG_ST7032_LCD

#if (defined CONFIG_CHAR_LCD) || (defined CONFIG_GRAPH_LCD) || (defined CONFIG_NT75451_LCD)
#error "ST7567 LCD already defined"
#endif




#define X_BYTES				128
#define Y_BYTES	      		4

#define COL_MIN				0x04//0x00
//#define COL_MAX				0x83
#define SCRN_TOP			0
#define SCRN_BOTTOM			31
#define SCRN_LEFT			0//0x04
#define SCRN_RIGHT			127//0x83


#define LCD_CS_LOW
#define LCD_CS_HIGH
#define LCD_A0_LOW
#define LCD_A0_HIGH
#define LCD_RESET_LOW
#define LCD_RESET_HIGH



#define PAGE_MIN			0
#define PAGE_MAX			(SCRN_BOTTOM/8)

#define MAX_SCROLL_TEXT_SIZE 4000

#define lcd_delay(X) (udelay(10*X))


	#define LCD_START_WRITE_DATA	{StartACCESSBus();I2C_8Bit_md_write(SLAVE_ADDR);I2C_8Bit_md_write(DATA_BYTES);}
	#define LCD_START_WRITE_INST	{StartACCESSBus();I2C_8Bit_md_write(SLAVE_ADDR);I2C_8Bit_md_write(CONTROL_BYTES);}
	#define LCD_WRITE(X, Y)			{I2C_8Bit_md_write(X);udelay(Y);}
	#define LCD_STOP_WRITE			{StopACCESSBus();}

	#define LCD_WRITE_INST_COMPLETE(cmd, delay_time) {LCD_START_WRITE_INST;LCD_WRITE(cmd, delay_time);LCD_STOP_WRITE}
	#define LCD_WRITE_DATA_DELAYED(cmd, delay_time) {LCD_START_WRITE_DATA;LCD_WRITE(cmd, delay_time);LCD_STOP_WRITE}
#define MIN(X,Y) ((X)>(Y))?(Y):(X)


typedef struct {
  	char visible_size;
  	char ScrollPos;
 	unsigned char addr;//a valid ddram address should be provided as starting address
	unsigned char length;//the number of bytes to be read	
	char visible[16];//string to be displayed
	char buf[LCD_CHARS_PER_LINE];//string to be displayed
	int enable;
}scroll_struct;

scroll_struct m_scroll_struct;

typedef struct {
	uchar left;
	uchar top;
	uchar font;
	uchar blinking_rate;
	uchar state;
	uchar enabled;
	uchar reserved[2];
}cursor_struct;

typedef struct {
	uchar left;
	uchar top;
	uchar width;
 	uchar blinking_rate;
	uchar state;
	uchar enabled;
	uchar bitmaps;
	uchar reserved;
	uchar  bitmap[4][128];
}animation_struct;

const unsigned char l_mask_array[8] ={0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
uchar l_display_array[Y_BYTES][X_BYTES];
struct Cmd_State_struct Cmd_State;
cursor_struct m_cursor;
scroll_struct m_scroll_data;

unsigned char ddram_line_end_addr(unsigned char start_addr);
/******************************************************* 
	Description:  Checks the validity of a ddram addr 	
	Returns:	Returns the last valid ddram addr of the line where input addr lies in.	
 ********************************************************/
 unsigned char ddram_line_end_addr(unsigned char start_addr){
	unsigned char end_addr=0;
	
	if(Cmd_State.function_cmd & FUNCTION_2LINES){
		if((char)start_addr >=DDRAM_TWOLINES_LINE1_BEGIN  && start_addr <= DDRAM_TWOLINES_LINE1_END){
				end_addr=DDRAM_TWOLINES_LINE1_END;
		}
		else if(start_addr >=DDRAM_TWOLINES_LINE2_BEGIN  && start_addr <= DDRAM_TWOLINES_LINE2_END){
			end_addr=DDRAM_TWOLINES_LINE2_END;
		}
	}
	else{//device operating in 1 LINE mode
		if((char)start_addr >=DDRAM_ONELINE_BEGIN  && start_addr <= DDRAM_ONELINE_END){
			end_addr=DDRAM_ONELINE_END;
		}				
	}
	return end_addr;
 }
void init_ACCESS1(unsigned char freq, unsigned char bits)
{
	SetBits(CLK_GPIO5_REG, SW_AB1_EN, 1);												// enable access1 bus Clock
	SetBits(CLK_GPIO5_REG, SW_AB1_DIV, 16);	
  SetPort(P1_15_MODE_REG, PORT_OUTPUT, PID_SCL1); // SDA1 op P1_14 AND SCL1 on P1_15
  SetPort(P1_14_MODE_REG, PORT_OUTPUT, PID_SDA1);
  
  SetBits(ACCESS1_CTRL_REG,SDA_OD, 1);              /* SDA open drain */
  SetBits(ACCESS1_CTRL_REG,SCL_OD, 0);              /* SCL as push pull */
  SetBits(ACCESS1_CTRL_REG,SCK_NUM, bits);          /* 8 or 9 Bits mode */               
  SetBits(ACCESS1_CTRL_REG,SCK_SEL, freq);           /* Clock Frequenty */                  

  SetBits(ACCESS1_CTRL_REG,EN_ACCESS_INT,1);	  //enable interrupt for the access1 bus
  SetWord(ACCESS1_CLEAR_INT_REG, 1);     		/* Clear ACCESS1 interrupt in ctrl register*/
 
}
//*********************************************************************/
//                      Stop Access BUS																//
//********************************************************************//
void StopACCESSBus(void)
{ 
	 /* Stop Condition */
	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 0);

	//Disable Access Bus     
	SetBits(ACCESS1_CTRL_REG,SDA_VAL, 0);											// SDA1 = 0
	SetBits(ACCESS1_CTRL_REG,SCL_VAL, 0);											// SCL1 = 0	
	SetBits(ACCESS1_CTRL_REG,SDA_VAL, 0);											// SDA1 = 0
	SetBits(ACCESS1_CTRL_REG,SCL_VAL, 1);											// SCL1 = 1
	SetBits(ACCESS1_CTRL_REG,SDA_VAL, 1);											// SDA1 = 1
	SetBits(ACCESS1_CTRL_REG,SCL_VAL, 1);											// SCL1 = 1	
}

//*********************************************************************/
//                      Start Access BUS															//
//********************************************************************//
void StartACCESSBus(void)
{ 
	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 0);							/* ---  Enable ACCESS1 BUS Procedure  --- */
	
	
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,1);
	  
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);

	  
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	   
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);
	  
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	  
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,0);	

	  SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 1);							// ENABLE Access1 BUS

	// Clear any interrupt
	SetWord(ACCESS1_CLEAR_INT_REG, 0x1);
}


//******************************************************************//
//                      I2C_8Bit_md_write														//
//******************************************************************//
inline void I2C_8Bit_md_write(unsigned int wdata)
{
  SetWord(ACCESS1_IN_OUT_REG, wdata);												// Write byte
  while(GetBits(ACCESS1_CTRL_REG, ACCESS_INT)==0) 	{}
  SetWord(ACCESS1_CLEAR_INT_REG, 0x00a1);
}



#define I2C_Start StartACCESSBus
#define I2C_Stop StopACCESSBus
#define I2C_out I2C_8Bit_md_write
#define Slave SLAVE_ADDR
#define Datasend DATA_BYTES
#define Comsend CONTROL_BYTES

unsigned char logo_str[]={"     VTech     "};

void ShowLogo(unsigned char *text)
{
	int n, d;
	d=0x00;
	I2C_Start();
	I2C_out(Slave);
	I2C_out(Datasend);
	
	for(n=0;n<15;n++){
		I2C_out(*text);
		++text;
		}
	I2C_Stop();
}



void lcdInitHW(void)
{

	//Initialize access bus
	init_ACCESS1(SCK_100, BITS_9);//Initialize access bus
	
	//Set LCD reset low - high
	SetPort(P1_13_MODE_REG, PORT_OUTPUT, PID_port);
	SetWord(P1_RESET_DATA_REG,(1<<13));	
	udelay(50000);
	udelay(50000);
	udelay(50000);
	udelay(50000);
	SetWord(P1_SET_DATA_REG,(1<<13));
	udelay(50000);
	udelay(50000);
	udelay(50000);
	udelay(50000);
	//Turn LCD backlight on
	SetPort(P0_05_MODE_REG, PORT_OUTPUT, PID_port);
	SetWord(P0_SET_DATA_REG,(1<<5));

}

void lcdInit()
{

	lcdInitHW();
	
	/*Set the default values after boot*/
	Cmd_State.entry_mode_cmd=ENTRY_MODE_CMD|ENTRY_MODE_INCREMENT;
	Cmd_State.display_cmd=DISPLAY_CMD|DISPLAY_ON;
	Cmd_State.shift_cmd=SHIFT_CMD;
	Cmd_State.function_cmd=FUNCTION_CMD|FUNCTION_8BIT;//1 line mode, IS = 0	
	Cmd_State.contrast_cmd=CONTRAST_SET_CMD|0x00;

	/*Hw initialization sequence*/
	LCD_START_WRITE_INST;
	LCD_WRITE(Cmd_State.function_cmd, SHORT_TIME);//0x38

	//Function set (once again). 
	Cmd_State.function_cmd|=FUNCTION_IS;//0x39
	LCD_WRITE(Cmd_State.function_cmd, SHORT_TIME);	

	/*IS = 1 Commands begin*/
	LCD_WRITE(INTERNAL_OSC_FREQ_CMD, SHORT_TIME);//0x14
	LCD_WRITE(Cmd_State.contrast_cmd, SHORT_TIME);//0x78
	LCD_WRITE(PIC_SET_CMD|PIC_ICON_ON|PIC_BOOST_ON|(1<<1), SHORT_TIME);//0x5E
	LCD_WRITE(FOL_CTRL_CMD|FOL_SET_FOL_ON|(0x05), SHORT_TIME);//0x6D
	LCD_STOP_WRITE;
	
	udelay(50000);
	udelay(50000);
	udelay(50000);
	udelay(50000);
	udelay(50000);
	
	LCD_START_WRITE_INST;
	//Display on/off control
	LCD_WRITE(Cmd_State.display_cmd, SHORT_TIME);	//0x0C
	//Display clear
	LCD_WRITE(CLEAR_DISPLAY_CMD, LONG_TIME);	//0x01
	//Entry mode set
	LCD_WRITE(Cmd_State.entry_mode_cmd, LONG_TIME);//0x06
	//Function set (IS = 0). 
	Cmd_State.function_cmd &= ~FUNCTION_IS;
	LCD_WRITE(Cmd_State.function_cmd, SHORT_TIME);	
	LCD_STOP_WRITE;
	
	
	ShowLogo(logo_str);


}

 void lcd_text(void)
 {
	 struct LCD_ddram_write_struct *ddram_w;
 	unsigned char end_addr=0;
	int i, min;
 		  
	ddram_w=(struct LCD_ddram_write_struct*)(m_scroll_data.visible); 


	end_addr=ddram_line_end_addr(m_scroll_data.addr);
	if(!end_addr){
		return ;	
	}
	LCD_WRITE_INST_COMPLETE(DDRAM_ADDR_CMD| m_scroll_data.addr, SHORT_TIME);
	i=0;
	min=MIN(m_scroll_data.visible_size, (end_addr-m_scroll_data.addr+1));
	LCD_START_WRITE_DATA;
	for(i=0;i<min;i++){			
		LCD_WRITE(m_scroll_data.visible[i], SHORT_TIME);
	}	
	LCD_STOP_WRITE;

}


#endif //CONFIG_NT75451_LCD
