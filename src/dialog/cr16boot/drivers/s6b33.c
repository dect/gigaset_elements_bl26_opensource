/*
 * lcd_st7529.c -- lcd char driver
 *Based on the code from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.
 *
 *
 */

/***************       Include headers section       ***************/

#include "armboot.h"
#ifdef CONFIG_S6B33_LCD
#include "s6b33.h"
#include "fonts.h"

#if 1
unsigned char sitel_logo_glyph_table[] = {
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xE0,0xE0,0xE0,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0xC0,0xC0,0xE0,0xE0,0xE0,0xE0,0xE0,
		0xE0,0xC0,0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x60,0x60,0x60,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,
		0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x7E,0xFF,0xFF,0xC3,0x81,
		0x00,0x00,0x70,0x70,0x71,0xF3,0xF3,0xF1,0x00,0x00,0x70,0x70,0x70,0x70,0x70,0x00,
		0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0xFF,0x1C,0x1C,0x1C,0x1C,0x1C,0x1C,0x1C,0x00,
		0x00,0x00,0xFF,0xFF,0xFF,0x06,0x03,0x03,0x03,0x00,0xFF,0xFF,0xFF,0x00,0x00,0xF8,
		0xFE,0xFE,0x07,0x03,0x03,0x07,0x8E,0x04,0x00,0x00,0x1C,0x3E,0x3F,0x73,0x73,0x73,
		0xE7,0xE6,0xC4,0x00,0x00,0x1C,0x3E,0x3F,0x73,0x73,0x73,0xE7,0xE6,0xC4,0x00,0x00,
		0xF8,0xFE,0xFE,0x07,0x03,0x03,0x07,0xFE,0xFE,0xF8,0x00,0x00,0xFF,0xFF,0xFF,0x06,
		0x03,0x03,0x07,0xFF,0xFE,0xFC,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x07,0x07,0x07,0x07,0x07,0x07,0x07,0x07,0x07,0x07,0x00,0x00,0x00,
		0x01,0x03,0x03,0x07,0x07,0x07,0x07,0x07,0x07,0x03,0x03,0x01,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x07,0x07,0x07,0x07,0x07,0x07,0x07,
		0x07,0x07,0x07,0x07,0x00,0x00,0x07,0x07,0x07,0x00,0x00,0x00,0x00,0x00,0x07,0x07,
		0x07,0x00,0x00,0x00,0x03,0x03,0x07,0x06,0x06,0x07,0x03,0x01,0x00,0x00,0x01,0x03,
		0x07,0x06,0x06,0x06,0x07,0x03,0x01,0x00,0x00,0x01,0x03,0x07,0x06,0x06,0x06,0x07,
		0x03,0x01,0x00,0x00,0x00,0x03,0x03,0x07,0x06,0x06,0x07,0x03,0x03,0x00,0x00,0x00,
		0x07,0x07,0x07,0x00,0x00,0x00,0x00,0x07,0x07,0x07,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,0x80,0x00,0x00,0x80,0x80,0x80,0x80,
		0x80,0x80,0x80,0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x80,0x80,0x80,0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,0x80,0x80,
		0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,0x00,0x00,0x00,0x00,
		0x00,0x00,0x80,0x80,0x80,0x80,0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0xFF,0x00,0x00,
		0xFF,0xFF,0xFF,0xE3,0xE3,0xE3,0xE3,0xF7,0xFF,0x7F,0x3E,0x00,0x00,0xC0,0xC0,0xC0,
		0xC0,0xC0,0x00,0x0E,0x9F,0xFF,0xF1,0x61,0xF1,0xFF,0x9F,0x0E,0x00,0x00,0x0E,0x9F,
		0xFF,0xF1,0x61,0xF1,0xFF,0x9F,0x0E,0x00,0x00,0x00,0x1C,0x0E,0x06,0xFF,0xFF,0xFF,
		0x00,0x00,0x00,0x00,0x30,0x3E,0x3F,0x1F,0x19,0x39,0xF9,0xF1,0xE0,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x1F,
		0x1F,0x1F,0x00,0x00,0x1F,0x1F,0x1F,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x01,0x01,0x01,0x01,0x01,0x00,0x07,0x0F,0x1F,0x18,0x18,0x18,0x1F,0x0F,0x07,
		0x00,0x00,0x07,0x0F,0x1F,0x18,0x18,0x18,0x1F,0x0F,0x07,0x00,0x00,0x00,0x00,0x00,
		0x00,0x1F,0x1F,0x1F,0x00,0x00,0x00,0x00,0x06,0x0E,0x1E,0x1C,0x18,0x1C,0x1F,0x0F,
		0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};



#endif

unsigned char  logo_glyph_table[20000];


/***************       Definitions section             ***************/
#define BUFFER_SIZE 50 /*Buffer size to support the ioctl with the largest data transfer*/
#define MAX_IOCTL_ARGS ((SCRN_RIGHT+1)*(SCRN_BOTTOM+1)+10)/*Maximum number of args to pass with ioctl*/
#define MAX_SCROLL_TEXT_SIZE 4000

//#define BENCHMARK
/***      Data types definitions       ***/
#define uchar unsigned char
//Driver structs


typedef struct {
	char left;
	char top;
	char font;
	char visible_size;
	int  size ;
	char text[MAX_SCROLL_TEXT_SIZE];
	int ScrollPos;
	int enable;
}scroll_struct;
scroll_struct m_scroll_struct;
typedef struct {
	uchar left;
	uchar top;
	uchar font;
	uchar blinking_rate;
	uchar state;
	uchar enabled;
	uchar reserved[2];
}cursor_struct;

typedef struct {
	uchar left;
	uchar top;
	uchar width;
 	uchar blinking_rate;
	uchar state;
	uchar enabled;
	uchar bitmaps;
	uchar reserved;
	uchar  bitmap[4][128];
}animation_struct;

/***      Flags definitions     ***/
/***      Hw specific definitions       ***/

/***   Macros   ***/

/***      Function Prototypes       ***/
/** Low level **/

/* LCD function prototype list */
void lcd_init(void);
void lcd_print_screen(unsigned char *screen);
void lcd_clear_ram(void);
void lcd_clear_rect(uchar left,  uchar top, uchar right, uchar bottom);
void lcd_invert_rect(uchar left, uchar top, uchar right, uchar bottom);
void lcd_horz_line(uchar left, uchar right, uchar row);
void lcd_vert_line(uchar top, uchar bottom, uchar column);
void lcd_clr_horz_line(uchar left, uchar right, uchar row);
void lcd_clr_vert_line(uchar top, uchar bottom, uchar column);
void lcd_draw_border(uchar left, uchar top, uchar right, uchar bottom);
void lcd_clear_border(uchar left, uchar top, uchar right, uchar bottom);
void lcd_glyph(uchar left, uchar top, uchar width, uchar height, uchar *glyph, uchar store_width);
void lcd_animated_glyph(uchar left, uchar top, uchar width, uchar num_of_bitmaps, uchar *glyph1, uchar *glyph2, uchar *glyph3);
void lcd_text(uchar left, uchar top, uchar font, char *str);
void lcd_update(uchar top, uchar bottom);
void lcd_cursor_set(uchar left, uchar top, uchar font, uchar ShowCursor, uchar blinking_rate);
/**/
void lcd_contrast_level(uchar level);
void lcd_contrast_up(void);
void lcd_contrast_down(void);
void lcd_sleepmode_enter(void);
void lcd_sleepmode_exit(void);
void lcd_scroll_init(char *buf);
 uchar lcd_font_width_get(uchar font_width);
/*Static functions*/

//Timers

static void lcd_timer_refresh_fn(unsigned long param);
static void scroll_timer_refresh(unsigned long param);
static void cursor_timer_refresh(unsigned long param);
static void animation_timer_refresh(unsigned long param);

//Module specific

void  LCD_cleanup_module(void);



/*** Parameters which can be set at load time  ***/

void lcd_update32 (uchar top, uchar heigth, uchar left, uchar width);




const unsigned char l_mask_array[8] ={0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
uchar l_display_array[Y_BYTES][X_BYTES];


cursor_struct m_cursor;

#define MAX_ANIMATED_BITMAPS 8
animation_struct m_animation[MAX_ANIMATED_BITMAPS];


/***************          Main body             ***************/
unsigned char scr_top_border, scr_bottom_border, scr_left_border, scr_right_border, scr_left_dim, scr_right_dim;
unsigned char scr_left_offset, scr_right_offset;
/****************************************************************************************************/
/************                                        LOW LEVEL DRIVER FUNTIONS                                                            *********/
/****************************************************************************************************/

/*******************************************************
	Description: 	Initializes hw and driver internal state
	Note:
 ********************************************************/
#if 1
		#define set_col2(X) \
	({\
			register short __r4 __asm__ ("r4") = (short)(X) ; \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"nop \n\t"\
    			"movd $0x1F:m,(r1,r0)\n\t"\
    			"1: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 1b\n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"nop \n\t"\
    			"nop \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1090000:l,(r1,r0) \n\t"\
    			"storw	r4,0x0:(r1,r0) \n\t"\
    			"nop \n\t"\
    			"movd $0x1F:m,(r1,r0)\n\t"\
    			"2: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 2b\n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
    			"nop \n\t"\
    			"nop \n\t"\
			    :"=r" (__r4) \
			    : "0" (__r4) \
			    : "r0","r1","r2","r3"); \
			 \
		})
#endif
		#define RESET_LCD {set_col2((0xFF)|((0)<<8)|(0<<13));}
		#define SET_LCD {set_col2((0xFF)|0x4000|((0)<<8)|(0<<13));}

void lcdInit (){
//void lcd_init(){

	unsigned char i;
	unsigned int j;
	/*Processor specific commands*/
	m_cursor.enabled=0;
	RESET_LCD;
	udelay(1000);
	SET_LCD;
	udelay(50000);			//required for busy time
	lcd_out_ctl (0x50); // OFF
	lcd_out_ctl (0x2C); //Standby OFF
	udelay(500000);
//----------------------------------------------------
	lcd_out_ctl (0x02);
	lcd_out_ctl (0x01);	//OSC ON
//----------------------------------------------------
	udelay(50000);
	lcd_out_ctl (0x26);
	lcd_out_ctl (0x01);	//DC/DC1 ON
	udelay(50000);
	lcd_out_ctl (0x26);
	lcd_out_ctl (0x09);	//AMP ON/OFF
	udelay(50000);
	lcd_out_ctl (0x26);
	lcd_out_ctl (0x0B);	//DC/DC2 ON
	udelay(50000);
	lcd_out_ctl (0x26);
	lcd_out_ctl (0x0F);	//DC/DC3 ON
	udelay(50000);

//----------------------------------------------------
	lcd_out_ctl (0x10);
	lcd_out_ctl (0x1c);	//DLN=11,SDIR=0,SWP=0
//----------------------------------------------------
	lcd_out_ctl (0x20);
	lcd_out_ctl (0x01);	//DC-DC2 x1, DC-DC1 x1
//----------------------------------------------------
	lcd_out_ctl (0x24);
	lcd_out_ctl (0x08);	//Clock Division /16,/16
//----------------------------------------------------
	lcd_out_ctl (0x28);
	lcd_out_ctl (0x01);	//Clock Division /16,/16
//----------------------------------------------------

#ifdef L_V3_BOARD
	lcd_out_ctl (0x2A);
	lcd_out_ctl (0x99);	//Contrast Control
#else
	lcd_out_ctl (0x2A);
	lcd_out_ctl (0xA9);	//Contrast Control
#endif
//----------------------------------------------------
	lcd_out_ctl (0x30);
	lcd_out_ctl (0x0a);	//Adressing Mode
//----------------------------------------------------
	lcd_out_ctl (0x32);
	lcd_out_ctl (0x0e);	//Adressing Mode
//----------------------------------------------------
	lcd_out_ctl (0x34);
	lcd_out_ctl (0x90);	//Adressing Mode
//----------------------------------------------------
	lcd_out_ctl (0x36);
	lcd_out_ctl (0x01);	//Adressing Mode
//----------------------------------------------------
	lcd_out_ctl (0x40);
	lcd_out_ctl (0x80);	//Adressing Mode
//----------------------------------------------------
	lcd_out_ctl (0x42);
	lcd_out_ctl (DDRAM_X_OFFSET);	//X AREA SET
	lcd_out_ctl (95);	//X AREA SET
//----------------------------------------------------
	lcd_out_ctl (0x43);
	lcd_out_ctl (DDRAM_Y_OFFSET);	//Y AREA SET
	lcd_out_ctl (95);	//Y AREA SET
//----------------------------------------------------
	lcd_out_ctl (0x45);
	lcd_out_ctl (0x0);
	lcd_out_ctl (0x53);
	lcd_out_ctl (0x0);
	lcd_out_ctl (0x55);
	lcd_out_ctl (0x0);

//----------------------------------------------------
	udelay(50000);
//----------------------------------------------------
	udelay(50000);
	lcd_out_ctl (0x51); //Display ON
//----------------------------------------------------

//----------------------------------------------------
	lcd_out_ctl (0x42);
	lcd_out_ctl (16);	//X AREA SET
	lcd_out_ctl (95);	//X AREA SET
//----------------------------------------------------
	lcd_out_ctl (0x43);
	lcd_out_ctl (46);	//Y AREA SET
	lcd_out_ctl (95);	//Y AREA SET

//================================
	lcd_clear_ram();

	lcd_print_screen(sitel_logo_glyph_table);

 }



void lcd_init_col_lines(unsigned char screen_top, unsigned char screen_bottom, \
						unsigned char screen_left, unsigned char screen_right){

	unsigned char tmp;

	//Setup the line\column borders

	if( (screen_bottom<=screen_top) || (screen_right<=screen_left))
		return;

	//Check if dimensions not alligned to 2pixels or 3 pixels
	scr_top_border =  DDRAM_X_OFFSET+screen_top;
	scr_bottom_border = DDRAM_X_OFFSET+screen_bottom;
	scr_left_border = screen_left;
	scr_right_border = screen_right;

	scr_left_dim =  DDRAM_Y_OFFSET+screen_left/3;
	scr_left_offset =  screen_left%3;
	if(screen_right%3){
		scr_right_dim =  DDRAM_Y_OFFSET+screen_right/3;
		scr_right_offset =  3-screen_right%3;
	}
	else{
		scr_right_dim =  DDRAM_Y_OFFSET+screen_right/3;
		scr_right_offset =  0;
	}

	lcd_out_ctl( 0x0042 ); //Line Address Set
	lcd_out_ctl( scr_top_border ); //Start Line
	lcd_out_ctl( scr_bottom_border); //End Line
	lcd_out_ctl( 0x0043 ); //Column Address Set
	lcd_out_ctl( scr_left_dim ); //Start Column=0
	lcd_out_ctl( scr_right_dim); //End Column =84 //columns represent 16-bit words = 3 pixels

}




 /*******************************************************
	Description:	Erases lcd RAM
	Note:
 ********************************************************/
void lcd_clear_ram(void)
{

	uchar p, i, j;

	lcd_init_col_lines(SCRN_TOP, SCRN_BOTTOM, SCRN_LEFT, SCRN_RIGHT );

	for(p=0; p<=SCRN_BOTTOM; p++)
	{
		l_display_array[p][0]=0; /*Clear buffer*/
		lcd_out_2b3_dat(0,0,0);
		for(i=1; i<SCRN_RIGHT; i+=3)
		{
			l_display_array[p][i]=0; /*Clear buffer*/
			l_display_array[p][i+1]=0; /*Clear buffer*/
			l_display_array[p][i+2]=0; /*Clear buffer*/
			lcd_out_2b3_dat(0,0,0);
		}
	}
}

/*
**
** 	Clears the display memory starting at the left/top  and going to
**  the right/bottom . No runtime error checking is performed. It is
**  assumed that left is less than right and that top is less than
**  bottom
**
*/

void lcd_clear_rect(uchar left,  uchar top,
			        uchar right, uchar bottom)
{

		unsigned short i, j, k=0, bulkstart;
		unsigned short xstart,xend,yend,ystart;
		volatile unsigned char lcddata[3];
		unsigned int alwidth;
		unsigned int dj;
		unsigned int width, height;

		width=right-left;
		height=bottom-top;

		if((!width) || (!height))
			return;

			for(i=0; i<height; i++)
			{
				memset (&(l_display_array[top+i][left]),0,width);
			}
			lcd_update32 (top, height, left, width);
}

/*
**
** Inverts the display memory starting at the left/top and going to
** the right/bottom. No runtime error checking is performed. It is
** assumed that left is less than right and that top is less than
** bottom
**
*/

void lcd_invert_rect(uchar left,  uchar top,
			         uchar right, uchar bottom)
{

	unsigned short i, j, k=0, bulkstart;
	unsigned short xstart,xend,yend,ystart;
	volatile unsigned char lcddata[3];
	unsigned int alwidth;
	unsigned int dj;
	unsigned int width, height;

	width=right-left;
	height=bottom-top;
	//printf ("%d,%d,%d,%d \n",left,right,bottom,top);
	return;

	if((!width) || (!height))
		return;

		for(i=0; i<height; i++)
		{
			for(j=left; j<right; j++)
			{
				l_display_array[top+i][j]=l_display_array[top+i][j]^0xF8;

			}
		}

		lcd_update32 (top, height, left, width);
}

/*
**
** Draws a line into the display memory starting at left going to
** right, on the given row. No runtime error checking is performed.
** It is assumed that left is less than right.
**
*/

void lcd_horz_line(uchar left, uchar right, uchar row)
{

	uchar bit_pos;
	uchar byte_offset;
	uchar mask;
	uchar col;


	return;
}

/*
**
** Draws a vertical line into display memory starting at the top
** going to the bottom in the given column. No runtime error checking
** is performed. It is assumed that top is less than bottom and that
** the column is in range.
**
*/

void lcd_vert_line(uchar top, uchar bottom, uchar column)
{
	return;

}

/*
**
** Clears a line into the display memory starting at left going to
** right, on the given row. No runtime error checking is performed.
** It is assumed that left is less than right.
**
*/

void lcd_clr_horz_line(uchar left, uchar right,
		               uchar row)
{
	return;

}


/*
**
** Clears a vertical line into display memory starting at the top
** going to the bottom in the given column. No runtime error checking
** is performed. It is assumed that top is less than bottom and that
** the column is in range.
**
*/

void lcd_clr_vert_line(uchar top, uchar bottom, uchar column)
{
	return;

}

/*
**
** 	Draws a box in display memory starting at the left/top and going
**  to the right/bottom. No runtime error checking is performed.
**  It is assumed that left is less than right and that top is less
**  than bottom.
**
*/

void lcd_draw_border(uchar left, uchar top, uchar right, uchar bottom)
{

  	/* to draw a box requires two vertical lines */
   	lcd_vert_line(top,bottom,left);
   	lcd_vert_line(top,bottom,right);
  	/* and two horizonal lines */
   	lcd_horz_line(left,right,top);
   	lcd_horz_line(left,right,bottom);
return ;
	lcd_vert_line(top+1,top+1,left+1);	//+1
	lcd_vert_line(top+2,bottom-2,left);
	lcd_vert_line(bottom-1, bottom-1,left+1); //-1

	lcd_vert_line(top+1,top+1,right-1);//+1
	lcd_vert_line(top+2,bottom-2,right);
	lcd_vert_line(bottom-1,bottom-1,right-1);//-1

	lcd_horz_line(left+2,right-2,top);
	lcd_horz_line(left+2,right-2,bottom);


}

/*
**
** Clears a box in display memory starting at the Top left and going
** to the bottom right. No runtime error checking is performed and
** it is assumed that Left is less than Right and that Top is less
** than Bottom.
**
*/

void lcd_clear_border(uchar left, uchar top, uchar right, uchar bottom)
{
	return;

}

/*
**
** Writes a glyph to the display at location x,y
**
** Arguments are:
**    column     - x corrdinate of the left part of glyph
**    row        - y coordinate of the top part of glyph
**    width  	 - size in pixels of the width of the glyph
**    height 	 - size in pixels of the height of the glyph
**    glyph      - an uchar pointer to the glyph pixels
**                 to write assumed to be of length "width"
**
*/

void lcd_glyph(uchar left, uchar top,
			   uchar width, uchar height,
			   uchar *glyph, uchar store_width)
{


		uchar byte_offset;
		uchar y_bits;
		uchar remaining_bits;
		uchar mask;
		uchar char_mask;
		uchar x,y;
		uchar *glyph_scan;
		uchar glyph_offset;

		glyph_offset = 0;			/* start at left side of the glyph rasters */
	    char_mask = 0x80;			/* initial character glyph mask */


	    //printk("Glyphing %d %d %d %d %d \r\n",left,top,width,height,*(glyph + glyph_offset));

	  	for (x = left; x < (left + width); x++)
	  	{
	    	byte_offset = top;        		/* get the byte offset into y direction */
			y_bits = height;				/* get length in y direction to write */
			glyph_scan = glyph + glyph_offset;	 /* point to base of the glyph */

	    	/* boundary checking here to account for the possibility of  */
	    	/* write past the bottom of the screen.                        */
	    	while((y_bits) && (byte_offset < Y_BYTES)) /* while there are bits still to write */
	    	{
				/* check if the character pixel is set or not */
				if(*glyph_scan & char_mask)
				{
					l_display_array[byte_offset][x] = 0xFF;	/* set image pixel */
					//printf ("*");
				}
				else
				{
	      			l_display_array[byte_offset][x] = 0;	/* clear the image pixel */
	      			//printf (" ");
				}

				y_bits--;
				byte_offset++;

				/* bump the glyph scan to next raster */
				glyph_scan += store_width;
			}
	    	//printf ("\n");
			/* shift over to next glyph bit */
			char_mask >>= 1;
			if(char_mask == 0)				/* reset for next byte in raster */
			{
				char_mask = 0x80;
				glyph_offset++;
		    }
		}
}

/*
**
** Writes a graphic to the display at location x,y
**
** Arguments are:
**    column     - x corrdinate of the left part of glyph
**    row        - y coordinate of the top part of glyph
**    width  	 - size in pixels of the width of the glyph
**    height 	 - size in pixels of the height of the glyph
**    pixelmode  - 0: 3B3P mode, 1: 2B3P mode
**
*/

void lcd_print_screen(unsigned char *screen)
{
	unsigned int x,y;
	uchar m_font = 0;

	//lcd_text(0,0,m_font,"BootC Started\0");
	//lcd_text(0,10,m_font,"Enter Password\0");
	for (y=SCRN_TOP;y<SCRN_BOTTOM-SCRN_TOP+1;y++)
		for (x=SCRN_LEFT;x<SCRN_RIGHT-SCRN_LEFT+1;x++)
			l_display_array[y][x]=((screen[((y>>3)*(X_BYTES))+(x)]&(1<<(y%8)))!=0?0xFF:0);

	lcd_update(1,64);
	//lcd_update32(SCRN_LEFT, SCRN_TOP,  SCRN_RIGHT-SCRN_LEFT+1, SCRN_BOTTOM-SCRN_TOP+1);
}

#ifndef SINGLETRIPLET
//char line23 [96*3];
unsigned int lcd_out_2b3_linedat(unsigned short int bulkstart,unsigned short int alwidth, long line23)
{
	volatile register short __r4 __asm__ ("r4") = (short)(bulkstart);
	volatile register short __r5 __asm__ ("r5") = (short)(alwidth);
	volatile register long __r10  __asm__ ("r10") = (long)(line23);

	asm volatile(\
					"sprd	psr,(r3,r2) \n\t"\
					"push $2,r2 \n\t"\
					"di \n\t"\
					"push $2,r12 \n\t"\
					"movd	$0x9:m,(r1,r0) \n\t"\
					"stord	(r1,r0),0xff0064 \n\t"\
					"movd	$0x1080080:l,(r12) \n\t"\
					"movd	$0x00F800E0:m,(r1,r0) \n\t"\
					"movd	$0x3,(r3,r2) \n\t"\
					"1: \n\t"\
					"loadd	 0x0:(r11,r10),(r9,r8)   \n\t"\
					"movzb 	 r8,r7 \n\t"\
					"/* Data are in r7, (r8 shifted <<8) ,r9 we need to do the 3 to 2 */ \n\t"\
					"/* ((X&0xF8)|((Y>>5)&0x7)) */ \n\t"\
					"/* (((Y<<3)&0xE0)|((Z>>3)&0x1F)) */ \n\t"\
					"/*increase r4 and r11,r10 by 3   */ \n\t"\
					"movw r8,r6 \n\t"\
					"lshw $-13,r6 \n\t"\
					"andw r1,r7 \n\t"\
					"orb r6,r7 \n\t"\
					"movw $0x9:m,r6 \n\t"\
					"storw	r6,0xff0064 \n\t"\
					"storw	r7,0x0:(r12) \n\t"\
					"ashuw $-5,r8 \n\t"\
					"andw r0,r8 \n\t"\
					"andw r1,r9 \n\t"\
					"lshw $-3,r9 \n\t"\
					"orb r8,r9 \n\t"\
					/*Required for correct LCD writes*/
					"storw	r6,0xff0064 \n\t"\
					"storw	r9,0x0:(r12) \n\t"\
					"/*increase r4 and r11,r10 by 3  */ \n\t"\
					"addd (r3,r2),(r11,r10) \n\t"\
					"addw r2,r4 \n\t"\
					"cmpw r5,r4  \n\t"\
					"bhi 1b \n\t"\
					"movd	$0xa,(r1,r0) \n\t"\
					"stord	(r1,r0),0xff0064 \n\t"\
					"xorw r5,r5 \n\t"\
					"pop $2,r12 \n\t"\
					"pop $2,r2 \n\t"\
					"lprd	(r3,r2), psr \n\t"\
				:"=r" (__r4),"=r" (__r5),"=r" (__r10)\
				: "0" (__r4),"1" (__r5),"2" (__r10)\
				: "r0","r1","r2","r3","r6","r7","r8","r9");
	return (__r4-bulkstart);

}

#endif
const unsigned char bulkstartarray[]={1,2,0};

void lcd_graphic32(uchar left, uchar top,
			   uchar width, uchar height,
			   uchar *buf)
{

	unsigned short i, j, k=0, bulkstart;
	unsigned short xstart,xend,yend,ystart;
	unsigned char lcddata[3];
	unsigned int alwidth;
	unsigned short int byteswritten;
	unsigned int dj;

	if((!width) || (!height))
	return;

	xstart=(left+X_OFFSET)-((left+X_OFFSET)%3);
	xend=left+width-1;

	xend=(xend+X_OFFSET)-((xend+X_OFFSET)%3);

	yend=top+height;
	ystart=top;


	lcd_init_col_lines(ystart, yend, xstart, xend  );

	bulkstart=bulkstartarray[xstart-left];
	alwidth=((width-bulkstart)/3)*3;

#ifdef BENCHMARK
		setstarttime();
#endif
		for(i=0; i<height; i++)
		{
			j=0;

			if (((xstart-X_OFFSET)!=left)||(xstart==0))
			{
				if (xstart==0)
				{
					lcddata[2]=l_display_array[ystart+i][xstart-X_OFFSET+2];
					l_display_array[ystart+i][xstart-X_OFFSET+2+j]=lcddata[2]=buf[k];
					k++;
				}
				else
				{
					lcddata[0]=l_display_array[ystart+i][xstart-X_OFFSET];
					lcddata[1]=l_display_array[ystart+i][xstart-X_OFFSET+1];
					lcddata[2]=l_display_array[ystart+i][xstart-X_OFFSET+2];
					for (j=0;j<3;j++)
					{
						if ((xstart-X_OFFSET+j)>=left)
						{
							l_display_array[ystart+i][xstart-X_OFFSET+j]=lcddata[j]=buf[k];
							k++;
						}
					}
				}
				lcd_init_col_lines(ystart+i, yend, xstart, xend   );
				lcd_out_2b3_dat(lcddata[0],lcddata[1],lcddata[2]);
			}
#if 1

			//memcpy(line23,&buf[k],alwidth);
			memcpy(&(l_display_array[ystart+i][left+bulkstart]),&buf[k],width-bulkstart);
			byteswritten=lcd_out_2b3_linedat(bulkstart,alwidth,(long)&(l_display_array[ystart+i][left+bulkstart]));
			k+=byteswritten;
			j=left+bulkstart+alwidth;
#else
			alwidth=((width-bulkstart)/3)*3;
			memcpy(&(l_display_array[ystart+i][left+bulkstart]),&buf[k],width-bulkstart);

			for(j=left+bulkstart; j<left+bulkstart+alwidth; j+=3)
			{
				lcd_out_2b3_dat(buf[k],buf[k+1],buf[k+2]);
				k+=3;
			}
#endif
			if (j<xend)
			{
				//printk ("j %d, left+width %d, xend %d \n",j, (left+width), xend );
				lcddata[0]=l_display_array[ystart+i][j];
				lcddata[1]=l_display_array[ystart+i][j+1];
				lcddata[2]=l_display_array[ystart+i][j+2];

				dj=0;
				for (;j<left+width;j++)
				{
					l_display_array[ystart+i][j]=lcddata[dj]=buf[k];
					k++;
					dj++;
				}
				lcd_init_col_lines(ystart+i, yend, j, xend   );
				lcd_out_2b3_dat(lcddata[0],lcddata[1],lcddata[2]);

				lcd_init_col_lines(ystart+i+1, yend, xstart, xend   );
			}



		}

		lcd_init_col_lines(SCRN_TOP, SCRN_BOTTOM, SCRN_LEFT, SCRN_RIGHT  );
}



/*
**
**	Prints the given string at location x,y in the specified font.
**  Prints each character given via calls to lcd_glyph. The entry string
**  is null terminated and non 0x20->0x7e characters are ignored.
**
**  Arguments are:
**      left       coordinate of left start of string.
**      top        coordinate of top of string.
**      font       font number to use for display
**      str   	   text string to display
**
*/

void lcd_text(uchar left, uchar top, uchar font, char *str)
{

  	uchar x = left;
  	uchar glyph;
  	uchar width;
	uchar height;
	uchar store_width;
	uchar  *glyph_ptr;

	 //printf("left %d top %d font %d str  %s\n", left,top,font,str);
if (str==NULL) {printf("INVALID STRING ....");}

  	while(*str != 0x00)
  	{
    	glyph = (uchar)*str;

		/* check to make sure the symbol is a legal one */
		/* if not then just replace it with the default character */
		if((glyph < fonts[font].glyph_beg) || (glyph > fonts[font].glyph_end))
		{
			glyph = fonts[font].glyph_def;
		}

    	/* make zero based index into the font data arrays */
    	glyph -= fonts[font].glyph_beg;
    	width = fonts[font].fixed_width;	/* check if it is a fixed width */
		if(width == 0)
		{
			width=fonts[font].width_table[glyph];	/* get the variable width instead */
		}

		height = fonts[font].glyph_height;
		store_width = fonts[font].store_width;

		glyph_ptr = fonts[font].glyph_table + ((unsigned int)glyph * (unsigned int)store_width * (unsigned int)height);

		/* range check / limit things here */
		if(x > SCRN_RIGHT)
		{
			x = SCRN_RIGHT;
		}
		if((x + width) > SCRN_RIGHT+1)
		{
			width = SCRN_RIGHT - x + 1;
		}
		if(top > SCRN_BOTTOM)
		{
			top = SCRN_BOTTOM;
		}
		if((top + height) > SCRN_BOTTOM+1)
		{
			height = SCRN_BOTTOM - top + 1;
		}

		lcd_glyph(x,top,width,height,glyph_ptr,store_width);  /* plug symbol into buffer */

		x += width;							/* move right for next character */
		str++;								/* point to next character in string */
	}
}


/*
**
** Updates area of the display. Writes data from display
** RAM to the lcd display controller.
**
** Arguments Used:
**    top     top line of area to update.
**    bottom  bottom line of area to update.
**
*/

int protection_flag = 0;
void lcd_update(uchar top, uchar bottom)
{
	uchar x, y;
	uchar *colptr;

	if (protection_flag) return;
	protection_flag = 1;

		lcd_update32(top, bottom-top+1, SCRN_LEFT, SCRN_RIGHT-SCRN_LEFT+1);

	protection_flag=0;
}

void lcd_update32 (uchar top, uchar height, uchar left, uchar width)
{
		unsigned short i, j, k=0, bulkstart;
		unsigned short xstart,xend,yend,ystart;
		unsigned char lcddata[3];
		unsigned int alwidth;
		unsigned short int byteswritten;
		unsigned int dj;

		if((!width) || (!height))
		return;

		xstart=(left+X_OFFSET)-((left+X_OFFSET)%3);
		xend=left+width-1;

		xend=(xend+X_OFFSET)-((xend+X_OFFSET)%3);

		yend=top+height;
		ystart=top;


		lcd_init_col_lines(ystart, yend, xstart, xend  );

		bulkstart=bulkstartarray[xstart-left];
		alwidth=((width-bulkstart)/3)*3;

			for(i=0; i<height; i++)
			{
				j=0;

				if (((xstart-X_OFFSET)!=left)||(xstart==0))
				{
					//printf ("xstart %d, left %d \n",xstart, left);
					if (xstart==0)
					{
						lcddata[2]=l_display_array[ystart+i][xstart-X_OFFSET+2];
						l_display_array[ystart+i][xstart-X_OFFSET+2+j]=lcddata[2];
						k++;
					}
					else
					{
						lcddata[0]=l_display_array[ystart+i][xstart-X_OFFSET];
						lcddata[1]=l_display_array[ystart+i][xstart-X_OFFSET+1];
						lcddata[2]=l_display_array[ystart+i][xstart-X_OFFSET+2];
					}
					lcd_init_col_lines(ystart+i, yend, xstart, xend   );
					lcd_out_2b3_dat(lcddata[0],lcddata[1],lcddata[2]);
				}
				if (alwidth>2)
				{
					//printf ("bulkstart %d, alwidth %d \n",bulkstart, alwidth);
					byteswritten=lcd_out_2b3_linedat(bulkstart,alwidth,(long)&(l_display_array[ystart+i][left+bulkstart]));
					j=left+bulkstart+alwidth;
				}
				else
					j=left+bulkstart;
				if (j<xend)
				{
					//printk ("j %d, left+width %d, xend %d \n",j, (left+width), xend );
					lcddata[0]=l_display_array[ystart+i][j];
					lcddata[1]=l_display_array[ystart+i][j+1];
					lcddata[2]=l_display_array[ystart+i][j+2];

					lcd_init_col_lines(ystart+i, yend, j, xend   );
					lcd_out_2b3_dat(lcddata[0],lcddata[1],lcddata[2]);

					lcd_init_col_lines(ystart+i+1, yend, xstart, xend   );
				}



			}

			lcd_init_col_lines(SCRN_TOP, SCRN_BOTTOM, SCRN_LEFT, SCRN_RIGHT  );
}
void lcd_cursor_set(uchar left, uchar top, uchar font, uchar ShowCursor, uchar blinking_rate)
{

	uchar width;

 	width=fonts[font].fixed_width;
	if (width==0) width=8;
	if (m_cursor.enabled)
	{
		m_cursor.enabled=0;
   		if(m_cursor.state)
		{
//			printk("Invert cursor position [%d] [%d] \n", m_cursor.left, width);
			  lcd_invert_rect(m_cursor.left, m_cursor.top, m_cursor.left + width - 1, m_cursor.top + fonts[m_cursor.font].glyph_height - 1);
			m_cursor.state = 0;
  		}//else printk("Cursor is in cleared state \n");
	}//else printk("new text ... \n");
 	if(ShowCursor == 0)	return;
 	m_cursor.left = left;
	m_cursor.top = top;
	m_cursor.font = font;
	m_cursor.blinking_rate = blinking_rate;
  	lcd_invert_rect(m_cursor.left, m_cursor.top, m_cursor.left + width - 1, m_cursor.top + fonts[m_cursor.font].glyph_height - 1);

	m_cursor.state = 1;
	m_cursor.enabled = 1;
}


void lcd_scroll_init(char *buf)
{
	return;
	char visible_buffer[50];
	char scroll_enable = buf[4];


	if ((scroll_enable == 0 )|| ( buf[3] >strlen(&buf[5])))
	{
  		memset(visible_buffer, 0 , 50);
		strncpy(visible_buffer, &buf[5], buf[3]);
 		lcd_text(buf[0], buf[1], buf[2], visible_buffer);
  		return;
	}


//	if(scroll_enable == -1)
//	{
 //		STOP_TIMER(scroll_timer);
//		return;
//	}
 	if( buf[3] <strlen(&buf[5]))
	{

		m_scroll_struct.left = buf[0];
		m_scroll_struct.top = buf[1];
		m_scroll_struct.font = buf[2];
		m_scroll_struct.visible_size = buf[3];
		strcpy(m_scroll_struct.text, &buf[5]);
		m_scroll_struct.size = strlen(m_scroll_struct.text);
		m_scroll_struct.ScrollPos = 0;

		memset(visible_buffer, 0 , 50);
		strncpy(visible_buffer, m_scroll_struct.text, m_scroll_struct.visible_size);
		lcd_text(m_scroll_struct.left, m_scroll_struct.top, m_scroll_struct.font, visible_buffer);


		//if(m_scroll_struct.visible_size < m_scroll_struct.size)
		{
			// add a space at the end of the text
			//strcat(m_scroll_struct.text, " ");
			//m_scroll_struct.size++;
			m_scroll_struct.enable =1;



		}
	}else{
  		memset(visible_buffer, 0 , 50);
		strncpy(visible_buffer, &buf[5], buf[3]);
 		lcd_text(buf[0], buf[1], buf[2], visible_buffer);

	}

}
uchar lcd_font_width_get(uchar font_width)
{

 	return fonts[font_width].fixed_width;
}


/*******************************************************
	Description: 	Sets the lcd contrast
	Input:		level: Brightness level (5-410). We map 0-255 to 0-511 by doubling the input level.
 ********************************************************/
void lcd_contrast_level(uchar level){
unsigned short level_16 = 	level << 1;
	lcd_out_ctl (0x2A);
	lcd_out_ctl (level);	//Contrast Control
}
/*******************************************************
	Description:
	Input:
 ********************************************************/


#endif

