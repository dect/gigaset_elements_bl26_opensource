
#ifndef NT7538_H
#define NT7538_H

#define LCD_CTRL_ADDR CONFIG_LCD_BASE 		//0x1300000
#define LCD_DATA_ADDR CONFIG_LCD_BASE+0x800	//0x1300080

#define lcd_ctrl_write(x) *(volatile unsigned short*)(LCD_CTRL_ADDR)=x
#define lcd_ctrl_read	*(volatile unsigned short*)(LCD_CTRL_ADDR)

#define lcd_data_write(x) *(volatile unsigned short*)(LCD_DATA_ADDR)=x
#define lcd_data_read	*(volatile unsigned short*)(LCD_DATA_ADDR)


#define G_LCD_DISPLAY_ON		lcd_ctrl_write(0xaf)
#define G_LCD_DISPLAY_OFF		lcd_ctrl_write(0xae)
#define G_LCD_ADC_SEL_NORMAL	lcd_ctrl_write(0xa0)
#define G_LCD_ADC_SEL_REV		lcd_ctrl_write(0xa1)
#define G_LCD_DISPLAY_NORM		lcd_ctrl_write(0xa6)
#define G_LCD_DISPLAY_REV		lcd_ctrl_write(0xa7)
#define G_LCD_ENT_DISPLAY_ON	lcd_ctrl_write(0xa4)
#define G_LCD_ENT_DISPLAY_OFF	lcd_ctrl_write(0xa5)
#define G_LCD_SET_BIAS_19		lcd_ctrl_write(0xa2)
#define G_LCD_SET_BIAS_17		lcd_ctrl_write(0xa3)
#define G_LCD_RESET				lcd_ctrl_write(0xe2)
#define G_LCD_SET_RMW			lcd_ctrl_write(0xe0)
#define G_LCD_RESET_RMW			lcd_ctrl_write(0xee)
#define G_LCD_SHL_NORMAL		lcd_ctrl_write(0xc0)
#define G_LCD_SHL_FLIPPED		lcd_ctrl_write(0xc8)
#define G_LCD_VOLUME_MODE_SET	lcd_ctrl_write(0x81)
#define G_LCD_NOP				lcd_ctrl_write(0xe3)
#define G_LCD_OSC_ON			lcd_ctrl_write(0xab)
#define G_LCD_RST_REVDRV		lcd_ctrl_write(0xe4)
#define G_LCD_PWR_SAVE_RESET	lcd_ctrl_write(0xe1)
#define G_LCD_SET_LINE			0x40
#define G_LCD_SET_PAGE			0xb0
#define G_LCD_SET_COL_LOW		0x00
#define G_LCD_SET_COL_HIGH		0x10
#define G_LCD_POWER_CTRL_SET	0x28
#define G_LCD_POWER_VC_SET		0x04
#define G_LCD_POWER_VR_SET		0x02
#define G_LCD_POWER_VF_SET		0x01
#define G_LCD_REG_RATIO			0x20

// initializes I/O pins connected to LCD
void lcdInitHW(void);
// initializes the LCD display (gets it ready for use)
void lcdInit(void);

// moves the cursor/position to Home (upper left corner)
void lcdHome(void);

// clears the LCD display
void lcdClearScreen(void);

// moves the cursor/position to the row,col requested
// ** this may not be accurate for all displays
void lcdGotoXY(unsigned short row, unsigned short col);

// prints a series of bytes/characters to the display
//void lcdPrintData(char* data, unsigned char nBytes,FONT_DEF *fontused);

void lcdCLS(void);

void graph_lcd_disp_line(unsigned short line,char* data);

void lcd_clear_ram(void);
void lcd_print_screen(unsigned char *screen);
void lcd_clear_rect(uchar left,  uchar top, uchar right, uchar bottom);
void lcd_invert_rect(uchar left, uchar top, uchar right, uchar bottom);
void lcd_horz_line(uchar left, uchar right, uchar row);
void lcd_vert_line(uchar top, uchar bottom, uchar column);
void lcd_clr_horz_line(uchar left, uchar right, uchar row);
void lcd_clr_vert_line(uchar top, uchar bottom, uchar column);
void lcd_draw_border(uchar left, uchar top, uchar right, uchar bottom);
void lcd_clear_border(uchar left, uchar top, uchar right, uchar bottom);
void lcd_glyph(uchar left, uchar top, uchar width, uchar height, uchar *glyph, uchar store_width);
void lcd_animated_glyph(uchar left, uchar top, uchar width, uchar num_of_bitmaps, uchar *glyph1, uchar *glyph2, uchar *glyph3);
void lcd_text(uchar left, uchar top, uchar font, char *str);
void lcd_update(uchar top, uchar bottom);
void lcd_cursor_set(uchar left, uchar top, uchar font, uchar ShowCursor, uchar blinking_rate);

#endif
