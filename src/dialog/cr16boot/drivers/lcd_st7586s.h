// This is the header file for the experimental lcd driver for SC1445x developoment board
// 			Supported displays:
// LCD:  (dotmatrix display) ---- using lcd driver ST7586s

/* 
	Driver interface description
	LCD		BOARD
	---------------------------
	RS		->        AD7
	DB[0..7]	->	DAB[0..7]
*/
#ifndef LCD_ST7586S_H 
#define LCD_ST7586S_H 
/***************       Definitions section             ***************/
/* HW dependent definitions */


/* Registers */
//Since RS -> AD11, data are accessed at 0x800 and instructions are accessed at 0x00
#define LCD_BASE 0x01080000
#define LCD_DR (LCD_BASE+0x800)
#define LCD_IR (LCD_BASE+0x00)
//#define LCD_LATCH 0x01090000

//#define LCD_SET_BACKLIGHT_ON  {PROTECT_OVER_16M_SDRAM_ACEESS;(*(volatile unsigned short *)(LCD_LATCH)=(0x6000));RESTORE_OVER_16M_SDRAM_ACEESS}//Set backlight on, rst high
//#define LCD_SET_BACKLIGHT_OFF {PROTECT_OVER_16M_SDRAM_ACEESS;(*(volatile unsigned short *)(LCD_LATCH)=(0x4000));RESTORE_OVER_16M_SDRAM_ACEESS}//Set backlight off, rst high

//Kernel MACROs
#define LCD_WRITE_DATA(x)	{(*(volatile unsigned char *)(LCD_DR)=(x));udelay(1);} /* write data to the display RAM (one byte)*/
#define LCD_WRITE_INST(x)	{(*(volatile unsigned char *)(LCD_IR)=(x));udelay(1);}
//#define LCD_READ_DATA(x)	{(x=*(volatile unsigned char *)(LCD_DR));udelay(1);} /* read data from the display RAM (one byte)*/

#define lcd_out_ctl(X)   {PROTECT_OVER_16M_SDRAM_ACEESS;LCD_WRITE_INST((X));RESTORE_OVER_16M_SDRAM_ACEESS;}
#define lcd_out_dat(X)   {PROTECT_OVER_16M_SDRAM_ACEESS;LCD_WRITE_DATA((X));RESTORE_OVER_16M_SDRAM_ACEESS;}

/* Display settings */
#define INVERSE_DISPLAY //When defined the display orientation is inversed 
#define B3P3_MODE 	//When defined 3bytes-2pixels mode is used. If not defined 2B3P_MODE is used

/* LCD screen and bitmap image array consants */
#define SCRN_TOP			0x0
//#define SCRN_BOTTOM			57
#define SCRN_BOTTOM			66
#define SCRN_LEFT			0x0
//#define SCRN_RIGHT			158
#define SCRN_RIGHT			101
#define DDRAM_SIZE			((SCRN_BOTTOM+1)*(SCRN_RIGHT+1)*2/8) //15360 bytes

#define X_BYTES				(SCRN_RIGHT+1)/3
#define Y_BYTES	      		((SCRN_BOTTOM+1))

#define COL_MIN				0x04//0x00
//#define COL_MAX				0x83
 



#endif
