// This is the header file for the experimental lcd driver for SC1445x developoment board
// 			Supported displays:
// LCD:  (dotmatrix display) ---- using lcd driver ST7529

/*
	Driver interface description
	LCD		BOARD
	---------------------------
	RS		->        AD7
	DB[0..7]	->	DAB[0..7]
*/
#ifndef LCD_ST7032_H
#define LCD_ST7032_H
/***************       Definitions section             ***************/
/***************       Definitions section             ***************/
/* HW dependent definitions */
// #define TOPWAY_LMB202ECC
// #ifdef TOPWAY_LMB162AFC
	// #define LCD_CHARS_PER_LINE 16
// #endif		
// #ifdef TOPWAY_LMB202ECC
	// #define LCD_CHARS_PER_LINE 20
// #endif	
#define LCD_I2C_IF //lcd i2c interface does not allow read commands
#define LCD_CHARS_PER_LINE  256
/* Registers */
/* READ/WRITE implementation*/
/***      Character code map definitions       ***/
//CGRAM (Character Generator RAM) - 8 codes (address space) - 16 addresses (aliased)
//NOTE: The driver considers valid only the 0x08-0x0F space, in order to restrict using addr 0x00 which can be confused with \0 null char in LCD_write_ddram_string ioctl
#define CGRAM_CODE_MIN 	0x08 
#define CGRAM_CODE_MAX	0x0F //code 0x08 equals code 0x00, code 0x09 equals code 0x01, etc
//CGROM (Character Generator ROM) 
#define CGROM_CODE_MIN 	0x10 
#define CGROM_CODE_MAX	0xFF
//DDRAM (Display Data RAM)

/***      Memory map definitions       ***/
//CGRAM address space
#define CGRAM_BEGIN 			0x00 
#define CGRAM_END		 		0x3F 
#define CGRAM_ADDR_MASK 		CGRAM_END 
//DDRAM address space
#define DDRAM_ONELINE_BEGIN 	0x00 
#define DDRAM_ONELINE_END		0x4F 

#define DDRAM_TWOLINES_LINE1_BEGIN 	0x00 
#define DDRAM_TWOLINES_LINE1_END	0x27 
#define DDRAM_TWOLINES_LINE2_BEGIN 	0x40 
#define DDRAM_TWOLINES_LINE2_END	0x67 
#define DDRAM_DUMMY_CONT			0xFF //dummy addr used by LCD_ddram_write_string ioctl, to continue from last position

/***      Instruction set definitions       ***/
/*** According to LMB162AFC datasheet ***/
//Timers
#define LONG_TIME 1100 // make this equal to 1,08 ms
#define SHORT_TIME 30 // make this equal to 26.3 us

//WRITE Commands
//1. Write "20h" to DDRAM and set DDRAM address (AC) to "00h"
#define CLEAR_DISPLAY_CMD 					(1<<0) // wait LONG_TIME 
//2. Set DDRAM address (AC) to "00h" and return cursor to its original position if shifted (DDRAM contents are not changed)
#define RET_HOME_CMD 						(1<<1) //wait LONG_TIME

//3. Set cursor moving direction and specify display shift, during data read and write of DDRAM and CGRAM
#define ENTRY_MODE_CMD						(1<<2)// wait SHORT_TIME
#define ENTRY_MODE_SHIFT_ON					(1<<0)//screen shifting
#define ENTRY_MODE_INCREMENT				(1<<1)//ID=1 or AC=AC+1 
//4. Set display on/off, cursor on/off, cursor blinking
#define DISPLAY_CMD			(1<<3) // wait SHORT_TIME
#define DISPLAY_CURSOR_BLINK_ON	(1<<0)//cursor blinkning on
#define DISPLAY_CURSOR_ON	(1<<1)//cursor on
#define DISPLAY_ON			(1<<2)//display on

//6.Function set : interface(4-bit, 8-bit), display (1-line, 2-line), fonts (5x11, 5x8)
#define FUNCTION_CMD		(1<<5)// wait SHORT_TIME
#define FUNCTION_8BIT		(1<<4)//else 4-bit interface
#define FUNCTION_2LINES		(1<<3)//else 1-line display
#define FUNCTION_5x11		(1<<2)//else 5x8 dots font
#define FUNCTION_IS			(1<<0)//instruction set 1 select

//8. Set DDRAM address in address counter
#define DDRAM_ADDR_CMD		(1<<7)// wait SHORT_TIME

//READ Commands
//8. Read busy flag and address
//#define BF_READ_CMD				(1<<7)// no wait time
//#define BF_READ_TIME			(0)

/*Instruction set 0 (IS =0 )*/
//1. Move the cursor or shift the display
#define SHIFT_CMD				(1<<4)// wait SHORT_TIME
#define SHIFT_SCREEN			(1<<3)//else shift cursor
#define SHIFT_RIGHT				(1<<2)//else left
//2. Set CGRAM address in address counter
#define CGRAM_ADDR_CMD		(1<<6)// wait SHORT_TIME

/*Instruction set 1 (IS =1 )*/
//1.Internal OSC freq
#define INTERNAL_OSC_FREQ_CMD (0x14)// 1/5 bias , wait SHORT_TIME

//2. Icon address
#define SET_ICON_ADDR_CMD 	(1<<6)// wait SHORT_TIME

//3.POWER/ICON/CONTRAST set (2 lsbits)
#define PIC_SET_CMD 		(0x50)// POWER/ICON/CONTRAST SET, wait SHORT_TIME
#define PIC_ICON_ON			(1<<3)
#define PIC_BOOST_ON		(1<<2)

//4.Follower set
#define FOL_CTRL_CMD 		(0x60)// follower control, wait SHORT_TIME
#define FOL_SET_FOL_ON 		(1<<3)// set follower on, wait SHORT_TIME

//5.Contrast set
#define CONTRAST_SET_CMD 	(0x70)// contrast set (4 lsbits), wait SHORT_TIME


 
//Commands states (used to store the previously issued cmds)
struct Cmd_State_struct {
	unsigned char entry_mode_cmd;
	unsigned char display_cmd;
	unsigned char shift_cmd;
	unsigned char function_cmd;
	unsigned char contrast_cmd;
};

struct LCD_ddram_write_struct {
	unsigned char addr;//a valid ddram address should be provided as starting address
	unsigned char length;//the number of bytes to be read	
	char buf[LCD_CHARS_PER_LINE];//string to be displayed
};
//NOTE: This is the biggest struct passed by the user. Otherwise update driver's LCD_buf size
struct LCD_ddram_read_struct {
	unsigned char addr;//a valid ddram address should be provided as starting address
	unsigned char length;//the number of bytes to be read
	char buf[LCD_CHARS_PER_LINE];//buffer where data will be stored
};
//Used for debugging
typedef struct lcd_debug_drv {
	unsigned char ver_major;
	unsigned char ver_minor;
}lcd_debug_drv_struct;

/* I2C definitions */
/*========================== Defintions ==================================*/
//#define PORT_OUTPUT GPIO_PUPD_OUT_NONE
//#define PID_SCL1 GPIO_PID_SCL1
//#define PID_SDA1 GPIO_PID_SDA1

#define SLAVE_ADDR		0x7C	//control byte in write-mode, 0111 1100  i2c serial LCD DRIVER st7032
//#define CONTROL_BYTE_READ	0x7D	//control byte in read -mode, 0111 1101  i2c serial LCD DRIVER st7032 -- not available
#define CONTROL_BYTES 	0x00
#define DATA_BYTES 		0x40

#define ACCESS_INT_PEND                    (0x0001)

/*========================== Local macro definitions & typedefs =============*/
#define SCK_100     0
#define SCK_400     1
#define SCK_1152    2  

#define BITS_8      0
#define BITS_9      1
/*========================== Global function prototypes =====================*/
void init_ACCESS1(unsigned char freq, unsigned char bits);



#endif
