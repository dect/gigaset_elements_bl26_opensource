/*
 * lcd_IST3020.c -- lcd char driver
 *Based on the code from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.   
 *
 * 
 */

/***************       Include headers section       ***************/

#include "armboot.h"
#include "ist3020.h"
#include "fonts.h"

#ifdef CONFIG_IST3020_LCD

typedef struct {
	char left;
	char top;
	char font;
	char visible_size;
	int  size ;
	char text[MAX_SCROLL_TEXT_SIZE];
	int ScrollPos;
}scroll_struct;

typedef struct {
	uchar left;
	uchar top;
	uchar font;
	uchar blinking_rate;
	uchar state;
	uchar enabled;
	uchar reserved[2];
}cursor_struct;

typedef struct {
	uchar left;
	uchar top;
	uchar width;
 	uchar blinking_rate;
	uchar state;   
	uchar enabled;   
	uchar bitmaps;   
	uchar reserved;   
	uchar  bitmap[4][128]; 
}animation_struct;

/***      Flags definitions     ***/
/***      Hw specific definitions       ***/

/***   Macros   ***/
#define	START_TIMER(TIMER, TMOUT) {TIMER.expires = jiffies + TMOUT;add_timer(&TIMER);}
#define	STOP_TIMER(TIMER) {del_timer(&TIMER);}
/***      Function Prototypes       ***/
/** Low level **/

/* LCD function prototype list */
void lcd_init(void);
void busy_wait(void);
void lcd_clear_ram(void);
void lcd_print_screen(unsigned char *screen);
void lcd_clear_rect(uchar left,  uchar top, uchar right, uchar bottom);
void lcd_invert_rect(uchar left, uchar top, uchar right, uchar bottom);
void lcd_horz_line(uchar left, uchar right, uchar row);
void lcd_vert_line(uchar top, uchar bottom, uchar column);
void lcd_clr_horz_line(uchar left, uchar right, uchar row);
void lcd_clr_vert_line(uchar top, uchar bottom, uchar column); 
void lcd_draw_border(uchar left, uchar top, uchar right, uchar bottom);
void lcd_clear_border(uchar left, uchar top, uchar right, uchar bottom);
void lcd_glyph(uchar left, uchar top, uchar width, uchar height, uchar *glyph, uchar store_width);
void lcd_animated_glyph(uchar left, uchar top, uchar width, uchar num_of_bitmaps, uchar *glyph1, uchar *glyph2, uchar *glyph3);
void lcd_text(uchar left, uchar top, uchar font, char *str);
void lcd_update(uchar top, uchar bottom);
void lcd_cursor_set(uchar left, uchar top, uchar font, uchar ShowCursor, uchar blinking_rate);
/**/
void lcd_contrast_level(uchar level);
void lcd_standby_enter(void);
void lcd_standby_exit(void);
void lcd_sleepmode_enter(void);
void lcd_sleepmode_exit(void);
void lcd_scroll_init(char *buf);
void busy_flag(void);

//Timers
static void InitializeTimer(struct timer_list *timer_ptr, void (*timer_function)(unsigned long param));
static void lcd_timer_refresh_fn(unsigned long param);
static void scroll_timer_refresh(unsigned long param);
static void cursor_timer_refresh(unsigned long param);
static void animation_timer_refresh(unsigned long param);



const unsigned char l_mask_array[8] ={0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
uchar l_display_array[Y_BYTES][X_BYTES];
scroll_struct m_scroll_struct;
cursor_struct m_cursor;

#define MAX_ANIMATED_BITMAPS 8
animation_struct m_animation[MAX_ANIMATED_BITMAPS];
 

/***************          Main body             ***************/

/****************************************************************************************************/
/************           DRAWING AND LCD CONTROLLER FUNTIONS  - EXPORTED BY IOCTL CMDS        *********/
/****************************************************************************************************/

/******************************************************* 
	Description: 	Initializes hw and driver internal state
	Note:			
 ********************************************************/
 #if defined(CONFIG_JW_BOARDS)
	#ifdef CONFIG_JW_V1_BOARD
		#define LCD_RESET_LOW		SetWord(P2_RESET_DATA_REG,GPIO_14)
		#define LCD_RESET_HIGH		SetWord(P2_SET_DATA_REG,GPIO_14)
		#define LCD_BKL_ON			SetWord(P1_SET_DATA_REG,GPIO_13)
		#define LCD_BKL_OFF			SetWord(P1_RESET_DATA_REG,GPIO_13)
	#endif
#endif	
 

void lcd_reset( void )
{
	
	LCD_BKL_ON;

	LCD_RESET_HIGH;
	udelay(50000);
	LCD_RESET_LOW;
	udelay(50000);
	udelay(50000);
	udelay(50000);
	udelay(50000);
	LCD_RESET_HIGH;
	
	
}
 
void lcdInit()
{

	unsigned i,j;

	lcd_reset();

	printf("\nIST3020 176x48 graphics lcd configuration\n");

	//The following cmds implement the initialization sequence as described in the datasheet
	
	lcd_out_ctl(LCD_SET_OSC_ON);    	  /* set built in oscillator ON*/
//    lcd_out_ctl(LCD_SET_ADC_NOR);          /* ADC select, cmd 8 */
	lcd_out_ctl(LCD_SET_ADC_REV);          /* ADC select, cmd 8 */
	 lcd_out_ctl(LCD_SET_SHL_NOR);
 //   lcd_out_ctl(LCD_SET_SHL_REV);          /* Flip on y-dir, cmd 15 */
	 lcd_out_ctl(LCD_SET_BIAS_1DIV9);      /* set bias to 1/9, cmd 11 */
	
	//lcd_out_ctl(0xe8/*LCD_SET_BIAS_1DIV9*/);      /* set bias to 1/9, cmd 11 */
	/*Power control*/
//	lcd_out_ctl(0xa1);          /* Flip on y-dir, cmd 15 */


	lcd_out_ctl(LCD_PWR_CTL|0x04);      /*turn on the volt converter, delay 50ms, cmd 16,, 2Ch*/
	udelay(50000);
	lcd_out_ctl(LCD_PWR_CTL|0x06);       /*turn on the volt regulator, delay 50ms, cmd 16, 2Eh  */
	udelay(50000);
	lcd_out_ctl(LCD_PWR_CTL|0x07);      /*turn on the volt follower, delay 50ms, cmd 16, 2Fh*/
	udelay(50000);
	
	lcd_out_ctl(LCD_REG_RESISTOR|0x03);//0x05); //regulator resistor select, cmd 17, 25h
	
	/*Set contrast*/
	lcd_out_ctl(LCD_REF_VOLT_MODE); //set refer volt mode, cmd 18, 81h, DUAL COMMAND
	lcd_out_ctl(LCD_REF_VOLT_REG|0x10); //set refer volt reg, cmd 18, 20h, CONTRAST VALUE


	udelay(50000);//wait for stabilizing LCD power levels
	udelay(50000);
	/*This code is not part of the hw init sequence*/
	/*User setting*/
	lcd_out_ctl(LCD_SET_LINE|0);        /*init line = 0 */	
	lcd_out_ctl(LCD_SET_PAGE+0);	/* set page 0 */
	lcd_out_ctl(LCD_SET_COL_HI+0);	/* set column 0 */
	lcd_out_ctl(LCD_SET_COL_LO+SCRN_LEFT);
	lcd_out_ctl(LCD_DISP_ON);		/* put the display on */	
	lcd_clear_ram();							// erase the display 	
	lcd_clear_rect(SCRN_LEFT,SCRN_TOP,SCRN_RIGHT,SCRN_BOTTOM);

	m_cursor.state =0;
	m_cursor.enabled =0;
	for (i=0;i<MAX_ANIMATED_BITMAPS;i++)	
	{
		m_animation[i].state = 0;
		m_animation[i].enabled = 0;
		m_animation[i].bitmaps = 0;
	}


#if defined(CONFIG_JW_BOARDS)
	#ifdef CONFIG_JW_V1_BOARD
		lcd_text(40, 20, 0, "ACP-124-P Boot...");
	#endif
#endif	
 
 

	lcd_draw_border(0, SCRN_TOP, SCRN_RIGHT, SCRN_BOTTOM);
	lcd_update(SCRN_TOP, SCRN_BOTTOM);

}

 /******************************************************* 
	Description:	Erases the lcd and the l_display_array
	Note:			
 ********************************************************/
void lcd_clear_ram(void)
{
	uchar p;
	uchar i;

	for(p=0; p<Y_BYTES; p++)
	{		
		lcd_out_ctl(LCD_SET_PAGE|p);	/* set page */
		lcd_out_ctl(LCD_SET_COL_HI|((COL_MIN & 0xF0)>>4));	/* set column 0 */
		lcd_out_ctl(LCD_SET_COL_LO|(COL_MIN & 0x0F));
		for(i=0; i<X_BYTES; i++)
		{
			lcd_out_dat(0);				/* clear the data */
			l_display_array[p][i]=0; /*Clear buffer*/
		}
	}
}

/******************************************************* 
	Description:	Inverts a rectangle area 
	Input:		left,  top, right,  bottom : reactange coordinates
	Note:			No runtime error check is performed
 ********************************************************/
void lcd_invert_rect(uchar left,  uchar top,    
			         uchar right, uchar bottom)
{
	uchar bit_pos;
	uchar x;
	uchar byte_offset;
	uchar y_bits;
	uchar remaining_bits;
	uchar mask;

	bit_pos = top & 0x07;					/* get starting bit offset into byte */

	for(x = left; x <= right; x++)
  	{
		byte_offset = top >> 3;				/* get byte offset into y direction */
		y_bits = (bottom - top) + 1;		/* get length in the x direction to write */
		remaining_bits = 8 - bit_pos;		/* number of bits left in byte */
		mask = l_mask_array[bit_pos];		/* get mask for this bit */

		while(y_bits)						/* while there are still bits to write */
    	{
			if((remaining_bits == 8) && (y_bits > 7))
			{
				/* here if we are byte aligned and have at least 1 byte to write */
				/* do the entire byte at once instead of bit by bit */
				while(y_bits > 7)			/* while there are at least 8 more bits to do */
				{
					l_display_array[byte_offset][x] ^= 0xFF;
					byte_offset++;
					y_bits -= 8;
				}
      		}
      		else
      		{
				/* here if not byte aligned or an entire byte does not need written */
				/* thus do bit by bit */
				l_display_array[byte_offset][x] ^= mask;
				if(l_mask_array[0] & 0x80)
				{
					mask >>= 1;
				}
				else
				{
					mask <<= 1;
				}
				y_bits--;
				remaining_bits--;
				if(remaining_bits == 0)
				{
					/* might have bust gotton byte aligned */
					/* so reset for beginning of a byte */
					remaining_bits = 8;
					byte_offset++;
					mask = l_mask_array[0];
				}
			}
		}
	}
}



/******************************************************* 
	Description:	Erases a rectangle area 
	Input:		left,  top, right,  bottom : reactange coordinates
	Note:			No runtime error check is performed
 ********************************************************/
void lcd_clear_rect(uchar left,  uchar top,    
			        uchar right, uchar bottom)
{
	uchar bit_pos;
	uchar x;
	uchar byte_offset;
	uchar y_bits;
	uchar remaining_bits;
	uchar mask;

	bit_pos = top & 0x07;					/* get starting bit offset into byte */

	for(x = left; x <= right; x++)
	{
		byte_offset = top >> 3;				/* get byte offset into y direction */
		y_bits = (bottom - top) + 1;		/* get length in the y direction to write */
		remaining_bits = 8 - bit_pos;		/* number of bits left in byte */
		mask = l_mask_array[bit_pos];		/* get mask for this bit */

		while(y_bits)						/* while there are still bits to write */
		{
			if((remaining_bits == 8) && (y_bits > 7))
			{
				/* here if we are byte aligned and have at least 1 byte to write */
				/* do the entire byte at once instead of bit by bit */
				while(y_bits > 7)			/* while there are at least 8 more bits to do */
				{
					l_display_array[byte_offset][x] = 0x00;
					byte_offset++;
					y_bits -= 8;
				}
			}
			else
			{
				/* here if not byte aligned or an entire byte does not need written */
				/* thus do bit by bit */
				l_display_array[byte_offset][x] &= ~mask;
				if(l_mask_array[0] & 0x80)
				{
					mask >>= 1;
				}
				else
				{
					mask <<= 1;
				}
				y_bits--;
				remaining_bits--;
				if(remaining_bits == 0)
				{
					/* might have bust gotton byte aligned */
					/* so reset for beginning of a byte */
					remaining_bits = 8;
					byte_offset++;
					mask = l_mask_array[0];
				}
			}
		}
	}
}

#if 1
/******************************************************* 
	Description:	Draws a rectangle border
	Input:		left,  top, right,  bottom : rectangle border coordinates
	Note:			No runtime error check is performed
 ********************************************************/
void lcd_draw_border(uchar left, uchar top, uchar right, uchar bottom)
{

	lcd_vert_line(top+1,top+1,left+1);	//+1
	lcd_vert_line(top+2,bottom-2,left);
	lcd_vert_line(bottom-1, bottom-1,left+1); //-1

	lcd_vert_line(top+1,top+1,right-1);//+1
	lcd_vert_line(top+2,bottom-2,right);
	lcd_vert_line(bottom-1,bottom-1,right-1);//-1

	lcd_horz_line(left+2,right-2,top);
	lcd_horz_line(left+2,right-2,bottom);
}

/******************************************************* 
	Description:	Clears a rectangle border
	Input:		left,  top, right,  bottom : rectangle border coordinates
	Note:			No runtime error check is performed
 ********************************************************/
void lcd_clear_border(uchar left, uchar top, uchar right, uchar bottom)
{
  	/* to undraw the box undraw the two vertical lines */
  	lcd_clr_vert_line(top,bottom,left);
  	lcd_clr_vert_line(top,bottom,right);

  	/* and the two horizonal lines that comprise it */
  	lcd_clr_horz_line(left,right,top);
    lcd_clr_horz_line(left,right,bottom);
}
#endif
/******************************************************* 
	Description: 	 Prints a glyph 
	Input:		left, top  : glyph coordinates
				width, height  : glyph width and height
				glyph  : pointer to glyph
				store_width  : glyph stored width
 ********************************************************/
void lcd_glyph(uchar left, uchar top,
			   uchar width, uchar height,
			   uchar *glyph, uchar store_width)
{
	uchar bit_pos;
	uchar byte_offset;
	uchar y_bits;
	uchar remaining_bits;
	uchar mask;
	uchar char_mask;
	uchar x;
	uchar *glyph_scan;
	uchar glyph_offset;

  	bit_pos = top & 0x07;		/* get the bit offset into a byte */

	glyph_offset = 0;			/* start at left side of the glyph rasters */
    char_mask = 0x80;			/* initial character glyph mask */

  	for (x = left; x < (left + width); x++)
  	{
    	byte_offset = top >> 3;        	/* get the byte offset into y direction */
		y_bits = height;				/* get length in y direction to write */
		remaining_bits = 8 - bit_pos;	/* number of bits left in byte */
		mask = l_mask_array[bit_pos];	/* get mask for this bit */
		glyph_scan = glyph + glyph_offset;	 /* point to base of the glyph */

    	/* boundary checking here to account for the possibility of  */
    	/* write past the bottom of the screen.                        */
    	while((y_bits) && (byte_offset < Y_BYTES)) /* while there are bits still to write */
    	{
			/* check if the character pixel is set or not */
			if(*glyph_scan & char_mask)
			{
				l_display_array[byte_offset][x] |= mask;	/* set image pixel */
			}
			else
			{
      			l_display_array[byte_offset][x] &= ~mask;	/* clear the image pixel */
			}

			if(l_mask_array[0] & 0x80)
			{
				mask >>= 1;
			}
			else
			{
				mask <<= 1;
			}
			
			y_bits--;
			remaining_bits--;
      		if(remaining_bits == 0)
      		{
				/* just crossed over a byte boundry, reset byte counts */
				remaining_bits = 8;
				byte_offset++;
				mask = l_mask_array[0];
      		}

			/* bump the glyph scan to next raster */
			glyph_scan += store_width;
		}

		/* shift over to next glyph bit */
		char_mask >>= 1;
		if(char_mask == 0)				/* reset for next byte in raster */
		{
			char_mask = 0x80;
			glyph_offset++;
	    }
	}
}
#if 0
void lcd_animated_glyph(uchar left, uchar top,
				uchar width, uchar num_of_bitmaps, uchar *glyph1, uchar *glyph2, uchar *glyph3)
{	 
	int i;
	STOP_TIMER(animation_timer);

 	// if width == 0, stop animation and clear glyph region
	if(width == 0) {
 		for (i=0;i<MAX_ANIMATED_BITMAPS;i++) {
			if (m_animation[i].enabled && (m_animation[i].left == left) && (m_animation[i].top == top)) {

				lcd_clear_rect(left, top, left + m_animation[i].width, top + m_animation[i].width);
 
				m_animation[i].enabled = 0;
				m_animation[i].state = 0;

 				lcd_update(top, top + m_animation[i].width);
 				break;
			}
		}
 
		for (i=0;i<MAX_ANIMATED_BITMAPS;i++) {
			if (m_animation[i].enabled) {
 				START_TIMER(animation_timer, 100 );
				return;
			}
		}

		return;
	}

	for (i=0;i<MAX_ANIMATED_BITMAPS;i++) {
		if (m_animation[i].enabled && (m_animation[i].left == left) && (m_animation[i].top == top)) {
			START_TIMER(animation_timer, 100 );
			return ;
		}
	}

	for (i=0;i<MAX_ANIMATED_BITMAPS;i++)
	{
	  if (!m_animation[i].enabled)
		  break;
	}
	if (i==MAX_ANIMATED_BITMAPS) {
		START_TIMER(animation_timer, 100 );
		return ;
	}

	m_animation[i].enabled = 1;
	m_animation[i].left =left; 
	m_animation[i].top =top; 
	m_animation[i].width =width; 
	m_animation[i].blinking_rate =200; 
	m_animation[i].state =0; 
	m_animation[i].bitmaps =num_of_bitmaps; 
	memcpy(m_animation[i].bitmap[0],glyph1,width*width/8);
	memcpy(m_animation[i].bitmap[1],glyph2,width*width/8);
	memcpy(m_animation[i].bitmap[2],glyph3,width*width/8);

	lcd_glyph(left,top,width,width,&m_animation[i].bitmap[0][0],width/8);  /* plug symbol into buffer */
	START_TIMER(animation_timer, 100 );
	lcd_update(SCRN_TOP, SCRN_BOTTOM);
 }
#endif
/******************************************************* 
	Description:	Prints text in specified location/font.
	Input:		left,  top: text coordinates
				font: the text font to be used
				*str: null terminated string
	Note:			non 0x20->0x7e characters are ignored
 ********************************************************/
void lcd_text(uchar left, uchar top, uchar font, char *str)
{
  	uchar x = left;
  	uchar glyph;
  	uchar width;
	uchar height;
	uchar store_width;
	uchar  *glyph_ptr;

  	while(*str != 0x00)
  	{
    	glyph = (uchar)*str;

		/* check to make sure the symbol is a legal one */
		/* if not then just replace it with the default character */
		if((glyph < fonts[font].glyph_beg) || (glyph > fonts[font].glyph_end))
		{
			glyph = fonts[font].glyph_def;
		}

    	/* make zero based index into the font data arrays */
    	glyph -= fonts[font].glyph_beg;
    	width = fonts[font].fixed_width;	/* check if it is a fixed width */
		if(width == 0)
		{
			width=fonts[font].width_table[glyph];	/* get the variable width instead */
		}

		height = fonts[font].glyph_height;
		store_width = fonts[font].store_width;

		glyph_ptr = fonts[font].glyph_table + ((unsigned int)glyph * (unsigned int)store_width * (unsigned int)height);

		/* range check / limit things here */
		if(x > SCRN_RIGHT)
		{
			x = SCRN_RIGHT;
		}
		if((x + width) > SCRN_RIGHT+1)
		{
			width = SCRN_RIGHT - x + 1;
		}
		if(top > SCRN_BOTTOM)
		{
			top = SCRN_BOTTOM;
		}
		if((top + height) > SCRN_BOTTOM+1)
		{
			height = SCRN_BOTTOM - top + 1;
		}

		lcd_glyph(x,top,width,height,glyph_ptr,store_width);  /* plug symbol into buffer */

		x += width;							/* move right for next character */
		str++;								/* point to next character in string */
	}
}

/******************************************************* 
	Description:	Updates a rectangle area of the lcd display. It copies data from the  l_display_array to the Lcd controller.
	Input:		top, bottom  : area coordinates
	Note:			The left, right coordinates of the updated rectangle are the leftmost, rightmost lcd coordinates 
 ********************************************************/
 void lcd_update(uchar top, uchar bottom)
{
	uchar x;
	uchar y;
	uchar yt;
	uchar yb;
	uchar *colptr;

	top =SCRN_TOP;
	bottom = SCRN_BOTTOM ;
	
	yt = top >> 3;				/* setup bytes of range */
	yb = bottom >> 3;

	for(y = yt; y <= yb; y++)
	{
#if 0	 
//giag debug	 
	(*(unsigned short *)(0x01400000))=1;
#endif	
		/* setup the page number for the y direction */
		lcd_out_ctl(LCD_SET_PAGE+y);	/* set page */
		/* setup column of update to left side */
		lcd_out_ctl(LCD_SET_COL_HI|((COL_MIN & 0xF0)>>4));	/* set column 0 */
		lcd_out_ctl(LCD_SET_COL_LO|(COL_MIN & 0x0F));

		colptr = &l_display_array[y][0];
		for (x=0; x < X_BYTES; x++)
		{
		
			lcd_out_dat(*colptr++);
		}
#if 0	 
//giag debug	 
	(*(unsigned short *)(0x01400000))=0;
#endif		
	}	
}

 void lcd_cursor_set(uchar left, uchar top, uchar font, uchar ShowCursor, uchar blinking_rate)
{
	uchar width;
	width=fonts[font].fixed_width;
	if (width==0) width=8;
	if (m_cursor.enabled)
	{
		m_cursor.enabled=0;
//		STOP_TIMER(cursor_timer);
 			
 		if(m_cursor.state) 
		{
//			printk("Invert cursor position [%d] [%d] \n", m_cursor.left, width);
			lcd_invert_rect(m_cursor.left, m_cursor.top, m_cursor.left + width - 1, m_cursor.top + fonts[m_cursor.font].glyph_height - 1);
			lcd_update(SCRN_TOP, SCRN_BOTTOM);
			m_cursor.state = 0;
			
 		}//else printk("Cursor is in cleared state \n");
	}//else printk("new text ... \n");

	if(ShowCursor == 0)	return;

	m_cursor.left = left;
	m_cursor.top = top;
	m_cursor.font = font;
	m_cursor.blinking_rate = blinking_rate;
//	printk("blink cursor .... ");

 	lcd_invert_rect(m_cursor.left, m_cursor.top, m_cursor.left + width - 1, m_cursor.top + fonts[m_cursor.font].glyph_height - 1);
 	lcd_update(SCRN_TOP, SCRN_BOTTOM);

// 	START_TIMER(cursor_timer, blinking_rate / 2);
	m_cursor.state = 1;
	m_cursor.enabled = 1;
}
#if 0
void lcd_scroll_init(char *buf)
{
	char visible_buffer[50];
	char scroll_enable = buf[4];

	if(scroll_enable == 0) {
		memset(visible_buffer, 0 , 50);
		strncpy(visible_buffer, &buf[5], buf[3]);
		lcd_text(buf[0], buf[1], buf[2], visible_buffer);
	
		return;
	}

	STOP_TIMER(scroll_timer);

	if(scroll_enable == -1)
		return;

	m_scroll_struct.left = buf[0];
	m_scroll_struct.top = buf[1];
	m_scroll_struct.font = buf[2];
	m_scroll_struct.visible_size = buf[3];
	strcpy(m_scroll_struct.text, &buf[5]);
	m_scroll_struct.size = strlen(m_scroll_struct.text);
	m_scroll_struct.ScrollPos = 0;

	memset(visible_buffer, 0 , 50);
	strncpy(visible_buffer, m_scroll_struct.text, m_scroll_struct.visible_size);
	lcd_text(m_scroll_struct.left, m_scroll_struct.top, m_scroll_struct.font, visible_buffer);

	if(m_scroll_struct.visible_size < m_scroll_struct.size) {
		// add a space at the end of the text
		strcat(m_scroll_struct.text, " ");
		m_scroll_struct.size++;

		START_TIMER(scroll_timer, 40);
	}
}

/******************************************************* 
	Description:  Enter standby mode	
	Input:			
 ********************************************************/
void lcd_standby_enter(void){	
	/*Set static indicator ON*/
	//do{
		lcd_out_ctl(LCD_ST_IND_MODE_ON);	 	
		lcd_out_ctl(LCD_ST_IND_REG|3);	 //set static indicator register to 3 (constantly on)
		//LCD_READ_STATUS(status);
	//}
	//while(status & BUSY_FLAG);
	lcd_out_ctl(LCD_DISP_OFF);
	lcd_out_ctl(LCD_EON_ON);
	lcd_out_ctl(LCD_SET_PWR_SAVE_STANDBY_MODE_ON);
}

/******************************************************* 
	Description:  Exit standby mode	
	Input:			
 ********************************************************/
void lcd_standby_exit(void){
	lcd_out_ctl(LCD_EON_OFF);
	lcd_out_ctl(LCD_DISP_ON);	//not written in the datasheet, but is needed
	lcd_out_ctl(LCD_SET_PWR_SAVE_MODE_OFF);
}	

/******************************************************* 
	Description:  Enter sleep mode	
	Input:			
 ********************************************************/
void lcd_sleepmode_enter(void){	
	/*Set static indicator ON*/
	lcd_out_ctl(LCD_ST_IND_MODE_OFF);	 	
	lcd_out_ctl(LCD_ST_IND_REG|0);	

	lcd_out_ctl(LCD_DISP_OFF);
	lcd_out_ctl(LCD_EON_ON);
	lcd_out_ctl(LCD_SET_PWR_SAVE_SLEEP_MODE_ON);
}

/******************************************************* 
	Description:  Exit standby mode	
	Input:			
 ********************************************************/
void lcd_sleepmode_exit(void){
	lcd_out_ctl(LCD_EON_OFF);
	lcd_out_ctl(LCD_ST_IND_MODE_ON);	 	
	lcd_out_ctl(LCD_ST_IND_REG|3);	
	lcd_out_ctl(LCD_DISP_ON);	//not written in the datasheet, but is needed
	lcd_out_ctl(LCD_SET_PWR_SAVE_MODE_OFF);
}

/******************************************************* 
	Description: 	Sets the lcd contrast level
	Input:		level: Brightness level (0-63).				
 ********************************************************/
void lcd_contrast_level(uchar level){	
	lcd_out_ctl(LCD_REF_VOLT_MODE);	 	 /* prime for the reference voltage */
	lcd_out_ctl(LCD_REF_VOLT_REG|(level & 0x3F));	 /* set reference voltage select */
}

#endif
/****************************************************************************************************/
/************           DRAWING AND LCD CONTROLLER FUNTIONS  - NOT EXPORTED BY IOCTL CMDS       *****/
/****************************************************************************************************/
/******************************************************* 
	Description:  Checks the busy flag and halts until it is cleared
	Returns:	Returns the last read byte.	
 ********************************************************/
void busy_wait(void){
	unsigned char e_flag;
	//while(delay_time--);
	//SET_LCD_READ();
	//LCD_ioctl_char(LCD_read_inst, &e_flag);
	// lcd_delay(20);
	LCD_READ_STATUS(e_flag);

	while(e_flag & BUSY_FLAG){
		LCD_READ_STATUS(e_flag);
		PDEBUG("LCD: busy_flag read busy \n");
	}
	//PDEBUG("LCD: busy_flag read clear \n");
	//LCD_ioctl_char(LCD_read_inst, &e_flag);
	//SET_LCD_WRITE();		
}

/******************************************************* 
	Description: 	Displays a bmp on the display.
	Input:		*screen: Pointer to an array of Y_BYTES*X_BYTES. 
				The array uses the same representation as the  l_display_array
 ********************************************************/
void lcd_print_screen(unsigned char *screen)
{
	unsigned char p;
	unsigned char i;

	for(p=0; p<Y_BYTES; p++)
	{
		lcd_out_ctl(LCD_SET_PAGE|p);	/* set page */
		lcd_out_ctl(LCD_SET_COL_HI|((COL_MIN & 0xF0)>>4));	/* set column 0 */
		lcd_out_ctl(LCD_SET_COL_LO|(COL_MIN & 0x0F));

		for(i=0; i<X_BYTES; i++)
		{
			// lcd_out_dat(screen[i*Y_BYTES+8-p]);				/* print the data */
			lcd_out_dat(screen[p*X_BYTES+i]);				/* print the data */
		}
	}
}
#if 1
/******************************************************* 
	Description: 	Draws a horizontal line 
	Input:		left, right, row  : line coordinates
 ********************************************************/
void lcd_horz_line(uchar left, uchar right, uchar row)
{
	uchar bit_pos;
	uchar byte_offset;
	uchar mask;
	uchar col;

  	bit_pos = row & 0x07;			/* get the bit offset into a byte */
  	byte_offset = row >> 3;		    /* get the byte offset into x array */
  	mask = l_mask_array[bit_pos]; 	/* get the mask for this bit */

  	for(col = left; col <= right; col++)
  	{
    	l_display_array[byte_offset][col] |= mask;
  	}
}

/******************************************************* 
	Description: 	Draws a vertical line 
	Input:		top, bottom, column  : line coordinates
 ********************************************************/
void lcd_vert_line(uchar top, uchar bottom, uchar column)
{
	uchar bit_pos;
	uchar byte_offset;
	uchar y_bits;
	uchar remaining_bits;
	uchar mask;

	bit_pos = top & 0x07;		   /* get starting bit offset into byte */

	byte_offset = top >> 3;		   /* get byte offset into y direction */
	y_bits = (bottom - top) + 1;   /* get length in the x direction to write */
	remaining_bits = 8 - bit_pos;  /* number of bits left in byte */
	mask = l_mask_array[bit_pos];  /* get mask for this bit */

	while(y_bits)				   /* while there are still bits to write */
	{
		if((remaining_bits == 8) && (y_bits > 7))
		{
			/* here if we are byte aligned and have at least 1 byte to write */
			/* do the entire byte at once instead of bit by bit */
			while(y_bits > 7)			/* while there are at least 8 more bits to do */
			{
				l_display_array[byte_offset][column] = 0xFF;
				byte_offset++;
				y_bits -= 8;
			}
		}
		else
		{
      		/* we are not byte aligned or an entire byte does not need written */
      		/* do each individual bit                                          */
			l_display_array[byte_offset][column] |= mask;
			if(l_mask_array[0] & 0x80)
			{
				mask >>= 1;
			}
			else
			{
				mask <<= 1;
			}
			y_bits--;
			remaining_bits--;
			if(remaining_bits == 0)
			{
				/* might have bust gotton byte aligned */
				/* so reset for beginning of a byte */
				remaining_bits = 8;
				byte_offset++;
				mask = l_mask_array[0];
			}
		}
	}
}

/******************************************************* 
	Description: 	Clears a horizontal line 
	Input:		left, right, row  : line coordinates
 ********************************************************/
void lcd_clr_horz_line(uchar left, uchar right,
		               uchar row)
{
	uchar bit_pos;
	uchar byte_offset;
	uchar mask;
	uchar col;

  	bit_pos = row & 0x07;			/* get the bit offset into a byte */
  	byte_offset = row >> 3;		    /* get the byte offset into x array */
  	mask = l_mask_array[bit_pos]; 	/* get the mask for this bit */

  	for(col = left; col <= right; col++)
  	{
    	l_display_array[byte_offset][col] &= ~mask;
  	}
}

/******************************************************* 
	Description: 	Clears a vertical line 
	Input:		left, right, row  : line coordinates
 ********************************************************/
void lcd_clr_vert_line(uchar top, uchar bottom, uchar column)
{
	uchar bit_pos;
	uchar byte_offset;
	uchar y_bits;
	uchar remaining_bits;
	uchar mask;

	bit_pos = top & 0x07;		   /* get starting bit offset into byte */

	byte_offset = top >> 3;		   /* get byte offset into y direction */
	y_bits = (bottom - top) + 1;   /* get length in the x direction to write */
	remaining_bits = 8 - bit_pos;  /* number of bits left in byte */
	mask = l_mask_array[bit_pos];  /* get mask for this bit */

	while(y_bits)				   /* while there are still bits to write */
	{
		if((remaining_bits == 8) && (y_bits > 7))
		{
			/* here if we are byte aligned and have at least 1 byte to write */
			/* do the entire byte at once instead of bit by bit */
			while(y_bits > 7)			/* while there are at least 8 more bits to do */
			{
				l_display_array[byte_offset][column] = 0x00;
				byte_offset++;
				y_bits -= 8;
			}
		}
		else
		{
      		/* we are not byte aligned or an entire byte does not need written */
      		/* do each individual bit                                          */
			l_display_array[byte_offset][column] &= ~mask;
			if(l_mask_array[0] & 0x80)
			{
				mask >>= 1;
			}
			else
			{
				mask <<= 1;
			}
			y_bits--;
			remaining_bits--;
			if(remaining_bits == 0)
			{
				/* might have bust gotton byte aligned */
				/* so reset for beginning of a byte */
				remaining_bits = 8;
				byte_offset++;
				mask = l_mask_array[0];
			}
		}
	}
}
#endif
#if 0
/****************************************************************************************************/
/************                                       LINUX DRIVER FUNTIONS                           	                                    *********/
/****************************************************************************************************/

/******************************************************* 
	Description: 	Open lcd dev file.

 ********************************************************/
int LCD_open(struct inode *inode, struct file *filp)
{
	lcd_sc14450_dev_struct *dev; /* device information */
	dev = container_of(inode->i_cdev, lcd_sc14450_dev_struct, cdev);
	filp->private_data = dev; /* for other methods */
	return 0;
}
/******************************************************* 
	Description: 	Close device file.

 ********************************************************/
int LCD_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static void InitializeTimer(struct timer_list *timer_ptr, void (*timer_function)(unsigned long param))
{
	init_timer(timer_ptr);	
	timer_ptr->data = 0; //no argument for kbd_timer_fn needed		
	timer_ptr->function = timer_function;
}

/******************************************************* 
	Description: 	Refresh timer routing.

 ********************************************************/
 static void lcd_timer_refresh_fn(unsigned long param){
	lcd_update(SCRN_TOP, SCRN_BOTTOM);
	START_TIMER(lcd_timer, lcd_conf->refresh_tmout);	
}

static void scroll_timer_refresh(unsigned long param){
 	char visible_buffer[50];
	int i;

	m_scroll_struct.text[m_scroll_struct.size] = m_scroll_struct.text[0];
  
	for(i=0 ; i < m_scroll_struct.size ; i++)
		m_scroll_struct.text[i] = m_scroll_struct.text[i+1];

	m_scroll_struct.text[m_scroll_struct.size] = '\0';

	memcpy(visible_buffer, m_scroll_struct.text, m_scroll_struct.visible_size);
	visible_buffer[(int)m_scroll_struct.visible_size] = '\0';
	lcd_text(m_scroll_struct.left, m_scroll_struct.top, m_scroll_struct.font, visible_buffer);

	lcd_update(SCRN_TOP, SCRN_BOTTOM);

	START_TIMER(scroll_timer, 40);
}

static void animation_timer_refresh(unsigned long param) {
	int i;
	int found =0; 


	for (i=0;i<MAX_ANIMATED_BITMAPS;i++)
	{
			if (m_animation[i].enabled)
			{
				m_animation[i].state++;
				if (m_animation[i].state==m_animation[i].bitmaps)
					m_animation[i].state = 0;

				lcd_glyph(m_animation[i].left,m_animation[i].top,
					m_animation[i].width,
					m_animation[i].width,	
					&m_animation[i].bitmap[m_animation[i].state][0],
					m_animation[i].width/8);  /* plug symbol into buffer */
				found =1; 
			}
	}
	lcd_update(SCRN_TOP, SCRN_BOTTOM);
	if (found)
		START_TIMER(animation_timer, 100 );
}

static void cursor_timer_refresh(unsigned long param) {
 	uchar width;
 	 
	if (!m_cursor.enabled) {printk("timer not enabled ...\n");return;}
 
	width= fonts[m_cursor.font].fixed_width;
 	if (width==0)  
  		width=8;//fonts[m_cursor.font].width_table[glyph];
 
	lcd_invert_rect(m_cursor.left, m_cursor.top,m_cursor.left+ width - 1, m_cursor.top + fonts[m_cursor.font].glyph_height - 1);
	if (m_cursor.state==1) m_cursor.state=0;
	else m_cursor.state=1;

  	lcd_update(SCRN_TOP, SCRN_BOTTOM);
 	START_TIMER(cursor_timer, m_cursor.blinking_rate / 2);		 
}

/******************************************************* 
	Description: 	lcd ioctl.

 ********************************************************/
char buf[10000];
int LCD_ioctl(struct inode *inode, struct file *filp,
                 unsigned int cmd, unsigned long arg)
{
	int i;
	//char buf[MAX_IOCTL_ARGS];
	lcd_sc14450_dev_struct *dev = filp->private_data;
	
		if (down_interruptible(&dev->sem))
			return -ERESTARTSYS; 
		switch (cmd){
			case LCD_init:
				lcd_init();
				up (&dev->sem);				
				return 0;		
			//Issue a LCD cmd directly from the user space (it is assumed that it is a valid cmd)
			case LCD_out_ctl:
				if(copy_from_user(buf, (char*)arg, 2*sizeof(char) )){//copy only addr and length first
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}	
				lcd_out_ctl(buf[0]|buf[1]);
				up (&dev->sem);				
				return 0;
			case LCD_clear_ram:
				lcd_clear_ram();
				up (&dev->sem);				
				return 0;		
			case LCD_clear_rect:
				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){//copy only addr and length first
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}	

				lcd_clear_rect(buf[0], buf[1], buf[2], buf[3]);
				PDEBUG("LCD: ioctl LCD_clear_rect(%d, %d, %d, %d)\n", buf[0], buf[1], buf[2], buf[3]);
				up (&dev->sem);				
				return 0;	
			case LCD_invert_rect:
				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}	
				lcd_invert_rect(buf[0], buf[1], buf[2], buf[3]);
				PDEBUG("LCD: ioctl LCD_invert_rect(%d, %d, %d, %d)\n", buf[0], buf[1], buf[2], buf[3]);
				up (&dev->sem);				
				return 0;		
			case LCD_draw_border:
				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}	
				lcd_draw_border(buf[0], buf[1], buf[2], buf[3]);
				PDEBUG("LCD: ioctl LCD_draw_border(%d, %d, %d, %d)\n", buf[0], buf[1], buf[2], buf[3]);
				up (&dev->sem);				
				return 0;
			case LCD_clear_border:
				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}	
				lcd_clear_border(buf[0], buf[1], buf[2], buf[3]);
				PDEBUG("LCD: ioctl LCD_clear_border(%d, %d, %d, %d)\n", buf[0], buf[1], buf[2], buf[3]);
				up (&dev->sem);				
				return 0;
			case LCD_text:
				/*Copy the string to be displayed*/
				i=0;									
				while(i<MAX_IOCTL_ARGS){
					if(copy_from_user(buf+i, (char*)arg+i, sizeof(char))){
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}
					PDEBUG("LCD: ioctl LCD_text i: %x, copied: %x \n", i, buf[i]);
					if(i>2 && buf[i]==0)
						break;
					i++;
				}
				PDEBUG("LCD: ioctl LCD_text(%d, %d, %d, %s)\n", buf[0], buf[1], buf[2], &buf[3]);
				lcd_text(buf[0], buf[1], buf[2], &buf[3]);
				up (&dev->sem);				
				return 0;

			case LCD_label:
				/*Copy the string to be displayed*/
				i=0;					
		
				while(i<MAX_IOCTL_ARGS){
			
					if(copy_from_user(buf+i, (char*)arg+i, sizeof(char))){
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}
					//printk("LCD: ioctl LCD_text i: %x, copied: %x \n", i, buf[i]);
					if(i>4 && buf[i]==0)
						break;
					i++;
				}
			//.	printk("LCD: ioctl LCD_label(%d, %d, %d, %d, %d, %s)\n", buf[0], buf[1], buf[2], buf[3], buf[4], &buf[5]);
			//printk("LCD BUFFER = %s\n\n", &buf[10]);
				            
				lcd_scroll_init(buf);

				up (&dev->sem);				
				return 0;	

			case LCD_glyph:
				if(copy_from_user(buf, (char*)arg, 3*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}

				if ((buf[2]*buf[2]/8)<(MAX_IOCTL_ARGS-3))
				{
					if(copy_from_user(&buf[3], (char*)arg+3, (buf[2]*buf[2]/8)*sizeof(char))){
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}
					PDEBUG("LCD: ioctl LCD_glyph(%d, %d, %d, %s)\n", buf[0], buf[1], buf[2], &buf[3]);
 					lcd_glyph((uchar)buf[0], (uchar)buf[1], (uchar)buf[2], (uchar)buf[2], (uchar*)&buf[3], (uchar)buf[2]/8);
					up (&dev->sem);
					return 0;
				}else {
					printk(KERN_WARNING "Too large bitmap\n");
					up (&dev->sem);
					return -EFAULT;
				}
				
				return 0;

			case LCD_animate_bitmap:
				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}

				if ((3 *buf[2]*buf[2]/8)<(MAX_IOCTL_ARGS-4))
				{
					if(copy_from_user(&buf[4], (char*)arg+4, (3*buf[2]*buf[2]/8)*sizeof(char))){
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}
					PDEBUG("LCD: ioctl LCD_animate_bitmap(%d, %d, %d, %d, %s)\n", buf[0], buf[1], buf[2], buf[3], &buf[4]);
 					lcd_animated_glyph((uchar)buf[0], (uchar)buf[1], (uchar)buf[2], (uchar)buf[3], (uchar*)&buf[4], (uchar*)&buf[4+buf[2]*buf[2]/8], (uchar*)&buf[4+2*buf[2]*buf[2]/8]);
					up (&dev->sem);
					return 0;
				}else {
					printk(KERN_WARNING "Too large bitmaps\n");
					up (&dev->sem);
					return -EFAULT;
				}
				
				return 0;

			case LCD_update:
				if(copy_from_user(buf, (char*)arg, 2*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}	
				PDEBUG("LCD: ioctl LCD_update(%d, %d)\n", buf[0], buf[1]);
				lcd_update(buf[0], buf[1]);
				up (&dev->sem);				
				return 0;				
			case LCD_standby_enter:
				lcd_standby_enter();
				up (&dev->sem);				
				return 0;		
			case LCD_standby_exit:
				lcd_standby_exit();
				up (&dev->sem);				
				return 0;		
			case LCD_sleepmode_enter:
				lcd_sleepmode_enter();
				up (&dev->sem);				
				return 0;						
			case LCD_sleepmode_exit:
				lcd_sleepmode_exit();
				up (&dev->sem);				
				return 0;	
			case LCD_backlight_level:			
				if(copy_from_user(buf, (char*)arg, sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}	
#ifndef CONFIG_SC14452				
				LCD_BACKLIGHT_LEVEL(buf[0]);					
				PDEBUG("LCD: ioctl LCD_backlight_level(%d)\n", buf[0]);				
#endif				
				up (&dev->sem);				
				return 0;					
			case LCD_contrast_level:			
				if(copy_from_user(buf, (char*)arg, sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}			
				lcd_contrast_level(buf[0]);		
				PDEBUG("LCD: ioctl lcd_contrast_level(%d)\n", buf[0]);								
				up (&dev->sem);				
				return 0;		
			case LCD_refresh_tmout:
				if ( (copy_from_user((char*)&lcd_conf->refresh_tmout, (char*)arg, sizeof(char))) ) {
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}			
				STOP_TIMER(lcd_timer);	
				if(lcd_conf->refresh_tmout){
					START_TIMER(lcd_timer, lcd_conf->refresh_tmout);	
					PDEBUG("LCD: timer set to (%d)\n", lcd_conf->refresh_tmout);					
				}
				else{
					STOP_TIMER(lcd_timer);	
					PDEBUG("LCD: timer stopped \n");					
				}
				up (&dev->sem);
				return 0;

			case LCD_show_cursor:
				if(copy_from_user(buf, (char*)arg, 5*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}			
				
				PDEBUG("LCD: ioctl LCD_show_cursor(%d, %d, %d, %d, %d)\n", buf[0], buf[1], buf[2], buf[3], buf[4]);
				lcd_cursor_set(buf[0], buf[1], buf[2], buf[3], buf[4]);	
				up (&dev->sem);				
				return 0;		
	}
		
/**** DEBUGGING IOCTLS ****/		
#ifdef LCD_SC14450_DEBUG	
#endif
	printk(KERN_WARNING "Invalid lcd ioctl \n");
	return -ENOTTY;

}

/******************************************************* 
	Description: 	Module initialization.

 ********************************************************/
int __init LCD_init_module(void)
{
	int result;
	dev_t dev = 0;

	/* Initializations*/
	PDEBUG("module init function, entry point\n");
/*
 * Get a range of minor numbers to work with, asking for a dynamic
 * major unless directed otherwise at load time.
 */
	if (lcd_sc14450_major) {
		dev = MKDEV(lcd_sc14450_major, lcd_sc14450_minor);
		result = register_chrdev_region(dev, 1, "lcd_sc14450");
	} 
	else {
		result = alloc_chrdev_region(&dev, lcd_sc14450_minor, 1, "lcd_sc14450");
		lcd_sc14450_major = MAJOR(dev);
	}
	if (result < 0) {
		printk(KERN_WARNING "lcd_sc14450: can't get major %d\n", lcd_sc14450_major);
		return result;
	}

	/*Initialize driver internal vars*/
	memset((char*)&lcd_dev, 0, sizeof(lcd_sc14450_dev_struct));	
	/*Copy init configuration*/
	memcpy(lcd_conf, &lcd_init_conf, sizeof(struct lcd_sc14450_conf_struct));	
	init_MUTEX(&lcd_dev.sem);
	spin_lock_init(&lcd_dev.lock);
	LCD_setup_cdev(&lcd_dev);	
	/*Enable kbe int if needed*/
	printk(KERN_NOTICE "lcd_sc14450: driver for ist3020 controller initialized with major devno: %d\n", lcd_sc14450_major);

#ifdef LCD_SC14450_DEBUG
#endif
	/*Init hw*/
	lcd_init();
	init_timer(&lcd_timer);	
	lcd_timer.data = 0; //no argument for timer_fn needed		
	lcd_timer.function = lcd_timer_refresh_fn;
	if(lcd_conf->refresh_tmout)
		START_TIMER(lcd_timer, lcd_conf->refresh_tmout);	
	return 0; /* succeed */

  fail:
	unregister_chrdev_region(dev, 1);
	return result;
}


void busy_flag(void){
	volatile unsigned char busy_flag;
	unsigned char max_read = 3; //max_read is used for safety in order to exit loop if something goes wrong
	LCD_READ_STATUS(busy_flag);
	while( (busy_flag & BUSY_FLAG) && (max_read--) ){	
		LCD_READ_STATUS(busy_flag);
		PDEBUG("LCD: busy_flag read busy \n");
	}
}

module_init(LCD_init_module);
module_exit(LCD_cleanup_module);

#endif
#endif

