// This is the header file for the experimental lcd driver for SC1445x developoment board
// 			Supported displays:
// LCD:  (dotmatrix display) ---- using lcd driver ST7529

/*
	Driver interface description
	LCD		BOARD
	---------------------------
	RS		->        AD7
	DB[0..7]	->	DAB[0..7]
*/
#ifndef LCD_ST7529_H
#define LCD_ST7529_H

#include "config.h"
/***************       Definitions section             ***************/
/* HW dependent definitions */

/* Registers */
//Since RS -> AD7, data are accessed at 0x80 and instructions are accessed at 0x00
#define LCD_BASE 0x01080000
#define LCD_DR (LCD_BASE+0x80)
#define LCD_IR (LCD_BASE+0x00)
#define LCD_LATCH 0x01090000

//#define LCD_SET_BACKLIGHT_ON  {PROTECT_OVER_16M_SDRAM_ACEESS;(*(volatile unsigned short *)(LCD_LATCH)=(0x6000));RESTORE_OVER_16M_SDRAM_ACEESS}//Set backlight on, rst high
//#define LCD_SET_BACKLIGHT_OFF {PROTECT_OVER_16M_SDRAM_ACEESS;(*(volatile unsigned short *)(LCD_LATCH)=(0x4000));RESTORE_OVER_16M_SDRAM_ACEESS}//Set backlight off, rst high
extern unsigned int lcd_backlight;
#define LCD_SET_BACKLIGHT_ON	{lcd_backlight=1;}
#define LCD_SET_BACKLIGHT_OFF	{lcd_backlight=0;}

/* commands of the ST7529 LCD controller */
//External Instruction Selection
//=================================
//CMD 1
#define LCD_EXT_IN			0x30	/* external instruction disable (parameter bytes: none)*/
//CMD 2
#define LCD_EXT_OUT			0x31	/* external instruction enable (parameter bytes: none)*/

//EXT == 0 Commands
//=================================
//CMD 1
#define LCD_DISP_ON			0xAF	/* turn LCD display ON (parameter bytes: none)*/
//CMD 2
#define LCD_DISP_OFF 		0xAE	/* turn LCD display OFF (parameter bytes: none)*/
//CMD 3
#define LCD_DISP_NOR		0xA6	/* set normal pixel display (parameter bytes: none) */
//CMD 4
#define LCD_DISP_REV		0xA7	/* set reverse pixel display (parameter bytes: none)*/
//CMD 5
#define LCD_COM_SCN			0xBB	/* set common scan direction (parameter bytes: 1)*/
//CMD 6
#define LCD_DIS_CTRL		0xCA	/* set display control (parameter bytes: 3)*/
//CMD 7
#define LCD_SLEEP_IN		0x95	/* enter sleep mode (parameter bytes: none)*/
//CMD 8
#define LCD_SLEEP_OUT		0x94	/* exit sleep mode (parameter bytes: none)*/
//CMD 9
#define LCD_LA_SET			0x75	/* set line address (parameter bytes: 2)*/
//CMD 10
#define LCD_CA_SET			0x15	/* set column address (parameter bytes: 2)*/
//CMD 11
#define LCD_DAT_SCAN_DIR	0xBC	/* set data scan direction (parameter bytes: 3)*/
//CMD 12
#define LCD_RAM_WR			0x5C	/* write data to memory (parameter bytes: number of data written)*/
//CMD 13
#define LCD_RAM_RD			0x5D	/* read data from memory (parameter bytes: number of data read)*/
//CMD 14
#define LCD_PARTIAL_IN		0xA8	/* specify partial display area (parameter bytes: 2)*/
//CMD 15
#define LCD_PARTIAL_OUT		0xA9	/* exit partial display mode (parameter bytes: none)*/
//CMD 16
#define LCD_RMW_IN			0xE0	/* read modify write in (parameter bytes: none)*/
//CMD 17
#define LCD_RMW_OUT			0xEE	/* exit read modify write in mode (parameter bytes: none)*/
//CMD 18
#define LCD_ASC_SET			0xAA	/* set area scroll (parameter bytes: 4)*/
//CMD 19
#define LCD_SCS_SET			0xAB	/* set scroll start address (parameter bytes: 1)*/
//CMD 20
#define LCD_OSC_ON			0xD1	/* turn internall oscillation on (parameter bytes: none)*/
//CMD 21
#define LCD_OSC_OFF			0xD2	/* turn internall oscillation off (parameter bytes: none)*/
//CMD 22
#define LCD_PWR_CTRL		0x20	/* turn on/off booster and voltage regulators and set ref.voltage (parameter bytes: none)*/
//CMD 23
#define LCD_VOL_CTRL		0x81	/* turn on/off booster and voltage regulators and set ref.voltage (parameter bytes: 2)*/
//CMD 24
#define LCD_VOL_UP			0xD6	/* increment control offset value of voltage regulator (parameter bytes: none)*/
//CMD 25
#define LCD_VOL_DOWN		0xD7	/* deccrement control offset value of voltage regulator (parameter bytes: none)*/
//CMD 26
#define LCD_RESERVED		0x82	/* reserved command (parameter bytes: none)*/
//CMD 27
#define LCD_EPS_RRD_1		0x7C	/* read register 1 (parameter bytes: none)*/
//CMD 28
#define LCD_EPS_RRD_2		0x7D	/* read register 2 (parameter bytes: none)*/
//CMD 29
#define LCD_NOP				0x25	/* nop command (parameter bytes: none)*/
//CMD 30
#define LCD_STAT_READ		0x00	/* status read command (parameter bytes: none)*/
//CMD 31
#define LCD_EP_INT			0x07	/* status read command (parameter bytes: 1)*/

//EXT == 1 Commands
//=================================
//CMD 1
#define LCD_GRAY1_SET		0x20	/* set the gray level of 16 odd frames (parameter bytes: 16) */
//CMD 2
#define LCD_GRAY2_SET		0x21	/* set the gray level of 16 even frames (parameter bytes: 16) */
//CMD 3
#define LCD_ANA_SET			0x32	/* set the analog circuit parameters (parameter bytes: 3) */
//CMD 4
#define LCD_SW_INIT			0x34	/* software initial (parameter bytes: none) */
//CMD 5
#define LCD_EPC_IN			0xCD	/* eeprom control in (enable read/write) (parameter bytes: 1) */
//CMD 6
#define LCD_EPC_OUT			0xCC	/* eeprom control out (parameter bytes: none) */
//CMD 7
#define LCD_EPM_WR			0xFC	/* eeprom data write (parameter bytes: none) */
//CMD 8
#define LCD_EPM_RD			0xFD	/* eeprom data read (parameter bytes: none) */

#define LCD_WRITE_DATA(x)	{(*(volatile unsigned char *)(LCD_DR)=(x));udelay(1);} /* write data to the display RAM (one byte)*/
#define LCD_WRITE_INST(x)	{(*(volatile unsigned char *)(LCD_IR)=(x));udelay(1);}
#define LCD_READ_DATA(x)	{(x=ReverseLookupTable[*(volatile unsigned char *)(LCD_DR)]);udelay(1);} /* read data from the display RAM (one byte)*/

#define lcd_out_ctl(X)   {PROTECT_OVER_16M_SDRAM_ACEESS;LCD_WRITE_INST(ReverseLookupTable[(X)]);RESTORE_OVER_16M_SDRAM_ACEESS;}
#define lcd_out_dat(X)   {PROTECT_OVER_16M_SDRAM_ACEESS;LCD_WRITE_DATA(ReverseLookupTable[(X)]);RESTORE_OVER_16M_SDRAM_ACEESS;}
#define lcd_in_dat(X)   {PROTECT_OVER_16M_SDRAM_ACEESS;LCD_READ_DATA((X));RESTORE_OVER_16M_SDRAM_ACEESS;}

extern unsigned int lcd_backlight;
#define LCD_SET_BACKLIGHT_ON	{lcd_backlight=1;}
#define LCD_SET_BACKLIGHT_OFF	{lcd_backlight=0;}

/* Display settings */
#if !(defined(L_V4_BOARD) || defined (L_V4_CNXT_BOARD))
	#define INVERSE_DISPLAY //When defined the display orientation is inversed
#endif
#define B3P3_MODE 	//When defined 3bytes-2pixels mode is used. If not defined 2B3P_MODE is used

/* LCD screen and bitmap image array constants */
#define SCRN_TOP			0x0
#if defined (L_V2_BOARD) || defined (L_V2_CNXT_BOARD)
#define SCRN_BOTTOM			55 //41
#elif defined(L_V5_BOARD) || defined (L_V5_CNXT_BOARD)
#define SCRN_BOTTOM			41 //41
#elif defined(L_V4_BOARD) || defined (L_V4_CNXT_BOARD)
#define SCRN_BOTTOM			143
#endif
#define SCRN_LEFT			0x0//0//0x04
#define SCRN_RIGHT			239//0x83
#define DDRAM_SIZE			((SCRN_BOTTOM+1)*(SCRN_RIGHT+1)*5/8) //8400 bytes

#define X_BYTES				(SCRN_RIGHT+1)

#if defined(L_V2_BOARD) || defined(L_V4_BOARD) || defined (L_V2_CNXT_BOARD)|| defined(L_V4_CNXT_BOARD)
#define Y_BYTES	      		((SCRN_BOTTOM+1)/8)//*/7
#elif defined(L_V5_BOARD) || defined(L_V5_CNXT_BOARD)
#define Y_BYTES	      		 6 //((SCRN_BOTTOM+1)/8)//*/7
#endif


#define COL_MIN				0x00//0x04//0x00
//#define COL_MAX				0x83






#endif
