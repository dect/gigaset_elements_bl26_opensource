#include <termios.h>

#define SERIAL_ERROR	-1	/* General error, see errno for details */
#define SERIAL_TIMEOUT	-2
#define SERIAL_EOF	-3

extern speed_t cvtspeed(char *);
extern int serialopen(char *, speed_t);
extern int serialreadchar(int, int);
extern int serialwrite(int, char *, int);
extern int serialclose(int);

extern void serial_lock();
extern void serial_unlock();
//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:54:49.772220914
//@@ -9,3 +9,6 @@
// extern int serialreadchar(int, int);
// extern int serialwrite(int, char *, int);
// extern int serialclose(int);
//+
//+extern void serial_lock();
//+extern void serial_unlock();
