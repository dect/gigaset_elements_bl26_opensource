/*
 * (C) Copyright 2000
 * DENX Software Engineering
 * Wolfgang Denk, wd@denx.de
 * All rights reserved.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

extern int errno;

#ifndef MAP_FAILED
#define MAP_FAILED -1
#endif

static	void	copy_file (int, const char *, int);

#define SC14450_LOADER		"sc14450loader.bin"
#define SC14450_12MHZ_LOADER	"sc14450loader12MHz.bin"
#define SC14452_LOADER		"sc14452loader.bin"
#define SC14450_BOOTLOADER	"sc14450boot.bin"
#define SC14452_BOOTLOADER	"sc14452boot.bin"

char	*imagefile = "image.bin";
char	*datafile1 = SC14450_LOADER;
char	*datafile2 = SC14450_BOOTLOADER;
char	*datafile3 = "vmlinuz";


char header[8]={0x70,0x50,0x00,0x00,0x00,0x00,0x10,0x00};


int
main (int argc, char **argv)
{
	int ifd;
	int dfd1;
	int dfd2;
	unsigned int i,pad_length;
	struct stat sbuf;
	int zero = 0;

	if( argc > 1 ) {
		if( !strcmp( argv[1], "450" ) ) {
			/* nothing, default settings are OK */
		} else if( !strcmp( argv[1], "450_12M" ) ) {
			datafile1 = SC14450_12MHZ_LOADER ;
		} else if( !strcmp( argv[1], "452" ) ) {
			datafile1 = SC14452_LOADER ;
			datafile2 = SC14452_BOOTLOADER ;
		} else {
			fprintf( stderr, "Usage: %s [ 450 | 450_12M | 452 ]\n",
								argv[0] ) ;
			exit( EXIT_FAILURE ) ;
		}
	}

	ifd = open(imagefile, O_RDWR|O_CREAT|O_TRUNC, 0666);

	if (ifd < 0) {
		fprintf (stderr, "Can't open %s: %s\n",
			imagefile, strerror(errno));
		exit (EXIT_FAILURE);
	}

	if (write(ifd, header, 8) != 8) {
		fprintf (stderr, "Write error on %s: %s\n",
			imagefile, strerror(errno));
		exit (EXIT_FAILURE);
	}
	copy_file (ifd, datafile1, 0);

	if (fstat(ifd, &sbuf) < 0) {
		fprintf (stderr, "Can't stat %s: %s\n",
			imagefile, strerror(errno));
		exit (EXIT_FAILURE);
	}
	pad_length = 0x1000 - sbuf.st_size;
	for (i=0;i<pad_length;i++) {
		if (write(ifd, (char *)&zero, 1) != 1) {
			fprintf (stderr, "Write error on %s: %s\n",
				imagefile, strerror(errno));
			exit (EXIT_FAILURE);
		}
	}

	copy_file (ifd, datafile2, 0);

	if (fstat(ifd, &sbuf) < 0) {
		fprintf (stderr, "Can't stat %s: %s\n",
			imagefile, strerror(errno));
		exit (EXIT_FAILURE);
	}
	pad_length = 0x20000 - sbuf.st_size;
	for (i=0;i<pad_length;i++) {
		if (write(ifd, (char *)&zero, 1) != 1) {
			fprintf (stderr, "Write error on %s: %s\n",
				imagefile, strerror(errno));
			exit (EXIT_FAILURE);
		}
	}

	copy_file (ifd, datafile3, 0);
	
	if (close(ifd)) {
		fprintf (stderr, "Write error on %s: %s\n",
			 imagefile, strerror(errno));
		exit (EXIT_FAILURE);
	}

	exit (EXIT_SUCCESS);
}
static void
copy_file (int ifd, const char *datafile, int pad)
{
	int dfd;
	struct stat sbuf;
	unsigned char *ptr;
	int tail;
	int zero = 0;

	if ((dfd = open(datafile, O_RDONLY)) < 0) {
		fprintf (stderr, "Can't open %s: %s\n",
			datafile, strerror(errno));
		exit (EXIT_FAILURE);
	}

	if (fstat(dfd, &sbuf) < 0) {
		fprintf (stderr, "Can't stat %s: %s\n",
			datafile, strerror(errno));
		exit (EXIT_FAILURE);
	}

	ptr = (unsigned char *)mmap(0, sbuf.st_size,
				    PROT_READ, MAP_SHARED, dfd, 0);
	if (ptr == MAP_FAILED) {
		fprintf (stderr, "Can't read %s: %s\n",
			datafile, strerror(errno));
		exit (EXIT_FAILURE);
	}

	if (write(ifd, ptr, sbuf.st_size) != sbuf.st_size) {
		fprintf (stderr, "Write error on %s: %s\n",
			imagefile, strerror(errno));
		exit (EXIT_FAILURE);
	}

	if (pad && ((tail = sbuf.st_size % 4) != 0)) {

		if (write(ifd, (char *)&zero, 4-tail) != 4-tail) {
			fprintf (stderr, "Write error on %s: %s\n",
				imagefile, strerror(errno));
			exit (EXIT_FAILURE);
		}
	}

	(void) munmap((void *)ptr, sbuf.st_size);
	(void) close (dfd);
}

