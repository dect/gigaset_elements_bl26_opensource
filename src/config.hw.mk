
ifndef CONFIG_HW_PROCESSED
export CONFIG_HW_PROCESSED=yes
# Path to cross compiler
export PATH :=$(TOP)/../tools/cr16-tools/bin:$(PATH)
endif # CONFIG_HW_PROCESSED

export ARCH = cr16

export CC_PREFIX = cr16-uclinux-
export CC = $(CC_PREFIX)gcc
export CXX = $(CC_PREFIX)g++
export CPP = $(CC_PREFIX)cpp
export LD = $(CC_PREFIX)ld
export AS = $(CC_PREFIX)as
export AR = $(CC_PREFIX)ar
export RANLIB = $(CC_PREFIX)ranlib
export STRIP = $(CC_PREFIX)strip

export CC_BARE = cr16-elf-



# start code for shared library
export SHARED_LIB_START = $(shell $(TOP)/../tools/cr16-tools/bin/$(CC) $(CFLAGS) -print-file-name=Scrt1.o)

# shared library configuration
include $(TOP)/shared.lib.mk

# For dmalloc use
DMALLOC_CFLAGS := -DDMALLOC -DDMALLOC_FUNC_CHECK
DMALLOC_LDLIBS := -L$(TMP_ROOTFS)/lib -ldmallocth
export DMALLOC_CFLAGS
export DMALLOC_LDLIBS
# Enable dmalloc (when=true) where possible or implemented :)
#DMALLOC_ENABLE := true
export DMALLOC_ENABLE


# generic flags
export CFLAGS 	:= -DBUILD_HW -mint32 -mcr16cplus -mdata-model=far -fPIC -O2 -std=c99 -Wall
export LDFLAGS	:= -mint32 -mcr16cplus -mdata-model=far -Wl,-elf2flt="-d" -fPIC


# for static linking compilation
export CFLAGS_STATIC 	:=
export LDFLAGS_STATIC	:= -Wl,-static


# for dynamic linking compilation
# Warning:		Remember that at least lib.c should be included for dynamic linking
#  example: 	-Wl,-R,$(LIBC_GDB)
export CFLAGS_DYNAMIC	:= -mid-shared-library
export LDFLAGS_DYNAMIC	:= -mid-shared-library -Wl,--no-relax



export CONFIG=config.hw.mk
export BUILD = hw


