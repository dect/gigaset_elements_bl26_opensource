#!/bin/bash
TEMPFILE="ca-bundle.pem.downloaded"
OUTFILE="ca-bundle.pem.src"

echo -n > ${OUTFILE}

for i in *.crt ;do
    # Ignore dangling symlinks (if any).
    [ ! -f "$i" ] && continue
    echo "Adding '$i'..."
    cat $i >> ${OUTFILE}
done


if [ "$1" != "n" ]; then
echo "-------------------------------------------------------------------"
echo "!!! Using huge bundle file consumes 4 x file size of RAM memory !!!"
echo "Do you want to download official CA bundle from mozilla [y/n] ? "
echo "-------------------------------------------------------------------"
read letter
if [ "${letter}" == "y" ]; then
echo "Downloading CA bundle..."
wget http://curl.haxx.se/ca/cacert.pem -O ${TEMPFILE}
cat ${TEMPFILE} >> ${OUTFILE}
echo "Finish"
fi
fi

echo

