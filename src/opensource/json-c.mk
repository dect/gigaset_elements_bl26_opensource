



##############################
# Build configuration for HW #
##############################

ifeq ($(BUILD),hw)

LIB_NO := $(LIB_JSON_C_NO)
LIB_STATIC := $(TMP_ROOTFS)/lib/libjson.a
LIB_SHARED := $(TMP_ROOTFS)/lib/lib$(LIB_NO).so


CFLAGS += $(CFLAGS_DYNAMIC)
LDFLAGS += $(LDFLAGS_DYNAMIC) 

LDLIBS = -Wl,-R,$(PTHREAD_GDB)

export CFLAGS
export LDFLAGS


configure:
	cd json-c && \
	autoreconf -fiv && \
	sed -i 's|ac_exec_ext -c|ac_exec_ext -p|' configure && \
	ac_cv_func_malloc_0_nonnull=yes ./configure --host=cr16-uclinux --prefix=$(TMP_ROOTFS) && \
	make clean


install: make
	cd json-c && \
	make install 

shared:	
	$(CC) $(LDFLAGS) -o $(LIB_SHARED) -nostartfiles $(SHARED_LIB_START) -Wl,-shared-lib-id,$(LIB_NO) -Wl,--whole-archive,$(LIB_STATIC)  -Wl,--no-whole-archive $(LDLIBS) 
	$(call install_on_root,$(LIB_SHARED),/lib) 
	cp $(LIB_SHARED).gdb $(GDB_DIR)
	
endif	# HW configuration














##############################
# Build configuration for PC #
##############################

ifeq ($(BUILD),pc)

#CFLAGS += -DREFCOUNT_DEBUG -DMC_MAINTAINER_MODE
export CFLAGS
export LDFLAGS

configure:
	cd json-c && \
	autoreconf -fiv && \
	sed -i 's|ac_exec_ext -c|ac_exec_ext -p|' configure && \
	./configure --prefix=$(TMP_ROOTFS) && \
	make clean
	


install: make
	cd json-c && \
	make install
	
endif	# PC configuration










########################
# Common build targets #
########################


make:
	cd json-c && \
	make -j $(JOBS)	
	

clean:
	cd json-c && \
	if [ -e Makefile ]; then make clean; fi
	

%:
	$(error Target "$@" is not supported)
	
	
.PHONY: %
		 