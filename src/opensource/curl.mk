##############################
# Build configuration for HW #
##############################

ifeq ($(BUILD),hw)
# DMALLOC:
ifeq ($(DMALLOC_ENABLE),true)
CFLAGS += $(DMALLOC_CFLAGS)
LDFLAGS += $(DMALLOC_LDLIBS)
LDFLAGS += -L$(TMP_ROOTFS)/lib
###LDFLAGS += -Wl,-Bstatic -ldmallocth
endif

CFLAGS += $(CFLAGS_DYNAMIC)
LDFLAGS += $(LDFLAGS_DYNAMIC) 

CFLAGS += -DSC14450 -std=gnu99 -D_REENTRANT
LDFLAGS += -Wl,-Map,curl.map -Wl,-R,$(PTHREAD_GDB)
LDFLAGS += -Wl,--verbose



install: 
	cd curl && \
	make -j $(JOBS) && \
	make -j $(JOBS) install
	$(call install_on_nfs,$(TMP_ROOTFS)/bin/curl,/bin)
	$(call install_on_nfs,$(TMP_ROOTFS)/bin/curl-config,/bin)


configure: 
	cd curl && \
	./config_sc1445x.sh $(TMP_ROOTFS)

endif	# HW configuration




##############################
# Build configuration for PC #
##############################

ifeq ($(BUILD),pc)

install: 
	cd curl && \
	make -j $(JOBS) && \
	make -j $(JOBS) install
	$(call install_on_root,$(INIT_ROOTFS)/etc/ssl,/etc)
#	$(call install_on_nfs,$(TMP_ROOTFS)/bin/curl,/bin)
#	$(call install_on_nfs,$(TMP_ROOTFS)/bin/curl-config,/bin)


configure: 
	cd curl && \
	./config_pc.sh $(TMP_ROOTFS)

endif	# PC configuration


clean:
	cd curl && \
	if [ -e Makefile ]; then make clean -j $(JOBS); fi

distclean:
	cd curl && \
	make distclean -j $(JOBS)

rebuild:
	make OPENSOURCE=curl clean && \
	make OPENSOURCE=curl configure && \
	make OPENSOURCE=curl install
	

%:
	$(error Target "$@" is not supported)
	
	
.PHONY: install clean %
		 