#!/bin/bash
#################################################################################################
#
#  Dialog Semiconductor
#  
#  
#  
#
#  Project: VoIP
#  RHEA SOFTWARE PLATFORM
#
#  Version generation script 
#  Version: 10
#  Comments: This script is called at every linux compilation. It checks the .git 
#       configuration files and produces the file ./sc14450_voip_version.h which contains 
#       the following definitions:
#	#define SC14450_VERSION    --> a string with the repository version of the sources
#	#define SC14450_VERSION_DATE --> date of repository commit
#	#define SC14450_VERSION_STATUS --> REPOSITORY VERSION if compiled from repository code
#					   NOT REPOSITORY VERSION otherwise 
#					NOTE: this should be "REPOSITORY VERSION" for releases
#	#define SC14450_VERSION_LOCAL --> empty if compiled sources are inline with repository
#					  "WITH LOCAL CHANGES" if local changes are not committeg
#
#  This file is included in the linux executable and the resulting information can be viewed 
#  in /proc/version 
#  Please contact voip.support@diasemi.com for questions and support
#
#################################################################################################

if test -d ../.git; then
	
	echo ==============================================
	echo Generating version information for SC14450 VoIP Project

#	Get the version numbers by reading the git tags 

	MAJOR=`git tag -l|cut -d_ -f2|cut -d. -f1|sort -n|tail -n 1`
	CUSTOMER=`git tag -l |grep v_$MAJOR. |cut -d. -f2|sort -n|tail -n 1`
	MINOR=`git tag -l |grep v_$MAJOR.$CUSTOMER. |cut -d. -f3|sort -n|tail -1`
	BUILD=`git tag -l |grep v_$MAJOR.$CUSTOMER.$MINOR. |cut -d. -f4|sort -n|tail -1`

#	Compose tag string, tag date, tag git identity and source code git identity

	TAG=v_$MAJOR.$CUSTOMER.$MINOR.$BUILD
	TAG_DATE=`ls -gGQ  --time-style=long-iso  ../.git/refs/tags/$TAG |cut -d \" -f 1|tail -c18`
	TAG_ID=`cat ../.git/refs/heads/master`
	HEAD_ID=`cat ../.git/refs/tags/$TAG`
	echo Last Project Tag: 
	echo $TAG
	echo Tag Date: $TAG_DATE
	echo " "

#	Remove existing version file

	rm -f ./sc14450_voip_version.h

#	Push version into new version file
	echo \#define SC14450_VERSION  \"$TAG\"  >> ./sc14450_voip_version.h

#	Push version into new version file
	echo \#define SC14450_VERSION_DATE  \"$TAG_DATE\" >> ./sc14450_voip_version.h
#	Check and push repository status variable
	if test $TAG_ID = $HEAD_ID ; then
		echo \#define SC14450_VERSION_STATUS  \"REPOSITORY VERSION\"  >> ./sc14450_voip_version.h
	else
		echo \#define SC14450_VERSION_STATUS  \"NOT REPOSITORY VERSION\" >> ./sc14450_voip_version.h
	fi
	
#	Check and push local changes variable
	if test `git status|grep "Changed but not updated"|wc -l` = 0 ; then
		echo \#define SC14450_VERSION_LOCAL \" \" >> ./sc14450_voip_version.h
	else
		if test `git status|grep -v "Changed"|grep ":   "|wc -l` = 1 ; then
			if test `git status|grep "modified:   linux-2.6.x/.config"|wc -l` = 1 ; then
				echo \#define SC14450_VERSION_LOCAL \" \" >> ./sc14450_voip_version.h
			else
				echo \#define SC14450_VERSION_LOCAL \"WITH LOCAL CHANGES\">> ./sc14450_voip_version.h
			fi
		else
		 	echo \#define SC14450_VERSION_LOCAL \"WITH LOCAL CHANGES\">> ./sc14450_voip_version.h
		fi
	fi

else
#	If no .git directory is present define accordingly
	if test `cat ./sc14450_voip_version.h|grep LOCAL_COMPILATION|wc -l` = 0 ; then
		echo \#define SC14450_LOCAL_COMPILATION 1 >> ./sc14450_voip_version.h
	fi
fi

