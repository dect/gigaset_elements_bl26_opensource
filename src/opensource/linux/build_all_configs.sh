#! /bin/bash
configs=`find config_files/ -type f`


orig_config=`mktemp $PWD/.config.XXXXX` &&
		echo "The current .config is copied to $orig_config"  ||  exit 1
cp .config $orig_config

for config in $configs
do
	echo -e "\n\n***** Building with $config *****\n"

	rm -f vmlinuz &&
	cp $config .config &&
	make &&
	echo -e "\n\nDone\n"

	if [ ! -f vmlinuz ]
	then
		echo -e "\nSomething went wrong with $config!"
		echo "The original .config was copied to $orig_config"
		exit 1
	fi
done

echo "Reverting original .config from $orig_config"
mv $orig_config .config

