/*	$OpenBSD: cryptodev.c,v 1.52 2002/06/19 07:22:46 deraadt Exp $	*/

/*-
 * Linux port done by David McCullough <david_mccullough@securecomputing.com>
 * Copyright (C) 2006-2007 David McCullough
 * Copyright (C) 2004-2005 Intel Corporation.
 * The license and original author are listed below.
 *
 * Copyright (c) 2001 Theo de Raadt
 * Copyright (c) 2002-2006 Sam Leffler, Errno Consulting
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *   derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Effort sponsored in part by the Defense Advanced Research Projects
 * Agency (DARPA) and Air Force Research Laboratory, Air Force
 * Materiel Command, USAF, under agreement number F30602-01-2-0537.
 *
__FBSDID("$FreeBSD: src/sys/opencrypto/cryptodev.c,v 1.34 2007/05/09 19:37:02 gnn Exp $");
 */

#ifndef AUTOCONF_INCLUDED
#include <linux/config.h>
#endif
#include <linux/types.h>
#include <linux/time.h>
#include <linux/delay.h>
#include <linux/list.h>
#include <linux/init.h>
#include <linux/unistd.h>
#include <linux/module.h>
#include <linux/wait.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/dcache.h>
#include <linux/file.h>
#include <linux/mount.h>
#include <linux/miscdevice.h>
#include <linux/version.h>
#include <asm/uaccess.h>

#include <asm/cacheflush.h>

#include <cryptodev.h>
#include <linux/proc_fs.h>

#define dprintk(a...)	do { if (debug) printk(a); } while(0)

#define CRYPTO_CTRL_AES    0x0        // Select AES algorithm
#define CRYPTO_CTRL_TDES   0x2        // Select Triple DES algorithm
#define CRYPTO_CTRL_DES    0x3        // Select DES algorithm
#define CRYPTO_CTRL_ECB    (0x0 << 2) // Select ECB mode of operation
#define CRYPTO_CTRL_CTR    (0x2 << 2) // Select CTR mode of operation
#define CRYPTO_CTRL_CBC    (0x3 << 2) // Select CBC mode of operation
#define CRYPTO_CTRL_ALL    (0x0 << 4) // Write back to memory all data results
#define CRYPTO_CTRL_LST    (0x1 << 4) // Write back to memory only the final block of results
#define CRYPTO_CTRL_K128   (0x0 << 5) // 128 AES Key
#define CRYPTO_CTRL_K192   (0x1 << 5) // 192 AES Key
#define CRYPTO_CTRL_K256   (0x2 << 5) // 256 AES Key
#define CRYPTO_CTRL_ENC    (0x1 << 7) // Encryption
#define CRYPTO_CTRL_DEC    (0x0 << 7) // Decryption
#define CRYPTO_CTRL_IRQDIS (0x0 << 8) // Interrupt Request Disabled
#define CRYPTO_CTRL_IRQEN  (0x1 << 8) // Interrupt Request Enabled


#define debug cryptodev_debug

#ifdef CONFIG_CRYPTO_SC14452_DEBUG
int cryptodev_debug = 1;
#else
int cryptodev_debug = 0;
#endif
module_param(cryptodev_debug, int, 0644);
MODULE_PARM_DESC(cryptodev_debug, "Enable cryptodev debug");

/*
 * Holds data specific to a single s452_crypto device.
 */

struct s452_crypto_session {
	u_int32_t       ses_cipher;
	u_int32_t	ses_used;
	u_int32_t	ses_klen;		/* key length in bits */
	u_int32_t	ses_key[8];		/* DES/3DES/AES key */
	u_int32_t       *ses_aes_rkeys;       /* Generated keys for AES */
};

struct s452_crypto_softc {
	int                                              sc_nsessions;
	struct s452_crypto_session	*sc_sessions;
	struct mutex	                           sc_mutexlock;              /* mutual exclusion for hw resources */
	int                                              sc_lastses; 	/* contains last session */
} ;

struct s452_crypto_list {
	u_int32_t sesid;
	struct s452_crypto_list *next;
};

struct s452_crypto_softc sc_state;

#ifdef CONFIG_RAM_SIZE_32MB
#define SIZEOFCRYPTODATA 4096
	volatile unsigned long long *s452_crypto_local_data=NULL;
#endif

static int s452_crypto_list_add (struct file *filp, u_int32_t sid) {
	struct s452_crypto_list *fcr;
	struct s452_crypto_list *temp;
	
	if (filp->private_data == NULL) {
		filp->private_data = kmalloc (sizeof (struct s452_crypto_list),GFP_ATOMIC);
		if (filp->private_data == NULL)
			return ENOMEM;
		fcr = filp->private_data;
		fcr->sesid = sid;
		fcr->next  = NULL;
	} else {
		fcr = filp->private_data;
		temp = fcr->next;
		
		fcr->next = kmalloc (sizeof (struct s452_crypto_list), GFP_ATOMIC);
		if (fcr->next == NULL)
			return ENOMEM;
					
		fcr->next->next  = temp;
		fcr->next->sesid = sid;
	}
	
	return 0;
}

static int s452_crypto_list_remove (struct file *filp, u_int32_t sid) {
	struct s452_crypto_list *temp = filp->private_data;
	struct s452_crypto_list *prev = NULL;
	char find_elm = 0;

	if (filp->private_data == NULL) {
		dprintk("%s,%d: EINVAL\n", __FILE__, __LINE__);
		return EINVAL;
	}
		
	while (temp !=NULL && !find_elm) {
		if (temp->sesid== sid)
			find_elm=1;
		else {
			prev = temp;
			temp =temp->next;
		}
	}
	
	if (!find_elm) {
		dprintk("%s,%d: EINVAL\n", __FILE__, __LINE__);
		return EINVAL;
	} else	if (filp->private_data == temp) {
		filp->private_data = temp->next;
		kfree(temp);
	} else {
		prev->next = temp->next;
		kfree (temp);
	}
	
	return 0;
}

// ********************************************************************
//                          Low Level Functions
// ********************************************************************


extern asmlinkage long sys_dup(unsigned int fildes);

//----------------------------------------------------------------------------
//	                       AES Key Expansion
//----------------------------------------------------------------------------

static const u_char SBox[256] = {0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 
                                 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
                                 0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0,
                                 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
                                 0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 
                                 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
                                 0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 
                                 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
                                 0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 
                                 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
                                 0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 
                                 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
                                 0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 
                                 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
                                 0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 
                                 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
                                 0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 
                                 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
                                 0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 
                                 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
                                 0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 
                                 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
                                 0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 
                                 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
                                 0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 
                                 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
                                 0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 
                                 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
                                 0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 
                                 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
                                 0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 
                                 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16};

                                
__inline__ static u_char s452_mulx2_gf256 (u_char num) {	
	if (num & 0x80)
		return( (num << 1) ^ 0x1b);
	else
		return (num <<1);
}

__inline__ static u_int32_t s452_Rcon (u_char num)
{
	u_char i;
	u_char temp;
	
	temp = 1;
	for (i=0;i < num-1; i = i+1)
		temp = s452_mulx2_gf256(temp);

	return (((u_int32_t) temp) << 24);
}


void static s452_aes_calc_keys (u_char keysize, u_int32_t *AESBaseKey, u_int32_t *AESKeys)
{
	u_int32_t temp;
	u_int32_t sboxed;
	u_char help;
	u_char i,Nr,Nk;

	if (keysize == 16) {         // 128 bits
		Nr = 10;
		Nk = 4;
	} else if (keysize == 24) {  // 192 bits
		Nr = 12;
		Nk = 6;
	} else {                    // 256 bits
		Nr = 14;
		Nk = 8;
	}

	i = 0;
	while (i < Nk) {
		AESKeys[i]  = (AESBaseKey[i] >> 24);
		AESKeys[i] |= (AESBaseKey[i] >>  8) & 0x0000ff00;
		AESKeys[i] |= (AESBaseKey[i] <<  8) & 0x00ff0000;
		AESKeys[i] |= (AESBaseKey[i] << 24);
		i           = i+1;
	}
	i = Nk;

	while (i < 4 * (Nr+1)) {
		temp = AESKeys[i-1];
		
		if ((i%Nk) == 0) {
			help    = (u_char) (temp >> 24);
			sboxed  = (u_int32_t) SBox[help];

			help    = (u_char) temp;
			sboxed |= ((u_int32_t) SBox[help]) << 8;
			
			help    = (u_char) (temp >> 8) ;
			sboxed |= ((u_int32_t) SBox[help]) << 16;
			
			help    = (u_char) (temp >> 16);
			sboxed |= ((u_int32_t) SBox[help]) << 24;
			
			temp = sboxed ^ s452_Rcon(i/Nk);
		} else if ((Nk > 6) & (i%Nk == 4)) {

			help    = (u_char) temp;
			sboxed  = (u_int32_t) SBox[help];
			
			help    = (u_char) (temp >> 8) ;
			sboxed |= ((u_int32_t) SBox[help]) << 8;
			
			help    = (u_char) (temp >> 16);
			sboxed |= ((u_int32_t) SBox[help]) << 16;

			help    = (u_char) (temp >> 24);
			sboxed |= ((u_int32_t) SBox[help]) << 24;
			
			temp = sboxed;
		}

		AESKeys[i] = AESKeys[i-Nk] ^ temp ;
		i          = i + 1;
	}
}

static void s452_aes_set_keys (u_char keysize, u_int32_t *AESKeys) {
	u_char thr;
	u_int32_t *pointer = (u_int32_t *) &CRYPTO_KEYS_START;
	
	if (keysize == 16)
		thr = 11 * 4;
	else if (keysize == 24)
		thr = 13 * 4;
	else
		thr = 15 * 4;
	
	memcpy (pointer, AESKeys, 4*thr);
}

// ********************************************************************

static int
cryptodev_process (struct crypt_op *crp)
{
	struct s452_crypto_session *ses;
	int sid;
	u_int32_t *from;
	u_int32_t ctrl_common;
	
#ifdef CONFIG_RAM_SIZE_32MB
	volatile unsigned long long *large_data_pt;
#endif

	dprintk("%s()\n", __FUNCTION__);

	if (crp == NULL) {
		return EINVAL;
	}
	
	sid = crp->ses;
	
	if (sid >= sc_state.sc_nsessions) {
		dprintk("%s,%d: ENOENT\n", __FILE__, __LINE__);
		return ENOENT;
	}

	ses = &sc_state.sc_sessions[sid];

	if (ses->ses_used != 1) {
		dprintk("%s,%d: ENOENT\n", __FILE__, __LINE__);
		return ENOENT;
	}

	if ( crp->len > 0xfffff) {
		printk("cryptodev : Maximum size of data is equal to 0xfffff. The actual size is bigger than the maximum (%x)\n",  crp->len);
		return EINVAL;
	}
	
	mutex_lock(&sc_state.sc_mutexlock);
	
	// Enable Crypto Clock
	CLK_AUX2_REG |= SW_CRYPTO_EN;
	
	// Set Key
	if (sid != sc_state.sc_lastses) {
	
		if (ses->ses_klen == 24 ) 
			ctrl_common =  CRYPTO_CTRL_K192;
		else if (ses->ses_klen == 32 )
			ctrl_common =  CRYPTO_CTRL_K256;
		else 
			ctrl_common =  CRYPTO_CTRL_K128;
			
		switch (ses->ses_cipher)  {
			case CRYPTO_DES_ECB:
				CRYPTO_MREG2_REG = ses->ses_key[0];
				CRYPTO_MREG3_REG = ses->ses_key[1];
				CRYPTO_CTRL_REG = CRYPTO_CTRL_DES | CRYPTO_CTRL_ECB ;
				break;
			case CRYPTO_DES_CBC:
				CRYPTO_MREG2_REG = ses->ses_key[0];
				CRYPTO_MREG3_REG = ses->ses_key[1];
				CRYPTO_CTRL_REG = CRYPTO_CTRL_DES | CRYPTO_CTRL_CBC ;
				break;
			case CRYPTO_3DES_ECB:
				CRYPTO_MREG2_REG = ses->ses_key[4];
				CRYPTO_MREG3_REG = ses->ses_key[5];
				CRYPTO_MREG4_REG = ses->ses_key[2];
				CRYPTO_MREG5_REG = ses->ses_key[3];
				CRYPTO_MREG6_REG = ses->ses_key[0];
				CRYPTO_MREG7_REG = ses->ses_key[1];
				CRYPTO_CTRL_REG = CRYPTO_CTRL_TDES | CRYPTO_CTRL_ECB  ;
				break;
			case CRYPTO_3DES_CBC:
				CRYPTO_MREG2_REG = ses->ses_key[4];
				CRYPTO_MREG3_REG = ses->ses_key[5];
				CRYPTO_MREG4_REG = ses->ses_key[2];
				CRYPTO_MREG5_REG = ses->ses_key[3];
				CRYPTO_MREG6_REG = ses->ses_key[0];
				CRYPTO_MREG7_REG = ses->ses_key[1];
				CRYPTO_CTRL_REG = CRYPTO_CTRL_TDES | CRYPTO_CTRL_CBC  ;
				break;
			case CRYPTO_AES_ECB:
				s452_aes_set_keys (ses->ses_klen, ses->ses_aes_rkeys);

				CRYPTO_CTRL_REG = ctrl_common | CRYPTO_CTRL_AES | CRYPTO_CTRL_ECB ;
				
				break;
			case CRYPTO_AES_CBC:
				s452_aes_set_keys (ses->ses_klen, ses->ses_aes_rkeys);
			
				CRYPTO_CTRL_REG =  ctrl_common | CRYPTO_CTRL_AES | CRYPTO_CTRL_CBC ;
				
				break;
			case CRYPTO_AES_CTR:			
				s452_aes_set_keys (ses->ses_klen, ses->ses_aes_rkeys);
	
				CRYPTO_CTRL_REG = ctrl_common | CRYPTO_CTRL_AES | CRYPTO_CTRL_CTR ;
				
				break;
			default:
				printk("cryptodev : unimplemented ses->ses_cipher %d\n", ses->ses_cipher);
				mutex_unlock(&sc_state.sc_mutexlock);
				return EINVAL;
		}
	}
	
	sc_state.sc_lastses = sid;
	
	// Set mode configuration
	switch (ses->ses_cipher)  {
		case CRYPTO_DES_ECB:
		case CRYPTO_3DES_ECB:
		case CRYPTO_AES_ECB:
			break;
		case CRYPTO_DES_CBC:
		case CRYPTO_3DES_CBC:
			/* get new IV */
			from = (u_int32_t *) crp->iv;
			CRYPTO_MREG1_REG = (*from << 24) | ((*from <<8) & 0x00ff0000) | ( (*from >>8) & 0x0000ff00)  | (*from >> 24);
			from++;
			CRYPTO_MREG0_REG =  (*from << 24) | ( (*from <<8) & 0x00ff0000) | ( (*from >>8) & 0x0000ff00)  | (*from >> 24);
			break;
		case CRYPTO_AES_CBC:
		case CRYPTO_AES_CTR:
			/* get new IV/CTR */
			from = (u_int32_t *) crp->iv;
			CRYPTO_MREG3_REG = (*from << 24) | ((*from <<8) & 0x00ff0000) | ( (*from >>8) & 0x0000ff00)  | (*from >> 24) ;
			from++;
			CRYPTO_MREG2_REG = (*from << 24) | ( (*from <<8) & 0x00ff0000) | ( (*from >>8) & 0x0000ff00)  | (*from >> 24) ;
			from++;
			CRYPTO_MREG1_REG = (*from << 24) | ( (*from <<8) & 0x00ff0000) | ( (*from >>8) & 0x0000ff00)  | (*from >> 24) ;
			from++;
			CRYPTO_MREG0_REG = (*from << 24) | ( (*from <<8) & 0x00ff0000) | ( (*from >>8) & 0x0000ff00)  | (*from >> 24) ;
			break;
		default:
			printk("cryptodev: unimplemented ses->ses_cipher %d\n", ses->ses_cipher);
			mutex_unlock(&sc_state.sc_mutexlock);
			return EINVAL;
	}

	// Encrypt or Decrypt ? That is the question ...
	if (crp->op & COP_ENCRYPT) 
		CRYPTO_CTRL_REG |= CRYPTO_CTRL_ENC;
	else
		CRYPTO_CTRL_REG &= ~CRYPTO_CTRL_ENC;
		
	if (crp->flags & COP_F_LSTBLOCK)
		CRYPTO_CTRL_REG |= CRYPTO_CTRL_LST;
	else
		CRYPTO_CTRL_REG &= ~CRYPTO_CTRL_LST;
	
#ifdef CONFIG_RAM_SIZE_32MB


	if ((((u_int32_t) crp->src)>=0x1000000)||\
		(((u_int32_t) crp->dst)>=0x1000000)||\
		(((u_int32_t) crp->src+crp->len)>=0x1000000)||\
		(((u_int32_t) crp->dst+crp->len)>=0x1000000))

	{
		if (crp->len<SIZEOFCRYPTODATA)
		{
			copy_from_user((void*)s452_crypto_local_data, (void*)crp->src, crp->len );

			CRYPTO_FETCH_ADDR_REG = (u_int32_t)s452_crypto_local_data;
			CRYPTO_LEN_REG =  crp->len;
			CRYPTO_DEST_ADDR_REG = (u_int32_t)s452_crypto_local_data;
			//start calculate....
			CRYPTO_START_REG = 1;
			// wait end of calculation
			while (CRYPTO_STATUS_REG!=0x1);
			// Disable crypto clock
			CLK_AUX2_REG &= ~SW_CRYPTO_EN;

			// cache invalidate
			flush_dcache_range((u_int32_t) s452_crypto_local_data, ((u_int32_t)s452_crypto_local_data) + crp->len);
			copy_to_user(crp->dst, (void*)s452_crypto_local_data, crp->len );
			mutex_unlock(&sc_state.sc_mutexlock);
			return 0;

		}
		else
		{
			//TODO break it down in multiple calls using s452_crypto_local_data
			//in this case fix the IV and CNT values.
			//printk("\nCR CAL2 SM CP\n");
			large_data_pt=kmalloc (crp->len,GFP_KERNEL|GFP_DMA);
			if (large_data_pt==NULL)
			{
				printk("cryptodev : Unable to allocate memory in GFP_DMA space (%x)\n",  crp->len);
				mutex_unlock(&sc_state.sc_mutexlock);
				return EINVAL;
			}
			copy_from_user((void*)large_data_pt, crp->src,crp->len);
			CRYPTO_FETCH_ADDR_REG = (u_int32_t)large_data_pt;
			CRYPTO_LEN_REG =  crp->len;
			CRYPTO_DEST_ADDR_REG = (u_int32_t)large_data_pt;
			//start calculate....
			CRYPTO_START_REG = 1;
			// wait end of calculation
			while (CRYPTO_STATUS_REG!=0x1);
			// Disable crypto clock
			CLK_AUX2_REG &= ~SW_CRYPTO_EN;

			// cache invalidate
			flush_dcache_range((u_int32_t) large_data_pt, ((u_int32_t)large_data_pt) + crp->len);

			copy_to_user(crp->dst,(void*)large_data_pt,crp->len);

			kfree((void*)large_data_pt);
			mutex_unlock(&sc_state.sc_mutexlock);
			return 0;
		}

	}
	else
	{
		// program dma
		CRYPTO_FETCH_ADDR_REG = (u_int32_t) crp->src;
		CRYPTO_LEN_REG =  crp->len;
		CRYPTO_DEST_ADDR_REG = (u_int32_t) crp->dst;

		//start calculate....
		CRYPTO_START_REG = 1;

		// wait end of calculation
		while (CRYPTO_STATUS_REG!=0x1);
		// Disable crypto clock
		CLK_AUX2_REG &= ~SW_CRYPTO_EN;

		mutex_unlock(&sc_state.sc_mutexlock);

		// cache invalidate
		flush_dcache_range((u_int32_t) crp->dst, ((u_int32_t)crp->dst) + crp->len);

	}

#else	 // CONFIG_RAM_SIZE_32MB
	// program dma
	CRYPTO_FETCH_ADDR_REG = (u_int32_t) crp->src;
	CRYPTO_LEN_REG =  crp->len;
	CRYPTO_DEST_ADDR_REG = (u_int32_t) crp->dst;

	//start calculate....
	CRYPTO_START_REG = 1;

	// wait end of calculation
	while (CRYPTO_STATUS_REG!=0x1);

	// Disable crypto clock
	CLK_AUX2_REG &= ~SW_CRYPTO_EN;

	mutex_unlock(&sc_state.sc_mutexlock);

	// cache invalidate
	flush_dcache_range((u_int32_t) crp->dst, ((u_int32_t)crp->dst) + crp->len);
#endif // CONFIG_RAM_SIZE_32MB
	return 0;
}

/*
 * Generate a new software session.
 */
 
static int
cryptodev_newses(struct session_op *sop)
{
	struct s452_crypto_session *ses = NULL;
	int sesn;
	int i;
	char *from,*to;

	/* validate algorithm and key length */
	switch (sop->cipher)  {
		case CRYPTO_DES_ECB:
		case CRYPTO_DES_CBC:
			if (sop->keylen != 8)
				return EINVAL;
			break;
		case CRYPTO_3DES_ECB:
		case CRYPTO_3DES_CBC:
			if (sop->keylen != 24) {
				return EINVAL;
			}
			break;
		case CRYPTO_AES_ECB:
		case CRYPTO_AES_CBC:
		case CRYPTO_AES_CTR:
			if (sop->keylen != 16 &&
			    sop->keylen != 24 &&
			    sop->keylen != 32)
				return EINVAL;
			break;
		default:
			dprintk("cryptodev : UNKNOWN sop->cipher %d\n", 
				sop->cipher);
			return EINVAL;
	}

	if (sc_state.sc_sessions == NULL) {
		ses = sc_state.sc_sessions = (struct s452_crypto_session *)
			kmalloc(sizeof(struct s452_crypto_session), SLAB_ATOMIC);
		if (ses == NULL)
			return ENOMEM;
		memset(ses, 0, sizeof(struct s452_crypto_session));
		sesn = 0;
		sc_state.sc_nsessions = 1;
	} else {
		for (sesn = 0; sesn < sc_state.sc_nsessions; sesn++) {
			if (sc_state.sc_sessions[sesn].ses_used == 0) {
				ses = &sc_state.sc_sessions[sesn];
				break;
			}
		}

		if (ses == NULL) {
			/* allocating session */
			sesn = sc_state.sc_nsessions;
			ses = (struct s452_crypto_session *) kmalloc(
				(sesn + 1) * sizeof(struct s452_crypto_session), 
				SLAB_ATOMIC);
			if (ses == NULL)
				return ENOMEM;
			memset(ses, 0,
				(sesn + 1) * sizeof(struct s452_crypto_session));
			memcpy(ses, sc_state.sc_sessions, 
				sesn * sizeof(struct s452_crypto_session));
			memset(sc_state.sc_sessions, 0,
				sesn * sizeof(struct s452_crypto_session));
			kfree(sc_state.sc_sessions);
			sc_state.sc_sessions = ses;
			ses = &sc_state.sc_sessions[sesn];
			sc_state.sc_nsessions++;
		}
	}

	ses->ses_used = 1;

	/* assign session ID */
	sop->ses = sesn;

	ses->ses_cipher = sop->cipher; 

	/* get Key */
	ses->ses_klen = sop->keylen;

	if ((sop->cipher == CRYPTO_AES_ECB) ||
	    (sop->cipher == CRYPTO_AES_CBC) || 
	    (sop->cipher == CRYPTO_AES_CTR)) {
	
		memcpy(ses->ses_key, sop->key, ses->ses_klen);
	
		if (ses->ses_klen == 16) 
			ses->ses_aes_rkeys = (u_int32_t *) kmalloc (11*4*sizeof (u_int32_t),GFP_ATOMIC);
		else if (ses->ses_klen == 24) 
			ses->ses_aes_rkeys = (u_int32_t *) kmalloc (13*4*sizeof (u_int32_t),GFP_ATOMIC);
		else 
			ses->ses_aes_rkeys = (u_int32_t *) kmalloc (15*4*sizeof (u_int32_t),GFP_ATOMIC);
	
		if (ses->ses_aes_rkeys  == NULL)
			return ENOMEM;
	
		s452_aes_calc_keys (ses->ses_klen,ses->ses_key, ses->ses_aes_rkeys);
	} else {
		from = (char *) sop->key;
		to = ((char *) ses->ses_key) + (ses->ses_klen-1);

		for (i=0;i<ses->ses_klen; i++)
			*(to--)=*(from++);
	}
	
	return 0;
}

/*
 * Deallocate a session.
 */
static int
cryptodev_freeses(u_int32_t sid)
{
	int session, ret;
	struct s452_crypto_session *ses = NULL;

	mutex_lock(&sc_state.sc_mutexlock);
	
	if (sid == sc_state.sc_lastses)
		sc_state.sc_lastses = -1;
	
	mutex_unlock(&sc_state.sc_mutexlock);

	session = sid;
	
	ses = &sc_state.sc_sessions[session];

	if (ses->ses_aes_rkeys!=NULL)
		kfree (ses->ses_aes_rkeys);
	
	if (session < sc_state.sc_nsessions) {
		memset(&sc_state.sc_sessions[session], 0,
			sizeof(sc_state.sc_sessions[session]));
		ret = 0;
	} else
		ret = EINVAL;
	return ret;
}

static int
cryptodev_ioctl(
	struct inode *inode,
	struct file *filp,
	unsigned int cmd,
	unsigned long arg)
{
	struct session_op *sop;
	struct crypt_op *cop;
	u_int32_t *sid;
	int feat,fd, error = 0;
	mm_segment_t fs;

	dprintk("%s(cmd=%x arg=%lx)\n", __FUNCTION__, cmd, arg);

	switch (cmd) {

	case CRIOGET: {
		dprintk("%s(CRIOGET)\n", __FUNCTION__);
		fs = get_fs();
		set_fs(get_ds());
		for (fd = 0; fd < files_fdtable(current->files)->max_fds; fd++)
			if (files_fdtable(current->files)->fd[fd] == filp)
				break;
		fd = sys_dup(fd);
		set_fs(fs);
		put_user(fd, (int *) arg);
		return IS_ERR_VALUE(fd) ? fd : 0;
		}

	case CIOCGSESSION:
		dprintk("%s(CIOCGSESSION)\n", __FUNCTION__);
		
		sop = (struct session_op *) arg;

		switch (sop->mac) {
		case 0:
			dprintk("%s(CIOCGSESSION) - no mac\n", __FUNCTION__);
			break;
		default:
			dprintk("%s(CIOCGSESSION) - mac not supported\n", __FUNCTION__);
			error = EINVAL;
			return(-error);
		}

		if (cryptodev_newses(sop)!=0) {
			dprintk("%s(CIOCGSESSION) - newsession %d\n",__FUNCTION__,error);
			error = EINVAL;
		} else if (s452_crypto_list_add (filp,sop->ses) !=0)  {
			dprintk("%s(CIOCGSESSION) - newsession %d\n",__FUNCTION__,error);
			error = EINVAL;
		}
		break;
	
	case CIOCFSESSION:
		dprintk("%s(CIOCFSESSION)\n", __FUNCTION__);
		sid = (uint32_t*)arg;
		error = cryptodev_freeses(*sid);
		if (!error)
			error = s452_crypto_list_remove (filp,*sid);
		break;
	
	case CIOCCRYPT:
		dprintk("%s(CIOCCRYPT)\n", __FUNCTION__);
		cop =(struct crypt_op *) arg;
		error = cryptodev_process(cop);
		break;
	
	case CIOCASYMFEAT:
		dprintk("%s(CIOCASYMFEAT)\n", __FUNCTION__);
		/*
		 * user asym crypto operations are
		 * not permitted return "no algorithms"
		 * so well-behaved applications will just
		 * fallback to doing them in software.
		 */
		feat=0;
		error = copy_to_user((void*)arg, &feat, sizeof(feat));
		break;
	
	default:
		dprintk("%s(unknown ioctl 0x%x)\n", __FUNCTION__, cmd);
		error = EINVAL;
		break;
	}

	return(-error);
}

static int
cryptodev_open(struct inode *inode, struct file *filp)
{
	dprintk("%s()\n", __FUNCTION__);
	if (filp->private_data) {
		printk("cryptodev: Private data already exists !\n");
		return(0);
	}

	filp->private_data = NULL;
	return(0);
}

static int
cryptodev_release(struct inode *inode, struct file *filp)
{
	struct s452_crypto_list *temp;
	struct s452_crypto_list *temp_next;
	
	if (!filp) {
		printk("cryptodev: No private data on release\n");
		return(0);
	}
	
	temp = filp->private_data;

	while (temp!=NULL) {
		cryptodev_freeses (temp->sesid);
		temp_next = temp->next;
		kfree(temp);
		temp=temp_next;
	}
	
	filp->private_data = NULL;
	return(0);
}

static struct file_operations cryptodev_fops = {
	.owner = THIS_MODULE,
	.open = cryptodev_open,
	.release = cryptodev_release,
	.ioctl = cryptodev_ioctl
};

static struct miscdevice cryptodev = {
	.minor = CRYPTODEV_MINOR,
	.name = "crypto",
	.fops = &cryptodev_fops,
};

static int __init
cryptodev_init(void)
{
	int rc;

	printk("cryptodev : SC14452 HW Crypto engine  des(ecb/cbc) 3des(ecb/cbc) aes(ecb/cbc/ctr)\n");
	dprintk("cryptodev : debug enabled\n");

	rc = misc_register(&cryptodev);
	if (rc) {
		printk(KERN_ERR "cryptodev: registration of /dev/crypto failed\n");
		return(rc);
	}
	#ifdef CONFIG_RAM_SIZE_32MB
		s452_crypto_local_data=kmalloc (SIZEOFCRYPTODATA,GFP_KERNEL|GFP_DMA);
	#endif
	memset(&sc_state, 0, sizeof(sc_state));

	sc_state.sc_sessions = NULL;
	sc_state.sc_nsessions =0;
	sc_state.sc_lastses = -1;
	
	mutex_init(&sc_state.sc_mutexlock);

	return(0);
}

static void __exit
cryptodev_exit(void)
{
	int i;
	dprintk("%s()\n", __FUNCTION__);
	misc_deregister(&cryptodev);
	#ifdef CONFIG_RAM_SIZE_32MB
	if (s452_crypto_local_data!=NULL)
		kfree((void*)s452_crypto_local_data);
	#endif
	if (sc_state.sc_sessions !=NULL) {
		for (i=0;i<sc_state.sc_nsessions; i++) 
			if (sc_state.sc_sessions[i].ses_aes_rkeys!=NULL)
				kfree (sc_state.sc_sessions[i].ses_aes_rkeys);		

		kfree(sc_state.sc_sessions);	
	}
}

module_init(cryptodev_init);
module_exit(cryptodev_exit);

MODULE_LICENSE("BSD");
MODULE_AUTHOR("David McCullough <david_mccullough@securecomputing.com>");
MODULE_DESCRIPTION("Cryptodev (user interface to OCF)");
