#ifndef _ASM_CR16_CONFIG_H
#define _ASM_CR16_CONFIG_H

#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT )
	#define ATA_DEV_FXS		0 //1 line
	#define ATA_DEV_FXS_FXO	1 //2 lines
	#define ATA_DEV_LINES(X) ((ATA_DEV_CNT>X)?((ATA_DEVS>>X & 1)+1):0) //Returns number of lines of ATA dev X (X counting from 0)
//1 ATA device	
	#if (defined(CONFIG_ATA_1_FXS_NO_FXO) || defined(CONFIG_ATA_1_FXS_NO_FXO_1_CVM))
		#define ATA_DEV_CNT 1
		#define ATA_DEVS	(ATA_DEV_FXS << 0)
		#define ATA_LINES	1
	#elif 	(defined(CONFIG_ATA_1_FXS_1_FXO))
		#define ATA_DEV_CNT 1		
		#define ATA_DEVS	(ATA_DEV_FXS_FXO << 0)
		#define ATA_LINES	2
//2 ATA devices
	#elif 	(defined(CONFIG_ATA_2_FXS_NO_FXO) || defined(CONFIG_ATA_2_FXS_NO_FXO_1_CVM))
		#define ATA_DEV_CNT 2
		#define ATA_DEVS	((ATA_DEV_FXS << 1) | (ATA_DEV_FXS << 0)) 
		#define ATA_LINES	2
	#elif 	(defined(CONFIG_ATA_2_FXS_1_FXO))
		#define ATA_DEV_CNT 2	
		#define ATA_DEVS	((ATA_DEV_FXS << 1) | (ATA_DEV_FXS_FXO << 0)) 	
		#define ATA_LINES	3
	#elif 	(defined(CONFIG_ATA_2_FXS_2_FXO))
		#define ATA_DEV_CNT 2	
		#define ATA_DEVS	((ATA_DEV_FXS_FXO << 1) | (ATA_DEV_FXS_FXO << 0)) 			
		#define ATA_LINES	4
//3 ATA devices
	#elif 	(defined(CONFIG_ATA_3_FXS_NO_FXO))
		#define ATA_DEV_CNT 3	
		#define ATA_DEVS	((ATA_DEV_FXS << 2) | (ATA_DEV_FXS << 1) | (ATA_DEV_FXS << 0)) 						
		#define ATA_LINES	3
	#elif 	(defined(CONFIG_ATA_3_FXS_1_FXO))	
		#define ATA_DEV_CNT 3
		#define ATA_DEVS	((ATA_DEV_FXS << 2) | (ATA_DEV_FXS << 1) | (ATA_DEV_FXS_FXO << 0)) 			
		#define ATA_LINES	4
//4 ATA devices
	#elif 	(defined(CONFIG_ATA_4_FXS_NO_FXO) || defined(CONFIG_ATA_NO_FXS_4_FXO))
		#define ATA_DEV_CNT 4
		#define ATA_DEVS	((ATA_DEV_FXS << 3) | (ATA_DEV_FXS << 2) | (ATA_DEV_FXS << 1) | (ATA_DEV_FXS << 0)) 		
		#define ATA_LINES	4
	#else	
		#define ATA_DEV_CNT 0
		#define ATA_LINES	0
	#endif
#else	
	#define ATA_DEV_CNT 0
	#define ATA_LINES	0
#endif

#endif //_ASM_CR16_CONFIG_H
