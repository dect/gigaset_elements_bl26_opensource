#ifndef _CR16_SYSTEM_H
#define _CR16_SYSTEM_H

#include <linux/linkage.h>
#include <asm/string.h>

/*
 * switch_to(n) should switch tasks to task ptr, first checking that
 * ptr isn't the current task, in which case it does nothing.  This
 * also clears the TS-flag if the task we switched to has used the
 * math co-processor latest.
 */
/*
 * switch_to() saves the extra registers, that are not saved
 * automatically by SAVE_SWITCH_STACK in resume(), ie. d0-d5 and
 * a0-a1. Some of these are used by schedule() and its predecessors
 * and so we might get see unexpected behaviors when a task returns
 * with unexpected register values.
 *
 * syscall stores these registers itself and none of them are used
 * by syscall after the function in the syscall has been called.
 *
 * Beware that resume now expects *next to be in d1 and the offset of
 * tss to be in a1. This saves a few instructions as we no longer have
 * to push them onto the stack and read them back right after.
 *
 * 02/17/96 - Jes Sorensen (jds@kom.auc.dk)
 *
 * Changed 96/09/19 by Andreas Schwab
 * pass prev in a0, next in a1, offset of tss in d1, and whether
 * the mm structures are shared in d2 (to avoid atc flushing).
 *
 * H8/300 Porting 2002/09/04 Yoshinori Sato
 * CR16 Porting 2007 Lee Jones
 */

asmlinkage void resume(void);
#if __DBL_MAX_EXP__ == 1024
/* if we have 64-bit doubles, i.e. proper 64-bit support */
#define switch_to(prev,next,last) \
{                                                           \
  void *_last;						    \
  __asm__ __volatile__(					    \
  			"movd	%1, (r1,r0)\n\t"	    \
			"movd	%2, (r3,r2)\n\t"	    \
                        "loadd   %3, (r5,r4)\n\t"           \
			"bal   (ra), _resume\n\t"           \
                        "movd  (r1,r0), %0\n\t"             \
		       : "=r" (_last)			    \
		       : "r" (&(prev->thread)),		    \
			 "r" (&(next->thread)),		    \
                         "g" (prev)                         \
		       : "cc", "r0", "r1", "r2", "r3", "r4", "r5"); \
  (last) = _last; 					    \
}
#else
#define switch_to(prev,next,last) \
{                                                           \
  void *_last;						    \
  __asm__ __volatile__(					    \
  			"movd	%1, (r1,r0)\n\t"	    \
			"movd	%2, (r3,r2)\n\t"	    \
                        "movd   %3, (r5,r4)\n\t"            \
			"bal   (ra), _resume\n\t"           \
                        "movd  (r1,r0), %0\n\t"             \
		       : "=r" (_last)			    \
		       : "r" (&(prev->thread)),		    \
			 "r" (&(next->thread)),		    \
                         "g" (prev)                         \
		       : "cc", "r0", "r1", "r2", "r3", "r4", "r5"); \
  (last) = _last; 					    \
}
#endif


#if 0
#define __sti() \
  asm volatile ("spr psr, r0\n\t"   \
                "tbit $9, r0\n\t"   \
                "bfs 1f\n\t"        \
                "ei \n\t"           \
                "nop \n\t"          \
                "1:")

#define __cli() \
  asm volatile ("spr psr, r0\n\t"   \
                "tbit $9, r0\n\t"   \
                "bfc 1f\n\t"        \
                "di \n\t"           \
                "nop \n\t"          \
                "1:")
#else
#define __sti() \
  asm volatile ("ei \n\t"	\
                "nop \n\t")

#define __cli() \
  asm volatile ("di \n\t"	\
                "nop \n\t")

#endif

#define __save_flags(x) asm volatile ("sprd psr,%0":"=r" (x))

#define __restore_flags(x) asm volatile ("lprd %0, psr":: "r" (x))

#define	irqs_disabled()	\
({					\
	unsigned long flags;		\
	__save_flags(flags);	        \
	((flags & 0x200) != 0x200);	\
})

#define iret() __asm__ __volatile__ ("retx": : :"memory", "sp")

/* For spinlocks etc */
#define local_irq_disable()	__cli()
#define local_irq_enable()      __sti()
#define local_irq_save(x)  ({ __save_flags(x); local_irq_disable(); })
#define local_irq_restore(x) __restore_flags(x)
#define local_save_flags(x)     __save_flags(x)

/*
 * Force strict CPU ordering.
 * Not really required on H8...
 */
#define nop()  asm volatile ("nop"::)
#define mb()   asm volatile (""   : : :"memory")
#define rmb()  asm volatile (""   : : :"memory")
#define wmb()  asm volatile (""   : : :"memory")
#define set_rmb(var, value)    do { xchg(&var, value); } while (0)
#define set_mb(var, value)     set_rmb(var, value)

#ifdef CONFIG_SMP
#define smp_mb()	mb()
#define smp_rmb()	rmb()
#define smp_wmb()	wmb()
#define smp_read_barrier_depends()	read_barrier_depends()
#else
#define smp_mb()	barrier()
#define smp_rmb()	barrier()
#define smp_wmb()	barrier()
#define smp_read_barrier_depends()	do { } while(0)
#endif

#define xchg(ptr,x) ((__typeof__(*(ptr)))__xchg((unsigned long)(x),(ptr),sizeof(*(ptr))))
#define tas(ptr) (xchg((ptr),1))

struct __xchg_dummy { unsigned long a[100]; };
#define __xg(x) ((volatile struct __xchg_dummy *)(x))

static inline unsigned long __xchg(unsigned long x, volatile void * ptr, int size)
{
  unsigned long tmp,flags; 


  local_irq_save(flags);

  if((size == 1) || (size == 2) || (size == 4)) 
    {
      memcpy(&tmp, (void *)__xg(ptr), size);
      memcpy((void *)__xg(ptr), &x, size);
    } 
  else
    tmp = 0;

  local_irq_restore(flags);
  return tmp;
}

#define HARD_RESET_NOW() ({		\
        local_irq_disable();		\
        asm("jump 0");			\
})

#define arch_align_stack(x) (x)

#endif /* _CR16_SYSTEM_H */
