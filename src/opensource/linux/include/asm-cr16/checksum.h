#ifndef _CR16_CHECKSUM_H
#define _CR16_CHECKSUM_H

/* 
 * uClinux CR16 support - Lee Jones 2007
 *
 * Based on the H8300
 *
 */

/*
 * computes the checksum of a memory block at buff, length len,
 * and adds in "sum" (32-bit) 
 *
 * returns a 32-bit number suitable for feeding into itself
 * or csum_tcpudp_magic
 *
 * this function must be called with even lengths, except
 * for the last fragment, which may be odd
 *
 * it's best to have buff aligned on a 32-bit boundary
 */
unsigned int csum_partial(const unsigned char * buff, int len, unsigned int sum);

/*
 * the same as csum_partial, but copies from src while it
 * checksums
 *
 * here even more important to align src and dst on a 32-bit (or even
 * better 64-bit) boundary
 */

unsigned int csum_partial_copy(const char *src, char *dst, int len, int sum);


/*
 * the same as csum_partial_copy, but copies from user space.
 *
 * here even more important to align src and dst on a 32-bit (or even
 * better 64-bit) boundary
 */

extern unsigned int csum_partial_copy_from_user(const char *src, char *dst,
						int len, int sum, int *csum_err);

#define csum_partial_copy_nocheck(src, dst, len, sum)	\
	csum_partial_copy((src), (dst), (len), (sum))

unsigned short ip_fast_csum(unsigned char *iph, unsigned int ihl);


/*
 *	Fold a partial checksum
 */
static inline unsigned int csum_fold(unsigned int sum)
//static unsigned int csum_fold(unsigned int sum)
{

  register long __r10 __asm__ ("r10"); 

  __asm__("movd %0, (r11, r10)\n\t"
	  "addw r11 ,r10\n\t"
	  "addcw $0, r10\n\t"
	  "subw r11, r11\n\t"
	  "movd (r11, r10), %0"
	  : "=r"(sum)
	  : "0"(sum), "r" (__r10)
	  );
#if 0
  __asm__("movd %0, (r11, r10)\n\t"
	  "addw r11 ,r10\n\t"
	  "subw r11, r11\n\t"
	  "addcw $0, r11\n\t"
	  "addw r11, r10\n\t"
	  "subw r11, r11\n\t"
	  "movd (r11, r10), %0"
	  : "=r"(sum)
	  : "0"(sum), "r" (__r10)
	  );
#endif
  return ~sum;

}

/*
 * computes the checksum of the TCP/UDP pseudo-header
 * returns a 16-bit checksum, already complemented
 */
static inline unsigned int
//static  unsigned int
csum_tcpudp_nofold(unsigned long saddr, unsigned long daddr, unsigned short len,
		  unsigned short proto, unsigned int sum)
{
	register short __r10 __asm__ ("r10");
	register short __r11 __asm__ ("r11");
	
	__asm__ ("subd   (r11, r10), (r11, r10)\n\t" /* Clear r10 and r11                 */
		 "addd   %2, %0\n\t"                 /* Add daddr to sum                  */
		 "addcw  $0, r10\n\t"                /* Stick the carry in reg   <---     */
		 "addd   %3, %0\n\t"                 /* Add saddr to sum             |    */
		 "addcw  $0, r10\n\t"                /* Add carry to inital carry ---     */
		 "addd   %4, %0\n\t"                 /* Add (len + proto) to sum     |    */
		 "addcw  $0, r10\n\t"                /* Add carry to inital carry ---     */
		 "addd   (r11, r10), %0\n\t"         /* Add carrys to sum                 */
		 "bcc	 1f\n\t"                     /* Branch if carry clear             */
		 "addd   $1, %0\n"                   /* Add the final carry - If set      */
		 "1:"
		 : "=&r" (sum)
		 : "0" (sum), "r" (daddr), "r" (saddr), "r" (ntohl(len + proto)), "r" (__r10), "r" (__r11)
		 );
	
	return sum;
}

static inline unsigned short int
csum_tcpudp_magic(unsigned long saddr, unsigned long daddr, unsigned short len,
		  unsigned short proto, unsigned int sum)
{
	return csum_fold(csum_tcpudp_nofold(saddr,daddr,len,proto,sum));
}

/*
 * this routine is used for miscellaneous IP-like checksums, mainly
 * in icmp.c
 */

extern unsigned short ip_compute_csum(const unsigned char * buff, int len);

#endif /* _CR16_CHECKSUM_H */
