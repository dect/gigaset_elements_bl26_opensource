/*
 *  linux/include/asm-cr16/traps.h
 *
 *  Copyright (C) 2003 Yoshinori Sato <ysato@users.sourceforge.jp>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive
 * for more details.
 */

#ifndef _CR16_TRAPS_H
#define _CR16_TRAPS_H

extern void system_call(void);
extern void interrupt_entry(void);
extern void trace_break(void);

#define JMP_OP 0xe0000000
#define JSR_OP 0xe0000000
#define VECTOR(address) ((JMP_OP)|((unsigned long)address))
#define REDIRECT(address) ((JSR_OP)|((unsigned long)address))

#define TRACE_VEC 5

#define TRAP0_VEC 8
#define TRAP1_VEC 9
#define TRAP2_VEC 10
#define TRAP3_VEC 11

#define NR_TRAPS 15

#endif /* _CR16_TRAPS_H */
