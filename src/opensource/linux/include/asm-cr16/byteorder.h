#ifndef _CR16_BYTEORDER_H
#define _CR16_BYTEORDER_H

#include <linux/compiler.h>
#include <asm/types.h>

#ifdef __GNUC__

static __inline__ __attribute_const__ __u16 cr16_arch__swab16(__u16 x)
{
	register __u16 __r0 __asm__ ("r0") = x;
	register __u16 __r1 __asm__ ("r1") ;

	__asm__(
		"movw	r0,r1\n\t"
		"ashud	$-8,(r1,r0)\n\t"
		:"=r" (__r0), "=r" (__r1)
		: "0" (__r0), "1" (__r1)
	);

	return __r0;
}
#define __arch__swab16(x)       cr16_arch__swab16(x)

static __inline__ __attribute_const__ __u32 cr16_arch__swab32(__u32 x)
{
	register __u32 __r1 __asm__ ("r1") = x;
	register __u32 __r0 __asm__ ("r0") ;
	__asm__(
		"movw	r1,r0\n\t"
		"movw	r2,r3\n\t"
		"ashud	$8,(r3,r2)\n\t"
		"ashud	$8,(r1,r0)\n\t"
		"movw	r3,r0\n\t"
		:"=r" (__r0)
		: "0" (__r0), "r" (__r1)
		: "r3"
	);

	return __r0;
}
#define __arch__swab32(x)       cr16_arch__swab32(x)

#endif


#if defined(__GNUC__) && !defined(__STRICT_ANSI__) || defined(__KERNEL__)
#  define __BYTEORDER_HAS_U64__
#  define __SWAB_64_THRU_32__
#endif

#include <linux/byteorder/little_endian.h>

#endif /* _CR16_BYTEORDER_H */
