/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 * kirk_dect_sc14450.h -- definitions for the char driver
 *Based on the code from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.   
 *
 * 
 */

#ifndef _SC1445x_POWER_H_
#define _SC1445x_POWER_H_

#define SC1445x_POWER_MAJOR 177

#ifndef SC1445x_POWER_NR_DEVS
#define SC1445x_POWER_NR_DEVS 1    
#endif

#define SC1445x_POWER_MAJ_VER 0
#define SC1445x_POWER_MIN_VER 1


/***      Data types definitions       ***/

/***      Msgs definitions       ***/

/***      Buffers definitions       ***/

/***      Ioctl cmds      ***/
enum SC1445x_POWER_IOCTL {
	SC1445x_POWER_IOCTL_GET_STATE=1,
	SC1445x_POWER_IOCTL_SET_STATE,
	SC1445x_POWER_IOCTL_GET_MAX,
	SC1445x_POWER_IOCTL_SET_MAX,
	SC1445x_POWER_IOCTL_GET_MIN,
	SC1445x_POWER_IOCTL_SET_MIN,
	SC1445x_POWER_IOCTL_AUDIO_CLOSE,
	SC1445x_POWER_IOCTL_AUDIO_OPEN,
	SC1445x_POWER_IOCTL_DISPLAY_CLOSE,
	SC1445x_POWER_IOCTL_DISPLAY_OPEN,
	SC1445x_POWER_IOCTL_AUDIO_DELAY,
	SC1445x_POWER_IOCTL_DISPLAY_DELAY,
    SC1445x_POWER_IOCTL_ENABLE_PORT2,
    SC1445x_POWER_IOCTL_DISABLE_PORT2,
    SC1445x_POWER_IOCTL_GET_NET_PORT2_STATE
} ;



#define SC1445x_POWER_SPEED_80   0
#define SC1445x_POWER_SPEED_40   1
#define SC1445x_POWER_SPEED_20   2
#define SC1445x_POWER_SPEED_XTAL 3

void SC1445x_power_pll_mode(void);
void SC1445x_power_ioctl_run_pll_mode(unsigned short);
void SC1445x_power_ioctl_set_state(unsigned short);


#define POS_POWER_MIN   0x84fa
#define POS_POWER_MAX   0x84fc
#define POS_POWER_STATE 0x84fe

#define POS_POWER_FLAG  0x84f6

#endif
