/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

#if !defined( _ASM_REGS_SC14452_ADDENDUM_H )
#define _ASM_REGS_SC14452_ADDENDUM_H


#if !defined( __HAVE_INCLUDED_ASM_REGS_H )
#error Don't directly #include <asm/regs_sc14452_addendum.h> -- use #include <asm/regs.h> instead.
#endif


/*
 * Some additional definitions related to registers.
 */



/*
 * UART
 */

#define UART_CTRL_INV_UTX	       (1 << 10)	/* R/W - UTX inversion (1) */
#define UART_CTRL_INV_URX	       (1 << 9)	/* R/W - URX inversion (1) */
#define UART_CTRL_IRDA_EN	       (1 << 8)	/* R/W - NRZ mode (1) or normal UART mode */
#define UART_CTRL_UART_MODE	       (1 << 7)	/* R/W - Force (1) to use 10.125kbaud even parity */
#define UART_CTRL_RI		       (1 << 6)	/* R - Set if RX IRQ received */
#define UART_CTRL_TI		       (1 << 5)	/* R - Set if TX IRQ received */
#define UART_CTRL_BAUDRATE_MASK	       (7 << 2)	/* Mask all the baud-rate config bits */
#define UART_CTRL_BAUDRATE_9K6	       (0 << 2)	/* R/W - 0 = 9.6 kBaud */
#define UART_CTRL_BAUDRATE_19K2	       (1 << 2)	/* R/W - 1 = 19.2 kBaud */
#define UART_CTRL_BAUDRATE_57K6	       (2 << 2)	/* R/W - 2 = 57.6 kBaud */
#define UART_CTRL_BAUDRATE_115K2       (3 << 2)	/* R/W - 3 = 115.2 kBaud */
#define UART_CTRL_BAUDRATE_FSYS	       (4 << 2)	/* R/W - 4 = Fsys/(X*45) (230.4 kBaud @10.368 MHz) */		
#define UART_CTRL_TEN		       (1 << 1)	/* R/W - enable (1) UART transmitter */
#define UART_CTRL_REN		       (1 << 0)	/* R/W - enable (1) UART receiver */

#define UART_RX_TX_MASK		       (0xff)		/* only the lower 8 bits contain data */

#define UART_ERROR_DMA_PARITY	       (1 << 1)	/* R/W Set when parity error duing DMA. clear by writing. */
#define UART_ERROR_PAR_STATUS	       (1)			/* R - 1= parity error, updated for every new byte */


#endif  /* _ASM_REGS_SC14452_ADDENDUM_H */

