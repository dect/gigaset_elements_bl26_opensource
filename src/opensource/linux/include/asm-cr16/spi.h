#if !defined( _CR16_SPI_H )
#define _CR16_SPI_H

/* structs used by the SPI driver and SPI devices */

struct sc1445x_spi_chip {
	unsigned use_dma	: 1 ;	/* prefer DMA over polling? */
	unsigned word_width	: 1 ;	/* 0: 8 bits, 1: 16 bits */
	unsigned cs_per_word	: 1 ;	/* change chip select on every word? */
} ;


#endif /* _CR16_SPI_H */

