#ifndef _CR16_GPIO_H
#define _CR16_GPIO_H

#define CR16_GPIO_P1 0
#define CR16_GPIO_P2 1
#define CR16_GPIO_P3 2
#define CR16_GPIO_P4 3
#define CR16_GPIO_P5 4
#define CR16_GPIO_P6 5
#define CR16_GPIO_P7 6
#define CR16_GPIO_P8 7
#define CR16_GPIO_P9 8
#define CR16_GPIO_PA 9
#define CR16_GPIO_PB 10
#define CR16_GPIO_PC 11
#define CR16_GPIO_PD 12
#define CR16_GPIO_PE 13
#define CR16_GPIO_PF 14
#define CR16_GPIO_PG 15
#define CR16_GPIO_PH 16

#define CR16_GPIO_B7 0x80
#define CR16_GPIO_B6 0x40
#define CR16_GPIO_B5 0x20
#define CR16_GPIO_B4 0x10
#define CR16_GPIO_B3 0x08
#define CR16_GPIO_B2 0x04
#define CR16_GPIO_B1 0x02
#define CR16_GPIO_B0 0x01

#define CR16_GPIO_INPUT 0
#define CR16_GPIO_OUTPUT 1

#define CR16_GPIO_RESERVE(port, bits) \
        cr16_reserved_gpio(port, bits)

#define CR16_GPIO_FREE(port, bits) \
        cr16_free_gpio(port, bits)

#define CR16_GPIO_DDR(port, bit, dir) \
        cr16_set_gpio_dir(((port) << 8) | (bit), dir)

#define CR16_GPIO_GETDIR(port, bit) \
        cr16_get_gpio_dir(((port) << 8) | (bit))

extern int cr16_reserved_gpio(int port, int bits);
extern int cr16_free_gpio(int port, int bits);
extern int cr16_set_gpio_dir(int port_bit, int dir);
extern int cr16_get_gpio_dir(int port_bit);
extern int cr16_init_gpio(void);

#endif
