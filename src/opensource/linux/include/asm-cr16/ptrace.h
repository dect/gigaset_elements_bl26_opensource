#ifndef _CR16_PTRACE_H
#define _CR16_PTRACE_H

#ifndef __ASSEMBLY__

#define PT_R0AND1      0
#define PT_R2AND3      1
#define PT_R4AND5      2
#define PT_R6AND7      3
#define PT_R8AND9      4
#define PT_R10AND11    5
#define PT_R12         6
#define PT_R13         7
#define PT_RA          8
#define PT_SP          9	   
#define PT_PSR         10	   
#define PT_CFG         11	   
#define PT_ORIG_R0AND1 12
#define PT_PC          13	   
#define PT_USP         14

/* This struct defines the way the registers are stored on the
   stack during a system call. */
struct pt_regs {
  unsigned short pad;	
  unsigned short psr;
  unsigned long pc;
  long r0and1;
  long r2and3;
  long r4and5;
  long r6and7;
  long r8and9;
  long r10and11;
  long r12;
  long r13;
  long ra;
  long orig_r0and1;
}__attribute__((aligned(4)));

#define PTRACE_GETREGS   15    
                               
#define PTRACE_SETREGS   16    
                               
#ifdef __KERNEL__
#ifndef PS_S                   /* Lee - Usermode check */
#define PS_S  (0x08)
#endif

#define CR16_REGS_NO 14        /* Total number of registers  */

/* Find the stack offset for a register, relative to thread.esp0. */
#define PT_REG(reg)	((long)&((struct pt_regs *)0)->reg)

//#define user_mode(regs) (!((regs)->psr & PS_S))
#define user_mode(regs) ((regs)->psr & PS_S)
#define instruction_pointer(regs) ((regs)->pc)
#define profile_pc(regs) instruction_pointer(regs)

extern void show_regs(struct pt_regs *); 

#endif /* __KERNEL__ */
#endif /* __ASSEMBLY__ */
#endif /* _CR16_PTRACE_H */
