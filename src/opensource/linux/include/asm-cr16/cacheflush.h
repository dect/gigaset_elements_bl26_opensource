/*
 * (C) Copyright 2002, Yoshinori Sato <ysato@users.sourceforge.jp>
 */

#ifndef _ASM_CR16_CACHEFLUSH_H
#define _AMS_CR16_CACHEFLUSH_H

//#include <linux/mm.h>
#include <asm/system.h>

#ifdef CONFIG_CR16_CACHE_ENABLE
void flush_cache_region(unsigned start,unsigned end);
#endif

#define flush_cache_all()			__flush_cache_all()
#define flush_cache_mm(mm)			do { } while (0)
#define flush_cache_range(vma, start, end)	__flush_cache_all()
#define flush_cache_page(vma, vmaddr)		do { } while (0)

//#ifdef CONFIG_SC14452
#ifdef CONFIG_SC14452_ES2
#define flush_dcache_range(start,len)		__flush_cache_all()
#define flush_icache_range(start,len)		__flush_cache_all()
#else
#define flush_dcache_range(start,end)		flush_cache_region(start,end)
#define flush_icache_range(start,end)		flush_cache_region(start,end)
#endif
//#else
//#define flush_dcache_range(start,len)		__flush_cache_all()
//#define flush_icache_range(start,len)		__flush_cache_all()
//#endif

#define flush_dcache_page(page)			do { } while (0)
#define flush_dcache_mmap_lock(mapping)		do { } while (0)
#define flush_dcache_mmap_unlock(mapping)	do { } while (0)

#define flush_icache_page(vma,pg)		do { } while (0)
#define flush_icache_user_range(vma,pg,adr,len)	do { } while (0)
#define flush_cache_vmap(start, end)		do { } while (0)
#define flush_cache_vunmap(start, end)		do { } while (0)

#define copy_to_user_page(vma, page, vaddr, dst, src, len) \
	memcpy(dst, src, len)
#define copy_from_user_page(vma, page, vaddr, dst, src, len) \
	memcpy(dst, src, len)


#ifdef CONFIG_CR16_CACHE_ENABLE


#ifdef CONFIG_SC14452

#define __flush_cache_all() \
{ \
	__asm__ __volatile__ ( \
    "push $4, r0\n\t" \
	"spr	psr,r3\n\t" \
	"di		\n\t" \
	"nop            \n\t" \
	"spr	cfg, r2\n\t" \
	"movw    r2,r0\n\t" \
	"andw $-21,r0\n\t" \
	"lpr	r0, cfg\n\t" \
	"loadw 0xff5006,r1 \n\t" \
 	"storw $0, 0xFF5006\n\t" \
	"storw $0, 0xFF4406\n\t" \
	"movw $-26624, r0\n\t" \
	"storw r0, 0xFF4404\n\t" \
	"movw  $0x200,r0\n\t" \
	"storw r0, 0xFF440A\n\t" \
	"movw  $0x1825,r0\n\t" \
	"storw r0, 0xFF440C\n\t" \
"1:	\n\t" \
	"loadw 0xFF440C,r0 \n\t" \
	"tbit $0,r0 \n\t" \
	"bfs	1b\n\t" \
 	"storw r1, 0xFF5006\n\t" \
	"lpr	r2, cfg\n\t" \
	"lpr	r3, psr\n\t" \
    "pop $4, r0\n\t" \
                  : \
			      : \
			      : "r0", "r1", "memory" \
			      ); \
} 

#else

#define __flush_cache_all() \
{ \
	__asm__ __volatile__ ( \
    "push $4, r0\n\t" \
	"spr	psr,r3\n\t" \
	"di		\n\t" \
	"nop            \n\t" \
	"spr	cfg, r2\n\t" \
	"movw    r2,r0\n\t" \
	"andw $-21,r0\n\t" \
	"lpr	r0, cfg\n\t" \
	"movd $0, (r1,r0)\n\t" \
	"stord (r1,r0), 0x97F0\n\t" \
	"storw $0, 0xFF5006\n\t" \
	"storw $0, 0xFF4402\n\t" \
	"movw	$-26640, r0\n\t" \
	"storw r0, 0xFF4400\n\t" \
	"storw $0, 0xFF4406\n\t" \
	"movw $-26624, r0\n\t" \
	"storw r0, 0xFF4404\n\t" \
	"movw  $0x200,r0\n\t" \
	"storw r0, 0xFF440A\n\t" \
	"movw  $0x25,r0\n\t" \
	"storw r0, 0xFF440C\n\t" \
"1:	\n\t" \
	"loadw 0xFF440C,r0 \n\t" \
	"tbit $0,r0 \n\t" \
	"bfs	1b\n\t" \
	"lpr	r2, cfg\n\t" \
	"movw  $0x14,r0\n\t" \
	"storw r0, 0xFF5006\n\t" \
	"lpr	r3, psr\n\t" \
    "pop $4, r0\n\t" \
                  : \
			      : \
			      : "r0", "r1", "memory" \
			      ); \
} 


#endif


#else

#   define __flush_cache_all() do { } while (0)
#   define flush_cache_region(start,end) do { } while (0)

#endif

#endif /* _ASM_CR16_CACHEFLUSH_H */
