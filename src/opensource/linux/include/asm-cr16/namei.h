/*
 * linux/include/asm-cr16/namei.h
 *
 * Included from linux/fs/namei.c
 */

#ifndef __CR16_NAMEI_H
#define __CR16_NAMEI_H

/* This dummy routine maybe changed to something useful
 * for /usr/gnemul/ emulation stuff.
 * Look at asm-sparc/namei.h for details.
 */

#define __emul_prefix() NULL

#endif
