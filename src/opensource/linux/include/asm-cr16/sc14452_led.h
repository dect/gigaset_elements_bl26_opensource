#ifndef SC14452_LED_H_
#define SC14452_LED_H_

#include <asm/regs.h>
#include <linux/types.h>

struct sc14452_led
{
    const char *name;
    volatile unsigned short *set_reg;
    volatile unsigned short *reset_reg;
    volatile unsigned short *port_mode_reg;
    const int port;
    char *default_trigger;
};

#endif /* LED_H_ */
