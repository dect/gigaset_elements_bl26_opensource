/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

#ifndef __DEMO450__
#define __DEMO450__

#define SC1445x_UART_NUM_CHANNELS         1
#define SC1445x_UART_CONSOLE_CHANNEL    0


#endif 
