#ifndef _VMLINUX_LDS_H
#define  _VMLINUX_LDS_H

#if defined( CONFIG_SDRAM_BASE_F0000 )
#  define DRAM_BASE 0xF0000
#elif defined( CONFIG_SDRAM_BASE_22000 )
#  define DRAM_BASE 0x22000
#elif defined( CONFIG_SDRAM_BASE_20000 )
#  define DRAM_BASE 0x20000
#else
#  error SDRAM base not selected
#endif

#if defined(CONFIG_RAM_SIZE_8MB)
#  if defined( CONFIG_SDRAM_BASE_F0000 )
#    define DRAM_SIZE 0x710000
#  elif defined( CONFIG_SDRAM_BASE_22000 )
#    define DRAM_SIZE 0x7DE000
#  elif defined( CONFIG_SDRAM_BASE_20000 )
#    define DRAM_SIZE 0x7E0000
#  endif
#elif defined(CONFIG_RAM_SIZE_14MB)
#  define DRAM_SIZE 0xE00000

#elif defined(CONFIG_RAM_SIZE_16MB)
#  if defined( CONFIG_SDRAM_BASE_F0000 )
#    define DRAM_SIZE 0xEFF000  /* Boot ROM starts at 0xFEF000 */
#  elif defined( CONFIG_SDRAM_BASE_22000 )
#    define DRAM_SIZE 0xFCD000  /* Boot ROM starts at 0xFEF000 */
#  elif defined( CONFIG_SDRAM_BASE_20000 )
#    define DRAM_SIZE 0xFCF000  /* Boot ROM starts at 0xFEF000 */
#  endif

#       define IRQ_STACK_SIZE         0x2000

#elif defined(CONFIG_RAM_SIZE_32MB)
/* we will later put a hole where boot ROM and DSP memories are */
/*
 *  0                           16MB    16,5MB                     32MB
 *  +-----------------------------+------+---------------------------+
 *  |        BANK #1 exe + data   | HOLE |       BANK #2 data only   |
 *  |----------------+------------+      +---+-----------------------+
 *  |                | IRQ stack ^|      |key|                       |
 *  |                | grows down |      |lcd|                       |
 *  |                +------------+------+---+                       |
 *  |                | kernel reserved       |                       |
 *  |                |    mem hole           |                       |
 *  +----------------+-----------------------+-----------------------+
 */
#       define PHYS_RAM_HOLE_START    0x0FEF000     /* address hole between 2 x 16MB RAM Banks */
#       define PHYS_RAM_HOLE_LEN      0x0091000
#       if defined( CONFIG_SC14452_REEF_BS_BOARD )
#           define PHYS_RAM_IO_HOLE_LEN   0x0
#       else
#           define PHYS_RAM_IO_HOLE_LEN   0x30000   /* 192kB for I/O , not used in REEF */
#       endif

#       define IRQ_STACK_SIZE         0x2000

#       define RAM_HOLE_START        ((PHYS_RAM_HOLE_START)-(IRQ_STACK_SIZE))                       /* (was 0x00FED000 including 8192 kb irq stack) */
#       define RAM_HOLE_LEN          ((PHYS_RAM_HOLE_LEN)+(IRQ_STACK_SIZE)+(PHYS_RAM_IO_HOLE_LEN))  /* (was 0x000C3000 including 8192 kb irq stack) */


#  if defined( CONFIG_SDRAM_BASE_F0000 )
#    define DRAM_SIZE       0x1F10000
#  elif defined( CONFIG_SDRAM_BASE_22000 )
#   if 0
#    define DRAM_SIZE 0x1FDE000
#   else
/* leave out the last 64KB */
#    define DRAM_SIZE 0x1FCE000
#   endif
#  elif defined( CONFIG_SDRAM_BASE_20000 )
#    define DRAM_SIZE 0x1FE0000
#  endif
#else
#  error RAM size not selected
#endif

#endif // _VMLINUX_LDS_H
