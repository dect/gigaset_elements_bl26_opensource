/****************************************************************************/

/*
 *  linux/include/asm-cr16/ide.h
 *
 *  Copyright (C) 1994-1996  Linus Torvalds & authors
 *  Copyright (C) 2001       Lineo Inc., davidm@snapgear.com
 *  Copyright (C) 2002       Greg Ungerer (gerg@snapgear.com)
 *  Copyright (C) 2002       Yoshinori Sato (ysato@users.sourceforge.jp)
 */

/****************************************************************************/
#ifndef _CR16_IDE_H
#define _CR16_IDE_H
/****************************************************************************/
#ifdef __KERNEL__
/****************************************************************************/

#define MAX_HWIFS	1

#include <asm-generic/ide_iops.h>

/****************************************************************************/
#endif /* __KERNEL__ */
#endif /* _CR16_IDE_H */
/****************************************************************************/
