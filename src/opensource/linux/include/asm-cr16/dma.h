#ifndef _CR16_DMA_H
#define _CR16_DMA_H 

#define MAX_DMA_CHANNELS 0
//#define MAX_DMA_ADDRESS PAGE_OFFSET
#define MAX_DMA_ADDRESS 0x1000000

/* These are in kernel/dma.c: */
extern int request_dma(unsigned int dmanr, const char *device_id);  /* reserve a DMA channel */
extern void free_dma(unsigned int dmanr);	                    /* release it again */
 
#endif /* _CR16_DMA_H */
