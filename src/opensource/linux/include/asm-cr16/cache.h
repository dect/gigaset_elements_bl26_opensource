#ifndef __ARCH_CR16_CACHE_H
#define __ARCH_CR16_CACHE_H

/* bytes per L1 cache line */
#define L1_CACHE_BYTES  32

#define __cacheline_aligned
#define ____cacheline_aligned

#endif
