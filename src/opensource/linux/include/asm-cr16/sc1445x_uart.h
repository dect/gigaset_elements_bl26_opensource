/*
 * include/asm-cr16/sc1445x_uart.h -- original SC1445x on-chip UART
 *
 *  Copyright (C) 2001,02,03  NEC Electronics Corporation
 *  Copyright (C) 2001,02,03  Miles Bader <miles@gnu.org>
 *
 * This file is subject to the terms and conditions of the GNU General
 * Public License.  See the file COPYING in the main directory of this
 * archive for more details.
 *
 * Written mostly by Miles Bader <miles@gnu.org>
 */

#ifndef __SC1445x_UART_H__
#define __SC1445x_UART_H__


/* Raw hardware interface.  */

typedef unsigned short sc1445x_uart_speed_t;


/* UART configuration interface.  */

/* Type of the uart config register; must be a scalar.  */
typedef u16 sc1445x_uart_config_t;


/* RX/TX interface.  */

/* Return true if uart is ready to transmit a character.  */
#define sc1445x_uart_xmit_ok	!( UART_CTRL_REG & UART_CTRL_TI );

/* Wait for this to be true.  */
#define sc1445x_uart_wait_for_xmit_complete  while ( !(UART_CTRL_REG & UART_CTRL_TI) ); 

/* Wait for this to be true.  */
#define sc1445x_uart_wait_for_xmit_ok  while ( UART_CTRL_REG & UART_CTRL_TI ) {UART_CLEAR_TX_INT_REG = 1;} 

/* Write character CH to uart channel CHAN.  */
#define sc1445x_uart_putc(ch)	(UART_RX_TX_REG = (ch & 0xFF))

/* Return latest character read on channel CHAN.  */
#define sc1445x_uart_getc		(UART_RX_TX_REG)

/* Return bit-mask of uart error status.  */
#define sc1445x_uart_err		UART_ERROR_REG
/* Various error bits set in the error result.  */

#define SC1445x_UART_ERR_PARITY	(UART_ERROR_REG & UART_ERROR_PAR_STATUS)	


#endif /* __SC1445x_UART_H__ */
