/*
 * linux/include/asm-cr16/timex.h
 *
 * H8/300 architecture timex specifications
 */
#ifndef _ASM_CR16_TIMEX_H
#define _ASM_CR16_TIMEX_H

#define CLOCK_TICK_RATE 1000

typedef unsigned long cycles_t;
extern short cr16_timer_count;

static inline cycles_t get_cycles(void)
{
	return 0;
}

#endif
