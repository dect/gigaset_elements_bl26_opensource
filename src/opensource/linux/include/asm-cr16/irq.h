#ifndef _ASM_IRQ_H
#define _ASM_IRQ_H

#include <asm/io.h>


static inline int irq_canonicalize(int irq)
{  
  return irq; 
}

#define NR_IRQS 15

/*
 * Use this value to indicate lack of interrupt capability.
 *
 */
#ifndef NO_IRQ
#define NO_IRQ	((unsigned int)(-1))
#endif

/* The first vector number used for IRQs in v10 is really 0x20 */
/* but all the code and constants are offseted to make 0 the first */
#define FIRST_IRQ 0


#define NMI_INT       1
#define SVC_TRAP      5
#define DVZ_TRAP      6
#define FLG_TRAP      7
#define BPT_TRAP      8
#define TRC_TRAP      9
#define UND_TRAP      10
#define IAD_TRAP      12
#define DBG_TRAP      14
#define ISE_INT       15


#if defined( CONFIG_SC14450 )

	#define ACCESS12_INT  0
	#define KEYB_INT      1   
	#define RESERVED_INT  2
	#define CT_CLASSD_INT 3
	#define UART_RI_INT   4
	#define UART_TI_INT   5
	#define SPI1_AD_INT   6
	#define TIM0_INT      7
	#define TIM1_INT      8
	#define CLK100_INT    9
	#define DIP_INT       10
	#define USB_INT       11
	#define SPI2_INT      12
	#define DSP1_INT      13
	#define DSP2_INT      14

	/* usb: controller at irq 31 + uses DMA8 and DMA9 */
	#define USB_HC_IRQ_NBR IO_BITNR(R_VECT_MASK_RD, usb)

#elif defined( CONFIG_SC14452 )

	#define ACCESS12_INT  0
	#define KEYB_INT      1   
	#define EMAC_INT      2
	#define CT_CLASSD_INT 3
	#define UART_RI_INT   4
	#define UART_TI_INT   5
	#define SPI1_AD_INT   6
	#define TIM0_INT      7
	#define TIM1_INT      8
	#define CLK100_INT    9
	#define DIP_INT       10
	#define CRYPTO_INT    11
	#define SPI2_INT      12
	#define DSP1_INT      13
	#define DSP2_INT      14

#endif


typedef void (*irqvectptr)(void);
struct cr16_interrupt_vector {
	irqvectptr v[31];
};

extern struct cr16_interrupt_vector *cr16_irv;
void set_int_vector(int n, irqvectptr addr);
void set_break_vector(int n, irqvectptr addr);
extern void unmask_irq (unsigned long int_number);

#define __STR(x) #x
#define STR(x) __STR(x)
 

#define IRQ_NAME2(nr) nr##_interrupt(void)
#define IRQ_NAME(nr) IRQ_NAME2(IRQ##nr)
#define sIRQ_NAME(nr) IRQ_NAME2(sIRQ##nr)
#define BAD_IRQ_NAME(nr) IRQ_NAME2(bad_IRQ##nr)




  /* the asm IRQ handler makes sure the causing IRQ is blocked, then it calls
   * do_IRQ (with irq disabled still). after that it unblocks and jumps to
   * ret_from_intr (entry.S)
   *
   * needs to match RESTORE_ALL in entry.S!
   */


#define BUILD_IRQ(nr) \
void IRQ_NAME(nr); \
__asm__ ( \
          ".text\n\t" \
          ".global _IRQ" #nr "_interrupt\n\t" \
          "_IRQ" #nr "_interrupt:\n\t" \
          "di \n\t" \
          "nop \n\t" \
          "push $2, r0\n\t" \
	  "push	$8, r8, ra\n\t" \
          "push $8, r0\n\t" \
          "sprd isp, (r1,r0)\n\t" \
          "loadd 0x0(r1,r0), (r3,r2)\n\t" \
          "push	$2, r2\n\t" \
          "loadw 0x4(r1,r0), r2\n\t" \
          "push	$1, r2\n\t" \
          "subd	$2, (sp)\n\t" \
	  "movd $" #nr ",(r3,r2)\n\t" \
	  "movd (sp), (r5,r4)\n\t" /* r5,r4 is struct pt_regs* */ \
	  "bal (ra), _do_IRQ\n\t" /* irq.c, r3,r2 is an argument */ \
	  "br _ret_from_interrupt\n\t");


#define sc1445x_irq_pending(irq)	(SET_INT_PENDING_REG & (1<<irq))
#define sc1445x_irq_clear_pending(irq)	(RESET_INT_PENDING_REG = (1<<irq))


static inline int sc1445x_irq_enabled (unsigned long int_number)
{
	unsigned short mask = 0x7 << ((int_number%4)*4);
	unsigned short * reg =
			((unsigned short*)&INT3_PRIORITY_REG) - (int_number/4);
    
	return (readw(reg)&mask);
}



#endif  /* _ASM_IRQ_H */


