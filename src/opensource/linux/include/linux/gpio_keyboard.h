// File created by Gigaset Elements GmbH
// All rights reserved.
/*
 * gpio_keyboard.h
 *
 *  Created on: Jan 30, 2013
 *      Author: Piotr L. Figlarek
 */

#ifndef GPIO_KEYBOARD_H_
#define GPIO_KEYBOARD_H_


struct gpio_key
{
	const char *name;
	int (*init)(void *private_date);
	int (*read)(void *private_date);
	void *private_data;
	int button;
};


#endif /* GPIO_KEYBOARD_H_ */
