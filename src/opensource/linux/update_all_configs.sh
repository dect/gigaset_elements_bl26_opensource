#! /bin/bash
configs=`find config_files/ -type f`

orig_config=`mktemp $PWD/.config.XXXXX` &&
		echo "The current .config is copied to $orig_config"  ||  exit 1
cp .config $orig_config

for config in $configs
do
	echo -e "\n\n***** Updating $config *****\n"

	cp $config .config &&
	make oldconfig &&
	cp .config $config &&
	echo -e "\n\nDone with $config\n"

	if [[ "$?" != "0" ]]
	then
		echo -e "\nSomething went wrong with $config!"
		echo "The original .config was copied to $orig_config"
		exit 2
	fi
done

echo "Reverting original .config from $orig_config"
mv $orig_config .config

