#!/bin/bash
#################################################################################################
#
#  Gigaset Elements GmbH sp. z o.o.
#  
#  Project: Reef BS
#
#  Version generation script 
#  Version: 1
#  Comments: This script is called at every linux compilation. It checks the .git 
#       configuration files and produces the file ./reef_version.h which contains 
#       the following definitions:
#	#define REEF_VERSION    --> a string with the repository version of the sources
#	#define REEF_VERSION_DATE --> date of repository commit
#	#define REEF_VERSION_STATUS --> REPOSITORY VERSION if compiled from repository code
#					   NOT REPOSITORY VERSION otherwise 
#					NOTE: this should be "REPOSITORY VERSION" for releases
#
#	Script based on mk_sc14450_version.sh
#################################################################################################

#	git for-each-ref --format="%(taggerdate): %(refname)" --sort=-taggerdate --count=50 refs/tags | grep ${TAG}
#	git log --tags --simplify-by-decoration --pretty="format:%ai %d"
# git for-each-ref --format="%(taggerdate):|%(refname:short)" --sort=-taggerdate --count=1

# Option  Description of Output
# %H  Commit hash
# %h  Abbreviated commit hash
# %T  Tree hash
# %t  Abbreviated tree hash
# %P  Parent hashes
# %p  Abbreviated parent hashes
# %an Author name
# %ae Author e-mail
# %ad Author date (format respects the --date= option)
# %ar Author date, relative
# %cn Committer name
# %ce Committer email
# %cd Committer date
# %cr Committer date, relative
# %s  Subject

# taggerdate	- only on annotated tags
# committerdate	- only on normal tag
# authodate	- only on normal tag
#
# creatordate 	- everywhere

#GIT_DIR="../../../.git"
GIT_DIR="`git rev-parse --show-toplevel`/.git"

if test -d ${GIT_DIR}; then
	
	echo ==============================================
	echo Generating version information for REEF BS Project

#	Get the version number from git tags. Official version tags are in "build-xxx" format (where xxx is a version number). Last successful 
#	daily build is tagged "stable"  

	BUILD_TAG=`git for-each-ref --format="%(refname:short)" --sort=-creatordate refs/tags | grep "build-" | head -n 1`
	BUILD_TAG_DATE=`git log --tags --simplify-by-decoration --pretty="format:%ai|%d" | grep ${BUILD_TAG} | cut -f 1 -d '|'`	
	BUILD_TAG_ID=`git log --tags --simplify-by-decoration --pretty="format:%H|%d" | grep ${BUILD_TAG} | cut -f 1 -d '|'`
	
	CURRENT_BRANCH=`git branch | grep '*' | cut -b 3-`	
	if [ "${CURRENT_BRANCH}" == "baseline" ] || [ "${CURRENT_BRANCH}" == "release-candidate" ]; then
		BS_TAG=`git for-each-ref --format="%(refname:short)" --sort=-creatordate refs/tags | grep "bas-" | head -n 1`
	else
		BS_TAG=`git for-each-ref --format="%(refname:short)" --sort=-creatordate refs/tags | grep "bas-" | head -n 1`.dev
	fi
	
	if [ -n "$BS_TAG" ]; then
	    BS_TAG_DATE=`git log --tags --simplify-by-decoration --pretty="format:%ai|%d" | grep ${BS_TAG} | head -n 1 | cut -f 1 -d '|'`
	    BS_TAG_ID=`git log --tags --simplify-by-decoration --pretty="format:%H|%d" | grep ${BS_TAG} | head -n 1 | cut -f 1 -d '|'`
	fi

#	Compose tag string, tag date, tag git identity and source code git identity


	HEAD_ID=`cat ${GIT_DIR}/refs/heads/${CURRENT_BRANCH}`



	echo
	echo -e "Build tag:\t$BUILD_TAG"
	echo -e "Build tag date:\t$BUILD_TAG_DATE"
	echo -e "Build tag id:\t$BUILD_TAG_ID"
        echo
	echo -e "Bas tag:\t$BS_TAG"
	echo -e "Bas tag date:\t$BS_TAG_DATE"
	echo -e "Bas tag id:\t$BS_TAG_ID"
	echo
	echo -e "Head id:\t$HEAD_ID"

#	Remove existing version file

	rm -f ./reef_version.h

	echo "#define REEF_BUILD_VERSION  \"${BUILD_TAG}\""  >> ./reef_version.h
	echo "#define REEF_BUILD_VERSION_DATE  \"${BUILD_TAG_DATE}\"" >> ./reef_version.h
	if [ "$BUILD_TAG_ID" = "$HEAD_ID" ] ; then
		echo "#define REEF_BUILD_VERSION_STATUS  \"REPOSITORY VERSION\""  >> ./reef_version.h
	else
		echo "#define REEF_BUILD_VERSION_STATUS  \"NOT REPOSITORY VERSION\"" >> ./reef_version.h
	fi
	
	echo >> ./reef_version.h
	if [ -z "${BS_TAG}" ]; then
	    BS_TAG="unknown"
	fi
	if [ -z "${BS_TAG_DATE}" ]; then
	    BS_TAG_DATE="unknown"
	fi
	if [ -z "${BS_TAG_ID}" ]; then
	    BS_TAG_DATE="unknown"
	fi
	
	
	echo "#define REEF_BAS_VERSION  \"${BS_TAG}\""  >> ./reef_version.h
	echo "#define REEF_BAS_VERSION_DATE  \"${BS_TAG_DATE}\"" >> ./reef_version.h	
	if [ "$BS_TAG_ID" = "$HEAD_ID" ] ; then
		echo "#define REEF_BAS_VERSION_STATUS  \"REPOSITORY VERSION\""  >> ./reef_version.h
		echo "#define REEF_BAS_HASH  \"${BS_TAG_ID}\""  >> ./reef_version.h
	else
		echo "#define REEF_BAS_VERSION_STATUS  \"NOT REPOSITORY VERSION\"" >> ./reef_version.h
		echo "#define REEF_BAS_HASH  \"${HEAD_ID}\""  >> ./reef_version.h			
		
	fi

	


else
#	If no .git directory is present define accordingly
	echo No .git directory found, assuming local compilation
	if test `cat ./reef_version.h|grep LOCAL_COMPILATION|wc -l` = 0 ; then
		echo \#define REEF_LOCAL_COMPILATION 1 >> ./reef_version.h
	fi
fi

