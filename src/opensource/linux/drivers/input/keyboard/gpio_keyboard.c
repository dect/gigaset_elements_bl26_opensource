// File created by Gigaset Elements GmbH
// All rights reserved.
/*
 * Simple GPIO keyboard.
 *
 * Copyright (c) 2013 Gigaset Elements GmbH
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/input.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/timer.h>

#include <linux/platform_device.h>

#include <linux/gpio_keyboard.h>
#include <linux/list.h>





struct key_config
{
    struct list_head list;
    struct input_dev *button_dev;
    int button;
    int (*read)(void *private_data);
    void *pivate_data;

};


#define TIMER_TIMEOUT    (HZ/CONFIG_GPIO_KEYBOARD_TIMER)

static struct timer_list my_timer;
static int my_timer_is_running=0;



static LIST_HEAD(keys);


void timer_func(unsigned long data)
{
    struct list_head *p;
    struct key_config *key;

    // check all known keys
    list_for_each(p, &keys) {
        key = list_entry(p, struct key_config, list);

        input_report_key(key->button_dev, key->button, key->read(key->pivate_data));
        input_sync(key->button_dev);

    }

    // restart timer
	my_timer.expires = jiffies + TIMER_TIMEOUT;
	add_timer(&my_timer);

}





/*
 * Register GPIO as keyboard key.
 */
static int gpio_keyboard_probe(struct platform_device *dev)
{
    int ret;
    struct gpio_key *gpio=0;
    struct input_dev *button_dev=0;
    struct key_config *cfg=0;

    // read device configuration
    gpio = dev->dev.platform_data;
    if (!gpio){
            dev_err(&dev->dev, "Missing configuration for GPIO key\n");
    }


    // adapt configuration to driver structure
    cfg = kmalloc(sizeof(struct key_config), GFP_KERNEL);
    if (!cfg)
    {
        dev_err(&dev->dev, "Can't allocate memory for GPIO %s configuration\n", gpio->name);
        ret = -ENOMEM;
        goto exit;
    }
//    cfg->button_dev = button_dev;
    cfg->button = gpio->button;
    cfg->read = gpio->read;
    cfg->pivate_data = gpio->private_data;


    // add to button list
    list_add(&cfg->list, &keys);


    // create input device
    button_dev = input_allocate_device();
    if( !button_dev )
    {
        dev_err(&dev->dev, "Can't create input device for GPIO %s\n", gpio->name);
        ret = -ENOMEM;
        goto free_key_config;
    }

    // configure input device
    button_dev->evbit[0] = BIT(EV_KEY);
    button_dev->keybit[LONG(gpio->button)] = BIT(gpio->button);
    button_dev->name = gpio->name;


    cfg->button_dev = button_dev;


    // register input device
    ret = input_register_device(button_dev);
    if (ret) {
        dev_err(&dev->dev, "Can't register input device for GPIO %s\n", gpio->name);
        ret = -EIO;
        goto free_input_device;

    }


    // initialize GPIO
    if( gpio->init )
        gpio->init(gpio->private_data);



    // start timer
    if( !my_timer_is_running )
    {
        init_timer(&my_timer);

        my_timer.expires = jiffies + TIMER_TIMEOUT;
        my_timer.data = 0;
        my_timer.function = timer_func;

        add_timer(&my_timer);

        my_timer_is_running = 1;
    }

    dev_info(&dev->dev, "Registered new button: %s\n", gpio->name);

	return 0;

free_input_device:
    input_free_device(button_dev);
free_key_config:
    kfree(cfg);
exit:
	return ret;
}




static struct platform_driver gpio_keyboard_driver = {
	.probe		= gpio_keyboard_probe,
	.remove		= 0,
	.suspend	= 0,
	.resume		= 0,
	.driver		= {
		.name		= "gpio-key",
		.owner		= THIS_MODULE,
	},
};

static int __init gpio_keyboard_init(void)
{
	return platform_driver_register(&gpio_keyboard_driver);
}

static void __exit gpio_keyboard_exit(void)
{
 	platform_driver_unregister(&gpio_keyboard_driver);
}

module_init(gpio_keyboard_init);
module_exit(gpio_keyboard_exit);

MODULE_AUTHOR("Piotr Figlarek <piotr.figlarek@gigaset.com>");
MODULE_DESCRIPTION("Simple GPIO keyboard");
MODULE_LICENSE("GPL");



