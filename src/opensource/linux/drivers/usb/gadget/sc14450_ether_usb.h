// File created by Gigaset Elements GmbH
// All rights reserved.
/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

#ifndef __LINUX_USB_GADGET_SC14450_H
#define __LINUX_USB_GADGET_SC14450_H

#include <linux/types.h>

#define UDC_DEBUG_ON



/*
 * USB Controller 
 */
#define USB_MCTRL_REG_POS		       0xFF6800  /* Main Control Register) */
#define USB_XCVDIAG_REG_POS		       0xFF6802  /* Transceiver diagnostic Register (for test purpose only) */
#define USB_TCR_REG_POS	       	       0xFF6804  /* Transceiver configuration Register */
#define USB_UTR_REG_POS	       	       0xFF6806  /* USB test Register (for test purpose only) */
#define USB_FAR_REG_POS		           0xFF6808  /* Function Address Register */
#define USB_NFSR_REG_POS		       0xFF680A  /* Node Functional State Register */
#define USB_MAEV_REG_POS		       0xFF680C  /* Main Event Register */
#define USB_MAMSK_REG_POS		       0xFF680E  /* Main Mask Register */
#define USB_ALTEV_REG_POS		       0xFF6810  /* Alternate Event Register */
#define USB_ALTMSK_REG_POS		       0xFF6812  /* Alternate Mask Register */
#define USB_TXEV_REG_POS		       0xFF6814  /* Transmit Event Register */
#define USB_TXMSK_REG_POS		       0xFF6816  /* Transmit Mask Register */
#define USB_RXEV_REG_POS		       0xFF6818  /* Receive Event Register */
#define USB_RXMSK_REG_POS		       0xFF681A  /* Receive Mask Register */
#define USB_NAKEV_REG_POS		       0xFF681C  /* NAK Event Register */
#define USB_NAKMSK_REG_POS		       0xFF681E  /* NAK Mask Register */
#define USB_FWEV_REG_POS		       0xFF6820  /* FIFO Warning Event Register */
#define USB_FWMSK_REG_POS		       0xFF6822  /* FIFO Warning Mask Register */
#define USB_FNH_REG_POS		           0xFF6824  /* Frame Number High Byte Register */
#define USB_FNL_REG_POS		           0xFF6826  /* Frame Number Low Byte Register */
#define USB_UX20CDR_REG_POS		       0xFF683E  /* Transceiver 2.0 Configuration and Diagnostics Register(for test purpose only) */

#define USB_EPC0_REG_POS	      	   0xFF6840  /* Endpoint Control 0 Register */
#define USB_TXD0_REG_POS		       0xFF6842  /* Transmit Data 0 Register */
#define USB_TXS0_REG_POS		       0xFF6844  /* Transmit Status 0 Register */
#define USB_TXC0_REG_POS	      	   0xFF6846  /* Transmit command 0 Register */
#define USB_EP0_NAK_REG_POS		       0xFF6848  /* EP0 INNAK and OUTNAK Register */
#define USB_RXD0_REG_POS		       0xFF684A  /* Receive Data 0 Register */
#define USB_RXS0_REG_POS		       0xFF684C  /* Receive Status 0 Register */
#define USB_RXC0_REG_POS		       0xFF684E  /* Receive Command 0 Register */

#define USB_EPC1_REG_POS		       0xFF6850  /* Endpoint Control Register 1 */
#define USB_TXD1_REG_POS		       0xFF6852  /* Transmit Data Register 1 */
#define USB_TXS1_REG_POS		       0xFF6854  /* Transmit Status Register 1 */
#define USB_TXC1_REG_POS		       0xFF6856  /* Transmit Command Register 1 */
#define USB_EPC2_REG_POS		       0xFF6858  /* Endpoint Control Register 2 */
#define USB_RXD1_REG_POS		       0xFF685A  /* Receive Data Register,1 */
#define USB_RXS1_REG_POS		       0xFF685C  /* Receive Status Register 1 */
#define USB_RXC1_REG_POS		       0xFF685E  /* Receive Command Register 1 */

#define USB_EPC3_REG_POS		       0xFF6860  /* Endpoint Control Register 3 */
#define USB_TXD2_REG_POS		       0xFF6862  /* Transmit Data Register 2 */
#define USB_TXS2_REG_POS		       0xFF6864  /* Transmit Status Register 2 */
#define USB_TXC2_REG_POS		       0xFF6866  /* Transmit Command Register 2 */
#define USB_EPC4_REG_POS		       0xFF6868  /* Endpoint Control Register 4 */
#define USB_RXD2_REG_POS		       0xFF686A  /* Receive Data Register 2 */
#define USB_RXS2_REG_POS		       0xFF686C  /* Receive Status Register 2 */
#define USB_RXC2_REG_POS		       0xFF686E  /* Receive Command Register 2 */

#define USB_EPC5_REG_POS		       0xFF6870  /* Endpoint Control Register 5 */
#define USB_TXD3_REG_POS		       0xFF6872  /* Transmit Data Register 3 */
#define USB_TXS3_REG_POS		       0xFF6874  /* Transmit Status Register 3 */
#define USB_TXC3_REG_POS		       0xFF6876  /* Transmit Command Register 3 */
#define USB_EPC6_REG_POS		       0xFF6878  /* Endpoint Control Register 6 */
#define USB_RXD3_REG_POS		       0xFF687A  /* Receive Data Register 3 */
#define USB_RXS3_REG_POS		       0xFF687C  /* Receive Status Register 3 */
#define USB_RXC3_REG_POS		       0xFF687E  /* Receive Command Register 3 */


#define BIT0   0x0001
#define BIT1   0x0002
#define BIT2   0x0004
#define BIT3   0x0008
#define BIT4   0x0010
#define BIT5   0x0020
#define BIT6   0x0040
#define BIT7   0x0080

#define BIT8   0x0100
#define BIT9   0x0200
#define BIT10  0x0400
#define BIT11  0x0800


// Main Event Register. Clears on read.
#define MAEV_WARN     BIT0
#define MAEV_ALT      BIT1
#define MAEV_TX_EV    BIT2
#define MAEV_FRAME    BIT3
#define MAEV_NAK      BIT4
#define MAEV_ULD      BIT5
#define MAEV_RX_EV    BIT6
#define MAEV_INTR     BIT7
#define MAEV_EP0_TX   BIT8
#define MAEV_EP0_RX   BIT9
#define MAEV_EP0_NAK  BIT10


// Alternate Event Register. Clears on read.
#define ALTEV_EOP     BIT3
#define ALTEV_SD3     BIT4
#define ALTEV_SD5     BIT5
#define ALTEV_RESET   BIT6
#define ALTEV_RESUME  BIT7

// Endpoint Control Registers.
#define EPC_EP_MASK 0xF
#define EPC_EP_EN   BIT4  // Not in EPC0.
#define EPC_ISO     BIT5  // Not in EPC0.
#define EPC_DEF     BIT6  // Only in EPC0.
#define EPC_STALL   BIT7

// TX status registers.
#define TXS_TCOUNT_MASK 0x1F
#define TXS_TX_DONE     BIT5
#define TXS_ACK_STAT    BIT6
#define TXS_TX_URUN     BIT7 // Not in TXS0.

// Transmit Command Registers.
#define TXC_TX_EN       BIT0
#define TXC_LAST        BIT1
#define TXC_TOGGLE      BIT2
#define TXC_FLUSH       BIT3
#define TXC_RFF         BIT4
#define TXC_TFWL_MASK   0x60
#define TXC_TFWL_4      (0x1 << 5)
#define TXC_TFWL_8      (0x2 << 5)
#define TXC_TFWL_16     (0x3 << 5)
#define TXC_IGN_ISOMSK  BIT7

// Receive Status Registers.
#define RXS_RCOUNT_MASK 0xF
#define RXS_RX_LAST     BIT4
#define RXS_TOGGLE      BIT5
#define RXS_SETUP       BIT6
#define RXS_RX_ERR      BIT7

// Receive Command Registers.
#define RXC_RX_EN       BIT0
#define RXC_IGN_OUT     BIT1
#define RXC_IGN_SETUP   BIT2
#define RXC_FLUSH       BIT3
#define RXC_RFWL_MASK   0x60
#define RXC_RFWL_4      (0x1 << 5)
#define RXC_RFWL_8      (0x2 << 5)
#define RXC_RFWL_16     (0x3 << 5)


struct sc14450_udc;

struct sc14450_ep {
	struct usb_ep				ep;
	struct sc14450_udc			*dev;

	const struct usb_endpoint_descriptor	*desc;
	struct list_head    queue;
	unsigned long       pio_irqs;
	unsigned long       dma_irqs;
	short               dma; 

	unsigned short  fifo_size;
	u8              bEndpointAddress;
	u8              bmAttributes;
    u16             PacketSize;

	unsigned        stopped : 1;
	unsigned        dma_fixup : 1;

    unsigned Type : 2;  // Control, iso, bulk or interrupt.
    unsigned ZeroTerminate : 1;
    unsigned Toggle : 1;
    unsigned Spare : 3;
    unsigned TxBusy : 1;

    // regs
    volatile u16 *epc;
    volatile u16 *txc;
    volatile u16 *txs;
    volatile u16 *txd;
    volatile u16 *rxc;
    volatile u16 *rxs;
    volatile u16 *rxd;
		 
};

struct sc14450_request {
	struct usb_request			req;
	struct list_head			queue;
};

enum ep0_state { 
	EP0_IDLE,
	EP0_IN_DATA_PHASE,
	EP0_OUT_DATA_PHASE,
	EP0_END_XFER,
	EP0_STALL,
};


#define EP0_FIFO_SIZE	((unsigned)8)
#define BULK_FIFO_SIZE	((unsigned)64)
#define ISO_FIFO_SIZE	((unsigned)32)
#define INT_FIFO_SIZE	((unsigned)8)


struct udc_stats {
	struct ep0stats {
		unsigned long		ops;
		unsigned long		bytes;
	} read, write;
	unsigned long			irqs;
};

#undef	USE_DMA
#define	SC14450_UDC_NUM_ENDPOINTS	3


struct sc14450_udc {
	struct usb_gadget           gadget;
	struct usb_gadget_driver    *driver;

	enum ep0_state              ep0state;
	struct udc_stats            stats;
	unsigned                    got_irq : 1,
	                            vbus : 1,
	                            pullup : 1,
	                            has_cfr : 1,
	                            req_pending : 1,
	                            req_std : 1,
	                            req_config : 1;

#define start_watchdog(dev) mod_timer(&dev->timer, jiffies + (HZ/100))
	struct timer_list			timer;

	struct device				  *dev;
	struct sc14450_udc_mach_info  *mach;
	u64					        dma_mask;
	struct sc14450_ep			ep [SC14450_UDC_NUM_ENDPOINTS];
};


/*-------------------------------------------------------------------------*/

/* LEDs are only for debug */
#ifndef HEX_DISPLAY
#define HEX_DISPLAY(n)		do {} while(0)
#endif

#ifdef UDC_DEBUG_ON
/*
#define LED_CONNECTED_ON	leds_event(led_green_on)
#define LED_CONNECTED_OFF	do { \
					leds_event(led_green_off); \
					HEX_DISPLAY(0); \
				} while(0)
*/                
#endif

#ifndef LED_CONNECTED_ON
#define LED_CONNECTED_ON	do {} while(0)
#define LED_CONNECTED_OFF	do {} while(0)
#endif

/*-------------------------------------------------------------------------*/

static struct sc14450_udc *the_controller;

/*
 * Debugging support vanishes in non-debug builds.  DBG_NORMAL should be
 * mostly silent during normal use/testing, with no timing side-effects.
 */
#define DBG_NORMAL	    1	/* error paths, device state transitions */
#define DBG_VERBOSE	    2	/* add some success path trace info */
#define DBG_NOISY	    3	/* ... even more: request level */
#define DBG_VERY_NOISY  4	/* ... even more: packet level */

#ifdef UDC_DEBUG_ON

static const char *state_name[] = {
	"EP0_IDLE",
	"EP0_IN_DATA_PHASE", "EP0_OUT_DATA_PHASE",
	"EP0_END_XFER", "EP0_STALL"
};

//#define DMSG(stuff...) printk(KERN_DEBUG "udc: " stuff)
#define DMSG(stuff...) printk("udc: " stuff)

#ifdef UDC_DEBUG_VERBOSE
#    define UDC_DEBUG DBG_VERBOSE
#else
#    define UDC_DEBUG DBG_NORMAL
#endif


#else

#define DMSG(stuff...)		do{}while(0)

#define UDC_DEBUG ((unsigned)0)

#endif

#define DBG(lvl, stuff...) do{if ((lvl) <= UDC_DEBUG) DMSG(stuff);}while(0)

#define WARN(stuff...) printk(KERN_WARNING "udc: " stuff)
#define INFO(stuff...) printk(KERN_INFO "udc: " stuff)


#endif /* __LINUX_USB_GADGET_SC14450_H */


