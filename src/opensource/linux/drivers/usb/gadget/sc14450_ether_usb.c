// File created by Gigaset Elements GmbH
// All rights reserved.
/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 * linux/drivers/usb/gadget/sc14450_ether_usb.c
*/


#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ioport.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/delay.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/timer.h>
#include <linux/list.h>
#include <linux/interrupt.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <linux/platform_device.h>
#include <linux/dma-mapping.h>
#include <linux/irq.h>

#include <asm/byteorder.h>
#include <asm/dma.h>
#include <asm/io.h>
#include <asm/system.h>

#include <asm/unaligned.h>


#include <linux/usb_ch9.h>
#include <linux/usb_gadget.h>


#define	DRIVER_VERSION	"13-Jan-2009"
#define	DRIVER_DESC	"SC14450 USB Device Controller driver"



#define LockType unsigned long lock_flags; spinlock_t
#define AcquireLock(mr_lock) mr_lock = SPIN_LOCK_UNLOCKED; spin_lock_irqsave(&mr_lock, lock_flags);
#define ReleaseLock(mr_lock) spin_unlock_irqrestore(&mr_lock, lock_flags);


static const char driver_name [] = "sc14450_udc";

static const char ep0name [] = "ep0";


#include "sc14450_ether_usb.h"


static void sc14450_ep_fifo_flush (struct usb_ep *ep);
static void nuke (struct sc14450_ep *, int status);
static void done(struct sc14450_ep *, struct sc14450_request *, int );

static inline void ep0_idle (struct sc14450_udc *dev)
{
    DBG(DBG_VERBOSE,"ep0_idle\n");
    
	dev->ep0state = EP0_IDLE;
    DBG(DBG_VERBOSE,"ep0_idle : dev->ep0state --> EP0_IDLE \n");

    dev->ep[0].TxBusy = 0;
}

static void TxDone(struct sc14450_ep	*ep, struct sc14450_request *req)
/*======================================
 * Inputs         Endpoint number and data.
 * Returns        None.
 *
 * Description    Check if TX is active for the endpoint, and complete it.
 *
 *======================================
 */
{
  DBG(DBG_VERBOSE, "TxDone_new ep:%d \n", (int) ep->desc);
  if (ep->TxBusy)
  {
    DBG(DBG_VERBOSE, "ep->TxBusy\n");
    ep->TxBusy = 0;
    
    if(ep->desc == 0) ep0_idle(ep->dev); 
    done(ep, req, 0); 
    
  }
}


int TxFill(struct sc14450_ep	*ep, struct sc14450_request *req)
/*======================================
 * Inputs         Endpoint number.
 * Returns        number of bytes.
 *
 * Description    Fill TX buffer for the endpoint.
 *
 *======================================
 */
{
   int sent_bytes=0;

   u16 saved = req->req.length - req->req.actual;

  DBG(DBG_VERBOSE, "TxFill start, %d / %d \n", req->req.actual, req->req.length);

  {
    volatile u16* txs = ep->txs;
    volatile u16* txd = ep->txd;
    u16 remain;

    u8 tcount = *txs & TXS_TCOUNT_MASK;
    
    // u8* pd = ep->Tx.Buffer + ep->Tx.ActualSize;
    u8* pd  = req->req.buf + req->req.actual;
    DBG(DBG_VERBOSE, "TxFill  &txs = %X \n", (int) txs);

    saved = min(saved, ep->fifo_size);

    remain = saved;
    // DBG(DBG_VERBOSE, "     EP%d (%d): ", EpNr, ep->Mps );
    // DBG(DBG_VERBOSE, ".");

    while (tcount && remain)
    {
      u8 n = min(tcount, remain);
      tcount -= n;
      remain -= n;
      while (n--)
      {
        DBG(DBG_VERBOSE, "%02X ",*pd);
        sent_bytes++;
        *txd = *pd++;
      }
      tcount = *txs & TXS_TCOUNT_MASK;
    }
    saved -= remain;
  }
  
  ep->PacketSize = saved;
  req->req.actual += saved;

  
  {
    volatile u16* txc = ep->txc;
    if (ep->Toggle)
    {
      *txc |= TXC_TOGGLE;
    }
    else
    {
      *txc &= ~TXC_TOGGLE;
    }
    *txc |= TXC_LAST | TXC_TX_EN;
    DBG(DBG_VERBOSE, "TxFill with toggle = %d , actualSize = %d of %d\n",*txc &TXC_TOGGLE,  req->req.actual , req->req.length);
  }
  // DBG(DBG_VERBOSE, "\n");
  
  return sent_bytes;
}


static int is_vbus_present(void)
{
	return 1;
}

static void pullup_off(void)
{
    ;
}

static void pullup_on(void)
{
    ;
}

static u8 SaveUsbInt(void)
/*======================================
 * Inputs         None.
 * Returns        Saved USB interrupt state.
 *
 * Description    Disable USB interrupt.
 *
 *======================================
 */
{
  LockType lock;
  u8 state;
  AcquireLock(lock);
  state = (USB_MAMSK_REG & USB_M_INTR)? 1 : 0;
  USB_MAMSK_REG &= ~USB_M_INTR;
  ReleaseLock(lock);
  return state;

}

static void RestoreUsbInt(u8 state)
/*======================================
 * Inputs         Saved USB interrupt state.
 * Returns        None.
 *
 * Description    Restore USB interrupt to previous state.
 *
 *======================================
 */
{
  LockType lock;
  AcquireLock(lock);
  // USB_MAMSK_REG_bit.USB_M_INTR = state;
  
  if (state) 
    USB_MAMSK_REG |= USB_M_INTR;
  else
    USB_MAMSK_REG &= ~USB_M_INTR;
    
  ReleaseLock(lock);
}

static int sc14450_ep_enable (struct usb_ep *_ep,
		const struct usb_endpoint_descriptor *desc)
{
	struct sc14450_ep        *ep;
	struct sc14450_udc       *dev;

    DBG(DBG_VERBOSE, "sc14450_ep_enable \n");

	ep = container_of (_ep, struct sc14450_ep, ep);
	if (!_ep || !desc || ep->desc || _ep->name == ep0name
			|| desc->bDescriptorType != USB_DT_ENDPOINT
			|| ep->bEndpointAddress != desc->bEndpointAddress
			|| ep->fifo_size < le16_to_cpu
						(desc->wMaxPacketSize)) {
		DMSG("%s, bad ep or descriptor\n", __FUNCTION__);
		return -EINVAL;
	}

	/* xfer types must match, except that interrupt ~= bulk */
	if (ep->bmAttributes != desc->bmAttributes
			&& ep->bmAttributes != USB_ENDPOINT_XFER_BULK
			&& desc->bmAttributes != USB_ENDPOINT_XFER_INT) {
		DMSG("%s, %s type mismatch\n", __FUNCTION__, _ep->name);
		return -EINVAL;
	}

	/* hardware _could_ do smaller, but driver doesn't */
	if ((desc->bmAttributes == USB_ENDPOINT_XFER_BULK
				&& le16_to_cpu (desc->wMaxPacketSize)
						!= BULK_FIFO_SIZE)
			|| !desc->wMaxPacketSize) {
		DMSG("%s, bad %s maxpacket\n", __FUNCTION__, _ep->name);
		return -ERANGE;
	}

	dev = ep->dev;
	if (!dev->driver || dev->gadget.speed == USB_SPEED_UNKNOWN) {
		DMSG("%s, bogus device state\n", __FUNCTION__);
		return -ESHUTDOWN;
	}

	ep->desc = desc;
	ep->dma = -1;
	ep->stopped = 0;
	ep->pio_irqs = ep->dma_irqs = 0;
	ep->ep.maxpacket = le16_to_cpu (desc->wMaxPacketSize);

	/* flush fifo (mostly for OUT buffers) */
	sc14450_ep_fifo_flush (_ep);

	/* ... reset halt state too, if we could ... */


   if (!strcmp(_ep->name,"ep1in-bulk")) {
        DBG(DBG_VERBOSE, "Writing Registers for 1\n");
        *(ep->epc) = 0x11;
    } 
    

    if (!strcmp(_ep->name,"ep2out-bulk")) {
        DBG(DBG_VERBOSE, "Writing Registers for 2\n");
        *(ep->epc) = 0x12;
        *(ep->rxc) = 0x1;
    } 
    
	DBG(DBG_VERBOSE, "enabled %s\n", _ep->name);
	return 0;
}

static int sc14450_ep_disable (struct usb_ep *_ep)
{
	struct sc14450_ep	*ep;
	unsigned long		flags;

	ep = container_of (_ep, struct sc14450_ep, ep);
	if (!_ep || !ep->desc) {
		DMSG("%s, %s not enabled\n", __FUNCTION__,
			_ep ? ep->ep.name : NULL);
		return -EINVAL;
	}
	local_irq_save(flags);

	nuke (ep, -ESHUTDOWN);


	/* flush fifo (mostly for IN buffers) */
	sc14450_ep_fifo_flush (_ep);

	ep->desc = NULL;
	ep->stopped = 1;

	local_irq_restore(flags);
	DBG(DBG_VERBOSE, "%s disabled\n", _ep->name);
	return 0;
}

/*-------------------------------------------------------------------------*/

static struct usb_request *
sc14450_ep_alloc_request (struct usb_ep *_ep, gfp_t gfp_flags)
{
	struct sc14450_request *req;

    DBG(DBG_VERBOSE, "sc14450_ep_alloc_request, flags = %X \n", gfp_flags);
	
    req = kzalloc(sizeof(*req), gfp_flags);
	if (!req)
		return NULL;

	INIT_LIST_HEAD (&req->queue);
	return &req->req;
}



static void
sc14450_ep_free_request (struct usb_ep *_ep, struct usb_request *_req)
{
	struct sc14450_request	*req;

    DBG(DBG_VERBOSE, "sc14450_ep_free_request\n");
	req = container_of (_req, struct sc14450_request, req);
	WARN_ON (!list_empty (&req->queue));
	kfree(req);
}


static void *
sc14450_ep_alloc_buffer(struct usb_ep *_ep, unsigned bytes,
	dma_addr_t *dma, gfp_t gfp_flags)
{
	char			*retval;

	retval = kmalloc (bytes, gfp_flags & ~(__GFP_DMA|__GFP_HIGHMEM));
	if (retval)
		*dma = (dma_addr_t)~0;
	return retval;
}

static void
sc14450_ep_free_buffer(struct usb_ep *_ep, void *buf, dma_addr_t dma,
		unsigned bytes)
{
	kfree (buf);
}

/*-------------------------------------------------------------------------*/


static void done(struct sc14450_ep *ep, struct sc14450_request *req, int status)
{
	unsigned		stopped = ep->stopped;


    DBG(DBG_VERBOSE, "usb request done\n");
    
    // ep->dev->req_pending = 0; 
    
	list_del_init(&req->queue);

	if (likely (req->req.status == -EINPROGRESS))
		req->req.status = status;
	else
		status = req->req.status;

	if (status && status != -ESHUTDOWN) {
		DBG(DBG_VERBOSE, "complete %s req %p stat %d len %u/%u\n",
			ep->ep.name, &req->req, status,
			req->req.actual, req->req.length);
    }

	ep->stopped = 1;
	req->req.complete(&ep->ep, &req->req);
	ep->stopped = stopped;
    
}



 
u16 trigger_tx_end;
struct sc14450_request *req_to_end;

static int
write_fifo (struct sc14450_ep *ep, struct sc14450_request *req)
{
	unsigned		max;

    DBG(DBG_VERBOSE, "write_fifo start \n");
    
   if ( req->req.actual == 0) {
         ep->TxBusy = 1;
    }
    
	max = le16_to_cpu(ep->desc->wMaxPacketSize);
	do {
		unsigned	count;
		int		is_last, is_short;

		// count = write_packet(ep->txd, req, max);
        DBG(DBG_VERBOSE, "executing TxFill ...\n");
        count = TxFill(ep,req);
        ep->dev->stats.write.bytes += count;
        
		/* last packet is usually short (or a zlp) */
		if (unlikely (count != max))
			is_last = is_short = 1;
		else {
			if (likely(req->req.length != req->req.actual)
					|| req->req.zero)
				is_last = 0;
			else
				is_last = 1;
			/* interrupt/iso maxpacket may not fill the fifo */
			is_short = unlikely (max < ep->fifo_size);
		}

		DBG(DBG_VERBOSE, "wrote %s %d bytes%s%s %d left %p\n",
			ep->ep.name, count,
			is_last ? "/L" : "", is_short ? "/S" : "",
			req->req.length - req->req.actual, req);

		/* requests complete when all IN data is in the FIFO */
		if (is_last) {
            trigger_tx_end = 1;
            req_to_end     = req;
			// done (ep, req, 0);
			return 1;
		} else {
            trigger_tx_end = 0;
        }


	} 
    while(0);
    
	return 0;
}


struct usb_request	usb_req;



static void TxFill_zero(struct sc14450_ep *ep)
/*======================================
 * Inputs         Endpoint number.
 * Returns        None.
 *
 * Description    Fill TX buffer for the endpoint.
 *
 *======================================
 */
{
  u16 saved = 0;
  {
    volatile u16* txs = ep->txs;
    volatile u16* txd = ep->txd;

    u8 tcount = *txs & TXS_TCOUNT_MASK;
    u8* pd = NULL;
    u16 remain;
    saved = min(saved, ep->fifo_size);
    remain = saved;
    // DBG(DBG_VERBOSE, "     EP%d (%d): ", EpNr, ep->Mps );
    // DBG(DBG_VERBOSE, ".");
    while (tcount && remain)
    {
      u8 n = min(tcount, remain);
      tcount -= n;
      remain -= n;
      while (n--)
      {
        DBG(DBG_VERBOSE, "%02X ",*pd);
        *txd = *pd++;
      }
      tcount = *txs & TXS_TCOUNT_MASK;
    }
    saved -= remain;
  }
  ep->PacketSize = saved;
  // ep->Tx.ActualSize += saved;
  
  {
    volatile u16* txc = ep->txc;
    volatile u16* rxc = ep->rxc;

    if (ep->Toggle)
    {
      *txc |= TXC_TOGGLE;
    }
    else
    {
      *txc &= ~TXC_TOGGLE;
    }

    *rxc &= ~RXC_RX_EN;
    *txc |= TXC_LAST | TXC_TX_EN;
    
    DBG(DBG_VERBOSE, "TxFill_zero : txc= %X, rxc=%X \n",*txc, USB_RXC0_REG);
  }
  // DBG(DBG_VERBOSE, "\n");
}


void UdEpTxStart_zero(struct sc14450_ep *ep, u8* Buffer, u16 Size)
/*======================================
 * Inputs         EpNr: The endpoint number to transmit on.
 *                Buffer: Data to transmit.
 *                Size: Number of bytes in Buffer.
 * Returns        None.
 *
 * Description    Transmit on an endpoint.
 *
 *======================================
 */
{
  u8 state = SaveUsbInt();

  DBG(DBG_VERBOSE, "UdEpTxStart_zero\n");

  // ep->Toggle = 1;
  
  TxFill_zero(ep);
  
  ep->TxBusy = 1;
 
  RestoreUsbInt(state);

}




static int
write_ep0_fifo (struct sc14450_ep *ep, struct sc14450_request *req)
{
	unsigned	count;
	int		is_short;

    DBG(DBG_VERBOSE,"\n\nwrite_ep0_fifo, len : %d, actual:%d \n", req->req.length, req->req.actual);

    if ( req->req.actual == 0) {
         ep->TxBusy = 1;
         // ep->Toggle = 1;
    }
    
	// count = write_packet((volatile u16 *)USB_TXD0_REG_POS, req, EP0_FIFO_SIZE);
    
    count = TxFill(ep, req);
    
    // count = EP0_FIFO_SIZE;
    
	ep->dev->stats.write.bytes += count;

	/* last packet "must be" short (or a zlp) */
	is_short = (count != EP0_FIFO_SIZE);

	DBG(DBG_VERY_NOISY, "ep0in %d bytes %d left %p\n", count,
		req->req.length - req->req.actual, req);

    DBG(DBG_VERBOSE, "ep->dev->req_pending = %d \n ", ep->dev->req_pending);

    
	return is_short;
}


static int
read_fifo(struct sc14450_ep *ep, struct sc14450_request *req, u16 rxs)
{
    int rx_count=0;
    
    

       
		u8		*buf;
		unsigned	bufferspace, count, is_short;

    DBG(DBG_VERBOSE, "usb: read_fifo, req = %X, len = %d, actual = %d\n", (u32) req, req->req.length, req->req.actual );
	
    	if ( (rxs & RXS_RX_LAST) == 0) {
            DBG(DBG_VERBOSE,"read_fifo returned 0\n");
			return 0;
        }

		buf = req->req.buf + req->req.actual;
		prefetchw(buf);
		bufferspace = req->req.length - req->req.actual;

        
        
            DBG(DBG_VERBOSE, "R: ");
        
            while (RXS_RCOUNT_MASK & *ep->rxs) {
			    u8	byte = (u8) *ep->rxd;
                DBG(DBG_VERBOSE, "%02X ", byte );
                rx_count++;

			    if (unlikely (bufferspace == 0)) {
				    /* this happens when the driver's buffer
				     * is smaller than what the host sent.
				     * discard the extra data.
				     */
				    if (req->req.status != -EOVERFLOW)
					    DMSG("%s overflow %d\n",
						    ep->ep.name, rx_count);
				    req->req.status = -EOVERFLOW;
			    } else {
				    *buf++ = byte;
				    bufferspace--;
			    }
		    } 
            DBG(DBG_VERBOSE, "\n");


        count = rx_count;
                
		is_short = (count < ep->ep.maxpacket);
		
        DBG(DBG_VERBOSE, "read from %s,  %d bytes%s req %p %d/%d\n",
			ep->ep.name, count,
			is_short ? "/S" : "",
			req, req->req.actual, req->req.length);

        req->req.actual += rx_count; // sitel ?
        

		if (ep->bmAttributes == USB_ENDPOINT_XFER_ISOC) {
			is_short = 1;
		}

		/* completion */
		if (is_short || req->req.actual == req->req.length) {
            DBG(DBG_VERBOSE, "Executing done \n ");
			done (ep, req, 0);
			
            {
                *(ep->rxc) |= 0x1;
                DBG(DBG_VERBOSE, "read_fifo returned 1\n");
                return 1;
            }
            
		}

    /* finished that packet.  the next one may be waiting... */
    DBG(DBG_VERBOSE, "read_fifo returned 0\n");
	return 0;
}

 

static int
read_ep0_fifo (struct sc14450_ep *ep, struct sc14450_request *req)
{
	u8		*buf;
    // u8 byte;
	unsigned	bufferspace;

	buf = req->req.buf + req->req.actual;
	bufferspace = req->req.length - req->req.actual;

	while (RXS_RCOUNT_MASK & *ep->rxs) {
		u8	byte = (u8) *ep->rxd;

		if (unlikely (bufferspace == 0)) {
			/* this happens when the driver's buffer
			 * is smaller than what the host sent.
			 * discard the extra data.
			 */
			if (req->req.status != -EOVERFLOW)
				DMSG("%s overflow\n", ep->ep.name);
			req->req.status = -EOVERFLOW;
		} else {
			*buf++ = byte;
			req->req.actual++;
			bufferspace--;
		}
	}

	/* completion */
	if (req->req.actual >= req->req.length)
		return 1;

	/* finished that packet.  the next one may be waiting... */
	return 0;
}


static int
sc14450_ep_queue(struct usb_ep *_ep, struct usb_request *_req, gfp_t gfp_flags)
{
	struct sc14450_request	*req;
	struct sc14450_ep	*ep;
	struct sc14450_udc	*dev;
	unsigned long		flags;


    
    DBG(DBG_VERBOSE, "sc14450_ep_queue \n");
    
	req = container_of(_req, struct sc14450_request, req);
	if (unlikely (!_req || !_req->complete || !_req->buf
			|| !list_empty(&req->queue))) {
		DMSG("%s, bad params\n", __FUNCTION__);
		return -EINVAL;
	}
    DBG(DBG_VERBOSE, "sc14450_ep_queue 1\n");

	ep = container_of(_ep, struct sc14450_ep, ep);
	if (unlikely (!_ep || (!ep->desc && ep->ep.name != ep0name))) {
		DMSG("%s, bad ep\n", __FUNCTION__);
		return -EINVAL;
	}
    DBG(DBG_VERBOSE, "sc14450_ep_queue 2\n");

    DBG(DBG_VERBOSE, "sc14450_ep_queue starts for ep : %X \n", (u32) ep->desc);

	dev = ep->dev;
	if (unlikely (!dev->driver
			|| dev->gadget.speed == USB_SPEED_UNKNOWN)) {
		DMSG("%s, bogus device state\n", __FUNCTION__);
		return -ESHUTDOWN;
	}

    DBG(DBG_VERBOSE, "sc14450_ep_queue, stopped : %d \n", ep->stopped);

	/* iso is always one packet per request, that's the only way
	 * we can report per-packet status.  that also helps with dma.
	 */
	if (unlikely (ep->bmAttributes == USB_ENDPOINT_XFER_ISOC
			&& req->req.length > le16_to_cpu
						(ep->desc->wMaxPacketSize)))
		return -EMSGSIZE;


	DBG(DBG_VERBOSE, "%s queue req %p, len %d buf %p\n",
	     _ep->name, _req, _req->length, _req->buf);

	local_irq_save(flags);

	_req->status = -EINPROGRESS;
	_req->actual = 0;

	/* kickstart this i/o queue? */
	if (list_empty(&ep->queue) && !ep->stopped) {
		if (ep->desc == 0 /* ep0 */) {
			unsigned length = _req->length;

			switch (dev->ep0state) {
			case EP0_IN_DATA_PHASE:
                DBG(DBG_VERBOSE, "dev->ep0state: EP0_IN_DATA_PHASE\n");
				dev->stats.write.ops++;
				if (write_ep0_fifo(ep, req)) {// if short was sent
                    DBG(DBG_VERBOSE, "Short was sent , length = %d\n", req->req.length);    
					req = NULL;
                } else {
                    DBG(DBG_VERBOSE, "NOT Short was sent\n");    
                }
				break;

			case EP0_OUT_DATA_PHASE:
				dev->stats.read.ops++;
				
                DBG(DBG_VERBOSE, "sc14450_ep_queue\n");
				
                if (dev->req_pending) {
                    DBG(DBG_VERBOSE, "sc14450_ep_queue : dev->req_pending, length = %d \n", length);
                    
                    UdEpTxStart_zero(ep, NULL, 0);
                    
                }
                
                break;

			default:
				DMSG("ep0 i/o, odd state %d\n", dev->ep0state);
				local_irq_restore (flags);
				return -EL2HLT;
			}
		/* can the FIFO can satisfy the request immediately? */
		} else if ((ep->bEndpointAddress & USB_DIR_IN) != 0) { // IN 
			if ( write_fifo(ep, req))
				req = NULL;
		} else {
            u16 rxs = *(ep->rxs);
             if ( (rxs & RXS_RX_LAST) &&  read_fifo(ep, req, rxs)) {
			    req = NULL;
            }
		}

	}

	if (likely (req != 0))
		list_add_tail(&req->queue, &ep->queue);
	local_irq_restore(flags);

	return 0;
}


/*
 * 	nuke - dequeue ALL requests
 */
static void nuke(struct sc14450_ep *ep, int status)
{
	struct sc14450_request *req;

	/* called with irqs blocked */
	while (!list_empty(&ep->queue)) {
		req = list_entry(ep->queue.next,
				struct sc14450_request,
				queue);
		done(ep, req, status);
	}
}


/* dequeue JUST ONE request */
static int sc14450_ep_dequeue(struct usb_ep *_ep, struct usb_request *_req)
{
	struct sc14450_ep	*ep;
	struct sc14450_request	*req;
	unsigned long		flags;

	ep = container_of(_ep, struct sc14450_ep, ep);
	if (!_ep || ep->ep.name == ep0name)
		return -EINVAL;

	local_irq_save(flags);

	/* make sure it's actually queued on this endpoint */
	list_for_each_entry (req, &ep->queue, queue) {
		if (&req->req == _req)
			break;
	}
	if (&req->req != _req) {
		local_irq_restore(flags);
		return -EINVAL;
	}


    done(ep, req, -ECONNRESET);

	local_irq_restore(flags);
	return 0;
}


/*-------------------------------------------------------------------------*/

static int sc14450_ep_set_halt(struct usb_ep *_ep, int value)
{

    DBG(DBG_VERBOSE, "usb : sc14450_ep_set_halt \n");
	return 0;
}


static int sc14450_ep_fifo_status(struct usb_ep *_ep)
{
	struct sc14450_ep        *ep;

	ep = container_of(_ep, struct sc14450_ep, ep);
	if (!_ep) {
		DMSG("%s, bad ep\n", __FUNCTION__);
		return -ENODEV;
	}
	if ((ep->bEndpointAddress & USB_DIR_IN) != 0)
		return -EOPNOTSUPP;
	if (ep->dev->gadget.speed == USB_SPEED_UNKNOWN
            )
		return 0;
	else
		return (*ep->rxs & RXS_RCOUNT_MASK); // sitel
}



static void sc14450_ep_fifo_flush(struct usb_ep *_ep)
{
	struct sc14450_ep        *ep;

	ep = container_of(_ep, struct sc14450_ep, ep);
	if (!_ep || ep->ep.name == ep0name || !list_empty(&ep->queue)) {
		DMSG("%s, bad ep\n", __FUNCTION__);
		return;
	}

    *ep->rxc |= RXC_FLUSH;
    *ep->txc |= TXC_FLUSH;


}


static struct usb_ep_ops sc14450_ep_ops = {
	.enable		= sc14450_ep_enable,
	.disable	= sc14450_ep_disable,

	.alloc_request	= sc14450_ep_alloc_request,
	.free_request	= sc14450_ep_free_request,

	.alloc_buffer	= sc14450_ep_alloc_buffer,
	.free_buffer	= sc14450_ep_free_buffer,

	.queue		= sc14450_ep_queue,
	.dequeue	= sc14450_ep_dequeue,

	.set_halt	= sc14450_ep_set_halt,
	.fifo_status	= sc14450_ep_fifo_status,
	.fifo_flush	= sc14450_ep_fifo_flush,
};


/* ---------------------------------------------------------------------------
 * 	device-scoped parts of the api to the usb controller hardware
 * ---------------------------------------------------------------------------
 */

static int sc14450_udc_get_frame(struct usb_gadget *_gadget)
{
    u16 frame;
    
    // sitel return frame no
	// return ((UFNRH & 0x07) << 8) | (UFNRL & 0xff);
    
    frame = USB_FNL_REG;
    frame |= (USB_FNH_REG & USB_FN_10_8_MASK) << 8;
    
    return frame;

}

static int sc14450_udc_wakeup(struct usb_gadget *_gadget)
{
    
    DBG(DBG_VERBOSE, "sc14450_udc_wakeup\n");
    
	return 0;
}

static void stop_activity(struct sc14450_udc *, struct usb_gadget_driver *);
static void udc_enable (struct sc14450_udc *);
static void udc_disable(struct sc14450_udc *);

/* We disable the UDC -- and its 48 MHz clock -- whenever it's not
 * in active use.  
 */
static int pullup(struct sc14450_udc *udc, int is_active)
{

	DBG(DBG_VERBOSE, "pullup\n");
    is_active = is_active && udc->vbus && udc->pullup;
	DMSG("%s\n", is_active ? "active" : "inactive");
	DBG(DBG_VERBOSE, "%s %d %d %d \n", is_active ? "active" : "inactive", is_active , udc->vbus, udc->pullup );
	if (is_active)
		udc_enable(udc);
	else {
		if (udc->gadget.speed != USB_SPEED_UNKNOWN) {
			DMSG("disconnect %s\n", udc->driver
				? udc->driver->driver.name
				: "(no driver)");
			stop_activity(udc, udc->driver);
		}
		udc_disable(udc);
	}
	return 0;
}


/* VBUS reporting logically comes from a transceiver */
static int sc14450_udc_vbus_session(struct usb_gadget *_gadget, int is_active)
{
	struct sc14450_udc	*udc;

	udc = container_of(_gadget, struct sc14450_udc, gadget);
	udc->vbus = is_active = (is_active != 0);
	DMSG("vbus %s\n", is_active ? "supplied" : "inactive");
	pullup(udc, is_active);
	return 0;
}

/* drivers may have software control over D+ pullup */
static int sc14450_udc_pullup(struct usb_gadget *_gadget, int is_active)
{
	struct sc14450_udc	*udc;

    DBG(DBG_VERBOSE, "sc14450_udc_pullup\n");

	udc = container_of(_gadget, struct sc14450_udc, gadget);

    
	/* not all boards support pullup control */
	
    // sitel if (!udc->mach->udc_command)
    // return -EOPNOTSUPP;

	is_active = (is_active != 0);
	udc->pullup = is_active;
	pullup(udc, is_active);
	return 0;
}

static const struct usb_gadget_ops sc14450_udc_ops = {
	.get_frame	= sc14450_udc_get_frame,
	.wakeup		= sc14450_udc_wakeup,
	.vbus_session	= sc14450_udc_vbus_session,
	.pullup		= sc14450_udc_pullup,

};




#define create_proc_files() do {} while (0)
#define remove_proc_files() do {} while (0)


/* "function" sysfs attribute */
static ssize_t
show_function (struct device *_dev, struct device_attribute *attr, char *buf)
{
	struct sc14450_udc	*dev = dev_get_drvdata (_dev);

	if (!dev->driver
			|| !dev->driver->function
			|| strlen (dev->driver->function) > PAGE_SIZE)
		return 0;
	return scnprintf (buf, PAGE_SIZE, "%s\n", dev->driver->function);
}

static DEVICE_ATTR (function, S_IRUGO, show_function, NULL);

/*-------------------------------------------------------------------------*/

/*
 * 	udc_disable - disable USB device controller
 */
static void udc_disable(struct sc14450_udc *dev)
{
    pullup_off();
    ep0_idle (dev);
	dev->gadget.speed = USB_SPEED_UNKNOWN;
	LED_CONNECTED_OFF;

}


/*
 * 	udc_reinit - initialize software state
 */
static void udc_reinit(struct sc14450_udc *dev)
{
	u32	i;

	/* device/ep0 records init */
	INIT_LIST_HEAD (&dev->gadget.ep_list);
	INIT_LIST_HEAD (&dev->gadget.ep0->ep_list);
	dev->ep0state = EP0_IDLE;
    DBG(DBG_VERBOSE, "udc_reinit : dev->ep0state --> EP0_IDLE \n");
    

	/* basic endpoint records init */
	for (i = 0; i < SC14450_UDC_NUM_ENDPOINTS; i++) {
		struct sc14450_ep *ep = &dev->ep[i];

		if (i != 0)
			list_add_tail (&ep->ep.ep_list, &dev->gadget.ep_list);

		ep->desc = NULL;
		ep->stopped = 0;
		INIT_LIST_HEAD (&ep->queue);
		ep->pio_irqs = ep->dma_irqs = 0;
        // sitel kda
        ep->TxBusy=0;
	}

	/* the rest was statically initialized, and is read-only */
}




void UdLowLevelInit(void)
/*======================================
 * Inputs         None.
 * Returns        None.
 *
 * Description    May be called during low-level HW initialization.
 *                Makes sure that the USB chip is properly disabled.
 *
 *======================================
 */
{
  LockType lock;
  AcquireLock(lock);
  // INT1_PRIORITY_REG_bit.USB_INT_PRIO = 0;
  INT1_PRIORITY_REG &= ~USB_INT_PRIO_MASK;
  USB_MCTRL_REG = 0;
  ReleaseLock(lock);
}

void UdInit(struct sc14450_udc *dev)
/*======================================
 * Inputs         None.
 * Returns        None.
 *
 * Description    Initialize the USB device driver & chip.
 *
 *======================================
 */
{

  unsigned i,temp;
  LockType lock;

  DBG(DBG_VERBOSE, "Entering UdInit\n");
  
  // PLL programming

  CLK_PLL2_DIV_REG  = 0x201b;
  CLK_PLL2_CTRL_REG = 0x0012;

  for(i = 0; i < 0x200; ++i)
		temp = *(volatile unsigned*)0xff4000;
  CLK_PLL2_CTRL_REG = 0x001a;
 
  
  UdLowLevelInit();

  // Power up USB hardware.

  USB_MCTRL_REG |= USBEN;

  // Configure interrupt sources.

  //#ifdef SC14450
  //USB_TXMSK_REG_bit.USB_M_TXFIFO31 = 0x7;
  USB_TXMSK_REG = (USB_TXMSK_REG & ~USB_M_TXFIFO31_MASK) | 0x7;
  
  // USB_RXMSK_REG_bit.USB_M_RXFIFO31 = 0x7;
  USB_RXMSK_REG = (USB_RXMSK_REG & ~USB_M_RXFIFO31_MASK) | 0x7;
  //#endif

//   USB_MAMSK_REG_bit.USB_M_EP0_NAK = 1;
//   USB_MAMSK_REG_bit.USB_M_EP0_RX = 1;
//   USB_MAMSK_REG_bit.USB_M_EP0_TX = 1;
//   USB_MAMSK_REG_bit.USB_M_RX_EV = 1;
//   USB_MAMSK_REG_bit.USB_M_ULD = 1;
//   USB_MAMSK_REG_bit.USB_M_NAK = 1;
//   USB_MAMSK_REG_bit.USB_M_FRAME = 1;
//   USB_MAMSK_REG_bit.USB_M_TX_EV = 1;
//   USB_MAMSK_REG_bit.USB_M_ALT = 1;

  USB_MAMSK_REG  |=  USB_M_EP0_NAK ;
  USB_MAMSK_REG  |=  USB_M_EP0_RX ;
  USB_MAMSK_REG  |=  USB_M_EP0_TX ;
  USB_MAMSK_REG  |=  USB_M_RX_EV ;
  USB_MAMSK_REG  |=  USB_M_ULD ;
  USB_MAMSK_REG  |=  USB_M_NAK ;
  USB_MAMSK_REG  |=  USB_M_FRAME ;
  USB_MAMSK_REG  |=  USB_M_TX_EV ;
  USB_MAMSK_REG  |=  USB_M_ALT ;


  // Enable interrupts.
  AcquireLock(lock);

  // INT1_PRIORITY_REG_bit.USB_INT_PRIO = USB_INTERRUPT_PRIORITY;
  INT1_PRIORITY_REG = (INT1_PRIORITY_REG & ~USB_INT_PRIO_MASK) | (/*USB_INTERRUPT_PRIORITY*/1<<12);

  RESET_INT_PENDING_REG = (1<<11) /*USB_INT_PEND*/;

  // USB_MAMSK_REG_bit.USB_M_INTR = 1;
  USB_MAMSK_REG |= USB_M_INTR ;

  ReleaseLock(lock);
  DBG(DBG_VERBOSE, "Exiting UdInit\n");

}


typedef enum
{
  NFSR_NODE_RESET,
  NFSR_NODE_RESUME,
  NFSR_NODE_OPERATIONAL,
  NFSR_NODE_SUSPEND
} NfsrType;
static volatile NfsrType UdNfsr;

typedef struct usb_endpoint_descriptor UsbEndpointDescriptorType;
typedef struct usb_device_descriptor UsbDeviceDescriptorType;
typedef struct usb_config_descriptor UsbConfigDescriptorType;
typedef struct usb_interface_descriptor UsbInterfaceDescriptorType;
typedef enum usb_device_state UsbDeviceStateType;


UsbDeviceStateType UsbDeviceState;


void UdBusAttach(void)
/*======================================
 * Inputs         None.
 * Returns        None.
 *
 * Description    Attach to USB bus.
 *
 *======================================
 */
{
  u8 state = SaveUsbInt();

  USB_MCTRL_REG |= USB_NAT;

  USB_FAR_REG = USB_FAR_REG & (~USB_AD_MASK);
  
  USB_FAR_REG |= USB_AD_EN ;

  USB_NFSR_REG = UdNfsr = NFSR_NODE_RESET;
  printk("UdBusAttach : UdNfsr -> %d \n",UdNfsr);

  USB_ALTMSK_REG|=USB_M_RESUME;
  USB_ALTMSK_REG|=USB_M_RESET;
  USB_ALTMSK_REG&=~USB_M_SD5;
  USB_ALTMSK_REG|=USB_M_SD3;

  RestoreUsbInt(state);

}


void UdEpConfigure(struct sc14450_udc *dev, u8 EpNr, bool ZeroTerminate, const UsbEndpointDescriptorType* Config)
/*======================================
 * Inputs         EpNr: The endpoint number to configure.
 *                ZeroTerminate: Set TRUE to enable zero termination of transfers that are
 *                an exact multiple of the endpoint packet size.
 *                Config: Reference to the endpoint descriptor.
 * Returns        None.
 *
 * Description    Configure an endpoint in the USB driver.
 *
 *======================================
 */
{
  struct sc14450_ep *ep = &dev->ep[EpNr];
  volatile u16* epc = ep->epc;

  DBG(DBG_VERBOSE, "UdEpConfigure ep : %d\n", EpNr);

  ep->ZeroTerminate = ZeroTerminate;
  ep->Toggle = 0;

  if (Config)
  {
    ep->Type = Config->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK;
    ep->fifo_size = Config->wMaxPacketSize;
    *epc = Config->bEndpointAddress & EPC_EP_MASK;
    if (ep->Type == USB_ENDPOINT_XFER_ISOC)
    {
      *epc |= EPC_ISO;
      if ((Config->bEndpointAddress & USB_ENDPOINT_DIR_MASK) == USB_DIR_IN)
      {
        *(ep->txc) |= TXC_IGN_ISOMSK;
      }
    }
  }
  else
  {
    ep->Type = USB_ENDPOINT_XFER_CONTROL;
    ep->fifo_size = 8;
  }
  *epc |= EPC_EP_EN;
}

void UsbInit(struct sc14450_udc *dev)
/*======================================
 * Inputs         None.
 * Returns        None.
 *
 * Description    Initialize USB function and driver.
 *                
 *======================================
 */
{
  UsbDeviceState = USB_STATE_NOTATTACHED;
  printk("UsbDeviceState -> USB_STATE_NOTATTACHED\n");

  DBG(DBG_VERBOSE, "Inside UsbInit \n");
  UdInit(dev);

 
  UsbDeviceState = USB_STATE_ATTACHED;
  printk("UsbDeviceState -> USB_STATE_ATTACHED\n");
  UdBusAttach();
  
}




void UdBusDetach(void)
/*======================================
 * Inputs         None.
 * Returns        None.
 *
 * Description    Detach from USB bus.
 *
 *======================================
 */
{
  u8 state = SaveUsbInt();
  USB_NFSR_REG = UdNfsr = NFSR_NODE_RESET;
  printk("UdBusDetach : UdNfsr -> %d \n",UdNfsr);

  USB_MCTRL_REG     &=  ~USB_NAT;
  USB_ALTMSK_REG    &=  ~USB_M_RESUME;
  USB_ALTMSK_REG    &=  ~USB_M_RESET;
  USB_ALTMSK_REG    &=  ~USB_M_SD5;
  USB_ALTMSK_REG    &=  ~USB_M_SD3;
  RestoreUsbInt(state);
}



/* until it's enabled, this UDC should be completely invisible
 * to any USB host.
 */
static void udc_enable (struct sc14450_udc *dev)
{

    DBG(DBG_VERBOSE,"udc_enable\n");

	ep0_idle(dev);
	dev->gadget.speed = USB_SPEED_UNKNOWN;
	dev->stats.irqs = 0;

	/* if hardware supports it, pullup D+ and wait for reset */
    
    UsbInit(dev);
    pullup_on();

}

static inline void clear_ep_state (struct sc14450_udc *dev)
{
	unsigned i;

	for (i = 1; i < SC14450_UDC_NUM_ENDPOINTS; i++)
		nuke(&dev->ep[i], -ECONNABORTED);
}


static void udc_watchdog(unsigned long _dev)
{
	struct sc14450_udc	*dev = (void *)_dev;

	local_irq_disable();
	if (dev->ep0state == EP0_STALL) {
		DBG(DBG_VERBOSE, "ep0 re-stall\n");
		start_watchdog(dev);
	}
	local_irq_enable();
    
}


void UdBusAddress(u8 Address)
/*======================================
 * Inputs         USB bus address.
 * Returns        None.
 *
 * Description    Set new USB bus address.
 *
 *======================================
 */
{
  u8 state;

  printk("UdBusAddress : %d\n", Address);

  state = SaveUsbInt();

  USB_EPC0_REG |= USB_DEF;
  USB_FAR_REG   = (USB_FAR_REG & ~USB_AD_MASK) | Address;
  USB_FAR_REG  |= USB_AD_EN;

  RestoreUsbInt(state);

}

static void SetAddressReq(struct sc14450_udc *dev, u8 addr)
/*======================================
 * Inputs         None.
 * Returns        None.
 *
 * Description    Handle USB SET ADDRESS request.
 *                
 *======================================
 */
{
  printk("--- SetAddressReq \n ");
  if (UsbDeviceState == USB_STATE_DEFAULT || UsbDeviceState == USB_STATE_ADDRESS)
  {
    printk("--- SetAddressReq OK \n ");

    UdBusAddress(addr);
    {volatile u32 delay=1000; while (delay--);}
    UdEpTxStart_zero(&dev->ep[0], 0, 0);
    
    // ep0start(dev, 0, "address");
    
    /// TxStart(dev,0);
    
    UsbDeviceState = USB_STATE_ADDRESS;
    printk("UsbDeviceState -> USB_STATE_ADDRESS \n");
    
    return;
  }
}


void show_dbg(struct sc14450_udc *dev, const char *msg){
    struct sc14450_ep	    *ep = &dev->ep[0];
    DBG(DBG_VERBOSE, "***** SHOW %s ******\n",msg);
    DBG(DBG_VERBOSE, "epc= %04X\n",*ep->epc);
    DBG(DBG_VERBOSE, "txc= %04X\n",*ep->txc);
    //DBG(DBG_VERBOSE, "txs= %04X\n",*ep->txs);
    DBG(DBG_VERBOSE, "rxc= %04X\n",*ep->rxc);
    // DBG(DBG_VERBOSE, "rxs= %04X\n",*ep->rxs);
}


void UdEpStall(struct sc14450_ep *ep)
/*======================================
 * Inputs         The endpoint number to stall.
 * Returns        None.
 *
 * Description    Stall an endpoint.
 *
 *======================================
 */
{
  volatile u16* epc = ep->epc;
  u8 state = SaveUsbInt();
  *epc |= EPC_STALL;
  RestoreUsbInt(state);
}


void UdEp0Stall(struct sc14450_ep *ep)
/*======================================
 * Inputs         None.
 * Returns        None.
 *
 * Description    Stall endpoint zero.
 *
 *======================================
 */
{
  u8 state;
  UdEpStall(ep);
  UdEpTxStart_zero(ep, NULL, 0);
  state = SaveUsbInt();
  ep->TxBusy = 0;
  RestoreUsbInt(state);
}




static void handle_ep0 (struct sc14450_udc *dev) // 1 : tx
{


    u16 rxs0;
    // u16 txs0;


    

	struct sc14450_ep	*ep = &dev->ep [0];
	struct sc14450_request	*req;
	
    union {
		struct usb_ctrlrequest	r;
		u8			raw [8];
		u32			word [2];
	} u;


    rxs0 = USB_RXS0_REG;
    if (!(rxs0 & RXS_RX_LAST) ) return; // exit when no successfull rx

    DBG(DBG_VERBOSE, "\n handle_ep0 start, rxs = %X, rxc=%X \n", rxs0, USB_RXC0_REG );
    
    // when a succesfull rx has occured, update Toggle flag
    
    if (rxs0 & 0xF) {  // not zero length packet
        if (rxs0 & (1<<5)) { // Toggle rx
            ep->Toggle = 0;
            DBG(DBG_VERBOSE, "RxToggle = 1\n");
        } else {
            ep->Toggle = 1;
            DBG(DBG_VERBOSE, "RxToggle = 0\n");
        }
    }
    
    
	if (list_empty(&ep->queue)) {
        DBG(DBG_VERBOSE, "req NULL");
		req = NULL;
	} else
		req = list_entry(ep->queue.next, struct sc14450_request, queue);



	/* clear stall status */ 
	if (*ep->epc & EPC_STALL) {
        DBG(DBG_VERBOSE, " rxs0 & EPC_STALL \n");
		nuke(ep, -EPIPE);
		*ep->epc &= ~EPC_STALL;
		del_timer(&dev->timer);
		ep0_idle(dev);
	}


	switch (dev->ep0state) {
	case EP0_IDLE:
        DBG(DBG_VERBOSE, "handle_ep0 : dev->ep0state = EP0_IDLE, rxs = %X\n", rxs0 );
        
		/* late-breaking status? */

		/* start control request? */
		if (likely((rxs0 & RXS_SETUP))) {
			int i;
            DBG(DBG_VERBOSE, "handle_ep0 : RXS_SETUP\n");
            ep->Toggle = 1;
            ep->ZeroTerminate = 1;

			nuke (ep, -EPROTO);

			/* read SETUP packet */
			for (i = 0; i < 8; i++) {
				u.raw [i] = (u8) USB_RXD0_REG;
			}

got_setup:
			DBG(DBG_VERBOSE, "SETUP %02x.%02x v%04x i%04x l%04x\n",
				u.r.bRequestType, u.r.bRequest,
				le16_to_cpu(u.r.wValue),
				le16_to_cpu(u.r.wIndex),
				le16_to_cpu(u.r.wLength));

			/* cope with automagic for some standard requests. */
			dev->req_std = (u.r.bRequestType & USB_TYPE_MASK)
						== USB_TYPE_STANDARD;
			dev->req_config = 0;
			dev->req_pending = 1;
			switch (u.r.bRequest) {
			/* hardware restricts gadget drivers here! */
			case USB_REQ_SET_CONFIGURATION:
                DBG(DBG_VERBOSE, "USB_REQ_SET_CONFIGURATION\n");
				if (u.r.bRequestType == USB_RECIP_DEVICE) {
                    DBG(DBG_VERBOSE, "USB_RECIP_DEVICE\n");
					/* reflect hardware's automagic
					 * up to the gadget driver.
					 */
config_change:
					dev->req_config = 1;
					clear_ep_state(dev);
                    //UsbDeviceState = USB_STATE_CONFIGURED;
                    //printk("UsbDeviceState -> USB_STATE_CONFIGURED\n");


				}
				break;
			/* ... and here, even more ... */
			case USB_REQ_SET_INTERFACE:
                DBG(DBG_VERBOSE, "USB_REQ_SET_INTERFACE\n");
				if (u.r.bRequestType == USB_RECIP_INTERFACE) {
					goto config_change;
				}
				break;
			/* hardware was supposed to hide this */
			case USB_REQ_SET_ADDRESS:
                DBG(DBG_VERBOSE, "USB_REQ_SET_ADDRESS: %d\n", le16_to_cpu(u.r.wValue));
				if (u.r.bRequestType == USB_RECIP_DEVICE) {
					
                    DBG(DBG_VERBOSE, "USB_REQ_SET_ADDRESS\n");

                    SetAddressReq(dev,le16_to_cpu((u8) u.r.wValue));
                    // dev->ep[0].Toggle = 0;

					return;
				}
				break;
			}

			if (u.r.bRequestType & USB_DIR_IN) {
				dev->ep0state = EP0_IN_DATA_PHASE;
                DBG(DBG_VERBOSE, "handle_ep0 : dev->ep0state --> EP0_IN_DATA_PHASE \n");
                
			} else {
				dev->ep0state = EP0_OUT_DATA_PHASE;
                DBG(DBG_VERBOSE, "handle_ep0 : dev->ep0state --> EP0_OUT_DATA_PHASE \n");
            }

			i = dev->driver->setup(&dev->gadget, &u.r);
            DBG(DBG_VERBOSE, "ev->driver->setup returned %d \n", i);
            
            if (i < 0) {
                DBG(DBG_VERBOSE, "dev->driver->setup returned < 0 \n");

				/* preventing STALL... */
				if (dev->req_config) {
					WARN("config change %02x fail %d?\n",
						u.r.bRequest, i);
					return;
				}
				DBG(DBG_VERBOSE, "protocol STALL %d\n", i);
stall:

               
				UdEp0Stall(&dev->ep[0]);
                // start_watchdog(dev);
				dev->ep0state = EP0_STALL;
                DBG(DBG_VERBOSE, "handle_ep0 : dev->ep0state --> EP0_STALL \n");

			}
			/* expect at least one data or status stage irq */
			return;

		} 

		break;
	case EP0_IN_DATA_PHASE:			/* GET_DESCRIPTOR etc */
        DBG(DBG_VERBOSE,"handle_ep0 : dev->ep0state = EP0_IN_DATA_PHASE \n");
			if (req) {
				(void) write_ep0_fifo(ep, req);
			} 
		break;
	case EP0_OUT_DATA_PHASE:		/* SET_DESCRIPTOR etc */
			if (req) {
				if (read_ep0_fifo(ep, req))
					done(ep, req, 0);
			} 
		break;
	case EP0_END_XFER:
   		if (req)
			done(ep, req, 0);
		ep0_idle(dev);
		break;
	case EP0_STALL:
        DBG(DBG_VERBOSE,"handle_ep0 : EP0_STALL\n");
		break;
	}

}


static void handle_ep_rx(struct sc14450_ep *ep)
{

	struct sc14450_request	*req;
    u16 rxs = *(ep->rxs);

    DBG(DBG_VERBOSE, "handle_ep_rx\n");

    
    if (!(rxs & RXS_RX_LAST)) {

        DBG(DBG_VERBOSE, "NO PACKET\n");

        return; // packet not completed yet
    }
    

	if (likely (!list_empty(&ep->queue)))
		req = list_entry(ep->queue.next,
				struct sc14450_request, queue);
	else
		req = NULL;


	/* fifos can hold packets, ready for reading... */
	if (likely(req)) {
		read_fifo(ep, req, rxs);
	} 

    *(ep->rxc) |=1;

}


typedef enum
{
  UBE_SUSPEND,  // 3 ms suspend detected.
  UBE_RWKUP_OK, // 5 ms suspend detected, remote wakeup allowed.
  UBE_RESUME,   // Resume detected.
  UBE_RESET,    // Reset detected.
  UBE_MAX
} UdBusEventType;



void Sd5Event(void)
/*======================================
 * Inputs         None.
 * Returns        None.
 *
 * Description    Process SD5 interrupt.
 *
 *======================================
 */
{
  ;
}


u32 frame_count=0;
void UdBusFrame(u16 FrameNr)
/*======================================
 * Inputs         Current USB frame number (0..7FF).
 * Returns        None.
 *
 * Description    Callback with current USB frame.
 *                
 *======================================
 */
{
  frame_count++;
  
  #if 0
  if ( (frame_count % 5000) == 2 )
    DBG(DBG_VERBOSE, "Frame Event\n");
  #endif
  
}


void FrameEvent(void)
/*======================================
 * Inputs         None.
 * Returns        None.
 *
 * Description    Process frame interrupt.
 *
 *======================================
 */
{
  u16 frame;
  frame = USB_FNL_REG;
  frame |= (USB_FNH_REG & USB_FN_10_8_MASK) << 8;
  UdBusFrame(frame);

  USB_ALTMSK_REG |= USB_M_RESET;

  
}

static void TxDone_1(u8 EpNr, struct sc14450_ep *ep)
/*======================================
 * Inputs         Endpoint number and data.
 * Returns        None.
 *
 * Description    Check if TX is active for the endpoint, and complete it.
 *
 *======================================
 */
{
  
  DBG(DBG_VERBOSE, "TxDone\n");

  if (ep->TxBusy)
  {
    ep->TxBusy = 0;
  }

  ep0_idle(ep->dev); // ???
  
}


void UdEpDisable(struct sc14450_udc	*dev, u8 EpNr, bool ClearToggle)
/*======================================
 * Inputs         The endpoint number to disable.
 * Returns        None.
 *
 * Description    Disable an endpoint. Any pending RX/TX operations are cancelled.
 *
 *======================================
 */
{
  struct sc14450_ep *ep = &dev->ep[EpNr];
  u8 state = SaveUsbInt();
  if (ep->txc)
  {
    *(ep->txc) &= ~TXC_TX_EN;
    *(ep->txc) |= TXC_FLUSH;
    if (*(ep->txs))
    {
      *(ep->txs) = 0;
    }
    TxDone_1(EpNr, ep);
  }
  if (ep->rxc)
  {
    *(ep->rxc) &= ~RXC_RX_EN;
    *(ep->rxc) |= RXC_FLUSH;
    if (*(ep->rxs))
    {
      *(ep->rxs) = 0;
    }
    // RxDone(EpNr, ep);
  }
  if (ClearToggle)
  {
    ep->Toggle = 0;
  }
  RestoreUsbInt(state);
}

void UdEpRxEnable(struct sc14450_udc *dev, u8 EpNr)
/*======================================
 * Inputs         The endpoint number to enable.
 * Returns        None.
 *
 * Description    Enable receive for an endpoint.
 *
 *======================================
 */
{
  struct sc14450_ep *ep = &dev->ep[EpNr];
  volatile u16* rxc = ep->rxc;

  u8 state = SaveUsbInt();
  
  DBG(DBG_VERBOSE, "UdEpRxEnable ep: %d\n", EpNr);
  
  // if (ep->Rx.MaxSize == 0)
  {

    if (EpNr != 0)
    {
      DBG(DBG_VERBOSE, "  UdEpRxEnable OK %d \n", EpNr);
      *rxc |= RXC_IGN_SETUP;
    }
    *rxc |= RXC_RX_EN;
  
  }
  
  RestoreUsbInt(state);
}


void UdBusEvent(struct sc14450_udc	*dev, UdBusEventType Event)
/*======================================
 * Inputs         The event that occured.
 * Returns        None.
 *
 * Description    Bus event callback.
 *                
 *======================================
 */
{
  u8 i;
  
  switch (Event)
  {
    case UBE_SUSPEND:
    case UBE_MAX:
    case UBE_RWKUP_OK:
      //// DBG(DBG_VERBOSE, "UdBusEvent UBE_RWKUP_OK\n");
      if (UsbDeviceState == USB_STATE_CONFIGURED)
      {
        UsbDeviceState = USB_STATE_SUSPENDED;
        printk("UsbDeviceState -> USB_STATE_SUSPENDED\n");
        for (i=0; i < 3; i++)
        {
          UdEpDisable(dev, i, 0/*FALSE*/);
        }
        ///// DfcAdd(UsbDisableApp, 0);
      }
      break;

    case UBE_RESUME:
      //// DBG(DBG_VERBOSE, "UdBusEvent UBE_RESUME\n");
      if (UsbDeviceState == USB_STATE_SUSPENDED)
      {
        UsbDeviceState = USB_STATE_CONFIGURED;
        printk("UsbDeviceState -> USB_STATE_CONFIGURED\n");
        UdEpRxEnable(dev, 0);
      }
      else
      {
        UsbDeviceState = USB_STATE_POWERED;
        printk("UsbDeviceState -> USB_STATE_POWERED\n");
      }
      break;

    case UBE_RESET:
      //// DBG(DBG_VERBOSE, "UdBusEvent UBE_RESET\n");
      if (UsbDeviceState != USB_STATE_DEFAULT)
      {
        UsbDeviceState = USB_STATE_DEFAULT;
        printk("UsbDeviceState -> USB_STATE_DEFAULT\n");
        for (i=0; i < 3; i++)
        {
          UdEpDisable(dev, i, 1/*TRUE*/);
        }
        UdBusAddress(0);
        UdEpRxEnable(dev,0);
        ///// DfcAdd(UsbDisableApp, 0);
      }
      break;
  }
}


void ResetEvent(struct sc14450_udc	*dev)
/*======================================
 * Inputs         None.
 * Returns        None.
 *
 * Description    Process reset interrupt.
 *
 *======================================
 */
{
  USB_NFSR_REG = UdNfsr = NFSR_NODE_RESET;
    DBG(DBG_VERBOSE, "ResetEvent : UdNfsr -> %d \n",UdNfsr);

  // USB_ALTMSK_REG_bit.USB_M_RESET = 0;
  USB_ALTMSK_REG  &= ~USB_M_RESET;
  
  USB_NFSR_REG = UdNfsr = NFSR_NODE_OPERATIONAL;
    DBG(DBG_VERBOSE, "ResetEvent : UdNfsr -> %d \n",UdNfsr);

  UdBusEvent(dev, UBE_RESET);
}

void ResumeEvent(struct sc14450_udc	*dev)
/*======================================
 * Inputs         None.
 * Returns        None.
 *
 * Description    Process resume interrupt.
 *
 *======================================
 */
{
  //USB_ALTMSK_REG_bit.USB_M_RESET = 1;
  
  USB_ALTMSK_REG  |= USB_M_RESET;
  
  if (UdNfsr == NFSR_NODE_SUSPEND)
  {
    USB_NFSR_REG = UdNfsr = NFSR_NODE_OPERATIONAL;
      DBG(DBG_VERBOSE, "ResumeEvent : UdNfsr -> %d \n",UdNfsr);

    UdBusEvent(dev, UBE_RESUME);
  }
}


void Sd3Event(struct sc14450_udc	*dev)
/*======================================
 * Inputs         None.
 * Returns        None.
 *
 * Description    Process SD3 interrupt.
 *
 *======================================
 */
{
  
  //USB_ALTMSK_REG_bit.USB_M_RESET = 1;
  USB_ALTMSK_REG |= USB_M_RESET;
  
  if (UdNfsr == NFSR_NODE_OPERATIONAL)
  {
    USB_NFSR_REG = UdNfsr = NFSR_NODE_SUSPEND;
      DBG(DBG_VERBOSE, "Sd3Event : UdNfsr -> %d \n",UdNfsr);


    UdBusEvent(dev, UBE_SUSPEND);
	
    DBG(DBG_VERBOSE, "USB suspend%s\n", is_vbus_present()
		? "" : "+disconnect");

    if (!is_vbus_present())
        stop_activity(dev, dev->driver);
    else if (dev->gadget.speed != USB_SPEED_UNKNOWN
					&& dev->driver
					&& dev->driver->suspend)
        dev->driver->suspend(&dev->gadget);
    
    ep0_idle (dev);
            
    
  }
 
}


void UdEpNak(struct sc14450_udc	*dev, u8 EpNr)
/*======================================
 * Inputs         The endpoint number that a NAK was generated for.
 * Returns        None.
 *
 * Description    Endpoint NAK callback.
 *                
 *======================================
 */
{
  if (EpNr == 0)
  {
    UdEpDisable(dev, 0, 0 /*FALSE*/);
    UdEpRxEnable(dev, 0);
  }
}


void NakEventEp0(struct sc14450_udc	*dev)
/*======================================
 * Inputs         None.
 * Returns        None.
 *
 * Description    Check for NAK interrupt from endpoints 0.
 *
 *======================================
 */
{
  struct sc14450_ep	*ep = &dev->ep[0];

  u8 nak = USB_EP0_NAK_REG;
  if (nak & 0x02)
  {
    // DBG(DBG_VERBOSE,"NakEventEp0 : epc=%X, txc=%X, txs=%X, rxc=%X\n",*(ep->epc),  *(ep->txc), *(ep->txs), *(ep->rxc) );

    // struct sc14450_ep* ep = &dev->ep[0];
    if (ep->TxBusy)
    {
      UdEpNak(dev, 0);
    }
  }
}


int sd3_event = 0;
int rx0_event = 0;
int reset_event = 0;
int resume_event = 0;


u8 rx_data[32];

int ep0_state = 0;
int ep0_remain;
int ep0_pos=0;


int nbytes;

int tx_active = 0;



void TxEp(struct sc14450_udc *dev, u8 EpNr)
/*======================================
 * Inputs         Endpoint number.
 * Returns        None.
 *
 * Description    Continue or complete TX for the endpoint.
 *
 *======================================
 */
{
  
  struct sc14450_request *req;
  u8 txs;
  int count;

  // EpDataType* ep = &UsbEndpoints[EpNr];
  struct sc14450_ep *ep = &(dev->ep[EpNr]);
  
  // const EpRegsType* er = &EpRegs[EpNr];
  struct sc14450_ep *er = &(dev->ep[EpNr]);
 
  
  if (list_empty(&ep->queue))
    req = NULL;
  else
	req = list_entry(ep->queue.next, struct sc14450_request, queue);
    
  if ((EpNr == 1) && trigger_tx_end) {
    DBG(DBG_VERBOSE,"trigger_tx_end, %X , %X \n", (int) req, (int) req_to_end);
    if (req != req_to_end) req = req_to_end;
  }
    
  txs = *(er->txs);
  
  if (EpNr == 0) DBG(DBG_VERBOSE,"TxEp %d : txs = %d\n", EpNr, txs);

  if (txs & TXS_TX_DONE)
  {
    if (EpNr == 0) DBG(DBG_VERBOSE,"  TXS_TX_DONE \n");
    if (txs & TXS_ACK_STAT || ep->Type == USB_ENDPOINT_XFER_ISOC)
    {
      if (EpNr == 0) DBG(DBG_VERBOSE,"Toggle : %d -> %d \n", ep->Toggle, !ep->Toggle);
      ep->Toggle = !ep->Toggle;
      
      
      if (req->req.actual < req->req.length )
      {
        count = TxFill(ep, req);
        if (EpNr != 0) ep->dev->stats.write.bytes += count;
        if (EpNr == 0) DBG(DBG_VERBOSE,"        TxFill 1 \n");
      }
      else if (ep->ZeroTerminate && (req->req.length-req->req.actual) == ep->fifo_size)
      {
        count = TxFill(ep, req);
        if (EpNr != 0) ep->dev->stats.write.bytes += count;
        if (EpNr == 0) DBG(DBG_VERBOSE,"        TxFill 2 \n");
      }
      else
      {
        TxDone(ep,req);
        if (EpNr == 0) DBG(DBG_VERBOSE,"        TxDone 1\n");
      }
    }
    else
    {
      if (ep->TxBusy)
      {
        if (EpNr == 0)
        {
          TxDone(ep,req);
          if (EpNr == 0) DBG(DBG_VERBOSE,"        TxDone 2\n");
        }
        else
        {
          // If we didnt get an ACK, refill fifo.
          *(er->txc) |= TXC_RFF | TXC_LAST | TXC_TX_EN;
          if (EpNr == 0) DBG(DBG_VERBOSE,"        no ACK \n");
        }
        // UdErr.TxRff++;
      }
    }
  }
}


static irqreturn_t sc14450_udc_irq(int irq, void *_dev)
{

    struct sc14450_udc	*dev = _dev;
    int handled;

  
    dev->stats.irqs++;

 
    // do 
    
    {
        u16 maev = USB_MAEV_REG & USB_MAMSK_REG;
		handled = 0;

        if (maev & MAEV_ALT) {

            u8 altev = USB_ALTEV_REG & USB_ALTMSK_REG;
            // DBG(DBG_VERBOSE," usb_check:   maev = %X, altev  = %X \n", maev, altev);

            if (altev & ALTEV_SD3)  {
                handled=1;
                // UdStat.Sd3++;
                Sd3Event(dev);
            }
            
            if (altev & ALTEV_SD5) {
                // UdStat.Sd5++;
                Sd5Event();
            }
            
            if (altev & ALTEV_RESET) {
                // printk("ALTEV_RESET\n");

                //UdStat.Reset++;
                ResetEvent(dev);
                stop_activity (dev, dev->driver);
                dev->gadget.speed = USB_SPEED_FULL;
			    memset(&dev->stats, 0, sizeof dev->stats);
                
            }
            
            if (altev & ALTEV_RESUME) {
                // printk("ALTEV_RESUME\n");

                // UdStat.Resume++;

                ResumeEvent(dev);
                
               
                #if 0
                if (dev->gadget.speed != USB_SPEED_UNKNOWN
					&& dev->driver
					&& dev->driver->resume
					/*&& is_vbus_present()*/)
                {
                    DBG(DBG_VERBOSE,"GADGET RESUME OK!\n");
                } else {
                    DBG(DBG_VERBOSE,"GADGET RESUME PROBLEM \n");
                    //printk(" dev->gadget.speed     %X %X\n", dev->gadget.speed,     USB_SPEED_UNKNOWN  );
                    //printk(" dev->driver           %X\n", (int) dev->driver            );
                    //printk(" dev->driver->resume   %X\n", (int) dev->driver->resume    );
                    //printk(" is_vbus_present()     %X\n", is_vbus_present()      );
                          
                }
                #endif
                
                
            }
        
        }
        
        if (maev & MAEV_FRAME) {
            handled = 1;
            FrameEvent();
        }
        
        if (maev & MAEV_NAK) {
            handled = 1;
            // NakEvent();
            printk(" MAEV_NAK\n");
        }
        
        if (maev & MAEV_TX_EV) {
            // printk(" MAEV_TX_EV\n");
            handled = 1;
            TxEp(dev, 1);
            { volatile u16 delay=300; while(delay--);}

        }
        
        if (maev & MAEV_RX_EV) {
            // printk(" MAEV_RX_EV\n");
            handle_ep_rx(&(dev->ep[2]));
            // { volatile u16 delay=100; while(delay--);}
            handled = 1;

        }
        if (maev & MAEV_EP0_TX) {
            // printk("\n\n\nMAEV_EP0_TX\n");
            handled = 1;
   			// handle_ep0_tx(dev);
            TxEp(dev, 0);
            { volatile u32 delay=30000; while(delay--);}
        }
        
        if (maev & MAEV_EP0_NAK) {
            volatile u16 delay=10000;
            handled = 1;
            NakEventEp0(dev);
            while(delay--);
            // printk("MAEV_EP0_NAK\n");
        }

        if (maev & MAEV_EP0_RX) {
            // printk(" \n\n\nMAEV_EP0_RX\n");
			dev->ep[0].pio_irqs++;
			
            handle_ep0(dev);
            
            // handle_ep0_rx(dev);
            
            // RxEp0(dev);
			
            handled = 1;
            // printk("rxs = %d", ret_val);
        }  
    
    } 
    
    //while (handled);
 
 
  RESET_INT_PENDING_REG = (1<<11);
  return IRQ_HANDLED;

}
// End of file.



/*-------------------------------------------------------------------------*/

static void nop_release (struct device *dev)
{
	DMSG("%s %s\n", __FUNCTION__, dev->bus_id);
}

/* this uses load-time allocation and initialization (instead of
 * doing it at run-time) to save code, eliminate fault paths, and
 * be more obviously correct.
 */
static struct sc14450_udc memory = {
	.gadget = {
		.ops		= &sc14450_udc_ops,
		.ep0		= &memory.ep[0].ep,
		.name		= driver_name,
		.dev = {
			.bus_id		= "gadget",
			.release	= nop_release,
		},
	},

	/* control endpoint */
	.ep[0] = {
		.ep = {
			.name		= ep0name,
			.ops		= &sc14450_ep_ops,
			.maxpacket	= EP0_FIFO_SIZE,
		},
		.dev		= &memory,
        .fifo_size	= EP0_FIFO_SIZE,
		.epc        =  (volatile u16 *)USB_EPC0_REG_POS,
		.txc        =  (volatile u16 *)USB_TXC0_REG_POS,
        .txs        =  (volatile u16 *)USB_TXS0_REG_POS,
        .txd        =  (volatile u16 *)USB_TXD0_REG_POS,
        .rxc        =  (volatile u16 *)USB_RXC0_REG_POS,
        .rxs        =  (volatile u16 *)USB_RXS0_REG_POS,
        .rxd        =  (volatile u16 *)USB_RXD0_REG_POS
 
	},

	/* first group of endpoints */
	.ep[1] = {
		.ep = {
			.name		= "ep1in-bulk",
			.ops		= &sc14450_ep_ops,
			.maxpacket	= BULK_FIFO_SIZE,
		},
		.dev		= &memory,
		.fifo_size	= BULK_FIFO_SIZE,
		.bEndpointAddress = USB_DIR_IN | 1,
		.bmAttributes	= USB_ENDPOINT_XFER_BULK,

        .epc        =  (volatile u16 *)USB_EPC1_REG_POS,
        .txc        =  (volatile u16 *)USB_TXC1_REG_POS,
        .txs        =  (volatile u16 *)USB_TXS1_REG_POS,
        .txd        =  (volatile u16 *)USB_TXD1_REG_POS,
        .rxc        =  NULL,
        .rxs        =  NULL,
        .rxd        =  NULL

	},

	.ep[2] = {
		.ep = {
			.name		= "ep2out-bulk",
			.ops		= &sc14450_ep_ops,
			.maxpacket	= BULK_FIFO_SIZE,
		},
		.dev		= &memory,
		.fifo_size	= BULK_FIFO_SIZE,
		.bEndpointAddress = 2,
		.bmAttributes	= USB_ENDPOINT_XFER_BULK,
        .epc        =  (volatile u16 *)USB_EPC2_REG_POS,
        .txc        =  NULL,
        .txs        =  NULL,
        .txd        =  NULL,
        .rxc        = (volatile u16 *)USB_RXC1_REG_POS,
        .rxs        = (volatile u16 *)USB_RXS1_REG_POS,
        .rxd        = (volatile u16 *)USB_RXD1_REG_POS

	}
};


/*
 * 	probe - binds to the platform device
 */
static int __init sc14450_udc_probe(struct platform_device *pdev)
{
	struct sc14450_udc *dev = &memory;
	int retval, out_dma = 1, vbus_irq;
	// u32 chiprev;

    DBG(DBG_VERBOSE,"sc14450_udc_probe\n");

    out_dma = 0;
    
	pr_debug("%s: IRQ %d\n", driver_name, 11);

	printk("%s: IRQ %d\n", driver_name, 11);

	/* other non-static parts of init */
	dev->dev = &pdev->dev;
	dev->mach = pdev->dev.platform_data;

    DBG(DBG_VERBOSE,"sc14450_udc_probe 2 \n");

    vbus_irq = 0;

	init_timer(&dev->timer);
	dev->timer.function = udc_watchdog;
	dev->timer.data = (unsigned long) dev;

	device_initialize(&dev->gadget.dev);
	dev->gadget.dev.parent = &pdev->dev;
	dev->gadget.dev.dma_mask = pdev->dev.dma_mask;

	the_controller = dev;
	platform_set_drvdata(pdev, dev);

	udc_disable(dev);
	udc_reinit(dev);

	dev->vbus = is_vbus_present();

	/* irq setup after old hardware state is cleaned up */
	retval = request_irq(11, sc14450_udc_irq,
			IRQF_DISABLED, driver_name, dev);
	if (retval != 0) {
		printk(KERN_ERR "%s: can't get irq %i, err %d\n",
			driver_name, 11, retval);
		return -EBUSY;
	} else {
        printk("%s: got irq %i, err %d\n",
			driver_name, 11, retval);
    }
	dev->got_irq = 1;

    DBG(DBG_VERBOSE,"sc14450_udc_probe: create_proc_files files ... \n");

    create_proc_files();

	return 0;
}

static void sc14450_udc_shutdown(struct platform_device *_dev)
{
	pullup_off();
}

static int __exit sc14450_udc_remove(struct platform_device *pdev)
{
	struct sc14450_udc *dev = platform_get_drvdata(pdev);

	udc_disable(dev);
	remove_proc_files();
	usb_gadget_unregister_driver(dev->driver);

	if (dev->got_irq) {
		free_irq(11, dev);
		dev->got_irq = 0;
	}
	
    platform_set_drvdata(pdev, NULL);
	the_controller = NULL;
	return 0;
}

/*-------------------------------------------------------------------------*/
// #if 1




static int sc14450_udc_suspend(struct platform_device *dev, pm_message_t state)
{
	struct sc14450_udc	*udc = platform_get_drvdata(dev);

    printk("sc14450_udc_suspend\n");
	
    Sd3Event(udc);
	
    pullup(udc, 0);

	return 0;
}

static int sc14450_udc_resume(struct platform_device *dev)
{
	// struct sc14450_udc	*udc = platform_get_drvdata(dev);

    DBG(DBG_VERBOSE,"sc14450_udc_resume\n");
    
    // ResumeEvent(udc);
    
	return 0;

}


/*-------------------------------------------------------------------------*/

static struct platform_driver udc_driver = {
	.probe		= sc14450_udc_probe,
	.shutdown	= sc14450_udc_shutdown,
	.remove		= __exit_p(sc14450_udc_remove),
	.suspend	= sc14450_udc_suspend,
	.resume		= sc14450_udc_resume,
	.driver		= {
		.owner	= THIS_MODULE,
		.name	= "sc14450-udc",
	},
};


/* These don't need to do anything because the pdev structures are
 * statically allocated. */

static const char	udc_gadget_name [] = "sc14450-udc";


static void
sc14450_udc_release (struct device *dev) {}

static struct platform_device		the_udc_pdev = {
	.name		= (char *) udc_gadget_name,
	.id		= -1,
	.dev		= {
		.release	= sc14450_udc_release,
	},
};


static int __init udc_init(void)
{
    int retval;
	printk(KERN_INFO "%s: version %s\n", driver_name, DRIVER_VERSION);

	retval = platform_driver_register(&udc_driver);
    
    DBG(DBG_VERBOSE,"udc_init 1\n");

    if (retval<0)  {
        DBG(DBG_VERBOSE,"Error in platform_driver_register\n");
        return retval;
    }

    DBG(DBG_VERBOSE,"udc_init 2\n");
	retval = platform_device_register(&the_udc_pdev);
    DBG(DBG_VERBOSE,"udc_init 3 %d\n",retval);
    return retval;
    
}

module_init(udc_init);

static void __exit udc_exit(void)
{
	platform_driver_unregister(&udc_driver);
}
module_exit(udc_exit);



int usb_gadget_register_driver (struct usb_gadget_driver *driver)
{
	struct sc14450_udc	*udc = the_controller;
	int		retval;

	if (!driver
			|| driver->speed != USB_SPEED_FULL
			|| !driver->bind
			|| !driver->unbind
			|| !driver->setup) {
		return -EINVAL;
	}
    
    DBG(DBG_VERBOSE,"usb_gadget_register_driver 1\n");
    
	if (udc->driver) {
		DBG(DBG_VERBOSE,"udc already has a gadget driver\n");
		return -EBUSY;
	}

	udc->driver = driver;
	udc->gadget.dev.driver = &driver->driver;
	udc->gadget.dev.driver_data = &driver->driver;
	//udc->enabled = 1;
	// udc->selfpowered = 1;

	retval = driver->bind(&udc->gadget);
	
    if (retval) {
		DBG(DBG_VERBOSE,"driver->bind() returned %d\n", retval);
		udc->driver = NULL;
        udc->gadget.dev.driver = NULL;
		return retval;
	}

	local_irq_disable();
    
    udc->pullup = 1;
    
	pullup(udc, 1);
	
    local_irq_enable();

	printk("bound to %s\n", driver->driver.name);
	return 0;
}

EXPORT_SYMBOL(usb_gadget_register_driver);

static void
stop_activity(struct sc14450_udc *dev, struct usb_gadget_driver *driver)
{
	int i;


    DBG(DBG_VERBOSE, "udc: stop_activity \n");
    
	/* don't disconnect drivers more than once */
	if (dev->gadget.speed == USB_SPEED_UNKNOWN)
		driver = NULL;
	dev->gadget.speed = USB_SPEED_UNKNOWN;

	/* prevent new request submissions, kill any outstanding requests  */
	for (i = 0; i < SC14450_UDC_NUM_ENDPOINTS; i++) {
		struct sc14450_ep *ep = &dev->ep[i];

		ep->stopped = 1;
		nuke(ep, -ESHUTDOWN);
	}
	del_timer_sync(&dev->timer);

	/* report disconnect; the driver is already quiesced */
	LED_CONNECTED_OFF;
	if (driver)
		driver->disconnect(&dev->gadget);

	/* re-init driver-visible data structures */
	udc_reinit(dev);
}

int usb_gadget_unregister_driver(struct usb_gadget_driver *driver)
{
	struct sc14450_udc	*dev = &memory;

	if (!dev)
		return -ENODEV;
	if (!driver || driver != dev->driver)
		return -EINVAL;

	local_irq_disable();
	pullup(dev, 0);
	stop_activity(dev, driver);
	local_irq_enable();

	driver->unbind(&dev->gadget);
	dev->driver = NULL;

	device_del (&dev->gadget.dev);
	// device_remove_file(dev->dev, &dev_attr_function);

	DMSG("unregistered gadget driver '%s'\n", driver->driver.name);
	// dump_state(dev);
	return 0;
}
EXPORT_SYMBOL(usb_gadget_unregister_driver);


MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_AUTHOR("Sitel Semi");
MODULE_LICENSE("GPL");

