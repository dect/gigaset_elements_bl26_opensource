/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <george.giannaras@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 * lcd_st7529.c -- lcd char driver
 *Based on the code from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.
 *
 *
 */

/***************       Include headers section       ***************/

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#ifdef LCD_TEST_PC
	#include <asm/uaccess.h>
#else
	#include <asm-cr16/regs.h>
	#include <asm-cr16/uaccess.h>
#endif
#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */

#include <linux/proc_fs.h> /*semaphores*/
#include <linux/cdev.h> /*cdev*/
#include <linux/wait.h>
#include <linux/delay.h>	/* delays */
#include <linux/spinlock.h>


//#include <linux/interrupt.h>/*irq*/
#include "fonts.h"
#include "lcd_st7529.h"


const unsigned char ReverseLookupTable[] =

{
     0x00, 0x80, 0x40, 0xC0, 0x20, 0xA0, 0x60, 0xE0, 0x10, 0x90, 0x50, 0xD0, 0x30, 0xB0, 0x70, 0xF0,
     0x08, 0x88, 0x48, 0xC8, 0x28, 0xA8, 0x68, 0xE8, 0x18, 0x98, 0x58, 0xD8, 0x38, 0xB8, 0x78, 0xF8,
     0x04, 0x84, 0x44, 0xC4, 0x24, 0xA4, 0x64, 0xE4, 0x14, 0x94, 0x54, 0xD4, 0x34, 0xB4, 0x74, 0xF4,
     0x0C, 0x8C, 0x4C, 0xCC, 0x2C, 0xAC, 0x6C, 0xEC, 0x1C, 0x9C, 0x5C, 0xDC, 0x3C, 0xBC, 0x7C, 0xFC,
     0x02, 0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62, 0xE2, 0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2, 0x72, 0xF2,
     0x0A, 0x8A, 0x4A, 0xCA, 0x2A, 0xAA, 0x6A, 0xEA, 0x1A, 0x9A, 0x5A, 0xDA, 0x3A, 0xBA, 0x7A, 0xFA,
     0x06, 0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6, 0x16, 0x96, 0x56, 0xD6, 0x36, 0xB6, 0x76, 0xF6,
     0x0E, 0x8E, 0x4E, 0xCE, 0x2E, 0xAE, 0x6E, 0xEE, 0x1E, 0x9E, 0x5E, 0xDE, 0x3E, 0xBE, 0x7E, 0xFE,
     0x01, 0x81, 0x41, 0xC1, 0x21, 0xA1, 0x61, 0xE1, 0x11, 0x91, 0x51, 0xD1, 0x31, 0xB1, 0x71, 0xF1,
     0x09, 0x89, 0x49, 0xC9, 0x29, 0xA9, 0x69, 0xE9, 0x19, 0x99, 0x59, 0xD9, 0x39, 0xB9, 0x79, 0xF9,
     0x05, 0x85, 0x45, 0xC5, 0x25, 0xA5, 0x65, 0xE5, 0x15, 0x95, 0x55, 0xD5, 0x35, 0xB5, 0x75, 0xF5,
     0x0D, 0x8D, 0x4D, 0xCD, 0x2D, 0xAD, 0x6D, 0xED, 0x1D, 0x9D, 0x5D, 0xDD, 0x3D, 0xBD, 0x7D, 0xFD,
     0x03, 0x83, 0x43, 0xC3, 0x23, 0xA3, 0x63, 0xE3, 0x13, 0x93, 0x53, 0xD3, 0x33, 0xB3, 0x73, 0xF3,
     0x0B, 0x8B, 0x4B, 0xCB, 0x2B, 0xAB, 0x6B, 0xEB, 0x1B, 0x9B, 0x5B, 0xDB, 0x3B, 0xBB, 0x7B, 0xFB,
     0x07, 0x87, 0x47, 0xC7, 0x27, 0xA7, 0x67, 0xE7, 0x17, 0x97, 0x57, 0xD7, 0x37, 0xB7, 0x77, 0xF7,
     0x0F, 0x8F, 0x4F, 0xCF, 0x2F, 0xAF, 0x6F, 0xEF, 0x1F, 0x9F, 0x5F, 0xDF, 0x3F, 0xBF, 0x7F, 0xFF
};


/***************       Definitions section             ***************/
#define BUFFER_SIZE 50 /*Buffer size to support the ioctl with the largest data transfer*/
#define MAX_IOCTL_ARGS ((SCRN_RIGHT+1)*(SCRN_BOTTOM+1)+10)/*Maximum number of args to pass with ioctl*/
#define LCD_DEFAULT_BACKLIGHT_LEVEL 0x7F
#define MAX_SCROLL_TEXT_SIZE 4000

//define this if you want to benchmark the lcd driver
//#define BENCHMARK
/***      Data types definitions       ***/
#define uchar unsigned char
//Driver structs
typedef struct {
	struct lcd_sc14450_conf_struct lcd_conf;
	struct semaphore sem;     /* mutual exclusion semaphore     */
	spinlock_t lock;
	struct cdev cdev;	  	  /* Char device structure		*/
	//uchar LCD_buf[Y_BYTES][X_BYTES]; /*Buffer size to support the ioctl with the largest data transfer*/
}lcd_sc14450_dev_struct;

typedef struct {
	char left;
	char top;
	char font;
	char visible_size;
	int  size ;
	char text[MAX_SCROLL_TEXT_SIZE];
	int ScrollPos;
	int enable;
}scroll_struct;
scroll_struct m_scroll_struct;
typedef struct {
	uchar left;
	uchar top;
	uchar font;
	uchar blinking_rate;
	uchar state;
	uchar enabled;
	uchar reserved[2];
}cursor_struct;

typedef struct {
	uchar left;
	uchar top;
	uchar width;
 	uchar blinking_rate;
	uchar state;
	uchar enabled;
	uchar bitmaps;
	uchar reserved;
	uchar  bitmap[4][128];
}animation_struct;

/***      Flags definitions     ***/
/***      Hw specific definitions       ***/

/***   Macros   ***/
#define	START_TIMER(TIMER, TMOUT) {TIMER.expires = jiffies + TMOUT;add_timer(&TIMER);}
#define	STOP_TIMER(TIMER) {del_timer(&TIMER);}
/***      Function Prototypes       ***/
/** Low level **/

/* LCD function prototype list */
void lcd_init(void);
void lcd_update32(uchar top, uchar height, uchar left, uchar width);
void lcd_clear_ram(void);
void lcd_print_screen(unsigned char *screen);
void lcd_clear_rect(uchar left,  uchar top, uchar right, uchar bottom);
void lcd_invert_rect(uchar left, uchar top, uchar right, uchar bottom);
void lcd_horz_line(uchar left, uchar right, uchar row);
void lcd_vert_line(uchar top, uchar bottom, uchar column);
void lcd_clr_horz_line(uchar left, uchar right, uchar row);
void lcd_clr_vert_line(uchar top, uchar bottom, uchar column);
void lcd_draw_border(uchar left, uchar top, uchar right, uchar bottom);
void lcd_clear_border(uchar left, uchar top, uchar right, uchar bottom);
void lcd_glyph(uchar left, uchar top, uchar width, uchar height, uchar *glyph, uchar store_width);
void lcd_animated_glyph(uchar left, uchar top, uchar width, uchar num_of_bitmaps, uchar *glyph1, uchar *glyph2, uchar *glyph3);
void lcd_text(uchar left, uchar top, uchar font, char *str);
void lcd_update(uchar top, uchar bottom);
void lcd_cursor_set(uchar left, uchar top, uchar font, uchar ShowCursor, uchar blinking_rate);
/**/
void lcd_contrast_level(uchar level);
void lcd_contrast_up(void);
void lcd_contrast_down(void);
void lcd_sleepmode_enter(void);
void lcd_sleepmode_exit(void);
void lcd_scroll_init(char *buf);
 uchar lcd_font_width_get(uchar font_width);
/*Static functions*/
static void LCD_setup_cdev(lcd_sc14450_dev_struct *dev);
//Timers
static void InitializeTimer(struct timer_list *timer_ptr, void (*timer_function)(unsigned long param));
static void lcd_timer_refresh_fn(unsigned long param);
static void scroll_timer_refresh(unsigned long param);
static void cursor_timer_refresh(unsigned long param);
static void animation_timer_refresh(unsigned long param);

//Module specific
static int __init LCD_init_module(void);
void  LCD_cleanup_module(void);

// Fops prototypes
int	LCD_open(struct inode *inode, struct file *filp);
int LCD_release(struct inode *inode, struct file *filp);
int LCD_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg);
/*** Parameters which can be set at load time  ***/
int lcd_sc14450_major =   LCD_SC14450_MAJOR;
int lcd_sc14450_minor =   0;


module_param(lcd_sc14450_major, int, S_IRUGO);
module_param(lcd_sc14450_minor, int, S_IRUGO);


MODULE_AUTHOR("George Giannaras");
MODULE_LICENSE("Dual BSD/GPL");
//#define AUTO_REFRESH
/***      Var definitions  and initializations     ***/
struct lcd_sc14450_conf_struct lcd_init_conf = {
	/* Set lcd default configuration */
#ifdef AUTO_REFRESH
	.refresh_tmout=500,
#else
	.refresh_tmout=0,
#endif
};
struct file_operations lcd_sc14450_fops = {
	.owner =    THIS_MODULE,
	.ioctl =    LCD_ioctl,
	.open =     LCD_open,
	.release =  LCD_release,
};
lcd_sc14450_dev_struct lcd_dev;

struct lcd_sc14450_conf_struct *lcd_conf = &(lcd_dev.lcd_conf);
const unsigned char l_mask_array[8] ={0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
uchar l_display_array[Y_BYTES][X_BYTES];

cursor_struct m_cursor;

#define MAX_ANIMATED_BITMAPS 8
animation_struct m_animation[MAX_ANIMATED_BITMAPS];

/***      Timer initializations     ***/
struct timer_list lcd_timer;
struct timer_list scroll_timer;
struct timer_list cursor_timer;
struct timer_list animation_timer;
 /***      Tasklet     ***/
//HW Patch

/***************          Main body             ***************/
unsigned char scr_top_border, scr_bottom_border, scr_left_border, scr_right_border, scr_left_dim, scr_right_dim;
unsigned char scr_left_offset, scr_right_offset;
/****************************************************************************************************/
/************                                        LOW LEVEL DRIVER FUNTIONS                                                            *********/
/****************************************************************************************************/

/*******************************************************
	Description: 	Initializes hw and driver internal state
	Note:
 ********************************************************/
void lcd_init_col_lines(unsigned char screen_top, unsigned char screen_bottom, unsigned char screen_left, unsigned char screen_right){
	unsigned char tmp;
	//Setup the line\column borders
	lcd_out_ctl( 0x0030 ); //Ext = 0

	if( (screen_bottom<=screen_top) || (screen_right<=screen_left))
		return;

	//Check if dimensions not alligned to 2pixels or 3 pixels
	scr_top_border =  screen_top;
	scr_bottom_border = screen_bottom;
	scr_left_border = screen_left;
	scr_right_border = screen_right;

	scr_left_dim =  screen_left/3;
	scr_left_offset =  screen_left%3;
	if(screen_right%3){
		scr_right_dim =  screen_right/3;
		scr_right_offset =  3-screen_right%3;
	}
	else{
		scr_right_dim =  screen_right/3;
		scr_right_offset =  0;
	}

#ifdef INVERSE_DISPLAY
	tmp = scr_top_border;
	scr_top_border = 159 - SCRN_BOTTOM + scr_top_border;
	scr_bottom_border = 159 - SCRN_BOTTOM + scr_bottom_border;
	tmp = scr_left_dim;
	scr_left_dim = 84 - (SCRN_RIGHT/3) + scr_left_dim;
	scr_right_dim = 84 - (SCRN_RIGHT/3) + scr_right_dim;
#endif

	lcd_out_ctl( 0x0075 ); //Line Address Set
	lcd_out_dat( scr_top_border ); //Start Line
	lcd_out_dat( scr_bottom_border); //End Line
#if (defined(CONFIG_L_V4_BOARD) || defined(CONFIG_L_V4_BOARD_CNX))
	scr_left_dim =  scr_left_dim;
	scr_right_dim = scr_right_dim;
#endif
	lcd_out_ctl( 0x0015 ); //Column Address Set
	lcd_out_dat( scr_left_dim ); //Start Column=0
	lcd_out_dat( scr_right_dim); //End Column =84 //columns represent 16-bit words = 3 pixels

	PDEBUG("Set borders to TOP: %d, BOTTOM: %d, LEFT: %d, RIGHT: %d \n", scr_top_border, scr_bottom_border, scr_left_dim, scr_right_dim);
	PDEBUG("Set offsets to LEFT: %d, RIGHT: %d \n", scr_left_offset, scr_right_offset);
}

#if (defined(CONFIG_L_V2_BOARD) || defined(CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD)\
		||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX))
		#define set_col2(X) \
	({\
			register short __r4 __asm__ ("r4") = (short)(X) ; \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"nop \n\t"\
    			"movd $0x1F:m,(r1,r0)\n\t"\
    			"1: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 1b\n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"nop \n\t"\
    			"nop \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1090000:l,(r1,r0) \n\t"\
    			"storw	r4,0x0:(r1,r0) \n\t"\
    			"nop \n\t"\
    			"movd $0x1F:m,(r1,r0)\n\t"\
    			"2: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 2b\n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
    			"nop \n\t"\
    			"nop \n\t"\
			    :"=r" (__r4) \
			    : "0" (__r4) \
			    : "r0","r1","r2","r3"); \
			 \
		})
		#define RESET_LCD {set_col2((0xFF)|((0)<<8)|(0<<13));}
		#define SET_LCD {set_col2((0xFF)|0x4000|((0)<<8)|(0<<13));}
#endif

void lcd_init(){
	unsigned char i;
	/*Processor specific commands*/
	OVER_16M_SDRAM_ACCESS_PATCH_FLAG;

#ifndef CONFIG_RAM_SIZE_32MB
	EBI_ACS4_CTRL_REG = 9;
#endif

	PROTECT_OVER_16M_SDRAM_ACEESS
	//setup timings
#if !(defined(CONFIG_L_V2_BOARD) || defined(CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD)\
		||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX))
	EBI_SMTMGR_SET2_REG=0xe278;
	#elif defined SLOWBUSTIMING
	EBI_SMTMGR_SET2_REG=0x7e278;
	#elif defined FASTBUSTIMING
	EBI_SMTMGR_SET2_REG=0x72d8a;
	#elif defined VFASTBUSTIMING
	EBI_SMTMGR_SET2_REG=0x72689;
#endif
#if (defined(CONFIG_L_V2_BOARD) || defined(CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD))
	// setup lcd interface
	SetPort(P2_10_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_ACS3);        /* P2_06 is ACS3 */
	EBI_ACS3_LOW_REG = LCD_BASE;    /* ACS3 base address */
	EBI_ACS3_CTRL_REG = 0x221;      /* use time setting 1, memory is sram, size is 64K  */

	//setup latch
	SetPort(P2_09_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_ACS2);        /* P2_09 is ACS2 */
	EBI_ACS2_CTRL_REG= 0x121;						/* use time setting 1, memory is sram, size is 64K */
	EBI_ACS2_LOW_REG=0x01090000;					/* ACS2 base address is 0x1090000 */
#elif defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	// setup lcd interface
	SetPort(P2_06_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_ACS3);        /* P2_06 is ACS3 */
	EBI_ACS3_LOW_REG = LCD_BASE;    /* ACS3 base address */
	EBI_ACS3_CTRL_REG = 0x221;      /* use time setting 1, memory is sram, size is 64K  */

	//setup latch
	SetPort(P0_02_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_ACS2);        /* P2_09 is ACS2 */
	EBI_ACS2_CTRL_REG= 0x121;						/* use time setting 1, memory is sram, size is 64K */
	EBI_ACS2_LOW_REG=0x01090000;					/* ACS2 base address is 0x1090000 */
#endif

#if !(defined(CONFIG_L_V2_BOARD) || defined(CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD)\
		||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX))
	(*(volatile unsigned short *)(LCD_LATCH)=(0x2000));//Set backlight on, rst low
	#endif
	RESTORE_OVER_16M_SDRAM_ACEESS

	mdelay(100);

	PROTECT_OVER_16M_SDRAM_ACEESS
	#if !(defined(CONFIG_L_V2_BOARD) || defined(CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD)\
			||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX))

	(*(volatile unsigned short *)(LCD_LATCH)=(0x6000));//Set backlight on, rst high
	#endif
	RESTORE_OVER_16M_SDRAM_ACEESS
	mdelay(1);

//LG driver
	#if (defined(CONFIG_L_V2_BOARD) || defined(CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD)\
			||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX))
		RESET_LCD;
		PDEBUG ("Resetting the LCD \r\n");
		mdelay(100);
		SET_LCD;
		mdelay(10);
	#endif
	lcd_out_ctl( 0x0030 ); //Ext = 0
	lcd_out_ctl( 0x0094 ); //Sleep Out
	lcd_out_ctl( 0x00D1 ); //OSC On
	mdelay( 5 );
	lcd_out_ctl( 0x0020 ); //Power Control Set
	lcd_out_dat( 0x000B ); //Booster Must Be On First
	mdelay( 10 );
//	lcd_out_ctl( 0x0020 ); //Power Control Set
//	lcd_out_dat( 0x000B ); //Booster, Regulator, Follower ON
//	lcd_out_ctl( 0x0081 ); //Electronic Control
//	lcd_out_dat( 0x002f);//18v  //04 ); //Vop=14.0V
//	lcd_out_dat( 0x0002 );//0x04

	lcd_out_ctl( 0x00CA ); //Display Control
	lcd_out_dat( 0x0004 ); //CL=X1
#if !(defined(CONFIG_L_V4_BOARD) || defined(CONFIG_L_V4_BOARD_CNX))
	lcd_out_dat( 0x000D ); //Duty//0x13
#else
	lcd_out_dat( 0x0024 ); //Duty//0x13
#endif
	lcd_out_dat( 0x0010 ); //FR Inverse-Set Value

	//lcd_out_ctl( 0x00A7 ); // A6: Normal Display, A7: Inverse Display
#if !(defined(CONFIG_L_V4_BOARD) || defined(CONFIG_L_V4_BOARD_CNX))
	lcd_out_ctl( 0x00BB ); //COM Scan Direction
	lcd_out_dat( 0x0000 ); // 0>79 159>80
#else
	lcd_out_ctl( 0x00BB ); //COM Scan Direction
	lcd_out_dat( 0x0002 ); // 0>79 159>80
#endif

	lcd_out_ctl( 0x0020 ); //Power Control Set
	lcd_out_dat( 0x0003 ); //Booster Must Be On First

	lcd_out_ctl( 0x0081 ); //Electronic Control
#if (defined (CONFIG_L_V2_BOARD) ||defined(CONFIG_L_V2_BOARD_CNX))
	lcd_out_dat( 0x000a);//18v  //04 ); //Vop=14.0V
	lcd_out_dat( 0x0002 );//0x04
#elif defined (CONFIG_L_V5_BOARD)||defined (CONFIG_L_V5_BOARD_CNX)
	lcd_out_dat( 0x000f);//18v  //04 ); //Vop=14.0V
	lcd_out_dat( 0x0002 );//0x04
#elif defined (CONFIG_L_V4_BOARD)||defined (CONFIG_L_V4_BOARD_CNX)
	lcd_out_dat( 0x0018);//18v  //04 ); //Vop=14.0V
	lcd_out_dat( 0x0003 );//0x04
#endif

	mdelay( 5 );
	lcd_out_ctl( 0x00A7 ); // A6: Normal Display, A7: Inverse Display

	lcd_out_ctl( 0x0031 ); //Ext = 1
	lcd_out_ctl( 0x0032 ); //Analog Circuit Set
	lcd_out_dat( 0x0005 ); //OSC Frequency =000 (Default)
	lcd_out_dat( 0x0001 ); //Booster Efficiency=01(Default)
	lcd_out_dat( 0x0005 ); //Bias=1/9
	mdelay( 10 );
	lcd_out_ctl( 0x0022 ); //WEIGHTING
	lcd_out_dat( 0x0003 ); //
	lcd_out_dat( 0x0002 ); //
	lcd_out_dat( 0x0000 ); //
	lcd_out_ctl( 0x0034 ); //Software Initial
	mdelay( 10 );
	lcd_out_ctl( 0x0020 );
	lcd_out_dat( 0x00 );
	lcd_out_dat( 0x03 );
	lcd_out_dat( 0x06 );
	lcd_out_dat( 0x09 );

	lcd_out_dat( 0x0B );
	lcd_out_dat( 0x0D );
	lcd_out_dat( 0x0E );
	lcd_out_dat( 0x0F );

	lcd_out_dat( 0x10 );
	lcd_out_dat( 0x11 );
	lcd_out_dat( 0x12 );
	lcd_out_dat( 0x14 );

	lcd_out_dat( 0x16 );
	lcd_out_dat( 0x18 );
	lcd_out_dat( 0x1B );
	lcd_out_dat( 0x1F );

	lcd_out_ctl( 0x0021 );

	lcd_out_dat( 0x00 );
	lcd_out_dat( 0x03 );
	lcd_out_dat( 0x06 );
	lcd_out_dat( 0x09 );

	lcd_out_dat( 0x0B );
	lcd_out_dat( 0x0D );
	lcd_out_dat( 0x0E );
	lcd_out_dat( 0x0F );

	lcd_out_dat( 0x10 );
	lcd_out_dat( 0x11 );
	lcd_out_dat( 0x12 );
	lcd_out_dat( 0x14 );

	lcd_out_dat( 0x16 );
	lcd_out_dat( 0x18 );
	lcd_out_dat( 0x1B );
	lcd_out_dat( 0x1F );

#ifdef INVERSE_DISPLAY
	lcd_out_ctl( 0x0030 ); //Ext = 0
	lcd_out_ctl( 0x00BC ); //Data Scan Direction
	lcd_out_dat( 0x0003 ); //Normal: 0, Inverted line/col: 3
	lcd_out_dat( 0x0001 ); //RGB Arrangement
#ifdef B3P3_MODE
	lcd_out_dat( 0x0002 ); //2:3B3P mode, 1: 2B3P mode
#else
	lcd_out_dat( 0x0001 ); //2:3B3P mode, 1: 2B3P mode
#endif
	lcd_out_ctl( 0x0075 ); //Line Address Set
	lcd_out_dat( 159 - SCRN_BOTTOM/*0x10*/ ); //Start Line
	lcd_out_dat( 159/*SCRN_BOTTOM*//*0x8f*/ ); //End Line
	lcd_out_ctl( 0x0015 ); //Column Address Set
	lcd_out_dat( 84 - ((SCRN_RIGHT+1)/3 -1)/*SCRN_LEFT */); //Start Column=0
	lcd_out_dat( 84 /* (SCRN_RIGHT+1)/3 -1*//*0x54*/ ); //End Column =84 //columns represent 16-bit words = 3 pixels
#else
	lcd_out_ctl( 0x0030 ); //Ext = 0
	lcd_out_ctl( 0x00BC ); //Data Scan Direction
	lcd_out_dat( 0x0002 ); //Normal: 0, Inverted line/col: 3
	lcd_out_dat( 0x0001 ); //RGB Arrangement
#ifdef B3P3_MODE
	lcd_out_dat( 0x0002 ); //2:3B3P mode, 1: 2B3P mode
#else
	lcd_out_dat( 0x0001 ); //2:3B3P mode, 1: 2B3P mode
#endif
	lcd_out_ctl( 0x0075 ); //Line Address Set
	lcd_out_dat( SCRN_TOP ); //Start Line
	lcd_out_dat( SCRN_BOTTOM); //End Line
	lcd_out_ctl( 0x0015 ); //Column Address Set
	lcd_out_dat( SCRN_LEFT ); //Start Column=0
	lcd_out_dat( (SCRN_RIGHT+1)/3 -1); //End Column =84 //columns represent 16-bit words = 3 pixels
#endif
	mdelay( 5 );
	mdelay(100);
	lcd_out_ctl( 0x0030 ); //Ext = 0
	lcd_out_ctl( 0x00AF ); //Display On

//================================
	lcd_clear_ram();
#if !(defined(CONFIG_L_V2_BOARD) || defined(CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD)\
	||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX))
	lcd_print_screen(sitel_logo_glyph_table);
#endif
	/*This code is not part of the hw init sequence*/
 	m_cursor.state =0;
	m_cursor.enabled =0;
	for (i=0;i<MAX_ANIMATED_BITMAPS;i++)
	{
		m_animation[i].state = 0;
		m_animation[i].enabled = 0;
		m_animation[i].bitmaps = 0;
	}

	InitializeTimer(&animation_timer, animation_timer_refresh);
	InitializeTimer(&cursor_timer, cursor_timer_refresh);
	InitializeTimer(&scroll_timer, scroll_timer_refresh);
}

void lcd_print_screen(unsigned char *screen)
{
	unsigned char x, y, z;
	unsigned char width=128, y_offset = 10;
	#ifndef B3P3_MODE
		return;
	#endif
	lcd_init_col_lines(SCRN_TOP, SCRN_BOTTOM, SCRN_LEFT, SCRN_RIGHT );
	lcd_out_ctl( 0x005C ); 					//Enter memory write mode
	z=40;									//(scr_right_border-width)/2;
	for( y = 0; y <SCRN_BOTTOM; y++)
	{
		for (x=0; x < z; x++)
		{
			lcd_out_dat(0x00);
		}
		for (x=0; x < width; x++)
		{
			if((screen[(width * ((y+y_offset)>>3)) + x])& (0x1<<((y+y_offset)%8))){
				lcd_out_dat(0xff);
			}
			else{
				lcd_out_dat(0x00);
			}
		}
		for (x=z+width; x < scr_right_border + 1; x++)
		{
			lcd_out_dat(0x00);
		}
	}
}

 /*******************************************************
	Description:	Erases lcd RAM
	Note:
 ********************************************************/
void lcd_clear_ram(void)
{
	uchar p, i;
	if (m_cursor.enabled)
	{
		m_cursor.enabled=0;
		STOP_TIMER(cursor_timer)
	}
	lcd_init_col_lines(SCRN_TOP, SCRN_BOTTOM, SCRN_LEFT, SCRN_RIGHT );
	lcd_out_ctl( 0x005C ); //Enter memory write mode
  	for(p=0; p<=SCRN_BOTTOM; p++)
	{
#ifdef B3P3_MODE
		for(i=0; i<=SCRN_RIGHT; i++)
		{
			l_display_array[p][i]=0; /*Clear buffer*/
			lcd_out_dat(0x00);				/* clear the data */
		}
#else
		for(i=0; i<=(SCRN_RIGHT); i+=3)
		{
			l_display_array[p][i]=0; /*Clear buffer*/
			l_display_array[p][i+1]=0; /*Clear buffer*/
			l_display_array[p][i+2]=0; /*Clear buffer*/
			lcd_out_2b3_dat(0,0,0);
		}

#endif
	}
}

/*
**
** 	Clears the display memory starting at the left/top  and going to
**  the right/bottom . No runtime error checking is performed. It is
**  assumed that left is less than right and that top is less than
**  bottom
**
*/

void lcd_clear_rect(uchar left,  uchar top,
			        uchar right, uchar bottom)
{
	unsigned short i;
	unsigned int width, height;

	width=right-left;
	height=bottom-top;

	if((!width) || (!height))
		return;

	for(i=0; i<height; i++)
	{
		memset (&(l_display_array[top+i][left]),0,width);
	}
	lcd_update32 (top, height, left, width);
}

/*
**
** Inverts the display memory starting at the left/top and going to
** the right/bottom. No runtime error checking is performed. It is
** assumed that left is less than right and that top is less than
** bottom
**
*/

void lcd_invert_rect(uchar left,  uchar top,
			         uchar right, uchar bottom)
{

	unsigned short i, j;
	unsigned int width, height;

	width=right-left;
	height=bottom-top;

	if((!width) || (!height))
		return;

	for(i=0; i<height; i++)
	{
		for(j=left; j<right; j++)
		{
			l_display_array[top+i][j]=(l_display_array[top+i][j])^0xFF;
		}
	}
	lcd_update32 (top, height, left, width);
}

/*
**
** Draws a line into the display memory starting at left going to
** right, on the given row. No runtime error checking is performed.
** It is assumed that left is less than right.
**
*/

void lcd_horz_line(uchar left, uchar right, uchar row)
{
	uchar col;

  	for(col = left; col <= right; col++)
  	{
    	l_display_array[row][col] = 0xFF;
  	}

  	lcd_update32 (row, 1, left, right-left);

}

/*
**
** Draws a vertical line into display memory starting at the top
** going to the bottom in the given column. No runtime error checking
** is performed. It is assumed that top is less than bottom and that
** the column is in range.
**
*/

void lcd_vert_line(uchar top, uchar bottom, uchar column)
{
	uchar row;

  	for(row = top; row <= bottom; row++)
  	{
    	l_display_array[column][row] = 0xFF;
  	}

  	lcd_update32 (top, bottom-top, column, 1);
}

/*
**
** Clears a line into the display memory starting at left going to
** right, on the given row. No runtime error checking is performed.
** It is assumed that left is less than right.
**
*/

void lcd_clr_horz_line(uchar left, uchar right,
		               uchar row)
{
	uchar col;

  	for(col = left; col <= right; col++)
  	{
    	l_display_array[row][col] = 0x00;
  	}

  	lcd_update32 (row, 1, left, right-left);
}


/*
**
** Clears a vertical line into display memory starting at the top
** going to the bottom in the given column. No runtime error checking
** is performed. It is assumed that top is less than bottom and that
** the column is in range.
**
*/

void lcd_clr_vert_line(uchar top, uchar bottom, uchar column)
{
	uchar row;

  	for(row = top; row <= bottom; row++)
  	{
    	l_display_array[column][row] = 0x00;
  	}

  	lcd_update32 (top, bottom-top, column, 1);
}

/*
**
** 	Draws a box in display memory starting at the left/top and going
**  to the right/bottom. No runtime error checking is performed.
**  It is assumed that left is less than right and that top is less
**  than bottom.
**
*/

void lcd_draw_border(uchar left, uchar top, uchar right, uchar bottom)
{

  	/* to draw a box requires two vertical lines */
   	lcd_vert_line(top,bottom,left);
   	lcd_vert_line(top,bottom,right);
  	/* and two horizonal lines */
   	lcd_horz_line(left,right,top);
   	lcd_horz_line(left,right,bottom);
   	return ;
}

/*
**
** Clears a box in display memory starting at the Top left and going
** to the bottom right. No runtime error checking is performed and
** it is assumed that Left is less than Right and that Top is less
** than Bottom.
**
*/

void lcd_clear_border(uchar left, uchar top, uchar right, uchar bottom)
{

  	/* to undraw the box undraw the two vertical lines */
  	lcd_clr_vert_line(top,bottom,left);
  	lcd_clr_vert_line(top,bottom,right);

  	/* and the two horizonal lines that comprise it */
  	lcd_clr_horz_line(left,right,top);
    lcd_clr_horz_line(left,right,bottom);
}

/*
**
** Writes a glyph to the display at location x,y
**
** Arguments are:
**    column     - x corrdinate of the left part of glyph
**    row        - y coordinate of the top part of glyph
**    width  	 - size in pixels of the width of the glyph
**    height 	 - size in pixels of the height of the glyph
**    glyph      - an uchar pointer to the glyph pixels
**                 to write assumed to be of length "width"
**
*/

void lcd_glyph(uchar left, uchar top,
			   uchar width, uchar height,
			   uchar *glyph, uchar store_width)
{
	uchar byte_offset;
	uchar y_bits;
	uchar char_mask;
	uchar x;
	uchar *glyph_scan;
	uchar glyph_offset;

	glyph_offset = 0;			/* start at left side of the glyph rasters */
    char_mask = 0x80;			/* initial character glyph mask */

  	for (x = left; x < (left + width); x++)
  	{
    	byte_offset = top;        		/* get the byte offset into y direction */
		y_bits = height;				/* get length in y direction to write */
		glyph_scan = glyph + glyph_offset;	 /* point to base of the glyph */

    	/* boundary checking here to account for the possibility of  */
    	/* write past the bottom of the screen.                        */
    	while((y_bits) && (byte_offset < Y_BYTES)) /* while there are bits still to write */
    	{
			/* check if the character pixel is set or not */
			if(*glyph_scan & char_mask)
			{
				l_display_array[byte_offset][x] = 0xFF;	/* set image pixel */
			}
			else
			{
      			l_display_array[byte_offset][x] = 0;	/* clear the image pixel */
			}

			y_bits--;
			byte_offset++;

			/* bump the glyph scan to next raster */
			glyph_scan += store_width;
		}
		/* shift over to next glyph bit */
		char_mask >>= 1;
		if(char_mask == 0)				/* reset for next byte in raster */
		{
			char_mask = 0x80;
			glyph_offset++;
	    }
	}
}
/*
**
** Writes a graphic to the display at location x,y
**
** Arguments are:
**    column     - x corrdinate of the left part of glyph
**    row        - y coordinate of the top part of glyph
**    width  	 - size in pixels of the width of the glyph
**    height 	 - size in pixels of the height of the glyph
**    pixelmode  - 0: 3B3P mode, 1: 2B3P mode
**
*/
#ifdef BENCHMARK
void setstarttime();
void  printtest();
#endif

#ifndef SINGLETRIPLET
unsigned int lcd_out_2b3_linedat(unsigned short int xstart,unsigned short int xend, long buf)
{
	volatile register short __r4 __asm__ ("r4") = (short)(xstart);
	volatile register short __r5 __asm__ ("r5") = (short)(xend);
	volatile register long __r10  __asm__ ("r10") = (long)(buf);
	volatile register long __r12  __asm__ ("r12") = (long)(ReverseLookupTable);

	asm volatile(\
					"sprd	psr,(r3,r2) \n\t"\
					"di \n\t"\
					"movd	$0x9:m,(r1,r0) \n\t"\
					"stord	(r1,r0),0xff0064 \n\t"\
					"movd	$0x1080080:l,(r1,r0) \n\t"\
					"1: \n\t"\
					"loadd	 0x0:(r11,r10),(r9,r8)   \n\t"\
					"movzb 	 r8,r7 \n\t"\
					"/* Data are in r7, (r8 shifted <<8) ,r9 we need to do the 3 to 2 */ \n\t"\
					"/* ((X&0xF8)|((Y>>5)&0x7)) */ \n\t"\
					"/* (((Y<<3)&0xE0)|((Z>>3)&0x1F)) */ \n\t"\
					"movw r8,r6 \n\t"\
					"ashuw $-13,r6 \n\t"\
					"andw $0x7:m,r6 \n\t"\
					"andw $0xF8:m,r7 \n\t"\
					"orb r6,r7 \n\t"\
					"ashuw $-5,r8 \n\t"\
					"andw $0xE0:m,r8 \n\t"\
					"ashuw $-3,r9 \n\t"\
					"andw $0x1F,r9 \n\t"\
					"orb r8,r9 \n\t"\
					"movzb r7,r8 \n\t"\
					"/* ReverseLookup Table r8,r9 */ \n\t"\
					"xorw	 r7,r7 \n\t"\
					"movzb 	 r8,r6 \n\t"\
					"addd	(r12),(r7,r6) \n\t"\
					"loadb	 0x0:(r7,r6),r8 \n\t"\
					"xorw	 r7,r7 \n\t"\
					"movzb 	 r9,r6 \n\t"\
					"addd	(r12),(r7,r6) \n\t"\
					"loadb	 0x0:(r7,r6),r9 \n\t"\
					"/* Data are in r8,r9 */\n\t"\
					"storw	r8,0x0:(r1,r0) \n\t"\
					"nop \n\t"\
					/*Required for correct LCD writes*/
					"storw	$0x9,0xff0064 \n\t"\
					"storw	r9,0x0:(r1,r0) \n\t"\
					"storw	$0x9,0xff0064 \n\t"\
					"/*increase r4 and r11,r10 by 3  */ \n\t"\
					"addd $0x3:m,(r11,r10) \n\t"\
					"addw $0x3:m,r4 \n\t"\
					"cmpw r5,r4  \n\t"\
					"bhi 1b \n\t"\
					"movd	$0xa,(r1,r0) \n\t"\
					"stord	(r1,r0),0xff0064 \n\t"\
					"xorw r5,r5 \n\t"\
					"lprd	(r3,r2), psr \n\t"\
				:"=r" (__r4),"=r" (__r5),"=r" (__r10),"=r" (__r12) \
				: "0" (__r4),"1" (__r5),"2" (__r10),"3" (__r12) \
				: "r0","r1","r2","r3","r6","r7","r8","r9");
	return (__r4-xstart);

}
#endif

const unsigned char bulkstartarray[]={0,2,1};

//this operation used for the cursor update
//it DOES NOT update the buffer

void lcd_invert32_noupdate	(  uchar left, uchar top,
							   uchar width, uchar height ,
							   char invert)
{

	char linedata [X_BYTES];
	unsigned short i, j;
	unsigned short xstart,xend,yend,ystart;
	unsigned short int byteswritten;

	if((!width) || (!height))
		return;


	//		012345678901234567890123456789012345678901234567890123456789
	//		012012012012012012012012012012012012012012012012012012012012
	//left             ^
	//width            -----------------------------^
	//xstart         ^
	//xend                                           ^
	//overallwidth   ---------------------------------
	//bulkstart         ^
	//bulkend                                     ^

	xstart=left-(left%3);
	xend=(left+width)+bulkstartarray[(left+width)%3]-1;
	yend=top+height;	//required to make it +1 line to correctly update the last line
	ystart=top;

	for(i=0; i<height; i++)
	{
		lcd_init_col_lines(ystart+i, yend, xstart, xend);
		lcd_out_ctl( 0x005C ); //Enter memory write mode

		if (invert)
		{
			memcpy (&linedata[xstart], &(l_display_array[ystart+i][xstart]),(xend-xstart)+1);
			for (j=left;j<(left+width);j++)
			{
				linedata[j]^=0xFF;
			}
			byteswritten=lcd_out_2b3_linedat(xstart,xend,\
								(long)&(linedata[xstart]));
		}
		else
		{

			byteswritten=lcd_out_2b3_linedat(xstart,xend,\
						(long)&(l_display_array[ystart+i][xstart]));
		}

	}
}

void lcd_graphic32(uchar left, uchar top,
			   uchar width, uchar height,
			   uchar *buf)
{
	unsigned short i, k=0;
	unsigned short xstart,xend,yend,ystart;
	unsigned short int byteswritten;

	//		012345678901234567890123456789012345678901234567890123456789
	//		012012012012012012012012012012012012012012012012012012012012
	//left             ^
	//width            -----------------------------^
	//xstart         ^
	//xend                                           ^
	//bulkstart         ^
	//bulkend                                     ^

	if((!width) || (!height))
		return;

	xstart=left-(left%3);
	xend=(left+width)+bulkstartarray[(left+width)%3]-1;

	yend=top+height;	//required to make it +1 line to correctly update the last line
	ystart=top;

#ifdef BENCHMARK
		setstarttime();
#endif

	for(i=0; i<height; i++)
	{
		lcd_init_col_lines(ystart+i, yend, xstart, xend);
		lcd_out_ctl( 0x005C ); //Enter memory write mode

		memcpy (&(l_display_array[ystart+i][left]), &buf[k],width);
		k+=width;

		byteswritten=lcd_out_2b3_linedat(xstart,xend,\
					(long)&(l_display_array[ystart+i][xstart]));

		if ((i&3)==3)
		{
#ifdef BENCHMARK
			printtime();
#endif
			yield();
#ifdef BENCHMARK
			setstarttime();
#endif
		}
	}
		lcd_init_col_lines(SCRN_TOP, SCRN_BOTTOM, SCRN_LEFT, SCRN_RIGHT  );
}

void lcd_animated_glyph(uchar left, uchar top,
						uchar width, uchar num_of_bitmaps,
						uchar *glyph1, uchar *glyph2, uchar *glyph3)
{
	int i;
	STOP_TIMER(animation_timer);

 	// if width == 0, stop animation and clear glyph region
	if(width == 0) {
 		for (i=0;i<MAX_ANIMATED_BITMAPS;i++) {
			if (m_animation[i].enabled && (m_animation[i].left == left) && (m_animation[i].top == top)) {

				lcd_clear_rect(left, top, left + m_animation[i].width, top + m_animation[i].width);

				m_animation[i].enabled = 0;
				m_animation[i].state = 0;

 				lcd_update(top, top + m_animation[i].width);
 				break;
			}
		}

		for (i=0;i<MAX_ANIMATED_BITMAPS;i++) {
			if (m_animation[i].enabled) {
				START_TIMER(animation_timer, 100 );
				return;
			}
		}

		return;
	}

	for (i=0;i<MAX_ANIMATED_BITMAPS;i++) {
		if (m_animation[i].enabled && (m_animation[i].left == left) && (m_animation[i].top == top)) {
			START_TIMER(animation_timer, 100 );
			return ;
		}
	}

	for (i=0;i<MAX_ANIMATED_BITMAPS;i++)
	{
	  if (!m_animation[i].enabled)
		  break;
	}
	if (i==MAX_ANIMATED_BITMAPS) {
 		START_TIMER(animation_timer, 100 );
		return ;
	}

	m_animation[i].enabled = 1;
	m_animation[i].left =left;
	m_animation[i].top =top;
	m_animation[i].width =width;
	m_animation[i].blinking_rate =200;
	m_animation[i].state =0;
	m_animation[i].bitmaps =num_of_bitmaps;
	memcpy(m_animation[i].bitmap[0],glyph1,width*width/8);
	memcpy(m_animation[i].bitmap[1],glyph2,width*width/8);
	memcpy(m_animation[i].bitmap[2],glyph3,width*width/8);

	lcd_glyph(left,top,width,width,&m_animation[i].bitmap[0][0],width/8);  /* plug symbol into buffer */
 	START_TIMER(animation_timer, 100 );
	lcd_update(SCRN_TOP, SCRN_BOTTOM);
 }


/*
**
**	Prints the given string at location x,y in the specified font.
**  Prints each character given via calls to lcd_glyph. The entry string
**  is null terminated and non 0x20->0x7e characters are ignored.
**
**  Arguments are:
**      left       coordinate of left start of string.
**      top        coordinate of top of string.
**      font       font number to use for display
**      str   	   text string to display
**
*/

void lcd_text(uchar left, uchar top, uchar font, char *str)
{
  	uchar x = left;
  	uchar glyph;
  	uchar width;
	uchar height;
	uchar store_width;
	uchar  *glyph_ptr;


if (str==NULL) {printk("INVALID STRING ....");}

  	while(*str != 0x00)
  	{
    	glyph = (uchar)*str;

		/* check to make sure the symbol is a legal one */
		/* if not then just replace it with the default character */
		if((glyph < fonts[font].glyph_beg) || (glyph > fonts[font].glyph_end))
		{
			glyph = fonts[font].glyph_def;
		}

    	/* make zero based index into the font data arrays */
    	glyph -= fonts[font].glyph_beg;
    	width = fonts[font].fixed_width;	/* check if it is a fixed width */
		if(width == 0)
		{
			width=fonts[font].width_table[glyph];	/* get the variable width instead */
		}

		height = fonts[font].glyph_height;
		store_width = fonts[font].store_width;

		glyph_ptr = fonts[font].glyph_table + ((unsigned int)glyph * (unsigned int)store_width * (unsigned int)height);

		/* range check / limit things here */
		if(x > SCRN_RIGHT)
		{
			x = SCRN_RIGHT;
		}
		if((x + width) > SCRN_RIGHT+1)
		{
			width = SCRN_RIGHT - x + 1;
		}
		if(top > SCRN_BOTTOM)
		{
			top = SCRN_BOTTOM;
		}
		if((top + height) > SCRN_BOTTOM+1)
		{
			height = SCRN_BOTTOM - top + 1;
		}

		lcd_glyph(x,top,width,height,glyph_ptr,store_width);  /* plug symbol into buffer */

		x += width;							/* move right for next character */
		str++;								/* point to next character in string */
	}
}


/*
**
** Updates area of the display. Writes data from display
** RAM to the lcd display controller.
**
** Arguments Used:
**    top     top line of area to update.
**    bottom  bottom line of area to update.
**
*/

int protection_flag = 0;
void lcd_update(uchar top, uchar bottom)
{
	if (protection_flag) return;
	protection_flag = 1;

		lcd_update32(SCRN_TOP, SCRN_BOTTOM-SCRN_TOP+1, SCRN_LEFT, SCRN_RIGHT-SCRN_LEFT+1);

	protection_flag=0;
}


void lcd_update32(uchar top, uchar height, uchar left, uchar width)
{
	unsigned short i;
	unsigned short xstart,xend,yend,ystart;
	unsigned short int byteswritten;

	//		012345678901234567890123456789012345678901234567890123456789
	//		012012012012012012012012012012012012012012012012012012012012
	//left             ^
	//width            -----------------------------^
	//xstart         ^
	//xend                                           ^
	//bulkstart         ^
	//bulkend                                     ^

	if((!width) || (!height))
		return;

	xstart=left-(left%3);
	xend=(left+width)+bulkstartarray[(left+width)%3]-1;

	yend=top+height;	//required to make it +1 line to correctly update the last line
	ystart=top;

#ifdef BENCHMARK
		setstarttime();
#endif

	for(i=0; i<height; i++)
	{
		lcd_init_col_lines(ystart+i, yend, xstart, xend);
		lcd_out_ctl( 0x005C ); //Enter memory write mode

		byteswritten=lcd_out_2b3_linedat(xstart,xend,\
					(long)&(l_display_array[ystart+i][xstart]));

		if ((i&3)==3)
		{
#ifdef BENCHMARK
			printtime();
#endif
			yield();
#ifdef BENCHMARK
			setstarttime();
#endif
		}
	}
		lcd_init_col_lines(SCRN_TOP, SCRN_BOTTOM, SCRN_LEFT, SCRN_RIGHT  );
}

void lcd_cursor_set(uchar left, uchar top, uchar font, uchar ShowCursor, uchar blinking_rate)
{
	uchar width;

	STOP_TIMER(cursor_timer);
 	width=fonts[font].fixed_width;
	if (width==0) width=8;

	if (m_cursor.enabled)
	{
		m_cursor.enabled=0;
   		if(m_cursor.state)
		{

   			lcd_invert32_noupdate	(  	m_cursor.left, m_cursor.top, \
										width , \
										fonts[m_cursor.font].glyph_height - 1, 0);
/*			lcd_invert_rect(m_cursor.left, m_cursor.top, \
							m_cursor.left + width - 1, \
							m_cursor.top + fonts[m_cursor.font].glyph_height - 1);
							*/
			m_cursor.state = 0;
  		}
	}

 	if(ShowCursor == 0)
			return;

 	m_cursor.left = left;
	m_cursor.top = top;
	m_cursor.font = font;
	m_cursor.blinking_rate = blinking_rate;

		lcd_invert32_noupdate	(  	m_cursor.left, m_cursor.top, \
								width , \
								fonts[m_cursor.font].glyph_height - 1, 1);


  	//lcd_invert_rect(m_cursor.left, m_cursor.top, m_cursor.left + width - 1, m_cursor.top + fonts[m_cursor.font].glyph_height - 1);
	m_cursor.state = 1;
	m_cursor.enabled = 1;
  	START_TIMER(cursor_timer, blinking_rate / 2);

}


void lcd_scroll_init(char *buf)
{
	char visible_buffer[50];
	char scroll_enable = buf[4];

  	if (m_scroll_struct.top == buf[1])
	{
  		STOP_TIMER(scroll_timer);
	}

	if ((scroll_enable == 0 )|| ( buf[3] >strlen(&buf[5])))
	{
  		memset(visible_buffer, 0 , 50);
		strncpy(visible_buffer, &buf[5], buf[3]);
 		lcd_text(buf[0], buf[1], buf[2], visible_buffer);
  		return;
	}

 	if( buf[3] <strlen(&buf[5]))
	{
		STOP_TIMER(scroll_timer);
		m_scroll_struct.left = buf[0];
		m_scroll_struct.top = buf[1];
		m_scroll_struct.font = buf[2];
		m_scroll_struct.visible_size = buf[3];
		strcpy(m_scroll_struct.text, &buf[5]);
		m_scroll_struct.size = strlen(m_scroll_struct.text);
		m_scroll_struct.ScrollPos = 0;

		memset(visible_buffer, 0 , 50);
		strncpy(visible_buffer, m_scroll_struct.text, m_scroll_struct.visible_size);
		lcd_text(m_scroll_struct.left, m_scroll_struct.top, m_scroll_struct.font, visible_buffer);


		//if(m_scroll_struct.visible_size < m_scroll_struct.size)
		{
			// add a space at the end of the text
			//strcat(m_scroll_struct.text, " ");
			//m_scroll_struct.size++;
			m_scroll_struct.enable =1;
  			START_TIMER(scroll_timer, 100);
		}
	}else{
  		memset(visible_buffer, 0 , 50);
		strncpy(visible_buffer, &buf[5], buf[3]);
 		lcd_text(buf[0], buf[1], buf[2], visible_buffer);
 		STOP_TIMER(scroll_timer);
	}

}
uchar lcd_font_width_get(uchar font_width)
{
 	return fonts[font_width].fixed_width;
}
static void InitializeTimer(struct timer_list *timer_ptr, void (*timer_function)(unsigned long param))
{
	init_timer(timer_ptr);
	timer_ptr->data = 0; //no argument for kbd_timer_fn needed
	timer_ptr->function = timer_function;
}

/*******************************************************
	Description: 	Sets the lcd contrast
	Input:		level: Brightness level (5-410). We map 0-255 to 0-511 by doubling the input level.
 ********************************************************/
void lcd_contrast_level(uchar level){
unsigned short level_16 = 	level << 1;

	lcd_out_ctl(LCD_VOL_CTRL);
	lcd_out_dat(level_16 & 0x3f);
	lcd_out_dat(level_16>>6);

}
/*******************************************************
	Description:
	Input:
 ********************************************************/

void lcd_contrast_up(void){


	lcd_out_ctl(LCD_VOL_UP);

}
/*******************************************************
	Description:
	Input:
 ********************************************************/
void lcd_contrast_down(void){


	lcd_out_ctl(LCD_VOL_DOWN);

}
/*******************************************************
	Description:
	Input:
 ********************************************************/
void lcd_sleepmode_enter(void){
	lcd_out_ctl(LCD_SLEEP_IN);
}
/*******************************************************
	Description:
	Input:
 ********************************************************/
void lcd_sleepmode_exit(void){
	lcd_out_ctl(LCD_SLEEP_OUT);
}
/****************************************************************************************************/
/************                                       HIGH LEVEL DRIVER FUNTIONS                                                            *********/
/****************************************************************************************************/

/*******************************************************
	Description: 	Open lcd dev file.

 ********************************************************/
int LCD_open(struct inode *inode, struct file *filp)
{
	lcd_sc14450_dev_struct *dev; /* device information */
	dev = container_of(inode->i_cdev, lcd_sc14450_dev_struct, cdev);
	filp->private_data = dev; /* for other methods */


	return 0;
}
/*******************************************************
	Description: 	Close device file.

 ********************************************************/
int LCD_release(struct inode *inode, struct file *filp)
{
	return 0;
}
/*******************************************************
	Description: 	Refresh timer routing.

 ********************************************************/
 static void lcd_timer_refresh_fn(unsigned long param){
	lcd_update(SCRN_TOP, SCRN_BOTTOM);
	START_TIMER(lcd_timer, lcd_conf->refresh_tmout);
}

 static void scroll_timer_refresh(unsigned long param)
 {
 	char visible_buffer[50];
	int i;

	m_scroll_struct.text[m_scroll_struct.size] = m_scroll_struct.text[0];

	for(i=0 ; i < m_scroll_struct.size ; i++)
		m_scroll_struct.text[i] = m_scroll_struct.text[i+1];

	m_scroll_struct.text[m_scroll_struct.size] = '\0';

	memcpy(visible_buffer, m_scroll_struct.text, m_scroll_struct.visible_size);
	visible_buffer[(int)m_scroll_struct.visible_size] = '\0';
	lcd_text(m_scroll_struct.left, m_scroll_struct.top, m_scroll_struct.font, visible_buffer);

	lcd_update(SCRN_TOP, SCRN_BOTTOM);
 	START_TIMER(scroll_timer, 100);
}

 static void animation_timer_refresh(unsigned long param) {
	int i;
	int found =0;

	for (i=0;i<MAX_ANIMATED_BITMAPS;i++)
	{
			if (m_animation[i].enabled)
			{
				m_animation[i].state++;
				if (m_animation[i].state==m_animation[i].bitmaps)
					m_animation[i].state = 0;

				lcd_glyph(m_animation[i].left,m_animation[i].top,
					m_animation[i].width,
					m_animation[i].width,
					&m_animation[i].bitmap[m_animation[i].state][0],
					m_animation[i].width/8);  /* plug symbol into buffer */
				found =1;
			}
	}
	lcd_update(SCRN_TOP, SCRN_BOTTOM);
	if (found){
		START_TIMER(animation_timer, 100 );
	}
  }




 static void cursor_timer_refresh(unsigned long param) {
 	uchar width;

	if (!m_cursor.enabled) {printk("timer not enabled ...\n");return;}

	width= fonts[m_cursor.font].fixed_width;
 	if (width==0)
  		width=8;//fonts[m_cursor.font].width_table[glyph];

	if (m_cursor.state==1) m_cursor.state=0;
	else m_cursor.state=1;

 	lcd_invert32_noupdate	(  	m_cursor.left, m_cursor.top, \
								width , \
								fonts[m_cursor.font].glyph_height - 1, m_cursor.state);

	//lcd_invert_rect(m_cursor.left, m_cursor.top,m_cursor.left+ width - 1, m_cursor.top + fonts[m_cursor.font].glyph_height - 1);

	START_TIMER(cursor_timer, m_cursor.blinking_rate / 2);
}

/*******************************************************
	Description: 	Kbd ioctl.

 ********************************************************/

#ifdef BENCHMARK
volatile unsigned int timestart;
volatile unsigned short int* timvalue2=(volatile u16 *)(0xFF497A);
volatile u16 moldtimstart=0;
volatile u16 moldtimend=0;

void setstarttime ()
{
	timestart=jiffies;
	moldtimstart=*timvalue2;
}

void printtest ()
{
			timestart=jiffies-timestart;
	        moldtimend=*timvalue2;

			if (moldtimstart<moldtimend)
				moldtimstart=moldtimend-moldtimstart;
	        else
	           {
					moldtimstart=moldtimend+0x2CFF-moldtimstart;
                 	timestart--;
               }
			printk ("T: %d | %d \r\n", timestart,moldtimstart);
			timestart=jiffies;
			moldtimstart=*timvalue2;

}

void printtime ()
{
			timestart=jiffies-timestart;
	        moldtimend=*timvalue2;

			if (moldtimstart>moldtimend)
				moldtimstart=moldtimstart-moldtimend;
	        else
	           {
					moldtimstart=0x2CFF+moldtimstart-moldtimend;
                 	timestart--;
               }

			printk ("Timespan: %d | %d \r\n", timestart,moldtimstart);
}

#endif


#define NOMMU_LGOPTIMIZE
unsigned char buf[MAX_IOCTL_ARGS];
int LCD_ioctl(struct inode *inode, struct file *filp,
                 unsigned int cmd, unsigned long arg)
{
	int i;
	lcd_sc14450_dev_struct *dev = filp->private_data;

		if (down_interruptible(&dev->sem))
			return -ERESTARTSYS;
		switch (cmd){
			case LCD_init:
				lcd_init();
				up (&dev->sem);
				return 0;
			//Issue a LCD cmd directly from the user space (it is assumed that it is a valid cmd)
			case LCD_out_ctl:
				if(copy_from_user(buf, (char*)arg, sizeof(char) )){//copy only addr and length first
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				lcd_out_ctl(buf[0]);
				up (&dev->sem);
				return 0;
			case LCD_out_data:
				if(copy_from_user(buf, (char*)arg, sizeof(char) )){//copy only addr and length first
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				printk (KERN_WARNING "IOCTL not supported in B2P3 Mode \n");
				up (&dev->sem);
				return 0;
			case LCD_clear_ram:
				lcd_clear_ram();
				up (&dev->sem);
				return 0;
			case LCD_clear_rect:
				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){//copy only addr and length first
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				lcd_clear_rect(buf[0], buf[1], buf[2], buf[3]);
				PDEBUG("LCD: ioctl LCD_clear_rect(%d, %d, %d, %d)\n", buf[0], buf[1], buf[2], buf[3]);
				up (&dev->sem);
				return 0;
			case LCD_invert_rect:
				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				lcd_invert_rect(buf[0], buf[1], buf[2], buf[3]);
				PDEBUG("LCD: ioctl LCD_invert_rect(%d, %d, %d, %d)\n", buf[0], buf[1], buf[2], buf[3]);
				up (&dev->sem);
				return 0;
			case LCD_draw_border:
				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				lcd_draw_border(buf[0], buf[1], buf[2], buf[3]);
				PDEBUG("LCD: ioctl LCD_draw_border(%d, %d, %d, %d)\n", buf[0], buf[1], buf[2], buf[3]);
				up (&dev->sem);
				return 0;
			case LCD_clear_border:
				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				lcd_clear_border(buf[0], buf[1], buf[2], buf[3]);
				PDEBUG("LCD: ioctl LCD_clear_border(%d, %d, %d, %d)\n", buf[0], buf[1], buf[2], buf[3]);
				up (&dev->sem);
				return 0;
			case LCD_text:
				/*Copy the string to be displayed*/
				i=0;
				while(i<MAX_IOCTL_ARGS){
					if(copy_from_user(buf+i, (char*)arg+i, sizeof(char))){
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}
					PDEBUG("LCD: ioctl LCD_text i: %x, copied: %x \n", i, buf[i]);
					if(i>2 && buf[i]==0)
						break;
					i++;
				}
				PDEBUG("LCD: ioctl LCD_text(%d, %d, %d, %s)\n", buf[0], buf[1], buf[2], &buf[3]);
				lcd_text(buf[0], buf[1], buf[2], &buf[3]);
				up (&dev->sem);
				return 0;

			case LCD_label:
				/*Copy the string to be displayed*/
				i=0;

				while(i<MAX_IOCTL_ARGS){

					if(copy_from_user(buf+i, (char*)arg+i, sizeof(char))){
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}
					if(i>4 && buf[i]==0)
						break;
					i++;
				}

				lcd_scroll_init(buf);

				up (&dev->sem);
				return 0;

			case LCD_glyph:
				if(copy_from_user(buf, (char*)arg, 3*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}

				if ((buf[2]*buf[2]/8)<(MAX_IOCTL_ARGS-3))
				{
					if(copy_from_user(&buf[3], (char*)arg+3, (buf[2]*buf[2]/8)*sizeof(char))){
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}
					PDEBUG("LCD: ioctl LCD_glyph(%d, %d, %d, %s)\n", buf[0], buf[1], buf[2], &buf[3]);
 					lcd_glyph((uchar)buf[0], (uchar)buf[1], (uchar)buf[2], (uchar)buf[2], (uchar*)&buf[3], (uchar)buf[2]/8);
					up (&dev->sem);
					return 0;
				}else {
					printk(KERN_WARNING "Too large bitmap\n");
					up (&dev->sem);
					return -EFAULT;
				}

				return 0;
#if !(defined(CONFIG_L_V2_BOARD) || defined(CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD)\
				||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX))
			case LCD_graphic:
				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				if ((buf[2]*buf[3]/8)<(MAX_IOCTL_ARGS-3))
				{
					if(copy_from_user(&buf[4], (char*)arg+4, (buf[2]*buf[3]/8)*sizeof(char))){
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}
					PDEBUG("LCD: ioctl LCD_glyph(%d, %d, %d, %d, %s)\n", buf[0], buf[1], buf[2], buf[3], &buf[4]);
 					lcd_glyph((uchar)buf[0], (uchar)buf[1], (uchar)buf[2], (uchar)buf[3], (uchar*)&buf[4], (uchar)buf[2]/8);
					up (&dev->sem);
					return 0;
				}else {
					printk(KERN_WARNING "Too large bitmap\n");
					up (&dev->sem);
					return -EFAULT;
				}

				return 0;
#else
			case LCD_graphic:
	#ifndef NOMMU_LGOPTIMIZE
				if(copy_from_user(buf, (char*)arg, 5*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				if ((buf[2]*buf[3])<(MAX_IOCTL_ARGS-3))
				{
					if(copy_from_user(&buf[4], (char*)arg+4, (buf[2]*buf[3])*sizeof(char))){
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}
					PDEBUG("LCD: ioctl LCD_graphic32(%d, %d, %d, %d, %s)\n", buf[0], buf[1], buf[2], buf[3], &buf[4]);

					lcd_graphic32((uchar)buf[0], (uchar)buf[1], (uchar)buf[2], (uchar)buf[3], (uchar*)&buf[4]);
				}
				else
				{
					printk(KERN_WARNING "Too large bitmap\n");
					up (&dev->sem);
					return -EFAULT;
				}
				up (&dev->sem);
				return 0;
	#else
				lcd_graphic32(((uchar*)arg)[0], ((uchar*)arg)[1], ((uchar*)arg)[2], ((uchar*)arg)[3], &((uchar*)arg)[4]);
				up (&dev->sem);
				return 0;
	#endif
#endif
			case LCD_animate_bitmap:
				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}

				if ((3 *buf[2]*buf[2]/8)<(MAX_IOCTL_ARGS-4))
				{
					if(copy_from_user(&buf[4], (char*)arg+4, (3*buf[2]*buf[2]/8)*sizeof(char))){
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}
					PDEBUG("LCD: ioctl LCD_glyph(%d, %d, %d, %d, %s)\n", buf[0], buf[1], buf[2], buf[3], &buf[4]);
 					lcd_animated_glyph((uchar)buf[0], (uchar)buf[1], (uchar)buf[2], (uchar)buf[3], (uchar*)&buf[4], (uchar*)&buf[4+buf[2]*buf[2]/8], (uchar*)&buf[4+2*buf[2]*buf[2]/8]);
					up (&dev->sem);
					return 0;
				}else {
					printk(KERN_WARNING "Too large bitmaps\n");
					up (&dev->sem);
					return -EFAULT;
				}

				return 0;

			case LCD_update:
				if(copy_from_user(buf, (char*)arg, 2*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				PDEBUG("LCD: ioctl LCD_update(%d, %d)\n", buf[0], buf[1]);
				lcd_update(buf[0], buf[1]);
				up (&dev->sem);
				return 0;
			case LCD_contrast_up:
				lcd_contrast_up();
				up (&dev->sem);
				return 0;
			case LCD_contrast_down:
				lcd_contrast_down();
				up (&dev->sem);
				return 0;
			case LCD_sleepmode_enter:
				lcd_sleepmode_enter();
				up (&dev->sem);
				return 0;
			case LCD_sleepmode_exit:
				lcd_sleepmode_exit();
				up (&dev->sem);
				return 0;
			case LCD_backlight_level:
				if(copy_from_user(buf, (char*)arg, sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				if(buf[0]){
					LCD_SET_BACKLIGHT_ON;
				}
				else{
					LCD_SET_BACKLIGHT_OFF;
				}
				up (&dev->sem);
				return 0;
			case LCD_contrast_level:
				if(copy_from_user(buf, (char*)arg, sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				lcd_contrast_level(buf[0]);
				PDEBUG("LCD: ioctl lcd_contrast_level(%d)\n", buf[0]);
				up (&dev->sem);
				return 0;
			case LCD_refresh_tmout:
				if ( (copy_from_user((char*)&lcd_conf->refresh_tmout, (char*)arg, sizeof(char))) ) {
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				STOP_TIMER(lcd_timer);
				if(lcd_conf->refresh_tmout){
					START_TIMER(lcd_timer, lcd_conf->refresh_tmout);
					PDEBUG("LCD: timer set to (%d)\n", lcd_conf->refresh_tmout);
				}
				else{
					STOP_TIMER(lcd_timer);
					PDEBUG("LCD: timer stopped \n");
				}
				up (&dev->sem);
				return 0;
			case LCD_show_cursor:
				if(copy_from_user(buf, (char*)arg, 5*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}

				PDEBUG("LCD: ioctl LCD_show_cursor(%d, %d, %d, %d, %d)\n", buf[0], buf[1], buf[2], buf[3], buf[4]);
				lcd_cursor_set(buf[0], buf[1], buf[2], buf[3], buf[4]);
				up (&dev->sem);
				return 0;

 			case LCD_fonts_width_get:

				buf[0]=lcd_font_width_get(buf[0]);
				if(copy_to_user((char*)arg, (char*) &buf[0],1)){
					printk(KERN_WARNING "Copy to user failed\n");
					up (&dev->sem);
					return -EFAULT;
		 		}
				up (&dev->sem);
				return 0;
	}


/**** DEBUGGING IOCTLS ****/
#ifdef LCD_SC14450_DEBUG
#endif
	printk(KERN_WARNING "Invalid lcd ioctl \n");
	return -ENOTTY;

}

/*******************************************************
	Description: 	Module initialization.

 ********************************************************/
 /*******************************************************
	Description: 	Cleanup the module and handle initialization failures .

 ********************************************************/
void LCD_cleanup_module(void)
{
	dev_t devno = MKDEV(lcd_sc14450_major, lcd_sc14450_minor);

	/* Get rid of our char dev entries */
	cdev_del(&lcd_dev.cdev);
	printk(KERN_NOTICE "lcd_sc14450: cleanup module\n");
	/* cleanup_module is never called if registering failed */
	unregister_chrdev_region(devno, 1);

}

/*******************************************************
	Description: 	Set up the cdev structure for this device.

 ********************************************************/
void LCD_setup_cdev(lcd_sc14450_dev_struct *dev)
{
	int err, devno = MKDEV(lcd_sc14450_major, lcd_sc14450_minor);

	cdev_init(&dev->cdev, &lcd_sc14450_fops);
	dev->cdev.owner = THIS_MODULE;
	dev->cdev.ops = &lcd_sc14450_fops;
	err = cdev_add (&dev->cdev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE "Error %d adding lcd_sc14450", err);
}

static int __init LCD_init_module(void)
{
	int result;
	dev_t dev = 0;

	/* Initializations*/
	PDEBUG("module init function, entry point\n");

/*
 * Get a range of minor numbers to work with, asking for a dynamic
 * major unless directed otherwise at load time.
 */
	if (lcd_sc14450_major) {
		dev = MKDEV(lcd_sc14450_major, lcd_sc14450_minor);
		result = register_chrdev_region(dev, 1, "lcd_sc14450");
	}
	else {
		result = alloc_chrdev_region(&dev, lcd_sc14450_minor, 1, "lcd_sc14450");
		lcd_sc14450_major = MAJOR(dev);
	}
	if (result < 0) {
		printk(KERN_WARNING "lcd_sc14450: can't get major %d\n", lcd_sc14450_major);
		printk( "FATAL ERROR ==========================lcd_sc14450: can't get major %d\n", lcd_sc14450_major);
		return result;
	}

	/*Initialize driver internal vars*/
	memset((char*)&lcd_dev, 0, sizeof(lcd_sc14450_dev_struct));
	/*Copy init configuration*/
	memcpy(lcd_conf, &lcd_init_conf, sizeof(struct lcd_sc14450_conf_struct));
	init_MUTEX(&lcd_dev.sem);
	spin_lock_init(&lcd_dev.lock);
	LCD_setup_cdev(&lcd_dev);
	/*Enable kbe int if needed*/
	printk(KERN_NOTICE "lcd_sc14450: driver for st7529 controller initialized with major devno: %d\n", lcd_sc14450_major);

#ifdef LCD_SC14450_DEBUG
#endif
	/*Init hw*/
	lcd_init();
//	init_timer(&lcd_timer);
//	lcd_timer.data = 0; //no argument for kbd_timer_fn needed
//	lcd_timer.function = lcd_timer_refresh_fn;

	if(lcd_conf->refresh_tmout){
				printk("START_TIMER 13 \n");
		START_TIMER(lcd_timer, lcd_conf->refresh_tmout);
	}

	return 0; /* succeed */

  fail:
	unregister_chrdev_region(dev, 1);
	return result;
}

module_init(LCD_init_module);
module_exit(LCD_cleanup_module);









