/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <george.giannaras@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 * lcd_st7529.c -- lcd char driver
 *Based on the code from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.
 *
 *
 */

/***************       Include headers section       ***************/

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#ifdef LCD_TEST_PC
	#include <asm/uaccess.h>
#else
	#include <asm-cr16/regs.h>
	#include <asm-cr16/uaccess.h>
#endif
#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */

#include <linux/proc_fs.h> /*semaphores*/
#include <linux/cdev.h> /*cdev*/
#include <linux/wait.h>
#include <linux/delay.h>	/* delays */
#include <linux/spinlock.h>


//#include <linux/interrupt.h>/*irq*/
#include "fonts.h"
#include "lcd_s6b33.h"

//#define BENCHMARK

/***************       Definitions section             ***************/
#define BUFFER_SIZE 50 /*Buffer size to support the ioctl with the largest data transfer*/
#define MAX_IOCTL_ARGS ((SCRN_RIGHT+1)*(SCRN_BOTTOM+1)+10)/*Maximum number of args to pass with ioctl*/
#define MAX_SCROLL_TEXT_SIZE 4000

//#define BENCHMARK
/***      Data types definitions       ***/
#define uchar unsigned char
//Driver structs
typedef struct {
	struct lcd_sc14450_conf_struct lcd_conf;
	struct semaphore sem;     /* mutual exclusion semaphore     */
	spinlock_t lock;
	struct cdev cdev;	  	  /* Char device structure		*/
	//uchar LCD_buf[Y_BYTES][X_BYTES]; /*Buffer size to support the ioctl with the largest data transfer*/
}lcd_sc14450_dev_struct;

typedef struct {
	char left;
	char top;
	char font;
	char visible_size;
	int  size ;
	char text[MAX_SCROLL_TEXT_SIZE];
	int ScrollPos;
	int enable;
}scroll_struct;
scroll_struct m_scroll_struct;
typedef struct {
	uchar left;
	uchar top;
	uchar font;
	uchar blinking_rate;
	uchar state;
	uchar enabled;
	uchar reserved[2];
}cursor_struct;

typedef struct {
	uchar left;
	uchar top;
	uchar width;
 	uchar blinking_rate;
	uchar state;
	uchar enabled;
	uchar bitmaps;
	uchar reserved;
	uchar  bitmap[4][128];
}animation_struct;

/***      Flags definitions     ***/
/***      Hw specific definitions       ***/

/***   Macros   ***/
#define	START_TIMER(TIMER, TMOUT) {TIMER.expires = jiffies + TMOUT;add_timer(&TIMER);}
#define	STOP_TIMER(TIMER) {del_timer(&TIMER);}
/***      Function Prototypes       ***/
/** Low level **/

/* LCD function prototype list */
void lcd_init(void);
void lcd_update32(uchar top, uchar height, uchar left, uchar width);
void lcd_clear_ram(void);
void lcd_print_screen(unsigned char *screen);
void lcd_clear_rect(uchar left,  uchar top, uchar right, uchar bottom);
void lcd_invert_rect(uchar left, uchar top, uchar right, uchar bottom);
void lcd_horz_line(uchar left, uchar right, uchar row);
void lcd_vert_line(uchar top, uchar bottom, uchar column);
void lcd_clr_horz_line(uchar left, uchar right, uchar row);
void lcd_clr_vert_line(uchar top, uchar bottom, uchar column);
void lcd_draw_border(uchar left, uchar top, uchar right, uchar bottom);
void lcd_clear_border(uchar left, uchar top, uchar right, uchar bottom);
void lcd_glyph(uchar left, uchar top, uchar width, uchar height, uchar *glyph, uchar store_width);
void lcd_animated_glyph(uchar left, uchar top, uchar width, uchar num_of_bitmaps, uchar *glyph1, uchar *glyph2, uchar *glyph3);
void lcd_text(uchar left, uchar top, uchar font, char *str);
void lcd_update(uchar top, uchar bottom);
void lcd_cursor_set(uchar left, uchar top, uchar font, uchar ShowCursor, uchar blinking_rate);
/**/
void lcd_contrast_level(uchar level);
void lcd_contrast_up(void);
void lcd_contrast_down(void);
void lcd_sleepmode_enter(void);
void lcd_sleepmode_exit(void);
void lcd_scroll_init(char *buf);
 uchar lcd_font_width_get(uchar font_width);
/*Static functions*/
static void LCD_setup_cdev(lcd_sc14450_dev_struct *dev);
//Timers
static void InitializeTimer(struct timer_list *timer_ptr, void (*timer_function)(unsigned long param));
static void lcd_timer_refresh_fn(unsigned long param);
static void scroll_timer_refresh(unsigned long param);
static void cursor_timer_refresh(unsigned long param);
static void animation_timer_refresh(unsigned long param);

//Module specific
static int __init LCD_init_module(void);
void  LCD_cleanup_module(void);

// Fops prototypes
int	LCD_open(struct inode *inode, struct file *filp);
int LCD_release(struct inode *inode, struct file *filp);
int LCD_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg);
/*** Parameters which can be set at load time  ***/
int lcd_sc14450_major =   LCD_SC14450_MAJOR;
int lcd_sc14450_minor =   0;

module_param(lcd_sc14450_major, int, S_IRUGO);
module_param(lcd_sc14450_minor, int, S_IRUGO);


MODULE_AUTHOR("George Giannaras");
MODULE_LICENSE("Dual BSD/GPL");
//#define AUTO_REFRESH
/***      Var definitions  and initializations     ***/
struct lcd_sc14450_conf_struct lcd_init_conf = {
	/* Set lcd default configuration */
#ifdef AUTO_REFRESH
	.refresh_tmout=500,
#else
	.refresh_tmout=0,
#endif
};
struct file_operations lcd_sc14450_fops = {
	.owner =    THIS_MODULE,
	.ioctl =    LCD_ioctl,
	.open =     LCD_open,
	.release =  LCD_release,
};
lcd_sc14450_dev_struct lcd_dev;

struct lcd_sc14450_conf_struct *lcd_conf = &(lcd_dev.lcd_conf);
const unsigned char l_mask_array[8] ={0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
uchar l_display_array[Y_BYTES][X_BYTES];

cursor_struct m_cursor;

#define MAX_ANIMATED_BITMAPS 8
animation_struct m_animation[MAX_ANIMATED_BITMAPS];

/***      Timer initializations     ***/
struct timer_list lcd_timer;
struct timer_list scroll_timer;
struct timer_list cursor_timer;
struct timer_list animation_timer;
 /***      Tasklet     ***/
//HW Patch

/***************          Main body             ***************/
unsigned char scr_top_border, scr_bottom_border, scr_left_border, scr_right_border, scr_left_dim, scr_right_dim;
unsigned char scr_left_offset, scr_right_offset;
/****************************************************************************************************/
/************                                        LOW LEVEL DRIVER FUNTIONS                                                            *********/
/****************************************************************************************************/

/*******************************************************
	Description: 	Initializes hw and driver internal state
	Note:
 ********************************************************/
void lcd_init_col_lines(unsigned char screen_top, unsigned char screen_bottom, \
						unsigned char screen_left, unsigned char screen_right){
	//Setup the line\column borders

	if( (screen_bottom<=screen_top) || (screen_right<=screen_left))
		return;

	//Check if dimensions not alligned to 2pixels or 3 pixels
	scr_top_border =  DDRAM_X_OFFSET+screen_top;
	scr_bottom_border = DDRAM_X_OFFSET+screen_bottom;
	scr_left_border = screen_left;
	scr_right_border = screen_right;

	scr_left_dim =  DDRAM_Y_OFFSET+screen_left/3;
	scr_left_offset =  screen_left%3;
	if(screen_right%3){
		scr_right_dim =  DDRAM_Y_OFFSET+screen_right/3;
		scr_right_offset =  3-screen_right%3;
	}
	else{
		scr_right_dim =  DDRAM_Y_OFFSET+screen_right/3;
		scr_right_offset =  0;
	}

	lcd_out_ctl( 0x0042 ); //Line Address Set
	lcd_out_ctl( scr_top_border ); //Start Line
	lcd_out_ctl( scr_bottom_border); //End Line
	lcd_out_ctl( 0x0043 ); //Column Address Set
	lcd_out_ctl( scr_left_dim ); //Start Column=0
	lcd_out_ctl( scr_right_dim); //End Column =84 //columns represent 16-bit words = 3 pixels

}
		#define set_col2(X) \
	({\
			register short __r4 __asm__ ("r4") = (short)(X) ; \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"nop \n\t"\
    			"movd $0x1F:m,(r1,r0)\n\t"\
    			"1: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 1b\n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"nop \n\t"\
    			"nop \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1090000:l,(r1,r0) \n\t"\
    			"storw	r4,0x0:(r1,r0) \n\t"\
    			"nop \n\t"\
    			"movd $0x1F:m,(r1,r0)\n\t"\
    			"2: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 2b\n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
    			"nop \n\t"\
    			"nop \n\t"\
			    :"=r" (__r4) \
			    : "0" (__r4) \
			    : "r0","r1","r2","r3"); \
			 \
		})

		#define RESET_LCD {set_col2((0xFF)|((0)<<8)|(0<<13));}
		#define SET_LCD {set_col2((0xFF)|0x4000|((0)<<8)|(0<<13));}


void lcd_init(){
	unsigned char i;
	/*Processor specific commands*/
	OVER_16M_SDRAM_ACCESS_PATCH_FLAG;

#ifndef CONFIG_RAM_SIZE_32MB
	EBI_ACS4_CTRL_REG = 9;
#endif

	PROTECT_OVER_16M_SDRAM_ACEESS
	//setup timings
	///EBI_SMTMGR_SET1_REG= (3<<16)|(0xF<<10)|(3<<8)|(3<<6)|(0xF<<0);
	// EBI_SMTMGR_SET1_REG= (3<<16)|(0x1F<<10)|(3<<8)|(3<<6)|(0x1F<<0);
	//---EBI_SMTMGR_SET2_REG=0x20E44;
#ifdef CONFIG_L_V3_BOARD
	EBI_SMTMGR_SET2_REG=0x10944;
#else
	EBI_SMTMGR_SET2_REG=(3<<16)|(0xF<<10)|(3<<8)|(3<<6)|(0xF<<0);
#endif

	//10000100101000100

	//00 00 0 0 00 - 0 0000 001 - 0000 10 01 - 01  00 0100
	//       00    -     01      -     09    -      44


	//00 00 0 0 00 - 0 010 1 011 - 000101 11 - 11 - 000100
	//       00    -     2B      -     17    -      C8
	//EBI_SMTMGR_SET2_REG=0x72d8a;

#ifdef CONFIG_L_V3_BOARD
	// setup lcd interface
	SetPort(P2_10_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_ACS3);
	EBI_ACS3_LOW_REG = LCD_BASE;    /* ACS3 base address */
	EBI_ACS3_CTRL_REG = 0x221;      /* use time setting 1, memory is sram, size is 64K  */

	//setup latch
	SetPort(P2_09_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_ACS2);        /* P2_09 is ACS2 */
	EBI_ACS2_CTRL_REG= 0x121;										/* use time setting 1, memory is sram, size is 64K */
	EBI_ACS2_LOW_REG=0x01090000;										/* ACS2 base address is 0x1090000 */
	//(*(volatile unsigned short *)(LCD_LATCH)=(0x2000));//Set backlight on, rst low
#elif defined (CONFIG_L_V3_BOARD_CNX)
	// setup lcd interface
	SetPort(P2_06_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_ACS3);
	EBI_ACS3_LOW_REG = LCD_BASE;    /* ACS3 base address */
	EBI_ACS3_CTRL_REG = 0x221;      /* use time setting 1, memory is sram, size is 64K  */

	//setup latch
	SetPort(P0_02_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_ACS2);        /* P2_09 is ACS2 */
	EBI_ACS2_CTRL_REG= 0x121;										/* use time setting 1, memory is sram, size is 64K */
	EBI_ACS2_LOW_REG=0x01090000;										/* ACS2 base address is 0x1090000 */
	//(*(volatile unsigned short *)(LCD_LATCH)=(0x2000));//Set backlight on, rst low

#endif	
	RESTORE_OVER_16M_SDRAM_ACEESS

	mdelay(100);

	PROTECT_OVER_16M_SDRAM_ACEESS
	//(*(volatile unsigned short *)(LCD_LATCH)=(0x6000));//Set backlight on, rst high
	RESTORE_OVER_16M_SDRAM_ACEESS
	mdelay(1);

	//Copied from datasheet

	RESET_LCD;
	mdelay(100);
	SET_LCD;
	mdelay(50);			//required for busy time
	lcd_out_ctl (0x50); // OFF
	lcd_out_ctl (0x2C); //Standby OFF
	mdelay(500);
//----------------------------------------------------
	lcd_out_ctl (0x02);
	lcd_out_ctl (0x01);	//OSC ON
//----------------------------------------------------
	mdelay(50);
	lcd_out_ctl (0x26);
	lcd_out_ctl (0x01);	//DC/DC1 ON
	mdelay(50);
	lcd_out_ctl (0x26);
	lcd_out_ctl (0x09);	//AMP ON/OFF
	mdelay(50);
	lcd_out_ctl (0x26);
	lcd_out_ctl (0x0B);	//DC/DC2 ON
	mdelay(50);
	lcd_out_ctl (0x26);
	lcd_out_ctl (0x0F);	//DC/DC3 ON
	mdelay(50);

//----------------------------------------------------
	lcd_out_ctl (0x10);
	lcd_out_ctl (0x1c);	//DLN=11,SDIR=0,SWP=0
//----------------------------------------------------
	lcd_out_ctl (0x20);
	lcd_out_ctl (0x01);	//DC-DC2 x1, DC-DC1 x1
//----------------------------------------------------
	lcd_out_ctl (0x24);
	lcd_out_ctl (0x08);	//Clock Division /16,/16
//----------------------------------------------------
	lcd_out_ctl (0x28);
	lcd_out_ctl (0x01);	//Clock Division /16,/16
//----------------------------------------------------
#ifdef CONFIG_L_V3_BOARD
	lcd_out_ctl (0x2A);
	lcd_out_ctl (0x99);	//Contrast Control
#elif defined (CONFIG_L_V3_BOARD_CNX)
	lcd_out_ctl (0x2A);
	lcd_out_ctl (0xA9);	//Contrast Control
#endif
//----------------------------------------------------
	lcd_out_ctl (0x30);
	lcd_out_ctl (0x0a);	//Adressing Mode
//----------------------------------------------------
	lcd_out_ctl (0x32);
	lcd_out_ctl (0x0e);	//Adressing Mode
//----------------------------------------------------
	lcd_out_ctl (0x34);
	lcd_out_ctl (0x90);	//Adressing Mode
//----------------------------------------------------
	lcd_out_ctl (0x36);
	lcd_out_ctl (0x01);	//Adressing Mode
//----------------------------------------------------
	lcd_out_ctl (0x40);
	lcd_out_ctl (0x80);	//Adressing Mode
//----------------------------------------------------
	lcd_out_ctl (0x42);
	lcd_out_ctl (DDRAM_X_OFFSET);	//X AREA SET
	lcd_out_ctl (95);	//X AREA SET
//----------------------------------------------------
	lcd_out_ctl (0x43);
	lcd_out_ctl (DDRAM_Y_OFFSET);	//Y AREA SET
	lcd_out_ctl (95);	//Y AREA SET
//----------------------------------------------------
	lcd_out_ctl (0x45);
	lcd_out_ctl (0x0);
	lcd_out_ctl (0x53);
	lcd_out_ctl (0x0);
	lcd_out_ctl (0x55);
	lcd_out_ctl (0x0);

//----------------------------------------------------
	mdelay(50);
//----------------------------------------------------
	mdelay(50);
	lcd_out_ctl (0x51); //Display ON
//----------------------------------------------------

//----------------------------------------------------
	lcd_out_ctl (0x42);
	lcd_out_ctl (16);	//X AREA SET
	lcd_out_ctl (95);	//X AREA SET
//----------------------------------------------------
	lcd_out_ctl (0x43);
	lcd_out_ctl (46);	//Y AREA SET
	lcd_out_ctl (95);	//Y AREA SET


//================================
	lcd_clear_ram();


	/*This code is not part of the hw init sequence*/
 	m_cursor.state =0;
	m_cursor.enabled =0;
	for (i=0;i<MAX_ANIMATED_BITMAPS;i++)
	{
		m_animation[i].state = 0;
		m_animation[i].enabled = 0;
		m_animation[i].bitmaps = 0;
	}

	InitializeTimer(&animation_timer, animation_timer_refresh);
	InitializeTimer(&cursor_timer, cursor_timer_refresh);
	InitializeTimer(&scroll_timer, scroll_timer_refresh);


 }


 /*******************************************************
	Description:	Erases lcd RAM
	Note:
 ********************************************************/
void lcd_clear_ram(void)
{
	uchar p, i;
	OVER_16M_SDRAM_ACCESS_PATCH_FLAG;

	if (m_cursor.enabled){
		m_cursor.enabled=0;
		STOP_TIMER(cursor_timer)
	}
	lcd_init_col_lines(SCRN_TOP, SCRN_BOTTOM, SCRN_LEFT, SCRN_RIGHT );

	for(p=0; p<=SCRN_BOTTOM; p++)
	{
		l_display_array[p][0]=0; /*Clear buffer*/
		lcd_out_2b3_dat(0,0,0);
		for(i=1; i<SCRN_RIGHT; i+=3)
		{
			l_display_array[p][i]=0; /*Clear buffer*/
			l_display_array[p][i+1]=0; /*Clear buffer*/
			l_display_array[p][i+2]=0; /*Clear buffer*/
			lcd_out_2b3_dat(0,0,0);
		}
	}
}

/*
**
** 	Clears the display memory starting at the left/top  and going to
**  the right/bottom . No runtime error checking is performed. It is
**  assumed that left is less than right and that top is less than
**  bottom
**
*/

void lcd_clear_rect(uchar left,  uchar top,
			        uchar right, uchar bottom)
{
		unsigned short i;
		unsigned int width, height;

		width=right-left;
		height=bottom-top;

		if((!width) || (!height))
			return;

			for(i=0; i<height; i++)
			{
				memset (&(l_display_array[top+i][left]),0,width);
			}
			lcd_update32 (top, height, left, width);
}

/*
**
** Inverts the display memory starting at the left/top and going to
** the right/bottom. No runtime error checking is performed. It is
** assumed that left is less than right and that top is less than
** bottom
**
*/

void lcd_invert_rect(uchar left,  uchar top,
			         uchar right, uchar bottom)
{

	unsigned short i, j;
	unsigned int width, height;

	width=right-left;
	height=bottom-top;

	if((!width) || (!height))
		return;

		for(i=0; i<height; i++)
		{
			for(j=left; j<right; j++)
			{
				l_display_array[top+i][j]=(l_display_array[top+i][j])^0xF8;
			}
		}

		lcd_update32 (top, height, left, width);

}

/*
**
** Draws a line into the display memory starting at left going to
** right, on the given row. No runtime error checking is performed.
** It is assumed that left is less than right.
**
*/

void lcd_horz_line(uchar left, uchar right, uchar row)
{
	uchar col;

  	for(col = left; col <= right; col++)
  	{
    	l_display_array[row][col] = 0xFF;
  	}

  	lcd_update32 (row, 1, left, right-left);
}

/*
**
** Draws a vertical line into display memory starting at the top
** going to the bottom in the given column. No runtime error checking
** is performed. It is assumed that top is less than bottom and that
** the column is in range.
**
*/

void lcd_vert_line(uchar top, uchar bottom, uchar column)
{
	uchar row;

  	for(row = top; row <= bottom; row++)
  	{
    	l_display_array[column][row] = 0xFF;
  	}

  	lcd_update32 (top, bottom-top, column, 1);
}

/*
**
** Clears a line into the display memory starting at left going to
** right, on the given row. No runtime error checking is performed.
** It is assumed that left is less than right.
**
*/

void lcd_clr_horz_line(uchar left, uchar right,
		               uchar row)
{
	uchar col;

  	for(col = left; col <= right; col++)
  	{
    	l_display_array[row][col] = 0x00;
  	}

  	lcd_update32 (row, 1, left, right-left);
}


/*
**
** Clears a vertical line into display memory starting at the top
** going to the bottom in the given column. No runtime error checking
** is performed. It is assumed that top is less than bottom and that
** the column is in range.
**
*/

void lcd_clr_vert_line(uchar top, uchar bottom, uchar column)
{
	uchar row;

  	for(row = top; row <= bottom; row++)
  	{
    	l_display_array[column][row] = 0x00;
  	}

  	lcd_update32 (top, bottom-top, column, 1);
}

/*
**
** 	Draws a box in display memory starting at the left/top and going
**  to the right/bottom. No runtime error checking is performed.
**  It is assumed that left is less than right and that top is less
**  than bottom.
**
*/

void lcd_draw_border(uchar left, uchar top, uchar right, uchar bottom)
{

  	/* to draw a box requires two vertical lines */
   	lcd_vert_line(top,bottom,left);
   	lcd_vert_line(top,bottom,right);
  	/* and two horizonal lines */
   	lcd_horz_line(left,right,top);
   	lcd_horz_line(left,right,bottom);
return ;
}

/*
**
** Clears a box in display memory starting at the Top left and going
** to the bottom right. No runtime error checking is performed and
** it is assumed that Left is less than Right and that Top is less
** than Bottom.
**
*/

void lcd_clear_border(uchar left, uchar top, uchar right, uchar bottom)
{

  	/* to undraw the box undraw the two vertical lines */
  	lcd_clr_vert_line(top,bottom,left);
  	lcd_clr_vert_line(top,bottom,right);

  	/* and the two horizonal lines that comprise it */
  	lcd_clr_horz_line(left,right,top);
    lcd_clr_horz_line(left,right,bottom);
}

/*
**
** Writes a glyph to the display at location x,y
**
** Arguments are:
**    column     - x corrdinate of the left part of glyph
**    row        - y coordinate of the top part of glyph
**    width  	 - size in pixels of the width of the glyph
**    height 	 - size in pixels of the height of the glyph
**    glyph      - an uchar pointer to the glyph pixels
**                 to write assumed to be of length "width"
**
*/

void lcd_glyph(uchar left, uchar top,
			   uchar width, uchar height,
			   uchar *glyph, uchar store_width)
{
	uchar byte_offset;
	uchar y_bits;
	uchar char_mask;
	uchar x;
	uchar *glyph_scan;
	uchar glyph_offset;

	glyph_offset = 0;			/* start at left side of the glyph rasters */
    char_mask = 0x80;			/* initial character glyph mask */

  	for (x = left; x < (left + width); x++)
  	{
    	byte_offset = top;        		/* get the byte offset into y direction */
		y_bits = height;				/* get length in y direction to write */
		glyph_scan = glyph + glyph_offset;	 /* point to base of the glyph */

    	/* boundary checking here to account for the possibility of  */
    	/* write past the bottom of the screen.                        */
    	while((y_bits) && (byte_offset < Y_BYTES)) /* while there are bits still to write */
    	{
			/* check if the character pixel is set or not */
			if(*glyph_scan & char_mask)
			{
				l_display_array[byte_offset][x] = 0xFF;	/* set image pixel */
			}
			else
			{
      			l_display_array[byte_offset][x] = 0;	/* clear the image pixel */
			}

			y_bits--;
			byte_offset++;

			/* bump the glyph scan to next raster */
			glyph_scan += store_width;
		}
		/* shift over to next glyph bit */
		char_mask >>= 1;
		if(char_mask == 0)				/* reset for next byte in raster */
		{
			char_mask = 0x80;
			glyph_offset++;
	    }
	}
}

/*
**
** Writes a graphic to the display at location x,y
**
** Arguments are:
**    column     - x corrdinate of the left part of glyph
**    row        - y coordinate of the top part of glyph
**    width  	 - size in pixels of the width of the glyph
**    height 	 - size in pixels of the height of the glyph
**    pixelmode  - 0: 3B3P mode, 1: 2B3P mode
**
*/
#ifdef BENCHMARK
void setstarttime();
void  printtest();
#endif


unsigned int lcd_out_2b3_linedat(unsigned short int xstart,unsigned short int xend, long buf)
{
	volatile register short __r4 __asm__ ("r4") = (short)(xstart);
	volatile register short __r5 __asm__ ("r5") = (short)(xend);
	volatile register long __r10  __asm__ ("r10") = (long)(buf);

	asm volatile(\
					"sprd	psr,(r3,r2) \n\t"\
					"push $2,r2 \n\t"\
					"di \n\t"\
					"push $2,r12 \n\t"\
					"movd	$0x9:m,(r1,r0) \n\t"\
					"stord	(r1,r0),0xff0064 \n\t"\
					"movd	$0x1080080:l,(r12) \n\t"\
					"movd	$0x00F800E0:m,(r1,r0) \n\t"\
					"movd	$0x3,(r3,r2) \n\t"\
					"1: \n\t"\
					"loadd	 0x0:(r11,r10),(r9,r8)   \n\t"\
					"movzb 	 r8,r7 \n\t"\
					"/* Data are in r7, (r8 shifted <<8) ,r9 we need to do the 3 to 2 */ \n\t"\
					"/* ((X&0xF8)|((Y>>5)&0x7)) */ \n\t"\
					"/* (((Y<<3)&0xE0)|((Z>>3)&0x1F)) */ \n\t"\
					"/*increase r4 and r11,r10 by 3   */ \n\t"\
					"movw r8,r6 \n\t"\
					"lshw $-13,r6 \n\t"\
					"andw r1,r7 \n\t"\
					"orb r6,r7 \n\t"\
					"movw $0x9:m,r6 \n\t"\
					"storw	r6,0xff0064 \n\t"\
					"storw	r7,0x0:(r12) \n\t"\
					"ashuw $-5,r8 \n\t"\
					"andw r0,r8 \n\t"\
					"andw r1,r9 \n\t"\
					"lshw $-3,r9 \n\t"\
					"orb r8,r9 \n\t"\
					"storw	r6,0xff0064 \n\t"\
					/*Required for correct LCD writes*/
					"storw	r9,0x0:(r12) \n\t"\
					"/*increase r4 and r11,r10 by 3  */ \n\t"\
					"addd (r3,r2),(r11,r10) \n\t"\
					"addw r2,r4 \n\t"\
					"cmpw r5,r4  \n\t"\
					"bhi 1b \n\t"\
					"movd	$0xa,(r1,r0) \n\t"\
					"stord	(r1,r0),0xff0064 \n\t"\
					"xorw r5,r5 \n\t"\
					"pop $2,r12 \n\t"\
					"pop $2,r2 \n\t"\
					"lprd	(r3,r2), psr \n\t"\
				:"=r" (__r4),"=r" (__r5),"=r" (__r10)\
				: "0" (__r4),"1" (__r5),"2" (__r10)\
				: "r0","r1","r2","r3","r6","r7","r8","r9");
	return (__r4-xstart);

}

const unsigned char bulkstartarray[]={1,2,0};

void lcd_invert32_noupdate	(  uchar left, uchar top,
							   uchar width, uchar height ,
							   char invert)
{
	char linedata [X_BYTES];
	unsigned short i, j;
	unsigned short xstart,xend,yend,ystart;
	unsigned char lcddata[3];
	unsigned short int byteswritten;


	//			012345678901234567890123456789012345678901234567890123456789
	//			2012012012012012012012012012012012012012012012012012012012
	//left                    ^
	//width                   ----------------------------^
	//xstart-X_OFFSET        ^
	//xend-X_OFFSET                                        ^
	//bulkstart                 ^
	//bulkend                                           ^

	//writes are from xstart-XOFFSET if xstart!=0 else xstart
	//				up to xend.

	if((!width) || (!height))
			return;

	xstart=(left+X_OFFSET)-((left+X_OFFSET)%3);

	xend=left+width;
	xend=(xend+X_OFFSET)-((xend+X_OFFSET)%3);

	yend=top+height;
	ystart=top;

	for(i=0; i<height; i++)
	{
		lcd_init_col_lines(ystart+i, yend, xstart, xend );

		if (invert)
		{
			if (xstart!=0)
				memcpy (&linedata[xstart-X_OFFSET],&(l_display_array[ystart+i][xstart-X_OFFSET]),\
						xend-(xstart-X_OFFSET)+1);
			else
				memcpy (&linedata[xstart],&(l_display_array[ystart+i][0]),\
						xend+1);

			for (j=left;j<(left+width);j++)
			{
				linedata[j]^=0xFF;
			}

			if (xstart==0)
			{
				lcddata[0]=lcddata[1]=0;
				lcddata[2]=linedata[0];
				lcd_out_2b3_dat(lcddata[0],lcddata[1],lcddata[2]);
				byteswritten=lcd_out_2b3_linedat(1,xend, \
						(long)&(linedata[1]));
			}
			else
			{
				byteswritten=lcd_out_2b3_linedat(xstart-X_OFFSET,xend,\
						(long)&(linedata [xstart-X_OFFSET]));
			}
		}
		else
		{
			if (xstart==0)
			{
				lcddata[0]=lcddata[1]=0;
				lcddata[2]=l_display_array[ystart+i][0];
				lcd_out_2b3_dat(lcddata[0],lcddata[1],lcddata[2]);
				byteswritten=lcd_out_2b3_linedat(1,xend, \
						(long)&(l_display_array[ystart+i][1]));
			}
			else
			{
				byteswritten=lcd_out_2b3_linedat(xstart-X_OFFSET,xend,\
						(long)&(l_display_array[ystart+i][xstart-X_OFFSET]));
			}
		}
	}

	lcd_init_col_lines(SCRN_TOP, SCRN_BOTTOM, SCRN_LEFT, SCRN_RIGHT  );

}


void lcd_graphic32(uchar left, uchar top,
			   uchar width, uchar height,
			   uchar *buf)
{
	unsigned short i, k=0;
	unsigned short xstart,xend,yend,ystart;
	unsigned char lcddata[3];
	unsigned short int byteswritten;



	//			012345678901234567890123456789012345678901234567890123456789
	//			2012012012012012012012012012012012012012012012012012012012
	//left                    ^
	//width                   ----------------------------^
	//xstart-X_OFFSET        ^
	//xend-X_OFFSET                                        ^
	//bulkstart                 ^
	//bulkend                                           ^

	//writes are from xstart-XOFFSET if xstart!=0 else xstart
	//				up to xend.

	if((!width) || (!height))
			return;

	xstart=(left+X_OFFSET)-((left+X_OFFSET)%3);

	xend=left+width;
	xend=(xend+X_OFFSET)-((xend+X_OFFSET)%3);

	yend=top+height;
	ystart=top;

	#ifdef BENCHMARK
			setstarttime();
	#endif
	for(i=0; i<height; i++)
	{
		lcd_init_col_lines(ystart+i, yend, xstart, xend );
		memcpy (&(l_display_array[ystart+i][left]), &buf[k],width);
		k+=width;

		if (xstart==0)
		{
			lcddata[0]=lcddata[1]=0;
			lcddata[2]=l_display_array[ystart+i][0];
			lcd_out_2b3_dat(lcddata[0],lcddata[1],lcddata[2]);
			byteswritten=lcd_out_2b3_linedat(1,xend, \
					(long)&(l_display_array[ystart+i][1]));
		}
		else
		{
			byteswritten=lcd_out_2b3_linedat(xstart-X_OFFSET,xend,\
					(long)&(l_display_array[ystart+i][xstart-X_OFFSET]));
		}

		if ((i&3)==3)
		{
	#ifdef BENCHMARK
					printtime();
	#endif
					yield();
	#ifdef BENCHMARK
					setstarttime();
	#endif
		}

	}

	lcd_init_col_lines(SCRN_TOP, SCRN_BOTTOM, SCRN_LEFT, SCRN_RIGHT  );
}

void lcd_animated_glyph(uchar left, uchar top,
				uchar width, uchar num_of_bitmaps, uchar *glyph1, uchar *glyph2, uchar *glyph3)
{
	int i;
	STOP_TIMER(animation_timer);

 	// if width == 0, stop animation and clear glyph region
	if(width == 0) {
 		for (i=0;i<MAX_ANIMATED_BITMAPS;i++) {
			if (m_animation[i].enabled && (m_animation[i].left == left) && (m_animation[i].top == top)) {

				lcd_clear_rect(left, top, left + m_animation[i].width, top + m_animation[i].width);

				m_animation[i].enabled = 0;
				m_animation[i].state = 0;

 				lcd_update(top, top + m_animation[i].width);
 				break;
			}
		}

		for (i=0;i<MAX_ANIMATED_BITMAPS;i++) {
			if (m_animation[i].enabled) {
				START_TIMER(animation_timer, 100 );
				return;
			}
		}

		return;
	}

	for (i=0;i<MAX_ANIMATED_BITMAPS;i++) {
		if (m_animation[i].enabled && (m_animation[i].left == left) && (m_animation[i].top == top)) {
			START_TIMER(animation_timer, 100 );
			return ;
		}
	}

	for (i=0;i<MAX_ANIMATED_BITMAPS;i++)
	{
	  if (!m_animation[i].enabled)
		  break;
	}
	if (i==MAX_ANIMATED_BITMAPS) {
 		START_TIMER(animation_timer, 100 );
		return ;
	}

	m_animation[i].enabled = 1;
	m_animation[i].left =left;
	m_animation[i].top =top;
	m_animation[i].width =width;
	m_animation[i].blinking_rate =200;
	m_animation[i].state =0;
	m_animation[i].bitmaps =num_of_bitmaps;
	memcpy(m_animation[i].bitmap[0],glyph1,width*width/8);
	memcpy(m_animation[i].bitmap[1],glyph2,width*width/8);
	memcpy(m_animation[i].bitmap[2],glyph3,width*width/8);

	lcd_glyph(left,top,width,width,&m_animation[i].bitmap[0][0],width/8);  /* plug symbol into buffer */
 	START_TIMER(animation_timer, 100 );
	lcd_update(SCRN_TOP, SCRN_BOTTOM);
 }


/*
**
**	Prints the given string at location x,y in the specified font.
**  Prints each character given via calls to lcd_glyph. The entry string
**  is null terminated and non 0x20->0x7e characters are ignored.
**
**  Arguments are:
**      left       coordinate of left start of string.
**      top        coordinate of top of string.
**      font       font number to use for display
**      str   	   text string to display
**
*/

void lcd_text(uchar left, uchar top, uchar font, char *str)
{
  	uchar x = left;
  	uchar glyph;
  	uchar width;
	uchar height;
	uchar store_width;
	uchar  *glyph_ptr;


if (str==NULL) {printk("INVALID STRING ....");}

  	while(*str != 0x00)
  	{
    	glyph = (uchar)*str;

		/* check to make sure the symbol is a legal one */
		/* if not then just replace it with the default character */
		if((glyph < fonts[font].glyph_beg) || (glyph > fonts[font].glyph_end))
		{
			glyph = fonts[font].glyph_def;
		}

    	/* make zero based index into the font data arrays */
    	glyph -= fonts[font].glyph_beg;
    	width = fonts[font].fixed_width;	/* check if it is a fixed width */
		if(width == 0)
		{
			width=fonts[font].width_table[glyph];	/* get the variable width instead */
		}

		height = fonts[font].glyph_height;
		store_width = fonts[font].store_width;

		glyph_ptr = fonts[font].glyph_table + ((unsigned int)glyph * (unsigned int)store_width * (unsigned int)height);

		/* range check / limit things here */
		if(x > SCRN_RIGHT)
		{
			x = SCRN_RIGHT;
		}
		if((x + width) > SCRN_RIGHT+1)
		{
			width = SCRN_RIGHT - x + 1;
		}
		if(top > SCRN_BOTTOM)
		{
			top = SCRN_BOTTOM;
		}
		if((top + height) > SCRN_BOTTOM+1)
		{
			height = SCRN_BOTTOM - top + 1;
		}

		lcd_glyph(x,top,width,height,glyph_ptr,store_width);  /* plug symbol into buffer */

		x += width;							/* move right for next character */
		str++;								/* point to next character in string */
	}
}


/*
**
** Updates area of the display. Writes data from display
** RAM to the lcd display controller.
**
** Arguments Used:
**    top     top line of area to update.
**    bottom  bottom line of area to update.
**
*/

int protection_flag = 0;
void lcd_update(uchar top, uchar bottom)
{
	if (protection_flag) return;
	protection_flag = 1;

		lcd_update32(SCRN_TOP, SCRN_BOTTOM-SCRN_TOP+1, SCRN_LEFT, SCRN_RIGHT-SCRN_LEFT+1);

	protection_flag=0;
}

void lcd_update32 (uchar top, uchar height, uchar left, uchar width)
{

	unsigned short i;
	unsigned short xstart,xend,yend,ystart;
	unsigned char lcddata[3];
	unsigned short int byteswritten;

	//			012345678901234567890123456789012345678901234567890123456789
	//			2012012012012012012012012012012012012012012012012012012012
	//left                    ^
	//width                   ----------------------------^
	//xstart-X_OFFSET        ^
	//xend-X_OFFSET                                        ^
	//bulkstart                 ^
	//bulkend                                           ^

	//writes are from xstart-XOFFSET if xstart!=0 else xstart
	//				up to xend.

	if((!width) || (!height))
			return;

	xstart=(left+X_OFFSET)-((left+X_OFFSET)%3);

	xend=left+width;
	xend=(xend+X_OFFSET)-((xend+X_OFFSET)%3);

	yend=top+height;
	ystart=top;

	#ifdef BENCHMARK
			setstarttime();
	#endif
	for(i=0; i<height; i++)
	{
		lcd_init_col_lines(ystart+i, yend, xstart, xend );

		if (xstart==0)
		{
			lcddata[0]=lcddata[1]=0;
			lcddata[2]=l_display_array[ystart+i][0];
			lcd_out_2b3_dat(lcddata[0],lcddata[1],lcddata[2]);
			byteswritten=lcd_out_2b3_linedat(1,xend, \
					(long)&(l_display_array[ystart+i][1]));
		}
		else
		{
			byteswritten=lcd_out_2b3_linedat(xstart-X_OFFSET,xend,\
					(long)&(l_display_array[ystart+i][xstart-X_OFFSET]));
		}

		if ((i&3)==3)
		{
	#ifdef BENCHMARK
					printtime();
	#endif
					yield();
	#ifdef BENCHMARK
					setstarttime();
	#endif
		}

	}

	lcd_init_col_lines(SCRN_TOP, SCRN_BOTTOM, SCRN_LEFT, SCRN_RIGHT  );


}
void lcd_cursor_set(uchar left, uchar top, uchar font, uchar ShowCursor, uchar blinking_rate)
{
	uchar width;

		STOP_TIMER(cursor_timer);
	 	width=fonts[font].fixed_width;
		if (width==0) width=8;

		if (m_cursor.enabled)
		{
			m_cursor.enabled=0;
	   		if(m_cursor.state)
			{

	   			lcd_invert32_noupdate	(  	m_cursor.left, m_cursor.top, \
											width , \
											fonts[m_cursor.font].glyph_height - 1, 0);
	/*			lcd_invert_rect(m_cursor.left, m_cursor.top, \
								m_cursor.left + width - 1, \
								m_cursor.top + fonts[m_cursor.font].glyph_height - 1);
								*/
				m_cursor.state = 0;
	  		}
		}

	 	if(ShowCursor == 0)
				return;

	 	m_cursor.left = left;
		m_cursor.top = top;
		m_cursor.font = font;
		m_cursor.blinking_rate = blinking_rate;

			lcd_invert32_noupdate	(  	m_cursor.left, m_cursor.top, \
									width , \
									fonts[m_cursor.font].glyph_height - 1, 1);


	  	//lcd_invert_rect(m_cursor.left, m_cursor.top, m_cursor.left + width - 1, m_cursor.top + fonts[m_cursor.font].glyph_height - 1);
		m_cursor.state = 1;
		m_cursor.enabled = 1;
	  	START_TIMER(cursor_timer, blinking_rate / 2);

}


void lcd_scroll_init(char *buf)
{
	char visible_buffer[50];
	char scroll_enable = buf[4];

  	if (m_scroll_struct.top == buf[1])
	{
  		STOP_TIMER(scroll_timer);
	}

	if ((scroll_enable == 0 )|| ( buf[3] >strlen(&buf[5])))
	{
  		memset(visible_buffer, 0 , 50);
		strncpy(visible_buffer, &buf[5], buf[3]);
 		lcd_text(buf[0], buf[1], buf[2], visible_buffer);
  		return;
	}


 	if( buf[3] <strlen(&buf[5]))
	{
		STOP_TIMER(scroll_timer);
		m_scroll_struct.left = buf[0];
		m_scroll_struct.top = buf[1];
		m_scroll_struct.font = buf[2];
		m_scroll_struct.visible_size = buf[3];
		strcpy(m_scroll_struct.text, &buf[5]);
		m_scroll_struct.size = strlen(m_scroll_struct.text);
		m_scroll_struct.ScrollPos = 0;

		memset(visible_buffer, 0 , 50);
		strncpy(visible_buffer, m_scroll_struct.text, m_scroll_struct.visible_size);
		lcd_text(m_scroll_struct.left, m_scroll_struct.top, m_scroll_struct.font, visible_buffer);


		//if(m_scroll_struct.visible_size < m_scroll_struct.size)
		{
			// add a space at the end of the text
			//strcat(m_scroll_struct.text, " ");
			//m_scroll_struct.size++;
			m_scroll_struct.enable =1;
  			START_TIMER(scroll_timer, 100);


		}
	}else{
  		memset(visible_buffer, 0 , 50);
		strncpy(visible_buffer, &buf[5], buf[3]);
 		lcd_text(buf[0], buf[1], buf[2], visible_buffer);
 		STOP_TIMER(scroll_timer);
	}

}
uchar lcd_font_width_get(uchar font_width)
{
 	return fonts[font_width].fixed_width;
}
static void InitializeTimer(struct timer_list *timer_ptr, void (*timer_function)(unsigned long param))
{
	init_timer(timer_ptr);
	timer_ptr->data = 0; //no argument for kbd_timer_fn needed
	timer_ptr->function = timer_function;
}

/*******************************************************
	Description: 	Sets the lcd contrast
	Input:		level: Brightness level (5-410). We map 0-255 to 0-511 by doubling the input level.
 ********************************************************/
void lcd_contrast_level(uchar level){
	OVER_16M_SDRAM_ACCESS_PATCH_FLAG;
	lcd_out_ctl (0x2A);
	lcd_out_ctl (level);	//Contrast Control
}
/*******************************************************
	Description:
	Input:
 ********************************************************/

void lcd_contrast_up(void){

	OVER_16M_SDRAM_ACCESS_PATCH_FLAG;
	printk ("lcd_contrast_up not implemented for s6b33 \r\n");

}
/*******************************************************
	Description:
	Input:
 ********************************************************/
void lcd_contrast_down(void){

	OVER_16M_SDRAM_ACCESS_PATCH_FLAG;
	printk ("lcd_contrast_down not implemented for s6b33 \r\n");

}
/*******************************************************
	Description:
	Input:
 ********************************************************/
void lcd_sleepmode_enter(void){
	OVER_16M_SDRAM_ACCESS_PATCH_FLAG;
	printk ("lcd_sleepmode_enter not implemented for s6b33 \r\n");
}
/*******************************************************
	Description:
	Input:
 ********************************************************/
void lcd_sleepmode_exit(void){
	OVER_16M_SDRAM_ACCESS_PATCH_FLAG;
	printk ("lcd_sleepmode_exit not implemented for s6b33 \r\n");

}
/****************************************************************************************************/
/************                                       HIGH LEVEL DRIVER FUNTIONS                                                            *********/
/****************************************************************************************************/

/*******************************************************
	Description: 	Open lcd dev file.

 ********************************************************/
int LCD_open(struct inode *inode, struct file *filp)
{
	lcd_sc14450_dev_struct *dev; /* device information */
	dev = container_of(inode->i_cdev, lcd_sc14450_dev_struct, cdev);
	filp->private_data = dev; /* for other methods */


	return 0;
}
/*******************************************************
	Description: 	Close device file.

 ********************************************************/
int LCD_release(struct inode *inode, struct file *filp)
{
	return 0;
}
/*******************************************************
	Description: 	Refresh timer routing.

 ********************************************************/
 static void lcd_timer_refresh_fn(unsigned long param){
	lcd_update(SCRN_TOP, SCRN_BOTTOM);
	START_TIMER(lcd_timer, lcd_conf->refresh_tmout);
}

 static void scroll_timer_refresh(unsigned long param)
 {
 	char visible_buffer[50];
	int i;

	m_scroll_struct.text[m_scroll_struct.size] = m_scroll_struct.text[0];

	for(i=0 ; i < m_scroll_struct.size ; i++)
		m_scroll_struct.text[i] = m_scroll_struct.text[i+1];

	m_scroll_struct.text[m_scroll_struct.size] = '\0';

	memcpy(visible_buffer, m_scroll_struct.text, m_scroll_struct.visible_size);
	visible_buffer[(int)m_scroll_struct.visible_size] = '\0';
	lcd_text(m_scroll_struct.left, m_scroll_struct.top, m_scroll_struct.font, visible_buffer);

	lcd_update(SCRN_TOP, SCRN_BOTTOM);
 	START_TIMER(scroll_timer, 100);
}


 static void animation_timer_refresh(unsigned long param) {
	int i;
	int found =0;

	for (i=0;i<MAX_ANIMATED_BITMAPS;i++)
	{
			if (m_animation[i].enabled)
			{
				m_animation[i].state++;
				if (m_animation[i].state==m_animation[i].bitmaps)
					m_animation[i].state = 0;

				lcd_glyph(m_animation[i].left,m_animation[i].top,
					m_animation[i].width,
					m_animation[i].width,
					&m_animation[i].bitmap[m_animation[i].state][0],
					m_animation[i].width/8);  /* plug symbol into buffer */
				found =1;
			}
	}
	lcd_update(SCRN_TOP, SCRN_BOTTOM);
	if (found){
		START_TIMER(animation_timer, 100 );
	}
  }
 static void cursor_timer_refresh(unsigned long param) {
 	uchar width;

	if (!m_cursor.enabled) {printk("timer not enabled ...\n");return;}

	width= fonts[m_cursor.font].fixed_width;
 	if (width==0)
  		width=8;//fonts[m_cursor.font].width_table[glyph];

	if (m_cursor.state==1) m_cursor.state=0;
	else m_cursor.state=1;

 	lcd_invert32_noupdate	(  	m_cursor.left, m_cursor.top, \
								width , \
								fonts[m_cursor.font].glyph_height - 1, m_cursor.state);

	//lcd_invert_rect(m_cursor.left, m_cursor.top,m_cursor.left+ width - 1, m_cursor.top + fonts[m_cursor.font].glyph_height - 1);

	START_TIMER(cursor_timer, m_cursor.blinking_rate / 2);
}

/*******************************************************
	Description: 	Kbd ioctl.

 ********************************************************/


#ifdef BENCHMARK
volatile unsigned int timestart;
volatile unsigned short int* timvalue2=(volatile u16 *)(0xFF497A);
volatile u16 moldtimstart=0;
volatile u16 moldtimend=0;

void setstarttime ()
{
	timestart=jiffies;
	moldtimstart=*timvalue2;
}

void printtest ()
{
			timestart=jiffies-timestart;
	        moldtimend=*timvalue2;

			if (moldtimstart<moldtimend)
				moldtimstart=moldtimend-moldtimstart;
	        else
	           {
					moldtimstart=moldtimend+0x2CFF-moldtimstart;
                 	timestart--;
               }
			printk ("T: %d | %d \r\n", timestart,moldtimstart);
			timestart=jiffies;
			moldtimstart=*timvalue2;

}

void printtime ()
{
			timestart=jiffies-timestart;
	        moldtimend=*timvalue2;

			if (moldtimstart>moldtimend)
				moldtimstart=moldtimstart-moldtimend;
	        else
	           {
					moldtimstart=0x2CFF+moldtimstart-moldtimend;
                 	timestart--;
               }

			printk ("Timespan: %d | %d \r\n", timestart,moldtimstart);
}

#endif


#define NOMMU_LGOPTIMIZE
unsigned char buf[MAX_IOCTL_ARGS];
int LCD_ioctl(struct inode *inode, struct file *filp,
                 unsigned int cmd, unsigned long arg)
{
	int i;

	lcd_sc14450_dev_struct *dev = filp->private_data;

		if (down_interruptible(&dev->sem))
			return -ERESTARTSYS;
		switch (cmd){
			case LCD_init:
				lcd_init();

				up (&dev->sem);
				return 0;
			//Issue a LCD cmd directly from the user space (it is assumed that it is a valid cmd)
			case LCD_out_ctl:

				if(copy_from_user(buf, (char*)arg, sizeof(char) )){//copy only addr and length first
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}

				lcd_out_ctl(buf[0]);
				up (&dev->sem);
				return 0;
			case LCD_out_data:
				if(copy_from_user(buf, (char*)arg, sizeof(char) )){//copy only addr and length first
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				printk (KERN_WARNING "IOCTL not supported  \n");

				up (&dev->sem);
				return 0;
			case LCD_clear_ram:

				lcd_clear_ram();
				up (&dev->sem);
				return 0;
			case LCD_clear_rect:

				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){//copy only addr and length first
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}

				lcd_clear_rect(buf[0], buf[1], buf[2], buf[3]);
				PDEBUG("LCD: ioctl LCD_clear_rect(%d, %d, %d, %d)\n", buf[0], buf[1], buf[2], buf[3]);
				up (&dev->sem);
				return 0;
			case LCD_invert_rect:
				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}

				lcd_invert_rect(buf[0], buf[1], buf[2], buf[3]);
				PDEBUG("LCD: ioctl LCD_invert_rect(%d, %d, %d, %d)\n", buf[0], buf[1], buf[2], buf[3]);
				up (&dev->sem);
				return 0;
			case LCD_draw_border:
				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}

				lcd_draw_border(buf[0], buf[1], buf[2], buf[3]);
				PDEBUG("LCD: ioctl LCD_draw_border(%d, %d, %d, %d)\n", buf[0], buf[1], buf[2], buf[3]);
				up (&dev->sem);
				return 0;
			case LCD_clear_border:
				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}

				lcd_clear_border(buf[0], buf[1], buf[2], buf[3]);
				PDEBUG("LCD: ioctl LCD_clear_border(%d, %d, %d, %d)\n", buf[0], buf[1], buf[2], buf[3]);
				up (&dev->sem);
				return 0;
			case LCD_text:
				/*Copy the string to be displayed*/
				i=0;
				while(i<MAX_IOCTL_ARGS){
					if(copy_from_user(buf+i, (char*)arg+i, sizeof(char))){
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}
					PDEBUG("LCD: ioctl LCD_text i: %x, copied: %x \n", i, buf[i]);
					if(i>2 && buf[i]==0)
						break;
					i++;
				}

				PDEBUG("LCD: ioctl LCD_text(%d, %d, %d, %s)\n", buf[0], buf[1], buf[2], &buf[3]);
				lcd_text(buf[0], buf[1], buf[2], &buf[3]);
				up (&dev->sem);
				return 0;

			case LCD_label:
				/*Copy the string to be displayed*/
				i=0;

				while(i<MAX_IOCTL_ARGS){

					if(copy_from_user(buf+i, (char*)arg+i, sizeof(char))){
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}

					if(i>4 && buf[i]==0)
						break;
					i++;
				}


				lcd_scroll_init(buf);

				up (&dev->sem);
				return 0;

			case LCD_glyph:
				if(copy_from_user(buf, (char*)arg, 3*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}

				if ((buf[2]*buf[2]/8)<(MAX_IOCTL_ARGS-3))
				{
					if(copy_from_user(&buf[3], (char*)arg+3, (buf[2]*buf[2]/8)*sizeof(char))){
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}
					PDEBUG("LCD: ioctl LCD_glyph(%d, %d, %d, %s)\n", buf[0], buf[1], buf[2], &buf[3]);

 					lcd_glyph((uchar)buf[0], (uchar)buf[1], (uchar)buf[2], (uchar)buf[2], (uchar*)&buf[3], (uchar)buf[2]/8);
					up (&dev->sem);
					return 0;
				}else {
					printk(KERN_WARNING "Too large bitmap\n");
					up (&dev->sem);
					return -EFAULT;
				}

				return 0;
			case LCD_graphic:

#ifndef NOMMU_LGOPTIMIZE
				if(copy_from_user(buf, (char*)arg, 5*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}

				if ((buf[2]*buf[3])<(MAX_IOCTL_ARGS-3))
				{
					if(copy_from_user(&buf[4], (char*)arg+4, (buf[2]*buf[3])*sizeof(char))){
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}
					PDEBUG("LCD: ioctl LCD_graphic32(%d, %d, %d, %d, %s)\n", buf[0], buf[1], buf[2], buf[3], &buf[4]);

 					lcd_graphic32((uchar)buf[0], (uchar)buf[1], (uchar)buf[2], (uchar)buf[3], (uchar*)&buf[4]);
				}else {
					printk(KERN_WARNING "Too large bitmap\n");
					up (&dev->sem);
					return -EFAULT;
				}
				up (&dev->sem);
				return 0;
#else
					lcd_graphic32(((uchar*)arg)[0], ((uchar*)arg)[1], ((uchar*)arg)[2], ((uchar*)arg)[3], &((uchar*)arg)[4]);
					up (&dev->sem);
					return 0;
#endif

			case LCD_animate_bitmap:
				if(copy_from_user(buf, (char*)arg, 4*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}

				if ((3 *buf[2]*buf[2]/8)<(MAX_IOCTL_ARGS-4))
				{
					if(copy_from_user(&buf[4], (char*)arg+4, (3*buf[2]*buf[2]/8)*sizeof(char))){
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}
					PDEBUG("LCD: ioctl LCD_glyph(%d, %d, %d, %d, %s)\n", buf[0], buf[1], buf[2], buf[3], &buf[4]);
 					lcd_animated_glyph((uchar)buf[0], (uchar)buf[1], (uchar)buf[2], (uchar)buf[3], (uchar*)&buf[4], (uchar*)&buf[4+buf[2]*buf[2]/8], (uchar*)&buf[4+2*buf[2]*buf[2]/8]);
					up (&dev->sem);
					return 0;
				}else {
					printk(KERN_WARNING "Too large bitmaps\n");
					up (&dev->sem);
					return -EFAULT;
				}

				return 0;

			case LCD_update:
				if(copy_from_user(buf, (char*)arg, 2*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				PDEBUG("LCD: ioctl LCD_update(%d, %d)\n", buf[0], buf[1]);
				lcd_update(buf[0], buf[1]);
				up (&dev->sem);
				return 0;
			case LCD_contrast_up:
				lcd_contrast_up();
				up (&dev->sem);
				return 0;
			case LCD_contrast_down:
				lcd_contrast_down();
				up (&dev->sem);
				return 0;
			case LCD_sleepmode_enter:
				lcd_sleepmode_enter();
				up (&dev->sem);
				return 0;
			case LCD_sleepmode_exit:
				lcd_sleepmode_exit();
				up (&dev->sem);
				return 0;
			case LCD_backlight_level:
				if(copy_from_user(buf, (char*)arg, sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				if(buf[0]){
					LCD_SET_BACKLIGHT_ON;
				}
				else{
					LCD_SET_BACKLIGHT_OFF;
				}
				up (&dev->sem);
				return 0;
			case LCD_contrast_level:
				if(copy_from_user(buf, (char*)arg, sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				lcd_contrast_level(buf[0]);
				PDEBUG("LCD: ioctl lcd_contrast_level(%d)\n", buf[0]);
				up (&dev->sem);
				return 0;
			case LCD_refresh_tmout:
				if ( (copy_from_user((char*)&lcd_conf->refresh_tmout, (char*)arg, sizeof(char))) ) {
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}
				STOP_TIMER(lcd_timer);
				if(lcd_conf->refresh_tmout){
					START_TIMER(lcd_timer, lcd_conf->refresh_tmout);
					PDEBUG("LCD: timer set to (%d)\n", lcd_conf->refresh_tmout);
				}
				else{
					STOP_TIMER(lcd_timer);
					PDEBUG("LCD: timer stopped \n");
				}
				up (&dev->sem);
				return 0;
			case LCD_show_cursor:
				if(copy_from_user(buf, (char*)arg, 5*sizeof(char) )){
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}

				PDEBUG("LCD: ioctl LCD_show_cursor(%d, %d, %d, %d, %d)\n", buf[0], buf[1], buf[2], buf[3], buf[4]);
				lcd_cursor_set(buf[0], buf[1], buf[2], buf[3], buf[4]);
				up (&dev->sem);
				return 0;

 			case LCD_fonts_width_get:

				buf[0]=lcd_font_width_get(buf[0]);
				if(copy_to_user((char*)arg, (char*) &buf[0],1)){
					printk(KERN_WARNING "Copy to user failed\n");
					up (&dev->sem);
					return -EFAULT;
		 		}
				up (&dev->sem);
				return 0;
	}


/**** DEBUGGING IOCTLS ****/
#ifdef LCD_SC14450_DEBUG
#endif
	printk(KERN_WARNING "Invalid lcd ioctl \n");
	return -ENOTTY;

}
/*******************************************************
	Description: 	Module initialization.

 ********************************************************/
 /*******************************************************
	Description: 	Cleanup the module and handle initialization failures .

 ********************************************************/
void LCD_cleanup_module(void)
{
	dev_t devno = MKDEV(lcd_sc14450_major, lcd_sc14450_minor);

	/* Get rid of our char dev entries */
	cdev_del(&lcd_dev.cdev);
	printk(KERN_NOTICE "lcd_sc14450: cleanup module\n");
	/* cleanup_module is never called if registering failed */
	unregister_chrdev_region(devno, 1);

}

/*******************************************************
	Description: 	Set up the cdev structure for this device.

 ********************************************************/
void LCD_setup_cdev(lcd_sc14450_dev_struct *dev)
{
	int err, devno = MKDEV(lcd_sc14450_major, lcd_sc14450_minor);

	cdev_init(&dev->cdev, &lcd_sc14450_fops);
	dev->cdev.owner = THIS_MODULE;
	dev->cdev.ops = &lcd_sc14450_fops;
	err = cdev_add (&dev->cdev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE "Error %d adding lcd_sc14450", err);
}

static int __init LCD_init_module(void)
{
	int result;
	dev_t dev = 0;

	/* Initializations*/
	PDEBUG("module init function, entry point\n");

/*
 * Get a range of minor numbers to work with, asking for a dynamic
 * major unless directed otherwise at load time.
 */
	if (lcd_sc14450_major) {
		dev = MKDEV(lcd_sc14450_major, lcd_sc14450_minor);
		result = register_chrdev_region(dev, 1, "lcd_sc14450");
	}
	else {
		result = alloc_chrdev_region(&dev, lcd_sc14450_minor, 1, "lcd_sc14450");
		lcd_sc14450_major = MAJOR(dev);
	}
	if (result < 0) {
		printk(KERN_WARNING "lcd_sc14450: can't get major %d\n", lcd_sc14450_major);
		printk( "FATAL ERROR ==========================lcd_sc14450: can't get major %d\n", lcd_sc14450_major);
		return result;
	}

	/*Initialize driver internal vars*/
	memset((char*)&lcd_dev, 0, sizeof(lcd_sc14450_dev_struct));
	/*Copy init configuration*/
	memcpy(lcd_conf, &lcd_init_conf, sizeof(struct lcd_sc14450_conf_struct));
	init_MUTEX(&lcd_dev.sem);
	spin_lock_init(&lcd_dev.lock);
	LCD_setup_cdev(&lcd_dev);
	/*Enable kbe int if needed*/
	printk(KERN_NOTICE "lcd_sc14450: driver for s6b33 controller initialized with major devno: %d\n", lcd_sc14450_major);

#ifdef LCD_SC14450_DEBUG
#endif
	/*Init hw*/
	lcd_init();
//	init_timer(&lcd_timer);
//	lcd_timer.data = 0; //no argument for kbd_timer_fn needed
//	lcd_timer.function = lcd_timer_refresh_fn;

	if(lcd_conf->refresh_tmout){
				printk("START_TIMER 13 \n");
		START_TIMER(lcd_timer, lcd_conf->refresh_tmout);
	}

	return 0; /* succeed */

  fail:
	unregister_chrdev_region(dev, 1);
	return result;
}

module_init(LCD_init_module);
module_exit(LCD_cleanup_module);









