/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <george.giannaras@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

// This is the header file for the experimental lcd driver for SC14450 developoment board
// 			Supported displays:
// LCD:  TOPWAY LM6038b (2x16 char display) ---- using lcd driver NT75451
// LCD:  TOPWAY LM6038b (2x20 char display) ---- using lcd driver NT75451 

/* 
	Driver interface description
	LCD		BOARD
	---------------------------
	RS		->        AD11
	DB[0..7]	->	DAB[0..7]
*/
#ifndef LCD_NT7538_H 
#define LCD_NT7538_H 
/***************       Definitions section             ***************/
/* HW dependent definitions */

/* Registers */
//Since RS -> AD11, data are accessed at 0x800 and instructions are accessed at 0x00
#define LCD_BASE 0x01300000
#define LCD_DR (LCD_BASE+0x800)
#define LCD_IR (LCD_BASE+0x00)

/* commands of the NT7538 LCD controller */
//CMD 1
#define LCD_DISP_OFF 		0xAE	/* turn LCD display OFF */
#define LCD_DISP_ON			0xAF	/* turn LCD display ON */
//CMD 2
#define LCD_SET_LINE		0x40	/* set the display RAM start line address(6 lsbs = ST5:ST4:ST3:ST2:ST1:ST0) */
//CMD 3
#define LCD_SET_PAGE		0xB0	/* set the display RAM page address (4 lsbs = P3:P2:P1:P0) */
//CMD 4
#define LCD_SET_COL_HI		0x10	/* set column address (4 lsbs = Y7:Y6:Y5:Y4) */
#define LCD_SET_COL_LO		0x00	/* set column address (4 lsbs = Y3:Y2:Y1:Y0) */
//CMD 5
#define LCD_READ_STATUS(x)  (x=*(volatile unsigned char *)(LCD_IR)) /* read the status data (4 msbs = RS7:RS6:RS5:RS4)*/
//CMD 6
#define LCD_WRITE_DATA(x)	(*(volatile unsigned char *)(LCD_DR)=(x)) /* write data to the display RAM (one byte)*/
//CMD 7
#define LCD_READ_DATA(x)	(x=*(volatile unsigned char *)(LCD_DR)) /* read data from the display RAM (one byte)*/
//CMD 8
#define LCD_SET_ADC_NOR		0xA0	/* set the display RAM addr SEG output for normal direction */
#define LCD_SET_ADC_REV		0xA1	/* set the display RAM addr SEG output for reverse direction */
//CMD 9
#define LCD_DISP_NOR		0xA6	/* set normal pixel display */
#define LCD_DISP_REV		0xA7	/* set reverse pixel display */
//CMD 10
#define LCD_EON_OFF			0xA4	/* set normal display mode */
#define LCD_EON_ON			0xA5	/* set entire dsplay on */
//CMD 11
#define LCD_SET_BIAS_1DIV9	0xA2	/* set LCD driving voltage bias to 1 div 9 */
#define LCD_SET_BIAS_1DIV7	0xA3	/* set LCD driving voltage bias to 1 div 7 */
//CMD 12
#define LCD_SET_MODIFY		0xE0	/* enter the "read-modify-write" mode, col addr will incr in each write disp data and decr in each read disp data */
//CMD 13
#define LCD_CLR_MODIFY		0xEE	/* clear the "read-modify-write" mode */
//CMD 14
#define LCD_RESET			0xE2	/* soft reset command */
//CMD 15
#define LCD_SET_SHL_NOR		0xC0	/* set COM scanning direction to normal */
#define LCD_SET_SHL_REV		0xC8	/* set COM scanning direction to reverse */
//CMD 16
#define LCD_PWR_CTL			0x28	/* set power circuit operation mode (3 lsbs = VC:VR:VF) */
//CMD 17
#define LCD_REG_RESISTOR	0x20	/* set the built in regulator resistor ratio Rb/Ra (3 lsbs = R2:R1:R0) */
//CMD 18  ( dual command) 
#define LCD_REF_VOLT_MODE	0x81	/* set reference voltage mode */
#define LCD_REF_VOLT_REG	0x00	/* set reference voltage register (display contrast value)  (6 lsbs = SV5:SV4:SV3:SV2:SV1:SV0) */
//CMD 19
#define LCD_ST_IND_MODE_OFF	0xAC	/* set static indicator mode to OFF*/
#define LCD_ST_IND_MODE_ON	0xAD	/* set static indicator mode to ON */
#define LCD_ST_IND_REG		0x00	/* set the static indicator register (2 lsbs = S1:S0) */
//CMD 20
#define LCD_NOP				0xE3	/* NOP command */

//Kernel MACROs
#define BUSY_FLAG			(1<<7)
#define LCD_WRITE_INST(x)	(*(volatile unsigned char *)(LCD_IR)=(x))
#define lcd_out_ctl(X)   {LCD_WRITE_INST(X);}
#define lcd_out_dat(X)   {LCD_WRITE_DATA(X);}
// #define lcd_out_ctl(X)   {spin_lock_irq(&lcd_dev.lock);LCD_WRITE_INST(X);spin_unlock_irq(&lcd_dev.lock);}
// #define lcd_out_dat(X)   {spin_lock_irq(&lcd_dev.lock);LCD_WRITE_DATA(X);spin_unlock_irq(&lcd_dev.lock);}
/* LCD screen and bitmap image array consants */
#define X_BYTES				128
#define Y_BYTES	      		8

#define COL_MIN				0x04//0x00
//#define COL_MAX				0x83
 
	#define SCRN_TOP			0
	#define SCRN_BOTTOM			64
	#define SCRN_LEFT			0//0x04
	#define SCRN_RIGHT			127//0x83
 

#define PAGE_MIN			0
#define PAGE_MAX			(SCRN_BOTTOM/8)

//Processor specific commands
#define LCD_BACKLIGHT_LEVEL(X)  (TIMER2_DUTY_REG=X|X<<8)

/***      Data types definitions       ***/
struct lcd_sc14450_conf_struct{	
	// unsigned short flags;	
	unsigned char refresh_tmout;	
} ;
//lcd_sc14450_conf_struct Flags 
// #ifdef AUTO_REFRESH	
	// #define	REFRESH_TIMER_ON  0x0001	
// #endif	

//IOCTL Commands
typedef enum IOCTL_Cmd_enum {
	LCD_init,		// 0
	LCD_out_ctl,		// 1
	LCD_clear_ram,		// 2
	LCD_clear_rect,		// 3
	LCD_invert_rect,	// 4
	LCD_draw_border,	// 5
	LCD_clear_border,	// 6
	LCD_text,		// 7
	LCD_label,		// 8
	LCD_glyph,		// 9
	LCD_graphic,		// 10
	LCD_animate_bitmap,	// 11
	LCD_update,		// 12
	LCD_standby_enter,	// 13
	LCD_standby_exit,	// 14
	LCD_sleepmode_enter,	// 15
	LCD_sleepmode_exit,	// 16
	LCD_backlight_level,	// 17
	LCD_contrast_level,	// 18
	LCD_refresh_tmout,	// 19
	LCD_show_cursor,	// 20
 	LCD_fonts_width_get,	// 21
#ifdef CONFIG_IN_V1_BOARD
	LCD_ram_copy,		// 22	
#endif
	LCD_IOCTL_MAX//this is a delimiter
} IOCTL_Cmd;
 

/****************************************************************************/
/**************           HW INDEPENDENT DEFINITIONS                    ***************/
/****************************************************************************/
/*
 * Macros to help debugging
 */
//#define LCD_SC14450_DEBUG
#undef PDEBUG             /* undef it, just in case */
#ifdef LCD_SC14450_DEBUG
#  ifdef __KERNEL__
     /* This one if debugging is on, and kernel space */
#    define PDEBUG(fmt, args...) printk( KERN_DEBUG "lcd_sc14450: " fmt, ## args)
#  else
     /* This one for user space */
#    define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif

#undef PDEBUGG
#define PDEBUGG(fmt, args...) /* nothing: it's a placeholder */

#define LCD_SC14450_MAJOR 251
#ifndef LCD_SC14450_MAJOR
#define LCD_SC14450_MAJOR 0   /* dynamic major by default */
#endif

#ifndef LCD_SC14450_NR_DEVS
#define LCD_SC14450_NR_DEVS 1    
#endif


#endif
