#
# Copyright (c) 2011, Dialog Semiconductor BV
#
# <george.giannaras@diasemi.com> and contributors.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 2 as published
# by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
#
#
# CONFIG_SC1445x_LCD_SUPPORT subsystem configuration
#

menu "SC1445x Lcd support"

comment "No SC1445x LCD support while support for ATA is enabled and the board is not VT_MS20_CNSL_BOARD"
	depends on (!VT_MS20_CNSL_BOARD && SC1445x_LEGERITY_890_SUPPORT )
	
config SC1445x_LCD_SUPPORT
	bool "SC1445x Lcd support"
	depends on (!SC1445x_LEGERITY_890_SUPPORT || VT_MS20_CNSL_BOARD)	

	---help---
	Adds support for lcd.
choice
	prompt "LCD type"
	depends on (SC1445x_LCD_SUPPORT	&& (!SC1445x_LEGERITY_890_SUPPORT || VT_MS20_CNSL_BOARD))		
#	help
#	  LCD support.
	config SC1445x_TEXT_LCD_SUPPORT	
		bool "Text lcd support"
		depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT)	
		---help---
		Adds support for text LCD controllers. 
	config SC1445x_DOTMATRIX_LCD_SUPPORT
		bool "Dot matrix lcd support"
		depends on (SC1445x_LCD_SUPPORT	&& (!SC1445x_LEGERITY_890_SUPPORT || VT_MS20_CNSL_BOARD))	
		---help---
		Adds support for dot matrix LCD controllers.
	config SC1445x_TFT_LCD_SUPPORT
		bool "Tft lcd support"
		depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && FB)	
		---help---
		Adds support for tft LCD controllers.		
endchoice
choice
	prompt "Text LCD controller"
	depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && SC1445x_TEXT_LCD_SUPPORT)		
#	help
#	  LCD support.
	config SC1445x_ST7066U_LCD_SUPPORT	
		bool "Sitronix st7066u lcd controller support"
		depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && SC1445x_TEXT_LCD_SUPPORT && !VT_V1_BOARD)	
		---help---
		Adds drivers for Sitronix st7066u text lcd controller (used in LCDs: TOPWAY LMB162AFC (2x16), TOPWAY LMB202EEC (2x20)). 
	config SC1445x_ST7032_LCD_SUPPORT	
		bool "Sitronix st7032 lcd controller support"
		depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && SC1445x_TEXT_LCD_SUPPORT && VT_V1_BOARD)	
		---help---
		Adds drivers for Sitronix st7032 text lcd controller.		
endchoice
choice
	prompt "Dotmatrix LCD controller"
	depends on (SC1445x_LCD_SUPPORT	&& (!SC1445x_LEGERITY_890_SUPPORT ||VT_MS20_CNSL_BOARD) && SC1445x_DOTMATRIX_LCD_SUPPORT)		
#	help
#	  LCD support.
	config SC1445x_ST7565P_LCD_SUPPORT	
		bool "Sitronix st7565p lcd controller support (mode:8080)"
		depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && SC1445x_DOTMATRIX_LCD_SUPPORT && !L_BOARDS && !F_G2_ST7567_2PORT_BOARD)	
		---help---
		Adds drivers for Sitronix st7565p dot matrix lcd controller using databus (mode 8080) ( used in LCD: TOPWAY LM6038b (128x64) ).
	config SC1445x_ST7565P_LCD_SERIAL_SUPPORT	
		bool "Sitronix st7565p lcd controller support (mode: serial)"
		depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && SC1445x_DOTMATRIX_LCD_SUPPORT && !L_BOARDS && !F_G2_ST7567_2PORT_BOARD)		
		---help---
		Adds drivers for Sitronix st7565p dot matrix lcd controller using spi (serial mode) ( used in LCD: TOPWAY LM6038b (128x64) ).		
	config SC1445x_ST7567_LCD_SERIAL_SUPPORT	
		bool "Sitronix st7567 lcd controller support (mode: serial)"
		depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && SC1445x_DOTMATRIX_LCD_SUPPORT && (F_G2_ST7567_2PORT_BOARD || BOARD_SMG_I))		
		---help---
		Adds drivers for Sitronix st7567 dot matrix lcd controller using spi (serial mode).			
	config SC1445x_ST7567_LCD_SERIAL_SUPPORT_OPTIMIZED	
		bool "Sitronix st7567 lcd contr. Optimized (mode: serial)"
		depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && SC1445x_DOTMATRIX_LCD_SUPPORT && (F_G2_ST7567_2PORT_BOARD || BOARD_SMG_I))		
		---help---
		Adds drivers for Sitronix st7567 dot matrix lcd controller using spi (serial mode).			
					
	config SC1445x_NT75451_LCD_SERIAL_SUPPORT	
		bool "Novatek NT75451 lcd controller support (mode: serial)"
		depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && SC1445x_DOTMATRIX_LCD_SUPPORT && L_V1_BOARD && !F_G2_ST7567_2PORT_BOARD)			
		---help---
		Adds drivers for Novatek nt75451 dot matrix lcd controller using spi (serial mode) ( used in LCD: ... ).
	config SC1445x_NT7538_LCD_SUPPORT	
		bool "Novatek NT7538 lcd controller support (mode: 8080)"
		depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && SC1445x_DOTMATRIX_LCD_SUPPORT && IN_V1_BOARD )			
		---help---
		Adds drivers for Novatek nt7538 dot matrix lcd controller using databus (serial mode).

	config SC1445x_ST7529_LCD_SUPPORT	
		bool "Sitronix ST7529 lcd controller support (mode: 8080)"
		depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && SC1445x_DOTMATRIX_LCD_SUPPORT && (L_V2_BOARD||L_V4_BOARD||L_V5_BOARD|| L_V2_BOARD_CNX ||L_V4_BOARD_CNX||L_V5_BOARD_CNX)&& !F_G2_ST7567_2PORT_BOARD)			
		---help---
		Adds drivers for Sitronix st7529v1 dot matrix lcd controller using databus (mode 8080) ( used in LCD: YB module:BTG-24056A-FBWA-J-T-A00).				
	config SC1445x_S6B33_LCD_SUPPORT	
		bool "Samsung S6B33 lcd controller support (mode: 8080)"
		depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && SC1445x_DOTMATRIX_LCD_SUPPORT && (L_V3_BOARD|| L_V3_BOARD_CNX))			
		---help---
		Adds drivers for Samsung S6B33 dot matrix lcd controller using databus (mode 8080) ( used in LCD: Elec&Eltek module:221-3175-1150).				
		
	config SC1445x_IST3020_LCD_SUPPORT
		bool "IST3020 lcd controller support"
		depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && !L_BOARDS && !F_G2_ST7567_2PORT_BOARD)	
		---help---
		Adds drivers for ist3020 dot matrix lcd controller ( used in LCD: NAN YA LMG77 (192x64) ).
#GZ
	config SC1445x_ST7586S_LCD_SUPPORT	
		bool "Sitronix ST7586s lcd controller support (mode: 8080)"
		depends on (SC1445x_LCD_SUPPORT	&& SC1445x_DOTMATRIX_LCD_SUPPORT )			
		---help---
		Adds drivers for Sitronix st7529v1 dot matrix lcd controller using databus (mode 8080). 
endchoice
choice
	prompt "Tft LCD controller"
	depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && FB && SC1445x_TFT_LCD_SUPPORT)		
#	help
#	  LCD support.
	config SC1445x_ILI9163_LCD_SUPPORT
		bool "Ilitek 9163 tft lcd controller support"
		depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && SC1445x_TFT_LCD_SUPPORT && SC14450)	
		select FB_CFB_FILLRECT
		select FB_CFB_COPYAREA
		select FB_CFB_IMAGEBLIT		
		---help---
		Adds drivers for Ilitek 9163ds tft lcd controller (used in LCD: TIANMA TM128160F4NFWGWC (128x160)). 
		This is a frame buffer device.
	config SC1445x_ILI9325_LCD_SUPPORT
		bool "Ilitek 9325 tft lcd controller support"
		depends on (SC1445x_LCD_SUPPORT	&& !SC1445x_LEGERITY_890_SUPPORT && SC1445x_TFT_LCD_SUPPORT && SC14452)	
		select FB_CFB_FILLRECT
		select FB_CFB_COPYAREA
		select FB_CFB_IMAGEBLIT		
		---help---
		Adds drivers for Ilitek 9325ds tft lcd controller (used in LCD: TIANMA TM128160F4NFWGWC (240x320)). 
		Is this a frame buffer device ??????
		
endchoice
	config SC1445x_FB_DOTMATRIX_LCD_SUPPORT
	        bool "Enable framebuffer support for dotmatrix lcd"
	        depends on FB && SC1445x_DOTMATRIX_LCD_SUPPORT && !SC1445x_LEGERITY_890_SUPPORT
        	select FB_CFB_FILLRECT
        	select FB_CFB_COPYAREA
	        select FB_CFB_IMAGEBLIT
	        ---help---
        	  This is a frame buffer device. 
endmenu


