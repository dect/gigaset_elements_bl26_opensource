/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <george.giannaras@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

// This is the header file for the sc1445x lcd driver of the ILITEK 9163ds lcd tft controller.
// 			Supported displays:
// LCD:  TIANMA TM128160F4NFWGWC (128x160 lcd tft display) ---- using lcd driver ILITEK 9163ds

/* 
	Driver interface description
	LCD		BOARD
	---------------------------
	RS		->        AD7
	DB[0..7]	->	DAB[0..7]
*/
#ifndef LCD_ILI9325_H 
#define LCD_ILI9325_H 
/***************       Definitions section             ***************/
/* Registers */
//Since RS -> AD7, data are accessed at 0x80 and instructions are accessed at 0x00
#define LCD_BASE 0x01300000
#define LCD_DR (LCD_BASE+0x80)
#define LCD_IR (LCD_BASE+0x00)

/* HW dependent definitions */
#define FRAMEBUFFER_VIRTUAL_MEMORY framebuffer_memory //LCD_DR //This address should point to a non-cachable memory space where the lcd memory is mapped.
#define FRAMEBUFFER_SIZE 153600//(320*240*2)//61440 //(128*160*3)//48063//(132*18*162/8)  
//#define FRAMEBUFFER_MAX_WR 0x80 //this is the max allowed copy to the screen_base (because in 0x80 it performes read)  
//Kernel MACROs
#define LCD_WRITE_INST(x)	{(*(volatile unsigned short *)(LCD_IR)=(x));/*;udelay(10);*/}
#define LCD_WRITE_DATA(x)	{(*(volatile unsigned short *)(LCD_DR)=(x));/*udelay(10);*/}
#define LCD_READ_DATA(x)	{((x)=*(volatile unsigned short *)(LCD_DR));/*udelay(10);*/}
#define LCD_DUMMY_READ_DATA LCD_READ_DATA //just for code readability
//New IOCTL - should be added in fb.h
// #define FBIOREFRESH_DISPLAY        0x4619

//Transfer image from the host processor to the peripheral
#define LCD_write_memory_start					{LCD_WRITE_INST(0x22);} //variable data writes should follow this command

//Set the column extent													||	(Type1 = Y, Type2 = Y, Type3 = N)
#define LCD_set_column_address(X, Y)		{LCD_WRITE_INST(0x50);LCD_WRITE_DATA(X);LCD_WRITE_INST(0x51);LCD_WRITE_DATA(Y);}
//Set the page extent													||	(Type1 = Y, Type2 = Y, Type3 = N)
#define LCD_set_page_address(X, Y)		{LCD_WRITE_INST(0x52);LCD_WRITE_DATA(X);LCD_WRITE_INST(0x53);LCD_WRITE_DATA(Y);}

#define LCD_wite_ctrl(X, Y)		{LCD_WRITE_INST(X);LCD_WRITE_DATA(Y);;}


/*Command Bit fields*/
//RGB Interface
#define LCD_IFPF0 0x03 //12-bit/pixel
#define LCD_IFPF1 0x05 //16-bit/pixel
#define LCD_IFPF2 0x06 //18-bit/pixel
//Address mode


//Display parameters
#define LCD_TFS 0 // TOP_FIXED_AREA (in No. of lines from Top of the Frame Memory and Display).
#define LCD_VSA 160 //VERTICAL_SCROLLING_AREA(in No. of lines of the Frame Memory [not the display] from the Vertical Scrolling Start Address).
#define LCD_BFA 0 //BOTTOM_FIXED_AREA(in No. of lines from Bottom of the Frame Memory and Display)

//Kernel MACROs
//#define LCD_WRITE_INST(x)	(*(volatile unsigned char *)(LCD_IR)=(x))
#define BUSY_FLAG			(1<<7)
// #define lcd_out_ctl(X)   {LCD_WRITE_INST(X);}
#define lcd_out_dat(X)   {LCD_WRITE_DATA(X);}
// #define lcd_out_ctl(X)   {spin_lock_irq(&lcd_dev.lock);LCD_WRITE_INST(X);spin_unlock_irq(&lcd_dev.lock);}
// #define lcd_out_dat(X)   {spin_lock_irq(&lcd_dev.lock);LCD_WRITE_DATA(X);spin_unlock_irq(&lcd_dev.lock);}
/* LCD screen and bitmap image array consants */
#define X_BYTES				192
#define Y_BYTES	      		8


#define COL_MIN				32//0x04
#define COL_MAX				223//0x83

#define SCRN_TOP			0
#define SCRN_BOTTOM			63
#define SCRN_LEFT			0//0x04

#define SCRN_RIGHT			191//0x83


#define PAGE_MIN			0
#define PAGE_MAX			(SCRN_BOTTOM/8)

//Processor specific commands
#define LCD_BACKLIGHT_LEVEL(X)  (TIMER2_DUTY_REG=X|X<<8)

/***      Data types definitions       ***/
struct lcd_sc14450_conf_struct{	
	// unsigned short flags;	
	unsigned char refresh_tmout;	
} ;


//IOCTL Commands
typedef enum IOCTL_Cmd_enum {
	LCD_init,//Initializes hw and driver internal state
	LCD_out_ctl,//Issues a LCD cmd directly from the user space
	LCD_clear_ram,//CLears LCD ram
	LCD_clear_rect,//Clears a rectangle defined by left/top /right/bottom coordinates in the LCD ram
	LCD_invert_rect,//Inverts a rectangle defined by left/top /right/bottom coordinates in the LCD ram
	LCD_draw_border,//Draws a border defined by left/top /right/bottom coordinates in the LCD ram
	LCD_clear_border,//Clear a border defined by left/top /right/bottom coordinates in the LCD ram
	LCD_text,//Write a string of characters using a specified font, starting at location defined by left, top coordinates in the LCD ram
	LCD_update,//Updates the LCD display with the contents of the LCD ram
	LCD_standby_enter,//Enter standby mode
	LCD_standby_exit,//Exit standby mode
	LCD_sleepmode_enter,//Enter sleep mode
	LCD_sleepmode_exit,//Exit sleep mode
	LCD_backlight_level,//Set backlight brightness level
	LCD_contrast_level,//Set backlight contrast level
	LCD_refresh_tmout,//Set LCD refresh timeout
	LCD_IOCTL_MAX//this is a delimiter
} IOCTL_Cmd;
 

/****************************************************************************/
/**************           HW INDEPENDENT DEFINITIONS                    ***************/
/****************************************************************************/
/*
 * Macros to help debugging
 */
#define LCD_SC14450_DEBUG
//#undef PDEBUG             /* undef it, just in case */
#ifdef LCD_SC14450_DEBUG
#  ifdef __KERNEL__
     /* This one if debugging is on, and kernel space */
// #    define PDEBUG(fmt, args...) printk( KERN_DEBUG "lcd_sc14450: " fmt, ## args)
#    define PDEBUG(fmt, args...) printk( KERN_WARNING "lcd_sc14450: " fmt, ## args)
#  else
     /* This one for user space */
#    define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif

#undef PDEBUGG
#define PDEBUGG(fmt, args...) /* nothing: it's a placeholder */

#define LCD_SC14450_MAJOR 251
#ifndef LCD_SC14450_MAJOR
#define LCD_SC14450_MAJOR 0   /* dynamic major by default */
#endif

#ifndef LCD_SC14450_NR_DEVS
#define LCD_SC14450_NR_DEVS 1    
#endif


#endif
