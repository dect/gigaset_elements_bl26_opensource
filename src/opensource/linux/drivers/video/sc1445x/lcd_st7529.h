/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <george.giannaras@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

// This is the header file for the experimental lcd driver for SC1445x developoment board
// 			Supported displays:
// LCD:  (dotmatrix display) ---- using lcd driver ST7529

/* 
	Driver interface description
	LCD		BOARD
	---------------------------
	RS		->        AD7
	DB[0..7]	->	DAB[0..7]
*/
#ifndef LCD_ST7529_H 
#define LCD_ST7529_H 
/***************       Definitions section             ***************/
/* HW dependent definitions */

#if 1
#ifdef CONFIG_RAM_SIZE_32MB

#define CONFIG_32M_SDRAM_PATCH


#ifdef CONFIG_32M_SDRAM_PATCH

#define OVER_16M_SDRAM_ACCESS_PATCH_FLAG register unsigned long m_32m_patch_flag
#define PROTECT_OVER_16M_SDRAM_ACEESS	{ local_irq_save(m_32m_patch_flag); EBI_ACS4_CTRL_REG = 9;}
#define RESTORE_OVER_16M_SDRAM_ACEESS	{ EBI_ACS4_CTRL_REG = 0xa; local_irq_restore(m_32m_patch_flag); }

#else

#define OVER_16M_SDRAM_ACCESS_PATCH_FLAG 
#define PROTECT_OVER_16M_SDRAM_ACEESS	
#define RESTORE_OVER_16M_SDRAM_ACEESS	


#endif

#else

#define OVER_16M_SDRAM_ACCESS_PATCH_FLAG 
#define PROTECT_OVER_16M_SDRAM_ACEESS	
#define RESTORE_OVER_16M_SDRAM_ACEESS	


#endif

#endif

/* Registers */
//Since RS -> AD7, data are accessed at 0x80 and instructions are accessed at 0x00
#define LCD_BASE 0x01080000
#define LCD_DR (LCD_BASE+0x80)
#define LCD_IR (LCD_BASE+0x00)
#define LCD_LATCH 0x01090000

#if !(defined(CONFIG_L_V2_BOARD) || defined(CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD)\
		||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX))
#define LCD_SET_BACKLIGHT_ON  {PROTECT_OVER_16M_SDRAM_ACEESS;(*(volatile unsigned short *)(LCD_LATCH)=(0x6000));RESTORE_OVER_16M_SDRAM_ACEESS}//Set backlight on, rst high
#define LCD_SET_BACKLIGHT_OFF {PROTECT_OVER_16M_SDRAM_ACEESS;(*(volatile unsigned short *)(LCD_LATCH)=(0x4000));RESTORE_OVER_16M_SDRAM_ACEESS}//Set backlight off, rst high
#else
extern unsigned int lcd_backlight;
#define LCD_SET_BACKLIGHT_ON	{lcd_backlight=1;}
#define LCD_SET_BACKLIGHT_OFF	{lcd_backlight=0;}
#endif


/* commands of the ST7529 LCD controller */
//External Instruction Selection
//=================================
//CMD 1
#define LCD_EXT_IN			0x30	/* external instruction disable (parameter bytes: none)*/
//CMD 2
#define LCD_EXT_OUT			0x31	/* external instruction enable (parameter bytes: none)*/

//EXT == 0 Commands
//=================================
//CMD 1
#define LCD_DISP_ON			0xAF	/* turn LCD display ON (parameter bytes: none)*/
//CMD 2
#define LCD_DISP_OFF 		0xAE	/* turn LCD display OFF (parameter bytes: none)*/
//CMD 3
#define LCD_DISP_NOR		0xA6	/* set normal pixel display (parameter bytes: none) */
//CMD 4
#define LCD_DISP_REV		0xA7	/* set reverse pixel display (parameter bytes: none)*/
//CMD 5
#define LCD_COM_SCN			0xBB	/* set common scan direction (parameter bytes: 1)*/
//CMD 6
#define LCD_DIS_CTRL		0xCA	/* set display control (parameter bytes: 3)*/
//CMD 7
#define LCD_SLEEP_IN		0x95	/* enter sleep mode (parameter bytes: none)*/
//CMD 8
#define LCD_SLEEP_OUT		0x94	/* exit sleep mode (parameter bytes: none)*/
//CMD 9
#define LCD_LA_SET			0x75	/* set line address (parameter bytes: 2)*/
//CMD 10
#define LCD_CA_SET			0x15	/* set column address (parameter bytes: 2)*/
//CMD 11
#define LCD_DAT_SCAN_DIR	0xBC	/* set data scan direction (parameter bytes: 3)*/
//CMD 12
#define LCD_RAM_WR			0x5C	/* write data to memory (parameter bytes: number of data written)*/
//CMD 13
#define LCD_RAM_RD			0x5D	/* read data from memory (parameter bytes: number of data read)*/
//CMD 14
#define LCD_PARTIAL_IN		0xA8	/* specify partial display area (parameter bytes: 2)*/
//CMD 15
#define LCD_PARTIAL_OUT		0xA9	/* exit partial display mode (parameter bytes: none)*/
//CMD 16
#define LCD_RMW_IN			0xE0	/* read modify write in (parameter bytes: none)*/
//CMD 17
#define LCD_RMW_OUT			0xEE	/* exit read modify write in mode (parameter bytes: none)*/
//CMD 18
#define LCD_ASC_SET			0xAA	/* set area scroll (parameter bytes: 4)*/
//CMD 19
#define LCD_SCS_SET			0xAB	/* set scroll start address (parameter bytes: 1)*/
//CMD 20
#define LCD_OSC_ON			0xD1	/* turn internall oscillation on (parameter bytes: none)*/
//CMD 21
#define LCD_OSC_OFF			0xD2	/* turn internall oscillation off (parameter bytes: none)*/
//CMD 22
#define LCD_PWR_CTRL		0x20	/* turn on/off booster and voltage regulators and set ref.voltage (parameter bytes: none)*/
//CMD 23
#define LCD_VOL_CTRL		0x81	/* turn on/off booster and voltage regulators and set ref.voltage (parameter bytes: 2)*/
//CMD 24
#define LCD_VOL_UP			0xD6	/* increment control offset value of voltage regulator (parameter bytes: none)*/
//CMD 25
#define LCD_VOL_DOWN		0xD7	/* deccrement control offset value of voltage regulator (parameter bytes: none)*/
//CMD 26
#define LCD_RESERVED		0x82	/* reserved command (parameter bytes: none)*/
//CMD 27
#define LCD_EPS_RRD_1		0x7C	/* read register 1 (parameter bytes: none)*/
//CMD 28
#define LCD_EPS_RRD_2		0x7D	/* read register 2 (parameter bytes: none)*/
//CMD 29
#define LCD_NOP				0x25	/* nop command (parameter bytes: none)*/
//CMD 30
#define LCD_STAT_READ		0x00	/* status read command (parameter bytes: none)*/
//CMD 31
#define LCD_EP_INT			0x07	/* status read command (parameter bytes: 1)*/

//EXT == 1 Commands
//=================================
//CMD 1
#define LCD_GRAY1_SET		0x20	/* set the gray level of 16 odd frames (parameter bytes: 16) */
//CMD 2
#define LCD_GRAY2_SET		0x21	/* set the gray level of 16 even frames (parameter bytes: 16) */
//CMD 3
#define LCD_ANA_SET			0x32	/* set the analog circuit parameters (parameter bytes: 3) */
//CMD 4
#define LCD_SW_INIT			0x34	/* software initial (parameter bytes: none) */
//CMD 5
#define LCD_EPC_IN			0xCD	/* eeprom control in (enable read/write) (parameter bytes: 1) */
//CMD 6
#define LCD_EPC_OUT			0xCC	/* eeprom control out (parameter bytes: none) */
//CMD 7
#define LCD_EPM_WR			0xFC	/* eeprom data write (parameter bytes: none) */
//CMD 8
#define LCD_EPM_RD			0xFD	/* eeprom data read (parameter bytes: none) */

//#define SLOWBUSTIMING
#define FASTBUSTIMING
//#define VFASTBUSTIMING

//Kernel MACROs
#ifndef CONFIG_32M_SDRAM_PATCH
#define LCD_WRITE_DATA(x)	{(*(volatile unsigned char *)(LCD_DR)=(x));udelay(1);} /* write data to the display RAM (one byte)*/
#define LCD_WRITE_INST(x)	{(*(volatile unsigned char *)(LCD_IR)=(x));udelay(1);}
#define LCD_READ_DATA(x)	{(x=ReverseLookupTable[*(volatile unsigned char *)(LCD_DR)]);udelay(1);} /* read data from the display RAM (one byte)*/

#define lcd_out_ctl(X)   {PROTECT_OVER_16M_SDRAM_ACEESS;LCD_WRITE_INST(ReverseLookupTable[(X)]);RESTORE_OVER_16M_SDRAM_ACEESS;}
#define lcd_out_dat(X)   {PROTECT_OVER_16M_SDRAM_ACEESS;LCD_WRITE_DATA(ReverseLookupTable[(X)]);RESTORE_OVER_16M_SDRAM_ACEESS;}
#define lcd_in_dat(X)   {PROTECT_OVER_16M_SDRAM_ACEESS;LCD_READ_DATA((X));RESTORE_OVER_16M_SDRAM_ACEESS;}
#else

#define LCD_WRITE_DATA(x)	{(*(volatile unsigned char *)(LCD_DR)=(x));} /* write data to the display RAM (one byte)*/
#define LCD_WRITE_INST(x)	{(*(volatile unsigned char *)(LCD_IR)=(x));}
#define LCD_READ_DATA(x)	{(x=ReverseLookupTable[*(volatile unsigned char *)(LCD_DR)]);} /* read data from the display RAM (one byte)*/

//#define lcd_out_ctl(X)   {PROTECT_OVER_16M_SDRAM_ACEESS;LCD_WRITE_INST(ReverseLookupTable[(X)]);RESTORE_OVER_16M_SDRAM_ACEESS;udelay(1);}
//#define lcd_out_dat(X)   {PROTECT_OVER_16M_SDRAM_ACEESS;LCD_WRITE_DATA(ReverseLookupTable[(X)]);RESTORE_OVER_16M_SDRAM_ACEESS;udelay(1);}
//#define lcd_in_dat(X)   {PROTECT_OVER_16M_SDRAM_ACEESS;LCD_READ_DATA((X));RESTORE_OVER_16M_SDRAM_ACEESS;udelay(1);}

extern unsigned int lcd_backlight;
#define LCD_SET_BACKLIGHT_ON	{lcd_backlight=1;}
#define LCD_SET_BACKLIGHT_OFF	{lcd_backlight=0;}
#if (defined(FASTBUSTIMING)||defined (VFASTBUSTIMING))

	#define lcd_out_ctl2(X) \
	({\
			register short __r4 __asm__ ("r4") = (short)(X) ; \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1080000:l,(r1,r0) \n\t"\
    			"storw	r4,0x0:(r1,r0) \n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
			    :"=r" (__r4) \
			    : "0" (__r4) \
			    : "r0","r1","r2","r3"); \
			 \
		})

#define lcd_out_ctl(X) lcd_out_ctl2(ReverseLookupTable[(X)])



#define lcd_out_dat2(X) \
		({\
			register short __r4 __asm__ ("r4") = (short)(X) ; \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1080080:l,(r1,r0) \n\t"\
    			"storw	r4,0x0:(r1,r0) \n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
			    :"=r" (__r4) \
			    : "0" (__r4) \
			    : "r0","r1","r2","r3"); \
			 \
		})

#define lcd_out_dat(X) {lcd_out_dat2(ReverseLookupTable[(X)]);}


#define lcd_in_dat2 \
	({\
			register short __r4 __asm__ ("r4") ; \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1080080:l,(r1,r0) \n\t"\
    			"loadw	0x0:s(r1,r0),r4 \n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
			    :"=r" (__r4) \
			    : "0" (__r4) \
			    : "r0","r1","r2","r3"); \
			    __r4;\
		})

#define lcd_in_dat(X) {X=ReverseLookupTable[lcd_in_dat2];}


#define lcd_out3b3_dat2(X,Y,Z) \
		({\
			volatile register short __r4 __asm__ ("r4") = (short)(X); \
			volatile register short __r5 __asm__ ("r5") = (short)(Y); \
			volatile register short __r6 __asm__ ("r6") = (short)(Z); \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1080080:l,(r1,r0) \n\t"\
    			"storw	r4,0x0:(r1,r0) \n\t"\
    			"storw	$0x9,0xff0064 \n\t"\
    			"storw	r5,0x0:(r1,r0) \n\t"\
    			"storw	$0x9,0xff0064 \n\t"\
    			"storw	r6,0x0:(r1,r0) \n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
			    :"=r" (__r4),"=r" (__r5),"=r" (__r6) \
			    : "0" (__r4),"1" (__r5),"2" (__r6) \
			    : "r0","r1","r2","r3"); \
			 \
		})

#define lcd_out_3b3_dat(X,Y,Z) {lcd_out3b3_dat2(    ReverseLookupTable[X],\
												    ReverseLookupTable[Y],\
													ReverseLookupTable[Z] );}







#define lcd_out2b3_dat2(X,Y) \
		({\
			volatile register short __r4 __asm__ ("r4") = (short)(X); \
			volatile register short __r5 __asm__ ("r5") = (short)(Y); \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1080080:l,(r1,r0) \n\t"\
    			"storw	r4,0x0:(r1,r0) \n\t"\
    			"storw	$0x9,0xff0064 \n\t"\
    			"storw	r5,0x0:(r1,r0) \n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
			    :"=r" (__r4),"=r" (__r5)\
			    : "0" (__r4),"1" (__r5) \
			    : "r0","r1","r2","r3"); \
			 \
		})

#define lcd_out_2b3_dat(X,Y,Z) { lcd_out2b3_dat2(    ReverseLookupTable[((X&0xF8)|((Y>>5)&0x7))],\
											    ReverseLookupTable[(((Y<<3)&0xE0)|((Z>>3)&0x1F))] );}
#else
#define lcd_out_ctl2(X) \
	({\
			register short __r4 __asm__ ("r4") = (short)(X) ; \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"nop \n\t"\
    			"movd $0x02:m,(r1,r0)\n\t"\
    			"1: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 1b\n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"nop \n\t"\
    			"nop \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1080000:l,(r1,r0) \n\t"\
    			"storw	r4,0x0:(r1,r0) \n\t"\
    			"nop \n\t"\
    			"movd $0x02:m,(r1,r0)\n\t"\
    			"2: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 2b\n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
    			"nop \n\t"\
    			"nop \n\t"\
			    :"=r" (__r4) \
			    : "0" (__r4) \
			    : "r0","r1","r2","r3"); \
			 \
		})

#define lcd_out_ctl(X) lcd_out_ctl2(ReverseLookupTable[(X)])



#define lcd_out_dat2(X) \
		({\
			register short __r4 __asm__ ("r4") = (short)(X) ; \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"nop \n\t"\
    			"movd $0x02:m,(r1,r0)\n\t"\
    			"1: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 1b\n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1080080:l,(r1,r0) \n\t"\
    			"storw	r4,0x0:(r1,r0) \n\t"\
    			"nop \n\t"\
    			"movd $0x02:m,(r1,r0)\n\t"\
    			"2: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 2b\n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
    			"nop \n\t"\
			    :"=r" (__r4) \
			    : "0" (__r4) \
			    : "r0","r1","r2","r3"); \
			 \
		})

#define lcd_out_dat(X) {lcd_out_dat2(ReverseLookupTable[(X)]);}


#define lcd_in_dat2 \
	({\
			register short __r4 __asm__ ("r4") ; \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"nop \n\t"\
    			"movd $0x02:m,(r1,r0)\n\t"\
    			"1: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 1b\n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1080080:l,(r1,r0) \n\t"\
    			"loadw	0x0:s(r1,r0),r4 \n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
    			"nop \n\t"\
    			"nop \n\t"\
    			"movd $0x02:m,(r1,r0)\n\t"\
    			"2: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 2b\n\t"\
			    :"=r" (__r4) \
			    : "0" (__r4) \
			    : "r0","r1","r2","r3"); \
			    __r4;\
		})

#define lcd_in_dat(X) {X=ReverseLookupTable[lcd_in_dat2];}



#define lcd_out3b3_dat2(X,Y,Z) \
		({\
			volatile register short __r4 __asm__ ("r4") = (short)(X); \
			volatile register short __r5 __asm__ ("r5") = (short)(Y); \
			volatile register short __r6 __asm__ ("r6") = (short)(Z); \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"nop \n\t"\
    			"movd $0x02:m,(r1,r0)\n\t"\
    			"1: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 1b\n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1080080:l,(r1,r0) \n\t"\
    			"storw	r4,0x0:(r1,r0) \n\t"\
    			"storw	$0x9,0xff0064 \n\t"\
    			"storw	r5,0x0:(r1,r0) \n\t"\
    			"storw	$0x9,0xff0064 \n\t"\
    			"storw	r6,0x0:(r1,r0) \n\t"\
    			"movd $0x02:m,(r1,r0)\n\t"\
    			"2: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 2b\n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
    			"nop \n\t"\
			    :"=r" (__r4),"=r" (__r5),"=r" (__r6) \
			    : "0" (__r4),"1" (__r5),"2" (__r6) \
			    : "r0","r1","r2","r3"); \
			 \
		})

#define lcd_out_3b3_dat(X,Y,Z) {lcd_out3b3_dat2(    ReverseLookupTable[X],\
												    ReverseLookupTable[Y],\
													ReverseLookupTable[Z] );}

#define lcd_out2b3_dat2(X,Y) \
		({\
			volatile register short __r4 __asm__ ("r4") = (short)(X); \
			volatile register short __r5 __asm__ ("r5") = (short)(Y); \
			asm volatile(\
    			"sprd	psr,(r3,r2) \n\t"\
    			"di \n\t"\
    			"nop \n\t"\
    			"movd $0x02:m,(r1,r0)\n\t"\
    			"1: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 1b\n\t"\
    			"movd	$0x9:m,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"movd	$0x1080080:l,(r1,r0) \n\t"\
    			"storw	r4,0x0:(r1,r0) \n\t"\
    			"storw	$0x9,0xff0064 \n\t"\
    			"storw	r5,0x0:(r1,r0) \n\t"\
    			"movd $0x02:m,(r1,r0)\n\t"\
    			"2: addd	$-1:s,(r1,r0) \n\t"\
    			"bcs 2b\n\t"\
    			"movd	$0xa,(r1,r0) \n\t"\
    			"stord	(r1,r0),0xff0064 \n\t"\
    			"lprd	(r3,r2), psr \n\t"\
    			"nop \n\t"\
			    :"=r" (__r4),"=r" (__r5)\
			    : "0" (__r4),"1" (__r5) \
			    : "r0","r1","r2","r3"); \
			 \
		})

#define lcd_out_2b3_dat(X,Y,Z) { lcd_out2b3_dat2(    ReverseLookupTable[((X&0xF8)|((Y>>5)&0x7))],\
											    ReverseLookupTable[(((Y<<3)&0xE0)|((Z>>3)&0x1F))] );}

//Critical !! the "storw	$0x9,0xff0064 \n\t"\ is required for proper timing of the
//CS
#endif
#endif
	//#define lcd_out_ctl(X)   {LCD_WRITE_INST(X);}
	//#define lcd_out_dat(X)   {LCD_WRITE_DATA(X);}
/* Display settings */
#if !(defined(CONFIG_L_V4_BOARD) || defined(CONFIG_L_V4_BOARD_CNX))
	#define INVERSE_DISPLAY //When defined the display orientation is inversed
#endif
//#define B3P3_MODE 	//When defined 3bytes-2pixels mode is used. If not defined 2B3P_MODE is used

/* LCD screen and bitmap image array consants */
#define SCRN_TOP			0x0

#if (defined (CONFIG_L_V2_BOARD) ||defined(CONFIG_L_V2_BOARD_CNX))
#define SCRN_BOTTOM			55 //41
#elif defined(CONFIG_L_V5_BOARD)||defined(CONFIG_L_V5_BOARD_CNX)
#define SCRN_BOTTOM			41 //41
#elif defined(CONFIG_L_V4_BOARD)||defined(CONFIG_L_V4_BOARD_CNX)
#define SCRN_BOTTOM			143
#endif
#define SCRN_LEFT			0x0//0//0x04
#define SCRN_RIGHT			239//0x83
#define DDRAM_SIZE			((SCRN_BOTTOM+1)*(SCRN_RIGHT+1)*5/8) //8400 bytes

#define X_BYTES				(SCRN_RIGHT+1)
//#define Y_BYTES	      		((SCRN_BOTTOM+1)/8)//*/7
#define Y_BYTES	      		(SCRN_BOTTOM+1)

#define COL_MIN				0x00//0x04//0x00
//#define COL_MAX				0x83 


//#define PAGE_MIN			0
//#define PAGE_MAX			(SCRN_BOTTOM/8)

//Processor specific commands
//#define LCD_BACKLIGHT_LEVEL(X)  (TIMER2_DUTY_REG=X|X<<8)

/***      Data types definitions       ***/
struct lcd_sc14450_conf_struct{	
	// unsigned short flags;	
	unsigned char refresh_tmout;	
} ;
//lcd_sc14450_conf_struct Flags 
// #ifdef AUTO_REFRESH	
	// #define	REFRESH_TIMER_ON  0x0001	
// #endif	

//IOCTL Commands
typedef enum IOCTL_Cmd_enum {
	LCD_init,
	LCD_out_ctl,
	LCD_out_data,
	LCD_clear_ram,
	LCD_clear_rect,
	LCD_invert_rect,
	LCD_draw_border,
	LCD_clear_border,
	LCD_text,
	LCD_label,
	LCD_glyph,
	LCD_graphic,
	LCD_animate_bitmap,
	LCD_update,
	LCD_contrast_up,
	LCD_contrast_down,
	LCD_sleepmode_enter,
	LCD_sleepmode_exit,
	LCD_backlight_level,
	LCD_contrast_level,
	LCD_refresh_tmout,
	LCD_show_cursor,
 	LCD_fonts_width_get,
	LCD_IOCTL_MAX//this is a delimiter
} IOCTL_Cmd;
 

/****************************************************************************/
/**************           HW INDEPENDENT DEFINITIONS                    ***************/
/****************************************************************************/
/*
 * Macros to help debugging
 */
//#define LCD_SC14450_DEBUG
#undef PDEBUG             /* undef it, just in case */
#ifdef LCD_SC14450_DEBUG
#  ifdef __KERNEL__
     /* This one if debugging is on, and kernel space */
#    define PDEBUG(fmt, args...) printk( KERN_DEBUG "lcd_sc14450: " fmt, ## args)
#  else
     /* This one for user space */
#    define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif

#undef PDEBUGG
#define PDEBUGG(fmt, args...) /* nothing: it's a placeholder */

#define LCD_SC14450_MAJOR 251
#ifndef LCD_SC14450_MAJOR
#define LCD_SC14450_MAJOR 0   /* dynamic major by default */
#endif

#ifndef LCD_SC14450_NR_DEVS
#define LCD_SC14450_NR_DEVS 1    
#endif


#endif
