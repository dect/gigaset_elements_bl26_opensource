#ifndef _FONTS_H
#define _FONTS_H

/* select desired fonts. (Simply comment out those not needed) */
//#define EN_FIVE_DOT
//#define EN_SIX_DOT
#define EN_SEVEN_DOT
#define EN_EIGHT_DOT
#define EN_NINE_DOT
#define EN_TEN_DOT
#define EN_FIFTEEN_DOT
//#define EN_EIGHTEEN_DOT
#define SITEL_LOGO

/* define number labels for the font selections */
typedef enum
{
#ifdef EN_FIVE_DOT
	FONT_FIVE_DOT,
#endif

#ifdef EN_SIX_DOT
	FONT_SIX_DOT,
#endif

#ifdef EN_SEVEN_DOT
	FONT_SEVEN_DOT,
#endif

#ifdef EN_EIGHT_DOT
	FONT_EIGHT_DOT,
#endif

#ifdef CONFIG_IN_V1_BOARD
	FONT_SEPCIAL_KS5601_14_DOT,
	FONT_HANGUL_KS5601_14_DOT,
#endif // CONFIG_IN_V1_BOARD


#ifdef EN_NINE_DOT
	FONT_NINE_DOT,
#endif

#ifdef EN_TEN_DOT
	FONT_TEN_DOT,
#endif

#ifdef EN_FIFTEEN_DOT
	FONT_FIFTEEN_DOT,
#endif

#ifdef EN_EIGHTEEN_DOT
	FONT_EIGHTEEN_DOT,
#endif
#ifdef CONFIG_L_BOARDS
	FONT_CURSOR_SHORT_1,
	LG_FONT_SIX_DOT,
#endif
	FONT_COUNT
} FONT_BASE;

struct FONT_DEF 
{
   unsigned char store_width;            /* glyph storage width in bytes */
   unsigned char glyph_height;  		 /* glyph height for storage */
   unsigned char  *glyph_table;      /* font table start address in memory */
   unsigned char fixed_width;            /* fixed width of glyphs. If zero */
                                         /* then use the width table. */
   unsigned char  *width_table; 	 /* variable width table start adress */
   unsigned char glyph_beg;			 	 /* start ascii offset in table */
   unsigned char glyph_end;				 /* end ascii offset in table */
   unsigned char glyph_def;				 /* code for undefined glyph code */
};

/* font definition tables for the three fonts */
extern struct FONT_DEF  fonts[FONT_COUNT];

/* glyph bitmap and width tables for the fonts */ 
#ifdef EN_FIVE_DOT
  extern unsigned char  five_dot_glyph_table[];
  extern unsigned char  five_dot_width_table[];
#endif

#ifdef EN_SIX_DOT
  extern unsigned char  six_dot_glyph_table[];
  extern unsigned char  six_dot_width_table[];
#endif

#ifdef EN_SEVEN_DOT
  extern unsigned char  seven_dot_glyph_table[];
  extern unsigned char  seven_dot_width_table[];
#endif

#ifdef EN_EIGHT_DOT
  extern unsigned char  eight_dot_glyph_table[];
#endif

#ifdef CONFIG_IN_V1_BOARD
extern unsigned char  special_ks5601_table[];
extern unsigned char  hangul_ks5601_table[];
#endif // CONFIG_IN_V1_BOARD

#ifdef EN_NINE_DOT
  extern unsigned char  nine_dot_glyph_table[];
#endif

#ifdef EN_TEN_DOT
  extern unsigned char  ten_dot_glyph_table[];
#endif

#ifdef EN_FIFTEEN_DOT
  extern unsigned char  fifteen_dot_glyph_table[];
  extern unsigned char  fifteen_dot_width_table[];
#endif

#ifdef EN_EIGHTEEN_DOT
  extern unsigned char  eighteen_dot_glyph_table[];
  extern unsigned char  eighteen_dot_width_table[];
#endif

#ifdef CONFIG_L_BOARDS  
  extern unsigned char  cusror_dummy_dot_glyph_table[];
  extern unsigned char  lg_six_dot_glyph_table[];
#endif  
#ifdef SITEL_LOGO
extern unsigned char  sitel_logo_glyph_table[];
#endif

#ifdef CONFIG_IN_BOARDS
extern unsigned char  company_logo_glyph_table[];
#endif	/* CONFIG_IN_BOARDS */

#endif
