/*
 * linux/drivers/video/skeletonfb.c -- Skeleton for a frame buffer device
 *
 *  Modified to new api Jan 2001 by James Simmons (jsimmons@transvirtual.com)
 *
 *  Created 28 Dec 1997 by Geert Uytterhoeven
 *
 *
 *  I have started rewriting this driver as a example of the upcoming new API
 *  The primary goal is to remove the console code from fbdev and place it
 *  into fbcon.c. This reduces the code and makes writing a new fbdev driver
 *  easy since the author doesn't need to worry about console internals. It
 *  also allows the ability to run fbdev without a console/tty system on top 
 *  of it. 
 *
 *  First the roles of struct fb_info and struct display have changed. Struct
 *  display will go away. The way the the new framebuffer console code will
 *  work is that it will act to translate data about the tty/console in 
 *  struct vc_data to data in a device independent way in struct fb_info. Then
 *  various functions in struct fb_ops will be called to store the device 
 *  dependent state in the par field in struct fb_info and to change the 
 *  hardware to that state. This allows a very clean separation of the fbdev
 *  layer from the console layer. It also allows one to use fbdev on its own
 *  which is a bounus for embedded devices. The reason this approach works is  
 *  for each framebuffer device when used as a tty/console device is allocated
 *  a set of virtual terminals to it. Only one virtual terminal can be active 
 *  per framebuffer device. We already have all the data we need in struct 
 *  vc_data so why store a bunch of colormaps and other fbdev specific data
 *  per virtual terminal. 
 *
 *  As you can see doing this makes the con parameter pretty much useless
 *  for struct fb_ops functions, as it should be. Also having struct  
 *  fb_var_screeninfo and other data in fb_info pretty much eliminates the 
 *  need for get_fix and get_var. Once all drivers use the fix, var, and cmap
 *  fbcon can be written around these fields. This will also eliminate the
 *  need to regenerate struct fb_var_screeninfo, struct fb_fix_screeninfo
 *  struct fb_cmap every time get_var, get_fix, get_cmap functions are called
 *  as many drivers do now. 
 *
 *  This file is subject to the terms and conditions of the GNU General Public
 *  License. See the file COPYING in the main directory of this archive for
 *  more details.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/fb.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#ifdef CONFIG_SC1445x_IST3020_LCD_SUPPORT
	#include "lcd_ist3020.h"
#else //	CONFIG_SC1445x_ST7565P_LCD_SUPPORT
	#include "lcd_st7565p.h"
#endif	


/*HW specific definitions*/
extern unsigned char l_display_array[Y_BYTES][X_BYTES];//defined in dotmatrix_lcd driver
static const u32 pseudo_palette[16] = {
		0x000000,
		0xaa0000,
		0x00aa00,
		0xaa5500,
		0x0000aa,
		0xaa00aa,
		0x00aaaa,
		0xaaaaaa,
		0x555555,
		0xff5555,
		0x55ff55,
		0xffff55,
		0x5555ff,
		0xff55ff,
		0x55ffff,
		0xffffff
};
#define FRAMEBUFFER_VIRTUAL_MEMORY l_display_array

    /*
     *  This is just simple sample code.
     *
     *  No warranty that it actually compiles.
     *  Even less warranty that it actually works :-)
     */

/*
 *  If your driver supports multiple boards, you should make the  
 *  below data types arrays, or allocate them dynamically (using kmalloc()). 
 */ 

/* 
 * This structure defines the hardware state of the graphics card. Normally
 * you place this in a header file in linux/include/video. This file usually
 * also includes register information. That allows other driver subsystems
 * and userland applications the ability to use the same header file to 
 * avoid duplicate work and easy porting of software. 
 */
struct st7565p_par{
	struct device*	dev ;
};

static struct fb_videomode dotmatrix_modes[] = {
	{
		.refresh	= 51,
#ifdef CONFIG_SC1445x_IST3020_LCD_SUPPORT		
		.xres		= 192,		.yres		= 64,
#else //	CONFIG_SC1445x_ST7565P_LCD_SUPPORT
		.xres		= 128,		.yres		= 64,
#endif		
		.pixclock	= 156250,  // 6.4 MHz

		.left_margin	= 0/*40*/,		.right_margin	= 0/*20*/,
		.upper_margin	= 0/*19*/,		.lower_margin	= 0/*47*/,
		.hsync_len	= 0,		.vsync_len	= 0,

		.sync		= 0x0, //  0,
		.vmode		= FB_VMODE_NONINTERLACED,
	},
};


static struct fb_monspecs default_monspecs = {
	.manufacturer	= "TS",
	.modedb		= dotmatrix_modes,
	.modedb_len	= ARRAY_SIZE(dotmatrix_modes),
	.monitor	= "LCD panel",
	.serial_no	= "xxxx",
	.ascii		= "yyyy",
	.hfmin		= 14820,
	.hfmax		= 22230,
	.vfmin		= 50,
	.vfmax		= 90,
	.dclkmax	= 30000000,
};
/*
 * Here we define the default structs fb_fix_screeninfo and fb_var_screeninfo
 * if we don't use modedb. If we do use modedb see st7565p_fb_init how to use it
 * to get a fb_var_screeninfo. Otherwise define a default var as well. 
 */
static struct fb_fix_screeninfo st7565p_fb_fix __initdata = {
	.id =		"st7565p_fb", 
	.type =		FB_TYPE_PACKED_PIXELS,
	.visual =	FB_VISUAL_MONO10,//FB_VISUAL_MONO01,
	.xpanstep =	1,
	.ypanstep =	1,
	.ywrapstep =	0, 
	.accel =	FB_ACCEL_NONE,
	//added for st7565p
	.smem_start= (unsigned long)FRAMEBUFFER_VIRTUAL_MEMORY,	/* Start of frame buffer mem */
					/* (physical address) */
	.smem_len= Y_BYTES*X_BYTES,			/* Length of frame buffer mem */
	//.type_aux,			/* Interleave for interleaved Planes */

	.line_length=X_BYTES,		/* length of a line in bytes    */
	.mmio_start= (unsigned long)FRAMEBUFFER_VIRTUAL_MEMORY,	/* Start of Memory Mapped I/O   */
					/* (physical address) */
	.mmio_len= Y_BYTES*X_BYTES,			/* Length of Memory Mapped I/O  */
					/*  specific chip/card we have	*/
	//reserved[3];		
};


    /*
     * 	Modern graphical hardware not only supports pipelines but some 
     *  also support multiple monitors where each display can have its  
     *  its own unique data. In this case each display could be  
     *  represented by a separate framebuffer device thus a separate 
     *  struct fb_info. Now the struct st7565p_par represents the graphics
     *  hardware state thus only one exist per card. In this case the 
     *  struct st7565p_par for each graphics card would be shared between 
     *  every struct fb_info that represents a framebuffer on that card. 
     *  This allows when one display changes it video resolution (info->var) 
     *  the other displays know instantly. Each display can always be
     *  aware of the entire hardware state that affects it because they share
     *  the same st7565p_par struct. The other side of the coin is multiple
     *  graphics cards that pass data around until it is finally displayed
     *  on one monitor. Such examples are the voodoo 1 cards and high end
     *  NUMA graphics servers. For this case we have a bunch of pars, each
     *  one that represents a graphics state, that belong to one struct 
     *  fb_info. Their you would want to have *par point to a array of device
     *  states and have each struct fb_ops function deal with all those 
     *  states. I hope this covers every possible hardware design. If not
     *  feel free to send your ideas at jsimmons@users.sf.net 
     */

    /*
     *  If your driver supports multiple boards or it supports multiple 
     *  framebuffers, you should make these arrays, or allocate them 
     *  dynamically using framebuffer_alloc() and free them with
     *  framebuffer_release().
     */ 
static struct fb_info info;

    /* 
     * Each one represents the state of the hardware. Most hardware have
     * just one hardware state. These here represent the default state(s). 
     */
//static struct st7565p_par __initdata current_par;



    /*
     *  Internal routines
     */

static unsigned short get_line_length(unsigned short xres_virtual, unsigned short bpp)
{
	unsigned short length;

	length = xres_virtual * bpp;
	length = (length + 31) & ~31;//GIAG .. this should be checked
	length >>= 3;
	return (length);
}

/**
 *	st7565p_fb_open - Optional function. Called when the framebuffer is
 *		     first accessed.
 *	@info: frame buffer structure that represents a single frame buffer
 *	@user: tell us if the userland (value=1) or the console is accessing
 *	       the framebuffer. 
 *
 *	This function is the first function called in the framebuffer api.
 *	Usually you don't need to provide this function. The case where it 
 *	is used is to change from a text mode hardware state to a graphics
 * 	mode state. 
 *
 *	Returns negative errno on error, or zero on success.
 */
static int st7565p_fb_open(struct fb_info *info, int user)
{
	PDEBUG("LCD st14450 : fbopen \n");
    return 0;
}

/**
 *	st7565p_fb_release - Optional function. Called when the framebuffer 
 *			device is closed. 
 *	@info: frame buffer structure that represents a single frame buffer
 *	@user: tell us if the userland (value=1) or the console is accessing
 *	       the framebuffer. 
 *	
 *	Thus function is called when we close /dev/fb or the framebuffer 
 *	console system is released. Usually you don't need this function.
 *	The case where it is usually used is to go from a graphics state
 *	to a text mode state.
 *
 *	Returns negative errno on error, or zero on success.
 */
static int st7565p_fb_release(struct fb_info *info, int user)
{
    return 0;
}
/*Custom mmap is implemented only for avoiding page alignment performed by fb_mmap()*/
static int st7565p_fb_mmap(struct fb_info *info,
		    struct vm_area_struct *vma)
{

		
/*Some housekeeping code copied by fb_mmap()*/
	//vma->vm_pgoff = off >> PAGE_SHIFT;
	/* This is an IO map - tell maydump to skip this VMA */
	vma->vm_flags |= VM_IO | VM_RESERVED;
	
	// unsigned long off = 0x1300000;
//vma->vm_pgoff << PAGE_SHIFT
	// printk(KERN_WARNING "FB giag_debug: vfb_mmap \n" );
	// if (io_remap_pfn_range(vma, vma->vm_start, off >> PAGE_SHIFT,
			     // vma->vm_end - vma->vm_start, vma->vm_page_prot))
		// return -EAGAIN;

		//vma->vm_pgoff=	info->fix.smem_start;
		vma->vm_start = info->fix.smem_start;
	// if (io_remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff,
			     // vma->vm_end - vma->vm_start, vma->vm_page_prot))
		// return -EAGAIN;
#ifdef GIAG_DEBUG
	printk(KERN_NOTICE "lcd_sc14450, in function : %s, vma->vm_start:%lx, vma->vm_pgoff:%lx, \
vma->vm_end :%lx, vma->vm_page_prot:%lx \n", __FUNCTION__, vma->vm_start, vma->vm_pgoff, \
			     vma->vm_end, vma->vm_page_prot);
#endif			
	return 0;		
				
	return -EINVAL;
}
/**
 *      st7565p_fb_check_var - Optional function. Validates a var passed in. 
 *      @var: frame buffer variable screen structure
 *      @info: frame buffer structure that represents a single frame buffer 
 *
 *	Checks to see if the hardware supports the state requested by
 *	var passed in. This function does not alter the hardware state!!! 
 *	This means the data stored in struct fb_info and struct st7565p_par do 
 *      not change. This includes the var inside of struct fb_info. 
 *	Do NOT change these. This function can be called on its own if we
 *	intent to only test a mode and not actually set it. The stuff in 
 *	modedb.c is a example of this. If the var passed in is slightly 
 *	off by what the hardware can support then we alter the var PASSED in
 *	to what we can do.
 *
 *      For values that are off, this function must round them _up_ to the
 *      next value that is supported by the hardware.  If the value is
 *      greater than the highest value supported by the hardware, then this
 *      function must return -EINVAL.
 *
 *      Exception to the above rule:  Some drivers have a fixed mode, ie,
 *      the hardware is already set at boot up, and cannot be changed.  In
 *      this case, it is more acceptable that this function just return
 *      a copy of the currently working var (info->var). Better is to not
 *      implement this function, as the upper layer will do the copying
 *      of the current var for you.
 *
 *      Note:  This is the only function where the contents of var can be
 *      freely adjusted after the driver has been registered. If you find
 *      that you have code outside of this function that alters the content
 *      of var, then you are doing something wrong.  Note also that the
 *      contents of info->var must be left untouched at all times after
 *      driver registration.
 *
 *	Returns negative errno on error, or zero on success.
 */
static int st7565p_fb_check_var(struct fb_var_screeninfo *var, struct fb_info *info)
{
	unsigned short line_length;

	/*
	 *  FB_VMODE_CONUPDATE and FB_VMODE_SMOOTH_XPAN are equal!
	 *  as FB_VMODE_SMOOTH_XPAN is only used internally
	 */

	if (var->vmode & FB_VMODE_CONUPDATE) {
		var->vmode |= FB_VMODE_YWRAP;
		var->xoffset = info->var.xoffset;
		var->yoffset = info->var.yoffset;
	}

	/*
	 *  Some very basic checks
	 */
	if (!var->xres)
		var->xres = 1;
	if (!var->yres)
		var->yres = 1;
	if (var->xres > var->xres_virtual)
		var->xres_virtual = var->xres;
	if (var->yres > var->yres_virtual)
		var->yres_virtual = var->yres;
	if (var->bits_per_pixel <= 1)
		var->bits_per_pixel = 1;
	else if (var->bits_per_pixel <= 8)
		var->bits_per_pixel = 8;
	else if (var->bits_per_pixel <= 16)
		var->bits_per_pixel = 16;
	else if (var->bits_per_pixel <= 24)
		var->bits_per_pixel = 24;
	else if (var->bits_per_pixel <= 32)
		var->bits_per_pixel = 32;
	else{
		#ifdef GIAG_DEBUG
		printk(KERN_NOTICE "lcd_sc14450, in function : %s, failed in basic checks \n", __FUNCTION__);
	#endif	
		return -EINVAL;
	}
	if (var->xres_virtual < var->xoffset + var->xres)
		var->xres_virtual = var->xoffset + var->xres;
	if (var->yres_virtual < var->yoffset + var->yres)
		var->yres_virtual = var->yoffset + var->yres;

	/*
	 *  Memory limit
	 */
	line_length =
	    get_line_length(var->xres_virtual, var->bits_per_pixel);
	//if (line_length * var->yres_virtual > videomemorysize)
	if (line_length * var->yres_virtual > info->fix.smem_len)	{
	#ifdef GIAG_DEBUG
		printk(KERN_NOTICE "lcd_sc14450, in function : %s, line_length:%lx, var->yres_virtual:%lx, \
var->xres_virtual :%lx, info->fix.smem_len:%lx, var->bits_per_pixel: %d \n", __FUNCTION__, line_length, var->yres_virtual, \
			     var->xres_virtual, info->fix.smem_len, var->bits_per_pixel);
	#endif	
		return -ENOMEM;
	}

	/*
	 * Now that we checked it we alter var. The reason being is that the video
	 * mode passed in might not work but slight changes to it might make it 
	 * work. This way we let the user know what is acceptable.
	 */
	switch (var->bits_per_pixel) {
	case 1:
	case 8:
		var->red.offset = 0;
		var->red.length = 8;
		var->green.offset = 0;
		var->green.length = 8;
		var->blue.offset = 0;
		var->blue.length = 8;
		var->transp.offset = 0;
		var->transp.length = 0;
		break;
	case 16:		/* RGBA 5551 */
		if (var->transp.length) {
			var->red.offset = 0;
			var->red.length = 5;
			var->green.offset = 5;
			var->green.length = 5;
			var->blue.offset = 10;
			var->blue.length = 5;
			var->transp.offset = 15;
			var->transp.length = 1;
		} else {	/* RGB 565 */
			var->red.offset = 0;
			var->red.length = 5;
			var->green.offset = 5;
			var->green.length = 6;
			var->blue.offset = 11;
			var->blue.length = 5;
			var->transp.offset = 0;
			var->transp.length = 0;
		}
		break;
	case 24:		/* RGB 888 */
		var->red.offset = 0;
		var->red.length = 8;
		var->green.offset = 8;
		var->green.length = 8;
		var->blue.offset = 16;
		var->blue.length = 8;
		var->transp.offset = 0;
		var->transp.length = 0;
		break;
	case 32:		/* RGBA 8888 */
		var->red.offset = 0;
		var->red.length = 8;
		var->green.offset = 8;
		var->green.length = 8;
		var->blue.offset = 16;
		var->blue.length = 8;
		var->transp.offset = 24;
		var->transp.length = 8;
		break;
	default:
		printk(KERN_NOTICE "st7565p_fb: color depth %d not supported\n",
		       var->bits_per_pixel);
		return -EINVAL;		
	}
	var->red.msb_right = 0;
	var->green.msb_right = 0;
	var->blue.msb_right = 0;
	var->transp.msb_right = 0;

    return 0;	   	
}

/**
 *      st7565p_fb_set_par - Optional function. Alters the hardware state.
 *      @info: frame buffer structure that represents a single frame buffer
 *
 *	Using the fb_var_screeninfo in fb_info we set the resolution of the
 *	this particular framebuffer. This function alters the par AND the
 *	fb_fix_screeninfo stored in fb_info. It doesn't not alter var in 
 *	fb_info since we are using that data. This means we depend on the
 *	data in var inside fb_info to be supported by the hardware. 
 *
 *      This function is also used to recover/restore the hardware to a
 *      known working state.
 *
 *	st7565p_fb_check_var is always called before st7565p_fb_set_par to ensure that
 *      the contents of var is always valid.
 *
 *	Again if you can't change the resolution you don't need this function.
 *
 *      However, even if your hardware does not support mode changing,
 *      a set_par might be needed to at least initialize the hardware to
 *      a known working state, especially if it came back from another
 *      process that also modifies the same hardware, such as X.
 *
 *      If this is the case, a combination such as the following should work:
 *
 *      static int st7565p_fb_check_var(struct fb_var_screeninfo *var,
 *                                struct fb_info *info)
 *      {
 *              *var = info->var;
 *              return 0;
 *      }
 *
 *      static int st7565p_fb_set_par(struct fb_info *info)
 *      {
 *              init your hardware here
 *      }
 *
 *	Returns negative errno on error, or zero on success.
 */
static int st7565p_fb_set_par(struct fb_info *info)
{
//    struct st7565p_par *par = info->par;
	info->fix.line_length = get_line_length(info->var.xres_virtual,
						info->var.bits_per_pixel);
    return 0;	
}

/**
 *  	st7565p_fb_setcolreg - Optional function. Sets a color register.
 *      @regno: Which register in the CLUT we are programming 
 *      @red: The red value which can be up to 16 bits wide 
 *	@green: The green value which can be up to 16 bits wide 
 *	@blue:  The blue value which can be up to 16 bits wide.
 *	@transp: If supported, the alpha value which can be up to 16 bits wide.
 *      @info: frame buffer info structure
 * 
 *  	Set a single color register. The values supplied have a 16 bit
 *  	magnitude which needs to be scaled in this function for the hardware. 
 *	Things to take into consideration are how many color registers, if
 *	any, are supported with the current color visual. With truecolor mode
 *	no color palettes are supported. Here a pseudo palette is created
 *	which we store the value in pseudo_palette in struct fb_info. For
 *	pseudocolor mode we have a limited color palette. To deal with this
 *	we can program what color is displayed for a particular pixel value.
 *	DirectColor is similar in that we can program each color field. If
 *	we have a static colormap we don't need to implement this function. 
 * 
 *	Returns negative errno on error, or zero on success.
 */
static int st7565p_fb_setcolreg(unsigned regno, unsigned red, unsigned green,
			   unsigned blue, unsigned transp,
			   const struct fb_info *info)
{
    if (regno >= 256)  /* no. of hw registers */
       return -EINVAL;
    /*
     * Program hardware... do anything you want with transp
     */

    /* grayscale works only partially under directcolor */
    if (info->var.grayscale) {
       /* grayscale = 0.30*R + 0.59*G + 0.11*B */
       red = green = blue = (red * 77 + green * 151 + blue * 28) >> 8;
    }

    /* Directcolor:
     *   var->{color}.offset contains start of bitfield
     *   var->{color}.length contains length of bitfield
     *   {hardwarespecific} contains width of DAC
     *   pseudo_palette[X] is programmed to (X << red.offset) |
     *                                      (X << green.offset) |
     *                                      (X << blue.offset)
     *   RAMDAC[X] is programmed to (red, green, blue)
     *   color depth = SUM(var->{color}.length)
     *
     * Pseudocolor:
     *    var->{color}.offset is 0
     *    var->{color}.length contains width of DAC or the number of unique
     *                        colors available (color depth)
     *    pseudo_palette is not used
     *    RAMDAC[X] is programmed to (red, green, blue)
     *    color depth = var->{color}.length
     *
     * Static pseudocolor:
     *    same as Pseudocolor, but the RAMDAC is not programmed (read-only)
     *
     * Mono01/Mono10:
     *    Has only 2 values, black on white or white on black (fg on bg),
     *    var->{color}.offset is 0
     *    white = (1 << var->{color}.length) - 1, black = 0
     *    pseudo_palette is not used
     *    RAMDAC does not exist
     *    color depth is always 2
     *
     * Truecolor:
     *    does not use RAMDAC (usually has 3 of them).
     *    var->{color}.offset contains start of bitfield
     *    var->{color}.length contains length of bitfield
     *    pseudo_palette is programmed to (red << red.offset) |
     *                                    (green << green.offset) |
     *                                    (blue << blue.offset) |
     *                                    (transp << transp.offset)
     *    RAMDAC does not exist
     *    color depth = SUM(var->{color}.length})
     *
     *  The color depth is used by fbcon for choosing the logo and also
     *  for color palette transformation if color depth < 4
     *
     *  As can be seen from the above, the field bits_per_pixel is _NOT_
     *  a criteria for describing the color visual.
     *
     *  A common mistake is assuming that bits_per_pixel <= 8 is pseudocolor,
     *  and higher than that, true/directcolor.  This is incorrect, one needs
     *  to look at the fix->visual.
     *
     *  Another common mistake is using bits_per_pixel to calculate the color
     *  depth.  The bits_per_pixel field does not directly translate to color
     *  depth. You have to compute for the color depth (using the color
     *  bitfields) and fix->visual as seen above.
     */

    /*
     * This is the point where the color is converted to something that
     * is acceptable by the hardware.
     */
#define CNVT_TOHW(val,width) ((((val)<<(width))+0x7FFF-(val))>>16)
    red = CNVT_TOHW(red, info->var.red.length);
    green = CNVT_TOHW(green, info->var.green.length);
    blue = CNVT_TOHW(blue, info->var.blue.length);
    transp = CNVT_TOHW(transp, info->var.transp.length);
#undef CNVT_TOHW
    /*
     * This is the point where the function feeds the color to the hardware
     * palette after converting the colors to something acceptable by
     * the hardware. Note, only FB_VISUAL_DIRECTCOLOR and
     * FB_VISUAL_PSEUDOCOLOR visuals need to write to the hardware palette.
     * If you have code that writes to the hardware CLUT, and it's not
     * any of the above visuals, then you are doing something wrong.
     */
    // if (info->fix.visual == FB_VISUAL_DIRECTCOLOR ||
	// info->fix.visual == FB_VISUAL_TRUECOLOR)
	    // write_{red|green|blue|transp}_to_clut();

    /* This is the point were you need to fill up the contents of
     * info->pseudo_palette. This structure is used _only_ by fbcon, thus
     * it only contains 16 entries to match the number of colors supported
     * by the console. The pseudo_palette is used only if the visual is
     * in directcolor or truecolor mode.  With other visuals, the
     * pseudo_palette is not used. (This might change in the future.)
     *
     * The contents of the pseudo_palette is in raw pixel format.  Ie, each
     * entry can be written directly to the framebuffer without any conversion.
     * The pseudo_palette is (void *).  However, if using the generic
     * drawing functions (cfb_imageblit, cfb_fillrect), the pseudo_palette
     * must be casted to (u32 *) _regardless_ of the bits per pixel. If the
     * driver is using its own drawing functions, then it can use whatever
     * size it wants.
     */
    if (info->fix.visual == FB_VISUAL_TRUECOLOR ||
	info->fix.visual == FB_VISUAL_DIRECTCOLOR) {
	    u32 v;

	    if (regno >= 16)
		    return -EINVAL;

	    v = (red << info->var.red.offset) |
		    (green << info->var.green.offset) |
		    (blue << info->var.blue.offset) |
		    (transp << info->var.transp.offset);

	    ((u32*)(info->pseudo_palette))[regno] = v;
    }

    /* ... */
    return 0;
}

/**
 *      st7565p_fb_pan_display - NOT a required function. Pans the display.
 *      @var: frame buffer variable screen structure
 *      @info: frame buffer structure that represents a single frame buffer
 *
 *	Pan (or wrap, depending on the `vmode' field) the display using the
 *  	`xoffset' and `yoffset' fields of the `var' structure.
 *  	If the values don't fit, return -EINVAL.
 *
 *      Returns negative errno on error, or zero on success.
 */
// static int st7565p_fb_pan_display(struct fb_var_screeninfo *var,
			     // const struct fb_info *info)
// {
    // /*
     // * If your hardware does not support panning, _do_ _not_ implement this
     // * function. Creating a dummy function will just confuse user apps.
     // */

    // /*
     // * Note that even if this function is fully functional, a setting of
     // * 0 in both xpanstep and ypanstep means that this function will never
     // * get called.
     // */

    // /* ... */
    // return 0;
// }

/**
 *      st7565p_fb_blank - NOT a required function. Blanks the display.
 *      @blank_mode: the blank mode we want. 
 *      @info: frame buffer structure that represents a single frame buffer
 *
 *      Blank the screen if blank_mode != FB_BLANK_UNBLANK, else unblank.
 *      Return 0 if blanking succeeded, != 0 if un-/blanking failed due to
 *      e.g. a video mode which doesn't support it.
 *
 *      Implements VESA suspend and powerdown modes on hardware that supports
 *      disabling hsync/vsync:
 *
 *      FB_BLANK_NORMAL = display is blanked, syncs are on.
 *      FB_BLANK_HSYNC_SUSPEND = hsync off
 *      FB_BLANK_VSYNC_SUSPEND = vsync off
 *      FB_BLANK_POWERDOWN =  hsync and vsync off
 *
 *      If implementing this function, at least support FB_BLANK_UNBLANK.
 *      Return !0 for any modes that are unimplemented.
 *
 */
// static int st7565p_fb_blank(int blank_mode, const struct fb_info *info)
// {
    // /* ... */
    // return 0;
// }

/* ------------ Accelerated Functions --------------------- */

/*
 * We provide our own functions if we have hardware acceleration
 * or non packed pixel format layouts. If we have no hardware 
 * acceleration, we can use a generic unaccelerated function. If using
 * a pack pixel format just use the functions in cfb_*.c. Each file 
 * has one of the three different accel functions we support.
 */

/**
 *      st7565p_fb_fillrect - REQUIRED function. Can use generic routines if 
 *		 	 non acclerated hardware and packed pixel based.
 *			 Draws a rectangle on the screen.		
 *
 *      @info: frame buffer structure that represents a single frame buffer
 *	@region: The structure representing the rectangular region we 
 *		 wish to draw to.
 *
 *	This drawing operation places/removes a retangle on the screen 
 *	depending on the rastering operation with the value of color which
 *	is in the current color depth format.
 */
// void st7565p_fillrect(struct fb_info *p, const struct fb_fillrect *region)
// {
// /*	Meaning of struct fb_fillrect
 // *
 // *	@dx: The x and y corrdinates of the upper left hand corner of the 
 // *	@dy: area we want to draw to. 
 // *	@width: How wide the rectangle is we want to draw.
 // *	@height: How tall the rectangle is we want to draw.
 // *	@color:	The color to fill in the rectangle with. 
 // *	@rop: The raster operation. We can draw the rectangle with a COPY
 // *	      of XOR which provides erasing effect. 
 // */
// }

/**
 *      st7565p_fb_copyarea - REQUIRED function. Can use generic routines if
 *                       non acclerated hardware and packed pixel based.
 *                       Copies one area of the screen to another area.
 *
 *      @info: frame buffer structure that represents a single frame buffer
 *      @area: Structure providing the data to copy the framebuffer contents
 *	       from one region to another.
 *
 *      This drawing operation copies a rectangular area from one area of the
 *	screen to another area.
 */
// void st7565p_fb_copyarea(struct fb_info *p, const struct fb_copyarea *area) 
// {
// /*
 // *      @dx: The x and y coordinates of the upper left hand corner of the
 // *	@dy: destination area on the screen.
 // *      @width: How wide the rectangle is we want to copy.
 // *      @height: How tall the rectangle is we want to copy.
 // *      @sx: The x and y coordinates of the upper left hand corner of the
 // *      @sy: source area on the screen.
 // */
// }


/**
 *      st7565p_fb_imageblit - REQUIRED function. Can use generic routines if
 *                        non acclerated hardware and packed pixel based.
 *                        Copies a image from system memory to the screen. 
 *
 *      @info: frame buffer structure that represents a single frame buffer
 *	@image:	structure defining the image.
 *
 *      This drawing operation draws a image on the screen. It can be a 
 *	mono image (needed for font handling) or a color image (needed for
 *	tux). 
 */
// void st7565p_fb_imageblit(struct fb_info *p, const struct fb_image *image) 
// {
// /*
 // *      @dx: The x and y coordinates of the upper left hand corner of the
 // *	@dy: destination area to place the image on the screen.
 // *      @width: How wide the image is we want to copy.
 // *      @height: How tall the image is we want to copy.
 // *      @fg_color: For mono bitmap images this is color data for     
 // *      @bg_color: the foreground and background of the image to
 // *		   write directly to the frmaebuffer.
 // *	@depth:	How many bits represent a single pixel for this image.
 // *	@data: The actual data used to construct the image on the display.
 // *	@cmap: The colormap used for color images.   
 // */

// /*
 // * The generic function, cfb_imageblit, expects that the bitmap scanlines are
 // * padded to the next byte.  Most hardware accelerators may require padding to
 // * the next u16 or the next u32.  If that is the case, the driver can specify
 // * this by setting info->pixmap.scan_align = 2 or 4.  See a more
 // * comprehensive description of the pixmap below.
 // */
// }

/**
 *	st7565p_fb_cursor - 	OPTIONAL. If your hardware lacks support
 *			for a cursor, leave this field NULL.
 *
 *      @info: frame buffer structure that represents a single frame buffer
 *	@cursor: structure defining the cursor to draw.
 *
 *      This operation is used to set or alter the properities of the
 *	cursor.
 *
 *	Returns negative errno on error, or zero on success.
 */
// int st7565p_fb_cursor(struct fb_info *info, struct fb_cursor *cursor)
// {
/*
 *      @set: 	Which fields we are altering in struct fb_cursor 
 *	@enable: Disable or enable the cursor 
 *      @rop: 	The bit operation we want to do. 
 *      @mask:  This is the cursor mask bitmap. 
 *      @dest:  A image of the area we are going to display the cursor.
 *		Used internally by the driver.	 
 *      @hot:	The hot spot. 
 *	@image:	The actual data for the cursor image.
 *
 *      NOTES ON FLAGS (cursor->set):
 *
 *      FB_CUR_SETIMAGE - the cursor image has changed (cursor->image.data)
 *      FB_CUR_SETPOS   - the cursor position has changed (cursor->image.dx|dy)
 *      FB_CUR_SETHOT   - the cursor hot spot has changed (cursor->hot.dx|dy)
 *      FB_CUR_SETCMAP  - the cursor colors has changed (cursor->fg_color|bg_color)
 *      FB_CUR_SETSHAPE - the cursor bitmask has changed (cursor->mask)
 *      FB_CUR_SETSIZE  - the cursor size has changed (cursor->width|height)
 *      FB_CUR_SETALL   - everything has changed
 *
 *      NOTES ON ROPs (cursor->rop, Raster Operation)
 *
 *      ROP_XOR         - cursor->image.data XOR cursor->mask
 *      ROP_COPY        - curosr->image.data AND cursor->mask
 *
 *      OTHER NOTES:
 *
 *      - fbcon only supports a 2-color cursor (cursor->image.depth = 1)
 *      - The fb_cursor structure, @cursor, _will_ always contain valid
 *        fields, whether any particular bitfields in cursor->set is set
 *        or not.
 */
// }

/**
 *	st7565p_fb_rotate -  NOT a required function. If your hardware
 *			supports rotation the whole screen then 
 *			you would provide a hook for this. 
 *
 *      @info: frame buffer structure that represents a single frame buffer
 *	@angle: The angle we rotate the screen.   
 *
 *      This operation is used to set or alter the properities of the
 *	cursor.
 */
// void st7565p_fb_rotate(struct fb_info *info, int angle)
// {
// /* Will be deprecated */
// }

/**
 *	st7565p_fb_poll - NOT a required function. The purpose of this
 *		     function is to provide a way for some process
 *		     to wait until a specific hardware event occurs
 *		     for the framebuffer device.
 * 				 
 *      @info: frame buffer structure that represents a single frame buffer
 *	@wait: poll table where we store process that await a event.     
 */
// void st7565p_fb_poll(struct fb_info *info, poll_table *wait)
// {
// }

/**
 *	st7565p_fb_sync - NOT a required function. Normally the accel engine 
 *		     for a graphics card take a specific amount of time.
 *		     Often we have to wait for the accelerator to finish
 *		     its operation before we can write to the framebuffer
 *		     so we can have consistent display output. 
 *
 *      @info: frame buffer structure that represents a single frame buffer
 *
 *      If the driver has implemented its own hardware-based drawing function,
 *      implementing this function is highly recommended.
 */
// void st7565p_fb_sync(struct fb_info *info)
// {
// }
/* ------------------------------------------------------------------------- */

    /*
     *  Frame buffer operations
     */

static struct fb_ops st7565p_fb_ops = {
	.owner		= THIS_MODULE,
	.fb_open	= st7565p_fb_open,
	//.fb_read	= st7565p_fb_read,
	//.fb_write	= st7565p_fb_write,
	.fb_release	= st7565p_fb_release,
	.fb_check_var	= st7565p_fb_check_var,
	.fb_set_par	= st7565p_fb_set_par,	
	.fb_setcolreg	= st7565p_fb_setcolreg,
	//.fb_blank	= st7565p_fb_blank,
	//.fb_pan_display	= st7565p_fb_pan_display,	
	//.fb_fillrect	= st7565p_fb_fillrect, 	/* Needed !!! */ 
	//.fb_copyarea	= st7565p_fb_copyarea,	/* Needed !!! */ 
	//.fb_imageblit	= st7565p_fb_imageblit,	/* Needed !!! */
	.fb_fillrect	= cfb_fillrect,//giag
	.fb_copyarea	= cfb_copyarea,//giag
	.fb_imageblit	= cfb_imageblit,//giag	
	//.fb_cursor	= st7565p_fb_cursor,		/* Optional !!! */
	//.fb_rotate	= st7565p_fb_rotate,
	//.fb_poll	= st7565p_fb_poll,
	//.fb_sync	= st7565p_fb_sync,
	// .fb_ioctl	= st7565p_fb_ioctl,
	.fb_mmap	= st7565p_fb_mmap,	
};

/* ------------------------------------------------------------------------- */


    /*
     *  Initialization
     */

static int __init st7565p_fb_probe (struct platform_device *device)
{
    struct fb_info *info;
    struct st7565p_par *par;
    int cmap_len, retval;	
    int res = 0 ;
	char *mode_option=NULL;
    /*
     * Dynamically allocate info and par
     */
    info = framebuffer_alloc(sizeof(struct st7565p_par), device);

    if (!info) {
		printk(KERN_NOTICE "fb_sc7565p: failed to allocate memory\n");
	    /* goto error path */
		goto framebuffer_rel;
    }

    par = info->par;
	par->dev=device;//giag store device pointer

    /* 
     * Here we set the screen_base to the virtual memory address
     * for the framebuffer. Usually we obtain the resource address
     * from the bus layer and then translate it to virtual memory
     * space via ioremap. Consult ioport.h. 
     */
	memcpy( &info->monspecs, &default_monspecs, sizeof(info->monspecs) ) ;//giag
    info->screen_base = (unsigned long)FRAMEBUFFER_VIRTUAL_MEMORY;
    info->fbops = &st7565p_fb_ops;
    info->fix = st7565p_fb_fix; /* this will be the only time st7565p_fb_fix will be
			    * used, so mark it as __initdata
			    */
    info->pseudo_palette = pseudo_palette; /* The pseudopalette is an
					    * 16-member array
					    */
    /*
     * Set up flags to indicate what sort of acceleration your
     * driver can provide (pan/wrap/copyarea/etc.) and whether it
     * is a module -- see FBINFO_* in include/linux/fb.h
     *
     * If your hardware can support any of the hardware accelerated functions
     * fbcon performance will improve if info->flags is set properly.
     *
     * FBINFO_HWACCEL_COPYAREA - hardware moves
     * FBINFO_HWACCEL_FILLRECT - hardware fills
     * FBINFO_HWACCEL_IMAGEBLIT - hardware mono->color expansion
     * FBINFO_HWACCEL_YPAN - hardware can pan display in y-axis
     * FBINFO_HWACCEL_YWRAP - hardware can wrap display in y-axis
     * FBINFO_HWACCEL_DISABLED - supports hardware accels, but disabled
     * FBINFO_READS_FAST - if set, prefer moves over mono->color expansion
     * FBINFO_MISC_TILEBLITTING - hardware can do tile blits
     *
     * NOTE: These are for fbcon use only.
     */
    info->flags = FBINFO_DEFAULT;

/********************* This stage is optional ******************************/
#ifdef PIXMAP_ON
     /*
     * The struct pixmap is a scratch pad for the drawing functions. This
     * is where the monochrome bitmap is constructed by the higher layers
     * and then passed to the accelerator.  For drivers that uses
     * cfb_imageblit, you can skip this part.  For those that have a more
     * rigorous requirement, this stage is needed
     */

    /* PIXMAP_SIZE should be small enough to optimize drawing, but not
     * large enough that memory is wasted.  A safe size is
     * (max_xres * max_font_height/8). max_xres is driver dependent,
     * max_font_height is 32.
     */
	 
    info->pixmap.addr = kmalloc(PIXMAP_SIZE, GFP_KERNEL);
    if (!info->pixmap.addr) {
	    /* goto error */
    }

    info->pixmap.size = PIXMAP_SIZE;

    /*
     * FB_PIXMAP_SYSTEM - memory is in system ram
     * FB_PIXMAP_IO     - memory is iomapped
     * FB_PIXMAP_SYNC   - if set, will call fb_sync() per access to pixmap,
     *                    usually if FB_PIXMAP_IO is set.
     *
     * Currently, FB_PIXMAP_IO is unimplemented.
     */
    info->pixmap.flags = FB_PIXMAP_SYSTEM;

    /*
     * scan_align is the number of padding for each scanline.  It is in bytes.
     * Thus for accelerators that need padding to the next u32, put 4 here.
     */
    info->pixmap.scan_align = 4;

    /*
     * buf_align is the amount to be padded for the buffer. For example,
     * the i810fb needs a scan_align of 2 but expects it to be fed with
     * dwords, so a buf_align = 4 is required.
     */
    info->pixmap.buf_align = 4;

    /* access_align is how many bits can be accessed from the framebuffer
     * ie. some epson cards allow 16-bit access only.  Most drivers will
     * be safe with u32 here.
     *
     * NOTE: This field is currently unused.
     */
    info->pixmap.scan_align = 32
#endif	
/***************************** End optional stage ***************************/
#if 1
    /*
     * This should give a reasonable default video mode. The following is
     * done when we can set a video mode. 
     */
    if (!mode_option) 	
#ifdef CONFIG_SC1445x_IST3020_LCD_SUPPORT		
	mode_option = "192x64-1@51";	
#else //	CONFIG_SC1445x_ST7565P_LCD_SUPPORT
	mode_option = "128x64-1@51";	
#endif	
    //retval = fb_find_mode(&info->var, info, mode_option, NULL, 0, NULL, 8);
	retval = fb_find_mode( &info->var, info, mode_option/*NULL*/, info->monspecs.modedb,
			    info->monspecs.modedb_len, info->monspecs.modedb,
			    8 ) ;  
    if (!retval || retval == 4){
		printk(KERN_ERR "st7565p_fb: No suitable video mode found\n" ) ;
		res = -EINVAL;
		goto framebuffer_rel ;
	}
#endif
    /* This has to been done !!! */	
	cmap_len=2;//monochrome display
    if(fb_alloc_cmap(&info->cmap, cmap_len, 0)){
		printk(KERN_ERR "st7565p_fb: Could not allocate color map\n" ) ;
		res = -ENOMEM ;
		goto free_cmap ;
	}
	
    /* 
     * The following is done in the case of having hardware with a static 
     * mode. If we are setting the mode ourselves we don't call this. 
     */	
   // info->var = st7565p_fb_var; giag

    /*
     * For drivers that can...
     */
    st7565p_fb_check_var(&info->var, info);

    /*
     * Does a call to fb_set_par() before register_framebuffer needed?  This
     * will depend on you and the hardware.  If you are sure that your driver
     * is the only device in the system, a call to fb_set_par() is safe.
     *
     * Hardware in x86 systems has a VGA core.  Calling set_par() at this
     * point will corrupt the VGA console, so it might be safer to skip a
     * call to set_par here and just allow fbcon to do it for you.
     */
    /* st7565p_fb_set_par(info); */

    if (register_framebuffer(info) < 0){
		goto free_cmap;
		res = -EINVAL;
	}

    printk(KERN_INFO "fb%d: %s frame buffer device\n", info->node,
	   info->fix.id);
    platform_set_drvdata(device, info) ;
	printk(KERN_INFO
	       "fb%d: sc14450 dotmatrix frame buffer device, using %ldK of video memory, starting at address %lx\n",
	       info->node, info->fix.smem_len, info->fix.smem_start);
	return 0;
free_cmap:
	fb_dealloc_cmap( &info->cmap ) ;	
framebuffer_rel:
	framebuffer_release( info ) ;
    return res ;
}

    /*
     *  Cleanup
     */
static void __exit st7565p_fb_remove(struct platform_device *device) 
{
	struct fb_info *info = platform_get_drvdata(device);

	if (info) {
		unregister_framebuffer(info);
		fb_dealloc_cmap(&info->cmap);
		/* ... */
		framebuffer_release(info);
	}

}



/* for platform devices */
static struct device_driver st7565p_fb_driver = {
	.name = "st7565p_fb",
	.bus  = &platform_bus_type,
	.probe = st7565p_fb_probe,
	.remove = st7565p_fb_remove,
	// .suspend = st7565p_fb_suspend, /* optional */
	// .resume = st7565p_fb_resume,   /* optional */
};

static struct platform_device st7565p_fb_device = {
	.name = "st7565p_fb",
};

static int __init st7565p_fb_init(void)
{
	int ret;
	/*
	 *  For kernel boot options (in 'video=st7565p_fb:<options>' format)
	 */
#ifndef MODULE
	char *option = NULL;

	if (fb_get_options("st7565p_fb", &option))
		return -ENODEV;
	st7565p_fb_setup(option);
#endif
	ret = driver_register(&st7565p_fb_driver);

	if (!ret) {
		ret = platform_device_register(&st7565p_fb_device);
		if (ret)
			driver_unregister(&st7565p_fb_driver);
	}

	return ret;
}

static void __exit st7565p_fb_exit(void)
{
	platform_device_unregister(&st7565p_fb_device);
	driver_unregister(&st7565p_fb_driver);
}


    /*
     *  Setup
     */

/* 
 * Only necessary if your driver takes special options,
 * otherwise we fall back on the generic fb_setup().
 */
int __init st7565p_fb_setup(char *options)
{
    /* Parse user speficied options (`video=st7565p_fb:') */
	return 0;
}




    /*
     *  Modularization
     */

module_init(st7565p_fb_init);
module_exit(st7565p_fb_exit);

MODULE_LICENSE("GPL");
