/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <george.giannaras@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 * This is a framebuffer driver for the ili9325 controller (tested on TIANMA 128160 tft display)
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <asm-cr16/uaccess.h>
#include <asm/regs.h>

#include <linux/kernel.h>
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/cdev.h> 	/*cdev*/
#include <linux/timer.h>
#include <linux/wait.h>
#include <linux/delay.h>	/* delays */ 

#include <linux/string.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/fb.h>

#include "lcd_ili9325_fb.h"


/*Switches*/
//#define TFT_AUTO_REFRESH // checks for changes and refreshes display if needed periodically
/*HW specific definitions*/
/*volatile */unsigned short framebuffer_memory[FRAMEBUFFER_SIZE/2];

///*volatile */unsigned  framebuffer_memory[FRAMEBUFFER_SIZE/4];
#ifdef TFT_AUTO_REFRESH
	/*volatile */unsigned short shadow[FRAMEBUFFER_SIZE/2];

	/***      Timer initializations     ***/
	struct timer_list tft_timer;
	#define	START_TIMER(TIMER, TMOUT) {TIMER.expires = jiffies + TMOUT;add_timer(&TIMER);}
	#define	STOP_TIMER(TIMER) {del_timer(&TIMER);}
	unsigned int refresh_tmout=5;//in 10 ms units
#endif	

/*Tasklets*/
void tft_refresh_tasklet_fn(void);
DECLARE_TASKLET(tft_refresh_tasklet,  tft_refresh_tasklet_fn,  0);

/*Function prototypes*/
//Timers
#ifdef TFT_AUTO_REFRESH
	static void tft_timer_refresh_fn(unsigned long param);
#endif	


static const u32 pseudo_palette[16] = {
		0x000000,
		0xaa0000,
		0x00aa00,
		0xaa5500,
		0x0000aa,
		0xaa00aa,
		0x00aaaa,
		0xaaaaaa,
		0x555555,
		0xff5555,
		0x55ff55,
		0xffff55,
		0x5555ff,
		0xff55ff,
		0x55ffff,
		0xffffff
};


/* 
 * This structure defines the hardware state of the graphics card. Normally
 * you place this in a header file in linux/include/video. This file usually
 * also includes register information. That allows other driver subsystems
 * and userland applications the ability to use the same header file to 
 * avoid duplicate work and easy porting of software. 
 */
struct ili9325_par{
	struct device*	dev ;
	struct fb_info *	info;	
	unsigned char LUT_table[128];	//maybe info->cmap could be used insted ??
	loff_t cur_ppos; //the cur ppos of the device file
};

static struct fb_videomode ili9325_modes[] = {
	{
		.refresh	= 51,
		.xres		= 320,//128,//20,
		.yres		= 240,//160,//20,
		.pixclock	= 156250,  // 6.4 MHz
		.left_margin	= 0/*40*/,		.right_margin	= 0/*20*/,
		.upper_margin	= 0/*19*/,		.lower_margin	= 0/*47*/,
		.hsync_len	= 0,		.vsync_len	= 0,

		.sync		= 0x0, //  0,
		.vmode		= FB_VMODE_NONINTERLACED,
	},
};


static struct fb_monspecs default_monspecs = {
	.manufacturer	= "ILIT",
	.modedb		= ili9325_modes,
	.modedb_len	= ARRAY_SIZE(ili9325_modes),
	.monitor	= "TIANMA 240x320",
	.serial_no	= "xxxx",
	.ascii		= "yyyy",
	.hfmin		= 14820,
	.hfmax		= 22230,
	.vfmin		= 50,
	.vfmax		= 90,
	.dclkmax	= 30000000,
};

/* Driver defaults */
static struct fb_fix_screeninfo ili9325_fb_fix __initdata = {
	.id =		"ili9325_fb", 
	.type =		FB_TYPE_PACKED_PIXELS,
	.visual =	FB_VISUAL_TRUECOLOR,//FB_VISUAL_MONO10,//FB_VISUAL_MONO01,
	.xpanstep =	1,
	.ypanstep =	1,
	.ywrapstep = 0, 
	.accel =	FB_ACCEL_NONE,
#if 1	
	//added for ili9325
	.smem_start= (unsigned long)FRAMEBUFFER_VIRTUAL_MEMORY,	/* Start of frame buffer mem */
					/* (physical address) */
	.smem_len= FRAMEBUFFER_SIZE,			/* Length of frame buffer mem */
	//.type_aux,			/* Interleave for interleaved Planes */

	//.line_length=X_BYTES,		/* length of a line in bytes    */
	.mmio_start= (unsigned long)FRAMEBUFFER_VIRTUAL_MEMORY,	/* Start of Memory Mapped I/O   */
					/* (physical address) */
	.mmio_len= FRAMEBUFFER_SIZE,			/* Length of Memory Mapped I/O  */
					/*  specific chip/card we have	*/
	//reserved[3];		
#endif	
};


    /*
     * 	Modern graphical hardware not only supports pipelines but some 
     *  also support multiple monitors where each display can have its  
     *  its own unique data. In this case each display could be  
     *  represented by a separate framebuffer device thus a separate 
     *  struct fb_info. Now the struct ili9325_par represents the graphics
     *  hardware state thus only one exist per card. In this case the 
     *  struct ili9325_par for each graphics card would be shared between 
     *  every struct fb_info that represents a framebuffer on that card. 
     *  This allows when one display changes it video resolution (info->var) 
     *  the other displays know instantly. Each display can always be
     *  aware of the entire hardware state that affects it because they share
     *  the same ili9325_par struct. The other side of the coin is multiple
     *  graphics cards that pass data around until it is finally displayed
     *  on one monitor. Such examples are the voodoo 1 cards and high end
     *  NUMA graphics servers. For this case we have a bunch of pars, each
     *  one that represents a graphics state, that belong to one struct 
     *  fb_info. Their you would want to have *par point to a array of device
     *  states and have each struct fb_ops function deal with all those 
     *  states. I hope this covers every possible hardware design. If not
     *  feel free to send your ideas at jsimmons@users.sf.net 
     */

    /*
     *  If your driver supports multiple boards or it supports multiple 
     *  framebuffers, you should make these arrays, or allocate them 
     *  dynamically using framebuffer_alloc() and free them with
     *  framebuffer_release().
     */ 
static struct fb_info *info_ptr;

    /* 
     * Each one represents the state of the hardware. Most hardware have
     * just one hardware state. These here represent the default state(s). 
     */
//static struct ili9325_par __initdata current_par;



    /*
     *  Internal routines
     */

#define DMA_CHANNEL_SPACE 0x10//(DMA1_A_STARTL_REG - DMA0_A_STARTL_REG)

static void dma_wait (unsigned char channel) {
	while (GetWord(DMA1_CTRL_REG) & DMA_ON);       // wait until finished
}

static void dma_port_write(unsigned long int src_addr, unsigned long int dest_src, 
                    unsigned int len,           unsigned char data_width, 
                    unsigned char max_burst,    unsigned char channel) {
	
	
	unsigned long int offset = channel * DMA_CHANNEL_SPACE;
	
	SetWord(DMA1_A_STARTL_REG , (src_addr) & 0xFFFF );
	SetWord(DMA1_A_STARTH_REG , ((unsigned long int)(src_addr) >> 16) & 0xFFFF);
	SetWord(DMA1_B_STARTL_REG , (dest_src) & 0xFFFF );
	SetWord(DMA1_B_STARTH_REG , ((unsigned long int)(dest_src) >> 16) & 0xFFFF);

	SetWord(DMA1_LEN_REG , len);
	SetWord(DMA1_INT_REG , 0);
	
	SetWord(DMA1_CTRL_REG , 0x0000);
	SetBits(DMA1_CTRL_REG , MAX_BURST, max_burst);				// 8-bit burst
    SetBits(DMA1_CTRL_REG , AINC,   1);             // increment the source
    SetBits(DMA1_CTRL_REG , BW,     data_width);    // set bus width to 32 bits
    SetBits(DMA1_CTRL_REG , DMA_ON, 1);             // enable the DMA
	SetBits(DMA1_CTRL_REG , DMA_PRIO, 1);             // enable the DMA

}





static unsigned short get_line_length(unsigned short xres_virtual, unsigned short bpp)
{
	unsigned short length=0;

	switch(bpp){
		case 12:
			length = xres_virtual * 3 / 2;//3 bytes for 2 pixels
			break;
		case 16:
			length = xres_virtual * 2;//2 bytes for 1 pixel
			break;		
		case 18:
			length = xres_virtual * 3;//1 bytes for 1 pixel
			break;				
	}
	
	// length = (length + 31) & ~31;//GIAG .. this should be checked
	// length >>= 3;
	//return (128*3);
	return (length);
}

/**
 *	ili9325_fb_open - Optional function. Called when the framebuffer is
 *		     first accessed.
 *	@info: frame buffer structure that represents a single frame buffer
 *	@user: tell us if the userland (value=1) or the console is accessing
 *	       the framebuffer. 
 *
 *	This function is the first function called in the framebuffer api.
 *	Usually you don't need to provide this function. The case where it 
 *	is used is to change from a text mode hardware state to a graphics
 * 	mode state. 
 *
 *	Returns negative errno on error, or zero on success.
 */
static int ili9325_fb_open(struct fb_info *info, int user)
{
	struct ili9325_par *par = info->par;

//	LCD_write_memory_start;
	par->cur_ppos=0;
#ifdef TFT_AUTO_REFRESH
	if(refresh_tmout){
		STOP_TIMER(tft_timer);
		START_TIMER(tft_timer, refresh_tmout);
	}
#endif
    return 0;
}

/*******************************************************
	Description: 	Tasklet called by ioctl or timer for refreshing display.

 ********************************************************/
void tft_refresh_tasklet_fn(void)
{
	int i, j;
	int total_pixels = info_ptr->var.xres_virtual * info_ptr->var.yres_virtual;

	dma_wait(1);

	LCD_wite_ctrl(0x0020, 0x0000);		// GRAM horizontal Address
	LCD_wite_ctrl(0x0021, 0x0000);		// GRAM Vertical Address
	LCD_wite_ctrl(0x0050, 0x0000);		// Horizontal GRAM Start Address
	LCD_wite_ctrl(0x0051, 0x00EF);		// Horizontal GRAM End Address
	LCD_wite_ctrl(0x0052, 0x0000);		// Vertical GRAM Start Address
	LCD_wite_ctrl(0x0053, 0x013F);		// Vertical GRAM End Address

	LCD_write_memory_start;

	dma_port_write(framebuffer_memory, LCD_DR, 38400, 2, 0, 1);

}
/*******************************************************
	Description: 	Refresh timer routine.

 ********************************************************/
#ifdef TFT_AUTO_REFRESH
 static void tft_timer_refresh_fn(unsigned long param){
	 unsigned short *ptr = (unsigned short*)info_ptr->fix.smem_start;
	 //PDEBUG("shadow = %x, ptr = %x , framebuffer_memory = %x \n", shadow, ptr, framebuffer_memory);	 
//	 if(memcmp(ptr,shadow,FRAMEBUFFER_SIZE))
	 {
//		 PDEBUG("Changes in framebuffer \n");	 		 
//		 memcpy(shadow,ptr,FRAMEBUFFER_SIZE/2);
	//	 tasklet_schedule(&tft_refresh_tasklet);
		 tft_refresh_tasklet_fn();
	 }
	 START_TIMER(tft_timer, refresh_tmout);
}
#endif
/**
 *	ili9325_fb_release - Optional function. Called when the framebuffer 
 *			device is closed. 
 *	@info: frame buffer structure that represents a single frame buffer
 *	@user: tell us if the userland (value=1) or the console is accessing
 *	       the framebuffer. 
 *	
 *	Thus function is called when we close /dev/fb or the framebuffer 
 *	console system is released. Usually you don't need this function.
 *	The case where it is usually used is to go from a graphics state
 *	to a text mode state.
 *
 *	Returns negative errno on error, or zero on success.
 */
static int ili9325_fb_release(struct fb_info *info, int user)
{
#ifdef TFT_AUTO_REFRESH	
	if(refresh_tmout){
		STOP_TIMER(tft_timer);
	}
#endif	
    return 0;
}

#if 1
	/*Custom mmap is implemented only for avoiding page alignment performed by fb_mmap()*/
static int ili9325_fb_mmap(struct fb_info *info, 
			    struct vm_area_struct *vma)
	{
	/*Some housekeeping code copied by fb_mmap()*/
		//vma->vm_pgoff = off >> PAGE_SHIFT;
		/* This is an IO map - tell maydump to skip this VMA */
		vma->vm_flags |= VM_IO | VM_RESERVED;
		
		// unsigned long off = 0x1300000;
	//vma->vm_pgoff << PAGE_SHIFT
		// printk(KERN_WARNING "FB giag_debug: vfb_mmap \n" );
		// if (io_remap_pfn_range(vma, vma->vm_start, off >> PAGE_SHIFT,
				     // vma->vm_end - vma->vm_start, vma->vm_page_prot))
			// return -EAGAIN;

			//vma->vm_pgoff=	info->fix.smem_start;
			vma->vm_start = info->fix.smem_start;
		// if (io_remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff,
				     // vma->vm_end - vma->vm_start, vma->vm_page_prot))
			// return -EAGAIN;
	#if 0
		printk(KERN_INFO "lcd_sc14450, in function : %s, vma->vm_start:%lx, vma->vm_pgoff:%lx, \
	vma->vm_end :%lx, vma->vm_page_prot:%lx \n", __FUNCTION__, vma->vm_start, vma->vm_pgoff, \
				     vma->vm_end, vma->vm_page_prot);
	#endif			
		return 0;		
					
		return -EINVAL;
	}
#endif	
/**
 *      ili9325_fb_check_var - Optional function. Validates a var passed in. 
 *      @var: frame buffer variable screen structure
 *      @info: frame buffer structure that represents a single frame buffer 
 *
 *	Checks to see if the hardware supports the state requested by
 *	var passed in. This function does not alter the hardware state!!! 
 *	This means the data stored in struct fb_info and struct ili9325_par do 
 *      not change. This includes the var inside of struct fb_info. 
 *	Do NOT change these. This function can be called on its own if we
 *	intent to only test a mode and not actually set it. The stuff in 
 *	modedb.c is a example of this. If the var passed in is slightly 
 *	off by what the hardware can support then we alter the var PASSED in
 *	to what we can do.
 *
 *      For values that are off, this function must round them _up_ to the
 *      next value that is supported by the hardware.  If the value is
 *      greater than the highest value supported by the hardware, then this
 *      function must return -EINVAL.
 *
 *      Exception to the above rule:  Some drivers have a fixed mode, ie,
 *      the hardware is already set at boot up, and cannot be changed.  In
 *      this case, it is more acceptable that this function just return
 *      a copy of the currently working var (info->var). Better is to not
 *      implement this function, as the upper layer will do the copying
 *      of the current var for you.
 *
 *      Note:  This is the only function where the contents of var can be
 *      freely adjusted after the driver has been registered. If you find
 *      that you have code outside of this function that alters the content
 *      of var, then you are doing something wrong.  Note also that the
 *      contents of info->var must be left untouched at all times after
 *      driver registration.
 *
 *	Returns negative errno on error, or zero on success.
 */
static int ili9325_fb_check_var(struct fb_var_screeninfo *var, struct fb_info *info)
{
	unsigned short line_length;

	/*
	 *  FB_VMODE_CONUPDATE and FB_VMODE_SMOOTH_XPAN are equal!
	 *  as FB_VMODE_SMOOTH_XPAN is only used internally
	 */

	if (var->vmode & FB_VMODE_CONUPDATE) {
		var->vmode |= FB_VMODE_YWRAP;
		var->xoffset = info->var.xoffset;
		var->yoffset = info->var.yoffset;
	}

	/*
	 *  Some very basic checks
	 */
	if (!var->xres)
		var->xres = 1;
	if (!var->yres)
		var->yres = 1;
	if (var->xres > var->xres_virtual)
		var->xres_virtual = var->xres;
	if (var->yres > var->yres_virtual)
		var->yres_virtual = var->yres;
	// if (var->bits_per_pixel <= 1)
		// var->bits_per_pixel = 1;
	if (var->bits_per_pixel <= 12)
		var->bits_per_pixel = 12;
	else if (var->bits_per_pixel <= 16)
		var->bits_per_pixel = 16;
	else if (var->bits_per_pixel <= 18)
		var->bits_per_pixel = 18;
	else{
		return -EINVAL;
	}
	if (var->xres_virtual < var->xoffset + var->xres)
		var->xres_virtual = var->xoffset + var->xres;
	if (var->yres_virtual < var->yoffset + var->yres)
		var->yres_virtual = var->yoffset + var->yres;

	/*
	 *  Memory limit
	 */
	line_length =
	    get_line_length(var->xres_virtual, var->bits_per_pixel);
	//if (line_length * var->yres_virtual > videomemorysize)
	if (line_length * var->yres_virtual > info->fix.smem_len)	{

		return -ENOMEM;
	}
	#if 1
		printk(KERN_INFO "lcd_sc14450, in function : %s, line_length:%lx, var->yres_virtual:%lx, \
			var->xres_virtual :%lx, info->fix.smem_len:%lx, var->bits_per_pixel: %d \n", __FUNCTION__, line_length, var->yres_virtual, \
			     var->xres_virtual, info->fix.smem_len, var->bits_per_pixel);
	#endif	
	/*
	 * Now that we checked it we alter var. The reason being is that the video
	 * mode passed in might not work but slight changes to it might make it 
	 * work. This way we let the user know what is acceptable.
	 */
	switch (var->bits_per_pixel) {
	case 12:
		var->red.offset = 0;
		var->red.length = 4;
		var->green.offset = 4;
		var->green.length = 4;
		var->blue.offset = 8;
		var->blue.length = 4;
		var->transp.offset = 0;
		var->transp.length = 0;
		break;
	case 16:		/* RGB 565 */
		var->red.offset = 0;
		var->red.length = 5;
		var->green.offset = 5;
		var->green.length = 6;
		var->blue.offset = 11;
		var->blue.length = 5;
		var->transp.offset = 0;
		var->transp.length = 0;
		break;
	case 18:		/* RGB 666 */
		var->red.offset = 0;
		var->red.length = 6;
		var->green.offset = 6;
		var->green.length = 6;
		var->blue.offset = 12;
		var->blue.length = 6;
		var->transp.offset = 0;
		var->transp.length = 0;
		break;
	default:
		printk(KERN_INFO "ili9325_fb: color depth %d not supported\n",
		       var->bits_per_pixel);
		return -EINVAL;		
	}
	var->red.msb_right = 0;
	var->green.msb_right = 0;
	var->blue.msb_right = 0;
	var->transp.msb_right = 0;

    return 0;	   	
}

/**
 *      ili9325_fb_set_par - Optional function. Alters the hardware state.
 *      @info: frame buffer structure that represents a single frame buffer
 *
 *	Using the fb_var_screeninfo in fb_info we set the resolution of the
 *	this particular framebuffer. This function alters the par AND the
 *	fb_fix_screeninfo stored in fb_info. It doesn't not alter var in 
 *	fb_info since we are using that data. This means we depend on the
 *	data in var inside fb_info to be supported by the hardware. 
 *
 *      This function is also used to recover/restore the hardware to a
 *      known working state.
 *
 *	ili9325_fb_check_var is always called before ili9325_fb_set_par to ensure that
 *      the contents of var is always valid.
 *
 *	Again if you can't change the resolution you don't need this function.
 *
 *      However, even if your hardware does not support mode changing,
 *      a set_par might be needed to at least initialize the hardware to
 *      a known working state, especially if it came back from another
 *      process that also modifies the same hardware, such as X.
 *
 *      If this is the case, a combination such as the following should work:
 *
 *      static int ili9325_fb_check_var(struct fb_var_screeninfo *var,
 *                                struct fb_info *info)
 *      {
 *              *var = info->var;
 *              return 0;
 *      }
 *
 *      static int ili9325_fb_set_par(struct fb_info *info)
 *      {
 *              init your hardware here
 *      }
 *
 *	Returns negative errno on error, or zero on success.
 */
static int ili9325_fb_set_par(struct fb_info *info)
{
	struct fb_var_screeninfo *var = &info->var;
	/*Set line length*/
	info->fix.line_length = get_line_length(info->var.xres_virtual,
						info->var.bits_per_pixel);
#if 0
	printk(KERN_INFO "In function : %s, bpp: %d \n", __FUNCTION__, var->bits_per_pixel);
#endif
	
	/* Set bpp/ colour mode */
	switch (var->bits_per_pixel){
		case 18:
			info->fix.visual = FB_VISUAL_TRUECOLOR;
//vm todo
			printk("frame buffer wrong pixel format req\n");
//			LCD_set_pixel_format(0x66);//18bpp		
			break;
		case 16:
			info->fix.visual = FB_VISUAL_TRUECOLOR;
//			LCD_set_pixel_format(0x55);//16bpp
			break;
		case 12:
			info->fix.visual = FB_VISUAL_TRUECOLOR;
			printk("frame buffer wrong pixel format req\n");
//			LCD_set_pixel_format(0x63);//12 bpp
			break;
	}
	
#if 0
	unsigned char px_frmt;
	LCD_get_pixel_format(px_frmt);
	printk(KERN_INFO "In function : %s, new pixel format!!!!!!!!!!!!= %x \n", __FUNCTION__, px_frmt);
#endif	
	
    return 0;	
}

/**
 *  	ili9325_fb_setcolreg - Optional function. Sets a color register.
 *      @regno: Which register in the CLUT we are programming 
 *      @red: The red value which can be up to 16 bits wide 
 *	@green: The green value which can be up to 16 bits wide 
 *	@blue:  The blue value which can be up to 16 bits wide.
 *	@transp: If supported, the alpha value which can be up to 16 bits wide.
 *      @info: frame buffer info structure
 * 
 *  	Set a single color register. The values supplied have a 16 bit
 *  	magnitude which needs to be scaled in this function for the hardware. 
 *	Things to take into consideration are how many color registers, if
 *	any, are supported with the current color visual. With truecolor mode
 *	no color palettes are supported. Here a pseudo palette is created
 *	which we store the value in pseudo_palette in struct fb_info. For
 *	pseudocolor mode we have a limited color palette. To deal with this
 *	we can program what color is displayed for a particular pixel value.
 *	DirectColor is similar in that we can program each color field. If
 *	we have a static colormap we don't need to implement this function. 
 * 
 *	Returns negative errno on error, or zero on success.
 */
static int ili9325_fb_setcolreg(unsigned regno, unsigned red, unsigned green,
			   unsigned blue, unsigned transp,
			   const struct fb_info *info)
{
	struct ili9325_par *par = info->par;
	unsigned int v32;
	unsigned char i;
	
    if (regno >= 64)  /* no. of hw registers */
       return -EINVAL;
    /*
     * Program hardware... do anything you want with transp
     */

    /* grayscale works only partially under directcolor */
    if (info->var.grayscale) {
       /* grayscale = 0.30*R + 0.59*G + 0.11*B */
       red = green = blue = (red * 77 + green * 151 + blue * 28) >> 8;
    }

	switch (info->fix.visual) {
		case FB_VISUAL_TRUECOLOR:
		    if (regno >= 16)
			    return -EINVAL;

		    v32 = (red << info->var.red.offset) |
			    (green << info->var.green.offset) |
			    (blue << info->var.blue.offset) |
			    (transp << info->var.transp.offset);

		    ((unsigned int*)(info->pseudo_palette))[regno] = v32;
			return 0;

		case FB_VISUAL_PSEUDOCOLOR:

printk("lcd frame buffer ?????????\n");
			if (regno < 64) {
				if (regno < 32) {//LUT  has 32 inputs for red/blue
					// val  = ((red   >> 11) & 0x001f);
					// val |= ((green >>  6) & 0x03e0);
					// val |= ((blue  >>  1) & 0x7c00);

					/*
					 * TODO: intensity bit. Maybe something like
					 *   ~(red[10] ^ green[10] ^ blue[10]) & 1
					 */
					 /*Update table stored in par*/
					par->LUT_table[regno] =  red >> 10;
					par->LUT_table[regno+32] =  green >> 10;
					par->LUT_table[regno+96] =  blue >> 10;			
				}
				else{//LUT  has 64 inputs for green
					par->LUT_table[regno+32] =  green >> 10;
				}
				/*Write to hw - only 128 bytes can be written */
	//			LCD_write_LUT;
	//			for (i=0;i<128;i++){
	//				LCD_WRITE_DATA(par->LUT_table[i]);
	//			}			
				return 0;
			}
			break;
	}
	

    return -ENOSYS;
}

/**
 *      ili9325_fb_pan_display - NOT a required function. Pans the display.
 *      @var: frame buffer variable screen structure
 *      @info: frame buffer structure that represents a single frame buffer
 *
 *	Pan (or wrap, depending on the `vmode' field) the display using the
 *  	`xoffset' and `yoffset' fields of the `var' structure.
 *  	If the values don't fit, return -EINVAL.
 *
 *      Returns negative errno on error, or zero on success.
 */
// static int ili9325_fb_pan_display(struct fb_var_screeninfo *var,
			     // const struct fb_info *info)
// {
    // /*
     // * If your hardware does not support panning, _do_ _not_ implement this
     // * function. Creating a dummy function will just confuse user apps.
     // */

    // /*
     // * Note that even if this function is fully functional, a setting of
     // * 0 in both xpanstep and ypanstep means that this function will never
     // * get called.
     // */

    // /* ... */
    // return 0;
// }

/**
 *      ili9325_fb_blank - NOT a required function. Blanks the display.
 *      @blank_mode: the blank mode we want. 
 *      @info: frame buffer structure that represents a single frame buffer
 *
 *      Blank the screen if blank_mode != FB_BLANK_UNBLANK, else unblank.
 *      Return 0 if blanking succeeded, != 0 if un-/blanking failed due to
 *      e.g. a video mode which doesn't support it.
 *
 *      Implements VESA suspend and powerdown modes on hardware that supports
 *      disabling hsync/vsync:
 *
 *      FB_BLANK_NORMAL = display is blanked, syncs are on.
 *      FB_BLANK_HSYNC_SUSPEND = hsync off
 *      FB_BLANK_VSYNC_SUSPEND = vsync off
 *      FB_BLANK_POWERDOWN =  hsync and vsync off
 *
 *      If implementing this function, at least support FB_BLANK_UNBLANK.
 *      Return !0 for any modes that are unimplemented.
 *
 */



 static int ili9325_fb_blank(int blank_mode, const struct fb_info *info)
 {
//vm todo
	switch (blank_mode) {
		case FB_BLANK_NORMAL:
//			LCD_enter_sleep_mode;
			return 0;
		case FB_BLANK_UNBLANK:
//			LCD_exit_sleep_mode;
			return 0;			
		default:
			return -EINVAL;
	}   
     return 0;
 }

/* ------------ Accelerated Functions --------------------- */

/*
 * We provide our own functions if we have hardware acceleration
 * or non packed pixel format layouts. If we have no hardware 
 * acceleration, we can use a generic unaccelerated function. If using
 * a pack pixel format just use the functions in cfb_*.c. Each file 
 * has one of the three different accel functions we support.
 */

/**
 *      ili9325_fb_fillrect - REQUIRED function. Can use generic routines if 
 *		 	 non acclerated hardware and packed pixel based.
 *			 Draws a rectangle on the screen.		
 *
 *      @info: frame buffer structure that represents a single frame buffer
 *	@region: The structure representing the rectangular region we 
 *		 wish to draw to.
 *
 *	This drawing operation places/removes a retangle on the screen 
 *	depending on the rastering operation with the value of color which
 *	is in the current color depth format.
 */
// void ili9325_fillrect(struct fb_info *p, const struct fb_fillrect *region)
// {
// /*	Meaning of struct fb_fillrect
 // *
 // *	@dx: The x and y corrdinates of the upper left hand corner of the 
 // *	@dy: area we want to draw to. 
 // *	@width: How wide the rectangle is we want to draw.
 // *	@height: How tall the rectangle is we want to draw.
 // *	@color:	The color to fill in the rectangle with. 
 // *	@rop: The raster operation. We can draw the rectangle with a COPY
 // *	      of XOR which provides erasing effect. 
 // */
// }

/**
 *      ili9325_fb_copyarea - REQUIRED function. Can use generic routines if
 *                       non acclerated hardware and packed pixel based.
 *                       Copies one area of the screen to another area.
 *
 *      @info: frame buffer structure that represents a single frame buffer
 *      @area: Structure providing the data to copy the framebuffer contents
 *	       from one region to another.
 *
 *      This drawing operation copies a rectangular area from one area of the
 *	screen to another area.
 */
// void ili9325_fb_copyarea(struct fb_info *p, const struct fb_copyarea *area) 
// {
// /*
 // *      @dx: The x and y coordinates of the upper left hand corner of the
 // *	@dy: destination area on the screen.
 // *      @width: How wide the rectangle is we want to copy.
 // *      @height: How tall the rectangle is we want to copy.
 // *      @sx: The x and y coordinates of the upper left hand corner of the
 // *      @sy: source area on the screen.
 // */
// }


/**
 *      ili9325_fb_imageblit - REQUIRED function. Can use generic routines if
 *                        non acclerated hardware and packed pixel based.
 *                        Copies a image from system memory to the screen. 
 *
 *      @info: frame buffer structure that represents a single frame buffer
 *	@image:	structure defining the image.
 *
 *      This drawing operation draws a image on the screen. It can be a 
 *	mono image (needed for font handling) or a color image (needed for
 *	tux). 
 */
// void ili9325_fb_imageblit(struct fb_info *p, const struct fb_image *image) 
// {
// /*
 // *      @dx: The x and y coordinates of the upper left hand corner of the
 // *	@dy: destination area to place the image on the screen.
 // *      @width: How wide the image is we want to copy.
 // *      @height: How tall the image is we want to copy.
 // *      @fg_color: For mono bitmap images this is color data for     
 // *      @bg_color: the foreground and background of the image to
 // *		   write directly to the frmaebuffer.
 // *	@depth:	How many bits represent a single pixel for this image.
 // *	@data: The actual data used to construct the image on the display.
 // *	@cmap: The colormap used for color images.   
 // */

// /*
 // * The generic function, cfb_imageblit, expects that the bitmap scanlines are
 // * padded to the next byte.  Most hardware accelerators may require padding to
 // * the next u16 or the next u32.  If that is the case, the driver can specify
 // * this by setting info->pixmap.scan_align = 2 or 4.  See a more
 // * comprehensive description of the pixmap below.
 // */
// }

/**
 *	ili9325_fb_cursor - 	OPTIONAL. If your hardware lacks support
 *			for a cursor, leave this field NULL.
 *
 *      @info: frame buffer structure that represents a single frame buffer
 *	@cursor: structure defining the cursor to draw.
 *
 *      This operation is used to set or alter the properities of the
 *	cursor.
 *
 *	Returns negative errno on error, or zero on success.
 */
// int ili9325_fb_cursor(struct fb_info *info, struct fb_cursor *cursor)
// {
/*
 *      @set: 	Which fields we are altering in struct fb_cursor 
 *	@enable: Disable or enable the cursor 
 *      @rop: 	The bit operation we want to do. 
 *      @mask:  This is the cursor mask bitmap. 
 *      @dest:  A image of the area we are going to display the cursor.
 *		Used internally by the driver.	 
 *      @hot:	The hot spot. 
 *	@image:	The actual data for the cursor image.
 *
 *      NOTES ON FLAGS (cursor->set):
 *
 *      FB_CUR_SETIMAGE - the cursor image has changed (cursor->image.data)
 *      FB_CUR_SETPOS   - the cursor position has changed (cursor->image.dx|dy)
 *      FB_CUR_SETHOT   - the cursor hot spot has changed (cursor->hot.dx|dy)
 *      FB_CUR_SETCMAP  - the cursor colors has changed (cursor->fg_color|bg_color)
 *      FB_CUR_SETSHAPE - the cursor bitmask has changed (cursor->mask)
 *      FB_CUR_SETSIZE  - the cursor size has changed (cursor->width|height)
 *      FB_CUR_SETALL   - everything has changed
 *
 *      NOTES ON ROPs (cursor->rop, Raster Operation)
 *
 *      ROP_XOR         - cursor->image.data XOR cursor->mask
 *      ROP_COPY        - curosr->image.data AND cursor->mask
 *
 *      OTHER NOTES:
 *
 *      - fbcon only supports a 2-color cursor (cursor->image.depth = 1)
 *      - The fb_cursor structure, @cursor, _will_ always contain valid
 *        fields, whether any particular bitfields in cursor->set is set
 *        or not.
 */
// }

/**
 *	ili9325_fb_rotate -  NOT a required function. If your hardware
 *			supports rotation the whole screen then 
 *			you would provide a hook for this. 
 *
 *      @info: frame buffer structure that represents a single frame buffer
 *	@angle: The angle we rotate the screen.   
 *
 *      This operation is used to set or alter the properities of the
 *	cursor.
 */
// void ili9325_fb_rotate(struct fb_info *info, int angle)
// {
// /* Will be deprecated */
// }

/**
 *	ili9325_fb_poll - NOT a required function. The purpose of this
 *		     function is to provide a way for some process
 *		     to wait until a specific hardware event occurs
 *		     for the framebuffer device.
 * 				 
 *      @info: frame buffer structure that represents a single frame buffer
 *	@wait: poll table where we store process that await a event.     
 */
// void ili9325_fb_poll(struct fb_info *info, poll_table *wait)
// {
// }

/**
 *	ili9325_fb_sync - NOT a required function. Normally the accel engine 
 *		     for a graphics card take a specific amount of time.
 *		     Often we have to wait for the accelerator to finish
 *		     its operation before we can write to the framebuffer
 *		     so we can have consistent display output. 
 *
 *      @info: frame buffer structure that represents a single frame buffer
 *
 *      If the driver has implemented its own hardware-based drawing function,
 *      implementing this function is highly recommended.
 */
// void ili9325_fb_sync(struct fb_info *info)
// {
// }	

typedef struct _fbio_area_refresh
{
	short x_start;
	short y_start;
	short x_end;
	short y_end;
	char do_dma;
}fbio_area_refresh;

static int tft_refresh_specific_area_fn(fbio_area_refresh *t_fbio_area_refresh);

static int 
ili9325_fb_ioctl(struct fb_info *info, unsigned int cmd,
	 unsigned long arg)
{
	void __user *argp = (void __user *)arg;
	int res = 0;
	fbio_area_refresh t_fbio_area_refresh;

	switch (cmd) 
	{
		case FBIOREFRESH_DISPLAY:
			tft_refresh_tasklet_fn();
			return 0;
		case FBIO_AREA_REFRESH:
			if(copy_from_user(&t_fbio_area_refresh, (void __user*)arg, sizeof( t_fbio_area_refresh ) ) )
				res = -1;
			else {
				res = tft_refresh_specific_area_fn(&t_fbio_area_refresh);
			}
			break ;
		default:
			return -1;
	}
}


static int tft_refresh_specific_area_fn(fbio_area_refresh *t_fbio_area_refresh)
{
	unsigned int size = 0;

	if (!( 0x13F >= t_fbio_area_refresh->x_start )) goto error; 
	if (!( t_fbio_area_refresh->x_start >= 0 )) goto error; 
	if (!( 0x13F >= t_fbio_area_refresh->x_end )) goto error;
	if (!( t_fbio_area_refresh->x_end >=0 )) goto error;
	if (!( 0xEF >= t_fbio_area_refresh->y_start )) goto error;
	if (!( t_fbio_area_refresh->y_start >=0 )) goto error;
	if (!( 0xEF >= t_fbio_area_refresh->y_end )) goto error;
	if (!( t_fbio_area_refresh->y_end >=0 )) goto error;
	if ((!( t_fbio_area_refresh->x_start <= t_fbio_area_refresh->x_end )) ||
		(!( t_fbio_area_refresh->y_start <= t_fbio_area_refresh->y_end ))) goto error;

	LCD_wite_ctrl(0x0020, t_fbio_area_refresh->y_start);	// GRAM horizontal Address
	LCD_wite_ctrl(0x0021, t_fbio_area_refresh->x_start);	// GRAM Vertical Address
	LCD_wite_ctrl(0x0050, t_fbio_area_refresh->y_start);	// Horizontal GRAM Start Address
	LCD_wite_ctrl(0x0051, t_fbio_area_refresh->y_end);		// Horizontal GRAM End Address
	LCD_wite_ctrl(0x0052, t_fbio_area_refresh->x_start);	// Vertical GRAM Start Address
	LCD_wite_ctrl(0x0053, t_fbio_area_refresh->x_end);		// Vertical GRAM End Address

	if (!t_fbio_area_refresh->do_dma) return 0;

	dma_wait(1);

	size = (t_fbio_area_refresh->x_end - t_fbio_area_refresh->x_start + 1)*(t_fbio_area_refresh->y_end - t_fbio_area_refresh->y_start + 1)/2;

	long new_fb_pos = t_fbio_area_refresh->x_start + t_fbio_area_refresh->y_start*320;

	LCD_write_memory_start;

	dma_port_write(&framebuffer_memory[new_fb_pos], LCD_DR, size, 2, 0, 1);

	return 0;

error:
	printk("[%s] Error: Dimensions given are out of lcd boundries\n", __FUNCTION__, tft_refresh_specific_area_fn);
	return -1;
}


static ssize_t ili9325_fb_write(struct file *file, const char __user *buf, size_t count)
{
	int i;
	unsigned short* ptemp;

	ptemp = (unsigned short*)buf;

	LCD_write_memory_start;

	for(i=0; i<=count/2; i++){
		LCD_WRITE_DATA(ptemp[i]);
	}

	return count;
}

/* ------------------------------------------------------------------------- */

    /*
     *  Frame buffer operations
     */

static struct fb_ops ili9325_fb_ops = {
	.owner		= THIS_MODULE,
	.fb_open	= ili9325_fb_open,
	//.fb_read	= ili9325_fb_read,
	.fb_write	= ili9325_fb_write,
	.fb_release	= ili9325_fb_release,
	.fb_check_var	= ili9325_fb_check_var,
	.fb_set_par	= ili9325_fb_set_par,	
	.fb_setcolreg	= ili9325_fb_setcolreg,
	.fb_blank	= ili9325_fb_blank,
	//.fb_pan_display	= ili9325_fb_pan_display,	
	//.fb_fillrect	= ili9325_fb_fillrect, 	/* Needed !!! */ 
	//.fb_copyarea	= ili9325_fb_copyarea,	/* Needed !!! */ 
	//.fb_imageblit	= ili9325_fb_imageblit,	/* Needed !!! */
	.fb_fillrect	= cfb_fillrect,//giag
	.fb_copyarea	= cfb_copyarea,//giag
	.fb_imageblit	= cfb_imageblit,//giag	
	//.fb_cursor	= ili9325_fb_cursor,		/* Optional !!! */
	//.fb_rotate	= ili9325_fb_rotate,
	//.fb_poll	= ili9325_fb_poll,
	//.fb_sync	= ili9325_fb_sync,
	.fb_ioctl	= ili9325_fb_ioctl,
	.fb_mmap	= ili9325_fb_mmap,	
};
//static __u16 red[16] = {
//    0x0000, 0x0000, 0x0000, 0x0000, 0xaaaa, 0xaaaa, 0xaaaa, 0xaaaa,
//    0x5555, 0x5555, 0x5555, 0x5555, 0xffff, 0xffff, 0xffff, 0xffff
//};
//
//static __u16 green[16] = {
//    0x0000, 0x0000, 0xaaaa, 0xaaaa, 0x0000, 0x0000, 0xaaaa, 0xaaaa,
//    0x5555, 0x5555, 0xffff, 0xffff, 0x5555, 0x5555, 0xffff, 0xffff
//};
//
//static __u16 blue[16] = {
//    0x0000, 0xaaaa, 0x0000, 0xaaaa, 0x0000, 0xaaaa, 0x0000, 0xaaaa,
//    0x5555, 0xffff, 0x5555, 0xffff, 0x5555, 0xffff, 0x5555, 0xffff
//};

    /*
     *  Initialization
     */

void ili9325_fb_initHW(void){
	unsigned char x, y, z, w;
	int i;

	// SetWord(CP_CTRL_REG,0x0003);
//	CP_CTRL_REG|=0x11;//enable PWM control by timer2
//	CP_CTRL_REG|=0x03;//the CP_VOUT1 is able to light up the LCD backlight. 
	//CP_CTRL_REG=0x01;//the CP_VOUT1 is able to light up the LCD backlight. 
	// TIMER_CTRL_REG|=0x80;//enable timer2	
 LCD_wite_ctrl(0x0001, 0x0000); // set SS and SM bit		//ok
 LCD_wite_ctrl(0x0002, 0x0700); // set 1 line inversion	   //ok
 LCD_wite_ctrl(0x0003, 0x10B8); // set GRAM write direction and BGR=1. //
 LCD_wite_ctrl(0x0004, 0x0000); // Resize register
 LCD_wite_ctrl(0x0008, 0x0202); //0202// set the back porch and front porch
 LCD_wite_ctrl(0x0009, 0x0000); // set non-display area refresh cycle ISC[3:0]
 LCD_wite_ctrl(0x000A, 0x0008); // FMARK function
 LCD_wite_ctrl(0x000C, 0x0000); // RGB interface setting
 LCD_wite_ctrl(0x000D, 0x0000); // Frame marker Position
 LCD_wite_ctrl(0x000F, 0x0000); // RGB interface polarity
 //*************Power On sequence ****************//
 LCD_wite_ctrl(0x0010, 0x0000); // SAP, BT[3:0], AP, DSTB, SLP, STB
 LCD_wite_ctrl(0x0011, 0x0007); // DC1[2:0], DC0[2:0], VC[2:0]
 LCD_wite_ctrl(0x0012, 0x0000); // VREG1OUT voltage
 LCD_wite_ctrl(0x0013, 0x0000); // VDV[4:0] for VCOM amplitude

 udelay(50000);
 udelay(50000);
 udelay(50000);
 udelay(50000);
 udelay(50000);
// delayms(200); // Dis-charge capacitor power voltage
 LCD_wite_ctrl(0x0010, 0x1690); // SAP, BT[3:0], AP, DSTB, SLP, STB
 LCD_wite_ctrl(0x0011, 0x0227); // R11h=0x0221 at VCI=3.3V, DC1[2:0], DC0[2:0], VC[2:0]
 udelay(50000);
 //delayms(50); // Delay 50ms
 LCD_wite_ctrl(0x0012, 0x009C); // External reference voltage= Vci;
 //delayms(50); // Delay 50ms
 udelay(50000);
 LCD_wite_ctrl(0x0013, 0x1c00); // R13=1200 when R12=009D;VDV[4:0] for VCOM amplitude
 LCD_wite_ctrl(0x0029, 0x001C); // R29=000C when R12=009D;VCM[5:0] for VCOMH
 LCD_wite_ctrl(0x002B, 0x000d); // Frame Rate = 91Hz
 //delayms(50); // Delay 50ms
 udelay(50000);
 LCD_wite_ctrl(0x0020, 0x0000); // GRAM horizontal Address
 LCD_wite_ctrl(0x0021, 0x0000); // GRAM Vertical Address
 // ----------- Adjust the Gamma Curve ----------//

 LCD_wite_ctrl(0x0030, 0x0007);
 LCD_wite_ctrl(0x0031, 0x0302);
 LCD_wite_ctrl(0x0032, 0x0105);
 LCD_wite_ctrl(0x0035, 0x0206);
 LCD_wite_ctrl(0x0036, 0x0808);
 LCD_wite_ctrl(0x0037, 0x0206);
 LCD_wite_ctrl(0x0038, 0x0504);
 LCD_wite_ctrl(0x0039, 0x0007);
 LCD_wite_ctrl(0x003C, 0x0105);
 LCD_wite_ctrl(0x003D, 0x0808);

 //------------------ Set GRAM area ---------------//
 LCD_wite_ctrl(0x0050, 0x0000); // Horizontal GRAM Start Address
 LCD_wite_ctrl(0x0051, 0x00EF); // Horizontal GRAM End Address
 LCD_wite_ctrl(0x0052, 0x0000); // Vertical GRAM Start Address
 LCD_wite_ctrl(0x0053, 0x013F); // Vertical GRAM Start Address
 LCD_wite_ctrl(0x0060, 0xA700); // Gate Scan Line
 LCD_wite_ctrl(0x0061, 0x0001); //1 // NDL,VLE, REV
 LCD_wite_ctrl(0x006A, 0x0000); // set scrolling line
 //-------------- Partial Display Control ---------//
 LCD_wite_ctrl(0x0080, 0x0000);
 LCD_wite_ctrl(0x0081, 0x0000);
 LCD_wite_ctrl(0x0082, 0x0000);
 LCD_wite_ctrl(0x0083, 0x0000);
 LCD_wite_ctrl(0x0084, 0x0000);
 LCD_wite_ctrl(0x0085, 0x0000);
 //-------------- Panel Control -------------------//
 LCD_wite_ctrl(0x0090, 0x0010);
 LCD_wite_ctrl(0x0092, 0x0000);
 LCD_wite_ctrl(0x0093, 0x0003);
 LCD_wite_ctrl(0x0095, 0x0110);
 LCD_wite_ctrl(0x0097, 0x0000);
 LCD_wite_ctrl(0x0098, 0x0000);
 LCD_wite_ctrl(0x0007, 0x0133); // 262K color and display ON

// P1_02_MODE_REG = 0x300;
}


static void __init fetch_hw_state(struct fb_info *info, struct ili9325_par *par)
{
// this function could fully replace the ili9325_fb_fix initialization
	struct fb_var_screeninfo *var = &info->var;
	struct fb_fix_screeninfo *fix = &info->fix;
	u8 d1, d2, d3, d4;//to store display information


	fix->type = FB_TYPE_PACKED_PIXELS;


			/* 5-6-5 RGB */
			fix->visual = FB_VISUAL_PSEUDOCOLOR;
			var->bits_per_pixel = 16;
			
			var->red.offset = 11;
			var->red.length = 5;
			var->green.offset = 5;
			var->green.length = 6;
			var->blue.offset = 0;
			var->blue.length = 5;
			
			var->transp.offset = 0;
			var->transp.length = 0;			
	

#if 0
	LCD_read_display_status(d1, d2, d3, d4);
	switch (LCD_read_display_status_MASK_IFPF(d1, d2, d3, d4)) {//bpp
		case LCD_IFPF0://12-bit/pixel 
			/* 4-4-4 RGB */
			fix->visual = FB_VISUAL_PSEUDOCOLOR;	
			var->bits_per_pixel = 12;
			
			var->red.offset = 0;
			var->red.length = 4;
			var->green.offset = 4;
			var->green.length = 4;
			var->blue.offset = 8;
			var->blue.length = 4;
			
			var->transp.offset = 0;
			var->transp.length = 0;	
		break;
		case LCD_IFPF1://16-bit/pixel
			/* 5-6-5 RGB */
			fix->visual = FB_VISUAL_PSEUDOCOLOR;
			var->bits_per_pixel = 16;
			
			var->red.offset = 11;
			var->red.length = 5;
			var->green.offset = 5;
			var->green.length = 6;
			var->blue.offset = 0;
			var->blue.length = 5;
			
			var->transp.offset = 0;
			var->transp.length = 0;			
			break;

		case LCD_IFPF2://18-bit/pixel
			/* RGB 666 */
			fix->visual = FB_VISUAL_TRUECOLOR;
			var->bits_per_pixel = 18;
			
			var->red.offset = 0;
			var->red.length = 6;
			var->green.offset = 6;
			var->green.length = 6;
			var->blue.offset = 12;
			var->blue.length = 6;
			
			var->transp.offset = 0;
			var->transp.length = 0;
			break;		
		default:
			BUG();
	}
#endif
}
static int __init ili9325_fb_probe (struct platform_device *device)
{
    struct fb_info *info;
    struct ili9325_par *par;
    int cmap_len, retval;	
    int res = 0 ;
	char *mode_option=NULL;
    /*
     * Dynamically allocate info and par
     */
	 
    info = framebuffer_alloc(sizeof(struct ili9325_par), device);

    if (!info) {
		printk(KERN_INFO "fb_ili9325: failed to allocate memory\n");
	    /* goto error path */
		goto framebuffer_rel;
    }
    else{
    	info_ptr = info; //store pointer
    }
	/*Initialize hw*/
	ili9325_fb_initHW();
	
    par = info->par;
	par->info = info ;	
	par->dev=device;// store device pointer
	par->cur_ppos=0;
	memset(par->LUT_table, 128, 0);//initialize LUT

    /* 
     * Here we set the screen_base to the virtual memory address
     * for the framebuffer. Usually we obtain the resource address
     * from the bus layer and then translate it to virtual memory
     * space via ioremap. Consult ioport.h. 
     */
	memcpy( &info->monspecs, &default_monspecs, sizeof(info->monspecs) ) ;//giag
    info->screen_base = (unsigned long)FRAMEBUFFER_VIRTUAL_MEMORY;
    info->fbops = &ili9325_fb_ops;
    info->fix = ili9325_fb_fix; /* this will be the only time ili9325_fb_fix will be
			    * used, so mark it as __initdata
			    */

    info->pseudo_palette = pseudo_palette; /* The pseudopalette is an 16-member array   */
	
    /*
     * Set up flags to indicate what sort of acceleration your
     * driver can provide (pan/wrap/copyarea/etc.) and whether it
     * is a module -- see FBINFO_* in include/linux/fb.h
     *
     * If your hardware can support any of the hardware accelerated functions
     * fbcon performance will improve if info->flags is set properly.
     *
     * FBINFO_HWACCEL_COPYAREA - hardware moves
     * FBINFO_HWACCEL_FILLRECT - hardware fills
     * FBINFO_HWACCEL_IMAGEBLIT - hardware mono->color expansion
     * FBINFO_HWACCEL_YPAN - hardware can pan display in y-axis
     * FBINFO_HWACCEL_YWRAP - hardware can wrap display in y-axis
     * FBINFO_HWACCEL_DISABLED - supports hardware accels, but disabled
     * FBINFO_READS_FAST - if set, prefer moves over mono->color expansion
     * FBINFO_MISC_TILEBLITTING - hardware can do tile blits
     *
     * NOTE: These are for fbcon use only.
     */
    info->flags = FBINFO_DEFAULT;

/********************* This stage is optional ******************************/
#ifdef PIXMAP_ON
     /*
     * The struct pixmap is a scratch pad for the drawing functions. This
     * is where the monochrome bitmap is constructed by the higher layers
     * and then passed to the accelerator.  For drivers that uses
     * cfb_imageblit, you can skip this part.  For those that have a more
     * rigorous requirement, this stage is needed
     */

    /* PIXMAP_SIZE should be small enough to optimize drawing, but not
     * large enough that memory is wasted.  A safe size is
     * (max_xres * max_font_height/8). max_xres is driver dependent,
     * max_font_height is 32.
     */
	 
    info->pixmap.addr = kmalloc(PIXMAP_SIZE, GFP_KERNEL);
    if (!info->pixmap.addr) {
	    /* goto error */
    }

    info->pixmap.size = PIXMAP_SIZE;

    /*
     * FB_PIXMAP_SYSTEM - memory is in system ram
     * FB_PIXMAP_IO     - memory is iomapped
     * FB_PIXMAP_SYNC   - if set, will call fb_sync() per access to pixmap,
     *                    usually if FB_PIXMAP_IO is set.
     *
     * Currently, FB_PIXMAP_IO is unimplemented.
     */
    info->pixmap.flags = FB_PIXMAP_SYSTEM;

    /*
     * scan_align is the number of padding for each scanline.  It is in bytes.
     * Thus for accelerators that need padding to the next u32, put 4 here.
     */
    info->pixmap.scan_align = 4;

    /*
     * buf_align is the amount to be padded for the buffer. For example,
     * the i810fb needs a scan_align of 2 but expects it to be fed with
     * dwords, so a buf_align = 4 is required.
     */
    info->pixmap.buf_align = 4;

    /* access_align is how many bits can be accessed from the framebuffer
     * ie. some epson cards allow 16-bit access only.  Most drivers will
     * be safe with u32 here.
     *
     * NOTE: This field is currently unused.
     */
    info->pixmap.scan_align = 32
#endif	
/***************************** End optional stage ***************************/
#if 1
	//fetch_hw_state(info, par);//read controller settings
    /*
     * This should give a reasonable default video mode. The following is
     * done when we can set a video mode. 
     */
    if (!mode_option) 	
//	mode_option = "128x160-18@51";
    mode_option = "128x160-16@51";	    	

    //retval = fb_find_mode(&info->var, info, mode_option, NULL, 0, NULL, 8);
	retval = fb_find_mode( &info->var, info, mode_option/*NULL*/, info->monspecs.modedb,
			    info->monspecs.modedb_len, info->monspecs.modedb,
			    16 ) ;
//			    18 ) ;  			    
    if (!retval || retval == 4){
		printk(KERN_ERR "ili9325_fb: No suitable video mode found\n" ) ;
		res = -EINVAL;
		goto framebuffer_rel ;
	}
#endif
    /* This has to been done !!! */	
	cmap_len=128;
    if(fb_alloc_cmap(&info->cmap, cmap_len, 0)){
		printk(KERN_ERR "ili9325_fb: Could not allocate color map\n" ) ;
		res = -ENOMEM ;
		goto free_cmap ;
	}
	
    /* 
     * The following is done in the case of having hardware with a static 
     * mode. If we are setting the mode ourselves we don't call this. 
     */	
   // info->var = ili9325_fb_var; giag

    /*
     * For drivers that can...
     */
    ili9325_fb_check_var(&info->var, info);
	//info->fix.line_length = get_line_length(info->var.xres_virtual,
					//	info->var.bits_per_pixel);
    /*
     * Does a call to fb_set_par() before register_framebuffer needed?  This
     * will depend on you and the hardware.  If you are sure that your driver
     * is the only device in the system, a call to fb_set_par() is safe.
     *
     * Hardware in x86 systems has a VGA core.  Calling set_par() at this
     * point will corrupt the VGA console, so it might be safer to skip a
     * call to set_par here and just allow fbcon to do it for you.
     */
    ili9325_fb_set_par(info);

    if (register_framebuffer(info) < 0){
		goto free_cmap;
		res = -EINVAL;
	}

    printk(KERN_INFO "fb%d: %s frame buffer device\n", info->node,
	   info->fix.id);
    platform_set_drvdata(device, info) ;
	printk(KERN_INFO
	       "fb%d: sc14450 ili9325 frame buffer device, using %ld of video memory, starting at address %lx\n",
	       info->node, info->fix.smem_len, info->fix.smem_start);
	printk(KERN_INFO
	       "xres=%d, yres=%d, bppp=%d \n",
	       info->var.xres, info->var.yres, info->var.bits_per_pixel);
	
	return 0;
free_cmap:
	fb_dealloc_cmap( &info->cmap ) ;	
framebuffer_rel:
	framebuffer_release( info ) ;
    return res ;
}

    /*
     *  Cleanup
     */
static void __exit ili9325_fb_remove(struct platform_device *device) 
{
	struct fb_info *info = platform_get_drvdata(device);

	if (info) {
		unregister_framebuffer(info);
		fb_dealloc_cmap(&info->cmap);
		/* ... */
		framebuffer_release(info);
	}

}



/* for platform devices */
static struct device_driver ili9325_fb_driver = {
	.name = "ili9325_fb",
	.bus  = &platform_bus_type,
	.probe = ili9325_fb_probe,
	.remove = ili9325_fb_remove,
	// .suspend = ili9325_fb_suspend, /* optional */
	// .resume = ili9325_fb_resume,   /* optional */
};

static struct platform_device ili9325_fb_device = {
	.name = "ili9325_fb",
};

    /*
     *  Setup
     */

/*
 * Only necessary if your driver takes special options,
 * otherwise we fall back on the generic fb_setup().
 */
int __init ili9325_fb_setup(char *options)
{
    /* Parse user speficied options (`video=ili9325_fb:') */
	return 0;
}
static int __init ili9325_fb_init(void)
{
	int ret;
	/*
	 *  For kernel boot options (in 'video=ili9325_fb:<options>' format)
	 */
#ifndef MODULE
	char *option = NULL;

	if (fb_get_options("ili9325_fb", &option))
		return -ENODEV;
	ili9325_fb_setup(option);
#endif
	ret = driver_register(&ili9325_fb_driver);

	if (!ret) {
#ifdef TFT_AUTO_REFRESH			
		if(refresh_tmout){
			init_timer(&tft_timer);
			tft_timer.data = 0; //no argument for timer_fn needed
			tft_timer.function = tft_timer_refresh_fn;
		}
#endif			
		ret = platform_device_register(&ili9325_fb_device);	
		if (ret){
			driver_unregister(&ili9325_fb_driver);
		}
	}

	return ret;
}

static void __exit ili9325_fb_exit(void)
{
	platform_device_unregister(&ili9325_fb_device);
	driver_unregister(&ili9325_fb_driver);
}







    /*
     *  Modularization
     */

module_init(ili9325_fb_init);
module_exit(ili9325_fb_exit);

MODULE_LICENSE("GPL");
