/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <george.giannaras@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

// This is the header file for the sc1445x lcd driver of the ILITEK 9163ds lcd tft controller.
// 			Supported displays:
// LCD:  TIANMA TM128160F4NFWGWC (128x160 lcd tft display) ---- using lcd driver ILITEK 9163ds

/* 
	Driver interface description
	LCD		BOARD
	---------------------------
	RS		->        AD7
	DB[0..7]	->	DAB[0..7]
*/
#ifndef LCD_ILI9163_H 
#define LCD_ILI9163_H 
/***************       Definitions section             ***************/
/* Registers */
//Since RS -> AD7, data are accessed at 0x80 and instructions are accessed at 0x00
#define LCD_BASE 0x01300000
#define LCD_DR (LCD_BASE+0x80)
#define LCD_IR (LCD_BASE+0x00)

/* HW dependent definitions */
#define FRAMEBUFFER_VIRTUAL_MEMORY framebuffer_memory //LCD_DR //This address should point to a non-cachable memory space where the lcd memory is mapped.
#define FRAMEBUFFER_SIZE 61440 //(128*160*3)//48063//(132*18*162/8)  
//#define FRAMEBUFFER_MAX_WR 0x80 //this is the max allowed copy to the screen_base (because in 0x80 it performes read)  
//Kernel MACROs
#define LCD_WRITE_INST(x)	{(*(volatile unsigned char *)(LCD_IR)=(x));/*;udelay(10);*/}
#define LCD_WRITE_DATA(x)	{(*(volatile unsigned char *)(LCD_DR)=(x));/*udelay(10);*/}
#define LCD_READ_DATA(x)	{((x)=*(volatile unsigned char *)(LCD_DR));/*udelay(10);*/}
#define LCD_DUMMY_READ_DATA LCD_READ_DATA //just for code readability
//New IOCTL - should be added in fb.h
// #define FBIOREFRESH_DISPLAY        0x4619

/* Commands of the ILI9163 LCD controller. The comments above each definition provide:
//Short command description												||	Implementation requrirement (Y=yes, N=No, O=Optional)
  */
//No Operation														||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_nop		 				{LCD_WRITE_INST(0x00);}
//Software reset														||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_soft_reset 				{LCD_WRITE_INST(0x01);}
//Get red channel														||	(Type1 = N, Type2 = Y, Type3 = Y)
#define LCD_get_red_channel(X)		{LCD_WRITE_INST(0x06);LCD_READ_DATA(X);}
//Get green channel													||	(Type1 = N, Type2 = Y, Type3 = Y)
#define LCD_get_green_channel(X)	{LCD_WRITE_INST(0x07);LCD_READ_DATA(X);}
//Get blue channel													||	(Type1 = N, Type2 = Y, Type3 = Y)
#define LCD_get_blue_channel(X) 	{LCD_WRITE_INST(0x08);LCD_READ_DATA(X);}
//Get the current pixel format												||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_get_pixel_format(X)	{LCD_WRITE_INST(0x0C);LCD_DUMMY_READ_DATA(X);LCD_READ_DATA(X);}
//Get the current power mode												||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_get_power_mode(X)		{LCD_WRITE_INST(0x0A);LCD_DUMMY_READ_DATA(X);LCD_READ_DATA(X);}
//Get the frame memory to the display panel read order								||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_get_address_mode(X)	{LCD_WRITE_INST(0x0B);LCD_READ_DATA(X);}
//Get the current display mode from the peripheral								||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_get_display_mode(X)	{LCD_WRITE_INST(0x0D);LCD_READ_DATA(X);}
//Get display module signaling mode 											||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_get_signal_mode(X)		{LCD_WRITE_INST(0x0E);LCD_READ_DATA(X);}
//Get Peripheral Self-Diagnostic Result										||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_get_diagnostic_result(X)		{LCD_WRITE_INST(0x0F);LCD_READ_DATA(X);}
//Power for the display panel is off											||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_enter_sleep_mode		{LCD_WRITE_INST(0x10);}
//Power for the display panel is on											||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_exit_sleep_mode			{LCD_WRITE_INST(0x11);}
//Part of the display area is used for image display								||	(Type1 = Y, Type2 = Y, Type3 = N)
#define LCD_enter_partial_mode		{LCD_WRITE_INST(0x12);}
//Part of the display area is used for image display								||	(Type1 = Y, Type2 = Y, Type3 = N)
#define LCD_enter_normal_mode		{LCD_WRITE_INST(0x13);}
//Displayed image colors are not inverted										||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_exit_invert_mode		{LCD_WRITE_INST(0x20);}
//Part of the display area is used for image display								||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_enter_invert_mode		{LCD_WRITE_INST(0x21);}
//Selects the gamma curve used by the display device								||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_set_gamma_curve(X)		{LCD_WRITE_INST(0x26);LCD_WRITE_DATA(X);}
//Blanks the display device												||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_set_display_off 		{LCD_WRITE_INST(0x28);}
//Show the image on the display device										||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_set_display_on 			{LCD_WRITE_INST(0x29);}
//Set the column extent													||	(Type1 = Y, Type2 = Y, Type3 = N)
#define LCD_set_column_address(X, Y, Z, W)		{LCD_WRITE_INST(0x2A);LCD_WRITE_DATA(X);LCD_WRITE_DATA(Y);LCD_WRITE_DATA(Z);LCD_WRITE_DATA(W);}
//Set the page extent													||	(Type1 = Y, Type2 = Y, Type3 = N)
#define LCD_set_page_address(X, Y, Z, W)		{LCD_WRITE_INST(0x2B);LCD_WRITE_DATA(X);LCD_WRITE_DATA(Y);LCD_WRITE_DATA(Z);LCD_WRITE_DATA(W);}
//Transfer image from the host processor to the peripheral 							||	(Type1 = Y, Type2 = Y, Type3 = N)
#define LCD_write_memory_start					{LCD_WRITE_INST(0x2C);} //variable data writes should follow this command
//Fills the peripheral LUT with data 											||	(Type1 = O, Type2 = N, Type3 = N)
#define LCD_write_LUT							{LCD_WRITE_INST(0x2D);} //variable data writes should follow this command
//Transfer image from the peripheral to the host processor 							||	(Type1 = Y, Type2 = Y, Type3 = N)
#define LCD_read_memory_start					{LCD_WRITE_INST(0x2E);} //variable data reads should follow this command
//Defines the partial display area on the display device								||	(Type1 = Y, Type2 = Y, Type3 = N)
#define LCD_set_partial_area(X, Y, Z, W)		{LCD_WRITE_INST(0x30);LCD_WRITE_DATA(X);LCD_WRITE_DATA(Y);LCD_WRITE_DATA(Z);LCD_WRITE_DATA(W);}
//Defines the vertical scrolling and fixed area on the display device					||	(Type1 = Y, Type2 = N, Type3 = N)
#define LCD_set_scroll_area(X1, X2, X3, X4, X5, X6)	{LCD_WRITE_INST(0x33);LCD_WRITE_DATA(X1);LCD_WRITE_DATA(X2);LCD_WRITE_DATA(X3);LCD_WRITE_DATA(X4);LCD_WRITE_DATA(X5);LCD_WRITE_DATA(X6);}
//Sync information is not sent by the display module to the host processor					||	(Type1 = Y, Type2 = N, Type3 = N)
#define LCD_tear_off 							{LCD_WRITE_INST(0x34);}
//Sync information is sent by the display module to the host processor at the VFP start			||	(Type1 = Y, Type2 = N, Type3 = N)
#define LCD_tear_on(X)							{LCD_WRITE_INST(0x35);LCD_WRITE_DATA(X);}
//Set the read order from frame memory to the display panel							||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_set_address_mode(X)	{LCD_WRITE_INST(0x36);LCD_WRITE_DATA(X);}
#define LCD_set_address_mode_BITS(MY, MX, MV, ML, RGB, MH)	LCD_set_address_mode((MY<<7)|(MX<<6)|(MV<<5)|(ML<<4)|(RGB<<3)|(MH<<2))
//Defines the verstical scrolling starting point									||	(Type1 = Y, Type2 = N, Type3 = N)
#define LCD_set_scroll_start(X, Y)				{LCD_WRITE_INST(0x37);LCD_WRITE_DATA(X);LCD_WRITE_DATA(Y);}
//Full color depth is used on the display panel									||	(Type1 = Y, Type2 = N, Type3 = N)
#define LCD_exit_idle_mode 							{LCD_WRITE_INST(0x38);}
//Reduced color depth is used on the display panel								||	(Type1 = Y, Type2 = N, Type3 = N)
#define LCD_enter_idle_mode 						{LCD_WRITE_INST(0x39);}
//Defines bits per pixel ratio												||	(Type1 = Y, Type2 = Y, Type3 = Y)
#define LCD_set_pixel_format(X)					{LCD_WRITE_INST(0x3A);LCD_WRITE_DATA(X);}
//Transfer image data from host to peripheral from the last written location				||	(Type1 = Y, Type2 = Y, Type3 = N)
#define LCD_write_memory_continue 					{LCD_WRITE_INST(0x3C);}//variable data writes should follow this command
//Read image data from peripheral to host continuing after the last read_mem_cont or read_mem_start		||	(Type1 = Y, Type2 = Y, Type3 = N)
#define LCD_read_memory_continue 					{LCD_WRITE_INST(0x3E);}//variable data reads should follow this command
//Sync info is send when the refresh reaches the provided scanline						||	(Type1 = Y, Type2 = Y, Type3 = N)
#define LCD_set_tear_scan_line(X, Y) 				{LCD_WRITE_INST(0x44);LCD_WRITE_DATA(X);LCD_WRITE_DATA(Y);}
//Get thecurrent scanline												||	(Type1 = Y, Type2 = Y, Type3 = N)
#define LCD_get_scan_line(X, Y) 					{LCD_WRITE_INST(0x44);LCD_READ_DATA(X);LCD_READ_DATA(Y);}
//Read ID1															||	(Type1 = ?, Type2 = ?, Type3 = ? )
#define LCD_read_id1(X) 							{LCD_WRITE_INST(0xDA);LCD_READ_DATA(X)}
//Read ID2															||	(Type1 = ?, Type2 = ?, Type3 = ? )
#define LCD_read_id2(X) 							{LCD_WRITE_INST(0xDB);LCD_READ_DATA(X)}
//Read ID3															||	(Type1 = ?, Type2 = ?, Type3 = ? )
#define LCD_read_id3(X) 							{LCD_WRITE_INST(0xDC);LCD_READ_DATA(X)}

/*Commands out of the command table*/
//Read display identification information
#define LCD_read_display_id_info(X, Y, Z)		{LCD_WRITE_INST(0x04);LCD_DUMMY_READ_DATA(X);LCD_READ_DATA(X);LCD_READ_DATA(Y);LCD_READ_DATA(Z);}
//Read display status
#define LCD_read_display_status(X, Y, Z, W)		{LCD_WRITE_INST(0x09);LCD_DUMMY_READ_DATA(X);LCD_READ_DATA(X);LCD_READ_DATA(Y);LCD_READ_DATA(Z);LCD_READ_DATA(W);}



/*Command Bit fields*/
//RGB Interface
#define LCD_IFPF0 0x03 //12-bit/pixel
#define LCD_IFPF1 0x05 //16-bit/pixel
#define LCD_IFPF2 0x06 //18-bit/pixel
//Address mode



//LCD_read_display_status
#define LCD_read_display_status_MASK_IFPF(D1, D2, D3, D4) ((D2>>4) & 0x07)
//LCD_set_address_mode
// #define LCD_set_address_mode_MASK_(X)		



//Display parameters
#define LCD_TFS 0 // TOP_FIXED_AREA (in No. of lines from Top of the Frame Memory and Display).
#define LCD_VSA 160 //VERTICAL_SCROLLING_AREA(in No. of lines of the Frame Memory [not the display] from the Vertical Scrolling Start Address).
#define LCD_BFA 0 //BOTTOM_FIXED_AREA(in No. of lines from Bottom of the Frame Memory and Display)

//Kernel MACROs
//#define LCD_WRITE_INST(x)	(*(volatile unsigned char *)(LCD_IR)=(x))
#define BUSY_FLAG			(1<<7)
// #define lcd_out_ctl(X)   {LCD_WRITE_INST(X);}
#define lcd_out_dat(X)   {LCD_WRITE_DATA(X);}
// #define lcd_out_ctl(X)   {spin_lock_irq(&lcd_dev.lock);LCD_WRITE_INST(X);spin_unlock_irq(&lcd_dev.lock);}
// #define lcd_out_dat(X)   {spin_lock_irq(&lcd_dev.lock);LCD_WRITE_DATA(X);spin_unlock_irq(&lcd_dev.lock);}
/* LCD screen and bitmap image array consants */
#define X_BYTES				192
#define Y_BYTES	      		8


#define COL_MIN				32//0x04
#define COL_MAX				223//0x83

#define SCRN_TOP			0
#define SCRN_BOTTOM			63
#define SCRN_LEFT			0//0x04

#define SCRN_RIGHT			191//0x83


#define PAGE_MIN			0
#define PAGE_MAX			(SCRN_BOTTOM/8)

//Processor specific commands
#define LCD_BACKLIGHT_LEVEL(X)  (TIMER2_DUTY_REG=X|X<<8)

/***      Data types definitions       ***/
struct lcd_sc14450_conf_struct{	
	// unsigned short flags;	
	unsigned char refresh_tmout;	
} ;


//IOCTL Commands
typedef enum IOCTL_Cmd_enum {
	LCD_init,//Initializes hw and driver internal state
	LCD_out_ctl,//Issues a LCD cmd directly from the user space
	LCD_clear_ram,//CLears LCD ram
	LCD_clear_rect,//Clears a rectangle defined by left/top /right/bottom coordinates in the LCD ram
	LCD_invert_rect,//Inverts a rectangle defined by left/top /right/bottom coordinates in the LCD ram
	LCD_draw_border,//Draws a border defined by left/top /right/bottom coordinates in the LCD ram
	LCD_clear_border,//Clear a border defined by left/top /right/bottom coordinates in the LCD ram
	LCD_text,//Write a string of characters using a specified font, starting at location defined by left, top coordinates in the LCD ram
	LCD_update,//Updates the LCD display with the contents of the LCD ram
	LCD_standby_enter,//Enter standby mode
	LCD_standby_exit,//Exit standby mode
	LCD_sleepmode_enter,//Enter sleep mode
	LCD_sleepmode_exit,//Exit sleep mode
	LCD_backlight_level,//Set backlight brightness level
	LCD_contrast_level,//Set backlight contrast level
	LCD_refresh_tmout,//Set LCD refresh timeout
	LCD_IOCTL_MAX//this is a delimiter
} IOCTL_Cmd;
 

/****************************************************************************/
/**************           HW INDEPENDENT DEFINITIONS                    ***************/
/****************************************************************************/
/*
 * Macros to help debugging
 */
#define LCD_SC14450_DEBUG
//#undef PDEBUG             /* undef it, just in case */
#ifdef LCD_SC14450_DEBUG
#  ifdef __KERNEL__
     /* This one if debugging is on, and kernel space */
// #    define PDEBUG(fmt, args...) printk( KERN_DEBUG "lcd_sc14450: " fmt, ## args)
#    define PDEBUG(fmt, args...) printk( KERN_WARNING "lcd_sc14450: " fmt, ## args)
#  else
     /* This one for user space */
#    define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif

#undef PDEBUGG
#define PDEBUGG(fmt, args...) /* nothing: it's a placeholder */

#define LCD_SC14450_MAJOR 251
#ifndef LCD_SC14450_MAJOR
#define LCD_SC14450_MAJOR 0   /* dynamic major by default */
#endif

#ifndef LCD_SC14450_NR_DEVS
#define LCD_SC14450_NR_DEVS 1    
#endif


#endif
