/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <george.giannaras@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

// This is the header file for the experimental lcd driver for F_G2  board
// 			Supported displays:
// LCD:  NAN YA LMG77 (64x192 dotmatrix display) ---- using lcd driver IST3020

/* 
	Driver interface description
	LCD		BOARD
	---------------------------
	RS		->        AD7
	DB[0..7]	->	DAB[0..7]
*/
#ifndef LCD_IST3020_H 
#define LCD_IST3020_H 
/***************       Definitions section             ***************/
/* HW dependent definitions */
//#define	AUTO_REFRESH // uncomment this definition to enable the automatic refresh of the LCD display (It is automatically defined when framebuffer support is enabled)

/* Registers */
#if defined(CONFIG_JW_BOARDS)
	#ifdef CONFIG_JW_V1_BOARD
//Since RS -> AD11, data are accessed at 0x800 and instructions are accessed at 0x000
	#define LCD_BASE 0x01300000
	#define LCD_DR (LCD_BASE+0x800)//(LCD_BASE+0x80)
	#define LCD_IR (LCD_BASE+0x000)
	#endif
#else	
//Since RS -> AD7, data are accessed at 0x80 and instructions are accessed at 0x00
	#define LCD_BASE 0x01300000
	#define LCD_DR (LCD_BASE+0x80)//(LCD_BASE+0x80)
	#define LCD_IR (LCD_BASE+0x00)
#endif
/* commands of the ist3020 LCD controller */
//CMD 1
#define LCD_DISP_OFF 		0xAE	/* turn LCD display OFF */
#define LCD_DISP_ON			0xAF	/* turn LCD display ON */
//CMD 2
#define LCD_SET_LINE		0x40	/* set the display RAM start line address(6 lsbs = ST5:ST4:ST3:ST2:ST1:ST0) */
//CMD 3
#define LCD_SET_PAGE		0xB0	/* set the display RAM page address (4 lsbs = P3:P2:P1:P0) */
//CMD 4
#define LCD_SET_COL_HI		0x10	/* set column address (4 lsbs = Y7:Y6:Y5:Y4) */
#define LCD_SET_COL_LO		0x00	/* set column address (4 lsbs = Y3:Y2:Y1:Y0) */
//CMD 5
#define LCD_READ_STATUS(x)  (x=*(volatile unsigned char *)(LCD_IR)) /* read the status data (4 msbs = RS7:RS6:RS5:RS4)*/
//CMD 6
#define LCD_WRITE_DATA(x)	(*(volatile unsigned char *)(LCD_DR)=(x)) /* write data to the display RAM (one byte)*/
//CMD 7
#define LCD_READ_DATA(x)	(x=*(volatile unsigned char *)(LCD_DR)) /* read data from the display RAM (one byte)*/
//CMD 8
#define LCD_SET_ADC_NOR		0xA0	/* set the display RAM addr SEG output for normal direction */
#define LCD_SET_ADC_REV		0xA1	/* set the display RAM addr SEG output for reverse direction */
//CMD 9
#define LCD_DISP_NOR		0xA6	/* set normal pixel display */
#define LCD_DISP_REV		0xA7	/* set reverse pixel display */
//CMD 10
#define LCD_EON_OFF			0xA4	/* set normal display mode */
#define LCD_EON_ON			0xA5	/* set entire dsplay on */
//CMD 11
#define LCD_SET_BIAS_1DIV9	0xA2	/* set LCD driving voltage bias to 1 div 9 */
#define LCD_SET_BIAS_1DIV7	0xA3	/* set LCD driving voltage bias to 1 div 7 */
//CMD 12
#define LCD_SET_MODIFY		0xE0	/* enter the "read-modify-write" mode, col addr will incr in each write disp data and decr in each read disp data */
//CMD 13
#define LCD_CLR_MODIFY		0xEE	/* clear the "read-modify-write" mode */
//CMD 14
#define LCD_RESET			0xE2	/* soft reset command */
//CMD 15
#define LCD_SET_SHL_NOR		0xC0	/* set COM scanning direction to normal */
#define LCD_SET_SHL_REV		0xC8	/* set COM scanning direction to reverse */
//CMD 16
#define LCD_PWR_CTL			0x28	/* set power circuit operation mode (3 lsbs = VC:VR:VF) */
//CMD 17
#define LCD_REG_RESISTOR	0x20	/* set the built in regulator resistor ratio Rb/Ra (3 lsbs = R2:R1:R0) */
//CMD 18  ( dual command) 
#define LCD_REF_VOLT_MODE	0x81	/* set reference voltage mode */
#define LCD_REF_VOLT_REG	0x00	/* set reference voltage register (display contrast value)  (6 lsbs = SV5:SV4:SV3:SV2:SV1:SV0) */
//CMD 19
#define LCD_ST_IND_MODE_OFF	0xAC	/* set static indicator mode to OFF*/
#define LCD_ST_IND_MODE_ON	0xAD	/* set static indicator mode to ON */
#define LCD_ST_IND_REG		0x00	/* set the static indicator register (2 lsbs = S1:S0) */
//CMD 20
#define LCD_SET_PWR_SAVE_STANDBY_MODE_ON	0xA8	/* set power save standby mode ON*/
#define LCD_SET_PWR_SAVE_SLEEP_MODE_ON		0xA9	/* set power save sleep mode ON*/
//CMD 21
#define LCD_SET_PWR_SAVE_MODE_OFF			0xE1	/* set power save mode OFF*/
//CMD 22
#define LCD_SET_NLINE_REV_DRV_REG			0x30	/* set n-Line reversal drive register 4 lsbs = SV3:SV2:SV1:SV0)*/
//CMD 23
#define LCD_RESET_NLINE_REV_DRV_REG			0xE4	/* reset n-Line reversal drive register */
//CMD 24
#define LCD_SET_OSC_ON						0xAB	/* set built-in oscillator ON */
//CMD 25
#define LCD_SET_DISCHARGE_EXT_CAP_ON		0x70	/* enable discharge of external capacitor */
#define LCD_SET_DISCHARGE_EXT_CAP_OFF		0x77	/* disable discharge of external capacitor */
//CMD 26
#define LCD_NOP								0xE3	/* NOP command */
//CMD 27
//test intruction should not be used
//CMD 28
#define LCD_SET_OTP_PROG_MODE				0x90	/* set OTP program mode  */
//CMD 29
#define LCD_OTP_PROG_CTRL					0x00	/* OTP program control (2msbs = OTPA_DJ:OTPP_ON)  */
//CMD 30
#define LCD_SET_CONTRAST_OFFSET_MODE_1		0x91	/* set contrast offset mode (1) */
//CMD 31
#define LCD_SET_CONTRAST_OFFSET_1			0x00	/* set contrast offset register (1)  6 lsbs = CTA5:CTA4:CTA3:CTA2:CTA1:CTA0*/
//CMD 32
#define LCD_SET_CONTRAST_OFFSET_MODE_2		0x92	/* set contrast offset mode (2) */
//CMD 33
#define LCD_SET_CONTRAST_OFFSET_2			0x00	/* set contrast offset register (2)  5 lsbs = CTAB:CTB3:CTB2:CTB1:CTB0*/
//CMD 34
#define LCD_SET_CONTRAST_OFFSET_MODE_3		0x93	/* set contrast offset mode (3) */
//CMD 35
#define LCD_SET_CONTRAST_OFFSET_3			0x00	/* set contrast offset register (3)  5 lsbs = CTC4:CTC3:CTC2:CTC1:CTC0*/

#if defined(CONFIG_JW_BOARDS)
	#ifdef CONFIG_JW_V1_BOARD
#define LCD_A0_LOW		P2_RESET_DATA_REG = 1<<15
#define LCD_A0_HIGH		P2_SET_DATA_REG = 1<<15
		//Kernel MACROs
		#define LCD_WRITE_INST(x)	(*(volatile unsigned char *)(LCD_IR)=(x))
		#define BUSY_FLAG			(1<<7)
		#define lcd_out_ctl(X)   {LCD_A0_LOW;LCD_WRITE_INST(X);}
		#define lcd_out_dat(X)   {LCD_A0_HIGH;LCD_WRITE_DATA(X);}
	#endif
#else
	//Kernel MACROs
	#define LCD_WRITE_INST(x)	(*(volatile unsigned char *)(LCD_IR)=(x))
	#define BUSY_FLAG			(1<<7)
	#define lcd_out_ctl(X)   {LCD_WRITE_INST(X);}
	#define lcd_out_dat(X)   {LCD_WRITE_DATA(X);}
#endif
// #define lcd_out_ctl(X)   {spin_lock_irq(&lcd_dev.lock);LCD_WRITE_INST(X);spin_unlock_irq(&lcd_dev.lock);}
// #define lcd_out_dat(X)   {spin_lock_irq(&lcd_dev.lock);LCD_WRITE_DATA(X);spin_unlock_irq(&lcd_dev.lock);}
/* LCD screen and bitmap image array consants */

#if defined(CONFIG_JW_BOARDS)
	#ifdef CONFIG_JW_V1_BOARD
		#define X_BYTES				176
		#define Y_BYTES	      		6


		#define COL_MIN				80//0x04
		#define COL_MAX				255//0x83

		#define SCRN_TOP			0
		#define SCRN_BOTTOM			47
		#define SCRN_LEFT			0//0x04

		#define SCRN_RIGHT			175//0x83
	#endif
#else
	#define X_BYTES				192
	#define Y_BYTES	      		8


	#define COL_MIN				32//0x04
	#define COL_MAX				223//0x83

	#define SCRN_TOP			0
	#define SCRN_BOTTOM			63
	#define SCRN_LEFT			0//0x04

	#define SCRN_RIGHT			191//0x83

#endif

#define PAGE_MIN			0
#define PAGE_MAX			(SCRN_BOTTOM/8)

//Processor specific commands
#define LCD_BACKLIGHT_LEVEL(X)  (TIMER2_DUTY_REG=X|X<<8)

/***      Data types definitions       ***/
struct lcd_sc14450_conf_struct{	
	// unsigned short flags;	
	unsigned char refresh_tmout;	
} ;


//IOCTL Commands
typedef enum IOCTL_Cmd_enum {
	LCD_init,//Initializes hw and driver internal state
	LCD_out_ctl,//Issues a LCD cmd directly from the user space
	LCD_clear_ram,//CLears LCD ram
	LCD_clear_rect,//Clears a rectangle defined by left/top /right/bottom coordinates in the LCD ram
	LCD_invert_rect,//Inverts a rectangle defined by left/top /right/bottom coordinates in the LCD ram
	LCD_draw_border,//Draws a border defined by left/top /right/bottom coordinates in the LCD ram
	LCD_clear_border,//Clear a border defined by left/top /right/bottom coordinates in the LCD ram
	LCD_text,//Write a string of characters using a specified font, starting at location defined by left, top coordinates in the LCD ram
	LCD_label,//Write a scrolling/not scrolling label at a specified position using a specified font
	LCD_glyph,//Prints a glyph at a specified position
	LCD_animate_bitmap,// Animate a bitmap at a specified position
	LCD_update,//Updates the LCD display with the contents of the LCD ram
	LCD_standby_enter,//Enter standby mode
	LCD_standby_exit,//Exit standby mode
	LCD_sleepmode_enter,//Enter sleep mode
	LCD_sleepmode_exit,//Exit sleep mode
	LCD_backlight_level,//Set backlight brightness level
	LCD_contrast_level,//Set backlight contrast level
	LCD_refresh_tmout,//Set LCD refresh timeout
	LCD_show_cursor,//Show or hide cursor at a specified position with a specified blinking rate
	LCD_IOCTL_MAX//this is a delimiter
} IOCTL_Cmd;
 

/****************************************************************************/
/**************           HW INDEPENDENT DEFINITIONS                    ***************/
/****************************************************************************/
/*
 * Macros to help debugging
 */
//#define LCD_SC14450_DEBUG
#undef PDEBUG             /* undef it, just in case */
#ifdef LCD_SC14450_DEBUG
#  ifdef __KERNEL__
     /* This one if debugging is on, and kernel space */
// #    define PDEBUG(fmt, args...) printk( KERN_DEBUG "lcd_sc14450: " fmt, ## args)
#    define PDEBUG(fmt, args...) printk( KERN_WARNING "lcd_sc14450: " fmt, ## args)
#  else
     /* This one for user space */
#    define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif

#undef PDEBUGG
#define PDEBUGG(fmt, args...) /* nothing: it's a placeholder */

#define LCD_SC14450_MAJOR 251
#ifndef LCD_SC14450_MAJOR
#define LCD_SC14450_MAJOR 0   /* dynamic major by default */
#endif

#ifndef LCD_SC14450_NR_DEVS
#define LCD_SC14450_NR_DEVS 1    
#endif


#endif
