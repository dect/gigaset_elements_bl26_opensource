// This is the header file for the experimental lcd driver for SC1445x developoment board
// 			Supported displays:
// LCD:  (dotmatrix display) ---- using lcd driver ST7586s

/* 
	Driver interface description
	LCD		BOARD
	---------------------------
	RS		->        AD7
	DB[0..7]	->	DAB[0..7]
*/
#ifndef LCD_ST7586S_H 
#define LCD_ST7586S_H 
/***************       Definitions section             ***************/
/* HW dependent definitions */

#if 1
#ifdef CONFIG_RAM_SIZE_32MB

#define CONFIG_32M_SDRAM_PATCH


#ifdef CONFIG_32M_SDRAM_PATCH

#define OVER_16M_SDRAM_ACCESS_PATCH_FLAG register unsigned long m_32m_patch_flag
#define PROTECT_OVER_16M_SDRAM_ACEESS	{ local_irq_save(m_32m_patch_flag); EBI_ACS4_CTRL_REG = 9;}
#define RESTORE_OVER_16M_SDRAM_ACEESS	{ EBI_ACS4_CTRL_REG = 0xa; local_irq_restore(m_32m_patch_flag); }

#else

#define OVER_16M_SDRAM_ACCESS_PATCH_FLAG 
#define PROTECT_OVER_16M_SDRAM_ACEESS	
#define RESTORE_OVER_16M_SDRAM_ACEESS	


#endif

#else

#define OVER_16M_SDRAM_ACCESS_PATCH_FLAG 
#define PROTECT_OVER_16M_SDRAM_ACEESS	
#define RESTORE_OVER_16M_SDRAM_ACEESS	


#endif

#endif

/* Registers */
//Since RS -> AD11, data are accessed at 0x800 and instructions are accessed at 0x00
#define LCD_BASE 0x01080000
#define LCD_DR (LCD_BASE+0x800)
#define LCD_IR (LCD_BASE+0x00)
#define LCD_LATCH 0x01090000

#define LCD_SET_BACKLIGHT_ON  {PROTECT_OVER_16M_SDRAM_ACEESS;(*(volatile unsigned short *)(LCD_LATCH)=(0x6000));RESTORE_OVER_16M_SDRAM_ACEESS}//Set backlight on, rst high
#define LCD_SET_BACKLIGHT_OFF {PROTECT_OVER_16M_SDRAM_ACEESS;(*(volatile unsigned short *)(LCD_LATCH)=(0x4000));RESTORE_OVER_16M_SDRAM_ACEESS}//Set backlight off, rst high

//Kernel MACROs
#define LCD_WRITE_DATA(x)	{(*(volatile unsigned char *)(LCD_DR)=(x));udelay(1);} /* write data to the display RAM (one byte)*/
#define LCD_WRITE_INST(x)	{(*(volatile unsigned char *)(LCD_IR)=(x));udelay(1);}
#define LCD_READ_DATA(x)	{(x=*(volatile unsigned char *)(LCD_DR));udelay(1);} /* read data from the display RAM (one byte)*/

#define lcd_out_ctl(X)   {PROTECT_OVER_16M_SDRAM_ACEESS;LCD_WRITE_INST((X));RESTORE_OVER_16M_SDRAM_ACEESS;}
#define lcd_out_dat(X)   {PROTECT_OVER_16M_SDRAM_ACEESS;LCD_WRITE_DATA((X));RESTORE_OVER_16M_SDRAM_ACEESS;}

/* Display settings */
#define INVERSE_DISPLAY //When defined the display orientation is inversed 
#define B3P3_MODE 	//When defined 3bytes-2pixels mode is used. If not defined 2B3P_MODE is used

/* LCD screen and bitmap image array consants */
#define SCRN_TOP			0x0
#define SCRN_BOTTOM			57
#define SCRN_LEFT			0x0
#define SCRN_RIGHT			158
#define DDRAM_SIZE			((SCRN_BOTTOM+1)*(SCRN_RIGHT+1)*2/8) //15360 bytes

#define X_BYTES				(SCRN_RIGHT+1)/3
#define Y_BYTES	      		((SCRN_BOTTOM+1))

#define COL_MIN				0x04//0x00
//#define COL_MAX				0x83
 

/***      Data types definitions       ***/
struct lcd_sc14450_conf_struct{	
	// unsigned short flags;	
	unsigned char refresh_tmout;	
} ;
//lcd_sc14450_conf_struct Flags 
// #ifdef AUTO_REFRESH	
	// #define	REFRESH_TIMER_ON  0x0001	
// #endif	

//IOCTL Commands
typedef enum IOCTL_Cmd_enum {
	LCD_init,
	LCD_out_ctl,
	LCD_out_data,
	LCD_clear_ram,
	LCD_clear_rect,
	LCD_invert_rect,
	LCD_draw_border,
	LCD_clear_border,
	LCD_text,
	LCD_label,
	LCD_glyph,
	LCD_graphic,
	LCD_animate_bitmap,
	LCD_update,
	LCD_contrast_up,
	LCD_contrast_down,
	LCD_sleepmode_enter,
	LCD_sleepmode_exit,
	LCD_backlight_level,
	LCD_contrast_level,
	LCD_refresh_tmout,
	LCD_show_cursor,
 	LCD_fonts_width_get,
	LCD_IOCTL_MAX//this is a delimiter
} IOCTL_Cmd;
 

/****************************************************************************/
/**************           HW INDEPENDENT DEFINITIONS                    ***************/
/****************************************************************************/
/*
 * Macros to help debugging
 */
//#define LCD_SC14450_DEBUG
#undef PDEBUG             /* undef it, just in case */
#ifdef LCD_SC14450_DEBUG
#  ifdef __KERNEL__
     /* This one if debugging is on, and kernel space */
#    define PDEBUG(fmt, args...) printk( KERN_DEBUG "lcd_sc14450: " fmt, ## args)
#  else
     /* This one for user space */
#    define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif

#undef PDEBUGG
#define PDEBUGG(fmt, args...) /* nothing: it's a placeholder */

#define LCD_SC14450_MAJOR 251
#ifndef LCD_SC14450_MAJOR
#define LCD_SC14450_MAJOR 0   /* dynamic major by default */
#endif

#ifndef LCD_SC14450_NR_DEVS
#define LCD_SC14450_NR_DEVS 1    
#endif


#endif
