//----------------------------------------------------------------------------
//							     
//		@@@@@@@      *   @@@@@@@@@	     *
//	       @       @	     @  	     *
//	       @	     *       @      ****     *
//		@@@@@@@      *       @     *	*    *
//	 ___---        @     *       @     ******    *
// ___---      @       @     *       @     *	     *
//  -_  	@@@@@@@  _   *       @      ****     *
//    -_		 _ -
//	-_	    _ -
//	  -_   _ -	  s   e   m   i   c   o   n   d   u   c   t   o   r
//	    -
//
//	S C 1 4 4 5 2
//	(c) 2008 Sitel Semiconductor B.V.
//	http://www.sitelsemi.com
//                                                                             
//	fileneme:       qspic_common.h                                   
//                                                                             
//	author: 	Dimitrios Papadopoulos                                             
//	created: 	September 2008                                                       
//----------------------------------------------------------------------------

#ifndef _QSPIC_COMMON_H_
#define _QSPIC_COMMON_H_

void qspic_clock_en (void);
void qspic_clock_dis(void);
void qspic_clksrc_hclk(void);
void qspic_clksrc_pll2 (void);
void qspic_clk_div2 (void);
void qspic_clk_div1 (void);
void qspic_pads_init (void);
void qspic_auto_mode (void);
void qspic_manual_mode (void);
void qspic_enable_quad_pads (void);
void qspic_disable_quad_pads (void);
void qspi_clk_set_PLL2_half (void);
void qspi_clk_set_PLL2_full (void);
void qspi_clk_set_HCLK_half (void);
void qspi_clk_set_HCLK_full (void);
void qspic_delay(void);
unsigned short qspic_flash_swap_addr (unsigned long Address);

#endif
