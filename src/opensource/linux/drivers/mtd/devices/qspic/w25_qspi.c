/*
 * MTD SPI driver for ST M25Pxx (and similar) serial flash chips
 *
 * Author: Mike Lavender, mike@steroidmicros.com
 *
 * Copyright (c) 2005, Intec Automation Inc.
 *
 * Some parts are based on lart.c by Abraham Van Der Merwe
 *
 * Cleaned up and generalized based on mtd_dataflash.c
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/mutex.h>
#include <linux/platform_device.h>


#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>

#include <linux/spi/flash.h>


#include <asm/regs.h>
#include "qspic_common.h"
#include "w25_qspi.h"

#define FLASH_PAGESIZE		256


/* Define max times to check status register before we give up. */
#define	MAX_READY_WAIT_COUNT	200000


#ifdef CONFIG_MTD_PARTITIONS
#define	mtd_has_partitions()	(1)
#else
#define	mtd_has_partitions()	(0)
#endif

/****************************************************************************/

struct w25q {
	struct mutex		lock;
	struct mtd_info		mtd;
	unsigned		partitioned:1;
};

static inline struct w25q *mtd_to_w25q(struct mtd_info *mtd)
{
	return container_of(mtd, struct w25q, mtd);
}



// Wait until the end of write/erase operation
static int WND_WaitEndOfWrite(void)
{
	volatile unsigned i;
	// Single Mode, CS Low	
	QSPIC_CTRLBUS_REG = QSPIC_SET_SINGLE | QSPIC_EN_CS;	
	
	// Read Status 1
	QSPIC_WRITEDATA8_REG = WND_INST_RSTAT1;

	i=0;
	
	while (( QSPIC_READDATA8_REG & WND_STATUS_BUSSY) && (i++<MAX_READY_WAIT_COUNT));
		
	// CS High
	QSPIC_CTRLBUS_REG= QSPIC_DIS_CS;

	if (i>=MAX_READY_WAIT_COUNT)
		return 1;
	return 0;
}



// Exit from Performance enhanced mode when a Quad I/O read is used.
static void WND_ResetQuadModeBits(void) {
	// Set Single Mode
	QSPIC_CTRLBUS_REG = QSPIC_SET_SINGLE;

	// Reset Mode Bit
	QSPIC_WRITEDATA8_REG = WND_INST_MRSTQIO;
}

// Exit from Performance enhanced mode when a Dual I/O read is used.
static void WND_ResetDualModeBits(void) {
	// Set Single Mode
	QSPIC_CTRLBUS_REG = QSPIC_SET_SINGLE;

	// Reset Mode Bit
	QSPIC_WRITEDATA16_REG = WND_INST_MRSTDIO;
}

// Release From Deep Power Down 
static void WND_ReleasePDM_HPM(void) {
	// Set Single Mode
	QSPIC_CTRLBUS_REG = QSPIC_SET_SINGLE;

	// Release Power Down
	QSPIC_WRITEDATA8_REG = WND_INST_PDID;
}

// Quad Output Byte Read
static void WND_ReadQuadOutputByte (unsigned long int Address, unsigned long int BSize, unsigned char *BBuf)
{
	int i;
	
	// Single Mode, CS Low	
	QSPIC_CTRLBUS_REG = (QSPIC_SET_SINGLE | QSPIC_EN_CS);
	
	// Read Data Instruction
	QSPIC_WRITEDATA8_REG = WND_INST_FRQDATA;
	QSPIC_WRITEDATA16_REG = qspic_flash_swap_addr(Address);
	QSPIC_WRITEDATA8_REG = (Address & 0xff);

	// Dummy Byte
	QSPIC_DUMMYDATA8_REG = 0x0;

	// Set Quad mode	
	QSPIC_CTRLBUS_REG = QSPIC_SET_QUAD;
	
	// Read Data
	for (i=0; i<BSize; i++) 
		BBuf[i] = QSPIC_READDATA8_REG;
	
	// CS High
	QSPIC_CTRLBUS_REG = QSPIC_DIS_CS;
}


// Set High Performance - To operate in high frequencies.
static void WND_SetHighPerf (void)
{
	// Single Mode
	QSPIC_CTRLBUS_REG = QSPIC_SET_SINGLE;	
	
	// Write Enable
	QSPIC_WRITEDATA8_REG = WND_INST_WEn;
		
	// CS Low
	QSPIC_CTRLBUS_REG = QSPIC_EN_CS;
	
	// Write High Performance Mode
	QSPIC_WRITEDATA8_REG = WND_INST_HIGHP;

	// 3 - Dummy Bytes
	QSPIC_DUMMYDATA16_REG = 0x0;
	QSPIC_DUMMYDATA8_REG = 0x0;

	// CS High
	QSPIC_CTRLBUS_REG = QSPIC_DIS_CS;
}


// Write Status and Configuration Registers
static void WND_WriteStatus (unsigned char w_status1, unsigned char w_status2) {
	// Single Mode
	QSPIC_CTRLBUS_REG = QSPIC_SET_SINGLE;
	
	// Write Enable
	QSPIC_WRITEDATA8_REG = WND_INST_WEn;
	
	// CS Low
	QSPIC_CTRLBUS_REG = QSPIC_EN_CS;
	
	// Write Status Registers
	QSPIC_WRITEDATA8_REG = WND_INST_WSTAT;
	QSPIC_WRITEDATA8_REG = w_status1;
	QSPIC_WRITEDATA8_REG = w_status2;
	
	// CS High
	QSPIC_CTRLBUS_REG = QSPIC_DIS_CS;

	// Wait end of write
	WND_WaitEndOfWrite();
}

// Erase 4KB sector.
static int WND_EraseSector (unsigned long int Address)
{
	// Wait end of Erase
	if (WND_WaitEndOfWrite())
		return 1;
	
		
	// Single Mode
	QSPIC_CTRLBUS_REG = QSPIC_SET_SINGLE;	

	// Write Enable
	QSPIC_WRITEDATA8_REG = WND_INST_WEn;

	// CS Low
	QSPIC_CTRLBUS_REG = QSPIC_EN_CS;
	
	// Sector 4KB Erase
	QSPIC_WRITEDATA8_REG = WND_INST_SERASE;
	
	// Sector Address
	QSPIC_WRITEDATA16_REG = qspic_flash_swap_addr(Address);
	QSPIC_WRITEDATA8_REG = (Address & 0xff);
	
	// CS High
	QSPIC_CTRLBUS_REG = QSPIC_DIS_CS;

	
	return 0;//(4*1024);
}


// Write a page (256  byte) using the quad spi mode.
static int WND_WritePageQuad (unsigned long int Address, const u_char *WPage,unsigned short len)
{
	unsigned int i;
   
	if (WND_WaitEndOfWrite())
		return 0;
			
	// Single Mode
	QSPIC_CTRLBUS_REG = QSPIC_SET_SINGLE;	

	// Write Enable
	QSPIC_WRITEDATA8_REG = WND_INST_WEn;
		
	// CS Low
	QSPIC_CTRLBUS_REG = QSPIC_EN_CS;	
	
	// Program Page 
	QSPIC_WRITEDATA8_REG = WND_INST_QPPROG;
	
	// Write Address
	QSPIC_WRITEDATA16_REG = qspic_flash_swap_addr(Address);
	QSPIC_WRITEDATA8_REG = (Address & 0xff);

	// Quad Mode
	QSPIC_CTRLBUS_REG = QSPIC_SET_QUAD;	
	
	for (i=0; i<len; i++) 
		QSPIC_WRITEDATA8_REG = WPage[i];
	
	// CS High
	QSPIC_CTRLBUS_REG = QSPIC_DIS_CS;


	return (len);  
}


/****************************************************************************/

/*
 * MTD implementation
 */

/*
 * Erase an address range on the flash chip.  The address range may extend
 * one or more erase sectors.  Return an error is there is a problem erasing.
 */
static int w25q_erase(struct mtd_info *mtd, struct erase_info *instr)
{
	struct w25q *flash = mtd_to_w25q(mtd);
	u32 addr,len;

	DEBUG(MTD_DEBUG_LEVEL2, "w25 qspi: %s %s 0x%08x, len %d\n",
			 __FUNCTION__, "at",
			(u32)instr->addr, instr->len);

	/* sanity checks */
	if (instr->addr + instr->len > flash->mtd.size)
		return -EINVAL;
	if ((instr->addr % mtd->erasesize) != 0
			|| (instr->len % mtd->erasesize) != 0) {
		return -EINVAL;
	}

	addr = instr->addr;
	len = instr->len;

	mutex_lock(&flash->lock);

	/* REVISIT in some cases we could speed up erasing large regions
	 * by using OPCODE_SE instead of OPCODE_BE_4K
	 */

	/* now erase those sectors */
	while (len) {
		if (WND_EraseSector(addr)) {		//todo
			instr->state = MTD_ERASE_FAILED;
			mutex_unlock(&flash->lock);
			return -EIO;
		}

		addr += mtd->erasesize;
		len -= mtd->erasesize;
	}

	mutex_unlock(&flash->lock);

	instr->state = MTD_ERASE_DONE;
	mtd_erase_callback(instr);

	return 0;
}

/*
 * Read an address range from the flash chip.  The address range
 * may be any size provided it is within the physical boundaries.
 */
static int w25q_read(struct mtd_info *mtd, loff_t from, size_t len,
	size_t *retlen, u_char *buf)
{
	struct w25q *flash = mtd_to_w25q(mtd);

	DEBUG(MTD_DEBUG_LEVEL2, "w25 qspi: %s %s 0x%08x, len %zd\n",
			 __FUNCTION__, "from",
			(u32)from, len);

	/* sanity checks */
	if (!len)
		return 0;

	if (from + len > flash->mtd.size)
		return -EINVAL;
	mutex_lock(&flash->lock);

	/* Wait till previous write/erase is done. */
	if (WND_WaitEndOfWrite()) {
		/* REVISIT status return?? */
		mutex_unlock(&flash->lock);
		return 1;
	}

	WND_ReadQuadOutputByte (from,len,buf);


	*retlen = len;

	mutex_unlock(&flash->lock);
	return 0;
}

/*
 * Write an address range to the flash chip.  Data must be written in
 * FLASH_PAGESIZE chunks.  The address range may be any size provided
 * it is within the physical boundaries.
 */
static int w25q_write(struct mtd_info *mtd, loff_t to, size_t len,
	size_t *retlen, const u_char *buf)
{
	struct w25q *flash = mtd_to_w25q(mtd);
	u32 page_offset, page_size;

	DEBUG(MTD_DEBUG_LEVEL2, "%s: %s %s 0x%08x, len %zd\n",
			flash->spi->dev.bus_id, __FUNCTION__, "to",
			(u32)to, len);

	if (retlen)
		*retlen = 0;

	/* sanity checks */
	if (!len)
		return(0);

	if (to + len > flash->mtd.size)
		return -EINVAL;

	mutex_lock(&flash->lock);

	
	/* what page do we start with? */
	page_offset = to % FLASH_PAGESIZE;

	/* do all the bytes fit onto one page? */
	if (page_offset + len <= FLASH_PAGESIZE) {

		if (!WND_WritePageQuad (to,buf,len))
		{
			mutex_unlock(&flash->lock);
			return 1;
		}

		*retlen = len;
	} else {
		u32 i;

		/* the size of data remaining on the first page */
		page_size = FLASH_PAGESIZE - page_offset;

		if (!WND_WritePageQuad (to,buf,page_size))
		{
			mutex_unlock(&flash->lock);
			return 1;
		}
		*retlen = page_size;

		/* write everything in PAGESIZE chunks */
		for (i = page_size; i < len; i += page_size) {
			page_size = len - i;
			if (page_size > FLASH_PAGESIZE)
				page_size = FLASH_PAGESIZE;

			if (!WND_WritePageQuad (to+i,buf+i,page_size))
			{
				mutex_unlock(&flash->lock);
				return 1;
			}

			*retlen+= page_size;
		}
	}

	mutex_unlock(&flash->lock);
	return 0;
}


/****************************************************************************/

/*
 * SPI device driver setup and teardown
 */

struct flash_info {
	char		*name;

	/* JEDEC id zero means "no ID" (most older chips); otherwise it has
	 * a high byte of zero plus three data bytes: the manufacturer id,
	 * then a two byte device id.
	 */
	u32		jedec_id;

	/* The size listed here is what works with OPCODE_SE, which isn't
	 * necessarily called a "sector" by the vendor.
	 */
	unsigned	sector_size;
	u16		n_sectors;

	u16		flags;
#define	SECT_4K		0x01		/* OPCODE_BE_4K works uniformly */
};


/* NOTE: double check command sets and memory organization when you add
 * more flash chips.  This current list focusses on newer chips, which
 * have been converging on command sets which including JEDEC ID.
 */
static struct flash_info __devinitdata w25q_data [] = {


	/* Winbond -- w25q QuadSPI, used as simple SPI */
	{ "w25q80", 0xef4014, 64 * 1024, 16, SECT_4K, },
	{ "w25q16", 0xef4015, 64 * 1024, 32, SECT_4K, },
	{ "w25q32", 0xef4016, 64 * 1024, 64, SECT_4K, },
};




// Read Jedec ID -  Byte Buffer
void WND_ReadJEDECID (unsigned char *JIDBuf)
{
	// Single Mode, CS Low
	QSPIC_CTRLBUS_REG = (QSPIC_SET_SINGLE | QSPIC_EN_CS);

	// Read Unique ID Instruction
	QSPIC_WRITEDATA8_REG = WND_INST_JID;
	
	// Manufacture ID 
	JIDBuf[0] = QSPIC_READDATA8_REG;

	// Device type
	JIDBuf[1] = QSPIC_READDATA8_REG;
		
	// Device ID
	JIDBuf[2] = QSPIC_READDATA8_REG;
	
	// CS High
	QSPIC_CTRLBUS_REG = QSPIC_DIS_CS;
	
}


static struct flash_info *__devinit jedec_probe(struct platform_device *pdev)
{

	int			tmp;
	u8			id[3];
	u32			jedec;
	struct flash_info	*info;

	/* JEDEC also defines an optional "extended device information"
	 * string for after vendor-specific data, after the three bytes
	 * we use here.  Supporting some chips might require using it.
	 */

	WND_ReadJEDECID(id);

	jedec = id[0];
	jedec = jedec << 8;
	jedec |= id[1];
	jedec = jedec << 8;
	jedec |= id[2];

	for (tmp = 0, info = w25q_data;
			tmp < ARRAY_SIZE(w25q_data);
			tmp++, info++) {
		if (info->jedec_id == jedec)
			return info;
	}
	dev_err(&pdev->dev, "unrecognized JEDEC id %06x\n", jedec);

	return NULL;
}


void w25q_dev_init (void)
{

	// Program the Peripherals pin assignment matrix to enable the quad spi controller interface 
	qspic_pads_init();
	
	// Sets the qspi bus clock equal to hclk/2 and enables the qspic clock.
	// This is a safe selection because the hclk is always active.
//	qspi_clk_set_HCLK_half();
	qspi_clk_set_HCLK_full();
	
	QSPIC_CTRLMODE_REG = (QSPIC_CLK_MD  | QSPIC_IO2_OEN | QSPIC_IO3_OEN | 
	                              QSPIC_IO2_DAT | QSPIC_IO3_DAT | QSPIC_RXD_NEG  );
	
	QSPIC_CTRLBUS_REG = (QSPIC_SET_SINGLE | QSPIC_DIS_CS);




		// IO3 = 1, IO2 = 1
	qspic_disable_quad_pads();
	
	// Escape from Performance Enchanced Mode
	WND_ResetQuadModeBits();
	WND_ResetDualModeBits();
	
	// Release From Deeep Power Down 
	WND_ReleasePDM_HPM();
	
	// Wait to exit from power down mode.
	qspic_delay();


		// Initialize the Status registers. 
	// Disable Write Protection and Enable Quad mode
	WND_WriteStatus(0x0,WND_STATUS_QE);
	
	// IO3 hi-z, IO2 hi-z 
	qspic_enable_quad_pads();
	
	// Enable high frequencies
	WND_SetHighPerf();




}

/*
 * board specific setup should have ensured the SPI clock used here
 * matches what the READ command supports, at least until this driver
 * understands FAST_READ (for clocks over 25 MHz).
 */
static int  w25q_qspi_probe(struct platform_device *pdev)
{
	struct flash_platform_data	*data;
	struct w25q			*flash;
	struct flash_info		*info;
	unsigned			i;


	w25q_dev_init();


	/* Platform data helps sort out which chip type we have, as
	 * well as how this board partitions it.  If we don't have
	 * a chip ID, try the JEDEC id commands; they'll work for most
	 * newer chips, even if we don't recognize the particular chip.
	 */
	data = pdev->dev.platform_data;

	if (data && data->type) {
		for (i = 0, info = w25q_data;
				i < ARRAY_SIZE(w25q_data);
				i++, info++) {
			if (strcmp(data->type, info->name) == 0)
				break;
		}

		/* unrecognized chip? */
		if (i == ARRAY_SIZE(w25q_data)) {
			DEBUG(MTD_DEBUG_LEVEL0, "w25 qspi : unrecognized id %s\n",
					 data->type);
			info = NULL;

		/* recognized; is that chip really what's there? */
		} else if (info->jedec_id) {
			struct flash_info	*chip = jedec_probe(pdev);

			if (!chip || chip != info) {
				dev_warn(&pdev->dev, "found %s, expected %s\n",
						chip ? chip->name : "UNKNOWN",
						info->name);
				info = NULL;
			}
		}
	} else
		info = jedec_probe(pdev);

	if (!info)
		return -ENODEV;

	
	flash = kzalloc(sizeof *flash, GFP_KERNEL);
	if (!flash)
		return -ENOMEM;


	mutex_init(&flash->lock);

	if (data && data->name)
		flash->mtd.name = data->name;
	else
		flash->mtd.name = "unknown";

	flash->mtd.type = MTD_NORFLASH;
	flash->mtd.writesize = 1;
	flash->mtd.flags = MTD_CAP_NORFLASH;
	flash->mtd.size = info->sector_size * info->n_sectors;
	flash->mtd.erase = w25q_erase;
	flash->mtd.read = w25q_read;
	flash->mtd.write = w25q_write;
	flash->mtd.erasesize = 4096;//info->sector_size;

	dev_info(&pdev->dev, "%s (%d Kbytes)\n", info->name,
			flash->mtd.size / 1024);

	DEBUG(MTD_DEBUG_LEVEL2,
		"mtd .name = %s, .size = 0x%.8x (%uMiB) "
			".erasesize = 0x%.8x (%uKiB) .numeraseregions = %d\n",
		flash->mtd.name,
		flash->mtd.size, flash->mtd.size / (1024*1024),
		flash->mtd.erasesize, flash->mtd.erasesize / 1024,
		flash->mtd.numeraseregions);

	if (flash->mtd.numeraseregions)
		for (i = 0; i < flash->mtd.numeraseregions; i++)
			DEBUG(MTD_DEBUG_LEVEL2,
				"mtd.eraseregions[%d] = { .offset = 0x%.8x, "
				".erasesize = 0x%.8x (%uKiB), "
				".numblocks = %d }\n",
				i, flash->mtd.eraseregions[i].offset,
				flash->mtd.eraseregions[i].erasesize,
				flash->mtd.eraseregions[i].erasesize / 1024,
				flash->mtd.eraseregions[i].numblocks);


	/* partitions should match sector boundaries; and it may be good to
	 * use readonly partitions for writeprotected sectors (BP2..BP0).
	 */
	if (mtd_has_partitions()) {
		struct mtd_partition	*parts = NULL;
		int			nr_parts = 0;

#ifdef CONFIG_MTD_CMDLINE_PARTS
		static const char *part_probes[] = { "cmdlinepart", NULL, };

		nr_parts = parse_mtd_partitions(&flash->mtd,
				part_probes, &parts, 0);
#endif

		if (nr_parts <= 0 && data && data->parts) {
			parts = data->parts;
			nr_parts = data->nr_parts;
		}

		if (nr_parts > 0) {
			for (i = 0; i < nr_parts; i++) {
				DEBUG(MTD_DEBUG_LEVEL2, "partitions[%d] = "
					"{.name = %s, .offset = 0x%.8x, "
						".size = 0x%.8x (%uKiB) }\n",
					i, parts[i].name,
					parts[i].offset,
					parts[i].size,
					parts[i].size / 1024);
			}
			flash->partitioned = 1;
			return add_mtd_partitions(&flash->mtd, parts, nr_parts);
		}
	} else if (data->nr_parts)
		dev_warn(&pdev->dev, "ignoring %d default partitions on %s\n",
				data->nr_parts, data->name);

	return add_mtd_device(&flash->mtd) == 1 ? -ENODEV : 0;
}

static int w25q_qspi_remove(struct platform_device *dev)
{
	return 0;
}
static int w25q_qspi_suspend(struct platform_device *dev, pm_message_t pm)
{
	return 0;
}
static int w25q_qspi_resume(struct platform_device *dev)
{
	return 0;
}


static struct platform_driver w25q_qspi_driver = {
	.probe		= w25q_qspi_probe,
	.remove		= w25q_qspi_remove,
	.suspend	= w25q_qspi_suspend,
	.resume		= w25q_qspi_resume,
	.driver		= {
		.name	= "w25q_qspi",
		.owner	= THIS_MODULE,
	},
};



static int __init w25q_init(void)
{

	return platform_driver_register(&w25q_qspi_driver);

}


static void w25q_exit(void)
{
	//todo
	qspic_clock_dis();
}


module_init(w25q_init);
module_exit(w25q_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("J&B");
MODULE_DESCRIPTION("MTD QSPI driver for W25Qxx flash chips");
