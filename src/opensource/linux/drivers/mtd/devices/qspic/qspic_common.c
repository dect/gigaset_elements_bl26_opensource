//----------------------------------------------------------------------------
//							     
//		@@@@@@@      *   @@@@@@@@@	     *
//	       @       @	     @  	     *
//	       @	     *       @      ****     *
//		@@@@@@@      *       @     *	*    *
//	 ___---        @     *       @     ******    *
// ___---      @       @     *       @     *	     *
//  -_  	@@@@@@@  _   *       @      ****     *
//    -_		 _ -
//	-_	    _ -
//	  -_   _ -	  s   e   m   i   c   o   n   d   u   c   t   o   r
//	    -
//
//	S C 1 4 4 5 2
//	(c) 2008 Sitel Semiconductor B.V.
//	http://www.sitelsemi.com
//                                                                             
//	fileneme:       qspic_common.c                                   
//                                                                             
//	author: 	Dimitrios Papadopoulos                                             
//	created: 	September 2008                                                       
//----------------------------------------------------------------------------

#include <asm/regs.h>


// Enable Quad SPI Clock
void qspic_clock_en (void) {
	CLK_AUX2_REG = CLK_AUX2_REG | SW_QSPIC_EN;
}


// Disable Quad SPI Clock
void qspic_clock_dis(void) {
	CLK_AUX2_REG = CLK_AUX2_REG & ~SW_QSPIC_EN;
}


// The the source of spi clock is the hclk.
void qspic_clksrc_hclk(void) {
	CLK_AUX2_REG = CLK_AUX2_REG | SW_QSPIC_SEL_SOURCE;
}

// The source of the spi clock is the PLL2.
void qspic_clksrc_pll2 (void) {
	CLK_AUX2_REG = CLK_AUX2_REG & ~SW_QSPIC_SEL_SOURCE;
}


// The divider for the spi clock is equal to 2.
void qspic_clk_div2 (void) {
	CLK_AUX2_REG = CLK_AUX2_REG | 0x2000;
}


// The divider for the spi clock is equal to 1.
void qspic_clk_div1 (void) {
	CLK_AUX2_REG = CLK_AUX2_REG & ~0x2000;
}

// Configure the Programmable Peripheral Matrix to enable the Quad SPI Controller.
void qspic_pads_init (void) {
	P0_03_MODE_REG = 0x338;
	P0_06_MODE_REG = 0x338;
	P0_11_MODE_REG = 56;
	P0_12_MODE_REG = 56;
	P0_13_MODE_REG = 56;
	P0_14_MODE_REG = 56;
}

// Auto Mode.
void qspic_auto_mode (void) {
	QSPIC_CTRLMODE_REG = QSPIC_AUTO_MD | QSPIC_CTRLMODE_REG;
}

// Manual Mode
void qspic_manual_mode (void) {
	QSPIC_CTRLMODE_REG = ~QSPIC_AUTO_MD & QSPIC_CTRLMODE_REG;
}

// Enable the pads for quad operation
void qspic_enable_quad_pads (void) {
	QSPIC_CTRLMODE_REG = QSPIC_CTRLMODE_REG & ~(QSPIC_IO3_OEN | QSPIC_IO2_OEN);
}

// Disable the quad pads operation
void qspic_disable_quad_pads (void) {
	QSPIC_CTRLMODE_REG = QSPIC_CTRLMODE_REG | (QSPIC_IO3_OEN | QSPIC_IO2_OEN | QSPIC_IO3_DAT | QSPIC_IO2_DAT);
}


// Set Clock frequency equal to PLL2/2 (25Mhz)
void qspi_clk_set_PLL2_half (void) {
	qspic_clock_dis();
	qspic_clksrc_pll2();
	qspic_clk_div2();
	qspic_clock_en();
}

// Set Clock frequency equal to PLL2 (50Mhz)
void qspi_clk_set_PLL2_full (void) {
	qspic_clock_dis();
	qspic_clksrc_pll2();
	qspic_clk_div1();
	qspic_clock_en();
}

// Set Clock frequency equal to HCLK/2 (41.5Mhz)
void qspi_clk_set_HCLK_half (void) {
	qspic_clock_dis();
	qspic_clksrc_hclk();
	qspic_clk_div2();
	qspic_clock_en();
}

// Set Clock frequency equal to HCLK (83Mhz)
void qspi_clk_set_HCLK_full (void) {
	qspic_clock_dis();
	qspic_clksrc_hclk();
	qspic_clk_div1();
	qspic_clock_en();
}

void qspic_delay(void) {
	volatile unsigned long int i;
	for (i=0;i<200; i++);
}

unsigned short qspic_flash_swap_addr (unsigned long Address) {
	unsigned long temp;
	
	temp = (Address >> 16) & 0x00ff;
	temp = temp | (Address & 0xff00);
	
	return ((unsigned short) temp);
}
