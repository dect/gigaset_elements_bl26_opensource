/*
 * mtdram - a test mtd device
 * $Id: mtdram.c,v 1.37 2005/04/21 03:42:11 joern Exp $
 * Author: Alexander Larsson <alex@cendio.se>
 *
 * Copyright (c) 1999 Alexander Larsson <alex@cendio.se>
 * Copyright (c) 2005 Joern Engel <joern@wh.fh-wedel.de>
 *
 * This code is GPL
 *
 */

#include <linux/module.h>
#include <linux/slab.h>
#include <linux/ioport.h>
#include <linux/vmalloc.h>
#include <linux/init.h>
#include <linux/mtd/compatmac.h>
#include <linux/mtd/mtd.h>
#include <config/mtd/mtdram.h>
#include <linux/mtd/partitions.h>

#if defined( CONFIG_SC1445x_ROMFS_XIP )
/* start/end address of romfs */
extern char __xip_romfs_start ;
extern char __xip_romfs_end ;
#endif

static unsigned long total_size = CONFIG_MTDRAM_TOTAL_SIZE;
static unsigned long erase_size = CONFIG_MTDRAM_ERASE_SIZE;
#if defined( CONFIG_SC1445x_ROMFS_XIP ) && ( 0 == CONFIG_MTDRAM_ABS_POS )
#	define MTDRAM_TOTAL_SIZE (&__xip_romfs_end - &__xip_romfs_start )
#else
#	define MTDRAM_TOTAL_SIZE (total_size * 1024)
#endif
#define MTDRAM_ERASE_SIZE (erase_size * 1024)

#ifdef MODULE
module_param(total_size, ulong, 0);
MODULE_PARM_DESC(total_size, "Total device size in KiB");
module_param(erase_size, ulong, 0);
MODULE_PARM_DESC(erase_size, "Device erase block size in KiB");
#endif

// We could store these in the mtd structure, but we only support 1 device..
static struct mtd_info *mtd_info;

static int ram_erase(struct mtd_info *mtd, struct erase_info *instr)
{
	if (instr->addr + instr->len > mtd->size)
		return -EINVAL;

	memset((char *)mtd->priv + instr->addr, 0xff, instr->len);

	instr->state = MTD_ERASE_DONE;
	mtd_erase_callback(instr);

	return 0;
}

static int ram_point(struct mtd_info *mtd, loff_t from, size_t len,
		size_t *retlen, u_char **mtdbuf)
{
	if (from + len > mtd->size)
		return -EINVAL;

	*mtdbuf = mtd->priv + from;
	*retlen = len;
	return 0;
}

static void ram_unpoint(struct mtd_info *mtd, u_char * addr, loff_t from,
		size_t len)
{
}

/*
 * Allow NOMMU mmap() to directly map the device (if not NULL)
 * - return the address to which the offset maps
 * - return -ENOSYS to indicate refusal to do the mapping
 */
static unsigned long ram_get_unmapped_area(struct mtd_info *mtd,
					   unsigned long len,
					   unsigned long offset,
					   unsigned long flags)
{
	return (unsigned long) mtd->priv + offset;
}

static int ram_read(struct mtd_info *mtd, loff_t from, size_t len,
		size_t *retlen, u_char *buf)
{
	if (from + len > mtd->size)
		return -EINVAL;

	memcpy(buf, mtd->priv + from, len);

	*retlen = len;
	return 0;
}

static int ram_write(struct mtd_info *mtd, loff_t to, size_t len,
		size_t *retlen, const u_char *buf)
{
	if (to + len > mtd->size)
		return -EINVAL;

	memcpy((char *)mtd->priv + to, buf, len);

	*retlen = len;
	return 0;
}

static void __exit cleanup_mtdram(void)
{
	if (mtd_info) {
		del_mtd_device(mtd_info);
#if CONFIG_MTDRAM_ABS_POS > 0  ||  defined( CONFIG_SC1445x_ROMFS_XIP )
		iounmap(mtd_info->priv);
#else
		vfree(mtd_info->priv);
#endif
		kfree(mtd_info);
	}
}

#if 0
#ifdef CONFIG_MTD_PARTITIONS

static struct mtd_partition sc1445x_mtdram_partitions = {
  .name = "RomFS",
  .size = CONFIG_MTDRAM_TOTAL_SIZE,
  .offset = CONFIG_MTDRAM_ABS_POS
};

#endif
#endif

int mtdram_init_device(struct mtd_info *mtd, void *mapped_address,
		unsigned long size, char *name)
{
	memset(mtd, 0, sizeof(*mtd));

	/* Setup the MTD structure */
	mtd->name = name;
	mtd->type = MTD_RAM;
	mtd->flags = MTD_CAP_RAM;
	mtd->size = size;
	mtd->writesize = 1;
	mtd->erasesize = MTDRAM_ERASE_SIZE;
	mtd->priv = mapped_address;

	mtd->owner = THIS_MODULE;
	mtd->erase = ram_erase;
	mtd->point = ram_point;
	mtd->unpoint = ram_unpoint;
	mtd->get_unmapped_area = ram_get_unmapped_area;
	mtd->read = ram_read;
	mtd->write = ram_write;

	if (add_mtd_device(mtd)) {
	  printk("add_mtd_device failed\n");
	  return -EIO;
	}

#if 0
#ifdef CONFIG_MTD_PARTITIONS
	add_mtd_partitions (mtd, &sc1445x_mtdram_partitions,1);
#endif
#endif
	return 0;
}

static int __init init_mtdram(void)
{
	void *addr;
	int err;

#if defined( CONFIG_SC1445x_ROMFS_XIP ) && ( 0 == CONFIG_MTDRAM_ABS_POS )
	total_size = MTDRAM_TOTAL_SIZE ;
#endif
	if (!total_size)
		return -EINVAL;

	/* Allocate some memory */
	mtd_info = kmalloc(sizeof(struct mtd_info), GFP_KERNEL);
	if (!mtd_info)
		return -ENOMEM;

#if CONFIG_MTDRAM_ABS_POS > 0
	addr = ioremap(CONFIG_MTDRAM_ABS_POS, MTDRAM_TOTAL_SIZE);
#elif defined( CONFIG_SC1445x_ROMFS_XIP )
printk( "%s: __xip_romfs_start=%p, __xip_romfs_end=%p\n", __FUNCTION__, &__xip_romfs_start, &__xip_romfs_end ) ;
	addr = ioremap( (unsigned)&__xip_romfs_start, MTDRAM_TOTAL_SIZE ) ;
#else
	addr = vmalloc(MTDRAM_TOTAL_SIZE);
#endif

	if (!addr) {
#if CONFIG_MTDRAM_ABS_POS > 0
	  DEBUG(MTD_DEBUG_LEVEL1,
		"Failed to ioremap memory region of size %ld at ABS_POS:%ld\n",
		(long)MTDRAM_TOTAL_SIZE, (long)CONFIG_MTDRAM_ABS_POS);
#elif defined( CONFIG_SC1445x_ROMFS_XIP )
	  DEBUG(MTD_DEBUG_LEVEL1,
		"Failed to ioremap memory region of size %ld at %ld\n",
		MTDRAM_TOTAL_SIZE, (long)&__xip_romfs_start);
#endif
	}

	if (!addr) {
		kfree(mtd_info);
		mtd_info = NULL;
		return -ENOMEM;
	}
	err = mtdram_init_device(mtd_info, addr, MTDRAM_TOTAL_SIZE, "mtdram");
	if (err) {
		vfree(addr);
		kfree(mtd_info);
		mtd_info = NULL;
		return err;
	}
#ifndef CONFIG_MTDRAM_ABS_POS
	memset(mtd_info->priv, 0xff, MTDRAM_TOTAL_SIZE);
#endif
	return err;
}

module_init(init_mtdram);
module_exit(cleanup_mtdram);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alexander Larsson <alexl@redhat.com>");
MODULE_DESCRIPTION("Simulated MTD driver for testing");
//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:53:55.702513834
//@@ -17,10 +17,22 @@
// #include <linux/init.h>
// #include <linux/mtd/compatmac.h>
// #include <linux/mtd/mtd.h>
//+#include <config/mtd/mtdram.h>
//+#include <linux/mtd/partitions.h>
//+
//+#if defined( CONFIG_SC1445x_ROMFS_XIP )
//+/* start/end address of romfs */
//+extern char __xip_romfs_start ;
//+extern char __xip_romfs_end ;
//+#endif
// 
// static unsigned long total_size = CONFIG_MTDRAM_TOTAL_SIZE;
// static unsigned long erase_size = CONFIG_MTDRAM_ERASE_SIZE;
//-#define MTDRAM_TOTAL_SIZE (total_size * 1024)
//+#if defined( CONFIG_SC1445x_ROMFS_XIP ) && ( 0 == CONFIG_MTDRAM_ABS_POS )
//+#	define MTDRAM_TOTAL_SIZE (&__xip_romfs_end - &__xip_romfs_start )
//+#else
//+#	define MTDRAM_TOTAL_SIZE (total_size * 1024)
//+#endif
// #define MTDRAM_ERASE_SIZE (erase_size * 1024)
// 
// #ifdef MODULE
//@@ -62,6 +74,19 @@
// {
// }
// 
//+/*
//+ * Allow NOMMU mmap() to directly map the device (if not NULL)
//+ * - return the address to which the offset maps
//+ * - return -ENOSYS to indicate refusal to do the mapping
//+ */
//+static unsigned long ram_get_unmapped_area(struct mtd_info *mtd,
//+					   unsigned long len,
//+					   unsigned long offset,
//+					   unsigned long flags)
//+{
//+	return (unsigned long) mtd->priv + offset;
//+}
//+
// static int ram_read(struct mtd_info *mtd, loff_t from, size_t len,
// 		size_t *retlen, u_char *buf)
// {
//@@ -90,11 +115,27 @@
// {
// 	if (mtd_info) {
// 		del_mtd_device(mtd_info);
//+#if CONFIG_MTDRAM_ABS_POS > 0  ||  defined( CONFIG_SC1445x_ROMFS_XIP )
//+		iounmap(mtd_info->priv);
//+#else
// 		vfree(mtd_info->priv);
//+#endif
// 		kfree(mtd_info);
// 	}
// }
// 
//+#if 0
//+#ifdef CONFIG_MTD_PARTITIONS
//+
//+static struct mtd_partition sc1445x_mtdram_partitions = {
//+  .name = "RomFS",
//+  .size = CONFIG_MTDRAM_TOTAL_SIZE,
//+  .offset = CONFIG_MTDRAM_ABS_POS
//+};
//+
//+#endif
//+#endif
//+
// int mtdram_init_device(struct mtd_info *mtd, void *mapped_address,
// 		unsigned long size, char *name)
// {
//@@ -113,13 +154,20 @@
// 	mtd->erase = ram_erase;
// 	mtd->point = ram_point;
// 	mtd->unpoint = ram_unpoint;
//+	mtd->get_unmapped_area = ram_get_unmapped_area;
// 	mtd->read = ram_read;
// 	mtd->write = ram_write;
// 
// 	if (add_mtd_device(mtd)) {
//-		return -EIO;
//+	  printk("add_mtd_device failed\n");
//+	  return -EIO;
// 	}
// 
//+#if 0
//+#ifdef CONFIG_MTD_PARTITIONS
//+	add_mtd_partitions (mtd, &sc1445x_mtdram_partitions,1);
//+#endif
//+#endif
// 	return 0;
// }
// 
//@@ -128,6 +176,9 @@
// 	void *addr;
// 	int err;
// 
//+#if defined( CONFIG_SC1445x_ROMFS_XIP ) && ( 0 == CONFIG_MTDRAM_ABS_POS )
//+	total_size = MTDRAM_TOTAL_SIZE ;
//+#endif
// 	if (!total_size)
// 		return -EINVAL;
// 
//@@ -136,20 +187,42 @@
// 	if (!mtd_info)
// 		return -ENOMEM;
// 
//+#if CONFIG_MTDRAM_ABS_POS > 0
//+	addr = ioremap(CONFIG_MTDRAM_ABS_POS, MTDRAM_TOTAL_SIZE);
//+#elif defined( CONFIG_SC1445x_ROMFS_XIP )
//+printk( "%s: __xip_romfs_start=%p, __xip_romfs_end=%p\n", __FUNCTION__, &__xip_romfs_start, &__xip_romfs_end ) ;
//+	addr = ioremap( (unsigned)&__xip_romfs_start, MTDRAM_TOTAL_SIZE ) ;
//+#else
// 	addr = vmalloc(MTDRAM_TOTAL_SIZE);
//+#endif
//+
//+	if (!addr) {
//+#if CONFIG_MTDRAM_ABS_POS > 0
//+	  DEBUG(MTD_DEBUG_LEVEL1,
//+		"Failed to ioremap memory region of size %ld at ABS_POS:%ld\n",
//+		(long)MTDRAM_TOTAL_SIZE, (long)CONFIG_MTDRAM_ABS_POS);
//+#elif defined( CONFIG_SC1445x_ROMFS_XIP )
//+	  DEBUG(MTD_DEBUG_LEVEL1,
//+		"Failed to ioremap memory region of size %ld at %ld\n",
//+		MTDRAM_TOTAL_SIZE, (long)&__xip_romfs_start);
//+#endif
//+	}
//+
// 	if (!addr) {
// 		kfree(mtd_info);
// 		mtd_info = NULL;
// 		return -ENOMEM;
// 	}
//-	err = mtdram_init_device(mtd_info, addr, MTDRAM_TOTAL_SIZE, "mtdram test device");
//+	err = mtdram_init_device(mtd_info, addr, MTDRAM_TOTAL_SIZE, "mtdram");
// 	if (err) {
// 		vfree(addr);
// 		kfree(mtd_info);
// 		mtd_info = NULL;
// 		return err;
// 	}
//+#ifndef CONFIG_MTDRAM_ABS_POS
// 	memset(mtd_info->priv, 0xff, MTDRAM_TOTAL_SIZE);
//+#endif
// 	return err;
// }
// 
