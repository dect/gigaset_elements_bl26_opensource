/*
 * lcd_st7032.c -- lcd char driver
 *Based on the code from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.   
 *
 * 
 */

/***************       Include headers section       ***************/
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#ifdef LCD_TEST_PC
	#include <asm/uaccess.h>
#else
	#include <asm-cr16/regs.h>
	#include <asm-cr16/uaccess.h>
#endif

#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */

#include <linux/proc_fs.h> /*semaphores*/
#include <linux/cdev.h> /*cdev*/
#include <linux/wait.h>

#include <linux/delay.h>	/* delays */ 
//#include <linux/interrupt.h>/*irq*/
//#include "access_polled.h"
#include "lcd_st7032.h"
/***************       Definitions section             ***************/
#define BUFFER_SIZE (sizeof(struct LCD_ddram_read_struct)) /*Buffer size to support the ioctl with the largest data transfer*/	
#define LCD_DEFAULT_BACKLIGHT_LEVEL 0x7F
#define MAX_SCROLL_TEXT_SIZE 256

#define	START_TIMER(TIMER, TMOUT) {TIMER.expires = jiffies + TMOUT;add_timer(&TIMER);}
#define	STOP_TIMER(TIMER) {del_timer(&TIMER);}
struct timer_list scroll_timer;

typedef struct {
  	char visible_size;
  	char ScrollPos;
 	unsigned char addr;//a valid ddram address should be provided as starting address
	unsigned char length;//the number of bytes to be read	
	char visible[16];//string to be displayed
	char buf[LCD_CHARS_PER_LINE];//string to be displayed
	int enable;
}scroll_struct;

/***      Data types definitions       ***/
//#define u16 short
//Driver structs
typedef struct {	
	struct semaphore sem;     /* mutual exclusion semaphore     */
	spinlock_t lock; 
	struct cdev cdev;	  	  /* Char device structure		*/
	unsigned char LCD_buf[BUFFER_SIZE]; /*Buffer size to support the ioctl with the largest data transfer*/	
	unsigned char ver_minor;/*stores driver's minor version number*/
	unsigned char ver_major;/*stores driver's majorversion number*/
}lcd_sc14450_dev_struct;

/***      Flags definitions     ***/
/***      Hw specific definitions       ***/

/***   Macros   ***/
#define lcd_delay(X) (udelay(10*X))


	#define LCD_START_WRITE_DATA	{StartACCESSBus();I2C_8Bit_md_write(SLAVE_ADDR);I2C_8Bit_md_write(DATA_BYTES);}
	#define LCD_START_WRITE_INST	{StartACCESSBus();I2C_8Bit_md_write(SLAVE_ADDR);I2C_8Bit_md_write(CONTROL_BYTES);}
	#define LCD_WRITE(X, Y)			{I2C_8Bit_md_write(X);udelay(Y);}
	#define LCD_STOP_WRITE			{StopACCESSBus();}

	#define LCD_WRITE_INST_COMPLETE(cmd, delay_time) {LCD_START_WRITE_INST;LCD_WRITE(cmd, delay_time);LCD_STOP_WRITE}
	#define LCD_WRITE_DATA_DELAYED(cmd, delay_time) {LCD_START_WRITE_DATA;LCD_WRITE(cmd, delay_time);LCD_STOP_WRITE}

#define MIN(X,Y) ((X)>(Y))?(Y):(X)

/***      Function Prototypes       ***/
/** Low level **/
unsigned char LCD_ioctl_char(unsigned char cmd, unsigned char *arg);
unsigned char ddram_line_end_addr(unsigned char start_addr);
void LCD_init(void);
//unsigned char busy_wait(void);

/** High level **/
#ifdef LCD_SC14450_TEST
unsigned char LCD_print_string(char *str, unsigned char pos);
unsigned char LCD_testbench();
unsigned char LCD_ioctl_buf(unsigned char cmd, char *args);
unsigned char LCD_buf[sizeof(struct LCD_ddram_read_struct)];//NOTE: The size of the biggest argument passed by the user

#endif

/*Static functions*/
static void lcd_setup_cdev(lcd_sc14450_dev_struct *dev);
//Module specific
void lcd_text(void);
 int __init lcd_init_module(void);
void  lcd_cleanup_module(void);

// Fops prototypes 
int	lcd_open(struct inode *inode, struct file *filp);
int lcd_release(struct inode *inode, struct file *filp);
int lcd_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg);
void lcd_scroll_disable(void);
void lcd_scroll_enable(void);
void lcd_scroll_start(void);

static void InitializeTimer(struct timer_list *timer_ptr, void (*timer_function)(unsigned long param));
static void scroll_timer_refresh(unsigned long param);
void lcd_text(void);


/*** Parameters which can be set at load time  ***/
int lcd_sc14450_major =   LCD_SC14450_MAJOR;
int lcd_sc14450_minor =   0;


scroll_struct m_scroll_data;
  
module_param(lcd_sc14450_major, int, S_IRUGO);
module_param(lcd_sc14450_minor, int, S_IRUGO);


MODULE_AUTHOR("George Giannaras");
MODULE_LICENSE("Dual BSD/GPL");

/***      Var definitions  and initializations     ***/
struct file_operations lcd_sc14450_fops = {
	.owner =    THIS_MODULE,
	.ioctl =    lcd_ioctl,
	.open =     lcd_open,
	.release =  lcd_release,
};
struct Cmd_State_struct Cmd_State;/*Stores the last state of each command group, so that the app does not need to remember and re-issue the previous settings */
lcd_sc14450_dev_struct lcd_dev;

/***      Timer initializations     ***/
/***      Tasklet     ***/

/***************          Main body             ***************/

//*********************************************************************/
//                      ACCESS1_Init																	//
//********************************************************************//
//void ACCESS1_Init(void)
//{ 
//	SetBits(CLK_GPIO5_REG, SW_AB1_EN, 1);												// enable access1 bus Clock
//	SetBits(CLK_GPIO5_REG, SW_AB1_DIV, 16);											// 10,368MHz/8
//		
//	SetPort(P1_01_MODE_REG, PORT_OUTPUT, GPIO_PID_SCL1);  			// MDC
//	SetPort(P1_00_MODE_REG, PORT_OUTPUT, GPIO_PID_SDA1);				// MDIO
//	
//	SetBits(ACCESS1_CTRL_REG, SCK_SEL, 0x1);										// Further division of the SCL
//	SetBits(ACCESS1_CTRL_REG, SCK_NUM, 1);											// 9 bits without ACK bits are generated, SDAx Open Drain
//	SetBits(ACCESS1_CTRL_REG, SCL_OD,  0);											// Set ACCESS Bus SCL Push Pull
//	SetBits(ACCESS1_CTRL_REG, SDA_OD,  1);											// Set ACCESS Bus SDA Open Drain
//	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_INT, 1);								// enable interrupt for the access1 bus
//    	
//	SetWord(ACCESS1_CLEAR_INT_REG, 1);     											/* Clear ACCESS1 interrupt in ctrl register*/
//	
//	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 0);								/* ---  Enable ACCESS1 BUS Procedure  --- */
//	SetBits(ACCESS1_CTRL_REG, SDA_VAL, 1);											// SDA1 = 1
//	SetBits(ACCESS1_CTRL_REG, SCL_VAL, 1);											// SCL1 = 1	
//	SetBits(ACCESS1_CTRL_REG, SDA_VAL, 0);											// SDA1 = 0
//	SetBits(ACCESS1_CTRL_REG, SCL_VAL, 1);											// SCL1 = 1
//	SetBits(ACCESS1_CTRL_REG, SDA_VAL, 0);											// SDA1 = 0
//	SetBits(ACCESS1_CTRL_REG, SCL_VAL, 0);											// SCL1 = 0	
//	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 1);								// ENABLE Access1 BUS
//}
void init_ACCESS1(unsigned char freq, unsigned char bits)
{
  /* Disable Interrupts */
//  __disable_interrupt();   
#if 1	
	SetBits(CLK_GPIO5_REG, SW_AB1_EN, 1);												// enable access1 bus Clock
	SetBits(CLK_GPIO5_REG, SW_AB1_DIV, 16);	
#endif	
  SetPort(P1_15_MODE_REG, PORT_OUTPUT, PID_SCL1); // SDA1 op P1_14 AND SCL1 on P1_15
  SetPort(P1_14_MODE_REG, PORT_OUTPUT, PID_SDA1);
  
  SetBits(ACCESS1_CTRL_REG,SDA_OD, 1);              /* SDA open drain */
  SetBits(ACCESS1_CTRL_REG,SCL_OD, 0);              /* SCL as push pull */
  SetBits(ACCESS1_CTRL_REG,SCK_NUM, bits);          /* 8 or 9 Bits mode */               
  SetBits(ACCESS1_CTRL_REG,SCK_SEL, freq);           /* Clock Frequenty */                  

  SetBits(ACCESS1_CTRL_REG,EN_ACCESS_INT,1);	  //enable interrupt for the access1 bus
  SetWord(ACCESS1_CLEAR_INT_REG, 1);     		/* Clear ACCESS1 interrupt in ctrl register*/
 
#if 0
	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 0);								/* ---  Enable ACCESS1 BUS Procedure  --- */
	SetBits(ACCESS1_CTRL_REG, SDA_VAL, 1);											// SDA1 = 1
	SetBits(ACCESS1_CTRL_REG, SCL_VAL, 1);											// SCL1 = 1	
	SetBits(ACCESS1_CTRL_REG, SDA_VAL, 0);											// SDA1 = 0
	SetBits(ACCESS1_CTRL_REG, SCL_VAL, 1);											// SCL1 = 1
	SetBits(ACCESS1_CTRL_REG, SDA_VAL, 0);											// SDA1 = 0
	SetBits(ACCESS1_CTRL_REG, SCL_VAL, 0);											// SCL1 = 0	
	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 1);	
#endif	
//  SetBits(RESET_INT_PENDING_REG,ACCESS_INT_PEND,1); 	/* Clear pending bit before call i2c_device function, this generates new interrupt*/
//  SetBits(INT3_PRIORITY_REG,ACCESS_INT_PRIO, 0);
//  SetWord(RESET_INT_PENDING_REG, ACCESS_INT_PEND);   
    
  //SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 1);            /*  Enable */                  
}
//*********************************************************************/
//                      Stop Access BUS																//
//********************************************************************//
void StopACCESSBus(void)
{ 
	 /* Stop Condition */
	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 0);

	//Disable Access Bus     
	SetBits(ACCESS1_CTRL_REG,SDA_VAL, 0);											// SDA1 = 0
	SetBits(ACCESS1_CTRL_REG,SCL_VAL, 0);											// SCL1 = 0	
	SetBits(ACCESS1_CTRL_REG,SDA_VAL, 0);											// SDA1 = 0
	SetBits(ACCESS1_CTRL_REG,SCL_VAL, 1);											// SCL1 = 1
	SetBits(ACCESS1_CTRL_REG,SDA_VAL, 1);											// SDA1 = 1
	SetBits(ACCESS1_CTRL_REG,SCL_VAL, 1);											// SCL1 = 1	
}

//*********************************************************************/
//                      Start Access BUS															//
//********************************************************************//
void StartACCESSBus(void)
{ 
	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 0);							/* ---  Enable ACCESS1 BUS Procedure  --- */
	
//	SetBits(ACCESS1_CTRL_REG, SDA_VAL, 1);										// SDA1 = 1
//	SetBits(ACCESS1_CTRL_REG, SCL_VAL, 1);										// SCL1 = 1	
//	SetBits(ACCESS1_CTRL_REG, SDA_VAL, 0);										// SDA1 = 0
//	SetBits(ACCESS1_CTRL_REG, SCL_VAL, 1);										// SCL1 = 1
//	SetBits(ACCESS1_CTRL_REG, SDA_VAL, 0);										// SDA1 = 0
//	SetBits(ACCESS1_CTRL_REG, SCL_VAL, 0);										// SCL1 = 0
	
#if 1	
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,1);
	  
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);

	  
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	   
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,1);
	  
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SDA_VAL,0);
	  
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,0);
	  SetBits(ACCESS1_CTRL_REG,SCL_VAL,0);	
#endif	  
	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 1);							// ENABLE Access1 BUS

	// Clear any interrupt
	SetWord(ACCESS1_CLEAR_INT_REG, 0x1);
}

//******************************************************************//
//                      I2C_8Bit_md_write														//
//******************************************************************//
inline void I2C_8Bit_md_write(unsigned int wdata)
{
  SetWord(ACCESS1_IN_OUT_REG, wdata);												// Write byte
  while(GetBits(ACCESS1_CTRL_REG, ACCESS_INT)==0) 	{}
//  if(GetBits(ACCESS1_CTRL_REG, ACKn)==1){
//	  printk("i2c NOT ACK !!!! \n");
//  }
  SetWord(ACCESS1_CLEAR_INT_REG, 0x00a1);
    //udelay(500);  //To ensure the bus is not busy <-- Add this line.
}

//******************************************************************//
//                      I2C_8Bit_md_write_complete														//
//******************************************************************//
inline void I2C_8Bit_md_write_complete(unsigned char reg_address, unsigned int wdata)
{
	StartACCESSBus();

  SetWord(ACCESS1_IN_OUT_REG, SLAVE_ADDR);												// Write Command
  while(GetBits(ACCESS1_CTRL_REG, ACCESS_INT)==0) 	{}
  SetWord(ACCESS1_CLEAR_INT_REG, 0x00a1);
   
  SetWord(ACCESS1_IN_OUT_REG, reg_address);									// Write Register Address
  while(GetBits(ACCESS1_CTRL_REG, ACCESS_INT)==0) 	{}
  SetWord(ACCESS1_CLEAR_INT_REG, 0x00a1);

  SetWord(ACCESS1_IN_OUT_REG, wdata);												// Write Register Data
  while(GetBits(ACCESS1_CTRL_REG, ACCESS_INT)==0) 	{}
  SetWord(ACCESS1_CLEAR_INT_REG, 0x00a1);
 
	StopACCESSBus();
}
/****************************************************************************************************/
/************                                        LOW LEVEL DRIVER FUNTIONS                                                            *********/
/****************************************************************************************************/
/*****************************************************/
#define I2C_Start StartACCESSBus
#define I2C_Stop StopACCESSBus
#define I2C_out I2C_8Bit_md_write
#define Slave SLAVE_ADDR
#define Datasend DATA_BYTES
#define Comsend CONTROL_BYTES

unsigned char text1[]={"Display !!!!"};

void Show(unsigned char *text)
{
	int n, d;
	d=0x00;
	I2C_Start();
	I2C_out(Slave);
	I2C_out(Datasend);
	for(n=0;n<16;n++){
		I2C_out(*text);
		++text;
		}
	I2C_Stop();
}
/*****************************************************/
//void nextline(void)
//{
//	I2C_Start();
//	I2C_out(Slave);
//	I2C_out(Comsend);
//	I2C_out(Line2);
//	I2C_Stop();
//}
void CGRAM (void)
{
 	I2C_Start();
	I2C_out(Slave);
	I2C_out(Comsend);
	I2C_out(0x38);		//go to instructino table 0
	I2C_out(0x40);		//Set CGRAM address to 0x00
	I2C_Stop();
	lcd_delay(10);
	
	I2C_Start();
	I2C_out(Slave);
	I2C_out(Datasend);
	I2C_out(0x00);		//write to first CGRAM address
	I2C_out(0x1E);
	I2C_out(0x18);
	I2C_out(0x14);
	I2C_out(0x12);
	I2C_out(0x01);
	I2C_out(0x00);
	I2C_out(0x00);		//8 bytes per character
	//continue writing to remaining CGRAM if desired
	I2C_Stop();
}
/****************************************************
*           Initialization For ST7032i              *
*****************************************************/
void init_LCD(void)  
{
	I2C_Start();
	I2C_out(Slave);
	I2C_out(Comsend);
	I2C_out(0x38);//8bit i/f, 2 lines, instructino table 0
	lcd_delay(10);
	I2C_out(0x39);//8bit i/f, 2 lines, instructino table 1
	lcd_delay(10);
	I2C_out(0x14);//1/5 bias, freq adjust b100
	I2C_out(0x78);//Set contrast to b1000
	I2C_out(0x5E);//Power on
	I2C_out(0x6D);//Follower ON, amplifier ratio 3(lsb)
	msleep(220);
	I2C_out(0x0F);//Display ON 0x0C
	I2C_out(0x01);//Clear display
	I2C_out(0x06);//Entry mode HIgh
	
	lcd_delay(10);
	
	I2C_Stop();
	CGRAM();			//define CGRAM	
#if 1
	I2C_Start();
	I2C_out(Slave);
	I2C_out(Comsend);
	I2C_out(0x39);		
	I2C_out(0x01);		//go back Home
	I2C_Stop();
	lcd_delay(10);
#endif	
}

/******************************************************* 
	Description: 	Initializes hw and driver internal state
	Note:			Internal reset circuit is expected to initialize hw during power up. 
				Initalization sequence is performed again by sw for the case that hw initialization failed.
 ********************************************************/
void LCD_init(){

	//Initialize access bus
	init_ACCESS1(SCK_100, BITS_9);//Initialize access bus
	
	/*Processor specific commands*/
	//Set LCD reset low - high
	SetPort(P1_13_MODE_REG, PORT_OUTPUT, GPIO_PID_port);
	SetWord(P1_RESET_DATA_REG,(1<<13));	
	msleep(1);
	SetWord(P1_SET_DATA_REG,(1<<13));
	msleep(45);	
	//Turn LCD backlight on
	SetPort(P0_05_MODE_REG, PORT_OUTPUT, GPIO_PID_port);
	SetWord(P0_SET_DATA_REG,(1<<5));
	
	/*Set the default values after boot*/
	Cmd_State.entry_mode_cmd=ENTRY_MODE_CMD|ENTRY_MODE_INCREMENT;
	Cmd_State.display_cmd=DISPLAY_CMD|DISPLAY_ON;
	Cmd_State.shift_cmd=SHIFT_CMD;
	Cmd_State.function_cmd=FUNCTION_CMD|FUNCTION_8BIT;//1 line mode, IS = 0	
	Cmd_State.contrast_cmd=CONTRAST_SET_CMD|0x08;

	/*Hw initialization sequence*/
	LCD_START_WRITE_INST;
	LCD_WRITE(Cmd_State.function_cmd, SHORT_TIME);//0x38

	//Function set (once again). 
	Cmd_State.function_cmd|=FUNCTION_IS;//0x39
	LCD_WRITE(Cmd_State.function_cmd, SHORT_TIME);	

	/*IS = 1 Commands begin*/
	LCD_WRITE(INTERNAL_OSC_FREQ_CMD, SHORT_TIME);//0x14
	LCD_WRITE(Cmd_State.contrast_cmd, SHORT_TIME);//0x78
	LCD_WRITE(PIC_SET_CMD|PIC_ICON_ON|PIC_BOOST_ON|(1<<1), SHORT_TIME);//0x5E
	LCD_WRITE(FOL_CTRL_CMD|FOL_SET_FOL_ON|(0x05), SHORT_TIME);//0x6D
	LCD_STOP_WRITE;
	
	msleep(220);
	LCD_START_WRITE_INST;
	//Display on/off control
	LCD_WRITE(Cmd_State.display_cmd, SHORT_TIME);	//0x0C
	//Display clear
	LCD_WRITE(CLEAR_DISPLAY_CMD, LONG_TIME);	//0x01
	//Entry mode set
	LCD_WRITE(Cmd_State.entry_mode_cmd, LONG_TIME);//0x06
	//Function set (IS = 0). 
	Cmd_State.function_cmd &= ~FUNCTION_IS;
	LCD_WRITE(Cmd_State.function_cmd, SHORT_TIME);	
	LCD_STOP_WRITE;

	InitializeTimer(&scroll_timer, scroll_timer_refresh);

}
/******************************************************* 
	Description: 	The LCD driver ioctl commands. The commands are divided in two groups:
				-The LOW LEVEL cmds, which implement the primitive functions provided by the hw, applying 1 instruction at a time (e.g set ddram address, write a char ) . 
				-The HIGH LEVEL cmds, which combine more than 1 primitive functions at a time in order to provide a higher level functionality (e.g set ddram address AND write multiple chars ) .
	Input:		cmd: The ioctl command
				*args: Pointer to the arguments. For the LOW LEVEL cmds this is a pointer to an uchar.
									For the HIGH LEVEL cmds this is a pointer to the corresponding ioctl struct
	Returns:		0 : on success, 1: otherwise
 ********************************************************/
 unsigned char LCD_ioctl_char(unsigned char cmd, unsigned char *arg){
	//unsigned char arg0, end_addr, i;
	 //printk("IOCTL: cmd=0x%x, arg=0x%x  \n", cmd, *arg);
	switch (cmd){
/*******     LOW LEVEL COMMANDS, MAPPING THE 11 PRIMITIVE HW FUNCTIONS DESCRIBED IN THE DATASHEET  *********/	
		case LCD_init_ioctl:
			LCD_init();
			break;
	//1. Write "20h" to DDRAM and set DDRAM address (AC) to "00h"
		case LCD_clear_display:
			LCD_WRITE_INST_COMPLETE(CLEAR_DISPLAY_CMD, LONG_TIME)
			break;
	//2. Set DDRAM address (AC) to "00h" and return cursor to its original position if shifted (DDRAM contents are not changed)
		case LCD_ret_home:
			LCD_WRITE_INST_COMPLETE(RET_HOME_CMD, LONG_TIME)		
			break;
	//3. Set cursor moving direction and specify display shift, during data read and write of DDRAM and CGRAM
		case LCD_entry_mode_shift_on:
			Cmd_State.entry_mode_cmd |= ENTRY_MODE_SHIFT_ON;
			LCD_WRITE_INST_COMPLETE(Cmd_State.entry_mode_cmd, SHORT_TIME)	
			break;	
		case LCD_entry_mode_shift_off:
			Cmd_State.entry_mode_cmd &= ~ENTRY_MODE_SHIFT_ON;
			LCD_WRITE_INST_COMPLETE(Cmd_State.entry_mode_cmd, SHORT_TIME)	
			break;	
		case LCD_entry_mode_increment:
			Cmd_State.entry_mode_cmd |= ENTRY_MODE_INCREMENT;
			LCD_WRITE_INST_COMPLETE(Cmd_State.entry_mode_cmd, SHORT_TIME)			
			break;
		case LCD_entry_mode_decrement:
			Cmd_State.entry_mode_cmd &= ~ENTRY_MODE_INCREMENT;
			LCD_WRITE_INST_COMPLETE(Cmd_State.entry_mode_cmd, SHORT_TIME)	
			break;
	//4. Set display on/off, cursor on/off, cursor blinking
		case LCD_display_cursor_blink_on:
			Cmd_State.display_cmd |= DISPLAY_CURSOR_BLINK_ON;
			LCD_WRITE_INST_COMPLETE(Cmd_State.display_cmd, SHORT_TIME)				
			break;
		case LCD_display_cursor_blink_off:
			Cmd_State.display_cmd &= ~DISPLAY_CURSOR_BLINK_ON;
			LCD_WRITE_INST_COMPLETE(Cmd_State.display_cmd, SHORT_TIME)							
			break;
		case LCD_display_cursor_on:
			Cmd_State.display_cmd |= DISPLAY_CURSOR_ON;
			LCD_WRITE_INST_COMPLETE(Cmd_State.display_cmd, SHORT_TIME)							
			break;
		case LCD_display_cursor_off:
			Cmd_State.display_cmd &= ~DISPLAY_CURSOR_ON;
			LCD_WRITE_INST_COMPLETE(Cmd_State.display_cmd, SHORT_TIME)					
			break;
		case LCD_display_display_on:
			Cmd_State.display_cmd |= DISPLAY_ON;
			LCD_WRITE_INST_COMPLETE(Cmd_State.display_cmd, SHORT_TIME)					
			break;
		case LCD_display_display_off:
			Cmd_State.display_cmd &= ~DISPLAY_ON;
			LCD_WRITE_INST_COMPLETE(Cmd_State.display_cmd, SHORT_TIME)						
			break;
	//5. Move the cursor or shift the display
		case LCD_shift_screen_right:
			Cmd_State.shift_cmd |= SHIFT_SCREEN;
			Cmd_State.shift_cmd |= SHIFT_RIGHT;
			LCD_WRITE_INST_COMPLETE(Cmd_State.shift_cmd, SHORT_TIME)				
			break;
		case LCD_shift_cursor_right:
			Cmd_State.shift_cmd &= ~SHIFT_SCREEN;
			Cmd_State.shift_cmd |= SHIFT_RIGHT;
			LCD_WRITE_INST_COMPLETE(Cmd_State.shift_cmd, SHORT_TIME)			
			break;	
		case LCD_shift_screen_left:
			Cmd_State.shift_cmd |= SHIFT_SCREEN;
			Cmd_State.shift_cmd &= ~SHIFT_RIGHT;
			LCD_WRITE_INST_COMPLETE(Cmd_State.shift_cmd, SHORT_TIME)	
			break;
		case LCD_shift_cursor_left:
			Cmd_State.shift_cmd &= ~SHIFT_SCREEN;
			Cmd_State.shift_cmd &= ~SHIFT_RIGHT;
			LCD_WRITE_INST_COMPLETE(Cmd_State.shift_cmd, SHORT_TIME)	
			break;

	//6.Function set : interface(4-bit, 8-bit), display (1-line, 2-line), fonts (5x11, 5x8)
		//LCD_function_8bit, //hw dependent-not controlled by the app
		//LCD_function_4bit,//hw dependent-not controlled by the app
		case LCD_function_2lines:
			Cmd_State.function_cmd |= FUNCTION_2LINES;
			LCD_WRITE_INST_COMPLETE(Cmd_State.function_cmd, SHORT_TIME)				
			break;
		case LCD_function_1line:
			Cmd_State.function_cmd &= ~FUNCTION_2LINES;
			LCD_WRITE_INST_COMPLETE(Cmd_State.function_cmd, SHORT_TIME)							
			break;
		//LCD_function_5x11,//hw dependent-not controlled by the app
		//LCD_function_5x8,//hw dependent-not controlled by the app
	//7. Set CGRAM address in address counter
		case LCD_cgram_addr:			
			if(*(char *)arg >= CGRAM_BEGIN && *arg <= CGRAM_END){
				LCD_WRITE_INST_COMPLETE(CGRAM_ADDR_CMD| *arg, SHORT_TIME)				
				break;
			}
			return 1;
	//8. Set DDRAM address in address counter
		case LCD_ddram_addr:	
			//PDEBUG("DDram address arg: %d \n", *arg);
			if(!ddram_line_end_addr(*arg)){
				printk(KERN_WARNING "Lcd ioctl LCD_ddram_addr failed. Invalid arg %d\n", *arg);
				return 1;
			}
			LCD_WRITE_INST_COMPLETE(DDRAM_ADDR_CMD| *arg, SHORT_TIME)
			break;
	//9. Read busy flag & address. NOTE: This is for reading addr. Reading busy flag from app has no meaning since this flag is checked by the driver
//		case LCD_read_inst:
//			LCD_READ_INST_GPIO(*arg);
//			break;
	//10. Write data to RAM
		case LCD_write_data:	
			LCD_WRITE_DATA_DELAYED(*arg, SHORT_TIME)
			break;
	//11. Read data from RAM
//		case LCD_read_data:	
//			LCD_READ_DATA_GPIO(*arg);
//			break;			
	//Set lcd backlight brightness level (processor specific command)
//		case LCD_backlight_level:			
//#ifndef CONFIG_SC14452
//			LCD_BACKLIGHT_LEVEL(*arg);//duty cycle for T2_PWM1  and T2_PWM2 signal 
//#endif 
//			//PDEBUG("Set backlight to : %d \n", *arg);
//			//PDEBUG("Backlight register value : %d \n", *arg|*arg<<8);
//			break;	
		case LCD_contrast_level:
			LCD_START_WRITE_INST;	
			LCD_WRITE(FUNCTION_CMD|FUNCTION_8BIT|FUNCTION_IS, SHORT_TIME);	//0x39
			Cmd_State.contrast_cmd &= ~0x0f;//clear 4 lsbits
			Cmd_State.contrast_cmd |= (*arg & 0x0f);//set 4 lsbits
			LCD_WRITE(Cmd_State.contrast_cmd, SHORT_TIME);//0x78
			LCD_WRITE(FUNCTION_CMD|FUNCTION_8BIT, SHORT_TIME);	//0x39
			LCD_STOP_WRITE;
			break;
		case LCD_scroll_enable:			
			lcd_scroll_enable( );
			break;
		case LCD_scroll_disable:			
			lcd_scroll_disable( );
			break;
		default:
			printk(KERN_WARNING "Lcd ioctl command: 0x%x not supported \n", cmd);
			break;	

	}
	return 0;//ok
 }

 /******************************************************* 
	Description:  Checks the validity of a ddram addr 	
	Returns:	Returns the last valid ddram addr of the line where input addr lies in.	
 ********************************************************/
 unsigned char ddram_line_end_addr(unsigned char start_addr){
	unsigned char end_addr=0;
	
	if(Cmd_State.function_cmd & FUNCTION_2LINES){
		if((char)start_addr >=DDRAM_TWOLINES_LINE1_BEGIN  && start_addr <= DDRAM_TWOLINES_LINE1_END){
				end_addr=DDRAM_TWOLINES_LINE1_END;
		}
		else if(start_addr >=DDRAM_TWOLINES_LINE2_BEGIN  && start_addr <= DDRAM_TWOLINES_LINE2_END){
			end_addr=DDRAM_TWOLINES_LINE2_END;
		}
	}
	else{//device operating in 1 LINE mode
		if((char)start_addr >=DDRAM_ONELINE_BEGIN  && start_addr <= DDRAM_ONELINE_END){
			end_addr=DDRAM_ONELINE_END;
		}				
	}
	return end_addr;
 }
 /******************************************************* 
	Description:  Checks the busy flag and halts until it is cleared
	Returns:	Returns the last read byte.	
 ********************************************************/
//unsigned char busy_wait(){
//	unsigned char e_flag, read_cnt=0, max_read = 2; //THIS SHOULD BE 1!! .. max_read is used for safety in order to exit loop if something goes wrong
//
//	SET_LCD_READ();
//	LCD_READ_INST(e_flag);
//	while( (e_flag & (1<<7)) && (read_cnt++ < max_read) ){
//		lcd_delay(SHORT_TIME);	
//		LCD_READ_INST(e_flag);
//	}
//	SET_LCD_WRITE();	
//
//	//NOTE: The following printk adds an extra delay which is needed ONLY IF the lcd_delay() in the previous while loop is removed.
//	if(read_cnt >= max_read){
//		printk(KERN_WARNING "lcd_sc14450: Busy flag read on: %d times\n", read_cnt);	
//	}
//	return(e_flag);
//}

/****************************************************************************************************/
/************                                       HIGH LEVEL DRIVER FUNTIONS                                                            *********/
/****************************************************************************************************/

/******************************************************* 
	Description: 	Open lcd dev file.

 ********************************************************/
int lcd_open(struct inode *inode, struct file *filp)
{
	lcd_sc14450_dev_struct *dev; /* device information */
	dev = container_of(inode->i_cdev, lcd_sc14450_dev_struct, cdev);
	filp->private_data = dev; /* for other methods */
	return 0;
}
/******************************************************* 
	Description: 	Close device file.

 ********************************************************/
int lcd_release(struct inode *inode, struct file *filp)
{
	return 0;
}

/******************************************************* 
	Description: 	Kbd ioctl.

 ********************************************************/
int lcd_ioctl(struct inode *inode, struct file *filp,
                 unsigned int cmd, unsigned long arg)
{
	int retval=0;
	unsigned char end_addr, i=0, j, min;
	struct LCD_ddram_write_struct *ddram_w;
	//struct LCD_ddram_read_struct * ddram_r; 	
	lcd_debug_drv_struct drv_debug;		
	lcd_sc14450_dev_struct *dev = filp->private_data;

//	PDEBUG("LCD ioctl: cmd=%d, arg= %d \n", cmd, arg);
	if(cmd == LCD_read_inst || cmd == LCD_read_data){//these ioctls return data to user
		if (down_interruptible(&dev->sem))
			return -ERESTARTSYS; 
		retval=LCD_ioctl_char(cmd, dev->LCD_buf);		
		if(copy_to_user((char*)arg, dev->LCD_buf, sizeof(char))){		
			printk(KERN_WARNING "Copy from user failed\n");
			up (&dev->sem);
			return -EFAULT;
		}
		up (&dev->sem);
		return retval;
	}	
	if(cmd < LCD_IOCTL_HIGH){
		if (down_interruptible(&dev->sem))
			return -ERESTARTSYS; 
		if(copy_from_user(dev->LCD_buf, (char*)&arg, sizeof(char) )){
			printk(KERN_WARNING "Copy from user failed\n");
			up (&dev->sem);
			return -EFAULT;
		}

		retval=LCD_ioctl_char(cmd, dev->LCD_buf);		
		if(cmd == LCD_read_inst || cmd == LCD_read_data){//these ioctls return data to user
			if(copy_to_user((char*)&arg, dev->LCD_buf, sizeof(char))){		
				printk(KERN_WARNING "Copy from user failed\n");
				up (&dev->sem);
				return -EFAULT;
			}
			//PDEBUG("LCD ioctl read: copied to user = %c \n", dev->LCD_buf[0]);
		}
		up (&dev->sem);
		return retval;
	}
 	if(cmd < LCD_IOCTL_MAX){
		if (down_interruptible(&dev->sem))
			return -ERESTARTSYS; 
		switch (cmd){
					/*******     HIGH LEVEL COMMANDS, COMBINING MULTIPLE  PRIMITIVE HW FUNCTIONS  *********/				
			case LCD_ddram_write_string:
				if(copy_from_user(dev->LCD_buf, (char*)arg, 2*sizeof(char) )){//copy only addr and length first
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}			
 				ddram_w=(struct LCD_ddram_write_struct*)(dev->LCD_buf); 
// 				if(ddram_w->addr == DDRAM_DUMMY_CONT){
//					LCD_READ_INST_GPIO(ddram_w->addr);
//					ddram_w->addr &= ~BF_READ_CMD;//mask out the busy flag
//					//PDEBUG("DDram address read: %d \n", ddram_w->addr);
//				}
 				end_addr=ddram_line_end_addr(ddram_w->addr);
				if(!end_addr){
					up (&dev->sem);
					return -EINVAL;	
				}
				if(copy_from_user(ddram_w->buf, (char*)(arg + 2*sizeof(char)), ddram_w->length )){//copy only addr and length first
					printk(KERN_WARNING "Copy from user failed\n");
					up (&dev->sem);
					return -EFAULT;
				}				
  
 				//edo
				if (m_scroll_data.enable)
				{
					m_scroll_data.enable=0;
					m_scroll_data.addr = ddram_w->addr;
					m_scroll_data.length = ddram_w->length;
 
					memset((char*)m_scroll_data.buf ,0, LCD_CHARS_PER_LINE );
   					memcpy((char*)m_scroll_data.buf ,(char*)ddram_w->buf, ddram_w->length);
 					lcd_scroll_start();

				}else{
					LCD_WRITE_INST_COMPLETE(DDRAM_ADDR_CMD| ddram_w->addr, SHORT_TIME);
					i=0;
					min=MIN(ddram_w->length, (end_addr-ddram_w->addr+1));
					LCD_START_WRITE_DATA;
					for(i=0;i<min;i++) LCD_WRITE(ddram_w->buf[i], SHORT_TIME);
					LCD_STOP_WRITE;
 				}

				up (&dev->sem);	
				return i;				
//			case LCD_ddram_read_buf://reads ddram_r->length bytes from address ddram_r->addr
//				if(copy_from_user(dev->LCD_buf, (char*)arg, 2*sizeof(char) )){//copy only addr and length first
//					printk(KERN_WARNING "Copy from user failed\n");
//					up (&dev->sem);
//					return -EFAULT;
//				}			
//				//PDEBUG("LCD ioctl read buf\n");
//				ddram_r=(struct LCD_ddram_read_struct*)(dev->LCD_buf); 
//				end_addr=ddram_line_end_addr(ddram_r->addr);
//				if(!end_addr){
//					up (&dev->sem);
//					return -EINVAL;	
//				}				
//				LCD_WRITE_INST_COMPLETE(DDRAM_ADDR_CMD| ddram_r->addr, SHORT_TIME);				
//				if(ddram_r->addr + ddram_r->length > end_addr)
//					ddram_r->length = end_addr - ddram_r->addr + 1;
//				for(i=0;i<ddram_r->length;i++){
//					LCD_READ_DATA_GPIO(ddram_r->buf[i]);
//					//lcd_delay(SHORT_TIME);
//				}
//				if(copy_to_user((char*)(arg + 2*sizeof(char)), ddram_r->buf, ddram_r->length )){//copy only addr and length first
//					printk(KERN_WARNING "Copy from user failed\n");
//					up (&dev->sem);
//					return -EFAULT;
//				}					
//				up (&dev->sem);	
//				return i;	
			case LCD_cgram_write_buf://writes the entire CGRAM table
				LCD_WRITE_INST_COMPLETE(CGRAM_ADDR_CMD| CGRAM_BEGIN, SHORT_TIME);
				i=0;
				min=MIN(BUFFER_SIZE, (CGRAM_END-CGRAM_BEGIN-i+1));
				while(min){
					if(copy_from_user(&dev->LCD_buf[0], (char*)(arg +i), min )){//copy only addr and length first
						printk(KERN_WARNING "Copy from user failed\n");
						up (&dev->sem);
						return -EFAULT;
					}
					j=0;
					LCD_START_WRITE_DATA;
					while(j<min){			
						LCD_WRITE(dev->LCD_buf[j++], SHORT_TIME);
					}	
					LCD_STOP_WRITE;
					i+=min;
					min=MIN(BUFFER_SIZE, (CGRAM_END-CGRAM_BEGIN-i+1));					
				}		
				up (&dev->sem);					
				return i;	
			case LCD_ioctl_drv_info_get:
				drv_debug.ver_major = dev->ver_major;
				drv_debug.ver_minor = dev->ver_minor;
				if(copy_to_user((char*)arg, (char*)&drv_debug, sizeof(lcd_debug_drv_struct))){
					printk("Ioctl failed \n");
					up (&dev->sem);
					return -EFAULT;
				}
				up (&dev->sem);	
				return 0;					
		}
	}
		
/**** DEBUGGING IOCTLS ****/		
#ifdef LCD_SC14450_DEBUG	
#endif
	printk(KERN_WARNING "Invalid lcd ioctl \n");
	return -ENOTTY;

}

/******************************************************* 
	Description: 	Module initialization.

 ********************************************************/
int __init lcd_init_module(void)
{
	int result;
	dev_t dev = 0;

	/* Initializations*/
	PDEBUG("module init function, entry point\n");
/*
 * Get a range of minor numbers to work with, asking for a dynamic
 * major unless directed otherwise at load time.
 */
	if (lcd_sc14450_major) {
		dev = MKDEV(lcd_sc14450_major, lcd_sc14450_minor);
		result = register_chrdev_region(dev, 1, "lcd_sc14450");
	} 
	else {
		result = alloc_chrdev_region(&dev, lcd_sc14450_minor, 1, "lcd_sc14450");
		lcd_sc14450_major = MAJOR(dev);
	}
	if (result < 0) {
		printk(KERN_WARNING "lcd_sc14450: can't get major %d\n", lcd_sc14450_major);
		return result;
	}
	/*Init hw*/
	LCD_init();
#ifdef LCD_SC14450_TEST
	LCD_testbench();
#endif	
	/*Initialize driver internal vars*/
	memset((char*)&lcd_dev, 0, sizeof(lcd_sc14450_dev_struct));	
	lcd_dev.ver_major=LCD_SC14450_MAJ_VER;
	lcd_dev.ver_minor=LCD_SC14450_MIN_VER;	
	init_MUTEX(&lcd_dev.sem);
	spin_lock_init(&lcd_dev.lock);
	lcd_setup_cdev(&lcd_dev);	
	/*Enable kbe int if needed*/
	printk(KERN_NOTICE "lcd_sc14450: driver initialized with major devno: %d\n", lcd_sc14450_major);

#ifdef LCD_SC14450_DEBUG
	//Blink cursor
			Cmd_State.display_cmd |= DISPLAY_CURSOR_BLINK_ON;
			LCD_WRITE_INST_COMPLETE(Cmd_State.display_cmd, SHORT_TIME)	
	//2 lines
			Cmd_State.function_cmd |= FUNCTION_2LINES;
			LCD_WRITE_INST_COMPLETE(Cmd_State.function_cmd, SHORT_TIME)
	//Dsiplay on
			Cmd_State.display_cmd |= DISPLAY_ON;
			LCD_WRITE_INST_COMPLETE(Cmd_State.display_cmd, SHORT_TIME)	
	
#endif

	return 0; /* succeed */

//  fail:
//	unregister_chrdev_region(dev, 1);
//	return result;
}

/******************************************************* 
	Description: 	Cleanup the module and handle initialization failures .

 ********************************************************/
void lcd_cleanup_module(void)
{
	dev_t devno = MKDEV(lcd_sc14450_major, lcd_sc14450_minor);

	/* Get rid of our char dev entries */
	cdev_del(&lcd_dev.cdev);
	printk(KERN_NOTICE "lcd_sc14450: cleanup module\n");
	/* cleanup_module is never called if registering failed */
	unregister_chrdev_region(devno, 1);

}

/******************************************************* 
	Description: 	Set up the cdev structure for this device.

 ********************************************************/
void lcd_setup_cdev(lcd_sc14450_dev_struct *dev)
{
	int err, devno = MKDEV(lcd_sc14450_major, lcd_sc14450_minor);
    
	cdev_init(&dev->cdev, &lcd_sc14450_fops);
	dev->cdev.owner = THIS_MODULE;
	dev->cdev.ops = &lcd_sc14450_fops;
	err = cdev_add (&dev->cdev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE "Error %d adding lcd_sc14450", err);
}

module_init(lcd_init_module);
module_exit(lcd_cleanup_module);



void lcd_scroll_disable(void)
{
 	STOP_TIMER(scroll_timer);

}
void lcd_scroll_enable(void)
{
 	m_scroll_data.enable=1;
}

void lcd_scroll_start(void)
{
 	STOP_TIMER(scroll_timer);
  	m_scroll_data.ScrollPos = 0;
	m_scroll_data.visible_size=14;
 	memset(m_scroll_data.visible, 0 , m_scroll_data.visible_size);
	strncpy(m_scroll_data.visible, m_scroll_data.buf, m_scroll_data.visible_size);

 	// add a space at the end of the text
	strcat(m_scroll_data.buf, " ");
	m_scroll_data.length++;
 
	lcd_text( );
	START_TIMER(scroll_timer, 500);
 }

static void InitializeTimer(struct timer_list *timer_ptr, void (*timer_function)(unsigned long param))
{
	m_scroll_data.enable=0;
 	init_timer(timer_ptr);	
	timer_ptr->data = 0; //no argument for kbd_timer_fn needed		
	timer_ptr->function = timer_function;
}
 
static void scroll_timer_refresh(unsigned long param)
{
 	int i;
 	m_scroll_data.buf[m_scroll_data.length] = m_scroll_data.buf[0];
  	for(i=0 ; i < m_scroll_data.length ; i++)
		m_scroll_data.buf[i] = m_scroll_data.buf[i+1];
 
	//	m_scroll_data.buf[m_scroll_data.length] = '\0';
 	memcpy(m_scroll_data.visible, m_scroll_data.buf,m_scroll_data.visible_size);
   	lcd_text( );
  	START_TIMER(scroll_timer, 80);
}

 void lcd_text(void)
 {
	 struct LCD_ddram_write_struct *ddram_w;
 	unsigned char end_addr=0;
	int i, min;
 		  
	ddram_w=(struct LCD_ddram_write_struct*)(m_scroll_data.visible); 

//	if(m_scroll_data.addr == DDRAM_DUMMY_CONT){
//		LCD_READ_INST_GPIO(m_scroll_data.addr);
//		m_scroll_data.addr &= ~BF_READ_CMD;//mask out the busy flag
//		//PDEBUG("DDram address read: %d \n", ddram_w->addr);
//	}

	end_addr=ddram_line_end_addr(m_scroll_data.addr);
	if(!end_addr){
		return ;	
	}
	LCD_WRITE_INST_COMPLETE(DDRAM_ADDR_CMD| m_scroll_data.addr, SHORT_TIME);
	i=0;
	min=MIN(m_scroll_data.visible_size, (end_addr-m_scroll_data.addr+1));
	LCD_START_WRITE_DATA;
	for(i=0;i<min;i++){			
		LCD_WRITE(m_scroll_data.visible[i], SHORT_TIME);
	}	
	LCD_STOP_WRITE;

}
