// This is the header file for the experimental lcd driver for SC14450 developoment board
// 			Supported displays:
// LCD:  TOPWAY LMB162AFC (2x16 char display) ---- using lcd driver ST7066U 
// LCD:  TOPWAY LMB202EEC (2x20 char display) ---- using lcd driver ST7066U 

/* 
	Driver interface description
	LCD		BOARD
	---------------------------
	RS		->        AD7
	DB[0..7]	->	DAB[0..7]
*/
#ifndef LCD_ST7066U_H 
#define LCD_ST7066U_H 
/***************       Definitions section             ***************/
/* HW dependent definitions */
// #define TOPWAY_LMB202ECC
// #ifdef TOPWAY_LMB162AFC
	// #define LCD_CHARS_PER_LINE 16
// #endif		
// #ifdef TOPWAY_LMB202ECC
	// #define LCD_CHARS_PER_LINE 20
// #endif	

#define LCD_CHARS_PER_LINE  256
 
#define LCD_BUSY_WAIT_DELAY 2 //This delay is used in busy_wait delay. The value was found by experimenting with a testbench app.
//#define IOEXP 1 //This mode is used for hw which utilizes IOEXP 

/* Registers */
//Since RS -> AD7, data are accessed at 0x80 and instructions are accessed at 0x00
#ifdef CONFIG_RAM_SIZE_32MB
#define LCD_BASE 0x1080000 //0x01300000
#else
#define LCD_BASE 0x01300000
#endif
#define LCD_DR (LCD_BASE+0x80)
#define LCD_IR (LCD_BASE+0x00)

/* READ/WRITE implementation*/
#define WRITE_CMD 	0x01
#define READ_CMD 	0x02

/***      Character code map definitions       ***/
//CGRAM (Character Generator RAM) - 8 codes (address space) - 16 addresses (aliased)
//NOTE: The driver considers valid only the 0x08-0x0F space, in order to restrict using addr 0x00 which can be confused with \0 null char in LCD_write_ddram_string ioctl
#define CGRAM_CODE_MIN 	0x08 
#define CGRAM_CODE_MAX	0x0F //code 0x08 equals code 0x00, code 0x09 equals code 0x01, etc
//CGROM (Character Generator ROM) 
#define CGROM_CODE_MIN 	0x10 
#define CGROM_CODE_MAX	0xFF
//DDRAM (Display Data RAM)

/***      Memory map definitions       ***/
//CGRAM address space
#define CGRAM_BEGIN 			0x00 
#define CGRAM_END		 		0x3F 
#define CGRAM_ADDR_MASK 		CGRAM_END 
//DDRAM address space
#define DDRAM_ONELINE_BEGIN 	0x00 
#define DDRAM_ONELINE_END		0x4F 

#define DDRAM_TWOLINES_LINE1_BEGIN 	0x00 
#define DDRAM_TWOLINES_LINE1_END	0x27 
#define DDRAM_TWOLINES_LINE2_BEGIN 	0x40 
#define DDRAM_TWOLINES_LINE2_END	0x67 
#define DDRAM_DUMMY_CONT			0xFF //dummy addr used by LCD_ddram_write_string ioctl, to continue from last position

/***      Instruction set definitions       ***/
/*** According to LMB162AFC datasheet ***/
//Timers
#define LONG_TIME 1600 // make this equal to 1,52 ms
#define SHORT_TIME 45 // make this equal to 37 us

//WRITE Commands
//1. Write "20h" to DDRAM and set DDRAM address (AC) to "00h"
#define CLEAR_DISPLAY_CMD 					(1<<0) // wait LONG_TIME 
//2. Set DDRAM address (AC) to "00h" and return cursor to its original position if shifted (DDRAM contents are not changed)
#define RET_HOME_CMD 						(1<<1) //wait LONG_TIME

//3. Set cursor moving direction and specify display shift, during data read and write of DDRAM and CGRAM
#define ENTRY_MODE_CMD						(1<<2)// wait SHORT_TIME
#define ENTRY_MODE_SHIFT_ON					(1<<0)//screen shifting
#define ENTRY_MODE_INCREMENT				(1<<1)//ID=1 or AC=AC+1 
//4. Set display on/off, cursor on/off, cursor blinking
#define DISPLAY_CMD			(1<<3) // wait SHORT_TIME
#define DISPLAY_CURSOR_BLINK_ON	(1<<0)//cursor blinkning on
#define DISPLAY_CURSOR_ON	(1<<1)//cursor on
#define DISPLAY_ON			(1<<2)//display on
//5. Move the cursor or shift the display
#define SHIFT_CMD				(1<<4)// wait SHORT_TIME
#define SHIFT_SCREEN			(1<<3)//else shift cursor
#define SHIFT_RIGHT				(1<<2)//else left
//6.Function set : interface(4-bit, 8-bit), display (1-line, 2-line), fonts (5x11, 5x8)
#define FUNCTION_CMD		(1<<5)// wait SHORT_TIME
#define FUNCTION_8BIT		(1<<4)//else 4-bit interface
#define FUNCTION_2LINES		(1<<3)//else 1-line display
#define FUNCTION_5x11		(1<<2)//else 5x8 dots font
//7. Set CGRAM address in address counter
#define CGRAM_ADDR_CMD		(1<<6)// wait SHORT_TIME
//8. Set DDRAM address in address counter
#define DDRAM_ADDR_CMD		(1<<7)// wait SHORT_TIME

//READ Commands
//8. Read busy flag and address
#define BF_READ_CMD				(1<<7)// no wait time
#define BF_READ_TIME			(0)

//Processor specific commands
#define LCD_BACKLIGHT_LEVEL(X)  (TIMER2_DUTY_REG=X|X<<8)

//Kernel MACROs
// #define LCD_WRITE_DATA(x)	(*(volatile unsigned char *)(LCD_DR)=(x))// wait SHORT_TIME
// #define LCD_READ_DATA(x)		(x=*(volatile unsigned char *)(LCD_DR))// wait SHORT_TIME
// #define LCD_WRITE_INST(x)	(*(volatile unsigned char *)(LCD_IR)=(x))
// #define LCD_READ_INST(x)		(x=*(volatile unsigned char *)(LCD_IR))

//IOCTL Commands
typedef enum IOCTL_Cmd_enum {
/*******     LOW LEVEL COMMANDS, MAPPING PRIMITIVE HW FUNCTIONS  *********/	
//These commands are a one to one mapping of the ST7066U controller commands as described in the datasheet.
//1. Write "20h" to DDRAM and set DDRAM address (AC) to "00h"
	LCD_clear_display,//2. Set DDRAM address (AC) to "00h" and return cursor to its original position if shifted (DDRAM contents are not changed)
	LCD_ret_home,//3. Set cursor moving direction and specify display shift, during data read and write of DDRAM and CGRAM
	LCD_entry_mode_shift_on,
	LCD_entry_mode_shift_off,
	LCD_entry_mode_increment,
	LCD_entry_mode_decrement,//4. Set display on/off, cursor on/off, cursor blinking
	LCD_display_cursor_blink_on,
	LCD_display_cursor_blink_off,
	LCD_display_cursor_on,
	LCD_display_cursor_off,
	LCD_display_display_on,
	LCD_display_display_off,//5. Move the cursor or shift the display
	LCD_shift_screen_right,
	LCD_shift_cursor_right,
	LCD_shift_screen_left,
	LCD_shift_cursor_left,//6.Function set : interface(4-bit, 8-bit), display (1-line, 2-line), fonts (5x11, 5x8)	//LCD_function_8bit, //hw dependent-not controlled by the app	//LCD_function_4bit,//hw dependent-not controlled by the app
	LCD_function_2lines,
	LCD_function_1line,	//LCD_function_5x11,//hw dependent-not controlled by the app	//LCD_function_5x8,//hw dependent-not controlled by the app//7. Set CGRAM address in address counter
	LCD_cgram_addr,//8. Set DDRAM address in address counter
	LCD_ddram_addr,	//9. Read busy flag & address. NOTE: This is for reading addr. Reading busy flag from app has no meaning since this flag is checked by the driver	
	LCD_read_inst,	//10. Write data to RAM
	LCD_write_data,	//11. Read data from RAM
	LCD_read_data,	//Set lcd backlight brightness level (processor specific command)
	LCD_backlight_level,/*******     HIGH LEVEL COMMANDS, COMBINING MULTIPLE  PRIMITIVE HW FUNCTIONS  *********/	
	LCD_scroll_enable,	 
	LCD_scroll_disable,			
	LCD_IOCTL_HIGH,//NOT an ioctl cmd. It is a delimiter used to separate "low" level from "high" level cmds
	LCD_ddram_write_string,//writes a a specified number of bytes to the lcd
	LCD_ddram_read_buf,//reads a specified number of bytes from the DDRAM 
	LCD_cgram_write_buf,//writes the entire CGRAM table
	LCD_ioctl_drv_info_get,//returns the version of the driver
 	LCD_IOCTL_MAX//this is a delimiter
} IOCTL_Cmd;
 
//Commands states (used to store the previously issued cmds)
struct Cmd_State_struct {
	unsigned char entry_mode_cmd;
	unsigned char display_cmd;
	unsigned char shift_cmd;
	unsigned char function_cmd;
};

struct LCD_ddram_write_struct {
	unsigned char addr;//a valid ddram address should be provided as starting address
	unsigned char length;//the number of bytes to be read	
	char buf[LCD_CHARS_PER_LINE];//string to be displayed
};
//NOTE: This is the biggest struct passed by the user. Otherwise update driver's LCD_buf size
struct LCD_ddram_read_struct {
	unsigned char addr;//a valid ddram address should be provided as starting address
	unsigned char length;//the number of bytes to be read
	char buf[LCD_CHARS_PER_LINE];//buffer where data will be stored
};
//Used for debugging
typedef struct lcd_debug_drv {
	unsigned char ver_major;
	unsigned char ver_minor;
}lcd_debug_drv_struct;

/****************************************************************************/
/**************           HW INDEPENDENT DEFINITIONS                    ***************/
/****************************************************************************/
/*
 * Macros to help debugging
 */
//#define LCD_SC14450_TEST 
#define LCD_SC14450_DEBUG
#undef PDEBUG             /* undef it, just in case */
#ifdef LCD_SC14450_DEBUG
#  ifdef __KERNEL__
     /* This one if debugging is on, and kernel space */
#    define PDEBUG(fmt, args...) printk( KERN_DEBUG "lcd_sc14450: " fmt, ## args)
#  else
     /* This one for user space */
#    define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif

#undef PDEBUGG
#define PDEBUGG(fmt, args...) /* nothing: it's a placeholder */

#define LCD_SC14450_MAJOR 251
#ifndef LCD_SC14450_MAJOR
#define LCD_SC14450_MAJOR 0   /* dynamic major by default */
#endif

#ifndef LCD_SC14450_NR_DEVS
#define LCD_SC14450_NR_DEVS 1    
#endif

#define LCD_SC14450_MAJ_VER 0
#define LCD_SC14450_MIN_VER 2

#endif
