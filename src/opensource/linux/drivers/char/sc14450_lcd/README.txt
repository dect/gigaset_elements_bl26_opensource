SC14450 CHAR LCD DRIVER


version 0.2
---------------------------------------------------------------------
-Added a max in busy_wait() loop in order to protect system from halting if busy flag never clears. 
-Moved cmd delays from busy_wait() to the macros which handle READ and WRITE cmds to lcd driver.
Note: 
The busy_flag read may fail due to noise. Turning the backlight off, reduces noise and eliminates busy flag re-reads.
Each busy_flag re-read adds a 50us delay (takes up cpu time). 


version 0.1
---------------------------------------------------------------------
-Driver creation