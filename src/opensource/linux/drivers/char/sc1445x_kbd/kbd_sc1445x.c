/*
 * kbd_sc1445x.c -- keypad char driver
 *Based on the code from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.
 *
 *
 */

/***************       Include headers section       ***************/

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#ifdef KBD_TEST_PC
	#include <asm/uaccess.h>
#else
	#include <asm-cr16/regs.h>
	#include <asm-cr16/uaccess.h>
#endif

#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */

#include <linux/proc_fs.h> /*semaphores*/
#include <linux/cdev.h> /*cdev*/
#include <linux/wait.h> /*kbd_sc1445x_dev_struct*/
#include <linux/interrupt.h>/*irq*/
#include <linux/spinlock.h>
#include <linux/poll.h>
#include <linux/delay.h>
#include "kbd_sc1445x.h"

/***************       Definitions section             ***************/

/***      Data types definitions       ***/
//#define u16 short
//Driver structs
typedef struct {
	struct keypad_sc1445x_conf_struct kbd_conf;
	unsigned short key_board_int_reg;/*stores the reg contents for enabling kbd intr*/
	struct semaphore sem;     /* mutual exclusion semaphore     */
    spinlock_t lock;          /* For mutual exclusion */
	struct cdev cdev;	  	  /* Char device structure		*/
	kbd_msg *buffer;
	unsigned short buffersize;
    unsigned char rp, wp;   /* begin of buf, end of buf */
    wait_queue_head_t inq;  /* read and write queues */
	unsigned char ver_minor;/*stores driver's minor version number*/
	unsigned char ver_major;/*stores driver's major version number*/
}kbd_sc1445x_dev_struct;

typedef struct {
	unsigned short state; //PRESS, RELEASE, REPEAT etc
	unsigned short flags;
	unsigned char pre_col;
	unsigned char pre_row;
	unsigned char tmout; //the time key is pressed
}key_scan_struct;

typedef struct {
	unsigned short state; //PRESS, RELEASE, DEBOUNCE etc
	unsigned short flags;
	unsigned char tmout; //the time key is pressed
}extra_key_scan_struct;

typedef struct {
//	unsigned short masks[2];//the led toggle masks are stored here
//	unsigned char tmouts[2]; //the timer for each toggle mask
	unsigned char index;
	unsigned char tmout; //the time remaing for next toggle
}led_toggle_struct;

typedef struct {
	/* Registers */
	volatile unsigned short *kbd_pio_stat_reg;
	unsigned short kbd_pio_id;
}keypad_rw_struct;

/***      Flags definitions     ***/
//key_scan FSM state
#define KBD_IDLE_STATE				(0x01)
#define KBD_PRESS_DEBOUNCE_STATE	(0x02)
#define KBD_PRESS_STATE				(0x03)
#define KBD_REPEAT_SHORT_STATE		(0x04)
#define KBD_WAIT_RELEASE_STATE		(0x05)
#define KBD_CHECK_FLASH_STATE		(0x06)

//key_scan flags
#define SAME_KEY	(0x01)
//extra_key_scan flags
#define EXTRA_KEY_PRESSED	(0x01)
//the total amount of keys that sohuld be concurrently scanned .
#define MAX_MULT_KEYS 2

volatile unsigned short *gpio_data_reg[3]={&P0_DATA_REG, &P1_DATA_REG, &P2_DATA_REG};
volatile unsigned short *gpio_set_reg[3]={&P0_SET_DATA_REG, &P1_SET_DATA_REG, &P2_SET_DATA_REG};
volatile unsigned short *gpio_reset_reg[3]={&P0_RESET_DATA_REG, &P1_RESET_DATA_REG, &P2_RESET_DATA_REG};
volatile unsigned short *gpio_mode_reg[3]={&P0_00_MODE_REG, &P1_00_MODE_REG, &P2_00_MODE_REG};

//unsigned char row_config[KBD_ROWS][3];

/* Foxconn remove start, Vincent Chen, 08/19/2009
#ifdef F_G2_P2_2PORT_BOARD
	unsigned char col_config[KBD_COLUMNS][2];
	unsigned char led_config[TOTAL_LEDS][2];
#endif
Foxconn remove end */

/***      Hw specific definitions       ***/

#ifdef CONFIG_RAM_SIZE_32MB
//#define KBD_BASE_ADDR 0x01090000
//#define ASM_KBD_BASE_ADDR_VAL (KBD_BASE_ADDR)
//#define ASM_KBD_PRE_DEL_VAL 2
//#define ASM_KBD_POST_DEL_VAL 2
//#define ASM_32MB_CS_VAL 0xa
//#define ASM_16MB_CS_VAL 0x9
//TODO MAKE THE CS REGISTER ALSO SELECTABLE
#define set_col(X,ASM_KBD_BASE_ADDR,ASM_KBD_PRE_DEL,ASM_KBD_POST_DEL,ASM_32MB_CS,ASM_16MB_CS) \
 ({\
   register short __r4 __asm__ ("r4") = (short)(X) ; \
   asm volatile(\
       "sprd psr,(r3,r2) /*save the interrupts*/ \n\t"\
       "di     /*disable interrupts*/  \n\t"\
       "nop \n\t"\
       "movd %5,(r1,r0)\n\t /*small delay*/  \n\t"\
       "1: addd $-1:s,(r1,r0) \n\t"\
       "bcs 1b\n\t"\
       "movd %3,(r1,r0) /*change to 16 MB mode*/  \n\t"\
       "nop \n\t"\
       "nop \n\t"\
       "stord (r1,r0),0x0ff0064 \n\t"\
       "movd %2,(r1,r0) \n\t"\
       "storw r4,0x0:(r1,r0) /*write value to col*/  \n\t"\
       "nop \n\t"\
       "movd %6,(r1,r0)    /*small delay*/  \n\t"\
       "2: addd $-1:s,(r1,r0) \n\t"\
       "bcs 2b\n\t"\
       "movd %4,(r1,r0) /*restore to 32 MB mode*/  \n\t"\
       "stord (r1,r0),0x0ff0064 \n\t"\
       "lprd (r3,r2), psr /*restore to interrupts*/  \n\t"\
       "nop \n\t"\
       "nop \n\t"\
       :"=r" (__r4) \
       : "0" (__r4),"i" (ASM_KBD_BASE_ADDR),"i" (ASM_16MB_CS),"i" (ASM_32MB_CS),"i" (ASM_KBD_PRE_DEL),"i" (ASM_KBD_POST_DEL) \
       : "r0","r1","r2","r3"); \
  })
#endif


#ifdef CONFIG_RPS_BOARD
	#define SCAN_ROUTINE_INVERT_LOGIC
	#ifndef CONFIG_L_V1_BOARD
	#define LEDS_MUX_COLS // Leds are multiplexed with columns
	#endif
	#define SET_COL_GPIO(X) {set_col_gpio(X);};

	#define SET_COLUMNS(X) SET_COL_GPIO(X)//{P0_SET_DATA_REG=((X)&0x3F);P0_RESET_DATA_REG=((~(X))& 0x3F);}
	#define EXTRA_KEY_CHECK(X) (BAT_STATUS_REG & PON_STS)
	#define EXTRA_KEY_CLEAR (BAT_STATUS_REG &= ~PON_STS)

	/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
	unsigned char row_config[KBD_ROWS][3] = {
		{1, 0, 4},//P100, INT4
		{1, 1, 5},//P101, INT5
		{1, 2, 0},//P102, INT0
		{0, 8, 2},//P008, INT2
		{0, 9, 3},//P009, INT3
	};

	/*For GPIO_X_YY com_config[i]={X, YY}*/
	unsigned char col_config[KBD_COLUMNS][2] = {
		{0, 0},//P000
		{0, 1},//P001
		{0, 2},//P002
		{0, 3},//P003
		{0, 4},//P004
		{0, 5},//P005
	};

#elif defined (CONFIG_F_G2_BOARDS)
	#define SCAN_ROUTINE_NORMAL
	#if defined (CONFIG_F_G2_P1_BOARD)
		#define LED_BASE_ADDR 0x01500000
		#define LED_WR_BUF   (*(unsigned short *)(LED_BASE_ADDR))
		#define KBD_BASE_ADDR 0x01400000
		#define COL_WR_BUF   (*(unsigned short *)(KBD_BASE_ADDR))

		//#define ROW_RD_BUF   (*(unsigned short *)(KBD_BASE_ADDR + 0x100))
		#define SET_LEDS(X) {LED_WR_BUF=X};
		#ifdef CONFIG_RAM_SIZE_32MB
			#define ASM_KBD_BASE_ADDR_VAL (KBD_BASE_ADDR)
			#define ASM_KBD_PRE_DEL_VAL 2
			#define ASM_KBD_POST_DEL_VAL 2
			#define ASM_32MB_CS_VAL 0xa
			#define ASM_16MB_CS_VAL 0x9
			#define SET_COLUMNS(X) {set_col(X|ETH_RST_COL,ASM_KBD_BASE_ADDR_VAL,ASM_KBD_PRE_DEL_VAL,ASM_KBD_POST_DEL_VAL,ASM_32MB_CS_VAL,ASM_16MB_CS_VAL);};
		#else
			#define SET_COLUMNS(X) {COL_WR_BUF=X|ETH_RST_COL;}
		#endif

		#define EXTRA_KEY_CHECK(X) (!(P2_DATA_REG & ((0x0001)<<2)))	//P202

		/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
		unsigned char row_config[KBD_ROWS][3] = {
			{1, 0, 4},//P100, INT4
			{1, 1, 5},//P101, INT5
			{1, 2, 0},//P102, INT0
			{0, 8, 2},//P008, INT2
			{0, 9, 3},//P009, INT3
		};
	#elif defined (CONFIG_F_G2_P2_2PORT_BOARD)
		#define SET_LED_GPIO(X) {set_led_gpio(X);};
		#define SET_COL_GPIO(X) {set_col_gpio(X);};

		#define SET_LEDS(X) SET_LED_GPIO(X)
		#define SET_COLUMNS(X) SET_COL_GPIO(X)
		#define EXTRA_KEY_CHECK(X) (X==0 ? (!(P0_DATA_REG & ((0x0001)<<4))) : (!(P2_DATA_REG & ((0x0001)<<2))) )	//P004: HOOK SW, 	P202: HS_MIC

		/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
		unsigned char row_config[KBD_ROWS][3] = {
			{0, 5, 4},//P005, INT4
			{0, 11, 5},//P011, INT5
			{0, 14, 0},//P014, INT0
			{2, 4, 2},//P204, INT2
			{2, 5, 3},//P205, INT3
		};

		/*For GPIO_X_YY com_config[i]={X, YY}*/
		unsigned char col_config[KBD_COLUMNS][2] = {
			{1, 2},//P102
			{1, 13},//P113
			{1, 14},//P114
			{1, 15},//P115
			{2, 11},//P211
			{2, 12},//P212
		};

		//Leds configuration
		unsigned char led_config[TOTAL_LEDS][2] = {
			{0, 2},//P002
			{0, 9},//P009
			{0, 10},//P010
			{0, 7},//P007
			{0, 8},//P008
			{2, 7},//P207
			{2, 8},//P208
			{2, 10},//P210
		};
	#elif defined (CONFIG_F_G2_ST7567_2PORT_BOARD)
		#define SET_LED_GPIO(X) {set_led_gpio(X);};
		#define SET_COL_GPIO(X) {set_col_gpio(X);};

		#define SET_LEDS(X) SET_LED_GPIO(X)
		#define SET_COLUMNS(X) SET_COL_GPIO(X)
		#define EXTRA_KEY_CHECK(X) (!(P0_DATA_REG & ((0x0001)<<4)))//P004: HOOK SW

		/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
		unsigned char row_config[KBD_ROWS][3] = {
			{0, 5, 4},//P005, INT4
			{0, 11, 5},//P011, INT5
			{0, 14, 0},//P014, INT0
			{2, 4, 2},//P204, INT2
			{2, 5, 3},//P205, INT3
		};

		/*For GPIO_X_YY com_config[i]={X, YY}*/
		unsigned char col_config[KBD_COLUMNS][2] = {
			{1, 2},//P102
			{1, 13},//P113
			{1, 14},//P114
			{1, 15},//P115
			{2, 11},//P211
		};

		//Leds configuration
		unsigned char led_config[TOTAL_LEDS][2] = {
			{0, 2},//P002
									};

	#endif

#elif defined (CONFIG_L_BOARDS)
	#define SCAN_ROUTINE_COLUMN_PORT_TRISTATE //SCAN_ROUTINE_NORMAL
	#if defined (CONFIG_L_V1_BOARD)
		//#define SET_LED_GPIO(X) {set_led_gpio(X);};
		#define SET_LED_GPIO(X)
		#define SET_COL_GPIO(X) {set_col_gpio(X);};

		//#define SET_LEDS(X) SET_LED_GPIO(X)
		#define SET_LEDS(X)
		#define SET_COLUMNS(X) SET_COL_GPIO(X)
		#define EXTRA_KEY_CHECK(X) ((P0_DATA_REG & ((0x0001)<<2)))	//P002

		/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
		unsigned char row_config[KBD_ROWS][3] = {
			{2, 13, 4},//P213, INT4
			{2, 14, 5},//P214, INT5
			{2, 15, 0},//P215, INT0
			{0, 7, 2},//P007, INT2
			{0, 8, 3},//P008, INT3
		};

		/*For GPIO_X_YY com_config[i]={X, YY}*/
		unsigned char col_config[KBD_COLUMNS][2] = {
			{2, 11},//P211
			{2, 12},//P212
			{2, 2},//P202
			{2, 3},//P203
			{0, 9},//P009
			{0, 10},//P010
		};

		//Leds configuration
		unsigned char led_config[TOTAL_LEDS][2] = {
			{0, 5},//P002
		};
#elif (defined (CONFIG_L_V2_BOARD) || defined (CONFIG_L_V5_BOARD))
		#define SCAN_ROUTINE_INVERT_LOGIC //SCAN_ROUTINE_DIODE_DISCHARGE
		//#define LEDS_MUX_COLS
		#define KBD_BASE_ADDR 0x01090000
		#define COL_WR_BUF   (*(volatile unsigned short *)(KBD_BASE_ADDR))

		#define SET_LEDS(X) //{LED_WR_BUF=X};
		#ifdef CONFIG_RAM_SIZE_32MB
			#define ASM_KBD_BASE_ADDR_VAL (KBD_BASE_ADDR)
			#define ASM_KBD_PRE_DEL_VAL 2
			#define ASM_KBD_POST_DEL_VAL 2
			#define ASM_32MB_CS_VAL 0xa
			#define ASM_16MB_CS_VAL 0x9


			#define SET_COLUMNS(X) {set_col((X)|ETH_RST_BLOFF_COL|((gpio_leds_mask)<<8)|(lcd_backlight<<13),\
											ASM_KBD_BASE_ADDR_VAL,\
											ASM_KBD_PRE_DEL_VAL,\
											ASM_KBD_POST_DEL_VAL,\
											ASM_32MB_CS_VAL,ASM_16MB_CS_VAL);}

		#else
			#define SET_COLUMNS(X) {COL_WR_BUF=X|ETH_RST_BLOFF_COL|((gpio_leds_mask)<<8)|(lcd_backlight<<13);}
		#endif

		#define EXTRA_KEY_CHECK(X) (!(P2_DATA_REG & ((0x0001)<<2)))	//P202

		/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
		unsigned char row_config[KBD_ROWS][3] = {
			{1, 13, 4},//P113, INT4
			{2, 3,  5},//P203, INT5
			{2, 4,  0},//P204, INT0
			{2, 5,  2},//P205, INT2
			{2, 6,  3},//P206, INT3
			{2, 8,  3},//P208, INT3
			{2, 11, 3},//P211, INT3
		//	{2, 12, 3},//P212, INT3
		};
	#elif defined (CONFIG_L_V2_BOARD_CNX)||defined (CONFIG_L_V5_BOARD_CNX)||defined (CONFIG_L_V4_BOARD_CNX)
		#define SCAN_ROUTINE_INVERT_LOGIC //SCAN_ROUTINE_DIODE_DISCHARGE
		//#define LEDS_MUX_COLS
		#define KBD_BASE_ADDR 0x01090000
		#define COL_WR_BUF   (*(volatile unsigned short *)(KBD_BASE_ADDR))

		#define SET_LEDS(X) //{LED_WR_BUF=X};
		#ifdef CONFIG_RAM_SIZE_32MB
			#define ASM_KBD_BASE_ADDR_VAL (KBD_BASE_ADDR)
			#define ASM_KBD_PRE_DEL_VAL 2
			#define ASM_KBD_POST_DEL_VAL 2
			#define ASM_32MB_CS_VAL 0xa
			#define ASM_16MB_CS_VAL 0x9


			#define SET_COLUMNS(X) {set_col((X)|ETH_RST_BLOFF_COL|((gpio_leds_mask)<<8)|(lcd_backlight<<13)|(dss_type_test<<15),\
											ASM_KBD_BASE_ADDR_VAL,\
											ASM_KBD_PRE_DEL_VAL,\
											ASM_KBD_POST_DEL_VAL,\
											ASM_32MB_CS_VAL,ASM_16MB_CS_VAL);}

		#else
			#define SET_COLUMNS(X) {COL_WR_BUF=X|ETH_RST_BLOFF_COL|((gpio_leds_mask)<<8)|(lcd_backlight<<13)|(dss_type_test<<15);}
		#endif

#define EXTRA_KEY_CHECK(X) (!(P3_DATA_REG & ((0x0001)<<2)))	//P302
		/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
		unsigned char row_config[KBD_ROWS][3] = {
			{1, 13, 4},//P113, INT4
			{2, 3,  5},//P203, INT5
			{2, 4,  0},//P204, INT0
			{2, 5,  2},//P205, INT2
			{2, 2,  3},//P202, INT3
			{2, 8,  3},//P208, INT3
			{2, 11, 3},//P211, INT3
		//	{2, 12, 3},//P212, INT3
		};
#elif defined (CONFIG_L_V3_BOARD_CNX)
		#define SCAN_ROUTINE_INVERT_LOGIC //SCAN_ROUTINE_DIODE_DISCHARGE
		//#define LEDS_MUX_COLS
		#define KBD_BASE_ADDR 0x01090000
		#define COL_WR_BUF   (*(volatile unsigned short *)(KBD_BASE_ADDR))

		#define SET_LEDS(X) //{LED_WR_BUF=X};
		#ifdef CONFIG_RAM_SIZE_32MB
			#define ASM_KBD_BASE_ADDR_VAL (KBD_BASE_ADDR)
			#define ASM_KBD_PRE_DEL_VAL 2
			#define ASM_KBD_POST_DEL_VAL 2
			#define ASM_32MB_CS_VAL 0xa
			#define ASM_16MB_CS_VAL 0x9
			#define SET_COLUMNS(X) {set_col(X|ETH_RST_BLOFF_COL|(gpio_leds_mask<<8),\
											ASM_KBD_BASE_ADDR_VAL,\
											ASM_KBD_PRE_DEL_VAL,\
											ASM_KBD_POST_DEL_VAL,\
											ASM_32MB_CS_VAL,ASM_16MB_CS_VAL);}
		#else
			#define SET_COLUMNS(X) {COL_WR_BUF=X|ETH_RST_BLOFF_COL|((gpio_leds_mask)<<8)|(lcd_backlight<<13);}
		#endif

		#define EXTRA_KEY_CHECK(X) (!(P2_DATA_REG & ((0x0001)<<11))) 	//P2011

		/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
		unsigned char row_config[KBD_ROWS][3] = {
		{1, 13, 4},//P113, INT4
		{2, 3,  5},//P203, INT5
		{2, 4,  0},//P204, INT0
		{2, 5,  2},//P205, INT2
		{2, 8,  3},//P205, INT2
	};
	#elif defined (CONFIG_L_V3_BOARD)
		#define SCAN_ROUTINE_INVERT_LOGIC //SCAN_ROUTINE_DIODE_DISCHARGE
		//#define LEDS_MUX_COLS
		#define KBD_BASE_ADDR 0x01090000
		#define COL_WR_BUF   (*(volatile unsigned short *)(KBD_BASE_ADDR))

		#define SET_LEDS(X) //{LED_WR_BUF=X};
		#ifdef CONFIG_RAM_SIZE_32MB
			#define ASM_KBD_BASE_ADDR_VAL (KBD_BASE_ADDR)
			#define ASM_KBD_PRE_DEL_VAL 2
			#define ASM_KBD_POST_DEL_VAL 2
			#define ASM_32MB_CS_VAL 0xa
			#define ASM_16MB_CS_VAL 0x9
			#define SET_COLUMNS(X) {set_col(X|ETH_RST_BLOFF_COL|(gpio_leds_mask<<8),\
											ASM_KBD_BASE_ADDR_VAL,\
											ASM_KBD_PRE_DEL_VAL,\
											ASM_KBD_POST_DEL_VAL,\
											ASM_32MB_CS_VAL,ASM_16MB_CS_VAL);}
		#else
			#define SET_COLUMNS(X) {COL_WR_BUF=X|ETH_RST_BLOFF_COL|((gpio_leds_mask)<<8)|(lcd_backlight<<13);}
		#endif

		#define EXTRA_KEY_CHECK(X) (!(P2_DATA_REG & ((0x0001)<<2)))	//P202

		/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
		unsigned char row_config[KBD_ROWS][3] = {
		{1, 13, 4},//P113, INT4
		{2, 3,  5},//P203, INT5
		{2, 4,  0},//P204, INT0
		{2, 5,  2},//P205, INT2
		{2, 6,  3},//P206, INT3
	};
#elif defined (CONFIG_L_V4_BOARD)
		#define SCAN_ROUTINE_INVERT_LOGIC //SCAN_ROUTINE_DIODE_DISCHARGE
		//#define LEDS_MUX_COLS
		#define KBD_BASE_ADDR 0x01090000
		#define COL_WR_BUF   (*(volatile unsigned short *)(KBD_BASE_ADDR))

		#define SET_LEDS(X) //{LED_WR_BUF=X};
		#ifdef CONFIG_RAM_SIZE_32MB
			#define ASM_KBD_BASE_ADDR_VAL (KBD_BASE_ADDR)
			#define ASM_KBD_PRE_DEL_VAL 2
			#define ASM_KBD_POST_DEL_VAL 2
			#define ASM_32MB_CS_VAL 0xa
			#define ASM_16MB_CS_VAL 0x9


		#define SET_COLUMNS(X) {set_col((X)|ETH_RST_BLOFF_COL|((gpio_leds_mask)<<8)|(lcd_backlight<<13),\
										ASM_KBD_BASE_ADDR_VAL,\
										ASM_KBD_PRE_DEL_VAL,\
										ASM_KBD_POST_DEL_VAL,\
										ASM_32MB_CS_VAL,ASM_16MB_CS_VAL);}

		#else
			#define SET_COLUMNS(X) {COL_WR_BUF=X|ETH_RST_BLOFF_COL|((gpio_leds_mask)<<8)|(lcd_backlight<<13);}
		#endif

		#define EXTRA_KEY_CHECK(X) (!(P2_DATA_REG & ((0x0001)<<2)))	//P202

	/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
		unsigned char row_config[KBD_ROWS][3] = {
				{1, 13, 4},//P113, INT4
				{2, 3,  5},//P203, INT5
				{2, 4,  0},//P204, INT0
				{2, 5,  2},//P205, INT2
				{2, 6,  3},//P206, INT3
				{2, 8,  3},//P208, INT3
				{2, 11, 3},//P211, INT3
				//	{2, 12, 3},//P212, INT3
			};
    #endif
#elif defined (CONFIG_IN_BOARDS)
	#if defined (CONFIG_IN_V1_BOARD)
		#define SCAN_ROUTINE_COLUMN_PORT_TRISTATE //SCAN_ROUTINE_NORMAL
		#define SET_COL_GPIO(X)		{set_col_gpio(X);};

		#define SET_COLUMNS(X)		SET_COL_GPIO(X)
		#define EXTRA_KEY_CHECK(X)	(!(P0_DATA_REG & ((0x0001) << 2)))	//P002

		#define SET_LED_GPIO(X)		{set_led_gpio(X);};
		#define SET_LEDS(X)			SET_LED_GPIO(X)

		/*For GPIO_X_YY com_config[i]={X, YY}*/
		unsigned char col_config[KBD_COLUMNS][2] = {
			{2, 11},//P211
			{2, 12},//P212
			{2, 2},//P202
			{2, 3},//P203
			{0, 9},//P009
			{0, 10},//P010
		};

		/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
		unsigned char row_config[KBD_ROWS][3] = {
			{ 2, 13, 4 },	// P213, INT4
			{ 2, 14, 5 },	// P214, INT5
			{ 2, 15, 0 },	// P215, INT0
			{ 0,  7, 2 },	// P007, INT2
			{ 0,  8, 3 },	// P008, INT3
			{ 2, 10, 3 },	// P008, INT3
		};

		//Leds configuration
		unsigned char led_config[TOTAL_LEDS][2] = {
			{ 1, 13	},	//P113
		};
		#endif
    
#elif defined (CONFIG_JW_BOARDS)
	#if defined (CONFIG_JW_V1_BOARD)
		#define SCAN_ROUTINE_COLUMN_PORT_TRISTATE //SCAN_ROUTINE_NORMAL
		#define SET_LED_GPIO(X) {set_led_gpio(X);};
		#define SET_COL_GPIO(X) {set_col_gpio(X);};

		#define SET_LEDS(X) SET_LED_GPIO(X)
		#define SET_COLUMNS(X) SET_COL_GPIO(X)
		#define EXTRA_KEY_CHECK(X) (X==0 ? (!(P0_DATA_REG & ((0x0001)<<4))) : (!(P0_DATA_REG & ((0x0001)<<2))) )	//P004: HOOK SW, 	P002: HEADSET_SW
		/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
		unsigned char row_config[KBD_ROWS][3] = {
			{0, 10, 4},//P010, INT4
			{0, 5, 5},//P005, INT5
			{1, 2, 0},//P102, INT0
			{0, 7, 1},//P007, INT1
			{0, 8, 2},//P008, INT2
			{0, 9, 3},//P009, INT3
		};

		/*For GPIO_X_YY com_config[i]={X, YY}*/
		unsigned char col_config[KBD_COLUMNS][2] = {
			{2, 8},//P208
			{2, 9},//P209
			{2, 10},//P210
			{2, 11},//P211
			{2, 12},//P212
			{2, 13},//P213
			{1, 0},//P100
			{1, 1},//P101
			{2, 2},//P202
			{2, 3},//P203
		};

		//Leds configuration
		unsigned char led_config[TOTAL_LEDS][2] = {
			{0, 5},//P002
		};
	#endif

#elif defined (CONFIG_VT_BOARDS)
	#define SCAN_ROUTINE_COLUMN_PORT_TRISTATE //SCAN_ROUTINE_NORMAL
	#if defined (CONFIG_VT_V1_BOARD)
		#define LED_BASE_ADDR 0x01500000 //This is the address assigned to ACS0
		#define LED_WR_BUF   (*(unsigned short *)(LED_BASE_ADDR))
		#define SET_COL_GPIO(X) {set_col_gpio(X);};

		#define SET_LEDS(X) {LED_WR_BUF=X;};
		#define SET_COLUMNS(X) SET_COL_GPIO(X)
		//#define EXTRA_KEY_CHECK(X) ((P0_DATA_REG & ((0x0001)<<2)))	//P002
		#define EXTRA_KEY_CHECK(X) (X==0 ? (!(P0_DATA_REG & ((0x0001)<<10))) : (!(P1_DATA_REG & ((0x0001)<<2))) )	//P010: HOOK SW, 	P102: PAGE_IN
		/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
		unsigned char row_config[KBD_ROWS][3] = {
				{2, 5, 0},//P205
				{2, 6, 0},//P206
				{2, 7, 0},//P207
				{2, 8, 0},//P208
				{2, 9, 0},//P209
				{2, 10, 0},//P210
		};

		/*For GPIO_X_YY com_config[i]={X, YY}*/
		unsigned char col_config[KBD_COLUMNS][2] = {
				{2, 2},//P202
				{2, 3},//P203
				{2, 4},//P204
				{2, 11},//P211
				{2, 12},//P212
				{0, 7},//P007
		};

	#endif

#elif defined( CONFIG_SMG_BOARDS )
	#ifdef CONFIG_BOARD_SMG_I

	#define SCAN_ROUTINE_NORMAL//SCAN_ROUTINE_COLUMN_PORT_TRISTATE//SCAN_ROUTINE_NORMAL
	#define SET_LED_GPIO(X) {set_led_gpio(X);};
	#define SET_COL_GPIO(X) {set_col_gpio(X);};

	#define SET_LEDS(X) SET_LED_GPIO(X)
	#define SET_COLUMNS(X) SET_COL_GPIO(X)
	#define EXTRA_KEY_CHECK(X) (!(P0_DATA_REG & ((0x0001)<<4)))

	/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
	unsigned char row_config[KBD_ROWS][3] = {
				{1, 2, 0},//P102, INT0
				{0, 7, 1},//P007, INT1
				{0, 8, 2},//P008, INT2
				{0, 9, 3},//P009, INT3
				{0, 10, 4},//P010, INT4
			};

	/*For GPIO_X_YY com_config[i]={X, YY}*/
	unsigned char col_config[KBD_COLUMNS][2] = {
				{0, 2},//P002
				{0, 5},//P005
				{0, 11},//P011
				{0, 14},//P014
				{1, 1},//P101
				{1, 13},//P113
			};

		//Leds configuration
		unsigned char led_config[TOTAL_LEDS][2] = {
			{2, 2},//P202
			{2, 3},//P203
			{2, 13},//P213
		};
	#endif
#elif defined (CONFIG_SC14452)
	#define LEDS_MUX_COLS // Leds are multiplexed with columns
	#ifdef CONFIG_SC14452_DK_REV_C
		#define SCAN_ROUTINE_DIODE_DISCHARGE
	#else
		#define SCAN_ROUTINE_INVERT_LOGIC
	#endif
#ifdef CONFIG_RAM_SIZE_32MB
	#define KBD_BASE_ADDR 0x1090000 //0x01400000
#else
	#define KBD_BASE_ADDR 0x01400000
#endif
	#define COL_WR_BUF   (*(unsigned short *)(KBD_BASE_ADDR))

	//#define ROW_RD_BUF   (*(unsigned short *)(KBD_BASE_ADDR + 0x100))
	#ifdef CONFIG_RAM_SIZE_32MB
		#define ASM_KBD_BASE_ADDR_VAL (KBD_BASE_ADDR)
		#define ASM_KBD_PRE_DEL_VAL 2
		#define ASM_KBD_POST_DEL_VAL 2
		#define ASM_32MB_CS_VAL 0xa
		#define ASM_16MB_CS_VAL 0x9
		#define SET_COLUMNS(X) {set_col(X,ASM_KBD_BASE_ADDR_VAL,ASM_KBD_PRE_DEL_VAL,ASM_KBD_POST_DEL_VAL,ASM_32MB_CS_VAL,ASM_16MB_CS_VAL);};
	#else
		#define SET_COLUMNS(X) {COL_WR_BUF=X;}
	#endif
	#define EXTRA_KEY_CHECK(X) (!(P2_DATA_REG & ((0x0001)<<2)))	//P202

	/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
	unsigned char row_config[KBD_ROWS][3] = {
		{0, 10, 4},	//P010, INT4
		{0, 5, 5},	//P005, INT5
		{2, 5, 0},	//P205, INT0
		{2, 6, 2},	//P206, INT2
		{0, 9, 3}	//P009, INT3
	};

#else//SC14450
	#ifndef CONFIG_L_V1_BOARD
	#define LEDS_MUX_COLS // Leds are multiplexed with columns
	#endif
	#define SCAN_ROUTINE_INVERT_LOGIC
	#define KBD_BASE_ADDR 0x01400000
	#define COL_WR_BUF   (*(unsigned short *)(KBD_BASE_ADDR))
	//#define ROW_RD_BUF   (*(unsigned short *)(KBD_BASE_ADDR + 0x100))
	#ifdef CONFIG_RAM_SIZE_32MB
		#define ASM_KBD_BASE_ADDR_VAL (KBD_BASE_ADDR)
		#define ASM_KBD_PRE_DEL_VAL 2
		#define ASM_KBD_POST_DEL_VAL 2
		#define ASM_32MB_CS_VAL 0xa
		#define ASM_16MB_CS_VAL 0x9
		#define SET_COLUMNS(X) {set_col(X,ASM_KBD_BASE_ADDR_VAL,ASM_KBD_PRE_DEL_VAL,ASM_KBD_POST_DEL_VAL,ASM_32MB_CS_VAL,ASM_16MB_CS_VAL);};
	#else
		#define SET_COLUMNS(X) {COL_WR_BUF=X;}
	#endif
	#define EXTRA_KEY_CHECK(X) (!(P0_DATA_REG & ((0x0001)<<11)))//P011

	/*For GPIO_X_YY and GPIO_PID_INTZn, row_config[i]={X, YY, Z}*/
	unsigned char row_config[KBD_ROWS][3] = {
		{1, 0, 4},//P100, INT4
		{1, 1, 5},//P101, INT5
		{1, 2, 0},//P102, INT0
		{0, 8, 2},//P008, INT2
		{0, 9, 3},//P009, INT3
	};


#endif

// #define WATCHDOG_REG 				(*(short *)(0xFF4C00))

// #define SET_FREEZE_REG 				(*(short *)(0xFF5000))
#define FRZ_WDOG_SYM 				(0x0008)

/***   Macros   ***/
#ifdef SC1445x_KBD_SYS_POLLING
	#define	START_TIMER(TIMER, TMOUT) {sc1445x_kbd_polling_timer += TMOUT;}
	#define	STOP_TIMER(TIMER) {sc1445x_kbd_polling_timer=0;}
#else
	#define	START_TIMER(TIMER, TMOUT) {TIMER.expires = jiffies + TMOUT;add_timer(&TIMER);}
	#define	STOP_TIMER(TIMER) {del_timer(&TIMER);}
#endif
#define KEY_ID(key_scan_index) (((unsigned short)1 << key_scan[key_scan_index].pre_col) << KBD_ROWS | (key_scan[key_scan_index].pre_row))

/***      Function Prototypes       ***/
/** Low level **/
void check_kbd(void);
void check_extra_keys(void);
void toggle_led_mask(void);
unsigned short hw_init(void);
char fast_scan_keypad(void);
unsigned char scan_keypad(char first_active_row);
unsigned char scan_extra_keys(void);
unsigned short kbd_send_msg(unsigned char msg_id, unsigned short key_id);
unsigned char check_keypad_conf_validity(struct keypad_sc1445x_conf_struct *conf);
unsigned short kbd_buffer_input(kbd_sc1445x_dev_struct *dev, kbd_msg *msg);
#ifdef SET_LED_GPIO
void set_led_gpio(unsigned short mask);
#endif
#ifdef SET_COL_GPIO
void set_col_gpio(unsigned short mask);
#endif
/** High level **/
//Interrupt
static irqreturn_t kbd_int_handler(int irq, void *dev_id, struct pt_regs *regs);
//Tasklet
void kbd_tasklet_fn(unsigned long param);
//Timers
#ifndef SC1445x_KBD_SYS_POLLING
static void kbd_timer_poll_fn(unsigned long param);
static void kbd_timer_int_fn(unsigned long param);
#endif
/*Static functions*/
static void kbd_setup_cdev(kbd_sc1445x_dev_struct *dev);
static int kbd_configure(void);
static void kbd_init_fsm(void);
static unsigned char spacefree(kbd_sc1445x_dev_struct *dev);
#ifdef SC1445x_KBD_SYS_POLLING
	unsigned short sc1445x_kbd_polling_timer=0;
	void (*sc1445x_kbd_polling_func) (unsigned long)=kbd_timer_poll_fn;
	void kbd_sys_timer_tasklet_fn(void);
#endif
//Module specific
int __init kbd_init_module(void);
void    kbd_cleanup_module(void);

// Fops prototypes
int	kbd_open(struct inode *inode, struct file *filp);
int kbd_release(struct inode *inode, struct file *filp);
//ssize_t kbd_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos);
int kbd_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg);
unsigned int kbd_poll(struct file *filp, poll_table *wait);
/*** Parameters which can be set at load time  ***/
int kbd_sc1445x_major =   KBD_SC1445x_MAJOR;
int kbd_sc1445x_minor =   0;


module_param(kbd_sc1445x_major, int, S_IRUGO);
module_param(kbd_sc1445x_minor, int, S_IRUGO);


MODULE_AUTHOR("George Giannaras");
MODULE_LICENSE("Dual BSD/GPL");

/***      Var definitions  and initializations     ***/
/** Low level **/
//kbd_init is used for storing driver's configuration that is used for initialization on device open
struct keypad_sc1445x_conf_struct kbd_init = {
	/* Set keypad default configuration */
	.flags= KBD_SC1445x_FLAGS_DEFAULT,//IF NOT INT then POLLING//KBD_SC1445x_LEVEL_HIGH
	.cols=KBD_COLUMNS,
	.rows=KBD_ROWS,//1-5
	#if !(defined(CONFIG_L_V1_BOARD)||defined(CONFIG_L_V2_BOARD)||defined(CONFIG_L_V3_BOARD)||defined(CONFIG_L_V4_BOARD)||defined(CONFIG_L_V5_BOARD)\
			||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX))
	.mult_keys=2,
	#else
	.mult_keys=1,
	#endif
	.repeat_tmouts_long=100,
	.repeat_tmouts_short=50,
	.polling_timer=1,
	.extra_key_debounce_tmouts=10,
	.hook_flash_tmouts=HOOK_FLASH_TMOUTS,
	//.int_debounce_timer=60;
	//.int_repeat_timer=60;
	/*Driver internals*/
//	.buffer=0;
	.buffersize=KBD_SC1445x_BUFFER_SIZE,
	.irq=2, //CHANGE THIS TO KBD_INT defined in irq.h
#ifdef CONFIG_F_G2_P1_BOARD
	.debounce_tmouts=6,
	.scanDelay=100,
#else//CONFIG_F_G2_P1_BOARD
		#if !(defined(CONFIG_L_V1_BOARD)||defined(CONFIG_L_V2_BOARD)||defined(CONFIG_L_V3_BOARD)||defined(CONFIG_L_V4_BOARD)||defined(CONFIG_L_V5_BOARD)\
				||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX))
	.debounce_tmouts=1,
	#else
	.debounce_tmouts=3,
	#endif
#endif //CONFIG_F_G2_P1_BOARD


//led_toggle_tmouts is initialized in kbd_init_module
//led_toggle_masks is initialized in kbd_init_module
//key_locks is initialized in kbd_init_module
};
struct keypad_sc1445x_conf_struct kbd_tmp;
kbd_sc1445x_dev_struct kbd_dev;
unsigned short col_mask=0;
//unsigned short led_mask=LED_LINE1|LED_LINE2|LED_GP1|LED_GP2|LED_AUDIO_FN1|LED_AUDIO_FN2;	//this var is never initialized again in the driver when operating in POLLING mode. In this mode it is used for controlling leds.
unsigned short led_mask=0;	//this var is never initialized again in the driver when operating in POLLING mode. In this mode it is used for controlling leds.
//unsigned short led_mask=LED_LINE3|LED_LINE2|LED_LINE1|LED_HOOK|LED_GP|LED_TRNSFR|LED_CONFR|LED_HOLD|LED_DND|LED_REDIAL|LED_MWI|LED_HNDSET|LED_HDSET|LED_SPKR|;
static key_scan_struct key_scan[MAX_MULT_KEYS];
static keypad_rw_struct keypad_row[KBD_ROWS];//stores info about the pios that should be read for getting kbd press events
//static keypad_rw_struct keypad_extra_key;//stores info about the pios that should be read for getting extra_key press events
static extra_key_scan_struct extra_key_scan[EXTRA_KEYS];//used for storing fsm state
static led_toggle_struct led_toggle;
unsigned char scan_col[KBD_COLUMNS];
unsigned char total_active_keys=0;
volatile unsigned char  dummy_read;
#if defined(CONFIG_L_V2_BOARD) || defined(CONFIG_L_V3_BOARD) ||defined(CONFIG_L_V4_BOARD)||defined(CONFIG_L_V5_BOARD)\
	||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)

	unsigned int gpio_leds_mask=0;
	unsigned int lcd_backlight=0;
#if defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	unsigned int dss_type_test=0;
	EXPORT_SYMBOL (dss_type_test);
#endif

	EXPORT_SYMBOL (lcd_backlight);
	EXPORT_SYMBOL (gpio_leds_mask);

#endif

/** High level **/
struct file_operations kbd_sc1445x_fops = {
	.owner =    THIS_MODULE,
	.poll =   	kbd_poll,
	.read =     NULL,
	.write =     NULL,
	.ioctl =    kbd_ioctl,
	.open =     kbd_open,
	.release =  kbd_release,
};
volatile unsigned char semaphore=0;
struct keypad_sc1445x_conf_struct *kbd_conf = &(kbd_dev.kbd_conf);
/***      Timer initializations     ***/
static DECLARE_WAIT_QUEUE_HEAD(wq);
//static short polling_tmout = 0;
struct timer_list kbd_timer;
/***      Tasklet     ***/
DECLARE_TASKLET(kbd_tasklet,  kbd_tasklet_fn,  0);
#ifdef SC1445x_KBD_SYS_POLLING
	DECLARE_TASKLET(kbd_sys_timer_tasklet,  kbd_sys_timer_tasklet_fn,  0);
#endif

/***************          Main body             ***************/

/****************************************************************************************************/
/************                                        LOW LEVEL DRIVER FUNTIONS                                                            *********/
/****************************************************************************************************/

/*******************************************************
	Description: 	Checks the validity of the configuration parameters that are passed to the driver
	Returns:		0: on succes, err_code: on error
 ********************************************************/
unsigned char check_keypad_conf_validity(struct keypad_sc1445x_conf_struct *conf){
	if(!conf->rows || conf->rows > KBD_ROWS){
		printk(KERN_WARNING "Invalid rows conf = %d \n", conf->rows);
		return 1; // ERRNO
	}
	if(!conf->cols || conf->cols > KBD_COLUMNS){
		printk(KERN_WARNING "Invalid cols conf = %d \n", conf->cols);

		return 2; // ERRNO
	}
	if(!conf->mult_keys || conf->mult_keys > MAX_MULT_KEYS){
		printk(KERN_WARNING "Invalid mult_keys conf = %d \n", conf->mult_keys);
		return 3; // ERRNO
	}
	/* Checks for INT mode*/
	// if(conf.flags & KBD_SC1445x_INT){
		// if(conf.int_repeat_timer > 63)
			// return 4; // ERRNO
		// else if(!conf.int_repeat_timer)
			// conf.repeat_tmouts_long=conf.repeat_tmouts_short=0;
		// if(conf.int_debounce_timer > 63)
			// return 4; // ERRNO
		// else if(!conf.int_debounce_timer)//means that no debounce is wanted
			// conf.debounce_tmouts=0;//because debounce_tmouts is used internally in check_kbd also in INT mode
		// else
			// conf.debounce_tmouts=1;//because debounce_tmouts is used internally in check_kbd also in INT mode
	// }
	/* Checks for POLLING mode*/
	//else if(conf.flags & KBD_SC1445x_POLLING){
		if(!conf->polling_timer){
			printk(KERN_WARNING "Invalid polling timer conf = %d \n", conf->polling_timer);
			return 4; // ERRNO
		}
	//}
	return 0;
}
/*******************************************************
	Description: 	The keypad fsm that generates the PRESS, REPEAT, RELEASE msgs based on keypad activity

 ********************************************************/
void check_kbd(){
	unsigned char i, j, k;

	/* Mask out the previously scanned key presses */
	for(i=0;i<kbd_conf->mult_keys;i++){
		key_scan[i].flags &= ~SAME_KEY;
		if( key_scan[i].pre_row && (key_scan[i].pre_row & scan_col[key_scan[i].pre_col]) ){
			scan_col[key_scan[i].pre_col] &= ~key_scan[i].pre_row;//mask out already stored key press
			key_scan[i].flags |= SAME_KEY;
		}
	}
	/* Run fsm for all  key_scans */
	i=0;
	while(i<kbd_conf->mult_keys){
		if(key_scan[i].tmout)
			key_scan[i].tmout--;
		switch(key_scan[i].state){
			case KBD_IDLE_STATE:
				/* Search for a key press*/
				j=0;
				while( j<kbd_conf->cols && !scan_col[j] )
					j++;
				if(j<kbd_conf->cols ){
					/* Find the first row that has this col active and mask it out*/
					key_scan[i].pre_col=j;
					k=0x01;
					while(!(scan_col[j] & k)) k<<=1;
					key_scan[i].pre_row=k;
					scan_col[j] &= ~k;
					if(kbd_conf->debounce_tmouts){
						key_scan[i].tmout=kbd_conf->debounce_tmouts;
						key_scan[i].state=KBD_PRESS_DEBOUNCE_STATE;
					}
					else{
						key_scan[i].state=KBD_PRESS_STATE;
						/*Send KBD_SC1445x_PRESS_MSG*/
						kbd_send_msg(KBD_SC1445x_PRESS_MSG, KEY_ID(i));
					}
					total_active_keys++;
				}
				i++;
				break;
			case KBD_PRESS_DEBOUNCE_STATE:
				if(!key_scan[i].tmout){//check only if debounce timer has expired
					if(key_scan[i].flags & SAME_KEY){
						key_scan[i].state=KBD_PRESS_STATE;
						/*Send KBD_SC1445x_PRESS_MSG*/
						kbd_send_msg(KBD_SC1445x_PRESS_MSG, KEY_ID(i));
						if(kbd_conf->repeat_tmouts_long){
							key_scan[i].tmout=kbd_conf->repeat_tmouts_long;
							key_scan[i].state=KBD_PRESS_STATE;
						}
					}
					else{//rerun the fsm (without sending release msg)
						key_scan[i].state=KBD_IDLE_STATE;
						key_scan[i].pre_row=key_scan[i].pre_col=0;
						total_active_keys--;
						break;
					}
				}
				i++;
				break;
			case KBD_PRESS_STATE:
				if(key_scan[i].flags & SAME_KEY){
					if(!key_scan[i].tmout){
						if(kbd_conf->repeat_tmouts_long){
							/*Send KBD_SC1445x_REPEAT_MSG*/
							kbd_send_msg(KBD_SC1445x_REPEAT_MSG, KEY_ID(i));
							if(kbd_conf->repeat_tmouts_short){
								key_scan[i].tmout=kbd_conf->repeat_tmouts_short;
								key_scan[i].state=KBD_REPEAT_SHORT_STATE;
							}
							else{
								key_scan[i].state=KBD_WAIT_RELEASE_STATE;
							}
						}
						else{
							key_scan[i].state=KBD_WAIT_RELEASE_STATE;
						}
					}
				}
				else{//rerun the fsm
					key_scan[i].state=KBD_WAIT_RELEASE_STATE;
					break;
				}
				i++;
				break;
			case KBD_REPEAT_SHORT_STATE:
				if(key_scan[i].flags & SAME_KEY){
					if(!key_scan[i].tmout){
						/*Send KBD_SC1445x_REPEAT_MSG*/
						kbd_send_msg(KBD_SC1445x_REPEAT_MSG, KEY_ID(i));
						key_scan[i].tmout=kbd_conf->repeat_tmouts_short;
					}
				}
				else{//rerun the fsm
					key_scan[i].state=KBD_WAIT_RELEASE_STATE;
					break;
				}
				i++;
				break;
			case KBD_WAIT_RELEASE_STATE:
				if(!(key_scan[i].flags & SAME_KEY)){
					/*Send KBD_SC1445x_RELEASE_MSG*/
					kbd_send_msg(KBD_SC1445x_RELEASE_MSG, KEY_ID(i));
					key_scan[i].state=KBD_IDLE_STATE;
					key_scan[i].tmout=0;
					key_scan[i].pre_row=key_scan[i].pre_col=0;
					i++;
					total_active_keys--;
				}
				break;
		}
	}
}
/*******************************************************
	Description: 	The  fsm that generates the PRESS, RELEASE msgs based on extra_key activity

 ********************************************************/
void check_extra_keys(){
	unsigned char i;

	for(i=0; i<EXTRA_KEYS; i++){
		if(extra_key_scan[i].tmout)
			extra_key_scan[i].tmout--;
		switch(extra_key_scan[i].state){
			case KBD_IDLE_STATE:
				if(extra_key_scan[i].flags & EXTRA_KEY_PRESSED){
					if(kbd_conf->extra_key_debounce_tmouts){
						extra_key_scan[i].tmout=kbd_conf->extra_key_debounce_tmouts;
						extra_key_scan[i].state=KBD_PRESS_DEBOUNCE_STATE;
					}
					else{
						extra_key_scan[i].state=KBD_PRESS_STATE;
						/*Send HOOK_PRESS_MSG*/
						kbd_send_msg(KBD_SC1445x_EXTRA_KEY_PRESS_MSG(i), HOOK_DUMMY_KEY_ID);
					}
					extra_key_scan[i].flags &= ~EXTRA_KEY_PRESSED;
				}
				break;
			case KBD_PRESS_DEBOUNCE_STATE:
				if(!extra_key_scan[i].tmout){//check only if debounce timer has expired
					if(extra_key_scan[i].flags & EXTRA_KEY_PRESSED){
						if((i==HOOK_KEY) && (kbd_conf->hook_flash_tmouts) && (kbd_conf->hook_flash_tmouts > kbd_conf->extra_key_debounce_tmouts)){
							extra_key_scan[i].tmout=kbd_conf->hook_flash_tmouts - kbd_conf->extra_key_debounce_tmouts;
							extra_key_scan[i].state=KBD_CHECK_FLASH_STATE;
							/*Send PRE_HOOK_FLASH_MSG*/
			 				kbd_send_msg(KBD_SC1445x_PRE_HOOK_FLASH_MSG, HOOK_DUMMY_KEY_ID);
 						}
						else{
						extra_key_scan[i].state=KBD_PRESS_STATE;
						/*Send HOOK_PRESS_MSG*/
						kbd_send_msg(KBD_SC1445x_EXTRA_KEY_PRESS_MSG(i), HOOK_DUMMY_KEY_ID);
						}
						extra_key_scan[i].flags &= ~EXTRA_KEY_PRESSED;
					}
					else{//rerun the fsm (without sending release msg)
						extra_key_scan[i].state=KBD_IDLE_STATE;
						break;
					}
				}
				break;
			case KBD_PRESS_STATE:
				if(!(extra_key_scan[i].flags & EXTRA_KEY_PRESSED)){
					extra_key_scan[i].state=KBD_IDLE_STATE;
					extra_key_scan[i].tmout=0;
					/*Send KBD_RELEASE_MSG*/
					kbd_send_msg(KBD_SC1445x_EXTRA_KEY_RELEASE_MSG(i), HOOK_DUMMY_KEY_ID);
				}
				else
					extra_key_scan[i].flags &= ~EXTRA_KEY_PRESSED;
				break;
			case KBD_CHECK_FLASH_STATE:
 				if(!extra_key_scan[i].tmout){
					if((extra_key_scan[i].flags & EXTRA_KEY_PRESSED)){
						extra_key_scan[i].state =KBD_PRESS_STATE;
						extra_key_scan[i].flags &= ~EXTRA_KEY_PRESSED;
						/*Send HOOK_PRESS_MSG*/
		 				kbd_send_msg(KBD_SC1445x_EXTRA_KEY_PRESS_MSG(i), HOOK_DUMMY_KEY_ID);
					}
					else{
						/*Send HOOK_FLASH_MSG*/
		 				kbd_send_msg(KBD_SC1445x_HOOK_FLASH_MSG, HOOK_DUMMY_KEY_ID);
						extra_key_scan[i].state=KBD_IDLE_STATE;
					}
				}
				break;
		}
	}
}
/*******************************************************
	Description: 	Toggles the led mask, in order to make the leds blink on a fixed time period

 ********************************************************/
void toggle_led_mask(){
	unsigned short led_mask_new=0;
	if(led_toggle.tmout)
		led_toggle.tmout--;
	else{
		led_toggle.index=(led_toggle.index+1)%LED_TOGGLE_MASKS;
#if defined (CONFIG_L_V1_BOARD)
		led_toggle.tmout=kbd_conf->led_toggle_tmouts[led_toggle.index];
#else
		led_toggle.tmout=kbd_conf->led_toggle_tmouts;
#endif
		led_mask_new=kbd_conf->led_toggle_masks[led_toggle.index];
#if !defined (LEDS_MUX_COLS)
		if(led_mask_new != led_mask){
			led_mask = led_mask_new;
			SET_LEDS(led_mask);
//			printk("UPDATE LEDS !!!! \n", __FUNCTION__);
		}
#endif
	}
}

/*******************************************************
	Description: 	Sets the columns when they are assigned to gpio's
	NOTE: This function is called very frequently. It should be as optimized as possible !!

 ********************************************************/
#ifdef SET_LED_GPIO
void set_led_gpio(unsigned short mask){
	unsigned char i;
	for (i=0; i< TOTAL_LEDS; i++){
		if (mask & (1<<i)){//set the pio high
		#if defined( CONFIG_SMG_BOARDS )
			#ifdef CONFIG_BOARD_SMG_I
				__REG(gpio_reset_reg[led_config[i][0]])= 1 << led_config[i][1];
			#endif
		#else
			__REG(gpio_set_reg[led_config[i][0]])  = 1 << led_config[i][1];
//			printk(KERN_ERR "gpio_set_reg[led_config[i][0]]= 0x%x, 1 << led_config[i][1]= 0x%x \n",
//					gpio_set_reg[led_config[i][0]], 1 << led_config[i][1]);
		#endif
		}
		else{//set the pio low
		#if defined( CONFIG_SMG_BOARDS )
			#ifdef CONFIG_BOARD_SMG_I
				__REG(gpio_set_reg[led_config[i][0]])  = 1 << led_config[i][1];
			#endif
		#else
			__REG(gpio_reset_reg[led_config[i][0]])= 1 << led_config[i][1];
//			printk(KERN_ERR "gpio_reset_reg[led_config[i][0]]= 0x%x, 1 << led_config[i][1]= 0x%x \n",
//					gpio_reset_reg[led_config[i][0]], 1 << led_config[i][1]);
		#endif
		}
	}
}
#endif
#ifdef SET_COL_GPIO
void set_col_gpio(unsigned short mask){
	unsigned char i;
	for (i=0; i< KBD_COLUMNS; i++){
		if (mask & (1<<i)){//set the pio high
			__REG(gpio_set_reg[col_config[i][0]])  = 1 << col_config[i][1];
//			printk(KERN_ERR "gpio_set_reg[col_config[i][0]]= 0x%x, 1 << col_config[i][1]= 0x%x \n",
//					gpio_set_reg[col_config[i][0]], 1 << col_config[i][1]);
		}
		else{//set the pio low
			__REG(gpio_reset_reg[col_config[i][0]])= 1 << col_config[i][1];
//			printk(KERN_ERR "gpio_reset_reg[col_config[i][0]]= 0x%x, 1 << col_config[i][1]= 0x%x \n",
//					gpio_reset_reg[col_config[i][0]], 1 << col_config[i][1]);
		}
	}
}
#endif
/*******************************************************
	Description: 	Configures hw for controlling the attached keypad
	Input:		The hw configuration stored in a kbd_conf struct
	Return:		In interrupt mod:, the value that should be written to KEY_BOARD_INT_REG, to enable the intrs.
				0: otherwise
	Notes:		The cols, rows are configured based on the ifdefs KBD_ROWS, KBD_COLUMNS and not the rows, cols fields of the kbd_conf
 ********************************************************/
unsigned short hw_init(){
	unsigned char i;
	unsigned short key_board_int_reg=0;
	unsigned short gpio_pupd;//stores gpio pull up/down configuration

#if defined  (CONFIG_L_V2_BOARD)||defined  (CONFIG_L_V3_BOARD) ||defined(CONFIG_L_V4_BOARD)||defined(CONFIG_L_V5_BOARD)
	//Temporary !!!!!! - BEGIN
	OVER_16M_SDRAM_ACCESS_PATCH_FLAG;
	PROTECT_OVER_16M_SDRAM_ACEESS;
	//setup latch
#ifdef AC_LATCH
	EBI_SMTMGR_SET1_REG=0x0d44;
#else
	EBI_SMTMGR_SET1_REG=((1<<16)|(0x7<<10)|(2<<8)|(1<<6)|(0x7<<0));
#endif

	SetPort(P2_09_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_ACS2);        /* P2_09 is ACS2 */
	EBI_ACS2_CTRL_REG= 0x121;						/* use time setting 1, memory is sram, size is 64K */
	EBI_ACS2_LOW_REG=0x01090000;					/* ACS2 base address is 0x1090000 */
	RESTORE_OVER_16M_SDRAM_ACEESS

	//disable row7
//	P2_12_MODE_REG=GPIO_PUPD_OUT_NONE;
//	P2_SET_DATA_REG=(1<<12);
	//Temporary !!!!!! - END
#endif

#if defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	//Temporary !!!!!! - BEGIN
	OVER_16M_SDRAM_ACCESS_PATCH_FLAG;
	PROTECT_OVER_16M_SDRAM_ACEESS;
	//setup latch
#ifdef AC_LATCH
	EBI_SMTMGR_SET1_REG=0x0d44;
#else
	EBI_SMTMGR_SET1_REG=((1<<16)|(0x7<<10)|(2<<8)|(1<<6)|(0x7<<0));
#endif

	SetPort(P0_02_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_ACS2);        /* P2_09 is ACS2 */
	EBI_ACS2_CTRL_REG= 0x121;						/* use time setting 1, memory is sram, size is 64K */
	EBI_ACS2_LOW_REG=0x01090000;					/* ACS2 base address is 0x1090000 */
	RESTORE_OVER_16M_SDRAM_ACEESS

	//disable row7
//	P2_12_MODE_REG=GPIO_PUPD_OUT_NONE;
//	P2_SET_DATA_REG=(1<<12);
	//Temporary !!!!!! - END
#endif

#ifndef KBD_TEST_PC
	/************** COLUMNS CONFIGURATION ***************/
	/* Columns configuration remains the same. Since it is memory mapped, it should be configured on chip initialization */
#if defined(SET_COL_GPIO)
	/* Set the PID  */
	for (i=0; i<KBD_COLUMNS; i++){
		__REG(gpio_mode_reg[col_config[i][0]]+col_config[i][1])=GPIO_PUPD_OUT_NONE;//Output
	}
#endif
	i=0;
	/* Disable kbd intr*/
#ifndef CONFIG_CVM480_SPI_DECT_SUPPORT
  KEY_BOARD_INT_REG=0x0000;
#endif
	/* Init vars used to store register configurations  */
	key_board_int_reg=gpio_pupd=0;

	/* Set key press sense logic */
	gpio_pupd = GPIO_PUPD_IN_PULLUP;
	//if(kbd_conf->flags & KBD_SC1445x_LEVEL_HIGH){
		key_board_int_reg |= (0x0400);
		gpio_pupd = GPIO_PUPD_IN_PULLDOWN;
		SET_COLUMNS(col_mask);
	//}

	/*************** EXTRA KEY CONFIGURATION ************/
	/* Set the PID and the pull-up or pull-down registores if needed */
#if defined( CONFIG_RPS_BOARD)
	//if(BAT_STATUS_REG & PON_STS){
		BAT_CTRL_REG |= REG_ON;//to keep LDO2 and LDO1 on.  FIXME: This should be done at an earlier stage, at the bootloader ???
		EXTRA_KEY_CLEAR;
	//}
#elif defined (CONFIG_F_G2_P1_BOARD)
	//Configure P202
	/* Set the PID and the pull-up or pull-down registores if needed */
	//P2_02_MODE_REG=gpio_pupd; //No mode register for P202
#elif defined( CONFIG_SMG_BOARDS )
	#ifdef CONFIG_BOARD_SMG_I
		P0_04_MODE_REG=GPIO_PUPD_IN_PULLUP;
	#endif
#elif defined  (CONFIG_F_G2_P2_2PORT_BOARD) || defined (CONFIG_F_G2_ST7567_2PORT_BOARD)
	P0_04_MODE_REG=gpio_pupd; //HOOK Input, registers according to LEVEL logic
	P2_02_MODE_REG=gpio_pupd; //HS_MIC Input, registors according to LEVEL logic
#elif defined  (CONFIG_L_V1_BOARD)
	P0_02_MODE_REG=gpio_pupd; //HOOK Input, registors according to LEVEL logic
	gpio_pupd = GPIO_PUPD_IN_PULLUP;
#elif defined  (CONFIG_L_V2_BOARD)  ||defined(CONFIG_L_V5_BOARD)
	gpio_pupd = GPIO_PUPD_IN_PULLUP;
	P2_02_MODE_REG=gpio_pupd; //HOOK Input, registors according to LEVEL logic
#elif defined  (CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	gpio_pupd = GPIO_PUPD_IN_PULLUP;
	P3_02_MODE_REG=gpio_pupd; //HOOK Input, registors according to LEVEL logic
	gpio_pupd = GPIO_PUPD_IN_PULLDOWN;
#elif defined(CONFIG_L_V4_BOARD_CNX)
	gpio_pupd = GPIO_PUPD_IN_PULLUP;
	P3_02_MODE_REG=gpio_pupd; //HOOK Input, registors according to LEVEL logic
	gpio_pupd = GPIO_PUPD_IN_PULLDOWN;
#elif defined  (CONFIG_L_V3_BOARD_CNX)
	gpio_pupd = GPIO_PUPD_IN_PULLUP;
	P2_11_MODE_REG=gpio_pupd; //HOOK Input, registors according to LEVEL logic
	gpio_pupd = GPIO_PUPD_IN_PULLDOWN;
#elif defined  (CONFIG_L_V3_BOARD)
	gpio_pupd = GPIO_PUPD_IN_PULLUP;
	P2_02_MODE_REG=gpio_pupd; //HOOK Input, registors according to LEVEL logic
	gpio_pupd = GPIO_PUPD_IN_PULLDOWN;
#elif defined  (CONFIG_L_V4_BOARD)
	gpio_pupd = GPIO_PUPD_IN_PULLUP;
	P2_02_MODE_REG=gpio_pupd; //HOOK Input, registors according to LEVEL logic
	gpio_pupd = GPIO_PUPD_IN_PULLDOWN;
#elif defined  (CONFIG_VT_V1_BOARD)
	P0_10_MODE_REG=gpio_pupd; //HOOK Input, registers according to LEVEL logic
	P1_02_MODE_REG=gpio_pupd; //PAGE_IN Input, registors according to LEVEL logic
	gpio_pupd = GPIO_PUPD_IN_PULLUP;

#elif defined  (CONFIG_JW_V1_BOARD)
	P0_04_MODE_REG=gpio_pupd; //HOOK Input, registors according to LEVEL logic
	P0_02_MODE_REG=gpio_pupd; //HEADS_SW
	gpio_pupd = GPIO_PUPD_IN_PULLUP;

#elif defined  (CONFIG_IN_V1_BOARD)
	P0_02_MODE_REG=gpio_pupd; //HOOK Input, registors according to LEVEL logic
	gpio_pupd = GPIO_PUPD_IN_PULLUP;

#elif defined  (CONFIG_SC14452)
	P2_02_MODE_REG=gpio_pupd; //Input, registors according to LEVEL logic

#else//CONFIG_SC14450
	//Configure P011
	/* Set the PID and the pull-up or pull-down registores if needed */
	P0_11_MODE_REG=gpio_pupd; //Input, registors according to LEVEL logic
#endif

	/************** LEDS CONFIGURATION ***************/
	/* Columns configuration remains the same. Since it is memory mapped, it should be configured on chip initialization */
#if defined(SET_LED_GPIO)
	/* Set the PID  */
	for (i=0; i<TOTAL_LEDS; i++){
		__REG(gpio_mode_reg[led_config[i][0]]+led_config[i][1])=GPIO_PUPD_OUT_NONE;//Output
	}
#endif

	/*************** ROWS CONFIGURATION **************/
	for(i=0; i<KBD_ROWS; i++){
		/* Define the register and the bit from where the status of the button should be read */
		keypad_row[i].kbd_pio_stat_reg=gpio_data_reg[row_config[i][0]];
		keypad_row[i].kbd_pio_id=(0x0001)<<row_config[i][1];
		//printk(KERN_ERR "P%d_00_MODE_REG = 0x%x \n", row_config[i][0], gpio_mode_reg[row_config[i][0]]);
		/* Set the PID and the pull-up or pull-down registores if needed */
		/*
		printk(KERN_ERR "Row= %d, &P%d_0%d_MODE_REG = 0x%x, val=0x%x \n",
				i, row_config[i][0], row_config[i][1], gpio_mode_reg[row_config[i][0]]+row_config[i][1], gpio_pupd);
		printk(KERN_ERR "gpio_mode_reg[row_config[i][0]]= 0x%x, 2*row_config[i][1]=0x%x \n",
				gpio_mode_reg[row_config[i][0]], 2*row_config[i][1]);
		printk(KERN_ERR "keypad_row[i].kbd_pio_stat_reg= 0x%x, keypad_row[i].kbd_pio_id= 0x%x \n",
				keypad_row[i].kbd_pio_stat_reg, keypad_row[i].kbd_pio_id);
		*/
		__REG(gpio_mode_reg[row_config[i][0]]+row_config[i][1])=gpio_pupd;//Input, registors according to LEVEL logic
		if(kbd_conf->flags & KBD_SC1445x_INT){
			__REG(gpio_mode_reg[row_config[i][0]]) |= GPIO_PID_INT0n+row_config[i][2];
			if(i<kbd_conf->rows)
				key_board_int_reg |= (0x0001)<<row_config[i][2];
		}
	}

	/***************** Enable KBD int ***************/
	if(kbd_conf->flags & KBD_SC1445x_INT){
		/* Configure debounce for key press and repeat timers */
		//KEY_DEBOUNCE_REG=(kbd_conf.int_debounce_timer & 0xFFFFFF) | ((kbd_conf.int_repeat_timer & 0xFFFFFF)<<6);
		KEY_DEBOUNCE_REG=0;//debounce, repeat are implemented using polling, in order to reduce intrs, and have more robust code

		/* Enable release int */
		key_board_int_reg |= (0x0800);

		/* Configure KBD int  priority */
		INT3_PRIORITY_REG=0x0070; //Set PR_LEVEL[0] (lowest priority)

		/* Configure KEY_BOARD_INT_REG */
		//KEY_BOARD_INT_REG=key_board_int_reg;

		//_enable_global_interrupt_();
	}
	return (key_board_int_reg);
#endif	//KBD_TEST_PC
}

/*******************************************************
	Description: 	Checks if any key is pressed
	Returns: 		The id of the first row where a key press was scanned, -1 : otherwise
 ********************************************************/
char fast_scan_keypad(){
	unsigned char col, row;
	char first_active_row;
#if defined (CONFIG_L_V2_BOARD) || defined (CONFIG_L_V3_BOARD) ||defined(CONFIG_L_V4_BOARD) ||defined(CONFIG_L_V5_BOARD)\
	||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	return 0;
#endif
	row=col=first_active_row=0;
	/* Chech if there is any keypad activity */
	SET_COLUMNS(col_mask);
	//if(kbd_conf->flags & KBD_SC1445x_LEVEL_HIGH){
		for(row=0;row<kbd_conf->rows;row++){
			if(*keypad_row[row].kbd_pio_stat_reg & keypad_row[row].kbd_pio_id){
				first_active_row = row+1;
				break;
			}
		}
	//}
	// else{
		// for(row=0;row<kbd_conf->rows;row++){
			// if(!(*keypad_row[row].kbd_pio_stat_reg & keypad_row[row].kbd_pio_id)){
				// first_active_row = row+1;
				// break;
			// }
		// }

	// }
#ifdef LEDS_MUX_COLS
	SET_COLUMNS(led_mask);
#endif

	return (first_active_row-1);
}

/*******************************************************
	Description: 	Checks which keys are pressed
				Updates a matrix of scan_code describing which rows are pressed per column, in the form:
				KBD_ROWS (8 BITS)	only the MAX_ROWS LSB bits are used (5)
		COL 0	000 00110
		COL 1	000 00000
		..
		MAX_COL   000 10000
	Returns: 		1:  if a key was pressed, 0 : otherwise
********************************************************/
#ifdef SCAN_ROUTINE_NORMAL//CONFIG_F_G2_BOARDS
/*******************************************************
	Logic: 	Sets all columns to 0. Loops between all columns and sets each column to 1 while it scans the status of all the rows.
********************************************************/
unsigned char scan_keypad(char first_active_row){
	unsigned char col, row;
	row=col=0;

#ifndef KBD_TEST_PC
	if(first_active_row == -1)
		return 1;
	col=0;
	while( col<kbd_conf->cols ){
		SET_COLUMNS(1<<col);//enable columns one by one
#ifdef CONFIG_F_G2_P1_BOARD
        {
            unsigned short delay;
            delay = kbd_conf->scanDelay;
            while (delay--);
        }
#endif
        {
            volatile unsigned short delay;
            //delay = kbd_conf->scanDelay;
            for (delay=0;delay<100;delay++);
        }



		scan_col[col]=0;
		for(row=first_active_row;row<kbd_conf->rows;row++){
			if(*keypad_row[row].kbd_pio_stat_reg & keypad_row[row].kbd_pio_id){
				scan_col[col] |= 1<<row;//shift
			}
		}

		// if(!(kbd_conf->flags & KBD_SC1445x_LEVEL_HIGH))
			// scan_col[col]=~scan_col[col];
		scan_col[col]&=(1<<kbd_conf->rows)-1;//mask out the unused rows
		col++;
	}
#ifdef LEDS_MUX_COLS
	SET_COLUMNS(led_mask);
#endif
#endif
	return 0;
}

#elif defined SCAN_ROUTINE_DIODE_DISCHARGE//(CONFIG_SC14452_DK_REV_C)
/*******************************************************
	Logic: 	Loops between all columns. Sets all rows to ouput 0 (to discharge the diodes).
	Sets all rows to input. Sets each column to 1 while it scans the status of all the rows.
********************************************************/
unsigned char scan_keypad(char first_active_row){
	unsigned char col, row, i;
	row=col=0;

#ifndef KBD_TEST_PC
	if(first_active_row == -1)
		return 1;
	col=0;

	while( col<kbd_conf->cols ){

		for(i=0; i<KBD_ROWS; i++){
			__REG(gpio_reset_reg[row_config[i][0]])= 1<<row_config[i][0]; //Rows Output 0
		}

		for(i=0; i<KBD_ROWS; i++){
			__REG(gpio_mode_reg[row_config[i][0]]+row_config[i][1])= 0x300; //Rows Output 0
		}

		SET_COLUMNS(0);// All columns 0

		for(i=0; i<KBD_ROWS; i++){
			__REG(gpio_mode_reg[row_config[i][0]]+row_config[i][1])= 0x200; //Rows Input
		}

		SET_COLUMNS(1<<col);//enable columns one by one

		scan_col[col]=0;
		for(row=first_active_row;row<kbd_conf->rows;row++){
			if((*keypad_row[row].kbd_pio_stat_reg & keypad_row[row].kbd_pio_id)){
				scan_col[col] |= 1<<row;//shift
			}
		}
		scan_col[col]&=(1<<kbd_conf->rows)-1;//mask out the unused rows
		col++;
	}
#if defined (CONFIG_L_V2_BOARD)||defined (CONFIG_L_V3_BOARD)||defined(CONFIG_L_V4_BOARD)||defined(CONFIG_L_V5_BOARD)\
	||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	SET_COLUMNS(0x00);
	return 0;
#endif
#ifdef LEDS_MUX_COLS
	SET_COLUMNS(led_mask);
#endif
#endif
	return 0;
}


#elif defined SCAN_ROUTINE_INVERT_LOGIC  // CONFIG_F_G2_BOARDS
/*******************************************************
	Logic: 	Sets all columns to 1. Loops between all columns and sets each column to 0 while it scans the status of all the rows.
********************************************************/
unsigned char scan_keypad(char first_active_row){
	unsigned char col, row, i;
	row=col=0;

#ifndef KBD_TEST_PC
	if(first_active_row == -1)
		return 1;

	//COL_WR_BUF=col_mask;//enable all columns
	memset(scan_col, 0, kbd_conf->cols);
	for(row=first_active_row;row<kbd_conf->rows;row++){
		SET_COLUMNS(col_mask);//enable all columns
		if(*keypad_row[row].kbd_pio_stat_reg & keypad_row[row].kbd_pio_id){
			for(col=0;col<kbd_conf->cols;col++){
				SET_COLUMNS((col_mask & ~(1<<col)));//disable columns one by one
#if !defined(CONFIG_RPS_BOARD)
				//Perform a dummy read to shift the bus fifo
				for (i=0; i<4; i++){
					dummy_read=COL_WR_BUF;//cpu_relax();
				}
#endif
				if(!(*keypad_row[row].kbd_pio_stat_reg & keypad_row[row].kbd_pio_id)){
					scan_col[col] |= 1<<row;//shift
				}
			}
		}
	}
#ifdef LEDS_MUX_COLS
	SET_COLUMNS(led_mask);
#endif
#endif
	return 0;
}
#	if (defined(CONFIG_L_V2_BOARD)||defined(CONFIG_L_V3_BOARD) ||defined(CONFIG_L_V4_BOARD))||defined(CONFIG_L_V5_BOARD)\
	||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
unsigned char g_ip8800_board_id;

unsigned char scan_ip8800_board_id (void)
{
	unsigned char col, row, i;
	row=col=0;

	//COL_WR_BUF=col_mask;//enable all columns
	memset(&g_ip8800_board_id, 0, sizeof(unsigned char));

	//SET_COLUMNS(colmask);//enable all columns
	SET_COLUMNS(0x00);//enable all columns
	row = KBD_ROWS;
	__REG(gpio_mode_reg[2]+KBD_TEST_BIT)=GPIO_PUPD_IN_PULLDOWN;//Input, registors according to LEVEL logic

	//if(*gpio_data_reg[2] & ((0x0001)<<KBD_TEST_BIT) )	{

	for (i=0; i<200; i++){ //required delay to settle row 7 to low
		dummy_read=COL_WR_BUF;//cpu_relax();
	}

	if ((*gpio_data_reg[2] & ((0x0001)<<KBD_TEST_BIT))==0)
	{
		for(col=0;col<KBD_COLUMNS;col++){
			//SET_COLUMNS((colmask & ~(1<<col)));//disable columns one by one
			SET_COLUMNS(((1<<col)));//disable columns one by one
#if !defined(CONFIG_RPS_BOARD)
			//Perform a dummy read to shift the bus fifo
			for (i=0; i<4; i++){
				dummy_read=COL_WR_BUF;//cpu_relax();
			}
#endif
			for (i=0; i<100; i++){
				dummy_read=COL_WR_BUF;//cpu_relax();
			}

			//if(!(*gpio_data_reg[2] & ((0x0001)<<KBD_TEST_BIT) ) )	{
			if((*gpio_data_reg[2] & ((0x0001)<<KBD_TEST_BIT) ) )	{
				g_ip8800_board_id |= 1<<col;//shift
				//printk("#####!!! col[%d]: enable \r\n",  col);
			}
		}
	}
printk ("\n#####!!! Board ID: 0x%x \r\n",  g_ip8800_board_id);
__REG(gpio_mode_reg[2]+KBD_TEST_BIT)=GPIO_PUPD_IN_PULLUP;

	return 0;
}
#	endif
#elif defined SCAN_ROUTINE_COLUMN_PORT_TRISTATE
/*******************************************************
	Logic: 	Sets all columns to input & pull up.
	Loops between all columns and sets each column to 0 while it scans the status of all the rows.
********************************************************/
unsigned char scan_keypad(char first_active_row){
	unsigned char col, row,i;
	row=col=0;

#ifndef KBD_TEST_PC
	if(first_active_row == -1)
		return 1;


	col=0;
	while( col<kbd_conf->cols ){
		for(i=0; i<kbd_conf->cols; i++){
			__REG(gpio_mode_reg[col_config[i][0]]+col_config[i][1])= GPIO_PUPD_IN_PULLUP; //All Columns Input pull up
		}
		
	
		__REG(gpio_mode_reg[col_config[col][0]]+col_config[col][1])= 0x300; //Column Output 0

		__REG(gpio_reset_reg[col_config[col][0]])= 1 << col_config[col][1];
		for (i=0; i<2; i++){
			dummy_read=*(volatile unsigned char*)0x140000;//cpu_relax();
		}
		scan_col[col]=0;
		for(row=first_active_row;row<kbd_conf->rows;row++){
			if(!(*keypad_row[row].kbd_pio_stat_reg & keypad_row[row].kbd_pio_id)){
				scan_col[col] |= 1<<row;//shift
			}
		}

		scan_col[col]&=(1<<kbd_conf->rows)-1;//mask out the unused rows

		
		__REG(gpio_set_reg[col_config[col][0]])= 1 << col_config[col][1];

		col++;

		
	}

	
	for(i=0; i<kbd_conf->cols; i++){
		__REG(gpio_mode_reg[col_config[i][0]]+col_config[i][1])= GPIO_PUPD_IN_PULLUP; //All Columns Input pull up
	}

#endif
	return 0;
}


#endif //SCAN_ROUTINE
/*******************************************************
	Description: 	Checks if extra_key is off
	Returns: 		1: on extra_key pressed, 0: otherwise
 ********************************************************/
unsigned char scan_extra_keys(){
	unsigned char i, result=0;
	//if(!(*keypad_extra_key.kbd_pio_stat_reg & keypad_extra_key.kbd_pio_id)){
	for(i=0; i<EXTRA_KEYS; i++){
#ifdef	CONFIG_IN_BOARDS
		if (extra_key_scan[i].flags &EXTRA_KEY_PRESSED)
			return 0;
#endif	/* CONFIG_IN_BOARDS */
		if(EXTRA_KEY_CHECK(i)){
			extra_key_scan[i].flags|=EXTRA_KEY_PRESSED;
			result|=(1<<i);
		}
	}
#ifdef CONFIG_RPS_BOARD
	EXTRA_KEY_CLEAR;
#endif
	return result ;
}

/****************************************************************************************************/
/************                                       HIGH LEVEL DRIVER FUNTIONS                                                            *********/
/****************************************************************************************************/

/*******************************************************
	Description: 	Open kbd dev file.

 ********************************************************/
int kbd_open(struct inode *inode, struct file *filp)
{
	kbd_sc1445x_dev_struct *dev; /* device information */
	dev = container_of(inode->i_cdev, kbd_sc1445x_dev_struct, cdev);
	filp->private_data = dev; /* for other methods */
	PDEBUG("KBD OPENS WITH FLAGS %d   \n", (filp->f_flags ));
	if (filp->f_flags & O_NONBLOCK){
		PDEBUG("KBD OPENED AS NONBLOCK \n");
	}
	/*If dev file is not opened as READ ONLY, initialize fsm*/
	if((filp->f_flags & O_ACCMODE) != O_RDONLY){
		PDEBUG("KBD NOT OPENED AS RDONLY \n");
		if (down_interruptible(&dev->sem))
			return -ERESTARTSYS;
		kbd_init_fsm();
		up (&dev->sem);
	}
	return 0;
}
/*******************************************************
	Description: 	Close device file.

 ********************************************************/
int kbd_release(struct inode *inode, struct file *filp)
{
	return 0;
}
/*******************************************************
	Description: 	Read kbd activity.

 ********************************************************/
// ssize_t kbd_read (struct file *filp, char __user *buf, size_t count, loff_t *pos)
// {
	// kbd_sc1445x_dev_struct *dev = filp->private_data;

	// if (down_interruptible(&dev->sem))
	// return -ERESTARTSYS;
	// while (dev->rp == dev->wp) { /* nothing to read */
		// up(&dev->sem); /* release the lock */
		// if (filp->f_flags & O_NONBLOCK)
			// return -EAGAIN;
		// PDEBUG("\"%s\" reading: going to sleep\n", current->comm);
		// if (wait_event_interruptible(dev->inq, (dev->rp != dev->wp)))
			// return -ERESTARTSYS; /* signal: tell the fs layer to handle it */
		// /* otherwise loop, but first reacquire the lock */
		// if (down_interruptible(&dev->sem))
			// return -ERESTARTSYS;
	// }
	// /* ok, data is there, return something */
	// if (dev->wp > dev->rp)
		// count = min(count, (size_t)(dev->wp - dev->rp));
	// else /* the write pointer has wrapped, return data up to dev->end */
		// count = min(count, (size_t)(dev->end - dev->rp));
	// if (copy_to_user(buf, dev->rp, count)) {
		// up (&dev->sem);
		// return -EFAULT;
	// }
	// dev->rp += count;
	// if (dev->rp == dev->end)
		// dev->rp = dev->buffer; /* wrapped */
	// up (&dev->sem);

	// /* finally, awake any writers and return */
	// wake_up_interruptible(&dev->outq);
	// PDEBUG("\"%s\" did read %li bytes\n",current->comm, (long)count);
	// return count;

// }
#if (defined (CONFIG_VT_BOARDS) || defined (CONFIG_VT_BOARDS_TEST))
void set_led_toggle_mask(unsigned char led_id, unsigned char led_timer){
	unsigned char i;

	switch(led_timer){
	case LED_TMOUT_OFF://led off
		for(i=0;i<LED_TOGGLE_MASKS;i++){
			kbd_conf->led_toggle_masks[i] &= ~(LED_D(led_id));
		}
		break;

	case LED_TMOUT_ON://led constantly on
		for(i=0;i<LED_TOGGLE_MASKS;i++){
			kbd_conf->led_toggle_masks[i] |= (LED_D(led_id));
		}
		break;

	case LED_TMOUT_O5://led blinking at 0,5 Hz
		//1 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[0], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[1], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[2], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[3], led_id);
		//1 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[4], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[5], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[6], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[7], led_id);
		//1 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[8], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[9], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[10], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[11], led_id);
		//1 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[12], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[13], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[14], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[15], led_id);
		break;

	case LED_TMOUT_1://led blinking at 1 Hz
		//1 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[0], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[1], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[2], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[3], led_id);
		//1 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[4], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[5], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[6], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[7], led_id);
		//1 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[8], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[9], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[10], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[11], led_id);
		//1 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[12], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[13], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[14], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[15], led_id);
		break;

	case LED_TMOUT_2://led blinking at 2 Hz
		//0,5 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[0], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[1], led_id);
		//0,5 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[2], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[3], led_id);
		//0,5 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[4], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[5], led_id);
		//0,5 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[6], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[7], led_id);
		//0,5 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[8], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[9], led_id);
		//0,5 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[10], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[11], led_id);
		//0,5 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[12], led_id);
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[13], led_id);
		//0,5 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[14], led_id);
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[15], led_id);
		break;

	case LED_TMOUT_4: //led blinking at 4 Hz
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[0], led_id);
		//0,25 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[1], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[2], led_id);
		//0,25 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[3], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[4], led_id);
		//0,25 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[5], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[6], led_id);
		//0,25 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[7], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[8], led_id);
		//0,25 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[9], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[10], led_id);
		//0,25 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[11], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[12], led_id);
		//0,25 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[13], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[14], led_id);
		//0,25 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[15], led_id);
		break;
	case LED_TMOUT_DB: //led double blinking
		//0,25 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[0], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[1], led_id);
		//0,25 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[2], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[3], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[4], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[5], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[6], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[7], led_id);

		//0,25 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[8], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[9], led_id);
		//0,25 second on
		LED_TOGGLE_MASK_ON(kbd_conf->led_toggle_masks[10], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[11], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[12], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[13], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[14], led_id);
		//0,25 second off
		LED_TOGGLE_MASK_OFF(kbd_conf->led_toggle_masks[15], led_id);
		break;
	}
}

void set_led_toggle_masks(unsigned char led_id, unsigned char led_timer){
	unsigned char i;
	if(led_id>TOTAL_LEDS)
		return;//invalid argument
	if(led_id == 0){//0: set mask to all leds
		for(i=0;i<TOTAL_LEDS;i++)
			set_led_toggle_mask(i, led_timer);
	}
	else
		set_led_toggle_mask(led_id, led_timer);
}
#endif
/*******************************************************
	Description: 	Kbd ioctl.

 ********************************************************/
int kbd_ioctl(struct inode *inode, struct file *filp,
                 unsigned int cmd, unsigned long arg)
{
	int retval=0;
	kbd_sc1445x_dev_struct *dev = filp->private_data;
	kbd_debug_drv_struct drv_debug;
	kbd_msg m;
#if (defined (CONFIG_VT_BOARDS) || defined (CONFIG_VT_BOARDS_TEST))
	kbd_led_ctrl_struct led_ctrl;
#endif
	PDEBUG("KBD IOCTL  \n");

	switch(cmd) {
/**** SET IOCTLS ****/
		case KBD_SC1445x_IOCTL_CONF_SET:
			if (down_interruptible(&dev->sem))
				return -ERESTARTSYS;
			if ( (copy_from_user((char*)&kbd_tmp, (char*)arg, sizeof(struct keypad_sc1445x_conf_struct))) ) {
				printk(KERN_WARNING "Copy from user failed\n");
				up (&dev->sem);
				return -EFAULT;
			}
			if( (retval=check_keypad_conf_validity(&kbd_tmp))){
				printk(KERN_WARNING "Invalid kbd configuration\n");
				up (&dev->sem);
				return(retval);
			}
			memcpy( (char*)&kbd_init, (char*)&kbd_tmp, sizeof(struct keypad_sc1445x_conf_struct) );
#ifndef KBD_TEST_PC
			if(kbd_configure()){
				up (&dev->sem);
				return -EFAULT;
			}
#ifndef CONFIG_CVM480_SPI_DECT_SUPPORT
			if(kbd_conf->flags & KBD_SC1445x_INT)
				KEY_BOARD_INT_REG=kbd_dev.key_board_int_reg;
#endif
#endif
			up (&dev->sem);
			break;
		case KBD_SC1445x_IOCTL_LEDS_SET:
			if (down_interruptible(&dev->sem))
				return -ERESTARTSYS;

#if (defined (CONFIG_VT_BOARDS) || defined (CONFIG_VT_BOARDS_TEST))
			if ( (copy_from_user((char*)&led_ctrl, (char*)arg, sizeof(kbd_led_ctrl_struct))) ) {
				printk(KERN_WARNING "Copy from user failed\n");
				up (&dev->sem);
				return -EFAULT;
			}
			set_led_toggle_masks(led_ctrl.led_id, led_ctrl.timer);
#else
			if ( (copy_from_user((char*)kbd_conf->led_toggle_masks, (char*)arg, 2*sizeof(unsigned short))) ) {
				printk(KERN_WARNING "Copy from user failed\n");
				up (&dev->sem);
				return -EFAULT;
			}
#endif
#if defined (CONFIG_L_V1_BOARD)
			if ( (copy_from_user((char*)kbd_conf->led_toggle_tmouts, (char*)arg+4, 2*sizeof(unsigned char))) ) {
				printk(KERN_WARNING "Copy from user failed\n");
				up (&dev->sem);
				return -EFAULT;
			}
#endif
			led_toggle.index=0;
			led_toggle.tmout=0;
			up (&dev->sem);
			break;
/**** GET IOCTLS ****/
		case KBD_SC1445x_IOCTL_CONF_GET:
			if (down_interruptible(&dev->sem))
				return -ERESTARTSYS;
			if(copy_to_user((char*)arg, (char*)kbd_conf, sizeof(struct keypad_sc1445x_conf_struct))){
				printk(KERN_WARNING "Ioctl failed \n");
				up (&dev->sem);
				return -EFAULT;
			}
			up (&dev->sem);
			break;
		case KBD_SC1445x_IOCTL_DRV_INFO_GET:
			if (down_interruptible(&dev->sem))
				return -ERESTARTSYS;
			drv_debug.buffer=dev->buffer;
			//drv_debug.end=dev->end;
			drv_debug.buffersize=dev->buffersize;
			drv_debug.wp=dev->wp;
			drv_debug.rp=dev->rp;
			drv_debug.free_space = spacefree(dev);
			drv_debug.ver_major = dev->ver_major;
			drv_debug.ver_minor = dev->ver_minor;
			if(copy_to_user((char*)arg, (char*)&drv_debug, sizeof(kbd_debug_drv_struct))){
				printk("Ioctl failed \n");
				up (&dev->sem);
				return -EFAULT;
			}
			up (&dev->sem);
			break;
		case KBD_SC1445x_IOCTL_INITCONF_GET:
			if (down_interruptible(&dev->sem))
				return -ERESTARTSYS;
			if(copy_to_user((char*)arg, (char*)&kbd_init, sizeof(struct keypad_sc1445x_conf_struct))){
				printk(KERN_WARNING "Ioctl failed \n");
				up (&dev->sem);
				return -EFAULT;
			}
			up (&dev->sem);
			break;
		case KBD_SC1445x_IOCTL_LEDS_GET:
			if (down_interruptible(&dev->sem))
				return -ERESTARTSYS;
			if(copy_to_user((char*)arg, (char*)kbd_conf->led_toggle_masks, 2*sizeof(short))){
				printk(KERN_WARNING "Ioctl failed \n");
				up (&dev->sem);
				return -EFAULT;
			}
			up (&dev->sem);
			break;
/**** DEBUGGING IOCTLS ****/
//#ifdef KBD_SC1445x_DEBUG
		case KBD_SC1445x_IOCTL_CONF_WRITE:
			if (down_interruptible(&dev->sem))
				return -ERESTARTSYS;
			//if (copy_from_user((char*)&buf[0], (char*)arg, 3 ) ) {
			if (copy_from_user((char*)&m, (char*)arg, sizeof(kbd_msg) ) ) {
				printk(KERN_WARNING "Copy from user failed\n");
				up (&dev->sem);
				return -EFAULT;
			}
			up (&dev->sem);
// 			kbd_send_msg(buf[0], *(unsigned short*)&buf[1]);
//       printk(KERN_WARNING "buf[0]=%d \n", buf[0]);
//       printk(KERN_WARNING "buf[1]=%d \n", buf[1]);
//       printk(KERN_WARNING "buf[2]=%d \n", buf[2]);
		    kbd_send_msg(m.msg_code, m.key_id);
			break;
//#endif

	    case KBD_SC1445x_IOCTL_CONF_READ:
	      if (down_interruptible(&dev->sem))
	        return -ERESTARTSYS;
			while (dev->rp == dev->wp) { /* nothing to read */
#ifndef TEST_SEMAPHORES
				up(&dev->sem); /* release the lock */
				if (filp->f_flags & O_NONBLOCK)
#endif
					return -EAGAIN;
				PDEBUG("\"%s\" reading: going to sleep\n", current->comm);
				if (wait_event_interruptible(dev->inq, (dev->rp != dev->wp)))
					return -ERESTARTSYS; /* signal: tell the fs layer to handle it */
				/* otherwise loop, but first reacquire the lock */
				if (down_interruptible(&dev->sem))
					return -ERESTARTSYS;
				//PDEBUG("IOCTL READ got semaphore\n");
	      }
	      /* ok, data is there, return something */
	      if(copy_to_user((char*)arg, (char*)&(dev->buffer[dev->rp]), sizeof(kbd_msg))) {
	        printk(KERN_WARNING "Ioctl failed \n");
#ifndef TEST_SEMAPHORES
	        up (&dev->sem);
#endif
	        return -EFAULT;
	      }
	      m=dev->buffer[dev->rp];
	      // printk(KERN_WARNING "msg_id=%x \n", m.msg_code);
	      // printk(KERN_WARNING "key_id=%x \n", m.key_id);
	      dev->rp = (dev->rp+1)%(dev->buffersize) ;
#ifndef TEST_SEMAPHORES
	      up (&dev->sem);
#endif

	      break;
#if (defined(CONFIG_L_V2_BOARD)||defined(CONFIG_L_V3_BOARD) ||defined(CONFIG_L_V4_BOARD)||defined(CONFIG_L_V5_BOARD)\
		||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX))
		case KBD_SC1445x_IOCTL_BOARD_ID:
			if (down_interruptible(&dev->sem))
				return -ERESTARTSYS;
			if(copy_to_user((char*)arg, (char*)&g_ip8800_board_id, sizeof(unsigned char))){
				printk(KERN_WARNING "Ioctl failed \n");
				up (&dev->sem);
				return -EFAULT;
			}
			up (&dev->sem);
			break;
#endif
		default:
			printk(KERN_WARNING "Invalid keypad ioctl \n");
			return -ENOTTY;
	}
	return retval;
}
/*******************************************************
	Description: 	Function implementing the poll system call.

 ********************************************************/
unsigned int kbd_poll(struct file *filp, poll_table *wait)
{
	kbd_sc1445x_dev_struct *dev = filp->private_data;
	unsigned int mask = 0;

	down(&dev->sem);
	poll_wait(filp, &dev->inq,  wait);
	if (dev->rp != dev->wp)
		mask |= POLLIN | POLLRDNORM;	/* readable */
	up(&dev->sem);
	return mask;
}
/*******************************************************
	Description: 	Tasklet called by the interrupt handler for running kbd fsm.

 ********************************************************/
void kbd_tasklet_fn(unsigned long param)
{
	/* Scan key pad */
	scan_keypad(0);
	check_kbd();
	START_TIMER(kbd_timer, kbd_conf->polling_timer);
}
#ifdef SC1445x_KBD_SYS_POLLING
/*******************************************************
	Description: 	Tasklet called by the interrupt handler for running kbd fsm.

 ********************************************************/
void kbd_sys_timer_tasklet_fn(void)
{
	/* Scan key pad */
	scan_keypad(0);
	check_kbd();
}
#endif
/*******************************************************
	Description: 	The kbd interrupt handler .

 ********************************************************/
static irqreturn_t kbd_int_handler(int irq, void *dev_id, struct pt_regs *regs)
{
#ifndef KBD_TEST_PC
	RESET_INT_PENDING_REG=0x0002;//reset pending KBD int
	/* Put a scan work to the work queue */
	//queue_work(kbd_workqueue, &kbd_work);
	tasklet_schedule(&kbd_tasklet);
	// _disable_interrupt_();
	//_retx_();
	return IRQ_HANDLED;
#endif
}
/*******************************************************
	Description: 	Timer function used in POLLING mode

 ********************************************************/
void kbd_timer_poll_fn(unsigned long param){

#ifdef SCAN_ROUTINE_COLUMN_PORT_TRISTATE
	char first_row=0;//fast_scan_keypad();
#else
	char first_row=fast_scan_keypad();
#endif
	scan_extra_keys();
	check_extra_keys();
	toggle_led_mask();
	if( (first_row != -1) || total_active_keys)	{
#ifdef SC1445x_KBD_SYS_POLLING
		tasklet_schedule(&kbd_sys_timer_tasklet);
#else
		scan_keypad(first_row);
		check_kbd();
#endif
	}
	START_TIMER(kbd_timer, kbd_conf->polling_timer);

}
/*******************************************************
	Description: 	Timer function used in INTERRUPT mode

 ********************************************************/
void kbd_timer_int_fn(unsigned long param)
{
	/* Scan key pad */
	scan_keypad(0);
	check_kbd();
	if(total_active_keys){
		START_TIMER(kbd_timer, kbd_conf->polling_timer);
	}
}

/*******************************************************
	Description: 	Checks the empty space of the buffer used for storing kbd msgs
	Returns:		The free buffer space
 ********************************************************/
unsigned char spacefree(kbd_sc1445x_dev_struct *dev)
{
	if (dev->rp == dev->wp)
		return dev->buffersize - 1;
	return ((dev->rp + dev->buffersize - dev->wp) % dev->buffersize) - 1;
}
/*******************************************************
	Description: 	Constructs a msg from  msg_id and key_id and calls kbd_buffer_input to put it into the circular buffer


 ********************************************************/
unsigned short kbd_send_msg(unsigned char msg_id, unsigned short key_id){
	kbd_msg m;
	unsigned char row;
	m.msg_code=msg_id;
	m.key_id=key_id;
	//printk("kbd_send_msg input: %x, %x\n", msg_id, key_id );
	/*Check key_locks*/
	if ((key_id != HOOK_DUMMY_KEY_ID) && (key_id != CALL_END_DUMMY_KEY_ID)){
		row=0;
		while(!(key_id & 1<<row++)&& row<KBD_ROWS);
		row--;key_id>>=KBD_ROWS;
		if(kbd_conf->key_locks[row] & key_id)
			return 0;//key is locked. abort
	}
#ifdef KBD_SC1445x_DEBUG
	char row,col;
	row=col=0;//key_scan[key_scan_index].pre_row;
	while(!(key_id & 1<<row++) && row<KBD_ROWS);row--;
	key_id>>=KBD_ROWS;
	while(!(key_id & 1<<col++) && col<KBD_COLUMNS);col--;
	switch(msg_id){
		case KBD_SC1445x_RELEASE_MSG:
			PDEBUG("KBD_RELEASE: col= %d, row= %d\n", col, row);
			break;
		case KBD_SC1445x_PRESS_MSG:
			PDEBUG("KBD_PRESS: col= %d, row= %d\n", col, row);
			break;
		case KBD_SC1445x_REPEAT_MSG:
			PDEBUG("KBD_REPEAT: col= %d, row= %d\n", col, row);
			break;
		case KBD_SC1445x_HOOK_ON_MSG:
			PDEBUG("HOOK_ON\n");
			break;
		case KBD_SC1445x_HOOK_OFF_MSG:
			PDEBUG("HOOK_OFF\n");
			break;
		case KBD_SC1445x_CALL_END_PRESSED_MSG:
			PDEBUG("CALL_END_PRESSED\n");
			break;
		case KBD_SC1445x_CALL_END_RELEASED_MSG:
			PDEBUG("CALL_END_RELEASED\n");
			break;
        default:
			PDEBUG("err! KBD msgid unknown = %x\n", msg_id);
            break;
	}
#endif


	return (kbd_buffer_input(&kbd_dev, &m));

}
/*******************************************************
	Description: 	Sends PRESS, RELEASE, REPEAT msgs to the application

 ********************************************************/
unsigned short kbd_buffer_input(kbd_sc1445x_dev_struct *dev, kbd_msg *msg){
	// if (down_interruptible(&dev->sem))
		// return -ERESTARTSYS;
	spin_lock_bh(&dev->lock);

	if(!spacefree(dev)){
		PDEBUG("kbd_driver: Circular buffer full\n");
		spin_unlock_bh(&dev->lock);
		//up(&dev->sem);
		return 0;
	}

	memcpy((char*)&dev->buffer[dev->wp],(char*)msg, sizeof(kbd_msg));
	dev->wp=(dev->wp+1)%(dev->buffersize);

	//up(&dev->sem);
	spin_unlock_bh(&dev->lock);
#ifdef TEST_SEMAPHORES
	if(spacefree(dev)<dev->buffersize-4){//release 4 sems after 4 events only
		for(i=0;i<4;i++){
			//PDEBUG("kbd_driver: Released read lock\n");
			up(&dev->sem);
		}
	}
#endif
	/* finally, awake any reader */
	wake_up_interruptible(&dev->inq);  /* blocked in read() and select() */

	return 0;
}
/*******************************************************
	Description: 	Module initialization.

 ********************************************************/
int __init kbd_init_module(void)
{
	int i, result;
	dev_t dev = 0;

	/* Initializations*/
	PDEBUG("module init function, entry point\n");
/*
 * Get a range of minor numbers to work with, asking for a dynamic
 * major unless directed otherwise at load time.
 */
	if (kbd_sc1445x_major) {
		dev = MKDEV(kbd_sc1445x_major, kbd_sc1445x_minor);
		result = register_chrdev_region(dev, 1, "kbd");
	}
	else {
		result = alloc_chrdev_region(&dev, kbd_sc1445x_minor, 1, "kbd");
		kbd_sc1445x_major = MAJOR(dev);
	}
	if (result < 0) {
		printk(KERN_WARNING "kbd_sc1445x: can't get major %d\n", kbd_sc1445x_major);
		return result;
	}

	/*Initialize driver internal vars*/
	memset((char*)&kbd_dev, 0, sizeof(kbd_sc1445x_dev_struct));
	kbd_dev.ver_major=KBD_SC1445x_MAJ_VER;
	kbd_dev.ver_minor=KBD_SC1445x_MIN_VER;
#ifndef SC1445x_KBD_SYS_POLLING
	init_timer(&kbd_timer);
#endif
	/*Initialize arrays in kbd_init configuration*/
#ifdef CONFIG_L_V1_BOARD
	kbd_init.led_toggle_tmouts[0]=LED_TOGGLE_TIMEOUTS;
	kbd_init.led_toggle_tmouts[1]=LED_TOGGLE_TIMEOUTS;
	kbd_init.led_toggle_masks[0]=(1<<TOTAL_LEDS) - 1;
	kbd_init.led_toggle_masks[1]=(1<<TOTAL_LEDS)- 1;
#else
	kbd_init.led_toggle_tmouts=LED_TOGGLE_TIMEOUTS;
	for(i=0;i<LED_TOGGLE_MASKS;i++){
		kbd_init.led_toggle_masks[i] = 0;
	}
#endif


	for(i=0;i<KBD_ROWS;i++){
		kbd_init.key_locks[i] = 0;
	}

	if(kbd_configure())
		goto fail;//up to this point only the dev region should be unregistered

    /* Initialize device. */
	init_waitqueue_head(&(kbd_dev.inq));
#ifdef TEST_SEMAPHORES
	sema_init(&kbd_dev.sem, 0);
#else
	init_MUTEX(&kbd_dev.sem);
#endif

	spin_lock_init(&kbd_dev.lock);
	kbd_setup_cdev(&kbd_dev);
	/*Enable kbe int if needed*/
#ifndef SC1445x_KBD_SYS_POLLING
#ifndef CONFIG_CVM480_SPI_DECT_SUPPORT
	if(kbd_conf->flags & KBD_SC1445x_INT)
		KEY_BOARD_INT_REG=kbd_dev.key_board_int_reg;
#endif
#endif
	printk(KERN_NOTICE "kbd_sc1445x: driver initialized with major devno: %d\n", kbd_sc1445x_major);

	return 0; /* succeed */

  fail:
	unregister_chrdev_region(dev, 1);
	return result;
}

/*******************************************************
	Description: 	Configures driver based on configuration stored in kbd_init struct.
	Notes:		Should be called atomic.
				Kbd int should be disabled before calling this function, and enabled afterwards if needed (by checking kbd_conf->flags)
 ********************************************************/
int kbd_configure(void){
	int result=0;
	kbd_sc1445x_dev_struct *dev = &kbd_dev; /* device information */
	unsigned char i;

#ifndef KBD_TEST_PC
	/* First Disable kbd intr*/
#ifndef CONFIG_CVM480_SPI_DECT_SUPPORT
	KEY_BOARD_INT_REG=0x0000;
#endif

	/*Reset previous configuration, if any. Check flags because in module_init no timers, irqs have been initialized yet*/
	if(!(kbd_conf->flags & KBD_SC1445x_INT)){
		STOP_TIMER(kbd_timer);
	}
	else if(kbd_conf->flags & KBD_SC1445x_INT){
		STOP_TIMER(kbd_timer);
		free_irq(kbd_conf->irq, NULL);
	}
	if(dev->buffer){
		kfree(dev->buffer);
		dev->buffer = NULL;
	}
#endif
	/*Copy init configuration*/
	memcpy(kbd_conf, &kbd_init, sizeof(struct keypad_sc1445x_conf_struct));

	/*Apply new configuration*/
	//setup the circular buffer
	if (!dev->buffer) {
		dev->buffer = kmalloc(kbd_conf->buffersize*sizeof(kbd_msg), GFP_KERNEL);
		dev->buffersize = kbd_conf->buffersize;
		if (!dev->buffer) {
			printk(KERN_INFO "kbd_sc1445x: can't allocate buffer \n");
			return -ENOMEM;
		}
	}
	dev->rp = dev->wp = 0; /* rd and wr from the beginning */

#ifndef KBD_TEST_PC
	//reset key_scan struct (kbd fsm)
	for(i=0;i<MAX_MULT_KEYS;i++){
		key_scan[i].state=KBD_IDLE_STATE;
		key_scan[i].pre_row=key_scan[i].pre_col=0x00;
		key_scan[i].tmout=0x00;
	}
	//reset extra_key_scan struct
	for(i=0; i<EXTRA_KEYS; i++){
#ifdef CONFIG_VT_V1_BOARD//In vtech boards PRESSED = ON_HOOK
		extra_key_scan[i].state=KBD_PRESS_STATE;
		extra_key_scan[i].flags=EXTRA_KEY_PRESSED;
#else			//	PRESSED = OFF_HOOK
		extra_key_scan[i].state=KBD_IDLE_STATE;
		extra_key_scan[i].flags=0;//extra_key is released
#endif
		extra_key_scan[i].tmout=0x00;
	}

	//init other vars
	col_mask=0;
	for(i=0;i<kbd_conf->cols;i++){
		col_mask |= 1<<i;//create a mask for all columns
	}

	//reset led toggle struct
//	led_toggle.masks[0]=0;
//	led_toggle.masks[1]=0;
	led_toggle.index=0;
	led_toggle.tmout=0;
//#ifdef CONFIG_L_BOARDS
//	for(i=0;i<TOTAL_LEDS;i++){
//		kbd_conf->led_toggle_masks[0]|=1<<i;
//		kbd_conf->led_toggle_masks[1]|=1<<i;
//	}
//#endif
#if defined(CONFIG_L_V2_BOARD) || defined(CONFIG_L_V3_BOARD) ||defined(CONFIG_L_V4_BOARD)||defined(CONFIG_L_V5_BOARD)\
	||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	scan_ip8800_board_id();
#endif


#ifdef CONFIG_F_G2_BOARDS  //F_G2_BOARDS, Foxconn modify, Vincent Chen, 08/12/2009, in LH
    led_mask = 0;//led mask is not used in this configuration
#else //CONFIG_F_G2_BOARDS
	if(kbd_conf->flags & KBD_SC1445x_INT){//in INT mode all cols should be high.. therefor LEDS CANNOT be controlled.
		led_mask=col_mask;
	}//In polling mode it is not initialized here, so that the leds configuration is not lost on ioctl open in write mode
#endif //CONFIG_F_G2_BOARDS

	//start polling timer or request irq
	if(!(kbd_conf->flags & KBD_SC1445x_INT)){
		/* register the timer */
#ifdef SC1445x_KBD_SYS_POLLING
		sc1445x_kbd_polling_func=kbd_timer_poll_fn;
#else
		kbd_timer.data = 0;//no argument for kbd_timer_fn needed
		kbd_timer.function = kbd_timer_poll_fn;
#endif
		START_TIMER(kbd_timer, kbd_conf->polling_timer);
	}
	else if (kbd_conf->flags & KBD_SC1445x_INT){
		/* register the timer */
#ifdef SC1445x_KBD_SYS_POLLING
		sc1445x_kbd_polling_func=kbd_timer_int_fn;
#else
		kbd_timer.data = 0;//no argument for kbd_timer_fn needed
		kbd_timer.function = kbd_timer_int_fn;
#endif
		/* Request the IRQ */
		result = request_irq(kbd_conf->irq, (irq_handler_t)kbd_int_handler, 0, "kbd_intr", NULL);

		if (result) {
			printk(KERN_INFO "kbd_sc1445x: can't get assigned irq %i\n",
					kbd_conf->irq);
			goto fail;
		}
	}


	/*Initialize hw */
	dev->key_board_int_reg=hw_init();

#endif
	return result;
	fail:
		if(dev->buffer){
			kfree(dev->buffer);
			dev->buffer = NULL;
		}
		return (result);

}
/*******************************************************
	Description: 	Initializes kbd and extra_key fsm.
	Notes:		Should be called atomic.
				This has become a function in order to be called on kbd_open (and get the current status of kbd and extra_key)
 ********************************************************/
void kbd_init_fsm(void){
	unsigned char i;

	kbd_sc1445x_dev_struct *dev = &kbd_dev; /* device information */
	//clear the msg buffer
	dev->rp = dev->wp = 0; /* rd and wr from the beginning */
	//reset key_scan struct (kbd fsm)
	for(i=0;i<MAX_MULT_KEYS;i++){
		key_scan[i].state=KBD_IDLE_STATE;
		key_scan[i].pre_row=key_scan[i].pre_col=0x00;
		key_scan[i].tmout=0x00;
	}
	//reset extra_key_scan struct
	for(i=0; i<EXTRA_KEYS; i++){
		extra_key_scan[i].state=KBD_IDLE_STATE;
		extra_key_scan[i].flags=0;//extra_key is released
		extra_key_scan[i].tmout=0x00;
	}
	//restart timer if operating in POLLING mode
	if(!(kbd_conf->flags & KBD_SC1445x_INT)){//POLLING MODE
		STOP_TIMER(kbd_timer);
		START_TIMER(kbd_timer, kbd_conf->polling_timer);
	}
	PDEBUG("KBD INITIALIZED  \n");
}
/*******************************************************
	Description: 	Cleanup the module and handle initialization failures .

 ********************************************************/
void kbd_cleanup_module(void)
{
	dev_t devno = MKDEV(kbd_sc1445x_major, kbd_sc1445x_minor);

	/* Get rid of our char dev entries */
	cdev_del(&kbd_dev.cdev);
	kfree(kbd_dev.buffer);
#ifdef KBD_SC1445x_DEBUG /* use proc only if debugging */
	//kbd_remove_proc();
#endif
	printk(KERN_NOTICE "kbd_sc1445x: cleanup module\n");
	/* cleanup_module is never called if registering failed */
	unregister_chrdev_region(devno, 1);

}

/*******************************************************
	Description: 	Set up the cdev structure for this device.

 ********************************************************/
void kbd_setup_cdev(kbd_sc1445x_dev_struct *dev)
{
	int err, devno = MKDEV(kbd_sc1445x_major, kbd_sc1445x_minor);

	cdev_init(&dev->cdev, &kbd_sc1445x_fops);
	dev->cdev.owner = THIS_MODULE;
	dev->cdev.ops = &kbd_sc1445x_fops;
	err = cdev_add (&dev->cdev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE "Error %d adding kbd_sc1445x", err);
}

module_init(kbd_init_module);
module_exit(kbd_cleanup_module);



