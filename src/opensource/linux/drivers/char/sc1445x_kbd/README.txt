SC14450 DRIVER

version 0.8
---------------------------------------------------------------------
-Fix bug of starting timer that is already running in kbd_init.

version 0.7
---------------------------------------------------------------------
-Add proper initializations toy cor and row configuration according to MAX_COL, MAX_ROW defines.

version 0.6
---------------------------------------------------------------------
-Add logic and ioctl cmd for toggling led mask.

version 0.5
---------------------------------------------------------------------
-Change the ioctl led mask cmd to pass parameters directly.

version 0.4
---------------------------------------------------------------------
-Add test for semaphores (enable it using TEST_SEMAPHORES switch).
-Add definitions for HI and MID|LO_END kbd versions.

version 0.3
---------------------------------------------------------------------
-Add support for contolling leds status in polling mode. NOTE: In interrupt mode, leds cannot be controlled.

version 0.2
---------------------------------------------------------------------
-Changed locking of kbd_buffer_input() using spinlock instead of semaphore


version 0.1
---------------------------------------------------------------------
-Added versioning (ver_major, ver_minor defined in kbd_sc14450_dev_struct, kbd_debug_drv struct)
-Create kbd_init_fsm which is called on kbd_open to fix bug of losing current kbd and hook state