/*
 * kbd_sc1445x.h -- definitions for the char driver
 * Based on the code from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.
 *
 *
 */

#ifndef _KBD_SC1445x_H_
#define _KBD_SC1445x_H_


#if defined(CONFIG_L_V2_BOARD)||defined(CONFIG_L_V3_BOARD)||defined(CONFIG_L_V4_BOARD)||defined(CONFIG_L_V5_BOARD)\
	||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	#ifdef CONFIG_RAM_SIZE_32MB
			#define OVER_16M_SDRAM_ACCESS_PATCH_FLAG register unsigned long m_32m_patch_flag
			#define PROTECT_OVER_16M_SDRAM_ACEESS	{ local_irq_save(m_32m_patch_flag); EBI_ACS4_CTRL_REG = 9;}
			#define RESTORE_OVER_16M_SDRAM_ACEESS	{ EBI_ACS4_CTRL_REG = 0xa; local_irq_restore(m_32m_patch_flag); }
	#else

		#define OVER_16M_SDRAM_ACCESS_PATCH_FLAG
		#define PROTECT_OVER_16M_SDRAM_ACEESS
		#define RESTORE_OVER_16M_SDRAM_ACEESS
	#endif
#endif

/*Configuration definitions*/
//Debug - temp definitions
//#define SC1445x_KBD_SYS_POLLING//Use system timer for polling
//#define TEST_SEMAPHORES //test semaphores operation. On ioctl read semaphore is only acquired, and on kbd_buffer_input sem is only released (after 4 events)
//#define KBD_SC1445x_DEBUG//prints debug msgs
/*HW specific switches*/

//#define CONFIG_VT_BOARDS_TEST This option is used for emulating VTECH board on a SC14452 DK
/*MACROS*/
#define LED_TOGGLE_MASK_OFF(MASK, LED_ID) (MASK &=~(LED_D(LED_ID)))
#define LED_TOGGLE_MASK_ON(MASK, LED_ID) (MASK |=(LED_D(LED_ID)))
/*HW specific definitions*/
/*Leds*/
#if defined(CONFIG_F_G2_P1_BOARD)
    //LED Definitions
	#define LED_D(X)  (unsigned short)(0x0100<<(X-1))
	#define LED_RING2  			LED_D(1)
	#define LED_RING1			LED_D(2)
	#define LED_LINE1			LED_D(3)
	#define LED_LINE2			LED_D(4)
	#define LED_SPEAKER			LED_D(5)
	#define LED_MUTE			LED_D(6)
	#define LED_EARPHONE		LED_D(7)
	#define LED_MSGWAIT			LED_D(8)

	#define LED_TOGGLE_MASKS	2
	#define LED_TOGGLE_TIMEOUTS	200

    //Keypad Layout
	#define KBD_COLUMNS 6
	#define KBD_ROWS 5
	#define ETH_RST_COL (1<<6)

	//Extra keys
	#define EXTRA_KEYS			1 //the hook switch
	#define HOOK_KEY			0
	#define HOOK_FLASH_TMOUTS	0

#elif defined(CONFIG_F_G2_P2_2PORT_BOARD)
    //LED Definitions
	#define TOTAL_LEDS			8//the total number of LEDS
	#define LED_D(X)  			(1<<(X-1))
	#define LED_LINE2			LED_D(1)
	#define LED_RING1			LED_D(2)
	#define LED_SPEAKER			LED_D(3)
	#define LED_MUTE			LED_D(4)
	#define LED_EARPHONE		LED_D(5)
	#define LED_LINE1			LED_D(6)
	#define LED_MSGWAIT			LED_D(7)
	#define LED_RING2  			LED_D(8)

	#define LED_TOGGLE_MASKS	2
	#define LED_TOGGLE_TIMEOUTS	200

    //Keypad Layout
	#define KBD_COLUMNS 6
	#define KBD_ROWS 5

	//Extra keys
	#define EXTRA_KEYS			2 //the hook switch and the headset_mic
	#define HOOK_KEY			0
	#define HS_MIC_KEY			1
	#define HOOK_FLASH_TMOUTS	0

#elif defined(CONFIG_F_G2_ST7567_2PORT_BOARD)
    //LED Definitions
	#define TOTAL_LEDS			1//the total number of LEDS
	#define LED_D(X)  			(1<<(X-1))
	#define LED_LINE2			LED_D(1)


    //Keypad Layout
	#define KBD_COLUMNS 5
	#define KBD_ROWS 5

	//Extra keys
	#define EXTRA_KEYS			1 //the hook switch
	#define HOOK_KEY			0
	#define HOOK_FLASH_TMOUTS	0

#elif defined (CONFIG_L_V1_BOARD)
  //LED Definitions
	#define TOTAL_LEDS		1//the total number of LEDS
	#define LED_D(X)  		(1<<(X-1))
	#define LED_MSG				LED_D(1)

	#define LED_TOGGLE_MASKS		2
	#define LED_TOGGLE_TIMEOUTS	200

  //Keypad Layout
	#define KBD_COLUMNS 6
	#define KBD_ROWS 5

	//Extra keys
	#define EXTRA_KEYS				1 //the hook switch
	#define HOOK_KEY					0
	#define HOOK_FLASH_TMOUTS	0
#elif defined (CONFIG_L_V2_BOARD) ||defined(CONFIG_L_V5_BOARD)
    //LED Definitions
	#define TOTAL_LEDS			0//1//the total number of LEDS
	#define LED_D(X)  			(1<<(X-1))
	#define LED_MSG				LED_D(1)

	#define LED_TOGGLE_MASKS	2
	#define LED_TOGGLE_TIMEOUTS	200

    //Keypad Layout
	#define KBD_COLUMNS 8
	#define KBD_ROWS 7
	#define ETH_RST_BLOFF_COL (0x4000)

	//Extra keys
	#define EXTRA_KEYS			1 //the hook switch
	#define HOOK_KEY			0
	#define HOOK_FLASH_TMOUTS	0
	#define KBD_TEST_BIT 12
#elif defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
    //LED Definitions
	#define TOTAL_LEDS			0//1//the total number of LEDS
	#define LED_D(X)  			(1<<(X-1))
	#define LED_MSG				LED_D(1)

	#define LED_TOGGLE_MASKS	2
	#define LED_TOGGLE_TIMEOUTS	200

    //Keypad Layout
	#define KBD_COLUMNS 8
	#define KBD_ROWS 7
	#define ETH_RST_BLOFF_COL (0x4000)

	//Extra keys
	#define EXTRA_KEYS			1 //the hook switch
	#define HOOK_KEY			0
	#define HOOK_FLASH_TMOUTS	0
	#define KBD_TEST_BIT 12
#elif defined (CONFIG_L_V3_BOARD_CNX)
    //LED Definitions
	#define TOTAL_LEDS			0//1//the total number of LEDS
	#define LED_D(X)  			(1<<(X-1))
	#define LED_MSG				LED_D(1)

	#define LED_TOGGLE_MASKS	2
	#define LED_TOGGLE_TIMEOUTS	200

    //Keypad Layout
	#define KBD_COLUMNS 8
	#define KBD_ROWS 5
	#define ETH_RST_BLOFF_COL (0x4000)

	//Extra keys
	#define EXTRA_KEYS			1 //the hook switch
	#define HOOK_KEY			0
	#define HOOK_FLASH_TMOUTS	0

	#define KBD_TEST_BIT 2
#elif defined (CONFIG_L_V3_BOARD)
    //LED Definitions
	#define TOTAL_LEDS			0//1//the total number of LEDS
	#define LED_D(X)  			(1<<(X-1))
	#define LED_MSG				LED_D(1)

	#define LED_TOGGLE_MASKS	2
	#define LED_TOGGLE_TIMEOUTS	200

    //Keypad Layout
	#define KBD_COLUMNS 8
	#define KBD_ROWS 5
	#define ETH_RST_BLOFF_COL (0x4000)

	//Extra keys
	#define EXTRA_KEYS			1 //the hook switch
	#define HOOK_KEY			0
	#define HOOK_FLASH_TMOUTS	0

	#define KBD_TEST_BIT 6
#elif defined (CONFIG_L_V4_BOARD)
    //LED Definitions
	#define TOTAL_LEDS			0//1//the total number of LEDS
	#define LED_D(X)  			(1<<(X-1))
	#define LED_MSG				LED_D(1)

	#define LED_TOGGLE_MASKS	2
	#define LED_TOGGLE_TIMEOUTS	200

    //Keypad Layout
	#define KBD_COLUMNS 8
	#define KBD_ROWS 7
	#define ETH_RST_BLOFF_COL (0x4000)

	//Extra keys
	#define EXTRA_KEYS			1 //the hook switch
	#define HOOK_KEY			0
	#define HOOK_FLASH_TMOUTS	0
	#define KBD_TEST_BIT 12
#elif defined (CONFIG_VT_V1_BOARD)
  //LED Definitions
	#define LED_D(X)			(unsigned short)(0x0100<<(X-1))
 	#define LED_D1(X)			(unsigned char)(X)
 	#define LED_HOLD  		LED_D1(1)
	#define LED_CONF			LED_D1(2)
	#define LED_CHARGE		LED_D1(3)
	#define LED_LINE1			LED_D1(4)
	#define LED_LINE2			LED_D1(5)
	#define LED_MUTE			LED_D1(6)
	#define LED_MWI				LED_D1(7)
	#define LED_SPKR			LED_D1(8)
	#define LED_INTERCOM	LED_D1(9)

	#define TOTAL_LEDS		8//the total number of LEDS

	#define LED_TOGGLE_MASKS		16
	#define LED_TOGGLE_TIMEOUTS	25

    //Keypad Layout
	#define KBD_COLUMNS 6
	#define KBD_ROWS 6

	//Extra keys
	#define EXTRA_KEYS			2 //the hook switch and the page in
	#define HOOK_KEY			0
	#define PAGE_IN_KEY			1
	#define HOOK_FLASH_TMOUTS	20

#elif defined( CONFIG_SMG_BOARDS )
	#ifdef CONFIG_BOARD_SMG_I
		//LED Definitions
		#define TOTAL_LEDS			3//the total number of LEDS
		#define LED_D(X)  			(1<<(X-1))
		#define LED_LINE1			LED_D(1)
		#define LED_LINE2			LED_D(2)
		#define LED_SPEAKER			LED_D(3)

		#define LED_TOGGLE_MASKS	2
		#define LED_TOGGLE_TIMEOUTS	200

		//Keypad Layout
		#define KBD_COLUMNS 6
		#define KBD_ROWS 5

		//Extra keys
		#define EXTRA_KEYS			1//2 //the hook switch and the headset_mic
		#define HOOK_KEY			0
		#define HS_MIC_KEY			1
		#define HOOK_FLASH_TMOUTS	0
	#endif
#elif defined(CONFIG_JW_BOARDS)
	#ifdef CONFIG_JW_V1_BOARD
	    //LED Definitions
		#define TOTAL_LEDS			0//TODO fix it

		#define LED_TOGGLE_MASKS	2
		#define LED_TOGGLE_TIMEOUTS	200

		//Keypad Layout
		#define KBD_COLUMNS 10
		#define KBD_ROWS 6

		//Extra keys
		#define EXTRA_KEYS			2 //the hook switch and the headset_mic
		#define HOOK_KEY			0
		#define HS_MIC_KEY			1
		#define HOOK_FLASH_TMOUTS	0
	#endif

#elif defined (CONFIG_IN_V1_BOARD)
  //LED Definitions
	#define TOTAL_LEDS			1//the total number of LEDS
	#define LED_D(X)  			(1 << (X-1))
	#define LED_LINE1  			LED_D(1)

	#define LED_TOGGLE_MASKS		2
	#define LED_TOGGLE_TIMEOUTS		200

  //Keypad Layout
	#define KBD_COLUMNS			6
	#define KBD_ROWS			6

	//Extra keys
	#define EXTRA_KEYS			1 //the hook switch
	#define HOOK_KEY			0
	#define HOOK_FLASH_TMOUTS		40

#else
	#define LED_D(X)  (1<<(X-1))
	#ifdef CONFIG_HI_END_KBD
		#define LED_LINE1  			LED_D(4)
		#define LED_LINE2				LED_D(3)
		#define LED_LINE3				LED_D(1)
		#define LED_LINE4				LED_D(2)
		#define LED_MUTE				LED_D(5)
		#define LED_SPKR				LED_D(13)
		#define LED_HNDSET			LED_D(12)
		#define LED_DECT				LED_D(14)
		#define LED_MWI					LED_D(11)
	#else //LO/MID END
	 	#define LED_LINE1  	LED_D(1)
		#define LED_LINE2		LED_D(4)
 		#define LED_SPKR		LED_D(2)
 		#define LED_HDSET		LED_D(3)
 		#define LED_MUTE		LED_D(5)
 		#define LED_AEC			LED_D(6)
 		#define LED_TRNSFR	LED_D(7)//OPTIONAL
		#define LED_CONFR		LED_D(8)//OPTIONAL
		#define LED_REDIAL	LED_D(9)//OPTIONAL
		#define LED_MWI			LED_D(10)//OPTIONAL
		#define LED_F1			LED_D(11)//OPTIONAL
	#endif

#ifdef CONFIG_VT_BOARDS_TEST
	#define LED_TOGGLE_MASKS	16
	#define LED_TOGGLE_TIMEOUTS	25
	#define TOTAL_LEDS			8

#else
	#define LED_TOGGLE_MASKS	2
	#define LED_TOGGLE_TIMEOUTS	200
#endif

	//Keypad size
	#ifdef CONFIG_HI_END_KBD
		#define KBD_COLUMNS 11
		#define KBD_ROWS 5
	#else //LO/MID END
		#define KBD_COLUMNS 6
		#define KBD_ROWS 5
	#endif

	//Extra keys
	#define EXTRA_KEYS			1 //the hook switch
	#define HOOK_KEY			0
#ifdef CONFIG_VT_BOARDS_TEST
	#define HOOK_FLASH_TMOUTS	20
#else
	#define HOOK_FLASH_TMOUTS	0
#endif
#endif// CONFIG_F_G2_BOARD
/*
 * Macros to help debugging
 */

#undef PDEBUG             /* undef it, just in case */
#ifdef KBD_SC1445x_DEBUG
#  ifdef __KERNEL__
     /* This one if debugging is on, and kernel space */
#    define PDEBUG(fmt, args...) printk( KERN_DEBUG "kbd_sc1445x: " fmt, ## args)
#  else
     /* This one for user space */
#    define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif

#undef PDEBUGG
#define PDEBUGG(fmt, args...) printk( KERN_DEBUG "kbd_sc1445x: " fmt, ## args)/* nothing: it's a placeholder */

#define KBD_SC1445x_MAJOR 253
#ifndef KBD_SC1445x_MAJOR
#define KBD_SC1445x_MAJOR 0   /* dynamic major by default */
#endif

#ifndef KBD_SC1445x_NR_DEVS
#define KBD_SC1445x_NR_DEVS 1
#endif

#define KBD_SC1445x_MAJ_VER 0
#define KBD_SC1445x_MIN_VER 11


/***      Data types definitions       ***/
struct keypad_sc1445x_conf_struct{
	/* Common in INT and POLLING modes*/
	unsigned char flags; //POLLING , INT, etc
	unsigned char rows;//the number of keypad rows
	unsigned char cols;//the number of keypad columns
	unsigned char repeat_tmouts_long; //tmouts before generating the first KBD_SC1445x_REPEAT_MSG (in polling timer units)
	unsigned char repeat_tmouts_short;//tmouts before generating the subsequent KBD_SC1445x_REPEAT_MSG (in polling timer units)
	unsigned char mult_keys; //the number of simultaneous keypad  events that should be scanned
	unsigned char extra_key_debounce_tmouts; //debounce tmouts for extra_key switch (in polling timer units)
	unsigned char hook_flash_tmouts; //tmouts for detecting FLASH event on HOOK rapid press (in polling timer units)
#if defined (CONFIG_L_V1_BOARD)
	unsigned char led_toggle_tmouts[2]; //led toggle tmouts (in polling timer units)
#else
	unsigned char led_toggle_tmouts; //led toggle tmouts (in polling timer units)
#endif
	/* Only for polling mode */
	unsigned char debounce_tmouts; //debounce tmouts for keypad buttons(in polling timer units)
	unsigned char polling_timer; // the multiples of the linux system timer (e.g 10ms) that will be used for polling
	/* Only for INT mode*/
	unsigned char int_debounce_timer; //debounce timer in ms, range: 0-63. This is the generation period of debounce interrupts
	unsigned char int_repeat_timer; //repeat timer in ms, range: 0-63. This is the generation period of repeat.release interrupts
	unsigned char irq; //the irq used for keyboard int
	unsigned short buffersize;	//the size of the circular buffer used for storing msgs
#ifdef CONFIG_F_G2_P1_BOARD
	unsigned short scanDelay;  //delay between column scan
#endif //CONFIG_F_G2_BOARD
	unsigned short led_toggle_masks[LED_TOGGLE_MASKS]; //led toggle masks
	unsigned short key_locks[KBD_ROWS]; //key locking masks
} ;
typedef struct  {
	unsigned char msg_code;
	unsigned short key_id;
}kbd_msg;
typedef struct  {
	char buf_len;
	char *buf;
}kbd_msg_buf;
//Used for debugging
typedef struct kbd_debug_drv {
	kbd_msg *buffer;       /* begin of buf, end of buf */
	unsigned char free_space;
	unsigned short buffersize;
    char rp, wp;
	unsigned char ver_major;
	unsigned char ver_minor;
}kbd_debug_drv_struct;

#if (defined (CONFIG_VT_BOARDS) || defined (CONFIG_VT_BOARDS_TEST))
typedef struct kbd_led_ctrl {
	unsigned char led_id;
	unsigned char timer;
}kbd_led_ctrl_struct;

enum KBD_LED_TIMERS {
	LED_TMOUT_OFF,//led off
	LED_TMOUT_ON,//led constantly on
	LED_TMOUT_O5,//led blinking at 0,5 Hz
	LED_TMOUT_1,//led blinking at 1 Hz
	LED_TMOUT_2,//led blinking at 2 Hz
	LED_TMOUT_4, //led blinking at 4 Hz
	LED_TMOUT_DB, //led double blinking
	LED_TMOUT_TOTAL_PATTERN
};
#endif
/***      Flags definitions     ***/
//keypad_conf flags
//#define KBD_SC1445x_POLLING 	(0x01)  //check keyboard activity using polling
#define KBD_SC1445x_INT     	(0x02)  //check keyboard using int
//#define KBD_SC1445x_LEVEL_HIGH	(0x04)  //key press logic is level high

#define KBD_SC1445x_FLAGS_DEFAULT 0//KBD_SC1445x_LEVEL_HIGH

/***      Msgs definitions       ***/
//key_scan msgs
#define KBD_SC1445x_RELEASE_MSG  (0x01)
#define KBD_SC1445x_PRESS_MSG    (0x02)
#define KBD_SC1445x_REPEAT_MSG	 (0x03)

//extra_key msg definitions
#define KBD_SC1445x_HOOK_ON_MSG				(0x04)
#define KBD_SC1445x_HOOK_OFF_MSG			(0x05)
//#ifdef CONFIG_RPS_BOARD
#define KBD_SC1445x_CALL_END_PRESSED_MSG	(0x06)
#define KBD_SC1445x_CALL_END_RELEASED_MSG	(0x07)
//#elif defined CONFIG_F_G2_P2_2PORT_BOARD
#define KBD_SC1445x_HS_MIC_PRESSED_MSG		(0x08)
#define KBD_SC1445x_HS_MIC_RELEASED_MSG		(0x09)
//#endif
#define KBD_SC1445x_PAGE_PRESSED_MSG		(0x0A)
#define KBD_SC1445x_PAGE_RELEASED_MSG		(0x0B)
#define KBD_SC1445x_HOOK_FLASH_MSG			(0x0C)
#define KBD_SC1445x_PRE_HOOK_FLASH_MSG		(0x0D)

#define HOOK_DUMMY_KEY_ID					(0xff)
#define CALL_END_DUMMY_KEY_ID				(0xff)

#ifdef CONFIG_RPS_BOARD
	//extra_key msgs
	#define KBD_SC1445x_EXTRA_KEY_RELEASE_MSG(X)	KBD_SC1445x_CALL_END_RELEASED_MSG
	#define KBD_SC1445x_EXTRA_KEY_PRESS_MSG(X)		KBD_SC1445x_CALL_END_PRESSED_MSG
	//extra_key dummy key_id
	#define EXTRA_KEY_DUMMY_KEY_ID(X)				CALL_END_DUMMY_KEY_ID
#elif defined CONFIG_F_G2_P2_2PORT_BOARD
	//extra_key msgs
	#define KBD_SC1445x_EXTRA_KEY_RELEASE_MSG(X)	(X==0 ? KBD_SC1445x_HOOK_ON_MSG : KBD_SC1445x_HS_MIC_RELEASED_MSG)
	#define KBD_SC1445x_EXTRA_KEY_PRESS_MSG(X)		(X==0 ? KBD_SC1445x_HOOK_OFF_MSG : KBD_SC1445x_HS_MIC_PRESSED_MSG)
	//extra_key dummy key_id
	#define EXTRA_KEY_DUMMY_KEY_ID(X)				HOOK_DUMMY_KEY_ID
#elif defined CONFIG_VT_V1_BOARD
	//extra_key msgs
	#define KBD_SC1445x_EXTRA_KEY_RELEASE_MSG(X)	(X==0 ? KBD_SC1445x_HOOK_OFF_MSG : KBD_SC1445x_PAGE_RELEASED_MSG)
	#define KBD_SC1445x_EXTRA_KEY_PRESS_MSG(X)		(X==0 ? KBD_SC1445x_HOOK_ON_MSG : KBD_SC1445x_PAGE_PRESSED_MSG)
	//extra_key dummy key_id
	#define EXTRA_KEY_DUMMY_KEY_ID(X)				HOOK_DUMMY_KEY_ID
#else
	//extra_key msgs
	#define KBD_SC1445x_EXTRA_KEY_RELEASE_MSG(X)	KBD_SC1445x_HOOK_ON_MSG
	#define KBD_SC1445x_EXTRA_KEY_PRESS_MSG(X)		KBD_SC1445x_HOOK_OFF_MSG
	//extra_key dummy key_id
	#define EXTRA_KEY_DUMMY_KEY_ID(X)				HOOK_DUMMY_KEY_ID
#endif

/***      Buffers definitions       ***/
#define KBD_SC1445x_BUFFER_SIZE 64
/***      Ioctl cmds      ***/
enum KBD_SC1445x_IOCTL {
	KBD_SC1445x_IOCTL_CONF_SET=5,//set driver configuration
	KBD_SC1445x_IOCTL_CONF_GET,//get current driver configuration
	KBD_SC1445x_IOCTL_INITCONF_GET,//get initial driver configuration
	KBD_SC1445x_IOCTL_DRV_INFO_GET,//get driver information
	KBD_SC1445x_IOCTL_CONF_WRITE,//write a msg to the circular buffer (for debugging)
	KBD_SC1445x_IOCTL_CONF_READ,//read a msg from the circular buffer
	KBD_SC1445x_IOCTL_LEDS_GET,//get led toggle mask
	KBD_SC1445x_IOCTL_LEDS_SET,//set led toggle mask
#if defined(CONFIG_L_V2_BOARD)||defined(CONFIG_L_V4_BOARD) ||defined(CONFIG_L_V5_BOARD)||defined(CONFIG_L_V3_BOARD)\
||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	KBD_SC1445x_IOCTL_BOARD_ID, //Get IP8800 variant
#endif
} ;


#ifdef SC1445x_KBD_SYS_POLLING
void kbd_timer_poll_fn(void);
void kbd_timer_int_fn(void);
#endif


#endif /* _KBD_SC1445x_H_ */
