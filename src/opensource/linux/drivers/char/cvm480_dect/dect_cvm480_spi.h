/*
 * dect_cvm480_spi.h -- definitions for the spi of the CVM480 spi driver
  */

#ifndef _DECT_CVM_480_SPI_H_
#define _DECT_CVM_480_SPI_H_


/*Hw configuration definitions*/
#define KBD_DEB_TIMER 0 //debounce timer used for issuing interrupt after specific time in ms

/*
 * Macros to help debugging
 */
//#define DECT_CVM480_SPI_DEBUG
#undef DECT_DEBUG             /* undef it, just in case */
#ifdef DECT_CVM480_SPI_DEBUG
#  ifdef __KERNEL__
     /* This one if debugging is on, and kernel space */
#    define DECT_DEBUG(fmt, args...) printk( KERN_DEBUG "dect_cvm480_spi: " fmt, ## args)
#  else
     /* This one for user space */
#    define DECT_DEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define DECT_DEBUG(fmt, args...) /* not debugging: nothing */
#endif

#undef DECT_DEBUGG
#define DECT_DEBUGG(fmt, args...) printk( KERN_DEBUG "dect_cvm480_spi: " fmt, ## args)/* nothing: it's a placeholder */

#define DECT_CVM480_SPI_MAJOR 244
#ifndef DECT_CVM480_SPI_MAJOR
#define DECT_CVM480_SPI_MAJOR 0   /* dynamic major by default */
#endif

#ifndef DECT_CVM480_SPI_NR_DEVS
#define DECT_CVM480_SPI_NR_DEVS 1    
#endif

#define DECT_CVM480_SPI_MAJ_VER 0
#define DECT_CVM480_SPI_MIN_VER 1

//#define DECT_CVM480_SPI_DEBUG_GPIO
#ifdef DECT_CVM480_SPI_DEBUG_GPIO
 /*Debugging*/
#define SET_P210_HI 		{P2_SET_DATA_REG = (1<<10);}
#define SET_P210_LO 		{P2_RESET_DATA_REG = (1<<10);}
#define SET_P007_HI 		{P0_SET_DATA_REG = 0x80;}
#define SET_P007_LO 		{P0_RESET_DATA_REG = 0x80;}
#endif //DECT_CVM480_SPI_DEBUG_GPIO

enum DECT_CVM480_SPI_IOCTL {
	DECT_CVM480_SPI_IOCTL_RST = 1,//reset cvm module
} ;

#endif /* _DECT_CVM_480_SPI_H_ */
