/*
 * This file is the Linux Spi Driver for cvm480 dect modules. 
 */

 /*========================== NOTES =====================================*/
/* This driver provides the spi interface with the cvm480 dect modules. The hdlc protocol is not integrated in this driver (it should be implemented in the user space)
* 
*/
 /*========================= Include files ==================================*/ 
/* linux/module specific headers */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>           /* loacation of file_operations structure*/
#include <asm/uaccess.h>        /* loacation copy_xyz_user*/
#include <linux/slab.h>         /* kmalloc and kfree */
#include <linux/delay.h>

#include <linux/proc_fs.h> 
#include <linux/cdev.h> /*cdev*/
#include <linux/types.h>  
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/workqueue.h>
#include <linux/wait.h> /*kbd_sc14450_dev_struct*/
#include <linux/interrupt.h> /* request_irq */	
#include <asm/irq.h>         /* request_irq */	
#include <asm/regs.h>
#include "dect_cvm480_spi.h"

/*========================== Definitions ==================================*/
/***      Data types definitions       ***/
//#define u16 short
//Driver structs
typedef struct {	
	struct semaphore sem;     /* mutual exclusion semaphore     */
	struct spi_device	*spi;
    wait_queue_head_t inq, outq;       /* read and write queues */
	struct cdev cdev;	  	  /* Char device structure		*/	
	unsigned char flags;
	unsigned char ver_minor;/*stores driver's minor version number*/
	unsigned char ver_major;/*stores driver's majorversion number*/
}dect_cvm480_spi_dev_struct;

/***      Flags definitions     ***/
#define CVM_READ_REQ 0x0001 // INT_1 of CVM module was detected high. CVM has data to read
/***   Macros   ***/

 /*========================== Function Prototypes ===========================*/		
/** High level **/
//Interrupt
//static irqreturn_t kbd_int_handler(int irq, void *dev_id, struct pt_regs *regs);
//Tasklet

//Timers
//Module specific
static int __init dect_cvm480_spi_init(void);
static void dect_cvm480_spi_exit(void);
void dect_cvm_480_spi_setup_cdev(dect_cvm480_spi_dev_struct *dev);
// Fops prototypes 
int dect_cvm480_spi_open(struct inode *inode, struct file *filp);
int dect_cvm480_spi__release(struct inode *inode, struct file *filp);
static ssize_t dect_cvm480_spi_read (struct file *filp, char __user *buf, size_t count, loff_t *pos);
static ssize_t dect_cvm480_spi_write(struct file *filp, const char __user *buf, size_t count,
                loff_t *f_pos);
static int __devinit dect_cvm480_spi_probe(struct spi_device *spi);
static int __devexit dect_cvm480_spi_remove(struct spi_device *spi);
static irqreturn_t dect_cvm480_spi_interrupt(int irq, void *dev_id);

/*** Parameters which can be set at load time  ***/
int dect_cvm480_spi_major =  DECT_CVM480_SPI_MAJOR;
int dect_cvm480_spi_minor =   0;


module_param(dect_cvm480_spi_major, int, S_IRUGO);
module_param(dect_cvm480_spi_minor, int, S_IRUGO);

MODULE_AUTHOR("George Giannaras");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("CVM480 Dect Spi Driver");

/***      Var definitions  and initializations     ***/
/** Low level **/
dect_cvm480_spi_dev_struct dect_cvm480_spi_dev;
// unsigned char tx_byte, rx_byte;


/** High level **/


/***      Timer initializations     ***/

/***      Tasklet     ***/


/***************          Main body             ***************/


/* ****************************************************************************
 	Description: 	Interrupt handler for DSP1 (dtmf detection).
 * ****************************************************************************/
static irqreturn_t dect_cvm480_spi_interrupt( int irq, void* dev_id )
{
	dect_cvm480_spi_dev_struct *dev = dev_id; /* device information */
	/* check if this interrupt is for INT0 - INT5  (irq line is being shared) */

#ifdef CONFIG_F_G2_BOARDS
	if( !( KEY_STATUS_REG & 0x02 ) )//if it is not a debounce or key repeat timer, exit
		return IRQ_NONE ;

	if (P1_DATA_REG & (1<<14))
	{
		KEY_GP_INT_REG |= 0x01;
// #ifdef DECT_CVM480_SPI_DEBUG_GPIO		
		 // SET_P210_HI	;
// #endif DECT_CVM480_SPI_DEBUG_GPIO		
		//printk(KERN_WARNING "dect_cvm480_spi: INT HIGH \n");	
		dect_cvm480_spi_dev.flags |= CVM_READ_REQ;
		/* awake any readers */
		wake_up_interruptible(&dev->inq);
	}
	else{
		KEY_GP_INT_REG &= ~0x01;
// #ifdef DECT_CVM480_SPI_DEBUG_GPIO		
		 // SET_P210_LO	;
// #endif DECT_CVM480_SPI_DEBUG_GPIO	
		//printk(KERN_WARNING "dect_cvm480_spi: INT LOW \n");		
		dect_cvm480_spi_dev.flags &= ~CVM_READ_REQ;
		/* awake any writers */
		wake_up_interruptible(&dev->outq);		
	}

	/* clear kbd int status bit */	
	KEY_STATUS_REG = 0x02 ;


#else
	if( !( KEY_STATUS_REG & 0x01 ) )//if it is not a debounce or key repeat timer, exit
		return IRQ_NONE ;
#ifdef	CONFIG_SC14452_ULE_DK_BOARDS
	if (P1_DATA_REG & (1<<1))
#else	
	if (P1_DATA_REG & (1<<13))
#endif		
	{
// #ifdef DECT_CVM480_SPI_DEBUG_GPIO		
		 // SET_P210_HI	;
// #endif DECT_CVM480_SPI_DEBUG_GPIO		
		//printk(KERN_WARNING "dect_cvm480_spi: INT HIGH \n");	
		dect_cvm480_spi_dev.flags |= CVM_READ_REQ;
		/* awake any readers */
		wake_up_interruptible(&dev->inq);
	}
	else{
// #ifdef DECT_CVM480_SPI_DEBUG_GPIO		
		 // SET_P210_LO	;
// #endif DECT_CVM480_SPI_DEBUG_GPIO	
		//printk(KERN_WARNING "dect_cvm480_spi: INT LOW \n");		
		
		dect_cvm480_spi_dev.flags &= ~CVM_READ_REQ;
		/* awake any writers */
		wake_up_interruptible(&dev->outq);		
	}
	/* clear kbd int status bit */	
	KEY_STATUS_REG = 0x01 ;
#endif
	/* clear pending  interrupt bit */
	RESET_INT_PENDING_REG = KEYB_INT_PEND;
	
	return IRQ_HANDLED ;		
}	
/******************************************************* 
	Description: 	Open dect_cvm480_spi_dev dev file.

 ********************************************************/
int dect_cvm480_spi_open(struct inode *inode, struct file *filp)
{
	dect_cvm480_spi_dev_struct *dev; /* device information */
	
	dev = container_of(inode->i_cdev, dect_cvm480_spi_dev_struct, cdev);
	filp->private_data = dev; /* for other methods */
	DECT_DEBUG("DECT_CVM480_SPI OPENS WITH FLAGS %d   \n", (filp->f_flags ));
	if (filp->f_flags & O_NONBLOCK){
		DECT_DEBUG("DECT_CVM480_SPI OPENED AS NONBLOCK \n");
	}

#ifdef CONFIG_F_G2_BOARDS
	KEY_GP_INT_REG |= 0x04;
#endif

	return 0;
}

/******************************************************* 
	Description: 	Close device file.

 ********************************************************/
int dect_cvm480_spi_release(struct inode *inode, struct file *filp)
{
	return 0;
}
/******************************************************* 
	Description: 	Read cvm480 module.

 ********************************************************/
static ssize_t dect_cvm480_spi_read (struct file *filp, char __user *buf, size_t count, loff_t *pos)
{
	dect_cvm480_spi_dev_struct *dev = filp->private_data;
	struct spi_message	*m;
	struct spi_transfer	*t;	
	char *tmp_buf;
	unsigned char i;

//#ifdef DECT_CVM480_SPI_DEBUG_GPIO		
		// SET_P007_HI	;
//#endif DECT_CVM480_SPI_DEBUG_GPIO		
	/*Block until CVM raises signal */
	while (!(dev->flags & CVM_READ_REQ)) { /* cvm module has nothing to read */
		if (filp->f_flags & O_NONBLOCK)
			return -EAGAIN;
		DECT_DEBUG("\"%s\" reading: going to sleep\n", current->comm);
		if (wait_event_interruptible(dev->inq, (dev->flags & CVM_READ_REQ)))
			return -ERESTARTSYS; /* signal: tell the fs layer to handle it */
	}
	
	/*Allocate memory for read*/
	tmp_buf = kzalloc(sizeof(char) * count,	GFP_KERNEL);
	if (!tmp_buf){
		return -EFAULT;	
	}
	/*Initialize spi msg structs*/
#ifndef CONFIG_CATIQ_V2_SUPPORT		
	m = kzalloc(sizeof(struct spi_message)
			+ count * sizeof(struct spi_transfer),
			GFP_KERNEL);
	if (m) {
		t = (struct spi_transfer *)(m + 1);
		INIT_LIST_HEAD(&m->transfers);
		for (i = 0; i < count; i++, t++){	
			t->delay_usecs = 50;//delay after each tx/rx . Not needed if reading only 1 byte
			t->len = 1;
			t->rx_buf = &tmp_buf[i];	
			spi_message_add_tail(t, m);
		}
	}
#else
	m = kzalloc(sizeof(struct spi_message)
			+ sizeof(struct spi_transfer),
			GFP_KERNEL);
	if (m) {
		t = (struct spi_transfer *)(m + 1);
		INIT_LIST_HEAD(&m->transfers);
		//for (i = 0; i < count; i++, t++){	
			//t->delay_usecs = 50;//delay after each tx/rx . Not needed if reading only 1 byte
			t->len = count;
			t->rx_buf = tmp_buf;//[i];	
			spi_message_add_tail(t, m);
		//}
	}
#endif	
	else{
		kfree(tmp_buf);
		return -EFAULT;
	}	

	/* cvm has data to read */	
	spi_sync(dev->spi, m);	
	if (copy_to_user(buf, tmp_buf, count)) {
		kfree(tmp_buf);
		spi_message_free(m);		
		return -EFAULT;
	}
	kfree(tmp_buf);
	spi_message_free(m);	
	/* finally, awake any writers and return */
	wake_up_interruptible(&dev->outq);
	DECT_DEBUG("\"%s\" did read %li bytes\n",current->comm, (long)count);
// #ifdef DECT_CVM480_SPI_DEBUG_GPIO		
		// SET_P007_LO	;
// #endif DECT_CVM480_SPI_DEBUG_GPIO	
	return count;  
	
}

/******************************************************* 
	Description: 	Write cvm480 module.

 ********************************************************/
static ssize_t dect_cvm480_spi_write(struct file *filp, const char __user *buf, size_t count,
                loff_t *f_pos)
{
	dect_cvm480_spi_dev_struct *dev = filp->private_data;
	struct spi_message	*m;
	struct spi_transfer	*t;	
	char *tmp_buf;
	unsigned char i;
//#ifdef DECT_CVM480_SPI_DEBUG_GPIO		
	//	SET_P210_HI	;
//#endif DECT_CVM480_SPI_DEBUG_GPIO	
	/*Block until CVM raises signal */	
	while (dev->flags & CVM_READ_REQ) { /* cvm module has requested to be read */
		if (filp->f_flags & O_NONBLOCK)
			return -EAGAIN;
		DECT_DEBUG("\"%s\" writing: going to sleep\n", current->comm);
		if (wait_event_interruptible(dev->outq, !(dev->flags & CVM_READ_REQ)))
			return -ERESTARTSYS; /* signal: tell the fs layer to handle it */
		/* otherwise loop*/
	}

	/* cvm can now be written */	
#ifndef CONFIG_CATIQ_V2_SUPPORT	
	/*Copy user buf*/
	tmp_buf = kzalloc(sizeof(char) * count,	GFP_KERNEL);
	if (tmp_buf){
		if (copy_from_user(tmp_buf, buf, count)) {
			kfree(tmp_buf);
			return -EFAULT;
		}	
	}
	else{
		return -EFAULT;	
	}
	
	/*Initialize spi msg structs*/
	m = kzalloc(sizeof(struct spi_message)
			+ count * sizeof(struct spi_transfer),
			GFP_KERNEL);
	if (m) {
		t = (struct spi_transfer *)(m + 1);
		INIT_LIST_HEAD(&m->transfers);
		for (i = 0; i < count; i++, t++){
			t->delay_usecs = 50;//delay after each tx/rx . Value by experimenting with CVM480		
			t->len = 1;
			t->tx_buf = &tmp_buf[i];	
			spi_message_add_tail(t, m);
		}
	}
#else
	size_t count_even;
	/*Make count always even*/
	if(count % 2)
		count_even=count+1;
	else
		count_even=count;
//	printk("\n\t count = 0x%x, count_even = 0x%x \n", count, count_even);
	/*Copy user buf*/
	tmp_buf = kzalloc(sizeof(char) * count_even,	GFP_KERNEL);
	if (tmp_buf){
		if (copy_from_user(tmp_buf, buf, count)) {
		kfree(tmp_buf);
		return -EFAULT;
	}	
	}
	else{
		return -EFAULT;	
	}
	
	/*Initialize spi msg structs*/
	m = kzalloc(sizeof(struct spi_message)
			+ count_even * sizeof(struct spi_transfer),
			GFP_KERNEL);
	if (m) {
		t = (struct spi_transfer *)(m + 1);
		INIT_LIST_HEAD(&m->transfers);
	
		//for (i = 0; i < count; i++, t++){
		//	t->delay_usecs = 50;//delay after each tx/rx . Value by experimenting with CVM480		
			t->len = count_even;
			t->tx_buf = tmp_buf;//[i];	
			spi_message_add_tail(t, m);
		//}
	}		
#endif		

	else{
		kfree(tmp_buf);
		return -EFAULT;
	}	
#ifdef CONFIG_SC14452_ULE_DK_BOARDS	
	if (P1_DATA_REG & (1<<1)){
		printk("INT RAISED WHILE IN TX !!!!\n");
	}
#endif	
	spi_sync(dev->spi, m);		
	spi_message_free(m);
	kfree(tmp_buf);

	/* finally, awake any readers and return */
	wake_up_interruptible(&dev->inq);//this is not needed ?
	DECT_DEBUG("\"%s\" did wrote %li bytes\n",current->comm, (long)count);

//#ifdef DECT_CVM480_SPI_DEBUG_GPIO		
	//	SET_P210_LO	;
//#endif DECT_CVM480_SPI_DEBUG_GPIO
	return count; 
}



static int dect_cvm480_spi_ioctl(struct inode *inode, struct file *filp,
                 unsigned int cmd, unsigned long arg)
{
	int retval=0;
	dect_cvm480_spi_dev_struct *dev = filp->private_data;
		
	DECT_DEBUG("CVM480 driver IOCTL  \n");

	switch(cmd) {
/**** SET IOCTLS ****/		
	
		case DECT_CVM480_SPI_IOCTL_RST:
			if (down_interruptible(&dev->sem))
				return -ERESTARTSYS;  
			#ifdef CONFIG_SC14452_ULE_DK_BOARDS	
				P2_RESET_DATA_REG |= 1<<5;
				msleep(5);
				P2_SET_DATA_REG |= 1<<5;
			#endif
			up (&dev->sem);
			break;				


		default:
			printk(KERN_WARNING "Invalid cvm480 driver ioctl \n");
			return -ENOTTY;
	}
	return retval;
}


/* ****************************************************************************
 * This function is called by the OS and when a spi device that uses this driver is probed during 
 * the system start-up. 
 * ****************************************************************************/
static int __devinit dect_cvm480_spi_probe(struct spi_device *spi)
{
	int result = 0;
	dev_t dev = 0;
	unsigned short key_board_int_reg=0;	
	//unsigned char *platform_data;
	
	//platform_data = spi->dev.platform_data;
/*
 * Get a range of minor numbers to work with, asking for a dynamic
 * major unless directed otherwise at load time.
 */
	if (dect_cvm480_spi_major) {
		dev = MKDEV(dect_cvm480_spi_major, dect_cvm480_spi_minor);
		result = register_chrdev_region(dev, 1, "dect_cvm480_spi");
	} 
	else {
		result = alloc_chrdev_region(&dev, dect_cvm480_spi_minor, 1, "dect_cvm480_spi");
		dect_cvm480_spi_major = MAJOR(dev);
	}
	if (result < 0) {
		printk(KERN_WARNING "dect_cvm480_spi: can't get major %d\n", dect_cvm480_spi_major);
		return result;
	}
	
	/*Initialize irq*/
	while( RESET_INT_PENDING_REG & 0x2 ) {
#ifdef CONFIG_F_G2_BOARDS
		KEY_STATUS_REG = 2 ;
#else
		KEY_STATUS_REG = 1 ;
#endif
		RESET_INT_PENDING_REG = 0x2;
	}
	result = request_irq( KEYB_INT, dect_cvm480_spi_interrupt, SA_SHIRQ, "dect_cvm480_spi", (void*)&dect_cvm480_spi_dev ) ;
	if( result < 0 ) {
		printk( KERN_ERR"dect_cvm480_spi: unable to register IRQ #%d\n",
								KEYB_INT ) ;
		return result;
	}	
	
	/*Initialize driver internal vars*/
	memset((char*)&dect_cvm480_spi_dev, 0, sizeof(dect_cvm480_spi_dev_struct));	
	dect_cvm480_spi_dev.ver_major=DECT_CVM480_SPI_MAJ_VER;
	dect_cvm480_spi_dev.ver_minor=DECT_CVM480_SPI_MIN_VER;
	dect_cvm480_spi_dev.spi = spi;	//link char device to spi device
	//dev_set_drvdata(&spi->dev, gLegVpDvrInfo[Dev_Id].pDevObj);//link spi device to char device ??
	
	/*Initialize mutexes, waitqueues, timers*/
	init_MUTEX(&dect_cvm480_spi_dev.sem);
	init_waitqueue_head(&(dect_cvm480_spi_dev.inq));
	init_waitqueue_head(&(dect_cvm480_spi_dev.outq));	
	
	/*Setup device*/
	dect_cvm_480_spi_setup_cdev(&dect_cvm480_spi_dev);
    printk(KERN_ALERT "CVM480 Dect module initialized, spi addr : %x\n", (unsigned int)spi);
	
	/*Initialize hw*/
#ifdef DECT_CVM480_SPI_DEBUG_GPIO		
	/*Debugging*/
	SetPort(P2_10_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_NONE);      
	SetPort(P0_07_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_NONE);    	
#endif //DECT_CVM480_SPI_DEBUG_GPIO	

#ifdef CONFIG_F_G2_BOARDS
	/*Configure pio for spi int */
	SetPort(P1_14_MODE_REG, GPIO_PUPD_IN_PULLUP/*GPIO_PUPD_IN_NONE*/,  GPIO_PID_INT6n);//spi int as gpio	
	/* Configure debounce for key press and repeat timers */	
	KEY_DEBOUNCE_REG=(KBD_DEB_TIMER & 0xFFFFFF);//set debounce timer 
	
	/* Enable release int */
//	key_board_int_reg = KEY_REL|KEY_LEVEL|INT5_EN;//enable int on release, level high, int5

	/* Configure KBD int  priority */
	//INT3_PRIORITY_REG=0x0070; //Set PR_LEVEL[0] (lowest priority)		these bits are set in ethernet driver

	/* Configure KEY_BOARD_INT_REG */
//	KEY_BOARD_INT_REG|=key_board_int_reg;
//	KEY_GP_INT_REG |= 0x04;

#else

	/*Configure pio for spi int */
#ifdef CONFIG_SC14452_ULE_DK_BOARDS
	printk("Set P1_01_MODE_REG port to 0x%x \n", GPIO_PID_INT5n);
	SetPort(P1_01_MODE_REG, GPIO_PUPD_IN_PULLUP/*GPIO_PUPD_IN_NONE*/,  GPIO_PID_INT5n);//spi int as gpio
	SetPort(P2_05_MODE_REG, GPIO_PUPD_IN_NONE,  0);//P205 as RESET pin
//	SetPort(P1_14_MODE_REG, GPIO_PUPD_OUT_NONE,  0);//debug pin  
#else	
	SetPort(P1_13_MODE_REG, GPIO_PUPD_IN_PULLUP/*GPIO_PUPD_IN_NONE*/,  GPIO_PID_INT5n);//spi int as gpio	
#endif	
	/* Configure debounce for key press and repeat timers */	
	KEY_DEBOUNCE_REG=(KBD_DEB_TIMER & 0xFFFFFF);//set debounce timer 
	
	/* Enable release int */
	key_board_int_reg = KEY_REL|KEY_LEVEL|INT5_EN;//enable int on release, level high, int5

	/* Configure KBD int  priority */
	//INT3_PRIORITY_REG=0x0070; //Set PR_LEVEL[0] (lowest priority)		these bits are set in ethernet driver

	/* Configure KEY_BOARD_INT_REG */
	KEY_BOARD_INT_REG|=key_board_int_reg;
#endif		
    return result; /* sucess */

    // fail_arg:   return -ENODEV;	
	
    // return -ENODEV;

    // printk(KERN_ALERT "CVM480 Dect module initialization complete\n");
    // return 0;
}
static int __devexit dect_cvm480_spi_remove(struct spi_device *spi)
{
	return 0;
}
struct file_operations dect_cvm480_spi_fops = {
	.owner =    THIS_MODULE,
	.poll =   	NULL,
	.read =     dect_cvm480_spi_read,
	.write =    dect_cvm480_spi_write,
	.ioctl =    NULL,//dect_cvm480_spi_ioctl,
	.open =     dect_cvm480_spi_open,
	.release =  dect_cvm480_spi_release,
};
static struct spi_driver dect_cvm480_spi_driver = {
	.driver = {
		.name 	= "dect_cvm480_spi",
		.bus	= &spi_bus_type,//defined in spi.c
		.owner	= THIS_MODULE,
	},
	.probe 	= dect_cvm480_spi_probe,
	.remove = __devexit_p(dect_cvm480_spi_remove),
};

/* ****************************************************************************
 * This function is called by the OS and registers the  spi driver to
 * the system. 
 * ****************************************************************************/
static int __init dect_cvm480_spi_init(void)
{
    int result = spi_register_driver(&dect_cvm480_spi_driver);
//	DECT_DEBUG("spi_register_drive returned : %d \n", result);
	return result;
	//return spi_register_driver(&dect_cvm480_spi_driver);	
}

/* ****************************************************************************
 * This function is called by the OS and unregisters the spi driver from
 * the system. This function will call necessary functions to properly
 * close down the driver and free up any resources it may have be using.
 * ****************************************************************************/
static void dect_cvm480_spi_exit(void)
{
	spi_unregister_driver(&dect_cvm480_spi_driver);
	
    printk(KERN_ALERT "Exiting dect_cvm480_spi Driver\n");
    return;

}

/******************************************************* 
	Description: 	Set up the cdev structure for this device.

 ********************************************************/
void dect_cvm_480_spi_setup_cdev(dect_cvm480_spi_dev_struct *dev)
{
	int err, devno = MKDEV(dect_cvm480_spi_major, dect_cvm480_spi_minor);
    
	cdev_init(&dev->cdev, &dect_cvm480_spi_fops);
	dev->cdev.owner = THIS_MODULE;
	dev->cdev.ops = &dect_cvm480_spi_fops;
	err = cdev_add (&dev->cdev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE "Error %d adding dect_cvm480_spi", err);
}

module_init(dect_cvm480_spi_init);
module_exit(dect_cvm480_spi_exit);






