/*
 * This file is the Led Spi Driver.
 */

 /*========================== NOTES =====================================*/
/* This driver provides the spi interface for CNXT SPI control.
*
*/
 /*========================= Include files ==================================*/
/* linux/module specific headers */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>           /* location of file_operations structure*/
#include <asm/uaccess.h>        /* location copy_xyz_user*/
#include <linux/slab.h>         /* kmalloc and kfree */
#include <linux/delay.h>

#include <linux/proc_fs.h>
#include <linux/cdev.h> /*cdev*/
#include <linux/types.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/workqueue.h>
#include <linux/wait.h> /*kbd_sc14450_dev_struct*/
#include <linux/interrupt.h> /* request_irq */
#include <asm/irq.h>         /* request_irq */
#include <asm/regs.h>
#include "cnxt_spi.h"

/*========================== Definitions ==================================*/

/***      Data types definitions       ***/
//#define u16 short
//Driver structs
typedef struct {
	struct semaphore sem;     /* mutual exclusion semaphore     */
	struct spi_device	*spi;
	//spinlock_t lock;
	wait_queue_head_t inq;
	wait_queue_head_t outq;
	struct cdev cdev;	  	  /* Char device structure		*/
	unsigned char flags;
	unsigned char ver_minor;/*stores driver's minor version number*/
	unsigned char ver_major;/*stores driver's majorversion number*/
}cnxt_spi_dev_struct;

/***      Flags definitions     ***/

/***   Macros   ***/
#define	START_TIMER(TIMER, TMOUT) {TIMER.expires = jiffies + TMOUT;add_timer(&TIMER);}
#define	STOP_TIMER(TIMER) {del_timer(&TIMER);}
 /*========================== Function Prototypes ===========================*/
/** High level **/
//Interrupt
//static irqreturn_t kbd_int_handler(int irq, void *dev_id, struct pt_regs *regs);
//Tasklet

//Timers
//Module specific
static int __init cnxt_spi_init(void);
static void cnxt_spi_exit(void);
void cnxt_spi_setup_cdev(cnxt_spi_dev_struct *dev);
// Fops prototypes
int cnxt_spi_open(struct inode *inode, struct file *filp);
int cnxt_spi_release(struct inode *inode, struct file *filp);
int cnxt_spi_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg);


static int __devinit cnxt_spi_probe(struct spi_device *spi);
static int __devexit cnxt_spi_remove(struct spi_device *spi);



/*** Parameters which can be set at load time  ***/
int cnxt_spi_major =  CNXT_SPI_MAJOR;
int cnxt_spi_minor =   0;


module_param(cnxt_spi_major, int, S_IRUGO);
module_param(cnxt_spi_minor, int, S_IRUGO);

MODULE_AUTHOR("Pavlos Bougas");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("CNXT Spi Driver");


/***      Var definitions  and initializations     ***/
/** Low level **/
cnxt_spi_dev_struct cnxt_spi_dev;
cnxt_spi_dev_struct *pt_cnxt_spi_dev=&cnxt_spi_dev;

/** High level **/


/***      Timer initializations     ***/

/***      Tasklet     ***/


/***************          Main body             ***************/
/*******************************************************
	Description: 	Open cnxt_spi_dev dev file.

 ********************************************************/
int cnxt_spi_open(struct inode *inode, struct file *filp)
{
	cnxt_spi_dev_struct *dev; /* device information */

	dev = container_of(inode->i_cdev, cnxt_spi_dev_struct, cdev);
	pt_cnxt_spi_dev=dev;

	filp->private_data = dev; /* for other methods */
	PDEBUG("CNXT_SPI OPENS WITH FLAGS %d   \n", (filp->f_flags ));

	if (filp->f_flags & O_NONBLOCK){
		PDEBUG("CNXT_SPI OPENED AS NONBLOCK \n");
	}

	return 0;
}

/*******************************************************
	Description: 	Close device file.

 ********************************************************/
int cnxt_spi_release(struct inode *inode, struct file *filp)
{
	return 0;
}


int cnxt_spi_ioctl(struct inode *inode, struct file *filp,
                 unsigned int cmd, unsigned long arg)
{
	int retval=0;
	cnxt_spi_dev_struct *dev = filp->private_data;

	char *cnxtldataw,*cnxtldatar;
	unsigned int cnxtudatar,cnxtudataw, cnxtlen;

	struct spi_message	m;
	struct spi_transfer	t_main;

	PDEBUG("CNXT SPI IOCTL  \n");

	switch(cmd)
	{
/**** SET IOCTLS ****/
	case CNXT_SPI_SC1445x_IOCTL_WRITE:
		if (down_interruptible(&dev->sem))
		{
			return -ERESTARTSYS;
		}

		if ((copy_from_user(&cnxtudataw, (char*)arg, 4)))
		{
			printk(KERN_WARNING "CNXT: Copy from user failed\n");
			up (&dev->sem);
			return -EFAULT;
		}

		if (!cnxtudataw)
		{
			printk(KERN_WARNING "CNXT: User Data are NULL\n");
			up (&dev->sem);
			return -EFAULT;
		}

		if ((copy_from_user(&(cnxtlen), (char*)arg+4, 4))) {
			printk(KERN_WARNING "CNXT: Copy from user failed\n");
			up (&dev->sem);
			return -EFAULT;
		}

		if ((cnxtlen==0)&&(cnxtlen>MAX_CNXT_DATA))
		{
			printk(KERN_WARNING "CNXT: Data len too big or zero\n");
			up (&dev->sem);
			return -EFAULT;
		}

		cnxtldataw=kzalloc(sizeof(char) * cnxtlen,	GFP_KERNEL);

		if (!cnxtldataw)
		{
			printk(KERN_WARNING "CNXT: Unable to allocate memory\n");
			up (&dev->sem);
			return -EFAULT;
		}

		PDEBUG("CNXT: IOCTL WRITE: %x,%x \n", cnxtudataw,cnxtlen);

		copy_from_user (cnxtldataw,(char *)cnxtudataw,cnxtlen);

		spi_message_init(&m);
		memset((void*)&t_main, 0, sizeof(struct spi_transfer));
		t_main.delay_usecs = 0;//wait 1 usec after each tx/rx
		t_main.len = cnxtlen;
		t_main.tx_buf = (void*)(cnxtldataw);
		t_main.rx_buf = NULL;
		spi_message_add_tail(&t_main, &m);
		spi_sync(pt_cnxt_spi_dev->spi, &m);

		kfree(cnxtldataw);

		up (&dev->sem);
		break;

	case CNXT_SPI_SC1445x_IOCTL_READ:
		if (down_interruptible(&dev->sem))
		{
			return -ERESTARTSYS;
		}

		if ((copy_from_user(&cnxtudataw, (char*)arg, 4)))
		{
			printk(KERN_WARNING "CNXT: Copy from user failed\n");
			up (&dev->sem);
			return -EFAULT;
		}

		if (!cnxtudataw)
		{
			printk(KERN_WARNING "CNXT: User Data are NULL\n");
			up (&dev->sem);
			return -EFAULT;
		}

		if ((copy_from_user(&cnxtudatar, (char*)arg+4, 4)))
		{
			printk(KERN_WARNING "CNXT: Copy from user failed\n");
			up (&dev->sem);
			return -EFAULT;
		}

		if (!cnxtudatar)
		{
			printk(KERN_WARNING "CNXT: User Data are NULL\n");
			up (&dev->sem);
			return -EFAULT;
		}

		if ((copy_from_user(&(cnxtlen), (char*)arg+8, 4))) {
			printk(KERN_WARNING "CNXT: Copy from user failed\n");
			up (&dev->sem);
			return -EFAULT;
		}

		if ((cnxtlen==0)&&(cnxtlen>MAX_CNXT_DATA))
		{
			printk(KERN_WARNING "CNXT: Data len too big or zero\n");
			up (&dev->sem);
			return -EFAULT;
		}

		cnxtldataw=kzalloc(sizeof(char) * cnxtlen,	GFP_KERNEL);

		if (!cnxtldataw)
		{
			printk(KERN_WARNING "CNXT: Unable to allocate memory\n");
			up (&dev->sem);
			return -EFAULT;
		}

		cnxtldatar=kzalloc(sizeof(char) * cnxtlen,	GFP_KERNEL);

		if (!cnxtldatar)
		{
			printk(KERN_WARNING "CNXT: Unable to allocate memory\n");
			kfree(cnxtldataw);
			up (&dev->sem);
			return -EFAULT;
		}

		PDEBUG("CNXT: IOCTL WRITE: %x,%x,%x \n", cnxtudataw,cnxtudatar,cnxtlen);

		copy_from_user (cnxtldataw,(char *)cnxtudataw,cnxtlen);

		spi_message_init(&m);
		memset((void*)&t_main, 0, sizeof(struct spi_transfer));
		t_main.delay_usecs = 0;//wait 1 usec after each tx/rx
		t_main.len = cnxtlen;
		t_main.tx_buf = (void*)(cnxtldataw);
		t_main.rx_buf = (void*)(cnxtldatar);
		spi_message_add_tail(&t_main, &m);
		spi_sync(pt_cnxt_spi_dev->spi, &m);

		kfree(cnxtldataw);



		if (copy_to_user((char*)cnxtudatar, cnxtldatar, cnxtlen)) {
			kfree(cnxtldatar);
			return -EFAULT;
		}

		kfree(cnxtldatar);
		up (&dev->sem);
		break;

	default:
			printk(KERN_WARNING "Invalid led spi ioctl \n");
			return -ENOTTY;
	}
	return retval;
}

/* ****************************************************************************
 * This function is called by the OS and when a spi device that uses this driver is probed during
 * the system start-up.
 * ****************************************************************************/
static int __devinit cnxt_spi_probe(struct spi_device *spi)
{
	int result = 0;
	dev_t dev = 0;

/*
 * Get a range of minor numbers to work with, asking for a dynamic
 * major unless directed otherwise at load time.
 */
	printk("Trying to initialize the cnxt_spi driver \r\n");


	if (cnxt_spi_major) {
		dev = MKDEV(cnxt_spi_major, cnxt_spi_minor);
		result = register_chrdev_region(dev, 1, "cnxt_spi");
	}
	else {
		result = alloc_chrdev_region(&dev, cnxt_spi_minor, 1, "cnxt_spi");
		cnxt_spi_major = MAJOR(dev);
	}
	if (result < 0) {
		printk(KERN_WARNING "cnxt_spi: can't get major %d\n", cnxt_spi_major);
		return result;
	}


	/*Initialize driver internal vars*/
	memset((char*)&cnxt_spi_dev, 0, sizeof(cnxt_spi_dev_struct));
	cnxt_spi_dev.ver_major=CNXT_SPI_MAJ_VER;
	cnxt_spi_dev.ver_minor=CNXT_SPI_MIN_VER;
	cnxt_spi_dev.spi = spi;	//link char device to spi device

	/*Initialize mutexes, waitqueues, timers*/
	init_MUTEX(&cnxt_spi_dev.sem);
	init_waitqueue_head(&(cnxt_spi_dev.inq));
	init_waitqueue_head(&(cnxt_spi_dev.outq));

	/*Setup device*/
	cnxt_spi_setup_cdev(&cnxt_spi_dev);
    printk(KERN_ALERT "CNXT spi driver initialized, spi addr : %x\n", (unsigned int)spi);

	/*Initialize hw*/
    return result; /* sucess */

}
static int __devexit cnxt_spi_remove(struct spi_device *spi)
{
	return 0;
}
struct file_operations cnxt_spi_fops = {
	.owner =    THIS_MODULE,
	.poll =   	NULL,
	.ioctl =    cnxt_spi_ioctl,
	.open =     cnxt_spi_open,
	.release =  cnxt_spi_release,
};

static struct spi_driver cnxt_spi_driver = {
	.driver = {
		.name 	= "cnxt_spi",
		.bus	= &spi_bus_type,//defined in spi.c
		.owner	= THIS_MODULE,
	},
	.probe 	= cnxt_spi_probe,
	.remove = __devexit_p(cnxt_spi_remove),
};


/* ****************************************************************************
 * This function is called by the OS and registers the  spi driver to
 * the system.
 * ****************************************************************************/
static int __init cnxt_spi_init(void)
{
	int result;
    result = spi_register_driver(&cnxt_spi_driver);
	PDEBUG("spi_register_drive returned : %d \n", result);
	return result;
}

/* ****************************************************************************
 * This function is called by the OS and unregisters the spi driver from
 * the system. This function will call necessary functions to properly
 * close down the driver and free up any resources it may have be using.
 * ****************************************************************************/
static void cnxt_spi_exit(void)
{
	spi_unregister_driver(&cnxt_spi_driver);

    printk(KERN_ALERT "Exiting cnxt_spi Driver\n");
    return;

}

/*******************************************************
	Description: 	Set up the cdev structure for this device.

 ********************************************************/
void cnxt_spi_setup_cdev(cnxt_spi_dev_struct *dev)
{
	int err, devno = MKDEV(cnxt_spi_major, cnxt_spi_minor);

	cdev_init(&dev->cdev, &cnxt_spi_fops);
	dev->cdev.owner = THIS_MODULE;
	dev->cdev.ops = &cnxt_spi_fops;
	err = cdev_add (&dev->cdev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE "Error %d adding cnxt_spi", err);
}

module_init(cnxt_spi_init);
module_exit(cnxt_spi_exit);






