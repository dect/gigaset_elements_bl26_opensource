/*
 * cnxt_spi.h -- definitions for the spi of the CVM480 spi driver
  */

#ifndef _CNXT_SPI_H_
#define _CNXT_SPI_H_

#define EXTENTED_DRIVER

/*
 * Macros to help debugging
 */
#undef PDEBUG             /* undef it, just in case */
#ifdef CNXT_SPI_DEBUG
#  ifdef __KERNEL__
     /* This one if debugging is on, and kernel space */
#    define PDEBUG(fmt, args...) printk( KERN_DEBUG "cnxt_spi: " fmt, ## args)
#  else
     /* This one for user space */
#    define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif

#define MAX_CNXT_DATA 65535

#define CNXT_SPI_MAJOR 246
#ifndef CNXT_SPI_MAJOR
#define CNXT_SPI_MAJOR 0   /* dynamic major by default */
#endif

#ifndef CNXT_SPI_NR_DEVS
#define CNXT_SPI_NR_DEVS 1
#endif

#define CNXT_SPI_MAJ_VER 0
#define CNXT_SPI_MIN_VER 1


enum SPI_CNXT_SC1445x_IOCTL {
	CNXT_SPI_SC1445x_IOCTL_WRITE=5,
	CNXT_SPI_SC1445x_IOCTL_READ
} ;

#endif /* _CNXT_SPI_H_ */
