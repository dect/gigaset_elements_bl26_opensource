/*
 * led_spi.h -- definitions for the spi of the CVM480 spi driver
  */

#ifndef _LED_SPI_H_
#define _LED_SPI_H_

//#define LED_SPI_DEBUG
/*Hw configuration definitions*/

#define LED_POLLING_TIMER 1 //in 10 ms units
#define EXTENTED_DRIVER

/*
 * Macros to help debugging
 */
#undef PDEBUG             /* undef it, just in case */
#ifdef LED_SPI_DEBUG
#  ifdef __KERNEL__
     /* This one if debugging is on, and kernel space */
#    define PDEBUG(fmt, args...) printk( KERN_DEBUG "led_spi: " fmt, ## args)
#  else
     /* This one for user space */
#    define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif


#define LED_SPI_MAJOR 245
#ifndef LED_SPI_MAJOR
#define LED_SPI_MAJOR 0   /* dynamic major by default */
#endif

#ifndef LED_SPI_NR_DEVS
#define LED_SPI_NR_DEVS 1
#endif

#define LED_SPI_MAJ_VER 0
#define LED_SPI_MIN_VER 1

//#define LED_SPI_DEBUG_GPIO
#ifdef LED_SPI_DEBUG_GPIO
 /*Debugging*/
//#define SET_P210_HI 		{P2_SET_DATA_REG = (1<<10);}
//#define SET_P210_LO 		{P2_RESET_DATA_REG = (1<<10);}
//#define SET_P007_HI 		{P0_SET_DATA_REG = 0x80;}
//#define SET_P007_LO 		{P0_RESET_DATA_REG = 0x80;}
#endif //LED_SPI_DEBUG_GPIO

#if defined (CONFIG_L_V2_BOARD) ||defined(CONFIG_L_V2_BOARD_CNX)
	#define NUM_OF_LEDS ((3*16)+5)
	#define SPI_LEDS (3*16)
	#define NUM_OF_SPI_LED_MASKS 6
	//#define LED_TOGGLE_MASKS 2
	#define NUM_OF_LED_MASKS 7
	#define MIN_LED_BLINK_RATE 10
	#define NUM_OF_BLINK_PATTERNS 15
#ifdef EXTENTED_DRIVER
	#define MAX_PATTERNS_STATES 8
#endif
#elif defined(CONFIG_L_V3_BOARD)||defined(CONFIG_L_V3_BOARD_CNX)
	#define NUM_OF_LEDS (16+5)
	#define SPI_LEDS (16)
	#define NUM_OF_SPI_LED_MASKS 2
	//#define LED_TOGGLE_MASKS 2
	#define NUM_OF_LED_MASKS 3
	#define MIN_LED_BLINK_RATE 10
	#define NUM_OF_BLINK_PATTERNS 15
#ifdef EXTENTED_DRIVER
	#define MAX_PATTERNS_STATES 8
#endif
#elif defined(CONFIG_L_V4_BOARD)||defined(CONFIG_L_V5_BOARD)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	#define NUM_OF_LEDS ((2*16)+5)
	#define SPI_LEDS (2*16)
	#define NUM_OF_SPI_LED_MASKS 4
	//#define LED_TOGGLE_MASKS 2
	#define NUM_OF_LED_MASKS 5
	#define MIN_LED_BLINK_RATE 10
	#define NUM_OF_BLINK_PATTERNS 15
#ifdef EXTENTED_DRIVER
	#define MAX_PATTERNS_STATES 8
#endif
//NOTE
//THE LEDS FOR L_V4 HAVE THE FOLLOWING SETUP
// 8-12
// 16-31
// 32-36

#elif defined(CONFIG_L_V1_BOARD)
	#define NUM_OF_LEDS ((0)+1)
	#define SPI_LEDS (0)
	#define NUM_OF_SPI_LED_MASKS 0
	//#define LED_TOGGLE_MASKS 2
	#define NUM_OF_LED_MASKS 1
	#define MIN_LED_BLINK_RATE 10
	#define NUM_OF_BLINK_PATTERNS 15
#ifdef EXTENTED_DRIVER
	#define MAX_PATTERNS_STATES 8
#endif
#else
	#define NUM_OF_LEDS 0
#endif



#ifdef EXTENTED_DRIVER
typedef struct {
	unsigned char no_pattern_states;							//how many states a pattern has
	unsigned short pattern_states [MAX_PATTERNS_STATES];		//the duration of each state in 10ms units
}st_led_pattern;
#endif

typedef struct {
	unsigned char led_status [NUM_OF_LEDS];						//LED_OFF, LED_ON, LED_BLINKING
	unsigned char led_pattern_no [NUM_OF_LEDS];					//Array that shows which pattern a specific
																//BLINKING LED follows
	unsigned char pat_led_mask [NUM_OF_BLINK_PATTERNS] [NUM_OF_LED_MASKS];
																//a mask of all leds that blink with a specific pattern
#ifdef EXTENTED_DRIVER
	st_led_pattern led_pattern [NUM_OF_BLINK_PATTERNS];			//the pattern definition data
#else
	unsigned short pat_tmout [NUM_OF_BLINK_PATTERNS] [2];		//the pattern definition data (2 state patterns)
#endif
	unsigned char active_pat [NUM_OF_BLINK_PATTERNS];			//which patterns are active (have LEDs blinking)
	unsigned char force_change;									//a flag that informs the handler
																//that a change has been forced from the user.
}st_led_spi;													//Led state information used by the driver

typedef struct {
	//TODO BETTER ISOLATE GPIO AND SPI MASKS
	unsigned char led_mask [NUM_OF_LED_MASKS];					//the current state(ON, OFF) of the leds as used from the handler
	unsigned char led_status [NUM_OF_LEDS];						//the current status of the LED_OFF, LED_ON, LED_BLINKING as used from the handler
	unsigned char pat_state_index [NUM_OF_BLINK_PATTERNS];		//the index number of the current state of a specific pattern.
#ifdef EXTENTED_DRIVER
	st_led_pattern led_pattern [NUM_OF_BLINK_PATTERNS];			//the pattern definition data
#else
	unsigned short pat_tmout [NUM_OF_BLINK_PATTERNS] [2];		//the pattern definition data (2 state patterns)
#endif
	unsigned char pat_led_mask [NUM_OF_BLINK_PATTERNS] [NUM_OF_LED_MASKS];
																//a mask of all leds that blink with a specific pattern
	unsigned char active_pat [NUM_OF_BLINK_PATTERNS];
																//which patterns are active (have LEDs blinking)
}st_sys_led_spi;												//Led state information used by the handler

//information should be duplicated, since the led changes status when blinking ONLY when in transition to avoid
//glitches or other effects that will not give a nice effect.

enum SPI_LED_STATUS {
	LED_OFF=0,
	LED_ON,
	LED_BLINKING
};

enum SPI_LED_SC1445x_IOCTL {
	LED_SPI_SC1445x_IOCTL_LEDS_SET=5,//set led toggle mask
	LED_SPI_SC1445x_IOCTL_LEDS_GET,
	LED_SPI_SC1445x_IOCTL_LEDS_SET_EXTENDED
} ;

#endif /* _LED_SPI_H_ */
