/*
 * This file is the Led Spi Driver.
 */

 /*========================== NOTES =====================================*/
/* This driver provides the spi interface for LED control.
*
*/
 /*========================= Include files ==================================*/
/* linux/module specific headers */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>           /* location of file_operations structure*/
#include <asm/uaccess.h>        /* location copy_xyz_user*/
#include <linux/slab.h>         /* kmalloc and kfree */
#include <linux/delay.h>

#include <linux/proc_fs.h>
#include <linux/cdev.h> /*cdev*/
#include <linux/types.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/workqueue.h>
#include <linux/wait.h> /*kbd_sc14450_dev_struct*/
#include <linux/interrupt.h> /* request_irq */
#include <asm/irq.h>         /* request_irq */
#include <asm/regs.h>
#include "led_spi.h"

/*========================== Definitions ==================================*/

/***      Data types definitions       ***/
//#define u16 short
//Driver structs
typedef struct {
	struct semaphore sem;     /* mutual exclusion semaphore     */
	struct spi_device	*spi;
	//spinlock_t lock;
	wait_queue_head_t inq;
	wait_queue_head_t outq;
	struct cdev cdev;	  	  /* Char device structure		*/
	unsigned char flags;
	unsigned char ver_minor;/*stores driver's minor version number*/
	unsigned char ver_major;/*stores driver's majorversion number*/
}led_spi_dev_struct;

volatile st_led_spi led_spi;
volatile st_sys_led_spi sys_led_spi;
/***      Flags definitions     ***/

/***   Macros   ***/
#define	START_TIMER(TIMER, TMOUT) {TIMER.expires = jiffies + TMOUT;add_timer(&TIMER);}
#define	STOP_TIMER(TIMER) {del_timer(&TIMER);}
 /*========================== Function Prototypes ===========================*/
/** High level **/
//Interrupt
//static irqreturn_t kbd_int_handler(int irq, void *dev_id, struct pt_regs *regs);
//Tasklet

//Timers
//Module specific
static int __init led_spi_init(void);
static void led_spi_exit(void);
void led_spi_setup_cdev(led_spi_dev_struct *dev);
// Fops prototypes
int led_spi_open(struct inode *inode, struct file *filp);
int led_spi_release(struct inode *inode, struct file *filp);
int led_spi_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg);

#if 0
static ssize_t led_spi_read (struct file *filp, char __user *buf, size_t count, loff_t *pos);
static ssize_t led_spi_write(struct file *filp, const char __user *buf, size_t count,
                loff_t *f_pos);
#endif

static int __devinit led_spi_probe(struct spi_device *spi);
static int __devexit led_spi_remove(struct spi_device *spi);



/*** Parameters which can be set at load time  ***/
int led_spi_major =  LED_SPI_MAJOR;
int led_spi_minor =   0;


module_param(led_spi_major, int, S_IRUGO);
module_param(led_spi_minor, int, S_IRUGO);

MODULE_AUTHOR("Pavlos Bougas");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("LED Spi Driver");


extern unsigned int gpio_leds_mask;


/***      Var definitions  and initializations     ***/
/** Low level **/
led_spi_dev_struct led_spi_dev;
led_spi_dev_struct *pt_led_spi_dev=&led_spi_dev;

struct timer_list led_timer;
struct spi_message	m;
struct spi_transfer	t_main;
int led_tr_lock=0;
/** High level **/


/***      Timer initializations     ***/

/***      Tasklet     ***/


/***************          Main body             ***************/
/*******************************************************
	Description: 	Open led_spi_dev dev file.

 ********************************************************/
int led_spi_open(struct inode *inode, struct file *filp)
{
	led_spi_dev_struct *dev; /* device information */

	dev = container_of(inode->i_cdev, led_spi_dev_struct, cdev);
	pt_led_spi_dev=dev;

	filp->private_data = dev; /* for other methods */
	PDEBUG("LED_SPI OPENS WITH FLAGS %d   \n", (filp->f_flags ));
	if (filp->f_flags & O_NONBLOCK){
		PDEBUG("LED_SPI OPENED AS NONBLOCK \n");
	}

	STOP_TIMER(led_timer);
	memset ((void*)&led_spi,0,sizeof(st_led_spi));
	led_spi.force_change=1;
    led_tr_lock=0;
	START_TIMER(led_timer, LED_POLLING_TIMER);

	return 0;
}

/*******************************************************
	Description: 	Close device file.

 ********************************************************/
int led_spi_release(struct inode *inode, struct file *filp)
{
	STOP_TIMER(led_timer);
	return 0;
}


#ifdef EXTENTED_DRIVER
int find_pattern(st_led_pattern *per)
#else
int find_pattern(unsigned short *per)
#endif
{
	unsigned int i;
	i=0;
	while (i<NUM_OF_BLINK_PATTERNS)
	{
#ifdef EXTENTED_DRIVER
		if ((led_spi.active_pat [i]!=0) && \
			(per->no_pattern_states == led_spi.led_pattern[i].no_pattern_states))
			{
			 unsigned int k;
			 k=0;
			 while ((k<per->no_pattern_states)&&(per->pattern_states[k]==led_spi.led_pattern[i].pattern_states[k]))
				 k++;

			 if (k==per->no_pattern_states)
				 break;
			}

#else
			if ((led_spi.active_pat [i]!=0) && \
				(((per[0]==led_spi.pat_tmout[i] [0])&& \
				  (per[1]==led_spi.pat_tmout[i] [1]))))
			break;

#endif

		i++;
	}

	if (i<NUM_OF_BLINK_PATTERNS)
		return (i);					//active pattern found

	i=0;
	while ((i<NUM_OF_BLINK_PATTERNS)&&(led_spi.active_pat [i]!=0))
	{
		i++;
	}

	if (i<NUM_OF_BLINK_PATTERNS)
		return (i);					//found a free pattern

	return (-1);	//nopattern found
}

int led_spi_ioctl(struct inode *inode, struct file *filp,
                 unsigned int cmd, unsigned long arg)
{
	int retval=0;
	led_spi_dev_struct *dev = filp->private_data;

	unsigned char ledindex, new_status;
	unsigned int intindex;
	int i,patno;
#ifdef EXTENTED_DRIVER
	st_led_pattern per;
#else
	unsigned short int per[2];
#endif

	PDEBUG("LED SPI IOCTL  \n");

	switch(cmd)
	{
/**** SET IOCTLS ****/
	case LED_SPI_SC1445x_IOCTL_LEDS_SET:
		if (down_interruptible(&dev->sem))
		{
			return -ERESTARTSYS;
		}

		if ((copy_from_user(&ledindex, (char*)arg, 1)))
		{
			printk(KERN_WARNING "Copy from user failed\n");
			up (&dev->sem);
			return -EFAULT;
		}
		intindex=ledindex;
		if (ledindex>=NUM_OF_LEDS)
		{
			printk(KERN_WARNING "Wrong LED id\n");
			up (&dev->sem);
			return -EFAULT;
		}

		if ((copy_from_user(&(new_status), (char*)arg+1, 1))) {
			printk(KERN_WARNING "Copy from user failed\n");
			up (&dev->sem);
			return -EFAULT;
		}
		PDEBUG("LED IOCTL: %x,%x,%x \n", intindex,new_status,led_spi.led_status[intindex]);

		if (led_spi.led_status[intindex]==LED_BLINKING)
			//TODO do not remove and add if the same blinking rate is going to be used.
		{
			//remove it from pattern mask
			led_spi.pat_led_mask[(led_spi.led_pattern_no[intindex])] [intindex>>3]&=~(1<<(intindex&7));
			i=0;
			while ((i<NUM_OF_LED_MASKS) && (led_spi.pat_led_mask[(led_spi.led_pattern_no[intindex])] [i]==0))
				i++;

			if (i==NUM_OF_LED_MASKS)
			{
				led_spi.active_pat[(led_spi.led_pattern_no[intindex])]=0;
#ifdef EXTENTED_DRIVER
				{
					int j;
					led_spi.led_pattern[(led_spi.led_pattern_no[intindex])].no_pattern_states=0;
					for (j=0;j<MAX_PATTERNS_STATES;j++)
						led_spi.led_pattern[(led_spi.led_pattern_no[intindex])].pattern_states[j]=0;
				}
#else
				led_spi.pat_tmout[(led_spi.led_pattern_no[intindex])][0]=0;
				led_spi.pat_tmout[(led_spi.led_pattern_no[intindex])][1]=0;
#endif
			}
			led_spi.led_pattern_no[intindex]=0;
		}
		led_spi.led_status[intindex]=new_status;

		//add it to a new or existing pattern
		if (led_spi.led_status[ledindex]==LED_BLINKING)
		{
#ifdef EXTENTED_DRIVER
			per.no_pattern_states=2;

			if ((copy_from_user((char*) (&per.pattern_states), (char*)arg+2, 4)))
			{
				printk(KERN_WARNING "Copy from user failed\n");
				up (&dev->sem);
				return -EFAULT;
			}
			//added to a pattern
			patno=find_pattern(&per);
#else
			if ((copy_from_user((char*) (&per), (char*)arg+2, 4))) {
			printk(KERN_WARNING "Copy from user failed\n");
			up (&dev->sem);
			return -EFAULT;
			}
			//added to a pattern
			patno=find_pattern(per);
#endif
			if (patno<0)
			{
				led_spi.led_status[intindex]=LED_OFF;
				return -EFAULT;
			}

			led_spi.led_pattern_no[intindex]=patno;
			led_spi.pat_led_mask[patno] [intindex>>3]|=(1<<(intindex&7));
			if (led_spi.active_pat[patno]==0)
			{
#ifdef EXTENTED_DRIVER
				led_spi.led_pattern[patno].no_pattern_states=2;
				{
						led_spi.led_pattern[patno].pattern_states[0]=per.pattern_states[0];
						led_spi.led_pattern[patno].pattern_states[1]=per.pattern_states[1];

				}
#else
				led_spi.pat_tmout[patno][0]=per[0];
				led_spi.pat_tmout[patno][1]=per[1];
#endif
				led_spi.active_pat[patno]=1;
			}
		}

#if 1
		led_spi.force_change=1;
#endif
		up (&dev->sem);
		break;

	case LED_SPI_SC1445x_IOCTL_LEDS_GET:
#if 0
		if (down_interruptible(&dev->sem))
			return -ERESTARTSYS;
		if(copy_to_user((char*)arg, (char*)led_spi_dev.led_spi.led_mask, sizeof(char [LED_TOGGLE_MASKS] [NUM_OF_SPILEDS]))){
			printk(KERN_WARNING "Ioctl failed \n");
			up (&dev->sem);
			return -EFAULT;
		}
		up (&dev->sem);
#endif

		break;
#ifdef EXTENTED_DRIVER
	case LED_SPI_SC1445x_IOCTL_LEDS_SET_EXTENDED:
		if (down_interruptible(&dev->sem))
		{
			return -ERESTARTSYS;
		}

		if ((copy_from_user(&ledindex, (char*)arg, 1)))
		{
			printk(KERN_WARNING "Copy from user failed\n");
			up (&dev->sem);
			return -EFAULT;
		}
		intindex=ledindex;
		if (ledindex>=NUM_OF_LEDS)
		{
			printk(KERN_WARNING "Wrong LED id\n");
			up (&dev->sem);
			return -EFAULT;
		}

		if ((copy_from_user(&(new_status), (char*)arg+1, 1))) {
			printk(KERN_WARNING "Copy from user failed\n");
			up (&dev->sem);
			return -EFAULT;
		}
		if (led_spi.led_status[intindex]==LED_BLINKING)
			//TODO do not remove and add if the same blinking rate is going to be used.
		{
			//remove it from pattern mask
			led_spi.pat_led_mask[(led_spi.led_pattern_no[intindex])] [intindex>>3]&=~(1<<(intindex&7));
			i=0;
			while ((i<NUM_OF_LED_MASKS) && (led_spi.pat_led_mask[(led_spi.led_pattern_no[intindex])] [i]==0))
				i++;

			if (i==NUM_OF_LED_MASKS)
			{
				led_spi.active_pat[(led_spi.led_pattern_no[intindex])]=0;
				{
#if MAX_PATTERNS_STATES!=8
					int j;
#endif
					led_spi.led_pattern[(led_spi.led_pattern_no[intindex])].no_pattern_states=0;

#if MAX_PATTERNS_STATES!=8
					for (j=0;j<MAX_PATTERNS_STATES;j++)
						led_spi.led_pattern[(led_spi.led_pattern_no[intindex])].pattern_states[j]=0;
#else
					{
					*(unsigned long*) ((&led_spi.led_pattern[(led_spi.led_pattern_no[intindex])].pattern_states[0]))=0;
					*(unsigned long*) ((&led_spi.led_pattern[(led_spi.led_pattern_no[intindex])].pattern_states[2]))=0;
					*(unsigned long*) ((&led_spi.led_pattern[(led_spi.led_pattern_no[intindex])].pattern_states[4]))=0;
					*(unsigned long*) ((&led_spi.led_pattern[(led_spi.led_pattern_no[intindex])].pattern_states[6]))=0;
					}
#endif

				}
			}
			led_spi.led_pattern_no[intindex]=0;
		}
		led_spi.led_status[intindex]=new_status;

		if (led_spi.led_status[ledindex]==LED_BLINKING)
		{
			per.no_pattern_states=*((char*)(arg+2));
			if ((per.no_pattern_states==0)||(per.no_pattern_states>MAX_PATTERNS_STATES))
			{
				printk(KERN_WARNING "Invalid number of pattern states\n");
				up (&dev->sem);
				return -EFAULT;
			}

			if ((copy_from_user((char*) (&per.pattern_states), (char*)arg+3, 2*per.no_pattern_states)))
			{
				printk(KERN_WARNING "Copy from user failed\n");
				up (&dev->sem);
				return -EFAULT;
			}
			//added to a pattern
			patno=find_pattern(&per);
			if (patno<0)
			{
				led_spi.led_status[intindex]=LED_OFF;
				return -EFAULT;
			}

			led_spi.led_pattern_no[intindex]=patno;
			led_spi.pat_led_mask[patno] [intindex>>3]|=(1<<(intindex&7));
			if (led_spi.active_pat[patno]==0)
			{
				led_spi.led_pattern[patno].no_pattern_states=per.no_pattern_states;
				{
					unsigned int j;
					for (j=0;j<per.no_pattern_states;j++)
						led_spi.led_pattern[patno].pattern_states[j]=per.pattern_states[j];

				}
				led_spi.active_pat[patno]=1;
			}
		}

#if 1
		led_spi.force_change=1;
#endif
		up (&dev->sem);
		break;


#endif

		default:
			printk(KERN_WARNING "Invalid led spi ioctl \n");
			return -ENOTTY;
	}
	return retval;
}

static void led_tr_complete (void *arg)
{
	//Previous update have been serviced.
	led_tr_lock=0;
}
static void led_timer_fn(unsigned long param)
{
	unsigned int i,j,index;
	char ch;
	unsigned short int change=0;
	unsigned short int needtoupdate=0;	//signal that we need to send new data to the SPI

	change=led_spi.force_change;		//check if at least one IOCTL was completed before last timer tick
	//assume this actions are atomic although it does not impact proper operation
	//TODO use a mutex mechanism
	led_spi.force_change=0;				//signal that we have received


	if (change!=0)						//if something has changed
	{
		for (i=0;i<NUM_OF_LEDS;i++)		//check all leds and if the new status is not BLINKING, copy the new status
		{
			ch=led_spi.led_status[i];
			switch (ch)
			{
				case LED_OFF:
						sys_led_spi.led_mask[(i>>3)]&=~(1<<(i&7));
						sys_led_spi.led_status[i]=led_spi.led_status[i];
						break;
				case LED_ON:
						sys_led_spi.led_mask[(i>>3)]|=(1<<(i&7));
						sys_led_spi.led_status[i]=led_spi.led_status[i];
						break;
				case LED_BLINKING:
						break;
			}
		}
	}


	for (i=0;i<NUM_OF_BLINK_PATTERNS;i++)				//start processing the active PATTERNS
	{
		if (sys_led_spi.active_pat[i]!=0)
		{
			index=sys_led_spi.pat_state_index[i];		//get the current state of the timer
#ifdef EXTENTED_DRIVER
			if	(sys_led_spi.led_pattern[i].pattern_states[index])	//if the current state timer is not zero
			{
				(sys_led_spi.led_pattern[i].pattern_states[index])--;	//update the state timer
				if (change!=0)
				{
					//restore old_mask value that might be ruined from moving a led in static mode.
					//the led will be removed when the pattern changes from one state to another (transition)
					if (index&1)
						for (j=0;j<NUM_OF_LED_MASKS;j++)
							sys_led_spi.led_mask[j]&=~(sys_led_spi.pat_led_mask[i][j]);
						else
							for (j=0;j<NUM_OF_LED_MASKS;j++)
								sys_led_spi.led_mask[j]|=(sys_led_spi.pat_led_mask[i][j]);
				}
			}

#else
			if	(sys_led_spi.pat_tmout[i] [index])
			{
				(sys_led_spi.pat_tmout[i] [index])--;
				if (change!=0)
				{
					//restore old_mask value that might be ruined from moving in static mode.
					if (index)
#if (NUM_OF_LED_MASKS!=7)
						for (j=0;j<NUM_OF_LED_MASKS;j++)
							sys_led_spi.led_mask[j]&=~(sys_led_spi.pat_led_mask[i][j]);
					else
						for (j=0;j<NUM_OF_LED_MASKS;j++)
							sys_led_spi.led_mask[j]|=(sys_led_spi.pat_led_mask[i][j]);
#else
					{
						*(unsigned long*) ((&sys_led_spi.led_mask[0]))&=~(*(unsigned long*) ((&sys_led_spi.pat_led_mask[i][0])));
						*(unsigned short*) ((&sys_led_spi.led_mask[4]))&=~(*(unsigned short*) ((&sys_led_spi.pat_led_mask[i][4])));
						sys_led_spi.led_mask[6]&=~(sys_led_spi.pat_led_mask[i][6]);
					}
					else
					{
						*(unsigned long*) ((&sys_led_spi.led_mask[0]))|=(*(unsigned long*) ((&sys_led_spi.pat_led_mask[i][0])));
						*(unsigned short*) ((&sys_led_spi.led_mask[4]))|=(*(unsigned short*) ((&sys_led_spi.pat_led_mask[i][4])));
						sys_led_spi.led_mask[6]|=(sys_led_spi.pat_led_mask[i][6]);
					}

#endif
				}
			}
#endif
			else //current state is in transition
#ifdef EXTENTED_DRIVER
			{
				//update only after you have finished
				needtoupdate=1;	//signal that we need to write to the physical interface (SPI, latch)

				sys_led_spi.pat_state_index[i]++;	//increase the state index
				if (sys_led_spi.pat_state_index[i]>=sys_led_spi.led_pattern[i].no_pattern_states)
					sys_led_spi.pat_state_index[i]=0;


				if ((!led_spi.force_change)&&(sys_led_spi.active_pat[i]!=led_spi.active_pat[i]))
				{
					led_spi.force_change=1;			//the pattern is disabled force the change for the next cycle as well
				}
				sys_led_spi.active_pat[i]=led_spi.active_pat[i];	//copy the pattern state from the driver state
				sys_led_spi.led_pattern[i].no_pattern_states=led_spi.led_pattern[i].no_pattern_states; //copy the pattern state from the driver state
				{
					//update only the next value for faster performance
					sys_led_spi.led_pattern[i].pattern_states[sys_led_spi.pat_state_index[i]]=led_spi.led_pattern[i].pattern_states[sys_led_spi.pat_state_index[i]];
				}
#else
			{
				//update only after you have finished
				needtoupdate=1; //signal that we need to write to the physical interface (SPI, latch)
				sys_led_spi.pat_state_index[i]= ((sys_led_spi.pat_state_index[i]+1)&1); //increase the state index

				if ((!led_spi.force_change)&&(sys_led_spi.active_pat[i]!=led_spi.active_pat[i]))
				{
					led_spi.force_change=1;		//the pattern is disabled force the change for the next cycle as well
				}
				sys_led_spi.active_pat[i]=led_spi.active_pat[i];			//copy the pattern state from the driver state
				sys_led_spi.pat_tmout[i] [0]=led_spi.pat_tmout[i] [0];
				sys_led_spi.pat_tmout[i] [1]=led_spi.pat_tmout[i] [1];
#endif

				//update the pattern led mask from the driver
				//(which leds are included in a specific pattern)
				for (j=0;j<NUM_OF_LED_MASKS;j++)
				{
					if ((!led_spi.force_change)&&(sys_led_spi.pat_led_mask[i][j]!=(led_spi.pat_led_mask[i][j])))
					{
							led_spi.force_change=1; //need to update to a new state some leds in the next cycle
					}
					sys_led_spi.pat_led_mask[i][j]=(led_spi.pat_led_mask[i][j]);
				}

				//update the current led mask with the pattern new state.
				if ((index&1)==0)
#if NUM_OF_LED_MASKS!=7
				{
					for (j=0;j<NUM_OF_LED_MASKS;j++)
						sys_led_spi.led_mask[j]&=~(sys_led_spi.pat_led_mask[i][j]);
				}
				else
				{
					for (j=0;j<NUM_OF_LED_MASKS;j++)
						sys_led_spi.led_mask[j]|=(sys_led_spi.pat_led_mask[i][j]);
				}
#else
				{
					*(unsigned long*) ((&sys_led_spi.led_mask[0]))&=~(*(unsigned long*) ((&sys_led_spi.pat_led_mask[i][0])));
					*(unsigned short*) ((&sys_led_spi.led_mask[4]))&=~(*(unsigned short*) ((&sys_led_spi.pat_led_mask[i][4])));
					sys_led_spi.led_mask[6]&=~(sys_led_spi.pat_led_mask[i][6]);
				}
				else
				{
					*(unsigned long*) ((&sys_led_spi.led_mask[0]))|=(*(unsigned long*) ((&sys_led_spi.pat_led_mask[i][0])));
					*(unsigned short*) ((&sys_led_spi.led_mask[4]))|=(*(unsigned short*) ((&sys_led_spi.pat_led_mask[i][4])));
					sys_led_spi.led_mask[6]|=(sys_led_spi.pat_led_mask[i][6]);
				}
#endif
				break;
			}
		}
		else	//if pattern is not active
		{
			if (led_spi.active_pat[i]!=0)	//if it is activated in the driver we need to activated in the handler as well
			{
				needtoupdate=1;										//copy the data from the driver
				sys_led_spi.pat_state_index[i]= 0;
				sys_led_spi.active_pat[i]=led_spi.active_pat[i];
#ifdef EXTENTED_DRIVER
				sys_led_spi.led_pattern[i].no_pattern_states=led_spi.led_pattern[i].no_pattern_states;
				{
					//copy only the first state
					sys_led_spi.led_pattern[i].pattern_states[0]=led_spi.led_pattern[i].pattern_states[0];
				}
#else
				sys_led_spi.pat_tmout[i] [0]=led_spi.pat_tmout[i] [0];
				sys_led_spi.pat_tmout[i] [1]=led_spi.pat_tmout[i] [1];
#endif
#if (NUM_OF_LED_MASKS!=7)
				for (j=0;j<NUM_OF_LED_MASKS;j++)
					sys_led_spi.pat_led_mask[i][j]=(led_spi.pat_led_mask[i][j]);
				for (j=0;j<NUM_OF_LED_MASKS;j++)
					sys_led_spi.led_mask[j]|=(sys_led_spi.pat_led_mask[i][j]);
#else
				*(unsigned long*) ((&sys_led_spi.pat_led_mask[i][0]))=*(unsigned long*) ((&led_spi.pat_led_mask[i][0]));
				*(unsigned short*) ((&sys_led_spi.pat_led_mask[i][4]))=*(unsigned short*) ((&led_spi.pat_led_mask[i][4]));
				sys_led_spi.pat_led_mask[i][6]=led_spi.pat_led_mask[i][6];
				
				*(unsigned long*) ((&sys_led_spi.led_mask[0]))|=(*(unsigned long*) ((&sys_led_spi.pat_led_mask[i][0])));
				*(unsigned short*) ((&sys_led_spi.led_mask[4]))|=(*(unsigned short*) ((&sys_led_spi.pat_led_mask[i][4])));
				sys_led_spi.led_mask[6]|=(sys_led_spi.pat_led_mask[i][6]);
				
#endif
			}
		}
	}

	if ((change==1)||(needtoupdate==1))			//if the physical interface needs to be updated
	{
		needtoupdate=0;
		PDEBUG(KERN_WARNING "%2.2x %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x \n", \
						(sys_led_spi.led_mask)[0], \
						(sys_led_spi.led_mask)[1], \
						(sys_led_spi.led_mask)[2], \
						(sys_led_spi.led_mask)[3], \
						(sys_led_spi.led_mask)[4], \
						(sys_led_spi.led_mask)[5], \
						(sys_led_spi.led_mask)[6]);
#if defined (CONFIG_SC1445x_LED_SPI_SUPPORT)
		if (led_tr_lock)							//Case that the previous update have not been serviced
		{											//from the SPI
			printk (KERN_WARNING "SPI Lock \r\n");
			led_spi.force_change=1;					//update next time if not locked
			START_TIMER(led_timer, LED_POLLING_TIMER);
			return ;
		}
		led_tr_lock=1;								//New Message

		spi_message_init(&m);
		memset((void*)&t_main, 0, sizeof(struct spi_transfer));
		m.complete = led_tr_complete;

		t_main.delay_usecs = 0;//wait 1 usec after each tx/rx
		t_main.len = NUM_OF_SPI_LED_MASKS;
		t_main.tx_buf = (void*)&(sys_led_spi.led_mask[0]);
		spi_message_add_tail(&t_main, &m);
		spi_async(pt_led_spi_dev->spi, &m); //cannot do spi_sync I get a Scheduling while atomic error.
#endif
	}
#if defined(CONFIG_L_V2_BOARD)||defined(CONFIG_L_V3_BOARD)||defined(CONFIG_L_V4_BOARD)||defined(CONFIG_L_V5_BOARD)\
	||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	//transfer MASK to KEYBOARD DRIVER
	gpio_leds_mask=sys_led_spi.led_mask[NUM_OF_LED_MASKS-1];
#endif
#if defined(CONFIG_L_V1_BOARD)
	if (sys_led_spi.led_mask[NUM_OF_LED_MASKS-1])
		P0_SET_DATA_REG=(1<<5);
	else
		P0_RESET_DATA_REG=(1<<5);
#endif
	START_TIMER(led_timer, LED_POLLING_TIMER);
}
/* ****************************************************************************
 * This function is called by the OS and when a spi device that uses this driver is probed during
 * the system start-up.
 * ****************************************************************************/
static int __devinit led_spi_probe(struct spi_device *spi)
{
	int result = 0;
	dev_t dev = 0;

/*
 * Get a range of minor numbers to work with, asking for a dynamic
 * major unless directed otherwise at load time.
 */
	printk("Trying to initialize the led driver \r\n");


	if (led_spi_major) {
		dev = MKDEV(led_spi_major, led_spi_minor);
		result = register_chrdev_region(dev, 1, "led_spi");
	}
	else {
		result = alloc_chrdev_region(&dev, led_spi_minor, 1, "led_spi");
		led_spi_major = MAJOR(dev);
	}
	if (result < 0) {
		printk(KERN_WARNING "led_spi: can't get major %d\n", led_spi_major);
		return result;
	}


	/*Initialize driver internal vars*/
	memset((char*)&led_spi_dev, 0, sizeof(led_spi_dev_struct));
	led_spi_dev.ver_major=LED_SPI_MAJ_VER;
	led_spi_dev.ver_minor=LED_SPI_MIN_VER;
	led_spi_dev.spi = spi;	//link char device to spi device

	/*Initialize mutexes, waitqueues, timers*/
	init_MUTEX(&led_spi_dev.sem);
	init_waitqueue_head(&(led_spi_dev.inq));
	init_waitqueue_head(&(led_spi_dev.outq));
	/*timers*/
	init_timer(&led_timer);
	led_timer.data = 0;
	led_timer.function = led_timer_fn;

	/*Setup device*/
	led_spi_setup_cdev(&led_spi_dev);
    printk(KERN_ALERT "LED spi driver initialized, spi addr : %x\n", (unsigned int)spi);

	/*Initialize hw*/
    return result; /* sucess */

}
static int __devexit led_spi_remove(struct spi_device *spi)
{
	return 0;
}
struct file_operations led_spi_fops = {
	.owner =    THIS_MODULE,
	.poll =   	NULL,
	.ioctl =    led_spi_ioctl,
	.open =     led_spi_open,
	.release =  led_spi_release,
};

static struct spi_driver led_spi_driver = {
	.driver = {
		.name 	= "led_spi",
		.bus	= &spi_bus_type,//defined in spi.c
		.owner	= THIS_MODULE,
	},
	.probe 	= led_spi_probe,
	.remove = __devexit_p(led_spi_remove),
};
#if 0
static ssize_t led_spi_read (struct file *filp, char __user *buf, size_t count, loff_t *pos)
{
	return count;
}
static ssize_t led_spi_write(struct file *filp, const char __user *buf, size_t count,
                loff_t *f_pos)
{
	return count;
}
#endif
#if defined (CONFIG_SC1445x_LED_SPI_SUPPORT)
/* ****************************************************************************
 * This function is called by the OS and registers the  spi driver to
 * the system.
 * ****************************************************************************/
static int __init led_spi_init(void)
{
	int result;
    result = spi_register_driver(&led_spi_driver);
	PDEBUG("spi_register_drive returned : %d \n", result);
	return result;
}

/* ****************************************************************************
 * This function is called by the OS and unregisters the spi driver from
 * the system. This function will call necessary functions to properly
 * close down the driver and free up any resources it may have be using.
 * ****************************************************************************/
static void led_spi_exit(void)
{
	spi_unregister_driver(&led_spi_driver);

    printk(KERN_ALERT "Exiting led_spi Driver\n");
    return;

}
#else
/* ****************************************************************************
 * This function is called by the OS and registers the  spi driver to
 * the system.
 * ****************************************************************************/
static int __init led_spi_init(void)
{
	int result = 0;
	dev_t dev = 0;

/*
 * Get a range of minor numbers to work with, asking for a dynamic
 * major unless directed otherwise at load time.
 */
	printk("Trying to initialize the led driver \r\n");


	if (led_spi_major) {
		dev = MKDEV(led_spi_major, led_spi_minor);
		result = register_chrdev_region(dev, 1, "led_spi");
	}
	else {
		result = alloc_chrdev_region(&dev, led_spi_minor, 1, "led_spi");
		led_spi_major = MAJOR(dev);
	}
	if (result < 0) {
		printk(KERN_WARNING "led_spi: can't get major %d\n", led_spi_major);
		return result;
	}


	/*Initialize driver internal vars*/
	memset((char*)&led_spi_dev, 0, sizeof(led_spi_dev_struct));
	led_spi_dev.ver_major=LED_SPI_MAJ_VER;
	led_spi_dev.ver_minor=LED_SPI_MIN_VER;


	/*Initialize mutexes, waitqueues, timers*/
	init_MUTEX(&led_spi_dev.sem);
	init_waitqueue_head(&(led_spi_dev.inq));
	init_waitqueue_head(&(led_spi_dev.outq));
	/*timers*/
	init_timer(&led_timer);
	led_timer.data = 0;
	led_timer.function = led_timer_fn;

	/*Setup device*/
	led_spi_setup_cdev(&led_spi_dev);
    printk(KERN_ALERT "LED spi driver initialized \r\n");

	/*Initialize hw*/
    return result; /* sucess */
}

/* ****************************************************************************
 * This function is called by the OS and unregisters the spi driver from
 * the system. This function will call necessary functions to properly
 * close down the driver and free up any resources it may have be using.
 * ****************************************************************************/
static void led_spi_exit(void)
{
	dev_t devno = MKDEV(led_spi_dev.ver_major, led_spi_dev.ver_minor);

	/* Get rid of our char dev entries */
	cdev_del(&led_spi_dev.cdev);
	/* cleanup_module is never called if registering failed */
	unregister_chrdev_region(devno, 1);

    printk(KERN_ALERT "Exiting led_spi Driver\n");
    return;

}



#endif
/*******************************************************
	Description: 	Set up the cdev structure for this device.

 ********************************************************/
void led_spi_setup_cdev(led_spi_dev_struct *dev)
{
	int err, devno = MKDEV(led_spi_major, led_spi_minor);

	memset ((void*)&sys_led_spi,0,sizeof(st_sys_led_spi));
	memset ((void*)&led_spi,0,sizeof(st_led_spi));
	cdev_init(&dev->cdev, &led_spi_fops);
	dev->cdev.owner = THIS_MODULE;
	dev->cdev.ops = &led_spi_fops;
	err = cdev_add (&dev->cdev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE "Error %d adding led_spi", err);
}

module_init(led_spi_init);
module_exit(led_spi_exit);






