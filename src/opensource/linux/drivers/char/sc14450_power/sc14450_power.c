/*
 * SC14450 Power Driver
 * Based on the code from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.   
 *
 * 
 */

/***************       Include headers section       ***************/

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <asm-cr16/uaccess.h>

#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */

#include <linux/proc_fs.h> /*semaphores*/
#include <linux/cdev.h> /*cdev*/
#include <linux/wait.h> /*kbd_sc14450_dev_struct*/
#include <linux/interrupt.h>/*irq*/
#include <linux/spinlock.h>

#include <asm/regs.h>

#include <asm-cr16/sc14450_power.h>


/***************       Definitions section             ***************/


/***      Data types definitions       ***/

//Driver structs
typedef struct {	
    struct semaphore sem;       /* mutual exclusion semaphore     */
    spinlock_t lock;            /* For mutual exclusion */
	struct cdev cdev;	  	    /* Char device structure		*/
    wait_queue_head_t inq;      /* read and write queues */		
	unsigned char ver_minor;    /*stores driver's minor version number*/
	unsigned char ver_major;    /*stores driver's majorversion number*/

    struct timer_list	watchdog;

    u16 audio_enable;
    u16 display_enable;

    u16 audio_delay;
    u16 display_delay;

    u16 audio_delay_val;
    u16 display_delay_val;
    
    u16 net_enable_port2;
    
} sc1445x_power_struct;

sc1445x_power_struct sc1445x_power_dev;


//Registers

/***   Macros   ***/
		
/***      Function Prototypes       ***/

static ssize_t sc1445x_power_read(struct file* file, char __user *buf, size_t count, loff_t *ppos);
void sc1445x_power_setup_cdev(sc1445x_power_struct *dev);
inline void sc1445x_power_ioctl_audio_close(void);
inline void sc1445x_power_ioctl_display_close(void);
void sc1445x_power_ioctl_set_max_state(u16);

/** Low level **/
//unsigned short hw_init(void);

/** High level **/
//Interrupt

//Tasklet

//Timers
int power_cnt=0;

static void sc1445x_power_watchdog(unsigned long arg)
{

    if (sc1445x_power_dev.audio_delay > 1) {
        sc1445x_power_dev.audio_delay--;
    } else if (sc1445x_power_dev.audio_delay == 1) {
            if (!sc1445x_power_dev.audio_enable) {
                sc1445x_power_ioctl_audio_close(); 
                sc1445x_power_dev.audio_delay=0;
            } else {
                sc1445x_power_dev.audio_delay=sc1445x_power_dev.audio_delay_val;
            }
    } 

    if (sc1445x_power_dev.display_delay > 1) {
        sc1445x_power_dev.display_delay--;
    } else if (sc1445x_power_dev.display_delay == 1) {
            if (!sc1445x_power_dev.display_enable) {
                sc1445x_power_ioctl_display_close(); 
                sc1445x_power_dev.display_delay=0;
            } else {
                sc1445x_power_dev.display_delay=sc1445x_power_dev.display_delay_val;
            }
    }

    // conter used for setting low power mode for the first time after startup
    if (power_cnt < 10) power_cnt++;
 //   if (power_cnt == 1) sc1445x_power_ioctl_set_max_state(3);

	mod_timer(&sc1445x_power_dev.watchdog, jiffies + HZ);
    
	return ;
    
}

/*Static functions*/

//Module specific

// Fops prototypes 
int sc1445x_power_open(struct inode *inode, struct file *filp);
int sc1445x_power_release(struct inode *inode, struct file *filp);
int sc1445x_power_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg);

/*** Parameters which can be set at load time  ***/
int sc1445x_power_major =   SC1445x_POWER_MAJOR;
int sc1445x_power_minor =   0;

module_param(sc1445x_power_major, int, S_IRUGO);
module_param(sc1445x_power_minor, int, S_IRUGO);


MODULE_AUTHOR("SITEL SEMICONDUCTORS");
MODULE_LICENSE("GPL");

/***      Var definitions  and initializations     ***/
/** Low level **/


u16 power_state = SC1445x_POWER_SPEED_80;
u16 sc1445x_power_audio_enable=0;

/** High level **/
struct file_operations sc1445x_power_fops = {
	.owner =    THIS_MODULE,
	.poll =   	NULL,
	.read =     sc1445x_power_read,
	.write =    NULL,
	.ioctl =    sc1445x_power_ioctl,
	.open =     sc1445x_power_open,
	.release =  sc1445x_power_release,
};


/***      Timer initializations     ***/


/***      Tasklet     ***/


/***************          Main body             ***************/



#if 0
#define _P2_DATA_REG                             (0xFF4890)  /* P2 Data input /out register */
#define _P2_SET_DATA_REG                         (0xFF4892)  /* P2 Set port pins register */
#define _P2_RESET_DATA_REG                       (0xFF4894)  /* P2 Reset port pins register */
#define _P2_07_MODE_REG                          (0xFF48AE)  /* P2y Mode Register */
#define _P2_10_MODE_REG                          (0xFF48B4)  /* P2y Mode Register */


#define MDC_BIT     7
#define MDIO_BIT    10


#define MDIO0 writew(1<<MDIO_BIT, _P2_RESET_DATA_REG)
#define MDIO1 writew(1<<MDIO_BIT, _P2_SET_DATA_REG)
#endif

/* this funcion will be copied to internal SRAM during boot */
void
__attribute__ ((__section__ (".text.low_power_text"))) 
sc1445x_power_pll_mode(void)
{
   volatile unsigned short creg;
   unsigned int ratio, xtal;
   int flags;
   
   local_irq_save(flags);

   // load power state variables from SRAM
   u16 power_state     = *(volatile unsigned short *) POS_POWER_STATE;
   u16 max_power_state = *(volatile unsigned short *) POS_POWER_MAX;
   u16 min_power_state = *(volatile unsigned short *) POS_POWER_MIN;
   
   u16 old_power_state=power_state;

   if (power_state>max_power_state) power_state = max_power_state;
   if (power_state<min_power_state) power_state = min_power_state;
   
    *(volatile unsigned short *) POS_POWER_STATE = power_state;

    *(volatile u16 *)0x84F2 = 0xCAFE;


#if defined( CONFIG_SC14450 ) 
   
    ratio = 0;
    xtal  = 0;
    if (power_state == 0) {
        ratio = 1;
        xtal  = 0;
    } else if (power_state == 1) {
        ratio = 2;
        xtal  = 0;
    } else if (power_state == 2) {
        ratio = 4;
        xtal  = 0;
    } else if (power_state == 3) {
        ratio = 0;
        xtal  = 1;
    } 

    if (ratio) {

        creg = CLK_AMBA_REG;

        // set amba clock = pll/ratio
        creg &= ~CLK_AMBA_HCLK_MASK;
        creg |=  CLK_AMBA_HCLK_DIV(ratio); //  ratio&0x7;
        creg &= ~CLK_AMBA_HCLK_PRE;  // ~0x200; // HLCK_PRE->0

        CLK_AMBA_REG = creg;

        creg = CLK_PLL1_CTRL_REG;
        CLK_PLL1_CTRL_REG = creg | CLK_PLL1_CTRL_PLL_CLK_SEL;  // set bit 3

        // CLK_CODEC_DIV_REG = 0x00c8;    
        // CLK_PER_DIV_REG   = 0x00c8;    
        // CLK_PER10_DIV_REG = 0x0041;  // PER20 /= 2, PER10 /= 1

        creg = CLK_DSP_REG;
        CLK_DSP_REG = (creg & ~CLK_DSP_DSP2_DIV_MASK & ~CLK_DSP_DSP1_DIV_MASK) | (CLK_DSP_DSP2_DIV(1) | CLK_DSP_DSP1_DIV(1));    // CLK_DSPx_DIV = 1 -> divide by 1
        
        // CLK_CODEC_REG  = 0x2D55;
        
    }

    if (xtal) {
        /* system clock in PLL mode */

        creg  = CLK_AMBA_REG;
        creg &= ~CLK_AMBA_HCLK_MASK;
        creg |= CLK_AMBA_HCLK_DIV(1);
        creg |= CLK_AMBA_HCLK_PRE; // HLCK_PRE->1
        CLK_AMBA_REG = creg;

        creg = CLK_PLL1_CTRL_REG;
        CLK_PLL1_CTRL_REG = creg & ~CLK_PLL1_CTRL_PLL_CLK_SEL; // clear bit 3

        
        // CLK_CODEC_DIV_REG = 0x0089; // test: maybe its not needed at all
        // CLK_PER_DIV_REG   = 0x0089;	
        // CLK_PER10_DIV_REG = 0x0041;


        // CLK_PER10_DIV_REG = 0x0041;
        // CLK_PER10_DIV_REG = 0x0001;     // PER20 /= 8, PER10 /= 1

#if 1        
        creg = CLK_DSP_REG;
        CLK_DSP_REG = creg & ~CLK_DSP_DSP2_DIV_MASK & ~CLK_DSP_DSP1_DIV_MASK;       // CLK_DSPx_DIV = 0 -> divide by 8
        
        /// CLK_CODEC_REG  = 0x0000;
#endif
        local_irq_restore(flags);

        // sleep to SDRAM if instructed
        if ( (*(unsigned short *) POS_POWER_FLAG) == 1) {
        
        
        
            __asm__("di");

            // sc1445x_power_ioctl_audio_close();            

            // CODEC_VREF_REG
            // CODEC_VREF_REG      = 0x285;
            // DSP_MAIN_SYNC1_REG  = 0xf000;
    
            __asm__("loadd	0xff000c,(r5,r4)");
            __asm__("ord	$0x2,(r5,r4)");
            __asm__("stord	(r5,r4),0xff000c");
            __asm__("_low_irq_loop2:");
            __asm__("loadd	0xff000c,(r5,r4)");
            __asm__("andd	$0x800,(r5,r4)");
            __asm__("cmpd	$0x0:s,(r5,r4)");
            __asm__("beq	_low_irq_loop2:");   
          
            __asm__("push $8,r0");
            __asm__("movd $0x8080, (r3,r2)          ");
             __asm__("lprd (r3,r2), intbase          ");
            *(unsigned short *) POS_POWER_FLAG = 3;
            __asm__("eiwait");
            __asm__("nop");
            __asm__("pop $8,r0");
            
        
        }
    }

#elif defined( CONFIG_SC14452 ) 

//    *(volatile u16 *)0x84F2 = power_state;
    ratio = 2;
    xtal  = 0;
    if (power_state == 0) {
//        *(volatile u16 *)0x84F2 = power_state;
        ratio = 2;
        xtal  = 0;
    } else if (power_state == 1) {
//        *(volatile u16 *)0x84F4 = power_state;
        ratio = 4;
        xtal  = 0;
    } else if (power_state == 2) {
//        *(volatile u16 *)0x84F4 = power_state;
        ratio = 8;
        xtal  = 0;
    } else if (power_state == 3) {
//        *(volatile u16 *)0x84F4 = power_state;
        ratio = 4; // 4
        xtal  = 1; // 1
    } 


    creg = CLK_AMBA_REG;
    creg &= ~SW_HCLK_DIV;
    creg |= SW_HCLK_DIV & ratio;
    CLK_AMBA_REG = creg;

    creg = CLK_GLOBAL_REG;

    if (xtal) {
        creg |= SW_SWITCH_CLK;
    } else {
        creg &= ~SW_SWITCH_CLK;
    }

    CLK_GLOBAL_REG = creg;
 
    local_irq_restore(flags);

    if (xtal) {
//        *(volatile u16 *)0x84F2 +=1;

        if ( (*(volatile u16 *) POS_POWER_FLAG) == 1) {
        
            __asm__("di");
         
            __asm__("push $8,r0");
            __asm__("movd $0x8080, (r3,r2)          ");
            __asm__("lprd (r3,r2), intbase          ");
            *(u16 *) POS_POWER_FLAG = 3; // signal main loop
            __asm__("eiwait");
            __asm__("nop");
            __asm__("pop $8,r0");
        
        }
    
    }

#endif

}


/****************************************************************************************************/
/************                  HIGH LEVEL DRIVER FUNCTIONS                                  *********/
/****************************************************************************************************/

static char __user *user_buf;
static int user_buf_length;

static ssize_t sc1445x_power_read(struct file* file, char __user *buf, size_t count, loff_t *ppos)
{

      char sout[20];
      user_buf = buf;
      user_buf_length = 0;

      sprintf(sout,"power state is %d",*(unsigned short *)POS_POWER_STATE);
      
      strcpy(buf,sout);

	  return strlen(sout);
}

/******************************************************* 
	Description: 	Open dev file.

 ********************************************************/
int sc1445x_power_open(struct inode *inode, struct file *filp)
{
   sc1445x_power_struct *dev; /* device information */

   dev = container_of(inode->i_cdev, sc1445x_power_struct, cdev);
   filp->private_data = dev; 	/* for other methods */

//   DBG(DBG_VERBOSE,"SC14450 POWER DEVICE  OPENS WITH FLAGS %d   \n", (filp->f_flags ));

   if (filp->f_flags & O_NONBLOCK){
      // DBG(DBG_VERBOSE, "DEVICE OPENED AS NONBLOCK \n");
   }

   return 0;
}

/******************************************************* 
	Description: 	Close device file.

 ********************************************************/
int sc1445x_power_release(struct inode *inode, struct file *filp)
{
//	DBG(DBG_VERBOSE, "SC14450 POWER DEVICE CLOSED\n");
	return 0;
}

void sc1445x_power_ioctl_set_state(u16 power_state) {

// jump to 0x8500
           *(volatile unsigned short *)POS_POWER_STATE = power_state;
           __asm__("push $0x2,r11\n");
           __asm__("push ra\n");
           __asm__("movd $0x4280:s,(r12,r11)\n");
           __asm__("jal (r12,r11)\n");
           __asm__("pop ra\n");
           __asm__("pop $0x2,r11\n");

}


void sc1445x_power_ioctl_set_initial_state(u16 new_power_state) {
   volatile unsigned short creg;
   unsigned int ratio;


#if defined( CONFIG_SC14450 )
    ratio = 0;
    if (new_power_state == 0) {
        ratio = 1;
    } else if (new_power_state == 1) {
        ratio = 2;
    } else if (new_power_state == 2) {
        ratio = 4;
    } else if (new_power_state == 3) {
        ratio = 0;
    } 

#elif defined( CONFIG_SC14452 )
    ratio = 0;
    if (new_power_state == 0) {
        ratio = 2;
    } else if (new_power_state == 1) {
        ratio = 4;
    } else if (new_power_state == 2) {
        ratio = 8;
    } else if (new_power_state == 3) {
        ratio = 4;
    } 

#endif


#if defined( CONFIG_SC14450 )
 //   DBG(DBG_VERBOSE, "sc1445x_power_ioctl_set_initial_state : Setting power_state to %d...\n",new_power_state);
    // set amba clock = pll/N
    creg = CLK_AMBA_REG;
    creg &= ~0x7;
    creg |= ratio&0x7;
    
    CLK_AMBA_REG = creg;

    // set apb clock = ahb/4;
    creg &= ~0x18;
    CLK_AMBA_REG = creg;

    creg = CLK_PLL1_CTRL_REG;
    creg |=  (1<<11);  // DYN_SW
    creg &= ~(0xF<<7); // clear PLL_DIP_DIV
    creg |=  (0x0<<7); // set   PLL_DIP_DIV
    CLK_PLL1_CTRL_REG = creg|0x8; // set bit 3 (PLL_MODE)

    CLK_CODEC_DIV_REG = 0xc8;    
    CLK_PER_DIV_REG   = 0x9;    // 0xc8;   // 0x0009;     // DIVN clock, div by 1 (no change)
    CLK_PER10_DIV_REG = 0x41;   // 0x48;   // 0x0041;     // PER20 /= 2, PER10_CLK_SEL:DIVN_CLK, PER10 /= 1

    // CLK_DSP_REG       = 0x99;       // // CLK_DSPx_DIV = 1 -> divide by 1   

#elif defined( CONFIG_SC14452 )
   // DBG(DBG_VERBOSE, "sc1445x_power_ioctl_set_initial_state : Setting power_state to %d...\n",new_power_state);
    creg = CLK_AMBA_REG;
    creg &= ~SW_HCLK_DIV; 
    creg |= SW_HCLK_DIV&ratio;
    CLK_AMBA_REG = creg;
#endif

}


void sc1445x_power_ioctl_set_max_state(u16 max_power_state) {
           *(u16 *)POS_POWER_MAX = max_power_state;
         // DBG(DBG_VERBOSE, "Setting new max state : %d \n", max_power_state);
          return;
}

void sc1445x_power_ioctl_set_min_state(u16 min_power_state) {
    *(u16 *)POS_POWER_MIN = min_power_state;
   // DBG(DBG_VERBOSE, "Setting new min state : %d \n", min_power_state);
    return;
}

#define CODEC_VREF_REG_REFINT_PD   (1<<9)
#define CODEC_VREF_REG_AGND_LSR_PD (1<<8)

#define CODEC_VREF_REG_BIAS_PD     (1<<7)
#define CODEC_VREF_REG_VREF_BG_PD  (1<<6)
#define CODEC_VREF_REG_AMP1V5_PD   (1<<5)
#define CODEC_VREF_REG_VREF_INIT   (1<<4)

#define CODEC_VREF_REG_VREF_FILT_CADJ  (1<<2)
#define CODEC_VREF_REG_VREF_FILT_PD    (1<<1)
#define CODEC_VREF_REG_VREF_PD         (1<<0)

inline void sc1445x_power_ioctl_audio_close(void) {

#if defined( CONFIG_SC14450 )
    
  //  DBG(DBG_VERBOSE, "sc1445x_power_ioctl_audio_close : CODEC_VREF_REG      = %X\n", CODEC_VREF_REG);
  //  DBG(DBG_VERBOSE, "sc1445x_power_ioctl_audio_close : DSP_MAIN_SYNC1_REG  = %X\n", DSP_MAIN_SYNC1_REG);
    
    CODEC_VREF_REG      = 0 
                          | CODEC_VREF_REG_REFINT_PD
                          | CODEC_VREF_REG_AGND_LSR_PD 
                          | CODEC_VREF_REG_BIAS_PD
                          | CODEC_VREF_REG_VREF_BG_PD
                          | CODEC_VREF_REG_AMP1V5_PD
                          | CODEC_VREF_REG_VREF_INIT
                          | CODEC_VREF_REG_VREF_FILT_CADJ
                          | CODEC_VREF_REG_VREF_FILT_PD
                          | CODEC_VREF_REG_VREF_PD
                          ;
                            // 0x285;
    DSP_MAIN_SYNC1_REG  = 0xf000;            
#elif defined( CONFIG_SC14452 ) 
  //  DBG(DBG_VERBOSE, "sc1445x_power_ioctl_audio_close\n");


//    *(volatile unsigned short*)0x12a02 = 0;
//    *(volatile unsigned short*)0x12a00 = 6;

    CODEC_VREF_REG      = 0 
                          | CODEC_VREF_REG_REFINT_PD
                          | CODEC_VREF_REG_AGND_LSR_PD 
                          | CODEC_VREF_REG_BIAS_PD
                          | CODEC_VREF_REG_VREF_BG_PD
                          | CODEC_VREF_REG_AMP1V5_PD
                          | CODEC_VREF_REG_VREF_INIT
                          | CODEC_VREF_REG_VREF_FILT_CADJ
                          | CODEC_VREF_REG_VREF_FILT_PD
                          | CODEC_VREF_REG_VREF_PD
                          ;

    CLASSD_CTRL_REG |= CLASSD_PD;
    

    // CLK_SPU1_REG    &= ~(1<<12);
    
#endif

}

inline void sc1445x_power_ioctl_audio_open(void) {
    unsigned short creg;

    volatile unsigned int delay;


#if defined( CONFIG_SC14450 )

  //  DBG(DBG_VERBOSE, "sc1445x_power_ioctl_audio_open : CODEC_VREF_REG      = %X\n", CODEC_VREF_REG);
  //  DBG(DBG_VERBOSE, "sc1445x_power_ioctl_audio_open : CLK_DSP_REG  = %X\n", CLK_DSP_REG);
    // CODEC_VREF_REG  = 0x4;
    CODEC_VREF_REG      = 0 
                      | CODEC_VREF_REG_VREF_INIT
                      | CODEC_VREF_REG_VREF_FILT_CADJ
                      ;

    delay = 80*200; while(delay--); // wait at least 100 us
    CODEC_VREF_REG      = 0 
                      | CODEC_VREF_REG_VREF_FILT_CADJ
                      ;

    creg = CLK_DSP_REG;
    CLK_DSP_REG = (creg & ~0x77) | 0x11;       // CLK_DSPx_DIV = 1 -> divide by 1

#elif defined(CONFIG_SC14452) 

  //  DBG(DBG_VERBOSE, "sc1445x_power_ioctl_audio_close\n");

    
    CODEC_VREF_REG      = 0 
                      | CODEC_VREF_REG_VREF_INIT
                      | CODEC_VREF_REG_VREF_FILT_CADJ
                      ;

    delay = 80*20; while(delay--); // wait at least 100 us

    CODEC_VREF_REG      = 0 
                      | CODEC_VREF_REG_VREF_FILT_CADJ
                      ;

    CLASSD_CTRL_REG &= ~CLASSD_PD;  // power up

//    *(volatile unsigned short*)0x12a02 = 1;
//    *(volatile unsigned short*)0x12a00 = 6;


    // CLK_SPU1_REG    |= (1<<12);

#endif

}


inline void sc1445x_power_ioctl_display_close(void) {

#if defined( CONFIG_SC14450_VoIP_RevB ) 
  //  DBG(DBG_VERBOSE, "sc1445x_power_ioctl_display_close\n");
    CP_CTRL_REG = 0x10; // disable charge pump for display
#endif

#if defined( CONFIG_SC14452_DK_REV_A ) || defined( CONFIG_SC14452_DK_REV_C ) 
  //  DBG(DBG_VERBOSE, "sc1445x_power_ioctl_display_close\n");
    P2_SET_DATA_REG |= 0x800; 
#endif

}

inline void sc1445x_power_ioctl_display_open(void) {

#if defined( CONFIG_SC14450_VoIP_RevB ) 
    CP_CTRL_REG = 0x13; // enable charge pump for display
#endif

#if defined(CONFIG_SC14452_DK_REV_A) || defined( CONFIG_SC14452_DK_REV_C ) 
    P2_RESET_DATA_REG |= 0x800; 
#endif

}


/******************************************************* 
	Description: 	sc1445x_power_ioctl.

 ********************************************************/

int sc1445x_power_ioctl(struct inode *inode, struct file *filp,
                 unsigned int cmd, unsigned long arg)
{
    
	int retval=0;
    unsigned short delay_val;
    
    if (cmd == SC1445x_POWER_IOCTL_GET_STATE) {
        *(unsigned long *)arg = *(unsigned short *)POS_POWER_STATE; // power_state;
    } else if (cmd == SC1445x_POWER_IOCTL_GET_MAX) {
        *(unsigned long *)arg = *(unsigned short *)POS_POWER_MAX; // max power_state;
    } else if (cmd == SC1445x_POWER_IOCTL_SET_STATE) {
	    power_state = *(unsigned int *)arg;
      //  DBG(DBG_VERBOSE, "Setting new power state = %d\n",power_state);
        if        (power_state == SC1445x_POWER_SPEED_80){
            sc1445x_power_ioctl_set_state(0);
        } else if (power_state == SC1445x_POWER_SPEED_40){
            sc1445x_power_ioctl_set_state(1);
        } else if (power_state == SC1445x_POWER_SPEED_20){
            sc1445x_power_ioctl_set_state(2);
        } else if (power_state == SC1445x_POWER_SPEED_XTAL){
            sc1445x_power_ioctl_set_state(3);
        }
    } else if (cmd == SC1445x_POWER_IOCTL_SET_MAX) {
	    power_state = *(unsigned int *)arg;
      //  DBG(DBG_VERBOSE, "Setting new max power state = %d\n",power_state);
        if        (power_state == SC1445x_POWER_SPEED_80){
            sc1445x_power_ioctl_set_max_state(0);
        } else if (power_state == SC1445x_POWER_SPEED_40){
            sc1445x_power_ioctl_set_max_state(1);
        } else if (power_state == SC1445x_POWER_SPEED_20){
            sc1445x_power_ioctl_set_max_state(2);
        } else if (power_state == SC1445x_POWER_SPEED_XTAL){
            sc1445x_power_ioctl_set_max_state(3);
        }
    } else if (cmd == SC1445x_POWER_IOCTL_SET_MIN) {
	    power_state = *(unsigned int *)arg;
      //  DBG(DBG_VERBOSE, "Setting new min power state = %d\n",power_state);
        if        (power_state == SC1445x_POWER_SPEED_80){
            sc1445x_power_ioctl_set_min_state(0);
        } else if (power_state == SC1445x_POWER_SPEED_40){
            sc1445x_power_ioctl_set_min_state(1);
        } else if (power_state == SC1445x_POWER_SPEED_20){
            sc1445x_power_ioctl_set_min_state(2);
        } else if (power_state == SC1445x_POWER_SPEED_XTAL){
            sc1445x_power_ioctl_set_min_state(3);
        }
    } else if (cmd == SC1445x_POWER_IOCTL_AUDIO_CLOSE) {
        sc1445x_power_dev.audio_enable=0;
        sc1445x_power_dev.audio_delay=sc1445x_power_dev.audio_delay_val;
    } else if (cmd == SC1445x_POWER_IOCTL_AUDIO_OPEN) {
        sc1445x_power_dev.audio_enable=1;
        sc1445x_power_ioctl_audio_open();
    } else if (cmd == SC1445x_POWER_IOCTL_DISPLAY_CLOSE) {
        sc1445x_power_dev.display_enable=0;
        sc1445x_power_dev.display_delay=sc1445x_power_dev.display_delay_val;
    } else if (cmd == SC1445x_POWER_IOCTL_DISPLAY_OPEN) {
        sc1445x_power_dev.display_enable=1;
        sc1445x_power_ioctl_display_open();
    } else if (cmd == SC1445x_POWER_IOCTL_AUDIO_DELAY) {
	    delay_val = *(unsigned short *)arg;
      //  DBG(DBG_VERBOSE, "Setting audio delay to %d\n",delay_val);
        sc1445x_power_dev.audio_delay_val = (delay_val>1) ? delay_val : 1;
    } else if (cmd == SC1445x_POWER_IOCTL_DISPLAY_DELAY) {
	    delay_val = *(unsigned short *)arg;
      //  DBG(DBG_VERBOSE, "Setting display delay to %d\n",delay_val);
        sc1445x_power_dev.display_delay_val = (delay_val>1) ? delay_val : 1;
    } else if (cmd == SC1445x_POWER_IOCTL_ENABLE_PORT2) {
       // DBG(DBG_VERBOSE, "Enabling second network port \n");
        sc1445x_power_dev.net_enable_port2 = 1;
    } else if (cmd == SC1445x_POWER_IOCTL_DISABLE_PORT2) {
       // DBG(DBG_VERBOSE, "Disabling second network port \n");
        sc1445x_power_dev.net_enable_port2 = 0;
    } else if (cmd == SC1445x_POWER_IOCTL_GET_NET_PORT2_STATE) {
        *(unsigned long *)arg = sc1445x_power_dev.net_enable_port2;
    } else
      //  DBG(DBG_VERBOSE, "SC1445x_POWER_IOCTL: Error\n");

	return retval;
}

int sc1445x_power_netport2_state(){
    return (int) sc1445x_power_dev.net_enable_port2;
}

/******************************************************* 
	Description: 	Module initialization.

 ********************************************************/
int __init sc1445x_power_init_module(void)
{
	int result;
    int i;
	dev_t dev = 0;

	/* Initializations*/
//	DBG(DBG_VERBOSE, "module init function, entry point\n");

/*
 * Get a range of minor numbers to work with, asking for a dynamic
 * major unless directed otherwise at load time.
 */
 
	if (sc1445x_power_major) {
		dev = MKDEV(sc1445x_power_major, sc1445x_power_minor);
		result = register_chrdev_region(dev, 1, "power");
	} 
	else {
		result = alloc_chrdev_region(&dev, sc1445x_power_minor, 1, "power");
		sc1445x_power_major = MAJOR(dev);
	}
	if (result < 0) {
	//	DBG(DBG_VERBOSE, KERN_WARNING "sc1445x_power: can't get major %d\n", sc1445x_power_major);
		return result;
	}

	/*Initialize driver internal vars*/
	memset((char*)&sc1445x_power_dev, 0, sizeof(sc1445x_power_struct));	
	sc1445x_power_dev.ver_major=SC1445x_POWER_MAJ_VER;
	sc1445x_power_dev.ver_minor=SC1445x_POWER_MIN_VER;


    /* Initialize device. */
	sc1445x_power_setup_cdev(&sc1445x_power_dev);	

    // setup timer
	init_timer(&sc1445x_power_dev.watchdog);
	sc1445x_power_dev.watchdog.function = &sc1445x_power_watchdog;
	sc1445x_power_dev.watchdog.expires = jiffies + 20*HZ;
	sc1445x_power_dev.watchdog.data = 0;
	add_timer(&sc1445x_power_dev.watchdog);

    sc1445x_power_dev.audio_enable=1;//0;
    sc1445x_power_dev.display_enable=1;//0;

    sc1445x_power_dev.audio_delay=1;
    sc1445x_power_dev.display_delay=1;

    sc1445x_power_dev.audio_delay_val=5;
    sc1445x_power_dev.display_delay_val=10;

    sc1445x_power_dev.net_enable_port2=0;

    // initialize alternate interrupt vector table
    for(i=0;i<32;i++){
        *(unsigned long *)(0x8080+4*i) = 0x4380+8*i;
    }

    sc1445x_power_ioctl_set_max_state(0);
    sc1445x_power_ioctl_set_min_state(0);
    
    *(volatile unsigned short *) POS_POWER_FLAG = 2;


#if 0 // enable when HW modifications have been applied
    
    BAT_CTRL_REG = (BAT_CTRL_REG & ~0xF00) | 0x700;
    printk ("BAT_CTRL_REG = %X\n", BAT_CTRL_REG);

    // BAT_CTRL2_REG
    BAT_CTRL2_REG = 0x1C5E;

#endif

    sc1445x_power_ioctl_set_initial_state(0);

	printk(KERN_NOTICE "sc1445x_power: driver initialized with major devno: %d\n", sc1445x_power_major);

	return 0; /* succeed */

}

/******************************************************* 
	Description: 	Cleanup the module and handle initialization failures .

 ********************************************************/
void sc1445x_power_cleanup_module(void)
{
	dev_t devno = MKDEV(sc1445x_power_major, sc1445x_power_minor);

	/* Get rid of our char dev entries */
	cdev_del(&sc1445x_power_dev.cdev);
    del_timer(&sc1445x_power_dev.watchdog);

	printk(KERN_NOTICE "sc1445x_power: cleanup module\n");
	/* cleanup_module is never called if registering failed */
	unregister_chrdev_region(devno, 1);

}

/******************************************************* 
	Description: 	Set up the cdev structure for this device.

 ********************************************************/
void sc1445x_power_setup_cdev(sc1445x_power_struct *dev)
{
	int err, devno = MKDEV(sc1445x_power_major, sc1445x_power_minor);
    
	cdev_init(&dev->cdev, &sc1445x_power_fops);
	dev->cdev.owner = THIS_MODULE;
	dev->cdev.ops = &sc1445x_power_fops;
	err = cdev_add (&dev->cdev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE "Error %d adding sc1445x_power", err);
}

module_init(sc1445x_power_init_module);
module_exit(sc1445x_power_cleanup_module);





#define SRAM_IRQ_NAME2(nr) nr##_interrupt(void)
#define SRAM_IRQ_NAME(nr) SRAM_IRQ_NAME2(SRAM_IRQ##nr)

#define BUILD_SRAM_IRQ(nr) \
void __attribute__ ((__section__ (".text.low_power_IRQs"))) SRAM_IRQ_NAME(nr); \
    __asm__(".section .text.low_power_IRQs"); \
    __asm__(".global _SRAM_IRQ" #nr "_interrupt"); \
    __asm__("_SRAM_IRQ" #nr "_interrupt: "); \
    __asm__("di"); \
    __asm__("movd $" #nr ",(r1,r0)"); \
    __asm__("movd $0x4480,(r3,r2)"); \
    __asm__("jump (r3,r2)"); \
    __asm__("nop"); \
    __asm__("nop"); \

    BUILD_SRAM_IRQ(0x10000)
    BUILD_SRAM_IRQ(0x10001)
    BUILD_SRAM_IRQ(0x10002)
    BUILD_SRAM_IRQ(0x10003)
    BUILD_SRAM_IRQ(0x10004)
    BUILD_SRAM_IRQ(0x10005)
    BUILD_SRAM_IRQ(0x10006)
    BUILD_SRAM_IRQ(0x10007)
    BUILD_SRAM_IRQ(0x10008)
    BUILD_SRAM_IRQ(0x10009)
    BUILD_SRAM_IRQ(0x1000a)
    BUILD_SRAM_IRQ(0x1000b)
    BUILD_SRAM_IRQ(0x1000c)
    BUILD_SRAM_IRQ(0x1000d)
    BUILD_SRAM_IRQ(0x1000e)
    BUILD_SRAM_IRQ(0x1000f)
    BUILD_SRAM_IRQ(0x10010)
    BUILD_SRAM_IRQ(0x10011)
    BUILD_SRAM_IRQ(0x10012)
    BUILD_SRAM_IRQ(0x10013)
    BUILD_SRAM_IRQ(0x10014)
    BUILD_SRAM_IRQ(0x10015)
    BUILD_SRAM_IRQ(0x10016)
    BUILD_SRAM_IRQ(0x10017)
    BUILD_SRAM_IRQ(0x10018)
    BUILD_SRAM_IRQ(0x10019)
    BUILD_SRAM_IRQ(0x1001a)
    BUILD_SRAM_IRQ(0x1001b)
    BUILD_SRAM_IRQ(0x1001c)
    BUILD_SRAM_IRQ(0x1001d)
    BUILD_SRAM_IRQ(0x1001e)
    BUILD_SRAM_IRQ(0x1001f)

void
__attribute__ ((__section__ (".text.low_power_IRQs"))) 
sc1445x_power_IRQs(void)
{
    __asm__("_low_power_IRQ_common:         ");
    

#if defined( CONFIG_SC14452 ) 

     // ASAP : SW_SWITCH_CLK -> 1 (PLL)
    __asm__("loadd	0xff4000:m,(r5,r4)");
    __asm__("andd	$0xffffffdf:l,(r5,r4)");
    __asm__("stord	(r5,r4),0xff4000:l");

#else

    // clear self refresh
    // EBI_SDCTLR_REG &= ~(1<<1);
    // while (EBI_SDCTLR_REG & (1<<11));

    __asm__("loadd	0xff000c:m,(r5,r4)");
    __asm__("andd	$0xfffffffd:l,(r5,r4)");
    __asm__("stord	(r5,r4),0xff000c:l");
    
    __asm__("_low_irq_loop:");
    __asm__("loadd	0xff000c:m,(r5,r4)");
    __asm__("andd	$0x800:l,(r5,r4)");
    __asm__("cmpd	$0x0:s,(r5,r4)");
    __asm__("bne	_low_irq_loop:"); 

#endif

    // jump to original setvice routine
    __asm__("movd $0x8000, (r3,r2)          ");
    __asm__("lprd (r3,r2), intbase          ");
    __asm__("movw $0,r1"); \
    __asm__("lshd $2,(r1,r0)                ");
    __asm__("loadd  0x8000(r1,r0),(r3,r2)   ");
    __asm__("jump (r3,r2)                   ");

}

