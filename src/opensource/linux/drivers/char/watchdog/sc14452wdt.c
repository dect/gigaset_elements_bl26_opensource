// File created by Gigaset Elements GmbH
// All rights reserved.
/*
 *	Watchdog driver for SC14452 platform.
 *
 *	    Based on acquirewdt.c
 *
 *
 *  Copyright (C) 2013 Gigaset Elements GmbH
 *  Author:
 *      Piotr L. Figlarek (piotr.figlarek@gigaset.com)
 */

/*
 * Details:
 *  Watchdog uses the maximal time interval for watchdog timer (approx. 2,56 sec) and this value is not editable.
 *  Because of this timeout another one timer was involved to kick the watchdog.
 *  Now, every kick from USERSPACE generate 40 (defined by WATCHDOG_TIMER_KICKS) kicks in the 500ms. intervals.
 *
 *  As mentioned in documentation:
 *  "Watchdog Timer generates hardware Reset signal and watchdog cannot be frozen.
 *   Note that this bit can only be set to 1 by SW and only be reset with a hardware reset."
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/miscdevice.h>
#include <linux/watchdog.h>
#include <linux/fs.h>
#include <linux/ioport.h>
#include <linux/notifier.h>
#include <linux/reboot.h>
#include <linux/init.h>
#include <linux/timer.h>

#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/system.h>

#include <asm-cr16/regs_sc14452.h>

#define WATCHDOG_RESET_VALUE    0xFF
#define WATCHDOG_TIMER_KICKS    40

#if defined CONFIG_SC14452WDT_NMI
int nmi_counter;
EXPORT_SYMBOL(nmi_counter);
#endif


static struct timer_list wdt_timer;
static int timer_kick = WATCHDOG_TIMER_KICKS;



/*
 * Kick the watchdog
 */
static void wdt_kick(void)
{
    WATCHDOG_REG = WATCHDOG_RESET_VALUE;

#if defined CONFIG_SC14452WDT_NMI
    nmi_counter = CONFIG_SC14452WDT_NMI_TOLERANCE;
#endif
}


/*
 * Callback for WDT timer.
 */
static void wdt_timer_callback(unsigned long data)
{
    if( timer_kick )
    {
       wdt_kick();  // kick the watchdog

       --timer_kick;

       mod_timer(&wdt_timer, jiffies + msecs_to_jiffies(500));  // restart timer
    }
}

/*
 * Watchdog trigger function (for userspace).
 */
static ssize_t wdt_write(struct file *file, const char __user *buf, size_t count, loff_t *ppos)
{
    wdt_kick();                         // kick the watchdog
    timer_kick = WATCHDOG_TIMER_KICKS;  // reset internal_kick value

    if( timer_pending(&wdt_timer) != 1 ) // if timer is not running
        mod_timer(&wdt_timer, jiffies + msecs_to_jiffies(500));  // restart timer

	return count;
}

/*
 * Open watchodog device.
 * Watchdog starts immediately!
 */
static int wdt_open(struct inode *inode, struct file *file)
{
	// reset watchdog timer
	WATCHDOG_REG = WATCHDOG_RESET_VALUE;
    printk("Watchdog is working and will generate ");

#if defined CONFIG_SC14452WDT_NMI
    printk ("NMI");
    TIMER_CTRL_REG &= ~WDOG_CTRL;   // configure to generate NMI
    RESET_FREEZE_REG |= FRZ_WDOG;   // do magic undocumented stu
#else
    printk ("reset");
    TIMER_CTRL_REG |= WDOG_CTRL;    // configure watchdog to generate reset not NMI
#endif

	printk(".\n");
	return 0;
}

/*
 * Close watchdog device.
 * Please remember, that it is impossible to stop running watchdog, so system will be rebooted quickly.
 */
static int wdt_close(struct inode *inode, struct file *file)
{
	return 0;
}

/*
 *	Kernel Interfaces
 */
static const struct file_operations wdt_fops =
{
	.owner = THIS_MODULE,
	.write = wdt_write,
	.open = wdt_open,
	.release = wdt_close
};

static struct miscdevice wdt_miscdev =
{
	.minor = WATCHDOG_MINOR,
	.name = "watchdog",
	.fops = &wdt_fops
};

/*
 * Initialize watchdog driver.
 */
static int __init wdt_init(void)
{
	int ret;

	printk(KERN_INFO "Initializing watchdog driver for SC14452 ...\n");

	ret = misc_register(&wdt_miscdev);
	if (ret != 0) {
		printk (KERN_ERR "Can't register watchdog device (err=%d)\n", ret);
		return ret;
	}

	// initialize watchdog timer
	setup_timer(&wdt_timer, wdt_timer_callback, 0);

	printk (KERN_INFO "Watchdog initialized, but not started.\n");

	return 0;
}

/*
 * Remove watchdog driver.
 *
 * On SC14552 platform it is impossible to disable running watchdog. So in case when:
 * 1. driver was loaded,
 * 2. watchdog was started,
 *
 * it is possible to unload driver, but not possible to stop the watchdog!
 * Reset in this case is expected.
 */
static void __exit wdt_exit(void)
{
    del_timer(&wdt_timer);
	misc_deregister(&wdt_miscdev);
}


module_init(wdt_init);
module_exit(wdt_exit);

MODULE_AUTHOR("Piotr L. Figlarek");
MODULE_DESCRIPTION("Watchdog driver for SC14452 microcontroller");
MODULE_LICENSE("GPL");
MODULE_ALIAS_MISCDEV(WATCHDOG_MINOR);
