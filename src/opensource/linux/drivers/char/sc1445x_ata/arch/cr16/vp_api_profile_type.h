/** \file vp_api_profile_type.h
 * vp_api_profile_type.h
 *
 * Header file for the VpProfileDataType typedef.  If using the API "apitypes",
 * this file is automatically included.  Otherwise, this type is defined in the
 * "profiles8.h" and "profiles16.h" files (app should include only one).
 *
 * Copyright (c) 2010, Zarlink Semiconductor, Inc.
 */

#ifndef API_PROFILE_TYPE_H
#define API_PROFILE_TYPE_H

typedef unsigned char VpProfileDataType;

#endif





