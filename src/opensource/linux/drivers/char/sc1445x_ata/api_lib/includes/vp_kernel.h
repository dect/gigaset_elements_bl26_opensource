/*
 * vp_kernel.h
 *
 * This file declares the VCP Host Bus Interface layer.
 *
 * Copyright (c) 2008, Zarlink Semiconductor, Inc.
 */


#ifndef _VP_KERNEL_H
#define _VP_KERNEL_H

#ifndef	VP_KERNEL_MAX_NUM_LINES
  #define VP_KERNEL_MAX_NUM_LINES         (32)    /* Maximum lines */
#endif /* VP_VCP_MAX_NUM_LINES */

/* Header files required for open() */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
/* Header file required for close() */
#include <unistd.h>
/* Header file required for ioctl() */
#include <sys/ioctl.h>
//#include <errno.h>

/* Kernel specific Device Object */
typedef struct {
    VpDeviceIdType  deviceId;       /* Device Id indication */

    int16           fileDes;        /* File descriptor */

    uint8           maxChannels;    /* Static Information */

} VpKWrapDeviceObjectType;

/* Kernel specific Line Object */
typedef struct {
    uint8           channelId;

    VpTermType      termType;

    VpLineIdType    lineId;     /* Application provided value for mapping a
                                 * line to a line context */
} VpKWrapLineObjectType;

/******************************************************************************
 *                        Kernel Specific FUNCTION PROTOTYPES                 *
******************************************************************************/
#if defined (VP_CC_KWRAP)
VpStatusType
VpMakeKWrapDeviceObject(
    VpDevCtxType            *pDevCtx,
    VpKWrapDeviceObjectType *pDevObj);

VpStatusType
VpMakeKWrapDeviceCtx(
    VpDevCtxType *pDevCtx,
    VpKWrapDeviceObjectType *pDevObj);

#endif /* VP_CC_KWRAP */
#endif /* _VP_KERNEL_H */
