/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Ganges 
Module name  : LINUX-SDIO driver
File Name    : ganges_linux_main.c

File Description:
This file contains all the LINUX network device specific code. 

List of functions:
    ganges_allocdev
    ganges_registerdev
    ganges_unregisterdev
    ganges_get_stats
    ganges_open
    ganges_stop
    ganges_netdevice_op
    ganges_sdio_module_init
    ganges_sdio_module_exit

Author :
  
Rev History:
Sl  By    date change details
---------------------------------------------------------
1   Fariya 
---------------------------------------------------------
*END*************************************************************************/

#include<linux/kernel.h>
#include<linux/module.h>

#include "ganges_linux.h"
#include <linux/spi/spi.h>

GANGES_EXTERN struct iw_handler_def ganges_handler_def;
GANGES_EXTERN UINT32 tcp_csumoffld_enable;

/*FUNCTION*********************************************************************
Function Name  : ganges_allocdev
Description    : Allocate & initialize the network device.
	         This function allocates memory for the network device &
		 initializes it with ethernet generic values 
Returned Value : Pointer to the network device structure is returned 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
sizeof_priv      |  X  |     |     | size of the priv area to be allocated
*END**************************************************************************/

struct net_device* 
ganges_allocdev
  (
    INT32 sizeof_priv
  )
{	
  struct net_device *dev = NULL;
  dev = alloc_netdev(sizeof_priv,"wlan0",ether_setup);
  return dev;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_registerdev
Description    : Register the network device
Returned Value : On success 0 is returned else a negative value signifying 
		 failure  
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | pointer to our network device structure
*END**************************************************************************/

INT32 
ganges_registerdev
  (
    struct net_device *dev
  )
{
  return register_netdev(dev);
}

/*FUNCTION*********************************************************************
Function Name  : ganges_unregisterdev
Description    : Unregisters the network device & returns it back to the kernel
Returned Value : None
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              | X   |     |     | pointer to our network device structure
*END**************************************************************************/

VOID 
ganges_unregisterdev
  (
    struct net_device *dev
  )
{
  unregister_netdev(dev);
  free_netdev(dev);
  return;
}

/*FUNCTION**********************************************************************
Function Name:  ganges_get_stats
Description:    This function gives the statistical information regarding 
	        the interface
Returned Value: Pointer to the net_device_stats structure 
Parameters    : 

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | pointer to our network device structure
*******************************************************************************/

struct net_device_stats*
ganges_get_stats
  (
    struct net_device *dev
  )
{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);
  return &Adapter->stats;
}

/*FUNCTION**********************************************************************
Function Name:  ganges_open
Description:    This function opens the interface
Returned Value: On success a value of 0 is returned, else a negative number
Parameters    :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | pointer to the network device structure
*******************************************************************************/

INT32 
ganges_open
  (
    struct net_device *dev
  )
{
  RSI_DEBUG(RSI_ZONE_INIT,"ganges_open:\n");      
  return 0;    
}

/**************************************************************************** 
Function Name:  ganges_stop
Description:    This function stops/brings down the interface 
Returned Value: On success a value of 0 is returned, else a negative number
Parameters    :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | pointer to our network device structure
*******************************************************************************/

INT32 
ganges_stop
  (
    struct net_device *dev
  )
{  	
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_stop\n");	
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_netdevice_op
Description    : This function performs all net device related operations like                 
	         allocating,initializing and registering the netdevice. 
Returned Value : Returns a ptr to the allocated net device
Parameters     : 

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pInterface       |     |     |     | Pointer to the card interface
dev		 |     |     |  X  | Pointer to net device structure
*END**************************************************************************/

struct net_device*
ganges_netdevice_op
  (
    VOID
  )
{    
  struct net_device *dev; 

  /*Allocate & initialize the network device structure*/  
  dev = ganges_allocdev(sizeof(RSI_ADAPTER));    
    
  if(dev == NULL)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              "ganges_netdevice_op: Failure in allocation of net-device\n");
    return dev;
  }

  dev->open                 = ganges_open;
  dev->stop                 = ganges_stop;
  dev->hard_start_xmit      = ganges_xmit;
  dev->get_stats            = ganges_get_stats;   
  dev->do_ioctl             = ganges_ioctl;
  dev->hard_header_len      = 30;
  dev->wireless_handlers    = &ganges_handler_def;  

  if(tcp_csumoffld_enable)
    dev->features          |= NETIF_F_IP_CSUM;

  if(ganges_registerdev(dev)!=0)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              "ganges_netdevice_op: Registration of net-device failed\n");
    free_netdev(dev);
    return NULL;      
  }
  
  return dev;

}
extern struct spi_driver rps_driver;
/*FUNCTION**********************************************************************
Function Name:  ganges_sdio_module_init
Description:    This function is invoked when the module is loaded into the 
                kernel. It registers the client driver.            
Returned Value: 0 in case of success or a negative value signifying failure
Paramaters: 
----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
None 
*******************************************************************************/

GANGES_STATIC INT32 __init 
ganges_sdio_module_init
  (
    VOID
  )
{
  UINT32 ret_val;
  ret_val =spi_register_driver(&rps_driver);
  RSI_DEBUG(RSI_ZONE_INIT,"The Return Value is %d\n",ret_val);
  return 0; 
}

/*FUNCTION*********************************************************************
Function Name:  ganges_sdio_module_exit
Description:    At the time of removing/unloading the module, this function is 
                called. It unregisters the client driver.    
Returned Value: None
Parameters: 
----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
None
*******************************************************************************/

GANGES_STATIC VOID __exit 
ganges_sdio_module_exit
  (
    VOID
  )
{
  /*Unregistering the client driver*/	
  if(ganges_disconnect()==0)
  {
     RSI_DEBUG(RSI_ZONE_INFO,
               "ganges_sdio_module_exit: Unregistered the client driver\n");
  }
  else    
  {
     RSI_DEBUG(RSI_ZONE_ERROR,
               "ganges_sdio_module_exit: Unable to unregister client driver\n");
  }
  return;    
}

MODULE_LICENSE("GPL");
module_init(ganges_sdio_module_init);
module_exit(ganges_sdio_module_exit);
