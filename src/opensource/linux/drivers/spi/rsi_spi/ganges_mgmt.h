/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Ganges
Module name  : LINUX driver
File Name    : ganges_mgmt.h

File Description:
        Management frames portion header file
List of functions:

Author :

Rev History:
Sl By date change details
---------------------------------------------------------
01.                   created

---------------------------------------------------------
*END**************************************************************************/

#ifndef __GANGES_MGMT_H__
#define __GANGES_MGMT_H__

#include "common.h"

/* While changing this change in channel_prog_val.h also */
#define STD_RATE_54   0x6c
#define STD_RATE_48   0x60
#define STD_RATE_36   0x48
#define STD_RATE_24   0x30
#define STD_RATE_18   0x24
#define STD_RATE_12   0x18
#define STD_RATE_11   0x16
#define STD_RATE_09   0x12
#define STD_RATE_06   0x0C
#define STD_RATE_5_5  0x0B
#define STD_RATE_02   0x04
#define STD_RATE_01   0x02
#define STD_RATE_00   0x00



#define IP_TOS_DSCP_0x38_AC_VO 0x38      /*Voice,                802.1d = 7 */
#define IP_TOS_DSCP_0x30_AC_VO 0x30      /*Voice,                802.1d = 6 */
#define IP_TOS_DSCP_0x28_AC_VI 0x28      /*Video,                802.1d = 5 */
#define IP_TOS_DSCP_0x20_AC_VI 0x20      /*Video,                802.1d = 4 */
#define IP_TOS_DSCP_0x18_AC_BE 0x18      /*Best effort,  802.1d = 3 */
#define IP_TOS_DSCP_0x10_AC_BK 0x10      /*Back ground,  802.1d = 2 */
#define IP_TOS_DSCP_0x08_AC_BK 0x08      /*Back ground,  802.1d = 1 */
#define IP_TOS_DSCP_0x00_AC_BE 0x00      /*Best effort,  802.1d = 0 */

#define MIN_THRESHOLD 64
#define MAX_THRESHOLD 2300

/*HT rate values */
#define RSI_RATE_1    0x0
#define RSI_RATE_2    0x2
#define RSI_RATE_5_5  0x4
#define RSI_RATE_11   0x6
#define RSI_RATE_6    0x4b
#define RSI_RATE_9    0x4f
#define RSI_RATE_12   0x4a
#define RSI_RATE_18   0x4e
#define RSI_RATE_24   0x49
#define RSI_RATE_36   0x4d
#define RSI_RATE_48   0x48
#define RSI_RATE_54   0x4c
#define RSI_RATE_MCS0 0x80
#define RSI_RATE_MCS1 0x81
#define RSI_RATE_MCS2 0x82
#define RSI_RATE_MCS3 0x83
#define RSI_RATE_MCS4 0x84
#define RSI_RATE_MCS5 0x85
#define RSI_RATE_MCS6 0x86
#define RSI_RATE_MCS7 0x87

#define RPS_RATE_MCS7 RSI_RATE_MCS7
#define RPS_RATE_MCS6 RSI_RATE_MCS6
#define RPS_RATE_MCS5 RSI_RATE_MCS5
#define RPS_RATE_MCS4 RSI_RATE_MCS4
#define RPS_RATE_MCS3 RSI_RATE_MCS3
#define RPS_RATE_MCS2 RSI_RATE_MCS2
#define RPS_RATE_MCS1 RSI_RATE_MCS1
#define RPS_RATE_MCS0 RSI_RATE_MCS0
#define RPS_RATE_54   RSI_RATE_54
#define RPS_RATE_48   RSI_RATE_48
#define RPS_RATE_36   RSI_RATE_36
#define RPS_RATE_24   RSI_RATE_24
#define RPS_RATE_18   RSI_RATE_18
#define RPS_RATE_12   RSI_RATE_12
#define RPS_RATE_11   RSI_RATE_11
#define RPS_RATE_09   RSI_RATE_9
#define RPS_RATE_06   RSI_RATE_6
#define RPS_RATE_5_5  RSI_RATE_5_5
#define RPS_RATE_02   RSI_RATE_2
#define RPS_RATE_01   RSI_RATE_1
#define RPS_RATE_00   0x00

extern UINT8 RPS_RATE_MCS7_PWR;
extern UINT8 RPS_RATE_MCS6_PWR;
extern UINT8 RPS_RATE_MCS5_PWR;
extern UINT8 RPS_RATE_MCS4_PWR;
extern UINT8 RPS_RATE_MCS3_PWR;
extern UINT8 RPS_RATE_MCS2_PWR;
extern UINT8 RPS_RATE_MCS1_PWR;
extern UINT8 RPS_RATE_MCS0_PWR;
extern UINT8 RPS_RATE_54_PWR;
extern UINT8 RPS_RATE_48_PWR;
extern UINT8 RPS_RATE_36_PWR;
extern UINT8 RPS_RATE_24_PWR;
extern UINT8 RPS_RATE_18_PWR;
extern UINT8 RPS_RATE_12_PWR;
extern UINT8 RPS_RATE_11_PWR;
extern UINT8 RPS_RATE_09_PWR;
extern UINT8 RPS_RATE_06_PWR;
extern UINT8 RPS_RATE_5_5_PWR;
extern UINT8 RPS_RATE_02_PWR;
extern UINT8 RPS_RATE_01_PWR;
extern UINT8 RPS_RATE_00_PWR;

#define PASSIVE_SCAN			   0	
#define ACTIVE_SCAN 			   1
#define SSID_SPECIFIC_SCAN 		   1
#define SCAN_REQUEST_ID               0x3000
#define JOIN_REQUEST_ID               0x4000
#define AUTHENTICATION_REQUEST_ID     0x5800
#define WEP_KEYS_REQUEST_ID           0xc800     
#define ASSOCIATE_REQUEST_ID          0x0000
#define REASSOCIATE_REQUEST_ID        0x1000
#define DISASSOCIATE_REQUEST_ID       0x5000
#define DEAUTHENTICATE_REQUEST_ID     0x6000
#define STATION_STATASTICS_REQUEST_ID 0xC000

#define SCAN_CONFIRM_ID               0x000D
#define JOIN_CONFIRM_ID               0x000E
#define AUTHENTICATION_CONFIRM_ID     0x000F
#define ASSOCIATE_CONFIRM_ID          0x0003
#define REASSOCIATE_CONFIRM_ID        0x0009
#define DISASSOCIATE_CONFIRM_ID       0x0011
#define DEAUTHENTICATE_CONFIRM_ID     0x0001
#define STATION_STATASTICS_INDICATION 0x0017
#define DATA_ACK                      0x0019

#define RSI_DESC_LEN                  16

/*Scan.Request parameters */
#define MODE_ANY                      0x0000
#define MODE_INFRASTRUCTURE           0x0004
#define MODE_INDEPENDENT              0x0008
#define MODE_RESERVED                 0x000C
#define SSID_BROADCAST                0x0000
#define SSID_PARTICULAR               0x0002
#define BSSID_BROADCAST               0x0000
#define BSSID_PARTICULAR              0x0001


/*FSM INFo */
/* Scan.confirm parameters */
#define SCAN_COMPLETED                0x001f
#define BSSID_FOUND                   0x000d        
#define SCAN_INCOMPLETE               0x0001
#define SCAN_CHANNEL_COMPLETE         0x0002

#define SCAN_SUCCESS                  0x0000
#define FRAME_EXCHANGE_IN_PROGRESS    0x0001
#define TIMEOUT_MIN_CHANNEL           0x0002
#define TIMEOUT_MAX_CHANNEL           0x0003
#define NO_BSSID_FOUND                0x0004
#define PARAMETERS_INVALID            0x0005


/*Join.confirm param */
#define JOIN_SUCCESS                  0x0000
#define JOIN_INVALID_PARAM            0x0001
#define JOIN_TIMEOUT                  0x0002


/*AUTH CONFIRM PARAM */
#define MGMT_SUCCESS                  0x0000
#define MGMT_FAIL                     0x0001
#define AUTH_INVALID_PARAM            0x0003
#define AUTH_TIMEOUT                  0x0002
#define AUTH_REFUSED_DA               0x0001
#define AUTH_REFUSED_DA1              0x000a
#define AUTH_REFUSED_DA2              0x000b


/*AUTHENTICATION Info */
#define AUTH_OPEN_SYSTEM              0x0000
#define AUTH_SHARED_KEY               0x0001


/*ASSOCIATE Info */
#define ASSOCIATE_SUCCESS             0x0000
#define ASSOCIATE_INVALID_PARAM       0x0003
#define ASSOCIATE_TIMEOUT             0x0002


/*DISASSOCIATE Info */
#define DISASSOCIATE_SUCCESS          0x0000
#define DISASSOCIATE_INVALID_PARAM    0x0001
#define DISASSOCIATE_TIMEOUT          0x0002
#define DISASSOCIATE_REFUSED_DA       0x0003


/*DEAUTHENTICATE Info */
#define DEAUTHENTICATE_SUCCESS        0x0000
#define DEAUTHENTICATE_INVALID_PARAM  0x0001

/*Indicates that this value would be filled by firmware */
#define FIRMWARE_DEFINED              0x0000

/*We send only the first auth req packet, hence our seq num is constant */
#define AUTH_SEQ_NUM                  0x0001      
#define AUTH_ALG_SHARED               0x0001
#define AUTH_ALG_OPEN                 0x0000
#define MGMT_FRAME_HDR_LEN            40
#define IEEE80211_FRAME_HDR_LEN       24


#define RSI_SSID_EID                   0
#define RSI_SUPPORTED_RATES_EID        1
#define RPS_AUTO_RATE_FALL             0x0020

/*predefined values */
#define RSI_SHORT_SLOT_TIME            0x0400
#define RSI_SHORT_PREAMBLE             0x0020
#define RSI_CAPABILITY_INFO            0x0001
#define RSI_LISTEN_INTERVAL            0x0010
#define RSI_SLEEP_IDLE_TIME            50

/* driver configuration slot modes */
#define RSI_SLOT_AUTO                  1 /* AP's slot will be used */
#define RSI_SLOT_LONG                  0 

/* preamble modes */
#define RSI_PREAMBLE_AUTO              1
#define RSI_PREAMBLE_LONG              0


/*MGMT frame types */

#define MGMT_DESC_TYP_FIRMWARE_CFM              0xFE00
#define MGMT_DESC_TYP_CARD_RDY_IND              0x8900

#define MGMT_DESC_TYP_AUTH_REQ                  0x1400
#define MGMT_DESC_TYP_ASSOCIAT_REQ              0x1600
#define MGMT_DESC_TYP_DEAUTH_REQ                0x1800
#define MGMT_DESC_TYP_DISASSOCIAT_REQ           0x1A00
#define MGMT_DESC_TYP_SEND_BEACON_REQ           0x1200
#define MGMT_DESC_TYP_SLEEP_CMD_REQ             0x2000
#define MGMT_DESC_TYP_SLEEP_PARAM_REQ           0x2200
#define MGMT_DESC_TYP_BEACONS_REQ               0x2400
#define MGMT_DESC_TYP_SET_BBGAIN_REQ            0x2600
#define MGMT_DESC_TYP_MSR_NF_REQ                0x2800
#define MGMT_DESC_TYP_PRB_RSP_REQ               0x2A00
#define MGMT_DESC_TYP_RF_PROG_REQ               0x2E00
#define MGMT_DESC_TYP_SET_KEYS_REQ              0x3000
#define MGMT_DESC_TYP_STATS_REQ                 0x3800
#define MGMT_DESC_TYP_JOIN_REQ                  0x3A00
#define MGMT_DESC_TYP_SCAN_REQ                  0x3C00
#define MGMT_DESC_TYP_RESET_REQ                 0x3E00
#define MGMT_DESC_TYP_LD_RATE_SYM_REQ           0x6A00
#define MGMT_DESC_TYP_WAKEUP_REQ                0x7E00
#define MGMT_DESC_TYP_SND_PRB_REQ               0x7A00
#define MGMT_DESC_TYP_TSF_SYNC_RESP             0x7C00
#define MGMT_DESC_TYP_MEASURE_NOISE_RSSI_REQ    0x7800
#define MGMT_DESC_TYP_UPDATE_NOISE_RSSI_REQ     0x7600
#define MGMT_DESC_TYP_FORM_IBSSDS_RSP           0x7400
#define MGMT_DESC_TYP_RESET_MAC_REQ             0x2C00
#define MGMT_DESC_TYP_AUTO_RATE_REQ             0x7000
#define MGMT_DESC_TYP_QOS_PARAM_REQ             0x7200
#define MGMT_DESC_TYP_PRG_BB_REG                0x2200

#define MGMT_DESC_TYP_RESET_CONT_TX_REQ         0x5E00
#define MGMT_DESC_TYP_AUTH_CFM                  0xC000
#define MGMT_DESC_TYP_ASSOCIAT_CFM              0xC200
#define MGMT_DESC_TYP_DEAUTH_CFM                0xC400
#define MGMT_DESC_TYP_DISASSOCIAT_CFM           0xC600
#define MGMT_DESC_TYP_DEAUTH_IND                0xC800
#define MGMT_DESC_TYP_DISASSOCIAT_IND           0xCA00
#define MGMT_DESC_TYP_GIVE_BEACON_REQ           0xAC00
#define MGMT_DESC_TYP_DATA_FRM_STS_IND          0xA600
#define MGMT_DESC_TYP_MALFUN_IND                0x9C00
#define MGMT_DESC_TYP_MSR_NF_CFM                0xB200
#define MGMT_DESC_TYP_STATS_RESP                0xA200
#define MGMT_DESC_TYP_BEACONS_RESP              0xB600
#define MGMT_DESC_TYP_SET_KEYS_CFM              0xB400
#define MGMT_DESC_TYP_RF_PROG_CFM               0xB000
#define MGMT_DESC_TYP_PRB_RSP_CFM               0xB800
#define MGMT_DESC_TYP_JOIN_CFM                  0x8400
#define MGMT_DESC_TYP_SCAN_RSP                  0x8200
#define MGMT_DESC_TYP_RESET_CFM                 0x8000
#define MGMT_DESC_TYP_MIC_ERR_IND               0x9C00
#define MGMT_DESC_TYP_PS_IND                    0xAE00 
#define MGMT_DESC_TYP_TSF_SYNC_REQ              0xCC00 
#define MGMT_DESC_TYP_RCVD_PRB_RESP             0xCE00
#define MGMT_DESC_TYP_MEASURE_NOISE_RSSI_CFM    0xD000
#define MGMT_DESC_TYP_FORM_IBSSDS_REQ           0xD200
#define MGMT_DESC_TYP_GIVE_SCAN_CMD             0xD400
#define MGMT_DESC_TYP_RESET_MAC_CFM             0xD600
#define MGMT_DESC_TYP_QOS_PARAM_CFM             0xD800
#define MGMT_DESC_TYP_UPDATE_QOS_PARAM_REQ      0xDA00
#define MGMT_DEEP_SLEEP_WAKEUP_CFM              0x8A00
#define MGMT_DESC_EEPROM_READ_CFM               0x8E00  
#define MGMT_DESC_EEPROM_WRITE_CFM              0x8F00 

/*TA mgmt types */
#define MGMT_DESC_TYP_LMAC_PROG_1_REQ           0x0600
#define MGMT_DESC_TYP_LMAC_PROG_VALS            0x2400
#define MGMT_DESC_TYP_LMAC_PROG_2_REQ           0x0700
#define MGMT_DESC_TYP_LMAC_PROG_3_REQ           0x0400

/** Eeprom Mgmt Type***/
#define MGMT_DESC_EEPROM_READ                   0x0B00  
#define MGMT_DESC_EEPROM_WRITE                  0x0C00

#define MGMT_DESC_TYP_LOAD_STA_CONFIG           0x0300
#define MGMT_DESC_TYP_PWR_SAVE_REQ              0x0200
#define MGMT_DESC_TYP_PWR_SAVE_CFM		0x8500
#define MGMT_DESC_TYP_LMAC_PROG_CFM		0x9000

#define SCAN_RSP_COMPLETED                      0
#define SCAN_RSP_BSSID_FOUND                    1

/* various fsm states */
#pragma pack()
#define CLOSED                                  0
#define FSM_SCAN_REQ_SENT                       1
#define FSM_JOIN_REQ_SENT                       2
#define FSM_AUTH_REQ_SENT                       3
#define FSM_ASSOCIATE_REQ_SENT                  4
#define FSM_OPEN                                5
#define FSM_NO_STATE                            6     
#define FSM_SET_WEPKEYS_SENT                    7
#define FSM_SET_WEPGRP_KEYS_SENT                8
#define FSM_SET_WPA_PAIRKEYS_SENT               9
#define FSM_SET_WPA_GRP_KEYS_SENT               10
#define FSM_DEEP_SLEEP                          11
#define FSM_SCAN_REQ_SENT_INOPEN                12
#define FSM_IBSS_CREATED                        13
#define FSM_RESET1_SENT                         14
#define FSM_RESET2_SENT                         15
#define FSM_RF_REQ_SENT                         16
#define FSM_CARD_NOT_READY                      17
#define FSM_LMAC_PROG_VALS_SENT                 18
#define FSM_DEEP_SLEEP_REQ_SENT                 19

#define FSM_FIRMWARE_LOADED                     20 

#define PS_FSM_AUTO				1
#define PS_FSM_NORMAL				2
#define PS_FSM_DEEP_SLEEP			3
#define PS_FSM_DISABLE				4

#define REGULAR_SLEEP_REQUEST			1
#define DEEP_SLEEP_REQUEST			2
#define DISABLE_PWR_SAVE			0

#define PS_AUTO_REQ				1
#define PS_NORMAL_REQ				2
#define PS_DEEP_SLEEP_REQ			3
#define PS_DISABLE_REQ				4

/*End of FSM INFO */

#define RPS_WEP_64                               0x0004
#define RPS_WEP_128                              0x000C
#define RPS_PROTECT_BROAD_CAST_DATA              0x2000
#define RPS_TKIP                                 0x0030 
#define RPS_CCMP                                 0x0010   

/* Managment frame status and control bits */
#define MGMT_CTRL_GIVE_STS_IN_MGMT_FRAME        0x0200
#define MGMT_CTRL_GIVE_STS_IN_QUEUE             0x0200
#define MGMT_CTRL_TX_POWER_VALID                0x0080
#define MGMT_CTRL_SEND_FRAME_AS_IT_IS           0x0002
#define MGMT_CTRL_TAKE_VAL_FRM_DESC             0x0001

#define MAX_NUM_ICV_ERRORS                      1
#define MAX_MSDU_LEN                            2300
#define MAX_MSDU_TX_LIFETIME                    6000
#define MAX_MSDU_RX_LIFETIME                    6000
#define MAX_LISTEN_INTRVL                       12
#define CWIN_MIN                                15
#define CWIN_MIN_B                              31
#define CWIN_MAX                                1023
#define DFL_FRAG_THRSH                          2346
#define DFL_RTS_THRSH                           2346
#define LONG_RTRY_LIMIT                         5
#define SHORT_RTRY_LIMIT                        4
#define OPERATIONAL_PARMS                       0

#define RPS_NO_ROAMING                          0
#define RPS_ROAMING                             1

#define RSI_DESC_QUEUE_NUM_MASK  		0x7
#define RSI_DESC_AGGR_ENAB_MASK  		0x80

#define RSI_SDIO_FRM_TRF_SIZE                   (256 - 16)
/* Power save values */
/*power modes */
typedef enum {
   RPS_POWER_NORMAL,
   RPS_POWER_SLEEP_HOST_CMD,
   RPS_POWER_SLEEP_FIRM,
   RPS_POWER_DSLEEP_HOST_INT,
   RPS_POWER_DSLEEP_TIME_OUT
}RPS_POWER_MODE;



/* sleep mode bit settings */
#define SLEEP_DEEP_MANUAL                       0x01
#define SLEEP_DEEP_AUTO                         0x02
#define SLEEP_NORMAL                            0x04
#define SLEEP_TX_POWE_CNTRL                     0x08
#define SLEEP_CLOCK_GATING                      0x10

/* power sava modes bits*/
#define PWR_SAVE_ENABLE                         0x01
#define PWR_SAVE_SOFT_CTRL                      0x02
#define PWR_SAVE_DEEP_SLEEP                     0x04
#define PWR_SAVE_DTIM_WAKEUP                    0x08
#define PWR_SAVE_FIRM_CTRL                      0x10
#define PWR_SAVE_AFE_CTRL                       0x20
#define PWR_SAVE_RSSI_TRG_CTRL                  0x40

/* clock gatin enables */
#define RPS_CLK_GATING_ENABLES_SLEEP            0x1CB
#define RPS_CLK_GATING_ENABLES_DSLEEP           0x1FF

/*wakeup time for various sleep profiles */
#define WAKEUP_TIME_SLEEP1                      3861
#define WAKEUP_TIME_SLEEP2                      861 
#define WAKEUP_TIME_SLEEP3                      711
#define WAKEUP_TIME_SLEEP4                      213
#define WAKEUP_TIME_SLEEP5                      63
#define WAKEUP_TIME_SLEEP6                      3 
#define WAKEUP_TIME_DSLEEP1                     14861
#define WAKEUP_TIME_DSLEEP2                     4861

/*components which are used in sleep profiles */
#define  RPS_COMPO_RF_PD1_PD2                   0x8000
#define  RPS_COMPO_RF_PLLON                     0x4000
#define  RPS_COMPO_SHUT_DOWN_IQ_DAC_ADC         0x2000
#define  RPS_COMPO_STND_BY_IQ_DAC_ADC           0x1000
#define  RPS_COMPO_SHUT_DOWN_AUX_ADC            0x0800
#define  RPS_COMPO_RX_TX_LOW                    0x0400
#define  RPS_COMPO_AFE_PLL                      0x0200
#define  RPS_COMPO_AFE_BIAS_CELL                0x0100

/*sleep wakeup register values*/
/* comes out of sleep values*/
#define WAKEUP_MGMT_PENDING                     0x04 
#define WAKEUP_MSDU_PENDING_Q0                  0x08
#define WAKEUP_MSDU_PENDING_Q1                  0x10
#define WAKEUP_MSDU_PENDING_Q2                  0x20
#define WAKEUP_MSDU_PENDING_Q3                  0x40

/* deep sleep wakeup modes */
#define DSLEEP_WAKEUP_TIMEOUT                   0x1
#define DSLEEP_WAKEUP_INT                       0x0

/* wakeup modes */
typedef enum {
  RPS_WAKE_SLEEP_HOST_CMD,
  RPS_WAKE_SLEEP_FIRM,
  RPS_WAKE_DSLEEP_HOST_INT,
  RPS_WAKE_DSLEEP_TIME_OUT
}RPS_WAKE_MODES;

#define RPS_FUNC_MSTR_CLK_GATING                0x0010
#define RPS_XTAL_GOOD_TIME                      0xFF00
#define RPS_PWR_GOOD_TIME                       0x00FF
#define FRAME_DESC_SZ                           16
#define MGMT_FRAME_DESC_SZ                      16
/* power save fsm sates*/
/* normal mode */
#define PWR_FSM_NORMAL                          0
/* power save params sent waiting for confirm*/
#define PWR_FSM_PARMS_SENT                      1
/* power save params sent failed*/
#define PWR_FSM_PARMS_FAILED                    2
/* power command sent and waiting for confirm */
#define PWR_FSM_CMD_SENT                        3
/* wakeup command sent and waiting for confirm */
#define PWR_FSM_WAKEUP_SENT                     4
/* current state in sleep type of sleep can be
 * find from Adapter->cur_power_mode */
#define PWR_FSM_SLEEP                           5

/* Recommended DSCP to 802.1d mapping (based on sec 3.3.1 of WMM Spec) */

#define IP_TOS_DSCP_0x38_AC_VO 0x38	 /*Voice,		 802.1d = 7 */
#define IP_TOS_DSCP_0x30_AC_VO 0x30	 /*Voice,		 802.1d = 6 */
#define IP_TOS_DSCP_0x28_AC_VI 0x28	 /*Video,		 802.1d = 5 */
#define IP_TOS_DSCP_0x2E_AC_VI 0x2e	 /*Video,		 */
#define IP_TOS_DSCP_0x20_AC_VI 0x20	 /*Video,		 802.1d = 4 */
#define IP_TOS_DSCP_0x18_AC_BE 0x18	 /*Best effort,	 802.1d = 3 */
#define IP_TOS_DSCP_0x10_AC_BK 0x10	 /*Back ground,	 802.1d = 2 */
#define IP_TOS_DSCP_0x08_AC_BK 0x08	 /*Back ground,	 802.1d = 1 */
#define IP_TOS_DSCP_0x00_AC_BE 0x00 	 /*Best effort,	 802.1d = 0 */

/* WMM Information element structure */
/* FIXME duplicate structure, we can remove this later*/
typedef struct wmm_info_s
{
  UINT8     eId;  
  UINT8     length;  
  UINT8     oui[3];  
  UINT8     ouiType;  
  UINT8     ouiSubType;      
  UINT8     version;      
  UINT8     acInfo;      
}wmm_info_t;

#define WIFI_WPA_OUI_TYPE       1
#define WIFI_WMM_OUI_TYPE       2
#define WMM_PARAM_IE_SUBTYPE  0x1

#define  ELEMENT_SSID          0
#define  ELEMENT_BASIC_RATESET 1
#define  ELEMENT_CHANNEL_NUM   3
#define  ELEMENT_TIM           5
#define  ELEMENT_EXT_RATES     0x32
#define  WLAN_EID_GENERIC      0xdd            
#define  WLAN_EID_RSN          0x30

#define  ELEMENT_HT_CAPABILITY  0x33
#define  ELEMENT_HT_CAPABILITY1  0x2d
#pragma pack(1)
/*
 * Management frames that are actually transmitted to the AP. These contain
 * the 802.11 header plus the frame type that is being sent out
 */


/*
 *Reset and Scan are only for the firmware.So they do not contain 802.11 header.
 */
typedef struct {
  UINT16 length;
  UINT16 header;
  UINT16 duration;
  UINT8  destination[6];
  UINT8  source[6];
  UINT8  bssid[6];
  UINT16 seq_frag_number;
}probe_req_frame_body_t;

struct RSI_Mac_frame{

  UINT16 descriptor[8];

  union {

    struct{
      UINT16 bb_val_length;
      UINT16 bb_program_val[126];
    }reset;

    struct{
      UINT16 minChannelTime;
      UINT16 maxChannelTime;
      UINT16 channel_val_len;
      UINT16 channelNumProgVal[6];
    }scan;

    struct{
      UINT16 minChannelTime;
      UINT16 responseTimeout;
      UINT16 channel_val_len;
      UINT8  var_data[1]; /* take care */
    }probe_req;

    struct{
      UINT16 length;
      UINT16 beacon_int;
      UINT16 capabilityInfo;
      UINT8  variable[1];
    }probe_resp;

    struct {
      UINT8   bssid[6];
      UINT16  beacon_intr;
      UINT16  dfl_tx_mgmt_rate;
      UINT16  dfl_tx_data_rate;
      UINT16  dfl_tx_data_B_rate;
      UINT16  max_no_ICV_err;
      UINT16  max_msdu_len;
      UINT16  max_msdu_tx_lifetime;
      UINT16  max_msdu_rx_lifetime;
      UINT16  cwin_min;
      UINT16  cwin_max;
      UINT16  res1[2];
      UINT16  dfl_frag_thresh;
      UINT16  dfl_rts_thresh;
      UINT16  max_listen_intr;
      UINT16  short_retry_limit;
      UINT16  long_retry_limit;
      UINT16  res2[2];
      UINT16  snap[3];
      UINT16  res3;
      UINT16  max_inactive;
      UINT16  timing_param[8];
      UINT16  dfl_tx_power;
      UINT8   mac_addr[6];
      UINT16  bcn_hdr_duration;
      UINT16  res4[8];
      UINT16  chnl_prog_val_len;
      UINT16  chnl_prog_val[6];
      UINT16  op_parms;
    }join;

    struct{
      UINT16 RFtype;
      UINT16 length;                  
      UINT16 channelProgrameVal[76];
    }rf_prog;

   struct {
      UINT16 authAlgoID;
      UINT16 authSeqNum;
      UINT16 statusCode;
    }auth;

    struct {
      UINT16 capabilityInfo;
      UINT16 listenInterval;
      UINT8  variable[1]; /*SSID and supported rates are variable length. */
    }assoc;

    struct {
      UINT16 reasonCode;
    }deauth, disassoc;
#if 0
    struct{
      UINT8   cipher;
      UINT8   nulldata;                       
      UINT8   ptk[32];
    }pair_key;
#endif    

    struct{
	    UINT16 keyDesc;
	    UINT8 TK[16];
	    UINT8 mic_tx_key[8];
	    UINT8 mic_rx_key[8];
    }pair_key;

    struct{
      UINT8   cipher;
      UINT8   key_id;                 
      UINT8   gtk[32];
      UINT8   SeqCount[6];
    }group_key;

    struct{
      UINT16 nf_cmds[2];
      UINT16 length;
      UINT16 variable[1];
    }nf_measure;
    
    struct{
      UINT16 gain_val[2];
    }setbb_gain;
	 
   
    union{
      struct{
        UINT16 keyDesc;
        UINT8  res;
        UINT8  key[5];
      }wep_64; 

      struct{
        UINT16 keyDesc;
        UINT8  res;
        UINT8  key[13];
      }wep_128;  
    }wep_key;

    union{
      struct{
        UINT16 sleep_idle_time;
        UINT16 dtim_count;
        UINT16 listen_interval;
        UINT16 sleep_mod_reg;
        UINT16 pwr_xtal_good_time_reg;
      }params;
      struct{
        UINT16 pwr_sava_mode;
        UINT16 wakeup_beacon_intrvl;
        UINT16 clk_gating_enables;
        UINT16 sleep_profile;
        UINT16 pwr_down_components;
        UINT16 sleep_wakeup_reg;
        UINT16 dsleep_wakeup_reg;
        UINT16 sleep_profile_wakeup_time;
        UINT8  dsleep_timeout_reg[3];
      }sleep_cmd;
    }sleep;
    struct{
      UINT16 residue;
    }tsf;
    struct{
      UINT16 set_msr_noise_rssi_cmd;
      UINT16 reset_msr_noise_rssi_cmd;
      UINT16 no_times;
      UINT16 lenth_pre_bb_prob_vals;
      UINT16 pre_bb_vals[2];
      UINT16 lenth_reset_bb_prob_vals;
      UINT16 reset_bb_vals[2];
    }measure_noise_rssi;
    struct{
      UINT16 val1;
      UINT16 val2;
    }update_noise_rssi;
    struct{
      UINT16 data_rate;
      UINT8  supported_rates[6];
    }form_ibssds;
    struct{
      UINT16 failure_limit;
      UINT16 Initial_boundary;
      UINT16 max_threshold_limt;
      UINT16 num_supported_rates;
      struct {
         UINT16  supported_rates;
         UINT16  frag_thrshld;
       }values[1]; /* This can be variable lenght */
    }auto_rate;
    struct{
      UINT16 cwmin[4];
      UINT16 cwmax[4];
      INT16 aifsn[4];
      UINT16 txop[2];
    }qos_params;
    struct{
      UINT16 rate_val[20];
    }rate_symb_req;
    struct{
      UINT16 length;
      UINT16 data[126];
    }lmac_prog_val_1;
    struct{
      UINT16 data[128];
    }lmac_prog_vals;
    struct{
      UINT16 rf_type;
      UINT16 length;
      UINT16 data[126];
    }lmac_prog_val_2;
    struct{
      UINT16 data[126];
    }lmac_prog_val_3;
    struct{
      UINT16 supp_protocols;
      UINT16 supp_features;
      UINT8  ampdu_param;
      UINT8  amsdu_len;
      UINT16 frag_thr;
      UINT16 data_rate;
      UINT16 num_tids;
      UINT16 supp_tids;
      UINT16 beacon_intr;
      UINT16 listen_intr;
      UINT16 dtim_period;
    }sta_config;
    struct{
      UINT16 length;
      UINT16 offset;
    }eeprom_read;
    struct{                
      UINT8  data[256]; /*  This data buff is for eeprom write only */
    }eeprom_write;
   
  }frameType;
  
};



/*State will be maintained by this variable */
GANGES_EXTERN UINT32 FSM_STATE;
GANGES_EXTERN UINT32 PS_FSM_STATE;
struct file* open_file(const INT8 *fn);
INT8 get_a_char(struct file *filp);
VOID close_file(struct file *filp);
UINT8 extract_token(struct file *fp, UINT8 * token);
UINT16 *extract_variable(UINT8 *arr_name,int);

VOID  ganges_ps_fsm(PRSI_ADAPTER Adapter,UINT16 status,UINT16 type);
VOID  ganges_measure_pwrup_nfm(PRSI_ADAPTER Adapter,UINT8 *mPkt,INT32 len);
VOID  ganges_mgmt_fsm(PRSI_ADAPTER Adapter, UINT8 MgmtRcvFrame[], UINT16 type, UINT32 MngPacketLen);
INT32 ganges_copy_scanconfirmInfo(PRSI_ADAPTER Adapter, UINT8 mgmtRcvFrame[], PScanConfirm scanConfirmVarArray[],
                                  UINT32 MngPacketLen);
INT32 ganges_mgmt_init(PRSI_ADAPTER Adapter);
INT32 ganges_is_AP_supported_rate(PScanConfirm scanConfirm, UINT8        rate);
VOID ganges_RF_program(PRSI_ADAPTER Adapter);
INT32 ganges_send_reset_request(PRSI_ADAPTER Adapter, UINT16 no_bb_values,  UINT16 *bb_values);
INT32 ganges_send_auth_request(PRSI_ADAPTER Adapter, UINT8  peerStationAddress[],UINT16 authenticationType);
INT32 ganges_sendRFProgRequest(PRSI_ADAPTER Adapter,UINT16 rf_type,UINT16 no_vals,UINT16 *rf_vals);
INT32 ganges_send_scanrequest(PRSI_ADAPTER Adapter, UINT32       channelNum);	
INT32 ganges_send_join_request(PRSI_ADAPTER Adapter, PScanConfirm scanConfirm);
INT32 ganges_send_Associate_request(PRSI_ADAPTER Adapter,UINT8 peerStationAddress[],PScanConfirm scanConfirm);
INT32 ganges_send_disassociate_request(PRSI_ADAPTER Adapter, UINT8 peerStationAddress[],UINT16 reasonCode);
INT32 ganges_send_DeAuthentication_request(PRSI_ADAPTER Adapter,UINT8 peerStationAddress[],UINT16 reason_code);	
INT32 ganges_send_WEPKeys_request(PRSI_ADAPTER Adapter,UINT8 LWEPKeysInfo_org[],UINT32 LWEPKeyLength,
		    UINT32 LWEPKeyIndex,UINT8 mac_addr[],UINT8 group_key);
INT32 ganges_send_pair_key_install(PRSI_ADAPTER Adapter);
INT32 ganges_send_sleep_params(PRSI_ADAPTER Adapter, UINT16 sleep_idle_time, UINT16 dtim_count,UINT16 listen_interval);
INT32 ganges_send_deep_sleep_request(PRSI_ADAPTER Adapter,UINT32 time_out,UINT16 sleep_profile,UINT16 clk_gating);
INT32 ganges_send_sleep_request(PRSI_ADAPTER  Adapter,UINT16 pwr_save_mode,UINT16 wakeup_beacon_intrvl,
		    RPS_SLEEP_PROFILE  sleep_profile,UINT16 clk_gating);
INT32 ganges_send_wakeup_request(PRSI_ADAPTER    Adapter);
INT32 ganges_send_TSF_resp(PRSI_ADAPTER    Adapter,UINT16 residue);
INT32 ganges_send_group_key_install(PRSI_ADAPTER Adapter,UINT8 *SeqCounter,UINT8 key_id);	
INT32 ganges_send_nf_measure_request(PRSI_ADAPTER Adapter,UINT16 rf_type,UINT32 length);	
INT32 ganges_setBBGainRequest(PRSI_ADAPTER Adapter,UINT16  rf_type,UINT32 length,INT32 error);	
INT32 ganges_send_measure_noise_rssi_request(PRSI_ADAPTER Adapter,UINT32 no_times);
INT32 ganges_send_update_noise_rssi_request(PRSI_ADAPTER Adapter,UINT16 update_noise_rssi_val);
INT32 ganges_send_ibssds_response(PRSI_ADAPTER Adapter,UINT8 sta_mac[],ProtoType ProtocolType);
INT32 ganges_send_auto_rate_request(PRSI_ADAPTER Adapter, INT8 power_control, INT8 antenna_diversity);
INT32 ganges_send_qos_params(PRSI_ADAPTER Adapter,genIe_t *wmmIe);
INT32 ganges_send_rate_symbol_request(PRSI_ADAPTER Adapter);
INT32 ganges_start_ibss(PRSI_ADAPTER Adapter);
INT32 ganges_measure_auto_nfm(PRSI_ADAPTER Adapter,INT8 nfm_val);
INT32 ganges_measure_auto_nfm_rssi(PRSI_ADAPTER Adapter,INT8 nfm_val);
INT32 ganges_send_reset_mac(PRSI_ADAPTER Adapter);
INT32 ganges_send_mgmt_frame( PRSI_ADAPTER Adapter,UINT8 frame[], UINT32 len);
VOID ganges_do_scan(PRSI_ADAPTER Adapter,UINT8 flag,NDIS_802_11_SSID *ssid);
VOID ganges_set_ssid(PRSI_ADAPTER Adapter, NDIS_802_11_SSID *ssid);
VOID ganges_set_bssid(PRSI_ADAPTER Adapter, UINT8 *bssid);
INT32 ganges_send_probe_request(PRSI_ADAPTER Adapter,UINT8 type, UINT32  channelNum, UINT32  broadcast,UINT8  *destination );
BOOLEAN  ganges_can_connect(PRSI_ADAPTER Adapter,INT32 selected);
VOID ganges_free_bssid_list(PRSI_ADAPTER Adapter);
VOID ganges_set_channel_type(PRSI_ADAPTER, RPS_CHANNEL_TYPE); 
VOID ganges_send_load_STA_config( PRSI_ADAPTER Adapter);
INT32 ganges_send_TA_mgmt_frame(PRSI_ADAPTER Adapter, UINT8 frame[], UINT32 len );
VOID ganges_do_adapter_Reset (PRSI_ADAPTER Adapter);
VOID ganges_set_protocol_type(PRSI_ADAPTER Adapter, RPS_PROTOCOL_TYPE ProtocolType);
VOID ganges_set_txrate( PRSI_ADAPTER Adapter,  RPS_TX_RATE  TxRate );
VOID ganges_send_LMAC_prog_val_1(PRSI_ADAPTER Adapter,UINT16 length,UINT16 *data);
VOID ganges_send_LMAC_prog_val_2(PRSI_ADAPTER Adapter,UINT16 length,UINT16 *data);
VOID ganges_send_LMAC_prog_val_3(PRSI_ADAPTER Adapter,UINT16 length,UINT16 *data);
GANGES_STATUS ganges_send_probe_resp_request(PRSI_ADAPTER Adapter,ProtoType ProtocolType);
GANGES_STATUS ganges_send_program_BB_registers(PRSI_ADAPTER Adapter,UINT16 no_registers,UINT16 bb_registers[]);
VOID ganges_send_power_save_req(PRSI_ADAPTER Adapter,UINT16 pwr_save_enable);
GANGES_STATUS ganges_get_station_statistics(PRSI_ADAPTER Adapter,UINT32 stats_interval);
GANGES_STATUS ganges_eeprom_write(PRSI_ADAPTER Adapter,PEEPROM_WRITE write_eeprom);
GANGES_STATUS ganges_eeprom_read(PRSI_ADAPTER Adapter,PEEPROM_READ read_eeprom);
GANGES_STATUS ganges_read_power_vals(PRSI_ADAPTER Adapter);
INT8 ganges_calculate_rssi(PRSI_ADAPTER Adapter,UINT8 *frame);
VOID ganges_reset_power_vals(PRSI_ADAPTER Adapter,UINT16 channel);
GANGES_STATUS ganges_load_config_vals(PVOID Adapter);
VOID ganges_send_LMAC_prog_vals(IN PRSI_ADAPTER Adapter);
VOID ganges_find_mgmt_type(IN UINT16 type);
#endif




