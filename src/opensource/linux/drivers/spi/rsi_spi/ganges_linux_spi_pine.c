/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Ganges 
Module name  : LINUX-SDIO driver
File Name    : ganges_linux_sdio.c

File Description:

List of functions:
      ganges_send_ssp_init_cmd
      ganges_send_ssp_read_intrStatus_cmd
      ganges_send_ssp_intrAck_cmd
      ganges_send_ta_load_cmd
      ganges_send_ta_sft_rst_cmd
      ganges_load_LMAC_instructions
      ganges_load_TA_instructions
      ganges_handle_interrupt
      ganges_card_write
      ganges_card_read
      ganges_init_interface
      ganges_deinit_interface
      ganges_enable_tcp_chk_sum
      ganges_send_dsleep_wakeup_request
      ganges_send_ssp_wakeup_cmd

Author :
  
Rev History:
Sl  By    date change details
---------------------------------------------------------

---------------------------------------------------------
*END*************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/delay.h>

#include "ganges_linux.h"
#include "ganges_mgmt.h"
#include "ganges_nic.h"
#include "ganges_ssp_bus.h"

struct cli_callBk_s c_callBk ;
/*FUNCTION*********************************************************************
Function Name  : ganges_send_ssp_init_cmd
Description    : This function sends the SPI initialization command
Returned Value : Returns 0 on success and -1 on failure
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

INT32
ganges_send_ssp_init_cmd
  (
    IN PRSI_ADAPTER Adapter
  )
{
  struct bus_cmd_s cmd;
  INT32  status = RSI_STATUS_SUCCESS;
  UINT32 ii = 0;

  /* Prepare the INITIALIZATION cmd */
  ganges_memset(&cmd, 0,sizeof(struct bus_cmd_s));
  cmd.type = SPI_INITIALIZATION;

  /* Send the cmd to the BUS */
  while(ii < 1)
  {
    status = ganges_ssp_bus_request_synch((SSP_BUS_HANDLE) Adapter->hDevice, &cmd);
    if( (status == RSI_STATUS_BUSY) ||
        (status == RSI_STATUS_FAIL))
    {
      ii++;
      continue;
    }
    break;
  }
  if(status != RSI_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,(TEXT("ERROR during SSP_INIT cmd\n")));
  }
  return(status);
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_ssp_read_intrStatus_cmd
Description    : This function sends the SPI INTERNAL READ interrupt status 
                 register command
Returned Value : Returns the register value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

UINT32
ganges_send_ssp_read_intrStatus_cmd
  (
    IN PRSI_ADAPTER Adapter
  )
{
  struct bus_cmd_s cmd;
  UINT32 intrStatus = 0;
  INT32  status = RSI_STATUS_SUCCESS;
  UINT32 ii = 0;

  /* Read the HOST INTERRUPT (SPI slave) register */
  /* Prepare the SPI_INTERNAL_READ cmd */
  ganges_memset(&cmd,0,sizeof(struct bus_cmd_s));
  cmd.type = SPI_INTERNAL_READ;
  cmd.data = (void *)&intrStatus;
  cmd.length = 2;
  cmd.address = 0x0000; /* HOST INTERRUPT REG */

  /* Send the cmd to the BUS */
  while(ii < 150)
  {
    status = ganges_ssp_bus_request_synch((SSP_BUS_HANDLE) Adapter->hDevice, &cmd);
    if( (status == RSI_STATUS_BUSY) ||
        (status == RSI_STATUS_FAIL))
    {
      ii++;
      continue;
    }
    break;
  }
  if(status != RSI_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
   "[send_ssp_read_intrStatus_cmd] ERROR while reading SSP_INTR_STATUS register %0x\n",status);
  }
  return(intrStatus);
}

/*FUNCTION*********************************************************************
Function Name  : send_ssp_intrAck_cmd
Description    : This function sends the AHB_MASTER_WRITE interrupt ack 
                 command
Returned Value : 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

UINT32
ganges_send_ssp_intrAck_cmd
  (
    IN PRSI_ADAPTER Adapter,
       UINT32          val
  )
{ 
  struct bus_cmd_s cmd;
  INT32  status = RSI_STATUS_SUCCESS;
  UINT32 ii = 0;
  
  /* Prepare the SPI_INTERNAL_WRITE cmd */
  ganges_memset(&cmd, 0, sizeof(struct bus_cmd_s));
  cmd.type = AHB_MASTER_WRITE;
  cmd.data = &val;
  cmd.length = 4;
  cmd.address = 0x22000010; /* HOST INTERRUPT REG */
  
  /* Send the cmd to the BUS */
  while(ii < 4)
  { 
    status = ganges_ssp_bus_request_synch((SSP_BUS_HANDLE) Adapter->hDevice, &cmd);
    if( (status == RSI_STATUS_BUSY) ||
        (status == RSI_STATUS_FAIL))
    {
      ii++;
      continue;
    }
    break;
  }
  if(status != RSI_STATUS_SUCCESS)
  { 
    RSI_DEBUG(RSI_ZONE_ERROR,(TEXT("ERROR during SSP_INTR_ACK cmd\n")));
  } 
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : send_ta_load_cmd
Description    : This function sends the AHB_MASTER_WRITE interrupt ack 
                 command
Returned Value : 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

INT32
ganges_send_ta_load_cmd
  (
    IN PRSI_ADAPTER Adapter,
    UINT8           *data,
    UINT32          size,
    UINT32          address
  )
{ 
  struct bus_cmd_s cmd;
  INT32    status = RSI_STATUS_SUCCESS;
  UINT32   ii = 0;
  
  /* Prepare the SPI_INTERNAL_WRITE cmd */
  ganges_memset(&cmd,0,sizeof(struct bus_cmd_s));
  cmd.type = AHB_MASTER_WRITE;
  cmd.data = data;
  cmd.length = size;
  cmd.address = address;
  
  /* Send the cmd to the BUS */
  while(ii < 4)
  { 
    status = ganges_ssp_bus_request_synch((SSP_BUS_HANDLE) Adapter->hDevice, &cmd);
    if( (status == RSI_STATUS_BUSY) ||
        (status == RSI_STATUS_FAIL))
    {
      ii++;
      continue;
    }
    break;
  }
  if(status != RSI_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR, (TEXT("ERROR during TA_LOAD cmd\n")));
  }
  return(status);
}

/*FUNCTION*********************************************************************
Function Name  : send_ta_sft_rst_cmd
Description    : This function sends the AHB_MASTER_WRITE interrupt ack 
                 command
Returned Value : 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

INT32
ganges_send_ta_sft_rst_cmd
  (
    IN PRSI_ADAPTER Adapter,
    UINT32          val
  )
{
  struct bus_cmd_s cmd;
  INT32  status = RSI_STATUS_SUCCESS;
  UINT32 ii = 0;

  /* Prepare the SPI_INTERNAL_WRITE cmd */
  ganges_memset(&cmd, 0,sizeof(struct bus_cmd_s));
  cmd.type = AHB_MASTER_WRITE;
  cmd.data = &val;
  cmd.length = 4;
  cmd.address = 0x22000004;

  /* Send the cmd to the BUS */
  while(ii < 4)
  {
    status = ganges_ssp_bus_request_synch((SSP_BUS_HANDLE) Adapter->hDevice, &cmd);
    if( (status == RSI_STATUS_BUSY) ||
        (status == RSI_STATUS_FAIL))
    {
      ii++;
      continue;
    }
    break;
  }
  if(status != RSI_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR, (TEXT("ERROR during SOFT_RST cmd\n")));
  }
  return(status);
}

/*FUNCTION*********************************************************************
Function Name  : ganges_read_qstatus
Description    : This function reads the isr
Returned Value : Success or fail will return
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
afe_type         |     |  X  |     | type of afe connected 
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

INT32
ganges_read_qstatus
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT32       val 	
  )
{
  struct bus_cmd_s cmd;
  INT32  status = RSI_STATUS_SUCCESS;
  UINT32 ii = 0;
  /* Prepare the SPI_INTERNAL_WRITE cmd */
  ganges_memset(&cmd, 0,sizeof(struct bus_cmd_s));
  cmd.type = AHB_MASTER_READ;
  cmd.data = &val;
  cmd.length = 4;
  cmd.address = 0x2200000C;

  /* Send the cmd to the BUS */
  while(ii < 40)
  {
    status = ganges_ssp_bus_request_synch((SSP_BUS_HANDLE) Adapter->hDevice, &cmd);
    if( (status == RSI_STATUS_BUSY) ||
        (status == RSI_STATUS_FAIL))
    {
      ii++;
      continue;
    }
    break;
  }
  if(status != RSI_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"ERROR during ISR read\n");
  }
  return(status);
}

/*FUNCTION*********************************************************************
Function Name  : ganges_read_afe_type
Description    : This function reads the AFE type which is present 
Returned Value : Success or fail will return
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
afe_type         |     |  X  |     | type of afe connected 
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

INT32
ganges_read_afe_type
  (
    IN PRSI_ADAPTER Adapter
  )
{
  struct bus_cmd_s cmd;
  INT32  status = RSI_STATUS_SUCCESS;
  UINT32 val, ii = 0;
  UINT8  afe_type;
  /* Prepare the SPI_INTERNAL_WRITE cmd */
  ganges_memset(&cmd, 0,sizeof(struct bus_cmd_s));
  cmd.type = AHB_MASTER_READ;
  cmd.data = &val;
  cmd.length = 4;
  cmd.address = 0x22000004;

  /* Send the cmd to the BUS */
  while(ii < 40)
  {
    status = ganges_ssp_bus_request_synch((SSP_BUS_HANDLE) Adapter->hDevice, &cmd);
    if( (status == RSI_STATUS_BUSY) ||
        (status == RSI_STATUS_FAIL))
    {
      ii++;
      continue;
    }
    break;
  }
  if(status != RSI_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"ERROR during read AFE tyep\n");
  }
  else
  {
    afe_type = (val & 0x1);
  }
  printk("Val %08x\n",val);
  if(Adapter->AFE_type == RSI_AFE_1)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"init_interface: Detected AFE1");
  }
  else if(Adapter->AFE_type == RSI_AFE_2)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"init_interface: Detected AFE2");
  }
  else 
  {
    printk("Undefined AFE tyep\n");          
    return RSI_STATUS_FAIL;
  }
  return(status);
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_ssp_wakeup_cmd
Description    : This function sends the SPI INTERNAL READ interrupt status
                 register command
Returned Value : Returns the register value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

UINT32
ganges_send_ssp_wakeup_cmd
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT8         byte
  )
{
  struct bus_cmd_s cmd;
  UINT32 status = RSI_STATUS_SUCCESS;
  UINT32 ii = 0;

  /* Read the HOST INTERRUPT (SPI slave) register */
  /* Prepare the SPI_INTERNAL_READ cmd */

  ganges_memset(&cmd, 0, sizeof(struct bus_cmd_s));

  cmd.type = SPI_INTERNAL_WRITE;
  cmd.data = (void *)&byte;
  cmd.length = 2;
  cmd.address = 0x003F;

  /* Send the cmd to the BUS */
  while(ii < 50)
  {
    status = ganges_ssp_bus_request_synch((SSP_BUS_HANDLE) Adapter->hDevice, &cmd);
    if( (status == RSI_STATUS_BUSY) ||
        (status == RSI_STATUS_FAIL))
    {
      ii++;
      continue;
    }
    break;
  }
  if(status != RSI_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              (TEXT("[send_ssp_read_intrStatus_cmd] ERROR while writing wakeup register\n")));
  }
  return(0);
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_ssp_wakeup_cmd_read
Description    : This function sends the SPI INTERNAL READ interrupt status
                 register command
Returned Value : Returns the register value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

UINT32
ganges_send_ssp_wakeup_cmd_read
  (
    IN PRSI_ADAPTER Adapter
  )
{
  struct bus_cmd_s cmd;
  UINT32 intrStatus = 0;
  UINT32 status = RSI_STATUS_SUCCESS;
  UINT32 ii = 0;

  /* Read the HOST INTERRUPT (SPI slave) register */
  /* Prepare the SPI_INTERNAL_READ cmd */
  ganges_memset(&cmd, 0, sizeof(struct bus_cmd_s));
  cmd.type = SPI_INTERNAL_READ;
  cmd.data = (void *)&intrStatus;
 
  cmd.length = 2;
  //cmd.length = 1;
  cmd.address = 0x003F;
  /* Send the cmd to the BUS */
  while(ii < 150)
  {
    status = ganges_ssp_bus_request_synch((SSP_BUS_HANDLE) Adapter->hDevice, &cmd);
    if( (status == RSI_STATUS_BUSY) ||
        (status == RSI_STATUS_FAIL))
    {
      ii++;
      continue;
    }
    break;
  }
  if(status != RSI_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              (TEXT("[send_ssp_read_intrStatus_cmd] ERROR while reading wakeup register\n")));
  }
  //printk("Wakup read: %d\n",ii); 
  return(status);
}

/*FUNCTION*********************************************************************
Function Name  : send_ta_config_params_cmd
Description    : This function sends the AHB_MASTER_WRITE interrupt ack 
                 command
Returned Value : 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/
 
UINT32
ganges_send_ta_config_params_cmd
 (
   IN PRSI_ADAPTER Adapter,
   UINT8           *data,
   UINT32          size,
   UINT32          address
 )
{
  struct bus_cmd_s cmd;
  INT32  status = RSI_STATUS_SUCCESS;
  UINT32 ii = 0;

  /* Prepare the SPI_INTERNAL_WRITE cmd */
  ganges_memset(&cmd, 0,sizeof(struct bus_cmd_s));
  cmd.type = AHB_MASTER_WRITE;
  cmd.data = data;
  cmd.length = size;
  cmd.address = address;

  /* Send the cmd to the BUS */
  while(ii < 4)
  {
    status = ganges_ssp_bus_request_synch((SSP_BUS_HANDLE) Adapter->hDevice, &cmd);
    if( (status == RSI_STATUS_BUSY) ||
        (status == RSI_STATUS_FAIL))
    {
      ii++;
      continue;
    }
    break;
  }
  if(status != RSI_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR, (TEXT("ERROR during TA_LOAD cmd\n")));
  }

  return(status);
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_dummy_write
Description    : This function does dummy writes 
Returned Value : Returns the register value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

UINT32
ganges_send_dummy_write
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT8         byte
  )
{
  struct bus_cmd_s cmd;
  UINT32 status = RSI_STATUS_SUCCESS;
  UINT32 ii = 0;


  ganges_memset(&cmd, 0, sizeof(struct bus_cmd_s));

  cmd.type = SPI_DUMMY_WRITE;
  cmd.data = (void *)&byte;
  cmd.length = 2;
  cmd.address = 0x003F;

  /* Send the cmd to the BUS */
  while(ii < 50)
  {
    status = ganges_ssp_bus_request_synch((SSP_BUS_HANDLE) Adapter->hDevice, &cmd);
    if( (status == RSI_STATUS_BUSY) ||
        (status == RSI_STATUS_FAIL))
    {
      ii++;
      continue;
    }
    break;
  }
  if(status != RSI_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              (TEXT("[send_ssp_read_intrStatus_cmd] ERROR while doing dummy writes\n")));
  }
  return(0);
}

/*FUNCTION*********************************************************************
Function Name  : ganges_init_interface
Description    : This function intializes the bus driver and checks for the 
                 presence of the SPI slave
Returned Value : Returns RSI_STATUS_SUCCESS or RSI_STATUS_FAILURE
Parameters     :
-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pContext         |  X  |     |     | pointer to our adapter
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

GANGES_STATUS 
ganges_init_interface
  (
    PVOID pContext
  )
{

  PRSI_ADAPTER Adapter = (PRSI_ADAPTER)pContext;
  c_callBk.cli_gpio_intr_callBk = (void *)ganges_interrupt_handler;	
  RSI_DEBUG(RSI_ZONE_INIT,"ganges_init_interface: Initializing interface\n"); 
  /* Initialize the SPI bus driver  */
  Adapter->hDevice = (PVOID)ganges_ssp_bus_init(Adapter, &c_callBk);
  if(!Adapter->hDevice)
  {
    return RSI_STATUS_FAIL;
  }
  mdelay(1000);
  RSI_DEBUG(RSI_ZONE_INIT,"ganges_init_interface: Sending init cmd\n"); 
  /* Check for the SPI slave is present or not */
  if(ganges_send_ssp_init_cmd(Adapter) == RSI_STATUS_FAIL)
  {
    return RSI_STATUS_FAIL;
  }

  return RSI_STATUS_SUCCESS;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_deinit_interface
Description    : This function de-intializes the bus driver 
Returned Value : 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pContext         |  X  |     |     | pointer to our adapter
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

VOID
ganges_deinit_interface
  (
    PVOID pContext
  )
{
  PRSI_ADAPTER Adapter = (PRSI_ADAPTER)pContext;
  /* TODO */
  ganges_ssp_bus_deinit( (SSP_BUS_HANDLE)(Adapter->hDevice));
  return;
}
