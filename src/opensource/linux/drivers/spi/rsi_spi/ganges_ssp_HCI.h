/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Pine1_Lp
Module name  : WinXP driver
File Name    : pxa27x_ssp_HCI.h

File Description:
        This file contains SSP HCI specific defintions and function prototypes.
        
List of functions:

Author :

Rev History:
Ver   By               date        Description
---------------------------------------------------------
1.1   Anji &           1 Aug06     Initial version
      Ravi Vaishnav
---------------------------------------------------------
*END**************************************************************************/

#ifndef _SPI_HCI_H_
#define _SPI_HCI_H_

#include "common.h"


#define  RSI_ZONE_SPI_ERROR     SDCARD_ZONE_ERROR

#define  RSI_ZONE_SPI_SEND      SDCARD_ZONE_8
#define  ENABLE_ZONE_SPI_SEND   ZONE_ENABLE_8

#define  RSI_ZONE_SPI_RCV       SDCARD_ZONE_9
#define  ENABLE_ZONE_SPI_RCV    ZONE_ENABLE_9

#define  RSI_ZONE_SPI_DEBUG     SDCARD_ZONE_10
#define  ENABLE_ZONE_SPI_DEBUG  ZONE_ENABLE_10

#define BIT_GRAN_8      1
#define BIT_GRAN_16     2
#define BIT_GRAN_32     4

#define SPI_STATUS_SUCCESS      0x58
#define SPI_STATUS_BUSY         0x54
#define SPI_STATUS_FAILURE      0x52

#define SSP_START_TOKEN_8   0x55
#define SSP_START_TOKEN_16  0x0055
#define SSP_START_TOKEN_32  0x00000055

struct sspBus_callBk_s
{
  void (*ssp_bus_intr_callBk)(void *priv); /* TODO */
};


typedef struct _SPI_INFO
{
  unsigned int               dwSysintrSPI;
  unsigned int               threadID;

  struct sspBus_callBk_s clb;
  void                *priv; /* SSP BUS clb */

}SSP_INFO, *PSSP_INFO;


typedef unsigned long  SSP_HANDLE;

int rsi_spi_read(struct spi_device *spi, u8 *buf, size_t len);
int rsi_spi_write(struct spi_device *spi, const u8 *buf, size_t len);
int rsi_spi_write_then_read(struct spi_device *spi,u8 *txbuf,u8 *rxbuf, unsigned rx_tx_length);
INT32 ganges_ssp_receive_start_token(SSP_HANDLE handle,UINT16 gran);

/* TODO HCI related function prototypes */

SSP_HANDLE ssp_init(void *priv, struct sspBus_callBk_s *clb, unsigned int port, unsigned int init_flags);

void ssp_deinit( SSP_HANDLE handle);

int ssp_config(SSP_HANDLE handle, unsigned int mode, unsigned int flags, unsigned short gran);

void ssp_enable(SSP_HANDLE handle);

void ssp_disable(SSP_HANDLE handle);

void ssp_flush(SSP_HANDLE handle);

void ssp_send(SSP_HANDLE handle, void *data, int length, unsigned short gran);
        
void ssp_receive(SSP_HANDLE handle, void *data, int length, unsigned short gran);

int ssp_receive_start_token( SSP_HANDLE handle, unsigned short gran);


#endif  // _SPI_HCI_H 
