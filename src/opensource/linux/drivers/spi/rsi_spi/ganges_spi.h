/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Pine1_Lp
Module name  : WinCE driver
File Name    : ganges_ce_spi.h

File Description:
      SPI specific header file
      
List of functions:

Author :

Rev History:
Ver   By               date        Description
---------------------------------------------------------
1.1   Anji &           1 Aug06     Initial version
      Ravi Vaishnav
---------------------------------------------------------
*END**************************************************************************/

#ifndef __PINE1_LP_CE_SSP_H
#define __PINE1_LP_CE_SSP_H

#include "common.h"
#include "ganges_nic.h"
#include <linux/spi/spi.h>


#define RSI_STATUS_SUCCESS                  0
#define RSI_STATUS_BUSY                     1
#define RSI_STATUS_FAIL                     -1

#define SSP_FIFO_HEIGHT  16

#define TRUE 1
#define FALSE 0
#define FRAME_DESC_SZ                     16
#define MGMT_FRAME_DESC_SZ                16 
#define TA_INSTRUCTIONS_SIZE              (64*1024)
#define TA_DM_SIZE                        (32*1024)
#define LMAC_INSTRUCTIONS_SIZE            (16*1024)   /*16Kbytes*/
#define TEMPLATES_SIZE                    512     /*512bytes*/
#define SD_CSA_PTR                        0x0010C /*16-bits are used i.e. 0x10C,0x10D*/


/*Queue Addresses */

#define RCV_LMAC_MGMT_Q         0x01
#define RCV_DATA_Q              0x02
#define RCV_TA_MGMT_Q           0x04

#define SND_MGMT_Q              0x1
#define SND_DATA_Q              0x2
#define SND_TA_MGMT_Q           0x4

#define RSI_SD_WRITE               1
#define RSI_SD_READ                0

/* LMAC CNT REG0 BITS */
#define SD_LMAC_RST                31
#define SD_LMAC_RF_CLK_SEL         1
#define SD_LMAC_MAC_CLK_SEL        2
#define SD_LMAC_SWITCH_MAC_CLK     3

/* these are valid only when we are doing HIR in 8 bit mode */
#define SD_SLEEP_NO_PKT_ACK  (1<<7)
#define SD_DRIVER_PKT_STATUS (1<<6)
#define SD_WAKEUP_INDCN      (1<<5)
#define SD_SLEEP_INDCN       (1<<4)
#define SD_DATA_PENDING      (1<<3)
#define SD_MGMT_PENDING      (1<<2)
#define SD_PKT_BUFF_EMPTY    (1<<1)
#define SD_PKT_BUFF_FULL     (1<<0)

/* SDIO_FUN1_FIRM_LD_CTRL_REG register bits */

#define TA_SOFT_RST_CLR      0
#define TA_SOFT_RST_SET      BIT(0)
#define TA_DM_LOAD_CLR       BIT(21)
#define TA_DM_LOAD_SET       BIT(20)

/* HIR BITS */
/* Interrupt Status Values */

#define ISR_BUFFER_FULL      (1 << SD_PKT_BUFF_FULL) 
#define ISR_BUFFER_FREE      (1 << SD_PKT_BUFF_EMPTY)                
#define ISR_DATA_PENDING     (1 << SD_DATA_PENDING)
#define ISR_MGMT_PENDING     (1 << SD_MGMT_PENDING)

#define SPI_PKT_BUFF_FULL    (1<<0)
#define SPI_PKT_BUFF_EMPTY   (1<<1)
#define SPI_MGMT_PENDING     (1<<2)
#define SPI_DATA_PENDING     (1<<3)
#define SPI_CARD_READY       (1<<4)
#define SPI_DSLEEP_WAKEUP    (1<<6)

#define  SDNDIS_MAXIMUM_LOOKAHEAD          252 - SDNDIS_ETHERNET_HEADER_SIZE
#define  SDNDIS_MAXIMUM_FRAME_SIZE         1500
#define  SDNDIS_ETHERNET_HEADER_SIZE       14
#define  SDNDIS_LINK_SPEED                 100000
#define  SDNDIS_TRANSMIT_BUFFER_SPACE      SDNDIS_MAXIMUM_TOTAL_SIZE
#define  SDNDIS_RECEIVE_BUFFER_SPACE       SDNDIS_MAXIMUM_TOTAL_SIZE
#define  SDNDIS_ETHERNET_ADDRESS_LENGTH    6

typedef enum {
  BUFFER_FULL   = 0X1,
  BUFFER_FREE   = 0X2,
  MGMT_PENDING  = 0X3,
  DATA_PENDING  = 0X4,
  SLEEP_INDCN   = 0X5,
  WAKEUP_INDCN  = 0x6,
  UNKNOWN       = 0XB
} INTERRUPT_TYPE;


#define ganges_get_interrupt_type(_I)              \
                (_I & ( SD_PKT_BUFF_FULL)) ?        \
                BUFFER_FULL :\
                        (_I & (SD_PKT_BUFF_EMPTY)) ?       \
                        BUFFER_FREE :\
                                (_I & (SD_MGMT_PENDING)) ?         \
                                MGMT_PENDING:\
                                        (_I & (SD_DATA_PENDING)) ? \
                                        DATA_PENDING: \
                                        (_I & (SD_SLEEP_INDCN)) ? \
                                        SLEEP_INDCN: \
                                        (_I & (SD_WAKEUP_INDCN)) ? \
                                        WAKEUP_INDCN: UNKNOWN
  
INT32 ganges_read_qstatus(IN PRSI_ADAPTER Adapter,IN UINT32 val);
VOID  ganges_interrupt_handler(PRSI_ADAPTER Adapter);
INT32 ganges_load_LMAC_instructions(PRSI_ADAPTER Adapter);
//INT32 ganges_read_AFEtype(PRSI_ADAPTER Adapter);
//GANGES_STATUS ganges_probe();
//GANGES_STATUS ganges_disconnect();
INT32 ganges_load_TA_instructions(PRSI_ADAPTER Adapter);
INT32 ganges_rcv_pkt(PRSI_ADAPTER Adapter);
GANGES_STATUS ganges_init_interface(PVOID pContext);
VOID  ganges_deinit_interface(PVOID pContext);
INT32    ganges_send_ssp_init_cmd(IN PRSI_ADAPTER Adapter );
UINT32   ganges_send_ssp_read_intrStatus_cmd(IN PRSI_ADAPTER Adapter); 
UINT32   ganges_send_ssp_intrAck_cmd(IN PRSI_ADAPTER Adapter,UINT32 val);
INT32    ganges_send_ta_load_cmd(IN PRSI_ADAPTER Adapter,UINT8 *data,UINT32 size,UINT32 address);
INT32    ganges_send_ta_sft_rst_cmd(IN PRSI_ADAPTER Adapter,UINT32 val);
INT32    ganges_read_afe_type(IN PRSI_ADAPTER Adapter);
GANGES_STATUS ganges_card_write(PRSI_ADAPTER Adapter,UINT8 *pkt,UINT32 Len,UINT32 queueno);
GANGES_STATUS ganges_probe(struct spi_device *spi_device);
GANGES_STATUS ganges_disconnect(VOID);
UINT32 ganges_send_ssp_wakeup_cmd(IN PRSI_ADAPTER Adapter, IN UINT8         byte);
UINT32 ganges_send_ssp_wakeup_cmd_read(IN PRSI_ADAPTER Adapter);
UINT32 ganges_send_ta_config_params_cmd(IN PRSI_ADAPTER Adapter,UINT8 *data,UINT32 size,UINT32 address);
UINT32 ganges_send_dummy_write(IN PRSI_ADAPTER Adapter,IN UINT8 byte);
#endif

