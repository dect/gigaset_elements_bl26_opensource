/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Pine1_Lp
Module name  : WinCE driver
File Name    : channel_prog.h

File Description:
        channel programming values
List of functions:

Author :

Rev History:
Ver  By    date       Description
---------------------------------------------------------
1.1  Fariya 
---------------------------------------------------------
*END*************************************************************************/

#ifndef RSI_CFG_PROG_VAL_H
#define RSI_CFG_PROG_VAL_H

#if 0
typedef struct config_vals{
   UINT16 no_params;     
   UINT16 internalPmuEnabled;  // 1 or 0
   UINT16 ldoControlRequired;  // 1 or 0 
   UINT16 crystalFrequency;      //0 � 9.6MHz, 1 � 19.2MHz, 2 � 38.4MHz, 4 � 13MHz
                                                //5 � 26MHz, 6 � 52MHz, 8 � 20MHz, 9 � 40MHz
   UINT16 crystalGoodTime;
   UINT16 TAPLLEnabled;  // 1 or 0
   UINT16 TAPLLFrequency;
   UINT16 sleepClockSource;
   UINT16 voltageControlEnabled;
   UINT16 wakeupThresholdTime;

}CONFIG_VALS,*PCONFIG_VALS;
#endif

UINT16 rsi_config_vals[] = {
9,
0x0,
0x0,
0x9,
0x28,
0x0,
0x0,
0x0,
0x0,
//0xBB8
//0xFA0
0x1388,
//0x2000
0,
0,
0
};

#endif
