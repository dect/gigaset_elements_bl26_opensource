/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Ganges 
Module name  : LINUX-SDIO driver
File Name    : ganges_linux_ioctl.c

File Description:
This file contains the code for the ioctl part, GUI/supplicant communicates
with the driver through this interface. 

List of functions:
	ganges_ioctl
	ganges_get_name
	ganges_set_mode
	ganges_get_mode
	ganges_set_wap
	ganges_get_wap
	ganges_set_rtsthreshold
	ganges_get_rtsthreshold
	ganges_set_fragthreshold
	ganges_get_fragthreshold
	ganges_set_txpower
	ganges_get_txpower
	ganges_set_scan
	ganges_get_scan
	ganges_set_essid
	ganges_get_essid
	ganges_get_range
	ganges_set_encode
	ganges_get_encode
	ganges_set_auth
	ganges_get_auth
	ganges_wx_set_genie
	ganges_wx_set_encodeext
	ganges_set_pmksa
	ganges_indicate_connect_status
	ganges_indicate_scan_complete
	ganges_indicate_mic_failure

Author :
Rev History:
Ver  By    date       Description
---------------------------------------------------------
1.1  Anji  22 Nov06     Initial version
1.2  Fariya 
---------------------------------------------------------
*END*************************************************************************/

#include <linux/if_arp.h>
#include <net/iw_handler.h>	/* New driver API */

#include "ganges_linux.h"
#include "ganges_mgmt.h"
#include "ganges_nic.h"
#include "ganges_linux_ioctl.h"

GANGES_EXTERN GANGES_EVENT Event;
GANGES_EXTERN GANGES_EVENT PwrSaveEvent;
GANGES_EXTERN UINT8 *fsm_states_str[];
UINT32 req=0;

GANGES_STATIC const struct iw_priv_args privtab[]={ 
  {OID_RPS_SET_CHANNEL,IW_PRIV_TYPE_INT|IW_PRIV_SIZE_FIXED|1,0,"set_channel"},
  {OID_RPS_GET_CHANNEL,0,IW_PRIV_TYPE_INT|IW_PRIV_SIZE_FIXED|1,"get_channel"},

  {OID_RPS_SET_PROTOCOL_TYPE,IW_PRIV_TYPE_CHAR|IW_PRIV_SIZE_FIXED|1,0,"set_protocol"},
  {OID_RPS_GET_PROTOCOL_TYPE,0,IW_PRIV_TYPE_CHAR|IW_PRIV_SIZE_FIXED|1,"get_protocol"},

  {OID_RPS_SET_TX_PREAMBLE,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|1,0,"set_preamble"},
  {OID_RPS_GET_TX_PREAMBLE,0,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|1,"get_preamble"},

  {OID_RPS_SET_SLOT,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|1,0,"set_slot"},
  {OID_RPS_GET_SLOT,0,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|1,"get_slot"}, 

  {OID_RPS_SET_RSSI_NFM_MODE,IW_PRIV_TYPE_INT|IW_PRIV_SIZE_FIXED|1,0,"set_nfmmode"}, 
  {OID_RPS_GET_RSSI_NFM_MODE,0,IW_PRIV_TYPE_INT|IW_PRIV_SIZE_FIXED|1,"get_nfmmode"},

  {OID_RPS_SET_QOS_MODE,IW_PRIV_TYPE_INT|IW_PRIV_SIZE_FIXED|1,0,"set_qos_mode"},
  {OID_RPS_GET_QOS_MODE,0,IW_PRIV_TYPE_INT|IW_PRIV_SIZE_FIXED|1,"get_qos_mode"},

  {OID_RPS_SET_NO_ACK,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|1,0,"set_noack"},
  {OID_RPS_GET_NO_ACK,0,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|1,"get_noack"},

  {OID_RPS_SET_WMM_PWR_SAVE,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|sizeof(wmm_pwr_save_t),0,"set_wmm_pwrsave"},
  {OID_RPS_GET_WMM_PWR_SAVE,0,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|sizeof(wmm_pwr_save_t),"get_wmm_pwrsave"},

  {OID_RPS_SET_AMPDU_PARAMS,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|sizeof(ampdu_params_t),0,"set_ampdu_par"},
  {OID_RPS_GET_AMPDU_PARAMS,0,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|sizeof(ampdu_params_t),"get_ampdu_par"},

  {OID_RPS_SET_AMSDU_PARAMS,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|sizeof(amsdu_params_t),0,"set_amsdu_par"},
  {OID_RPS_GET_AMSDU_PARAMS,0,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|sizeof(amsdu_params_t),"get_amsdu_par"},

  {OID_RPS_SET_BLOCK_ACK,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|sizeof(block_ack_t),0,"set_blockack"},
  {OID_RPS_GET_BLOCK_ACK,0,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|sizeof(block_ack_t),"get_blockack"},

  {OID_RPS_GET_STATS,0,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|sizeof(station_stats_t),"get_stats"},

  {OID_RPS_SET_LINK_ADAPTATION,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|1,0,"set_link"},
  
  {OID_RPS_ADD_TSPEC,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|sizeof(tspec_t),0,"add_tspec"},  
 
  {OID_RPS_SET_TXRATE,IW_PRIV_TYPE_INT|IW_PRIV_SIZE_FIXED|1,0,"set_txrate"},  
  {OID_RPS_GET_TXRATE,0,IW_PRIV_TYPE_CHAR|IW_PRIV_SIZE_FIXED|13,"get_txrate"},
 
  {OID_RPS_SET_REG_PWR_SAVE,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|1,0,"set_reg_pwrsave"},
  {OID_RPS_GET_REG_PWR_SAVE,0,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|1,"get_reg_pwrsave"},

  {OID_RPS_RESET_ADAPTER,IW_PRIV_TYPE_NONE,0,"reset_adapter"},

  {OID_RPS_SET_PWR_SAVE_10BIT_MODE,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|1,0,"set_10b_mode"},
  {OID_RPS_GET_PWR_SAVE_10BIT_MODE,0,IW_PRIV_TYPE_BYTE|IW_PRIV_SIZE_FIXED|1,"get_10b_mode"}

};

/*FUNCTION*********************************************************************
Function Name  : ganges_ioctl
Description    : This function is the ioctl handler registered at the init time 
Returned Value : On success 0 is returned else a negative number
Parameters     :
-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to n/w device structure
ifr              |     |     |  X  | Ptr to ifreq structure 
cmd              |  X  |     |     | Ioctl cmd number 
END**************************************************************************/

INT32 
ganges_ioctl
  (
    struct net_device *dev, 
    struct ifreq *ifr, 
    INT32  cmd
  )
{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);  
  struct iwreq *wrq =(struct iwreq *)ifr;
  switch(cmd)
  {
    case SIOCGIWPRIV:
      {
        /*Export private ioctl info*/      
        if(wrq->u.data.pointer)
 	{
          RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl\n");
          wrq->u.data.length = sizeof(privtab)/sizeof(privtab[0]);
          if(ganges_copy_to_user(wrq->u.data.pointer,
			         privtab,
				 sizeof(privtab)))
	 {
	   RSI_DEBUG(RSI_ZONE_OID,
                     "ganges_ioctl: Failed to perform operation\n");
           return -EFAULT;
         }
        }	 
      break;         
      }

    case OID_RPS_GET_CHANNEL:
      {
        RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Querying the Channel num\n");
        if(Adapter->ScanAllChannel)
        {
	  wrq->u.param.value=0;
        }
        else
        {
  	  wrq->u.param.value=Adapter->ScanChannelNum;
        }
        break;
      }
    case OID_RPS_GET_PROTOCOL_TYPE:
      {
        RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Querying the protocol type\n");
        switch (Adapter->ProtocolType)
        {
          case ProtoType802_11A:
	    wrq->u.name[0]='A';
            break;
          case ProtoType802_11B:
	    wrq->u.name[0]='B';
            break;
	  case ProtoType802_11BG: //FIXME what we can do
          case ProtoType802_11G:
	    wrq->u.name[0]='G';
            break;
	  case ProtoType802_11N_mixed:  //FIXME what we can do
          case ProtoType802_11N:
	    wrq->u.name[0]='N';
            break;
        }
        break;
      }

    case OID_RPS_GET_TX_PREAMBLE:
      {
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: OID_RPS_GET_TX_PREAMBLE\n");
        wrq->u.name[0]=Adapter->preamble_type;	
        RSI_DEBUG(RSI_ZONE_OID, 
                  "ganges_ioctl: Tx Preamble: %d\n",wrq->u.name[0]);	
        break;
      }

    case OID_RPS_GET_SLOT:
      {
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: OID_RPS_GET_SLOT\n");
        wrq->u.name[0]=Adapter->slot_type;
        break;
      }

    case OID_RPS_GET_RSSI_NFM_MODE:
      { 
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: OID_RPS_RSSI_NFM_MODE\n");
        wrq->u.param.value = Adapter->rssi_nfm_mode;
        break;
      }

    case OID_RPS_GET_QOS_MODE:
      {
        RSI_DEBUG(RSI_ZONE_OID,
	          "ganges_ioctl: OID_RPS_QOS_MODE\n");
        wrq->u.param.value = Adapter->qos_type; 	
        break;
      }

    case OID_RPS_GET_NO_ACK:
      { 
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: OID_RPS_NO_ACK\n");
        wrq->u.name[0] = Adapter->no_ack;	
        break;
      }

    case OID_RPS_GET_WMM_PWR_SAVE:
      {
	 RSI_DEBUG(RSI_ZONE_OID,
	           "ganges_ioctl: OID_RPS_WMM_PWR_SAVE\n");
         ganges_memcpy(wrq->u.name,
                       &Adapter->pwr_save_param,
                       sizeof(wmm_pwr_save_t));
         break;
      }

    case OID_RPS_GET_AMPDU_PARAMS:
      {
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: OID_RPS_AMPDU_PARAMS\n");
	ganges_memcpy(wrq->u.name,
                      &Adapter->ampdu_param,
	              sizeof(ampdu_params_t)); 
        break;
      }
  
    case OID_RPS_GET_AMSDU_PARAMS:
      {
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: OID_RPS_AMSDU_PARAMS\n");
        ganges_memcpy(wrq->u.name,
                      &Adapter->amsdu_param,
                      sizeof(amsdu_params_t));
        break;
      }

    case OID_RPS_GET_BLOCK_ACK:
      {
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: OID_RPS_BLOCK_ACK\n");
	ganges_memcpy(wrq->u.name,
	              &Adapter->blk_ack_param,
	              sizeof(block_ack_t));	
        break;
      }

    case OID_RPS_GET_STATS:
      {
         RSI_DEBUG(RSI_ZONE_OID,
                   "ganges_ioctl: IOCTL_SDIO_GET_STATS\n");
         wrq->u.data.length = sizeof(station_stats_t);
         /* Copy the statistics information */
         if(ganges_copy_to_user(wrq->u.data.pointer,
		                &Adapter->sta_info,
			        sizeof(station_stats_t)))
	 {
	   RSI_DEBUG(RSI_ZONE_OID,
                     "ganges_ioctl: Failed to perform operation\n");
           return -EFAULT;
         }			   
         break;
      }

    case OID_RPS_GET_TXRATE:
      {
         RSI_DEBUG(RSI_ZONE_OID,
		   "ganges_ioctl: IOCTL_GET_TXRATE\n");         
         switch(Adapter->TxRate)
         {
           case 0:
                  ganges_strcpy(&wrq->u.name[0],"RATE_AUTO");
                  break;
           case 1:
                  ganges_strcpy(&wrq->u.name[0],"RATE_1_MBPS");
                  break;
           case 2: 
                  ganges_strcpy(&wrq->u.name[0],"RATE_2_MBPS");
                  break;
           case 3:
                  ganges_strcpy(&wrq->u.name[0],"RATE_5_5_MBPS");
                  break;
           case 4:
                  ganges_strcpy(&wrq->u.name[0],"RATE_11_MBPS");
                  break;
           case 5:
                  ganges_strcpy(&wrq->u.name[0],"RATE_6_MBPS");
                  break;
           case 6:
                  ganges_strcpy(&wrq->u.name[0],"RATE_9_MBPS");
                  break;
           case 7:
                  ganges_strcpy(&wrq->u.name[0],"RATE_12_MBPS");
                  break;
           case 8:
                  ganges_strcpy(&wrq->u.name[0],"RATE_18_MBPS");
                  break;
           case 9:
                  ganges_strcpy(&wrq->u.name[0],"RATE_24_MBPS");
                  break;
           case 10:
                  ganges_strcpy(&wrq->u.name[0],"RATE_36_MBPS");
                  break;
           case 11:
                  ganges_strcpy(&wrq->u.name[0],"RATE_48_MBPS");
                  break;           
           case 12:
                  ganges_strcpy(&wrq->u.name[0],"RATE_54_MBPS");
                  break;
           case 13:
                  ganges_strcpy(&wrq->u.name[0],"RATE_MCS0");
                  break;
           case 14:
                  ganges_strcpy(&wrq->u.name[0],"RATE_MCS1");
                  break;
           case 15:
                  ganges_strcpy(&wrq->u.name[0],"RATE_MCS2");
                  break;
           case 16:
                  ganges_strcpy(&wrq->u.name[0],"RATE_MCS3");
                  break;
           case 17:
                  ganges_strcpy(&wrq->u.name[0],"RATE_MCS4");
                  break;
           case 18:
                  ganges_strcpy(&wrq->u.name[0],"RATE_MCS5");
                  break;
           case 19:
                  ganges_strcpy(&wrq->u.name[0],"RATE_MCS6");
                  break;
           case 20:
                  ganges_strcpy(&wrq->u.name[0],"RATE_MCS7");
                  break;
           default:
                  break;
         }
         RSI_DEBUG(RSI_ZONE_OID,
                   "ganges_ioctl: Tx Rate is %s %d\n",wrq->u.name,Adapter->TxRate);
         break;
      }           

    case OID_RPS_GET_REG_PWR_SAVE:
      {
         RSI_DEBUG(RSI_ZONE_OID,
		   "ganges_ioctl: IOCTL_GET_REG_PWR_SAVE\n");
         wrq->u.name[0] = PS_FSM_STATE; 
         break;
      } 	
    case OID_RPS_GET_PWR_SAVE_10BIT_MODE:
      {
         RSI_DEBUG(RSI_ZONE_OID,
		   "ganges_ioctl: IOCTL_GET_PWRSAVE_10BIT_MODE\n");
         wrq->u.name[0] = Adapter->pwr_save_10bit_mode;
         break;
      } 
    case OID_RPS_RESET_ADAPTER:
      {
         RSI_DEBUG(RSI_ZONE_OID,
		   "ganges_ioctl: RESET MAC\n");
         ganges_do_adapter_Reset(Adapter); 
	 break;
      }  
    case OID_RPS_SET_CHANNEL:
      {
        UINT32 Channelnum = wrq->u.param.value;
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: Received Set OID_RPS_CHANNEL\n");
        if(Channelnum<0 || Channelnum>11)
        {
          RSI_DEBUG(RSI_ZONE_OID,
		    "ganges_ioctl: Invalid channel num\n");
          break;
        } 
        else
        {        
          Adapter->selectedChannelNum = Channelnum;
          ganges_set_channel_type(Adapter, Channelnum);
        }
       break;
      }

    case OID_RPS_SET_PROTOCOL_TYPE:
      {
        UINT8 protocoltype = wrq->u.name[0];
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: OID_RPS_SET_PROTOCOL_TYPE\n");
        if(protocoltype=='a' || protocoltype=='A')
        {
          RSI_DEBUG(RSI_ZONE_OID,
                    "ganges_ioctl: Setting Protocol type 'A'\n");
          Adapter->ProtocolType = WLAN_802_11_A;
          ganges_set_protocol_type(Adapter,WLAN_802_11_A);
        }
        else if(protocoltype=='b' || protocoltype=='B')
        {
          RSI_DEBUG(RSI_ZONE_OID,
                    "ganges_ioctl: Setting Protocol type 'B'\n");
          Adapter->ProtocolType = WLAN_802_11_B;
          ganges_set_protocol_type(Adapter,WLAN_802_11_B);
        }
        else if(protocoltype=='g' || protocoltype=='G')
        {
          RSI_DEBUG(RSI_ZONE_OID,
                    "ganges_ioctl: Setting Protocol type 'G'\n");
          Adapter->ProtocolType = WLAN_802_11_G;
          ganges_set_protocol_type(Adapter,WLAN_802_11_G);
        }
        else if(protocoltype=='n' || protocoltype=='N')
        {
          RSI_DEBUG(RSI_ZONE_OID,
                    "ganges_ioctl: Setting Protocol type 'N'\n");
          Adapter->ProtocolType = WLAN_802_11_N;
          ganges_set_protocol_type(Adapter,WLAN_802_11_N);
        }
        else
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
		    "ganges_ioctl: Invalid protocol type\n");
        } 
      }
      break;
    case OID_RPS_SET_TX_PREAMBLE:
      {
        UINT8 preamble_type = wrq->u.name[0];
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: OID_RPS_SET_TX_PREAMBLE->\n");
        if(preamble_type==1)
        {
          RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Auto Preamble mode\n");
          Adapter->preamble_type = RSI_PREAMBLE_AUTO;
        }

        else if(preamble_type==0)
        {
          RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl:Long Preamble mode\n");  
          Adapter->preamble_type = RSI_PREAMBLE_LONG;
        }
        else
        { 
          RSI_DEBUG(RSI_ZONE_ERROR,"ganges_ioctl: Invalid Tx Preamble\n");
        }
        break;
      }
    case OID_RPS_SET_SLOT:
      {
        UINT8 slot_type = wrq->u.name[0];
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: OID_RPS_SET_SLOT\n");
        if(slot_type==1)
        { 
          RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Setting to Auto mode\n");
          Adapter->slot_type = RSI_SLOT_AUTO;
        }			
        else if(slot_type==0)
        {
          RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Setting to Long mode\n");
          Adapter->slot_type = RSI_SLOT_LONG;
        }
        else
        {
          RSI_DEBUG(RSI_ZONE_ERROR,"ganges_ioctl: Invalid slot type\n");
        }
        break;
      }   	
    case OID_RPS_SET_QOS_MODE:
      {
        Adapter->qos_type = wrq->u.param.value;                 
        RSI_DEBUG(RSI_ZONE_OID,
                   "ganges_ioctl: OID_RPS_QOS_MODE:  %d\n", Adapter->qos_type);
        break;
      }
    case OID_RPS_SET_NO_ACK:
      {
        UINT8 no_ack = wrq->u.name[0];
        Adapter->no_ack = no_ack;
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: OID_RPS_NO_ACK:  %d\n", Adapter->no_ack);
        break;
      }
    case OID_RPS_SET_LINK_ADAPTATION:
      {
 
	RSI_DEBUG(RSI_ZONE_OID,
		  "ganges_ioctl: OID_RPS_SET_LINK_ADAPTATION\n");
        Adapter->enable_link_adaptation = wrq->u.name[0];
        break;
      }
    case OID_RPS_SET_WMM_PWR_SAVE:
      {
        RSI_DEBUG(RSI_ZONE_OID,
	          "ganges_ioctl: OID_RPS_SET_WMM_PWR_SAVE recvd\n");
        ganges_memcpy(&Adapter->pwr_save_param,
                      wrq->u.name,
                      sizeof(wmm_pwr_save_t));
        if(Adapter->pwr_save_param.enable)   
        {
          RSI_DEBUG(RSI_ZONE_OID,
                    "ganges_ioctl: WMM Pwr save enabled \nvo:%d  vi:%d  bk:%d  be:%d\n",
                    Adapter->pwr_save_param.ac_vo,
                    Adapter->pwr_save_param.ac_vi,
                    Adapter->pwr_save_param.ac_bk,
                    Adapter->pwr_save_param.ac_be);
        }
        break;
      }
    case OID_RPS_SET_AMSDU_PARAMS:
      {
	RSI_DEBUG(RSI_ZONE_OID,
		  "ganges_ioctl: OID_RPS_SET_AMSDU_PARAMS\n");
        ganges_memcpy(&Adapter->amsdu_param,
                      wrq->u.name,
                      sizeof(amsdu_params_t));

        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: AMSDU %d\n",Adapter->amsdu_param.enable);
          
        if(Adapter->amsdu_param.enable)
        {
          RSI_DEBUG(RSI_ZONE_OID,
                    "ganges_ioctl: max_length: %d\n",Adapter->amsdu_param.max_amsdu_len);
        }
        break;
      }
    case OID_RPS_SET_BLOCK_ACK:
      {
        RSI_DEBUG(RSI_ZONE_OID,
		  "ganges_ioctl: OID_RPS_SET_BLOCK_ACK\n"); 		
        ganges_memcpy(&Adapter->blk_ack_param,
	              wrq->u.name,
	              sizeof(block_ack_t));
        if(Adapter->blk_ack_param.txenable)
        {
          RSI_DEBUG(RSI_ZONE_OID,
		    "ganges_ioctl: Implicit block ack: %d",Adapter->blk_ack_param.implicit_ba);
	}   	
        break;
      } 
    case OID_RPS_SET_AMPDU_PARAMS:
      {
	ganges_memcpy(&Adapter->ampdu_param,
	              wrq->u.name,
	              sizeof(ampdu_params_t));		
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: Setting AMPDU param: %d",Adapter->ampdu_param.enable);
        break;
      }

      case OID_RPS_SET_RSSI_NFM_MODE:
      {
        UINT8 rssi_nfm_mode = wrq->u.name[0]; 
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: OID_RPS_RSSI_NFM_MODE\n");
        if((rssi_nfm_mode == RPS_RSSI_NFM_PERIODIC) &&
           (Adapter->rssi_nfm_mode == RPS_RSSI_NFM_PERIODIC))
        {
          RSI_DEBUG(RSI_ZONE_OID,
                    "ganges_ioctl: Already in periodic mode\n");
          break;
        }  
        Adapter->rssi_nfm_mode = rssi_nfm_mode;
        RSI_DEBUG(RSI_ZONE_OID,"ioctl: set NFM mode %d\n",
                                Adapter->rssi_nfm_mode);
        if(Adapter->rssi_nfm_mode == RPS_RSSI_NFM_POWERUP)
        {
          RSI_DEBUG(RSI_ZONE_OID,
                    "ganges_ioctl:sending measure POWERUP RSSI NFM req\n");
          ganges_send_measure_noise_rssi_request(Adapter,
                                                 RPS_RSSI_PWRUP_NFM_CNT);
        }
        else if(Adapter->rssi_nfm_mode == RPS_RSSI_NFM_PERIODIC)
        {
          RSI_DEBUG(RSI_ZONE_OID,
                    "ganges_ioctl: setting to RSSI PERIODIC NFM mode\n");
          Adapter->periodic_rssi_nfm_smpl_cnt = 0;
        }
        break;
      }
    case OID_RPS_SET_TXRATE:
      {
        RSI_DEBUG(RSI_ZONE_OID,
		  "ganges_ioctl: OID_RPS_SET_TXRATE\n");
        if(wrq->u.param.value < 0 || wrq->u.param.value>20)
        {
          RSI_DEBUG(RSI_ZONE_OID,
                    "ganges_ioctl: Invalid Tx Rate\n");
        }  
        else 
        {
          Adapter->TxRate = wrq->u.param.value;
          ganges_set_txrate(Adapter,Adapter->TxRate);
        }
        break;
      }
    case OID_RPS_SET_REG_PWR_SAVE:
      {
	UINT8 byte=1;
	UINT32 status;
        req = wrq->u.name[0];
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: IOCTL_SET_REG_PWR_SAVE: %d \n",PS_FSM_STATE);
        if((req==0)||(req>4))
         break;
        switch(FSM_STATE)
        {
          case FSM_NO_STATE:
            {
	      RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: FSM_NO_STATE\n");	
	      if((req == PS_DISABLE_REQ)&& 
                 (PS_FSM_STATE != PS_FSM_DISABLE))
              {
	        RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Disabling Pwr save\n");
	        ganges_Reset_Event(&PwrSaveEvent);
	        ganges_send_ssp_wakeup_cmd(Adapter,byte);
    	        ganges_send_ssp_wakeup_cmd_read(Adapter);
		Adapter->halt_flag=1;
		RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Waiting for deep sleep cfm\n");
                status = ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);
		RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Wokeup on sleep cfm event\n");
                PS_FSM_STATE = req;
	      } 
	      else
              {
		if((PS_FSM_STATE == PS_FSM_DISABLE) && (req != PS_DISABLE_REQ))
	    	{
	          ganges_Reset_Event(&PwrSaveEvent);
                  ganges_send_power_save_req(Adapter,DEEP_SLEEP_REQUEST);
                  status = ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);		  
		  RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Wokeup on sleep cfm event\n");
                }                
                PS_FSM_STATE = req;
		RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: PS_FSM_STATE is: %d\n",PS_FSM_STATE);
              }
              break; 
            }
	  case FSM_SCAN_REQ_SENT:
	  case FSM_JOIN_REQ_SENT:
	  case FSM_AUTH_REQ_SENT:
	  case FSM_ASSOCIATE_REQ_SENT:
            {
	      RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Present PS_FSM_STATE is:%d\n",req);
              PS_FSM_STATE = req;  
              break;
            } 
          case FSM_OPEN:
            {
	      switch(req)
                {
                  case PS_DISABLE_REQ: 
                    if((PS_FSM_STATE != PS_FSM_DISABLE)&&
		       (PS_FSM_STATE != PS_FSM_NORMAL))
                    { 
	              RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Disabling Pwr save: %d\n",PS_FSM_STATE);
	              ganges_Reset_Event(&PwrSaveEvent);
		      if(PS_FSM_STATE == PS_FSM_AUTO)	
	               ganges_send_power_save_req(Adapter,DISABLE_PWR_SAVE); 
                      else
                      { 
	        	ganges_send_ssp_wakeup_cmd(Adapter,byte);
	                ganges_send_ssp_wakeup_cmd_read(Adapter);
		        Adapter->halt_flag=1;
		      }	
                      status = ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);
		      RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Wokeup on wakeup cfm event\n");
                    }
                    PS_FSM_STATE = PS_FSM_DISABLE;
                    break;
		  case PS_NORMAL_REQ:
                    if(PS_FSM_STATE == PS_FSM_DEEP_SLEEP)
                    {
	              ganges_Reset_Event(&PwrSaveEvent);
	              ganges_send_ssp_wakeup_cmd(Adapter,byte);
	              ganges_send_ssp_wakeup_cmd_read(Adapter);
		      Adapter->halt_flag=1;
                      ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);
		      RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Wokeup on wakeup cfm event\n");
                    }
		    else if (PS_FSM_STATE == PS_FSM_AUTO)
                    {
	              RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Disabling Pwr save\n");
	              ganges_Reset_Event(&PwrSaveEvent);
	              ganges_send_power_save_req(Adapter,DISABLE_PWR_SAVE);
                      ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);
		      RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Wokeup on wakeup cfm event\n");
                    }
                    PS_FSM_STATE = PS_FSM_NORMAL;
		    break;
                  case PS_DEEP_SLEEP_REQ:
		    if(PS_FSM_STATE == PS_FSM_AUTO)
		    {
	              ganges_Reset_Event(&PwrSaveEvent);
		      ganges_send_power_save_req(Adapter,DISABLE_PWR_SAVE);
	              RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Disabling Pwr save\n");
                      status = ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);
		      RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Wokeup on wakeup cfm event\n");
		      ganges_send_DeAuthentication_request(Adapter,
		     				           Adapter->connectedAP.BSSID,
							   0x03);     		
		      FSM_STATE = FSM_NO_STATE;	
	              ganges_Reset_Event(&PwrSaveEvent);
                      status = ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);
		      RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Wokeup on event\n");
		      PS_FSM_STATE = PS_FSM_DEEP_SLEEP;
		    }
		    else if((PS_FSM_STATE == PS_FSM_NORMAL)||
                            (PS_FSM_STATE == PS_FSM_DISABLE))
                    {
		      ganges_send_DeAuthentication_request(Adapter,
		     				           Adapter->connectedAP.BSSID,
							   0x03);     		
		      FSM_STATE = FSM_NO_STATE;	
		      PS_FSM_STATE = PS_FSM_DEEP_SLEEP;
	            }
		    break;
		  case PS_AUTO_REQ: 
                    if((PS_FSM_STATE == PS_FSM_NORMAL)||
		       (PS_FSM_STATE == PS_FSM_DISABLE))
                    {
	              RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Enabling Regular sleep\n");
	              ganges_Reset_Event(&PwrSaveEvent);
                      ganges_send_power_save_req(Adapter,REGULAR_SLEEP_REQUEST);
                      status = ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);
		      RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Wokeup on sleep cfm event\n");
  		    }
                    PS_FSM_STATE = PS_FSM_AUTO;
	            break;
		  default:
		    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_ioctl: Invalid request\n");
	            break;
                }  
		break;
            }        
            default:
	      RSI_DEBUG(RSI_ZONE_ERROR,"ganges_ioctl: Invalid state");
          break;
        }    	  
	RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Present PS_FSM_STATE is:%d\n",req);
      break;
      }    	       
    case OID_RPS_ADD_TSPEC:
      {
        
        RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: OID_RPS_ADD_TSPEC recvd\n");
		
        wrq->u.data.length = sizeof(tspec_t);
        if(ganges_copy_from_user(&Adapter->tspec_param,
                                 wrq->u.data.pointer,
                                 sizeof(tspec_t)))
        {
	   RSI_DEBUG(RSI_ZONE_OID,
                     "ganges_ioctl: Failed to perform operation\n");
           return -EFAULT;
        }		
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: tspec.tsid: %d", Adapter->tspec_param.tsid);
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: tspec.nom_msdu_size: %d", Adapter->tspec_param.nom_msdu_size);
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: tspec.max_msdu_size: %d", Adapter->tspec_param.max_msdu_size);
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: tspec.ack_policy: %d", Adapter->tspec_param.ack_policy);
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: tspec.access_policy: %d", Adapter->tspec_param.access_policy);
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: tspec.min_data_rate: %d", Adapter->tspec_param.min_data_rate);
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: tspec.mean_data_rate: %d", Adapter->tspec_param.mean_data_rate);
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: tspec.peak_data_rate: %d", Adapter->tspec_param.peak_data_rate);
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: tspec.min_phy_rate: %d", Adapter->tspec_param.min_phy_rate);
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: tspec.burst_size: %d", Adapter->tspec_param.burst_size);
        RSI_DEBUG(RSI_ZONE_OID,
                  "ganges_ioctl: tspec.delay_bound: %d", Adapter->tspec_param.delay_bound);
        break;
      }

    case OID_RPS_SET_PWR_SAVE_10BIT_MODE:
      {
        UINT16 bb_reg[1];
        if(Adapter->AFE_type != RSI_AFE_2)
        {
          RSI_DEBUG(RSI_ZONE_OID,
                    "ganges_ioctl: OID_RPS_SET_PWR_SAVE_10BIT_MODE allowed only for AFE2\n");
          break;
        } 
        Adapter->pwr_save_10bit_mode = wrq->u.name[0];
        RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: OID_RPS_SET_PWR_SAVE_10BIT_MODE: %d\n",
		  Adapter->pwr_save_10bit_mode);
        if(Adapter->pwr_save_10bit_mode)
        {
          bb_reg[0] = 0x8800;
        }
        else
        {
          bb_reg[0] = 0x8900;
        }
        ganges_send_program_BB_registers(Adapter,1,bb_reg);
        break;
      }    

     default:
       RSI_DEBUG(RSI_ZONE_OID, "%s: Unknow ioctl 0x%x\n",__FUNCTION__, cmd);
       return -EOPNOTSUPP;
  }
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_get_name
Description    : This function returns the protocol name 
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
cwrq             |     |  X  |     | Char ptr
extra            |     |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_get_name
  (
    struct net_device *dev,
    struct iw_request_info *info,
    INT8   *cwrq,
    INT8   *extra
  )
{
  RSI_DEBUG(RSI_ZONE_OID, "%s : is called \n", __FUNCTION__ );
  ganges_strcpy(cwrq, "IEEE 802.11");
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_set_mode
Description    : This function sets the operation mode to Infrastructure/IBSS 
		 mode 
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
uwrq             |  X  |     |     | Integer ptr
extra            |     |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_set_mode
  (
    struct net_device *dev,
    struct iw_request_info *info,
    __u32 *uwrq,
    INT8  *extra
  )
{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);  

  if (*uwrq == IW_MODE_ADHOC )
  {
    RSI_DEBUG(RSI_ZONE_OID, "%s: trying to set ADHOC\n", __FUNCTION__);
    Adapter->NetworkType = Ndis802_11IBSS;
  }
  else if(*uwrq == IW_MODE_INFRA)
  {
    RSI_DEBUG(RSI_ZONE_OID, "%s: trying to set INFRA\n", __FUNCTION__);
    Adapter->NetworkType = Ndis802_11Infrastructure;
  } 
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_get_mode
Description    : This function returns the operation mode which is either
		 Infrastructure mode/Adhoc mode in our case  	
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
uwrq             |     |  X  |     | Integer ptr
extra            |     |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_get_mode
  (
    struct net_device *dev,
    struct iw_request_info *info,
    __u32 *uwrq,
    INT8  *extra
 )
{	
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);
  RSI_DEBUG(RSI_ZONE_OID, "%s: SIOCGIWMODE\n", __FUNCTION__);
  if(Adapter->NetworkType==Ndis802_11IBSS)
    *uwrq = IW_MODE_ADHOC;
  else
    *uwrq = IW_MODE_INFRA;
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_set_wap
Description    : This function sets the AP's MAC address, the station should
	         connect to
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
awrq             |  X  |     |     | Ptr to sockaddr structure
extra            |     |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_set_wap
  (
    struct net_device *dev,
    struct iw_request_info *info,
    struct sockaddr *awrq,
    INT8   *extra
  )
{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev); 
  UINT8 ii; 

  RSI_DEBUG(RSI_ZONE_OID, "%s : is called \n", __FUNCTION__ );
  if (awrq->sa_family != ARPHRD_ETHER)
  {
    RSI_DEBUG(RSI_ZONE_ERROR, "%s: not ether family\n", __FUNCTION__ );
    return -EINVAL;
  }
  for(ii=0;ii<6;ii++)
    RSI_DEBUG(RSI_ZONE_OID,"%0x:", awrq->sa_data[ii]); 
  RSI_DEBUG(RSI_ZONE_OID,"\n"); 
 
  if(ganges_memcmp(&awrq->sa_data,"\0\0\0\0\0\0",ETH_ADDR_LEN)==0)
  {
    if(Adapter->NetworkType==Ndis802_11IBSS)
    {
      if((FSM_STATE != FSM_OPEN) && (FSM_STATE != FSM_IBSS_CREATED))
      {
        RSI_DEBUG(RSI_ZONE_OID,"%s: Not in OPEN state\n",__FUNCTION__); 
        return 0;
      }
      else
      {
        RSI_DEBUG(RSI_ZONE_OID,"%s: Sending reset MAC frame\n",__FUNCTION__);
        ganges_send_reset_mac(Adapter);
        ganges_send_rate_symbol_request(Adapter);
        FSM_STATE = FSM_NO_STATE; 
      }
    }
    else
    {
      if(FSM_STATE != FSM_OPEN) 
      {
        RSI_DEBUG(RSI_ZONE_OID,"%s: Not in OPEN state\n",__FUNCTION__); 
        return 0;
      }
      if((PS_FSM_STATE != PS_FSM_DISABLE) && (PS_FSM_STATE != PS_FSM_NORMAL))
      {	
        ganges_Reset_Event(&PwrSaveEvent);
        ganges_send_power_save_req(Adapter,DISABLE_PWR_SAVE);
        RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Disabling Pwr save\n");
        ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);
	RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Wokeup on event\n");
      }

      RSI_DEBUG(RSI_ZONE_OID,"%s: Sending deauthentication frame\n",__FUNCTION__);
      ganges_send_DeAuthentication_request(Adapter,
                                           Adapter->connectedAP.BSSID,
	   				   0x03);
      FSM_STATE = FSM_NO_STATE;
    }
  }   
  else
  {
    ganges_memcpy(Adapter->pbssid,awrq->sa_data,ETH_ADDR_LEN);
    ganges_set_bssid(Adapter,Adapter->pbssid);
  }
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_get_wap
Description    : This function gets the connected AP's MAC address
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
awrq             |     |  X  |     | Ptr to sockaddr structure
extra            |     |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_get_wap
  (
    struct net_device *dev,
    struct iw_request_info *info,
    struct sockaddr *awrq,
    INT8   *extra
  )
{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);  
  
  /* we should return connected BSSID */
  // ganges_memcpy(&awrq->sa_data, priv->bssid, WLAN_BSSID_LEN);
  if(FSM_STATE != FSM_OPEN)
  {
    RSI_DEBUG(RSI_ZONE_OID, "%s: Not in open state\n",__FUNCTION__ );
    return -EFAULT;
  }
#define WLAN_BSSID_LEN 6
  ganges_memcpy(&awrq->sa_data, &Adapter->connectedAP.BSSID[0], WLAN_BSSID_LEN);
  RSI_DEBUG(RSI_ZONE_OID, "%s:  %02x:%02x:%02x:%02x:%02x:%02x\n",
                         __FUNCTION__,
                         Adapter->connectedAP.BSSID[0],
                         Adapter->connectedAP.BSSID[1],
                         Adapter->connectedAP.BSSID[2],
                         Adapter->connectedAP.BSSID[3],
                         Adapter->connectedAP.BSSID[4],
                         Adapter->connectedAP.BSSID[5]);
  awrq->sa_family = ARPHRD_ETHER;
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_set_rtsthreshold
Description    : This function sets the RTS threshold 
Returned Value : On success 0 is returned else a negative value
Parameters     :
  
-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
wrq              |  X  |     |     | Ptr to iw_param structure
extra            |     |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_set_rtsthreshold
  (
    struct net_device *dev,
    struct iw_request_info *info,
    struct iw_param *wrq,
    INT8   *extra
  )
{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);
  UINT32  RTS_threshold = wrq->value;
  RSI_DEBUG(RSI_ZONE_OID, "%s : is called \n", __FUNCTION__ );

  if(RTS_threshold%4)
  {
    RTS_threshold += 4-(RTS_threshold%4);
  }

  if(RTS_threshold<MIN_THRESHOLD || RTS_threshold >MAX_THRESHOLD)
  {
    RSI_DEBUG(RSI_ZONE_OID,"%s: Setting to default RTS threshold value\n",
              __FUNCTION__);
    RTS_threshold = DFL_RTS_THRSH - 28;
  }  
  Adapter->rts_threshold = RTS_threshold;
  return 0;
}


/*FUNCTION*********************************************************************
Function Name  : ganges_get_rtsthreshold
Description    : This function returns the station's RTS threshold 
Returned Value : On success 0 is returned else a negative value
Parameters     :
  
-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
wrq              |     |  X  |     | Ptr to iw_param structure
extra            |     |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_get_rtsthreshold
  (
    struct net_device *dev,
    struct iw_request_info *info,
    struct iw_param *wrq,
    INT8    *extra
  )
{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);
  RSI_DEBUG(RSI_ZONE_OID, "%s : is called \n", __FUNCTION__ );
  wrq->value = Adapter->rts_threshold;
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_set_fragthreshold
Description    : This function sets the station's fragmentation threshold 
Returned Value : On success 0 is returned else a negative value
Parameters     :
  
-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
wrq              |  X  |     |     | Ptr to iw_param structure
extra            |     |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_set_fragthreshold
  (
    struct net_device *dev,
    struct iw_request_info *info,
    struct iw_param *wrq,
    INT8   *extra
  )
{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);
  UINT32       frag_threshold = wrq->value;
  RSI_DEBUG(RSI_ZONE_OID, "%s : is called \n", __FUNCTION__ );
  if(frag_threshold%4)
  {
    frag_threshold += 4-(frag_threshold%4);
  }
  if(frag_threshold<MIN_THRESHOLD || frag_threshold >MAX_THRESHOLD)
  {
    RSI_DEBUG(RSI_ZONE_OID,"%s: Setting to default frag_threshold value\n",
	      __FUNCTION__);
    frag_threshold = DFL_FRAG_THRSH-28;
  }
  Adapter->frag_threshold = frag_threshold;
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_get_fragthreshold
Description    : This function gets the station's fragmentation threshold 
Returned Value : On success 0 is returned else a negative value
Parameters     :
  
-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
wrq              |     |  X  |     | Ptr to iw_param structure
extra            |     |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_get_fragthreshold
  (
    struct net_device *dev,
    struct iw_request_info *info,
    struct iw_param *wrq,
    INT8   *extra
  )
{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);
  RSI_DEBUG(RSI_ZONE_OID, "%s : is called \n", __FUNCTION__ );
  wrq->value = Adapter->frag_threshold;
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_set_txpower
Description    : This function sets the station's transmit power(dBm) 
Returned Value : On success 0 is returned else a negative value
Parameters     :
  
-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
wrq              |  X  |     |     | Ptr to iw_param structure
extra            |     |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_set_txpower
  (
    struct net_device *dev,
    struct iw_request_info *info,
    struct iw_param *wrq,
    INT8   *extra
  )
{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);
  RSI_DEBUG(RSI_ZONE_OID, "%s : is called \n", __FUNCTION__ );
  Adapter->txPower = wrq->value; 
#if 0
  txpower = wrq->value;
  switch(txpower)
  {
    case 11:
            RSI_DEBUG(RSI_ZONE_OID,"Setting tx power to 11dbm\n");
            Adapter->txPower = RPS_POWER_11dBm;
            break;
    case 13:
            RSI_DEBUG(RSI_ZONE_OID,"Setting tx power to 13dbm\n");
            Adapter->txPower = RPS_POWER_13dBm;
            break;
    case 15:
            RSI_DEBUG(RSI_ZONE_OID,"Setting tx power to 15dbm\n");
            Adapter->txPower = RPS_POWER_15dBm;
            break;
    case 17:
            RSI_DEBUG(RSI_ZONE_OID,"Setting tx power to 17dbm\n");
            Adapter->txPower = RPS_POWER_17dBm;
            break;
    default:
           RSI_DEBUG(RSI_ZONE_OID,"Unsupported txpower\n"); 
  }
#endif
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_get_txpower
Description    : This function gets the station's transmit power(dBm) 
Returned Value : On success 0 is returned else a negative value
Parameters     :
  
-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
wrq              |     |  X  |     | Ptr to iw_param structure
extra            |     |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_get_txpower
  (
    struct net_device *dev,
    struct iw_request_info *info,
    struct iw_param *wrq,
    INT8   *extra
  )
{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);
  RSI_DEBUG(RSI_ZONE_OID, "%s : is called \n", __FUNCTION__ );
  wrq->value = Adapter->txPower;
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_set_scan
Description    : This function triggers the scanning 
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
vwrq             |     |     |     | Ptr to iw_param structure
extra            |     |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_set_scan
  (
    struct net_device *dev,
    struct iw_request_info *info,
    struct iw_point *data,
    INT8   *extra
  )
{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);  
  struct iw_scan_req *req = (struct iw_scan_req *)extra;
  RSI_DEBUG(RSI_ZONE_OID, "%s : is called \n", __FUNCTION__ );
  if((FSM_STATE == FSM_NO_STATE) || (FSM_STATE == FSM_OPEN))
  {
    if(data->length < sizeof(struct iw_scan_req))
    {
       req = NULL;
    } 
    if(req && data->flags & IW_SCAN_THIS_ESSID) 
    {
      RSI_DEBUG(RSI_ZONE_OID,"%s: Checking for hidden networks\n",__FUNCTION__);
      Adapter->hidden_mode=1;
      memcpy(&Adapter->scan_ssid.Ssid,req->essid,req->essid_len);
      Adapter->scan_ssid.SsidLength = req->essid_len;
      RSI_DEBUG(RSI_ZONE_OID,"%s: Scanning for Network name: %s of length %d\n",
                __FUNCTION__, 
                Adapter->scan_ssid.Ssid,Adapter->scan_ssid.SsidLength); 
      ganges_do_scan(Adapter,SSID_SPECIFIC_SCAN,&Adapter->scan_ssid);
    }
    else
    {
      RSI_DEBUG(RSI_ZONE_OID,
                "%s: Sending Broadcast scan request %d\n",__FUNCTION__,FSM_STATE);
      Adapter->hidden_mode=0;
      ganges_do_scan(Adapter,0,NULL);
    }
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"%s: Scan request not allowed in this state: %s\n",
	      __FUNCTION__,
              fsm_states_str[FSM_STATE]); 
  }
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_get_scan
Description    : This function gives all the scanned results by means of events
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
dwrq             |     |  X  |     | Ptr to iw_point structure
extra            |     |  X  |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_get_scan
  (
    struct net_device *dev,
    struct iw_request_info *info,
    struct iw_point *dwrq,
    INT8   *extra
  )
{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);  
  INT32  ii,jj,channel_num=0;
  INT8   *current_ev = extra;
  struct iw_event	iwe;

  RSI_DEBUG(RSI_ZONE_OID, "%s : is called \n", __FUNCTION__ );
  
  if((FSM_STATE == FSM_SCAN_REQ_SENT) ||
    (FSM_STATE == FSM_SCAN_REQ_SENT_INOPEN))
  {
    /* scanning in progress */
    RSI_DEBUG(RSI_ZONE_OID, "%s:Scan in progress\n", __FUNCTION__ );
    return -EAGAIN;
  }
  RSI_DEBUG(RSI_ZONE_OID, "%s: available APs %d\n", 
       	    __FUNCTION__, Adapter->BssidFound);
  for(ii=0; ii<Adapter->BssidFound; ii++) 
  { 
    RSI_DEBUG(RSI_ZONE_OID, "%s: AP %d\n", __FUNCTION__,ii);
    iwe.cmd = SIOCGIWAP;
    iwe.u.ap_addr.sa_family = ARPHRD_ETHER;
    ganges_memcpy(iwe.u.ap_addr.sa_data,Adapter->scanConfirmVarArray[ii]->BSSID, 6);
    current_ev = iwe_stream_add_event(current_ev, 
                                      extra + IW_SCAN_MAX_DATA, 
                                      &iwe, 
                                      IW_EV_ADDR_LEN);

    iwe.u.data.length =  Adapter->scanConfirmVarArray[ii]->SSIDLen;
    if (iwe.u.data.length > 32)
      iwe.u.data.length = 32;
    iwe.cmd = SIOCGIWESSID;
    iwe.u.data.flags = 1;
    current_ev = iwe_stream_add_point(current_ev, 
                                      extra + IW_SCAN_MAX_DATA, 
                                      &iwe, 
                                      Adapter->scanConfirmVarArray[ii]->SSID);

    RSI_DEBUG(RSI_ZONE_OID, "%s: SSID Length %d\t",__FUNCTION__,iwe.u.data.length);
    for(jj=0;jj<iwe.u.data.length;jj++)
     RSI_DEBUG(RSI_ZONE_OID, "%c",Adapter->scanConfirmVarArray[ii]->SSID[jj]); 
    RSI_DEBUG(RSI_ZONE_OID,"\n");
		
    iwe.cmd = SIOCGIWMODE;
    if(Adapter->scanConfirmVarArray[ii]->CapabilityInfo & 0x0002) 
    {
      iwe.u.mode = IW_MODE_ADHOC;
      RSI_DEBUG(RSI_ZONE_OID, "%s: ADHOC ",__FUNCTION__);
    }
    else
    {
      iwe.u.mode = IW_MODE_INFRA;
      RSI_DEBUG(RSI_ZONE_OID, "%s: INFRA ",__FUNCTION__);
    }
    current_ev = iwe_stream_add_event(current_ev, 
                                      extra + IW_SCAN_MAX_DATA, 
                                      &iwe, 
                                      IW_EV_UINT_LEN);
#define WLAN_BASE_FREQ 2412	
    iwe.cmd = SIOCGIWFREQ;
    iwe.u.freq.e = 1;
    channel_num  = Adapter->scanConfirmVarArray[ii]->PhyParam[0];
    iwe.u.freq.m = (WLAN_BASE_FREQ + 5*(channel_num-1)) * 100000; //MHz
    RSI_DEBUG(RSI_ZONE_OID, "%s: CH:%d ",
			    __FUNCTION__,
                            Adapter->scanConfirmVarArray[ii]->PhyParam[0]);
    current_ev = iwe_stream_add_event(current_ev, 
                                      extra + IW_SCAN_MAX_DATA, 
                                      &iwe, 
                                      IW_EV_FREQ_LEN);
		
    iwe.cmd = IWEVQUAL;
    iwe.u.qual.level   = 0x100 - Adapter->scanConfirmVarArray[ii]->signal_lvl; //dDm
    iwe.u.qual.updated = IW_QUAL_LEVEL_UPDATED | IW_QUAL_DBM; 
    RSI_DEBUG(RSI_ZONE_OID, "%s: RSSI %d\n ",__FUNCTION__,iwe.u.qual.level);
    current_ev = iwe_stream_add_event(current_ev,
				      extra + IW_SCAN_MAX_DATA,
				      &iwe,
				      IW_EV_QUAL_LEN);			
		
    iwe.cmd = SIOCGIWENCODE;
    if(Adapter->scanConfirmVarArray[ii]->CapabilityInfo & 0x0010) 
    {
      RSI_DEBUG(RSI_ZONE_OID, "%s: SECU ",__FUNCTION__);
      iwe.u.data.flags = IW_ENCODE_ENABLED | IW_ENCODE_NOKEY;
    }
    else
    {
      RSI_DEBUG(RSI_ZONE_OID, "%s: OPEN ",__FUNCTION__);
      iwe.u.data.flags = IW_ENCODE_DISABLED;
    }
    iwe.u.data.length = 0;
    current_ev = iwe_stream_add_point(current_ev, 
                                      extra + IW_SCAN_MAX_DATA, 
                                      &iwe, 
                                      NULL);
    if(Adapter->scanConfirmVarArray[ii]->wpa_capable)
    {
      RSI_DEBUG(RSI_ZONE_OID, "%s: WPA ",__FUNCTION__);
      ganges_memset(&iwe, 0, sizeof(iwe));
      iwe.u.data.length = Adapter->scanConfirmVarArray[ii]->wpa_ie[1]+2;
      iwe.cmd = IWEVGENIE;
      current_ev = iwe_stream_add_point(current_ev, 
                                        extra + IW_SCAN_MAX_DATA, 
                                        &iwe, 
                                    Adapter->scanConfirmVarArray[ii]->wpa_ie);
      
    }
    if(Adapter->scanConfirmVarArray[ii]->rsn_capable)
    {
      RSI_DEBUG(RSI_ZONE_OID, "%s: WPA2 ",__FUNCTION__);
      ganges_memset(&iwe, 0, sizeof(iwe));
      iwe.u.data.length = Adapter->scanConfirmVarArray[ii]->rsn_ie[1]+2;
      iwe.cmd = IWEVGENIE;
      current_ev = iwe_stream_add_point(
			current_ev, 
                        extra + IW_SCAN_MAX_DATA, 
                        &iwe, 
                        Adapter->scanConfirmVarArray[ii]->rsn_ie);
    }

    RSI_DEBUG(RSI_ZONE_OID, "\n");
  }

  /* Length of data */
  dwrq->length = (current_ev - extra);
  dwrq->flags = 0;   
  return 0;
}
	
/*FUNCTION*********************************************************************
Function Name  : ganges_set_essid
Description    : This function sets the ESSID/network name of the AP to 
		 connect to  
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
dwrq             |  X  |     |     | Ptr to iw_point structure
extra            |  X  |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_set_essid
  (
    struct net_device *dev,
    struct iw_request_info *info,
    struct iw_point *dwrq,
    INT8   *ssid
  )
{
  INT32 ii=0;
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);  

  RSI_DEBUG(RSI_ZONE_OID, "%s : is called \n", __FUNCTION__ );
  /* Check if we asked for `any' */
  if(dwrq->flags == 0) 
  {
    RSI_DEBUG(RSI_ZONE_OID, "%s: Connect any AP\n", __FUNCTION__ );
  } 
  else
  {
    Adapter->pssid.SsidLength = dwrq->length-1;/*WE <21 requires this */
    ganges_memcpy(Adapter->pssid.Ssid, ssid, dwrq->length);
    RSI_DEBUG(RSI_ZONE_OID, "%s: SSID Len %d\n",__FUNCTION__, Adapter->pssid.SsidLength);
    for(ii=0;ii<Adapter->pssid.SsidLength;ii++)
      RSI_DEBUG(RSI_ZONE_OID,"%c",Adapter->pssid.Ssid[ii]); 
    RSI_DEBUG(RSI_ZONE_OID,"\n"); 
    if(Adapter->NetworkType==Ndis802_11IBSS)
    {
      ganges_set_ssid(Adapter,&Adapter->pssid);
    }    
  }
  //ssid[9]='\0';

 // return -EINPROGRESS;
 return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_get_essid
Description    : This function returns the connected AP's ESSID/network name 
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
dwrq             |     |  X  |     | Ptr to iw_point structure
extra            |     |  X  |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_get_essid
  (
    struct net_device *dev,
    struct iw_request_info *info,
    struct iw_point *dwrq,
    INT8   *extra
  )
{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);  
  if(FSM_STATE == FSM_OPEN)
  {
    Adapter->connectedAP.SSID[Adapter->connectedAP.SSIDLen] = 0;
    ganges_memcpy(extra, Adapter->connectedAP.SSID, Adapter->connectedAP.SSIDLen+1);
    dwrq->length = Adapter->connectedAP.SSIDLen;
    RSI_DEBUG(RSI_ZONE_OID, "%s : %s \n", __FUNCTION__, 
                                          Adapter->connectedAP.SSID);
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_OID, "%s: Not connected\n",__FUNCTION__ );
    dwrq->length = 0;
  }
  dwrq->flags = 1;

  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_get_range
Description    : This function gets a range of parameters 
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
dwrq             |     |  X  |     | Ptr to iwreq_data
extra            |     |  X  |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_get_range
  (
    struct net_device *dev,
    struct iw_request_info *info,
    union iwreq_data *wrqu,
    INT8  *extra
  )
{
  struct iw_range *range = (struct iw_range *) extra;
  INT32   ret = 0;

  RSI_DEBUG(RSI_ZONE_OID, "%s : is called \n", __FUNCTION__ );
  /* Set the length (very important for backward compatibility) */
  wrqu->data.length = sizeof(struct iw_range);

  /* Set all the info we don't care or don't know about to zero */
  ganges_memset(range, 0, sizeof(struct iw_range));

  /* Set the Wireless Extension versions */
  range->we_version_compiled = WIRELESS_EXT;
  range->we_version_source = 9;

  /* Set information in the range struct.  */
  range->throughput = 1.4 * 1000 * 1000;	/* don't argue on this ! */
  range->min_nwid = 0x0000;
  range->max_nwid = 0xFFFF;

#define	GANGES_SGNL_QUAL		0x0F		/* signal quality */
#define	GANGES_SIGNAL_LVL		0x3F		/* signal level */
#define	GANGES_SILENCE_LVL		0x3F		/* silence level */
  range->sensitivity = 0x3F;
  range->max_qual.qual = GANGES_SGNL_QUAL;
  range->max_qual.level = GANGES_SIGNAL_LVL;
  range->max_qual.noise = GANGES_SILENCE_LVL;
  range->avg_qual.qual = GANGES_SGNL_QUAL; /* Always max */
  /* Need to get better values for those two */
  range->avg_qual.level = 30;
  range->avg_qual.noise = 8;

  range->num_bitrates = 1;
  range->bitrate[0] = 2000000;	/* 2 Mb/s */

  /* Attempt to recognise 2.00 cards (2.4 GHz frequency selectable). */
  range->num_channels = 0;
  range->num_frequency = 0;
#if 0
	range->num_channels = 10;
	range->num_frequency = wv_frequency_list(base, range->freq,
							IW_MAX_FREQUENCIES);
#endif

  /* Encryption supported ? */
  range->num_encoding_sizes = 0;
  range->max_encoding_tokens = 0;
  return ret;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_set_encode
Description    : This function sets the encoding token and mode 
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
wrq              |  X  |     |     | Ptr to iwreq_data 
extra            |     |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_set_encode
  (
    struct net_device *dev,
    struct iw_request_info *info,
    union iwreq_data *wrqu,
    INT8  *extra
  )
{
  INT32 ret = 0;
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);  
  RSI_DEBUG(RSI_ZONE_OID, "+%s:called\n", __FUNCTION__);
  RSI_DEBUG(RSI_ZONE_OID, "%s: length %d flags 0x%x\n",
			 __FUNCTION__,
                         wrqu->encoding.length,
                         wrqu->encoding.flags);
    /* disable encryption */
    if (wrqu->encoding.flags & IW_ENCODE_DISABLED) 
    {
      RSI_DEBUG(RSI_ZONE_OID, "%s: disabling encryption\n",__FUNCTION__);
      Adapter->encryptAlgo = Ndis802_11WEPDisabled;
    }
    else
    {
      RSI_DEBUG(RSI_ZONE_OID, "%s: enabling encryption\n",__FUNCTION__);
      if(wrqu->encoding.length)
      {
        Adapter->WepKeyIndex =  wrqu->encoding.flags & 0xF;
        --Adapter->WepKeyIndex; /*Firmware expects offsets 0-3*/
        Adapter->WepKeyLength = wrqu->encoding.length;
        ganges_memcpy(Adapter->RSIWepkey, 
                      wrqu->encoding.pointer, 
                      Adapter->WepKeyLength);
        RSI_DEBUG(RSI_ZONE_OID, "%s: WEP key indx %dlen %d\n",
			        __FUNCTION__,
        			Adapter->WepKeyIndex,
	                        Adapter->WepKeyLength);
        Adapter->encryptAlgo = Ndis802_11WEPEnabled;
      }
    }
  return ret;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_get_encode
Description    : This function gets the encoding token and mode 
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
wrqu             |     |  X  |     | Ptr to iwreq_data
extra            |     |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_get_encode
  (
    struct net_device *dev,
    struct iw_request_info *info,
    union iwreq_data *wrqu,
    INT8  *extra
  )
{
  INT32 ret = 0;
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);  
  RSI_DEBUG(RSI_ZONE_OID, "+%s:called\n", __FUNCTION__);
  /*TODO check and return correct encoding value */
   if(Adapter->encryptAlgo != Ndis802_11WEPEnabled)
     wrqu->encoding.flags = IW_ENCODE_DISABLED;
   else
     RSI_DEBUG(RSI_ZONE_OID, "%s: WEP Enabled\n",__FUNCTION__);
  return ret;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_set_auth
Description    : This function sets the authentication mode 
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
wrqu             |  X  |     |     | Ptr to iwreq_data
extra            |     |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_set_auth
  (
    struct net_device *dev,
    struct iw_request_info *info,
    union iwreq_data *wrqu, 
    INT8  *extra
  )
{
  struct iw_param *param = &wrqu->param;
  INT32  ret = 0;
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);
  /*RSI_DEBUG(RSI_ZONE_OID, "%s: auth indx %d\n", 
                          __FUNCTION__, param->flags & IW_AUTH_INDEX);*/
  switch (param->flags & IW_AUTH_INDEX) 
  {
    case IW_AUTH_WPA_VERSION:
  	RSI_DEBUG(RSI_ZONE_OID, "Enable IW_AUTH_WPA_VERSION");
        if(param->value & IW_AUTH_WPA_VERSION_WPA)
        {
  	  RSI_DEBUG(RSI_ZONE_OID, " WPA\n");
       	  Adapter->authMode = Ndis802_11AuthModeWPAPSK;
        }
        else if(param->value & IW_AUTH_WPA_VERSION_WPA2)
        {
          RSI_DEBUG(RSI_ZONE_OID, " WPA2\n");
          Adapter->authMode = Ndis802_11AuthModeWPA2PSK;
        }
        else if(param->value & IW_AUTH_WPA_VERSION_DISABLED)
        {
          RSI_DEBUG(RSI_ZONE_OID, " DISABLE\n");
        }
        break;
    case IW_AUTH_CIPHER_PAIRWISE:
  	RSI_DEBUG(RSI_ZONE_OID, "Enable IW_AUTH_CIPHER_PAIRWISE ");
        if(param->value & IW_AUTH_CIPHER_TKIP)
        {
          Adapter->encryptAlgo = Ndis802_11Encryption2Enabled;
          RSI_DEBUG(RSI_ZONE_OID, " TKIP\n");
        }
        else if(param->value & IW_AUTH_CIPHER_CCMP)
        {
          Adapter->encryptAlgo = Ndis802_11Encryption3Enabled;
   	  RSI_DEBUG(RSI_ZONE_OID, " CCMP\n");
        }
        else if(param->value & IW_AUTH_CIPHER_NONE)
        {
	  RSI_DEBUG(RSI_ZONE_OID, " NONE\n");
        }
        else if(param->value & IW_AUTH_CIPHER_WEP40)
        {
          RSI_DEBUG(RSI_ZONE_OID, " WEP40\n");
        }
        else if(param->value & IW_AUTH_CIPHER_WEP104)
        {
          RSI_DEBUG(RSI_ZONE_OID, " WEP104\n");
        }
	break;
    case IW_AUTH_CIPHER_GROUP:
  	RSI_DEBUG(RSI_ZONE_OID, "Enable IW_AUTH_CIPHER_GROUP ");
        if(param->value & IW_AUTH_CIPHER_TKIP)
        {
	  RSI_DEBUG(RSI_ZONE_OID, " TKIP\n");
        }
        else if(param->value & IW_AUTH_CIPHER_CCMP)
        {
          RSI_DEBUG(RSI_ZONE_OID, " CCMP\n");
        }
        else if(param->value & IW_AUTH_CIPHER_NONE)
        {
  	  RSI_DEBUG(RSI_ZONE_OID, " NONE\n");
        }
        else if(param->value & IW_AUTH_CIPHER_WEP40)
        {
  	  RSI_DEBUG(RSI_ZONE_OID, " WEP40\n");
        }
        else if(param->value & IW_AUTH_CIPHER_WEP104)
        {
          RSI_DEBUG(RSI_ZONE_OID, " WEP104\n");
        }
        break;
    case IW_AUTH_KEY_MGMT:
  	RSI_DEBUG(RSI_ZONE_OID, "Enable IW_AUTH_KEY_MGMT ");
        if(param->value & IW_AUTH_KEY_MGMT_802_1X)
        {
  	  RSI_DEBUG(RSI_ZONE_OID, "802_1X\n");
       	  if(Adapter->authMode == Ndis802_11AuthModeWPAPSK)
       	    Adapter->authMode = Ndis802_11AuthModeWPA;
       	  if(Adapter->authMode == Ndis802_11AuthModeWPA2PSK)
    	    Adapter->authMode = Ndis802_11AuthModeWPA2;
        }
        else
        {
  	  RSI_DEBUG(RSI_ZONE_OID, "PSK\n");
        }
	break;
    case IW_AUTH_TKIP_COUNTERMEASURES:
  	RSI_DEBUG(RSI_ZONE_OID, "Enable IW_AUTH_TKIP_COUNTERMEASURES\n");
	break;
    case IW_AUTH_DROP_UNENCRYPTED:
  	RSI_DEBUG(RSI_ZONE_OID, "Enable IW_AUTH_DROP_UNENCRYPTED\n");
	break; 
    case IW_AUTH_80211_AUTH_ALG:
        RSI_DEBUG(RSI_ZONE_OID, "Enable IW_AUTH_80211_AUTH_ALG\n");
        if(param->value & IW_AUTH_ALG_SHARED_KEY)
        {
          Adapter->authMode = Ndis802_11AuthModeShared; 
  	  RSI_DEBUG(RSI_ZONE_OID, "changing to shared auth mode\n");
	}
	else if(param->value & IW_AUTH_ALG_OPEN_SYSTEM)
	{
	  Adapter->authMode = Ndis802_11AuthModeOpen;
	  RSI_DEBUG(RSI_ZONE_OID,"setting auth mode to open\n");
	}
	break;
    case IW_AUTH_WPA_ENABLED:
  	RSI_DEBUG(RSI_ZONE_OID, "Enable IW_AUTH_WPA_ENABLED\n");
	break;
    case IW_AUTH_RX_UNENCRYPTED_EAPOL:
        RSI_DEBUG(RSI_ZONE_OID, "Enable IW_AUTH_RX_UNENCRYPTED_EAPOL\n");
	break;
	//case IW_AUTH_ROAMING_CONTROL:
    case IW_AUTH_PRIVACY_INVOKED:
        RSI_DEBUG(RSI_ZONE_OID, "Enable IW_AUTH_PRIVACY_INVOKED\n");
	break;

    default:
        RSI_DEBUG(RSI_ZONE_OID, "Unsupported AUTH\n");
	return -EOPNOTSUPP;
  }
  return ret;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_mlme_request
Description    : This function requests MLME operation i.e deauthentication/
		 diassociation request	 
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
wrqu             |     |     |     | Ptr to union iwreq_data
extra            |  X  |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_mlme_request
  (
    struct net_device *dev,
    struct iw_request_info *info,
    union  iwreq_data *wrqu,
    INT8   *extra 
  )
{
  PRSI_ADAPTER Adapter  = ganges_getpriv(dev);
  struct iw_mlme *mlme  = (struct iw_mlme *)extra; 
  UINT16 reason = mlme->reason_code; 
  /*FIXME - CHk this & remove*/
  if(mlme == NULL && extra == NULL)
  {
    return 0;
  }  
  reason = mlme->reason_code; 
  switch(mlme->cmd)
  {
    case IW_MLME_DEAUTH:
         //FSM_STATE = FSM_NO_STATE;
         RSI_DEBUG(RSI_ZONE_OID,"%s: Sending Deauthentication request\n",
		   __FUNCTION__);        
         if(FSM_STATE != FSM_OPEN) 
         {
           RSI_DEBUG(RSI_ZONE_OID,"%s: Not in OPEN state\n",
		     __FUNCTION__); 
           break;
         }
         if((PS_FSM_STATE != PS_FSM_DISABLE) && (PS_FSM_STATE != PS_FSM_NORMAL))
	 {
           ganges_Reset_Event(&PwrSaveEvent);
	   ganges_send_power_save_req(Adapter,DISABLE_PWR_SAVE);
	   RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Disabling Pwr save\n");
           ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);
	   RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Wokeup on event\n");
         }

         ganges_send_DeAuthentication_request(Adapter,Adapter->connectedAP.BSSID,reason);         
         FSM_STATE = FSM_NO_STATE;
         break;
    case IW_MLME_DISASSOC:
         //FSM_STATE = FSM_NO_STATE;
         RSI_DEBUG(RSI_ZONE_OID,"%s: Sending Disassociation request\n",
		   __FUNCTION__);
         if((PS_FSM_STATE != PS_FSM_DISABLE) && (PS_FSM_STATE != PS_FSM_NORMAL))
	 {
	   ganges_Reset_Event(&PwrSaveEvent);
	   ganges_send_power_save_req(Adapter,DISABLE_PWR_SAVE);
	   RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Disabling Pwr save\n");
           ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);
	   RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Wokeup on event\n");
	 }
         ganges_send_disassociate_request(Adapter,Adapter->connectedAP.BSSID,reason);
	 FSM_STATE = FSM_NO_STATE;
         break;
    default:
	 return -EOPNOTSUPP;
  }
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_get_auth
Description    : This function gets the authentication mode params 
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
wrqu             |     |  X  |     | Ptr to iwreq_data
extra            |     |     |     | Char ptr
END**************************************************************************/
 
GANGES_STATIC INT32 
ganges_get_auth
  (
    struct net_device *dev,
    struct iw_request_info *info,
    union iwreq_data *wrqu,
    INT8  *extra
  )
{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);  
  struct iw_param *param = &wrqu->param;
  INT32 ret = 0;

  RSI_DEBUG(RSI_ZONE_OID, "%s: is called\n", __FUNCTION__);
  switch (param->flags & IW_AUTH_INDEX) 
  {
    case IW_AUTH_WPA_VERSION:
    case IW_AUTH_CIPHER_PAIRWISE:
    case IW_AUTH_CIPHER_GROUP:
    case IW_AUTH_KEY_MGMT:
      /*
       * wpa_supplicant will control these internally
       */
      ret = -EOPNOTSUPP;
      break;

	case IW_AUTH_TKIP_COUNTERMEASURES:
		ret = -EOPNOTSUPP;
		break;

	case IW_AUTH_DROP_UNENCRYPTED:
		break;

	case IW_AUTH_80211_AUTH_ALG:
                if(Adapter->authMode == Ndis802_11AuthModeShared)
                {
                  param->value = IW_AUTH_ALG_SHARED_KEY;
                }
                else 
                {
		  param->value = IW_AUTH_ALG_OPEN_SYSTEM;
                }
		break;

	case IW_AUTH_WPA_ENABLED:
		break;

	case IW_AUTH_RX_UNENCRYPTED_EAPOL:
		break;

	case IW_AUTH_ROAMING_CONTROL: /* we  should return firmware based/app
		                         based roaming */
	case IW_AUTH_PRIVACY_INVOKED:
		break;

	default:
		return -EOPNOTSUPP;
	}
	return 0;
}

#define MAX_WPA_IE_LEN 64

/*FUNCTION*********************************************************************
Function Name  : ganges_wx_set_genie
Description    : This function sets generic Information Element (IE) 
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
wrqu             |  X  |     |     | Ptr to iwreq_data
extra            |  X  |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_wx_set_genie
  (
    struct net_device *dev,
    struct iw_request_info *info,
    union iwreq_data *wrqu, 
    INT8  *extra
  )
{
  PRSI_ADAPTER Adapter = netdev_priv(dev);  
  RSI_DEBUG(RSI_ZONE_OID, "%s: is called\n", __FUNCTION__);
  if ((wrqu->data.length > MAX_WPA_IE_LEN)||(wrqu->data.length && extra == NULL))
    return -EINVAL;
  if(wrqu->data.length)
  {
    Adapter->wpa_ie_len = wrqu->data.length;
    ganges_memcpy(Adapter->wpa_ie,wrqu->data.pointer,Adapter->wpa_ie_len);
    ganges_dump(RSI_ZONE_OID, wrqu->data.pointer, wrqu->data.length);
  }
  else
  {
    Adapter->wpa_ie_len = 0;
  }
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_wx_set_encodeext
Description    : This function sets the encoding token & mode 
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
wrqu             |  X  |     |     | Ptr to iwreq_data
extra            |  X  |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32 
ganges_wx_set_encodeext
  (
    struct net_device *dev,
    struct iw_request_info *info,
    union iwreq_data *wrqu, 
    INT8  *extra
  )
{
  struct iw_encode_ext *ext = (struct iw_encode_ext *)extra;
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);  

  RSI_DEBUG(RSI_ZONE_OID, "%s: is called\n", __FUNCTION__);
  RSI_DEBUG(RSI_ZONE_OID, "%s: ext_flags 0x%x key_len %d alg 0x%x\n",
			  __FUNCTION__,
                          ext->ext_flags,
                          ext->key_len,ext->alg);
  switch(ext->alg)
  {
    case IW_ENCODE_ALG_NONE:
      RSI_DEBUG(RSI_ZONE_OID, "%s: Disabling encryption\n", __FUNCTION__);
      Adapter->encryptAlgo = Ndis802_11WEPDisabled;
      break;
    case IW_ENCODE_ALG_WEP:
      {
        RSI_DEBUG(RSI_ZONE_OID, 
                  "%s: Install WEP keys\n", __FUNCTION__);
        if(wrqu->encoding.length)
        {
          Adapter->WepKeyIndex =  wrqu->encoding.flags & 0xF;
          --Adapter->WepKeyIndex; /*Firmware expects offsets 0-3*/
          Adapter->WepKeyLength = ext->key_len;
          ganges_memcpy(Adapter->RSIWepkey, 
                        ext + 1,
                        ext->key_len);
          RSI_DEBUG(RSI_ZONE_OID, "WEP key indx %dlen %d\n",
                                Adapter->WepKeyIndex,
                                Adapter->WepKeyLength);
          Adapter->encryptAlgo = Ndis802_11WEPEnabled;
          if(ext->ext_flags & IW_ENCODE_EXT_GROUP_KEY)
          {
            if((Adapter->authMode == Ndis802_11AuthModeWPAPSK) ||
              (Adapter->authMode == Ndis802_11AuthModeWPA))
            {
              RSI_DEBUG(RSI_ZONE_OID, "Indicate thread to install WPA grp keys\n");
              Adapter->wpa_splcnt.group_cipher = WPA_CIPHER_WEP40;
              Adapter->wpa_grp_install_key =1;
              ganges_Wakeup_Event(&Event);
            }
            else if((Adapter->authMode == Ndis802_11AuthModeWPA2PSK)||
                    (Adapter->authMode == Ndis802_11AuthModeWPA2))
            {
              RSI_DEBUG(RSI_ZONE_OID, "Indicate thread to install WPA2 keys\n");
              Adapter->wpa_splcnt.group_cipher = WPA_CIPHER_WEP40;
              Adapter->wpa2_install_keys =1;
              ganges_Wakeup_Event(&Event);
            }
          }
        }
        break;
      } 
    case IW_ENCODE_ALG_TKIP:
    case IW_ENCODE_ALG_CCMP:
    {
      if(ext->ext_flags & IW_ENCODE_EXT_SET_TX_KEY)
      {
        if(ext->alg == IW_ENCODE_ALG_TKIP)
        {
          RSI_DEBUG(RSI_ZONE_OID, "Installing TKIP ");
          Adapter->wpa_splcnt.pairwise_cipher = WPA_CIPHER_TKIP;
        }
        else if(ext->alg == IW_ENCODE_ALG_CCMP)
        {
          RSI_DEBUG(RSI_ZONE_OID, "Installing CCMP ");
          Adapter->wpa_splcnt.pairwise_cipher = WPA_CIPHER_CCMP;
        }
 
        RSI_DEBUG(RSI_ZONE_OID, "PAIR KEY is\n");

        /*This is a unicast key */
        ganges_memcpy(Adapter->wpa_splcnt.temporal_ptk.tk1, 
                      ext + 1,
                      ext->key_len);
        ganges_dump(RSI_ZONE_OID, ext+1, ext->key_len);
        /*Check in which mode we are
          If we are in WPA mode request to install pair keys */
        if((Adapter->authMode == Ndis802_11AuthModeWPAPSK) ||
           (Adapter->authMode == Ndis802_11AuthModeWPA))
        {
          RSI_DEBUG(RSI_ZONE_OID,
                    "ganges_ioctl: Indicate thread to install WPA Pair key\n");
	  RSI_DEBUG(RSI_ZONE_OID,
                    "ganges_ioctl: Pair key length is %d\n",ext->key_len);
          Adapter->wpa_pair_install_key =1;
          ganges_Wakeup_Event(&Event); 
        }        
	
        /* FIXME 4/4 not transmitting because of thread */
        /*if(ext->key_len)
       ganges_send_pair_key_install(Adapter);*/
      }
      if(ext->ext_flags & IW_ENCODE_EXT_GROUP_KEY)
      {
        UINT32 key_id = wrqu->encoding.flags; 
        RSI_DEBUG(RSI_ZONE_OID, "GROUP KEY key id %d & grp key is\n", key_id);
        if(ext->alg == IW_ENCODE_ALG_TKIP)
        {
          RSI_DEBUG(RSI_ZONE_OID, "Installing TKIP Grp key\n");
          Adapter->wpa_splcnt.group_cipher = WPA_CIPHER_TKIP;
        }
        else if(ext->alg == IW_ENCODE_ALG_CCMP)
        {
          RSI_DEBUG(RSI_ZONE_OID, "Installing CCMP Grp key\n");
          Adapter->wpa_splcnt.group_cipher = WPA_CIPHER_CCMP;
        }
        Adapter->wpa_gkey_id = --key_id; 
        if(!ext->key_len)
	  break;
        ganges_memcpy(Adapter->wpa_splcnt.temporal_gtk, 
                      ext + 1,
                      ext->key_len);
        ganges_dump(RSI_ZONE_OID, ext+1, ext->key_len);
        if((Adapter->authMode == Ndis802_11AuthModeWPAPSK) ||
           (Adapter->authMode == Ndis802_11AuthModeWPA))
        {
          RSI_DEBUG(RSI_ZONE_OID, "Indicate thread to install WPA grp keys\n");
          Adapter->wpa_grp_install_key =1;
          ganges_Wakeup_Event(&Event); 
           //ganges_send_pair_key_install(Adapter);
           //ganges_send_group_key_install(Adapter, NULL, key_id);
          //Adapter->wpa_splcnt.group_keys_installed =1;
        }
	else
        {
          RSI_DEBUG(RSI_ZONE_OID, "Indicate thread to install WPA2 keys\n");
          Adapter->wpa2_install_keys =1;
          ganges_Wakeup_Event(&Event); 
        } 
      }
      break;
    }
    default:
    RSI_DEBUG(RSI_ZONE_OID, "Invalid auth alg\n");
    break;
  }
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_set_pmksa
Description    : This function sets the PMKID fld
Returned Value : On success 0 is returned else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
dev              |  X  |     |     | Ptr to our n/w device structure
info             |     |     |     | Ptr to iw_request_info structure 
wrqu             |  X  |     |     | Ptr to iwreq_data
extra            |  X  |     |     | Char ptr
END**************************************************************************/

GANGES_STATIC INT32
ganges_set_pmksa
 (
   struct net_device *dev,
   struct iw_request_info *info,
   union iwreq_data *wrqu,
   INT8  *extra
 )
{
  PRSI_ADAPTER Adapter = netdev_priv(dev);  
  if(Adapter->authMode == Ndis802_11AuthModeWPA2)
  {
    if((Adapter->pmkid_count <= RSI_NUM_PMKID_SUPPORTED))
    {
      struct iw_pmksa *pmksa = (struct iw_pmksa *)extra;
      INT32  ii=0;
      if(pmksa->cmd == IW_PMKSA_ADD)
      {
        INT32 index = Adapter->pmkid_count;
        for(ii=0;ii<Adapter->pmkid_count;ii++)
        {  
          if(ganges_memcmp(Adapter->pmkid_info[ii].BSSID,
                           pmksa->bssid.sa_data,
			   ETH_ADDR_LEN)==0)
          {
            RSI_DEBUG(RSI_ZONE_INFO,
                      "ganges_set_pmksa: Overwriting existing entry in Adapter's PMK Cache\n");
            RSI_DEBUG(RSI_ZONE_OID, "%s:  %02x:%02x:%02x:%02x:%02x:%02x\n",
                      __FUNCTION__,
                      pmksa->bssid.sa_data[0],
                      pmksa->bssid.sa_data[1],
                      pmksa->bssid.sa_data[2],
                      pmksa->bssid.sa_data[3],
                      pmksa->bssid.sa_data[4],
                      pmksa->bssid.sa_data[5]);
            ganges_memcpy(&Adapter->pmkid_info[ii].PMKID,pmksa->pmkid,IW_PMKID_LEN);
            ganges_dump(RSI_ZONE_INFO,&Adapter->pmkid_info[ii].PMKID,IW_PMKID_LEN);
            return 0;
          }
        }
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "ganges_set_pmksa: Adding an entry to the Adapter's PMK cache\n");
        ganges_memcpy(&Adapter->pmkid_info[index].PMKID,pmksa->pmkid,IW_PMKID_LEN);
        ganges_memcpy(&Adapter->pmkid_info[index].BSSID,pmksa->bssid.sa_data,ETH_ADDR_LEN);
        ganges_dump(RSI_ZONE_ERROR,&Adapter->pmkid_info[index].PMKID,IW_PMKID_LEN);
        ++Adapter->pmkid_count;
      }	      
      else if(pmksa->cmd == IW_PMKSA_REMOVE)
      {
        if(Adapter->pmkid_count)
        {
          INT32 jj=0;        
          for(ii=0;ii<Adapter->pmkid_count;ii++)
          {
            if(!ganges_memcmp(Adapter->pmkid_info[ii].PMKID,pmksa->pmkid,IW_PMKID_LEN) &&
               !ganges_memcmp(Adapter->pmkid_info[ii].BSSID,pmksa->bssid.sa_data,ETH_ADDR_LEN))
            {
              RSI_DEBUG(RSI_ZONE_INFO,
                        "ganges_set_pmksa: Removing entry %d from the Adapter's PMK cache\n",ii);
              ganges_dump(RSI_ZONE_ERROR,&Adapter->pmkid_info[ii].PMKID,IW_PMKID_LEN);
              for(jj=ii+1;jj<Adapter->pmkid_count;ii++,jj++)
               ganges_memcpy(&Adapter->pmkid_info[ii],&Adapter->pmkid_info[jj],sizeof(BSSIDInfo));
              --Adapter->pmkid_count;                 
              break; 
            }
          }
        }
      }
      else if(pmksa->cmd == IW_PMKSA_FLUSH)
      {
        RSI_DEBUG(RSI_ZONE_INFO,
                  "ganges_set_pmksa: Flushing the Adapter's PMK cache\n");
        Adapter->pmkid_count = 0;
        for(ii=0;ii<RSI_NUM_PMKID_SUPPORTED;ii++)
          ganges_memset(Adapter->pmkid_info,0,sizeof(BSSIDInfo));
      }
      else
      {
        RSI_DEBUG(RSI_ZONE_INFO,
                  "ganges_set_pmksa: Invalid Operation\n");
      }
    }
    else
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
                "ganges_indicate_pmkid: PMKID count more than supported list\n");
    }
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
	      "ganges_indicate_pmkid: PMK Caching valid in WPA2 mode only\n");
    return -EINVAL;  
  } 
  return 0;
}

                                                      
#define IW_IOCTL(x) [(x)-SIOCSIWCOMMIT]
GANGES_STATIC const iw_handler		ganges_handlers[] =
{
  IW_IOCTL(SIOCGIWNAME)  = (iw_handler) ganges_get_name,  
  IW_IOCTL(SIOCSIWMODE)  = (iw_handler) ganges_set_mode,
  IW_IOCTL(SIOCGIWMODE)  = (iw_handler) ganges_get_mode,
  IW_IOCTL(SIOCGIWRANGE) = (iw_handler) ganges_get_range,
  IW_IOCTL(SIOCSIWAP)    = (iw_handler) ganges_set_wap,	
  IW_IOCTL(SIOCGIWAP)    = (iw_handler) ganges_get_wap,
  IW_IOCTL(SIOCSIWSCAN)  = (iw_handler) ganges_set_scan,
  IW_IOCTL(SIOCGIWSCAN)  = (iw_handler) ganges_get_scan,
  IW_IOCTL(SIOCSIWESSID) = (iw_handler) ganges_set_essid,
  IW_IOCTL(SIOCGIWESSID) = (iw_handler) ganges_get_essid,
  IW_IOCTL(SIOCSIWRTS)   = (iw_handler) ganges_set_rtsthreshold,
  IW_IOCTL(SIOCGIWRTS)   = (iw_handler) ganges_get_rtsthreshold,
  IW_IOCTL(SIOCSIWFRAG)  = (iw_handler) ganges_set_fragthreshold,
  IW_IOCTL(SIOCGIWFRAG)  = (iw_handler) ganges_get_fragthreshold,
  IW_IOCTL(SIOCSIWTXPOW) = (iw_handler) ganges_set_txpower,
  IW_IOCTL(SIOCGIWTXPOW) = (iw_handler) ganges_get_txpower,
  IW_IOCTL(SIOCSIWENCODE)= (iw_handler) ganges_set_encode,
  IW_IOCTL(SIOCGIWENCODE)= (iw_handler) ganges_get_encode,
  IW_IOCTL(SIOCSIWAUTH)  = (iw_handler) ganges_set_auth,
  IW_IOCTL(SIOCGIWAUTH)  = (iw_handler) ganges_get_auth,
  IW_IOCTL(SIOCSIWGENIE) = (iw_handler) ganges_wx_set_genie,
  IW_IOCTL(SIOCSIWENCODEEXT) = (iw_handler) ganges_wx_set_encodeext,
  IW_IOCTL(SIOCSIWMLME)  = (iw_handler) ganges_mlme_request,
  IW_IOCTL(SIOCSIWPMKSA) = (iw_handler) ganges_set_pmksa,
};

GANGES_STATIC const iw_handler private_handler[]=
{
  NULL,
};

struct iw_handler_def	ganges_handler_def =
{
  .num_standard	= sizeof(ganges_handlers)/sizeof(iw_handler),
  .num_private	= sizeof(private_handler)/sizeof(iw_handler), 
  .num_private_args = sizeof(privtab)/sizeof(struct iw_priv_args), 
  .standard	= (iw_handler *) ganges_handlers,
  .private	= (iw_handler *) private_handler, 
  .private_args	= (struct iw_priv_args *) privtab
};

/*FUNCTION*********************************************************************
Function Name  : ganges_indicate_connect_status
Description    : This function indicates connection status by generating a 
		 wireless event 
Returned Value : None 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |     |     |  X  | Ptr to the Adapter structure
END**************************************************************************/

VOID 
ganges_indicate_connect_status
  (
    PRSI_ADAPTER Adapter
  )
{
  union iwreq_data wrqu;
  wrqu.ap_addr.sa_family = ARPHRD_ETHER;
  if( FSM_STATE == FSM_OPEN )
  {
    ganges_memcpy(wrqu.ap_addr.sa_data, Adapter->connectedAP.BSSID, WLAN_BSSID_LEN);
    RSI_DEBUG(RSI_ZONE_OID, "%s:  %02x:%02x:%02x:%02x:%02x:%02x\n",
              __FUNCTION__,
              wrqu.ap_addr.sa_data[0],
              wrqu.ap_addr.sa_data[1],
              wrqu.ap_addr.sa_data[2],
              wrqu.ap_addr.sa_data[3],
              wrqu.ap_addr.sa_data[4],
              wrqu.ap_addr.sa_data[5]);
 
    RSI_DEBUG(RSI_ZONE_INFO, 
              "ganges_indicate_connect_status: Indicating status CONNECTED\n");
    //ganges_carrier_on(Adapter->dev);        
  }
  else
  {
    ganges_memset(wrqu.ap_addr.sa_data, 0, WLAN_BSSID_LEN);
    Adapter->hidden_mode=0;
    //ganges_carrier_off(Adapter->dev);        
    RSI_DEBUG(RSI_ZONE_INFO, 
              "ganges_indicate_connect_status: Indicating status DISCONNECTED\n");
  }

  wireless_send_event( Adapter->dev, SIOCGIWAP, &wrqu, NULL); 
}

/*FUNCTION*********************************************************************
Function Name  : ganges_indicate_scan_complete
Description    : This function sends a scan complete wireless event 
Returned Value : None
Parameters     : 

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | Ptr to our Adapter structure
END**************************************************************************/

VOID 
ganges_indicate_scan_complete
  (
    PRSI_ADAPTER Adapter
  )
{
  union iwreq_data wrqu;
  wrqu.data.length = 0;
  wrqu.data.flags = 0;
  wireless_send_event( Adapter->dev, SIOCGIWSCAN, &wrqu, NULL);  
}

/*FUNCTION*********************************************************************
Function Name  : ganges_indicate_mic_failure
Description    : This function sends MIC Failure event
Returned Value : None
Parameters     : 

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | Ptr to our Adapter structure
END**************************************************************************/

VOID
ganges_indicate_mic_failure
  (
    PRSI_ADAPTER Adapter,
    INT8 flag
  )
{
  union iwreq_data wrqu;
  struct iw_michaelmicfailure mic;
  RSI_DEBUG(RSI_ZONE_INFO,
            "ganges_indicate_mic_failure: Sending MIC Failure event\n");
  ganges_memset(&mic,0,sizeof(mic));
  ganges_memset(&wrqu,0,sizeof(wrqu));
  if(!flag)
  {
    mic.flags |= IW_MICFAILURE_PAIRWISE;
  }
  else
  {
    mic.flags |= IW_MICFAILURE_GROUP;
  }
  mic.src_addr.sa_family = ARPHRD_ETHER;
  ganges_memcpy(mic.src_addr.sa_data,&Adapter->connectedAP.BSSID[0],6);
  RSI_DEBUG(RSI_ZONE_OID, "%s:  %02x:%02x:%02x:%02x:%02x:%02x\n",
            __FUNCTION__,
            Adapter->connectedAP.BSSID[0],
            Adapter->connectedAP.BSSID[1],
            Adapter->connectedAP.BSSID[2],
            Adapter->connectedAP.BSSID[3],
            Adapter->connectedAP.BSSID[4],
            Adapter->connectedAP.BSSID[5]);
  wrqu.data.length = sizeof(mic);
  wireless_send_event(Adapter->dev,IWEVMICHAELMICFAILURE,&wrqu,(char *)&mic);
}

