/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Ganges 
Module name  : LINUX-SDIO driver
File Name    : ganges_linux_data.c

File Description:
This file contains all the LINUX specific data path code. 

List of functions:
    ganges_get_qId
    ganges_xmit
    ganges_transmit_thread
    ganges_indicate_packet

Author :
  
Rev History:
Sl  By    date change details
---------------------------------------------------------
1   Fariya
---------------------------------------------------------
*END*************************************************************************/

#include "ganges_linux.h"
#include "ganges_mgmt.h"
#include "ganges_nic.h"

GANGES_EXTERN GANGES_EVENT  Event;
#define GANGES_TCP_CHKSUM_CLC 0x8

/*FUNCTION*********************************************************************
Function Name  : ganges_get_qId
Description    : This function gives the qId based on the TOS fld of the pkt
Returned Value :  
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | Pointer to our Adapter
skb              |  X  |     |     | Pointer to the socket buffer structure
*END**************************************************************************/

UINT32
ganges_get_qId
  (
    PRSI_ADAPTER   Adapter,
    struct sk_buff *skb
  )

{
  UINT8  qos = skb->data[15];
  UINT32 qId;
  /* Classifying the traffic based on IP TOS */
  qos >>=2;    
  RSI_DEBUG(RSI_ZONE_DATA_SEND,"ganges_get_qId: IP TOS field is %02x\n",qos);
  switch(qos)
  {
    case IP_TOS_DSCP_0x38_AC_VO:
      qId = DATA_QUEUE_VO;
      break;
    case IP_TOS_DSCP_0x30_AC_VO:
      qId = DATA_QUEUE_VO;
      break;
    case IP_TOS_DSCP_0x28_AC_VI:
      qId = DATA_QUEUE_VI;
      break;
    case IP_TOS_DSCP_0x2E_AC_VI:
      qId = DATA_QUEUE_VI;
      break;
    case IP_TOS_DSCP_0x20_AC_VI:
      qId = DATA_QUEUE_VI;
      break;
    case IP_TOS_DSCP_0x18_AC_BE:
      qId = DATA_QUEUE_BE;
      break;
    case IP_TOS_DSCP_0x00_AC_BE:
      qId = DATA_QUEUE_BE;
      break;
    case IP_TOS_DSCP_0x10_AC_BK:
      qId = DATA_QUEUE_BK;
      break;
    case IP_TOS_DSCP_0x08_AC_BK:
      default:
      qId = DATA_QUEUE_BK;
      break;
  }
      /* change QID based on acm bits information */
  while(qId < 5)
  {
    if(!Adapter->acm_bits[qId - 1])
    {
      break;
    }
    else
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
                "ganges_get_qId: ACM setted in Q %d\n", qId);
      qId++;
    }
  }
  return qId;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_xmit
Description    : When a packet is sent by the upper layers, the transmit entry 
                 point registered at the time of initialization will be called. 
                 This function will que the skb and set the event
Returned Value : On success 0 is returned, else a negative number
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
skb              |  X  |     |     | Pointer to the socket buffer structure
dev              |  X  |     |     | Pointer to our network device structure
*END**************************************************************************/

INT32
ganges_xmit
  (
    struct sk_buff *skb,
    struct net_device *dev
  )

{
  PRSI_ADAPTER Adapter = ganges_getpriv(dev);
  UINT32 qId;

  RSI_DEBUG(RSI_ZONE_DATA_SEND,"+ganges_xmit\n");
    
  /*Checking if data pkt is rcvd in open state or not*/
  if(FSM_STATE != FSM_OPEN)
  { 
    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_xmit: Not in open state ");
    goto fail;
  }
  
  /* Checking the size of the packet*/
  if(skb->len > 1528)
  { 
    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_xmit: Big packet ");
    goto fail;
  }

  if(Adapter->qos_enable)
  {
    if(skb->len>16)
    { 
      qId = ganges_get_qId(Adapter, skb);  
      if(qId == 5)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "ganges_xmit: ACM bit has been set to all queues ");
        goto fail;
      }
    }
    else 
    {
      RSI_DEBUG(RSI_ZONE_ERROR,"ganges_xmit: Small pkt recvd ");
      goto fail;
    }
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_DATA_SEND,"ganges_xmit: Qos not enabled\n");
    qId = DATA_QUEUE;
  }

  /*Acquiring the lock on the skb queues*/
  ganges_lock_bh(&Adapter->list[qId-1].lock); /*FIXME - chk the type of spin lock*/
  ganges_queue_tail(&Adapter->list[qId-1],skb);
  Adapter->total_pkts_qd++;
  ganges_unlock_bh(&Adapter->list[qId-1].lock);

  if(!Adapter->sleep_indcn)
  {
    ganges_Wakeup_Event(&Event);
    RSI_DEBUG(RSI_ZONE_DATA_SEND,"ganges_xmit: Signalled event\n");
  }
  else
  {
    if(Adapter->total_pkts_qd >= 20)
    {
      if(!ganges_netif_queue_stopped(Adapter->dev))
       ganges_netif_stop_queue(Adapter->dev);
    }
  }
  return 0;

fail:
  RSI_DEBUG(RSI_ZONE_ERROR,"-Dropped\n");
  Adapter->stats.tx_dropped++;
  ganges_kfree_skb(skb);
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_transmit_thread
Description    : This thread dequeues the skb from the list and adds the 
                 descriptor. Finally, it writes onto the card
Returned Value : NULL
Parameters     :

-----------------------+-----+-----+-----+------------------------------
Name                   | I/P | O/P | I/O | Purpose
-----------------------+-----+-----+-----+------------------------------
pContext               |     |     |     |
*END**************************************************************************/

VOID
ganges_transmit_thread
  (
    PVOID pContext
  )
{
  PRSI_ADAPTER Adapter = (PRSI_ADAPTER)pContext;
  UINT32 Len=0,i;
  UINT32 q_num,que_end;
  UINT32 que_start=0;
  INT32  status;
  UINT8  cmd_qnum = SND_DATA_Q;

  RSI_DEBUG(RSI_ZONE_DATA_SEND,
            "ganges_transmit_thread: XMIT thread started\n");
 do{
     UINT8 InterruptStatus=0;
     struct sk_buff *rcvskb;
     status = ganges_Wait_Event(&Event, EVENT_WAIT_FOREVER);
     RSI_DEBUG(RSI_ZONE_DATA_SEND,"ganges_transmit_thread:event released\n");
     ganges_Reset_Event(&Event);
     que_end   = (Adapter->qos_enable)?4:1;

que_loop:
     for(q_num = que_start; q_num<que_end;++q_num)
     {
       UINT32 num_pkts=0;
       UINT32 pkts_given=0;
       RSI_DEBUG(RSI_ZONE_DATA_SEND,"ganges_transmit_thread: Q_NUM:%d\n",q_num);
       if(Adapter->BufferFull)
       {
         RSI_DEBUG(RSI_ZONE_DATA_SEND,"ganges_transmit_thread: Buffer full\n");
         break;
       }
       do
       {
	 Adapter->about_to_send = TRUE; 
	 if(Adapter->sleep_indcn)
         {
           Adapter->about_to_send = FALSE;
           break;
         }
         ganges_lock_bh(&Adapter->locks[q_num]);
         num_pkts = ganges_queue_len(&Adapter->list[q_num]);
         if(!num_pkts)
         {
           RSI_DEBUG(RSI_ZONE_DATA_SEND,"ganges_transmit_thread: Que empty %d\n",q_num);
           ganges_unlock_bh(&Adapter->locks[q_num]);
	   Adapter->about_to_send = FALSE; 
           break;
         }
         --num_pkts;
         ++pkts_given;
         rcvskb = ganges_dequeue(&Adapter->list[q_num]);
         ganges_unlock_bh(&Adapter->locks[q_num]);

         RSI_DEBUG(RSI_ZONE_DATA_SEND,"Deque frm%d\n",q_num);
         if(rcvskb == NULL)
         {
           RSI_DEBUG(RSI_ZONE_ERROR,
                     "ganges_transmit_thread: ERROR!!!! skb NULL %d\n", q_num);
	   Adapter->about_to_send = FALSE; 
           break;
         }
         if(skb_headroom(rcvskb)>=16)
         {
           ganges_memset(skb_push(rcvskb,16),0,16);
           *(UINT16 *)&rcvskb->data[0] = ((rcvskb->len-16));
           rcvskb->data[14] = SND_DATA_Q;
           Len = rcvskb->len;
           cmd_qnum = SND_DATA_Q;
         }
         if(Adapter->NetworkType == Ndis802_11IBSS)
         {
           UINT8 b_cast_addr[6] ={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
           if(ganges_memcmp(&Adapter->DataSndPacket[FRAME_DESC_SZ],
                            b_cast_addr,
                            6) == 0)
           {
             RSI_DEBUG(RSI_ZONE_DATA_SEND,
                       "ganges_transmit_thread: Setting broadcast bit\n");
             *(UINT16 *)&Adapter->DataSndPacket[0] |= 0x4000;
           }
         }

         if((Adapter->authMode == Ndis802_11AuthModeWPAPSK) ||
            (Adapter->authMode == Ndis802_11AuthModeWPA2PSK) ||
            (Adapter->authMode == Ndis802_11AuthModeWPA) ||
            (Adapter->authMode == Ndis802_11AuthModeWPA2))
         {
           if(Adapter->wpa_splcnt.group_keys_installed == 0)
           {
             if((Adapter->DataSndPacket[FRAME_DESC_SZ + 12] == 0x88) &&
                ((Adapter->DataSndPacket[FRAME_DESC_SZ + 13] == 0x8E) ||
                (Adapter->DataSndPacket[FRAME_DESC_SZ + 13] == 0xC7)))
             {
               RSI_DEBUG(RSI_ZONE_INFO, "ganges_transmit_thread: Tx EAPOL\n");
               ganges_dump(RSI_ZONE_INFO, &Adapter->DataSndPacket[0],Len);
             }
             else
             {
               RSI_DEBUG(RSI_ZONE_ERROR, "ganges_transmit_thread: only EAPOL allowed\n");
               Adapter->stats.tx_dropped++;
               continue;
             }
           }
         }

         --Adapter->total_pkts_qd;
	status = ganges_card_write(Adapter,
                                    rcvskb->data,
                                    16,
                                    SND_DATA_Q);
         if(status!=GANGES_STATUS_SUCCESS)
         {
           RSI_DEBUG(RSI_ZONE_ERROR,
              "ganges_transmit_thread: Dropped Pkt of %d bytes\n",Len);
           ganges_kfree_skb(rcvskb);
	   Adapter->about_to_send = FALSE; 
           Adapter->stats.tx_dropped++;
           break;
         }
         if(PS_FSM_STATE == PS_FSM_AUTO)
         {
	   ganges_send_dummy_write(Adapter,0);
	 }
         status = ganges_card_write(Adapter,
                                    &rcvskb->data[16],
                                    Len-16,
                                    SND_DATA_Q);
         if(status!=GANGES_STATUS_SUCCESS)
         {
           RSI_DEBUG(RSI_ZONE_ERROR,
              "ganges_transmit_thread: Dropped Pkt of %d bytes\n",Len);
           ganges_kfree_skb(rcvskb);
	   Adapter->about_to_send = FALSE; 
           Adapter->stats.tx_dropped++;
           break;
         }

	 RSI_DEBUG(RSI_ZONE_DATA_SEND,
                  "ganges_transmit_thread: Pkt of %d bytes written on card\n",Len);
         ganges_kfree_skb(rcvskb);
         Adapter->stats.tx_packets++;
         Adapter->stats.tx_bytes += Len;

         ganges_down_interruptible(&Adapter->sleep_ack_sem);
         Adapter->about_to_send = FALSE;
         if(Adapter->sleep_indcn)
         {
            ganges_send_ssp_intrAck_cmd(Adapter,  
                                 SD_SLEEP_NO_PKT_ACK);
         }		
         ganges_up_sem(&Adapter->sleep_ack_sem);
         if((pkts_given==10) && (Adapter->qos_enable))
          break;
       }while(num_pkts);
     }

     if((Adapter->authMode == Ndis802_11AuthModeWPA2PSK) ||
        (Adapter->authMode == Ndis802_11AuthModeWPA2))
     {
       if(Adapter->wpa2_install_keys)
       {
         RSI_DEBUG(RSI_ZONE_INFO,
                   "ganges_transmit_thread: Installing WPA2 keys\n");
         ganges_send_pair_key_install(Adapter);
         if(Adapter->wpa_splcnt.group_cipher == WPA_CIPHER_WEP40)
         {
           RSI_DEBUG(RSI_ZONE_INFO,
                     "ganges_transmit_thread: Installing WEP Grp key\n");
           ganges_send_WEPKeys_request(Adapter,
                                       Adapter->RSIWepkey,
                                       Adapter->WepKeyLength,
                                       Adapter->WepKeyIndex,
                                       Adapter->scanConfirmVarArray[Adapter->
                                       SelectedBssidNum]->BSSID,
                                       1); /* wep keys flag */
         }
         else
         {
           ganges_send_group_key_install(Adapter, NULL, Adapter->wpa_gkey_id);
         }
         Adapter->wpa2_install_keys = 0;
         Adapter->wpa_splcnt.group_keys_installed =1;
       }
     }

     if((Adapter->authMode == Ndis802_11AuthModeWPAPSK) ||
        (Adapter->authMode == Ndis802_11AuthModeWPA))
     {  
       if(Adapter->wpa_pair_install_key)
       {
         RSI_DEBUG(RSI_ZONE_INFO,
                   "ganges_transmit_thread: Installing WPA Pair key\n");
         ganges_send_pair_key_install(Adapter);
         Adapter->wpa_pair_install_key = 0;
       }
       if(Adapter->wpa_grp_install_key)
       {
         if(Adapter->wpa_splcnt.group_cipher == WPA_CIPHER_WEP40)
         { 
           RSI_DEBUG(RSI_ZONE_INFO,
                     "ganges_transmit_thread: Installing WEP Grp key\n");
           ganges_send_WEPKeys_request(Adapter,
                                       Adapter->RSIWepkey, 
                                       Adapter->WepKeyLength,
                                       Adapter->WepKeyIndex,
                                       Adapter->scanConfirmVarArray[Adapter->
                                       SelectedBssidNum]->BSSID,
                                       1); /* wep keys flag */
         }
         else
         {
           RSI_DEBUG(RSI_ZONE_INFO,
                     "ganges_transmit_thread: Installing WPA Grp key\n");
           ganges_send_group_key_install(Adapter, NULL, Adapter->wpa_gkey_id);
         }
         Adapter->wpa_grp_install_key = 0;
         Adapter->wpa_splcnt.group_keys_installed =1;         
       }
     }

     for(i=que_start;i<que_end;i++)
     {
       if((!Adapter->BufferFull) && !(InterruptStatus&(1<<(i+4))) &&
          ganges_queue_len(&Adapter->list[i]) && (!Adapter->sleep_indcn))
       {
         RSI_DEBUG(RSI_ZONE_DATA_SEND,
                   "BFl: %d\nQue0:%d\nQue1:%d\nQue2:%d\nQue3:%d\n",
                   Adapter->BufferFull,
                   ganges_queue_len(&Adapter->list[0]),
                   ganges_queue_len(&Adapter->list[1]),
                   ganges_queue_len(&Adapter->list[2]),
                   ganges_queue_len(&Adapter->list[3]));
         q_num = i;
         goto que_loop;
       }
     }
   }while(!ganges_Signal_Pending());
}
                
/******************************************************************************
Functionn Name : ganges_indicate_packet
Description    : This function copies the packet into a skb structure and indicates 
                 to the upper layer
Returned Value : On success 0 will be returned else a negative number
Parameters     :
  
-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | Pointer to the private data area of the device
DataRcvPacket    |  X  |     |     | Pointer to a packet
pktLen           |  X  |     |     | Length of the packet
*END**************************************************************************/

INT32
ganges_indicate_packet
  (
    PRSI_ADAPTER Adapter,
    UINT8        *DataRcvPacket,
    UINT32       pktLen
  )
{ 
  struct sk_buff *rxskb;
  
  if(FSM_STATE !=FSM_OPEN)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_indicate_packet: Not in open state\n");
    goto fail;
  }

  if(pktLen>1536)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_indicate_packet: Big packet revd\n");
    goto fail;
  }

  if(pktLen < RSI_HEADER_SIZE)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_indicate_packet: Runt packet recvd\n");
    goto fail;
  }

  if((Adapter->authMode == Ndis802_11AuthModeWPAPSK) ||
     (Adapter->authMode == Ndis802_11AuthModeWPA2PSK) ||
     (Adapter->authMode == Ndis802_11AuthModeWPA) ||
     (Adapter->authMode == Ndis802_11AuthModeWPA2))
  {
    if( (DataRcvPacket[12] ==  0x88) &&
        ((DataRcvPacket[13] == 0x8E) || (DataRcvPacket[13] == 0xC7)))
     {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_indicate_packet: Rx EAPOL\n");
      //ganges_dump(RSI_ZONE_INFO, DataRcvPacket, pktLen);
     }
  }
  /*Allocate skb and copy the data into it*/
  rxskb = ganges_alloc_skb(pktLen);
  if(!rxskb)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_indicate_packet: Low on memory, packet dropped\n");
    goto fail;
  }

  ganges_memcpy(ganges_skb_put(rxskb,pktLen),DataRcvPacket,pktLen);
  
  /*Filling the various other fields of skb*/
  rxskb->dev       = Adapter->dev;
  rxskb->protocol  = ganges_eth_type_trans(rxskb,Adapter->dev);

#if 0
  if((Adapter->chk_sum_offld_cfg)&&
     ((rxskb->data[9] == 0x06) ||
     (rxskb->data[9]  == 0x11))) 
  {
    rxskb->ip_summed  = CHECKSUM_UNNECESSARY;
  }
#endif  
  rxskb->ip_summed = CHECKSUM_NONE;

  ganges_netif_rx(rxskb); 

  RSI_DEBUG(RSI_ZONE_DATA_RCV,
            "ganges_indicate_packet: Pkt of %d bytes indicated to upper layers\n",pktLen);

  Adapter->stats.rx_packets++;
  Adapter->stats.rx_bytes += pktLen;
  return 0;

fail:

  Adapter->stats.rx_dropped++;
  return -1;
}
