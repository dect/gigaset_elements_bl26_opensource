/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Ganges
Module name  : WinCE driver
File Name    : pxa27x_ssp_bus.c

File Description:
  This file contains the SPI BUS driver specific code.
  
List of functions:
     ssp_bus_init
     ssp_bus_deinit
     ssp_bus_request_synch
    
Author :

Rev History:
Ver   By               date        Description
---------------------------------------------------------
1.1   Anji &           1 Aug06     Initial version
      Ravi Vaishnav
---------------------------------------------------------
*END*************************************************************************/

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/delay.h> 
#include <asm-cr16/irq.h>
#include "ganges_linux.h"
#include "ganges_ssp_HCI.h"
#include "ganges_ssp_bus.h"
#include "ganges_nic.h"
#include "ganges_mgmt.h"
/* Global variables */

extern UINT8 buffer[400];
struct semaphore  int_check_sem;

/*FUNCTION*********************************************************************
Function Name  : irqreturn_t
Description    : This is the Interrupt Service Routine
Returned Value : Returns a value to indicate if interrupt is handled or not
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
irq              |  X  |     |     | 
dev_instance     |  X  |     |     | 
regs             |  X  |     |     | 
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

GANGES_STATIC irqreturn_t 
my_interrupt 
 ( 
   INT32 irq, 
   PVOID dev_instance, 
   struct pt_regs *regs
 )
{
  struct net_device *dev = (struct net_device *)dev_instance;
  PRSI_ADAPTER Adapter   = netdev_priv(dev);
  schedule_work(&Adapter->handler);
  KEY_STATUS_REG |= 0x4;
  RESET_INT_PENDING_REG |=0x2;	
  disable_irq(KEYB_INT); 
  return IRQ_HANDLED;
}

/*FUNCTION*********************************************************************
Function Name  : ssp_bus_init
Description    : This function initializes the SPI BUS module and SSP HCI
Returned Value : Returns a handle to the SPI bus info structure
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
cli_callBk       |  X  |     |     | pointer to the client call-back structure
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

SSP_BUS_HANDLE
ganges_ssp_bus_init( void *priv, struct cli_callBk_s *cli_callBk)
{
  struct ssp_bus_s *sspBus_info = NULL;
  int ret = 0;
  
  /* Allocate memory for ssp bus clb */
  sspBus_info = (struct ssp_bus_s *)kmalloc(sizeof(struct ssp_bus_s),GFP_ATOMIC);
  if( ! sspBus_info)
  {
    RSI_DEBUG(RSI_ZONE_ERROR, (TEXT("ssp_init: Error allocating SSP BUS clb\n")));
    return((SSP_BUS_HANDLE)NULL);
  }

  /* Get the reference to the Upper layer handle*/
  sspBus_info->priv = priv;
  
  /* Get the reference to the CLient callback structure */
  sspBus_info->cli_callBk.cli_gpio_intr_callBk = cli_callBk->cli_gpio_intr_callBk;

  init_MUTEX(&int_check_sem);
  sspBus_info->sspHandle = ((PRSI_ADAPTER)priv)->hDevice;
  
  sspBus_info->cur_sspGran = BIT_GRAN_8;
  ganges_spinlock_init(&sspBus_info->busLock);

  INT3_PRIORITY_REG |= 0x70;        // Set interrupt priority
  KEY_BOARD_INT_REG = 0x0;
  KEY_DEBOUNCE_REG = 0x0;
  P1_15_MODE_REG = 0x2A;        // P1_15 is int7n
  KEY_GP_INT_REG = 0x20;        // set bits 5:3 as required for the polarity of you interrupt (see datasheet)
  KEY_STATUS_REG |= 0x4;        // clear status register

  ret = request_irq(KEYB_INT, (void *)my_interrupt, SA_SHIRQ,"RSI_INTR",((PRSI_ADAPTER) priv)->dev);
  if(ret)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_ssp_bus_init: Failed to request interrupt %d\n",ret);
    return (SSP_BUS_HANDLE)NULL;
  }
  RSI_DEBUG(RSI_ZONE_INIT,"ganges_ssp_bus_init: Successfully  requested the interrupt %d\n",ret);
          
  return((SSP_BUS_HANDLE)sspBus_info);
};

/*FUNCTION*********************************************************************
Function Name  : ssp_bus_deinit
Description    : This function de-initializes the SPI BUS module and SSP HCI
Returned Value :
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
handle           |  X  |     |     | SSP_BUS_HANDLE
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/
void 
ganges_ssp_bus_deinit( SSP_BUS_HANDLE  handle)
{
  struct ssp_bus_s *sspBus_info = (struct ssp_bus_s *)handle;
  /* Free the allocated memory */ 
  kfree(sspBus_info);
  
  return;
}
/*FUNCTION*********************************************************************
Function Name  : ssp_bus_request_synch
Description    : This function prepares the cmd pkt according to the request
                 and either writes to or reads from the SSP FIFOs
Returned Value : Returns the status of the cmd
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
handle           |  X  |     |     | SSP_BUS_HANDLE
cmd              |     |     |  X  | pointer to the bus cmd structure
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/
INT32
ganges_ssp_bus_request_synch(SSP_BUS_HANDLE handle, struct bus_cmd_s *cmd)
{
  struct ssp_bus_s *sspBus_info = (struct ssp_bus_s *)handle;
  struct cmd_fmt_s tx_cmd;
  INT32    status = RSI_STATUS_FAIL;
  INT32    tknStatus = RSI_STATUS_SUCCESS;
  UINT8  junk[MAX_SPI_PKT_SIZE] = {0};
  UINT8  rx_buf[MAX_SPI_PKT_SIZE];
  UINT16 busy_retry = 0;

  down(&int_check_sem);
  memset((PVOID)&tx_cmd,0,sizeof(struct cmd_fmt_s));
  memset((PVOID)rx_buf, 0,MAX_SPI_PKT_SIZE);
  
  switch(cmd->type)
  {
    case SPI_INITIALIZATION:

      RSI_DEBUG(RSI_ZONE_SPI_DBG,"[ssp_bus_request] SPI_INITIALIZATION\n");
      /* Prepare the cmd */
      tx_cmd.buf[0] = CMD_INITIALIZATION;

        status= rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,(UINT8 *)&tx_cmd,rx_buf,2);	
      /* Check the status */
      if( rx_buf[1] == SPI_STATUS_SUCCESS)
      {
        RSI_DEBUG(RSI_ZONE_SPI_DBG,
                  "[ssp_bus_request_synch] SPI_INIT status SUCCESS\n");
        status = RSI_STATUS_SUCCESS;
      }
      else if( rx_buf[1] == SPI_STATUS_FAILURE)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "[ssp_bus_request_synch] SPI_INIT status FAILURE\n");
        status = RSI_STATUS_FAIL;
      }
      else
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "[ssp_bus_request_synch] SPI_INIT status Unknown:\n");
        status = RSI_STATUS_FAIL;
      }
      
      break;

    case SPI_INTERNAL_READ:

      RSI_DEBUG(RSI_ZONE_SPI_DBG,
                "[ssp_bus_request] SPI_INTERNAL_READ Len %d\n", cmd->length);
      /* Prepare the cmd */
      tx_cmd.buf[0] = CMD_READ_WRITE;
      tx_cmd.buf[0] |= (unsigned char)(cmd->length & 0x0003); 
     
      /* Only LSB 5 bits of the address are valid */
      tx_cmd.buf[1] |= (unsigned char)(cmd->address & 0x3F);


      rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,(UINT8 *)&tx_cmd,rx_buf,2); 

      sspBus_info->cur_sspGran = BIT_GRAN_8;

      /* Check the status */
      if( rx_buf[1] == SPI_STATUS_SUCCESS)
      {
        RSI_DEBUG(RSI_ZONE_SPI_DBG,
                  (TEXT("[ssp_bus_request_synch] SPI_INTERNAL_READ status SUCCESS\n")));
        /* Get the Start token first */
        tknStatus = ganges_ssp_receive_start_token( sspBus_info->sspHandle, BIT_GRAN_8);
        if( tknStatus == RSI_STATUS_BUSY)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                   (TEXT(" Start token BUSY while SPI_INTERNAL_READ\n")));
          
          status = RSI_STATUS_BUSY;
          break;
        }
        else if( tknStatus == RSI_STATUS_FAIL)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                   (TEXT("Start token ERROR while SPI_INTERNAL_READ\n")));
          
          status = RSI_STATUS_FAIL;
          break;
        }
        else
        {
          //RSI_DEBUG(RSI_ZONE_SPI_DBG,"Recvd token success\n");
        }        
	rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,junk,cmd->data,2);
        status = RSI_STATUS_SUCCESS;
      }
      else if( rx_buf[1] == SPI_STATUS_BUSY)
      {
        //RSI_DEBUG(RSI_ZONE_ERROR,
        //          (TEXT("[ssp_bus_request_synch] SPI_INTERNAL_READ status BUSY\n")));
        status = RSI_STATUS_BUSY;
      }
      else if( rx_buf[1] == SPI_STATUS_FAILURE)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  (TEXT("[ssp_bus_request_synch] SPI_INTERNAL_READ status FAILURE\n")));
        status = RSI_STATUS_FAIL;
      }
      else
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "[ssp_bus_request_synch] SPI_INTERNAL_READ status Unknown:  %02x\n", rx_buf[1]);
        status = RSI_STATUS_FAIL;
      }
      break;

    case SPI_INTERNAL_WRITE:
      RSI_DEBUG(RSI_ZONE_SPI_DBG,
                "[ssp_bus_request] SPI_INTERNAL_WRITE Len %d\n", cmd->length);
      /* Prepare the cmd */
      tx_cmd.buf[0] = CMD_READ_WRITE;
      tx_cmd.buf[0] |= CMD_WRITE;
      tx_cmd.buf[0] |= (unsigned char)(cmd->length & 0x0003); 
     
      /* Only LSB 5 bits of the address are valid */
      tx_cmd.buf[1] |= (unsigned char)(cmd->address & 0x3F);

      if( sspBus_info->cur_sspGran != BIT_GRAN_8)
      {
        sspBus_info->cur_sspGran = BIT_GRAN_8;
      }
      rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,(UINT8 *)&tx_cmd,rx_buf,2); 
      if( rx_buf[1] == SPI_STATUS_SUCCESS)
      {
        RSI_DEBUG(RSI_ZONE_SPI_DBG,
                 "[ssp_bus_request_synch] SPI_INTERNAL_WRITE status SUCCESS\n");

	rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,cmd->data,rx_buf,cmd->length);
        status = RSI_STATUS_SUCCESS;
        break;
      }
      else if( rx_buf[1] == SPI_STATUS_BUSY)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  (TEXT("[ssp_bus_request_synch] SPI_INTERNAL_WRITE status BUSY\n")));
        status = RSI_STATUS_BUSY;
        break;
      }
      else if( rx_buf[1] == SPI_STATUS_FAILURE)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  (TEXT("[ssp_bus_request_synch] SPI_INTERNAL_WRITE status FAILURE\n")));
        status = RSI_STATUS_FAIL;
        break;
      }
      else
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "[ssp_bus_request_synch] SPI_INTERNAL_WRITE status Unknown:  %02x\n", rx_buf[1]);
        status = RSI_STATUS_FAIL;
        break;
      }
      break;

    case AHB_MASTER_READ:

      RSI_DEBUG(RSI_ZONE_SPI_DBG,
                "[ssp_bus_request] AHB_MASTER_READ Len %d\n"
                 , cmd->length);
      /* Prepare the cmd */
      tx_cmd.buf[0] = CMD_READ_WRITE;
      tx_cmd.buf[0] |= AHB_BUS_ACCESS;
      tx_cmd.buf[0] |= TRANSFER_LEN_16_BITS;

      *(UINT16 *)&tx_cmd.buf[2] = cmd->length;
      
      /* Configure the TX FIFO to the corresponding granularity */
      sspBus_info->cur_sspGran = BIT_GRAN_8;
  
      rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,(UINT8 *)&tx_cmd,rx_buf,4); 
      rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,(UINT8 *)&(cmd->address),&rx_buf[4],4); 
      /* Check the status */
      if( rx_buf[1] == SPI_STATUS_SUCCESS)
      {
        RSI_DEBUG(RSI_ZONE_SPI_DBG,
                  "[ssp_bus_request_synch] AHB_MASTER_READ status SUCCESS\n");

        /* Get the Start token first */
        tknStatus = ganges_ssp_receive_start_token(sspBus_info->sspHandle, 
                                                   BIT_GRAN_8);
        if( tknStatus == RSI_STATUS_BUSY)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                   " Start token BUSY while AHB_MASTER_READ\n");
          
          status = RSI_STATUS_BUSY;
          break;
        }
        else if( tknStatus == RSI_STATUS_FAIL)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                   (TEXT(" ERROR while AHB_MASTER_READ\n")));
          
          status = RSI_STATUS_FAIL;
          break;
        }
	rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,junk,cmd->data,4);
        status = RSI_STATUS_SUCCESS;
        break;
      }
      else if( rx_buf[1] == SPI_STATUS_BUSY)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "[ssp_bus_request_synch] AHB_MASTER_READ status BUSY\n");
        status = RSI_STATUS_BUSY;
        break;
      }
      else if( rx_buf[1] == SPI_STATUS_FAILURE)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "[ssp_bus_request_synch] AHB_MASTER_READ status FAILURE\n");
        status = RSI_STATUS_FAIL;
        break;
      }
      else
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "[ssp_bus_request_synch] AHB_MASTER_READ status Unknown:  %02x\n", rx_buf[1]);
        status = RSI_STATUS_FAIL;
        break;
      }
      
      break;

    case AHB_MASTER_WRITE:
    {  

      RSI_DEBUG(RSI_ZONE_SPI_DBG,"[ssp_bus_request] AHB_MASTER_WRITE Len %d\n", 
                      cmd->length);
      /* Prepare the cmd */
      tx_cmd.buf[0] = CMD_READ_WRITE;
      tx_cmd.buf[0] |= CMD_WRITE;
      tx_cmd.buf[0] |= AHB_BUS_ACCESS;
      tx_cmd.buf[0] |= TRANSFER_LEN_16_BITS;

      *(UINT16 *)&tx_cmd.buf[2] = cmd->length;
      sspBus_info->cur_sspGran = BIT_GRAN_8;
      do
      {
        rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,(char *)&tx_cmd,rx_buf,2);
       if(rx_buf[1] == SPI_STATUS_SUCCESS )
       {
         rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,(char *)&tx_cmd.buf[2],&rx_buf[2],2);
         rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,(char *)&(cmd->address),&rx_buf[4],4);
         break;
       }
       else if ((rx_buf[1]== SPI_STATUS_BUSY) && busy_retry < 5 )
        continue;
       else
         break;
      }while(1);

      if( rx_buf[1] == SPI_STATUS_SUCCESS)
      {
        RSI_DEBUG(RSI_ZONE_SPI_DBG,
                  "[ssp_bus_request_synch] AHB_MASTER_WRITE status SUCCESS\n");


        rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,cmd->data,rx_buf,cmd->length);
        status = RSI_STATUS_SUCCESS;
        break;
      }
      else if( rx_buf[1] == SPI_STATUS_BUSY)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  (TEXT("[ssp_bus_request_synch] AHB_MASTER_WRITE status BUSY\n")));
        status = RSI_STATUS_BUSY;
        break;
      }
      else if( rx_buf[1] == SPI_STATUS_FAILURE)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  (TEXT("[ssp_bus_request_synch] AHB_MASTER_WRITE status FAILURE\n")));
        status = RSI_STATUS_FAIL;
        break;
      }
      else
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "[ssp_bus_request_synch] AHB_MASTER_WRITE status Unknown:  %02x\n",rx_buf[1]);
        status = RSI_STATUS_FAIL;
        break;
      }

      break;
    }

    case AHB_SLAVE_READ:

      RSI_DEBUG(RSI_ZONE_SPI_DBG,
                "[ssp_bus_request] AHB_SLAVE_READ Len %d\n", cmd->length);
      /* Prepare the cmd */
      tx_cmd.buf[0] = CMD_READ_WRITE;
      tx_cmd.buf[0] |= AHB_BUS_ACCESS;
      tx_cmd.buf[0] |= AHB_SLAVE;
      tx_cmd.buf[0] |= TRANSFER_LEN_16_BITS;
     
      //tx_cmd.buf[1] = (WRITE_READ_32_BIT_GRAN | (0x3F & (UINT8)cmd->address));
      tx_cmd.buf[1] = (UINT8)cmd->address;
      
      *(UINT16 *)&tx_cmd.buf[2] = cmd->length;
      
      /* Configure the TX FIFO to the corresponding granularity */
      if( sspBus_info->cur_sspGran != BIT_GRAN_8)
      {
        sspBus_info->cur_sspGran = BIT_GRAN_8;
      }

      rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,(UINT8 *)&tx_cmd,rx_buf,4);

      /* Check the status */
      if( rx_buf[1] == SPI_STATUS_SUCCESS)
      {
        RSI_DEBUG(RSI_ZONE_SPI_DBG,
                  "[ssp_bus_request_synch] AHB_SLAVE_READ status SUCCESS\n");
        
        /* Get the Start token first */
        tknStatus = ganges_ssp_receive_start_token( sspBus_info->sspHandle, BIT_GRAN_8);
        if( tknStatus == RSI_STATUS_BUSY)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                    " Start token BUSY while AHB_SLAVE_READ\n");
          
          status = RSI_STATUS_BUSY;
          break;
        }
        else if( tknStatus == RSI_STATUS_FAIL)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                   " ERROR while AHB_SLAVE_READ\n");
          
          status = RSI_STATUS_FAIL;
          break;
        }
        else
        {
          //RSI_DEBUG(RSI_ZONE_SPI_DBG,"Start token %d\n",tknStatus);
        }        
	rsi_spi_read((struct spi_device *)sspBus_info->sspHandle,&((UINT8 *)cmd->data)[0],cmd->length);
        status = RSI_STATUS_SUCCESS;
        break;
      }
      else if( rx_buf[1] == SPI_STATUS_BUSY)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "[ssp_bus_request_synch] AHB_SLAVE_READ status BUSY\n");
        status = RSI_STATUS_BUSY;
        break;
      }
      else if( rx_buf[1] == SPI_STATUS_FAILURE)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "[ssp_bus_request_synch] AHB_SLAVE_READ status FAILURE\n");
        status = RSI_STATUS_FAIL;
        break;
      }
      else
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "[ssp_bus_request_synch] AHB_SLAVE_READ status Unknown:  %02x\n",rx_buf[1]);
        status = RSI_STATUS_FAIL;
        break;
      }
      break;

    case AHB_SLAVE_WRITE:
      {
      RSI_DEBUG(RSI_ZONE_SPI_DBG,"[ssp_bus_request] AHB_SLAVE_WRITE Len%d\n", cmd->length);
      /* Prepare the cmd */
      tx_cmd.buf[0] = CMD_READ_WRITE;
      tx_cmd.buf[0] |= CMD_WRITE;
      tx_cmd.buf[0] |= AHB_BUS_ACCESS;
      tx_cmd.buf[0] |= AHB_SLAVE;
      tx_cmd.buf[0] |= TRANSFER_LEN_16_BITS;

     
      tx_cmd.buf[1] = (cmd->address);

      *(UINT16 *)&tx_cmd.buf[2] = cmd->length;
      
      sspBus_info->cur_sspGran = BIT_GRAN_8;
      /* Configure the TX FIFO to the corresponding granularity */
 
      rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,(UINT8 *)&tx_cmd,rx_buf,2); 

      /* Check the status */
      if( rx_buf[1] == SPI_STATUS_SUCCESS)
      {
        //RSI_DEBUG(RSI_ZONE_SPI_DBG,
        //          "[ssp_bus_request_synch] AHB_SLAVE_WRITE status SUCCESS\n");

        /* Send the remaining cmd */
        rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,(UINT8 *)&tx_cmd.buf[2],rx_buf,2); 
        rsi_spi_write((struct spi_device *)sspBus_info->sspHandle,cmd->data,cmd->length);
 
        status = RSI_STATUS_SUCCESS;
        break;
      }
      else if( rx_buf[1] == SPI_STATUS_BUSY)
      {
        //RSI_DEBUG(RSI_ZONE_ERROR,"Busy Status\n");      
        status = RSI_STATUS_BUSY;
        break;
      }
      else if( rx_buf[1] == SPI_STATUS_FAILURE)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "[ssp_bus_request_synch] AHB_SLAVE_WRITE status FAILURE\n");
        status = RSI_STATUS_FAIL;
        break;
      }
      else
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "[ssp_bus_request_synch] AHB_SLAVE_WRITE status Unknown:  %02x\n", rx_buf[1]);
        ganges_dump(RSI_ZONE_ERROR, (UINT8 *)&tx_cmd, 4);
        status = RSI_STATUS_FAIL;
        break;
      }
      }
      break;

      case SPI_DUMMY_WRITE:
      {
      RSI_DEBUG(RSI_ZONE_SPI_DBG,
                "[ssp_bus_request] SPI_DUMMY_WRITE Len %d\n", cmd->length);
      /* Prepare the cmd */
      tx_cmd.buf[0] = CMD_READ_WRITE;
      tx_cmd.buf[0] |= CMD_WRITE;
      tx_cmd.buf[0] |= (unsigned char)(cmd->length & 0x0003); 
     
      /* Only LSB 5 bits of the address are valid */
      tx_cmd.buf[1] |= (unsigned char)(cmd->address & 0x3F);

      if( sspBus_info->cur_sspGran != BIT_GRAN_8)
      {
        sspBus_info->cur_sspGran = BIT_GRAN_8;
      }
      rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,(UINT8 *)&buffer,rx_buf,280);   
      if(rx_buf[277] == SPI_STATUS_SUCCESS)
      {
        status = RSI_STATUS_SUCCESS;
        break;
      }
      else   
      {
        while(1)
        {
          rsi_spi_write_then_read((struct spi_device *)sspBus_info->sspHandle,(UINT8 *)&buffer,rx_buf,40);   
          if(rx_buf[37] == SPI_STATUS_SUCCESS)
          {
           status = RSI_STATUS_SUCCESS;
           break;
          }
          //else
          //printk("$:\n"); 
         }
         break;  
       }  
       } 
       break;
    default:
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "[ssp_bus_request_synch] ERROR, cmd-type = Unknown\n");
        status = RSI_STATUS_FAIL;
      break;
  }

  up(&int_check_sem); 

  return(status);
}
