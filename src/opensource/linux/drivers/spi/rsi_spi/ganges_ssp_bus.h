/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Pine1_Lp
Module name  : WinXP driver
File Name    : pxa27x_ssp_bus.h

File Description:
        This file contains SPI BUS specific defintions and function prototypes.
        
List of functions:

Author :

Rev History:
Ver   By               date        Description
---------------------------------------------------------
1.1   Anji &           1 Aug06     Initial version
      Ravi Vaishnav
---------------------------------------------------------
*END**************************************************************************/

#ifndef _SPI_CORE_H_
#define _SPI_CORE_H_

#include "common.h"

#define GPIO_EDGE_RISING 1
#define GPIO_EDGE_FALLING 2

#define FILE_DEVICE_HAL			0x00000101
#define METHOD_BUFFERED                 0
#define FILE_ANY_ACCESS                 0

#define IOCTL_HAL_GPIO2IRQ CTL_CODE(FILE_DEVICE_HAL, 2048, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_HAL_IRQ2GPIO CTL_CODE(FILE_DEVICE_HAL, 2049, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_HAL_IRQEDGE CTL_CODE(FILE_DEVICE_HAL, 2050, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_HAL_REQUEST_SYSINTR CTL_CODE(FILE_DEVICE_HAL, 38, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_HAL_RELEASE_SYSINTR CTL_CODE(FILE_DEVICE_HAL, 54, METHOD_BUFFERED, FILE_ANY_ACCESS)


typedef unsigned long  SSP_BUS_HANDLE;


#define MAX_SPI_PKT_SIZE        1600


#define CMD_INITIALIZATION      0x15
#define CMD_READ_WRITE          0x40
#define CMD_WRITE               0x20
#define AHB_BUS_ACCESS          0x10
#define AHB_SLAVE               0x08
#define TRANSFER_LEN_16_BITS    0x04

#define WRITE_READ_32_BIT_GRAN  0x40


  
struct bus_cmd_s 
{
        
#define  SPI_INITIALIZATION 1
#define  SPI_INTERNAL_READ  2
#define  SPI_INTERNAL_WRITE 3
#define  AHB_MASTER_READ    4
#define  AHB_MASTER_WRITE   5
#define  AHB_SLAVE_READ     6
#define  AHB_SLAVE_WRITE    7
#define  SPI_DUMMY_WRITE    8 
        
  unsigned short  type;
  unsigned short  length;
  void  *data;
  unsigned int address;
};


struct cmd_fmt_s
{
  unsigned char  buf[4];
};


struct cli_callBk_s
{
  void (*cli_gpio_intr_callBk)(void *priv); //TODO
};


/*
 * spi_bus
 *
 * No members should be set directly
 * private_data can be used by the implementer if the bus driver.
 */

struct ssp_bus_s
{
  const char          *name;
  struct cli_callBk_s cli_callBk;
  void                *priv;

  unsigned short      gpioIstThreadPriority;

  unsigned long       sspHandle;
  unsigned short      cur_sspGran;

  spinlock_t          busLock;
};



/* Function prototypes defined in pxa27x_bus.c */
        
SSP_BUS_HANDLE
ganges_ssp_bus_init( void *priv, struct cli_callBk_s *cli_callBk);

void 
ganges_ssp_bus_deinit( SSP_BUS_HANDLE  handle);

INT32
ganges_ssp_bus_request_synch( SSP_BUS_HANDLE handle, struct bus_cmd_s *cmd);

void 
ssp_bus_request_asynch( SSP_BUS_HANDLE handle, struct bus_cmd_s *cmd);

        
#endif	// _SPI_CORE_H_
