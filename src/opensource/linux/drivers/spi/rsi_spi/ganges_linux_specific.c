/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Ganges 
Module name  : LINUX-SDIO driver
File Name    : ganges_linux_specific.c

File Description:
This file contains all the LINUX specific code. 

List of functions:
    ganges_read_cardinfo
    ganges_check_file_type
    ganges_load_file
    ganges_proc_version_read
    ganges_proc_stats_read
    ganges_proc_debug_zone_read
    ganges_proc_debug_zone_write
    ganges_init_proc
    ganges_remove_proc_entry
    ganges_alloc_skb
    ganges_dump
    ganges_read_reg_paramters
    ganges_Wait_Event
    ganges_Start_Thread
    ganges_Kill_Thread
    ganges_Internal_Thread 
    ganges_StartThread
    ganges_Init_Thread

Author :
  
Rev History:
Sl  By    date change details
---------------------------------------------------------
1   Fariya
---------------------------------------------------------
*END*************************************************************************/

#include<linux/moduleparam.h>
#include<linux/proc_fs.h>

#include "ganges_linux.h"
#include "ganges_mgmt.h"

#define GANGES_MAX_FILE_SIZE   74*1024

UINT32 ganges_zone_enabled = RSI_ZONE_INFO |
                             RSI_ZONE_ERROR |
                             RSI_ZONE_INIT |
                             RSI_ZONE_OID |
                             RSI_ZONE_MGMT_SEND |
                             //RSI_ZONE_MGMT_RCV |
                             //RSI_ZONE_DATA_SEND |
                             //RSI_ZONE_DATA_RCV |
                             RSI_ZONE_FSM     |
                             //RSI_ZONE_ISR    |
                             RSI_ZONE_MGMT_DUMP ;
                             //RSI_ZONE_DATA_DUMP |
			     //RSI_ZONE_SPI_DBG;
                             //RSI_ZONE_PARSER ;

/*Command Line arguments*/

UINT8 data_rate[5]           = "AUTO";
UINT8 Protocol_type[2]       = "N";
UINT8 RFtype[11]             = "AL2236";
UINT8 firmware_path[256]     = "/rpine_bin/";

UINT16 tx_bytes              = 0;
UINT16 rx_bytes              = 0;
UINT16 traffic_interval      = 0;
UINT16 uapsd_wakeup_interval = 30;
UINT32 mac_address           = 0x05;
UINT32 mac_address1          = 0x05;
UINT32 RF_pwr_val_set        = 0;                             
UINT32 frag_threshold        = DFL_FRAG_THRSH - 28;
UINT32 RTS_threshold         = DFL_RTS_THRSH - 28;
UINT32 enable_high_speed     = 0;
UINT32 sdio_clock	     = 0;
UINT32 channel_num           = 0;
UINT32 disable_active_scan   = FALSE;
UINT32 enable_power_save     = FALSE;
UINT32 ibss_creator          = 1;
UINT32 tcp_csumoffld_enable  = 0;

module_param(tx_bytes,ushort,0);
module_param(rx_bytes,ushort,0);
module_param(traffic_interval,ushort,0);
module_param(uapsd_wakeup_interval,ushort,0);

module_param_string(data_rate,data_rate,sizeof(data_rate),0);
module_param_string(RFtype,RFtype,sizeof(RFtype),0);
module_param_string(Protocol_type,Protocol_type,sizeof(Protocol_type),0);
module_param_string(firmware_path,firmware_path,sizeof(firmware_path),0);

module_param(sdio_clock,uint,0);
module_param(RF_pwr_val_set,uint,0);
module_param(RTS_threshold,uint,0);
module_param(frag_threshold,uint,0);
module_param(enable_high_speed,uint,0);
module_param(channel_num,uint,0);
module_param(disable_active_scan,uint,0);
module_param(enable_power_save,uint,0);
module_param(mac_address,uint,0);
module_param(mac_address1,uint,0);
module_param(ibss_creator,uint,0);
module_param(tcp_csumoffld_enable,uint,0);

GANGES_STATIC struct proc_dir_entry *ganges_entry = NULL;


extern UINT32 PS_FSM_STATE;
/*FUNCTION*********************************************************************
Function Name  : ganges_read_cardinfo
Description    : Assigns the MAC address to the Adapter
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |     |     |  X  | Pointer to the Adapter structure
*END**************************************************************************/

GANGES_STATUS
ganges_read_cardinfo
  (
    PRSI_ADAPTER Adapter
  )
{
  GANGES_STATUS Status   = GANGES_STATUS_SUCCESS;
  struct net_device *device = Adapter->dev; 
  INT32  ii;
#ifdef MAC_INFO_EEPROM_READ
  EEPROM_READ mac_info_read;
  mac_info_read.off_set = 0x0082;
  mac_info_read.length  = 6;     /* Length in words i.e 3*2 = 6 bytes */

  Status = ganges_eeprom_read(Adapter,(UINT8 *)&mac_info_read);
  if(Status != GANGES_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_read_cardinfo: Eeprom read Failed\n");
    return Status;
  }
#else
  Adapter->PermanentAddress[0] = 0x00;
  Adapter->PermanentAddress[1] = 0x04;
  Adapter->PermanentAddress[2] = 0x04;
  Adapter->PermanentAddress[3] = 0x04;
  Adapter->PermanentAddress[4] = (UINT8)mac_address; 
  Adapter->PermanentAddress[5] = (UINT8)mac_address1; 
  for(ii=0; ii<6;ii++)
    device->dev_addr[ii] = Adapter->PermanentAddress[ii];
#endif
  return Status;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_check_file_type
Description    : This function checks if the file is of the UNIX/DOS type
Parameters     : Returns 8 if the file is of UNIX type else
                 returns 9 if the file is of DOS  type  

-----------------------+-----+-----+-----+------------------------------
Name                   | I/P | O/P | I/O | Purpose
-----------------------+-----+-----+-----+------------------------------
filp		       |  X  |     |     | Pointer to the opened file desc
*END**************************************************************************/

INT32
ganges_check_file_type
  (
    struct file *filp,
    INT8   *ptr
  )
{
  loff_t pos = 0;
  INT32  temp=0;
  temp = vfs_read(filp,ptr,8, &pos);
  if(temp!=8)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              "ganges_check_file_type: Unable to check %d\n",temp);
    return -1;
  }
  if(ptr[7]==10)
  {
    RSI_DEBUG(RSI_ZONE_INFO,
              "ganges_check_file_type: UNIX file type\n");
    return 8;
  }
  else //if(ptr[7]==13)
  {
    RSI_DEBUG(RSI_ZONE_INFO,
              "ganges_check_file_type: DOS file type\n");
    return 9;
  }
}

/*FUNCTION*********************************************************************
Function Name  : ganges_load_file
Description    : This function opens a file and reads data from it
Returned Value : On success, a pointer to the data read is returned 
	         else a NULL pointer is returned
Parameters     :

-----------------------+-----+-----+-----+------------------------------
Name                   | I/P | O/P | I/O | Purpose
-----------------------+-----+-----+-----+------------------------------
fn		       |  X  |     |     | File name to be opened for reading	
*END**************************************************************************/

PVOID
ganges_load_file
  (
    const INT8 *fn,
    UINT32 *read_length,
    UINT32 type	
  )
{
  UINT32 length; 
  UINT32 temp;
  UINT16 *dp;
  loff_t pos;
  UINT8  file_to_open[266];
  struct file* filp;
  mm_segment_t fs = get_fs();

  set_fs(get_ds());
  /* prepend file name with path */
  ganges_strcpy(file_to_open, firmware_path);
  ganges_strcat(file_to_open,fn); 
  RSI_DEBUG(RSI_ZONE_INFO,
              "ganges_load_file: trying to open '%s'.\n", file_to_open);
  filp = filp_open(file_to_open,0,0);
  if (IS_ERR(filp))
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              "ganges_load_file: Failed to open file '%s'.\n", file_to_open);
    return NULL;
  }

  length = filp->f_dentry->d_inode->i_size;
  if(length <= 0 || length > GANGES_MAX_FILE_SIZE)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              "ganges_load_file: Too big file '%s'\n", file_to_open);
    filp_close(filp, current->files);
    return NULL;
  }
  dp = ganges_vmalloc(length);
  if (dp == NULL)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              "ganges_load_file: Low on memory '%s'.\n", fn);
    filp_close(filp, current->files);
    return NULL;
  }
  pos = 0;
  RSI_DEBUG(RSI_ZONE_INFO,
            "ganges_load_file: reading size %d.\n",length);

  /* LMAC firmware will be in HEX file format */
  if(type == HEX_FILE)   
  {
    UINT8 read_data[10];
    INT32 ii=0,num=8;

    for(ii=0;ii<LMAC_INSTRUCTIONS_SIZE/2;ii++)
    {
      ganges_memset(read_data,0,10);
      temp = vfs_read(filp,read_data,num, &pos);
      if(!ii) 
      {
        /*check for the file type either DOS/UNIX */
        num = ganges_check_file_type(filp,read_data);      
        if(num==-1)
        {
          ganges_vfree(dp);
          filp_close(filp, current->files);
          return NULL;
        }  
        else if(num==9)
        { 
          temp = vfs_read(filp,&read_data[8],1,&pos);
          num  = 9;
          if(temp!=1)
          {
            
            RSI_DEBUG(RSI_ZONE_ERROR,
                      "ganges_load_file: Failed to read '%s %d %d'.\n",
                      fn, temp, ii);
            ganges_vfree(dp);
            filp_close(filp, current->files);
            return NULL;
          }
        }                             
        else
        {
          num =8;
        }        
      }      
      else
      {
        if (temp != num)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                    "ganges_load_file: Failed to read '%s %d %d'.\n",
                    fn, temp, ii);
          ganges_vfree(dp);
          filp_close(filp, current->files);
          return NULL;
        }
      }  
      *(dp+ii) = (UINT16 )simple_strtol(&read_data[2],NULL,16);
    }
  }
  /*For tadm & taim cases*/
  else 
  {
    temp = vfs_read(filp,(UINT8 *)dp,length, &pos);
    if (temp != length)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
                "ganges_load_file: Failed to read '%s %d'.\n", fn,temp);
      ganges_vfree(dp);
      filp_close(filp, current->files);
      return NULL;
    }
  }    
  filp_close(filp, current->files);
  RSI_DEBUG(RSI_ZONE_INFO,
            "ganges_load_file: Successfully read the f/w\n");
  set_fs(fs);
  *read_length = length;
  return dp;
}
      
/*FUNCTION*********************************************************************
Function Name  : ganges_proc_version_read
Description    : This function gives the driver's version number
Returned Value : Returns the number of bytes read 
Parameters     :

-----------------------+-----+-----+-----+------------------------------
Name                   | I/P | O/P | I/O | Purpose
-----------------------+-----+-----+-----+------------------------------
page                   |     |     |     |
start                  |     |     |     |
off                    |     |     |     |
count                  |     |     |     |
eof                    |     |     |     |
data		       |     |	   |     |
*END**************************************************************************/

INT32 
ganges_proc_version_read
  ( 
    INT8  *page,
    INT8  **start,
    off_t off,
    INT32 count,
    INT32 *eof,
    PVOID data
  )
{
  INT32        len;
  PRSI_ADAPTER Adapter    = data;
  /*FIXME version number should be initialized */
  len  = ganges_sprintf(page,"Driver : %d.%d\nTA : %d.%d\nLMAC : %d.%d\n",
                        Adapter->driver_ver.major,
                        Adapter->driver_ver.minor, 
                        Adapter->ta_ver.major,
                        Adapter->ta_ver.minor, 
                        Adapter->lmac_ver.major,
                        Adapter->lmac_ver.minor );
  *eof = 1;
  return len;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_proc_stats_read
Description    : This function gives information regarding the present FSM state,
		 the status of the hardware buffer, if it is full or not &
                 the number of skbs enqueued in all the four software queues
Returned Value :  
Parameters     :

-----------------------+-----+-----+-----+------------------------------
Name                   | I/P | O/P | I/O | Purpose
-----------------------+-----+-----+-----+------------------------------
page                   |     |     |     |
start                  |     |     |     |
off                    |     |     |     |
count                  |     |     |     | 
eof	               |     |     |     |
data		       |     |     |     |
*END**************************************************************************/

INT32 
ganges_proc_stats_read
  (
    INT8 *page,
    INT8 **start,
    off_t off,
    INT32 count,
    INT32 *eof,
    PVOID data
  )
{
  INT32 len=0;
  PRSI_ADAPTER Adapter    = data;
  INT8 *fsm_state_string[]= {
    "CLOSED",
    "FSM_SCAN_REQ_SENT",
    "FSM_JOIN_REQ_SENT",
    "FSM_AUTH_REQ_SENT",
    "FSM_ASSOCIATE_REQ_SENT",
    "FSM_OPEN",  
    "FSM_NO_STATE",
    "FSM_SET_WEPKEYS_SENT",
    "FSM_SET_WEPGRP_KEYS_SENT", 
    "FSM_SET_WPA_PAIRKEYS_SENT",
    "FSM_SET_WPA_GRP_KEYS_SENT",
    "FSM_DEEP_SLEEP",          
    "FSM_SCAN_REQ_SENT_INOPEN",
    "FSM_IBSS_CREATED",       
    "FSM_RESET1_SENT",
    "FSM_RESET2_SENT",
    "FSM_RF_REQ_SENT",
    "FSM_CARD_NOT_READY",
    "FSM_FIRMWARE_LOADED",
    };
    INT8 *ps_fsm_state_string[]={
     "PS_FSM_AUTO",
     "PS_FSM_NORMAL",
     "PS_FSM_DEEP_SLEEP",
     "PS_FSM_DISABLE",
    };

  len = ganges_sprintf(page,"Current state is: %s\nBuffer Full: %d\n" \
                       "Num of skbs in:\nQue0:%d\nQue1:%d\nQue2:%d\n" \
                       "Que3:%d\n" \
			"PS_FSM_STATE: %s\n",
                        fsm_state_string[FSM_STATE],
                        Adapter->BufferFull,
                        skb_queue_len(&Adapter->list[0]),
                        skb_queue_len(&Adapter->list[1]),
                        skb_queue_len(&Adapter->list[2]),
                        skb_queue_len(&Adapter->list[3]),
    	 	        ps_fsm_state_string[PS_FSM_STATE-1]);
 
  len += ganges_sprintf(page+len,
		        "Buffer full  Interrupts: %0x\n" \
		        "Buffer empty Interrupts: %0x\n",
		        Adapter->num_bfull,
		        Adapter->num_bfempty); 

  *eof = 1;
  return len;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_proc_debug_zone_read
Description    : This function gives the present value of "ganges_zone_enabled" 
		 variable which indicates the various print zones enabled &
		 disabled.  			 
Returned Value :  
Parameters     :

-----------------------+-----+-----+-----+------------------------------
Name                   | I/P | O/P | I/O | Purpose
-----------------------+-----+-----+-----+------------------------------
page		       |     |     |     |	 
start                  |     |     |     | 
off                    |     |     |     |
count                  |     |     |     | 
eof                    |     |     |     | 
data		       |     |     |     |	 
*END**************************************************************************/

INT32 
ganges_proc_debug_zone_read
  (
    INT8  *page,
    INT8  **start,
    off_t off,
    INT32 count,
    INT32 *eof,
    PVOID data
  )
{
  INT32 len;
  len  = ganges_sprintf(page,"The zones available are 0x%08x\n",*(UINT32 *)data);
  *eof = 1;
  return len;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_proc_debug_zone_write
Description    : This function changes the value of the "ganges_debug_zone"
		 variable depending on the input given.
Returned Value :  
Parameters     :

-----------------------+-----+-----+-----+------------------------------
Name                   | I/P | O/P | I/O | Purpose
-----------------------+-----+-----+-----+------------------------------
filp		       |     |     |     |	
buff                   |     |     |     |
len                    |     |     |     |
data                   |     |     |     |
*END**************************************************************************/

INT32
ganges_proc_debug_zone_write
  (
    struct file *filp,
    const INT8 *buff,
    UINT32 len,
    PVOID data
  )
{
  INT8 zone[10];
  INT8 *ptr;
  UINT32 num=0, count;
  if(!len)
  {
    return 0;
  }
  if(len>9)
  {
    return -EINVAL;
  }
  if(copy_from_user(zone,buff,len))
  {
    return -EFAULT;
  }
  else
  {
    count =len;
    ptr =zone;
    do{
     INT32 c=0;
     if(*ptr==10)
     {
       break;
     }
     else if(*ptr >='0' && *ptr <='9')
     {
       c = *ptr-'0';
     }
     else if((*ptr>='a' && *ptr<='f')||(*ptr>='A' && *ptr<='F'))
     {
       switch(*ptr)
       {
         case 'a':
         case 'A':
                  c=10;
         break;
         case 'b':
         case 'B':
                  c=11;
         break;
         case 'c':
         case 'C':
                  c=12;
         break;
         case 'd':
         case 'D':
                  c=13;
         break;
         case 'e':
         case 'E':
                  c=14;
         break;
         case 'f':
         case 'F':
                  c=15;
         break;
       }
    }
    else
    {
      return -EINVAL;
    }
    num = num*16+c;
    ptr++;
    }while(--count);
  }
  *(UINT32 *)data =num;
  return len;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_init_proc
Description    : This function creates a directory entry and three file entries 
                 in the proc file system
Returned Value : 0 in case of success else a negative value 
Parameters     :

-----------------------+-----+-----+-----+------------------------------
Name                   | I/P | O/P | I/O | Purpose
-----------------------+-----+-----+-----+------------------------------
pAdapter               |     |     |     | Pointer to the Adapter structure
*END**************************************************************************/

INT32
ganges_init_proc
  (
    PRSI_ADAPTER Adapter
  )
{
  struct proc_dir_entry *entry =NULL;
  ganges_entry = proc_mkdir("ganges",0);

  if(ganges_entry == NULL)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_init_proc: Unable to create dir entry\n");
    return -1;
  }

  else
  {
    entry = create_proc_read_entry("version",
                                   0,
                                   ganges_entry,
                                   ganges_proc_version_read,
                                   Adapter);

    if(entry == NULL)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
                "ganges_init_proc: Unable to create version entry\n");
      return -1;
    }

    entry = create_proc_read_entry("stats",
                                   0,
                                   ganges_entry,
                                   ganges_proc_stats_read,
                                   Adapter);

    if(entry == NULL)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
                "ganges_init_proc: Unable to create stats entry\n");
      return -1;
    }

     entry = create_proc_entry("debug_zone",
                               0,
                               ganges_entry);


    if(entry == NULL)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
                "ganges_init_proc: Unable to create debug zone entry\n");
      return -1;
    }
    else
    {
      entry->data       = &ganges_zone_enabled;
      entry->write_proc = ganges_proc_debug_zone_write;
      entry->read_proc  = ganges_proc_debug_zone_read;
    }
  }
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_remove_proc_entry
Description    : Removes the previously created proc file entries in the reverse 
		 order of creation
Returned Value : NONE
Parameters     :

-----------------------+-----+-----+-----+------------------------------
Name                   | I/P | O/P | I/O | Purpose
-----------------------+-----+-----+-----+------------------------------
*END**************************************************************************/

VOID 
ganges_remove_proc_entry
  (
    VOID
  )
{
  remove_proc_entry("stats",ganges_entry);
  remove_proc_entry("version",ganges_entry);
  remove_proc_entry("debug_zone",ganges_entry);
  remove_proc_entry("ganges",0);
}


/**************************************************************************
Function Name  : ganges_alloc_skb
Description    : This function is used to allocate skb. 
Returned Value : Returns a pointer to the allocated socket buffer
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Len              |  X  |     |     | The length to be allocated
*END**************************************************************************/

struct sk_buff* 
ganges_alloc_skb
  (
    UINT32 Len
  )
{
  return dev_alloc_skb(Len + 2); /*FIXME - CHECK DIS*/
}

/*FUNCTION*********************************************************************
Function Name  : ganges_dump
Description    : This function dumps the given data through the debugger.
Returned Value : 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
zone             |  X  |     |     | Pointer to the Adapter
data             |  X  |     |     | Data to dump                           
len              |  X  |     |     | Length of the data to be dump         
*END**************************************************************************/

VOID
ganges_dump
  (
     INT32 zone,
     PVOID vdata,
     INT32 len
  )
{
  INT32 ii;
  UINT8 *data = vdata;
  for(ii=0; ii< len; ii++)
  {
    if(!(ii % 16))
    {
      RSI_DEBUG(zone,"\n");
    }
    RSI_DEBUG(zone,"%02x ", data[ii]);
  }
  RSI_DEBUG(zone,"\n");
}
UINT8 buffer[400]={0};
/*FUNCTION*********************************************************************
Function Name  : ganges_read_reg_parameters
Description    : Read the parameters from registry
Returned Value : On success 0 will be returned
Parameters     :

----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
pAdapter                    |  X  |     |     | Pointer to the adapter structure
*END**************************************************************************/

INT32 
ganges_read_reg_paramters
  (
    PRSI_ADAPTER Adapter
  )
{
  UINT32 j,ii=0;

#ifdef GANGES_PER_MODE
  Adapter->ppe_per_mode = 1;
#endif
  UINT8 *ptr=buffer;
  UINT8 tx_buf[4]={0x62,0x3f,0,0};
  memset(buffer,0,sizeof(buffer)); 
  for(ii=0;ii<400;ii+=4)
  {
    memcpy(ptr,tx_buf,4);
    ptr+=4;
  }
  PS_FSM_STATE = PS_FSM_AUTO; 
  if((ganges_equal_string(Protocol_type,"a")==0)||
     (ganges_equal_string(Protocol_type,"A")==0))
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: Setting Protocol Type A\n");
    Adapter->ProtocolType   = ProtoType802_11A;
  }

  else if((ganges_equal_string(Protocol_type,"b")==0) ||
          (ganges_equal_string(Protocol_type,"B")==0))
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: Setting Protocol Type B\n");
    Adapter->ProtocolType   = ProtoType802_11B;
  }

  else if((ganges_equal_string(Protocol_type,"g")==0) ||
          (ganges_equal_string(Protocol_type,"G")==0))
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: Setting Protocol Type G\n");
    Adapter->ProtocolType   = ProtoType802_11G;
  }

  else if((ganges_equal_string(Protocol_type,"n")==0) ||
          (ganges_equal_string(Protocol_type,"N")==0))
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: Setting Protocol Type N\n");
    Adapter->ProtocolType   = ProtoType802_11N;
  }
   else
  {
    RSI_DEBUG(RSI_ZONE_INIT,
              "ganges_read_reg_param: Setting to default Protocol Type G\n");
    Adapter->ProtocolType   = ProtoType802_11G;
  }

  if(disable_active_scan)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: Setting to passive scan\n");
    Adapter->active_scan = 0;
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: Setting to active scan\n");
    Adapter->active_scan = 1;
  }

  if(enable_high_speed)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: Setting to high speed mode\n");
    Adapter->sdio_high_speed_enable = 1;     
  }
  else
  {
    Adapter->sdio_high_speed_enable = 0;     
  } 

  if(channel_num>11)
  {
    channel_num = Channel_11;
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: Setting to default channel num 11\n");
  }
  ganges_set_channel_type(Adapter,channel_num);

  if(ganges_equal_string(data_rate,"AUTO")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_AUTO\n");
    ganges_set_txrate(Adapter,RATE_AUTO);
  }
  else if(ganges_equal_string(data_rate,"1")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_1\n");
    ganges_set_txrate(Adapter,RATE_1_MBPS);
  }
  else if(ganges_equal_string(data_rate,"2")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_2\n");
    ganges_set_txrate(Adapter,RATE_2_MBPS);
  }
  else if(ganges_equal_string(data_rate,"5.5")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_5.5\n");
    ganges_set_txrate(Adapter,RATE_5_5_MBPS);
  }
  else if(ganges_equal_string(data_rate,"11")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_11\n");
    ganges_set_txrate(Adapter,RATE_11_MBPS);
  }
  else if(ganges_equal_string(data_rate,"6")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_6\n");
    ganges_set_txrate(Adapter,RATE_6_MBPS);
  }
  else if(ganges_equal_string(data_rate,"9")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_9\n");
    ganges_set_txrate(Adapter,RATE_9_MBPS);
  }
  else if(ganges_equal_string(data_rate,"12")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_12\n");
    ganges_set_txrate(Adapter,RATE_12_MBPS);
  }
  else if(ganges_equal_string(data_rate,"18")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_18\n");
    ganges_set_txrate(Adapter,RATE_18_MBPS);
  }
  else if(ganges_equal_string(data_rate,"24")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_24\n");
    ganges_set_txrate(Adapter,RATE_24_MBPS);
  }
  else if(ganges_equal_string(data_rate,"36")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_36\n");
    ganges_set_txrate(Adapter,RATE_36_MBPS);
  }

  else if(ganges_equal_string(data_rate,"48")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_48\n");
    ganges_set_txrate(Adapter,RATE_48_MBPS);
  }
  else if(ganges_equal_string(data_rate,"54")==0)
   {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_54\n");
    ganges_set_txrate(Adapter,RATE_54_MBPS);
  }
  else if(ganges_equal_string(data_rate,"MCS0")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_MCS0\n");
    ganges_set_txrate(Adapter,RATE_MCS0);
  }
  else if(ganges_equal_string(data_rate,"MCS1")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_MCS1\n");
    ganges_set_txrate(Adapter,RATE_MCS1);
  }
  else if(ganges_equal_string(data_rate,"MCS2")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_MCS2\n");
    ganges_set_txrate(Adapter,RATE_MCS2);
  }
  else if(ganges_equal_string(data_rate,"MCS3")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_MCS3\n");
    ganges_set_txrate(Adapter,RATE_MCS3);
  }
  else if(ganges_equal_string(data_rate,"MCS4")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_MCS4\n");
    ganges_set_txrate(Adapter,RATE_MCS4);
  }
  else if(ganges_equal_string(data_rate,"MCS5")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_MCS5\n");
    ganges_set_txrate(Adapter,RATE_MCS5);
  }
  else if(ganges_equal_string(data_rate,"MCS6")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_MCS6\n");
    ganges_set_txrate(Adapter,RATE_MCS6);
  }
  else if(ganges_equal_string(data_rate,"MCS7")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: RSI_RATE_MCS7\n");
    ganges_set_txrate(Adapter,RATE_MCS7);
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_INIT,
              "ganges_read_reg_param:Setting to default: RSI_RATE_54\n");
    ganges_set_txrate(Adapter,RATE_54_MBPS);
  }
  if(ganges_equal_string(RFtype,"MAXIM_2831")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,
              "ganges_read_reg_param: Setting to RSI_RF_MAXIM_2831\n");
    Adapter->RFType= RSI_RF_MAXIM_2831;
  }
  else if(ganges_equal_string(RFtype,"AL2230")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,
             "ganges_read_reg_param:Setting to RSI_RF_AL2230\n");
    Adapter->RFType = RSI_RF_AL2236;
  }
  else if(ganges_equal_string(RFtype,"AL2230S")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,
              "ganges_read_reg_param: Setting to RSI_RF_AL2230S\n");
    Adapter->RFType = RSI_RF_AL2230S;
  }
  else if(ganges_equal_string(RFtype,"AL2236")==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,
              "ganges_read_reg_param: Setting to RSI_RF_AL2236\n");
    Adapter->RFType = RSI_RF_AL2236;
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_INIT,
              "ganges_read_reg_param: Setting to default RF type\n");
    Adapter->RFType = RSI_RF_AL2236;
  }

  /*Assigning a unique BSSID*/
  j = jiffies;
  Adapter->Ibss_info.Beacon = 100;
  *(UINT32 *)&Adapter->Ibss_info.BSSID[0] = j;
  *(UINT16 *)&Adapter->Ibss_info.BSSID[4] = (UINT16)j;

  RSI_DEBUG(RSI_ZONE_INFO,
            "ganges_read_reg_param:IBSS BSSID: %02x:%02x:%02x:%02x:%02x:%02x\n",
            Adapter->Ibss_info.BSSID[0],
            Adapter->Ibss_info.BSSID[1],
            Adapter->Ibss_info.BSSID[2],
            Adapter->Ibss_info.BSSID[3],
            Adapter->Ibss_info.BSSID[4],
            Adapter->Ibss_info.BSSID[5]);

  Adapter->Ibss_info.CapabilityInfo = 0x2;
  Adapter->NetworkType = Ndis802_11Infrastructure;

  if(ibss_creator)
  {
    RSI_DEBUG(RSI_ZONE_INIT,
              "ganges_read_reg_param: IBSS configured as creator mode\n");
    Adapter->IbssCreator = IBSS_CREATOR;
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_INIT,
              "ganges_read_reg_param: IBSS configured as joiner\n");
    Adapter->IbssCreator = IBSS_JOINER;
  }
  //Adapter->encryptAlgo = Ndis802_11WEPKeyAbsent;
  Adapter->encryptAlgo = Ndis802_11EncryptionDisabled;
  Adapter->wpa_enabled = 0;
  Adapter->next_read_delay = 0;
  Adapter->slot_type = RSI_SLOT_AUTO;
  Adapter->preamble_type = RSI_PREAMBLE_AUTO;
  Adapter->nfm_mode = 0;
  Adapter->rssi_nfm_mode = 0;
  Adapter->selected_pwr_val_set = RF_pwr_val_set;
  RSI_DEBUG(RSI_ZONE_INFO, "ganges_read_reg_param: RF pwr value selected %d\n",
                            Adapter->selected_pwr_val_set);

  Adapter->tx_bytes         = tx_bytes;
  RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: Wakeup threshold Tx bytes: %d\n",
            Adapter->tx_bytes);
  Adapter->rx_bytes         = rx_bytes;
  RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: Wakeup threshold Rx bytes: %d\n",
            Adapter->rx_bytes);
  Adapter->traffic_interval = traffic_interval;
  RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: Traffic monitoring interval: %d\n",
            Adapter->traffic_interval);
  Adapter->uapsd_wakeup     = uapsd_wakeup_interval;
  RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: UAPSD wakup interval: %d\n",
            Adapter->uapsd_wakeup);

  Adapter->sdio_clock_speed = sdio_clock;
  Adapter->rssi_nf_auto_no_pkts_to_avg =  100;
    /* power save control soft/firm */
  Adapter->ps_ctrl = RPS_SLEEP_CTRL_SOFT;
  Adapter->JoinRequestPending = 0;

  if(frag_threshold%4)
  {
    frag_threshold += 4-(frag_threshold%4);
  }

  if(frag_threshold<MIN_THRESHOLD || frag_threshold >MAX_THRESHOLD)
  {
    RSI_DEBUG(RSI_ZONE_INIT,
              "ganges_read_reg_param: Setting to default frag_threshold value\n");
    frag_threshold = DFL_FRAG_THRSH-28;
  }

  Adapter->frag_threshold = frag_threshold;

  if(RTS_threshold%4)
  {
    RTS_threshold += 4-(RTS_threshold%4);
  }
  
   if(RTS_threshold<MIN_THRESHOLD || RTS_threshold >MAX_THRESHOLD)
  {
    RSI_DEBUG(RSI_ZONE_INIT,
              "ganges_read_reg_param: Setting to default RTS threshold value\n");
    RTS_threshold = DFL_RTS_THRSH - 28;
  }

  Adapter->rts_threshold = RTS_threshold;

  if(tcp_csumoffld_enable)
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: Enabling TCP checksum offloading\n");
    Adapter->chk_sum_offld_cfg = 1;
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_read_reg_param: Disabling TCP checksum offloading\n");
    Adapter->chk_sum_offld_cfg  = 0;
  }  

  Adapter->roaming_mode = RPS_NO_ROAMING;
    /*FIXME initialize this according to RF values */
  Adapter->txPower = RPS_POWER_15dBm;

  Adapter->BufferFull = FALSE;
  Adapter->power_sava_fsm = PWR_FSM_NORMAL;
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_Wait_Event
Description    : This function is used to put the current execution in a queue 
		 and reschedules itself for execution on "timeout" or when a 
		 wakeup is generated. 
Returned Value : Returns 0 if a wakeup signal is delivered else -1 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pEvent           |     |     |     | Ptr to the Event structure
timeout          |  X  |     |     | Timeout value in msecs                                   
*END**************************************************************************/

INT32 
ganges_Wait_Event
  (
    GANGES_EVENT *pEvent,
    UINT32 timeOut
  )
{
  UINT32 timeOutjiffies = 0;
  INT32  Status = 0;
  DECLARE_WAITQUEUE(ganges_wait,current);
  if(timeOut != 0) 
  {
    timeOutjiffies = (timeOut * HZ)/1000;
    if(timeOutjiffies == 0) 
    {
      timeOutjiffies = 1;
    }
  } 
  else 
  {
    timeOutjiffies = MAX_SCHEDULE_TIMEOUT;
  }
  add_wait_queue(&pEvent->eventQueue,&ganges_wait);
  set_current_state(TASK_INTERRUPTIBLE);

  while((test_bit(0,(unsigned long *)&pEvent->eventCondition) == 0) && timeOutjiffies) 
  {
    timeOutjiffies = schedule_timeout(timeOutjiffies);
    if(signal_pending(current)) 
    {
      Status = -1;
      break;
    }
  }
  set_current_state(TASK_RUNNING);
  remove_wait_queue(&pEvent->eventQueue,&ganges_wait);

  if((Status == 0) && (timeOutjiffies <= 0)) 
  {
    Status = -1;
  }
  return Status;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_Start_Thread
Description    : This function starts the thread 
Returned Value : 0 is returned 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pHandle          |     |     |     | Ptr to GANGES_THREAD_HANDLE structure                 
*END**************************************************************************/

INT32 
ganges_Start_Thread
  (
    PGANGES_THREAD_HANDLE   pHandle
  )
{
  ganges_Schedule_Work(&pHandle->TaskQueue);
  down(&pHandle->syncThread);
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_Kill_Thread
Description    : This function kills the thread  
Returned Value : 0 in case of success else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pHandle          |     |     |     | Ptr to GANGES_THREAD_HANDLE structure                 
*END**************************************************************************/

INT32 
ganges_Kill_Thread
  (
    PGANGES_THREAD_HANDLE   pHandle
  )
{
  INT32 Status;
  lock_kernel();
  pHandle->KillThread = 1;
  Status = kill_proc(pHandle->ThreadId, SIGKILL, 1);
  if(Status) 
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              "ganges_Kill_Thread: Unable to Kill Thread %s\n", pHandle->Name);
    return -1;
  }
  else 
  {
    wait_for_completion(&pHandle->Completion);
  }
  unlock_kernel();
  /* Cleanup Zombie Threads */
  kill_proc(2, SIGCHLD, 1);
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_Internal_Thread
Description    : This function is a wrapper to be used for all Linux thread 
		 functions.  
Returned Value : 0 is returned.
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pContext         |     |     |     |        
*END**************************************************************************/

GANGES_STATIC INT32 
ganges_Internal_Thread 
  (
    PVOID pContext
  )
{
  PGANGES_THREAD_HANDLE pHandle = (PGANGES_THREAD_HANDLE)pContext;

  lock_kernel();
  siginitsetinv(&current->blocked,
               sigmask(SIGKILL)|sigmask(SIGINT)|sigmask(SIGTERM));
  ganges_strcpy(current->comm, pHandle->Name);
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,5,0)
  daemonize(pHandle->Name);
  allow_signal(SIGKILL);
#ifdef CONFIG_PM
  current->flags |= PF_NOFREEZE;
#endif
#else
  daemonize();
  reparent_to_init();
#endif
  unlock_kernel();
  up(&pHandle->syncThread);
  do {
      pHandle->pFunction(pHandle->pContext);
      if (ganges_Signal_Pending()) {
          flush_signals(current);
      }
  }while (pHandle->KillThread == 0);

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,5,0)
  current->io_context = NULL; // 2.6 TODO
#endif
  complete_and_exit(&pHandle->Completion, 0);
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_StartThread
Description    :  
Returned Value : None 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pContext         |     |     |     | 
*END**************************************************************************/

GANGES_STATIC VOID
ganges_StartThread
  (
    PVOID pContext
  )
{
  PGANGES_THREAD_HANDLE pHandle = (PGANGES_THREAD_HANDLE)pContext;
  pHandle->ThreadId = kernel_thread(ganges_Internal_Thread, pContext, 0);
  return;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_Init_Thread
Description    : This function initialized and creates a thread with the thread
		 function and name.
Returned Value : 0 on success else a negative value
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pHandle          |     |     |  X  | Ptr to GANGES_THREAD_HANDLE structure
pName            | X   |     |     | Thread name                         
Priority         | X   |     |     | Thread priority         
pFunction        | X   |     |     | Thread function name
pContext         | X   |     |     | Context to be passed to the thrd
*END**************************************************************************/

INT32 
ganges_Init_Thread
  (
    PGANGES_THREAD_HANDLE   pHandle,
    UINT8                   *pName,
    UINT32                  Priority,
    Thread_Func             pFunction,
    PVOID                   pContext
  )
{
  GANGES_ASSERT(pFunction);
  GANGES_ASSERT(pHandle);
  GANGES_ASSERT(pName);

  if (!(pFunction && pHandle && pName)) 
  {
    return -1;
  }

  ganges_memset(pHandle, 0, sizeof(GANGES_THREAD_HANDLE));
  pHandle->pFunction = pFunction;
  pHandle->pContext = pContext;
  pHandle->KillThread = 0;
  init_completion(&pHandle->Completion);
  init_MUTEX_LOCKED(&pHandle->syncThread);

  /* Make sure that the name does not exceed the length */
  ganges_strncpy(pHandle->Name, pName, GANGES_THREAD_NAME_LEN);
  pHandle->Name[GANGES_THREAD_NAME_LEN] = '\0';

  /* Initialize Kernel Start Tasklet */

  ganges_Init_Work_Queue(&pHandle->TaskQueue, ganges_StartThread, pHandle);
  return 0;
}
