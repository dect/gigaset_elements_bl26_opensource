/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Ganges
Module name  : Linux driver
File Name    : ganges_fsm.c

File Description:
    This file contains the code for State machine. Received mangement packet will
    be process through this module only. In this file process the received packets
    and send the proper responses to the firmware and change the connect status.
    
List of functions:
    ganges_mgmt_fsm
    ganges_copy_scanconfirmInfo
    ganges_ps_fsm

Author :

Rev History:
Ver  By    date       Description
---------------------------------------------------------
1   Anji
2   Fariya
*END*************************************************************************/

#include "ganges_linux.h"
#include "ganges_nic.h"
#include "ganges_mgmt.h"
#define POWER_SAVE 0

GANGES_EXTERN UINT32 enable_power_save;
GANGES_EXTERN GANGES_EVENT PwrSaveEvent;
UINT32 PS_FSM_STATE;
UINT8 flags=0;
extern int req;

INT32  noise_measure_ct = 50;
GANGES_EXTERN UINT16 bb_maxgain_reg_values[];
UINT16 gain_scalinghigh_reg_values[] = {
        0x1032, 0x102E,
        0x102C, 0x102A,
        0x1028, 0x1026,
        0x1024, 0x1022,
        0x1020, 0x101E,
        0x101C, 0x101A,
        0x1018, 0x1016,
        0x1014, 0x1012,
        0x1010, 0x1008};

/*FUNCTION*********************************************************************
Function Name  :  ganges_mgmt_fsm
Description    :  This routine maintians the main state mechine.
                  Initially we will start with the FSM_NOSTATE.
                  Based on the requests FSM states may change.
                  FSM_OPEN is the connected state, where we will accept packets to
                  transmit and receive.
                  Same way we will maintain power save states also
Returned Value : 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |     |  X  |     | pointer to our adapter
MgmtRcvFrame     |     |     |     | The buffer containing the received 
                                     managemnt frame
type             |     |     |     | The management packet type received
MngPacketLen     |     |     |     | The length of managemnt packet received
*END**************************************************************************/

VOID 
ganges_mgmt_fsm
  (
    IN PRSI_ADAPTER Adapter, 
    IN UINT8 MgmtRcvFrame[], 
    IN UINT16 type, 
    IN UINT32 MngPacketLen
  )
{
  UINT32  ii;
  UINT16  *mgmtRcvFrame=(UINT16 *)MgmtRcvFrame;
  BOOLEAN copyScanConfirm;
  struct net_device *device = Adapter->dev;
  copyScanConfirm = FALSE;

  /* FSM indipendent mgmt pkts */
  switch(type)
  {
    case MGMT_DESC_EEPROM_READ_CFM:
      {
        if(MgmtRcvFrame[2] == 0x82)
        { 
          RSI_DEBUG(RSI_ZONE_FSM,(TEXT("ganges_mgmt_fsm: EEPROM read MAC address successfully\n")));
          ganges_dump(RSI_ZONE_FSM,&MgmtRcvFrame[16],RSI_MAC_ADDR_LEN);
          ganges_memcpy(&Adapter->PermanentAddress[0],
                        &MgmtRcvFrame[16],
                        RSI_MAC_ADDR_LEN);
          for(ii=0; ii<6;ii++)
	  {
            device->dev_addr[ii] = Adapter->PermanentAddress[ii];
          }  
        }
        else if(MgmtRcvFrame[2] == 0x88)
        {
          RSI_DEBUG(RSI_ZONE_FSM,(TEXT("ganges_mgmt_fsm: EEPROM read power vals successfully\n")));
          ganges_dump(RSI_ZONE_FSM,&MgmtRcvFrame[16],RSI_TOTAL_PWR_VALS_LEN);
          ganges_memcpy(&Adapter->power_set,
                        &MgmtRcvFrame[16],
                        RSI_TOTAL_PWR_VALS_LEN);
          ganges_reset_power_vals(Adapter,8);
        }
        else
        {
          RSI_DEBUG(RSI_ZONE_FSM,
                    (TEXT("ganges_mgmt_fsm: Unknown EEPROM Response\n")));
          break;
        }
        return;
      }

    case MGMT_DESC_EEPROM_WRITE_CFM:
      {
        if(MgmtRcvFrame[4] == 1)
        {
          RSI_DEBUG(RSI_ZONE_FSM,(TEXT("ganges_mgmt_fsm: No EEPROM suport found\n")));
          Adapter->eeprom_check_status = 1;
          break;
        }
        else
        {
#ifdef EEPROM_READ_POWER_VALS
           if(!Adapter->eeprom_check_status)		 
	   {
             if(ganges_read_power_vals(Adapter)!= GANGES_STATUS_SUCCESS)
             {
               RSI_DEBUG(RSI_ZONE_ERROR,
                        (TEXT("ganges_mgmt_fsm: Failed to read power Set values\n")));
             }
	   }
#endif
        }  
        if(MgmtRcvFrame[2] == 0x82)
        {
          RSI_DEBUG(RSI_ZONE_FSM,
                    (TEXT("ganges_mgmt_fsm: EEPROM write success recvd for MAC info \n")));
          if(ganges_read_cardinfo(Adapter)!=GANGES_STATUS_SUCCESS)
          {
	    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_mgmt_fsm: MAC Address read unsuccessful\n");
          }
        } 
        else if(MgmtRcvFrame[2] == 0x88)
        {
          RSI_DEBUG(RSI_ZONE_INFO,
                    (TEXT("ganges_mgmt_fsm: EEPROM Write  Success recvd For PowerSet vals\n")));
          if(ganges_read_power_vals(Adapter)!= GANGES_STATUS_SUCCESS)
          {
            RSI_DEBUG(RSI_ZONE_ERROR,
                      (TEXT("ganges_mgmt_fsm: Failed to write Power Set values\n")));
          }
        }
        else
        {
           RSI_DEBUG(RSI_ZONE_ERROR,(TEXT("ganges_mgmt_fsm: EEPROM Write....unknown Offset Found\n")));
        }
        return;
      }

    case MGMT_DESC_TYP_STATS_RESP:
      {
        UINT16 *frameBody;

        frameBody = (UINT16 *)&MgmtRcvFrame[16];
        if( ((UINT16 *)MgmtRcvFrame)[1])
        {
          RSI_DEBUG(RSI_ZONE_FSM, (TEXT("[mgmt_fsm]  Radar detected\n")));
        }  
 
        /* Update all the statistics information*/
        Adapter->sta_info.tx_frames       = frameBody[0];
        Adapter->sta_info.rx_frames       = frameBody[1];
        Adapter->sta_info.tx_retries      = frameBody[2];
        Adapter->sta_info.rx_retries      = frameBody[3];
        Adapter->sta_info.rx_duplicates   = frameBody[4];
        Adapter->sta_info.crc_errors      = frameBody[5];
        Adapter->sta_info.buffer_full     = frameBody[6];
        Adapter->sta_info.cca_stuck       = frameBody[7];
        Adapter->sta_info.false_rxstart   = frameBody[8];
        Adapter->sta_info.false_cca       = frameBody[9];
        Adapter->sta_info.rx_data         = frameBody[10];
        Adapter->sta_info.tx_rate         = frameBody[11];
        Adapter->sta_info.false_trigger   = frameBody[12];
        Adapter->sta_info.signal_power    = frameBody[13];
        Adapter->sta_info.noise_pwr       = frameBody[14];
        Adapter->sta_info.rssi_loc        = frameBody[15];
        Adapter->sta_info.tx_ack          = frameBody[16];
        Adapter->sta_info.rx_ack          = frameBody[17];

        return;
      }
         
    case MGMT_DESC_TYP_BEACONS_RESP:
      {
	Adapter->connectedAP.signal_lvl = ganges_calculate_rssi(Adapter,MgmtRcvFrame);
        return;
      }
 
    /* This frame will come whenever AP update their WMM element in beacon*/
    case MGMT_DESC_TYP_UPDATE_QOS_PARAM_REQ:
      {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Update QOS params req received\n");
        if(Adapter->qos_enable) 
        {
          /*entire beacon we received process the beacon and take wmm element*/
          UINT8  *beacon = &MgmtRcvFrame[16];
          UINT8 WLAN_WIFI_OUI[] = {0x0, 0x50, 0xF2};
          UINT32   ii;
          
          for(ii=20; ii< MngPacketLen; ii += beacon[ii+1] + 2)
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: %x\n", beacon[ii]);
            if(beacon[ii] == WLAN_EID_GENERIC)
            {
              genIe_t *genIe = (struct wlan_gen_ie_t *)&beacon[ii];
              if(!ganges_memcmp( genIe->oui, WLAN_WIFI_OUI, 3) && 
                     ( genIe->ouiType == WIFI_WMM_OUI_TYPE ) && 
                     ( beacon[ii + 6] == WMM_PARAM_IE_SUBTYPE ))  
              {
                /* Install the updated QOS parameters */
                RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Installing updated QOS parms\n");
                ganges_send_qos_params(Adapter, genIe);
                break;
              }
            }
          }
          return;                         
        }
      }
    case MGMT_DESC_TYP_QOS_PARAM_CFM:
      {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: QOS parms confirm received\n");
        return;
      }
    case MGMT_DESC_TYP_RESET_MAC_CFM:
      {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Reset MAC confirm\n");
        ganges_memset(&Adapter->connectedAP,0,sizeof(ScanConfirm));
        ganges_indicate_connect_status(Adapter);
        ganges_send_LMAC_prog_vals(Adapter);
  	FSM_STATE    = FSM_LMAC_PROG_VALS_SENT;
        return;
      }
    /* Associated AP's not found send scan request if roaming is enabled*/
    case MGMT_DESC_TYP_GIVE_SCAN_CMD:
      RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: received Give Scan command\n");
      if(Adapter->ScanAllChannel)
       Adapter->ScanChannelNum=0;
      if((FSM_STATE == FSM_OPEN) || (FSM_STATE == FSM_NO_STATE))
      {
        FSM_STATE = FSM_NO_STATE;
        if((PS_FSM_STATE == PS_FSM_AUTO))
        {
          flags=1;
          ganges_send_power_save_req(Adapter,DISABLE_PWR_SAVE);
        }
        else if(PS_FSM_STATE == PS_FSM_DEEP_SLEEP)
        {
          UINT8 byte=0x01;
	  ganges_send_ssp_wakeup_cmd(Adapter,byte);
    	  ganges_send_ssp_wakeup_cmd_read(Adapter);
          Adapter->halt_flag=1;
          flags=1;
        }
        else
        {
          ganges_send_reset_mac(Adapter);
          ganges_send_rate_symbol_request(Adapter);
        }
      } 
      else
      {
        FSM_STATE = FSM_NO_STATE;
        ganges_send_reset_mac(Adapter);
        ganges_send_rate_symbol_request(Adapter);
      }
      return;
    case MGMT_DESC_TYP_DEAUTH_IND:
      Adapter->roam_ssid.SsidLength = 0;
      ganges_memset(Adapter->roam_ssid.Ssid,0,32);
      ganges_memset(Adapter->roam_bssid,0,6);
      Adapter->wpa_splcnt.group_keys_installed  = 0;
      
      /* Clean-up the connected AP member in the Adapter structure */
      ganges_memset(&Adapter->connectedAP, 0, sizeof(ScanConfirm));      
    case MGMT_DESC_TYP_DEAUTH_CFM:
      if(FSM_STATE == FSM_NO_STATE)
      {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm:Received DeAuthentication in NOSTATE\n");
        ganges_send_reset_mac(Adapter);
        ganges_send_rate_symbol_request(Adapter);
        return;
      } /* This condition in FSM_OPEN will be handled later */
      Adapter->wpa_enabled = 0;
      break;

    case MGMT_DESC_TYP_DATA_FRM_STS_IND:
      RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Received data frame Indication\n");
      if(Adapter->wpa_enabled)
      {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Installing pair keys\n");
        ganges_send_pair_key_install(Adapter);
      }
      return;
            
    case  MGMT_DESC_TYP_FORM_IBSSDS_REQ:
      {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Received Form IBSS DS req\n");
        if(mgmtRcvFrame[1]==1)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Requested to transmit probe req\n");
          ganges_send_probe_request(Adapter, 
			            ACTIVE_SCAN,
                                    Adapter->ScanChannelNum, 
                                    0, 
                                    &MgmtRcvFrame[4]);
          return;
        }
        if(mgmtRcvFrame[1] == 2)
        {
        }
        else
        {
          /* parse the received beacon info and send the 
           * IBSS DS response */
          if(MngPacketLen< 5)
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: zero sized form ibss ds\n");
            return;
          }
          copyScanConfirm = TRUE;
          /* For each BSSID we have to serach in the list */
          for(ii = 0; ii < Adapter->BssidFound; ii++) 
          {
            /* Does it match with the recently scanned BSSID */
            if(ganges_memcmp(Adapter->scanConfirmVarArray[ii]->BSSID,
                      &mgmtRcvFrame[MGMT_FRAME_DESC_SZ/2], 6) == 0)
            {
              copyScanConfirm = FALSE ;
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: ibss ds response\n");
              ganges_send_ibssds_response(Adapter,
                                          &MgmtRcvFrame[4], 
                                          Adapter->scanConfirmVarArray[ii]->ProtocolType);
              if(Adapter->encryptAlgo == Ndis802_11WEPEnabled)
              {
                RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Sending WEP keys\n");
                ganges_send_WEPKeys_request(Adapter, 
                                            Adapter->RSIWepkey, 
                                            Adapter->WepKeyLength, 
                                            Adapter->WepKeyIndex,
                                            &MgmtRcvFrame[4], 
                                            0); /* wep keys flag */
                ganges_send_WEPKeys_request(Adapter, 
                                            Adapter->RSIWepkey, 
                                            Adapter->WepKeyLength, 
                                            Adapter->WepKeyIndex,
                                            &MgmtRcvFrame[4], 
                                           1); /* group keys flag */
                /*FIXME we are not caring response came are not */

              }
              if(FSM_STATE != FSM_OPEN)
              {
                FSM_STATE = FSM_OPEN;
                //ganges_carrier_on(Adapter->dev);                
                ganges_memcpy(&Adapter->connectedAP.BSSID[0], Adapter->Ibss_info.BSSID, 6);
                ganges_netif_start_queue(Adapter->dev);
                Adapter->connectedAP.SSIDLen = Adapter->pssid.SsidLength;
                ganges_memcpy(&Adapter->connectedAP.SSID[0], Adapter->pssid.Ssid,
                              Adapter->connectedAP.SSIDLen);
                ganges_indicate_connect_status(Adapter);
              }
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Joined to existed IBSS cell\n");
              return;
            }
          }
          if(copyScanConfirm == TRUE)
          {
            if(ganges_copy_scanconfirmInfo(Adapter, 
                                           (UINT8 *)&mgmtRcvFrame[
                                           MGMT_FRAME_DESC_SZ/2],
                                           Adapter->scanConfirmVarArray, 
                                           MngPacketLen)
                                  == 0)/*FIXME*/
            {
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: ibss ds response\n");
              ganges_send_ibssds_response(Adapter,
                                          &MgmtRcvFrame[4], 
                                          Adapter->scanConfirmVarArray[Adapter->BssidFound]->
                                          ProtocolType);
              if(Adapter->encryptAlgo == Ndis802_11WEPEnabled)
              {
                RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Sending WEP keys\n");
                ganges_send_WEPKeys_request(Adapter, 
                                            Adapter->RSIWepkey, 
                                            Adapter->WepKeyLength, 
                                            Adapter->WepKeyIndex,
                                            &MgmtRcvFrame[4], 
                                            0); /* wep keys flag */
                ganges_send_WEPKeys_request(Adapter, 
                                            Adapter->RSIWepkey, 
                                            Adapter->WepKeyLength, 
                                            Adapter->WepKeyIndex,
                                            &MgmtRcvFrame[4], 
                                            1); /* group keys flag */
                /*FIXME we are not caring response came are not */

              }
              Adapter->BssidFound++;
              if(FSM_STATE != FSM_OPEN)
              {
                FSM_STATE = FSM_OPEN;
                //ganges_carrier_on(Adapter->dev);               
                ganges_memcpy(&Adapter->connectedAP.BSSID[0], Adapter->Ibss_info.BSSID, 6);
                ganges_netif_start_queue(Adapter->dev);
                Adapter->connectedAP.SSIDLen = Adapter->pssid.SsidLength;
                ganges_memcpy(&Adapter->connectedAP.SSID[0], Adapter->pssid.Ssid,
                              Adapter->connectedAP.SSIDLen);
                ganges_indicate_connect_status(Adapter);
 
              }
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: New station Joined in the cell\n");
            }
            else
            {  
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Failed to allocate scanconfirmarray\n");
            }
          }
        }
        return;
      }
    case  MGMT_DESC_TYP_MEASURE_NOISE_RSSI_CFM:
      {
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Received RSSI Noise confirm %d\n",MngPacketLen);
         if(Adapter->rssi_nfm_mode == RPS_RSSI_NFM_PERIODIC)
         {
           Adapter->periodic_rssi_nfm_count += (UINT16)MgmtRcvFrame[16];
           Adapter->periodic_rssi_nfm_smpl_cnt++;
         }
         else if(Adapter->rssi_nfm_mode == RPS_RSSI_NFM_POWERUP)
         {
           INT32    ii;
           UINT16 rssi_nfm_avg = 0;

           if(MngPacketLen < RPS_RSSI_PWRUP_NFM_CNT * 2)
           {
             RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Not received correct number of vals\n");
             return;
           }

           for(ii=0;ii<RPS_RSSI_PWRUP_NFM_CNT;ii++)
           {
             rssi_nfm_avg += mgmtRcvFrame[ii+8];
           }
           rssi_nfm_avg /= RPS_RSSI_PWRUP_NFM_CNT;
           RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: RSSI noise avg val 0x%x\n",rssi_nfm_avg);
           RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: sending update RSSI noise request\n");
           ganges_send_update_noise_rssi_request(Adapter, rssi_nfm_avg);
         }
         return;
      }
   
    case  MGMT_DESC_TYP_DISASSOCIAT_CFM:
      {
	if(FSM_STATE == FSM_NO_STATE)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: **Received Disassociation cofirm ***\n");
          ganges_send_reset_mac(Adapter);
          ganges_send_rate_symbol_request(Adapter);
          return;
        }
	break;	
      }

    case  MGMT_DESC_TYP_MIC_ERR_IND:
      {
        switch(MgmtRcvFrame[5])
        {
          case 0:
            RSI_DEBUG(RSI_ZONE_ERROR,"ganges_mgmt_fsm: !!!Malfunction No activity!!!\n");
            break;
          case 1:
            RSI_DEBUG(RSI_ZONE_ERROR,"ganges_mgmt_fsm: !!! ICV error max reached!!!\n");
            break;
          case 2:
            {
              if((Adapter->authMode == Ndis802_11AuthModeWPAPSK) ||
                 (Adapter->authMode == Ndis802_11AuthModeWPA2PSK))
              {
	        if(mgmtRcvFrame[1] == 0)
                { 
                  RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: !!!MIC Pair key error!!!\n");
                  ganges_indicate_mic_failure(Adapter,0);
                }
                else
                {
                  RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: !!!MIC Grp key error!!!\n");
                  ganges_indicate_mic_failure(Adapter,1);
                }
              }
	      break;
            }
          case 3:
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: !!!MIC error Replay detected!!!\n");
            break;
        }
        return;
      }
    case MGMT_DESC_TYP_PS_IND:
      {
        ganges_ps_fsm(Adapter, 
                      mgmtRcvFrame[1], 
                      mgmtRcvFrame[RSI_DESC_LEN/2]); 
        return;
      }
    case MGMT_DESC_TYP_GIVE_BEACON_REQ:
      {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Give beacon received\n");
        return;
      }
  }

  switch(FSM_STATE)
  {
    case FSM_CARD_NOT_READY:
       if(type == MGMT_DESC_TYP_CARD_RDY_IND)
       {
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: CARD_READY\n");
         if(ganges_load_LMAC_instructions(Adapter)== 0)
         {
           RSI_DEBUG(RSI_ZONE_FSM,
                     "ganges_interrupt_handler: Firmware loaded successfully\n");
           FSM_STATE = FSM_FIRMWARE_LOADED;
         }
         else
         {
           RSI_DEBUG(RSI_ZONE_ERROR,
                     "ganges_interrupt_handler: Failed to load firmware\n");
         }
       }
       break;
    case FSM_FIRMWARE_LOADED:
       if( type == MGMT_DESC_TYP_FIRMWARE_CFM)
       {
         if(mgmtRcvFrame[1] == 0)
         {
           RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: state: FW_LOADED  SUCCESS\n");
#ifdef EEPROM_READ_POWER_VALS
	   EEPROM_WRITE write_buf;
	   write_buf.length  = 16;
	   write_buf.off_set = 0x016;
	   ganges_eeprom_write(Adapter,&write_buf); 
#endif
	   if(ganges_read_cardinfo(Adapter)!=GANGES_STATUS_SUCCESS)
	   {
	     RSI_DEBUG(RSI_ZONE_ERROR,"ganges_mgmt_fsm: MAC Address read unsuccesful\n");
	   }		   
           /* Read additional info from such as power value read */
            /* Read Power Set vals info */
           ganges_send_reset_request(Adapter,
                                     Adapter->BBProgramVal[0],
                                     &Adapter->BBProgramVal[1]);
           FSM_STATE = FSM_RESET1_SENT;           
         }
         else
         {
           RSI_DEBUG(RSI_ZONE_ERROR,"ganges_mgmt_fsm: state: FW_LOADED FAILURE\n");
         }
       }
       else
       {
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: state: Expecting FW_LOADED\n");
       }
       break;
       
         
    case FSM_RESET1_SENT:
      {
        if(type == MGMT_DESC_TYP_RESET_CFM)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: RESET1 confirm\n");
          ganges_sendRFProgRequest(Adapter, 
                                   Adapter->RFType, 
                                   Adapter->resetChannelProgVal[0],
                                   &Adapter->resetChannelProgVal[1]);
          FSM_STATE = FSM_RF_REQ_SENT;
        }
        break;
      }
    case FSM_RF_REQ_SENT:
      {
        if(type == MGMT_DESC_TYP_RF_PROG_CFM)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: RF confirm\n");
          if(Adapter->RFType == RSI_RF_AL2236)
          {
            ganges_send_reset_request(Adapter,
                                      Adapter->BBProgramVal_set2[0],
                                      &Adapter->BBProgramVal_set2[1]);
          }
          else
          {
            ganges_send_reset_request(Adapter,
                                      Adapter->BBProgramVal[0],
                                      &Adapter->BBProgramVal[1]);
          }         
          FSM_STATE = FSM_RESET2_SENT;
        }
        break;
      }
    case FSM_RESET2_SENT:
      {
        if(type == MGMT_DESC_TYP_RESET_CFM)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: RESET2 confirm\n");
          if(Adapter->RFType == RSI_RF_AL2236)
          {
            ganges_sendRFProgRequest(Adapter, 
                                     Adapter->RFType, 
                                     Adapter->resetChannelProgVal_set2[0],
                                     &Adapter->resetChannelProgVal_set2[1]);

            ganges_send_reset_request(Adapter,
                                      Adapter->BBProgramVal_set3[0],
                                      &Adapter->BBProgramVal_set3[1]);

          }
          ganges_send_rate_symbol_request(Adapter);
#ifdef GANGES_PER_MODE 
          /* PER mode is enabled we should send scan request to start
           * stats*/
          if(Adapter->ppe_per_mode == TRUE)
          {
            ganges_send_probe_request(Adapter, PASSIVE_SCAN,Adapter->ScanChannelNum,1,NULL);
          }
#else
#ifdef ENABLE_STATS
          /* Start Station stats timer */
          RSI_DEBUG(RSI_ZONE_FSM,
                    (TEXT("init_adapter: Starting station stats timer\n")));
          ganges_get_station_statistics(Adapter,1000);//msecs
          /*FIXME do some thing in linux version */
          /*NdisSetTimer(&Adapter->station_stats_timer, 1000);*/
#endif
#endif 
          ganges_send_LMAC_prog_vals(Adapter);
	  FSM_STATE    = FSM_LMAC_PROG_VALS_SENT;
        }
        break;
      }
    case FSM_LMAC_PROG_VALS_SENT:
      {
        switch(type)
        {
          case MGMT_DESC_TYP_LMAC_PROG_CFM:
	    {
	      RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Recvd Load vals cfm\n");
	      if(req != PS_FSM_DISABLE)
              {
	        ganges_send_power_save_req(Adapter,DEEP_SLEEP_REQUEST); 
	        FSM_STATE = FSM_DEEP_SLEEP_REQ_SENT;  
              } 
	      else
               FSM_STATE = FSM_NO_STATE; 
	      break;	      	 
            }
	  default:
	    {
	      ganges_find_mgmt_type(type);   	
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Invalid mgmt frame in FSM_PROG_VALS_SENT \n");
	      break;
            } 
          break;
        }
	break;
      }			
    case FSM_DEEP_SLEEP_REQ_SENT:
      {
        switch(type)
        {
	  case MGMT_DESC_TYP_PWR_SAVE_CFM:
	    {
	      RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Pwr save cfm received\n"); 	              
              {
	        RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Present state FSM_NO_STATE\n");   
	        Adapter->halt_flag=0; 
	        ganges_Wakeup_Event(&PwrSaveEvent);
	        RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Waking up event\n");
	        FSM_STATE = FSM_NO_STATE; 	
              }
	      break;	
	    }	
	  default:
	    {
	      ganges_find_mgmt_type(type);   	
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Invalid mgmt frame in PWR_SAVE_REQ_SENT\n");
	      break;
            } 
          break;
        }
        break; 
      } 

    case FSM_NO_STATE:
      {
        switch(type)
        {
          case MGMT_DESC_TYP_PRB_RSP_CFM:
            {
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: probe response confirm received\n");
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Starting IBSS\n");
              ganges_start_ibss(Adapter);
              break;
            }
	  case MGMT_DESC_TYP_LMAC_PROG_CFM:
	    {
	      RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: LMAC prog cfm received\n");
	      break;    	
	    }
	  case MGMT_DESC_TYP_PWR_SAVE_CFM:
            {
              RSI_DEBUG(RSI_ZONE_INFO, "ganges_mgmt_fsm: Power save cfm recvd\n");
  	      ganges_Wakeup_Event(&PwrSaveEvent);
	      RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Waking up event\n");
	      Adapter->halt_flag=0;
              break;
            }
	 case MGMT_DEEP_SLEEP_WAKEUP_CFM:
            {
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Recvd wakeup cfm\n");
  	      ganges_Wakeup_Event(&PwrSaveEvent);
	      RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Waking up event\n");
	      Adapter->halt_flag=0;
	      if(flags) 
              {
                ganges_send_reset_mac(Adapter);
                ganges_send_rate_symbol_request(Adapter);
	        flags=0;
              }
	      break;
	    }	
         default:
            {
	      ganges_find_mgmt_type(type);
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: invalid mgmt packet in FSM_NO_STATE\n");
              break;
            }
        }
        break;
      }
        
    case FSM_SCAN_REQ_SENT:
    case FSM_SCAN_REQ_SENT_INOPEN:
      {
        if((type == MGMT_DESC_TYP_SCAN_RSP) ||
           (type == MGMT_DESC_TYP_RCVD_PRB_RESP))
        {
          if( (type == MGMT_DESC_TYP_SCAN_RSP) && 
              (mgmtRcvFrame[1] == SCAN_RSP_COMPLETED))
          {
            case SCAN_RSP_COMPLETED:
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: SCAN_COMPLETED\n");
              if(Adapter->ScanAllChannel == TRUE)
              { /* scan all channels */
                Adapter->ScanChannelNum++;
                if(Adapter->ScanChannelNum < 12)
                { /* scan next channel */

                  if(FSM_STATE == FSM_SCAN_REQ_SENT_INOPEN)
                  {
                    /* In connect state we will allow only active scan */
                    ganges_send_probe_request(Adapter, 
					      ACTIVE_SCAN,  	
                                              Adapter->ScanChannelNum, 
                                              1, 
                                              NULL);
                  }
                  else
                  {
                    if(Adapter->active_scan)
                    {
		      if(Adapter->hidden_mode)
                      {
                        ganges_send_probe_request(Adapter,
					          ACTIVE_SCAN, 
                                                  Adapter->ScanChannelNum, 
                                                  1, 
                                                  (UINT8 *)&Adapter->scan_ssid);
                      }
                      else
		      {	
                        ganges_send_probe_request(Adapter, 
					          ACTIVE_SCAN, 
                                                  Adapter->ScanChannelNum, 
                                                  1, 
                                                  NULL);
		      }  	
                    }
                    else
                    {
                      ganges_send_probe_request(Adapter, 
				                PASSIVE_SCAN,
					        Adapter->ScanChannelNum,
						1,
						NULL);
                    }
                  }
                  break;
                }
                else
                { /* scan completed */
                  RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: All Channel Scan Completed\n");
                  Adapter->ScanChannelNum = 0;
                /* do channel program here in connected channel if we are in 
                 * connected state*/
                if(FSM_STATE == FSM_SCAN_REQ_SENT_INOPEN)
                {
                  UINT32 channel;
                  
                  if(Adapter->NetworkType == Ndis802_11Infrastructure)
                  {
                    channel = Adapter->connectedAP.PhyParam[0];
                  }
                  else
                  {
                    channel = Adapter->Ibss_info.channelNum;
                  }
                  /* Send scan request with previous connected channel */
                  ganges_send_probe_request(Adapter, 
					    ACTIVE_SCAN,
                                            channel,
                                            1, 
                                            NULL); 
                }
              }
            }
            if(PS_FSM_STATE != PS_FSM_DISABLE)
            {
	      //Sending deep sleep request frame 
	      ganges_send_power_save_req(Adapter,DEEP_SLEEP_REQUEST);
            }
            /* restore the state back */
            if(FSM_STATE == FSM_SCAN_REQ_SENT_INOPEN)
            {
              FSM_STATE = FSM_OPEN;
              ganges_indicate_scan_complete(Adapter);           
            }
            else
            {
              FSM_STATE = FSM_NO_STATE;
              ganges_indicate_scan_complete(Adapter);           
              
              /* Check wether any join requests are in pending */
              if(Adapter->JoinRequestPending == 1)
              {
                RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Join Pending\n");
                ganges_set_bssid(Adapter, Adapter->pbssid);
              }
              if(Adapter->JoinIBSSPending)
              {
                RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Join IBSS Pending\n");
                ganges_set_ssid(Adapter, &Adapter->pssid);
                Adapter->JoinIBSSPending = 0;
              }
            }
          }
          else if((type == MGMT_DESC_TYP_RCVD_PRB_RESP) || 
                   ((type == MGMT_DESC_TYP_SCAN_RSP) && 
                    (mgmtRcvFrame[1] == SCAN_RSP_BSSID_FOUND)))
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: BSSID found\n");
            copyScanConfirm = TRUE;
            
            /* For each BSSID we have in the list */
            for(ii = 0; ii < Adapter->BssidFound; ii++) 
            {
              /* Does it match with the recently scanned BSSID */
              if(ganges_memcmp(Adapter->scanConfirmVarArray[ii]->BSSID,
                        &mgmtRcvFrame[MGMT_FRAME_DESC_SZ/2], 6) == 0)
              {
	        Adapter->scanConfirmVarArray[ii]->signal_lvl =
                              ganges_calculate_rssi(Adapter,(UINT8 *)MgmtRcvFrame);
                copyScanConfirm = FALSE ;
                break ;
              }
            }

            if(copyScanConfirm == TRUE)
            {
              if(ganges_copy_scanconfirmInfo(Adapter, 
                                             (UINT8 *)&mgmtRcvFrame[
                                             MGMT_FRAME_DESC_SZ/2],
                                             Adapter->scanConfirmVarArray,
                                             MngPacketLen)
                               == GANGES_STATUS_SUCCESS)
              {
	        Adapter->scanConfirmVarArray[Adapter->BssidFound]->signal_lvl =
                              ganges_calculate_rssi(Adapter,(UINT8 *)MgmtRcvFrame);
                Adapter->BssidFound++;
              }
              else
              {
                RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Failed to allocate scanconfirmarray\n");
              }
            }
            //break ;
          }
          else
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Unknow scan resp type\n");
        }
        else
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Expecting scan response\n");
        }
        break;
      } /* End of scan response */


    case FSM_JOIN_REQ_SENT:
      {

        if(type == MGMT_DESC_TYP_JOIN_CFM)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Recieved JOIN response %d-->\n ",
                      mgmtRcvFrame[1]);
          if(mgmtRcvFrame[1] != 0 && mgmtRcvFrame[1] != 1)
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: FAILED %d\n", mgmtRcvFrame[1]);
           
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm:Sending scan request\n");
            FSM_STATE = FSM_NO_STATE;
	    Adapter->power_state = 1;
            ganges_do_scan(Adapter,0,NULL);
            break;
          }
          else
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: SUCCESS\n");
          }
          /* Check if the access point supports wep in the first place. This 
           * could be checked through capability info. If it supports wep and 
           * you indeed have wep key in place then send shared key request 
           * otherwise reset your system to open key and send open key 
           * request. */
          if(Adapter->NetworkType == Ndis802_11IBSS)
          {
            FSM_STATE = FSM_IBSS_CREATED;
           /* Copy the info related to connected AP */
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: IBSS JOIN SUCCESS\n");
            break;
          }
            if(Adapter->encryptAlgo == Ndis802_11WEPEnabled) 
            {
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Sending WEP keys\n");
              ganges_send_WEPKeys_request(Adapter, 
                                          Adapter->RSIWepkey, 
                                          Adapter->WepKeyLength, 
                                          Adapter->WepKeyIndex,
                                          Adapter->scanConfirmVarArray[Adapter->
                                          SelectedBssidNum]->BSSID,
                                          0); /* wep keys flag */
              FSM_STATE = FSM_SET_WEPKEYS_SENT;
            }
          else
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: open authetication request being sent\n");
            Adapter->encryptAlgo = Ndis802_11WEPKeyAbsent;
            ganges_send_auth_request(Adapter,
                                     (UINT8*)((Adapter->
                                               scanConfirmVarArray[Adapter->
                                               SelectedBssidNum])->BSSID),
                                     Adapter->encryptAlgo);
            FSM_STATE = FSM_AUTH_REQ_SENT;
          }
        }
        else
        {
          RSI_DEBUG(RSI_ZONE_ERROR,"ganges_mgmt_fsm: Expecting join confirm\n");
        }
        break;
      }
    case FSM_SET_WEPKEYS_SENT:
      {
        if(type == MGMT_DESC_TYP_SET_KEYS_CFM)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: received wep keys confirm\n");
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: sending group keys\n");
          FSM_STATE = FSM_SET_WEPGRP_KEYS_SENT;       
          ganges_send_WEPKeys_request(Adapter, 
                                      Adapter->RSIWepkey, 
                                      Adapter->WepKeyLength, 
                                      Adapter->WepKeyIndex,
                                      Adapter->scanConfirmVarArray[Adapter->
                                      SelectedBssidNum]->BSSID,
                                      1); /* group keys flag */
        }
        else
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Expecting set keys confirm\n");
        }
        break;

      }
    case FSM_SET_WEPGRP_KEYS_SENT:
      {
        if(type == MGMT_DESC_TYP_SET_KEYS_CFM)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Received group keys confirm\n");
          if(Adapter->NetworkType == Ndis802_11IBSS)
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Starting IBSS\n");
            ganges_start_ibss(Adapter);
            break;
          }
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Sending Authentication Request\n");
          ganges_send_auth_request(Adapter,
                                   (UINT8*)(Adapter->scanConfirmVarArray[
                                   Adapter->SelectedBssidNum])->BSSID,
                                   Adapter->authMode);
          FSM_STATE = FSM_AUTH_REQ_SENT;       
        }
        else
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Expecting set group keys confirm\n");
        }
        break;
      }
     
    case FSM_AUTH_REQ_SENT:
      {
        if(type == MGMT_DESC_TYP_AUTH_CFM)
        {
          if(mgmtRcvFrame[1] == MGMT_SUCCESS)
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: !!!!!!Authentication SUCCESS!!!!!!\n");
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Sending association request\n");
            ganges_send_Associate_request(Adapter, 
                                          (UINT8*)Adapter->scanConfirmVarArray[
                                          Adapter->SelectedBssidNum]->BSSID,
                                          Adapter->scanConfirmVarArray[
                                          Adapter->SelectedBssidNum]);
            FSM_STATE = FSM_ASSOCIATE_REQ_SENT;
          }
          else
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: !!!!! Authentication FAILED !!!!! %d\n",
                         mgmtRcvFrame[1]);
            FSM_STATE = FSM_NO_STATE ;
            /* In WEP mode first we will try shared authentication
             * if it fails try open authentication */
            if((mgmtRcvFrame[1] ==  13) && 
                (Adapter->authMode == Ndis802_11AuthModeShared))
            {
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Trying Open Authentication\n");
              ganges_send_auth_request(Adapter,
                                       (UINT8*)(Adapter->scanConfirmVarArray[
                                       Adapter->SelectedBssidNum])->BSSID,
                                       Ndis802_11AuthModeOpen);
              /* This will break the loop */
              Adapter->authMode = Ndis802_11AuthModeOpen;
              FSM_STATE = FSM_AUTH_REQ_SENT;   
            }
            if(PS_FSM_STATE != PS_FSM_DISABLE)
	     ganges_send_power_save_req(Adapter,DEEP_SLEEP_REQUEST);	
          }
          break;
        }
        else
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Expecting authentication confirm\n");
          break;
        }
      }
    case FSM_ASSOCIATE_REQ_SENT:
      {
        if(type == MGMT_DESC_TYP_ASSOCIAT_CFM)
        {
          if(mgmtRcvFrame[1] == MGMT_SUCCESS)
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: !!!!!ASSOCIATION SUCCESS\n");
            FSM_STATE = FSM_OPEN;
            ganges_netif_start_queue(Adapter->dev);/*FIXME*/ 
            
            /* Copy the info related to connected AP */
            ganges_memcpy(&Adapter->connectedAP, 
                          Adapter->scanConfirmVarArray[Adapter->SelectedBssidNum],
                          sizeof(ScanConfirm));
            Adapter->qos_enable = 0; 
            if(Adapter->scanConfirmVarArray[Adapter
                                            ->SelectedBssidNum]->wmm_capable)
            {
              /* If selected AP is wmm capable then only send this 
               * qos_param request */
              Adapter->qos_enable = 1; 
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Sending QOS params\n");
              ganges_send_qos_params(Adapter, 
                                     &Adapter->scanConfirmVarArray[Adapter->SelectedBssidNum]->wmm_ie);
            }
            /* If we are in auto rate, send auto request,
             * we will not allow auto rate in IBSS mode */
            if((Adapter->TxRate == RATE_AUTO) &&
               (Adapter->NetworkType == Ndis802_11Infrastructure))
            {
              /* If auto rate is selected send this auto rate frame */
              RSI_DEBUG(RSI_ZONE_INFO,
                        "ganges_mgmt_fsm: Sending auto rate request\n");
              ganges_send_auto_rate_request(Adapter, 0, 0);
            }

            RSI_DEBUG(RSI_ZONE_INFO,
                      "ganges_mgmt_fsm: Sending TA STA config request\n");
            ganges_send_load_STA_config(Adapter);     
            ganges_indicate_connect_status(Adapter);

	    if(PS_FSM_STATE == PS_FSM_AUTO)
            {
	      ganges_send_power_save_req(Adapter,REGULAR_SLEEP_REQUEST);
            }              
	    else if(PS_FSM_STATE == PS_FSM_DEEP_SLEEP)
            {
              ganges_send_DeAuthentication_request(Adapter,
	     				           Adapter->connectedAP.BSSID,
						   0x03);         		
	      ganges_send_power_save_req(Adapter,DEEP_SLEEP_REQUEST);
	    }	
          }
          else
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: ****ASSOCIATION FAILED******\n");
            if(PS_FSM_STATE != PS_FSM_DISABLE)
	     ganges_send_power_save_req(Adapter,DEEP_SLEEP_REQUEST);	
            FSM_STATE = FSM_NO_STATE ;
          }
        }
        else
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Expecting associate confirm\n");
        }
        break;
      }
    case FSM_OPEN:
      {
        if(type == MGMT_DESC_TYP_SET_KEYS_CFM)
        {
           if(Adapter->encryptAlgo==Ndis802_11WEPEnabled)
          {
            RSI_DEBUG(RSI_ZONE_INFO,
                      "ganges_mgmt_fsm: Received WEP keys confirm\n");
          }
          else
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: received WPA pair keys confirm\n");
          }
          break;
        }
	else if(type == MGMT_DESC_TYP_PWR_SAVE_CFM)
        {
          //RSI_DEBUG(RSI_ZONE_INFO,
          //          "ganges_mgmt_fsm: Recvd regular power save confirmation\n");
          Adapter->halt_flag=0;
  	  ganges_Wakeup_Event(&PwrSaveEvent);
	  //RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Waking up event\n");
        }
        else if(type == MGMT_DEEP_SLEEP_WAKEUP_CFM)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Recvd wakeup cfm\n");
  	  ganges_Wakeup_Event(&PwrSaveEvent);
	  RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Waking up event\n");
          Adapter->halt_flag=0;
	  break;
	}
        else if((type == MGMT_DESC_TYP_DEAUTH_IND) ||
           (type == MGMT_DESC_TYP_DEAUTH_CFM))
        {
          Adapter->wpa_splcnt.group_keys_installed  = 0;
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: **Received DeAuthentication ***\n");
          FSM_STATE = FSM_NO_STATE;
 	  if((PS_FSM_STATE == PS_FSM_AUTO)||(PS_FSM_STATE == PS_FSM_DEEP_SLEEP))
	  {
            ganges_send_power_save_req(Adapter,DISABLE_PWR_SAVE);
            flags=1;
          }  
	  else
          {
            ganges_send_reset_mac(Adapter);
            ganges_send_rate_symbol_request(Adapter);
          }         
          //RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Sending scan request\n");
          //ganges_do_scan(Adapter);
        }
        else if((type == MGMT_DESC_TYP_DISASSOCIAT_IND) ||
                (type == MGMT_DESC_TYP_DISASSOCIAT_CFM))
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: **Received Disassociate ***\n");
          FSM_STATE = FSM_NO_STATE;
	  if((PS_FSM_STATE == PS_FSM_AUTO)||(PS_FSM_STATE == PS_FSM_DEEP_SLEEP))
	  {
	    ganges_send_power_save_req(Adapter,DISABLE_PWR_SAVE);
	    flags=1;
	  } 
	  else
          {
            ganges_send_reset_mac(Adapter);
            ganges_send_rate_symbol_request(Adapter);
          }
        }
        else
        {
	  ganges_find_mgmt_type(type);
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Received unknown packet in OPEN state\n");
        }
        break;
      }
        
    default:
      RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Unknown state\n");
      break;
  }
  return;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_copy_scanconfirmInfo
Description    : This routine parse the given beacon and allocate memory to 
                 to store AP info and copy the info to that structer.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

--------------------+-----+-----+-----+------------------------------
Name                | I/P | O/P | I/O | Purpose
--------------------+-----+-----+-----+------------------------------
Adapter             |     |  X  |     | pointer to our adapter
MgmtRcvFrame        |     |     |     | The buffer containing the received 
                                        managemnt frame
scanConfirmVarArray |     |     |     | The array which contains the information
                                        of all the BSSids found
MngPacketLen        |     |     |     | The length of managemnt packet received
*END**************************************************************************/

GANGES_STATUS 
ganges_copy_scanconfirmInfo
  (
    PRSI_ADAPTER Adapter,
    UINT8 mgmtRcvFrame[], 
    PScanConfirm scanConfirmVarArray[], 
    UINT32 MngPacketLen
  )
{
  UINT16 currentPtr = 0;
  UINT8  stopprocess=0;
  UINT32 ii;
  UINT8 eId, eIdLength;
  GANGES_STATUS Status;

  MngPacketLen -= RSI_DESC_LEN;
  
  RSI_DEBUG(RSI_ZONE_INFO,
            "ganges_copy_scanconfirmInfo %d\n",Adapter->BssidFound);
  
  scanConfirmVarArray[Adapter->BssidFound] = 
                    (ScanConfirm *)ganges_mem_alloc(sizeof(ScanConfirm),
                                                    GFP_KERNEL);   
  if(scanConfirmVarArray+Adapter->BssidFound == NULL)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"copy_scanconfirm: low on mem\n");
    Status = GANGES_STATUS_FAILURE;
    return Status;
  }
 
  /*The first thing that a scan confirm packet contains is bssid */
  ganges_memcpy(&scanConfirmVarArray[Adapter->BssidFound]->BSSID, 
                mgmtRcvFrame+currentPtr, 
                6); 
  
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: Copying Beacon info\n");
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: BSSID:\n");
  for(ii=0; ii<6; ii++)
  {
    RSI_DEBUG(RSI_ZONE_INFO,"%02x\n", mgmtRcvFrame[currentPtr+ii]);
  }
  currentPtr += 6;

  /* One word is left for reserved field */
  currentPtr += 2;
  
  /* Time Stamp */
  ganges_memcpy(&scanConfirmVarArray[Adapter->BssidFound]->TimeStamp, 
                mgmtRcvFrame+currentPtr, 
                8); 
  currentPtr += 8;

  /*Beacon interval */
  ganges_memcpy(&scanConfirmVarArray[Adapter->BssidFound]->Beacon, 
                mgmtRcvFrame+currentPtr, 
                2);
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: Beacon Interval %d\n", 
               scanConfirmVarArray[Adapter->BssidFound]->Beacon);
  currentPtr += 2;

  /*Capability Information */
  ganges_memcpy(&scanConfirmVarArray[Adapter->BssidFound]->CapabilityInfo, 
                mgmtRcvFrame+currentPtr, 
                2);

  RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: Capability Info %04x\n", 
               scanConfirmVarArray[Adapter->BssidFound]->CapabilityInfo);
  currentPtr += 2;

  scanConfirmVarArray[Adapter->BssidFound]->wpa_capable = 0;
  scanConfirmVarArray[Adapter->BssidFound]->rsn_capable = 0;
  scanConfirmVarArray[Adapter->BssidFound]->wmm_capable = 0;
  scanConfirmVarArray[Adapter->BssidFound]->capable_11n = 0;
  scanConfirmVarArray[Adapter->BssidFound]->ht_capable  = 0;
  scanConfirmVarArray[Adapter->BssidFound]->ht_capable1  = 0;
  scanConfirmVarArray[Adapter->BssidFound]->wmmPS_capable = 0;


  while (1)
  {
     
    if(currentPtr >= MngPacketLen)
    {
      break;
    }
    eId          = mgmtRcvFrame[currentPtr];
    eIdLength    = mgmtRcvFrame[currentPtr+1];

    switch (eId)
    {
        
      case ELEMENT_SSID: //0
        /*
         * SSID can be of variable length. First word contains the length of the
         * ssid and then the ssid itself
         */
        if(eIdLength>32)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: Junk beacon!!!!!\n");
          stopprocess=1;
          break;
        }
        for(ii=0;ii<eIdLength;ii++)
        {
          if((mgmtRcvFrame[(currentPtr+2)+ii]<0x20) ||
             (mgmtRcvFrame[(currentPtr+2)+ii]>0x7E))
          {
            RSI_DEBUG(RSI_ZONE_ERROR,"ganges_scanconfirmInfo: SSID Field Invalid\n");
            stopprocess=1;
            goto out;
          } 
        }

        scanConfirmVarArray[Adapter->BssidFound]->SSIDLen =eIdLength;

        ganges_memcpy(&scanConfirmVarArray[Adapter->BssidFound]->SSID, 
                      mgmtRcvFrame+(currentPtr+2), 
                      eIdLength); /*SSID*/

        RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: SSID Len %d\n", 
                         scanConfirmVarArray[Adapter->BssidFound]->SSIDLen); 

        RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: SSID :\n");
        for(ii=0;ii<(UINT32)(scanConfirmVarArray[Adapter->BssidFound]->SSIDLen); ii++)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"%c\n",scanConfirmVarArray[Adapter->BssidFound]->SSID[ii]);
        }
        break;
     
      case ELEMENT_EXT_RATES:  //0x32
        /*Supported rates is again variable length. */
        scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRatesLen = eIdLength;
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: Extended Supported Rates Len %d\n", 
                eIdLength); 

        ganges_memcpy(&scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates, 
                      mgmtRcvFrame+(currentPtr+2), 
                      eIdLength); 
        
        for(ii=0;ii<scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRatesLen; ii++)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"%02x\n",scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii]);
          if((scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0x24) ||
             (scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0x30) ||
             (scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0x48) ||
             (scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0x6C) ||
             (scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0xA4) ||
             (scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0xB0) ||
             (scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0xC8) ||
             (scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0xEC) ||  
             (scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0x0C) ||
             (scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0x12) ||
             (scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0x18) ||
             (scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0x60) ||
             (scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0x8C) ||
             (scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0x92) ||
             (scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0x98) ||
             (scanConfirmVarArray[Adapter->BssidFound]->ExtSupportedRates[ii] == 0xE0))  
          {
            scanConfirmVarArray[Adapter->BssidFound]->ProtocolType = ProtoType802_11G;
          }

        }
        RSI_DEBUG(RSI_ZONE_INFO,"\n");
        if(scanConfirmVarArray[Adapter->BssidFound]->ProtocolType == 
                        ProtoType802_11B)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: AP in B only mode\n");
        }
        break;
      case ELEMENT_BASIC_RATESET:  
        /*Extended Supported rates is again variable length. */
        scanConfirmVarArray[Adapter->BssidFound]->SupportedRatesLen = eIdLength;
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: Supported Rates Len %d\n", 
                eIdLength); 

        ganges_memcpy(&scanConfirmVarArray[Adapter->BssidFound]->SupportedRates, 
                      mgmtRcvFrame+(currentPtr+2), 
                      eIdLength); 
        
        scanConfirmVarArray[Adapter->BssidFound]->ProtocolType = ProtoType802_11B;
        for(ii=0;ii<scanConfirmVarArray[Adapter->BssidFound]->SupportedRatesLen; ii++)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"%02x\n", 
                     scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii]);
          if((scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0x24) ||
             (scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0x30) ||
             (scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0x48) ||
             (scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0x6C) ||
             (scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0xA4) ||
             (scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0xB0) ||
             (scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0xC8) ||
             (scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0xEC) || 
             (scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0x0C) ||
             (scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0x12) ||
             (scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0x18) ||
             (scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0x60) ||
             (scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0x8C) ||
             (scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0x92) ||
             (scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0x98) ||
             (scanConfirmVarArray[Adapter->BssidFound]->SupportedRates[ii] == 0xE0))  
          {
            scanConfirmVarArray[Adapter->BssidFound]->ProtocolType = ProtoType802_11G;
          }

        }
        RSI_DEBUG(RSI_ZONE_INFO,"\n");
        if(scanConfirmVarArray[Adapter->BssidFound]->ProtocolType == 
                        ProtoType802_11B)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: AP in B only mode\n");
        }
        break;
      
      case ELEMENT_CHANNEL_NUM: 
        /*
         * We got phy param with EID 3. Copy this to phy param array.
         */
        scanConfirmVarArray[Adapter->BssidFound]->PhyParamLen =eIdLength;
        ganges_memcpy(&scanConfirmVarArray[Adapter->BssidFound]->PhyParam, 
                      mgmtRcvFrame+(currentPtr+2), 
                      eIdLength);
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: Channel number is %d\n",
               scanConfirmVarArray[Adapter->BssidFound]->PhyParam[0]);      
        /*FIXME check channel number */
        break;
      
      case ELEMENT_TIM:
        scanConfirmVarArray[Adapter->BssidFound]->DTIM_Param = 
                mgmtRcvFrame[currentPtr+3];
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: DTIM %x\n",
                   scanConfirmVarArray[Adapter->BssidFound]->DTIM_Param);
        break;
  
      case 6:
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: EID 6 Not Supported\n");
        break;    
      
      case WLAN_EID_GENERIC:
      {
        UINT8 WLAN_WIFI_OUI[] = {0x0, 0x50, 0xF2};
        UINT8 WLAN_WIFI_OUI_TMP[]= {0x00, 0x90, 0x4c};

        genIe_t *genIe;

        RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: GEN EID Length %d\n", eIdLength);

        genIe = (struct wlan_gen_ie_t *)&mgmtRcvFrame[currentPtr];
        if( !ganges_memcmp( genIe->oui, WLAN_WIFI_OUI, 3) && 
                      (genIe->ouiType == WIFI_WPA_OUI_TYPE) )
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: WPA capable AP\n");
          ganges_memcpy(scanConfirmVarArray[Adapter->BssidFound]->wpa_ie,
                        mgmtRcvFrame+currentPtr,
                        eIdLength+2);
          scanConfirmVarArray[Adapter->BssidFound]->wpa_capable = 1;
        }
        else if(!ganges_memcmp( genIe->oui, WLAN_WIFI_OUI, 3) && 
                       ( genIe->ouiType == WIFI_WMM_OUI_TYPE ) && 
                       ( mgmtRcvFrame[currentPtr + 6] == WMM_PARAM_IE_SUBTYPE ))  
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: WMM capable AP\n");
          
          if( genIe->wmm.ac_info & 0x80)
          {
            RSI_DEBUG(RSI_ZONE_INFO,"scanconfirmInfo:  WMM-PS capable AP\n");
            scanConfirmVarArray[Adapter->BssidFound]->wmmPS_capable = 1;
          }     
          if(!Adapter->config_legacy)
          {
            scanConfirmVarArray[Adapter->BssidFound]->wmm_capable = 1;
          }
          ganges_memcpy(&scanConfirmVarArray[Adapter->BssidFound]->wmm_ie, 
                        mgmtRcvFrame+currentPtr, 
			eIdLength + 2);
        }  
        else if( !ganges_memcmp( genIe->oui, WLAN_WIFI_OUI_TMP, 3) &&
                      (genIe->ouiType == 0x33) )
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: 11n capable AP\n");
          
          ganges_memcpy(&scanConfirmVarArray[Adapter->BssidFound]->ie_11n,
                        mgmtRcvFrame+currentPtr,
                        eIdLength+2);

          //NdisZeroMemory(&scanConfirmVarArray[Adapter->BssidFound]->ie_11n.temp);
          /* MCS set */

          scanConfirmVarArray[Adapter->BssidFound]->ie_11n.ele_11n.temp[0] = 0x00;
          scanConfirmVarArray[Adapter->BssidFound]->ie_11n.ele_11n.temp[1] = 0x00;
          scanConfirmVarArray[Adapter->BssidFound]->ie_11n.ele_11n.temp[2] = 0x18;
          scanConfirmVarArray[Adapter->BssidFound]->ie_11n.ele_11n.temp[3] = 0xFF;
          scanConfirmVarArray[Adapter->BssidFound]->ie_11n.ele_11n.temp[4] = 0x00;

          scanConfirmVarArray[Adapter->BssidFound]->capable_11n = 1;
        }
        else
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: Unknown WLN Generic element\n");
        }
      }
      break;    
    case WLAN_EID_RSN:
      {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanconfirmInfo: WPA2 capable AP\n");
        ganges_memcpy(scanConfirmVarArray[Adapter->BssidFound]->rsn_ie,
                      mgmtRcvFrame+currentPtr,
                      eIdLength+2);
        scanConfirmVarArray[Adapter->BssidFound]->rsn_capable = 1;
      }
      break;

    /* Included for 11n support */
    case ELEMENT_HT_CAPABILITY: //51
      {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanConfirmInfo: HT capable AP\n");

        scanConfirmVarArray[Adapter->BssidFound]->ht_capable = 1;
        ganges_memcpy(scanConfirmVarArray[Adapter->BssidFound]->htCap_ie,
                      (mgmtRcvFrame + currentPtr), 
                      (eIdLength + 2));
      }
      break;

    case  ELEMENT_HT_CAPABILITY1:
     {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanConfirmInfo: HT capable1 AP\n");

        scanConfirmVarArray[Adapter->BssidFound]->ht_capable1 = 1;
        ganges_memcpy(scanConfirmVarArray[Adapter->BssidFound]->htCap1_ie,
                      (mgmtRcvFrame + currentPtr), 
                      (eIdLength + 2));
        
        scanConfirmVarArray[Adapter->BssidFound]->htCap1_ie[2] = 0x00;
        scanConfirmVarArray[Adapter->BssidFound]->htCap1_ie[3] = 0x00;
        scanConfirmVarArray[Adapter->BssidFound]->htCap1_ie[4] = 0x18;
        scanConfirmVarArray[Adapter->BssidFound]->htCap1_ie[5] = 0xFF;
        scanConfirmVarArray[Adapter->BssidFound]->htCap1_ie[6] = 0x00;
     }
     break;

    default:
      {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_scanConfirmInfo: EID not supported: %d\n", eId);
      } 
      break;
    }
out:
    if(stopprocess) 
    {
      ganges_mem_free(scanConfirmVarArray[Adapter->BssidFound]);
      return GANGES_STATUS_FAILURE;
    }
    /*Goto next Information Element */
    currentPtr = currentPtr + eIdLength + 2;
  }
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_copy_scanconfirmInfo:\n");
  return GANGES_STATUS_SUCCESS;

}
/*FUNCTION*********************************************************************
Function Name  : ganges_ps_fsm
Description    : This routine maintians the Power save FSM states. 
                 
Returned Value : 
Parameters     :

--------------------+-----+-----+-----+------------------------------
Name                | I/P | O/P | I/O | Purpose
--------------------+-----+-----+-----+------------------------------
Adapter             |     |  X  |     | pointer to our adapter
*END**************************************************************************/

VOID 
ganges_ps_fsm
  (
    IN PRSI_ADAPTER Adapter, 
    IN UINT16       status,
    IN UINT16       type
  )
{
  switch(Adapter->power_sava_fsm)
  {
    case PWR_FSM_PARMS_SENT:
      {
        if(type == 0)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: received sleep parameters confirm\n");
          if(status)
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: FAILED\n");
            Adapter->power_sava_fsm=PWR_FSM_PARMS_FAILED;
          }
          else
          {
            RSI_DEBUG(RSI_ZONE_INFO,"SUCCESS\n");
            Adapter->power_sava_fsm=PWR_FSM_NORMAL;
#if POWER_SAVE /* send sleep command */
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: sending sleep request\n");
            Adapter->cur_power_mode = RPS_POWER_SLEEP_HOST_CMD;
            Adapter->power_sava_fsm = PWR_FSM_CMD_SENT;
            ganges_send_sleep_request(Adapter,
                                      PWR_SAVE_SOFT_CTRL|PWR_SAVE_ENABLE,
                                      0, /*wakeup_beacon_intrvl*/
                                      RPS_PROFILE_SLEEP1,
                                      RPS_CLK_GATING_ENABLES_SLEEP
                                      );
#endif

          }
        }
        else
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: expecting sleep params confirm %d\n",
                      type);
        }

        break;
      }
    case PWR_FSM_CMD_SENT:
      {
        if(type ==  1)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: received sleep command confirm\n");
          if(status)
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: FAILED\n");
            Adapter->power_sava_fsm=PWR_FSM_NORMAL;
          }
          else
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: SUCCESS\n");
            Adapter->power_sava_fsm=PWR_FSM_SLEEP;
            if(FSM_STATE == FSM_DEEP_SLEEP)
            {
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: Going to DEEP SLEEP\n");
#if 0 /* This only for PCI */
#define DEEP_SLEEP_MAX_PWR_SAVE  0x2
              rsi_outl(DEEP_SLEEP_CTRL_REG, DEEP_SLEEP_MAX_PWR_SAVE);
#endif
            }
          }
        }
        else
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: expecting sleep command confirm\n");
        }
       break;
      }
    case PWR_FSM_WAKEUP_SENT:
      {
        if(type == 2)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: received wakeup cmd confirm\n");
          if(status)
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm:FAILED\n");
            Adapter->power_sava_fsm=PWR_FSM_SLEEP;
          }
          else
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: SUCCESS\n");
            Adapter->power_sava_fsm=PWR_FSM_NORMAL;
          }
        }
        else
        {
           RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: expecting wakeup command confirm\n");
        }
        break;
      }
    case PWR_FSM_SLEEP:
      {
        if(type == 3)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: received wakeup indication from firmware\n");
          if(status)
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: FAILED\n");
          }
          else
          {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: SUCCESS\n");
            Adapter->power_sava_fsm=PWR_FSM_NORMAL;
          }

        }
        else
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: expecting wakeup indication\n");
        }
        break;
      }
    default:
      {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_ps_fsm: invalid power save fsm state\n");
      }
  }
}
