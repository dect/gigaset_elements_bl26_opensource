#ifndef __GANGES_DEFINES__
#define __GANGES_DEFINES__
#include "common.h"
#include "ganges_spi.h"
#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/if.h>
#include <linux/wireless.h>
#include <linux/wait.h>
#include <linux/vmalloc.h>
#include <linux/version.h>

#define BIT(n) (1 << (n))
#define TEXT(arg...) arg
#define RSI_DEBUG(zone, fmt, arg...) if(zone & ganges_zone_enabled) printk(fmt, ##arg)
#define RSI_ZONE_INFO           BIT(0)
#define RSI_ZONE_ERROR          BIT(1)
#define RSI_ZONE_INIT           BIT(2)
#define RSI_ZONE_OID            BIT(3)
#define RSI_ZONE_MGMT_SEND      BIT(4)
#define RSI_ZONE_MGMT_RCV       BIT(5)
#define RSI_ZONE_DATA_SEND      BIT(6)
#define RSI_ZONE_DATA_RCV       BIT(7)
#define RSI_ZONE_FSM            BIT(8)
#define RSI_ZONE_ISR            BIT(9)
#define RSI_ZONE_MGMT_DUMP      BIT(10)
#define RSI_ZONE_DATA_DUMP      BIT(11)
#define RSI_ZONE_PARSER         BIT(12)
#define RSI_ZONE_SPI_DBG        BIT(13)
#define RSI_ZONE_NFM_PATH       BIT(14)

#define ganges_carrier_on(dev)        netif_carrier_on(dev)
#define ganges_carrier_off(dev)       netif_carrier_off(dev)
#define ganges_vmalloc(temp)          vmalloc(temp)
#define ganges_vfree(ptr)	      vfree(ptr)

#define ganges_queue_init(a)          skb_queue_head_init(a)
#define ganges_queue_purge(a)         skb_queue_purge(a)
#define ganges_queue_tail(a,b)        skb_queue_tail(a,b)
#define ganges_queue_len(a)           skb_queue_len(a)
#define ganges_dequeue(a)             skb_dequeue(a)
#define ganges_skb_put(a,b)           skb_put(a,b)
#define ganges_kfree_skb(a)           dev_kfree_skb(a)

#define ganges_spinlock_init(a)       spin_lock_init(a)
#define ganges_lock_bh(a)             spin_lock_bh(a)
#define ganges_unlock_bh(a)           spin_unlock_bh(a)

#define ganges_memcpy(a,b,c)          memcpy(a,b,c)
#define ganges_memset(a,b,c)          memset(a,b,c)
#define ganges_memcmp(a,b,c)          memcmp(a,b,c)
#define ganges_strcpy(a,b)            strcpy(a,b)
#define ganges_strcat(a,b)            strcat(a,b)
#define ganges_strcpy(a,b)            strcpy(a,b)
#define ganges_strncpy(a,b,c)         strncpy(a,b,c)
#define ganges_equal_string(a,b)      strcmp(a,b)
#define ganges_simple_strtol(a,b,c)   simple_strtol(a,b,c)

#define ganges_Signal_Pending()       signal_pending(current) 
#define ganges_netif_rx(a)            netif_rx_ni(a)
#define ganges_netif_start_queue(a)   netif_start_queue(a)
#define ganges_netif_queue_stopped(a) netif_queue_stopped(a)
#define ganges_netif_stop_queue(a)    netif_stop_queue(a)
#define ganges_sprintf                sprintf 
#define ganges_mem_free(ptr)          kfree(ptr)
#define ganges_mem_alloc(a,b)             kmalloc(a,b)

#define ganges_copy_to_user(a,b,c)    copy_to_user(a,b,c)
#define ganges_copy_from_user(a,b,c)  copy_from_user(a,b,c)
#define ganges_getpriv(a)             netdev_priv(a)

#define EVENT_WAIT_FOREVER       (0x00)
#define ganges_Delete_Event(pEvent)                  

#define ganges_eth_type_trans(a,b)    eth_type_trans(a,b)
#define ganges_down_interruptible(a)  down_interruptible(a)
#define ganges_up_sem(a)              up(a)
#define ganges_init_mutex(a)          init_MUTEX(a)
#define ganges_schedule()             schedule()

#define BIN_FILE 0
#define HEX_FILE 1

#define GANGES_THREAD_NAME_LEN      15
#define GANGES_THREAD_PRIORITY      0
#define GANGES_INT_THREAD_PRIORITY  0

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,5,0)
typedef  struct work_struct         GANGES_WORK_QUEUE;
#define  ganges_Init_Work_Queue(a,b,c)  INIT_WORK(a,b,c)
#define  ganges_Schedule_Work(a)        schedule_work(a)
#else
/*
typedef  struct tq_struct           GANGES_WORK_QUEUE;
#define  ganges_Init_Work_Queue(a,b,c)  INIT_TQUEUE(a,b,c)
#define  ganges_Schedule_Work(a)        schedule_task(a)*/
#endif
#define GANGES_THREAD_NAME_LEN 15
#define GANGES_ASSERT(exp)                                                  \
    do {                                                                    \
        if (!(exp)) {                                                       \
            RSI_DEBUG(RSI_ZONE_ERROR,"Assertion Failed! %s, %s(), %s:%d\n", \
                    #exp, __FUNCTION__, __FILE__, __LINE__);                \
        }                                                                   \
    } while (0)

typedef struct _GANGES_EVENT {  
    UINT32             eventCondition;         
    wait_queue_head_t  eventQueue;            
}GANGES_EVENT, *PGANGES_EVENT;

typedef struct semaphore GANGES_SEMAPHORE, *PGANGES_SEMAPHORE;
typedef void (*Thread_Func)(void *);

typedef struct _GANGES_THREAD_HANDLE {
    UINT8               Name[GANGES_THREAD_NAME_LEN+1];   /* Thread Name */
    INT32               ThreadId;       /* OS Specific ThreadId */
    INT32               KillThread;     /* Flag to indicate Thread Kill */
    Thread_Func         pFunction;      /* Thread Function */
    PVOID               pContext;       /* Argument to thread function */
    struct completion   Completion;     /* Thread Compeletion event */
    GANGES_WORK_QUEUE   TaskQueue;
    GANGES_SEMAPHORE    syncThread;
}GANGES_THREAD_HANDLE, *PGANGES_THREAD_HANDLE;

static __inline INT32 ganges_Init_Event (GANGES_EVENT *pEvent)
{   
    clear_bit(0,(unsigned long *)&pEvent->eventCondition);
    init_waitqueue_head(&pEvent->eventQueue);
    return 0;
}
    
static __inline INT32 ganges_Wakeup_Event(GANGES_EVENT *pEvent)
{
    if(!test_and_set_bit(0,(unsigned long *)&pEvent->eventCondition)) {
        wake_up(&pEvent->eventQueue);
    }
    return 0;
}

static __inline INT32 ganges_Reset_Event(GANGES_EVENT *pEvent)
{
    clear_bit(0,(unsigned long *)&pEvent->eventCondition);
    return 0;
}

/*Function prototypes*/
struct net_device* ganges_netdevice_op(VOID);
VOID   ganges_unregisterdev(struct net_device *dev);
struct sk_buff *ganges_alloc_skb(UINT32);
VOID   ganges_kfree_skb(struct sk_buff *skb);
int    ganges_ioctl(struct net_device *dev, struct ifreq *ifr, INT32 cmd);
INT32  ganges_xmit(struct sk_buff *, struct net_device *);
VOID   ganges_kfree_skb(struct sk_buff *skb);
struct net_device* ganges_allocdev(INT32 sizeof_priv);
INT32  ganges_registerdev( struct net_device *dev );
struct net_device_stats *ganges_get_stats( struct net_device *dev);
struct iw_statistics *ganges_get_wireless_stats(struct net_device *dev);
INT32  ganges_open(struct net_device *dev);
INT32  ganges_stop(struct net_device *dev);
VOID   ganges_indicate_connect_status(PRSI_ADAPTER Adapter);
VOID   ganges_indicate_scan_complete(PRSI_ADAPTER Adapter);
VOID   ganges_remove_proc_entry(VOID);
INT32  ganges_init_proc(PRSI_ADAPTER Adapter);
PVOID  ganges_load_file(const INT8 *fn, UINT32 *length, UINT32 type);
INT32  ganges_proc_version_read(INT8 *page,INT8 **start,off_t off,INT32 count,INT32 *eof,PVOID data);
INT32  ganges_proc_stats_read(INT8 *page,INT8 **start,off_t off,INT32 count,INT32 *eof,PVOID data);
INT32  ganges_proc_stats_read(INT8 *page,INT8 **start,off_t off,INT32 count,INT32 *eof,PVOID data);
INT32  ganges_proc_debug_zone_read(INT8 *page,INT8 **start,off_t off,INT32 count,INT32 *eof,PVOID data); 
INT32  ganges_proc_debug_zone_write(struct file *filp,const INT8 *buff,UINT32 len,VOID *data);
INT32  ganges_indicate_packet(PRSI_ADAPTER Adapter, UINT8 *DataRcvPacket,UINT32 pktLen);
UINT32 ganges_get_qId(PRSI_ADAPTER   Adapter,struct sk_buff *skb);
VOID   ganges_transmit_thread(VOID *pContext);
VOID   ganges_dump(INT32 zone, void *data, INT32   len);
INT32  ganges_read_reg_paramters(PRSI_ADAPTER Adapter);
INT32  ganges_Wait_Event(GANGES_EVENT *pEvent,UINT32 timeOut);
INT32  ganges_Start_Thread(PGANGES_THREAD_HANDLE pHandle);
INT32  ganges_Kill_Thread(PGANGES_THREAD_HANDLE pHandle);
INT32  ganges_Init_Thread(PGANGES_THREAD_HANDLE pHandle,UINT8 *pName,UINT32 Priority,Thread_Func pFunction,
                          PVOID pContext);
GANGES_STATUS ganges_read_cardinfo(PRSI_ADAPTER Adapter);
VOID   ganges_indicate_mic_failure(PRSI_ADAPTER Adapter,INT8 flag);

#endif
