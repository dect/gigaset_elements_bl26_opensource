
/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Ganges
Module name  : LINUX-SDIO driver
File Name    : ganges_linux_parser.c

File Description:

List of functions:
	open_file
	get_a_char
	close_file
	extract_token
	extract_variable

Author :

Rev History:
Ver  By             date       Description
---------------------------------------------------------
---------------------------------------------------------
*END*************************************************************************/

#include "ganges_linux.h"

GANGES_STATIC mm_segment_t fs;
GANGES_STATIC loff_t pos=0;
GANGES_EXTERN UINT8 firmware_path[256];

/*FUNCTION**********************************************************************
Function Name  : open_file
Description    :    
Returned Value : 
Paramaters     : 
----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
None 
*******************************************************************************/

struct file* 
open_file
  (
    const INT8 *fn
  )
{
  struct file* filp;
  UINT8  file_to_open[282]={0};

  fs=get_fs();
  set_fs(get_ds());

  pos = 0;
  ganges_strcpy(file_to_open,firmware_path);
  ganges_strcat(file_to_open,fn);
  filp = filp_open(file_to_open,0,0); 
  if (IS_ERR(filp))
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"open_file: Unable to load '%s'.\n", fn);
    return 0;
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_PARSER,"open_file: File opened successfully\n");
    return filp;
  } 
}

/*FUNCTION**********************************************************************
Function Name  : get_a_char 
Description    :    
Returned Value : 
Paramaters     : 
----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
None 
*******************************************************************************/

INT8 
get_a_char
  (
    struct file *filp
  )
{
  UINT32 temp;
  UINT8 var;
  temp = vfs_read(filp,&var,1,&pos);
  if(temp!=1)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"get_a_char: Failed to read data\n");
    return -1;
  }
  else
  {
    return var;
  }
}
  
/*FUNCTION**********************************************************************
Function Name  : close_file
Description    :    
Returned Value : 
Paramaters     : 
----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
None 
*******************************************************************************/

VOID 
close_file
  (
    struct file *filp
  )
{
  filp_close(filp,current->files);
  set_fs(fs);
}
  
/* This function extract a token from the specified file 
 * Store the extracted token in the argument token
 * It ignores the white space separator characters
 * It ignores the tokens inside comments and #if 0 block
 */
/*FUNCTION**********************************************************************
Function Name  : extract_token
Description    :    
Returned Value : 
Paramaters     : 
----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
None 
*******************************************************************************/

UINT8 
extract_token
  (
    struct file *fp, 
    UINT8 * token
  )
{
  INT8 ch;
  INT32 i = 0;
  INT32 star_flag = 0;

  while((ch = get_a_char(fp)) != -1)
  {
    switch(ch)
    { 
      /* comments neglected */
      case '/': 
        ch = get_a_char(fp);
        if(ch == -1)
        {
          return ch;
        }
        /* Single line comment so neglect that line*/
        else if(ch == '/')
        {
          while(ch != '\n')
          {
            ch = get_a_char(fp);
            if(ch == -1)
            {
              /* May be a comment on the last line
               * This case may not come 
               * as vim insert new line at the end of each file
               */
              RSI_DEBUG(RSI_ZONE_ERROR,
                        "In // comment end of line not found\n");
              return ch;
            }
          }   
        }
        /* Block comment start */
        else if(ch == '*')
        {
          /* Detecting the end of the block comment */
          while((ch = get_a_char(fp)) != -1)
          {
            if(ch == '*')
            {
              star_flag = 1;
            }
            else if(ch == '/')
            {
              if(star_flag == 1)
              {    
                break; /* End of block comment */
              }
            } 
            else
            {
              star_flag = 0;
            }
          }
          /* If end of block comment found ch contain '/'
           * If not found then ch contain EOF */
          if(ch == -1)
          {
            RSI_DEBUG(RSI_ZONE_ERROR, 
                      "End of the block comment not found\n");
            return ch;
          }       
        }
        break;
      case '#': 
        token[i++] = ch;
        ch = get_a_char(fp);
        if(ch == -1)
        {
          /* This case may not come any where 
           * and not a valid c syntax
           * as vim insert new line at the end of file */
          RSI_DEBUG(RSI_ZONE_ERROR, 
                    "# is the last character of file\n");
          return ch;
        }    
        while((ch == ' ') || (ch == '\t'))
        {
          ch = get_a_char(fp);
          if(ch == -1)
          {
            /* This is also not a valid c syntax */
            return ch;
          }
        }
        token[i++] = ch;
        break;
        /* To handle cases when there is no separator between { and the value
         * e.g. int arr[]={0x00ff,... ...};*/ 
      case '{': 
        if(i == 0)
        {
          token[i] = ch;
          token[++i] = '\0';
          return ch;
        }
        else
        {
          /* This case may not come any where ( '{' inside a token ) */
          token[i++] = ch;
        }
        break;
        /* To handle cases when there is no separator between } and the variable
         * e.g. int arr[]={...., .. ,0x00ff};*/ 
      case '}': 
        if(i != 0)
        {
          token[i] = '\0';
          /* Returning a invalid char to detect the end of block */
          return (UINT8)-99;
        }
        else /* i == 0 */
        {
          token[i++] = ch;
          token[i] = '\0';
          return ch;
        }     
        /* no need of break as both block r returning */
        /* Separator characters neglected */
      case ',':
      case '\n':
      case '\r':
      case '\t':
      case '[':
      case ']':
      case '=':  
      case ';':  
      case ' ': 
        if (i != 0)
        {
          token[i] = '\0';
          return ch;
        } 
        break;
        /*Default characters are valid characters */
      default: 
        token[i++] = ch;
        break;
    }
  }
  /* Return the last read char to detect the EOF in the calling function */
  token[i] = '\0'; /* if EOF comes '\0' may not be inserted */
  /* There is maximum probability that the file contain a new line before EOF
   * If not the last last token can not be extracted */
  return ch;
}

/*FUNCTION**********************************************************************
Function Name  : extract_variable 
Description    :    
Returned Value : 
Paramaters     : 
----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
None 
*******************************************************************************/

UINT16 *
extract_variable
  (
    UINT8  *arr_name,
    UINT16 channel_prog_flag
  )
{
  UINT16 *arr=NULL;
  INT8 retval;
  UINT8 token[100];
  UINT16 arr_len, loopvar;
  UINT16 exit_flag = 0; /* For handling 2D array */
  struct file *fp;
  
  if(channel_prog_flag)
  { 
    fp = open_file("ganges_channel_prog_val.h");
  }
  else
  {
    fp = open_file("litefi_config.h");
  }
  
  /* Checking whether the specified file exhisht or not */
  if(fp == NULL)
  {
    RSI_DEBUG(RSI_ZONE_ERROR, 
              "Specified file does not exist in current dir\n");
    return (UINT16 *)NULL;
  }
  retval = extract_token(fp, token);
  //RSI_DEBUG(RSI_ZONE_INFO,"extr_var:Token extracted %s\n",token); 
  /* Checking whether the file contain any token or not */
  if(retval == -1)
  {
    RSI_DEBUG(RSI_ZONE_ERROR, 
               (TEXT("File is blank or contain no valid token\n")));
    close_file(fp);
    return (UINT16 *)NULL;
  }

  /* Searching the array name in the file */
  while (ganges_equal_string(token, arr_name) != 0)/* Userlevel */
  {
    
    retval = extract_token(fp, token); 
    if(retval == -1)
    {
      RSI_DEBUG(RSI_ZONE_ERROR, 
                 (TEXT("Specifed Array name not found in the file\n")));
      close_file(fp);
      return (UINT16 *)NULL;
    }
  }
  RSI_DEBUG(RSI_ZONE_PARSER,"Array name found\n");
  RSI_DEBUG(RSI_ZONE_PARSER,"extr_var: Array Name:%s\n",token); 
    
  
  /* Now token contain the array name */
  
  /* Searching for the starting of the array */  
  while (ganges_equal_string(token, "{") != 0)/* Userlevel */
  {
    retval = extract_token(fp, token);
    if(retval == -1)
    {
      RSI_DEBUG(RSI_ZONE_ERROR, 
                 (TEXT("EOF reached starting of array '{' not found\n")));
      close_file(fp);
      return (UINT16 *)NULL;
    }
    if((ganges_equal_string(token,"}") == 0) || (retval == (char) -99))
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
                "End of array '}' reached not found '{'\n");
    }
    
  }

  //RSI_DEBUG(RSI_ZONE_ERROR,"Starting of array found \n");
  //RSI_DEBUG(RSI_ZONE_INFO,"extr_var: Starting of array:%s\n",token); 
  
  /* Now array block is already started */
  
  /* Extracting the data up to the end of the array */
  while((ganges_equal_string("}",token) != 0) && (exit_flag != 1))
  {
    //RSI_DEBUG(RSI_ZONE_ERROR, (TEXT("Inside the while block \n")));
    retval = extract_token(fp, token);
  
    if(retval == -1)
    {
      RSI_DEBUG(RSI_ZONE_ERROR, 
                 (TEXT("EOF reached end of array '}' not found")));
      close_file(fp);
      return (UINT16 *)NULL;
    }
   
    /* Removing the #if 0 block */   
    if(ganges_equal_string("#if", token) == 0)/* Userlevel */
    {
      RSI_DEBUG(RSI_ZONE_PARSER,"extr_var: Inside the #if:%s\n",token); 
      retval = extract_token(fp, token);
      if (retval == -1)
      {
        RSI_DEBUG(RSI_ZONE_ERROR, (TEXT("No token present after #if\n")));
        close_file(fp);
        return (UINT16 *)NULL;
      }
      if(ganges_equal_string(token,"0") == 0)
      {
        RSI_DEBUG(RSI_ZONE_PARSER, (TEXT("Inside the #if 0 block \n")));
        /* Now we r inside if 0 block, neglecting all token untill #endif */
        while(ganges_equal_string(token,"#endif") != 0)
        {
          retval = extract_token(fp, token);
          if(retval == -1)
          {
            RSI_DEBUG(RSI_ZONE_PARSER, 
                       (TEXT("EOF reached in the #if #endif block\n")));
            RSI_DEBUG(RSI_ZONE_ERROR, (TEXT("#endif block not found\n")));
            close_file(fp);
            return (UINT16 *)NULL;
          }
        }
      }
    }
    /* Starting of multi-dimensional array */
    else if(ganges_equal_string(token,"{") == 0) 
    {
      RSI_DEBUG(RSI_ZONE_PARSER, (TEXT("Starting of the 2D array\n")));
      exit_flag -= 1; /* Can not exit */    
    }
    /* The following else if is not reqd 
     * handle blank array cases
     * eg {{},{0xff.... which is not valid */
    else if(ganges_equal_string(token,"}") == 0) /* End of one field of 2D array */
    {
      exit_flag += 1; 
    }      
    /* Extracting the elements of the array */
    else
    {            
      UINT16 temp;
      if((token[0] == '0') && ((token[1] == 'x') || (token[1] == 'X')))
      {
        arr_len = (UINT16)ganges_simple_strtol(&token[2],NULL, 16);
      }
      else
      {
        arr_len = (UINT16)ganges_simple_strtol(token, NULL, 10);
      }    
      /* ddarr = (int *)malloc(sizeof(int) * arr_len); */
      temp = (arr_len + 1) * sizeof(UINT16);

      RSI_DEBUG(RSI_ZONE_PARSER,"parser: allocating memory %d %d\n", temp,arr_len);
      arr = (UINT16 *)ganges_mem_alloc(temp,GFP_KERNEL);
      *arr = arr_len; 

      if (arr == NULL) 
      {
        RSI_DEBUG(RSI_ZONE_ERROR, (TEXT("Sufficient memory not available\n")));
        return (UINT16 *)NULL;
      }

      //RSI_DEBUG(RSI_ZONE_ERROR, (TEXT("Inside the else block\n")));
      
      /* Converting the token into long type and storing in the array */
      for(loopvar = 1; loopvar <= *arr ; loopvar++)
      {
        retval = extract_token(fp, token);
        if( retval == -1)
        {
          RSI_DEBUG(RSI_ZONE_ERROR, 
                     (TEXT("Specified num of values not present in file\n")));
          close_file(fp);
          return (UINT16 *)NULL;
        }
	/* Starting of a element block for multi-dimensional array */
        else if(ganges_equal_string(token,"{") == 0) 
        {
          loopvar--; /* As these are not considered as array element */
          exit_flag -= 1;arr_len = arr_len + sizeof(UINT16);
        }
	/* End of one block of multi-dimensional array */
        else if(ganges_equal_string(token,"}") == 0) 
        {
          loopvar--;
          exit_flag += 1;
          if(exit_flag == 1)
          {
            *arr = loopvar;  
            RSI_DEBUG(RSI_ZONE_ERROR, (TEXT("End of the array\n")));
            break; 
          }
        } 
        /* The .h file contain both hexadecimal and decimal value 
         * so checking 0x or 0X to distinguish hex and dec values */
        else
        {
          if((token[0] == '0') && ((token[1] == 'x') || (token[1] == 'X')))
          {
            *(arr + loopvar) = (UINT16)ganges_simple_strtol(&token[2], NULL, 16);
          }
          else
          {
            *(arr + loopvar) = (UINT16)ganges_simple_strtol(token, NULL, 10);
          }
        }
        /* Detecting the end of block in case of ... 0x00ff}; */
        if(retval == (INT8)-99)
        {
          /* For 1D array this is the end of the loop */
          RSI_DEBUG(RSI_ZONE_ERROR, 
                     (TEXT("Inside the if char -99 block\n")));
          token[0] = '}';
          token[1] = '\0';
          /* For 2D array end of block */
          exit_flag += 1;
          if(exit_flag == 1)
          {
            RSI_DEBUG(RSI_ZONE_ERROR, (TEXT("End of array\n")));  
            break; 
            /* Suppose we have given the length of array which is more
             * than the size of the array.
             * In such case if we dont break here our program go on reading
             * upto the length specifed.
             * it will not give any error but read garbage.
             * Whether we will break here or not ? */ 
          }
        }  
      } /* End of for loop */
      close_file(fp);
      return arr;
    }
  }
  RSI_DEBUG(RSI_ZONE_PARSER, (TEXT("End of the array \n")));
  /* Control comes here only when there is no element in the array */
  close_file(fp);
  return (UINT16 *)NULL;
}
