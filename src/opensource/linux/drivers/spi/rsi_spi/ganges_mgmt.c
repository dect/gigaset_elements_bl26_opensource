/*All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Ganges_SDIO_LINUX
Module name  : Ganges_SDIO_LINUX_HostDriver
File Name    : ganges_mgmt.c

File Description:
    This file contians the code for initializing the management module.

List of functions:

	ganges_load_channel_prog_vals
	ganges_load_BB_prog_vals
	ganges_load_RF_prog_vals
	ganges_load_RF_power_vals
	ganges_load_power_save_params
	ganges_mgmt_init
        ganges_reset_power_vals
	ganges_RF_program
	ganges_sendRFProgRequest
	ganges_send_reset_request
	ganges_send_scanrequest
	ganges_send_join_request
	ganges_send_auth_request
	ganges_send_Associate_request
	ganges_send_disassociate_request
	ganges_send_DeAuthentication_request
	ganges_send_WEPKeys_request
	ganges_send_pair_key_install
	ganges_send_sleep_params
	ganges_send_deep_sleep_request
	ganges_send_sleep_request
	ganges_send_wakeup_request
	ganges_send_TSF_resp
	ganges_send_group_key_install
	ganges_setBBGainRequest
	ganges_send_probe_request
	ganges_send_probe_resp_request
	ganges_send_measure_noise_rssi_request
	ganges_send_update_noise_rssi_request
	ganges_send_ibssds_response
	ganges_send_auto_rate_request
	ganges_send_qos_params
	ganges_send_rate_symbol_request
	ganges_start_ibss
	ganges_measure_auto_nfm_rssi
        ganges_send_reset_mac
	ganges_send_reset_cont_tx
	ganges_is_AP_supported_rate
	ganges_get_station_statistics
	ganges_send_LMAC_prog_vals
	ganges_send_LMAC_prog_val_1
	ganges_send_LMAC_prog_val_2
	ganges_send_LMAC_prog_val_3
	ganges_send_load_STA_config
	ganges_send_power_save_req
	ganges_send_program_BB_registers
	ganges_set_channel_type
 	ganges_set_protocol_type
	ganges_set_txrate
	ganges_do_adapter_Reset
	ganges_do_scan
	ganges_set_ssid
	ganges_set_bssid
	ganges_can_connect
	ganges_free_bssid_list
	ganges_read_power_vals
	ganges_eeprom_write
	ganges_eeprom_read
	ganges_find_mgmt_type
Author :

Rev History:

---------------------------------------------------------
1    Anji
2    Fariya
---------------------------------------------------------
*END**************************************************************************/

#include "ganges_linux.h"
#include "ganges_nic.h"
#include "ganges_mgmt.h"
#include "ganges_channel_prog_val.h"

#define RSI_MAX_PWR_VAL_SET_2236  2
#define DYNAMIC_VARIABLES 1
GANGES_EXTERN UINT16 gTxPower;
GANGES_EXTERN GANGES_EVENT PwrSaveEvent;

GANGES_EXTERN INT32 nf_error1[];
GANGES_EXTERN INT32 nf_error2[];

UINT16 macTimingValuesFreq_44[] = { 
     4400, 440, 1760, 792, 0, 880, 396, 9592};

UINT8 RPS_RATE_MCS7_PWR;
UINT8 RPS_RATE_MCS6_PWR;
UINT8 RPS_RATE_MCS5_PWR;
UINT8 RPS_RATE_MCS4_PWR;
UINT8 RPS_RATE_MCS3_PWR;
UINT8 RPS_RATE_MCS2_PWR;
UINT8 RPS_RATE_MCS1_PWR;
UINT8 RPS_RATE_MCS0_PWR;
UINT8 RPS_RATE_54_PWR;
UINT8 RPS_RATE_48_PWR;
UINT8 RPS_RATE_36_PWR;
UINT8 RPS_RATE_24_PWR;
UINT8 RPS_RATE_18_PWR;
UINT8 RPS_RATE_12_PWR;
UINT8 RPS_RATE_11_PWR;
UINT8 RPS_RATE_09_PWR;
UINT8 RPS_RATE_06_PWR;
UINT8 RPS_RATE_5_5_PWR;
UINT8 RPS_RATE_02_PWR;
UINT8 RPS_RATE_01_PWR;
UINT8 RPS_RATE_00_PWR;

UINT8 *fsm_states_str[] = {
"CLOSED",
"FSM_SCAN_REQ_SENT",
"FSM_JOIN_REQ_SENT",
"FSM_AUTH_REQ_SENT",
"FSM_ASSOCIATE_REQ_SENT",
"FSM_OPEN",
"FSM_NO_STATE",
"FSM_SET_WEPKEYS_SENT",
"FSM_SET_WEPGRP_KEYS_SENT",
"FSM_SET_WPA_PAIRKEYS_SENT",
"FSM_SET_WPA_GRP_KEYS_SENT",
"FSM_DEEP_SLEEP",
"FSM_SCAN_REQ_SENT_INOPEN",
"FSM_IBSS_CREATED",
"FSM_RESET1_SENT",
"FSM_RESET2_SENT",
"FSM_RF_REQ_SENT",
"FSM_CARD_NOT_READY",
"FSM_FIRMWARE_LOADED"
};

/*FUNCTION*********************************************************************
Function Name  : ganges_load_channel_prog_vals
Description    :  
Returned Value : On success 0 will be returned else a ngeative number
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |     |     |  X  | pointer to our adapter
*END**************************************************************************/

GANGES_STATIC GANGES_STATUS
ganges_load_channel_prog_vals
  (
    IN PRSI_ADAPTER Adapter
  )
{
  GANGES_STATUS Status = GANGES_STATUS_SUCCESS;
  UINT8  i;

  do
  {
    if((Adapter->RFType == RSI_RF_AL2230S ) ||
       (Adapter->RFType == RSI_RF_AL2230 )  ||
       (Adapter->RFType == RSI_RF_AL2236 ))
    {

#ifdef DYNAMIC_VARIABLES
      UINT16 *rsi_channel_vals_B_airoha=NULL;

      /** For 40 Mhz Crustal Clock**/
      switch(Adapter->config_params.crystalFrequency)
      {
        case 0x9:
        {
          RSI_DEBUG(RSI_ZONE_INIT,
                   "load_channel_prog_vals: Extracting Channel values for 40 Mhz Crystal Clock\n");
          rsi_channel_vals_B_airoha = extract_variable("rsi_channel_vals_B_airoha_40mhz",1);          
        }
        break;

        case 0x8:
        {
          RSI_DEBUG(RSI_ZONE_INIT,
                   "load_channel_prog_vals: Extracting Channel values for 20 Mhz Crystal Clock\n");
          rsi_channel_vals_B_airoha = extract_variable("rsi_channel_vals_B_airoha_20mhz",1);          
                
        }
        break;

        case 0x7:
        {
          RSI_DEBUG(RSI_ZONE_INIT,
                   "load_channel_prog_vals: Extracting Channel values for 44 Mhz Crystal Clock\n");
          rsi_channel_vals_B_airoha = extract_variable("rsi_channel_vals_B_airoha_44mhz",1);          
        }
        break;

        case 0x6:
        {
	  /*	
          RSI_DEBUG(RSI_ZONE_INIT,
                   "load_channel_prog_vals: Extracting Channel values for 52 Mhz Crystal Clock\n");
          rsi_channel_vals_B_airoha = extract_variable("rsi_channel_vals_B_airoha_52mhz",1);                    
	  */
          RSI_DEBUG(RSI_ZONE_ERROR,
                   (TEXT("ERROR: 52 Mhz Crystal Clock Not Supported\n")));
	  return GANGES_STATUS_FAILURE;
		
         }
         break;

         case 0x5:
         {
           RSI_DEBUG(RSI_ZONE_INIT,
                    "load_channel_prog_vals: Extracting RF values for 26 Mhz Crystal Clock\n");
           rsi_channel_vals_B_airoha = extract_variable("rsi_channel_vals_B_airoha_26mhz",1);                                
         }
         break;
         case 0x4:
         {
           RSI_DEBUG(RSI_ZONE_INIT,
                    "load_channel_prog_vals: Extracting channel values for 13 Mhz Crystal Clock\n");
           rsi_channel_vals_B_airoha = extract_variable("rsi_channel_vals_B_airoha_13mhz",1);      
         }       
         break;

         case 0x2:
         {
	   /*	 
           RSI_DEBUG(RSI_ZONE_INIT,
                    "load_channel_prog_vals: Extracting channel values for 38.4 Mhz Crystal Clock\n");
           rsi_channel_vals_B_airoha = extract_variable("rsi_channel_vals_B_airoha_38p4mhz",1);                 
	   */
           RSI_DEBUG(RSI_ZONE_ERROR,
                    (TEXT("ERROR: 38.4 Mhz Crystal Clock Not Supported\n")));
           return GANGES_STATUS_FAILURE;
		 
         }
         break;
         case 0x1:
         {
 	   	 
           RSI_DEBUG(RSI_ZONE_INIT,
                    "load_channel_prog_vals: Extracting RF values for 19.2 Mhz Crystal Clock\n");
           rsi_channel_vals_B_airoha = extract_variable("rsi_channel_vals_B_airoha_19p2mhz",1);                            
		 
         }
         break;

         case 0x0:
         {
           /*		 
           RSI_DEBUG(RSI_ZONE_INIT,
                    "load_channel_prog_vals: Extracting RF values for 9.6 Mhz Crystal Clock\n");
           rsi_channel_vals_B_airoha = extract_variable("rsi_channel_vals_B_airoha_19p2mhz",1);                                         */
           RSI_DEBUG(RSI_ZONE_ERROR,
                    "ERROR: 9.6 Mhz Crystal Clock Not Supported\n");
           return GANGES_STATUS_FAILURE;
	 
         }
         break;

         default:
         {
           RSI_DEBUG(RSI_ZONE_ERROR,
                    "load_channel_prog_vals: Error Unknown Crystal Clock Found\n");
           Status = GANGES_STATUS_FAILURE;
           break;
         }
      }
 

      /* Extract channel programing values */
      if(rsi_channel_vals_B_airoha == NULL)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "ganges_load_channel_prog_vals: Unable to extract rsi_channel_vals_B_airoha\n");
        Status = GANGES_STATUS_FAILURE;
        break;
      }
      RSI_DEBUG(RSI_ZONE_INFO,
                "ganges_load_channel_prog_vals: rsi_channel_vals_B_airoha\n");
          
      ganges_dump(RSI_ZONE_INIT, 
                  rsi_channel_vals_B_airoha, 
                  ((rsi_channel_vals_B_airoha[0] + 1) * sizeof(UINT16)));

      if(rsi_channel_vals_B_airoha[0] != 66)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
		  "ganges_load_channel_prog_vals: Wrong number of channel values\n");
        Status = GANGES_STATUS_FAILURE;
        break;
      }
      for(i=0; i<12; i++)
      {
        Adapter->rsi_channel_vals[i+1].val1 = *(rsi_channel_vals_B_airoha + 1 + (i * 6));
        Adapter->rsi_channel_vals[i+1].val2 = *(rsi_channel_vals_B_airoha + 2 + (i * 6));
        Adapter->rsi_channel_vals[i+1].val3 = *(rsi_channel_vals_B_airoha + 3 + (i * 6));
        Adapter->rsi_channel_vals[i+1].val4 = *(rsi_channel_vals_B_airoha + 4 + (i * 6));
        Adapter->rsi_channel_vals[i+1].val5 = *(rsi_channel_vals_B_airoha + 5 + (i * 6));
        Adapter->rsi_channel_vals[i+1].val6 = *(rsi_channel_vals_B_airoha + 6 + (i * 6));
      }
      /* free the memory allocated for dynamic variables */
      RSI_DEBUG(RSI_ZONE_INFO,
                "ganges_load_channel_prog_vals: Freeing dynamic variables memory\n");
      ganges_mem_free(rsi_channel_vals_B_airoha);

#endif
    }
    else if(Adapter->RFType == RSI_RF_MAXIM_2831)
    {
#ifdef DYNAMIC_VARIABLES
      UINT16 *rsi_channel_vals_B_maxim;

      /* Extract channel programing values */
      rsi_channel_vals_B_maxim = extract_variable("rsi_channel_vals_B_maxim",1);
      if(rsi_channel_vals_B_maxim == NULL)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "ganges_load_channel_prog_vals: Unable to extract rsi_channel_vals_B_maxim\n");
        Status = GANGES_STATUS_FAILURE;
        break;
      }
      RSI_DEBUG(RSI_ZONE_INFO,
                "ganges_load_channel_prog_vals: rsi_channel_vals_B_maxim\n");
        
      ganges_dump(RSI_ZONE_INIT, 
                  rsi_channel_vals_B_maxim, 
                  ((rsi_channel_vals_B_maxim[0] + 1) * sizeof(UINT16)));

      if(rsi_channel_vals_B_maxim[0] != 66)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,"ganges_load_channel_prog_vals: Wrong number of channel values\n");
        Status = GANGES_STATUS_FAILURE;
        break;
      }
      for(i=0; i<12; i++)
      {
        Adapter->rsi_channel_vals[i+1].val1 = *(rsi_channel_vals_B_maxim + 1 + (i * 6));
        Adapter->rsi_channel_vals[i+1].val2 = *(rsi_channel_vals_B_maxim + 2 + (i * 6));
        Adapter->rsi_channel_vals[i+1].val3 = *(rsi_channel_vals_B_maxim + 3 + (i * 6));
        Adapter->rsi_channel_vals[i+1].val4 = *(rsi_channel_vals_B_maxim + 4 + (i * 6));
        Adapter->rsi_channel_vals[i+1].val5 = *(rsi_channel_vals_B_maxim + 5 + (i * 6));
        Adapter->rsi_channel_vals[i+1].val6 = *(rsi_channel_vals_B_maxim + 6 + (i * 6));
      }
      /* free the memory allocated for dynamic variables */
      RSI_DEBUG(RSI_ZONE_INFO,
                "ganges_load_channel_prog_vals: Freeing dynamic variables memory\n");
      ganges_mem_free(rsi_channel_vals_B_maxim);

#else

      if(rsi_channel_vals_B_maxim[0][0] != 66)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,"ganges_load_channel_prog_vals: Wrong number of channel values %d\n",
                                  rsi_channel_vals_B_maxim[0][0]);
        Status = GANGES_STATUS_FAILURE;
        break;
      }
      for(i=1; i<12; i++)
      {
        Adapter->rsi_channel_vals[i].val1 = rsi_channel_vals_B_maxim[i][0];
        Adapter->rsi_channel_vals[i].val2 = rsi_channel_vals_B_maxim[i][1];
        Adapter->rsi_channel_vals[i].val3 = rsi_channel_vals_B_maxim[i][2];
        Adapter->rsi_channel_vals[i].val4 = rsi_channel_vals_B_maxim[i][3];
        Adapter->rsi_channel_vals[i].val5 = rsi_channel_vals_B_maxim[i][4];
        Adapter->rsi_channel_vals[i].val6 = rsi_channel_vals_B_maxim[i][5];
      }

#endif
    }
  }while(FALSE);

  return Status;
}


/*FUNCTION*********************************************************************
Function Name  : ganges_load_BB_prog_vals
Description    :  
Returned Value : On success 0 will be returned else a ngeative number
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |     |     |  X  | pointer to our adapter
*END**************************************************************************/

GANGES_STATIC GANGES_STATUS
ganges_load_BB_prog_vals
  ( 
    PRSI_ADAPTER Adapter
  )
{
  GANGES_STATUS Status = GANGES_STATUS_SUCCESS;
  UINT8  i;

  do
  {
    if(Adapter->AFE_type == RSI_AFE_1)
    {
      if((Adapter->RFType == RSI_RF_AL2230S ) ||
         (Adapter->RFType == RSI_RF_AL2230 ))
      {
#ifdef DYNAMIC_VARIABLES
        UINT16 *BBprogramValues_B_airoha_afe1;

        /* Extract BB values from file */
        BBprogramValues_B_airoha_afe1 = extract_variable("BBprogramValues_B_airoha_afe1",1);
        if(BBprogramValues_B_airoha_afe1 == NULL)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                    "ganges_load_BB_prog_vals: Unable to extract BBprogramValues_B_airoha_afe1\n");
          Status = GANGES_STATUS_FAILURE;
          break;
        }
        RSI_DEBUG(RSI_ZONE_INFO,
                  "ganges_load_BB_prog_vals: BBprogramValues_B_airoha_afe1\n");
        ganges_dump(RSI_ZONE_INIT, 
                    BBprogramValues_B_airoha_afe1, 
                    ((BBprogramValues_B_airoha_afe1[0] + 1) * sizeof(UINT16)));

#endif

        if(BBprogramValues_B_airoha_afe1[0] > BB_PROGRAM_VAL_MAX)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,"ganges_load_BB_prog_vals: Too many BB values %d\n",
                                  BBprogramValues_B_airoha_afe1[0]);
          Status = GANGES_STATUS_FAILURE;
          break;
        }

        /* make sure max size is less than BB_PROGRAM_VAL_MAX */
        for(i=0; i<=BBprogramValues_B_airoha_afe1[0]; i++) 
        {
          Adapter->BBProgramVal[i] = BBprogramValues_B_airoha_afe1[i] ; 
        }

#ifdef DYNAMIC_VARIABLES
        ganges_mem_free(BBprogramValues_B_airoha_afe1);
#endif

      }
      else if(Adapter->RFType == RSI_RF_AL2236)
      {
#ifdef DYNAMIC_VARIABLES
        UINT16 *BBprogramValues_B_airoha_afe1_2236_set1;
        UINT16 *BBprogramValues_B_airoha_afe1_2236_set2;
        UINT16 *BBprogramValues_B_airoha_afe1_2236_set3;

        /* Extract BB values from file */
        BBprogramValues_B_airoha_afe1_2236_set1 = extract_variable("BBprogramValues_B_airoha_afe1_2236_set1",1);
        if(BBprogramValues_B_airoha_afe1_2236_set1 == NULL)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                    "ganges_load_BB_prog_vals: Unable to extract BBprogramValues_B_airoha_afe1_2236_set1\n");
          Status = GANGES_STATUS_FAILURE;
          break;
        }
        RSI_DEBUG(RSI_ZONE_INFO,
                  "ganges_load_BB_prog_vals: BBprogramValues_B_airoha_afe1_2236_set1\n");
        ganges_dump(RSI_ZONE_INIT, 
                    BBprogramValues_B_airoha_afe1_2236_set1, 
                    ((BBprogramValues_B_airoha_afe1_2236_set1[0] + 1) * sizeof(UINT16)));

        BBprogramValues_B_airoha_afe1_2236_set2 = extract_variable("BBprogramValues_B_airoha_afe1_2236_set2",1);
        if(BBprogramValues_B_airoha_afe1_2236_set2 == NULL)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                    "ganges_load_BB_prog_vals: Unable to extract BBprogramValues_B_airoha_afe1_2236_set2\n");
          ganges_mem_free(BBprogramValues_B_airoha_afe1_2236_set1);
          Status = GANGES_STATUS_FAILURE;
          break;
        }
        RSI_DEBUG(RSI_ZONE_INFO,
                  "ganges_load_BB_prog_vals: BBprogramValues_B_airoha_afe1_2236_set2\n");
        ganges_dump(RSI_ZONE_INIT, 
                    BBprogramValues_B_airoha_afe1_2236_set2, 
                    ((BBprogramValues_B_airoha_afe1_2236_set2[0] + 1) * sizeof(UINT16)));

        BBprogramValues_B_airoha_afe1_2236_set3 = extract_variable("BBprogramValues_B_airoha_afe1_2236_set3",1);
        if(BBprogramValues_B_airoha_afe1_2236_set3 == NULL)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                    "ganges_load_BB_prog_vals: Unable to extract BBprogramValues_B_airoha_afe1_2236_set3\n");
          ganges_mem_free(BBprogramValues_B_airoha_afe1_2236_set1);
          ganges_mem_free(BBprogramValues_B_airoha_afe1_2236_set2);
          Status = GANGES_STATUS_FAILURE;
          break;
        }
        RSI_DEBUG(RSI_ZONE_INFO,
                  "ganges_load_BB_prog_vals: BBprogramValues_B_airoha_afe1_2236_set3\n");
        ganges_dump(RSI_ZONE_INIT, 
                    BBprogramValues_B_airoha_afe1_2236_set3, 
                    ((BBprogramValues_B_airoha_afe1_2236_set3[0] + 1) * sizeof(UINT16)));

#endif

        if(BBprogramValues_B_airoha_afe1_2236_set1[0] > BB_PROGRAM_VAL_MAX)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,"ganges_load_BB_prog_vals: Too many BB values %d\n",
                                  BBprogramValues_B_airoha_afe1_2236_set1[0]);
          Status = GANGES_STATUS_FAILURE;
          break;
        }

        /* make sure max size is less than BB_PROGRAM_VAL_MAX */
        for(i=0; i<=BBprogramValues_B_airoha_afe1_2236_set1[0]; i++) 
        {
          Adapter->BBProgramVal[i] = BBprogramValues_B_airoha_afe1_2236_set1[i] ; 
        }
        for(i=0; i<=BBprogramValues_B_airoha_afe1_2236_set2[0]; i++) 
        {
          Adapter->BBProgramVal_set2[i] = BBprogramValues_B_airoha_afe1_2236_set2[i] ; 
        }
        for(i=0; i<=BBprogramValues_B_airoha_afe1_2236_set3[0]; i++) 
        {
          Adapter->BBProgramVal_set3[i] = BBprogramValues_B_airoha_afe1_2236_set3[i] ; 
        }

#ifdef DYNAMIC_VARIABLES
        ganges_mem_free(BBprogramValues_B_airoha_afe1_2236_set1);
        ganges_mem_free(BBprogramValues_B_airoha_afe1_2236_set2);
        ganges_mem_free(BBprogramValues_B_airoha_afe1_2236_set3);
#endif

      }
      else if(Adapter->RFType == RSI_RF_MAXIM_2831)
      {
#ifdef DYNAMIC_VARIABLES
        UINT16 *BBprogramValues_maxim_afe1;

        /* Extract BB values from file */
        BBprogramValues_maxim_afe1 = extract_variable("BBprogramValues_maxim_afe1",1);
        if(BBprogramValues_maxim_afe1 == NULL)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                    "ganges_load_BB_prog_vals: Unable to extract BBprogramValues_maxim_afe1\n");
          Status = GANGES_STATUS_FAILURE;
          break;
        }
        RSI_DEBUG(RSI_ZONE_INFO,
                  "ganges_load_BB_prog_vals: BBprogramValues_maxim_afe1\n");
        ganges_dump(RSI_ZONE_INIT, 
                    BBprogramValues_maxim_afe1, 
                    ((BBprogramValues_maxim_afe1[0] + 1) * sizeof(UINT16)));

#endif

        if(BBprogramValues_maxim_afe1[0] > BB_PROGRAM_VAL_MAX)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,"ganges_load_BB_prog_vals: Too many BB values %d\n",
                                  BBprogramValues_maxim_afe1[0]);
          Status = GANGES_STATUS_FAILURE;
          break;
        }

        /* make sure max size is less than BB_PROGRAM_VAL_MAX */
        for(i=0; i<=BBprogramValues_maxim_afe1[0]; i++) 
        {
          Adapter->BBProgramVal[i] = BBprogramValues_maxim_afe1[i] ; 
        }

#ifdef DYNAMIC_VARIABLES
        ganges_mem_free(BBprogramValues_maxim_afe1);
#endif
      }
      else
      {
        Status = GANGES_STATUS_FAILURE;
        break;
      }
    }
    else if(Adapter->AFE_type == RSI_AFE_2)
    {
      if((Adapter->RFType == RSI_RF_AL2230S ) ||
         (Adapter->RFType == RSI_RF_AL2230 ))
      {
#ifdef DYNAMIC_VARIABLES
        UINT16 *BBprogramValues_B_airoha_afe2;

        /* Extract BB values from file */
        BBprogramValues_B_airoha_afe2 = extract_variable("BBprogramValues_B_airoha_afe2",1);
        if(BBprogramValues_B_airoha_afe2 == NULL)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                    "ganges_load_BB_prog_vals: Unable to extract BBprogramValues_B_airoha_afe2\n");
          Status = GANGES_STATUS_FAILURE;
          break;
        }
        RSI_DEBUG(RSI_ZONE_INFO,
                  "ganges_load_BB_prog_vals: BBprogramValues_B_airoha_afe2\n");
        ganges_dump(RSI_ZONE_INIT, 
                    BBprogramValues_B_airoha_afe2, 
                    ((BBprogramValues_B_airoha_afe2[0] + 1) * sizeof(UINT16)));

#endif
        if(BBprogramValues_B_airoha_afe2[0] > BB_PROGRAM_VAL_MAX)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,"ganges_load_BB_prog_vals: Too many BB values %d\n",
                                  BBprogramValues_B_airoha_afe2[0]);
          Status = GANGES_STATUS_FAILURE;
          break;
        }

        /* make sure max size is less than BB_PROGRAM_VAL_MAX */
        for(i=0; i<=BBprogramValues_B_airoha_afe2[0]; i++) 
        {
          Adapter->BBProgramVal[i] = BBprogramValues_B_airoha_afe2[i] ; 
        }

#ifdef DYNAMIC_VARIABLES
        ganges_mem_free(BBprogramValues_B_airoha_afe2);
#endif

      }
      else if(Adapter->RFType == RSI_RF_AL2236)
      {
#ifdef DYNAMIC_VARIABLES
        UINT16 *BBprogramValues_B_airoha_afe2_2236_set1;
        UINT16 *BBprogramValues_B_airoha_afe2_2236_set2;
        UINT16 *BBprogramValues_B_airoha_afe2_2236_set3;

        /* Extract BB values from file */
        BBprogramValues_B_airoha_afe2_2236_set1 = extract_variable("BBprogramValues_B_airoha_afe2_2236_set1",1);
        if(BBprogramValues_B_airoha_afe2_2236_set1 == NULL)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                    "ganges_load_BB_prog_vals: Unable to extract BBprogramValues_B_airoha_afe2_2236_set1\n");
          Status = GANGES_STATUS_FAILURE;
          break;
        }
        RSI_DEBUG(RSI_ZONE_INFO,
                  "ganges_load_BB_prog_vals: BBprogramValues_B_airoha_afe2_2236_set1\n");
        ganges_dump(RSI_ZONE_INIT, 
                    BBprogramValues_B_airoha_afe2_2236_set1, 
                    ((BBprogramValues_B_airoha_afe2_2236_set1[0] + 1) * sizeof(UINT16)));

        /* Extract Set 2 values */
        BBprogramValues_B_airoha_afe2_2236_set2 = extract_variable("BBprogramValues_B_airoha_afe2_2236_set2",1);
        if(BBprogramValues_B_airoha_afe2_2236_set2 == NULL)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                    "ganges_load_BB_prog_vals: Unable to extract BBprogramValues_B_airoha_afe2_2236_set2\n");
          ganges_mem_free(BBprogramValues_B_airoha_afe2_2236_set1);
          Status = GANGES_STATUS_FAILURE;
          break;
        }
        RSI_DEBUG(RSI_ZONE_INFO,
                  "ganges_load_BB_prog_vals: BBprogramValues_B_airoha_afe2_2236_set2\n");
        ganges_dump(RSI_ZONE_INIT, 
                    BBprogramValues_B_airoha_afe2_2236_set2, 
                    ((BBprogramValues_B_airoha_afe2_2236_set2[0] + 1) * sizeof(UINT16)));

        /* Extract set 3 values */
        BBprogramValues_B_airoha_afe2_2236_set3 = extract_variable("BBprogramValues_B_airoha_afe2_2236_set3",1);
        if(BBprogramValues_B_airoha_afe2_2236_set3 == NULL)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                    "ganges_load_BB_prog_vals: Unable to extract BBprogramValues_B_airoha_afe2_2236_set3\n");
          ganges_mem_free(BBprogramValues_B_airoha_afe2_2236_set1);
          ganges_mem_free(BBprogramValues_B_airoha_afe2_2236_set2);
          Status = GANGES_STATUS_FAILURE;
          break;
        }
        RSI_DEBUG(RSI_ZONE_INFO,
                  "ganges_load_BB_prog_vals: BBprogramValues_B_airoha_afe2_2236_set3\n");
        ganges_dump(RSI_ZONE_INIT, 
                    BBprogramValues_B_airoha_afe2_2236_set3, 
                    ((BBprogramValues_B_airoha_afe2_2236_set3[0] + 1) * sizeof(UINT16)));
#endif

        if(BBprogramValues_B_airoha_afe2_2236_set1[0] > BB_PROGRAM_VAL_MAX)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,"ganges_load_BB_prog_vals: Too many BB values %d\n",
                                  BBprogramValues_B_airoha_afe2_2236_set1[0]);
          Status = GANGES_STATUS_FAILURE;
          break;
        }

        /* make sure max size is less than BB_PROGRAM_VAL_MAX */
        for(i=0; i<=BBprogramValues_B_airoha_afe2_2236_set1[0]; i++) 
        {
          Adapter->BBProgramVal[i] = BBprogramValues_B_airoha_afe2_2236_set1[i] ; 
        }
        for(i=0; i<=BBprogramValues_B_airoha_afe2_2236_set2[0]; i++) 
        {
          Adapter->BBProgramVal_set2[i] = BBprogramValues_B_airoha_afe2_2236_set2[i] ; 
        }
        for(i=0; i<=BBprogramValues_B_airoha_afe2_2236_set3[0]; i++) 
        {
          Adapter->BBProgramVal_set3[i] = BBprogramValues_B_airoha_afe2_2236_set3[i] ; 
        }

#ifdef DYNAMIC_VARIABLES
        ganges_mem_free(BBprogramValues_B_airoha_afe2_2236_set1);
        ganges_mem_free(BBprogramValues_B_airoha_afe2_2236_set2);
        ganges_mem_free(BBprogramValues_B_airoha_afe2_2236_set3);
#endif

      }
      else if(Adapter->RFType == RSI_RF_MAXIM_2831)
      {
#ifdef DYNAMIC_VARIABLES
        UINT16 *BBprogramValues_maxim_afe2;

        /* Extract BB values from file */
        BBprogramValues_maxim_afe2 = extract_variable("BBprogramValues_maxim_afe2",1);
        if(BBprogramValues_maxim_afe2 == NULL)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,
                    "ganges_load_BB_prog_vals: Unable to extract BBprogramValues_maxim_afe2\n");
          Status = GANGES_STATUS_FAILURE;
          break;
        }
        RSI_DEBUG(RSI_ZONE_INFO,
                  "ganges_load_BB_prog_vals: BBprogramValues_maxim_afe2\n");
        ganges_dump(RSI_ZONE_INIT, 
                    BBprogramValues_maxim_afe2, 
                    ((BBprogramValues_maxim_afe2[0] + 1) * sizeof(UINT16)));

#endif

        if(BBprogramValues_maxim_afe2[0] > BB_PROGRAM_VAL_MAX)
        {
          RSI_DEBUG(RSI_ZONE_ERROR,"ganges_load_BB_prog_vals: Too many BB values %d\n",
                                  BBprogramValues_maxim_afe2[0]);
          Status = GANGES_STATUS_FAILURE;
          break;
        }

        /* make sure max size is less than BB_PROGRAM_VAL_MAX */
        for(i=0; i<=BBprogramValues_maxim_afe2[0]; i++) 
        {
          Adapter->BBProgramVal[i] = BBprogramValues_maxim_afe2[i] ; 
        }

#ifdef DYNAMIC_VARIABLES
        ganges_mem_free(BBprogramValues_maxim_afe2);
#endif

      }
    }
  }while(FALSE);

  return Status;
}



/*FUNCTION*********************************************************************
Function Name  : ganges_load_RF_prog_vals
Description    :  
Returned Value : On success 0 will be returned else a ngeative number
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |     |     |  X  | pointer to our adapter
*END**************************************************************************/

GANGES_STATIC GANGES_STATUS
ganges_load_RF_prog_vals
  (
    PRSI_ADAPTER Adapter
  )
{
  GANGES_STATUS Status = GANGES_STATUS_SUCCESS;
  UINT8  i;

  do
  {
    if(Adapter->RFType == RSI_RF_MAXIM_2831)
    {
#ifdef DYNAMIC_VARIABLES
      UINT16 *resetChannelProgVal_B_maxim_AL2831;

      /* Extract RF programing values */
      resetChannelProgVal_B_maxim_AL2831 = extract_variable("resetChannelProgVal_B_maxim_AL2831",1);
      if(resetChannelProgVal_B_maxim_AL2831 == NULL)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "ganges_load_RF_prog_vals: Unable to extract resetChannelProgVal_B_maxim_AL2831\n");
        Status = GANGES_STATUS_FAILURE;
        break;
      }
      RSI_DEBUG(RSI_ZONE_INFO,
                "ganges_load_RF_prog_vals: resetChannelProgVal_B_maxim_AL2831\n");
       
      ganges_dump(RSI_ZONE_INFO, 
                  resetChannelProgVal_B_maxim_AL2831,
                  ((resetChannelProgVal_B_maxim_AL2831[0] + 1) * sizeof(UINT16)));

#endif

      if(resetChannelProgVal_B_maxim_AL2831[0] > RF_PRG_VAL_MAX)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,"ganges_load_RF_prog_vals: Too many RF values %d\n",
                                  resetChannelProgVal_B_maxim_AL2831[0]);
        Status = GANGES_STATUS_FAILURE;
        break;
      }
      for(i=0; i<=resetChannelProgVal_B_maxim_AL2831[0]; i++) 
      {
        Adapter->resetChannelProgVal[i] = resetChannelProgVal_B_maxim_AL2831[i] ;      
      }

#ifdef DYNAMIC_VARIABLES
      ganges_mem_free(resetChannelProgVal_B_maxim_AL2831);
#endif

    }
    else if(Adapter->RFType == RSI_RF_AL2230 )
    {
#ifdef DYNAMIC_VARIABLES
      UINT16 *resetChannelProgVal_B_airoha_AL2230;

      /* Extract RF programing values */
      resetChannelProgVal_B_airoha_AL2230 = extract_variable("resetChannelProgVal_B_airoha_AL2230",1);
      if(resetChannelProgVal_B_airoha_AL2230 == NULL)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "ganges_load_RF_prog_vals: Unable to extract resetChannelProgVal_B_airoha_AL2230\n");
        Status = GANGES_STATUS_FAILURE;
        break;
      }
      RSI_DEBUG(RSI_ZONE_INFO,
                "ganges_load_RF_prog_vals: resetChannelProgVal_B_airoha_AL2230\n");
       
      ganges_dump(RSI_ZONE_INFO, 
                  resetChannelProgVal_B_airoha_AL2230,
                  ((resetChannelProgVal_B_airoha_AL2230[0] + 1) * sizeof(UINT16)));

#endif

      if(resetChannelProgVal_B_airoha_AL2230[0] > RF_PRG_VAL_MAX)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,"ganges_load_RF_prog_vals: Too many RF values %d\n",
                                  resetChannelProgVal_B_airoha_AL2230[0]);
        Status = GANGES_STATUS_FAILURE;
        break;
      }
      for(i=0; i<=resetChannelProgVal_B_airoha_AL2230[0]; i++) 
      {
        Adapter->resetChannelProgVal[i] = resetChannelProgVal_B_airoha_AL2230[i] ;      
      }

#ifdef DYNAMIC_VARIABLES
      ganges_mem_free(resetChannelProgVal_B_airoha_AL2230);
#endif

    }
    else
    {
      if(Adapter->AFE_type == RSI_AFE_1)
      {
        if(Adapter->RFType == RSI_RF_AL2230S )
        {
#ifdef DYNAMIC_VARIABLES
          UINT16 *resetChannelProgVal_B_airoha_AL2230S_afe1;

          /* Extract RF programing values */
          resetChannelProgVal_B_airoha_AL2230S_afe1 =
                                 extract_variable("resetChannelProgVal_B_airoha_AL2230S_afe1",1);
          if(resetChannelProgVal_B_airoha_AL2230S_afe1 == NULL)
          {
            RSI_DEBUG(RSI_ZONE_ERROR,
                      "ganges_load_RF_prog_vals: Unable to extract resetChannelProgVal_B_airoha_AL2230S_afe1\n");
            Status = GANGES_STATUS_FAILURE;
            break;
          }
          RSI_DEBUG(RSI_ZONE_INFO,
                    "ganges_load_RF_prog_vals: resetChannelProgVal_B_airoha_AL2230S_afe1\n");
       
          ganges_dump(RSI_ZONE_INFO, 
                      resetChannelProgVal_B_airoha_AL2230S_afe1,
                      ((resetChannelProgVal_B_airoha_AL2230S_afe1[0] + 1) * sizeof(UINT16)));

#endif
          if(resetChannelProgVal_B_airoha_AL2230S_afe1[0] > RF_PRG_VAL_MAX)
          {
	    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_load_RF_prog_vals: Too many RF values %d\n",resetChannelProgVal_B_airoha_AL2230S_afe1[0]);
            Status = GANGES_STATUS_FAILURE;
            break;
          }
          for(i=0; i<=resetChannelProgVal_B_airoha_AL2230S_afe1[0]; i++) 
          {
            Adapter->resetChannelProgVal[i] = resetChannelProgVal_B_airoha_AL2230S_afe1[i] ;      
          }

#ifdef DYNAMIC_VARIABLES
          ganges_mem_free(resetChannelProgVal_B_airoha_AL2230S_afe1);
#endif

        }
        else if(Adapter->RFType == RSI_RF_AL2236 )
        {
#ifdef DYNAMIC_VARIABLES
          UINT16 *resetChannelProgVal_B_airoha_AL2236_afe1_set1;
          UINT16 *resetChannelProgVal_B_airoha_AL2236_afe1_set2;

          /* Extract RF programing values */
          resetChannelProgVal_B_airoha_AL2236_afe1_set1 =
                                 extract_variable("resetChannelProgVal_B_airoha_AL2236_afe1_set1",1);
          if(resetChannelProgVal_B_airoha_AL2236_afe1_set1 == NULL)
          {
            RSI_DEBUG(RSI_ZONE_ERROR,
                      "ganges_load_RF_prog_vals: Unable to extract resetChannelProgVal_B_airoha_AL2236_afe1_set1\n");
            Status = GANGES_STATUS_FAILURE;
            break;
          }
          RSI_DEBUG(RSI_ZONE_INFO,
                    "ganges_load_RF_prog_vals: resetChannelProgVal_B_airoha_AL2236_afe1_set1\n");
       
          ganges_dump(RSI_ZONE_INFO, 
                      resetChannelProgVal_B_airoha_AL2236_afe1_set1,
                      ((resetChannelProgVal_B_airoha_AL2236_afe1_set1[0] + 1) * sizeof(UINT16)));

          /* Extract RF programing values set 2*/
          resetChannelProgVal_B_airoha_AL2236_afe1_set2 =
                                 extract_variable("resetChannelProgVal_B_airoha_AL2236_afe1_set2",1);
          if(resetChannelProgVal_B_airoha_AL2236_afe1_set2 == NULL)
          {
            RSI_DEBUG(RSI_ZONE_ERROR,
                      "ganges_load_RF_prog_vals: Unable to extract resetChannelProgVal_B_airoha_AL2236_afe1_set2\n");
            ganges_mem_free(resetChannelProgVal_B_airoha_AL2236_afe1_set1);
            Status = GANGES_STATUS_FAILURE;
            break;
          }
          RSI_DEBUG(RSI_ZONE_INFO,
                    "ganges_load_RF_prog_vals: resetChannelProgVal_B_airoha_AL2236_afe1_set2\n");
       
          ganges_dump(RSI_ZONE_INFO, 
                      resetChannelProgVal_B_airoha_AL2236_afe1_set2,
                      ((resetChannelProgVal_B_airoha_AL2236_afe1_set2[0] + 1) * sizeof(UINT16)));
#endif
          if(resetChannelProgVal_B_airoha_AL2236_afe1_set1[0] > RF_PRG_VAL_MAX)
          {
            RSI_DEBUG(RSI_ZONE_ERROR,"ganges_load_RF_prog_vals: Too many RF values %d\n",
                       resetChannelProgVal_B_airoha_AL2236_afe1_set1[0]);
            Status = GANGES_STATUS_FAILURE;
            break;
          }
          for(i=0; i<=resetChannelProgVal_B_airoha_AL2236_afe1_set1[0]; i++) 
          {
            Adapter->resetChannelProgVal[i] = resetChannelProgVal_B_airoha_AL2236_afe1_set1[i] ;      
          }
          for(i=0; i<=resetChannelProgVal_B_airoha_AL2236_afe1_set2[0]; i++) 
          {
            Adapter->resetChannelProgVal_set2[i] = resetChannelProgVal_B_airoha_AL2236_afe1_set2[i] ;      
          }



#ifdef DYNAMIC_VARIABLES
          ganges_mem_free(resetChannelProgVal_B_airoha_AL2236_afe1_set1);
          ganges_mem_free(resetChannelProgVal_B_airoha_AL2236_afe1_set2);
#endif

        }

      }
      else if(Adapter->AFE_type == RSI_AFE_2)
      {
        if(Adapter->RFType == RSI_RF_AL2230S )
        {
#ifdef DYNAMIC_VARIABLES
          UINT16 *resetChannelProgVal_B_airoha_AL2230S_afe2;

          /* Extract RF programing values */
          resetChannelProgVal_B_airoha_AL2230S_afe2 =
                                 extract_variable("resetChannelProgVal_B_airoha_AL2230S_afe2",1);
          if(resetChannelProgVal_B_airoha_AL2230S_afe2 == NULL)
          {
            RSI_DEBUG(RSI_ZONE_ERROR,
                      "ganges_load_RF_prog_vals: Unable to extract resetChannelProgVal_B_airoha_AL2230S_afe2\n");
            Status = GANGES_STATUS_FAILURE;
            break;
          }
          RSI_DEBUG(RSI_ZONE_INFO,
                    "ganges_load_RF_prog_vals: resetChannelProgVal_B_airoha_AL2230S_afe2\n");
       
          ganges_dump(RSI_ZONE_INFO, 
                      resetChannelProgVal_B_airoha_AL2230S_afe2,
                      ((resetChannelProgVal_B_airoha_AL2230S_afe2[0] + 1) * sizeof(UINT16)));

#endif
          if(resetChannelProgVal_B_airoha_AL2230S_afe2[0] > RF_PRG_VAL_MAX)
          {
            RSI_DEBUG(RSI_ZONE_ERROR,"ganges_load_RF_prog_vals: Too many RF values %d\n",
                  resetChannelProgVal_B_airoha_AL2230S_afe2[0]);
            Status = GANGES_STATUS_FAILURE;
            break;
          }
          for(i=0; i<=resetChannelProgVal_B_airoha_AL2230S_afe2[0]; i++) 
          {
            Adapter->resetChannelProgVal[i] = resetChannelProgVal_B_airoha_AL2230S_afe2[i] ;      
          }

#ifdef DYNAMIC_VARIABLES
          ganges_mem_free(resetChannelProgVal_B_airoha_AL2230S_afe2);
#endif

        }
        else if(Adapter->RFType == RSI_RF_AL2236 )
        {
#ifdef DYNAMIC_VARIABLES
          UINT16 *resetChannelProgVal_B_airoha_AL2236_afe2_set1;
          UINT16 *resetChannelProgVal_B_airoha_AL2236_afe2_set2;

          switch(Adapter->config_params.crystalFrequency)
          {
            case 0x9:
            {
              RSI_DEBUG(RSI_ZONE_ERROR,
                        "load_RF_prog_vals: Extracting RF values for 40 Mhz Crystal Clock\n");
              resetChannelProgVal_B_airoha_AL2236_afe2_set1 =
                                 extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set1_40mhz",1);
              resetChannelProgVal_B_airoha_AL2236_afe2_set2 =
                                  extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set2_40mhz",1);
            }
            break;

            case 0x8:
            {
              RSI_DEBUG(RSI_ZONE_ERROR,
                        "load_RF_prog_vals: Extracting RF values for 20 Mhz Crystal Clock\n");
              resetChannelProgVal_B_airoha_AL2236_afe2_set1 =
                                 extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set1_20mhz",1);
              resetChannelProgVal_B_airoha_AL2236_afe2_set2 =
                                  extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set2_20mhz",1);
            }
            break;

            case 0x7:
            {
              RSI_DEBUG(RSI_ZONE_ERROR,
                        "load_RF_prog_vals: Extracting RF set1 and set2 values for 44 Mhz Crystal Clock\n");
              resetChannelProgVal_B_airoha_AL2236_afe2_set1 =
                                 extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set1_44mhz",1);
              resetChannelProgVal_B_airoha_AL2236_afe2_set2 =
                                  extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set2_44mhz",1);
            }
            break;

            case 0x6:
            {
              /* 		    
              RSI_DEBUG(RSI_ZONE_ERROR,
                        "load_RF_prog_vals: Extracting RF values for 52 Mhz Crystal Clock\n");
              resetChannelProgVal_B_airoha_AL2236_afe2_set1 =
                                 extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set1_52mhz",1);
              resetChannelProgVal_B_airoha_AL2236_afe2_set2 =
                                  extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set2_52mhz",1);
				  */
              RSI_DEBUG(RSI_ZONE_ERROR,
                       "ERROR: RF Values: 52 Mhz Crystal Clock Not Supported\n");
              return GANGES_STATUS_FAILURE;
		    
            }
            break;

            case 0x5:
            {
              RSI_DEBUG(RSI_ZONE_ERROR,
                        "load_RF_prog_vals: Extracting RF values for 26 Mhz Crystal Clock\n");
              resetChannelProgVal_B_airoha_AL2236_afe2_set1 =
                                 extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set1_26mhz",1);
              resetChannelProgVal_B_airoha_AL2236_afe2_set2 =
                                  extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set2_26mhz",1);
            }
            break;

            case 0x4:
            {
              RSI_DEBUG(RSI_ZONE_ERROR,
                        "load_RF_prog_vals: Extracting RF values for 13 Mhz Crystal Clock\n");
              resetChannelProgVal_B_airoha_AL2236_afe2_set1 =
                                 extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set1_13mhz",1);
              resetChannelProgVal_B_airoha_AL2236_afe2_set2 =
                                  extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set2_13mhz",1);
            }
            break;

            case 0x2:
            {
	      /*	    
              RSI_DEBUG(RSI_ZONE_ERROR,
                        "load_RF_prog_vals: Extracting RF values for 38.4 Mhz Crystal Clock\n");
              resetChannelProgVal_B_airoha_AL2236_afe2_set1 =
                                 extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set1_38p4mhz",1);
              resetChannelProgVal_B_airoha_AL2236_afe2_set2 =
                                  extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set2_38p4mhz",1);
				  */
              RSI_DEBUG(RSI_ZONE_ERROR,
                       "ERROR: RF Values: 38.4 Mhz Crystal Clock Not Supported\n");
              return GANGES_STATUS_FAILURE;
		    
            }
            break;
            case 0x1:
            {
              RSI_DEBUG(RSI_ZONE_ERROR,
                        "load_RF_prog_vals: Extracting RF values for 19.2 Mhz Crystal Clock\n");
              resetChannelProgVal_B_airoha_AL2236_afe2_set1 =
                                 extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set1_19p2mhz",1);
              resetChannelProgVal_B_airoha_AL2236_afe2_set2 =
                                  extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set2_19p2mhz",1);
            }
            break;

            case 0x0:
            {
	      /*	    
              RSI_DEBUG(RSI_ZONE_ERROR,
                        "load_RF_prog_vals: Extracting RF values for 9.6 Mhz Crystal Clock\n");
              resetChannelProgVal_B_airoha_AL2236_afe2_set1 =
                                 extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set1_9p6mhz",1);
              resetChannelProgVal_B_airoha_AL2236_afe2_set2 =
                                  extract_variable("resetChannelProgVal_B_airoha_AL2236_afe2_set2_9p6mhz",1);
				  */
             RSI_DEBUG(RSI_ZONE_ERROR,
                      "ERROR: RF Values: 9.6 Mhz Crystal Clock Not Supported\n");
             return GANGES_STATUS_FAILURE;
		    
            }
            break;

            default:
            {
              RSI_DEBUG(RSI_ZONE_ERROR,
                       "load_RF_prog_vals: Error Unknown Crystal Clock Found\n");
              return GANGES_STATUS_FAILURE;
            }
          }


          if(resetChannelProgVal_B_airoha_AL2236_afe2_set1 == NULL)
          {
            RSI_DEBUG(RSI_ZONE_ERROR,
                      "ganges_load_RF_prog_vals: Unable to extract resetChannelProgVal_B_airoha_AL2236_afe2_set1\n");
            Status = GANGES_STATUS_FAILURE;
            break;
          }
          RSI_DEBUG(RSI_ZONE_INFO,
                    "ganges_load_RF_prog_vals: resetChannelProgVal_B_airoha_AL2236_afe2_set1\n");
       
          ganges_dump(RSI_ZONE_INFO, 
                      resetChannelProgVal_B_airoha_AL2236_afe2_set1,
                      ((resetChannelProgVal_B_airoha_AL2236_afe2_set1[0] + 1) * sizeof(UINT16)));

          if(resetChannelProgVal_B_airoha_AL2236_afe2_set2 == NULL)
          {
            RSI_DEBUG(RSI_ZONE_ERROR,
                      "ganges_load_RF_prog_vals: Unable to extract resetChannelProgVal_B_airoha_AL2236_afe2_set2\n");
            ganges_mem_free(resetChannelProgVal_B_airoha_AL2236_afe2_set1);
            Status = GANGES_STATUS_FAILURE;
            break;
          }
          RSI_DEBUG(RSI_ZONE_INFO,
                    "ganges_load_RF_prog_vals: resetChannelProgVal_B_airoha_AL2236_afe2_set2\n");
       
          ganges_dump(RSI_ZONE_INFO, 
                      resetChannelProgVal_B_airoha_AL2236_afe2_set2,
                      ((resetChannelProgVal_B_airoha_AL2236_afe2_set2[0] + 1) * sizeof(UINT16)));

#endif
          if(resetChannelProgVal_B_airoha_AL2236_afe2_set1[0] > RF_PRG_VAL_MAX)
          {
            RSI_DEBUG(RSI_ZONE_ERROR,"ganges_load_RF_prog_vals: Too many RF values %d\n",
            resetChannelProgVal_B_airoha_AL2236_afe2_set1[0]);
            Status = GANGES_STATUS_FAILURE;
            break;
          }
          for(i=0; i<=resetChannelProgVal_B_airoha_AL2236_afe2_set1[0]; i++) 
          {
            Adapter->resetChannelProgVal[i] = resetChannelProgVal_B_airoha_AL2236_afe2_set1[i] ;      
          }
          for(i=0; i<=resetChannelProgVal_B_airoha_AL2236_afe2_set2[0]; i++) 
          {
            Adapter->resetChannelProgVal_set2[i] = resetChannelProgVal_B_airoha_AL2236_afe2_set2[i] ;      
          }

#ifdef DYNAMIC_VARIABLES
          ganges_mem_free(resetChannelProgVal_B_airoha_AL2236_afe2_set1);
          ganges_mem_free(resetChannelProgVal_B_airoha_AL2236_afe2_set2);
#endif

        }
      }
    }
  }while(FALSE);

  return Status;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_load_RF_power_vals
Description    :  
Returned Value : On success 0 will be returned else a ngeative number
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |     |     |  X  | pointer to our adapter
*END**************************************************************************/

GANGES_STATIC GANGES_STATUS
ganges_load_RF_power_vals
  (
    PRSI_ADAPTER Adapter
  )
{
  GANGES_STATUS Status = GANGES_STATUS_SUCCESS;
  UINT8  i,j;

  do
  {
    if(Adapter->RFType == RSI_RF_AL2236)
    {
#ifdef DYNAMIC_VARIABLES
      UINT16 *power_val_set_AL2236;

      /* Extract RF power values */
      power_val_set_AL2236 = extract_variable("power_val_set_AL2236",1);
      if(power_val_set_AL2236 == NULL)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
               "ganges_load_RF_power_vals: Unable to extract power_val_set_AL2236\n");
        Status = GANGES_STATUS_FAILURE;
        break;
      }
      RSI_DEBUG(RSI_ZONE_INFO,
                "ganges_load_RF_power_vals: power_val_set_AL2236\n");
       
      if(power_val_set_AL2236[0] > RSI_MAX_PWR_VAL_SET_2236 * 20)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "ganges_load_RF_power_vals: Too many RF power values %d\n",
                   power_val_set_AL2236[0]);
        Status = GANGES_STATUS_FAILURE;
        break;
      }

      for(i=0; i<2; i++)
      {
        for(j=0; j<=20; j++)
        {        
          Adapter->power_val_set[i][j] = *(power_val_set_AL2236+1+ j + (i*20));
        }
      }
      ganges_mem_free(power_val_set_AL2236);
#else

      if(power_val_set_AL2236[0][0] > RSI_MAX_PWR_VAL_SET_2236 * 20)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,"ganges_load_RF_power_vals: Too many RF power values %d\n",
                                  power_val_set_AL2236[0][0]);
        Status = GANGES_STATUS_FAILURE;
        break;
      }
      for(i=1; i<=2; i++) 
      {
        for(j=0; j<20; j++)
        {        
          Adapter->power_val_set[i-1][j] = (UINT8)power_val_set_AL2236[i][j];
        }
        RSI_DEBUG(RSI_ZONE_INFO, "\n");
      }
#endif
      RSI_DEBUG(RSI_ZONE_INFO, "power_val_set_AL2236\n");
      ganges_dump(RSI_ZONE_INFO, Adapter->power_val_set, 2* 20);

    }
  }while(FALSE);

  return Status;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_load_power_save_params
Description    :  
Returned Value : On success 0 will be returned else a ngeative number
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |     |     |  X  | pointer to our adapter
*END**************************************************************************/

GANGES_STATUS
ganges_load_power_save_params
  (
     IN PRSI_ADAPTER Adapter
  )
{
  GANGES_STATUS Status = GANGES_STATUS_SUCCESS;
  UINT32 i;
  if(Adapter->AFE_type == RSI_AFE_1)
  {
#ifdef DYNAMIC_VARIABLES
    UINT16 *BB_sleep_prog_values_2236_AFE1;
    UINT16 *BB_wakeup_prog_values_2236_AFE1;
    UINT16 *RF_sleep_prog_values_2236_AFE1;
    UINT16 *RF_wakeup_prog_values_2236_AFE1;
  
    /* Extract BB sleep programing values */
    BB_sleep_prog_values_2236_AFE1 =
                         extract_variable("BB_sleep_prog_values_2236_AFE1",1);
    if(BB_sleep_prog_values_2236_AFE1 == NULL)
    { 
      RSI_DEBUG(RSI_ZONE_ERROR,
        (TEXT("ganges_load_power_save_params: Unable to extract BB_sleep_prog_values_2236_AFE1\n")));
      Status = GANGES_STATUS_FAILURE;
      return Status;
    }

    /* Extract RF sleep programing values */
    RF_sleep_prog_values_2236_AFE1 =
                      extract_variable("RF_sleep_prog_values_2236_AFE1",1);
    if(RF_sleep_prog_values_2236_AFE1 == NULL)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
         (TEXT("ganges_load_power_save_params: Unable to extract RF_sleep_prog_values_2236_AFE1\n")));
      Status = GANGES_STATUS_FAILURE;
      return Status;
    }

    /* Extract BB wakeup programing values */
    BB_wakeup_prog_values_2236_AFE1 =
                            extract_variable("BB_wakeup_prog_values_2236_AFE1",1);
    if(BB_wakeup_prog_values_2236_AFE1 == NULL)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
        (TEXT("ganges_load_RF_prog_vals: Unable to extract BB_wakeup_prog_values_2236_AFE1\n")));
      Status = GANGES_STATUS_FAILURE;
      return Status;
    }

    /* Extract RF wakeup programing values */
    RF_wakeup_prog_values_2236_AFE1 =
                         extract_variable("RF_wakeup_prog_values_2236_AFE1",1);
    if(RF_wakeup_prog_values_2236_AFE1 == NULL)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
       (TEXT("ganges_load_RF_prog_vals: Unable to extract RF_wakeup_prog_values_2236_AFE1\n")));
      Status = GANGES_STATUS_FAILURE;
      return Status;
    }
#endif

    for(i=0; i<=BB_sleep_prog_values_2236_AFE1[0]; i++) 
    {
      Adapter->BB_sleep_prog_val[i] = BB_sleep_prog_values_2236_AFE1[i] ;      
    }
    for(i=0; i<=RF_sleep_prog_values_2236_AFE1[0]; i++) 
    {
      Adapter->RF_sleep_prog_val[i] = RF_sleep_prog_values_2236_AFE1[i] ;      
    }
    for(i=0; i<=BB_wakeup_prog_values_2236_AFE1[0]; i++) 
    {
      Adapter->BB_wakeup_prog_val[i] = BB_wakeup_prog_values_2236_AFE1[i] ;      
    }
    for(i=0; i<=RF_wakeup_prog_values_2236_AFE1[0]; i++) 
    {
      Adapter->RF_wakeup_prog_val[i] = RF_wakeup_prog_values_2236_AFE1[i] ;      
    }


    RSI_DEBUG(RSI_ZONE_INFO,
              (TEXT("BB_sleep_prog_values_2236_AFE1:\n")));
    ganges_dump(RSI_ZONE_INFO,
                BB_sleep_prog_values_2236_AFE1,
               ((BB_sleep_prog_values_2236_AFE1[0] + 1) * sizeof(UINT16)));

    RSI_DEBUG(RSI_ZONE_INFO,
              (TEXT("RF_sleep_prog_values_2236_AFE1\n")));
    ganges_dump(RSI_ZONE_INFO,
                RF_sleep_prog_values_2236_AFE1,
                ((RF_sleep_prog_values_2236_AFE1[0] + 1) * sizeof(UINT16)));

    RSI_DEBUG(RSI_ZONE_INFO,
                    (TEXT("BB_wakeup_prog_values_2236_AFE1\n")));
    ganges_dump(RSI_ZONE_INFO,
                BB_wakeup_prog_values_2236_AFE1,
                ((BB_wakeup_prog_values_2236_AFE1[0] + 1) * sizeof(UINT16)));


    RSI_DEBUG(RSI_ZONE_INFO,
              (TEXT("RF_wakeup_prog_values_2236_AFE1:\n")));
    ganges_dump(RSI_ZONE_INFO,
                RF_wakeup_prog_values_2236_AFE1,
                ((RF_wakeup_prog_values_2236_AFE1[0] + 1) * sizeof(UINT16)));

#ifdef DYNAMIC_VARIABLES
    ganges_mem_free(BB_sleep_prog_values_2236_AFE1);
    ganges_mem_free(RF_sleep_prog_values_2236_AFE1);
    ganges_mem_free(BB_wakeup_prog_values_2236_AFE1);
    ganges_mem_free(RF_wakeup_prog_values_2236_AFE1);
#endif
  }
  else
  {
#ifdef DYNAMIC_VARIABLES
    UINT16 *BB_sleep_prog_values_2236_AFE2;
    UINT16 *BB_wakeup_prog_values_2236_AFE2;
    UINT16 *RF_sleep_prog_values_2236_AFE2;
    UINT16 *RF_wakeup_prog_values_2236_AFE2;

    /* Extract BB sleep programing values */
    BB_sleep_prog_values_2236_AFE2 =
                       extract_variable("BB_sleep_prog_values_2236_AFE2",1);
    if(BB_sleep_prog_values_2236_AFE2 == NULL)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
       (TEXT("ganges_load_RF_prog_vals: Unable to extract BB_sleep_prog_values_2236_AFE2\n")));
      Status = GANGES_STATUS_FAILURE;
      return Status;
    }

    /* Extract RF sleep programing values */
    RF_sleep_prog_values_2236_AFE2 =
                   extract_variable("RF_sleep_prog_values_2236_AFE2",1);
    if(RF_sleep_prog_values_2236_AFE2 == NULL)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
          (TEXT("ganges_load_RF_prog_vals: Unable to extract RF_sleep_prog_values_2236_AFE2\n")));
      Status = GANGES_STATUS_FAILURE;
      return Status;
    }

    /* Extract BB wakeup programing values */
    BB_wakeup_prog_values_2236_AFE2 =
                         extract_variable("BB_wakeup_prog_values_2236_AFE2",1);
    if(BB_wakeup_prog_values_2236_AFE2 == NULL)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
        (TEXT("ganges_load_RF_prog_vals: Unable to extract BB_wakeup_prog_values_2236_AFE2\n")));
      Status = GANGES_STATUS_FAILURE;
      return Status;
    }

    /* Extract RF wakeup programing values */
    RF_wakeup_prog_values_2236_AFE2 =
                      extract_variable("RF_wakeup_prog_values_2236_AFE2",1);
    if(RF_wakeup_prog_values_2236_AFE2 == NULL)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
         (TEXT("ganges_load_RF_prog_vals: Unable to extract RF_wakeup_prog_values_2236_AFE2\n")));
      Status = GANGES_STATUS_FAILURE;
      return Status;
    }
#endif
    for(i=0; i<=BB_sleep_prog_values_2236_AFE2[0]; i++) 
    {
      Adapter->BB_sleep_prog_val[i] = BB_sleep_prog_values_2236_AFE2[i] ;      
    }
    for(i=0; i<=RF_sleep_prog_values_2236_AFE2[0]; i++) 
    {
      Adapter->RF_sleep_prog_val[i] = RF_sleep_prog_values_2236_AFE2[i] ;      
    }
    for(i=0; i<=BB_wakeup_prog_values_2236_AFE2[0]; i++) 
    {
      Adapter->BB_wakeup_prog_val[i] = BB_wakeup_prog_values_2236_AFE2[i] ;      
    }
    for(i=0; i<=RF_wakeup_prog_values_2236_AFE2[0]; i++) 
    {
      Adapter->RF_wakeup_prog_val[i] = RF_wakeup_prog_values_2236_AFE2[i] ;      
    }


    RSI_DEBUG(RSI_ZONE_INFO, (TEXT("BB_sleep_prog_values_2236_AFE2\n")));
    ganges_dump(RSI_ZONE_INFO,
                BB_sleep_prog_values_2236_AFE2,
                ((BB_sleep_prog_values_2236_AFE2[0] + 1) * sizeof(UINT16)));

    RSI_DEBUG(RSI_ZONE_INFO, (TEXT("RF_sleep_prog_values_2236_AFE2\n")));
    ganges_dump(RSI_ZONE_INFO,
                RF_sleep_prog_values_2236_AFE2,
                ((RF_sleep_prog_values_2236_AFE2[0] + 1) * sizeof(UINT16)));

    RSI_DEBUG(RSI_ZONE_INFO, (TEXT("BB_wakeup_prog_values_2236_AFE2\n")));
    ganges_dump(RSI_ZONE_INFO,
                BB_wakeup_prog_values_2236_AFE2,
                ((BB_wakeup_prog_values_2236_AFE2[0] + 1) * sizeof(UINT16)));

    RSI_DEBUG(RSI_ZONE_INFO, (TEXT("RF_wakeup_prog_values_2236_AFE2\n")));
    ganges_dump(RSI_ZONE_INFO,
                RF_wakeup_prog_values_2236_AFE2,
               ((RF_wakeup_prog_values_2236_AFE2[0] + 1) * sizeof(UINT16)));

#ifdef DYNAMIC_VARIABLES
    ganges_mem_free(BB_sleep_prog_values_2236_AFE2);
    ganges_mem_free(RF_sleep_prog_values_2236_AFE2);
    ganges_mem_free(BB_wakeup_prog_values_2236_AFE2);
    ganges_mem_free(RF_wakeup_prog_values_2236_AFE2);
#endif
  }
  return Status;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_load_config_vals
Description    :  
Returned Value : On success 0 will be returned else a ngeative number
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |     |     |  X  | pointer to our adapter
*END**************************************************************************/
GANGES_STATUS 
ganges_load_config_vals
  (
    PVOID Pcontext
  )
{
  UINT16 *rsi_config_vals;
  PRSI_ADAPTER Adapter = (PRSI_ADAPTER)Pcontext;

  /* Extract ganges_load_config_vals programing values */
  rsi_config_vals = extract_variable("rsi_config_vals",0);
  if(rsi_config_vals == NULL)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
             (TEXT("ganges_load_config_vals: Unable to extract rsi_config_vals\n")));
    return GANGES_STATUS_FAILURE;
  }
  RSI_DEBUG(RSI_ZONE_INFO,
           (TEXT("ganges_load_config_vals: rsi_config_vals\n")));

  ganges_dump(RSI_ZONE_INIT, 
              rsi_config_vals, 
              ((rsi_config_vals[0] + 1) * sizeof(UINT16)));

  /** Driver Expecting Only 9 paramaeters***/
  if(rsi_config_vals[0] == 13)
  {
    ganges_memcpy(&Adapter->config_params,
                  &rsi_config_vals[1],
                  26);    
    //Adapter->config_params.host_wkup_enable=1;
    ganges_dump(RSI_ZONE_INIT,&Adapter->config_params,26);	
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_INFO,
             (TEXT("ganges_load_config_vals: Wrong Number of Config parameters Found\n")));
    return GANGES_STATUS_FAILURE;
      
  }

   if(Adapter->config_params.TAPLLEnabled)
   {
     RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: TA PLL Enabled\n"); 
     if(Adapter->config_params.crystalFrequency == 0x09) //40Mhz  
     {
       RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: Cyrstal Frequency: 40Mhz\n"); 
       if(Adapter->config_params.TAPLLFrequency == 20)
       {
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: TA PLL Frequency: 20\n"); 
	 Adapter->config_params.mratio       = 0x0c; 
	 Adapter->config_params.inputdratio  = 0x01;
	 Adapter->config_params.outputdratio = 0x0c;
       } 
       else if(Adapter->config_params.TAPLLFrequency == 40)
       {
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: TA PLL Frequency: 40\n"); 
	 Adapter->config_params.mratio       = 0x09;
	 Adapter->config_params.inputdratio  = 0x00;
	 Adapter->config_params.outputdratio = 0x09;
       } 
       else if(Adapter->config_params.TAPLLFrequency == 60)
       {
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: TA PLL Frequency: 60\n"); 
	 Adapter->config_params.mratio       = 0x0b;
	 Adapter->config_params.inputdratio  = 0x01;
	 Adapter->config_params.outputdratio = 0x03;
       } 
       else if(Adapter->config_params.TAPLLFrequency == 80)
       {
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: TA PLL Frequency: 80\n"); 
	 Adapter->config_params.mratio       = 0x09;
	 Adapter->config_params.inputdratio  = 0x00;
	 Adapter->config_params.outputdratio = 0x04;
       } 
       else if(Adapter->config_params.TAPLLFrequency == 100)
       {
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: TA PLL Frequency: 100\n"); 
	 Adapter->config_params.mratio       = 29;
	 Adapter->config_params.inputdratio  = 0x03;
	 Adapter->config_params.outputdratio = 0x02;
       } 
       else if(Adapter->config_params.TAPLLFrequency == 117)
       {
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: TA PLL Frequency: 117\n"); 
	 Adapter->config_params.mratio       = 46;
	 Adapter->config_params.inputdratio  = 0x03;
	 Adapter->config_params.outputdratio = 0x03;
       } 
       else if(Adapter->config_params.TAPLLFrequency == 120)
       {
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: TA PLL Frequency: 120\n"); 
	 Adapter->config_params.mratio       = 11;
	 Adapter->config_params.inputdratio  = 0x01;
	 Adapter->config_params.outputdratio = 0x01;
       } 
       else if(Adapter->config_params.TAPLLFrequency == 140)
       {
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: TA PLL Frequency: 140\n"); 
	 Adapter->config_params.mratio       = 0x0d;
	 Adapter->config_params.inputdratio  = 0x01;
	 Adapter->config_params.outputdratio = 0x01;
       } 
       else if(Adapter->config_params.TAPLLFrequency == 150)
       {
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: TA PLL Frequency: 150\n"); 
	 Adapter->config_params.mratio       = 29;
	 Adapter->config_params.inputdratio  = 0x03;
	 Adapter->config_params.outputdratio = 0x01; 
       } 
       else 
       {
	 RSI_DEBUG(RSI_ZONE_ERROR,"No match found\n");
	 Adapter->config_params.mratio       = 0; 
	 Adapter->config_params.inputdratio  = 0;
	 Adapter->config_params.outputdratio = 0;
       } 
     }  
     else if(Adapter->config_params.TAPLLFrequency == 40)
     {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: TA PLL Frequency: 40\n"); 
        if(Adapter->config_params.crystalFrequency == 0) //9.6MHz
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: Cyrstal Frequency: 9.6Mhz\n"); 
	  Adapter->config_params.mratio       = 29;
	  Adapter->config_params.inputdratio  = 0x00;
	  Adapter->config_params.outputdratio = 0x06; 
        }
	else if(Adapter->config_params.crystalFrequency == 4) //13Mhz
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: Cyrstal Frequency: 13Mhz\n"); 
	  Adapter->config_params.mratio       = 0x15;
	  Adapter->config_params.inputdratio  = 0x00;
	  Adapter->config_params.outputdratio = 0x06; 
        }
	else if(Adapter->config_params.crystalFrequency == 1) //19.2Mhz
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: Cyrstal Frequency: 19.2Mhz\n"); 
	  Adapter->config_params.mratio       = 0x31;
	  Adapter->config_params.inputdratio  = 0x01;
	  Adapter->config_params.outputdratio = 11; 
        }
	else if(Adapter->config_params.crystalFrequency == 8) //20Mhz
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: Cyrstal Frequency: 20Mhz\n"); 
	  Adapter->config_params.mratio       = 19;
	  Adapter->config_params.inputdratio  = 0x00;
	  Adapter->config_params.outputdratio = 0x09; 
        }
	else if(Adapter->config_params.crystalFrequency == 5) //26Mhz
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: Cyrstal Frequency: 6Mhz\n"); 
	  Adapter->config_params.mratio       = 10;
	  Adapter->config_params.inputdratio  = 0x00;
	  Adapter->config_params.outputdratio = 0x06; 
        }
	else if(Adapter->config_params.crystalFrequency == 9) //40Mhz
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: Cyrstal Frequency: 40Mhz\n"); 
	  Adapter->config_params.mratio       = 9;
	  Adapter->config_params.inputdratio  = 0;
	  Adapter->config_params.outputdratio = 9; 
        }
	else if(Adapter->config_params.crystalFrequency == 6) //52Mhz
        {
          RSI_DEBUG(RSI_ZONE_INFO,"ganges_load_config_vals: Cyrstal Frequency: 52Mhz\n"); 
	  Adapter->config_params.mratio       = 7;
	  Adapter->config_params.inputdratio  = 0;
	  Adapter->config_params.outputdratio = 0x09; 
        }
	else
        {
	  Adapter->config_params.mratio       = 0;
	  Adapter->config_params.inputdratio  = 0;
	  Adapter->config_params.outputdratio = 0; 
        }
     }
   }   
   else
   {
     RSI_DEBUG(RSI_ZONE_ERROR,"TA PLL Disabled\n");
     Adapter->config_params.mratio=0; 
     Adapter->config_params.inputdratio  = 0;
     Adapter->config_params.outputdratio = 0;      
   } 
   /* free the memory allocated for dynamic variables */
   RSI_DEBUG(RSI_ZONE_INFO,
            (TEXT("ganges_load_config_vals: Freeing dynamic variables memory\n")));
   ganges_mem_free(rsi_config_vals);

   return GANGES_STATUS_SUCCESS;

}

/*FUNCTION*********************************************************************
Function Name  : ganges_mgmt_init
Description    : Initializing managment layer which implies, initializing 
		 default rates, loading baseband, RF & channel programming values.
                 And finally program the base band and RF.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |     |     |  X  | pointer to our adapter
*END**************************************************************************/

GANGES_STATUS 
ganges_mgmt_init
  (
     IN PRSI_ADAPTER Adapter
  )
{
  GANGES_STATUS Status = GANGES_STATUS_SUCCESS;

  do
  {
    Adapter->minChannelTime = 0x0200;
    Adapter->maxChannelTime = 0x0300;
    /* setting buffer info */
    Adapter->BssidFound = 0;
    Adapter->SelectedBssidNum = 255;
    Adapter->JoinRequestPending = 0 ; 
    Adapter->bb_max_gain_current = 0x382E;
    Adapter->gain_scaling_current = 0x102E;
    Adapter->nf_auto_no_pkts_to_avg = NF_AUTO_PKT_CNT_TO_AVERAGE;

    FSM_STATE = FSM_CARD_NOT_READY;

    Adapter->WepKeyLength     = 0;
    ganges_memset(Adapter->RSIWepkey, 0, 120);

    if((Adapter->ProtocolType == ProtoType802_11G) ||
       (Adapter->ProtocolType == ProtoType802_11B) || 
       (Adapter->ProtocolType == ProtoType802_11N ))
    {
      if((Adapter->ProtocolType == ProtoType802_11G)||
        (Adapter->ProtocolType == ProtoType802_11B))
      {
        Adapter->dfl_mgmt_rate   = RSI_RATE_1;
        Adapter->dfl_B_data_rate = RSI_RATE_11;
      }
      else if(Adapter->ProtocolType == ProtoType802_11N)
      {
	/*FIXME*/
        Adapter->dfl_mgmt_rate   = RSI_RATE_1;
        Adapter->dfl_B_data_rate = RSI_RATE_11;
      }

#ifdef DYNAMIC_VARIABLES
      RSI_DEBUG(RSI_ZONE_INIT,"mgmt_init: Loading dynamic programming values\n");
#else      
      RSI_DEBUG(RSI_ZONE_INIT,"mgmt_init: Loading static programming\n");
#endif
      /* Load BB programming values */
      Status = ganges_load_BB_prog_vals(Adapter);
      if(Status == GANGES_STATUS_FAILURE)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "mgmt_init: ERROR while loading BB prog vals\n");
        break;
      }

      /* Load RF programming values */
      Status = ganges_load_RF_prog_vals(Adapter);
      if(Status == GANGES_STATUS_FAILURE)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "mgmt_init: ERROR while loading RF prog vals\n");
        break;
      }
      /* Load Channel programming values */
      Status = ganges_load_channel_prog_vals(Adapter);
      if(Status == GANGES_STATUS_FAILURE)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "mgmt_init: ERROR while loading Channel prog vals\n");
        break;
      }
      Status = ganges_load_power_save_params(Adapter);
      if(Status == GANGES_STATUS_FAILURE)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "mgmt_init: ERROR while loading Channel prog vals\n");
        break;
      }

      Status = ganges_load_RF_power_vals(Adapter);
      if(Status == GANGES_STATUS_FAILURE)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                  "mgmt_init: ERROR while loading RF power vals\n");
        break;
      }

      if(Adapter->RFType == RSI_RF_AL2236)
      {
          RPS_RATE_MCS7_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][0];
          RPS_RATE_MCS6_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][1];
          RPS_RATE_MCS5_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][2];
          RPS_RATE_MCS4_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][3];
          RPS_RATE_MCS3_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][4];
          RPS_RATE_MCS2_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][5];
          RPS_RATE_MCS1_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][6];
          RPS_RATE_MCS0_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][7];
          RPS_RATE_54_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][8];
          RPS_RATE_48_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][9];
          RPS_RATE_36_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][10];
          RPS_RATE_24_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][11];
          RPS_RATE_18_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][12];
          RPS_RATE_12_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][13];
          RPS_RATE_11_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][14];
          RPS_RATE_09_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][15];
          RPS_RATE_06_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][16];
          RPS_RATE_5_5_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][17];
          RPS_RATE_02_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][18];
          RPS_RATE_01_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][19];
          RPS_RATE_00_PWR = 0; 
      }

    } /* End of B/G proto type */
    if(Adapter->ProtocolType == ProtoType802_11A )
    {
      Status = GANGES_STATUS_FAILURE;
    }
    ganges_set_txrate(Adapter,Adapter->TxRate); /*FIXME*/
  }while(FALSE);
  return Status;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_reset_power_vals
Description    : This function reinitializes the Power vals
Returned Value : None
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |     |     |  X  | pointer to our adapter
*END**************************************************************************/

VOID
ganges_reset_power_vals
  (
    PRSI_ADAPTER Adapter,
    UINT16       channel
  )
{
  UINT16 channel_set;

  channel_set = channel / 4;

  if(channel == 4)
  {
    channel_set = 0;
  }

  RSI_DEBUG(RSI_ZONE_INFO,
            "ganges_reset_power_vals: Assigning Channel set %x to the channel num %x\n",channel_set,channel);
#ifdef EEPROM_READ_POWER_VALS

  switch(Adapter->power_mode)
  {
    case RSI_PWR_LOW:
    {
      RSI_DEBUG(RSI_ZONE_INFO,(TEXT("Low Power Mode Found\n")));
      RPS_RATE_MCS7_PWR  = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_MCS6_PWR  = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_MCS5_PWR  = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_MCS4_PWR  = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_MCS3_PWR  = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_MCS2_PWR  = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_MCS1_PWR  = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_MCS0_PWR  = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_54_PWR    = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_48_PWR    = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_36_PWR    = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_24_PWR    = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_18_PWR    = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_12_PWR    = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_11_PWR    = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_09_PWR    = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_06_PWR    = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_5_5_PWR   = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_02_PWR    = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_01_PWR    = (UINT8)Adapter->power_set[channel_set].low ;
      RPS_RATE_00_PWR    = 0;

    }
    break;

    case RSI_PWR_MEDIUM:
    {
      RSI_DEBUG(RSI_ZONE_INFO,(TEXT("Medium Power Mode Found\n")));
      RPS_RATE_MCS7_PWR  = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_MCS6_PWR  = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_MCS5_PWR  = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_MCS4_PWR  = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_MCS3_PWR  = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_MCS2_PWR  = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_MCS1_PWR  = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_MCS0_PWR  = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_54_PWR    = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_48_PWR    = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_36_PWR    = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_24_PWR    = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_18_PWR    = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_12_PWR    = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_11_PWR    = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_09_PWR    = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_06_PWR    = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_5_5_PWR   = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_02_PWR    = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_01_PWR    = (UINT8)Adapter->power_set[channel_set].medium ;
      RPS_RATE_00_PWR    = 0;

    }
    break;
    case RSI_PWR_HIGH:
    {
        RPS_RATE_MCS7_PWR  = (UINT8)Adapter->power_set[channel_set].high[2];
        RPS_RATE_MCS6_PWR  = (UINT8)Adapter->power_set[channel_set].high[2];
        RPS_RATE_MCS5_PWR  = (UINT8)Adapter->power_set[channel_set].high[2];
        RPS_RATE_MCS4_PWR  = (UINT8)Adapter->power_set[channel_set].high[2];
        RPS_RATE_MCS3_PWR  = (UINT8)Adapter->power_set[channel_set].high[1];
        RPS_RATE_MCS2_PWR  = (UINT8)Adapter->power_set[channel_set].high[1];
        RPS_RATE_MCS1_PWR  = (UINT8)Adapter->power_set[channel_set].high[1];
        RPS_RATE_MCS0_PWR  = (UINT8)Adapter->power_set[channel_set].high[0];
        RPS_RATE_54_PWR    = (UINT8)Adapter->power_set[channel_set].high[2];
        RPS_RATE_48_PWR    = (UINT8)Adapter->power_set[channel_set].high[2];
        RPS_RATE_36_PWR    = (UINT8)Adapter->power_set[channel_set].high[1];
        RPS_RATE_24_PWR    = (UINT8)Adapter->power_set[channel_set].high[1];
        RPS_RATE_18_PWR    = (UINT8)Adapter->power_set[channel_set].high[1];
        RPS_RATE_12_PWR    = (UINT8)Adapter->power_set[channel_set].high[1];
        RPS_RATE_11_PWR    = (UINT8)Adapter->power_set[channel_set].high[0];
        RPS_RATE_09_PWR    = (UINT8)Adapter->power_set[channel_set].high[1];
        RPS_RATE_06_PWR    = (UINT8)Adapter->power_set[channel_set].high[1];
        RPS_RATE_5_5_PWR   = (UINT8)Adapter->power_set[channel_set].high[0];
        RPS_RATE_02_PWR    = (UINT8)Adapter->power_set[channel_set].high[0];
        RPS_RATE_01_PWR    = (UINT8)Adapter->power_set[channel_set].high[0];
        RPS_RATE_00_PWR    = 0;
    }
    break;
    default:
    {
      RSI_DEBUG(RSI_ZONE_INFO,(TEXT("Unknown Mode Found\n")));
    }
    break;

  }

#else

  RPS_RATE_MCS7_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][0];
  RPS_RATE_MCS6_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][1];
  RPS_RATE_MCS5_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][2];
  RPS_RATE_MCS4_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][3];
  RPS_RATE_MCS3_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][4];
  RPS_RATE_MCS2_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][5];
  RPS_RATE_MCS1_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][6];
  RPS_RATE_MCS0_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][7];
  RPS_RATE_54_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][8];
  RPS_RATE_48_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][9];
  RPS_RATE_36_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][10];
  RPS_RATE_24_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][11];
  RPS_RATE_18_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][12];
  RPS_RATE_12_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][13];
  RPS_RATE_11_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][14];
  RPS_RATE_09_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][15];
  RPS_RATE_06_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][16];
  RPS_RATE_5_5_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][17];
  RPS_RATE_02_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][18];
  RPS_RATE_01_PWR = Adapter->power_val_set[Adapter->selected_pwr_val_set][19];
  RPS_RATE_00_PWR = 0;
#endif

  return;
}


                                                                                                                                        
/*FUNCTION*********************************************************************
Function Name  : ganges_RF_program
Description    : This function program the BB and RF
Returned Value : None
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |     |     |  X  | pointer to our adapter
*END**************************************************************************/

VOID
ganges_RF_program
  (
    IN PRSI_ADAPTER Adapter
  )
{
#if 0 /* TODO while testing look into this */
  /*FIXME we should care about RF type */
  /* For AIROHA RF we need to Reset the Mac before RF program*/
  ganges_send_reset_request(Adapter,AIROHA_BB_VAL_SIZE, Adapter->BBProgramVal);
  ganges_sendRFProgRequest(Adapter, RSI_RF_AIROHA, RF_AIROHA_VALUES_SIZE );
  /* Reset the Mac */
  ganges_send_reset_request(Adapter, AIROHA_BB_VAL_SIZE, Adapter->BBProgramVal);
#endif
}

/*FUNCTION*********************************************************************
Function Name  : ganges_sendRFProgRequest
Description    : This function prepare RF Programming request frame and set it
                 MAC.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |     |     |  X  | pointer to our adapter
rf_type		 |  X  |     |     | RF type
no_vals		 |  X  |     |     | Programming values length
rf_vals		 |  X  |     |     | RF programming values
*END**************************************************************************/

GANGES_STATUS 
ganges_sendRFProgRequest
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT16       rf_type,
    IN UINT16       no_vals,
    IN UINT16       *rf_vals
  )
{
  struct RSI_Mac_frame mgmt;
  
  RSI_DEBUG(RSI_ZONE_INIT, 
             "sendRFProgRequest: Sending RFProgRequest %d\n",no_vals);

  ganges_memset(mgmt.descriptor, 0, 16);

  mgmt.descriptor[0] = MGMT_DESC_TYP_RF_PROG_REQ | ((no_vals + 2) * 2 );
  mgmt.frameType.rf_prog.RFtype  = rf_type;
  mgmt.frameType.rf_prog.length  = (UINT16)no_vals;

  ganges_memcpy(mgmt.frameType.rf_prog.channelProgrameVal, 
                rf_vals, 
                no_vals*2);

  return ganges_send_mgmt_frame(Adapter, 
                            (UINT8*)&mgmt, 
                            (RSI_DESC_LEN + (no_vals + 2) *2 ));
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_reset_request
Description    : Send reset managment frame to to hardware
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |  X  |     |     | pointer to our adapter
no_bb_values	 |  X  |     |     | Length of BB programming values
bb_values	 |  X  |     |     | BB programming values
*END**************************************************************************/

GANGES_STATUS 
ganges_send_reset_request 
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT16       no_bb_values,
    IN UINT16       *bb_values
  )
                        
{
  UINT16      frame_length;  
  struct      RSI_Mac_frame mgmt;
  
  RSI_DEBUG(RSI_ZONE_INIT,"ganges_sendreset_request: Sending reset frame\n");
  ganges_memset(mgmt.descriptor,0,16);

  /* frame length phy val length + phy values + AFE len + AFE vals */
  frame_length = 2 + (no_bb_values *2)+ 2 + 2; 

  mgmt.descriptor[0] = MGMT_DESC_TYP_RESET_REQ | 
                       frame_length;
  mgmt.frameType.reset.bb_val_length = no_bb_values;
  ganges_memcpy(mgmt.frameType.reset.bb_program_val, 
                bb_values, 
                no_bb_values*2);
  mgmt.frameType.reset.bb_program_val[no_bb_values] = 1;
  mgmt.frameType.reset.bb_program_val[no_bb_values+1] = 0;
  return ganges_send_mgmt_frame(Adapter, 
                            (UINT8*)&mgmt, 
                            MGMT_FRAME_DESC_SZ +frame_length);
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_scanrequest
Description    : This function sends scan request to the hardware
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |  X  |     |     | pointer to our adapter
channelNum       |  X  |     |     | On which channel we want scan
*END**************************************************************************/

GANGES_STATUS
ganges_send_scanrequest
  (
    IN PRSI_ADAPTER Adapter, 
    IN UINT32       channelNum
  )
{
  struct RSI_Mac_frame mgmt;
  
  channelNum = channelNum;
         
  ganges_memset(mgmt.descriptor, 0, 16);

  mgmt.descriptor[0] = MGMT_DESC_TYP_SCAN_REQ | (sizeof(mgmt.frameType.scan));
  
  if(Adapter->NetworkType == Ndis802_11Infrastructure)
  {
    mgmt.descriptor[1] = 1;
  }
  else
  {
    /* IBSS */
    mgmt.descriptor[1] = 0;
  }
#ifdef GANGES_PER_MODE
  mgmt.descriptor[2] = Adapter->mac_stats_timer_intrvl;

  if(Adapter->ppe_per_mode)
  {
    mgmt.descriptor[3] = 0x1;
  }
#endif

  mgmt.frameType.scan.minChannelTime = Adapter->minChannelTime;
  mgmt.frameType.scan.maxChannelTime = Adapter->maxChannelTime;

  mgmt.frameType.scan.channel_val_len = 6;
  /* Overwriting with particular channel values */
  ganges_memcpy((mgmt.frameType.scan.channelNumProgVal), 
                &Adapter->rsi_channel_vals[channelNum], 
                12);

  RSI_DEBUG(RSI_ZONE_INFO,"\nganges_send_scanrequest:sending Scanrequest chnl Num %d\n",channelNum);
  /* Our Scan.request is ready */
  return ganges_send_mgmt_frame(Adapter, 
                               (UINT8*)&mgmt, 
                               RSI_DESC_LEN + 18
                               );
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_join_request
Description    : This function send scan request to the hardware
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |  X  |     |     | pointer to our adapter
scanConfirm      |  X  |     |     | This array would contain all the 
                                     information about BSSid received in sacn
*END**************************************************************************/

GANGES_STATUS 
ganges_send_join_request
  (
    IN PRSI_ADAPTER Adapter, 
    IN PScanConfirm scanConfirm
  )
{
  UINT32                 i;
  struct RSI_Mac_frame mgmt;

  ganges_memset(mgmt.descriptor, 0, sizeof(struct RSI_Mac_frame));
  mgmt.descriptor[0] = MGMT_DESC_TYP_JOIN_REQ|
                       sizeof(mgmt.frameType.join);
  if(Adapter->NetworkType == Ndis802_11Infrastructure)
  {
    mgmt.descriptor[1] = 1;
  }
  else
  {
    mgmt.descriptor[1] = 0;
    if(Adapter->IbssCreator == IBSS_CREATOR)
    {
      RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_join_request: IBSS Creator\n");
      mgmt.descriptor[3] = 0;
    }
    else
    {
      RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_join_request: IBSS Joiner\n");
      mgmt.descriptor[3] = 1;
    }
  }
  /* JOIN failure time out*/
  mgmt.descriptor[2] = 600;

  mgmt.frameType.join.dfl_tx_data_rate   = Adapter->dataRate;
  mgmt.frameType.join.cwin_min           = CWIN_MIN;
  /* Copy BSSID */
  if(Adapter->NetworkType == Ndis802_11IBSS)
  {
    ganges_memcpy(mgmt.frameType.join.bssid, Adapter->Ibss_info.BSSID, 6);
    RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_join_request: becon intrvl %d\n",
               Adapter->Ibss_info.Beacon);
    mgmt.frameType.join.beacon_intr = Adapter->Ibss_info.Beacon;
    mgmt.frameType.join.op_parms    = OPERATIONAL_PARMS|RSI_SHORT_SLOT_TIME;

    if(Adapter->ProtocolType == ProtoType802_11B)
    {
      mgmt.frameType.join.cwin_min           = CWIN_MIN_B;
      if((Adapter->dataRate&0x00FF) > RSI_RATE_11)
      {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_join: setting to B only rate \n");
        /* AP in B only mode but our data rate is G rate, set to B rate */
        mgmt.frameType.join.dfl_tx_data_rate   = RSI_RATE_11|(RPS_RATE_11_PWR<<8);
        Adapter->TxRate = RATE_11_MBPS;
      }
    }
  }
  else
  {
    ganges_memcpy(mgmt.frameType.join.bssid, scanConfirm->BSSID, 6);
    mgmt.frameType.join.beacon_intr = scanConfirm->Beacon;
    mgmt.frameType.join.op_parms    = OPERATIONAL_PARMS;
    if(scanConfirm->ProtocolType == ProtoType802_11B)
    {
      mgmt.frameType.join.cwin_min           = CWIN_MIN_B;
      if((Adapter->dataRate & 0x00FF) > RSI_RATE_11)
      {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_join: setting to B only rate\n");
        /* AP in B only mode but our data rate is G rate, set to B rate */
        mgmt.frameType.join.dfl_tx_data_rate   = RSI_RATE_11|(RPS_RATE_11_PWR<<8);
        Adapter->TxRate = RATE_11_MBPS;
      }
    }
  }

  mgmt.frameType.join.dfl_tx_mgmt_rate     = Adapter->dfl_mgmt_rate;
  mgmt.frameType.join.dfl_tx_data_B_rate   = Adapter->dfl_B_data_rate;
  mgmt.frameType.join.max_no_ICV_err       = MAX_NUM_ICV_ERRORS;
  mgmt.frameType.join.max_msdu_len         = MAX_MSDU_LEN;
  mgmt.frameType.join.max_msdu_tx_lifetime = MAX_MSDU_TX_LIFETIME;
  mgmt.frameType.join.max_msdu_rx_lifetime = MAX_MSDU_RX_LIFETIME;
  mgmt.frameType.join.cwin_max             = CWIN_MAX;
  mgmt.frameType.join.dfl_frag_thresh      = (UINT16)Adapter->frag_threshold;
  mgmt.frameType.join.dfl_rts_thresh       = (UINT16)Adapter->rts_threshold;
  mgmt.frameType.join.max_listen_intr      = MAX_LISTEN_INTRVL;
  mgmt.frameType.join.short_retry_limit    = SHORT_RTRY_LIMIT;
  mgmt.frameType.join.long_retry_limit     = LONG_RTRY_LIMIT;
  
  mgmt.frameType.join.snap[0]              = 0xAAAA;
  mgmt.frameType.join.snap[1]              = 0x0003;
  mgmt.frameType.join.snap[2]              = 0x0000;
  mgmt.frameType.join.max_inactive         = 20;

  /* If number of values changed structure should be changed */
  for(i=0;i<8;i++)
  {
    mgmt.frameType.join.timing_param[i] = macTimingValuesFreq_44[i];
  }
  mgmt.frameType.join.dfl_tx_power = 55;
  ganges_memcpy(mgmt.frameType.join.mac_addr, 
                Adapter->PermanentAddress, 
                ETH_ADDR_LEN);
  
  /*Beacon header duration */
  switch(Adapter->dfl_mgmt_rate)
  {
    case RSI_RATE_1: /*1mbs*/
      mgmt.frameType.join.bcn_hdr_duration = 384+2;
      break;
    case RSI_RATE_2: /*2mbs*/
      mgmt.frameType.join.bcn_hdr_duration = 288+2;
      break;
    case RSI_RATE_5_5:/*5.5*/
      mgmt.frameType.join.bcn_hdr_duration = 227+2;
      break;
    case RSI_RATE_11:/*11*/
      mgmt.frameType.join.bcn_hdr_duration = 210+2;
      break;
    case RSI_RATE_6:/*6*/
      mgmt.frameType.join.bcn_hdr_duration = 52+8;
      break;
    case RSI_RATE_9:/*9*/
      mgmt.frameType.join.bcn_hdr_duration = 62+8;
      break;
    case RSI_RATE_12:/*12*/
      mgmt.frameType.join.bcn_hdr_duration = 36+8;
      break;
    case RSI_RATE_18:/*18*/
      mgmt.frameType.join.bcn_hdr_duration = 33+8;
      break;
    case RSI_RATE_24:/*24*/
      mgmt.frameType.join.bcn_hdr_duration = 28+8;
      break;
    case RSI_RATE_36:/*36*/
      mgmt.frameType.join.bcn_hdr_duration = 26+8;
      break;
    case RSI_RATE_48:/*48*/
      mgmt.frameType.join.bcn_hdr_duration = 24+8;
      break;
    case RSI_RATE_54:/*54*/
      mgmt.frameType.join.bcn_hdr_duration = 24+8;
      break;
    default: /* 1*/
      mgmt.frameType.join.bcn_hdr_duration = 384+2;
      break;
  }
  
  /* Overwriting with particular channel values*/
  mgmt.frameType.join.chnl_prog_val_len = 6;
  if(Adapter->ProtocolType == ProtoType802_11A )
  {
    if(Adapter->NetworkType == Ndis802_11IBSS) 
    {  
      ganges_memcpy(&mgmt.frameType.join.chnl_prog_val[0], 
                    &Adapter->rsi_channel_vals[(Adapter->Ibss_info.channelNum - 32)/4],
                    12);

    }
    else
    {        
      ganges_memcpy(&mgmt.frameType.join.chnl_prog_val[0], 
                    &Adapter->rsi_channel_vals[(scanConfirm->PhyParam[0]-32)/4],
                    12);
    }  
  }
  else
  {
    if(Adapter->NetworkType == Ndis802_11IBSS) 
    {
      RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_join_request: Channel Number %d\n",
                              Adapter->Ibss_info.channelNum);
      ganges_memcpy(&mgmt.frameType.join.chnl_prog_val[0],
                    &Adapter->rsi_channel_vals[Adapter->Ibss_info.channelNum], 
                    12);

    }
    else
    {
      RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_join_request: Channel Number %d\n",
                              scanConfirm->PhyParam[0]);
      ganges_memcpy(&mgmt.frameType.join.chnl_prog_val[0], 
                    &Adapter->rsi_channel_vals[(scanConfirm->PhyParam[0])],
                    12);
    }
  }  
  /*we have join request frame ready. send it*/
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_join_request: Final length of join request is -> %lu\n", 
                    sizeof(mgmt.frameType.join));

  return ganges_send_mgmt_frame(Adapter, 
                            (UINT8 *)&mgmt, 
                            RSI_DESC_LEN + sizeof(mgmt.frameType.join));
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_auth_request
Description    : This function send authentication request to the hardware
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter            |  X  |     |     | pointer to our adapter
peerStationAddress |  X  |     |     | Address of the peer station with which 
                                       we are trying to authenticate.
authenticationType |  X  |     |     | Type of authentication required (Open 
                                       system/Shared system)
*END**************************************************************************/

GANGES_STATUS
ganges_send_auth_request
  (
    IN PRSI_ADAPTER       Adapter, 
    IN UINT8              peerStationAddress[],
    IN UINT16             authenticationType
  )
{
  struct RSI_Mac_frame mgmt;

  ganges_memset(mgmt.descriptor,0, 16);

  mgmt.descriptor[0] = MGMT_DESC_TYP_AUTH_REQ;
  /* zeroth location of descriptor contains frame type and length. */
  mgmt.descriptor[0]|= sizeof(mgmt.frameType.auth);  
  mgmt.descriptor[1] = 0;
  ganges_memcpy(&mgmt.descriptor[4], peerStationAddress, ETH_ADDR_LEN);



  if(authenticationType == Ndis802_11AuthModeShared)
  {
    mgmt.frameType.auth.authAlgoID = AUTH_ALG_SHARED;     /*Enable WEP */
  }
  else
  {
    mgmt.frameType.auth.authAlgoID = AUTH_ALG_OPEN;       /* Open system */
  }

  mgmt.frameType.auth.authSeqNum = AUTH_SEQ_NUM;
  mgmt.frameType.auth.statusCode = FIRMWARE_DEFINED;



  return ganges_send_mgmt_frame(Adapter, 
                            (UINT8 *)&mgmt, 
                            MGMT_FRAME_DESC_SZ+sizeof(mgmt.frameType.auth));
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_Associate_request
Description    : This function sends association request frame to the hardware
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter            |  X  |     |     | pointer to our adapter
peerStationAddress |  X  |     |     | 
ScanConfirm        |  X  |     |     | The structure that contains the 
                                       information about the BSS with which we 
                                       are trying to associate.
*END**************************************************************************/

GANGES_STATUS 
ganges_send_Associate_request
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT8        peerStationAddress[],
    IN PScanConfirm scanConfirm
  )
{
  UINT8  buffer[256];
  UINT8  *temp;
  UINT8  temp_ssid[33];
  struct RSI_Mac_frame *mgmt;
  wmm_info_t wmm_ie;

  mgmt = (struct RSI_Mac_frame*)buffer;
  ganges_memset(mgmt->descriptor, 0, 16);


  ganges_memcpy(&mgmt->descriptor[4], peerStationAddress, ETH_ADDR_LEN);

  mgmt->frameType.assoc.capabilityInfo = RSI_CAPABILITY_INFO;
  mgmt->frameType.assoc.capabilityInfo |=
                          (scanConfirm->CapabilityInfo & 0x0010);

  if(Adapter->slot_type == RSI_SLOT_AUTO)
  { /* slot mode is auto, copy AP slot mode */
    RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_Association_request: Auto slot\n");
    mgmt->frameType.assoc.capabilityInfo |= 
                         (scanConfirm->CapabilityInfo & RSI_SHORT_SLOT_TIME);
  }

  if(Adapter->preamble_type == RSI_PREAMBLE_AUTO)
  {
    RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_Association_request: Auto preamble\n");
    /*mgmt->frameType.assoc.capabilityInfo |= 
                         (scanConfirm->CapabilityInfo & RSI_SHORT_PREAMBLE);*/
    /*FIXME for testing we are setting to short mode  */
    mgmt->frameType.assoc.capabilityInfo |= 
                         RSI_SHORT_PREAMBLE;
  }
  mgmt->frameType.assoc.listenInterval = RSI_LISTEN_INTERVAL;

  /*filling the variable length items */
  temp = &mgmt->frameType.assoc.variable[0];

  *temp++ = RSI_SSID_EID;
  *temp++ = scanConfirm->SSIDLen;

  ganges_memcpy(temp, scanConfirm->SSID, scanConfirm->SSIDLen);
  ganges_memcpy(temp_ssid, scanConfirm->SSID, scanConfirm->SSIDLen);

  temp_ssid[scanConfirm->SSIDLen] = 0;

  RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_Association_request: to SSID %s of SSID Length %d\n",
                                    temp_ssid, (UINT16)scanConfirm->SSIDLen);

  temp += scanConfirm->SSIDLen;

  *temp++ = RSI_SUPPORTED_RATES_EID;


  if((scanConfirm->ProtocolType == ProtoType802_11B  ) ||
     (Adapter->ProtocolType == ProtoType802_11B))
  {
    RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_Association_request: Sending 11b basic rate set\n");	

    *temp++ = 0x04;         /*Supported rates length*/

    *temp++ = 0x82;     /* 1 */
    *temp++ = 0x84;     /* 2 */
    *temp++ = 0x8B;     /* 5.5 */
    *temp++ = 0x96;     /* 11 */
  }
  else if(scanConfirm->ProtocolType == ProtoType802_11G)
  {
    RSI_DEBUG(RSI_ZONE_INFO,
              "ganges_send_Association_request: Sending 11g basic & extended rate set\n");	
    *temp++ = 0x08;         /* Supported rates length */

    *temp++ = 0x82;         /* 1 */
    *temp++ = 0x84;         /* 2 */
    *temp++ = 0x8B;         /* 5.5*/
    *temp++ = 0x96;         /* 11*/
    *temp++ = 0x24;         /* 18 */
    *temp++ = 0x30;         /* 24 */
    *temp++ = 0x48;         /* 36 */
    *temp++ = 0x6c;         /* 54 */

    *temp++ = 0x32;         /*Extended Rates */
    *temp++ = 0x04;   
    *temp++ = 0x0c;         /* 6 */
    *temp++ = 0x12;         /* 9 */
    *temp++ = 0x18;         /* 12 */
    *temp++ = 0x60;         /* 48 */
    
  }

  if(scanConfirm->ProtocolType == ProtoType802_11A )
  {

    RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_Association_request: Sending 11a basic rate set\n");	
    *temp++ = 0x08;         /* Supported rates length */

    /* *temp++ = 0x82; */
    /* *temp++ = 0x84;   */    /*6Mbps */
    /* *temp++ = 0x8B;
       *temp++ = 0x96; */
    *temp++ = 0x24;
    *temp++ = 0x30;
    *temp++ = 0x48;
    *temp++ = 0x6c;
    /**temp++ = 0x32;
      *temp++ = 0x04; */
    *temp++ = 0x8c;
    *temp++ = 0x12;
    *temp++ = 0x18;
    *temp++ = 0x60;
  }
  if(scanConfirm->wmm_capable)
  {
    RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_Association_request: Inserting wmm element\n");
    wmm_ie.eId        = 0xdd;
    wmm_ie.length     = 7;
    wmm_ie.oui[0]     = 0x00;
    wmm_ie.oui[1]     = 0x50;
    wmm_ie.oui[2]     = 0xf2;
    wmm_ie.ouiType    = 0x02;
    wmm_ie.ouiSubType = 0x00;
    wmm_ie.version    = 1;
    //wmm_ie.acInfo     = 0x00;
    /* Add the wmm Power save features if the AP supports */
    if( scanConfirm->wmmPS_capable)
    {
      RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_Association_request: Idicating wmmps\n");
      wmm_ie.acInfo = ( ( Adapter->pwr_save_param.ac_vo |
                        ( Adapter->pwr_save_param.ac_vi << 1) |
                        ( Adapter->pwr_save_param.ac_bk << 2) |
                        ( Adapter->pwr_save_param.ac_be << 3)));
    }
    else
    {
      wmm_ie.acInfo = 0x00;
    }

    ganges_memcpy(temp,&wmm_ie,sizeof(wmm_info_t));
    temp += sizeof(wmm_info_t);
  }
 
  /* Insert RSN element */
  if((Adapter->authMode == Ndis802_11AuthModeWPA2PSK) ||
    (Adapter->authMode == Ndis802_11AuthModeWPA2))
  {
    UINT8 rsn_ass_req[] ={0x30, 0x14, 0x01, 0x00, 
                           0x00, 0x0f, 0xAC, 0x04, /* group key cipher */
                           0x01, 0x00, 
                           0x00, 0x0F, 0xAC, 0x04, /* pairwise key cipher */
                           0x01, 0x00, 
                           0x00, 0x0F, 0xAC, 0x02,  /* Auth key mgmt */
                           0x00, 0x00,              /* RSN capabilities */
                           0x00, 0x00,              /* PMKID count */
                           0x00, 0x00, 0x00, 0x00,  /* PMKID list */
                           0x00, 0x00, 0x00, 0x00, 
                           0x00, 0x00, 0x00, 0x00, 
                           0x00, 0x00, 0x00, 0x00};

     
    /* FIXME Instead of depending index extract the AP capabilities */
    /* Select the group cipher based on the beacon info RSN */
    RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_Associate_request: Inserting WPA2 IE\n");
    rsn_ass_req[7] = scanConfirm->rsn_ie[7];

    /* Select the pair cipher default is CCMP*/
    if(Adapter->encryptAlgo == Ndis802_11Encryption2Enabled)
    {
      /* select TKIP */ 
      rsn_ass_req[13] = 0x02;
      /* if group cipher is CCMP fail this case 
       * Association should not happen*/
      if(rsn_ass_req[7] == 0x04)
      {
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_Association_request: Invalid combination G-CCMP P-TKIP\n");
        return GANGES_STATUS_FAILURE;
      }
    }
    if(Adapter->authMode == Ndis802_11AuthModeWPA2)
    {
      /* 802.1x authentication */
      rsn_ass_req[19] = 0x01;
      /* Check for PMK cache */
      if(Adapter->pmkid_count)
      {
	UINT32 ii;
        RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_Association_request: PMKSA available...searching\n");
        for(ii=0; ii< Adapter->pmkid_count; ii++)
        {
          /* check cache is avialable for this BSSID */
          if(ganges_memcmp(peerStationAddress, Adapter->pmkid_info[ii].BSSID, 6) == 0)
	  {
            RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_Association_request: Inserting PMKID\n");
            rsn_ass_req[22] = 0x01;
	    ganges_memcpy(&rsn_ass_req[24], 
                          Adapter->pmkid_info[ii].PMKID, 
                          sizeof(NDIS_802_11_PMKID_VALUE));
	    rsn_ass_req[1] += 18;
            ganges_dump(RSI_ZONE_INFO,&rsn_ass_req[24],16);
	    break;
	  }
        }
      }
      else
      {
	RSI_DEBUG(RSI_ZONE_INFO,
                  "ganges_send_Associate_request: Adapter's PMK Cache is presently empty\n");
      }	 		
    }

    ganges_memcpy(temp, rsn_ass_req, rsn_ass_req[1]+2);
    temp += rsn_ass_req[1]+2;
    ganges_memcpy(scanConfirm->rsn_ass_req, rsn_ass_req, rsn_ass_req[1]+2);
  }
  else if((Adapter->authMode == Ndis802_11AuthModeWPAPSK) ||
         (Adapter->authMode == Ndis802_11AuthModeWPA))
  {
    RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_Associate_request: Inserting WPA IE\n");
    if(Adapter->wpa_ie_len)
    { 
      ganges_memcpy(temp, 
                    Adapter->wpa_ie,
                    Adapter->wpa_ie_len);
      temp +=Adapter->wpa_ie_len;    
    }
  }

  /* Added for HT support */
  if(Adapter->ProtocolType==ProtoType802_11N)
  {
    if(scanConfirm->ht_capable)
    {
      RSI_DEBUG(RSI_ZONE_INFO,
                "ganges_send_Associate_request: Inserting HT Capable IE\n");
      ganges_memcpy(temp,
                    scanConfirm->htCap_ie,
                    scanConfirm->htCap_ie[1] + 2);

      temp += scanConfirm->htCap_ie[1] + 2;
    }

    if(scanConfirm->ht_capable1)
    {
      RSI_DEBUG(RSI_ZONE_INFO,
                "ganges_send_Associate_request: Inserting HT Capable1 IE\n");
      ganges_memcpy(temp,
                    scanConfirm->htCap1_ie,
                    scanConfirm->htCap1_ie[1] + 2);

      temp += scanConfirm->htCap1_ie[1] + 2;
    }

    if(scanConfirm->capable_11n)
    {
      RSI_DEBUG(RSI_ZONE_INFO,
                "ganges_send_Associate_request: Inserting 11n Capable IE\n");
      ganges_memcpy(temp,
                    &scanConfirm->ie_11n,
                    scanConfirm->ie_11n.eid_len + 2);
      temp += (scanConfirm->ie_11n.eid_len + 2);
    }
  }
  
  mgmt->descriptor[0] = MGMT_DESC_TYP_ASSOCIAT_REQ|(temp - (UINT8*)mgmt - 16); 

  return ganges_send_mgmt_frame(Adapter, (UINT8*)mgmt, (temp - (UINT8 *)mgmt));

}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_disassociate_request
Description    : This function send disAssociate request frame to the hardware
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter            |  X  |     |     | pointer to our adapter
peerStationAddress |  X  |     |     | Address of the peer station which we are
                                       trying to disassociate
reasonCode         |  X  |     |     | The reason code for our disassociation
*END**************************************************************************/

GANGES_STATUS 
ganges_send_disassociate_request
  (
    IN PRSI_ADAPTER Adapter, 
    IN UINT8 peerStationAddress[], 
    IN UINT16 reasonCode
  )
{
  struct RSI_Mac_frame mgmt;

  ganges_memset(mgmt.descriptor,0,16);

  mgmt.descriptor[0] = MGMT_DESC_TYP_DISASSOCIAT_REQ;
  mgmt.descriptor[0]|= sizeof(mgmt.frameType.disassoc); 

  ganges_memcpy(&mgmt.descriptor[4], peerStationAddress, ETH_ADDR_LEN);

  /*Sending station is leaving the BSS */
  mgmt.frameType.disassoc.reasonCode = reasonCode;
   
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_disassociate_request: Sending dissociate frame\n");
  /* SND FRAME */
  return ganges_send_mgmt_frame(Adapter, 
                            (UINT8 *)&mgmt, 
                            MGMT_FRAME_DESC_SZ+sizeof(mgmt.frameType.disassoc));

}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_DeAuthentication_request
Description    : This function send disAssociate request frame to the hardware
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter            |  X  |     |     | pointer to our adapter
peerStationAddress |  X  |     |     | Address of the peer station which we are
                                       trying to deauthenticat
*END**************************************************************************/

GANGES_STATUS
ganges_send_DeAuthentication_request
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT8  peerStationAddress[],
    IN UINT16 reason_code
  )
{
  struct RSI_Mac_frame mgmt;

  ganges_memset(mgmt.descriptor,0,16);

  mgmt.descriptor[0] = MGMT_DESC_TYP_DEAUTH_REQ;
  mgmt.descriptor[0]|= sizeof(mgmt.frameType.deauth); 

        
  ganges_memcpy(&mgmt.descriptor[4], peerStationAddress, ETH_ADDR_LEN);

  /*Sending station is leaving the BSS */
  mgmt.frameType.disassoc.reasonCode = reason_code;
  RSI_DEBUG(RSI_ZONE_INFO,
     "ganges_send_DeAuthentication_request: Sending deauthentication frame\n");
  /* SND FRAME */
  return ganges_send_mgmt_frame(Adapter, 
                            (UINT8*)&mgmt, 
                            MGMT_FRAME_DESC_SZ+sizeof(mgmt.frameType.deauth));

}


/*FUNCTION*********************************************************************
Function Name  : ganges_send_WEPKeys_request
Description    : This routine prepare and send WEP keys request to firmware
Returned Value : Returns 0 in case of success or a negative value incase of failure.
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     |Pointer to our adapter
LWEPKeysInfo     |  X  |     |     |The array that actually contains the key 
                                    to be used
LWEPKeysLength   |  X  |     |     |The length of the key to be used
LWEPKeysIndex    |  X  |     |     |The index of the key to be used
mac_addr	 |  X  |     |     |MAC add
group_key        |  X  |     |     |Group key
*END**************************************************************************/

GANGES_STATUS 
ganges_send_WEPKeys_request
  (
    IN PRSI_ADAPTER Adapter, 
    IN UINT8 LWEPKeysInfo_org[],
    IN UINT32 LWEPKeyLength,
    IN UINT32 LWEPKeyIndex,
    IN UINT8 mac_addr[],
    IN UINT8 group_key
  )
{
  struct      RSI_Mac_frame mgmt;
  UINT32 ii;
  UINT8 LWEPKeysInfo[26];

  ganges_memcpy(LWEPKeysInfo, LWEPKeysInfo_org, LWEPKeyLength);
  ganges_memset(mgmt.descriptor, 0,
                 RSI_DESC_LEN + sizeof(mgmt.frameType.wep_key.wep_128));
  if(LWEPKeyLength == 5 || LWEPKeyLength == 10)
  {
    mgmt.descriptor[0] = MGMT_DESC_TYP_SET_KEYS_REQ | 
                       (sizeof(mgmt.frameType.wep_key.wep_64));
  }
  else
  {
    mgmt.descriptor[0] = MGMT_DESC_TYP_SET_KEYS_REQ | 
                       (sizeof(mgmt.frameType.wep_key.wep_128));
  }  
       
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_WEPKeys_request: Index: %d\n", LWEPKeyIndex);
  /* If group keys frame set this bit */
  if(group_key) 
  {
    mgmt.descriptor[1] = 1; 
  }
  if(Adapter->NetworkType == Ndis802_11Infrastructure)
  {
    ganges_memcpy((INT8*) &mgmt.descriptor[4],
                  &Adapter->scanConfirmVarArray[Adapter->SelectedBssidNum]->BSSID,
                  sizeof(NDIS_802_11_MAC_ADDRESS));
  }
  else
  {
    ganges_memcpy((INT8 *) &mgmt.descriptor[4],
                  mac_addr,
                  sizeof(NDIS_802_11_MAC_ADDRESS));
  }
  
  if(LWEPKeyLength == 5)
  {
          
    mgmt.frameType.wep_key.wep_64.keyDesc =
            (UINT16)(((UINT16)LWEPKeyIndex<<14) | RPS_WEP_64 |
                     RPS_PROTECT_BROAD_CAST_DATA);
    ganges_memcpy(mgmt.frameType.wep_key.wep_64.key, 
                  LWEPKeysInfo, 
                  5);

  }
  else if(LWEPKeyLength == 10)
  {
    INT32 xx,jj=0; 
    UINT8 byte1, byte2;
    
    mgmt.frameType.wep_key.wep_64.keyDesc =
            (UINT16)(((UINT16)LWEPKeyIndex<<14) | RPS_WEP_64 |
                     RPS_PROTECT_BROAD_CAST_DATA);
    
    for(xx=0;xx<10;xx+=2)
    {
      byte1 = LWEPKeysInfo[xx]-0x30;
      byte2 = LWEPKeysInfo[xx+1]-0x30;
      if(byte1 > 0x30)
      {
        byte1 -= 39;
      }
      else if(byte1 > 0x10)
      {
        byte1 -= 7;
      } 
      if(byte2 > 0x30)
      {
        byte2 -= 39;
      }
      else if(byte2 > 0x10)
      {
        byte2 -= 7;
      } 
      LWEPKeysInfo[jj++] = (byte1<<4) | byte2;
    }
    for(ii = 0; ii < 5; ii++)
    {
      RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_WEPKeys_request: %02x\n", LWEPKeysInfo[ii]);
    }

    ganges_memcpy(mgmt.frameType.wep_key.wep_64.key,LWEPKeysInfo, 5);

  }
  else if(LWEPKeyLength == 13)
  {

    mgmt.frameType.wep_key.wep_128.keyDesc =
            (UINT16)(((UINT16)LWEPKeyIndex<<14) | RPS_WEP_128 | 
                     RPS_PROTECT_BROAD_CAST_DATA);
    ganges_memcpy(mgmt.frameType.wep_key.wep_128.key,LWEPKeysInfo, 13);
  }
  else if(LWEPKeyLength == 26)
  {
    INT32 xx,jj=0; 
    UINT8 byte1, byte2;

    mgmt.frameType.wep_key.wep_128.keyDesc =
             (UINT16)(((UINT16)LWEPKeyIndex<<14) | RPS_WEP_128 |
                      RPS_PROTECT_BROAD_CAST_DATA);
             
    for(xx=0;xx<26;xx+=2)
    {
      byte1 = LWEPKeysInfo[xx]-0x30;
      byte2 = LWEPKeysInfo[xx+1]-0x30;
      if(byte1 > 0x30)
      {
        byte1 -= 39;
      }
      else if(byte1 > 0x10)
      {
        byte1 -= 7;
      } 
      if(byte2 > 0x30)
      {
        byte2 -= 39;
      }
      else if(byte2 > 0x10)
      {
        byte2 -= 7;
      } 
      LWEPKeysInfo[jj++] = (byte1<<4) | byte2;
    }
    for(ii = 0; ii < 13; ii++)
    {
      RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_WEPKeys_request: %02x\n", LWEPKeysInfo[ii]);
    }

    ganges_memcpy(mgmt.frameType.wep_key.wep_128.key,LWEPKeysInfo, 13);
   }      



  if(LWEPKeyLength == 5 || LWEPKeyLength == 10)
  {

    return ganges_send_mgmt_frame(Adapter,
                              (UINT8*)&mgmt,
                              RSI_DESC_LEN + 
                              sizeof(mgmt.frameType.wep_key.wep_64));
  }
  else
  {
    return ganges_send_mgmt_frame(Adapter,(UINT8*)&mgmt,
                              RSI_DESC_LEN + 
                              sizeof(mgmt.frameType.wep_key.wep_128));
    }      
}


/*FUNCTION*********************************************************************
Function Name  : ganges_send_pair_key_install
Description    : This routine prepare and send WPA pair key request.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
*END**************************************************************************/

GANGES_STATUS 
ganges_send_pair_key_install
  (
    IN PRSI_ADAPTER Adapter
  )
{
  UINT8 ptk_array[64];
  GANGES_STATUS Status = GANGES_STATUS_SUCCESS;
  struct RSI_Mac_frame mgmt; 

  ganges_memset(mgmt.descriptor, 0, sizeof(mgmt.frameType.pair_key)+16);

  mgmt.descriptor[0] = MGMT_DESC_TYP_SET_KEYS_REQ | 
                       (sizeof(mgmt.frameType.pair_key));
  
  ganges_memcpy((INT8*) &mgmt.descriptor[4],
         &Adapter->connectedAP.BSSID,
         sizeof(NDIS_802_11_MAC_ADDRESS));

  ganges_memcpy(ptk_array, &Adapter->wpa_splcnt.temporal_ptk, 64);
  //ganges_memcpy(ptk_array, &Adapter->wpa_splcnt.temporal_ptk.tk1, 32);
  ganges_memcpy(mgmt.frameType.pair_key.TK, &ptk_array[32], 16);
  //ganges_memcpy(mgmt.frameType.pair_key.TK, &ptk_array[0], 16);
  if(Adapter->wpa_splcnt.pairwise_cipher == WPA_CIPHER_CCMP)
  {
     mgmt.frameType.pair_key.keyDesc  = RPS_PROTECT_BROAD_CAST_DATA | RPS_CCMP;
  }
  else
  {
     mgmt.frameType.pair_key.keyDesc  = RPS_PROTECT_BROAD_CAST_DATA | RPS_TKIP;
     ganges_memcpy(mgmt.frameType.pair_key.mic_rx_key, &ptk_array[56], 8);
     ganges_memcpy(mgmt.frameType.pair_key.mic_tx_key, &ptk_array[48], 8);
/*

     ganges_memcpy(mgmt.frameType.pair_key.mic_tx_key, &ptk_array[16], 8);
     ganges_memcpy(mgmt.frameType.pair_key.mic_rx_key, &ptk_array[24], 8);*/
  }
  
        
  Status =  ganges_send_mgmt_frame(Adapter, 
                            (UINT8*)&mgmt, 
                            RSI_DESC_LEN+sizeof(mgmt.frameType.pair_key));
        
  if(Status == GANGES_STATUS_SUCCESS)
    Adapter->wpa_splcnt.unicast_keys_installed = 1;
  return Status;
        
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_sleep_params
Description    : This command send sleep parameters to the MAC.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
sleep_idle_time  |  X  |     |     |
dtim_count       |  X  |     |     |
listen_interval  |  X  |     |     |
*END**************************************************************************/

GANGES_STATUS 
ganges_send_sleep_params
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT16       sleep_idle_time,
    IN UINT16       dtim_count,     
    IN UINT16       listen_interval
  )
{
  struct RSI_Mac_frame mgmt;
  ganges_memset(mgmt.descriptor,0, 16);

  mgmt.descriptor[0] = MGMT_DESC_TYP_SLEEP_PARAM_REQ | 
                       (sizeof(mgmt.frameType.sleep.params));      
  mgmt.frameType.sleep.params.sleep_idle_time = sleep_idle_time;
  mgmt.frameType.sleep.params.dtim_count      = dtim_count;
  mgmt.frameType.sleep.params.listen_interval = listen_interval;
  mgmt.frameType.sleep.params.sleep_mod_reg = RPS_FUNC_MSTR_CLK_GATING;
  mgmt.frameType.sleep.params.pwr_xtal_good_time_reg = RPS_XTAL_GOOD_TIME |
                                                       RPS_PWR_GOOD_TIME;
  return ganges_send_mgmt_frame(Adapter, 
                            (UINT8*)&mgmt, 
                            RSI_DESC_LEN+
                                      sizeof(mgmt.frameType.sleep.params));
  
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_deep_sleep_request
Description    : This command send deep sleep management frame to the MAC.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
time_out         |  X  |     |     | If time out is non zero then wakeup mode 
                                     is time out, otherwise host interrupt.
sleep_profile    |  X  |     |     | which is going to set in sleep profile 
                                     register
*END**************************************************************************/

GANGES_STATUS 
ganges_send_deep_sleep_request
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT32       time_out,
    IN UINT16       sleep_profile,
    IN UINT16       clk_gating
  )
{
  struct RSI_Mac_frame mgmt;
  ganges_memset(mgmt.descriptor,0, 16);

  mgmt.descriptor[0] = MGMT_DESC_TYP_SLEEP_CMD_REQ | 
                       (sizeof(mgmt.frameType.sleep.sleep_cmd));
  /* wake up type 
   * If value is passed then time out deep sleep
   * If value is zero host interrupt deep sleep 
   * */
  if(time_out)
  {
    /* This mode can wakeup from int also */
    mgmt.frameType.sleep.sleep_cmd.dsleep_wakeup_reg = DSLEEP_WAKEUP_TIMEOUT;
    /*mgmt.frameType.sleep.sleep_cmd.dsleep_timeout_reg = time_out; */
  }
  else
  {
    mgmt.frameType.sleep.sleep_cmd.dsleep_wakeup_reg = DSLEEP_WAKEUP_INT;
  }
  /* sleep mode type */
  mgmt.frameType.sleep.sleep_cmd.pwr_sava_mode = PWR_SAVE_DEEP_SLEEP|
                                                 PWR_SAVE_ENABLE;
  /* sleeep profile register */
  mgmt.frameType.sleep.sleep_cmd.sleep_profile = sleep_profile;
  mgmt.frameType.sleep.sleep_cmd.clk_gating_enables = clk_gating;
  /* FIXME fill power down components 
   * mgmt.frameType.sleep.sleep_cmd.pwr_down_components = ; */
  return ganges_send_mgmt_frame(Adapter, 
                            (UINT8*)&mgmt, 
                            RSI_DESC_LEN+
                            sizeof(mgmt.frameType.sleep.sleep_cmd));

}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_sleep_request
Description    : This command send normal sleep command to the MAC.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

----------------------+-----+-----+-----+------------------------------
Name                  | I/P | O/P | I/O | Purpose
----------------------+-----+-----+-----+------------------------------
Adapter               |  X  |     |     | pointer to our adapter
pwr_save_mode         |     |     |     |
wakeup_beacon_intrvl  |     |     |     |                  
sleep_profile         |  X  |     |     | which is going to set in sleep profile 
                                          register
clk_gating            |     |     |     |					  
*END**************************************************************************/

GANGES_STATUS 
ganges_send_sleep_request
  (
    PRSI_ADAPTER       Adapter,
    IN UINT16             pwr_save_mode,
    IN UINT16             wakeup_beacon_intrvl,
    RPS_SLEEP_PROFILE  sleep_profile,
    IN UINT16             clk_gating
  )
{
  struct RSI_Mac_frame mgmt;
  ganges_memset(&mgmt, 0, RSI_DESC_LEN+sizeof(mgmt.frameType.sleep.sleep_cmd));

  mgmt.descriptor[0] = MGMT_DESC_TYP_SLEEP_CMD_REQ | 
                       (sizeof(mgmt.frameType.sleep.sleep_cmd));      

  mgmt.frameType.sleep.sleep_cmd.pwr_sava_mode = pwr_save_mode|
                                                 PWR_SAVE_ENABLE|
                                                 PWR_SAVE_DTIM_WAKEUP|
                                                 PWR_SAVE_AFE_CTRL|
                                                 PWR_SAVE_RSSI_TRG_CTRL;
  mgmt.frameType.sleep.sleep_cmd.wakeup_beacon_intrvl = wakeup_beacon_intrvl;
  mgmt.frameType.sleep.sleep_cmd.clk_gating_enables = clk_gating;
  mgmt.frameType.sleep.sleep_cmd.pwr_down_components = 0;
  mgmt.frameType.sleep.sleep_cmd.sleep_wakeup_reg = WAKEUP_MGMT_PENDING |
                                                    WAKEUP_MSDU_PENDING_Q0;
  switch(sleep_profile)
  {
    case RPS_PROFILE_AFE_CTRL:
      {
        mgmt.frameType.sleep.sleep_cmd.pwr_sava_mode = PWR_SAVE_AFE_CTRL;
        mgmt.frameType.sleep.sleep_cmd.wakeup_beacon_intrvl = 0;
        mgmt.frameType.sleep.sleep_cmd.clk_gating_enables = 0;
        mgmt.frameType.sleep.sleep_cmd.pwr_down_components = 0;
        mgmt.frameType.sleep.sleep_cmd.sleep_wakeup_reg = 0;
        break;
      }
    case RPS_PROFILE_RSSI_TRG_AGC:
      {
        mgmt.frameType.sleep.sleep_cmd.pwr_sava_mode = PWR_SAVE_AFE_CTRL|
                                                       PWR_SAVE_RSSI_TRG_CTRL;
        mgmt.frameType.sleep.sleep_cmd.wakeup_beacon_intrvl = 0;
        mgmt.frameType.sleep.sleep_cmd.clk_gating_enables = 0;
        mgmt.frameType.sleep.sleep_cmd.pwr_down_components = 0;
        mgmt.frameType.sleep.sleep_cmd.sleep_wakeup_reg = 0;
        break;
      }
    case RPS_PROFILE_SLEEP1:
      {
        mgmt.frameType.sleep.sleep_cmd.sleep_profile = 
                                                RPS_COMPO_RF_PD1_PD2 |
                                                RPS_COMPO_RF_PLLON | 
                                                RPS_COMPO_SHUT_DOWN_IQ_DAC_ADC |
                                                RPS_COMPO_SHUT_DOWN_AUX_ADC|  
                                                RPS_COMPO_AFE_PLL    |
                                                RPS_COMPO_AFE_BIAS_CELL;
        mgmt.frameType.sleep.sleep_cmd.sleep_profile_wakeup_time = 
                                                WAKEUP_TIME_SLEEP1;
        break;
      }
    case RPS_PROFILE_SLEEP2:
      {
        mgmt.frameType.sleep.sleep_cmd.sleep_profile = 
                                                RPS_COMPO_RF_PD1_PD2 |
                                                RPS_COMPO_RF_PLLON | 
                                                RPS_COMPO_SHUT_DOWN_IQ_DAC_ADC |
                                                RPS_COMPO_SHUT_DOWN_AUX_ADC|  
                                                RPS_COMPO_AFE_PLL;
        mgmt.frameType.sleep.sleep_cmd.sleep_profile_wakeup_time = 
                                                WAKEUP_TIME_SLEEP2;
        break;
      }

    case RPS_PROFILE_SLEEP3:
      {
        mgmt.frameType.sleep.sleep_cmd.sleep_profile = 
                                                RPS_COMPO_RF_PD1_PD2 |
                                                RPS_COMPO_RF_PLLON | 
                                                RPS_COMPO_SHUT_DOWN_IQ_DAC_ADC |
                                                RPS_COMPO_SHUT_DOWN_AUX_ADC;
        mgmt.frameType.sleep.sleep_cmd.sleep_profile_wakeup_time = 
                                                WAKEUP_TIME_SLEEP3;
        break;
      }

    case RPS_PROFILE_SLEEP4:
      {
        mgmt.frameType.sleep.sleep_cmd.sleep_profile = 
                                                RPS_COMPO_RF_PD1_PD2 |
                                                RPS_COMPO_RF_PLLON | 
                                                RPS_COMPO_STND_BY_IQ_DAC_ADC|
                                                RPS_COMPO_SHUT_DOWN_AUX_ADC;
        mgmt.frameType.sleep.sleep_cmd.sleep_profile_wakeup_time = 
                                                WAKEUP_TIME_SLEEP4;
        break;
      }

    case RPS_PROFILE_SLEEP5:
      {
        mgmt.frameType.sleep.sleep_cmd.sleep_profile = 
                                                RPS_COMPO_STND_BY_IQ_DAC_ADC|
                                                RPS_COMPO_SHUT_DOWN_AUX_ADC |
                                                RPS_COMPO_RX_TX_LOW;
        mgmt.frameType.sleep.sleep_cmd.sleep_profile_wakeup_time = 
                                                WAKEUP_TIME_SLEEP5;
        break;
      }
    case RPS_PROFILE_SLEEP6:
      {
        mgmt.frameType.sleep.sleep_cmd.sleep_profile = 
                                                RPS_COMPO_STND_BY_IQ_DAC_ADC|
                                                RPS_COMPO_SHUT_DOWN_AUX_ADC;
        mgmt.frameType.sleep.sleep_cmd.sleep_profile_wakeup_time = 
                                                WAKEUP_TIME_SLEEP6;
        break;
      }

    default:
      {
        /* by default we set sleep mode 1 */
        RSI_DEBUG(RSI_ZONE_ERROR,"ganges_send_sleep_request: invalid sleep profile\n");
        RSI_DEBUG(RSI_ZONE_ERROR,"ganges_send_sleep_request: setting sleep1\n");
        mgmt.frameType.sleep.sleep_cmd.sleep_profile = 
                                                RPS_COMPO_RF_PD1_PD2 |
                                                RPS_COMPO_RF_PLLON | 
                                                RPS_COMPO_SHUT_DOWN_IQ_DAC_ADC |
                                                RPS_COMPO_SHUT_DOWN_AUX_ADC|  
                                                RPS_COMPO_AFE_PLL    |
                                                RPS_COMPO_AFE_BIAS_CELL;
        mgmt.frameType.sleep.sleep_cmd.sleep_profile_wakeup_time = 
                                                WAKEUP_TIME_SLEEP1;
        break;
      }


  }
  return ganges_send_mgmt_frame(Adapter, 
                                      (UINT8*)&mgmt, 
                            RSI_DESC_LEN+
                                      sizeof(mgmt.frameType.sleep.sleep_cmd));
}


/*FUNCTION*********************************************************************
Function Name  : ganges_send_wakeup_request
Description    : This command send wake up command request to the MAC.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
*END**************************************************************************/

GANGES_STATUS 
ganges_send_wakeup_request
  (
    IN PRSI_ADAPTER    Adapter
  )
{
  struct RSI_Mac_frame mgmt;
  ganges_memset(&mgmt,0,RSI_DESC_LEN);

  mgmt.descriptor[0] = MGMT_DESC_TYP_WAKEUP_REQ ; 
  
  return ganges_send_mgmt_frame(Adapter, 
                            (UINT8*)&mgmt, 
                                      RSI_DESC_LEN);
}


/*FUNCTION*********************************************************************
Function Name  : ganges_send_TSF_resp
Description    : This command send wake up command request to the MAC.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
*END**************************************************************************/

GANGES_STATUS 
ganges_send_TSF_resp
  (
    IN PRSI_ADAPTER    Adapter,
    IN UINT16             residue
  )
{
  struct RSI_Mac_frame mgmt;
  ganges_memset(&mgmt, 0, RSI_DESC_LEN);

  mgmt.descriptor[0] = MGMT_DESC_TYP_TSF_SYNC_RESP|2; 
  mgmt.frameType.tsf.residue = residue;
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_TSF_resp: Sending wakeup cmd\n");
  return ganges_send_mgmt_frame(Adapter, 
                            (UINT8*)&mgmt, 
                                   RSI_DESC_LEN+sizeof(mgmt.frameType.tsf));
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_group_key_install
Description    : This routine prepares and sends group keys request
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
SeqCounter       |  X  |     |     | 
key_id           |  X  |     |     | 
*END**************************************************************************/

GANGES_STATUS
ganges_send_group_key_install
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT8 *SeqCounter,
    IN UINT8 key_id
  )
{
  struct RSI_Mac_frame mgmt;

  UINT8 gtk_array[32];
  ganges_memset(mgmt.descriptor, 0, sizeof(mgmt.frameType.pair_key)+16);
  mgmt.descriptor[0] = MGMT_DESC_TYP_SET_KEYS_REQ| 
                       (sizeof(mgmt.frameType.pair_key));

  mgmt.descriptor[1] = 1; 
  
  ganges_memcpy((INT8*) &mgmt.descriptor[4],
                &Adapter->connectedAP.BSSID,
                sizeof(NDIS_802_11_MAC_ADDRESS));

  ganges_memcpy(gtk_array, &Adapter->wpa_splcnt.temporal_gtk, 32);
  ganges_memcpy(mgmt.frameType.pair_key.TK, &gtk_array[0], 16);
  if(Adapter->wpa_splcnt.group_cipher == WPA_CIPHER_CCMP)
  {
    RSI_DEBUG(RSI_ZONE_INFO,
              "ganges_send_group_key_install: Grp key cipher is CCMP\n"); 
    mgmt.frameType.pair_key.keyDesc  = RPS_PROTECT_BROAD_CAST_DATA | 
                                       RPS_CCMP|
                                       (key_id<<14);
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_INFO,
              "ganges_send_group_key_install: Grp key cipher is TKIP\n"); 
    mgmt.frameType.pair_key.keyDesc  = RPS_PROTECT_BROAD_CAST_DATA | 
                                       RPS_TKIP |
                                       (key_id<<14);
    ganges_memcpy(mgmt.frameType.pair_key.mic_tx_key,&gtk_array[16],8);
    ganges_memcpy(mgmt.frameType.pair_key.mic_rx_key,&gtk_array[24],8);
  }
        
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_group_key_install: sending grp key install frame\n");
  return ganges_send_mgmt_frame(Adapter, 
                             (UINT8*)&mgmt, 
                             RSI_DESC_LEN+sizeof(mgmt.frameType.pair_key));

}

/*FUNCTION*********************************************************************
Function Name  : ganges_setBBGainRequest
Description    : This function prepare set BB Gain Request frame and sends  it to 
                 the MAC.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |  X  |     |     | pointer to our adapter
rf_type          |  X  |     |     | RF type
length           |  X  |     |     | Length of the set bb gain request frame 
error            |  X  |     |     | Error to be applied to the current BB Max gain
                                     and Gain Scaling High Registers
*END**************************************************************************/

GANGES_STATUS 
ganges_setBBGainRequest
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT16       rf_type,
    IN UINT32       length,
    IN INT32        error
  )
{
  struct RSI_Mac_frame mgmt;
 
  Adapter->bb_max_gain_current  = Adapter->bb_max_gain_current + error;
  Adapter->gain_scaling_current = Adapter->gain_scaling_current+ error;
  
  if(Adapter->bb_max_gain_current > 0x382E)
    Adapter->bb_max_gain_current = 0x382E;
  else if(Adapter->bb_max_gain_current < 0x3802) 
    Adapter->bb_max_gain_current = 0x3802;

  if(Adapter->gain_scaling_current > 0x102E)
    Adapter->gain_scaling_current =  0x102E;
  else if(Adapter->gain_scaling_current < 0x1002)
    Adapter->gain_scaling_current =  0x1002;

  if(Adapter->bb_max_gain_current  == Adapter->bb_max_gain_previous)
  {
    RSI_DEBUG(RSI_ZONE_NFM_PATH,"ganges_setBBGainRequest: No updation .. returning\n");
    return GANGES_STATUS_SUCCESS;
  }
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_setBBGainRequest: Updating %x %x\n",Adapter->bb_max_gain_current, 
                          Adapter->gain_scaling_current);
  
  ganges_memset(mgmt.descriptor, 0, 16);
  Adapter->bb_max_gain_previous = Adapter->bb_max_gain_current;
  
  mgmt.descriptor[0] = MGMT_DESC_TYP_SET_BBGAIN_REQ | 
                       (sizeof(mgmt.frameType.setbb_gain));
   		    
  mgmt.frameType.setbb_gain.gain_val[0] = Adapter->bb_max_gain_current;
  mgmt.frameType.setbb_gain.gain_val[1] = Adapter->gain_scaling_current;


  return ganges_send_mgmt_frame(Adapter, 
                             (UINT8*)&mgmt, 
                             (RSI_DESC_LEN +  
                             (sizeof(mgmt.frameType.setbb_gain))));
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_probe_request
Description    : This function send probe request to the hardware
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :
  
-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |  X  |     |     | pointer to our adapter
channelNum       |  X  |     |     | On which channel we want scan
broadcast        |  X  |     |     | broadcast probe
*END**************************************************************************/
  
GANGES_STATUS
ganges_send_probe_request
  ( 
    IN PRSI_ADAPTER Adapter, 
    IN UINT8        type,   
    IN UINT32       channelNum,
    IN UINT32       broadcast,
    IN UINT8        *destination
  )
{   
  struct RSI_Mac_frame *mgmt;
  UINT8  buffer[256];
  UINT8  *temp;
  UINT32   length;
  probe_req_frame_body_t *probe_req_frame_body;
  
  mgmt = (struct RSI_Mac_frame*)buffer;
  ganges_memset(buffer,0,256);

  mgmt->frameType.probe_req.minChannelTime = Adapter->minChannelTime;
  if(type == ACTIVE_SCAN)
  {
    mgmt->frameType.probe_req.responseTimeout = 100;
  }
  else
  {
    mgmt->frameType.probe_req.responseTimeout = 400;
  }
  
  temp = mgmt->frameType.probe_req.var_data; 
  if(FSM_STATE == FSM_OPEN)
  {
    mgmt->frameType.probe_req.channel_val_len = 0;
  }
  else
  {
    mgmt->frameType.probe_req.channel_val_len = 7;
    /* Channel programming values */
    ganges_memcpy(temp,
                  &Adapter->rsi_channel_vals[channelNum],
                  12);
    temp += 12;
    *(UINT16 *)temp = 150;
    temp+=2;
  }
  probe_req_frame_body = (probe_req_frame_body_t *)temp;
  if(type == ACTIVE_SCAN)
  {
    probe_req_frame_body->header   = 0x0040;
    probe_req_frame_body->duration = 0x0;
    ganges_memset(probe_req_frame_body->destination, 0xFF, 6);
    ganges_memset(probe_req_frame_body->bssid, 0xFF, 6);
    ganges_memcpy(probe_req_frame_body->source,
                  Adapter->PermanentAddress,
                  ETH_ADDR_LEN);
    temp += sizeof(probe_req_frame_body_t);

  /* prepare ssid eid */
    *temp++ = 0;
    if(!broadcast)
    {
      ganges_memcpy(probe_req_frame_body->destination, destination, 6);
#if 0
    /* unicast probe request */
    *temp++ = Adapter->scanConfirmVarArray[Adapter->SelectedBssidNum]->SSIDLen;

    ganges_memcpy(temp,
                   Adapter->scanConfirmVarArray[Adapter->SelectedBssidNum]->
                                                                           SSID,
                   Adapter->scanConfirmVarArray[Adapter->SelectedBssidNum]->
                                                                      SSIDLen);
    temp += Adapter->scanConfirmVarArray[Adapter->SelectedBssidNum]->SSIDLen;
#endif

    }
    else
    {
      /* broadcast probe request */
      if(destination !=NULL)
      {
        UINT8 length = *(UINT32 *)destination;
        *temp++= length;  
        destination +=4;
        ganges_memcpy(temp,destination,length);
        temp+=length;  
      }
      else  
      {
        *temp++ = 0;
      } 
    }

    *temp++ =  RSI_SUPPORTED_RATES_EID;
    if(Adapter->ProtocolType == ProtoType802_11B)
    {
      *temp++ = 0x04;         /*Supported rates length*/

      *temp++ = 0x82;        /* 1 */
      *temp++ = 0x84;        /* 2 */
      *temp++ = 0x8B;        /* 5.5 */
      *temp++ = 0x96;        /* 11 */
    }
    else if ((Adapter->ProtocolType == ProtoType802_11G) || 
             (Adapter->ProtocolType == ProtoType802_11N))
    {

      *temp++ = 0x08;         /* Supported rates length */

      *temp++ = 0x82;         /* 1 */
      *temp++ = 0x84;         /* 2 */
      *temp++ = 0x8B;         /* 5.5 */
      *temp++ = 0x96;         /* 11 */
      *temp++ = 0x24;         /* 18 */
      *temp++ = 0x30;         /* 24 */
      *temp++ = 0x48;         /* 36 */
      *temp++ = 0x6c;         /*  54 */
      *temp++ = 0x32;        /*Extended Rates */
      *temp++ = 0x04;
      *temp++ = 0x0c;         /* 6 */
      *temp++ = 0x12;         /* 9 */
      *temp++ = 0x18;         /* 12 */
      *temp++ = 0x60;         /* 48 */
    }
    if(Adapter->ProtocolType == ProtoType802_11N)
    {
       /*Inset 11n element */
      ganges_memset(temp, 0, 0x1C);
      *temp++ = 0x2D;
      *temp++ = 0x1A;
      *temp++ = 0x00;
      *temp++ = 0x00;
      *temp++ = 0x18;
      *temp++ = 0xFF;
      temp += 0x16;
      ganges_memset(temp, 0, 0x20);
      *temp++ = 0xDD;
      *temp++ = 0x1E;
      *temp++ = 0x00;
      *temp++ = 0x90;
      *temp++ = 0x4C;
      *temp++ = 0x33;
      *temp++ = 0x00;
      *temp++ = 0x00;
      *temp++ = 0x18;
      *temp++ = 0xFF;
      temp += 0x16;
    }
#if 0
  /* prepare supported rates eid */
  *temp++ = 1;
  *temp++ = 4;
  *temp++ = 0x82;
  *temp++ = 0x84;
  *temp++ = 0x0B;
  *temp++ = 0x16;
#endif
  }
  probe_req_frame_body->length   = temp - (UINT8 *)probe_req_frame_body - 2;
  length = (temp - (UINT8 *)mgmt);
  /* SDOutputBuffer(mgmt, length);*/

  if(type == ACTIVE_SCAN)
  {
    RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_probe_request:sending probe request chnl Num %d\n",
              channelNum);
    mgmt->descriptor[0] = MGMT_DESC_TYP_SND_PRB_REQ | (length-16);
    mgmt->descriptor[1] = 0x0040;
    mgmt->descriptor[4] = Adapter->dataRate;
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_scan_request:sending scan request chnl Num %d\n",
              channelNum);
    mgmt->descriptor[0] = MGMT_DESC_TYP_SCAN_REQ | (length-16);
    if(Adapter->NetworkType == Ndis802_11Infrastructure)
    {
      mgmt->descriptor[1] = 1;
    }
    else
    {
     /* IBSS */
      mgmt->descriptor[1] = 0;
    }
    mgmt->descriptor[5] = 0x01;
  }
  /* Our probe request is ready */
  return ganges_send_mgmt_frame(Adapter,
                         (UINT8*)mgmt,
                         length);
}


/*FUNCTION*********************************************************************
Function Name  : ganges_send_probe_resp_request
Description    : This function send a probe response frame to the hardware
                 This will be used in the IBSS mode only.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter            |  X  |     |     | pointer to our adapter
*END**************************************************************************/

GANGES_STATUS 
ganges_send_probe_resp_request
  (
    IN PRSI_ADAPTER Adapter,
    ProtoType       ProtocolType
  )
{
  struct RSI_Mac_frame * mgmt;
  UINT8  buffer[256];
  UINT8  *temp;

  mgmt = (struct RSI_Mac_frame*)buffer;
  ganges_memset(mgmt->descriptor, 0, 16);

  if(ProtocolType == ProtoType802_11B)
  { 
    Adapter->ProtocolType = ProtoType802_11B;
  }
  
  mgmt->frameType.probe_resp.beacon_int = Adapter->Ibss_info.Beacon;
  mgmt->frameType.probe_resp.capabilityInfo = Adapter->Ibss_info.CapabilityInfo;
  if(Adapter->encryptAlgo == Ndis802_11WEPEnabled)
  {
    mgmt->frameType.probe_resp.capabilityInfo |= 0x0010;
  }

  /* FIXME Give the correct capability info based on the preamble, slot etc */


  /*filling the variable length items */
  temp = &mgmt->frameType.probe_resp.variable[0];

  *temp++ = RSI_SSID_EID;
  *temp++ = Adapter->Ibss_info.SSIDLen;

  ganges_memcpy(temp, Adapter->Ibss_info.SSID, Adapter->Ibss_info.SSIDLen);

  RSI_DEBUG(RSI_ZONE_INFO,
             "Sending Probe response ssid %s SSID Length %d\n",
                   (char *)(Adapter->Ibss_info.SSID), (UINT16)Adapter->Ibss_info.SSIDLen);

  temp += Adapter->Ibss_info.SSIDLen;

  *temp++ = RSI_SUPPORTED_RATES_EID;

  //if(Adapter->ProtocolType == PotoType802_11B)
  {

    *temp++ = 0x04;         /*Supported rates length*/

    *temp++ = 0x82;    /* 1 */
    *temp++ = 0x84;    /* 2 */
    *temp++ = 0x8B;    /* 5.5 */
    *temp++ = 0x96;    /* 11 */
  }

  if(ProtocolType == ProtoType802_11A)
  {

    *temp++ = 0x08;         /* Supported rates length */

    /* *temp++ = 0x82; */
    /* *temp++ = 0x84;   */    /*6Mbps */
    /* *temp++ = 0x8B;
       *temp++ = 0x96; */
    *temp++ = 0x24;
    *temp++ = 0x30;
    *temp++ = 0x48;
    *temp++ = 0x6c;
    /**temp++ = 0x32;
      *temp++ = 0x04; */
    *temp++ = 0x8c;
    *temp++ = 0x12;
    *temp++ = 0x18;
    *temp++ = 0x60;
  }

  /* DS_PARAMETER_SET EID, Length, channel no */
  *temp++ = 0x3;
  *temp++ = 0x1;
  *temp++ = (UINT8) Adapter->Ibss_info.channelNum;

 /* IBSS Parameter Set */
  *temp++ = 0x6; //element id IBSS_PARAMETER_SET;
  *temp++ = 0x2;
  ganges_memcpy(temp,&(Adapter->Ibss_info.ATIMWindow),2);
  temp += 2 ;

  if((ProtocolType == ProtoType802_11G)||
     (ProtocolType == ProtoType802_11N))
  {
    *temp++ = 0x2A ;
    /*ERP_INFO_ELM_ID*/
    *temp++ = 1;
    *temp++ = 0;
    /* Extended supported rates , length */
    *temp++ = 0x32;    
    *temp++ = 0x08; 
    *temp++ = 0x0C;   /* 6 */
    *temp++ = 0x12;   /* 9 */
    *temp++ = 0x18;   /* 12 */
    *temp++ = 0x24;   /* 18  */
    *temp++ = 0x30;   /* 24 */
    *temp++ = 0x48;   /* 36 */
    *temp++ = 0x60;   /* 48 */
    *temp++ = 0x6c;   /* 54 */
  }

  if(ProtocolType == ProtoType802_11N)
  {
      /*Inset 11n element */
    ganges_memset(temp, 0, 0x1C);
    *temp++ = 0x2D;
    *temp++ = 0x1A;
    *temp++ = 0x00;
    *temp++ = 0x00;
    *temp++ = 0x18;
    *temp++ = 0xFF;
    temp += 0x16;
    ganges_memset(temp, 0, 0x20);
    *temp++ = 0xDD;
    *temp++ = 0x1E;
    *temp++ = 0x00;
    *temp++ = 0x90;
    *temp++ = 0x4C;
    *temp++ = 0x33;
    *temp++ = 0x00;
    *temp++ = 0x00;
    *temp++ = 0x18;
    *temp++ = 0xFF;
    temp += 0x16;
  }

#if 0
  /* We are not supporting IBSS WPA now */
   /* Adding WPA Element */
   if(Adapter->wpa_enabled)
   {
     *temp++ = 0xdd;
     *temp++ = 0x05;
     *temp++ = 0x00;
     *temp++ = 0x10; 
     *temp++ = 0x18;
     *temp++ = 0x01;
     *temp++ = 0x00;

     ganges_memcpy(temp, 
                    Adapter->wpa_splcnt->wpa_ie,
                    Adapter->wpa_splcnt->wpa_ie[1] + 2);
  
     temp += Adapter->wpa_splcnt->wpa_ie[1] + 2;
   }
#endif
        
  
  mgmt->descriptor[0] = MGMT_DESC_TYP_PRB_RSP_REQ|(temp -(UINT8 *)mgmt - 16);
  mgmt->frameType.probe_resp.length = temp-(UINT8 *)mgmt - RSI_DESC_LEN - 2;
/*  if(mgmt->descriptor[0] % 2)
    mgmt->descriptor[0]++;
  if(mgmt->frameType.probe_resp.length %2)
    mgmt->frameType.probe_resp.length++;
*/
  return ganges_send_mgmt_frame(Adapter,
                         (UINT8 *)mgmt,
                         (temp - (UINT8 *)mgmt));


}                              

/*FUNCTION*********************************************************************
Function Name  : ganges_send_measure_noise_rssi_request
Description    : This function send a measure noise rssi request to the firmware.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter            |  X  |     |     | pointer to our adapter
no_times           |     |     |     |
*END**************************************************************************/

GANGES_STATUS 
ganges_send_measure_noise_rssi_request
  (
    IN PRSI_ADAPTER Adapter,
    UINT32          no_times
  )
{
  struct RSI_Mac_frame mgmt;

  ganges_memset(mgmt.descriptor, 0, sizeof(mgmt.frameType.measure_noise_rssi)+RSI_DESC_LEN);
  mgmt.descriptor[0] = MGMT_DESC_TYP_MEASURE_NOISE_RSSI_REQ| 
                       (sizeof(mgmt.frameType.measure_noise_rssi));
  mgmt.frameType.measure_noise_rssi.set_msr_noise_rssi_cmd = 0x36DE;
  mgmt.frameType.measure_noise_rssi.reset_msr_noise_rssi_cmd = 0x36DC;
  mgmt.frameType.measure_noise_rssi.no_times = (UINT16)no_times;
  mgmt.frameType.measure_noise_rssi.lenth_pre_bb_prob_vals = 2;
  mgmt.frameType.measure_noise_rssi.pre_bb_vals[0] = 0xC56;
  mgmt.frameType.measure_noise_rssi.pre_bb_vals[1] = 0x301E;
  mgmt.frameType.measure_noise_rssi.lenth_reset_bb_prob_vals = 2;
  mgmt.frameType.measure_noise_rssi.reset_bb_vals[0] = 0xD56;
  mgmt.frameType.measure_noise_rssi.reset_bb_vals[1] = 0x311E;

  return ganges_send_mgmt_frame(Adapter, 
                            (UINT8 *)&mgmt, 
                            (sizeof(mgmt.frameType.measure_noise_rssi)+
                                RSI_DESC_LEN));
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_update_noise_rssi_request
Description    : This function send a update noise rssi request to the firmware.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------------+-----+-----+-----+------------------------------
Name                   | I/P | O/P | I/O | Purpose
-----------------------+-----+-----+-----+------------------------------
Adapter                |  X  |     |     | pointer to our adapter
update_noise_rssi_val  |  X  |     |     |
*END**************************************************************************/

GANGES_STATUS 
ganges_send_update_noise_rssi_request
  (
    IN PRSI_ADAPTER Adapter,
    UINT16          update_noise_rssi_val
  )
{
  struct RSI_Mac_frame mgmt;

  ganges_memset(mgmt.descriptor, 0, sizeof(mgmt.frameType.update_noise_rssi)+
                                        RSI_DESC_LEN);
  mgmt.descriptor[0] = MGMT_DESC_TYP_UPDATE_NOISE_RSSI_REQ| 
                       sizeof(mgmt.frameType.update_noise_rssi);
  mgmt.frameType.update_noise_rssi.val1 = (0x4B << 9) | 
                                          (update_noise_rssi_val<<1);
  mgmt.frameType.update_noise_rssi.val2 = (0x4C << 9) | 
                                          (update_noise_rssi_val+5)<<1;
  return ganges_send_mgmt_frame(Adapter, 
                             (UINT8 *)&mgmt, 
                             (sizeof(mgmt.frameType.update_noise_rssi)+
                                RSI_DESC_LEN));
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_ibssds_response
Description    : 
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
pAdater            |  X  |     |     | Adapter 
sta_mac            |  X  |     |     |  
ProtocolType       |     |     |     | Protocol type
*END**************************************************************************/

GANGES_STATUS
ganges_send_ibssds_response
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT8 sta_mac[],
    ProtoType ProtocolType
  )
{
  struct RSI_Mac_frame mgmt;

  ganges_memset(mgmt.descriptor, 0, sizeof(mgmt.frameType.form_ibssds)+
                                        RSI_DESC_LEN);
  mgmt.descriptor[0] = MGMT_DESC_TYP_FORM_IBSSDS_RSP| 
                       sizeof(mgmt.frameType.form_ibssds);

  ganges_memcpy(&mgmt.descriptor[2], 
                sta_mac, 
                6);
  if(ProtocolType == ProtoType802_11G)
  {
    mgmt.frameType.form_ibssds.data_rate = RSI_RATE_54|(RPS_RATE_54_PWR<<8);
  }
  else if(ProtocolType == ProtoType802_11B)
  {
    mgmt.frameType.form_ibssds.data_rate = RSI_RATE_11|(RPS_RATE_11_PWR<<8);
  }
  else if(ProtocolType == ProtoType802_11N)
  {
    mgmt.frameType.form_ibssds.data_rate = RSI_RATE_MCS0|(RPS_RATE_MCS0_PWR<<8);
  }
  else
  {
    mgmt.frameType.form_ibssds.data_rate = RSI_RATE_54|(RPS_RATE_54_PWR<<8);
  }
   
  /*  FIXME fill this support later
  mgmt.frameType.form_ibssds.supported_rates[6] = */
  return ganges_send_mgmt_frame(Adapter, 
                            (UINT8 *)&mgmt, 
                            (sizeof(mgmt.frameType.form_ibssds)+
                                RSI_DESC_LEN));
}


/*FUNCTION*********************************************************************
Function Name  : ganges_send_auto_rate_request
Description    : This function prepare and send the auto rate request packet.
                 with this frame not only rate, we can control power and 
                 antenna diversity also.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
pAdapter           |  X  |     |     | Adapter
*END**************************************************************************/

GANGES_STATUS
ganges_send_auto_rate_request
  (
    IN PRSI_ADAPTER Adapter,
    IN INT8         power_control,
    IN INT8         antenna_diversity
  )
{

  INT32  ii;
  struct RSI_Mac_frame mgmt;
  UINT16 *temp;
  UINT16 jj;
  UINT8  frame_length;

  

  UINT8 std_rates[] = { STD_RATE_00, STD_RATE_01, STD_RATE_02, STD_RATE_5_5,
                         STD_RATE_06, STD_RATE_09, STD_RATE_11, STD_RATE_12,
                         STD_RATE_18, STD_RATE_24, STD_RATE_36, STD_RATE_48,
                         STD_RATE_54
                       };

  UINT8 rps_rates[] = { RPS_RATE_00, RPS_RATE_01, RPS_RATE_02, RPS_RATE_5_5,
                        RPS_RATE_06, RPS_RATE_09, RPS_RATE_11, RPS_RATE_12,
                        RPS_RATE_18, RPS_RATE_24, RPS_RATE_36, RPS_RATE_48,
                        RPS_RATE_54, RPS_RATE_MCS0,RPS_RATE_MCS1,RPS_RATE_MCS2,
                        RPS_RATE_MCS3,RPS_RATE_MCS4,RPS_RATE_MCS5,RPS_RATE_MCS6,
                        RPS_RATE_MCS7};


UINT8 rps_power_val[] = { RPS_RATE_00_PWR, RPS_RATE_01_PWR, RPS_RATE_02_PWR,
                            RPS_RATE_5_5_PWR, RPS_RATE_06_PWR, RPS_RATE_09_PWR,
                            RPS_RATE_11_PWR, RPS_RATE_12_PWR, RPS_RATE_18_PWR,
                            RPS_RATE_24_PWR, RPS_RATE_36_PWR, RPS_RATE_48_PWR,
                            RPS_RATE_54_PWR,RPS_RATE_MCS0_PWR,RPS_RATE_MCS1_PWR,
                            RPS_RATE_MCS2_PWR,RPS_RATE_MCS3_PWR,RPS_RATE_MCS4_PWR,
                            RPS_RATE_MCS5_PWR,RPS_RATE_MCS6_PWR,RPS_RATE_MCS7_PWR};
  

  typedef struct {
     UINT16 failure_limit;
     UINT16 Initial_boundary;
     UINT16 max_threshold_limt;
     UINT16 num_supported_rates;
     struct { 
       UINT16 supported_rates;
       UINT16 frag_thrshld;
     }values[1];
  }autorate_table_t;
  autorate_table_t  *auto_rate_tbl2;

   ganges_memset(&mgmt, 0,sizeof(mgmt.frameType.auto_rate)+RSI_DESC_LEN);
  
  mgmt.frameType.auto_rate.failure_limit       =  3;
  mgmt.frameType.auto_rate.Initial_boundary    = 3;
  mgmt.frameType.auto_rate.max_threshold_limt  = 27;
  mgmt.frameType.auto_rate.num_supported_rates = 0;
  jj = 0;

  if((Adapter->connectedAP.ProtocolType  == ProtoType802_11B) ||
     (Adapter->ProtocolType== ProtoType802_11B))
  {
    RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_auto_rate_request: Sending auto rate for B only\n");

    /* either AP or STA in B only mode copy the B rates */
    for(ii=RATE_54_MBPS; ii > 0; ii--)
    {
      if((std_rates[ii] == STD_RATE_11) ||
         (std_rates[ii] == STD_RATE_5_5) ||
         (std_rates[ii] == STD_RATE_02) ||
         (std_rates[ii] == STD_RATE_01))
      {
        if(ganges_is_AP_supported_rate(
          Adapter->scanConfirmVarArray[Adapter->SelectedBssidNum],
                std_rates[ii]) == GANGES_STATUS_SUCCESS)
        {
          mgmt.frameType.auto_rate.values[jj].supported_rates =
                  (rps_rates[ii] | rps_power_val[ii]<<8);
          mgmt.frameType.auto_rate.values[jj++].frag_thrshld =
                    Adapter->frag_threshold;
        }
      }
    }
    auto_rate_tbl2 = (autorate_table_t *)&mgmt.frameType.auto_rate.values[jj];
    mgmt.frameType.auto_rate.num_supported_rates = jj;
    auto_rate_tbl2->failure_limit       = 0;
    auto_rate_tbl2->Initial_boundary    = 0;
    auto_rate_tbl2->max_threshold_limt  = 0;
    auto_rate_tbl2->num_supported_rates = 0;
 
    /* In B mode no need of second table */
    temp = (UINT16 *)&auto_rate_tbl2->values[auto_rate_tbl2->num_supported_rates];
    *temp++ = 0; /* Failure count to go to tbl2 */
    *temp++ = 0; /* success count to go to tbl1 */
    *temp++ = 1; /* collision error rate allowable at stage 2*/

  }
  else /* BG mixed mode */
  {
    RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_auto_rate_request: Sending auto rate for BG/BGN\n");
    /* Fill the rates in descending order */
    for(ii=RATE_54_MBPS; ii > 0; ii--)
    {
      if((std_rates[ii] == STD_RATE_54) ||
         (std_rates[ii] == STD_RATE_36) ||
         (std_rates[ii] == STD_RATE_24) ||
         (std_rates[ii] == STD_RATE_12) ||
         (std_rates[ii] == STD_RATE_11))
      {
        if(ganges_is_AP_supported_rate(
          Adapter->scanConfirmVarArray[Adapter->SelectedBssidNum],
                std_rates[ii]) == GANGES_STATUS_SUCCESS)
        {
          mgmt.frameType.auto_rate.values[jj].supported_rates =
                  (rps_rates[ii] | rps_power_val[ii]<<8);
          mgmt.frameType.auto_rate.values[jj++].frag_thrshld =
                    Adapter->frag_threshold;
        }
      }
    }
    mgmt.frameType.auto_rate.values[jj].supported_rates =
                  (RPS_RATE_MCS6 | RPS_RATE_MCS6_PWR<<8);
    mgmt.frameType.auto_rate.values[jj++].frag_thrshld =
                    Adapter->frag_threshold;
    mgmt.frameType.auto_rate.values[jj].supported_rates =
                  (RPS_RATE_MCS4 | RPS_RATE_MCS4_PWR<<8);
    mgmt.frameType.auto_rate.values[jj++].frag_thrshld =
                    Adapter->frag_threshold;
    mgmt.frameType.auto_rate.values[jj].supported_rates =
                  (RPS_RATE_MCS3 | RPS_RATE_MCS3_PWR<<8);
    mgmt.frameType.auto_rate.values[jj++].frag_thrshld =
                    Adapter->frag_threshold;
    mgmt.frameType.auto_rate.values[jj].supported_rates =
                  (RPS_RATE_MCS1 | RPS_RATE_MCS1_PWR<<8);
    mgmt.frameType.auto_rate.values[jj++].frag_thrshld =
                    Adapter->frag_threshold;
    mgmt.frameType.auto_rate.values[jj].supported_rates =
                  (RPS_RATE_MCS1 | RPS_RATE_MCS1_PWR<<8);
    mgmt.frameType.auto_rate.values[jj++].frag_thrshld =
                    Adapter->frag_threshold;

    auto_rate_tbl2 = (autorate_table_t *)&mgmt.frameType.auto_rate.values[jj];

    mgmt.frameType.auto_rate.num_supported_rates = jj;
    auto_rate_tbl2->failure_limit       =  3;
    auto_rate_tbl2->Initial_boundary    = 3;
    auto_rate_tbl2->max_threshold_limt  = 27;
    auto_rate_tbl2->num_supported_rates = 0;
    jj = 0;

     /* Fill the rates in descending order */
    for(ii=RATE_54_MBPS; ii > 0; ii--)
    {
      if((std_rates[ii] == STD_RATE_11) ||
         (std_rates[ii] == STD_RATE_06) ||
         (std_rates[ii] == STD_RATE_02) ||
         (std_rates[ii] == STD_RATE_01))
      {
        if(ganges_is_AP_supported_rate(
            Adapter->scanConfirmVarArray[Adapter->SelectedBssidNum],
                std_rates[ii]) == GANGES_STATUS_SUCCESS)
        {
          auto_rate_tbl2->values[jj].supported_rates =
                  (rps_rates[ii] | rps_power_val[ii]<<8);

          auto_rate_tbl2->values[jj++].frag_thrshld =
                        Adapter->frag_threshold;
        }
      }
    }
    auto_rate_tbl2->values[jj].supported_rates =
                  (RPS_RATE_MCS1 | RPS_RATE_MCS1_PWR<<8);
    auto_rate_tbl2->values[jj++].frag_thrshld =
                    Adapter->frag_threshold;
    auto_rate_tbl2->values[jj].supported_rates =
                  (RPS_RATE_MCS0 | RPS_RATE_MCS0_PWR<<8);
    auto_rate_tbl2->values[jj++].frag_thrshld =
                    Adapter->frag_threshold;
    auto_rate_tbl2->values[jj].supported_rates =
                  (RPS_RATE_02 | RPS_RATE_02_PWR<<8);
    auto_rate_tbl2->values[jj++].frag_thrshld =
                    Adapter->frag_threshold;
    auto_rate_tbl2->values[jj].supported_rates =
                  (RPS_RATE_01 | RPS_RATE_01_PWR<<8);
    auto_rate_tbl2->values[jj++].frag_thrshld =
                    Adapter->frag_threshold;

    temp = (UINT16 *)&auto_rate_tbl2->values[jj];
    auto_rate_tbl2->num_supported_rates = jj;
    *temp++ = 5; /* Failure count to go to tbl2 */
    *temp++ = 5; /* success count to go to tbl1 */
    *temp++ = 1; /* collision error rate allowable at stage 2*/
  }

  frame_length = (8+ (mgmt.frameType.auto_rate.num_supported_rates*4) +
                        8+ (auto_rate_tbl2->num_supported_rates*4) + 6);
  mgmt.descriptor[0] = MGMT_DESC_TYP_AUTO_RATE_REQ| frame_length;

  if(power_control)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
          (TEXT("send_auto_rate: Auto power enabled\n")));
    mgmt.descriptor[1] = power_control;
  }
  if(antenna_diversity)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
          (TEXT("send_auto_rate: Antenna diversity enabled\n")));
    mgmt.descriptor[2] = antenna_diversity;
  }

  return ganges_send_mgmt_frame(Adapter, 
                                (UINT8 *)&mgmt,
                                frame_length + RSI_DESC_LEN);
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_qos_params
Description    : This function sends qos params to the firmware.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
pAdapter           |  X  |     |     | Adapter
wmmIe	           |     |     |     |
*END**************************************************************************/

GANGES_STATUS
ganges_send_qos_params
  (
    IN PRSI_ADAPTER Adapter,
    genIe_t         *wmmIe
  )
{
  INT32 ii, indx = 0;
  struct RSI_Mac_frame mgmt;

  ganges_memset(&mgmt, 0, sizeof(mgmt.frameType.qos_params)+RSI_DESC_LEN);

#define AC_VO    3
#define AC_VI    2
#define AC_BK    1
#define AC_BE    0
  for(ii=0;ii<4;ii++)
  {
    if(wmmIe->wmm.ac_param[ii].aci == AC_VO)
    {
      indx = 0;
      /* units of 32us */
      mgmt.frameType.qos_params.txop[indx]  = wmmIe->wmm.ac_param[ii].txop*32;
    }
    if(wmmIe->wmm.ac_param[ii].aci == AC_VI)
    {
      indx = 1;
      /* units of 32us */
      mgmt.frameType.qos_params.txop[indx]  = wmmIe->wmm.ac_param[ii].txop*32;
    }
    if(wmmIe->wmm.ac_param[ii].aci == AC_BE)
    {
      indx = 2;
    }
    if(wmmIe->wmm.ac_param[ii].aci == AC_BK)
    {
      indx = 3;
    }
    Adapter->acm_bits[indx] = wmmIe->wmm.ac_param[ii].acm;
    mgmt.descriptor[2] |= (wmmIe->wmm.ac_param[ii].acm << indx);
    RSI_DEBUG(RSI_ZONE_INFO, "ACM %x %x", wmmIe->wmm.ac_param[ii].acm,
                                          mgmt.descriptor[2]);
    mgmt.frameType.qos_params.aifsn[indx] = (wmmIe->wmm.ac_param[ii].aifsn - 2);
#if 1
    /* FIXME NETGEAR AP giving aifsn as 1 */
    if(mgmt.frameType.qos_params.aifsn[indx] < 0)
      mgmt.frameType.qos_params.aifsn[indx] = 0;
#endif
    mgmt.frameType.qos_params.cwmin[indx] = (1<<wmmIe->wmm.ac_param[ii].ECW_min)-1;
    mgmt.frameType.qos_params.cwmax[indx] = (1<<wmmIe->wmm.ac_param[ii].ECW_max)-1;
  }

  mgmt.descriptor[0] = MGMT_DESC_TYP_QOS_PARAM_REQ| 
                       (sizeof(mgmt.frameType.qos_params));
  return ganges_send_mgmt_frame(Adapter, 
                                (UINT8 *)&mgmt, 
                                sizeof(mgmt.frameType.qos_params)+
                                RSI_DESC_LEN);
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_rate_symbol_request
Description    : This function sends rate symbol request to firmware.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
pAdapter           |     |     |     | Adapter
*END**************************************************************************/

GANGES_STATUS
ganges_send_rate_symbol_request
  (
    IN PRSI_ADAPTER Adapter
  )
{
  struct RSI_Mac_frame mgmt;
  RSI_DEBUG(RSI_ZONE_INFO,
            "ganges_send_rate_symbol_request: Sending rate symbol req frame\n");
  ganges_memset(&mgmt, 0, sizeof(mgmt.frameType.rate_symb_req)+RSI_DESC_LEN);

  mgmt.frameType.rate_symb_req.rate_val[0] = RPS_RATE_01_PWR<<8 | 1;   /*1*/
  mgmt.frameType.rate_symb_req.rate_val[1] = RPS_RATE_02_PWR<<8 | 2;   /*2*/
  mgmt.frameType.rate_symb_req.rate_val[2] = RPS_RATE_5_5_PWR<<8 | 11; /*5.5*/
  mgmt.frameType.rate_symb_req.rate_val[3] = RPS_RATE_11_PWR<<8 | 11;    /*11*/
  mgmt.frameType.rate_symb_req.rate_val[4] = RPS_RATE_MCS0_PWR<<8 | 26;  /*MCS0*/
  mgmt.frameType.rate_symb_req.rate_val[5] = RPS_RATE_MCS1_PWR<<8 | 52;  /*MCS1*/
  mgmt.frameType.rate_symb_req.rate_val[6] = RPS_RATE_MCS2_PWR<<8 | 78;  /*MCS2*/
  mgmt.frameType.rate_symb_req.rate_val[7] = RPS_RATE_MCS3_PWR<<8 | 104; /*MCS3*/
  mgmt.frameType.rate_symb_req.rate_val[8] = RPS_RATE_MCS4_PWR<<8 | 156; /*MCS4*/
  mgmt.frameType.rate_symb_req.rate_val[9] = RPS_RATE_MCS5_PWR<<8 | 208; /*MCS5*/
  mgmt.frameType.rate_symb_req.rate_val[10] = RPS_RATE_MCS6_PWR<<8 | 234;/*MCS6*/
  mgmt.frameType.rate_symb_req.rate_val[11] = RPS_RATE_MCS7_PWR<<8 | 255;/*MCS7*/
  mgmt.frameType.rate_symb_req.rate_val[12] = RPS_RATE_48_PWR<<8 | 192;  /*48*/
  mgmt.frameType.rate_symb_req.rate_val[13] = RPS_RATE_24_PWR<<8 | 96;   /*24*/
  mgmt.frameType.rate_symb_req.rate_val[14] = RPS_RATE_12_PWR<<8 | 48;   /*12*/
  mgmt.frameType.rate_symb_req.rate_val[15] = RPS_RATE_06_PWR<<8 | 24;   /*6*/
  mgmt.frameType.rate_symb_req.rate_val[16] = RPS_RATE_54_PWR<<8 | 216;  /*54*/
  mgmt.frameType.rate_symb_req.rate_val[17] = RPS_RATE_36_PWR<<8 | 144;  /*36*/
  mgmt.frameType.rate_symb_req.rate_val[18] = RPS_RATE_18_PWR<<8 | 72;   /*18*/
  mgmt.frameType.rate_symb_req.rate_val[19] = RPS_RATE_09_PWR<<8 | 36;   /*9*/

  mgmt.descriptor[0] = MGMT_DESC_TYP_LD_RATE_SYM_REQ| 
                       (sizeof(mgmt.frameType.rate_symb_req));
  return ganges_send_mgmt_frame(Adapter, 
                                (UINT8 *)&mgmt, 
                                RSI_DESC_LEN +
                                sizeof(mgmt.frameType.rate_symb_req));
}

/*FUNCTION*********************************************************************
Function Name  : ganges_start_ibss
Description    : This function start the ibss network. if network already is 
                 there then join to that network.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter            |  X  |     |     | pointer to the Adapter structure
*END**************************************************************************/

GANGES_STATUS 
ganges_start_ibss
  (
    PRSI_ADAPTER Adapter
  )
{
  UINT32 idx = Adapter->SelectedBssidNum;
  /* Check IBSS network with given ssid already exist */
  /*FIXME check this is resetting or not when network not available case */
  if(idx == 255)
  {
    /* IBSS network with given ssid not found
     * even though we are not IBSS_CREATER create ibss region */
    if(Adapter->IbssCreator == IBSS_JOINER)
    {
      RSI_DEBUG(RSI_ZONE_INFO,"\nganges_start_ibss: IBSS network not found with given SSID");
      RSI_DEBUG(RSI_ZONE_INFO,"\nganges_start_ibss: Creating IBSS network");
      Adapter->IbssCreator = IBSS_CREATOR;
    }
    FSM_STATE = FSM_JOIN_REQ_SENT;
    return ganges_send_join_request(Adapter, NULL);
  }
  else
  {
    if(Adapter->IbssCreator == IBSS_CREATOR)
    {
      RSI_DEBUG(RSI_ZONE_INFO,"\nganges_start_ibss: IBSS network found with given SSID");
      RSI_DEBUG(RSI_ZONE_INFO,"\nganges_start_ibss: Joining to the existing IBSS network");
      Adapter->IbssCreator = IBSS_JOINER;
    }
    Adapter->Ibss_info.channelNum      = Adapter->scanConfirmVarArray[idx]->PhyParam[0] ;
    Adapter->Ibss_info.Beacon = Adapter->scanConfirmVarArray[idx]->Beacon ;
    Adapter->Ibss_info.ATIMWindow = 0 ;
    ganges_memcpy(Adapter->Ibss_info.BSSID, 
                  Adapter->scanConfirmVarArray[idx]->BSSID,
                  6);
    FSM_STATE = FSM_JOIN_REQ_SENT;
    return ganges_send_join_request(Adapter,NULL);
  }      
}


/*FUNCTION*********************************************************************
Function Name  : ganges_measure_auto_nfm_rssi
Description    : This function measures the auto rssi nfm value if required 
                 then it will give Base band gain request also.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter            |  X  |     |     | pointer to the Adapter structure.
nfm_val            |  X  |     |     | nfm value
*END**************************************************************************/

GANGES_STATUS
ganges_measure_auto_nfm_rssi
  (
   IN PRSI_ADAPTER Adapter,
    IN INT8 nfm_val
  )
{
  GANGES_STATUS Status=GANGES_STATUS_SUCCESS;
  RSI_DEBUG(RSI_ZONE_NFM_PATH,"ganges_measure_auto_nfm_rssi: Noise RSSI 0x%x\n", nfm_val);
  Adapter->rssi_nf_auto_total+=nfm_val;
  Adapter->rssi_nf_auto_pkt_ct++;

  /*Averager for auto_average packets */
  if(Adapter->rssi_nf_auto_pkt_ct == Adapter->rssi_nf_auto_no_pkts_to_avg)
  {
    INT16 rssi_nf_auto_avg;

    rssi_nf_auto_avg = Adapter->rssi_nf_auto_total/
                       Adapter->rssi_nf_auto_no_pkts_to_avg;
    RSI_DEBUG(RSI_ZONE_NFM_PATH,"ganges_measure_auto_nfm_rssi: Calculating avg Noise RSSI %x\n", 
                                   rssi_nf_auto_avg);

    RSI_DEBUG(RSI_ZONE_NFM_PATH,"ganges_measure_auto_nfm_rssi: Updating noise rssi\n");
    if(ganges_send_update_noise_rssi_request(Adapter, rssi_nf_auto_avg)!=GANGES_STATUS_SUCCESS)
	    Status = GANGES_STATUS_FAILURE;
    Adapter->rssi_nf_auto_pkt_ct = 0;
    Adapter->rssi_nf_auto_total = 0;
  }
  return Status;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_reset_mac
Description    : This routine send the reset mac frame to the firmware.
                 With this internal data structures will be resetted.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter            |  X  |     |     | pointer to the Adapter structure.
*END**************************************************************************/

GANGES_STATUS
ganges_send_reset_mac
  (
    IN PRSI_ADAPTER Adapter
  )
{
  struct RSI_Mac_frame mgmt;
  ganges_memset(&mgmt, 0, RSI_DESC_LEN);

  mgmt.descriptor[0] = MGMT_DESC_TYP_RESET_MAC_REQ|8; 
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_reset_mac: Sending reset MAC frame\n");  
  return ganges_send_mgmt_frame(Adapter, 
                                (UINT8*)&mgmt, 
                                RSI_DESC_LEN+8);
}


/*FUNCTION*********************************************************************
Function Name  : ganges_send_reset_cont_tx
Description    : This routine send the reset_cont_tx mac frame to the firmware.
Returned Value : 
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter            |  X  |     |     | pointer to the Adapter structure.
*END**************************************************************************/

GANGES_STATUS
ganges_send_reset_cont_tx
  (
    IN PRSI_ADAPTER Adapter
  )
{
  struct RSI_Mac_frame mgmt;
  ganges_memset(&mgmt,0,RSI_DESC_LEN);

  mgmt.descriptor[0] = MGMT_DESC_TYP_RESET_CONT_TX_REQ|8;

  return ganges_send_mgmt_frame(Adapter,
                                (UINT8*)&mgmt,
                                RSI_DESC_LEN+8);
}

/*FUNCTION*********************************************************************
Function Name  : ganges_is_AP_supported_rate
Description    : This function checks given AP supports the specified rate or 
                 not. 
Returned Value : Returns 0 in case of success or a negative value incase of failure          
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
scanCOnfirm	   |  X  |     |     | Info regarding BSSID received in scan
rate		   |  X  |     |     | AP supported rate	
*END**************************************************************************/

GANGES_STATUS
ganges_is_AP_supported_rate
  (
    IN PScanConfirm scanConfirm,
    IN UINT8        rate
  )
{
  UINT32 ii;

  for(ii=0; ii<scanConfirm->SupportedRatesLen; ii++)
  {
    if((scanConfirm->SupportedRates[ii] & 0x7F) == rate )
      return GANGES_STATUS_SUCCESS;
  }

  for(ii=0; ii<scanConfirm->ExtSupportedRatesLen; ii++)
  {
    if((scanConfirm->ExtSupportedRates[ii] & 0x7F) == rate )
      return GANGES_STATUS_SUCCESS;
  }

  return GANGES_STATUS_FAILURE;
}


/*FUNCTION*********************************************************************
Function Name  : ganges_get_station_statistics
Description    : This function gets the sation statistics. 
Returned Value : GANGES_STATUS_SUCCESS on supports
                 GANGES_STATUS_FAILURE on don't supports
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter 	   |  X  |     |     | Ptr to Adapter structure
stats_interval	   |  X  |     |     | Stats interval	
*END**************************************************************************/

GANGES_STATUS
ganges_get_station_statistics
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT32       stats_interval    
  )
{
  GANGES_STATUS Status = GANGES_STATUS_SUCCESS;
  struct RSI_Mac_frame mgmt;

  /* Intialize the descriptor with zero */
  ganges_memset(mgmt.descriptor, 0, RSI_DESC_LEN);
  
  mgmt.descriptor[0] = MGMT_DESC_TYP_STATS_REQ|16;
  mgmt.descriptor[2] = stats_interval;

  ganges_send_mgmt_frame(Adapter, 
                         (UINT8 *)&mgmt, 
                         RSI_DESC_LEN);
  
  return Status;
}


/*FUNCTION*********************************************************************
Function Name  : ganges_send_LMAC_prog_val_1
Description    : This function sends LMAC programming values to TA
Returned Value : GANGES_STATUS_SUCCESS on success will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter 	   |  X  |     |     | Ptr to Adapter structure
length  	   |  X  |     |     | 	
data		   |  X  |     |     |
*END**************************************************************************/

VOID 
ganges_send_LMAC_prog_val_1
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT16       length,
    IN UINT16       *data
  )
{
  struct RSI_Mac_frame mgmt;

  /* Intialize the descriptor with zero */
  ganges_memset(mgmt.descriptor, 0, RSI_DESC_LEN);
  mgmt.frameType.lmac_prog_val_1.length = length;  /* Length in words */

  length *= 2; 
  ganges_memcpy(mgmt.frameType.lmac_prog_val_1.data, 
 	        data, 
		length); 

  length += sizeof(mgmt.frameType.lmac_prog_val_1.length);
  mgmt.descriptor[0] = (MGMT_DESC_TYP_LMAC_PROG_1_REQ | length);

  ganges_send_TA_mgmt_frame(Adapter, 
                            (UINT8 *)&mgmt, 
                            (RSI_DESC_LEN + length));
  return;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_LMAC_prog_vals
Description    : This function sends LMAC programming values to TA
Returned Value : GANGES_STATUS_SUCCESS on success will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter 	   |  X  |     |     | Ptr to Adapter structure
length  	   |  X  |     |     | 	
data		   |  X  |     |     |
*END**************************************************************************/

VOID 
ganges_send_LMAC_prog_vals
  (
    IN PRSI_ADAPTER Adapter
  )
{
  struct RSI_Mac_frame mgmt;
  UINT16 rf_wakeup_vals = Adapter->RF_wakeup_prog_val[0];
  UINT16 bb_wakeup_vals = Adapter->BB_wakeup_prog_val[0];
  UINT16 rf_sleep_vals  = Adapter->RF_sleep_prog_val[0];
  UINT16 bb_sleep_vals  = Adapter->BB_sleep_prog_val[0];
  UINT16 length = 0;
 
  RSI_DEBUG(RSI_ZONE_INFO,
            "ganges_send_LMAC_prog_vals: Sending pwr save vals to LMAC\n");
  /* Intialize the descriptor with zero */
  ganges_memset(mgmt.descriptor, 0, RSI_DESC_LEN);

  ganges_memcpy(&mgmt.frameType.lmac_prog_vals.data[0],
	        &Adapter->BB_wakeup_prog_val[0],
		bb_wakeup_vals*2+2);	  
  length +=bb_wakeup_vals+1;

  //mgmt.frameType.lmac_prog_vals.data[length] = rf_type; 
  ganges_memcpy(&mgmt.frameType.lmac_prog_vals.data[length],
	        &Adapter->RF_wakeup_prog_val[0],
		rf_wakeup_vals*2+2);	
  length +=rf_wakeup_vals+1;

  ganges_memcpy(&mgmt.frameType.lmac_prog_vals.data[length],
	        &Adapter->BB_sleep_prog_val[0],
		bb_sleep_vals*2+2);	
  length +=bb_sleep_vals+1;

  ganges_memcpy(&mgmt.frameType.lmac_prog_vals.data[length],
	        &Adapter->RF_sleep_prog_val[0],
		rf_sleep_vals*2+2);	
  length +=rf_sleep_vals+1;
  length *=2;
  mgmt.descriptor[0] = (MGMT_DESC_TYP_LMAC_PROG_VALS | length);
  mgmt.descriptor[1] = (1<<5);

  ganges_send_mgmt_frame(Adapter, 
                            (UINT8 *)&mgmt, 
                            (RSI_DESC_LEN + length));
  return;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_LMAC_prog_val_2
Description    : This function sends LMAC programming values to TA
Returned Value : GANGES_STATUS_SUCCESS on success will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter 	   |  X  |     |     | Ptr to Adapter structure
length  	   |  X  |     |     | 	
data		   |  X  |     |     |
*END**************************************************************************/

VOID 
ganges_send_LMAC_prog_val_2
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT16       length,
    IN UINT16       *data
  )
{
  struct RSI_Mac_frame mgmt;

  /* Intialize the descriptor with zero */
  ganges_memset(mgmt.descriptor, 0, RSI_DESC_LEN);
  mgmt.frameType.lmac_prog_val_2.length = length;  /* Length in words */

  length *= 2;
  ganges_memcpy(mgmt.frameType.lmac_prog_val_2.data, 
  		data, 
		length); 
  length = length + sizeof(mgmt.frameType.lmac_prog_val_2.length) + 
           sizeof(mgmt.frameType.lmac_prog_val_2.rf_type);
  mgmt.descriptor[0] = (MGMT_DESC_TYP_LMAC_PROG_2_REQ | length);
  ganges_send_TA_mgmt_frame(Adapter, 
                            (UINT8 *)&mgmt, 
                            (RSI_DESC_LEN + length));
  
  return;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_LMAC_prog_val_3
Description    : This function sends LMAC programming values to TA
Returned Value : GANGES_STATUS_SUCCESS on success will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter 	   |  X  |     |     | Ptr to Adapter structure
length  	   |  X  |     |     | 	
data		   |  X  |     |     |
*END**************************************************************************/

VOID 
ganges_send_LMAC_prog_val_3
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT16       length,
    IN UINT16       *data
  )
{
  struct RSI_Mac_frame mgmt;

  /* Intialize the descriptor with zero */
  ganges_memset(mgmt.descriptor, 0, RSI_DESC_LEN);
  
  mgmt.descriptor[0] = (MGMT_DESC_TYP_LMAC_PROG_3_REQ | length);
  ganges_memcpy(mgmt.frameType.lmac_prog_val_3.data, data, length); 

  ganges_send_TA_mgmt_frame(Adapter, 
                            (UINT8 *)&mgmt, 
                            (RSI_DESC_LEN + length));
  
  return;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_load_STA_config
Description    : This function sends Station configuration frame to TA
Returned Value : GANGES_STATUS_SUCCESS on success else
                 GANGES_STATUS_FAILURE will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter 	   |  X  |     |     | Ptr to Adapter structure
*END**************************************************************************/

VOID 
ganges_send_load_STA_config
  (
    IN PRSI_ADAPTER Adapter
  )
{
  struct RSI_Mac_frame mgmt;
  UINT16 length;

  /* Intialize the descriptor with zero */
  ganges_memset(mgmt.descriptor, 0,RSI_DESC_LEN);
  /* Copy BSSID */
  ganges_memcpy((INT8 *) &mgmt.descriptor[4],
                &Adapter->connectedAP.BSSID,
                sizeof(NDIS_802_11_MAC_ADDRESS));
  
  ganges_memset(&mgmt.frameType.sta_config,0,sizeof(mgmt.frameType.sta_config));

  /* Protocol type (B/BG/11n/11n_mixed) */
  if( Adapter->ProtocolType == ProtoType802_11B)
  {
    mgmt.frameType.sta_config.supp_protocols = SUPPORTED_PROTO_B_ONLY;
  }
  else if( Adapter->ProtocolType == ProtoType802_11BG)
  {
    mgmt.frameType.sta_config.supp_protocols = SUPPORTED_PROTO_MIXED_BG;
  }
  else if( Adapter->ProtocolType == ProtoType802_11N)
  {
    mgmt.frameType.sta_config.supp_protocols = SUPPORTED_PROTO_11N_ONLY;
  }
  else if( Adapter->ProtocolType == ProtoType802_11N_mixed)
  {
    mgmt.frameType.sta_config.supp_protocols = SUPPORTED_PROTO_MIXED_11N;
  }

  /* Access policy (NO_QOS/EDCA/HCCA/MIXED) */
  if(Adapter->qos_type == RPS_NO_QOS)
  {
    mgmt.frameType.sta_config.supp_protocols |= (SUPPORTED_NO_QOS << 2);
  }
  else if(Adapter->qos_type == RPS_WMM)
  {
    mgmt.frameType.sta_config.supp_protocols |= (SUPPORTED_EDCA << 2);
  }
  else if(Adapter->qos_type == RPS_WMMSA)
  {
    mgmt.frameType.sta_config.supp_protocols |= (SUPPORTED_HCCA << 2);
  }
  else if(Adapter->qos_type == RPS_WMM_AND_WMMSA)
  {
    mgmt.frameType.sta_config.supp_protocols |= (SUPPORTED_MIXED_MODE << 2);
  }
  if(Adapter->amsdu_param.enable)
  {
    mgmt.frameType.sta_config.supp_features |= SUPPORT_A_MSDU_BIT;

    /* A-MSDU parameters */
    mgmt.frameType.sta_config.amsdu_len = (UINT8)Adapter->amsdu_param.max_amsdu_len;
  }
  if(Adapter->ampdu_param.enable)
  {
    mgmt.frameType.sta_config.supp_features |= SUPPORT_A_MPDU_BIT;
 
    /* A-MPDU parameters */
    /*mgmt.frameType.sta_config.ampdu_param = (Adapter->ampdu_param.rx_factor & 0x3);
    mgmt.frameType.sta_config.ampdu_param |= ((Adapter->ampdu_param.density & 0x7) << 2);*/ /*FIXME*/
  }
  if( Adapter->enable_link_adaptation)
  {
    mgmt.frameType.sta_config.supp_features |= SUPPORT_PERIODIC_LINK_ADAPT;
  }
  if( (Adapter->pwr_save_param.ac_vo) ||
      (Adapter->pwr_save_param.ac_vi) ||
      (Adapter->pwr_save_param.ac_bk) ||
      (Adapter->pwr_save_param.ac_be))
  {
    mgmt.frameType.sta_config.supp_features |= SUPPORT_UAPSD_BIT;
  }
  
  if((Adapter->blk_ack_param.txenable) || (Adapter->blk_ack_param.rxenable))
  {
    mgmt.frameType.sta_config.supp_features |= SUPPORT_BA_BIT;

#if 0
    if(Adapter->blk_ack_param.delayed_ba)
    {
      mgmt.frameType.sta_config.supp_features |= SUPPORT_DELAYED_BA_BIT;
    }
#endif

    /* Num of BA supported tids and supported tids */
    mgmt.frameType.sta_config.num_tids = 2;
    /* FIXME insert all the supported tids */
    mgmt.frameType.sta_config.supp_tids = 0;
  }
  if(Adapter->no_ack)
  {
    mgmt.frameType.sta_config.supp_features |= SUPPORT_NO_ACK_BIT;
  }

  /* Fragmentation threshold */
  mgmt.frameType.sta_config.frag_thr = Adapter->frag_threshold;

  /* Data rate */
  mgmt.frameType.sta_config.data_rate = Adapter->dataRate;

  /* Beacon interval */
  mgmt.frameType.sta_config.beacon_intr = Adapter->connectedAP.Beacon;
  
  /* Listen interval */
  mgmt.frameType.sta_config.listen_intr = MAX_LISTEN_INTRVL;
  
  /* DTIM Period */
  mgmt.frameType.sta_config.dtim_period = Adapter->connectedAP.DTIM_Param & 0xFF;
  
  length = sizeof(mgmt.frameType.sta_config);
  mgmt.descriptor[0] = (MGMT_DESC_TYP_LOAD_STA_CONFIG | length);

  ganges_send_TA_mgmt_frame( Adapter, 
                            (UINT8 *)&mgmt, 
                            (RSI_DESC_LEN + length));

  return;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_power_save_req
Description    : This function sends power save request frame to TA
Returned Value : GANGES_STATUS_SUCCESS on success else
                 GANGES_STATUS_FAILURE will be returned
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter 	   |  X  |     |     | Ptr to Adapter structure
*END**************************************************************************/

VOID 
ganges_send_power_save_req
  (
    IN PRSI_ADAPTER Adapter,
    UINT16  pwr_save_enable
  )
{
  UINT8    ta_mgmt[32]={0};
  
  Adapter->halt_flag=1;	
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_send_power_save_req: ");
  ganges_memset(&ta_mgmt, 0, RSI_DESC_LEN); /* FIXME  , frame body too! */
  if(!pwr_save_enable)
  {
    RSI_DEBUG(RSI_ZONE_INFO,"disable\n");
    *(UINT16 *)&ta_mgmt[2] = 0; /* Indicates power save disable */
  }
  else
  {
    if(pwr_save_enable==1)
    {
      RSI_DEBUG(RSI_ZONE_INFO,"Regular pwr save enable\n");
    }
    else 
    {
      RSI_DEBUG(RSI_ZONE_INFO,"Deep sleep enable\n");
    }

    *(UINT16 *)&ta_mgmt[2] = pwr_save_enable; /* 1 Indicates regular power save enable */
                                             /*  2 Indicates deepsleep */

    /* Indicate WMM PS params in 0-3 bits of word 2 of the FD */
    *(UINT16 *)&ta_mgmt[4] = ( ( Adapter->pwr_save_param.ac_vo |
                               ( Adapter->pwr_save_param.ac_vi << 1) |
                               ( Adapter->pwr_save_param.ac_bk << 2) |
                               ( Adapter->pwr_save_param.ac_be << 3))| (Adapter->uapsd_wakeup<<8));
    *(UINT16 *)&ta_mgmt[6]  = Adapter->tx_bytes; 
    *(UINT16 *)&ta_mgmt[8]  = Adapter->rx_bytes; 
    *(UINT16 *)&ta_mgmt[10] = Adapter->traffic_interval; 
  }
  *(UINT16 *)&ta_mgmt[0] = ((MGMT_DESC_TYP_PWR_SAVE_REQ)|16);
  ganges_send_TA_mgmt_frame( Adapter, 
                            &ta_mgmt[0], 
                            RSI_DESC_LEN+16);
}

/*FUNCTION*********************************************************************
Function Name  : ganges_send_program_BB_registers
Description    : This management frame programs the given BB registers.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |  X  |     |     | pointer to our adapter
no_registers     |  X  |     |     | no of registers to proram
bb_registers     |  X  |     |     | registers to program
*END**************************************************************************/

GANGES_STATUS 
ganges_send_program_BB_registers
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT16       no_registers,
    IN UINT16       bb_registers[]
  )
                        
{
  struct      RSI_Mac_frame mgmt;
  RSI_DEBUG(RSI_ZONE_INFO,
            "ganges_send_program_BB_registers: Programing BB registers\n");
  memset(mgmt.descriptor,0,16);

  mgmt.descriptor[0] = (MGMT_DESC_TYP_PRG_BB_REG | 
                        (2+ (no_registers * 2)));
  mgmt.descriptor[2] |=0x01;
  mgmt.frameType.reset.bb_val_length = no_registers;
  memcpy(mgmt.frameType.reset.bb_program_val, 
         &bb_registers[0], 
         no_registers*2);
  return ganges_send_mgmt_frame(Adapter, 
                                (UINT8*)&mgmt, 
                                MGMT_FRAME_DESC_SZ + 2 + (no_registers*2));
}

/*FUNCTION*********************************************************************
Function Name  : ganges_set_channel_type
Description    : This function sets the channel type to given type.
Returned Value : 
Parameters     :

----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
Adapter                     |  X  |     |     | Ponter to the adapter structure
ChannelType                 |  X  |     |     | channel type which is going to 
                                                set
*END**************************************************************************/

VOID 
ganges_set_channel_type
  (
    PRSI_ADAPTER Adapter, 
    RPS_CHANNEL_TYPE ChannelType
  )
{
  switch (ChannelType)
  {
     case All_Channel:
       RSI_DEBUG(RSI_ZONE_INFO, 
                  TEXT("set_channel_type: Scan all channel\n"));
       Adapter->ScanAllChannel = TRUE;
       Adapter->ScanChannelNum = 0;
       break;
     default:
       RSI_DEBUG(RSI_ZONE_INFO, 
                  TEXT("set_channel_type: Scan channel %d\n"), ChannelType);
       Adapter->ScanAllChannel = FALSE;
       Adapter->ScanChannelNum = ChannelType;
       Adapter->Ibss_info.channelNum  = ChannelType;
       break;
  }
}

/*FUNCTION*********************************************************************
Function Name  : ganges_set_protocol_type
Description    : This function changes the protocol type
Returned Value : 
Parameters     :

----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
Adapter                     |  X  |     |     | Ponter to the adapter structure
ProtocolType                |  X  |     |     | Protocol type which is going to 
                                                set
*END**************************************************************************/

VOID 
ganges_set_protocol_type
  (
    PRSI_ADAPTER Adapter, 
    RPS_PROTOCOL_TYPE ProtocolType
  )
{
  switch (ProtocolType)
  {
     case WLAN_802_11_A:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_protocol_type: A\n"));
       Adapter->ProtocolType = ProtoType802_11A;
       break;
     case WLAN_802_11_B:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_protocol_type: B\n"));
       Adapter->ProtocolType = ProtoType802_11B;
       break;
     case WLAN_802_11_G:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_protocol_type: G\n"));
       Adapter->ProtocolType = ProtoType802_11G;
       break;
     case WLAN_802_11_N:
       RSI_DEBUG(RSI_ZONE_INFO, (TEXT("set_protocol_type: N\n")));
       Adapter->ProtocolType = ProtoType802_11N;
       break;
  }
}

/*FUNCTION*********************************************************************
Function Name  : ganges_set_txrate
Description    : This function set the given tx rate
Returned Value : 
Parameters     :

----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
Adapter                     |  X  |     |     | Ponter to the adapter structure
TxRate                      |  X  |     |     | Tx Rate which is going to set
*END**************************************************************************/

VOID 
ganges_set_txrate
  (
    PRSI_ADAPTER Adapter, 
    RPS_TX_RATE  TxRate
  )
{
  switch (TxRate)
  {
     case RATE_1_MBPS:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: 1Mbps\n"));
       Adapter->dataRate = RSI_RATE_1|(RPS_RATE_01_PWR<<8);
       break;
     case RATE_2_MBPS:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: 2Mbps\n"));
       Adapter->dataRate = RSI_RATE_2|(RPS_RATE_02_PWR<<8);
       break;
     case RATE_5_5_MBPS:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: 5.5Mbps\n"));
       Adapter->dataRate = RSI_RATE_5_5|(RPS_RATE_5_5_PWR<<8);
       break;
     case RATE_11_MBPS:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: 11Mbps\n"));
       Adapter->dataRate = RSI_RATE_11|(RPS_RATE_11_PWR<<8);
       break;
     case RATE_6_MBPS:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: 6Mbps\n"));
       Adapter->dataRate = RSI_RATE_6|(RPS_RATE_06_PWR<<8);
       break;
     case RATE_9_MBPS:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: 9Mbps\n"));
       Adapter->dataRate = RSI_RATE_9|(RPS_RATE_09_PWR<<8);
       break;
     case RATE_12_MBPS:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: 12Mbps\n"));
       Adapter->dataRate = RSI_RATE_12|(RPS_RATE_12_PWR<<8);
       break;
     case RATE_18_MBPS:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: 18Mbps\n"));
       Adapter->dataRate = RSI_RATE_18|(RPS_RATE_18_PWR<<8);
       break;
     case RATE_24_MBPS:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: 24Mbps\n"));
       Adapter->dataRate = RSI_RATE_24|(RPS_RATE_24_PWR<<8);
       break;
     case RATE_36_MBPS:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: 36Mbps\n"));
       Adapter->dataRate = RSI_RATE_36|(RPS_RATE_36_PWR<<8);
       break;
     case RATE_48_MBPS:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: 48Mbps\n"));
       Adapter->dataRate = RSI_RATE_48|(RPS_RATE_48_PWR<<8);
       break;
     case RATE_54_MBPS:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: 54Mbps\n"));
       Adapter->dataRate = RSI_RATE_54|(RPS_RATE_54_PWR<<8);
       break;
     case RATE_AUTO:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: Auto Rate\n"));
       /* Adapter->dataRate = 0x000b;*/
       Adapter->dataRate = RSI_RATE_54|(RPS_RATE_54_PWR<<8);
       break;
     case RATE_MCS0:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: MCS0\n"));
       Adapter->dataRate = RSI_RATE_MCS0|(RPS_RATE_MCS0_PWR<<8);
       break;
     case RATE_MCS1:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: MCS1\n"));
       Adapter->dataRate = RSI_RATE_MCS1|(RPS_RATE_MCS1_PWR<<8);
       break;
     case RATE_MCS2:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: MCS2\n"));
       Adapter->dataRate = RSI_RATE_MCS2|(RPS_RATE_MCS2_PWR<<8);
       break;
     case RATE_MCS3:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: MCS3\n"));
       Adapter->dataRate = RSI_RATE_MCS3|(RPS_RATE_MCS3_PWR<<8);
       break;
     case RATE_MCS4:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: MCS4\n"));
       Adapter->dataRate = RSI_RATE_MCS4|(RPS_RATE_MCS4_PWR<<8);
       break;
     case RATE_MCS5:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: MCS5\n"));
       Adapter->dataRate = RSI_RATE_MCS5|(RPS_RATE_MCS5_PWR<<8);
       break;
     case RATE_MCS6:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: MCS6\n"));
       Adapter->dataRate = RSI_RATE_MCS6|(RPS_RATE_MCS6_PWR<<8);
       break;
     case RATE_MCS7:
       RSI_DEBUG(RSI_ZONE_INFO, TEXT("set_txrate: MCS7\n"));
       Adapter->dataRate = RSI_RATE_MCS7|(RPS_RATE_MCS7_PWR<<8);
       break;
  }

  Adapter->TxRate = TxRate;
}


/*FUNCTION*********************************************************************
Function Name  : ganges_do_adapter_Reset
Description    : This function reset our adapter, and reset some variables also.
Returned Value : 
Parameters     :

----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
Adapter                     |  X  |     |     | Ponter to the adapter structure
*END**************************************************************************/

VOID 
ganges_do_adapter_Reset
  (
    PRSI_ADAPTER Adapter
  )
{
  /* TODO we should disconnect here */

  /* add the code which we want initialize in reset */
  Adapter->JoinRequestPending = 0;  
  
  Adapter->BufferFull = FALSE;
  Adapter->power_sava_fsm = PWR_FSM_NORMAL;
  //ganges_mgmt_init(Adapter);
  if(FSM_STATE==FSM_OPEN)
  {
    if((PS_FSM_STATE != PS_FSM_DISABLE) && (PS_FSM_STATE != PS_FSM_NORMAL))
    {
     ganges_Reset_Event(&PwrSaveEvent);
     ganges_send_power_save_req(Adapter,DISABLE_PWR_SAVE);
     RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Disabling Pwr save\n");
     ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);
     RSI_DEBUG(RSI_ZONE_OID,"ganges_ioctl: Wokeup on event\n");
    }
    ganges_send_disassociate_request(Adapter,
 			             Adapter->connectedAP.BSSID,
				     0x03);
  } 
  FSM_STATE = FSM_NO_STATE;
  return;
  /*ganges_init_adapter(Adapter);*/
}

/*FUNCTION*********************************************************************
Function Name  : ganges_do_scan
Description    : This function send scan request to the firmware.
                 before giving scan request free the scanconfirmarray memory.
Returned Value : 
Parameters     :

----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
Adapter                     |  X  |     |     | Ponter to the adapter structure
*END**************************************************************************/

VOID 
ganges_do_scan
  (
    IN PRSI_ADAPTER Adapter,
    IN UINT8        flag,
    IN NDIS_802_11_SSID *ssid  
  )
{
  UINT32 status=GANGES_STATUS_SUCCESS;
  if((PS_FSM_STATE == PS_FSM_DEEP_SLEEP)|| 
     (Adapter->halt_flag==1))
  {
     RSI_DEBUG(RSI_ZONE_ERROR,"ganges_do_scan: Scanning not allowed in deep sleep mode\n");
     return;
  }
  else if((PS_FSM_STATE != PS_FSM_DISABLE)&& (FSM_STATE != FSM_OPEN))
  {
    if(Adapter->power_state==0)
    {
    UINT8 byte=0x01;
    ganges_Reset_Event(&PwrSaveEvent);
    ganges_send_ssp_wakeup_cmd(Adapter,byte);
    ganges_send_ssp_wakeup_cmd_read(Adapter);
    Adapter->halt_flag=1;
    RSI_DEBUG(RSI_ZONE_INFO,"ganges_do_scan: Writing into deep sleep register: %d\n",PS_FSM_STATE); 
    status = ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);
    RSI_DEBUG(RSI_ZONE_INFO,"ganges_mgmt_fsm: Wokeup event\n");
    Adapter->halt_flag=0;
    }
    else
    {
      Adapter->power_state=0;
    }
  }

  if((FSM_STATE == FSM_NO_STATE) || (FSM_STATE == FSM_OPEN))
  //if((FSM_STATE == FSM_NO_STATE))
  {
#ifdef GANGES_PER_MODE
    /* only passive scan allowed in PER mode */
    if(Adapter->ppe_per_mode == TRUE)
    {
      ganges_send_probe_request(Adapter, PASSIVE_SCAN,Adapter->ScanChannelNum,1,NULL);
      return;   
    }
#endif
    if(Adapter->ScanAllChannel == TRUE)
    {
      if(Adapter->ScanChannelNum == 0)
      {
        Adapter->ScanChannelNum++;
      }
      else
      {
        RSI_DEBUG(RSI_ZONE_INFO,TEXT("All channel scan in progress \n"));
        return;
      }
    }
    
    if(FSM_STATE == FSM_OPEN)
    {        
      /* We don't know when this state will come back 
       * so don't go into this state */

      {
       if(Adapter->ScanAllChannel == TRUE)
       {
         Adapter->ScanChannelNum = 0;
       }
      }
    }
    else
    { 
      FSM_STATE = FSM_SCAN_REQ_SENT;
      if(Adapter->active_scan)
      {        
        if(flag==SSID_SPECIFIC_SCAN)
        {
          RSI_DEBUG(RSI_ZONE_INFO,"Sending directed probe request\n");
          ganges_send_probe_request(Adapter, ACTIVE_SCAN,Adapter->ScanChannelNum, 1, (UINT8 *)ssid);
        }
        else 
        {
          /*Free already allocated bssid list */
          ganges_free_bssid_list(Adapter);
          ganges_send_probe_request(Adapter, ACTIVE_SCAN,Adapter->ScanChannelNum, 1, NULL);
        }
      }
      else
      {
        ganges_free_bssid_list(Adapter);
        ganges_send_probe_request(Adapter, PASSIVE_SCAN,Adapter->ScanChannelNum,1,NULL);
      }
    }
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_INFO, 
              TEXT("ganges_do_scan: scan request not allowed in this state %s\n"),
              fsm_states_str[FSM_STATE]); 
  }
  
  return;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_set_ssid
Description    : This function connects the specified AP, If already connected
                 to other than this AP, send disassociate and connects.
Returned Value : 
Parameters     :

----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
Adapter                     |  X  |     |     |  pointer to Adapter
ssid                        |  X  |     |     |  pointer to NDIS_802_11_SSID
                                                 which we are going to connect
----------------------------+-----+-----+-----+------------------------------
*END**************************************************************************/

VOID 
ganges_set_ssid
  (
    PRSI_ADAPTER      Adapter,
    NDIS_802_11_SSID *ssid
  )
{
  UINT32  ii;
  UINT32  join_pend_status = Adapter->JoinRequestPending;
        
#ifdef GANGES_PER_MODE
  if(Adapter->ppe_per_mode==TRUE)
  {
    RSI_DEBUG(RSI_ZONE_OID,
               TEXT("set_info:Connect not allowed in PER mode\n"));
    return;
  }
#endif
  if((FSM_STATE != FSM_NO_STATE) && (FSM_STATE != FSM_OPEN)&&
     (FSM_STATE != FSM_SCAN_REQ_SENT))
  {
    RSI_DEBUG(RSI_ZONE_OID,
               TEXT("set_info: Card not yet ready\n"));
    return;
  }

  Adapter->JoinRequestPending = 0;
  RSI_DEBUG(RSI_ZONE_OID,
               TEXT("set_info: SSIDLen %d\n"), ssid->SsidLength);

  if(ssid->SsidLength == 0) 
  {
    RSI_DEBUG(RSI_ZONE_OID,
               TEXT("set_info: SSIDLen 0/32\n"));
    return;
  }
  if(ssid->SsidLength == 32)
  {
    for(ii=0;ii<32;ii++)
    {
      if(ssid->Ssid[ii] <0x20 || ssid->Ssid[ii] > 0x7E)
      {
        RSI_DEBUG(RSI_ZONE_OID, 
                     TEXT("set_ssid: ssid len 32 and non ascii value\n"));
        /* I think this type request means we have to disconnect */
        if(FSM_STATE == FSM_OPEN)
        {
          RSI_DEBUG(RSI_ZONE_OID, 
                     TEXT("set_ssid: sending deauthentication\n"));
          
          if(Adapter->NetworkType == Ndis802_11IBSS)
          {
            FSM_STATE = FSM_NO_STATE; 
            ganges_send_reset_mac(Adapter);
	    //ganges_carrier_off(Adapter->dev);
          }
          else 
          {
            if((PS_FSM_STATE != PS_FSM_DISABLE) && (PS_FSM_STATE != PS_FSM_NORMAL))
            {
              ganges_Reset_Event(&PwrSaveEvent);
              ganges_send_power_save_req(Adapter,DISABLE_PWR_SAVE);
              RSI_DEBUG(RSI_ZONE_OID,(TEXT("ganges_ioctl: Disabling Pwr save\n")));
              ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);
              RSI_DEBUG(RSI_ZONE_OID,(TEXT("ganges_ioctl: Wokeup on event\n")));
            }
            ganges_send_DeAuthentication_request(Adapter, 
                                     Adapter->connectedAP.BSSID,
				     0x03);
          }
        }
        return;
      }
    }
  }
  if(FSM_STATE == FSM_IBSS_CREATED)
  {
    RSI_DEBUG(RSI_ZONE_OID,TEXT("set_ssid: IBSS already created\n"));
    return;
  }

  if(FSM_STATE == FSM_DEEP_SLEEP)
  {
    RSI_DEBUG(RSI_ZONE_OID,TEXT("set_ssid: join in deep sleep state\n"));
    return;
  }
  /* if scan in progress set join request pending */
  if(FSM_STATE == FSM_SCAN_REQ_SENT)
  {
    RSI_DEBUG(RSI_ZONE_OID, TEXT("set_ssid: join pending %d\n"), 
                             ssid->SsidLength);
    Adapter->JoinRequestPending = 1;
    ganges_memcpy(&Adapter->pssid, ssid,sizeof(NDIS_802_11_SSID));
    return;
  } 
  if((FSM_STATE == FSM_OPEN) && 
     (Adapter->NetworkType == Ndis802_11Infrastructure))
  {
    /* connected to AP, so first disassociate */
    RSI_DEBUG(RSI_ZONE_OID, TEXT("set_ssid: Already connected\n"));

    if(Adapter->authMode != Ndis802_11AuthModeWPA2)
    {
      if(Adapter->connectedAP.SSIDLen == ssid->SsidLength)
      {
        if(ganges_memcmp(&Adapter->connectedAP.SSID[0],
                  ssid->Ssid,
                  ssid->SsidLength) == 0)
        {
          RSI_DEBUG(RSI_ZONE_OID, 
                     TEXT("set_ssid: tried to connect connected AP\n"));
          return;
        }
              
        /* NdisMIndicateStatus(Adapter->AdapterHandle,
                            NDIS_STATUS_MEDIA_DISCONNECT, 
                            (PVOID) 0, 
                            0); 
        NdisMIndicateStatusComplete(Adapter->AdapterHandle); */
        RSI_DEBUG(RSI_ZONE_OID, TEXT("set_ssid: disassociating\n"));
        if((PS_FSM_STATE != PS_FSM_DISABLE) && (PS_FSM_STATE != PS_FSM_NORMAL))
        {
          ganges_Reset_Event(&PwrSaveEvent);
          ganges_send_power_save_req(Adapter,DISABLE_PWR_SAVE);
          RSI_DEBUG(RSI_ZONE_OID,(TEXT("ganges_ioctl: Disabling Pwr save\n")));
          ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);
          RSI_DEBUG(RSI_ZONE_OID,(TEXT("ganges_ioctl: Wokeup on event\n")));
        }
      
        FSM_STATE = FSM_NO_STATE;
        /* First DisAssociate with the current AP */
        ganges_send_disassociate_request(Adapter,
                                         Adapter->connectedAP.BSSID, 
                                         2);
      }
    }
  }
  
  /* search given ssid is in found list */
  RSI_DEBUG(RSI_ZONE_OID, TEXT("set_ssid: totoal found ssids %d\n"),
                           Adapter->BssidFound);
  RSI_DEBUG(RSI_ZONE_OID, TEXT("set_ssid: searching for ssid\n"));
  for(ii = 0; ii < Adapter->BssidFound; ii++)
  {
      int j;
      RSI_DEBUG(RSI_ZONE_OID, "%d %d", Adapter->scanConfirmVarArray[ii]->SSIDLen,
				     ssid->SsidLength);
      for(j=0;j<ssid->SsidLength;j++)
        RSI_DEBUG(RSI_ZONE_OID,"%c",ssid->Ssid[j]); 
    if(Adapter->scanConfirmVarArray[ii]->SSIDLen == ssid->SsidLength)
    {
      if(ganges_memcmp(Adapter->scanConfirmVarArray[ii]->SSID,
                ssid->Ssid,
                ssid->SsidLength) == 0)
      {
        /* ssid matched send join request*/
        RSI_DEBUG(RSI_ZONE_OID, TEXT("set_ssid: found ssid\n"));
        Adapter->SelectedBssidNum = ii;
        /* If scanning is under progress, then have this join pending.
         *  Send the join request after scanning all channels */

        if(FSM_STATE == FSM_SCAN_REQ_SENT)
        {
          RSI_DEBUG(RSI_ZONE_OID, TEXT("set_ssid: join pending\n"));
          Adapter->JoinRequestPending = 0;
          ganges_memcpy(&Adapter->pssid, ssid,sizeof(NDIS_802_11_SSID));
        }
        else    
        {
          if(Adapter->NetworkType == Ndis802_11Infrastructure)
          {
            /* check the AP capabilities */
            if(ganges_can_connect(Adapter,ii))
            {
              RSI_DEBUG(RSI_ZONE_OID, TEXT("set_ssid: sending join\n"));

              //ganges_set_channel_type(Adapter, 
              //                  Adapter->scanConfirmVarArray[ii]->PhyParam[0]);
              Adapter->roam_ssid.SsidLength = Adapter->scanConfirmVarArray[
                                       ii]->SSIDLen;
                    
              ganges_memcpy(Adapter->roam_ssid.Ssid, 
                            Adapter->scanConfirmVarArray[ii]->SSID,
                            Adapter->roam_ssid.SsidLength);
              ganges_memcpy(Adapter->roam_bssid, 
                            Adapter->scanConfirmVarArray[ii]->BSSID,
                            6);
	    if((PS_FSM_STATE == PS_FSM_DEEP_SLEEP))
            {
              RSI_DEBUG(RSI_ZONE_INFO,
                        "ganges_set_bssid: Connection not allowed in deep sleep mode: %d\n",Adapter->halt_flag);
	      return;
	    } 
            if(Adapter->halt_flag ==1)
            {
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_set_bssid: Waiting for power save cfm\n");
              ganges_Reset_Event(&PwrSaveEvent);
              ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);		  
            }  	
            if((PS_FSM_STATE == PS_FSM_AUTO)
             ||(PS_FSM_STATE == PS_FSM_NORMAL))
	    {
	      UINT8 byte=0x01;
	      RSI_DEBUG(RSI_ZONE_INFO,"ganges_set_bssid: Sending deep sleep disable request\n");	
              ganges_Reset_Event(&PwrSaveEvent);
	      ganges_send_ssp_wakeup_cmd(Adapter,byte);
	      ganges_send_ssp_wakeup_cmd_read(Adapter);
              Adapter->halt_flag=1;
              ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);		  
              Adapter->halt_flag=0;
            }
              FSM_STATE = FSM_JOIN_REQ_SENT;
              ganges_send_join_request(Adapter, 
                                       Adapter->scanConfirmVarArray[ii]);
            }
            else
            {
              RSI_DEBUG(RSI_ZONE_OID, 
                         TEXT("set_ssid: Capabilities not matched\n"));
              continue;
            }
          }
          else
          {
            /* IBSS related join request want to send 
             * first we will send probe resp request 
             * */
            if(Adapter->scanConfirmVarArray[ii]->CapabilityInfo & 0x0002)
            {
              RSI_DEBUG(RSI_ZONE_OID, 
                        TEXT("set_ssid: sending probe response request\n"));

    	      FSM_STATE = FSM_NO_STATE;
              ganges_set_channel_type(Adapter, 
                                      Adapter->Ibss_info.channelNum);
              ganges_memcpy(Adapter->Ibss_info.SSID, ssid->Ssid, ssid->SsidLength);
              Adapter->Ibss_info.SSIDLen = (UINT8)ssid->SsidLength;
              Adapter->Ibss_info.Beacon = 
                      Adapter->scanConfirmVarArray[ii]->Beacon ;
              Adapter->Ibss_info.CapabilityInfo |= 
                      (Adapter->scanConfirmVarArray[ii]->CapabilityInfo & 
                        RSI_SHORT_SLOT_TIME);
              ganges_send_probe_resp_request(Adapter,
                                             Adapter->scanConfirmVarArray[ii]->ProtocolType);
            }
            else
            {
              RSI_DEBUG(RSI_ZONE_OID, 
                         TEXT("set_ssid: This is not an ibss network\n"));
            }
          }
        }
        return;
      }/* ssid matched condition */
    } /* length match condition */
  } /* bssid found for loop */
  
  /* check wether we are in ibss mode*/
  if(Adapter->NetworkType == Ndis802_11IBSS) 
  {
    Adapter->SelectedBssidNum = 255;
    FSM_STATE = FSM_NO_STATE;
    ganges_memcpy(Adapter->Ibss_info.SSID, ssid->Ssid, ssid->SsidLength);
    Adapter->Ibss_info.SSIDLen = (UINT8)ssid->SsidLength;
    ganges_set_channel_type(Adapter, 
                            Adapter->Ibss_info.channelNum);
 
    if(Adapter->IbssCreator == IBSS_CREATOR)
    {
      RSI_DEBUG(RSI_ZONE_OID, 
               TEXT("set_ssid: creating IBSS network\n"));
      ganges_send_probe_resp_request(Adapter, Adapter->ProtocolType);
    }
    else
    {
    Adapter->JoinIBSSPending = 1;

    FSM_STATE = FSM_SCAN_REQ_SENT;
    RSI_DEBUG(RSI_ZONE_OID, TEXT("set_ssid: Sending Probe request\n"));
    ganges_free_bssid_list(Adapter);
    ganges_memcpy(&Adapter->pssid, ssid,sizeof(NDIS_802_11_SSID));
    ganges_send_probe_request(Adapter, ACTIVE_SCAN, Adapter->ScanChannelNum, 1, NULL);
    }
  }
  else if((Adapter->NetworkType == Ndis802_11Infrastructure) &&
          (join_pend_status == 0))
  {
    FSM_STATE = FSM_SCAN_REQ_SENT;
    RSI_DEBUG(RSI_ZONE_OID, TEXT("set_ssid: Sending Probe request\n"));
    ganges_free_bssid_list(Adapter);
    ganges_send_probe_request(Adapter, ACTIVE_SCAN, Adapter->ScanChannelNum, 1, NULL);
    RSI_DEBUG(RSI_ZONE_OID, TEXT("set_ssid: join pending\n"));
    Adapter->JoinRequestPending = 1;
    ganges_memcpy(&Adapter->pssid, ssid,sizeof(NDIS_802_11_SSID));
  }
}
 
/*FUNCTION*********************************************************************
Function Name  : ganges_set_bssid
Description    : This function connects the specified AP, If already connected
                 to other than this AP, send disassociate and connects.
Returned Value : 
Parameters     :

----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
Adapter                     |  X  |     |     |  pointer to Adapter
ssid                        |  X  |     |     |  pointer to NDIS_802_11_SSID
                                                 which we are going to connect
----------------------------+-----+-----+-----+------------------------------
*END**************************************************************************/

VOID
ganges_set_bssid
  (
    PRSI_ADAPTER      Adapter,
    UINT8             *bssid
  )
{
  UINT32  ii,jj;
  UINT32  join_pend_status = Adapter->JoinRequestPending;

#ifdef GANGES_PER_MODE
  if(Adapter->ppe_per_mode==TRUE)
  {
    RSI_DEBUG(RSI_ZONE_OID,
               TEXT("ganges_set_bssid: Connect not allowed in PER mode\n"));
    return;
  }
#endif

  if((FSM_STATE != FSM_NO_STATE) && (FSM_STATE != FSM_OPEN)&&
     (FSM_STATE != FSM_SCAN_REQ_SENT))
  {
    RSI_DEBUG(RSI_ZONE_OID,
              TEXT("ganges_set_bssid: Card not yet ready\n"));
    return;
  }

  Adapter->JoinRequestPending = 0;

  if(FSM_STATE == FSM_DEEP_SLEEP)
  {
    RSI_DEBUG(RSI_ZONE_OID,TEXT("ganges_set_bssid: join in deep sleep state\n"));
    return;
  }

  /* if scan in progress set join request pending */
  if(FSM_STATE == FSM_SCAN_REQ_SENT)
  {
    RSI_DEBUG(RSI_ZONE_OID, TEXT("ganges_set_bssid: join pending\n"));
    Adapter->JoinRequestPending = 1;
    ganges_memcpy(&Adapter->pbssid, bssid,ETH_ADDR_LEN);
    return;
  }

  if((FSM_STATE == FSM_OPEN) &&
     (Adapter->NetworkType == Ndis802_11Infrastructure))
  {
    /* connected to AP, so first disassociate */
    RSI_DEBUG(RSI_ZONE_OID, TEXT("ganges_set_bssid: Already connected\n"));

    if(Adapter->authMode != Ndis802_11AuthModeWPA2)
    {
      if(ganges_memcmp(&Adapter->connectedAP.BSSID[0],
                       bssid,
                       ETH_ADDR_LEN) == 0)
      {
        RSI_DEBUG(RSI_ZONE_OID,
                  TEXT("ganges_set_bssid: tried to connect connected AP\n"));
 	ganges_indicate_connect_status(Adapter);
        return;
      }

      /* NdisMIndicateStatus(Adapter->AdapterHandle,
                            NDIS_STATUS_MEDIA_DISCONNECT, 
                            (PVOID) 0, 
                            0); 
       NdisMIndicateStatusComplete(Adapter->AdapterHandle); */
      RSI_DEBUG(RSI_ZONE_OID, TEXT("ganges_set_bssid: disassociating\n"));

        /* First DisAssociate with the current AP */
      ganges_send_disassociate_request(Adapter,
                                       Adapter->connectedAP.BSSID,
                                       2);
    }
  }

  /* search given ssid is in found list */
  RSI_DEBUG(RSI_ZONE_OID, TEXT("ganges_set_bssid: total found Bssids %d %d\n"),
            Adapter->BssidFound, FSM_STATE);
  RSI_DEBUG(RSI_ZONE_OID, TEXT("ganges_set_bssid: searching for Bssid: \n"));
  
  for(jj=0;jj<6;jj++)
    RSI_DEBUG(RSI_ZONE_INFO,"%0x:",bssid[jj]);
    RSI_DEBUG(RSI_ZONE_INFO,"\n");

  for(ii = 0; ii < Adapter->BssidFound; ii++)
  { 
    RSI_DEBUG(RSI_ZONE_OID, TEXT("ganges_set_bssid: Trying to match with: \n"));
  
    for(jj=0;jj<6;jj++)
      RSI_DEBUG(RSI_ZONE_INFO,"%0x:",Adapter->scanConfirmVarArray[ii]->BSSID[jj]);
      RSI_DEBUG(RSI_ZONE_INFO,"\n");

    if(ganges_memcmp(Adapter->scanConfirmVarArray[ii]->BSSID,
                     bssid,
                     ETH_ADDR_LEN) == 0)
    {
      RSI_DEBUG(RSI_ZONE_OID, TEXT("ganges_set_bssid: found bssid\n"));

      Adapter->SelectedBssidNum = ii;

      if(FSM_STATE == FSM_SCAN_REQ_SENT)
      {
        RSI_DEBUG(RSI_ZONE_OID, TEXT("ganges_set_bssid: join pending\n"));
        Adapter->JoinRequestPending = 0;
        ganges_memcpy(&Adapter->pbssid, bssid,ETH_ADDR_LEN);
      }
      else
      {
        if(Adapter->NetworkType == Ndis802_11Infrastructure)
        {
            /* check the AP capabilities */
          if(ganges_can_connect(Adapter,ii))
          {
            RSI_DEBUG(RSI_ZONE_OID, TEXT("ganges_set_bssid: sending join\n"));
            //ganges_set_channel_type(Adapter, 
              //                  Adapter->scanConfirmVarArray[ii]->PhyParam[0]);
            Adapter->roam_ssid.SsidLength = Adapter->scanConfirmVarArray[
                                      ii]->SSIDLen;

            ganges_memcpy(Adapter->roam_ssid.Ssid,
                          Adapter->scanConfirmVarArray[ii]->SSID,
                          Adapter->roam_ssid.SsidLength);
            ganges_memcpy(Adapter->roam_bssid,
                          Adapter->scanConfirmVarArray[ii]->BSSID,
                          6);
            if((PS_FSM_STATE == PS_FSM_DEEP_SLEEP))
            {
              RSI_DEBUG(RSI_ZONE_INFO,
                        "ganges_set_bssid: Connection not allowed in deep sleep mode: %d\n",Adapter->halt_flag);
	      return;
	    } 
            if(Adapter->halt_flag ==1)
            {
              RSI_DEBUG(RSI_ZONE_INFO,"ganges_set_bssid: Waiting for power save cfm\n");
              ganges_Reset_Event(&PwrSaveEvent);
              ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);		  
            }  	
            if((PS_FSM_STATE == PS_FSM_AUTO)
             ||(PS_FSM_STATE == PS_FSM_NORMAL))
	    {
	      UINT8 byte=0x01;
	      RSI_DEBUG(RSI_ZONE_INFO,"ganges_set_bssid: Sending deep sleep disable request\n");	
              ganges_Reset_Event(&PwrSaveEvent);
	      ganges_send_ssp_wakeup_cmd(Adapter,byte);
	      ganges_send_ssp_wakeup_cmd_read(Adapter);
              Adapter->halt_flag=1;
              ganges_Wait_Event(&PwrSaveEvent, EVENT_WAIT_FOREVER);		  
              Adapter->halt_flag=0;
            }
            FSM_STATE = FSM_JOIN_REQ_SENT;
            ganges_send_join_request(Adapter,
                                     Adapter->scanConfirmVarArray[ii]);
          }
          else
          {
            RSI_DEBUG(RSI_ZONE_OID,
                      TEXT("ganges_set_bssid: Capabilities not matched\n"));
            continue;
          }
        }
      }
      return;
    }
  }/* bssid matched condition */
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_set_bssid: No BSSID matched\n");

  if((Adapter->NetworkType == Ndis802_11Infrastructure) &&
          (join_pend_status == 0))
  {
    FSM_STATE = FSM_SCAN_REQ_SENT;
    RSI_DEBUG(RSI_ZONE_OID, TEXT("ganges_set_bssid: Sending Probe request\n"));
    ganges_free_bssid_list(Adapter);
    ganges_send_probe_request(Adapter, ACTIVE_SCAN, Adapter->ScanChannelNum, 1, NULL);
    RSI_DEBUG(RSI_ZONE_OID, TEXT("ganges_set_bssid: Join pending\n"));
    Adapter->JoinRequestPending = 1;
    ganges_memcpy(&Adapter->pbssid,bssid,ETH_ADDR_LEN);
  }
}

/*FUNCTION*********************************************************************
Function Name  : ganges_can_connect
Description    : This routine checks for the capabilities of AP and connect
                 parameters
Returned Value : 
                 TRUE  if we can connect to that AP
                 FALSE if we can't
Parameters     :

------------------------+-----+-----+-----+------------------------------------
Name                    | I/P | O/P | I/O | Purpose
------------------------+-----+-----+-----+------------------------------------
Adapter                 |  X  |     |     |pointer to Adapter
selected                |  X  |     |     | selected bssid
------------------------+-----+-----+-----+------------------------------------
*END**************************************************************************/

BOOLEAN 
ganges_can_connect
  (
    PRSI_ADAPTER      Adapter,
    INT32               selected
  )
{
  if(!(Adapter->scanConfirmVarArray[selected]->CapabilityInfo & 0x0010) &&
                  (Adapter->authMode == Ndis802_11AuthModeOpen)) 
  {
    /* Sation and AP both are in open allow to connect */
    return TRUE;
  }
  if(Adapter->scanConfirmVarArray[selected]->CapabilityInfo & 0x0010)
  {
    if((Adapter->encryptAlgo != Ndis802_11Encryption1Enabled ) &&
       (Adapter->encryptAlgo != Ndis802_11Encryption2Enabled ) &&
       (Adapter->encryptAlgo != Ndis802_11Encryption3Enabled ) )
    {
      RSI_DEBUG(RSI_ZONE_INFO, 
              TEXT("can_connect: STA in OPEN and AP in security\n"));
      return FALSE;
    }
  }
          
  if((Adapter->encryptAlgo == Ndis802_11EncryptionDisabled) &&
     (Adapter->scanConfirmVarArray[selected]->CapabilityInfo & 0x0010))
  {
    /* STA configured in open but AP in security mode */
    /* But supplicant not setting to encryption disabled mode to open connect */
    RSI_DEBUG(RSI_ZONE_INFO, 
              TEXT("can_connect: STA in OPEN and AP in security\n"));
    return FALSE;
  }
  if((Adapter->encryptAlgo != Ndis802_11EncryptionDisabled) &&
     (!(Adapter->scanConfirmVarArray[selected]->CapabilityInfo & 0x0010)))
  {
    /* STA configured in WEP/WPA/WPA2 but AP in open */
    RSI_DEBUG(RSI_ZONE_INFO, 
              TEXT("can_connect: STA in Security and AP in OPEN\n"));
      return FALSE;
  }
  if((Adapter->scanConfirmVarArray[selected]->rsn_capable == 1) ||
    (Adapter->scanConfirmVarArray[selected]->wpa_capable == 1))
  {
    if((Adapter->encryptAlgo == Ndis802_11Encryption1Enabled ) ||
      (Adapter->encryptAlgo == Ndis802_11EncryptionDisabled ))
    {
      RSI_DEBUG(RSI_ZONE_INFO, 
                TEXT("can_connect: WPA/WPA2 capable AP but OPEN/WEP STA\n"));
      return FALSE;
    }
    
  }
          
  if((Adapter->encryptAlgo == Ndis802_11Encryption2Enabled) ||
     (Adapter->encryptAlgo == Ndis802_11Encryption3Enabled))
  {
    if((Adapter->scanConfirmVarArray[selected]->rsn_capable != 1) &&
       (Adapter->scanConfirmVarArray[selected]->wpa_capable != 1))
    {
      RSI_DEBUG(RSI_ZONE_INFO, 
                TEXT("can_connect: trying in WPA/WPA2 but not WPA/WPA2\n"));
      return FALSE;
    }
  }
  if(Adapter->authMode == Ndis802_11AuthModeOpen)
  {
    if((Adapter->encryptAlgo == Ndis802_11Encryption2Enabled) ||
       (Adapter->encryptAlgo == Ndis802_11Encryption3Enabled))
    {
      RSI_DEBUG(RSI_ZONE_INFO, 
                TEXT("can_connect: Invalid combination OPEN CCMP/TKIP\n"));
      return FALSE;
    }
  }
  return TRUE;
}
/*FUNCTION*********************************************************************
Function Name  : ganges_free_bssid_list
Description    : This routine free the allocated memory for the bssid list
Returned Value : 
Parameters     :

------------------------+-----+-----+-----+------------------------------------
Name                    | I/P | O/P | I/O | Purpose
------------------------+-----+-----+-----+------------------------------------
Adapter                 |  X  |     |     |pointer to Adapter
------------------------+-----+-----+-----+------------------------------------
*END**************************************************************************/

VOID 
ganges_free_bssid_list
  (
    PRSI_ADAPTER      Adapter
  )
{
  UINT32 ii;
  /* free the allocated memory for scanconfirm array */
  for(ii=0; ii<Adapter->BssidFound; ii++)
  {
    RSI_DEBUG(RSI_ZONE_INFO,
              TEXT("free_bssid_list: Freeing bssid list array\n")); 
    ganges_mem_free((PVOID)(Adapter->scanConfirmVarArray[ii]));
/*    ganges_mem_free((PVOID)(Adapter->scanConfirmVarArray[ii]),
                    sizeof(ScanConfirm), 
                    0);*/
  }
  Adapter->BssidFound=0;
  return;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_calculate_rssi
Description    : This function calculates the RSSI
Returned Value : 
Parameters     :

------------------------+-----+-----+-----+------------------------------------
Name                    | I/P | O/P | I/O | Purpose
------------------------+-----+-----+-----+------------------------------------
Adapter                 |  X  |     |     |pointer to Adapter
------------------------+-----+-----+-----+------------------------------------
*END**************************************************************************/

INT8 
ganges_calculate_rssi
  (
    PRSI_ADAPTER Adapter,
    UINT8 *frame
  )
{
  UINT8 BB_gain     = (frame[4] & 0x1F); 
  UINT8 BB_LNA_gain = (frame[6]>>6);  //2 bit gain
  UINT8 LNA_gain;
  INT16 rssi_signal = 0;  

  if(BB_LNA_gain == 0)
  {
    LNA_gain = -2;
  }
  else if(BB_LNA_gain ==2)
  {
    LNA_gain = 16;
  }
  else if(BB_LNA_gain ==3)
  {
    LNA_gain=36;         
  }
  else
  {
    LNA_gain=0;
  }

  if(Adapter->RFType == RSI_RF_AL2236)
  {       
    rssi_signal =  -(BB_gain*2 + LNA_gain) - 4;  //dBm
  }
  else if(Adapter->RFType == RSI_RF_MAXIM_2831)
  {
    rssi_signal =  -(BB_gain*2 + LNA_gain) - 6; //dBm
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_calculate_rssi: Undefined RSSI for RFType\n");
  } 
  Adapter->currentTxRate = frame[8];
  return rssi_signal;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_read_power_vals
Description    : Reads the power values from the adapter
Returned Value : On success GANGES_STATUS_SUCCESS will be returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |     |  X  |     | pointer to our adapter
*END**************************************************************************/

GANGES_STATUS
ganges_read_power_vals
  (
    PRSI_ADAPTER Adapter
  )
{
  GANGES_STATUS Status = GANGES_STATUS_SUCCESS;
  EEPROM_READ  mac_info_read;

  RSI_DEBUG(RSI_ZONE_INIT,(TEXT("ganges_read_power_vals: EEPROM read Enabled for Power vals Read\n")));

  mac_info_read.length   =  30;     /* Length in words i.e 15*2 = 30 bytes */
  mac_info_read.off_set  =  0x0088;                    /* '0' for read and '1' for write */

  Status = ganges_eeprom_read(Adapter,&mac_info_read);
  if(Status != GANGES_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_INFO,(TEXT("ganges_read_power_vals: EEPROM read Failed while reading Power Vals\n")));
    return GANGES_STATUS_FAILURE;
  }
  return Status;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_eeprom_write
Description    : This routine sends the eeprom_write frame to the TA firmware.
Returned Value : 
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter            |  X  |     |     | pointer to the Adapter structure.
*END**************************************************************************/

GANGES_STATUS
ganges_eeprom_write
  (
    IN PRSI_ADAPTER Adapter,
    IN PEEPROM_WRITE write_eeprom
  )
{
  struct RSI_Mac_frame mgmt;

  /* Intialize the descriptor with zero */
  ganges_memset(mgmt.descriptor,0,RSI_DESC_LEN);

  RSI_DEBUG(RSI_ZONE_INFO,(TEXT("ganges_eeprom_write: Writing to the EEPROM\n")));

  ganges_memcpy(mgmt.frameType.eeprom_write.data,
                write_eeprom->buff,
                write_eeprom->length);
  mgmt.descriptor[0] = MGMT_DESC_EEPROM_WRITE | write_eeprom->length;//sizeof(mgmt.frameType.eeprom_read));//Descriptor yet to be decided
  mgmt.descriptor[1] = write_eeprom->length;
  mgmt.descriptor[2] = write_eeprom->off_set;

  return ganges_send_TA_mgmt_frame(Adapter,
                                   (UINT8 *)&mgmt,
                                   (RSI_DESC_LEN + write_eeprom->length));
}

/*FUNCTION*********************************************************************
Function Name  : ganges_eeprom_read
Description    : This function sends the eeprom_read frame to the TA firmware.
Returned Value : 
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
Adapter            |  X  |     |     | pointer to the Adapter structure.
*END**************************************************************************/

GANGES_STATUS
ganges_eeprom_read
  (
    IN PRSI_ADAPTER Adapter,
    IN PEEPROM_READ   buff
  )
{
  struct RSI_Mac_frame mgmt;
  PEEPROM_READ read_eeprom = buff;

  RSI_DEBUG(RSI_ZONE_INFO,(TEXT("ganges_eeprom_read: Reading EEPROM\n")));
  mgmt.descriptor[0] = MGMT_DESC_EEPROM_READ ;//sizeof(mgmt.frameType.eeprom_read));//Descriptor yet to be decided
  mgmt.descriptor[1] = read_eeprom->length;
  mgmt.descriptor[2] = read_eeprom->off_set;
  return ganges_send_TA_mgmt_frame(Adapter,
                                   (UINT8 *)&mgmt,
                                   (RSI_DESC_LEN));
}

/*FUNCTION*********************************************************************
Function Name  : ganges_find_mgmt_type
Description    : This function finds the mgmt type of the frame
Returned Value : 
Parameters     :

-------------------+-----+-----+-----+------------------------------
Name               | I/P | O/P | I/O | Purpose
-------------------+-----+-----+-----+------------------------------
type               |  X  |     |     | mgmt type
*END**************************************************************************/

VOID
ganges_find_mgmt_type
  (
    IN UINT16 type
  )
{
  switch(type)
  {
    case MGMT_DESC_TYP_RF_PROG_CFM:
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_find_mgmt_type: Recvd RF prog cfm\n");
    break;
    case MGMT_DESC_TYP_PRB_RSP_CFM:
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_find_mgmt_type: Recvd IBSS probe rsponse cfm\n");
    break;
    case MGMT_DESC_TYP_RCVD_PRB_RESP:
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_find_mgmt_type: Recvd probe rsponse cfm\n");
    break;
    case MGMT_DESC_TYP_SCAN_RSP:
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_find_mgmt_type: Recvd scan cfm\n");
    break;
    case MGMT_DESC_EEPROM_WRITE_CFM:
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_find_mgmt_type: Recvd eeprom write cfm\n");
    break;
    case MGMT_DESC_EEPROM_READ_CFM:
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_find_mgmt_type: Recvd eeprom read cfm\n");
    break;
    case MGMT_DESC_TYP_GIVE_BEACON_REQ:
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_find_mgmt_type: Recvd Give beacon req\n");
    break;
    case MGMT_DESC_TYP_SET_KEYS_CFM:
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_find_mgmt_type: Set keys cfm \n");
    break;
    case MGMT_DESC_TYP_RESET_CFM:
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_find_mgmt_type: Reset cfm recvd\n");
    break;
    case MGMT_DESC_TYP_LMAC_PROG_CFM:
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_find_mgmt_type: LMAC prog cfm recvd\n");
    break;
    case MGMT_DESC_TYP_PWR_SAVE_CFM:
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_find_mgmt_type: Power save cfm recvd\n");
    break;
    case MGMT_DEEP_SLEEP_WAKEUP_CFM:
         RSI_DEBUG(RSI_ZONE_INFO,"ganges_find_mgmt_type: Wakeup cfm recvd\n");
    break;
    default:
    break;
  }
  return;
}

