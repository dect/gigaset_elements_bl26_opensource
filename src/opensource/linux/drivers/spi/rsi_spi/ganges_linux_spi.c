/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Ganges 
Module name  : LINUX-SDIO driver
File Name    : ganges_linux_spi.c

File Description:

List of functions:
    ganges_read_AFEtype
    ganges_load_TA_instructions
    ganges_card_write
    ganges_send_mgmt_frame
    ganges_send_TA_mgmt_frame
    ganges_card_read
    ganges_rcv_pkt
    ganges_load_LMAC_instructions
    ganges_ack_interrupt
    ganges_interrupt_handler 
    ganges_setupcard

Author :
  
Rev History:
Sl  By    date change details
---------------------------------------------------------

---------------------------------------------------------
*END*************************************************************************/

#include <linux/device.h>
#include <linux/spi/spi.h>
#include <linux/workqueue.h>

#include "ganges_linux.h"
#include "ganges_linux_firmware.h"
#include "ganges_mgmt.h"
#include "ganges_nic.h"
#include "ganges_ssp_bus.h"
#include <linux/skbuff.h>
#define TA_DM_BASE_ADDR  0x20000000
#define TA_IM_BASE_ADDR  0x00000000
#define CONFIG_TA_ADDR   0x5F00  

UINT32 FSM_STATE;
GANGES_EVENT  Event;
GANGES_EVENT  PwrSaveEvent;
GANGES_STATIC GANGES_THREAD_HANDLE Thread;
struct net_device *device;

//#define spi_mem_cs_low()        P0_RESET_DATA_REG = 0x40
static int __devinit rps_probe(struct spi_device *spi)
{
  //spi_mem_cs_low(); 
  if(ganges_probe(spi)==0)
  {
    RSI_DEBUG(RSI_ZONE_INIT,
              "ganges_spi_module_init: Successfully initialized the interface\n");	    
    return 0;
  }
  else
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              "ganges_spi_module_init: Unable to iniitialize interface\n");
    return -1; /*returning a negative value to imply error condition*/
  }  
  return 0;
}

static int __devexit rps_remove(struct spi_device *spi)
{
  RSI_DEBUG(RSI_ZONE_INFO,"rps_remove: Redpine remove called\n");
  return 0;
}

struct spi_driver rps_driver = {
        .driver = {
                .name   = "rsisitel",
                .bus    = &spi_bus_type,
                .owner  = THIS_MODULE,
        },
        .probe  = rps_probe,
        .remove = __devexit_p(rps_remove),

        /* REVISIT: many of these chips have deep power-down modes, which
         * should clearly be entered on suspend() to minimize power use.
         * And also when they're otherwise idle...
         */
};

/*******************************************************************************
Function Name:  ganges_probe
Description:    This function is called by kernel when the driver provided 
                Vendor and device IDs are matched. All the initialization 
                work is done here.
Returned Value: GANGES_STATUS_SUCCESS in case of successful initialization 
                or a negative error code signifying failure
Paramaters: 
----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
pInterface                  |     |     |     | Pointer to the card interface   
*******************************************************************************/
 
GANGES_STATUS ganges_probe(struct spi_device *spi_device  )
{
  struct net_device *dev = NULL;
  GANGES_STATUS Status = GANGES_STATUS_SUCCESS;
  PRSI_ADAPTER Adapter;
  UINT8 ii;

  RSI_DEBUG(RSI_ZONE_INIT,"ganges_probe: Initialization function called\n");

  dev= ganges_netdevice_op();
  if(!dev)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              "ganges_probe: Failed to perform netdevice operations\n");
    return GANGES_STATUS_FAILURE;
  }
  device = dev;
  RSI_DEBUG(RSI_ZONE_INIT,
            "ganges_probe: Net device operations suceeded\n");
  Adapter = ganges_getpriv(dev); /*we can also use dev->priv;*/
  ganges_memset(Adapter, 0, sizeof(RSI_ADAPTER));

  Adapter->dev = dev;
  Adapter->hDevice = (VOID *) spi_device;
  /*Enable the SPI interface*/
  Status = ganges_init_interface(Adapter);

  if(Status == GANGES_STATUS_FAILURE)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_probe: Failed to enable interface\n");
    goto fail;
  }

  RSI_DEBUG(RSI_ZONE_INIT,"ganges_probe: Enabled the interface\n");
  
  if(ganges_init_proc(Adapter)!=0)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_prove: Failed to initialize procfs\n");
    goto fail_out1;
  }

  RSI_DEBUG(RSI_ZONE_INIT,"ganges_probe: Initialized Procfs\n");
  
  for(ii=0;ii<4;ii++)
   ganges_queue_init(&Adapter->list[ii]);

  ganges_spinlock_init(&Adapter->lockqueue);
  ganges_init_mutex(&Adapter->int_check_sem);
  ganges_init_mutex(&Adapter->sleep_ack_sem);
  if(ganges_read_reg_paramters(Adapter)!=0)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_probe: Failed to read reg params\n");
    goto fail_out;
  }

  INIT_WORK(&Adapter->handler,(void (*)(void *))ganges_interrupt_handler,(PVOID)Adapter);
  RSI_DEBUG(RSI_ZONE_INIT,"ganges_probe: Read reg params\n");

  for(ii=0; ii<6; ii++)
    dev->dev_addr[ii] = Adapter->PermanentAddress[ii];

  Adapter->AFE_type = RSI_AFE_2;
  RSI_DEBUG(RSI_ZONE_INIT,"ganges_probe: Successfully read the AFE type\n");

  /******* reading the TA Config Vals from ganges_config_vals**********/
  Status = ganges_load_config_vals((PVOID)Adapter);
  if(Status != GANGES_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
               (TEXT("ganges_init_hardware: Initializing the Configuration vals Failed\n")));
    return Status;
  }
  RSI_DEBUG(RSI_ZONE_INIT,"ganges_probe: Config values loaded \n");

  if(ganges_mgmt_init(Adapter)!=0)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,"ganges_probe: Unable to initialize mgmt layer\n");
    goto fail_out;
  }
  RSI_DEBUG(RSI_ZONE_INIT,"ganges_probe: Initailized mgmt layer\n");




  if(ganges_load_TA_instructions(Adapter) != 0)
  {
    RSI_DEBUG(RSI_ZONE_ERROR, "ganges_probe: Failed to download TA inst\n");
    goto fail_out;
  }
  RSI_DEBUG(RSI_ZONE_INIT,"ganges_probe:Downloaded TA instructions\n");

  /*Requesting thread*/
  ganges_Init_Event(&Event);
  ganges_Init_Event(&PwrSaveEvent);
  ganges_Init_Thread(&Thread, "XMIT-Thread", 0, ganges_transmit_thread, Adapter);

  RSI_DEBUG(RSI_ZONE_INIT,"ganges_probe: Initialized thread & Event\n");

  if(Adapter->hDevice!=NULL)
    ganges_Start_Thread(&Thread);

  return Status;
  /*Failure in one of the steps will cause the control to be transferred here*/
fail_out:
  for(ii=0;ii<4;ii++)
    ganges_queue_purge(&Adapter->list[ii]);
  ganges_remove_proc_entry();

fail_out1:
  ganges_deinit_interface(Adapter);

fail:
  if(dev)
  {
    /*Unregisters & returns the network device to the kernel*/
    ganges_unregisterdev(dev);
  }

  /*Disable the interface*/

  RSI_DEBUG(RSI_ZONE_ERROR,"ganges_probe: Failure to initialize\n");
  return GANGES_STATUS_FAILURE;
}

/*******************************************************************************
Function Name:  ganges_disconnect
Description:    This function performs the reverse of the probe function.
Returned Value: SD_SUCCESS in case of successful initialization or a negative error code
                signifying failure
Paramaters: 
----------------------------+-----+-----+-----+------------------------------
Name                        | I/P | O/P | I/O | Purpose
----------------------------+-----+-----+-----+------------------------------
pInterface                  |     |     |     | Pointer to the card interface   
*******************************************************************************/

GANGES_STATUS
ganges_disconnect
  (
    VOID 
  )
{
  GANGES_STATUS Status = GANGES_STATUS_SUCCESS;
  UINT32 ii;
  PRSI_ADAPTER Adapter   = ganges_getpriv(device);
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_disconnect: Deinitializing\n");
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_disconnect: Deletingt event\n");
  ganges_Delete_Event(&Event);
  ganges_Kill_Thread(&Thread);
  for(ii=0;ii<4;ii++)
   ganges_queue_purge(&Adapter->list[ii]);

  ganges_remove_proc_entry();

  /*Return the network device to the kernel*/
  ganges_deinit_interface(Adapter);
  ganges_unregisterdev(device);

  /*Disable the interface*/
  RSI_DEBUG(RSI_ZONE_INFO,"ganges_disconnect: Disabling the interface\n");

  return Status;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_load_TA_instructions
Description    : This functions loads TA instructions and data memory
Returned Value : On success 0 will be returned else a negative number
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |     |  X  |     | pointer to our adapter
*END**************************************************************************/
INT32
ganges_load_TA_instructions
  ( 
    PRSI_ADAPTER Adapter
  )
{
  UINT32 data;  
  UINT32 block_size = 256;
  UINT32 instructions_sz;
  UINT32 num_blocks;
  UINT32 cur_indx=0,ii;
  UINT8 *ta_firmware;
  do
  {
    data = TA_SOFT_RST_SET;
    if(ganges_send_ta_sft_rst_cmd(Adapter,data)!= GANGES_STATUS_SUCCESS)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
                 (TEXT("ganges_load_TA_instructions:  ERROR while loading instructions \n")));
      return -1;
    }     
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_load_TA_instructions: TA put into soft reset\n");
    /*loading in non-multi block mode*/
    instructions_sz = sizeof(tadm);
    ta_firmware = &tadm[0];

    if(ta_firmware==NULL)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
                "ganges_load_TA_instructions: Failed to open file");
      return -1;
    }

    if(instructions_sz % 4)
    {
      instructions_sz += 4 -(instructions_sz %4);
    }

    num_blocks = instructions_sz /block_size;
    ganges_dump(RSI_ZONE_ERROR,&ta_firmware[0],12);
    //for(cur_indx = 0, ii=0;ii<num_blocks;ii++, cur_indx +=block_size)
    for(cur_indx = 0, ii=0;ii<num_blocks;ii++, cur_indx +=block_size)
    {
      //ganges_dump(RSI_ZONE_INIT, ta_firmware+cur_indx, block_size);
      if(ganges_send_ta_load_cmd(Adapter,
                                 (UINT8 *)&ta_firmware[cur_indx],
                                 block_size,
                                 (TA_DM_BASE_ADDR + cur_indx)) != GANGES_STATUS_SUCCESS)
      {
         RSI_DEBUG(RSI_ZONE_ERROR,
                   (TEXT("[load LMAC instr] ERROR while loading instructions \n")));
        return -1;
      }
    }

    /* This should be 4 byte aligned */
    if(instructions_sz % block_size)
    {

      /*
      UINT32 temp = instructions_sz % block_size;
      if(temp % 4)
      {
        temp = temp + (4- (temp%4));
      }*/

      //ganges_dump(RSI_ZONE_INIT, ta_firmware+cur_indx, instructions_sz%block_size);
      if(ganges_send_ta_load_cmd(Adapter,
                                 (UINT8 *)&ta_firmware[cur_indx],
                                 //temp,
                                 (instructions_sz % block_size),
                                 (TA_DM_BASE_ADDR + cur_indx)) != GANGES_STATUS_SUCCESS)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                   (TEXT("[load LMAC instr] ERROR while loading instructions \n")));
        return -1;
      }
    }
    RSI_DEBUG(RSI_ZONE_INIT,
              "ganges_load_TA_inst: num of bytes transferred %0x\n",((num_blocks*block_size) + instructions_sz %block_size));

    RSI_DEBUG(RSI_ZONE_INIT,
              "ganges_load_TA_inst: Succesfully loaded TA DM instructions\n");
#if 1
    RSI_DEBUG(RSI_ZONE_SPI_DBG,
              (TEXT("load_TA_instructions: Loading TA Config Params\n")));
        /*** Loading the TA config Params*********/
    if(ganges_send_ta_config_params_cmd(Adapter,
                                       (UINT8 *)&Adapter->config_params,
                                       (block_size),
                                       (TA_DM_BASE_ADDR + CONFIG_TA_ADDR)) != RSI_STATUS_SUCCESS)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
               (TEXT("[load LMAC instr] ERROR while loading TA Config Params\n")));
      return GANGES_STATUS_FAILURE;
    }
    ganges_dump(RSI_ZONE_ERROR,&Adapter->config_params,sizeof(CONFIG_VALS));	

    RSI_DEBUG(RSI_ZONE_INIT,(TEXT("ganges_load_TA_insrtuctions: Loaded TA Config Params\n")));
#endif
    //ganges_vfree(ta_firmware); 
    /*loading in non-multi block mode*/
    instructions_sz = sizeof(taim);
    ta_firmware = &taim[0];

    if(ta_firmware==NULL)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
                "ganges_load_TA_instructions: Failed to open file");
      return -1;
    }
    if(instructions_sz % 4)
    {
      instructions_sz += 4 -(instructions_sz %4);
    }
    num_blocks      = instructions_sz /block_size;

    for(cur_indx = 0,ii=0;ii<num_blocks;ii++, cur_indx +=block_size)
    {
      if(ganges_send_ta_load_cmd(Adapter,
                                 (UINT8 *)&ta_firmware[cur_indx],
                                 (block_size),
                                 (TA_IM_BASE_ADDR + cur_indx)) != GANGES_STATUS_SUCCESS)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                   (TEXT("[load LMAC instr] ERROR while loading instructions \n")));
        return -1;
      }

      //ganges_dump(RSI_ZONE_INIT, ta_firmware+cur_indx, block_size);
    }
    if(instructions_sz % block_size)
    {
      /*    
      UINT32 temp = instructions_sz % block_size;
      if(temp % 4)
      {
        temp = temp + (4- (temp%4));
      }*/
      //ganges_dump(RSI_ZONE_INIT, ta_firmware+cur_indx, instructions_sz%block_size);
      if(ganges_send_ta_load_cmd(Adapter,
                                 (UINT8 *)&ta_firmware[cur_indx],
                                 //temp,
                                 (instructions_sz % block_size),
                                 (TA_IM_BASE_ADDR + cur_indx)) != GANGES_STATUS_SUCCESS)
      {
        RSI_DEBUG(RSI_ZONE_ERROR,
                   (TEXT("[load LMAC instr] ERROR while loading instructions \n")));
        return -1;
      }
    }
    RSI_DEBUG(RSI_ZONE_INIT,
              "ganges_load_TA_inst: num of bytes transferred %d %d %d\n",num_blocks*block_size,instructions_sz%block_size,
              ((num_blocks*block_size) + instructions_sz %block_size));

    RSI_DEBUG(RSI_ZONE_INIT,
              "ganges_load_TA_inst: Successfully loaded TA IM instructions\n");
    data = TA_SOFT_RST_CLR;
    /*bringing TA out of reset*/
    if(ganges_send_ta_sft_rst_cmd(Adapter,data) != GANGES_STATUS_SUCCESS)
    {
      RSI_DEBUG(RSI_ZONE_ERROR,
                 (TEXT("[load LMAC instr] ERROR while loading instructions \n")));
      return -1;
    }
    RSI_DEBUG(RSI_ZONE_INIT,"ganges_load_TA_instr: Successfully got TA out of reset\n");
  }while(FALSE);
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_card_write
Description    : This function writes frames to the SD card
Returned Value : On success GANGES_STATUS_SUCCESS will be returned 
                 else GANGES_STATUS_FAILURE is returned
Parameters     :

-----------------------+-----+-----+-----+------------------------------
Name                   | I/P | O/P | I/O | Purpose
-----------------------+-----+-----+-----+------------------------------
Adapter                |     |     |     |  
pkt                    |     |     |     |                               
Len                    |     |     |     |
queueno                |     |     |     |   
*END**************************************************************************/

GANGES_STATUS
ganges_card_write
  (
    PRSI_ADAPTER Adapter,
    UINT8        *pkt,
    UINT32        Len,
    UINT32        queueno
  )
{
  struct bus_cmd_s cmd;
  INT32  Status = RSI_STATUS_SUCCESS;
  UINT32 ii = 0;
  UINT32 frag = Len % 4;
  /* Prepare the AHB_SLAVE_WRITE cmd */
  if(frag)
  {
    /* If we are running at 32 bit granularity length should be 4 byte aligned */
    //memset((void *)&pkt[Len],0,(4-frag));
    Len += (4 - frag);
  }

  ganges_memset(&cmd, 0,sizeof(struct bus_cmd_s));
  cmd.type    = AHB_SLAVE_WRITE;
  cmd.data    = (void *)pkt;
  cmd.length  = Len;
  cmd.address = queueno;
  /* Send the cmd to the BUS */
  
  while(ii < 50)
  {
    Status = ganges_ssp_bus_request_synch((SSP_BUS_HANDLE) Adapter->hDevice, &cmd);
    if( (Status == RSI_STATUS_BUSY) ||
        (Status == RSI_STATUS_FAIL))
    {
      ii++;
      continue;
    }
    break;
  }
  if(Status!=GANGES_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
	      "ganges_card_write: Unable to write onto the card\n");
    return Status;
  }
  
  //printk("Card write Value is %d %d\n",ii,Len);
  RSI_DEBUG(RSI_ZONE_DATA_SEND,
            "ganges_card_write: Successfully written onto card\n");
  return Status;
}                                         

/*FUNCTION*********************************************************************
Function Name  : ganges_send_mgmt_frame
Description    : This is a wrapper function which will invoke 
                 ganges_card_write to send the given mangement packet.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned 
                 else GANGES_STATUS_FAILURE is returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
frame            |  X  |     |     | management frame which is going to send
len              |  X  |     |     | length of the managemnet frame
*END**************************************************************************/

GANGES_STATUS
ganges_send_mgmt_frame
  (
    PRSI_ADAPTER Adapter,
    UINT8 frame[],
    UINT32 len
  )
{
  GANGES_STATUS status = GANGES_STATUS_SUCCESS;
  ((UINT8 *)frame)[14] = SND_MGMT_Q;
  ganges_dump(RSI_ZONE_SPI_DBG, frame, 16);
  status = ganges_card_write(Adapter,
                             frame,
                             16,
                             SND_MGMT_Q);

  if(status != GANGES_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              (TEXT("send_mgmt_frame:  Error while sending descriptor")));
    return(status);
  }
  
  ganges_dump(RSI_ZONE_SPI_DBG, &frame[16], (len - 16));
  status = ganges_card_write(Adapter,
                             &frame[16],
                             (len - 16),
                              SND_MGMT_Q);

  if(status != GANGES_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              (TEXT("send_mgmt_frame:  Error while sending pkt")));
    return(status);
  }
  return status;
}

/*************************************************************************
Function Name  : ganges_send_TA_mgmt_frame
Description    : This is a wrapper function which will invoke 
                 ganges_send_pkt to send the given mangement packet.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned 
                 else GANGES_STATUS_FAILURE is returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
Adapter          |  X  |     |     | pointer to our adapter
frame            |  X  |     |     | management frame which is going to send
len              |  X  |     |     | length of the managemnet frame
*END**************************************************************************/

GANGES_STATUS
ganges_send_TA_mgmt_frame
  (
    PRSI_ADAPTER Adapter,
    UINT8 frame[], 
    UINT32 len
  )
{
  GANGES_STATUS status = GANGES_STATUS_SUCCESS;
  UINT8 ii=0;
  /* Add the queue number in the frame descriptor */
  ((UINT8 *)frame)[14] = SND_TA_MGMT_Q;
    
  ganges_dump(RSI_ZONE_MGMT_DUMP, frame, 16);
    
  status = ganges_card_write(Adapter,
                             frame,
                             16,
                             SND_TA_MGMT_Q);

  if(status != GANGES_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              (TEXT("send_mgmt_frame:  Error while sending descriptor")));
    return(status);
  } 
  ganges_dump(RSI_ZONE_MGMT_DUMP, &frame[16], len - 16);
  for(ii=0;ii<5;ii++)
  {
    ganges_send_ssp_wakeup_cmd_read(Adapter);
  }
  status = ganges_card_write(Adapter,
                             &frame[16],
                             (len - 16),
                             SND_TA_MGMT_Q);

  if(status != GANGES_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              (TEXT("send_mgmt_frame:  Error while sending pkt")));
    return(status);
  }

  return status;
} 

static inline void ganges_skb_set_tail_pointer(struct sk_buff *skb, int offset)
{
        skb->tail = skb->data + offset;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_card_read
Description    : This function read frames from the SD card
Returned Value : On success GANGES_STATUS_SUCCESS will be returned 
                 else GANGES_STATUS_FAILURE is returned
Parameters     :

-----------------------+-----+-----+-----+------------------------------
Name                   | I/P | O/P | I/O | Purpose
-----------------------+-----+-----+-----+------------------------------
Adapter                |     |     |     |  
frame_desc             |     |     |     |                               
ptr                    |     |     |     |
pktlen                 |     |     |     |
queueno                |     |     |     |   
*END**************************************************************************/

GANGES_STATUS
ganges_card_read
  (
    PRSI_ADAPTER Adapter,
    UINT8  *frame_desc,
    UINT8  *ptr,
    UINT32 *pktLen,
    UINT32 *queueno
  )
{
  UINT32 length    = 0; 
  UINT16 ii = 0;
  struct bus_cmd_s cmd;
  INT32  Status = GANGES_STATUS_SUCCESS;
  UINT32 frag=0;
  int flag=0;
  struct sk_buff *rxskb=NULL;      

  /* Read the descriptor first */ 
  /* Prepare the AHB_SLAVE_READ cmd */
  ganges_memset(&cmd,0,sizeof(struct bus_cmd_s));
  cmd.type = AHB_SLAVE_READ;
  cmd.data = frame_desc;
  cmd.length = FRAME_DESC_SZ; 
  //cmd.address = queueNum;

  /* Send the cmd to the BUS */
  while(ii < 4)
  {
    Status = ganges_ssp_bus_request_synch((SSP_BUS_HANDLE) Adapter->hDevice, &cmd);
    if((Status == RSI_STATUS_BUSY) ||
       (Status == RSI_STATUS_FAIL))
    {
      ii++;
      continue;
    }
    break;
  }
  if(Status!=GANGES_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
              "ganges_card_read: Failed to read frame desc from the card\n");
    return Status;
  }
  RSI_DEBUG(RSI_ZONE_DATA_RCV,
            "ganges_card_read:Read the desc succesffully\n");
  *queueno = frame_desc[14] & RSI_DESC_QUEUE_NUM_MASK;
  if(*queueno == RCV_LMAC_MGMT_Q)
  {
    length = frame_desc[0];
    RSI_DEBUG(RSI_ZONE_MGMT_RCV,"ganges_card_read: MGMT Pkt rcvd %d\n",length);
  }
  else if(*queueno == RCV_TA_MGMT_Q)
  {
    RSI_DEBUG(RSI_ZONE_MGMT_RCV,"ganges_card_read: TA MGMT Pkt rcvd\n");
    length = frame_desc[0];
  }
  else
  {
    /*Data pkt*/	  
    if(frame_desc[15] & RSI_DESC_AGGR_ENAB_MASK)
    {
      UINT16 ii,no_pkts, cur_length;
      RSI_DEBUG(RSI_ZONE_DATA_RCV,
                "ganges_card_read: Aggregated pkt recvd\n");

      no_pkts = (frame_desc[15]) & 0x7;

      for(ii=0; ii<no_pkts;ii++)
      {
        
	cur_length = (*(UINT16 *)&frame_desc[ii*2]) & 0xFFF;
        length +=cur_length;
        frag    = cur_length % 4;
        if(frag)
        {
          length += (4 - frag);
          RSI_DEBUG(RSI_ZONE_DATA_RCV,"pad len %d ",cur_length +(4-frag));
        }        
      }
    }
    else
    {
      length = ((frame_desc[1] << 8) + (frame_desc[0] & 0x0FFF));
      if(length > RSI_RCV_BUFFER_LEN)
      {
        printk("Too big packet: %d\n",length);
        length = RSI_RCV_BUFFER_LEN;
      }
      if(length < RSI_HEADER_SIZE)
      {
        printk("Too small packet: %d\n",length);
      }
      flag =1;
      *pktLen = length;
      if(length%4)
      {
        length += (4-length%4);
      }
      rxskb  = ganges_alloc_skb(length);
      ptr    = ganges_skb_put(rxskb,length);
    }
  }  

  if(!length)
  {
    RSI_DEBUG(RSI_ZONE_DATA_RCV,"!!!!!!!!!!!!!!!ganges_card_read: Pkt size is zero\n");
    return Status;
  }
   
  if(flag ==0){
  *pktLen = length;
  frag = length % 4;
  if(frag)
  {
    length += (4-frag);
  }
  }

  /* Read the data frame */
  /* Prepare the AHB_SLAVE_READ cmd */
  ganges_memset(&cmd, 0,sizeof(struct bus_cmd_s));
  cmd.type = AHB_SLAVE_READ;
  cmd.data = ptr;
  cmd.length = length;
  //cmd.address = queueNum;
  
  /* Send the cmd to the BUS */      
  while(ii < 4) 
  {
    Status = ganges_ssp_bus_request_synch((SSP_BUS_HANDLE) Adapter->hDevice, &cmd);
    if( (Status == RSI_STATUS_BUSY) ||
        (Status == RSI_STATUS_FAIL))
    {
      ii++;
      continue;
    }
    break;
  }

  if(Status!=GANGES_STATUS_SUCCESS)
  {
    RSI_DEBUG(RSI_ZONE_ERROR,
	      "ganges_card_read: Failed to read frame from the card\n");
    return Status;
  }            
    
  if(flag)
  {
    rxskb->dev = Adapter->dev;
    rxskb->protocol = ganges_eth_type_trans(rxskb,Adapter->dev);
    rxskb->ip_summed = CHECKSUM_NONE;
    ganges_skb_set_tail_pointer(rxskb,*pktLen);
    rxskb->len = *pktLen;
    ganges_netif_rx(rxskb);
    Adapter->stats.rx_packets++;
    Adapter->stats.rx_bytes +=*pktLen;
    flag =0;
  }

  RSI_DEBUG(RSI_ZONE_DATA_RCV,
            "ganges_card_read: Read %d bytes of frame frm the card\n",length);
  return Status;
  
}  
  
/*FUNCTION*********************************************************************
Function Name  : ganges_rcv_pkt
Description    : Upon the occurence of a data pending interrupt, this 
                 function will be called. Determine what type of packet it is
		 and handle it accordingly.
Returned Value : On success GANGES_STATUS_SUCCESS will be returned 
                 else GANGES_STATUS_FAILURE is returned
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter	 |     |     |     | Pointer to private data area of the device
*END**************************************************************************/

GANGES_STATUS
ganges_rcv_pkt
  (
    PRSI_ADAPTER Adapter
  )
{
  GANGES_STATUS Status = GANGES_STATUS_SUCCESS;
  UINT32 pktLen;
  UINT32 q_no;
  UINT16 type;
  RSI_DEBUG(RSI_ZONE_DATA_RCV,"ganges_rcv_pkt:\n");
  do
  {
    Status = ganges_card_read(Adapter,
                              Adapter->DataRcvPacket,
     	  		      &Adapter->DataRcvPacket[FRAME_DESC_SZ],
			      &pktLen,
			      &q_no);
    if(Status !=GANGES_STATUS_SUCCESS)
    {
      Adapter->stats.rx_dropped++;
      break;
    }
      
    switch(q_no)
    {
      case RCV_DATA_Q:      
	     /*check if aggregation enabled*/
	     if(Adapter->DataRcvPacket[15] & RSI_DESC_AGGR_ENAB_MASK)
	     {
	       
	       UINT16 ii, no_pkts, cur_length, pkt_offset,frag, pad_len=0;
	       /* 8to 10 bits in word7 indicates number of packets aggegated */
	       no_pkts = (Adapter->DataRcvPacket[15] & 0x7);
	       pkt_offset = 16; /*skip the descriptor*/

	       for(ii=0; ii<no_pkts; ii++)
	       {
	         cur_length = (*(UINT16 *)&Adapter->DataRcvPacket[ii*2] & 0xFFF);
                 frag = cur_length % 4;
                 if(frag)
                 {
                   /*calculate how much padding is added */
                   pad_len = (4 - frag);
		   
                 }
                 if(cur_length < 16)
                 {
		   RSI_DEBUG(RSI_ZONE_ERROR,"Too small packet to indicate\n");
		   RSI_DEBUG(RSI_ZONE_ERROR,
                             "ganges_rcv_packet: Indicating pktnum %d of length %d\n", 
                             ii,cur_length);
                   ganges_dump(RSI_ZONE_ERROR, Adapter->DataRcvPacket, 16);
                 }
                 if(cur_length > 1536)
                 {
		   RSI_DEBUG(RSI_ZONE_ERROR,"Too big packet to indicate\n");
		   RSI_DEBUG(RSI_ZONE_ERROR,
                             "ganges_rcv_packet: Indicating pktnum %d of length %d\n", 
                             ii,cur_length);
                   ganges_dump(RSI_ZONE_ERROR, Adapter->DataRcvPacket, 16);
                 }
		 RSI_DEBUG(RSI_ZONE_DATA_RCV,
                           "ganges_rcv_packet: Indicating pktnum %d of length %d\n", 
                           ii,cur_length);
		 
		 if(ganges_indicate_packet(Adapter, 
                                           &Adapter->DataRcvPacket[pkt_offset], 
                                           cur_length)!=0)
		 {
		   RSI_DEBUG(RSI_ZONE_ERROR,
                         "ganges_rcv_packet: Error! Unsuccessful attempt at indicating\n");
	           return GANGES_STATUS_FAILURE; 
	         }      	 
                 pkt_offset = pkt_offset + cur_length + pad_len;
	       }/*End of for loop*/
             }	        
	     else
             {
#if 0
               if(pktLen!=4)	       
               {
		 
		 RSI_DEBUG(RSI_ZONE_DATA_RCV,
                           "ganges_rcv_packet: Indicating pkt of length %d\n", pktLen);	
		 if(ganges_indicate_packet(Adapter, 
                                           &Adapter->DataRcvPacket[16], 
                                           pktLen)!=0) 
		 {
		   RSI_DEBUG(RSI_ZONE_ERROR,
                         "ganges_rcv_packet: Error! Unssucceful attempt at indicating\n");
	           return GANGES_STATUS_FAILURE; 
		 }  	         
               }
#endif
	     }

             break;
	     
      case RCV_LMAC_MGMT_Q:
	     pktLen = ((UINT32)(Adapter->DataRcvPacket[0]));
	     if(!pktLen)
             {
               pktLen = 4;
             }
             pktLen += 16;  
	     
             /* type is upper 5 bits of descriptor's second byte */
             type = Adapter->DataRcvPacket[1]  & 0xFE;   
             type = type << 8; 
	     if((type !=MGMT_DESC_TYP_STATS_RESP) && (type != MGMT_DESC_TYP_BEACONS_RESP)
		&& (type !=MGMT_DESC_TYP_PWR_SAVE_CFM))
	     {
	       ganges_dump(RSI_ZONE_MGMT_DUMP,
			   Adapter->DataRcvPacket,
			   pktLen);
             }	       
             ganges_mgmt_fsm(Adapter,  
                             Adapter->DataRcvPacket, 
                             type, 
                             pktLen);
             break;
	     
      case RCV_TA_MGMT_Q:
             RSI_DEBUG(RSI_ZONE_MGMT_RCV,"ganges_rcv_pkt: TA MGMT pkt\n"); 
	     pktLen = (UINT32)(Adapter->DataRcvPacket[0]);
	     if(!pktLen)
             {
               pktLen = 4;
             }
             pktLen += 16;  
	     
             /* type is upper 5 bits of descriptor's second byte */
             type = Adapter->DataRcvPacket[1]  & 0xFF;   
             type = type << 8; 
	
	     if((type !=MGMT_DESC_TYP_STATS_RESP) && (type != MGMT_DESC_TYP_BEACONS_RESP)
		&& (type !=MGMT_DESC_TYP_PWR_SAVE_CFM))
	     {

	     ganges_dump(RSI_ZONE_MGMT_DUMP,
                         Adapter->DataRcvPacket,
		         pktLen);
             }
             ganges_mgmt_fsm(Adapter,  
                             Adapter->DataRcvPacket, 
                             type, 
                             pktLen);
 
             break;
	     
      default:
             RSI_DEBUG(RSI_ZONE_DATA_RCV,
                       "ganges_rcv_pkt: packet from invalid queue\n"); 
             ganges_dump(RSI_ZONE_ERROR, Adapter->DataRcvPacket, 16);
             break;
    }
    //ganges_dump(RSI_ZONE_NFM_PATH, Adapter->DataRcvPacket, pktLen+16);
      
  }while(FALSE);

  return Status;
}      

/*FUNCTION*********************************************************************
Function Name  : ganges_load_LMAC_instructions
Description    : This function loads instructions or templates to the card
Returned Value : On success 0 is returned else a negative number
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pAdapter         |     |  X  |     | pointer to device's private data area
END**************************************************************************/

INT32
ganges_load_LMAC_instructions
  (
    PRSI_ADAPTER Adapter
  )
{
  UINT16 cur_indx = 0;
  UINT32 inst_sz_words = LMAC_INSTRUCTIONS_SIZE/2;
  UINT16 *ganges_inst_set;
  //UINT32 block_size = Adapter->TransmitBlockSize;
  UINT16 temp_inst_block[RSI_SDIO_FRM_TRF_SIZE+16];

  
  RSI_DEBUG(RSI_ZONE_INIT,"+ganges_load_LMAC_instructions\n");      
  ganges_inst_set = &lmac_inst[0];
  do
  {
    for(cur_indx = 0;cur_indx < inst_sz_words; cur_indx +=(RSI_SDIO_FRM_TRF_SIZE/2))
    {
      /*For each frame desc is to be added*/      
      ganges_memset(&temp_inst_block[0],0,RSI_DESC_LEN);
	
	/*Check if the last frame*/
      if((inst_sz_words - cur_indx)<=(RSI_SDIO_FRM_TRF_SIZE>>1))
      {
        /*Indicate in the descriptor that it is the last frame*/		
        temp_inst_block[0] = (inst_sz_words - cur_indx)*2;
        temp_inst_block[2] = 0x0001;	  
        RSI_DEBUG(RSI_ZONE_INIT,
                  "ganges_load_LMAC_instructions: Last frame %d\n",temp_inst_block[0]+16);
      }
      else
      {
        temp_inst_block[0] = RSI_SDIO_FRM_TRF_SIZE;
      }
      temp_inst_block[1] = cur_indx;
      ganges_memcpy(&temp_inst_block[8],
                    ganges_inst_set+cur_indx,
                    temp_inst_block[0]);
      
      ((UINT8 *)temp_inst_block)[14] = SND_DATA_Q;
       
      RSI_DEBUG(RSI_ZONE_INIT, "Load LMAC block at %d num_bytes: %d\n", cur_indx,temp_inst_block[0]);
      //ganges_dump(RSI_ZONE_INIT,&temp_inst_block[0],256);
      if(ganges_card_write(Adapter, 
                           ((UINT8 *)&temp_inst_block[0]), 
                           16,
                           SND_DATA_Q)!=GANGES_STATUS_SUCCESS)
      {
 	RSI_DEBUG(RSI_ZONE_ERROR,
                  "ganges_load_LMAC_inst: Error loading at %d offset failed\n",
                  cur_indx);
	return -1;
      }  				    				  
          /* Send the pkt */      
      if((ganges_card_write(Adapter, 
                            (UINT8 *)(&((UINT8 *)temp_inst_block)[16]),
                            temp_inst_block[0],
                            SND_DATA_Q))!=GANGES_STATUS_SUCCESS)

      {
        RSI_DEBUG(RSI_ZONE_ERROR, 
                   (TEXT("[load LMAC instr] ERROR while loading instructions \n")));

        return -1;
      }
    }
  }while(FALSE);   
  return 0;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_interrupt_handler
Description    : Upon the occurence of an interrupt, the interrupt handler will be called
Returned Value : None
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
pInterface       |     |     |     | Ptr to the card interface
*END**************************************************************************/

VOID
ganges_interrupt_handler 
  (
    PRSI_ADAPTER Adapter 
  )
{

  INTERRUPT_TYPE InterruptType;
  UINT8 InterruptStatus;
  do{
    InterruptStatus = ganges_send_ssp_read_intrStatus_cmd(Adapter);
    Adapter->InterruptStatus = ((unsigned char)InterruptStatus & 0x0F);
    RSI_DEBUG(RSI_ZONE_ISR,"ganges_interrupt_handler:\n");

    InterruptType = ganges_get_interrupt_type(Adapter->InterruptStatus);
    switch(InterruptType)
    {
      case BUFFER_FULL:
        {
	  /*This i/p is recvd when the hardware buffer is full*/	  
	  Adapter->BufferFull = TRUE; 
          ganges_send_ssp_intrAck_cmd(Adapter, SPI_PKT_BUFF_FULL);
	  RSI_DEBUG(RSI_ZONE_ERROR, "F:%x", InterruptStatus);	  
	}
	break;
	  
      case BUFFER_FREE:
	{
 	  /*This i/p is recvd when the hardware buffer becomes empty*/
	  Adapter->BufferFull = FALSE;
          ganges_send_ssp_intrAck_cmd(Adapter, SPI_PKT_BUFF_EMPTY); 
          ganges_Wakeup_Event(&Event); 
          if(ganges_netif_queue_stopped(Adapter->dev))
            ganges_netif_start_queue(Adapter->dev);
	  RSI_DEBUG(RSI_ZONE_ERROR, "E:%x", InterruptStatus);	  
	}
	break;
	  
      case SLEEP_INDCN:
        {
          ganges_down_interruptible(&Adapter->sleep_ack_sem);
          RSI_DEBUG(RSI_ZONE_ERROR,"SI%x",InterruptStatus);
          Adapter->sleep_indcn = TRUE;
          if(Adapter->about_to_send == FALSE)
          {
            ganges_send_ssp_intrAck_cmd(Adapter,  
                                 SD_SLEEP_NO_PKT_ACK|SD_SLEEP_INDCN);
          }
          else
          {
            ganges_send_ssp_intrAck_cmd(Adapter,  
                                 SD_SLEEP_INDCN);
          }
          ganges_up_sem(&Adapter->sleep_ack_sem);
          ganges_up_sem(&Adapter->int_check_sem);
        }
	  KEY_STATUS_REG |= 0x4;
	  RESET_INT_PENDING_REG |=0x2;	
	  enable_irq(KEYB_INT);
        return;

      case WAKEUP_INDCN:
        {
          UINT32 q_num,que_end;
          UINT32 que_start=0,num_pkts;
          que_end   = (Adapter->qos_enable)?4:1;
                                                                                                                             
          RSI_DEBUG(RSI_ZONE_ERROR,"WI%x",InterruptStatus);
          Adapter->sleep_indcn = FALSE;
          for(q_num = que_start; q_num<que_end;++q_num)
          {
            ganges_lock_bh(&Adapter->locks[q_num]);
            num_pkts = ganges_queue_len(&Adapter->list[q_num]);
            ganges_unlock_bh(&Adapter->locks[q_num]);
            if(num_pkts)
            {
              ganges_send_ssp_intrAck_cmd(Adapter,  
                                 SD_WAKEUP_INDCN);
              goto BREAK;
            }
            if(q_num == que_end-1)
            {
	      ganges_send_ssp_intrAck_cmd(Adapter,  
                                   SD_DRIVER_PKT_STATUS|SD_WAKEUP_INDCN);
            }
          }
        }
BREAK:
        ganges_up_sem(&Adapter->int_check_sem);
        if(ganges_netif_queue_stopped(Adapter->dev))
           ganges_netif_start_queue(Adapter->dev);
        ganges_Wakeup_Event(&Event);
        break;

      case MGMT_PENDING:
	{
	  RSI_DEBUG(RSI_ZONE_ERROR,
                    "ganges_interrupt_handler: Shud never occur\n");         
	} 
	break;
	  
      case DATA_PENDING:
	{
	  RSI_DEBUG(RSI_ZONE_ISR,
                    "ganges_interrupt_handler: Pending pkt interrupt recvd\n");
          if(ganges_rcv_pkt(Adapter) != 0)
          {
	    RSI_DEBUG(RSI_ZONE_ERROR,
                      "ganges_interrupt_handler: Unable to recv pkt\n");
	  }
          else
          {        
	    RSI_DEBUG(RSI_ZONE_ISR,
                      "ganges_interrupt_handler: Pkt recvd succesfully\n");
          }  
	} 
        break;
      default:
        {
          RSI_DEBUG(RSI_ZONE_ISR,
                    "ganges_interrupt_handler: Undefined interrupt rcvd\n"); 
  	  KEY_STATUS_REG |= 0x4;
	  RESET_INT_PENDING_REG |=0x2;	
	  enable_irq(KEYB_INT);
          return;
        }
    }  
  }while(TRUE);

  KEY_STATUS_REG |= 0x4;
  RESET_INT_PENDING_REG |=0x2;	
  enable_irq(KEYB_INT);
  return;
}
