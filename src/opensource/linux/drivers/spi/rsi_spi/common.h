/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Ganges
Module name  : LINUX-SDIO-driver
File Name    : common.h

File Description:
        Some common defines will be here.
List of functions:

Author :

Rev History:
Sl By date change details
---------------------------------------------------------
01.                   created
---------------------------------------------------------
Anji
*END*************************************************************************/

#ifndef __COMMON_H__
#define __COMMON_H__

#ifndef UINT32
#define UINT32 unsigned int
#endif
#ifndef UINT16
#define UINT16 unsigned short
#endif
#ifndef UINT8 
#define UINT8 unsigned char
#endif 
#ifndef PVOID
#define PVOID void*
#endif

#ifndef INT32
#define INT32 int
#endif
#ifndef INT16
#define INT16 short
#endif
#ifndef INT8
#define INT8 char
#endif
#ifndef VOID
#define VOID void
#endif 

#ifndef ARASAN_SDIO_STACK
typedef void (*SD_INTERRUPT) (void *pContext);
#endif

extern UINT32 ganges_zone_enabled;

#define GANGES_STATIC   static
#define GANGES_EXTERN   extern

#define TRUE   1
#define FALSE  0
#define OUT
#define IN
#define GANGES_STATUS_FAILURE  -1
#define GANGES_STATUS_SUCCESS 0
#define GANGES_STATUS INT32

#define RSI_TOTAL_PWR_VALS_LEN   30
#define RSI_MAC_ADDR_LEN         6          

#define SIOCSWFIRSTPRIV                         0x8BE0

#define OID_RPS_SET_CHANNEL		        SIOCSWFIRSTPRIV + 0x00
#define OID_RPS_GET_CHANNEL                     SIOCSWFIRSTPRIV + 0x01
#define OID_RPS_SET_PROTOCOL_TYPE               SIOCSWFIRSTPRIV + 0x02
#define OID_RPS_GET_PROTOCOL_TYPE               SIOCSWFIRSTPRIV + 0x03  
#define OID_RPS_SET_TX_PREAMBLE                 SIOCSWFIRSTPRIV + 0x04
#define OID_RPS_GET_TX_PREAMBLE                 SIOCSWFIRSTPRIV + 0x05
#define OID_RPS_SET_SLOT                        SIOCSWFIRSTPRIV + 0x06 
#define OID_RPS_GET_SLOT                        SIOCSWFIRSTPRIV + 0x07
#define OID_RPS_SET_RSSI_NFM_MODE               SIOCSWFIRSTPRIV + 0x08
#define OID_RPS_GET_RSSI_NFM_MODE               SIOCSWFIRSTPRIV + 0x09
#define OID_RPS_SET_QOS_MODE                    SIOCSWFIRSTPRIV + 0x0A
#define OID_RPS_GET_QOS_MODE                    SIOCSWFIRSTPRIV + 0x0B
#define OID_RPS_SET_NO_ACK                      SIOCSWFIRSTPRIV + 0x0C
#define OID_RPS_GET_NO_ACK                      SIOCSWFIRSTPRIV + 0x0D 
#define OID_RPS_SET_WMM_PWR_SAVE                SIOCSWFIRSTPRIV + 0x0E
#define OID_RPS_GET_WMM_PWR_SAVE                SIOCSWFIRSTPRIV + 0x0F
#define OID_RPS_SET_AMPDU_PARAMS                SIOCSWFIRSTPRIV + 0x10
#define OID_RPS_GET_AMPDU_PARAMS                SIOCSWFIRSTPRIV + 0x11
#define OID_RPS_SET_AMSDU_PARAMS                SIOCSWFIRSTPRIV + 0x12
#define OID_RPS_GET_AMSDU_PARAMS                SIOCSWFIRSTPRIV + 0x13
#define OID_RPS_SET_BLOCK_ACK                   SIOCSWFIRSTPRIV + 0x14
#define OID_RPS_GET_BLOCK_ACK                   SIOCSWFIRSTPRIV + 0x15
#define OID_RPS_SET_LINK_ADAPTATION             SIOCSWFIRSTPRIV + 0x16
#define OID_RPS_GET_STATS                       SIOCSWFIRSTPRIV + 0x17
#define OID_RPS_ADD_TSPEC                       SIOCSWFIRSTPRIV + 0x18
#define OID_RPS_GET_TXRATE                      SIOCSWFIRSTPRIV + 0x19 
#define OID_RPS_SET_TXRATE                      SIOCSWFIRSTPRIV + 0x1A
#define OID_RPS_GET_REG_PWR_SAVE                SIOCSWFIRSTPRIV + 0x1B
#define OID_RPS_SET_REG_PWR_SAVE                SIOCSWFIRSTPRIV + 0x1C
#define OID_RPS_GET_PWR_SAVE_10BIT_MODE         SIOCSWFIRSTPRIV + 0x1D
#define OID_RPS_SET_PWR_SAVE_10BIT_MODE         SIOCSWFIRSTPRIV + 0x1E
#define OID_RPS_RESET_ADAPTER                   SIOCSWFIRSTPRIV + 0x1F


typedef enum _RPS_CHANNEL_TYPE
{
                All_Channel = 0,   
                Channel_1 = 1,
                Channel_2,
                Channel_3,
                Channel_4,
                Channel_5,
                Channel_6,
                Channel_7,
                Channel_8,
                Channel_9,
                Channel_10,
                Channel_11,
                Channel_12,
                Channel_13,
}RPS_CHANNEL_TYPE, *PRPS_CHANNEL_TYPE;


typedef enum _RPS_PROTOCOL_TYPE
{
                WLAN_802_11_A = 0,
                WLAN_802_11_B = 1,
                WLAN_802_11_G = 2,
		WLAN_802_11_N = 3,
}RPS_PROTOCOL_TYPE ;

typedef enum _RPS_TX_RATES
{
                RATE_AUTO,
                RATE_1_MBPS,
                RATE_2_MBPS,
                RATE_5_5_MBPS,
                RATE_11_MBPS,
                RATE_6_MBPS,
                RATE_9_MBPS,
                RATE_12_MBPS,
                RATE_18_MBPS,
                RATE_24_MBPS,
                RATE_36_MBPS,
                RATE_48_MBPS,
                RATE_54_MBPS,
                RATE_MCS0,
                RATE_MCS1,
                RATE_MCS2,
                RATE_MCS3,
                RATE_MCS4,
                RATE_MCS5,
                RATE_MCS6,
                RATE_MCS7,
}RPS_TX_RATE ;

typedef enum _RPS_ACK_TYPE{
             RPS_NORMAL_ACK,
             RPS_NO_ACK,
             RPS_BLOCK_ACK
}RPS_ACK_TYPE;

typedef enum _RPS_QOS_TYPE{
             RPS_NO_QOS,
             RPS_WMM,
             RPS_WMMSA,
             RPS_WMM_AND_WMMSA
}RPS_QOS_TYPE;

typedef enum _RPS_AUTH_TYPE{
             RPS_AUTH_OPEN,
             RPS_AUTH_WEP,
             RPS_AUTH_WPA
}RPS_AUTH_TYPE;
typedef int RPS_DURATION_VAL ;

typedef UINT8 NDIS_802_11_PMKID_VALUE[16];

typedef UINT8 NDIS_802_11_MAC_ADDRESS[6];

typedef struct _BSSIDInfo
{
  NDIS_802_11_MAC_ADDRESS  BSSID;
  NDIS_802_11_PMKID_VALUE  PMKID;
} BSSIDInfo, *PBSSIDInfo;

typedef struct 
{
  UINT16 tsid;
  UINT16 nom_msdu_size;
  UINT16 max_msdu_size;
  UINT16 ack_policy;
#define ACCESS_POLICY_EDCA  0x1
#define ACCESS_POLICY_HCCA  0x2
  UINT16 access_policy;
  UINT16 min_data_rate;
  UINT16 mean_data_rate;
  UINT16 peak_data_rate;
  UINT16 min_phy_rate;
  UINT16 burst_size;
  UINT16 delay_bound;
}tspec_t;
typedef struct {
  UINT16 enable;
  UINT16 ac_vo;   /* 0 disable 1-enable */
  UINT16 ac_vi;
  UINT16 ac_bk;
  UINT16 ac_be;
}wmm_pwr_save_t;

typedef struct {
  UINT16 enable;
//  UINT16 rx_factor;
// UINT16 density;  
}ampdu_params_t;

typedef struct {
  UINT16 enable;
  UINT16 max_amsdu_len;  
}amsdu_params_t;

typedef struct {
   UINT16 txenable;
   UINT16 rxenable;	
//   UINT16 delayed_ba;
   UINT16 implicit_ba;
//   UINT16 num_tids;
//   UINT16 supp_tids[16];
} block_ack_t;

typedef struct
{
  UINT16  tx_frames;
  UINT16  rx_frames;
  UINT16  tx_retries;
  UINT16  rx_retries;
  UINT16  rx_duplicates;
  UINT16  crc_errors;
  UINT16  buffer_full;
  UINT16  cca_stuck;
  UINT16  false_rxstart;
  UINT16  false_cca;  
  UINT16  rx_data;     
  UINT16  tx_rate;
  UINT16  false_trigger;  
  UINT16  signal_power;
  UINT16  noise_pwr;
  UINT16  rssi_loc;  
  UINT16  tx_ack;  
  UINT16  rx_ack;  

}station_stats_t;
/* sleep profiles */

typedef enum {
  RPS_PROFILE_NORMAL,
  RPS_PROFILE_AFE_CTRL,
  RPS_PROFILE_RSSI_TRG_AGC,
  RPS_PROFILE_SLEEP1,
  RPS_PROFILE_SLEEP2,
  RPS_PROFILE_SLEEP3,
  RPS_PROFILE_SLEEP4,
  RPS_PROFILE_SLEEP5,
  RPS_PROFILE_SLEEP6,
  RPS_PROFILE_DSLEEP1,
  RPS_PROFILE_DSLEEP2
}RPS_SLEEP_PROFILE;

typedef struct eeprom_read{
  UINT16 length;
  UINT16 off_set;
}EEPROM_READ,*PEEPROM_READ;
    
typedef struct eeprom_write{
  UINT16 length;
  UINT16 off_set;
  UINT8  buff[256];
}EEPROM_WRITE,*PEEPROM_WRITE;

#endif 
