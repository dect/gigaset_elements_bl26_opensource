/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Ganges
Module name  : Linux driver
File Name    : ganges_ssp_hcd.c

File Description:
  This file contains the SSP hardware specific code.
  
List of functions:
    	rsi_spi_write
    	rsi_spi_read
    	rsi_spi_write_then_read
        ganges_ssp_receive_start_token
 	
Author :

Rev History:
Ver   By               date        Description
---------------------------------------------------------
1     Fariya
---------------------------------------------------------
*END*************************************************************************/

#include <linux/delay.h>
#include <linux/device.h>
#include <linux/cache.h>
#include <linux/spi/spi.h>
#include <asm/io.h>

#include "ganges_spi.h"
#include "ganges_ssp_bus.h"
#include "ganges_ssp_HCI.h"
#include "ganges_linux.h"
#define RSI_SPI_BUFSIZ 6000

/*FUNCTION*********************************************************************
Function Name  : rsi_spi_write_then_read
Description    : This function writes the data into the SPI FIFO.
Returned Value : 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
spi              |  X  |     |     | Ptr to spi_device structure
buf              |  X  |     |     | Tx buffer
len              |  X  |     |     | Number of bytes to write
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

int rsi_spi_write(struct spi_device *spi, const u8 *buf, size_t len)
{
        struct spi_transfer     t = {
                        .tx_buf         = buf,
                        .len            = len,
                };
        struct spi_message      m;

        spi_message_init(&m);
        spi_message_add_tail(&t, &m);
        return spi_sync(spi, &m);
}

/*FUNCTION*********************************************************************
Function Name  : rsi_spi_write_then_read
Description    : This function reads the data from the SPI FIFO.
Returned Value : 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
spi              |  X  |     |     | Ptr to spi_device structure
buf              |  X  |     |     | Rx buffer
len              |  X  |     |     | Number of bytes to be read 
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

int rsi_spi_read(struct spi_device *spi, u8 *buf, size_t len)
{
        struct spi_transfer     t = {
                        .rx_buf         = buf,
                        .len            = len,
                };
        struct spi_message      m;

        spi_message_init(&m);
        spi_message_add_tail(&t, &m);
        return spi_sync(spi, &m);
}

/*FUNCTION*********************************************************************
Function Name  : rsi_spi_write_then_read
Description    : This function writes and/or reads data 
Returned Value : 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
spi              |  X  |     |     | Ptr to spi_device structure
txbuf            |     |     |  X  | Tx buffer
rxbuf            |     |     |  X  | Rx buffer
rx_tx_length     |  X  |     |     | Length to read/write
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

int rsi_spi_write_then_read(struct spi_device *spi,u8 *txbuf,u8 *rxbuf, unsigned rx_tx_length)
{

        int                     status;
        struct spi_message      message;
        struct spi_transfer     t;

        /* Use preallocated DMA-safe buffer.  We can't avoid copying here,
         * (as a pure convenience thing), but we can keep heap costs
         * out of the hot path ...
         */
        if (rx_tx_length > RSI_SPI_BUFSIZ)
                return -EINVAL;

        spi_message_init(&message);
        memset((void *)&t, 0, sizeof t);
        t.tx_buf = txbuf;
        t.rx_buf = rxbuf;
        t.len = rx_tx_length;
        spi_message_add_tail(&t, &message);
        /* do the i/o */
        status = spi_sync(spi, &message);
        if (status == 0) {
                //memcpy(rxbuf, t.rx_buf, t.len);
                status = message.status;
        }

        return status;
}

/*FUNCTION*********************************************************************
Function Name  : ganges_ssp_receive_start_token
Description    : This function reads the data from the RX FIFO till it
                 receives the valid start token
Returned Value : 
Parameters     :

-----------------+-----+-----+-----+------------------------------
Name             | I/P | O/P | I/O | Purpose
-----------------+-----+-----+-----+------------------------------
handle           |  X  |     |     | Handle to the SSP info structure
gran             |  X  |     |     | data granularity
-----------------+-----+-----+-----+------------------------------
*END**************************************************************************/

INT32
ganges_ssp_receive_start_token
  (
    SSP_HANDLE handle, 
    UINT16     gran
  )
{
  UINT32    count = 0;  
  UINT8 tx=7,rx=0;
  while(1)
  {
    if((count++) > 260)
    {
      RSI_DEBUG(RSI_ZONE_SPI_DBG,
                "\n[-ssp_receive_start_token] ERROR start token timeout \n");

      return RSI_STATUS_FAIL;
    }
    rsi_spi_write_then_read((struct spi_device *)handle,&tx,&rx,1); 
    if( gran == BIT_GRAN_8)
    {       
      /* This is debug code remove me */
      if( rx == SSP_START_TOKEN_8)
      {
        RSI_DEBUG(RSI_ZONE_SPI_DBG,"Success token recvd %02x\n",rx);
        break;
      }
      else if( rx == SPI_STATUS_BUSY)
      {
        RSI_DEBUG(RSI_ZONE_SPI_DBG,"Busy token recvd %02x\n",rx);
        return RSI_STATUS_BUSY;
      }
      else
      {
        RSI_DEBUG(RSI_ZONE_SPI_DBG,"Unknown token recvd %02x\n",rx);
        //return RSI_STATUS_FAIL;
      }
    }
  }
  return RSI_STATUS_SUCCESS;
}
  
