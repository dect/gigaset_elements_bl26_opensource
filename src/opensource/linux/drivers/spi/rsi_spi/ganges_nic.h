/*HEADER**********************************************************************
Copyright (c)
All rights reserved
This software embodies materials and concepts that are confidential to Redpine
Signals and is made available solely pursuant to the terms of a written license
agreement with Redpine Signals

Project name : Ganges
Module name  : LINUX SDIO driver
File Name    : ganges_nic.h

File Description:

List of functions:

Author :

Rev History:
Ver  By    date       Description
---------------------------------------------------------
1.1  Anji  1 Aug06     Initial version
1.2  Anji  15 Aug06    WiFi fixes from PP driver added
1.3  Anji  26 Aug06    SDIO clock parameter added in adapter struct
---------------------------------------------------------
*END**************************************************************************/
#ifndef __GANGES_NIC__
#define __GANGES_NIC__

#include "linux/netdevice.h"
#include "common.h"

/* Size of the ethernet address */
#define RSI_LENGTH_OF_ADDRESS            6
#define RSI_SIZE_OF_SSID                 32
#define ETH_ADDR_LEN                     6
#define RSI_RCV_BUFFER_LEN               1600
#define RSI_XMIT_BUFFER_LEN              2400

#define RSI_RCV_MGMT_BUFFER_LEN          256
#define RSI_XMIT_MGMT_BUFFER_LEN         256

#define RSI_HEADER_SIZE              14

#define BB_MAX_GAIN_MAX              17

#define BB_PROGRAM_VAL_MAX           120
#define RF_PRG_VAL_MAX               100
#define BB_MAX_GAIN_MAX_VALUES       17
#define NF_THRESHOLD                 14
#define MEASURE_NF_NO_OF_TIMES       6
#define RSI_NUM_PMKID_SUPPORTED      16

/* * Struct for Scan.Confirm */
#define NF_AUTO_PKT_CNT_TO_AVERAGE 10
#define RSI_PERIODIC_NFM_FRQ       200
#define RSI_PERIODIC_RSSI_NFM_FRQ  200
#define RPS_RSSI_PWRUP_NFM_CNT     100


#define BIT(n) (1 << (n))
#define WPA_CIPHER_NONE BIT(0)
#define WPA_CIPHER_WEP40 BIT(1)
#define WPA_CIPHER_WEP104 BIT(2)
#define WPA_CIPHER_TKIP BIT(3)
#define WPA_CIPHER_CCMP BIT(4)

#define SUPPORTED_PROTO_B_ONLY    0x0
#define SUPPORTED_PROTO_11N_ONLY  0x1
#define SUPPORTED_PROTO_MIXED_BG  0x2
#define SUPPORTED_PROTO_MIXED_11N 0x3

#define SUPPORTED_NO_QOS          0x0
#define SUPPORTED_HCCA            0x1
#define SUPPORTED_EDCA            0x2
#define SUPPORTED_MIXED_MODE      0x3

#define SUPPORT_A_MSDU_BIT       BIT(0)
#define SUPPORT_A_MPDU_BIT       BIT(1)
#define SUPPORT_BA_BIT           BIT(2)
#define SUPPORT_DELAYED_BA_BIT   BIT(3)
#define SUPPORT_NO_ACK_BIT       BIT(6)
#define SUPPORT_UAPSD_BIT        BIT(7)
#define SUPPORT_PERIODIC_LINK_ADAPT  0x10
#define SUPPORT_COARSE_LINK_ADAPT    0x20



#define MGMT_QUEUE                       0
#define DATA_QUEUE                       1
#define DATA_QUEUE_VO                    1
#define DATA_QUEUE_VI                    2
#define DATA_QUEUE_BE                    3
#define DATA_QUEUE_BK                    4

typedef enum{ 
	RSI_TRUE = 1,
	RSI_FALSE= 0
}BOOLEAN;

typedef enum _NDIS_802_11_NETWORK_INFRASTRUCTURE {
  Ndis802_11IBSS,
  Ndis802_11Infrastructure,
  Ndis802_11AutoUnknown,
  Ndis802_11InfrastructureMax,
} NDIS_802_11_NETWORK_INFRASTRUCTURE, *PNDIS_802_11_NETWORK_INFRASTRUCTURE;


typedef enum {
   RPS_POWER_11dBm,
   RPS_POWER_13dBm,
   RPS_POWER_15dBm,
   RPS_POWER_17dBm
}RPS_POWER_VALUE;

#pragma pack(1)
struct channel_val{
        UINT16 val1;
        UINT16 val2;
        UINT16 val3;
        UINT16 val4;
        UINT16 val5;
        UINT16 val6;
};

#pragma pack()

typedef enum _ProtoType { 
        ProtoType802_11A = 1 , 
        ProtoType802_11B = 2, 
        ProtoType802_11G = 3,
        ProtoType802_11BG = 4,
        ProtoType802_11N = 5,
	ProtoType802_11N_mixed=6
}ProtoType;

/* IBSS type */
typedef enum _IbssCreatorType { 
    IBSS_JOINER,
    IBSS_CREATOR
}IbssCreatorType;

/* RF Types */
typedef enum  { 
    RSI_RF_MAXIM_2831,
    RSI_RF_AL2230,
    RSI_RF_AL2230S,
    RSI_RF_AL2236
}rf_type;
#define RSI_AFE_1     0
#define RSI_AFE_2     1

/* Noise Floor modes */
typedef enum _NFM_mode{ 
        PERIODIC_NFM = 1 , 
        POWERUP_NFM = 2, 
        AUTO_NFM = 3
}RPS_NFM_mode;

/* Noise RSSI modes */
typedef enum _RSSI_NFM_mode{ 
        RPS_RSSI_NFM_PERIODIC = 1 , 
        RPS_RSSI_NFM_POWERUP  = 2, 
        RPS_RSSI_NFM_AUTO     = 3
}RPS_RSSI_NFM_mode;

/* different types of sleep controls */
typedef enum {
   RPS_SLEEP_CTRL_SOFT,
   RPS_SLEEP_CTRL_FIRM,
   RPS_DSLEEP_CTRL_INT,
   RPS_DSLEEP_CTRL_TIMEOUT
}RPS_SLEEP_CTRL;

//#pragma pack(1)
typedef struct wlan_gen_ie_t{
  UINT8 eid;
  UINT8 eid_len;
  UINT8 oui[3];
  UINT8 ouiType;
  
  union{
    struct {
      UINT8 ouiSubTyp;
      UINT8 version;
      UINT8 ac_info;
      UINT8 res;
      struct{
        UINT8 aifsn:4;
        UINT8 acm:1;
        UINT8 aci:2;
        UINT8 res:1;
             
        UINT8 ECW_min:4;
        UINT8 ECW_max:4;

        UINT16 txop;
      }ac_param[4];
    }wmm;

    struct {
      UINT8 temp[100];
    }ele_11n;

  };
}genIe_t;

typedef struct _scanConfirm {
        UINT16  channelNum;
        UINT8   BSSID[6];
        UINT16  TimeStamp[4];
        UINT16  Beacon;
        UINT16  CapabilityInfo;
        UINT8   SSIDLen;
        UINT8   SSID[33];
        UINT8   SupportedRatesLen;
        UINT8   SupportedRatesEID;
        UINT8   SupportedRates[16];
        UINT8   ExtSupportedRatesLen;
        UINT8   ExtSupportedRates[16];
        UINT16  ProbeDelay;
        UINT8   PhyParamLen;
        UINT8   PhyParam[3];
        UINT8   CF_IBSS_Len;
        UINT8   CF_IBSS_Param[8];
        UINT8   TIM_Len;
        UINT8   DTIM_Param;
 	UINT8   ATIMWindow;
        UINT16  LocalTime[4];
        UINT8   wpa_ie[64]; 
        UINT8   wpa_capable;
        UINT8   rsn_ie[64];
        UINT8   rsn_capable;
        UINT8   rsn_ass_req[64];
        UINT8   wmm_capable;
        UINT8   wmmPS_capable;
        genIe_t wmm_ie;
        UINT8   htCap_ie[27];
        UINT8   htCap1_ie[27];
        UINT8   ht_capable;
        UINT8   ht_capable1;
        UINT8   capable_11n;
        INT8    signal_lvl;
        genIe_t ie_11n;
        struct _scanConfirm*  nextConfirm;
        ProtoType         ProtocolType ;

} ScanConfirm, *PScanConfirm;

struct wpa_ptk 
{
  UINT8 mic_key[16]; /* EAPOL-Key MIC Key (MK) */
  UINT8 encr_key[16]; /* EAPOL-Key Encryption Key (EK) */
  UINT8 tk1[16]; /* Temporal Key 1 (TK1) */
  union 
  {
    UINT8 tk2[16]; /* Temporal Key 2 (TK2) */
    struct 
    {
      UINT8 tx_mic_key[8];
      UINT8 rx_mic_key[8];
    } auth;
  } u;
};

typedef struct wpa_suplicant_s
{
  UINT32   unicast_keys_installed;
  UINT32   group_keys_installed;
  UINT32   four_hand_shake_complete;
  UINT32   pairwise_cipher;
  UINT32   group_cipher;
  UINT32   key_mgmt;
  struct wpa_ptk temporal_ptk;
  UINT8  temporal_gtk[32];
}wpa_suplicant_t;

typedef enum _NDIS_802_11_PRIVACY_FILTER {
  Ndis802_11PrivFilterAcceptAll,
  Ndis802_11PrivFilter8021xWEP,
} NDIS_802_11_PRIVACY_FILTER, *PNDIS_802_11_PRIVACY_FILTER;

typedef enum _NDIS_802_11_WEP_STATUS {
  Ndis802_11WEPEnabled,
  Ndis802_11Encryption1Enabled = Ndis802_11WEPEnabled,
  Ndis802_11WEPDisabled,
  Ndis802_11EncryptionDisabled = Ndis802_11WEPDisabled,
  Ndis802_11WEPKeyAbsent,
  Ndis802_11WEPNotSupported,
  Ndis802_11Encryption1KeyAbsent = Ndis802_11WEPNotSupported,
  Ndis802_11EncryptionNotSupported = Ndis802_11WEPNotSupported,
  Ndis802_11Encryption2Enabled,
  Ndis802_11Encryption2KeyAbsent,
  Ndis802_11Encryption3Enabled,
  Ndis802_11Encryption3KeyAbsent
} NDIS_802_11_WEP_STATUS, *PNDIS_802_11_WEP_STATUS, NDIS_802_11_ENCRYPTION_STATUS, *PNDIS_802_11_ENCRYPTION_STATUS;


typedef enum _NDIS_802_11_AUTHENTICATION_MODE {
  Ndis802_11AuthModeOpen,
  Ndis802_11AuthModeShared,
  Ndis802_11AuthModeAutoSwitch,
  Ndis802_11AuthModeMax,
  Ndis802_11AuthModeWPA,
  Ndis802_11AuthModeWPAPSK,
  Ndis802_11AuthModeWPANone,
  Ndis802_11AuthModeWPA2,
  Ndis802_11AuthModeWPA2PSK  
} NDIS_802_11_AUTHENTICATION_MODE, *PNDIS_802_11_AUTHENTICATION_MODE;


typedef struct _NDIS_802_11_SSID {
  UINT32   SsidLength;
  UINT8  Ssid[32];
}NDIS_802_11_SSID, *PNDIS_802_11_SSID;

typedef struct _NDIS_802_11_FIXED_IEs
{
  UINT8  Timestamp[8];
  UINT16 BeaconInterval;
  UINT16 Capabilities;
} NDIS_802_11_FIXED_IEs, *PNDIS_802_11_FIXED_IEs;


typedef struct _NDIS_802_11_VARIABLE_IEs
{
  UINT8 ElementID;
  UINT8 Length;
  UINT8 data[1];
} NDIS_802_11_VARIABLE_IEs, *PNDIS_802_11_VARIABLE_IEs;


typedef INT32  NDIS_802_11_RSSI;


typedef enum _NDIS_802_11_NETWORK_TYPE {
  Ndis802_11FH,
  Ndis802_11DS,
  Ndis802_11NetworkTypeMax,
} NDIS_802_11_NETWORK_TYPE, *PNDIS_802_11_NETWORK_TYPE;


typedef struct _NDIS_802_11_CONFIGURATION_FH {
 UINT32 Length; 
 UINT32 HopPattern;
 UINT32 HopSet; 
 UINT32 DwellTime;
} NDIS_802_11_CONFIGURATION_FH, *PNDIS_802_11_CONFIGURATION_FH;


typedef struct _NDIS_802_11_CONFIGURATION {
  UINT32 Length;
  UINT32 BeaconPeriod;
  UINT32 ATIMWindow;
  UINT32 DSConfig;
  NDIS_802_11_CONFIGURATION_FH FHConfig;
} NDIS_802_11_CONFIGURATION, *PNDIS_802_11_CONFIGURATION;

typedef UINT8 NDIS_802_11_RATES_EX[8];

typedef struct _NDIS_WLAN_BSSID_EX {
  UINT32  Length;
  NDIS_802_11_MAC_ADDRESS  MacAddress;
  UINT8 Reserved[2];
  NDIS_802_11_SSID  Ssid;
  UINT32  Privacy;
  NDIS_802_11_RSSI  Rssi;
  NDIS_802_11_NETWORK_TYPE  NetworkTypeInUse;
  NDIS_802_11_CONFIGURATION  Configuration;
  NDIS_802_11_NETWORK_INFRASTRUCTURE  InfrastructureMode;
  NDIS_802_11_RATES_EX  SupportedRates;
  UINT32 IELength;
  UINT8 IEs[1];
}NDIS_WLAN_BSSID_EX, *PNDIS_WLAN_BSSID_EX;


typedef struct _NDIS_802_11_BSSID_LIST_EX
{
  UINT32  NumberOfItems;
  NDIS_WLAN_BSSID_EX  Bssid[1];
} NDIS_802_11_BSSID_LIST_EX, *PNDIS_802_11_BSSID_LIST_EX;

typedef struct
{
  UINT16  major;
  UINT16  minor;
}version_st;

/*** Power Vals Set For Three Channels***/
/****** POWER_VAL_SET[0] = Channel1, [1] is for Channel6,[2] is for Channel11**/

typedef enum _power_mode{ 
        RSI_PWR_LOW    = 1, 
        RSI_PWR_MEDIUM = 2, 
        RSI_PWR_HIGH   = 3
}POWER_MODE;
typedef struct power_val_set
{
  UINT16 low;
  UINT16 medium;
  UINT16 high[3];

}POWER_VAL_SET,*PPOWER_VAL_SET;

typedef struct config_vals{
   UINT16 internalPmuEnabled;  // 1 or 0
   UINT16 ldoControlRequired;  // 1 or 0 
   UINT16 crystalFrequency;      //0 � 9.6MHz, 1 � 19.2MHz, 2 � 38.4MHz, 4 � 13MHz
                                                //5 � 26MHz, 6 � 52MHz, 8 � 20MHz, 9 � 40MHz
   UINT16 crystalGoodTime;
   UINT16 TAPLLEnabled;  // 1 or 0
   UINT16 TAPLLFrequency;
   UINT16 sleepClockSource;
   UINT16 voltageControlEnabled;
   UINT16 wakeupThresholdTime;
   UINT16 mratio;
   UINT16 inputdratio;
   UINT16 outputdratio;
   UINT16 host_wkup_enable;
}CONFIG_VALS,*PCONFIG_VALS;


typedef struct _RSI_ADAPTER
{ 
  struct net_device * dev;
  struct net_device_stats stats;
  struct sk_buff_head list[4];
  struct work_struct handler;
  PVOID		    hDevice;	
  spinlock_t        lockqueue;
  spinlock_t        locks[4];
 
  ProtoType         ProtocolType ;
  /* The ethernet address that is burned into the adapter. */
  UINT8             PermanentAddress[RSI_LENGTH_OF_ADDRESS];

  /* List of multicast addresses in use. */
  INT8              Addresses[8][RSI_LENGTH_OF_ADDRESS];
  UINT32            MulticastNumber;   /* number of elements of size */

  /* The current packet filter in use. */
  UINT32            PacketFilter;
  station_stats_t   sta_info;

  /*RECEIVE AND TRANSMIT BUFFERS*/
  /*Management Packets*/
  UINT8             MgmtRcvPacket[RSI_RCV_MGMT_BUFFER_LEN];
  UINT8             MgmtSndPacket[RSI_RCV_MGMT_BUFFER_LEN];

  /*Data Packets */
  UINT32            PktLen; /*Transmit pkt length*/
  UINT8             DataRcvPacket[RSI_RCV_BUFFER_LEN*4];
  UINT8             DataSndPacket[RSI_XMIT_BUFFER_LEN];
  UINT32            RcvPktLen;

  BOOLEAN           BufferFull;
  BOOLEAN           bufferfull_indicated;

  /* Total TX Packet count, Added by Venkat */
  UINT32            total_pkts_qd; 

  /* InterruptStatus tracks interrupt sources that still need to be 
   * serviced it is the logical OR of all card interrupts that have 
   * been received and not processed and cleared. */

  UINT32            InterruptStatus;
   /* 802.11 related variables */
  UINT16            minChannelTime;
  UINT16            maxChannelTime;

  UINT16            dataRate;
  UINT16            dfl_mgmt_rate;
  UINT16            dfl_B_data_rate;

  /*Check if these can be optimized */
  UINT16            BBProgramVal[BB_PROGRAM_VAL_MAX];
  UINT16            BBProgramVal_set2[BB_PROGRAM_VAL_MAX];
  UINT16            BBProgramVal_set3[BB_PROGRAM_VAL_MAX];

  UINT16            resetChannelProgVal[RF_PRG_VAL_MAX];
  UINT16            resetChannelProgVal_set2[RF_PRG_VAL_MAX];
  UINT16            resetChannelProgVal_set3[RF_PRG_VAL_MAX];
  UINT8             power_val_set[10][20];
  UINT16            BB_sleep_prog_val[BB_PROGRAM_VAL_MAX];
  UINT16            RF_sleep_prog_val[RF_PRG_VAL_MAX];
  UINT16            BB_wakeup_prog_val[BB_PROGRAM_VAL_MAX];
  UINT16            RF_wakeup_prog_val[RF_PRG_VAL_MAX];
  POWER_VAL_SET     power_set[3];

  struct            channel_val rsi_channel_vals[12];

  UINT32            selectedChannelNum;

  /*PER mode*/
  UINT16            ppe_per_mode;
  UINT16            mac_stats_timer_intrvl;

  /*WPA Relavent data */
  UINT32            wpa_enabled;
  wpa_suplicant_t   wpa_splcnt;
  UINT32            wpa_gkey_id;
  UINT32	    wpa_ie_len;
  UINT8		    wpa_ie[64]; 	 

  UINT32            SelectedBssidNum;
  UINT32            WepKeyLength;
  UINT32            WepKeyIndex;
  UINT8             RSIWepkey[120];
  UINT32            BssidFound;
  NDIS_802_11_WEP_STATUS    encryptAlgo;
  NDIS_802_11_AUTHENTICATION_MODE authMode;
  UINT32            pmkid_count;
  BSSIDInfo         pmkid_info[RSI_NUM_PMKID_SUPPORTED];	

  PScanConfirm      scanConfirmVarArray[50];
  ScanConfirm       connectedAP; /* Contains info related to connected AP */

  BOOLEAN           ScanAllChannel;
  UINT32            ScanChannelNum;     

  BOOLEAN	    hidden_mode;
  NDIS_802_11_SSID  scan_ssid;
  
  UINT32            JoinRequestPending;
  NDIS_802_11_SSID  pssid;
  UINT8             pbssid[6];
  NDIS_802_11_SSID  roam_ssid;
  UINT8             roam_bssid[6];
 
  /*IBSS related variables*/ 
  ScanConfirm       Ibss_info;
  IbssCreatorType   IbssCreator;
  UINT32            JoinIBSSPending;

  NDIS_802_11_NETWORK_INFRASTRUCTURE  NetworkType;

  UINT32            LinkStatus;             /* link status */
  BOOLEAN           ShutDown;               /* driver shutdown */

  UINT8             Function;               /* I/O function number */
  UINT32            TransmitBlockSize;/*FIXME*/
  UINT32            ReceiveBlockSize;
  UINT32            CardCapability;
  UINT32            ReadBlocks;
  UINT32            WriteBlocks;
  /*Asynchronous callback related variables */
  UINT32            readCmdErrors;          /* error count */
  
  RPS_TX_RATE       TxRate ;
  UINT8		    halt_flag;
  UINT8		    sleep_indcn;
  UINT8		    about_to_send;
  UINT8             power_state;
  RPS_TX_RATE       currentTxRate ;
  rf_type           RFType;
  UINT32            AFE_type;
  /* noise floor related variables */
  UINT16            gain_scalinghigh_reg_values[BB_MAX_GAIN_MAX];
  UINT16            gain_scaling_current;
  UINT16            gain_scaling_previous;
  UINT16            bb_max_gain_current;
  UINT16            bb_max_gain_previous;
  RPS_NFM_mode      nfm_mode;
  RPS_RSSI_NFM_mode rssi_nfm_mode;
  UINT32            periodic_noise_count;
  UINT16            periodic_nfm_count;
  INT32             nf_auto_total;
  INT16             nf_auto_pkt_ct;
  INT16             nf_auto_no_pkts_to_avg;
  INT32             rssi_nf_auto_total;
  INT16             rssi_nf_auto_pkt_ct;
  INT16             rssi_nf_auto_no_pkts_to_avg;
  UINT16            periodic_rssi_nfm_smpl_cnt;
  INT32             periodic_rssi_nfm_count;

  RPS_SLEEP_CTRL    ps_ctrl;
  RPS_SLEEP_PROFILE cur_ps_profile;
  UINT32            power_sava_fsm;
  UINT8             next_read_delay;
  UINT8             sdio_high_speed_enable;
  /* TCP check sum related varilables */
  UINT8             chk_sum_offld_cfg;
  UINT8             encap_h_size; /* if this is non zero tcp checksum is
                                     enabled */
  UINT8             cmdReadData;
  UINT16            capabilityInfo_reg;/* capability info read from registry */
  UINT8             slot_type;
  UINT8             preamble_type;
  RPS_POWER_VALUE   txPower;
  UINT8             roaming_mode;
  BOOLEAN           active_scan;
  UINT32            frag_threshold;
  UINT32            rts_threshold;
  UINT32            rate_auto_fall;
  NDIS_802_11_PRIVACY_FILTER   privacy_filter;
  UINT8             qos_enable;
  UINT8             config_legacy; /* if this is set STA is configured in legacy mode */
  UINT8             mac_add_last_byte; /* MAC addr last byte value */
  UINT8             sdio_clock_speed;
  UINT8             pwr_save_10bit_mode;
  genIe_t           wmm_ie;
  UINT8             acm_bits[4];
 
  UINT16            power_mode;
  tspec_t           tspec_param;
  wmm_pwr_save_t    pwr_save_param;
  UINT16            tx_bytes; 
  UINT16 	    rx_bytes; 
  UINT16            traffic_interval; 
  UINT8             uapsd_wakeup;
  ampdu_params_t    ampdu_param;
  amsdu_params_t    amsdu_param;
  block_ack_t       blk_ack_param;
  UINT8             enable_link_adaptation;
  RPS_QOS_TYPE      qos_type;
  UINT8             no_ack;
  UINT8		    selected_pwr_val_set;
  UINT8             reg_pwrsave_enable;   
  version_st        driver_ver;
  version_st        ta_ver;
  version_st        lmac_ver;
  UINT8             buffer[200];
  /*Keys*/
  UINT8             wpa2_install_keys;
  UINT8             wpa_pair_install_key;
  UINT8             wpa_grp_install_key;
  struct semaphore  int_check_sem;
  struct semaphore  sleep_ack_sem;
  UINT32            num_bfull;
  UINT32            num_bfempty;
  CONFIG_VALS       config_params;  
  UINT8             eeprom_check_status;
} RSI_ADAPTER, *PRSI_ADAPTER;
#endif
