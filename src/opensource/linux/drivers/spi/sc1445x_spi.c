// File created by Gigaset Elements GmbH
// All rights reserved.
/*
 * SPI controller driver for SC1445x-based systems (based on spi_bfin5xx.c).
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <linux/io.h>
#include <linux/ioport.h>
#include <linux/irq.h>
#include <linux/errno.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/dma-mapping.h>
#include <linux/spi/spi.h>
#include <linux/workqueue.h>
#include <asm/dma.h>
#include <asm/spi.h>
#include <asm/irq.h>


#define DRV_NAME	"sc1445x-spi"
#define DRV_AUTHOR	"Aristotelis Iordanidis"
#define DRV_DESC	"SC1445x SPI Controller Driver"
#define DRV_VERSION	"1.0"

MODULE_AUTHOR( DRV_AUTHOR ) ;
MODULE_DESCRIPTION( DRV_DESC ) ;
MODULE_LICENSE( "GPL" ) ;


#define SPI_ON		(0x0001)
#define SPI_PHA		(0x0002)
#define SPI_POL		(0x0004)
#define SPI_CLK		(0x0018)
#define SPI_DO		(0x0020)
#define SPI_SMN		(0x0040)
#define SPI_WORD	(0x0180)
#define SPI_RST		(0x0200)
#define SPI_FORCE_DO	(0x0400)
#define SPI_TXH		(0x0800)
#define SPI_DI		(0x1000)
#define SPI_INT_BIT	(0x2000)
#define SPI_MINT	(0x4000)
#define SPI_EN_CTRL	(0x8000)

#define DMA_ON		(0x0001)
#define BW		(0x0006)
#define DINT_MODE	(0x0008)
#define DREQ_MODE	(0x0010)
#define BINC		(0x0020)
#define AINC		(0x0040)
#define CIRCULAR	(0x0080)
#define DMA_PRIO	(0x0300)

#define SPI1_AD_INT_PRIO	(0x0700)
#if defined( CONFIG_SC14452 )
#  define SPI1_AD_INT_PEND	SPI1_ADC_INT_PEND
#  define SPI1_CTRL_REG		SPI1_CTRL_REG0
#endif



#ifdef CONFIG_RAM_SIZE_32MB

#  if !defined( CONFIG_BOARD_SMG_I )
#    define CONFIG_32M_SDRAM_PATCH
#  endif


#  ifdef CONFIG_32M_SDRAM_PATCH

#    define OVER_16M_SDRAM_ACCESS_PATCH_FLAG	register unsigned long m_32m_patch_flag
#    define PROTECT_OVER_16M_SDRAM_ACCESS	do { \
							local_irq_save(m_32m_patch_flag); \
							EBI_ACS4_CTRL_REG = 9; \
						} while( 0 )
#    define RESTORE_OVER_16M_SDRAM_ACCESS	do { \
							EBI_ACS4_CTRL_REG = 0xa; \
							local_irq_restore(m_32m_patch_flag); \
						} while( 0 )

#  else

#    define OVER_16M_SDRAM_ACCESS_PATCH_FLAG	do {} while( 0 )
#    define PROTECT_OVER_16M_SDRAM_ACCESS	do {} while( 0 )
#    define RESTORE_OVER_16M_SDRAM_ACCESS	do {} while( 0 )


#  endif

#else

#  define OVER_16M_SDRAM_ACCESS_PATCH_FLAG	do {} while( 0 )
#  define PROTECT_OVER_16M_SDRAM_ACCESS		do {} while( 0 )
#  define RESTORE_OVER_16M_SDRAM_ACCESS		do {} while( 0 )

#endif



#define DUMMY		0xffff

/* dummy buffers used when 1-way DMA is done */
static u16 dummy_tx_buffer = DUMMY ;
static u16 dummy_rx_buffer ;


/* transfers of size (in bytes) greater that this will DMA'ed, if applicable */
#define DMA_THRESHOLD	9

#define START_STATE	((void *)0)
#define RUNNING_STATE	((void *)1)
#define DONE_STATE	((void *)2)
#define ERROR_STATE	((void *)-1)
#define QUEUE_RUNNING	0
#define QUEUE_STOPPED	1

#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT )
#  ifdef CONFIG_RAM_SIZE_32MB
#    define SC1445x_LEGERITY_890_CS_REG	*(volatile unsigned short*)0x01080000
#  else
#    define SC1445x_LEGERITY_890_CS_REG	*(volatile unsigned short*)0x01400000
#  endif
#endif

#if defined( CONFIG_SPI_REDPINE_RS9110_LI )
#if defined( CONFIG_SC14450_VoIP_RevB ) || defined (CONFIG_RPS_BOARD)
#	define REDPINE_RS9110_CS_LOW	P0_RESET_DATA_REG = 0x80
#	define REDPINE_RS9110_CS_HIGH	P0_SET_DATA_REG = 0x80
#elif defined( CONFIG_SC14452 )
#	define REDPINE_RS9110_CS_HIGH 		P1_SET_DATA_REG = 0x8000 //though ignored by the cvm module
#	define REDPINE_RS9110_CS_LOW 		P1_RESET_DATA_REG = 0x8000 //though ignored by the cvm module
#else
#	define REDPINE_RS9110_CS_LOW	P0_RESET_DATA_REG = 0x40
#	define REDPINE_RS9110_CS_HIGH	P0_SET_DATA_REG = 0x40
#endif
#endif


#if defined( CONFIG_CVM480_SPI_DECT_SUPPORT )
#if defined( CONFIG_SC14452 )
#	define CVM480_SPI_DECT_CS_HIGH 		P1_SET_DATA_REG = 0x8000 //though ignored by the cvm module
#	define CVM480_SPI_DECT_CS_LOW 		P1_RESET_DATA_REG = 0x8000 //though ignored by the cvm module
#else
#	define CVM480_SPI_DECT_CS_HIGH 		P0_SET_DATA_REG = 0x80 //though ignored by the cvm module
#	define CVM480_SPI_DECT_CS_LOW 		P0_RESET_DATA_REG = 0x80 //though ignored by the cvm module
#endif
#endif

#if defined (CONFIG_SC1445x_ST7565P_LCD_SERIAL_SUPPORT)
unsigned char set_gpio=0;
	#if defined( CONFIG_SC14452 )
		#	define LCD_SPI_GPIO_HIGH 	P1_SET_DATA_REG = (1<<15)
		#	define LCD_SPI_GPIO_LOW 	P1_RESET_DATA_REG = (1<<15)
		#	define LCD_SPI_CS_HIGH		P2_SET_DATA_REG = (1<<10)
		#	define LCD_SPI_CS_LOW		P2_RESET_DATA_REG = (1<<10)
	#else
		#	define LCD_SPI_GPIO_HIGH 	P0_SET_DATA_REG = (1<<7)
		#	define LCD_SPI_GPIO_LOW 	P0_RESET_DATA_REG = (1<<7)
		#	define LCD_SPI_CS_HIGH		P2_SET_DATA_REG = (1<<6)
		#	define LCD_SPI_CS_LOW		P2_RESET_DATA_REG = (1<<6)
	#endif
#endif

#if defined (CONFIG_SC1445x_ST7567_LCD_SERIAL_SUPPORT)|| defined(CONFIG_SC1445x_ST7567_LCD_SERIAL_SUPPORT_OPTIMIZED)
unsigned char set_gpio=0;
	#if defined( CONFIG_SC14452 )
		#if defined( CONFIG_SMG_BOARDS )
			#ifdef CONFIG_BOARD_SMG_I
				#	define LCD_SPI_GPIO_HIGH 	P1_SET_DATA_REG = (1<<15)
				#	define LCD_SPI_GPIO_LOW 	P1_RESET_DATA_REG = (1<<15)
				#	define LCD_SPI_CS_HIGH		P1_SET_DATA_REG = (1<<0)
				#	define LCD_SPI_CS_LOW		P1_RESET_DATA_REG = (1<<0)
			#endif
		#else
			#	define LCD_SPI_GPIO_HIGH 	P2_SET_DATA_REG = (1<<9)
			#	define LCD_SPI_GPIO_LOW 	P2_RESET_DATA_REG = (1<<9)
			#	define LCD_SPI_CS_HIGH		P0_SET_DATA_REG = (1<<7)
			#	define LCD_SPI_CS_LOW		P0_RESET_DATA_REG = (1<<7)
		#endif
	#endif

#endif

#if defined (CONFIG_SC1445x_NT75451_LCD_SERIAL_SUPPORT)
unsigned char set_gpio=0;
	#if defined( CONFIG_SC14452 )
		#	define LCD_SPI_GPIO_HIGH 	P2_SET_DATA_REG = (1<<6)
		#	define LCD_SPI_GPIO_LOW 	P2_RESET_DATA_REG = (1<<6)
		#	define LCD_SPI_CS_HIGH		P1_SET_DATA_REG = (1<<15)
		#	define LCD_SPI_CS_LOW		P1_RESET_DATA_REG = (1<<15)
	#endif
#endif
#if defined (CONFIG_SC1445x_LED_SPI_SUPPORT)
#if defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	#	define LED_SPI_CS_HIGH		P2_SET_DATA_REG = (1<<13)
	#	define LED_SPI_CS_LOW		P2_RESET_DATA_REG = (1<<13)
#elif defined(CONFIG_L_V3_BOARD_CNX)
	#	define LED_SPI_CS_HIGH		P2_SET_DATA_REG = (1<<13)
	#	define LED_SPI_CS_LOW		P2_RESET_DATA_REG = (1<<13)
#else
	#	define LED_SPI_CS_HIGH		P1_SET_DATA_REG = (1<<1)
	#	define LED_SPI_CS_LOW		P1_RESET_DATA_REG = (1<<1)
#endif
#endif

#if defined (CONFIG_SC1445x_CNXT_SPI_SUPPORT)
#if defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	#	define CNXT_SPI_CS_HIGH		P2_SET_DATA_REG = (1<<1)
	#	define CNXT_SPI_CS_LOW		P2_RESET_DATA_REG = (1<<1)
#elif defined(CONFIG_L_V3_BOARD_CNX)
	#	define CNXT_SPI_CS_HIGH		P2_SET_DATA_REG = (1<<1)
	#	define CNXT_SPI_CS_LOW		P2_RESET_DATA_REG = (1<<1)
#endif

#endif

#if defined(CONFIG_L_V6_BOARD)
	#	define VEGA_SPI_CS_HIGH		P0_SET_DATA_REG = (1<<7)
	#	define VEGA_SPI_CS_LOW		P0_RESET_DATA_REG = (1<<7)
#endif

struct sc1445x_spi {
	/* Driver model hookup */
	struct platform_device* pdev ;

	/* SPI framework hookup */
	struct spi_master* master ;

	/* Regs base of SPI controller */
	unsigned short __iomem* CTRL_REG ;
	unsigned short __iomem* RX_TX_REG0 ;
	unsigned short __iomem* RX_TX_REG1 ;
	unsigned short __iomem* CLEAR_INT_REG ;
#if defined( CONFIG_SC14452 )
	unsigned short __iomem* CTRL_REG1 ;
#endif

	/* Driver message queue */
	struct workqueue_struct* workqueue ;
	struct work_struct pump_messages ;
	spinlock_t lock ;
	struct list_head queue ;
	int busy ;
	int run ;

	/* Message Transfer pump */
	struct tasklet_struct pump_transfers ;

	/* Current message transfer state info */
	struct spi_message* cur_msg ;
	struct spi_transfer* cur_transfer ;
	struct chip_data* cur_chip ;
	size_t len ;
	u8* tx ;
	u8* tx_end ;
	u8* rx ;
	u8* rx_end ;

	/* DMA */
	u8 dma_registered ;

	u8 n_bytes ;
	u8 cs_change ;
	void (* write)( struct sc1445x_spi* ) ;
	void (* read)( struct sc1445x_spi* ) ;
	void (* duplex)( struct sc1445x_spi* ) ;

	/* byte counter for burst transfers */
	unsigned burst_xfer_bytes ;
} ;


struct chip_data {
	u16 ctl_reg ;
	u16 spi_clk ;

	u8 chip_select_num ;
	u8 n_bytes ;
	u8 width ;		/* 0 or 1 */
	u8 enable_dma ;
	u8 bits_per_word ;	/* 8 or 16 */
	u8 cs_change_per_word ;
	void (* write)( struct sc1445x_spi* ) ;
	void (* read)( struct sc1445x_spi* ) ;
	void (* duplex)( struct sc1445x_spi* ) ;
} ;


#if 0
#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT )
#  define is_spi_ata_chip( chip )	\
		( chip->chip_select_num >= 1  &&  chip->chip_select_num <= 4 )
#else
#  define is_spi_ata_chip( x )		( 0 )
#endif
#endif

/* available chip selects for SPI slaves */
#define SPI_CS_COUNT	11


/* wait until the SPI transfer has finished */
static inline
void wait_until_xfer_done( struct sc1445x_spi* my_spi )
{
#if defined( CONFIG_SC14450 )

	while( !( *my_spi->CTRL_REG & SPI_INT_BIT ) )
		cpu_relax() ;
	while( *my_spi->CTRL_REG & SPI_INT_BIT )
		*my_spi->CLEAR_INT_REG = 1 ;

#elif defined( CONFIG_SC14452 )

	while( *my_spi->CTRL_REG1 & SPI_BUSY )
		cpu_relax() ;

#endif
}


static void spi_cs_activate( const struct sc1445x_spi* my_spi,
						const struct chip_data* chip )
{
#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT )
	OVER_16M_SDRAM_ACCESS_PATCH_FLAG;
#endif
	switch( chip->chip_select_num ) {
		case 0:		/* serial flash */
		case 7:		/* alternate serial flash */
#if defined( CONFIG_SC14452 )
			P0_RESET_DATA_REG = GPIO_3 ;
#else
			P0_RESET_DATA_REG = 0x400 ;
#endif
			break ;

#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT )
		case 1:		/* ATA */
#if defined (CONFIG_VT_DT_BOARD)
			P2_RESET_DATA_REG = 0x40; // Reset P2_06, CS0 active low
#elif defined (CONFIG_VT_MS20_CNSL_BOARD)
            P2_RESET_DATA_REG = 0x4; // MS20_4FXO. Reset P2_02, CS0 active low
#else
            PROTECT_OVER_16M_SDRAM_ACCESS;
			SC1445x_LEGERITY_890_CS_REG = 0xfffe;
			RESTORE_OVER_16M_SDRAM_ACCESS;
#endif
			break;
		case 2:
#if defined (CONFIG_VT_DT_BOARD)
            P2_RESET_DATA_REG = 0x4; // Reset P2_02, CS1 active low
#elif defined (CONFIG_VT_MS20_CNSL_BOARD)
            P1_RESET_DATA_REG = 0x4; // MS20_4FXO. Reset P1_02, CS1 active low
#else
            PROTECT_OVER_16M_SDRAM_ACCESS;
			SC1445x_LEGERITY_890_CS_REG = 0xfffd;
			RESTORE_OVER_16M_SDRAM_ACCESS;
#endif
			break;
		case 3:
#if defined (CONFIG_VT_MS20_CNSL_BOARD)
            P0_RESET_DATA_REG = 0x200; // MS20_4FXO. Reset P0_09, CS2 active low
#else
			PROTECT_OVER_16M_SDRAM_ACCESS;
			SC1445x_LEGERITY_890_CS_REG = 0xfffb;
			RESTORE_OVER_16M_SDRAM_ACCESS;
#endif
			break;
		case 4:
#if defined (CONFIG_VT_MS20_CNSL_BOARD)
            P0_RESET_DATA_REG = 0x400; // MS20_4FXO. Reset P0_10, CS3 active low
#else
			PROTECT_OVER_16M_SDRAM_ACCESS;
			SC1445x_LEGERITY_890_CS_REG = 0xfff7;
			RESTORE_OVER_16M_SDRAM_ACCESS;
#endif
			break;
#endif

#if defined( CONFIG_SPI_REDPINE_RS9110_LI )
		case 5:		/* WiFi */
			REDPINE_RS9110_CS_LOW;//P0_RESET_DATA_REG = 0x40 ;
			break ;
#endif
#if defined( CONFIG_CVM480_SPI_DECT_SUPPORT )
		case 6:		/* CVM480 dect spi */
			CVM480_SPI_DECT_CS_LOW;
			break ;
#endif
#if defined (CONFIG_SC1445x_ST7565P_LCD_SERIAL_SUPPORT) || defined (CONFIG_SC1445x_NT75451_LCD_SERIAL_SUPPORT) || defined (CONFIG_SC1445x_ST7567_LCD_SERIAL_SUPPORT) || defined(CONFIG_SC1445x_ST7567_LCD_SERIAL_SUPPORT_OPTIMIZED)
		case 8:		/* LCD spi */
			if(set_gpio) {LCD_SPI_GPIO_HIGH;} else {LCD_SPI_GPIO_LOW;}
			LCD_SPI_CS_LOW;
			break ;
#endif
#if defined (CONFIG_SC1445x_LED_SPI_SUPPORT)
		case 9:		/* LED spi */
			LED_SPI_CS_LOW;
			break ;
#endif
#if defined (CONFIG_SC1445x_CNXT_SPI_SUPPORT)
		case 10:		/* LED spi */
			CNXT_SPI_CS_LOW;
			break ;
#endif
#if defined (CONFIG_L_V6_BOARD)
		case 11:		
			VEGA_SPI_CS_LOW;
			break ;
#endif

		default:
			printk( KERN_ERR "%s: invalid SPI chip select %d\n",
					__FUNCTION__, chip->chip_select_num ) ;
	}
}

void spi_cs_deactivate( const struct sc1445x_spi* my_spi,
						const struct chip_data* chip )
{
#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT )
	OVER_16M_SDRAM_ACCESS_PATCH_FLAG;
#endif
volatile int dummy_data;
	switch( chip->chip_select_num ) {
		case 0:		/* serial flash */
		case 7:		/* alternate serial flash */
#if defined( CONFIG_SC14452 )
			P0_SET_DATA_REG = GPIO_3 ;
#else
			P0_SET_DATA_REG = 0x400 ;
#endif
			break ;

#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT )
		case 1:		/* ATA */
#if defined (CONFIG_VT_DT_BOARD)
            P2_SET_DATA_REG = 0x40; // Reset P2_06, CS0
#elif defined (CONFIG_VT_MS20_CNSL_BOARD)
            P2_SET_DATA_REG = 0x4; // MS20_4FXO. Reset P2_02, CS1
            break ;
#endif
		case 2:
#if defined (CONFIG_VT_DT_BOARD)
            P2_SET_DATA_REG = 0x4; // Reset P2_02, CS1
#elif defined (CONFIG_VT_MS20_CNSL_BOARD)
		    P1_SET_DATA_REG = 0x4; // MS20_4FXO. set P1_02, CS1 active low
            break ;
#endif
		case 3:
#if defined (CONFIG_VT_MS20_CNSL_BOARD)
		    P0_SET_DATA_REG = 0x200; // MS20_4FXO. set P0_09, CS2 active low
		     break;
#endif
		case 4:
#if defined (CONFIG_VT_MS20_CNSL_BOARD)
      P0_SET_DATA_REG = 0x400; // MS20_4FXO. Reset P0_10, CS3 active low
#endif
#if  ((!defined (CONFIG_VT_DT_BOARD)) || (!defined (CONFIG_VT_MS20_CNSL_BOARD)))
			PROTECT_OVER_16M_SDRAM_ACCESS;
			SC1445x_LEGERITY_890_CS_REG = 0xffff ;
			RESTORE_OVER_16M_SDRAM_ACCESS;
#endif
			break ;
#endif

#if defined( CONFIG_SPI_REDPINE_RS9110_LI )
		case 5:		/* WiFi */
			REDPINE_RS9110_CS_HIGH;//P0_SET_DATA_REG = 0x40 ;
			break ;
#endif
#if defined( CONFIG_CVM480_SPI_DECT_SUPPORT )
		case 6:		/* CVM480 dect spi */
			CVM480_SPI_DECT_CS_HIGH;
			break ;
#endif
#if defined (CONFIG_SC1445x_ST7565P_LCD_SERIAL_SUPPORT) || defined (CONFIG_SC1445x_NT75451_LCD_SERIAL_SUPPORT) || defined (CONFIG_SC1445x_ST7567_LCD_SERIAL_SUPPORT) || defined(CONFIG_SC1445x_ST7567_LCD_SERIAL_SUPPORT_OPTIMIZED)
		case 8:		/* LCD spi */
			LCD_SPI_CS_HIGH;
			break ;
#endif
#if defined (CONFIG_SC1445x_LED_SPI_SUPPORT)
		case 9:		/* LED spi */
			LED_SPI_CS_HIGH;
			dummy_data++;
			dummy_data++;
			LED_SPI_CS_LOW;
			//printk ("Deactivating .. \r\n");
			break ;
#endif
#if defined (CONFIG_SC1445x_CNXT_SPI_SUPPORT)
		case 10:		/* LED spi */
			CNXT_SPI_CS_HIGH;
			break ;
#endif
#if defined (CONFIG_L_V6_BOARD)
		case 11:		
			VEGA_SPI_CS_HIGH;
			break ;
#endif

		default:
			printk( KERN_ERR "%s: invalid SPI chip select %d\n",
					__FUNCTION__, chip->chip_select_num ) ;
	}
}

u16 hz_to_spi_clk( unsigned hz )
{
	if( hz >= 20000000 )
		return 2 ;
	else if( hz >= 10000000 )
		return 1 ;
	else if( hz >= 5000000 )
		return 0 ;
	else
		return 3 ;
}


static int flush( struct sc1445x_spi* my_spi )
{
	unsigned long limit = loops_per_jiffy << 1 ;

	/* wait for stop and clear stat */
#if defined( CONFIG_SC14450 )

	while( (*my_spi->CTRL_REG & SPI_INT_BIT) && limit-- )
		*my_spi->CLEAR_INT_REG = 1 ;

#elif defined( CONFIG_SC14452 )

	while( ( *my_spi->CTRL_REG1 & SPI_BUSY )  &&  limit-- )
		;

#endif

	return limit ;
}


/* used to kick off transfer in rx mode */
static inline unsigned short dummy_read( struct sc1445x_spi* my_spi )
{
	unsigned short tmp ;

	tmp = *my_spi->RX_TX_REG0 ;
	return tmp ;
}

static void null_writer( struct sc1445x_spi* my_spi )
{
	u8 n_bytes = my_spi->n_bytes ;

	while( my_spi->tx < my_spi->tx_end ) {
		*my_spi->RX_TX_REG0 = 0 ;
		wait_until_xfer_done( my_spi ) ;
		my_spi->tx += n_bytes ;
	}
}

static void null_reader( struct sc1445x_spi* my_spi )
{
	u8 n_bytes = my_spi->n_bytes ;

	while( my_spi->rx < my_spi->rx_end ) {
		*my_spi->RX_TX_REG0 = DUMMY ;
		wait_until_xfer_done( my_spi ) ;
		dummy_read( my_spi ) ;
		my_spi->rx += n_bytes ;
	}
}

static void u8_writer( struct sc1445x_spi* my_spi )
{
	while( my_spi->tx < my_spi->tx_end ) {
		*my_spi->RX_TX_REG0 = *(u8*)(my_spi->tx) ;
		wait_until_xfer_done( my_spi ) ;
		++my_spi->tx ;
	}
}

static void u8_writer_cs_change( struct sc1445x_spi* my_spi )
{
	struct chip_data* chip = my_spi->cur_chip ;

	while( my_spi->tx < my_spi->tx_end ) {
		spi_cs_activate( my_spi, chip ) ;

		*my_spi->RX_TX_REG0 = *(u8*)(my_spi->tx) ;
		wait_until_xfer_done( my_spi ) ;

		spi_cs_deactivate( my_spi, chip ) ;

		++my_spi->tx ;
	}
}

static void u8_reader( struct sc1445x_spi* my_spi )
{
	while( my_spi->rx < my_spi->rx_end ) {
		*my_spi->RX_TX_REG0 = DUMMY ;
		wait_until_xfer_done( my_spi ) ;
		*(u8*)(my_spi->rx) = *my_spi->RX_TX_REG0 ;
		++my_spi->rx ;
	}
}

static void u8_reader_cs_change( struct sc1445x_spi* my_spi )
{
	struct chip_data* chip = my_spi->cur_chip ;

	while( my_spi->rx < my_spi->rx_end ) {
		spi_cs_activate( my_spi, chip ) ;

		*my_spi->RX_TX_REG0 = DUMMY ;
		wait_until_xfer_done( my_spi ) ;
		*(u8*)(my_spi->rx) = *my_spi->RX_TX_REG0 ;

		spi_cs_deactivate( my_spi, chip ) ;

		++my_spi->rx ;
	}
}

static void u8_duplex( struct sc1445x_spi* my_spi )
{
	while( my_spi->rx < my_spi->rx_end ) {
		*my_spi->RX_TX_REG0 = *(u8*)(my_spi->tx) ;
		wait_until_xfer_done( my_spi ) ;
		*(u8*)(my_spi->rx) = *my_spi->RX_TX_REG0 ;
		++my_spi->rx ;
		++my_spi->tx ;
	}
}

static void u8_duplex_cs_change( struct sc1445x_spi* my_spi )
{
	struct chip_data* chip = my_spi->cur_chip ;

	while( my_spi->rx < my_spi->rx_end ) {
		spi_cs_activate( my_spi, chip ) ;

		*my_spi->RX_TX_REG0 = *(u8*)(my_spi->tx) ;
		wait_until_xfer_done( my_spi ) ;
		*(u8*)(my_spi->rx) = *my_spi->RX_TX_REG0 ;

		spi_cs_deactivate( my_spi, chip ) ;

		++my_spi->rx ;
		++my_spi->tx ;
	}
}


static void u16_writer( struct sc1445x_spi* my_spi )
{
	while( my_spi->tx < my_spi->tx_end ) {
		*my_spi->RX_TX_REG0 = *(u16*)(my_spi->tx) ;
		wait_until_xfer_done( my_spi ) ;
		my_spi->tx += 2 ;
	}
}

static void u16_writer_cs_change( struct sc1445x_spi* my_spi )
{
	struct chip_data* chip = my_spi->cur_chip ;

	while( my_spi->tx < my_spi->tx_end ) {
		spi_cs_activate( my_spi, chip ) ;

		*my_spi->RX_TX_REG0 = *(u16*)(my_spi->tx) ;
		wait_until_xfer_done( my_spi ) ;

		spi_cs_deactivate( my_spi, chip ) ;

		my_spi->tx += 2 ;
	}
}

static void u16_reader( struct sc1445x_spi* my_spi )
{
	while( my_spi->rx < my_spi->rx_end ) {
		*my_spi->RX_TX_REG0 = DUMMY ;
		wait_until_xfer_done( my_spi ) ;
		*(u16*)(my_spi->rx) = *my_spi->RX_TX_REG0 ;
		my_spi->rx += 2 ;
	}
}

static void u16_reader_cs_change( struct sc1445x_spi* my_spi )
{
	struct chip_data* chip = my_spi->cur_chip ;

	while( my_spi->rx < my_spi->rx_end ) {
		spi_cs_activate( my_spi, chip ) ;

		*my_spi->RX_TX_REG0 = DUMMY ;
		wait_until_xfer_done( my_spi ) ;
		*(u16*)(my_spi->rx) = *my_spi->RX_TX_REG0 ;

		spi_cs_deactivate( my_spi, chip ) ;

		my_spi->rx += 2 ;
	}
}

static void u16_duplex( struct sc1445x_spi* my_spi )
{
	while( my_spi->rx < my_spi->rx_end ) {
		*my_spi->RX_TX_REG0 = *(u16*)(my_spi->tx) ;
		wait_until_xfer_done( my_spi ) ;
		*(u16*)(my_spi->rx) = *my_spi->RX_TX_REG0 ;
		my_spi->rx += 2 ;
		my_spi->tx += 2 ;
	}
}

static void u16_duplex_cs_change( struct sc1445x_spi* my_spi )
{
	struct chip_data* chip = my_spi->cur_chip ;

	while( my_spi->rx < my_spi->rx_end ) {
		spi_cs_activate( my_spi, chip ) ;

		*my_spi->RX_TX_REG0 = *(u16*)(my_spi->tx) ;
		wait_until_xfer_done( my_spi ) ;
		*(u16*)(my_spi->rx) = *my_spi->RX_TX_REG0 ;

		spi_cs_deactivate( my_spi, chip ) ;

		my_spi->rx += 2 ;
		my_spi->tx += 2 ;
	}
}


/* test if ther is more transfer to be done */
static void* next_transfer( struct sc1445x_spi* my_spi )
{
	struct spi_message* msg = my_spi->cur_msg ;
	struct spi_transfer* trans = my_spi->cur_transfer ;

	/* Move to next transfer */
	if( trans->transfer_list.next != &msg->transfers ) {
		my_spi->cur_transfer =
		    list_entry(trans->transfer_list.next,
			       struct spi_transfer, transfer_list ) ;
		return RUNNING_STATE ;
	} else
		return DONE_STATE ;
}

/*
 * caller already set message->status;
 * give finished message back
 */
static void pump_messages( struct work_struct* work ) ;
static void giveback( struct sc1445x_spi* my_spi )
{
	unsigned long flags ;
	struct spi_message* msg ;
	struct chip_data* chip = my_spi->cur_chip ;

	spin_lock_irqsave( &my_spi->lock, flags ) ;
	msg = my_spi->cur_msg ;
	my_spi->cur_msg = NULL ;
	my_spi->cur_transfer = NULL ;
	my_spi->cur_chip = NULL ;
	/* keep the message pumping going... */
	queue_work( my_spi->workqueue, &my_spi->pump_messages ) ;
	my_spi->busy = 0 ;
	//pump_messages( &my_spi->pump_messages ) ;  /* this monopolizes cpu */
	spin_unlock_irqrestore( &my_spi->lock, flags ) ;

	msg->state = NULL ;

	if( !my_spi->cs_change )
		spi_cs_deactivate( my_spi, chip ) ;

//P0_RESET_DATA_REG = 0x40 ;
	if( msg->complete )
		msg->complete( msg->context ) ;
}


static void pump_transfers(unsigned long data)
{
	struct sc1445x_spi* my_spi = (struct sc1445x_spi*)data ;
	struct spi_message* message = NULL ;
	struct spi_transfer* transfer = NULL ;
	struct spi_transfer* previous = NULL ;
	struct chip_data* chip = NULL ;
	u8 width ;
	u16 tranf_success = 1 ;
	const unsigned bytes_limit = 128 ;

	do {
		/* Get current state information */
		message = my_spi->cur_msg ;
		transfer = my_spi->cur_transfer ;
		chip = my_spi->cur_chip ;

	 	/* Handle for abort */
		if( message->state == ERROR_STATE ) {
			message->status = -EIO ;
			giveback( my_spi ) ;
			my_spi->burst_xfer_bytes = 0 ;
			return ;
		}

		/* Handle end of message */
		if( message->state == DONE_STATE ) {
			message->status = 0 ;
			giveback( my_spi ) ;
			my_spi->burst_xfer_bytes = 0 ;
			return ;
		}

		/* Delay if requested at end of transfer */
		if( message->state == RUNNING_STATE ) {
			previous = list_entry( transfer->transfer_list.prev,
				      struct spi_transfer, transfer_list ) ;
#if 0
			if( is_spi_ata_chip( chip ) ) {
				//TODO: see if we can remove spi_cs_deactivate()
				previous->delay_usecs = 5 ;
				//udelay( 5 ) ;
				transfer->cs_change = 0 ;
				//spi_cs_deactivate( my_spi ) ;
			}
#endif
			if( previous->delay_usecs )
				udelay( previous->delay_usecs ) ;
		}

		/* Setup the transfer state based on the type of transfer */
		if( flush( my_spi ) == 0 ) {
			dev_err( &my_spi->pdev->dev,
					"pump_transfers: flush failed\n" ) ;
			message->status = -EIO ;
			giveback( my_spi ) ;
			my_spi->burst_xfer_bytes = 0 ;
			return ;
		}

		if( transfer->tx_buf != NULL ) {
			my_spi->tx = (void*)transfer->tx_buf ;
			my_spi->tx_end = my_spi->tx + transfer->len ;
			dev_dbg( &my_spi->pdev->dev, "tx_buf is %p, tx_end is %p\n",
				transfer->tx_buf, my_spi->tx_end ) ;
		} else {
			my_spi->tx = NULL ;
		}

		if( transfer->rx_buf != NULL ) {
			my_spi->rx = transfer->rx_buf ;
			my_spi->rx_end = my_spi->rx + transfer->len ;
			dev_dbg( &my_spi->pdev->dev, "rx_buf is %p, rx_end is %p\n",
					transfer->rx_buf, my_spi->rx_end ) ;
		} else {
			my_spi->rx = NULL;
		}

		my_spi->cs_change = transfer->cs_change ;

		/* Bits per word setup */
		switch( transfer->bits_per_word ) {
		case 8:
			my_spi->n_bytes = 1 ;
			width = 0 ;
			if( chip->cs_change_per_word ) {
				my_spi->read = u8_reader_cs_change ;
				my_spi->write = u8_writer_cs_change ;
				my_spi->duplex = u8_duplex_cs_change ;
			} else {
				my_spi->read = u8_reader ;
				my_spi->write = u8_writer ;
				my_spi->duplex = u8_duplex ;
			}
			break ;

		case 16:
			my_spi->n_bytes = 2 ;
			width = 1 ;
			if( chip->cs_change_per_word ) {
				my_spi->read = u16_reader_cs_change ;
				my_spi->write = u16_writer_cs_change ;
				my_spi->duplex = u16_duplex_cs_change ;
			} else {
				my_spi->read = u16_reader ;
				my_spi->write = u16_writer ;
				my_spi->duplex = u16_duplex ;
			}
			break ;

		default:
			/* No change, the same as default setting */
			my_spi->n_bytes = chip->n_bytes ;
			width = chip->width ;
			my_spi->write = my_spi->tx ? chip->write : null_writer ;
			my_spi->read = my_spi->rx ? chip->read : null_reader ;
			my_spi->duplex = chip->duplex ? chip->duplex
							: null_writer ;
			break ;
		}

		SetBits( *my_spi->CTRL_REG, SPI_WORD, width ) ;

		my_spi->len = transfer->len >> width ;
#if 1
		if( unlikely( my_spi->len << width  !=  transfer->len ) ) {
#if 1
			dev_err( &my_spi->pdev->dev, "transfer length "
				"(%d bytes) is not aligned (width=%d)\n",
					transfer->len, width ) ;
#else
			++my_spi->len ;
#endif
		}
#endif

		/* speed and width has been set on per message */
		message->state = RUNNING_STATE ;

		dev_dbg( &my_spi->pdev->dev,
			"now pumping a transfer: width is %d, len is %d\n",
					width, transfer->len ) ;

#if defined (CONFIG_SC1445x_ST7565P_LCD_SERIAL_SUPPORT) || defined (CONFIG_SC1445x_NT75451_LCD_SERIAL_SUPPORT) || defined (CONFIG_SC1445x_ST7567_LCD_SERIAL_SUPPORT) || defined(CONFIG_SC1445x_ST7567_LCD_SERIAL_SUPPORT_OPTIMIZED)
		if( transfer->set_gpio )
			set_gpio=1;
		else
			set_gpio=0;
#endif
		if( transfer->speed_hz )
			SetBits( *my_spi->CTRL_REG, SPI_CLK,
					hz_to_spi_clk( transfer->speed_hz ) ) ;
		else
			SetBits( *my_spi->CTRL_REG, SPI_CLK, chip->spi_clk ) ;
		spi_cs_activate( my_spi, chip ) ;

//P0_SET_DATA_REG = 0x40 ;

		if( chip->enable_dma  &&  transfer->len > DMA_THRESHOLD ) {
			/* DMA mode */
			u16 first_hw ;
			unsigned base ;

			dev_dbg( &my_spi->pdev->dev, "doing DMA transfer\n" ) ;

			while( SET_INT_PENDING_REG & SPI1_AD_INT_PEND )
				cpu_relax() ;

			if( my_spi->tx ) {
				base = (unsigned)(my_spi->tx + 2) ;
				DMA1_A_STARTH_REG = base >> 16 ;
				DMA1_A_STARTL_REG = base ;
				SetBits( DMA1_CTRL_REG, AINC, 1 ) ;
				first_hw = *(u16*)my_spi->tx ;
			} else {
				base = (unsigned)&dummy_tx_buffer ;
				DMA1_A_STARTH_REG = base >> 16 ;
				DMA1_A_STARTL_REG = base ;
				SetBits( DMA1_CTRL_REG, AINC, 0 ) ;
				first_hw = DUMMY ;
			}
			/* the first half-word will be written */
			/* below, the rest will be done automatically */
			DMA1_LEN_REG = my_spi->len - 2 ;
			DMA1_INT_REG = my_spi->len - 2 ;

			if( my_spi->rx ) {
				base = (unsigned)my_spi->rx ;
				DMA0_B_STARTH_REG = base >> 16 ;
				DMA0_B_STARTL_REG = base ;
				SetBits( DMA0_CTRL_REG, BINC, 1 ) ;
			} else {
				base = (unsigned)&dummy_rx_buffer ;
				DMA0_B_STARTH_REG = base >> 16 ;
				DMA0_B_STARTL_REG = base ;
				SetBits( DMA0_CTRL_REG, BINC, 0 ) ;
			}
			DMA0_LEN_REG = my_spi->len - 1 ;
			DMA0_INT_REG = my_spi->len - 1 ;

			SetBits( DMA0_CTRL_REG, DMA_ON, 1 ) ;
			SetBits( DMA1_CTRL_REG, DMA_ON, 1 ) ;

			/* kick off transfer */
			SPI1_RX_TX_REG0 = first_hw ;

			break ;
		}

		/* IO mode write then read */
		dev_dbg( &my_spi->pdev->dev, "doing IO transfer\n" ) ;

		if( my_spi->tx != NULL && my_spi->rx != NULL ) {
			/* full duplex */
			BUG_ON( ( my_spi->tx_end - my_spi->tx ) !=
				( my_spi->rx_end - my_spi->rx ) ) ;
			my_spi->duplex( my_spi ) ;

			if( my_spi->tx != my_spi->tx_end )
				tranf_success = 0 ;
		} else if( my_spi->tx != NULL ) {
			my_spi->write( my_spi ) ;

			if( my_spi->tx != my_spi->tx_end )
				tranf_success = 0 ;
		} else if( my_spi->rx != NULL ) {
			my_spi->read( my_spi ) ;
			if( my_spi->rx != my_spi->rx_end )
				tranf_success = 0 ;
		}
//P0_RESET_DATA_REG = 0x40 ;

		if( !tranf_success ) {
			//dev_dbg( &my_spi->pdev->dev, "IO error!\n" ) ;
			dev_printk( KERN_ERR, &my_spi->pdev->dev, "IO error!\n" ) ;
			message->state = ERROR_STATE ;
			break ;
		} else {
			/* Update total byte transfered */
			message->actual_length += transfer->len ;
			my_spi->burst_xfer_bytes += transfer->len ;

			/* Move to next transfer of this msg */
			message->state = next_transfer( my_spi ) ;

			/* Handle end of message here */
			if( message->state == DONE_STATE ) {
				message->status = 0 ;
				giveback( my_spi ) ;
				my_spi->burst_xfer_bytes = 0 ;
				return ;
			}
		}
	} while( my_spi->burst_xfer_bytes < bytes_limit ) ;

	my_spi->burst_xfer_bytes = 0 ;

	/* Schedule next transfer tasklet */
	tasklet_schedule( &my_spi->pump_transfers ) ;
}

/* pop a msg from queue and kick off real transfer */
static void pump_messages( struct work_struct* work )
{
	struct sc1445x_spi* my_spi ;
	unsigned long flags ;

	my_spi = container_of( work, struct sc1445x_spi, pump_messages ) ;

	/* Lock queue and check for queue work */
	spin_lock_irqsave( &my_spi->lock, flags ) ;
	if( list_empty( &my_spi->queue ) || my_spi->run == QUEUE_STOPPED ) {
		/* pumper kicked off but no work to do */
		my_spi->busy = 0 ;
		spin_unlock_irqrestore( &my_spi->lock, flags ) ;
		return ;
	}

	/* Make sure we are not already running a message */
	if( my_spi->cur_msg) {
		spin_unlock_irqrestore( &my_spi->lock, flags ) ;
		return ;
	}

	/* Extract head of queue */
	my_spi->cur_msg = list_entry( my_spi->queue.next,
				       struct spi_message, queue ) ;

	my_spi->cur_chip = spi_get_ctldata( my_spi->cur_msg->spi ) ;
	dev_dbg( &my_spi->pdev->dev, "restoring spi ctl state\n" ) ;
	*my_spi->CTRL_REG = my_spi->cur_chip->ctl_reg & ~SPI_ON ;
	SetBits( *my_spi->CTRL_REG, SPI_CLK, my_spi->cur_chip->spi_clk ) ;
	*my_spi->CTRL_REG |= SPI_ON ;
	spi_cs_activate( my_spi, my_spi->cur_chip ) ;

	list_del_init( &my_spi->cur_msg->queue ) ;

	/* Initial message state */
	my_spi->cur_msg->state = START_STATE ;
	my_spi->cur_transfer = list_entry( my_spi->cur_msg->transfers.next,
				    struct spi_transfer, transfer_list ) ;

	dev_dbg( &my_spi->pdev->dev, "got a message to pump, "
		"state is set to: ctl 0x%x\n",
		my_spi->cur_chip->ctl_reg ) ;

	dev_dbg( &my_spi->pdev->dev,
		"the first transfer len is %d\n",
		my_spi->cur_transfer->len ) ;

	spin_unlock_irqrestore( &my_spi->lock, flags ) ;

	my_spi->busy = 1 ;
	pump_transfers( (long)my_spi ) ;
}

/*
 * got a msg to transfer, queue it in drv_data->queue.
 * And kick off message pumper
 */
static int transfer( struct spi_device* spi, struct spi_message* msg )
{
	struct sc1445x_spi* my_spi = spi_master_get_devdata( spi->master ) ;
	unsigned long flags ;

//P0_SET_DATA_REG = 0x40 ;
	spin_lock_irqsave( &my_spi->lock, flags ) ;

	if( my_spi->run == QUEUE_STOPPED ) {
		spin_unlock_irqrestore( &my_spi->lock, flags ) ;
		return -ESHUTDOWN ;
	}

	msg->actual_length = 0 ;
	msg->status = -EINPROGRESS ;
	msg->state = START_STATE ;

	dev_dbg( &spi->dev, "adding an msg in transfer() \n" ) ;
	list_add_tail( &msg->queue, &my_spi->queue ) ;

	spin_unlock_irqrestore( &my_spi->lock, flags ) ;
	if( my_spi->run == QUEUE_RUNNING && !my_spi->busy )
		pump_messages( &my_spi->pump_messages ) ;

	return 0 ;
}


/* SPI1 DMA ISR */
static irqreturn_t spi1_dma_isr( int irq, void* devid )
{
	struct sc1445x_spi* my_spi = devid ;
	//struct chip_data* chip = my_spi->cur_chip ;
	struct spi_message* message = my_spi->cur_msg ;
	struct spi_transfer* transfer = my_spi->cur_transfer ;

	RESET_INT_PENDING_REG = SPI1_AD_INT_PEND ;

	dev_dbg( &my_spi->pdev->dev,  "entering %s\n", __FUNCTION__ ) ;
	/* Update total byte transfered */
	message->actual_length += transfer->len ;
	my_spi->burst_xfer_bytes += transfer->len ;

	/* Move to next transfer of this msg */
	message->state = next_transfer( my_spi ) ;

	/* Handle end of message here */
	if( message->state == DONE_STATE ) {
		message->status = 0 ;
		giveback( my_spi ) ;
		my_spi->burst_xfer_bytes = 0 ;
	} else
		tasklet_schedule( &my_spi->pump_transfers ) ;

	return IRQ_HANDLED ;
}


/* first setup for new devices */
static int setup( struct spi_device* spi )
{
	struct sc1445x_spi_chip* chip_info = NULL ;
	struct chip_data *chip ;
	struct sc1445x_spi* my_spi = spi_master_get_devdata( spi->master ) ;

	/* Abort device setup if requested features are not supported */
	if( spi->mode & ~( SPI_CPOL | SPI_CPHA | SPI_LSB_FIRST ) ) {
		dev_err( &spi->dev, "requested mode not fully supported\n" ) ;
		return -EINVAL ;
	}

	/* Zero (the default) here means 8 bits */
	if( !spi->bits_per_word )
		spi->bits_per_word = 8 ;

	if( spi->bits_per_word != 8  &&  spi->bits_per_word != 16 )
		return -EINVAL ;

	/* Only alloc (or use chip_info) on first setup */
	chip = spi_get_ctldata( spi ) ;
	if( !chip ) {
		chip = kzalloc( sizeof( struct chip_data ), GFP_KERNEL ) ;
		if( !chip )
			return -ENOMEM ;
		chip_info = spi->controller_data ;
	}

	if( chip_info ) {
		chip->enable_dma = chip_info->use_dma ;
		chip->bits_per_word = 8 << chip_info->word_width ;
		chip->cs_change_per_word = chip_info->cs_per_word ;
		dev_dbg( &my_spi->pdev->dev, "from chip_info: enable_dma=%d, "
				"bits_per_word=%d, cs_change_per_word=%d\n",
				chip->enable_dma, chip->bits_per_word,
				chip->cs_change_per_word ) ;
	}

	chip->ctl_reg = *my_spi->CTRL_REG ;

	/* SPI2 pins are configured in the bootloader */

	/* SPI1 pins configuration */
#if defined( CONFIG_SC14450_VoIP_RevB ) || defined (CONFIG_F_G2_P1_BOARD)
	/* Set PPA MATRIX FOR SPI1 */
	P0_06_MODE_REG = 0x0313 ;	/* P0_12 as SPI1_CLK */
	P2_05_MODE_REG = 0x0012 ;	/* P0_14 as SPI1_DI  */
	P2_08_MODE_REG = 0x0311 ;	/* P2_08 as SPI1_DO  */

#endif

#if defined( CONFIG_SC14452 )
#ifdef CONFIG_F_G2_ST7567_2PORT_BOARD
	/* Set PPA MATRIX FOR SPI1 */
	P0_10_MODE_REG = 0x0313 ;	/* P0_10 as SPI1_CLK */
	//P1_13_MODE_REG = 0x0012 ;	/* P0_14 as SPI1_DI  */ --not used in this configuration
	P0_08_MODE_REG = 0x0311 ;	/* P0_08 as SPI1_DO  */
#elif defined( CONFIG_SMG_BOARDS )
		#ifdef CONFIG_BOARD_SMG_I
			P2_10_MODE_REG = 0x0313 ;	/* P2_10 as SPI1_CLK */
			P2_08_MODE_REG = 0x0311 ;	/* P2_08 as SPI1_DO  */
		#endif
#elif defined( CONFIG_JW_BOARDS )
#elif defined(CONFIG_SC1445x_LED_SPI_SUPPORT) && (defined (CONFIG_L_V2_BOARD)||defined (CONFIG_L_V3_BOARD)|| defined (CONFIG_L_V4_BOARD)|| defined (CONFIG_L_V5_BOARD))
	P1_02_MODE_REG = 0x0313 ;	/* P0_12 as SPI1_CLK */
//	P1_13_MODE_REG = 0x0012 ;	/* P0_14 as SPI1_DI  */
	P1_00_MODE_REG = 0x0311 ;	/* P2_04 as SPI1_DO  */
#elif (defined(CONFIG_SC1445x_LED_SPI_SUPPORT)||defined(CONFIG_SC1445x_CNXT_SPI_SUPPORT)) && (defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX))
	P1_02_MODE_REG = 0x0313 ;	/* P0_12 as SPI1_CLK */
	P0_01_MODE_REG = 0x0012 ;	/* P0_14 as SPI1_DI  */
	P0_00_MODE_REG = 0x0311 ;	/* P2_04 as SPI1_DO  */
#elif defined (CONFIG_L_V6_BOARD)
	P1_02_MODE_REG = 0x0313 ;	/* P1_02 is SPI_CLK  */
	P1_13_MODE_REG = 0x0012 ;       /* P1_13 is SPI_DI  */
	P2_04_MODE_REG = 0x0311 ;	/* P2_04 is SPI_DO */
	P0_07_MODE_REG = 0x0300 ;	/* P0_07 as SPI_EN  */
#elif defined (CONFIG_VT_MS20_CNSL_BOARD) // MS20_4FXO
  P2_10_MODE_REG = 0x0313 ; /* P2_10 as SPI1_CLK */
  P2_09_MODE_REG = 0x0012 ; /* P0_14 as SPI1_DI  */
  P2_08_MODE_REG = 0x0311 ; /* P2_08 as SPI1_DO  */	
#else
	#ifndef CONFIG_VT_V1_BOARD
		/* Set PPA MATRIX FOR SPI1 */
		P1_02_MODE_REG = 0x0313 ;	/* P0_12 as SPI1_CLK */
		P1_13_MODE_REG = 0x0012 ;	/* P0_14 as SPI1_DI  */
		P2_04_MODE_REG = 0x0311 ;	/* P2_04 as SPI1_DO  */
	#endif
#endif
#if defined( CONFIG_SPI_REDPINE_RS9110_LI )
	P1_15_MODE_REG = 0x0300 ;	/* P1_15 as SPI_EN, for WiFi  */
#endif

	CLK_GPIO1_REG&=0x3ff;
//	CLK_GPIO1_REG|=62<<9;
	CLK_GPIO1_REG|=4<<10;
//	CLK_GPIO1_REG|=8<<10;

	CLK_GPIO3_REG&=~0x1000;
	CLK_GPIO3_REG|=0x1000;


#endif

#if defined(CONFIG_SC1445x_LED_SPI_SUPPORT)
#if defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	P2_13_MODE_REG = 0x0300 ;	/* P1_1 as SPI1 EN */
#elif defined(CONFIG_L_V3_BOARD_CNX)
	P2_13_MODE_REG = 0x0300 ;	/* P1_1 as SPI1 EN */
#else
	P1_01_MODE_REG = 0x0300 ;	/* P1_1 as SPI1 EN */
#endif
#endif

#if defined(CONFIG_SC1445x_CNXT_SPI_SUPPORT)
	#if defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	P2_01_MODE_REG = 0x0300 ;
	#elif defined(CONFIG_L_V3_BOARD_CNX)
	P2_01_MODE_REG = 0x0300 ;
	#endif
#endif
#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT )
	// P2_05 is ACS2
//	SetPort( P2_05_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_ACS2 ) ;
//	EBI_ACS2_LOW_REG = &SC1445x_LEGERITY_890_CS_REG ;
//	EBI_ACS2_CTRL_REG = 0x00000121 ;  // FLASH 1MB region
#endif
#if defined( CONFIG_SPI_REDPINE_RS9110_LI )
#if defined( CONFIG_SC14450_VoIP_RevB ) || defined (CONFIG_RPS_BOARD)
	P0_07_MODE_REG = 0x0300 ;	/* P0_07 as SPI_EN, for WiFi  */
#else
	#ifndef CONFIG_SC14452

	P0_06_MODE_REG = 0x0300 ;	/* P0_06 as SPI_EN, for WiFi  */
#endif
#endif
#endif
#if defined( CONFIG_CVM480_SPI_DECT_SUPPORT )
	//SetPort(P0_15_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_NONE);/* P0_15 as SPI_EN, for CVM480  */
	//P0_15_MODE_REG = 0x0300;/* P0_15 as SPI_EN, for CVM480  */
#if defined( CONFIG_SC14452 )
	P1_15_MODE_REG = 0x0300;/* P0_07 as SPI_EN, for CVM480  */
#else
	P0_07_MODE_REG = 0x0300;/* P0_07 as SPI_EN, for CVM480  */
#endif
#endif

#ifdef CONFIG_SC1445x_ST7565P_LCD_SERIAL_SUPPORT
	#if defined( CONFIG_SC14452 )
		P2_10_MODE_REG = 0x0300 ;	/* P0_10 as SPI_EN*/
		P1_15_MODE_REG = 0x0300;/* P0_07 as A0 (gpio)  */
	#else
		P2_06_MODE_REG = 0x0300 ;	/* P1_00  as SPI_EN*/
		P0_07_MODE_REG = 0x0300;/* P0_07 as A0 (gpio)  */
	#endif
#endif
#ifdef CONFIG_SC1445x_NT75451_LCD_SERIAL_SUPPORT
	#if defined( CONFIG_SC14452 )
		P1_15_MODE_REG = 0x0300 ;	/* P1_15 as SPI_EN*/
		P2_06_MODE_REG = 0x0300;	/* P2_06 as A0 (gpio)  */
	#endif
#endif
#if defined (CONFIG_SC1445x_ST7567_LCD_SERIAL_SUPPORT) || defined(CONFIG_SC1445x_ST7567_LCD_SERIAL_SUPPORT_OPTIMIZED)
	#if defined( CONFIG_SC14452 )
		#if defined( CONFIG_SMG_BOARDS )
			#ifdef CONFIG_BOARD_SMG_I
				P1_00_MODE_REG = 0x0300 ;	/* P0_07 as SPI_EN*/
				P1_15_MODE_REG = 0x0300;	/* P2_09 as A0 (gpio)  */
			#endif
		#else
			P0_07_MODE_REG = 0x0300 ;	/* P0_07 as SPI_EN*/
			P2_09_MODE_REG = 0x0300;	/* P2_09 as A0 (gpio)  */
		#endif
	#endif
#endif

	/* translate common spi framework into our register */
	chip->ctl_reg &= ~( SPI_POL | SPI_PHA ) ;
	if( spi->mode & SPI_CPOL )
		chip->ctl_reg |= SPI_POL ;
	if( spi->mode & SPI_CPHA )
		chip->ctl_reg |= SPI_PHA ;
	/* we dont support running in slave mode */
	chip->ctl_reg &= ~SPI_SMN ;
	/* we use polling (might be overriden below) */
	chip->ctl_reg &= ~SPI_MINT ;
	/* 8-bit words */
	//chip->ctl_reg &= ~SPI_WORD ;
	/* SPI clock frequency */
	chip->ctl_reg &= ~SPI_CLK ;
	chip->spi_clk = hz_to_spi_clk( spi->max_speed_hz ) ;
	SetBits( chip->ctl_reg, SPI_CLK, chip->spi_clk ) ;
	/* turn on */
	chip->ctl_reg |= SPI_ON ;

	BUG_ON( spi->chip_select >= SPI_CS_COUNT ) ;
	chip->chip_select_num = spi->chip_select ;

	switch( chip->bits_per_word ) {
		case 8:
			chip->n_bytes = 1 ;
			chip->width = 0 ;
			if( chip->cs_change_per_word ) {
				chip->read = u8_reader_cs_change ;
				chip->write = u8_writer_cs_change ;
				chip->duplex = u8_duplex_cs_change ;
			} else {
				chip->read = u8_reader ;
				chip->write = u8_writer ;
				chip->duplex = u8_duplex ;
			}
			break ;

		case 16:
			chip->n_bytes = 2 ;
			chip->width = 1 ;
			if( chip->cs_change_per_word ) {
				chip->read = u16_reader_cs_change ;
				chip->write = u16_writer_cs_change ;
				chip->duplex = u16_duplex_cs_change ;
			} else {
				chip->read = u16_reader ;
				chip->write = u16_writer ;
				chip->duplex = u16_duplex ;
			}
			break ;

		default:
			dev_err( &spi->dev,
					"%d bits_per_word is not supported\n",
						chip->bits_per_word ) ;
			kfree( chip ) ;
			return -ENODEV ;
	}

	SetBits( chip->ctl_reg, SPI_WORD, chip->width ) ;



	if( chip->enable_dma ) {
		if( my_spi->CTRL_REG != &SPI1_CTRL_REG ) {
			dev_err( &spi->dev, "can use DMA only with SPI1! "
					"disabling DMA for this device\n" ) ;
			printk("can use DMA only with SPI1!");
			chip->enable_dma = 0 ;
		} else if( chip->bits_per_word != 16 ) {
			dev_err( &spi->dev, "can use DMA only with 16-bit "
				"transfers! disabling DMA for this device\n" ) ;
			chip->enable_dma = 0 ;
		} else if( !my_spi->dma_registered ) {
			int res ;

			//TODO: initialize DMA
			DMA0_CTRL_REG = 0 ;
			DMA1_CTRL_REG = 0 ;

			/* RX */
			/* DMA source is SPI1_RX_TX_REG0 (0xFF4942) */
			DMA0_A_STARTH_REG = 0xFF ;
			DMA0_A_STARTL_REG = 0x4942 ;
			SetBits( DMA0_CTRL_REG, DMA_PRIO, 3 ) ;			
			SetBits( DMA0_CTRL_REG, DREQ_MODE, 1 ) ;
			SetBits( DMA0_CTRL_REG, BW, 1 ) ;
			SetBits( DMA0_CTRL_REG, DINT_MODE, 1 ) ;

			/* TX */
			/* DMA destination is SPI1_RX_TX_REG0 (0xFF4942) */
			DMA1_B_STARTH_REG = 0xFF ;
			DMA1_B_STARTL_REG = 0x4942 ;
			SetBits( DMA1_CTRL_REG, DMA_PRIO, 1 ) ;
			SetBits( DMA1_CTRL_REG, DREQ_MODE, 1 ) ;
			SetBits( DMA1_CTRL_REG, BW, 1 ) ;
			SetBits( DMA1_CTRL_REG, DINT_MODE, 1 ) ;

			/* register isr only for DMA0 (RX); since TX has */
			/* higher priority, when we get the RX interrupt */
			/* the transfer will have finished */
			RESET_INT_PENDING_REG = SPI1_AD_INT_PEND ;
			SetBits( INT2_PRIORITY_REG, SPI1_AD_INT_PRIO, 4 ) ;
			res = request_irq( SPI1_AD_INT, spi1_dma_isr, 0,
						"SPI1 DMA", (void*)my_spi ) ;
			if( res < 0 ) {
				dev_err( &spi->dev, "cannot install ISR for "
							"SPI1 DMA!\n" ) ;
				return -EPERM ;
			}

			my_spi->dma_registered = 1 ;
		}
	}

	dev_dbg( &spi->dev, "setup spi chip %s, width is %d\n",
					spi->modalias, chip->width ) ;
	dev_dbg( &spi->dev, "ctl_reg is 0x%x\n", chip->ctl_reg ) ;

	spi_set_ctldata(spi, chip);

	dev_dbg( &spi->dev, "chip select number is %d\n",
						chip->chip_select_num ) ;

	spi_cs_deactivate( my_spi, chip ) ;

	return 0 ;
}


/*
 * callback for spi framework.
 * clean driver specific data
 */
static void cleanup( struct spi_device* spi )
{
	struct chip_data* chip = spi_get_ctldata( spi ) ;

	kfree( chip ) ;
}


static inline int init_queue( struct sc1445x_spi* my_spi )
{
	INIT_LIST_HEAD( &my_spi->queue ) ;
	spin_lock_init( &my_spi->lock ) ;

	my_spi->run = QUEUE_STOPPED ;
	my_spi->busy = 0 ;

	/* init transfer tasklet */
	tasklet_init( &my_spi->pump_transfers,
		     pump_transfers, (unsigned long)my_spi ) ;

	/* init messages workqueue */
	INIT_WORK( &my_spi->pump_messages, pump_messages,
						&my_spi->pump_messages ) ;
	my_spi->workqueue =
	    create_singlethread_workqueue( my_spi->pdev->dev.parent->bus_id ) ;
	if( NULL == my_spi->workqueue )
		return -EBUSY ;

	return 0 ;
}

static inline int start_queue( struct sc1445x_spi* my_spi )
{
	unsigned long flags ;

//P2_10_MODE_REG = 0x300 ;  /* set to output (AD16) */
//P2_RESET_DATA_REG = 0x400 ;
//P0_06_MODE_REG = 0x300 ;
//P0_RESET_DATA_REG = 0x40 ;

	spin_lock_irqsave( &my_spi->lock, flags ) ;

	if( my_spi->run == QUEUE_RUNNING  ||  my_spi->busy ) {
		spin_unlock_irqrestore( &my_spi->lock, flags ) ;
		return -EBUSY ;
	}

	my_spi->run = QUEUE_RUNNING ;
	my_spi->cur_msg = NULL ;
	my_spi->cur_transfer = NULL ;
	my_spi->cur_chip = NULL ;
	spin_unlock_irqrestore( &my_spi->lock, flags ) ;

	//queue_work( my_spi->workqueue, &my_spi->pump_messages ) ;
	pump_messages( &my_spi->pump_messages ) ;

	return 0 ;
}

static inline int stop_queue( struct sc1445x_spi* drv_data )
{
	unsigned long flags;
	unsigned limit = 500;
	int status = 0;

	spin_lock_irqsave(&drv_data->lock, flags);

	/*
	 * This is a bit lame, but is optimized for the common execution path.
	 * A wait_queue on the drv_data->busy could be used, but then the common
	 * execution path (pump_messages) would be required to call wake_up or
	 * friends on every SPI message. Do this instead
	 */
	drv_data->run = QUEUE_STOPPED;
	while (!list_empty(&drv_data->queue) && drv_data->busy && limit--) {
		spin_unlock_irqrestore(&drv_data->lock, flags);
		msleep(10);
		spin_lock_irqsave(&drv_data->lock, flags);
	}

	if (!list_empty(&drv_data->queue) || drv_data->busy)
		status = -EBUSY;

	spin_unlock_irqrestore(&drv_data->lock, flags);

	return status;
}

static inline int destroy_queue( struct sc1445x_spi* drv_data )
{
	int status ;

	status = stop_queue( drv_data ) ;
	if( status != 0 )
		return status ;

	destroy_workqueue( drv_data->workqueue ) ;

	return 0 ;
}


static int sc1445x_spi_probe( struct platform_device* pdev )
{
	struct device* dev = &pdev->dev ;
	struct spi_master* master ;
	struct resource* res ;
	struct sc1445x_spi* my_spi = NULL ;
	int status = 0 ;

	/* Allocate master with space for sc1445x_spi */
	master = spi_alloc_master( dev, sizeof( struct sc1445x_spi ) ) ;
	if( !master ) {
		dev_err( &pdev->dev, "can not alloc spi_master\n" ) ;
		return -ENOMEM ;
	}

	my_spi = spi_master_get_devdata( master ) ;
	my_spi->master = master ;
	my_spi->pdev = pdev ;

	master->bus_num = pdev->id ;
	master->num_chipselect = SPI_CS_COUNT ;
	master->cleanup = cleanup ;
	master->setup = setup ;
	master->transfer = transfer ;

        /* Find and map our resources */
	res = platform_get_resource( pdev, IORESOURCE_MEM, 0 ) ;
	if( NULL == res ) {
		dev_err( dev, "Cannot get IORESOURCE_MEM\n" ) ;
		status = -ENOENT ;
		goto out_error_get_res ;
	}

	my_spi->CTRL_REG = ioremap( res->start, res->end - res->start ) ;
	if( NULL == my_spi->CTRL_REG ) {
		dev_err( dev, "Cannot map IO\n" ) ;
		status = -ENXIO ;
		goto out_error_ioremap;
	}
	my_spi->RX_TX_REG0 = my_spi->CTRL_REG + 1 ;
	my_spi->RX_TX_REG1 = my_spi->CTRL_REG + 2 ;
	my_spi->CLEAR_INT_REG = my_spi->CTRL_REG + 3 ;
#if defined( CONFIG_SC14452 )
	my_spi->CTRL_REG1 = my_spi->CTRL_REG + 4 ;
#endif
	my_spi->burst_xfer_bytes = 0 ;
	dev_dbg( dev,
		"SPI regs: CTRL@%p, RX_TX_0@%p, RX_TX_1@%p, CLEAR_INT@%p\n",
		my_spi->CTRL_REG, my_spi->RX_TX_REG0, my_spi->RX_TX_REG1,
						my_spi->CLEAR_INT_REG ) ;

	/* Initial and start queue */
	status = init_queue( my_spi ) ;
	if( status ) {
		dev_err( dev, "problem initializing queue\n" ) ;
		goto out_error_queue_alloc ;
	}

	status = start_queue( my_spi ) ;
	if( status ) {
		dev_err( dev, "problem starting queue\n" ) ;
		goto out_error_queue_alloc ;
	}

	/* Register with the SPI framework */
	platform_set_drvdata( pdev, my_spi ) ;
	status = spi_register_master( master ) ;
	if( status ) {
		dev_err( dev, "problem registering spi master\n" ) ;
		goto out_error_queue_alloc ;
	}

	dev_info( dev, "%s, Version %s\n", DRV_DESC, DRV_VERSION ) ;
	return status ;

out_error_queue_alloc:
	destroy_queue( my_spi ) ;
	iounmap( (void*)my_spi->CTRL_REG ) ;
out_error_ioremap:
out_error_get_res:
	spi_master_put( master ) ;

	return status ;
}


/* stop hardware and remove the driver */
static int __devexit sc1445x_spi_remove( struct platform_device* pdev )
{
	struct sc1445x_spi* my_spi = platform_get_drvdata( pdev ) ;
	int status = 0 ;

	if( !my_spi )
		return 0 ;

	/* Remove the queue */
	status = destroy_queue( my_spi ) ;
	if( status != 0 )
		return status ;

	iounmap( (void*)my_spi->CTRL_REG ) ;

	/* Disconnect from the SPI framework */
	spi_unregister_master( my_spi->master ) ;

	/* Prevent double remove */
	platform_set_drvdata( pdev, NULL ) ;

	return 0 ;
}


static struct platform_driver sc1445x_spi_driver = {
	.driver	= {
		.name	= DRV_NAME,
		.owner	= THIS_MODULE,
	},
	.probe		= sc1445x_spi_probe,
#if defined( CONFIG_PM )
	.suspend	= NULL,
	.resume		= NULL,
#endif
	.remove		= __devexit_p( sc1445x_spi_remove ),
} ;


static int __init sc1445x_spi_init( void )
{
	return platform_driver_register( &sc1445x_spi_driver ) ;
}

module_init( sc1445x_spi_init ) ;


static void __exit sc1445x_spi_exit(void)
{
	platform_driver_unregister( &sc1445x_spi_driver ) ;
}

module_exit( sc1445x_spi_exit ) ;

