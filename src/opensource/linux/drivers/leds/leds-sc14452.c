// File created by Gigaset Elements GmbH
// All rights reserved.
/* (c) 2012 Gigaset Elements GmbH
 *
 * Piotr Figlarek <piotr.figlarek@gigaset.com>
 *
 * SC14452 - LED driver
 * based on: drivers/leds/leds-s3c24xx.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/leds.h>

#include <asm/sc14452_led.h>
#include <asm/regs_sc14452.h>


struct sc14452_led_data {
    struct led_classdev     cdev;
    struct sc14452_led      *pdata;
};


/*
 * Set brightness according to request.
 *
 * Only On/OFF mode is supported.
 */
void sc14452_brightness_set(struct led_classdev *led_cdev, enum led_brightness brightness)
{
    struct sc14452_led_data *led = container_of(led_cdev, struct sc14452_led_data, cdev);

    if( brightness == LED_OFF )
        *led->pdata->reset_reg = led->pdata->port;
    else
        *led->pdata->set_reg = led->pdata->port;
}




static int sc14452_led_probe(struct platform_device *dev)
{
    int ret;
    struct sc14452_led_data *led;

    struct sc14452_led *cfg = dev->dev.platform_data;
	if( !cfg )
	{
	    dev_err(&dev->dev, "Missing configuration for LED device\n");
	}

	led = kzalloc(sizeof(struct sc14452_led_data), GFP_KERNEL);
    if (led == NULL) {
      dev_err(&dev->dev, "No memory for device\n");
      return -ENOMEM;
    }

    led->cdev.brightness_set = &sc14452_brightness_set;
    led->cdev.name = cfg->name;
    led->cdev.default_trigger = cfg->default_trigger;
    led->pdata = cfg;

    platform_set_drvdata(dev, led);

    ret = led_classdev_register(&dev->dev, &led->cdev);
    if (ret < 0) {
      dev_err(&dev->dev, "led_classdev_register failed\n");
      goto exit_err1;
    }

    // set port as OUTPUT
    SetPort(*cfg->port_mode_reg, GPIO_PUPD_OUT_NONE,  GPIO_PID_port);


	return 0;

 exit_err1:
	kfree(led);
	return ret;
}

static int sc14452_led_remove(struct platform_device *dev)
{
    struct sc14452_led_data *led = platform_get_drvdata(dev);

    led_classdev_unregister(&led->cdev);
    kfree(led);

    return 0;
}

#ifdef CONFIG_PM
static int sc14452_led_suspend(struct platform_device *dev, pm_message_t state)
{
	struct sc14452_led_data *led = container_of(led_cdev, struct sc14452_led_data, cdev);

	led_classdev_suspend(&led->cdev);
	return 0;
}

static int sc14452_led_resume(struct platform_device *dev)
{
    struct sc14452_led_data *led = container_of(led_cdev, struct sc14452_led_data, cdev);

	led_classdev_resume(&led->cdev);
	return 0;
}
#else
#define sc14452_led_suspend NULL
#define sc14452_led_resume NULL
#endif

static struct platform_driver sc14452_led_driver = {
	.probe		= sc14452_led_probe,
	.remove		= sc14452_led_remove,
	.suspend	= sc14452_led_suspend,
	.resume		= sc14452_led_resume,
	.driver		= {
		.name		= "sc14452-led",
		.owner		= THIS_MODULE,
	},
};

static int __init sc14452_led_init(void)
{
	return platform_driver_register(&sc14452_led_driver);
}

static void __exit sc14452_led_exit(void)
{
 	platform_driver_unregister(&sc14452_led_driver);
}

module_init(sc14452_led_init);
module_exit(sc14452_led_exit);

MODULE_AUTHOR("Piotr Figlarek <piotr.figlarek@gigaset.com>");
MODULE_DESCRIPTION("SC14452 LED driver");
MODULE_LICENSE("GPL");
