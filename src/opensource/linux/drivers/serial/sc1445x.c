// File created by Gigaset Elements GmbH
// All rights reserved.
/*
 * drivers/serial/sc1445x.c -- Serial I/O using SC1445x on-chip UART
 *
 *  Copyright (C) 2001,02,03  Jonathan Mikhailovich Short MPC Data Limited
 *  Copyright (C) 2001,02,03  NEC Electronics Corporation
 *  Copyright (C) 2001,02,03  Miles Bader <miles@gnu.org>
 *
 * This file is subject to the terms and conditions of the GNU General
 * Public License.  See the file COPYING in the main directory of this
 * archive for more details.
 *
 * Mostly by Miles Bader <miles@gnu.org>
 * 
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/console.h>
#include <linux/tty.h>
#include <linux/tty_flip.h>
#include <linux/serial.h>
#include <linux/serial_core.h>


#include <asm/io.h>
#include <asm/irq.h>
#include <asm/regs.h>
#if defined( CONFIG_SC14450 )
#  include <asm/demo450.h>
#elif defined( CONFIG_SC14452 )
#  include <asm/demo452.h>
#endif
#include <asm/sc1445x_uart.h>


#if defined(CONFIG_L_V1_BOARD) || defined(CONFIG_L_V2_BOARD)  || defined(CONFIG_L_V3_BOARD)|| defined(CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD)\
	|| defined(CONFIG_L_V2_BOARD_CNX)|| defined(CONFIG_L_V3_BOARD_CNX)|| defined(CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX)

#if defined(CONFIG_L_V2_BOARD) || defined(CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD)\
	|| defined(CONFIG_L_V2_BOARD_CNX)|| defined(CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX)
#define RTSCTSDSRSUPPORT
#endif
#define SOFTSTOPRXSUPPORT
#define RXDISABLENOCABLE

#ifdef RXDISABLENOCABLE

/***   Macros   ***/
#define	START_TIMER(TIMER, TMOUT) {TIMER.expires = jiffies + TMOUT;add_timer(&TIMER);}
#define	STOP_TIMER(TIMER) {del_timer(&TIMER);}


unsigned int sc14452_uart_status_flag=0;
unsigned int sc14452_uart_filter_value=0;
const unsigned int sc14452_uart_timer_period_pattern [10]={20,15,10,5,4,3,2,1,1,1};
struct timer_list sc14452_uart_timer;


#endif

#ifdef SOFTSTOPRXSUPPORT
volatile unsigned short stop_sc14452_uart_rx=1;
volatile char sc14452_enable_uart_rx_pin_buf[5];
volatile char sc14452_enable_uart_rx_buf_flag=0;
volatile int sc14452_enable_uart_rx_pin_indx=0;
const char sc14452_enable_uart_rx_pin_code[5]="1234";
#endif

#ifdef RTSCTSDSRSUPPORT
	#if (defined(CONFIG_L_V2_BOARD) || defined (CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD))

	volatile unsigned short sc14452_console_mode=0;

	#define NO_DSS_MODULE_CONNECTED 0
	#define SYSTEM_DSS_CONNECTED 1
	#define CARRIER_DSS_CONNECTED 2

	volatile unsigned short sc14452_DSS_TYPE=NO_DSS_MODULE_CONNECTED;

	#define DSS_TYPE_TEST_SET	(P3_SET_DATA_REG=(1<<2))
	#define DSS_TYPE_TEST_RESET	(P3_RESET_DATA_REG=(1<<2))
	#define DSS_TYPE_TEST_MODEREG	P3_02_MODE_REG

	#define DSS_TYPE_EN_SET	(P2_SET_DATA_REG=(1<<13))
	#define DSS_TYPE_EN_RESET	(P2_RESET_DATA_REG=(1<<13))
	#define DSS_TYPE_EN_MODEREG	P2_13_MODE_REG
	#define DSSPORT   	((P1_DATA_REG&(1<<15)))
	#define DSSMODE_REG	 P1_15_MODE_REG

	#define RTSSET		 (P2_SET_DATA_REG=(1<<15))
	#define RTSRESET	 (P2_RESET_DATA_REG=(1<<15))
	#define RTSMODE_REG	 P2_15_MODE_REG

	#define CTSMODE_REG  P2_14_MODE_REG
	#define CTSPORT   	((P2_DATA_REG&(1<<14)))

	#endif

	#if (defined(CONFIG_L_V2_BOARD_CNX)|| defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX))

	volatile unsigned short sc14452_console_mode=0;
	extern unsigned int dss_type_test;
	extern unsigned int gpio_leds_mask;
	extern unsigned int lcd_backlight;

	#define NO_DSS_MODULE_CONNECTED 0
	#define SYSTEM_DSS_CONNECTED 1
	#define CARRIER_DSS_CONNECTED 2

	volatile unsigned short sc14452_DSS_TYPE=NO_DSS_MODULE_CONNECTED;

	#define KBD_BASE_ADDR 0x01090000
	#define COL_WR_BUF   (*(volatile unsigned short *)(KBD_BASE_ADDR))
	#ifdef CONFIG_RAM_SIZE_32MB
		#define ASM_KBD_BASE_ADDR_VAL (KBD_BASE_ADDR)
		#define ASM_KBD_PRE_DEL_VAL 2
		#define ASM_KBD_POST_DEL_VAL 2
		#define ASM_32MB_CS_VAL 0xa
		#define ASM_16MB_CS_VAL 0x9

		#define ETH_RST_BLOFF_COL (0x4000)

	#define set_col(X,ASM_KBD_BASE_ADDR,ASM_KBD_PRE_DEL,ASM_KBD_POST_DEL,ASM_32MB_CS,ASM_16MB_CS) \
		({\
		register short __r4 __asm__ ("r4") = (short)(X) ; \
		asm volatile(\
       "sprd psr,(r3,r2) /*save the interrupts*/ \n\t"\
       "di     /*disable interrupts*/  \n\t"\
       "nop \n\t"\
       "movd %5,(r1,r0)\n\t /*small delay*/  \n\t"\
       "1: addd $-1:s,(r1,r0) \n\t"\
       "bcs 1b\n\t"\
       "movd %3,(r1,r0) /*change to 16 MB mode*/  \n\t"\
       "nop \n\t"\
       "nop \n\t"\
       "stord (r1,r0),0x0ff0064 \n\t"\
       "movd %2,(r1,r0) \n\t"\
       "storw r4,0x0:(r1,r0) /*write value to col*/  \n\t"\
       "nop \n\t"\
       "movd %6,(r1,r0)    /*small delay*/  \n\t"\
       "2: addd $-1:s,(r1,r0) \n\t"\
       "bcs 2b\n\t"\
       "movd %4,(r1,r0) /*restore to 32 MB mode*/  \n\t"\
       "stord (r1,r0),0x0ff0064 \n\t"\
       "lprd (r3,r2), psr /*restore to interrupts*/  \n\t"\
       "nop \n\t"\
       "nop \n\t"\
       :"=r" (__r4) \
       : "0" (__r4),"i" (ASM_KBD_BASE_ADDR),"i" (ASM_16MB_CS),"i" (ASM_32MB_CS),"i" (ASM_KBD_PRE_DEL),"i" (ASM_KBD_POST_DEL) \
       : "r0","r1","r2","r3"); \
		})

		#define SET_COLUMNS(X) {set_col((X)|ETH_RST_BLOFF_COL|((gpio_leds_mask)<<8)|(lcd_backlight<<13)|(dss_type_test<<15),\
									ASM_KBD_BASE_ADDR_VAL,\
									ASM_KBD_PRE_DEL_VAL,\
									ASM_KBD_POST_DEL_VAL,\
									ASM_32MB_CS_VAL,ASM_16MB_CS_VAL);}

		#else
		#define SET_COLUMNS(X) {COL_WR_BUF=X|ETH_RST_BLOFF_COL|((gpio_leds_mask)<<8)|(lcd_backlight<<13)|(dss_type_test<<15);}
		#endif


		#define DSS_TYPE_TEST_SET	{dss_type_test=1; SET_COLUMNS(0xFF);}
		#define DSS_TYPE_TEST_RESET	{dss_type_test=0; SET_COLUMNS(0xFF);}

		#define DSSPORT   	((P2_DATA_REG&(1<<14)))
		#define DSSMODE_REG	 P2_14_MODE_REG

		#define RTSSET		 (P2_SET_DATA_REG=(1<<15))
		#define RTSRESET	 (P2_RESET_DATA_REG=(1<<15))
		#define RTSMODE_REG	 P2_15_MODE_REG

		#define CTSMODE_REG  P2_00_MODE_REG
		#define CTSPORT   	((P2_DATA_REG&(1<<0)))

	#endif

#endif
#endif
#ifndef CONFIG_SC14452_ES2
/* uncomment to use DMA for UART RX */
#define USE_RX_DMA
#endif

#if defined( USE_RX_DMA )
#ifdef CONFIG_SC14452
static volatile unsigned char* uart_rx_buffer = (void*)0x0021800 ;
#else
static volatile unsigned char* uart_rx_buffer = (void*)0x1000 ;
#endif
#define UART_RX_BUFFER_SIZE	128

static struct tasklet_struct sc1445x_uart_tasklet ;

static unsigned short dma_read_pos = 0 ;

#endif

/* Initial UART state.  This may be overridden by machine-dependent headers. */
#ifndef SC1445x_UART_INIT_BAUD
#define SC1445x_UART_INIT_BAUD	115200
#endif
#ifndef SC1445x_UART_INIT_CFLAGS
#define SC1445x_UART_INIT_CFLAGS	(B115200 | CS8 | CREAD)
#endif

/* A string used for prefixing printed descriptions; since the same UART
   macro is actually used on other chips than the SC1445x.  This must be a
   constant string.  */
#ifndef SC1445x_UART_CHIP_NAME
#define SC1445x_UART_CHIP_NAME	"SC1445x"
#endif

#define SC1445x_UART_MINOR_BASE	64	   /* First tty minor number */


//vm 
volatile unsigned short uart_arbit_flag = 0;
volatile unsigned short uart_tx_int_flag = 0;
volatile unsigned short clk_reg_value;
/* Low-level UART functions.  */

#ifdef RXDISABLENOCABLE
static void sc14452_uart_timer_fn(unsigned long param)
{
	unsigned int port_value;
#ifdef RTSCTSDSRSUPPORT
	if (sc14452_console_mode)
#endif
	{
#if defined(CONFIG_L_V2_BOARD_CNX)|| defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX)
		port_value=P2_DATA_REG&(0x1<<9);
#elif defined(CONFIG_L_V2_BOARD) || defined(CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD)
		port_value=P0_DATA_REG&(0x1<<1);
#endif
		if (!sc14452_uart_status_flag) 	//trying to enable
		{
			if (!port_value)
			{
			//still the line is zero
			sc14452_uart_filter_value=0;
			START_TIMER(sc14452_uart_timer, sc14452_uart_timer_period_pattern[sc14452_uart_filter_value]);
			}
			else
			{
				//still the line is zero
				sc14452_uart_filter_value++;
				if (sc14452_uart_filter_value>=10)
				{
					sc14452_uart_filter_value=0;
					sc14452_uart_status_flag=1;
					printk("\nThe UART RX may be enabled. Type password. \n");
					START_TIMER(sc14452_uart_timer, sc14452_uart_timer_period_pattern[sc14452_uart_filter_value]);
				}
			else
			START_TIMER(sc14452_uart_timer, sc14452_uart_timer_period_pattern[sc14452_uart_filter_value]);
		}
	}
	else
	{
		if (port_value)
		{
			//still the line is zero
			sc14452_uart_filter_value=0;
			START_TIMER(sc14452_uart_timer, sc14452_uart_timer_period_pattern[sc14452_uart_filter_value]);
		}
		else
		{
			//still the line is zero
			sc14452_uart_filter_value++;
			if (sc14452_uart_filter_value>=10)
			{
				sc14452_uart_filter_value=0;
				sc14452_uart_status_flag=0;
				stop_sc14452_uart_rx=1;
				printk("The UART RX is disabled\n");
				START_TIMER(sc14452_uart_timer, sc14452_uart_timer_period_pattern[sc14452_uart_filter_value]);
			}
			else
			START_TIMER(sc14452_uart_timer, sc14452_uart_timer_period_pattern[sc14452_uart_filter_value]);
		}
	}
	}
#ifdef RTSCTSDSRSUPPORT
	else
	{
		sc14452_uart_status_flag=1;
		sc14452_uart_filter_value=0;
		stop_sc14452_uart_rx=0;
	}
#endif
}
#endif

/* Configure and turn on uart channel CHAN, using the termios `control
   modes' bits in CFLAGS, and a baud-rate of BAUD.  */
void sc1445x_uart_configure (unsigned chan, unsigned cflags, unsigned baud)
{
	int flags;
//vm	
//	unsigned short new_config = UART_CTRL_TEN | UART_CTRL_REN;
	unsigned short new_config = UART_CTRL_REN;

	/* Disable interrupts while we're twiddling the hardware.  */
	local_irq_save (flags);

	if(baud){
		switch(baud){
			case 115200: new_config |= UART_CTRL_BAUDRATE_115K2; break;
			case 19200 : new_config |= UART_CTRL_BAUDRATE_19K2; break;
			case 57600 : new_config |= UART_CTRL_BAUDRATE_57K6; break;
			case 9600  : new_config |= UART_CTRL_BAUDRATE_9K6; break;
			#if defined (CONFIG_L_V2_BOARD) || defined (CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD) \
			|| defined(CONFIG_L_V2_BOARD_CNX) || defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX)
			case 38400 : new_config |= UART_CTRL_BAUDRATE_19K2; CLK_GPIO6_REG=0x48; break;
			#endif
		}
	}
	else
//vm
//		new_config = UART_CTRL_TEN | UART_CTRL_REN | UART_CTRL_BAUDRATE_115K2;
		new_config = UART_CTRL_REN | UART_CTRL_BAUDRATE_115K2;

	UART_CTRL_REG = new_config;

	local_irq_restore (flags);
}


#if defined (CONFIG_L_V2_BOARD) || defined (CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD)\
	|| defined(CONFIG_L_V2_BOARD_CNX)|| defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX)
#ifdef RTSCTSDSRSUPPORT
static int detect_DSS_type()
{
	volatile int dummy;


#if !(defined(CONFIG_L_V2_BOARD_CNX) || defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX))
	DSS_TYPE_EN_SET;
#endif

	DSS_TYPE_TEST_RESET;

#if !(defined(CONFIG_L_V2_BOARD_CNX)|| defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX))
	DSS_TYPE_TEST_MODEREG =0x300;
	DSS_TYPE_EN_MODEREG =0x300;
	DSS_TYPE_EN_RESET;
#endif

	for (dummy=0;dummy<100;dummy++);

	if (DSSPORT)
	{
		// TEST WITH ZERO READING ONE
		//
#if !(defined(CONFIG_L_V2_BOARD_CNX)|| defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX))
		DSS_TYPE_EN_SET;
#endif
		DSS_TYPE_TEST_SET;

#if !(defined(CONFIG_L_V2_BOARD_CNX)|| defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX))
		DSS_TYPE_TEST_MODEREG =0x00;
		DSS_TYPE_EN_MODEREG =0x00;
#endif

		return (CARRIER_DSS_CONNECTED);
	}
	else
	{
		// TEST WITH ONE
		//
		DSS_TYPE_TEST_SET;
		for (dummy=0;dummy<100;dummy++);
		if (DSSPORT)
		{
			// TEST WITH ONE READING ONE
#if !(defined(CONFIG_L_V2_BOARD_CNX)|| defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX))
			DSS_TYPE_EN_SET;
			DSS_TYPE_TEST_SET;
			DSS_TYPE_TEST_MODEREG =0x00;
			DSS_TYPE_EN_MODEREG =0x00;
#else
			DSS_TYPE_TEST_RESET;
#endif
			return(NO_DSS_MODULE_CONNECTED);
		}
		else

		{
			// TEST WITH ONE READING ZERO
#if !(defined(CONFIG_L_V2_BOARD_CNX)|| defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX))
			DSS_TYPE_EN_SET;
#endif
			DSS_TYPE_TEST_RESET;
#if !(defined(CONFIG_L_V2_BOARD_CNX)|| defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX))
			DSS_TYPE_TEST_MODEREG =0x00;
			DSS_TYPE_EN_MODEREG =0x00;
#endif
			return(SYSTEM_DSS_CONNECTED);
		}
	}


}
#endif
#endif
/*  Low-level console. */

#ifdef CONFIG_SC1445x_CONSOLE

static void sc1445x_uart_cons_write (struct console *co,
				   const char *s, unsigned count)
{


	if (count > 0) {
		unsigned irq = UART_TI_INT;
		int irq_was_enabled, irq_was_pending, flags;

		/* We don't want to get `transmission completed'
		   interrupts, since we're busy-waiting, so we disable them
		   while sending (we don't disable interrupts entirely
		   because sending over a serial line is really slow).  We
		   save the status of the tx interrupt and restore it when
		   we're done so that using printk doesn't interfere with
		   normal serial transmission (other than interleaving the
		   output, of course!).  This should work correctly even if
		   this function is interrupted and the interrupt printks
		   something.  */

		/* Disable interrupts while fiddling with tx interrupt.  */
		local_irq_save (flags);
		/* Get current tx interrupt status.  */
		irq_was_enabled = sc1445x_irq_enabled (irq);
		irq_was_pending = sc1445x_irq_pending (irq);
		/* Disable tx interrupt if necessary.  */
		if (irq_was_enabled)
			disable_irq(irq);
		/* Turn interrupts back on.  */

		local_irq_restore (flags);

		if (uart_tx_int_flag) {
			sc1445x_uart_wait_for_xmit_complete;
			sc1445x_uart_wait_for_xmit_ok;
		}

		/* Send characters.  */
		while (count > 0) {
			int ch = *s++;

	//vm
			if (ch == '\n') {
				/* We don't have the benefit of a tty
				   driver, so translate NL into CR LF.  */
				sc1445x_uart_putc('\r');
				UART_CTRL_REG |= UART_CTRL_TEN;
				sc1445x_uart_wait_for_xmit_complete;
				sc1445x_uart_wait_for_xmit_ok;
			}
			
			sc1445x_uart_putc(ch);
			UART_CTRL_REG |= UART_CTRL_TEN;
			sc1445x_uart_wait_for_xmit_complete;
			sc1445x_uart_wait_for_xmit_ok;
			count--;

		}
		UART_CTRL_REG &= ~UART_CTRL_TEN;

		local_irq_save (flags);	
		/* Restore saved tx interrupt status.  */
		if (irq_was_enabled) {
			/* Clear pending interrupts received due
			   to our transmission, unless there was already
			   one pending, in which case we want the
			   handler to be called.  */
			if (!(uart_arbit_flag || uart_tx_int_flag )){
				if (! irq_was_pending)
					sc1445x_irq_clear_pending (irq);
			}
			uart_tx_int_flag = 0;
			/* ... and then turn back on handling.  */
			enable_irq (irq);	
		}
		local_irq_restore (flags);	
	}

}

static struct uart_driver sc1445x_uart_driver;
static struct console sc1445x_uart_cons =
{
    .name	= "ttyS",
    .write	= sc1445x_uart_cons_write,
    .device	= uart_console_device,
    .flags	= CON_PRINTBUFFER,
    .cflag	= SC1445x_UART_INIT_CFLAGS,
    .index	= -1,
    .data	= &sc1445x_uart_driver,
};

void sc1445x_uart_cons_init (unsigned chan)
{

	sc1445x_uart_configure (chan, SC1445x_UART_INIT_CFLAGS,
			      SC1445x_UART_INIT_BAUD);
	sc1445x_uart_cons.index = chan;
	register_console (&sc1445x_uart_cons);
	printk ("Console: %s on-chip UART channel %d\n",
		SC1445x_UART_CHIP_NAME, chan);
}

void sc1445x_uart_cons_fini( void )
{
	int res ;

	printk( "Unregistering console @%p...\n", &sc1445x_uart_cons ) ;
	res = unregister_console( &sc1445x_uart_cons ) ;
	printk( "...%s\n", res ?  "FAILED" :  "OK" ) ;
}

/* This is what the init code actually calls.  */
static int sc1445x_uart_console_init (void)
{

#if ((defined(CONFIG_L_V2_BOARD) || defined (CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD)\
		|| defined(CONFIG_L_V2_BOARD_CNX)|| defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX)) && defined (RTSCTSDSRSUPPORT))
	volatile int dummy;
	int dsstype;

	DSSMODE_REG=0x000;
#if defined(CONFIG_L_V2_BOARD_CNX)|| defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX)

	P2_RESET_DATA_REG = (1<<0);
	SetPort(P2_00_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_port);        /* CLOSE THE CLASSD FUNCTION */\
	P2_RESET_DATA_REG = (1<<1);
	SetPort(P2_01_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_port);    /* CLOSE THE CLASSD FUNCTION */


	EBI_SMTMGR_SET1_REG=((1<<16)|(0x7<<10)|(2<<8)|(1<<6)|(0x7<<0));
	SetPort(P0_02_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_ACS2);        /* P2_09 is ACS2 */
	EBI_ACS2_CTRL_REG= 0x121;						/* use time setting 1, memory is sram, size is 64K */
	EBI_ACS2_LOW_REG=0x01090000;					/* ACS2 base address is 0x1090000 */
#endif
	sc14452_DSS_TYPE=detect_DSS_type();
	printk ("DSS Type %d \n",sc14452_DSS_TYPE);

	sc14452_console_mode=1;
	sc1445x_uart_driver.cons=&sc1445x_uart_cons;

	switch (sc14452_DSS_TYPE)

	{
		case SYSTEM_DSS_CONNECTED:
		case CARRIER_DSS_CONNECTED:
							sc14452_console_mode=0;
							sc1445x_uart_driver.cons=0;
							return 0;
							break;
		case NO_DSS_MODULE_CONNECTED:
		default:
							sc14452_console_mode=1;
							sc1445x_uart_driver.cons=&sc1445x_uart_cons;
							break;
	}
#else
	sc1445x_uart_driver.cons=&sc1445x_uart_cons;
#endif

	//Reinitialize the console ports.
	//the early_printk may not be called
/* UART RX/TX pins are already configured by boot-loader */
#if !defined(CONFIG_SC14452_REEF_BS_BOARD)
	#if defined( CONFIG_G_MERKUR_BOARDS )
		P2_08_MODE_REG
	#elif (defined(CONFIG_L_V2_BOARD_CNX)|| defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX))
		P2_10_MODE_REG
	#else
		P0_00_MODE_REG
	#endif
		= GPIO_PID_UTX | GPIO_PUPD_OUT_NONE;

	#if defined( CONFIG_G_MERKUR_BOARDS )
		P2_09_MODE_REG
	#elif (defined(CONFIG_L_V2_BOARD_CNX)|| defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX))
		P2_09_MODE_REG
	#else
		P0_01_MODE_REG
	#endif
	#if (defined(CONFIG_L_V2_BOARD) || defined(CONFIG_L_V1_BOARD)|| defined (CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD) ||defined(CONFIG_L_V3_BOARD)\
		|| defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)|| defined (CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX))
		= GPIO_PID_URX | GPIO_PUPD_IN_PULLDOWN;
	#else
		= GPIO_PID_URX | GPIO_PUPD_IN_PULLUP;
	#endif
#endif /* #if !defined(CONFIG_SC14452_REEF_BS_BOARD) */

	sc1445x_uart_cons_init (SC1445x_UART_CONSOLE_CHANNEL);
	return 0;
}
console_initcall(sc1445x_uart_console_init);

#define SC1445x_CONSOLE &sc1445x_uart_cons

#else /* !CONFIG_SC1445x_CONSOLE */
#define SC1445x_CONSOLE 0
#endif /* CONFIG_SC1445x_CONSOLE */

/* TX/RX interrupt handlers.  */

static void sc1445x_uart_stop_tx (struct uart_port *port);

void sc1445x_uart_tx (struct uart_port *port)
{
	struct circ_buf *xmit = &port->info->xmit;
	int tx_ch;
//vm

	uart_arbit_flag = 0;
	uart_tx_int_flag = 0;
{
	if (port->x_char) {
		tx_ch = port->x_char;
		port->x_char = 0;
	} else 
	if (!uart_circ_empty (xmit)) {
		tx_ch = xmit->buf[xmit->tail];
		xmit->tail = (xmit->tail + 1) & (UART_XMIT_SIZE - 1);
	} else
		goto no_xmit;
	
	sc1445x_uart_putc (tx_ch);
	UART_CLEAR_TX_INT_REG = 1;
//vm	
	UART_CTRL_REG |= UART_CTRL_TEN;
	
	uart_tx_int_flag = 1;

	sc1445x_irq_clear_pending (UART_TI_INT);


	port->icount.tx++;

	return;

 no_xmit:
	if (uart_circ_empty (xmit)){
		uart_write_wakeup (port);
		sc1445x_uart_stop_tx (port);
//vm
		UART_CTRL_REG &= ~ UART_CTRL_TEN;
	}
	sc1445x_irq_clear_pending (UART_TI_INT);
		
}
}

static irqreturn_t sc1445x_uart_tx_irq(int irq, void *data)
{
	struct uart_port *port = data;
	sc1445x_uart_tx (port);
	return IRQ_HANDLED;
}
//todo
#ifdef SOFTSTOPRXSUPPORT
static inline int smart_tty_insert_flip_char (struct tty_struct *tty, unsigned char ch, char ch_stat)
{
	int return_val=1;
#ifdef RTSCTSDSRSUPPORT
	if (((stop_sc14452_uart_rx==0)&&(sc14452_uart_status_flag!=0))||(sc14452_console_mode==0))
#else
	if (((stop_sc14452_uart_rx==0)&&(sc14452_uart_status_flag!=0)))
#endif
	//if ((stop_sc14452_uart_rx==0)||(sc14452_console_mode==0))
	 	{
	 		sc14452_enable_uart_rx_buf_flag=1;
	 		return_val=(tty_insert_flip_char (tty, ch, ch_stat));

	 	}
#if 1
#ifdef RTSCTSDSRSUPPORT
	 	if ((sc14452_console_mode)&& (ch!=0xFF)&&(ch!=0))
#else
		if ((ch!=0xFF)&&(ch!=0))
#endif
	 		{
	 			//to conserve on CPU time
	 			STOP_TIMER(sc14452_uart_timer);
	 			sc14452_uart_filter_value=0;
	 			START_TIMER(sc14452_uart_timer, sc14452_uart_timer_period_pattern[sc14452_uart_filter_value]);
	 		}
#endif
#ifdef RTSCTSDSRSUPPORT
	 	if (sc14452_console_mode==1)
#endif
	 	{
	 		switch (ch)
	 		{
	 		case 0xa:
	 		case 0xd:
				 if (sc14452_enable_uart_rx_pin_indx==4)
	 			 {
					 if (memcmp((void*)sc14452_enable_uart_rx_pin_buf,(void*)sc14452_enable_uart_rx_pin_code,4)==0)
	 			 	{
	 			 		stop_sc14452_uart_rx=(stop_sc14452_uart_rx+1)&0x1;
	 			 	}
	 			 }
	 			sc14452_enable_uart_rx_pin_indx=0;
	 			break;
	 		default:
	 			if (sc14452_enable_uart_rx_pin_indx<4)
	 			{
	 				sc14452_enable_uart_rx_pin_buf[sc14452_enable_uart_rx_pin_indx]=ch;
	 			}
	 			sc14452_enable_uart_rx_pin_indx++;
	 		}
	 	}
	 		return(return_val);
}

static inline void smart_tty_flip_buffer_push (struct tty_struct *tty )
{
	if (sc14452_enable_uart_rx_buf_flag)
		tty_flip_buffer_push(tty);
	sc14452_enable_uart_rx_buf_flag=0;
}
#endif
static irqreturn_t sc1445x_uart_rx_irq(int irq, void *data)
{
#if !defined( USE_RX_DMA )
	unsigned char ch_stat = TTY_NORMAL;
	unsigned char ch = sc1445x_uart_getc;
	unsigned short err = sc1445x_uart_err;
#endif
	struct uart_port *port = data;
	unsigned long flags ;

	spin_lock_irqsave( &port->lock, flags ) ;

#if defined( USE_RX_DMA )
	/* DMA2_IDX_REG shows how many bytes have been DMA'ed */
	tasklet_schedule( &sc1445x_uart_tasklet ) ;
#if defined( CONFIG_SC14452 )
	DMA2_INT_REG = DMA2_IDX_REG + 1 ;
#else
	DMA2_INT_REG = DMA2_IDX_REG ;
#endif
#else
	if (err) { 
		if (err & SC1445x_UART_ERR_PARITY) { 
			ch_stat = TTY_PARITY; 
			port->icount.parity++; 
		} 
	} 

 	port->icount.rx++; 

#ifndef SOFTSTOPRXSUPPORT
 		tty_insert_flip_char (port->info->tty, ch, ch_stat);
 		tty_flip_buffer_push(port->info->tty);
#else
 		smart_tty_insert_flip_char (port->info->tty, ch, ch_stat);
 		smart_tty_flip_buffer_push(port->info->tty);
#endif
	UART_CLEAR_RX_INT_REG = 1;
#endif

	sc1445x_irq_clear_pending (UART_RI_INT);

	spin_unlock_irqrestore( &port->lock, flags ) ;

	return IRQ_HANDLED;
}

#if defined( USE_RX_DMA )
static void handle_rx( unsigned long data )
{
	struct uart_port* port = (void*)data ;
	unsigned char ch_stat = TTY_NORMAL ;
	unsigned char ch /*= sc1445x_uart_getc*/ ;
	unsigned short err = sc1445x_uart_err ;
	unsigned short i, end, t_end, wrap ;
	unsigned short len ;
	unsigned long flags ;

	spin_lock_irqsave( &port->lock, flags ) ;

	if( err ) { 
		if( err & SC1445x_UART_ERR_PARITY ) { 
			ch_stat = TTY_PARITY ; 
			port->icount.parity++ ; 
		} 
	} 

	i = dma_read_pos ;
	t_end = end = DMA2_IDX_REG ;
	if( i > end ) {
		wrap = 1 ;
#if defined( CONFIG_SC14450 )
		end = DMA2_LEN_REG + 1 ;
#else
		end = DMA2_LEN_REG ;
#endif
		len = end - i + t_end ;
	} else {
		wrap = 0 ;
		len = end - i ;
	}
	if( !len ) {
		spin_unlock_irqrestore( &port->lock, flags ) ;

		return ;
	}

	if( err ) {
		++port->icount.rx ;
		ch = sc1445x_uart_getc ;
	#ifndef SOFTSTOPRXSUPPORT
		tty_insert_flip_char( port->info->tty, ch, ch_stat ) ;
	#else
		smart_tty_insert_flip_char( port->info->tty, ch, ch_stat ) ;
	#endif
	} else {
		for( ;  i < end ;  ++i ) {
			++port->icount.rx ;
			ch = uart_rx_buffer[i] ;
#ifndef SOFTSTOPRXSUPPORT
			tty_insert_flip_char( port->info->tty,
							ch, ch_stat ) ;
#else
			smart_tty_insert_flip_char( port->info->tty,
										ch, ch_stat ) ;
#endif
		}
		if( wrap ) {
			for( i = 0 ;  i < t_end ;  ++i ) {
				++port->icount.rx ;
				ch = uart_rx_buffer[i] ;
#ifndef SOFTSTOPRXSUPPORT
				tty_insert_flip_char( port->info->tty,
							ch, ch_stat ) ;
#else
				smart_tty_insert_flip_char( port->info->tty,
											ch, ch_stat ) ;
#endif
			}
		}
	}
#ifndef SOFTSTOPRXSUPPORT
	tty_flip_buffer_push( port->info->tty ) ;
#else
	smart_tty_flip_buffer_push( port->info->tty ) ;
#endif

#if defined( CONFIG_SC14450 )
	if( DMA2_LEN_REG + 1  ==  i )
#else
	if( DMA2_LEN_REG == i )
#endif
		i = 0 ;
	dma_read_pos = i ;

	spin_unlock_irqrestore( &port->lock, flags ) ;
}
#endif

/* Control functions for the serial framework.  */

static void sc1445x_uart_nop (struct uart_port *port) { }
static int sc1445x_uart_success (struct uart_port *port) { return 0; }

static unsigned sc1445x_uart_tx_empty (struct uart_port *port)
{
	return TIOCSER_TEMT;	/* Can't detect.  */
}

static void sc1445x_uart_set_mctrl (struct uart_port *port, unsigned mctrl)
{
#ifdef RTSCTSDSRSUPPORT
	if ((mctrl&TIOCM_RTS)!=0)
				RTSSET;
			else
				RTSRESET;
#endif
}

static unsigned sc1445x_uart_get_mctrl (struct uart_port *port)
{
#ifdef RTSCTSDSRSUPPORT
	//return (DSSPORT? TIOCM_DSR :0)| TIOCM_CAR | (CTSPORT? TIOCM_CTS :0);
	sc14452_DSS_TYPE=detect_DSS_type();
	return ((sc14452_DSS_TYPE==0)? TIOCM_DSR :0)| TIOCM_CAR | (CTSPORT? TIOCM_CTS :0);
#else
	return TIOCM_CAR | TIOCM_DSR | TIOCM_CTS;
#endif
}

void msleep(unsigned int msecs);

static void sc1445x_uart_start_tx (struct uart_port *port)
{

	//struct circ_buf *xmit = &port->info->xmit;
	//int tx_ch;
	int flags;

	local_irq_save (flags);
	uart_arbit_flag = 1;
//vm
	if (!(UART_CTRL_REG & UART_CTRL_TEN))
		SET_INT_PENDING_REG = 0x20;

	local_irq_restore (flags);

}

static void sc1445x_uart_stop_tx (struct uart_port *port)
{
}

static void sc1445x_uart_stop_rx (struct uart_port *port)
{
}

static void sc1445x_uart_break_ctl (struct uart_port *port, int break_ctl)
{
	unsigned long flags;

	spin_lock_irqsave( &port->lock, flags ) ;
	if (break_ctl == -1)
	{
#if defined( CONFIG_G_MERKUR_BOARDS )
		P2_08_MODE_REG = 0x300;
		P2_RESET_DATA_REG = 0x100;
#else
		P0_00_MODE_REG = 0x300;
		P0_RESET_DATA_REG = 1;
#endif
	}
	else
	{
#if defined( CONFIG_G_MERKUR_BOARDS )
		P2_08_MODE_REG = 0x30B;
#else
		P0_00_MODE_REG = 0x30B;
#endif
	}
	spin_unlock_irqrestore( &port->lock, flags ) ;
}

static int sc1445x_uart_startup (struct uart_port *port)
{
	int err;

	/* Alloc RX irq.  */
	UART_CLEAR_RX_INT_REG = 1;
	sc1445x_irq_clear_pending (UART_RI_INT);
	err = request_irq (UART_RI_INT, sc1445x_uart_rx_irq,
			   IRQF_DISABLED, "sc1445x_uart", port);
	if (err)
		return err;

	/* Alloc TX irq.  */
	UART_CLEAR_TX_INT_REG = 1;
	sc1445x_irq_clear_pending (UART_TI_INT);
	err = request_irq (UART_TI_INT, sc1445x_uart_tx_irq,
			   IRQF_DISABLED, "sc1445x_uart", port);
	if (err) {
		free_irq (UART_RI_INT, port);
		return err;
	}
//rts-cts
#ifdef RTSCTSDSRSUPPORT

			DSSMODE_REG=0x000;
			RTSMODE_REG = 0x300;
			RTSRESET;
			CTSMODE_REG = 0x200;

#endif
#if defined( USE_RX_DMA )
	/* DMA source is UART_RX_TX_REG (0xFF4902) */
	DMA2_A_STARTH_REG = 0xFF ;
	DMA2_A_STARTL_REG = 0x4902 ;
	/* DMA destination is our RX buffer */
	DMA2_B_STARTH_REG = ( (unsigned long)uart_rx_buffer ) >> 16 ;
	DMA2_B_STARTL_REG = (unsigned long)uart_rx_buffer ;
	DMA2_LEN_REG = UART_RX_BUFFER_SIZE - 1 ;
	DMA2_INT_REG = 0 ;

	dma_read_pos = 0 ;
	tasklet_init( &sc1445x_uart_tasklet, handle_rx, (unsigned long)port ) ;

	DMA2_CTRL_REG = 0x2b1 ;
#endif

	return 0;
}

static void sc1445x_uart_shutdown (struct uart_port *port)
{
#if defined( USE_RX_DMA )
#if 0
	printk( "DMA2_INT_REG=%04X\n", DMA2_INT_REG ) ;
	printk( "DMA2_IDX_REG=%04X\n", DMA2_IDX_REG ) ;
	printk( "RX bytes: %u (%08x)\n", port->icount.rx, port->icount.rx ) ;
	port->icount.rx = 0 ;
#endif
	DMA2_CTRL_REG = 0 ;
#endif

	/* Disable port interrupts.  */
	free_irq (UART_TI_INT, port);
	free_irq (UART_RI_INT, port);

	/* Turn off xmit/recv enable bits.  */
	UART_CTRL_REG &= ~(UART_CTRL_TEN | UART_CTRL_REN);
	/* Then reset the channel.  */
	UART_CTRL_REG= 0;
}

static void
sc1445x_uart_set_termios (struct uart_port *port, struct termios *termios,
		        struct termios *old)
{
	unsigned cflags = termios->c_cflag;

	/* Restrict flags to legal values.  */
	if ((cflags & CSIZE) != CS7 && (cflags & CSIZE) != CS8)
		/* The new value of CSIZE is invalid, use the old value.  */
		cflags = (cflags & ~CSIZE)
			| (old ? (old->c_cflag & CSIZE) : CS8);

	termios->c_cflag = cflags;

#if 0
	//sc1445x_uart_configure (port->line, cflags,115200);
	sc1445x_uart_configure (port->line, cflags,38400;
	//sc1445x_uart_configure (port->line, cflags, termios->c_ispeed);
#else /* 2010.08.30 by jkkim */
	if((cflags & B38400) == B38400 ) {
		sc1445x_uart_configure (port->line, cflags,38400);
	} else if((cflags & B57600) == B57600 ) {
		sc1445x_uart_configure (port->line, cflags,57600);
	} else {
		sc1445x_uart_configure (port->line, cflags,115200);
	}
#endif

}

static const char *sc1445x_uart_type (struct uart_port *port)
{
	
#if (defined(CONFIG_L_V2_BOARD) || defined (CONFIG_L_V4_BOARD)|| defined(CONFIG_L_V5_BOARD)\
		|| defined(CONFIG_L_V2_BOARD_CNX)|| defined(CONFIG_L_V4_BOARD_CNX)|| defined(CONFIG_L_V5_BOARD_CNX))&& (defined(RTSCTSDSRSUPPORT))

	sc14452_DSS_TYPE=detect_DSS_type();

	switch (sc14452_DSS_TYPE)

	{
		case SYSTEM_DSS_CONNECTED:
							return port->type == PORT_SC1445x_UART ? "sc1445x_uart_SDSS" : 0;
		case CARRIER_DSS_CONNECTED:
							return port->type == PORT_SC1445x_UART ? "sc1445x_uart_CDSS" : 0;
		case NO_DSS_MODULE_CONNECTED:
		default:
							return port->type == PORT_SC1445x_UART ? "sc1445x_uart" : 0;
							break;
	}


#else
	return port->type == PORT_SC1445x_UART ? "sc1445x_uart" : 0;
#endif
}

static void sc1445x_uart_config_port (struct uart_port *port, int flags)
{
	if (flags & UART_CONFIG_TYPE)
		port->type = PORT_SC1445x_UART;
}

static int
sc1445x_uart_verify_port (struct uart_port *port, struct serial_struct *ser)
{
	if (ser->type != PORT_UNKNOWN && ser->type != PORT_SC1445x_UART)
		return -EINVAL;
	if (ser->irq != UART_TI_INT)
		return -EINVAL;
	return 0;
}

static int sc1445x_uart_ioctl( struct uart_port* uport, unsigned int cmd,
							unsigned long arg )
{
	int res = -ENOIOCTLCMD ;

	switch( cmd ) {
#if defined( CONFIG_SC1445x_CONSOLE )
		case TSC1445x_START_CONSOLE:
			console_start( &sc1445x_uart_cons ) ;
			printk( "%s: START\n", __FUNCTION__ ) ;
			res = 0 ;
			break ;

		case TSC1445x_STOP_CONSOLE:
			//console_stop( &sc1445x_uart_cons ) ;
		  // By ED......
		  printk( "Unregistering console @%p...\n", &sc1445x_uart_cons ) ;
		  res = unregister_console( &sc1445x_uart_cons ) ;
		  printk( "...%s\n", res ?  "FAILED" :  "OK" ) ;
		  //.............................
			//printk( "%s: STOP\n", __FUNCTION__ ) ;
			res = 0 ;
			break ;
#endif

		default:
			break ;
	}

	return res ;
}

static struct uart_ops sc1445x_uart_ops = {
	.tx_empty	= sc1445x_uart_tx_empty,
	.get_mctrl	= sc1445x_uart_get_mctrl,
	.set_mctrl	= sc1445x_uart_set_mctrl,
	.start_tx	= sc1445x_uart_start_tx,
	.stop_tx	= sc1445x_uart_stop_tx,
	.stop_rx	= sc1445x_uart_stop_rx,
	.enable_ms	= sc1445x_uart_nop,
	.break_ctl	= sc1445x_uart_break_ctl,
	.startup	= sc1445x_uart_startup,
	.shutdown	= sc1445x_uart_shutdown,
	.set_termios	= sc1445x_uart_set_termios,
	.type		= sc1445x_uart_type,
	.release_port	= sc1445x_uart_nop,
	.request_port	= sc1445x_uart_success,
	.config_port	= sc1445x_uart_config_port,
	.verify_port	= sc1445x_uart_verify_port,
	.ioctl		= sc1445x_uart_ioctl,
};

/* Initialization and cleanup.  */

static struct uart_driver sc1445x_uart_driver = {
	.owner			= THIS_MODULE,
	.driver_name		= "sc1445x_uart",
	.dev_name		= "ttyS",
	.major			= TTY_MAJOR,
	.minor			= SC1445x_UART_MINOR_BASE,
	.nr			= SC1445x_UART_NUM_CHANNELS,
#ifndef RTSCTSDSRSUPPORT
	.cons			= SC1445x_CONSOLE,
#else
	.cons			= NULL,
#endif
};


static struct uart_port sc1445x_uart_ports[SC1445x_UART_NUM_CHANNELS];

static int __init sc1445x_uart_init (void)
{
	int rval;

	printk (KERN_INFO "%s on-chip UART\n", SC1445x_UART_CHIP_NAME);

	rval = uart_register_driver (&sc1445x_uart_driver);
	if (rval == 0) {
		unsigned chan;

		for (chan = 0; chan < SC1445x_UART_NUM_CHANNELS; chan++) {
			struct uart_port *port = &sc1445x_uart_ports[chan];
			
			memset (port, 0, sizeof *port);

			spin_lock_init(&port->lock); //pag

			port->ops = &sc1445x_uart_ops;
			port->line = chan;
			port->iotype = UPIO_MEM;
			port->flags = UPF_BOOT_AUTOCONF;

			/* We actually use multiple IRQs, but the serial
			   framework seems to mainly use this for
			   informational purposes anyway.  Here we use the TX
			   irq.  */
			port->irq = UART_TI_INT;

			/* The serial framework doesn't really use these
			   membase/mapbase fields for anything useful, but
			   it requires that they be something non-zero to
			   consider the port `valid', and also uses them
			   for informational purposes.  */
			port->membase = (void*)&UART_CTRL_REG;
			port->mapbase = UART_CTRL_REG;

			/* The framework insists on knowing the uart's master
			   clock freq, though it doesn't seem to do anything
			   useful for us with it.  We must make it at least
			   higher than (the maximum baud rate * 16), otherwise
			   the framework will puke during its internal
			   calculations, and force the baud rate to be 9600.
			   To be accurate though, just repeat the calculation
			   we use when actually setting the speed.  */
			port->uartclk = 115200 *16;

			uart_add_one_port (&sc1445x_uart_driver, port);

			

		}
	}
	#ifdef RXDISABLENOCABLE

init_timer(&sc14452_uart_timer);
sc14452_uart_timer.data = 0;
sc14452_uart_timer.function = sc14452_uart_timer_fn;
sc14452_uart_status_flag=0;
sc14452_uart_filter_value=0;

#ifdef RXDISABLENOCABLE
#ifdef RTSCTSDSRSUPPORT
if (sc14452_console_mode)
#endif
{
	START_TIMER(sc14452_uart_timer, sc14452_uart_timer_period_pattern[sc14452_uart_filter_value]);
}
#endif
#endif
	return rval;
}

static void __exit sc1445x_uart_exit (void)
{
	unsigned chan;
	for (chan = 0; chan < SC1445x_UART_NUM_CHANNELS; chan++)
		uart_remove_one_port (&sc1445x_uart_driver,
				      &sc1445x_uart_ports[chan]);

	uart_unregister_driver (&sc1445x_uart_driver);
	#ifdef RXDISABLENOCABLE
	STOP_TIMER(sc14452_uart_timer);
#endif
}

module_init (sc1445x_uart_init);
module_exit (sc1445x_uart_exit);

MODULE_AUTHOR ("Jonathan Mikhailovich Short (modifications by V. Maniotis, A. Iordanidis)");
MODULE_DESCRIPTION ("SITEL " SC1445x_UART_CHIP_NAME " on-chip UART");
MODULE_LICENSE ("GPL");
