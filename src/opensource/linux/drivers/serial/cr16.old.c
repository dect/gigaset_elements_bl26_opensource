// File created by Gigaset Elements GmbH
// All rights reserved.
/*
 *  linux/drivers/serial/cr16.c
 *
 *  Based on drivers/serial/8250.c by Russell King.
 *
 *  Author:	Nicolas Pitre
 *  Created:	Feb 20, 2003
 *  Copyright:	(C) 2003 Monta Vista Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Note 1: This driver is made separate from the already too overloaded
 * 8250.c because it needs some kirks of its own and that'll make it
 * easier to add DMA support.
 *
 * Note 2: I'm too sick of device allocation policies for serial ports.
 * If someone else wants to request an "official" allocation of major/minor
 * for this driver please be my guest.  And don't forget that new hardware
 * to come from Intel might have more than 3 or 4 of those UARTs.  Let's
 * hope for a better port registration and dynamic device allocation scheme
 * with the serial core maintainer satisfaction to appear soon.
 */


#if defined(CONFIG_CR16_CONSOLE) && defined(CONFIG_MAGIC_SYSRQ)
#define SUPPORT_SYSRQ
#endif

#include <linux/module.h>
#include <linux/ioport.h>
#include <linux/init.h>
#include <linux/console.h>
#include <linux/sysrq.h>
#include <linux/serial_reg.h>
#include <linux/circ_buf.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/tty.h>
#include <linux/tty_flip.h>
#include <linux/serial_core.h>

#include <asm/io.h>
#include <asm/irq.h>
#include <asm/regs_sc14450.h>


static void serial_cr16_stop_tx(struct uart_port *port)
{
	printk("-----serial_cr16_stop_tx\n");
//	struct uart_cr16_port *up = (struct uart_cr16_port *)port;

//	if (up->ier & UART_IER_THRI) {
//		up->ier &= ~UART_IER_THRI;
//		serial_out(up, UART_IER, up->ier);
//	}
}

static void serial_cr16_stop_rx(struct uart_port *port)
{
	printk("-----serial_cr16_stop_rx\n");
//	struct uart_cr16_port *up = (struct uart_cr16_port *)port;

//	up->ier &= ~UART_IER_RLSI;
//	up->port.read_status_mask &= ~UART_LSR_DR;
//	serial_out(up, UART_IER, up->ier);
}



static void serial_cr16_start_tx(struct uart_port *port)
{
	printk("-----serial_cr16_start_tx\n");
//	serial_out(port, UART_IER, port->ier);
#if 0
	struct uart_cr16_port *up = (struct uart_cr16_port *)port;

	if (!(up->ier & UART_IER_THRI)) {
		up->ier |= UART_IER_THRI;
		serial_out(port, UART_IER, up->ier);
	}
#endif
}


/*
 * This handles the interrupt from one port.
 */
static inline irqreturn_t serial_cr16_irq(int irq, void *dev_id)
{
	printk("-----serial_cr16_irq\n");

//	struct uart_cr16_port *up = dev_id;
	unsigned int iir, lsr;
#if 0
	iir = serial_in(up, UART_IIR);
	if (iir & UART_IIR_NO_INT)
		return IRQ_NONE;
	lsr = serial_in(up, UART_LSR);
	if (lsr & UART_LSR_DR)
		receive_chars(up, &lsr);
	check_modem_status(up);
	if (lsr & UART_LSR_THRE)
		transmit_chars(up);
#endif
	return IRQ_HANDLED;
}

static unsigned int serial_cr16_tx_empty(struct uart_port *port)
{
	printk("-----serial_cr16_tx_empty\n");
//	struct uart_cr16_port *up = (struct uart_cr16_port *)port;
	unsigned long flags;
	unsigned int ret;
#if 0
	spin_lock_irqsave(&up->port.lock, flags);
	ret = serial_in(up, UART_LSR) & UART_LSR_TEMT ? TIOCSER_TEMT : 0;
	spin_unlock_irqrestore(&up->port.lock, flags);
#endif
	return ret;
}

static unsigned int serial_cr16_get_mctrl(struct uart_port *port)
{
	printk("-----serial_cr16_get_mctrl\n");
//	struct uart_cr16_port *up = (struct uart_cr16_port *)port;
	unsigned char status;
	unsigned int ret;
#if 0
	status = serial_in(up, UART_MSR);

	ret = 0;
	if (status & UART_MSR_DCD)
		ret |= TIOCM_CAR;
	if (status & UART_MSR_RI)
		ret |= TIOCM_RNG;
	if (status & UART_MSR_DSR)
		ret |= TIOCM_DSR;
	if (status & UART_MSR_CTS)
		ret |= TIOCM_CTS;
#endif
	return ret;
}

static void serial_cr16_set_mctrl(struct uart_port *port, unsigned int mctrl)
{
	printk("-----serial_cr16_set_mctrl\n");
//	struct uart_cr16_port *up = (struct uart_cr16_port *)port;
	unsigned char mcr = 0;
#if 0
	if (mctrl & TIOCM_RTS)
		mcr |= UART_MCR_RTS;
	if (mctrl & TIOCM_DTR)
		mcr |= UART_MCR_DTR;
	if (mctrl & TIOCM_OUT1)
		mcr |= UART_MCR_OUT1;
	if (mctrl & TIOCM_OUT2)
		mcr |= UART_MCR_OUT2;
	if (mctrl & TIOCM_LOOP)
		mcr |= UART_MCR_LOOP;

	mcr |= up->mcr;

	serial_out(up, UART_MCR, mcr);
#endif
}

static void serial_cr16_break_ctl(struct uart_port *port, int break_state)
{
	printk("-----serial_cr16_break_ctl\n");
//	struct uart_cr16_port *up = (struct uart_cr16_port *)port;
	unsigned long flags;
#if 0
	spin_lock_irqsave(&up->port.lock, flags);
	if (break_state == -1)
		up->lcr |= UART_LCR_SBC;
	else
		up->lcr &= ~UART_LCR_SBC;
	serial_out(up, UART_LCR, up->lcr);
	spin_unlock_irqrestore(&up->port.lock, flags);
#endif
}


static int serial_cr16_startup(struct uart_port *port)
{
	printk("-----serial_cr16_startup\n");
//	struct uart_cr16_port *up = (struct uart_cr16_port *)port;
	unsigned long flags;
	int retval;
#if 0
	if (port->line == 3) /* HWUART */
		up->mcr |= UART_MCR_AFE;
	else
		up->mcr = 0;

	/*
	 * Allocate the IRQ
	 */
	retval = request_irq(up->port.irq, serial_cr16_irq, 0, up->name, up);
	if (retval)
		return retval;

	/*
	 * Clear the FIFO buffers and disable them.
	 * (they will be reenabled in set_termios())
	 */
	serial_out(up, UART_FCR, UART_FCR_ENABLE_FIFO);
	serial_out(up, UART_FCR, UART_FCR_ENABLE_FIFO |
			UART_FCR_CLEAR_RCVR | UART_FCR_CLEAR_XMIT);
	serial_out(up, UART_FCR, 0);

	/*
	 * Clear the interrupt registers.
	 */
	(void) serial_in(up, UART_LSR);
	(void) serial_in(up, UART_RX);
	(void) serial_in(up, UART_IIR);
	(void) serial_in(up, UART_MSR);

	/*
	 * Now, initialize the UART
	 */
	serial_out(up, UART_LCR, UART_LCR_WLEN8);

	spin_lock_irqsave(&up->port.lock, flags);
	up->port.mctrl |= TIOCM_OUT2;
	serial_cr16_set_mctrl(&up->port, up->port.mctrl);
	spin_unlock_irqrestore(&up->port.lock, flags);

	/*
	 * Finally, enable interrupts.  Note: Modem status interrupts
	 * are set via set_termios(), which will be occurring imminently
	 * anyway, so we don't enable them here.
	 */
	up->ier = UART_IER_RLSI | UART_IER_RDI | UART_IER_RTOIE | UART_IER_UUE;
	serial_out(up, UART_IER, up->ier);

	/*
	 * And clear the interrupt registers again for luck.
	 */
	(void) serial_in(up, UART_LSR);
	(void) serial_in(up, UART_RX);
	(void) serial_in(up, UART_IIR);
	(void) serial_in(up, UART_MSR);
#endif
	return 0;
}

static void serial_cr16_shutdown(struct uart_port *port)
{
	printk("-----serial_cr16_shutdown\n");
//	struct uart_cr16_port *up = (struct uart_cr16_port *)port;
	unsigned long flags;
#if 0
	free_irq(up->port.irq, up);

	/*
	 * Disable interrupts from this port
	 */
	up->ier = 0;
	serial_out(up, UART_IER, 0);

	spin_lock_irqsave(&up->port.lock, flags);
	up->port.mctrl &= ~TIOCM_OUT2;
	serial_cr16_set_mctrl(&up->port, up->port.mctrl);
	spin_unlock_irqrestore(&up->port.lock, flags);

	/*
	 * Disable break condition and FIFOs
	 */
	serial_out(up, UART_LCR, serial_in(up, UART_LCR) & ~UART_LCR_SBC);
	serial_out(up, UART_FCR, UART_FCR_ENABLE_FIFO |
				  UART_FCR_CLEAR_RCVR |
				  UART_FCR_CLEAR_XMIT);
	serial_out(up, UART_FCR, 0);
#endif
}

static void
serial_cr16_set_termios(struct uart_port *port, struct termios *termios,
		       struct termios *old)
{
	printk("-----serial_cr16_set_termios\n");
//	struct uart_cr16_port *up = (struct uart_cr16_port *)port;
	unsigned char cval, fcr = 0;
	unsigned long flags;
	unsigned int baud, quot;

	switch (termios->c_cflag & CSIZE) {
	case CS5:
		cval = UART_LCR_WLEN5;
		break;
	case CS6:
		cval = UART_LCR_WLEN6;
		break;
	case CS7:
		cval = UART_LCR_WLEN7;
		break;
	default:
	case CS8:
		cval = UART_LCR_WLEN8;
		break;
	}

	if (termios->c_cflag & CSTOPB)
		cval |= UART_LCR_STOP;
	if (termios->c_cflag & PARENB)
		cval |= UART_LCR_PARITY;
	if (!(termios->c_cflag & PARODD))
		cval |= UART_LCR_EPAR;
#if 0
	/*
	 * Ask the core to calculate the divisor for us.
	 */
	baud = uart_get_baud_rate(port, termios, old, 0, port->uartclk/16);
	quot = uart_get_divisor(port, baud);

	if ((up->port.uartclk / quot) < (2400 * 16))
		fcr = UART_FCR_ENABLE_FIFO | UART_FCR_CR16R1;
	else if ((up->port.uartclk / quot) < (230400 * 16))
		fcr = UART_FCR_ENABLE_FIFO | UART_FCR_CR16R8;
	else
		fcr = UART_FCR_ENABLE_FIFO | UART_FCR_CR16R32;

	/*
	 * Ok, we're now changing the port state.  Do it with
	 * interrupts disabled.
	 */
	spin_lock_irqsave(&up->port.lock, flags);

	/*
	 * Ensure the port will be enabled.
	 * This is required especially for serial console.
	 */
	up->ier |= IER_UUE;

	/*
	 * Update the per-port timeout.
	 */
	uart_update_timeout(port, termios->c_cflag, baud);

	up->port.read_status_mask = UART_LSR_OE | UART_LSR_THRE | UART_LSR_DR;
	if (termios->c_iflag & INPCK)
		up->port.read_status_mask |= UART_LSR_FE | UART_LSR_PE;
	if (termios->c_iflag & (BRKINT | PARMRK))
		up->port.read_status_mask |= UART_LSR_BI;

	/*
	 * Characters to ignore
	 */
	up->port.ignore_status_mask = 0;
	if (termios->c_iflag & IGNPAR)
		up->port.ignore_status_mask |= UART_LSR_PE | UART_LSR_FE;
	if (termios->c_iflag & IGNBRK) {
		up->port.ignore_status_mask |= UART_LSR_BI;
		/*
		 * If we're ignoring parity and break indicators,
		 * ignore overruns too (for real raw support).
		 */
		if (termios->c_iflag & IGNPAR)
			up->port.ignore_status_mask |= UART_LSR_OE;
	}

	/*
	 * ignore all characters if CREAD is not set
	 */
	if ((termios->c_cflag & CREAD) == 0)
		up->port.ignore_status_mask |= UART_LSR_DR;

	/*
	 * CTS flow control flag and modem status interrupts
	 */
	up->ier &= ~UART_IER_MSI;
	if (UART_ENABLE_MS(&up->port, termios->c_cflag))
		up->ier |= UART_IER_MSI;

	serial_out(up, UART_IER, up->ier);

	serial_out(up, UART_LCR, cval | UART_LCR_DLAB);/* set DLAB */
	serial_out(up, UART_DLL, quot & 0xff);		/* LS of divisor */
	serial_out(up, UART_DLM, quot >> 8);		/* MS of divisor */
	serial_out(up, UART_LCR, cval);		/* reset DLAB */
	up->lcr = cval;					/* Save LCR */
	serial_cr16_set_mctrl(&up->port, up->port.mctrl);
	serial_out(up, UART_FCR, fcr);
	spin_unlock_irqrestore(&up->port.lock, flags);
#endif
}

static void
serial_cr16_pm(struct uart_port *port, unsigned int state,
	      unsigned int oldstate)
{
	printk("-----serial_cr16_pm\n");
//	struct uart_cr16_port *up = (struct uart_cr16_port *)port;
//	cr16_set_cken(up->cken, !state);
//	if (!state)
//		udelay(1);
}

static void serial_cr16_release_port(struct uart_port *port)
{
	printk("-----serial_cr16_release_port\n");
}

static int serial_cr16_request_port(struct uart_port *port)
{
	printk("-----serial_cr16_request_port\n");
	return 0;
}

static void serial_cr16_config_port(struct uart_port *port, int flags)
{
	printk("-----serial_cr16_config_port\n");
//	struct uart_cr16_port *up = (struct uart_cr16_port *)port;
//	up->port.type = PORT_CR16;
}

static int
serial_cr16_verify_port(struct uart_port *port, struct serial_struct *ser)
{
	printk("-----serial_cr16_verify_port\n");
	/* we don't want the core code to modify any port params */
	return -EINVAL;
}

static const char *
serial_cr16_type(struct uart_port *port)
{
	printk("-----serial_cr16_type\n");
//	struct uart_cr16_port *up = (struct uart_cr16_port *)port;
	return 0;
//port->name;
}

#ifdef CONFIG_CR16_CONSOLE

static struct uart_port serial_cr16_port;
static struct uart_driver serial_cr16_reg;

#define BOTH_EMPTY (UART_LSR_TEMT | UART_LSR_THRE)

static void serial_cr16_console_putchar(struct uart_port *port, int ch)
{
	printk("-----serial_cr16_console_putchar\n");
#if 0
	struct uart_cr16_port *up = (struct uart_cr16_port *)port;

	wait_for_xmitr(up);
	serial_out(up, UART_TX, ch);
#endif
}


static void sitel_sercon_putc(int c)
{
	/* always transmit a CR before each NL */
	if (c == '\n')
		sitel_sercon_putc('\r');

	/* clear down interrupt condition (empty buffer) */
	while ( UART_CTRL_REG & UART_CTRL_TI ) {
		UART_CLEAR_TX_INT_REG = 0;
	}

	/* send character to TX register */
	UART_RX_TX_REG = c;

	/* wait until byte has been transmitted */
	while ( !(UART_CTRL_REG & UART_CTRL_TI) );
}

/*
 * Print a string to the serial port trying not to disturb
 * any possible real use of the port...
 *
 *	The console_lock must be held when we get here.
 */
static void
serial_cr16_console_write(struct console *co, const char *s, unsigned int count)
{

	while (count-- > 0)
		sitel_sercon_putc(*s++);


//	printk("-----serial_cr16_console_write\n");
#if 0
//	struct uart_cr16_port *up = &serial_cr16_ports[co->index];
	unsigned int ier;

	/*
	 *	First save the IER then disable the interrupts
	 */
	ier = serial_in(up, UART_IER);
	serial_out(up, UART_IER, UART_IER_UUE);

	uart_console_write(&up->port, s, count, serial_cr16_console_putchar);

	/*
	 *	Finally, wait for transmitter to become empty
	 *	and restore the IER
	 */
	wait_for_xmitr(up);
	serial_out(up, UART_IER, ier);
#endif
}

static int __init
serial_cr16_console_setup(struct console *co, char *options)
{
	printk("-----serial_cr16_console_setup\n");
	struct uart_port *port;
	int baud = 115200;
	int bits = 8;
	int parity = 'n';
	int flow = 'n';

	port = &serial_cr16_port;

	if (options)
		uart_parse_options(options, &baud, &parity, &bits, &flow);

	return uart_set_options(port, co, baud, parity, bits, flow);
}

static struct console serial_cr16_console = {
	.name		= "ttyS",
	.write		= serial_cr16_console_write,
	.device		= uart_console_device,
	.setup		= serial_cr16_console_setup,
	.flags		= CON_PRINTBUFFER,
	.index		= -1,
	.data		= &serial_cr16_reg,
};

static int __init
serial_cr16_console_init(void)
{
	printk("-----serial_cr16_console_init\n");
	uart_add_one_port(&serial_cr16_reg, &serial_cr16_port);
	register_console(&serial_cr16_console);
	return 0;
}

console_initcall(serial_cr16_console_init);

#define CR16_CONSOLE	&serial_cr16_console
#else
#define CR16_CONSOLE	NULL
#endif

struct uart_ops serial_cr16_pops = {
	.tx_empty	= serial_cr16_tx_empty,
	.set_mctrl	= serial_cr16_set_mctrl,
	.get_mctrl	= serial_cr16_get_mctrl,
//	.stop_tx	= serial_cr16_stop_tx,
//	.start_tx	= serial_cr16_start_tx,
//	.stop_rx	= serial_cr16_stop_rx,
//	.enable_ms	= serial_cr16_enable_ms,
	.break_ctl	= serial_cr16_break_ctl,
//	.startup	= serial_cr16_startup,
//	.shutdown	= serial_cr16_shutdown,
	.set_termios	= serial_cr16_set_termios,
//	.pm		= serial_cr16_pm,
	.type		= serial_cr16_type,
	.release_port	= serial_cr16_release_port,
	.request_port	= serial_cr16_request_port,
	.config_port	= serial_cr16_config_port,
	.verify_port	= serial_cr16_verify_port,
};

static struct uart_port serial_cr16_port = {
		.type		= PORT_SC14450,
		.iotype		= UPIO_MEM,
		.membase	= (void *)0xFF4900,
//		.mapbase	= __PREG(FFUART),
//		.irq		= IRQ_FFUART,
		.uartclk	= 921600 * 16,
		.fifosize	= 64,
		.ops		= &serial_cr16_pops,
		.line		= 0,
};

static struct uart_driver serial_cr16_reg = {
	.owner		= THIS_MODULE,
	.driver_name	= "CR16 serial",
	.dev_name	= "ttyS",
	.major		= TTY_MAJOR,
	.minor		= 64,
	.nr		= 1,
	.cons		= CR16_CONSOLE,
};

static int serial_cr16_suspend(struct platform_device *dev, pm_message_t state)
{
	printk("-----serial_cr16_suspend\n");
#if 0
        struct uart_cr16_port *sport = platform_get_drvdata(dev);

        if (sport)
                uart_suspend_port(&serial_cr16_reg, &sport->port);
#endif
        return 0;
}

static int serial_cr16_resume(struct platform_device *dev)
{
	printk("-----serial_cr16_resume\n");
#if 0
        struct uart_cr16_port *sport = platform_get_drvdata(dev);

        if (sport)
                uart_resume_port(&serial_cr16_reg, &sport->port);
#endif
        return 0;
}

static int serial_cr16_probe(struct platform_device *dev)
{
	printk("-----serial_cr16_probe\n");
#if 0
	serial_cr16_ports[dev->id].port.dev = &dev->dev;
	uart_add_one_port(&serial_cr16_reg, &serial_cr16_ports[dev->id].port);
	platform_set_drvdata(dev, &serial_cr16_ports[dev->id]);
#endif
	return 0;
}

static int serial_cr16_remove(struct platform_device *dev)
{
	printk("-----serial_cr16_remove\n");
#if 0
	struct uart_cr16_port *sport = platform_get_drvdata(dev);

	platform_set_drvdata(dev, NULL);

	if (sport)
		uart_remove_one_port(&serial_cr16_reg, &sport->port);
#endif
	return 0;
}

static struct platform_driver serial_cr16_driver = {
        .probe          = serial_cr16_probe,
        .remove         = serial_cr16_remove,

	.suspend	= serial_cr16_suspend,
	.resume		= serial_cr16_resume,
	.driver		= {
	        .name	= "cr162xx-uart",
	},
};

int __init serial_cr16_init(void)
{
	int ret;

	printk(KERN_INFO "Serial: CR16C+ serial driver $Revision: 0.1 $\n");

	ret = uart_register_driver(&serial_cr16_reg);
	if (ret != 0){
		printk("----fail 1\n");
		return ret;
	}
	ret = platform_driver_register(&serial_cr16_driver);
	if (ret != 0){
		printk("----fail 2\n");
		uart_unregister_driver(&serial_cr16_reg);
	}

	uart_add_one_port(&serial_cr16_reg, &serial_cr16_port);
	return ret;
}

void __exit serial_cr16_exit(void)
{
	printk("-----serial_cr16_exit\n");

	platform_driver_unregister(&serial_cr16_driver);
	uart_unregister_driver(&serial_cr16_reg);
}

module_init(serial_cr16_init);
module_exit(serial_cr16_exit);

MODULE_LICENSE("GPL");

