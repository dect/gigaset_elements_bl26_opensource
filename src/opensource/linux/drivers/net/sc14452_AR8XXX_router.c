// File created by Gigaset Elements GmbH
// All rights reserved.
/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 *
 * RHEA SOFTWARE PLATFORM
 * SC14452 EMAC LINUX DRIVER
 * File sc14452_AR8XXX_router.c
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ioport.h>
#include <linux/init.h>
#include <linux/dma-mapping.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <asm/io.h>
#include <asm/system.h>

#include "sc14452_AR8XXX_router.h"
#include "asm.h"

static int emac_tx_timeout(struct net_device *dev);
struct net_device * __init sc14452_emac_probe(int unit);
static int __init emac_init(struct net_device *dev, int unit);
static int emac_tx_timeout(struct net_device *dev);
static int emac_open(struct net_device *dev);
static int emac_close(struct net_device *dev);
static void emac_timer_handler(unsigned long data);
static irqreturn_t emac_interrupt(int irq, void *dev_id, struct pt_regs * regs);
static int sc14452_ioctl(struct net_device *dev, struct ifreq *ifr, int cmd);
static int emac_xmit(struct sk_buff *skb, struct net_device *dev);
static struct net_device_stats *emac_get_stats(struct net_device *dev);
static void emac_set_multicast_list(struct net_device *dev);
static void emac_rx_tasklet_fn(unsigned long data);

#define PORT_1  1
#define PORT_0  0

typedef struct _TOS_entry {
	unsigned char TOS_reg;
	unsigned char TOS_DiffServ;
	unsigned char TOS_reg_val;
}TOS_entry;

#define MAX_TOS_CLASS_VALUES	64

TOS_entry TOS_table[MAX_TOS_CLASS_VALUES] =
{
	{0x60, 0x00, 0x03},
	{0x60, 0x04, 0x0C},
	{0x60, 0x08, 0x30},
	{0x60, 0x0C, 0xC0},
	{0x61, 0x10, 0x03},
	{0x61, 0x14, 0x0C},
	{0x61, 0x18, 0x30},
	{0x61, 0x1C, 0xC0},
	{0x62, 0x20, 0x03},
	{0x62, 0x24, 0x0C},
	{0x62, 0x28, 0x30},
	{0x62, 0x2C, 0xC0},
	{0x63, 0x30, 0x03},
	{0x63, 0x34, 0x0C},
	{0x63, 0x38, 0x30},
	{0x63, 0x3C, 0xC0},
	{0x64, 0x40, 0x03},
	{0x64, 0x44, 0x0C},
	{0x64, 0x48, 0x30},
	{0x64, 0x4C, 0xC0},
	{0x65, 0x50, 0x03},
	{0x65, 0x54, 0x0C},
	{0x65, 0x58, 0x30},
	{0x65, 0x5C, 0xC0},
	{0x66, 0x60, 0x03},
	{0x66, 0x64, 0x0C},
	{0x66, 0x68, 0x30},
	{0x66, 0x6C, 0xC0},
	{0x67, 0x70, 0x03},
	{0x67, 0x74, 0x0C},
	{0x67, 0x78, 0x30},
	{0x67, 0x7C, 0xC0},
	{0x68, 0x80, 0x03},
	{0x68, 0x84, 0x0C},
	{0x68, 0x88, 0x30},
	{0x68, 0x8C, 0xC0},
	{0x69, 0x90, 0x03},
	{0x69, 0x94, 0x0C},
	{0x69, 0x98, 0x30},
	{0x69, 0x9C, 0xC0},
	{0x6A, 0xA0, 0x03},
	{0x6A, 0xA4, 0x0C},
	{0x6A, 0xA8, 0x30},
	{0x6A, 0xAC, 0xC0},
	{0x6B, 0xB0, 0x03},
	{0x6B, 0xB4, 0x0C},
	{0x6B, 0xB8, 0x30},
	{0x6B, 0xBC, 0xC0},
	{0x6C, 0xC0, 0x03},
	{0x6C, 0xC4, 0x0C},
	{0x6C, 0xC8, 0x30},
	{0x6C, 0xCC, 0xC0},
	{0x6D, 0xD0, 0x03},
	{0x6D, 0xD4, 0x0C},
	{0x6D, 0xD8, 0x30},
	{0x6D, 0xDC, 0xC0},
	{0x6E, 0xE0, 0x03},
	{0x6E, 0xE4, 0x0C},
	{0x6E, 0xE8, 0x30},
	{0x6E, 0xEC, 0xC0},
	{0x6F, 0xF0, 0x03},
	{0x6F, 0xF4, 0x0C},
	{0x6F, 0xF8, 0x30},
	{0x6F, 0xFC, 0xC0}
};

#define MAX_REG_NUMBER 167

struct SWITCH_REGS_CMD {
	unsigned char cmd;
	unsigned int reg;
	unsigned int data;
	unsigned int data1;
	unsigned int all_regs[MAX_REG_NUMBER];
	unsigned int statistics[126];
};

struct audio_rtp_port {
	unsigned short local_rtp_port;
	unsigned char priotity_enable;
};

struct audio_rtp_port ports_to_prioritize[MAX_RTP_AUDIO_PORTS];

#define SWITCH_CMD_READ										0
#define SWITCH_CMD_WRITE									1
#define SWITCH_CMD_ST_MAC_READ								2
#define SWITCH_CMD_ST_MAC_WRITE								3
#define SWITCH_CMD_ST_MAC_DEL 								4
#define SWITCH_CMD_VLAN_WRITE								5
#define SWITCH_CMD_VLAN_DEL									6
#define SWITCH_CMD_LINK_DETECT								7
#define SWITCH_CMD_OPERATION_SELECT							8
#define SWITCH_CMD_TOS_ADD									9
#define SWITCH_CMD_LIMIT_BW									10
#define SWITCH_CMD_PRIORITIZE_RTP_PORT						11
#define SWITCH_CMD_READ_ALL									12
#define SWITCH_CMD_GET_STATISTICS							13
#define SWITCH_CMD_GET_PORT_SPEED							14
#define SWITCH_CMD_SET_STORM_CNTRL							15
#define SWITCH_CMD_GET_PC_ETH_ADDR							16
#define SWITCH_CMD_BLOCK_8021X_atPC							17

typedef enum _AR8325_READ_PART {
		GLOBAL_REGS = 0,
		EEE_REGS, 
		PARSER_REGS,
		ACL_REGS,
		LOOKUP_REGS,
		QM_REGS,
		PKT_EDIT_REGS,
		PORT0_MIB,
		PORT1_MIB,
		PORT2_MIB,
		PORT3_MIB,
		PORT4_MIB,
		PORT5_MIB,
		ACL_MATCH_REGS,
		VLAN_TRANSL_REGS,
		PPPoE_REGS,
		INVALID_REGS
}AR8325_READ_PART;

extern unsigned int ar8xxx_port_status(int port);

#define TMOUT (HZ >> 1)   // Timeout is at 1/2 sec

struct net_device * __init sc14452_probe(int unit)
{
	struct net_device *dev;

	if(unit > 1)
		return NULL;

	dev = alloc_netdev(sizeof(struct net_local), "eth%d", ether_setup);
	register_netdev(dev);

	if (dev == NULL)
		return (struct net_device *)-ENOMEM;

	emac_init(dev, unit);

	printk("SC14452 EMAC %s\n", dev->name);

	return dev;
}

static int __init emac_init(struct net_device *dev, int unit)
{
	int i;
	struct net_local *lp;
	char *tmp;
	/* Initialize the device structure. */
	if (unit == 0) {
		dev->priv = (struct net_local*)  kmalloc(sizeof(struct net_local), GFP_KERNEL);
		if (dev->priv == NULL)
			return -ENOMEM;
		memset(dev->priv, 0, sizeof(struct net_local));
		dev->base_addr = 0xFF2000;
		lp = (struct net_local *) dev->priv;

		lp->state = STATE_OFF;
		#ifdef CONFIG_SC14452_ATHEROS_AR8236
			lp->mode = MODE_SWITCH;
		#else
			lp->mode = MODE_ROUTER;
		#endif
		lp->devices[PORT_0] = dev;
	}
	else {
		dev->priv = (dev_get_by_name("eth0"))->priv;
		lp = (struct net_local *) dev->priv;
		lp->devices[PORT_1] = dev;
	}
	printk(KERN_INFO "%s: %s found at %lx", dev->name ,cardname, dev->base_addr);
 	if (unit == 0)
	{
		tmp = strstr(saved_command_line, "ethaddr=");

		if (tmp) {
			sscanf(tmp, "ethaddr=%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
			&(dev->dev_addr[0]),&(dev->dev_addr[1]),&(dev->dev_addr[2]),
			&(dev->dev_addr[3]),&(dev->dev_addr[4]),&(dev->dev_addr[5]));
		}
    else {
			/* Some error occured during system startup and the ethernet
			address was not passed from bootloader. Assign a fixed predefined MAC address
			to make the device accessible and debugable.
			*/
			//FATAL ERROR
 			dev->dev_addr[0] = 0x00; dev->dev_addr[1] = 0x88; dev->dev_addr[2] = 0x88;
			dev->dev_addr[3] = 0x77; dev->dev_addr[4] = 0x99; dev->dev_addr[5] = 0x66;
		}
	}
	else if (unit == 1) {
 		tmp = strstr(saved_command_line, "eth2addr=");

    if(tmp) {
			sscanf(tmp,"eth2addr=%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
			&(dev->dev_addr[0]),&(dev->dev_addr[1]),&(dev->dev_addr[2]),
			&(dev->dev_addr[3]),&(dev->dev_addr[4]),&(dev->dev_addr[5]));
		}
    else {
			/*Some error occured during system startup and the ethernet
			address was not passed from bootloader. Assign a fixed predefined MAC address
			to make the device accessible and debugable.
			*/
			//FATAL ERROR
 			dev->dev_addr[0] = 0x00; dev->dev_addr[1] = 0x88; dev->dev_addr[2] = 0x88;
			dev->dev_addr[3] = 0x77; dev->dev_addr[4] = 0x99; dev->dev_addr[5] = 0x67;
		}
	}
 	/* Retrieve and print the ethernet address. */
	printk ("\n EMAC DEVICE ADDRESS (port:%d): ", unit);
	for (i = 0; i < 6; i++)
		printk(" %2.2x", dev->dev_addr[i]);
	printk("\n");
	
	for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) { 
		ports_to_prioritize[i].local_rtp_port = 0; 
		ports_to_prioritize[i].priotity_enable = 0;                 
	} 

	/* assign handlers */
	dev->open	= emac_open;
	dev->stop	= emac_close;
	dev->hard_start_xmit = emac_xmit;
	dev->get_stats = emac_get_stats;
	dev->set_multicast_list = &emac_set_multicast_list;
	dev->tx_timeout = (void *)emac_tx_timeout;
	dev->watchdog_timeo	= 30*HZ;
	dev->tx_queue_len = 100;
	dev->do_ioctl = sc14452_ioctl;
 	dev->irq = EMAC_INT;
 	/* Fill in the fields of the device structure with ethernet values. */
	ether_setup(dev);

	// init ethernet once 
	// because if we have two eth devices, under router mode, the emac_init will be called twice
	if (unit == 0)
		eth_init(dev);
	
	// Disable VLAN after reset of SOFT reboot. LV request
	disable_AR8xxx_VLAN();

 	SET_MODULE_OWNER(dev);
	return 0;
}

static int emac_tx_timeout(struct net_device *dev)
{
	return 0;
}

static int emac_open(struct net_device *dev)
{
  struct net_local *lp = (struct net_local *)dev->priv;
	unsigned short i;
	int result;
	struct sk_buff *skb;
 	unsigned short id;

	/* Prepare descriptors and skbs */
	/* Prepare tx descriptors and ring buffer */

	if (lp->mode == MODE_UNDEFINED) {
		#ifdef CONFIG_SC14452_ATHEROS_AR8236
			lp->mode = MODE_SWITCH;
		#else
			lp->mode = MODE_ROUTER;
		#endif
	}

	if (lp->mode == MODE_SWITCH)
		eth_init(dev);

	spin_lock_init(&lp->lock);

	if ((dev->name)[3] == '0')
		id = PORT_0;
	else
		id = PORT_1;

	lp->port_state[id] = 1;

#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
	spin_lock_init(&lp->filter_lock);
#endif
	if(lp->state == STATE_OFF) {
		lp->tx_ring_size = 0;
		lp->TX_RING_TAIL = 0;
		lp->TX_RING_HEAD = 0;

		for (i=0;i<MAX_TX_DESCS;i++)
		{
			lp->TX_RING[i] = NULL;
		}

		/* prepare rx descriptors and ring buffer */
		for(i=0;i<MAX_RX_DESCS;i++)
		{
			skb = dev_alloc_skb(MAX_PACKET_LEN);
			if (!skb)
			{
				printk("Could not allocate skb at open. \n");
				goto out;
			}
			skb_reserve(skb, 4);
			lp->RX_RING[i] = skb;
			lp->RX_DESCS[i].DES2 = (volatile unsigned long int) skb->data;
			lp->RX_DESCS[i].DES0 = 0x80000000; /* DMA OWNED... */
		}

		lp->CUR_RX_DESC = 0;
		lp->max_tx_packets = 0;

		tasklet_init(&lp->emac_rx_tasklet, emac_rx_tasklet_fn, (unsigned long)dev);

		emac_cfg(dev->dev_addr, lp);

		/* Acquire the IRQ before any is generated */
		result = request_irq(dev->irq, (void *)&emac_interrupt, SA_INTERRUPT, "emac_irq", lp->devices[0]);
		if (result)
		{
			printk("Could not acquire EMAC IRQ %d \n",dev->irq);
			goto out;
		}

		init_timer(&lp->emac_timer);
		lp->emac_timer.expires = jiffies + (2* TMOUT);
		lp->emac_timer.data = (unsigned long)dev;
		lp->emac_timer.function = &emac_timer_handler;
		add_timer(&lp->emac_timer);

#if defined (CONFIG_SC1445x_POWER_DRIVER)
    {
			u16 creg, phy_addr, phy_reg;

			for ( phy_addr = 1; phy_addr < 5; phy_addr++ ) {
					phy_reg = 0;
					creg = EMAC_MACR4_MII_ADDR_REG;
					while ((creg & 0x1) == 1) creg = EMAC_MACR4_MII_ADDR_REG;
					creg &= ~( 0x1f<<6 | 0x1f<<11);
					creg |=  (phy_addr << 11) | (phy_reg << 6) | 0x3;
					EMAC_MACR5_MII_DATA_REG = 0x800;
					EMAC_MACR4_MII_ADDR_REG = creg;
			 }
    }
#endif
		lp->state = STATE_ON;
	}

	if(lp->mode == MODE_ROUTER) {
		if (ar8xxx_port_status(id)) {
			printk("EMAC: eth%d Link detected\n", id);
			lp->Link[id] = 1;
			netif_carrier_on(dev);
			netif_start_queue(dev);
		}
		else{
			printk("EMAC: eth%d Link NOT detected\n", id);
			lp->Link[id] = 0;
			netif_carrier_off(dev);
		}
	}
	else //MODE SWITCH
	{
		if (ar8xxx_port_status(id))
		{
			printk("EMAC: Port%d Link detected\n", id);
			lp->Link[id] = 1;
		}
		else
		{
			printk("EMAC: Port%d Link NOT detected\n", id);
			lp->Link[id] = 0;
		}

		if (ar8xxx_port_status(1 - id))
		{
			printk("EMAC: Port%d Link detected\n", 1 - id);
			lp->Link[1 - id] = 1;
		}
		else
		{
			printk("EMAC: Port%d Link NOT detected\n", 1 - id);
			lp->Link[id] = 0;
		}

		netif_carrier_on(dev);
		netif_start_queue(dev);
	}
	return 0;

out:
	/* Clean any gathered SKBs */
	for(i=0;i<MAX_RX_DESCS;i++)
	{
		if(lp->RX_RING[i])
			dev_kfree_skb(lp->RX_RING[i]);
	}
	return -ENOMEM;
}

static int emac_close(struct net_device *dev)
{
  struct net_local *lp = (struct net_local *)dev->priv;
	unsigned short i, id;

	if((dev->name)[3] == '0')
		id = PORT_0;
	else
		id = PORT_1;

	// Re-Initialize statistics
	memset(&lp->stats[id], 0, sizeof(struct net_device_stats));

	lp->port_state[id] = 0;
	lp->Link[id] = 0;

	netif_stop_queue(dev);
	if(lp->port_state[1-id] == 0) { // Both interfaces down ....

		lp->state= STATE_OFF;
 		del_timer(&lp->emac_timer);

		free_irq(dev->irq, lp->devices[0]);
		for(i=0;i<MAX_RX_DESCS;i++)
			if(lp->RX_RING[i])
				dev_kfree_skb(lp->RX_RING[i]);

		for(i=0;i<MAX_TX_DESCS;i++)
			if(lp->TX_RING[i])
				dev_kfree_skb(lp->TX_RING[i]);

		tasklet_kill(&lp->emac_rx_tasklet);

		EMAC_MACR0_CONFIG_REG &= ~0x3;

		emac_init_tx_desc(lp->TX_DESCS);
		emac_init_rx_desc(lp->RX_DESCS);

		emac_soft_rst();

	} // Both interfaces down...

	return 0;
}

/* ********************************************************************************************************
*  The ioctl function is used to pass address filtering information between the driver and the application.
*  The SC14452 can protect up to 4 Ethernet addresses. This mechanism is used to ensure phone quality in
*  	malicious network load conditions.
*  The SIOCDEVPRIVATE slot is used for this purpose.
*  The commands exchanged are:
*
*	EMAC_LF_STATUS          0
*	EMAC_LF_ADD             1
*	EMAC_LF_REMOVE          2
*	EMAC_LF_REMOVE_ALL      3
*	EMAC_LF_START_ALL       4
*	EMAC_LF_START_BCAST     5
*	EMAC_LF_START_UCAST     6
*	EMAC_LF_STOP_ALL        7
*	EMAC_LF_STOP_BCAST      8
*	EMAC_LF_STOP_UCAST      9
*****************************************************************************************************/
					
static int sc14452_ioctl(struct net_device *dev, struct ifreq *ifr, int cmd)
{
  struct net_local *lp = (struct net_local *)dev->priv;
	struct SWITCH_REGS_CMD *switch_cmd_p;
	unsigned int i, k = 0;
	unsigned int temp = 0;

	switch (cmd) {
#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
		case SIOCDEVPRIVATE:
			spin_lock_irqsave(&lp->filter_lock,flags); /* protect access from other processes */
			command_struct_p = ( struct addr_filter_ioctl *) ifr->ifr_data;
			switch (command_struct_p->command)
			{
				case EMAC_LF_STATUS:
					command_struct_p ->command = EMAC_LF_COMMAND_SUCCESS;
					break;
				case EMAC_LF_ADD:
					/* Broadcast addresses are not allowed ...*/
					if ( command_struct_p->status.address_filters[0].MAC[0] & 0x1){
						command_struct_p->command = EMAC_LF_COMMAND_FAIL_BCAST;
						break;
					}
					for (i=0;i<4;i++)
					{	/* look for an empty slot */
						if(lp->filter_config.address_filters[i].enable){
							count++;
						}
						else
						{
							lp->filter_config.address_filters[i].enable = 1;
							lp->filter_config.address_filters[i].pid = command_struct_p->pid;
							memcpy(lp->filter_config.address_filters[i].MAC ,command_struct_p->status.address_filters[0].MAC , 6);
							/* It is important here that the second address register is writen last. Otherwise the HW will not
							   recognize the MAC address correctly */
							*(unsigned long *)(0xff2048 +(i*8))= (0xc000 <<16)| (lp->filter_config.address_filters[i].MAC[5] <<8 )| (lp->filter_config.address_filters[i].MAC[4] )	;
							*(unsigned long *)(0xff204c +(i*8))= (lp->filter_config.address_filters[i].MAC[3] <<24 )| (lp->filter_config.address_filters[i].MAC[2] << 16 ) | (lp->filter_config.address_filters[i].MAC[1] <<8 )| (lp->filter_config.address_filters[i].MAC[0]  );
							command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
							break;
						}
					}
					if(count == 4)
						command_struct_p->command = EMAC_LF_COMMAND_FAIL_FULL;
					break;
				case EMAC_LF_REMOVE:
					found = 0;
					for (i=0;i<4;i++)
					{
						if (lp->filter_config.address_filters[i].enable == 1)
						{
							if(!memcmp(lp->filter_config.address_filters[i].MAC , command_struct_p->status.address_filters[i].MAC,6))
							{
								found = 1;
								lp->filter_config.address_filters[i].enable = 0;
								*(unsigned long *)(0xff2048 +(i*8)) = 0x0;
								*(unsigned long *)(0xff204c +(i*8)) = 0x0;
								command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
								break;
							}
						}
					}
					if(!found)
					{
						command_struct_p->command = EMAC_LF_COMMAND_FAIL_NFOUND;
					}
					break;
				case EMAC_LF_REMOVE_ALL:
					break;
				case EMAC_LF_START_ALL:
					break;
				case EMAC_LF_START_BCAST:
					lp->filter_config.blp_enable = 1;
					lp->filter_config.blp_threshold = command_struct_p->status.blp_threshold;
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
				case EMAC_LF_START_UCAST:
					lp->filter_config.ulp_enable = 1;
					lp->filter_config.ulp_threshold = command_struct_p->status.ulp_threshold;
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
				case EMAC_LF_STOP_ALL:
					lp->filter_config.ulp_enable = 0;
					lp->filter_config.blp_enable = 0;
					lp->filter_config.blp_threshold = 0;
					lp->filter_config.ulp_threshold = 0;
					lp->filter_config.blp_protect = 0;
					lp->filter_config.ulp_protect = 0;
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 5);
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 9);
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
				case EMAC_LF_STOP_BCAST:
					lp->filter_config.blp_enable = 0;
					lp->filter_config.blp_threshold = 0;
					lp->filter_config.blp_protect = 0;
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 5);
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
				case EMAC_LF_STOP_UCAST:
					lp->filter_config.ulp_enable = 0;
					lp->filter_config.ulp_threshold = 0;
					lp->filter_config.ulp_protect = 0;
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 9);
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
				default:
					return -EOPNOTSUPP;
			}
			spin_unlock_irqrestore(&lp->filter_lock, flags);
			/* Always copy the status structure to provide status info to the application */
			memcpy(&(command_struct_p->status),&(lp->filter_config),sizeof(struct addr_filter_status));
      return 0;
#endif

		case SIOCDEVPRIVATE:
			switch_cmd_p = ( struct SWITCH_REGS_CMD *) ifr->ifr_data;
			switch (switch_cmd_p->cmd) {
				case SWITCH_CMD_READ:
					//printk("reading switch_cmd_p->reg=[%x]\n", switch_cmd_p->reg);
					switch_cmd_p->data = read_ar8xxx(switch_cmd_p->reg);
					//printk("reading 0x%04x = 0x%08x \n", switch_cmd_p->reg, switch_cmd_p->data);
					break;
				case SWITCH_CMD_READ_ALL:
					k = 0;
#ifdef CONFIG_SC14452_ATHEROS_AR8325
					if (switch_cmd_p->reg == GLOBAL_REGS) {
						for (i=0; i<57; i++) {
							switch_cmd_p->all_regs[i] = read_ar8xxx(i*4);
							//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						}
					}
					else if (switch_cmd_p->reg == EEE_REGS) {
						for (i=0; i<27; i++) {
							switch_cmd_p->all_regs[i] = read_ar8xxx(0x100 + i*4);
							//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						}
					}
					else if (switch_cmd_p->reg == PARSER_REGS) {
						for (i=0; i<29; i++) {
							switch_cmd_p->all_regs[i] = read_ar8xxx(0x200 + i*4);
							//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						}
					} 
					else if (switch_cmd_p->reg == ACL_REGS) {
						for (i=0; i<22; i++) {
							switch_cmd_p->all_regs[i] = read_ar8xxx(0x400 + i*4);
							//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						}
					}
					else if (switch_cmd_p->reg == LOOKUP_REGS) {
						for (i=0; i<67; i++) {
							switch_cmd_p->all_regs[i] = read_ar8xxx(0x600 + i*4);
							//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						}
					}
					else if (switch_cmd_p->reg == QM_REGS) {
						for (i=0; i<221; i++) {
							switch_cmd_p->all_regs[i] = read_ar8xxx(0x800 + i*4);
							//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						}
					} 
					else if (switch_cmd_p->reg == PKT_EDIT_REGS) {
						for (i=0; i<26; i++) {
							switch_cmd_p->all_regs[i] = read_ar8xxx(0xC00 + i*4);
							//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						}
					}
					else if ((switch_cmd_p->reg == PORT0_MIB) || (switch_cmd_p->reg == PORT1_MIB) ||
									(switch_cmd_p->reg == PORT2_MIB) || (switch_cmd_p->reg == PORT3_MIB) ||
									(switch_cmd_p->reg == PORT4_MIB) || (switch_cmd_p->reg == PORT5_MIB)) {
						for (i=0; i<42; i++) {
							switch_cmd_p->all_regs[i] = read_ar8xxx(0x1000 + 0x100*(switch_cmd_p->data - PORT0_MIB) + i*4);
							//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						}
					}
					else if (switch_cmd_p->reg == ACL_MATCH_REGS) {
						for (i=0; i<26; i++) {
							switch_cmd_p->all_regs[i] = read_ar8xxx(0x1C000 + i*4);
							//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						}
					}
					else if (switch_cmd_p->reg == VLAN_TRANSL_REGS) {
						for (i=0; i<129; i++) {
							switch_cmd_p->all_regs[i] = read_ar8xxx(0x5AC00 + i*4);
							//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						}
					}
					else if (switch_cmd_p->reg == PPPoE_REGS) {
						for (i=0; i<17; i++) {
							switch_cmd_p->all_regs[i] = read_ar8xxx(0x5F000 + i*4);
							//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						}
					}
					else printk("ETH ERROR: Read All out of range\n");
#else
					for (i=0; i<47; i++) {
						switch_cmd_p->all_regs[k] = read_ar8xxx(i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}

					for (i=0; i<9; i++) {
						switch_cmd_p->all_regs[k] = read_ar8xxx(0x100+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x100+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}
#ifdef CONFIG_SC14452_ATHEROS_AR8236
					for (i=0; i<9; i++) {
						switch_cmd_p->all_regs[k] = read_ar8xxx(0x200+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x100+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}
#endif
					for (i=0; i<9; i++) {
						switch_cmd_p->all_regs[k] = read_ar8xxx(0x300+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x300+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}
					for (i=0; i<9; i++) {
						switch_cmd_p->all_regs[k] = read_ar8xxx(0x400+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x400+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}

#ifdef CONFIG_SC14452_ATHEROS_AR8236
					for (i=0; i<9; i++) {
						switch_cmd_p->all_regs[k] = read_ar8xxx(0x500+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x100+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}
#endif
					for (i=0; i<9; i++) {
						switch_cmd_p->all_regs[k] = read_ar8xxx(0x600+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x600+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}

					for (i=0; i<32; i++) {
						switch_cmd_p->all_regs[k] = emac_md_read (1, i, EMAC_MDC_DIV_42);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x600+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}

					for (i=0; i<32; i++) {
						switch_cmd_p->all_regs[k] = emac_md_read (2, i, EMAC_MDC_DIV_42);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x600+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}
					break;
#endif
				case SWITCH_CMD_GET_STATISTICS:
					k = 0;
#ifdef CONFIG_SC14452_ATHEROS_AR8325
					//CPU port
					for (i=0; i<42; i++) {
						switch_cmd_p->statistics[k] = read_ar8xxx(0x1000+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						k++;
					}
					//Port2 PC port
					for (i=0; i<42; i++) {
						switch_cmd_p->statistics[k] = read_ar8xxx(0x1200+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x100+i, switch_cmd_p->all_regs[k]);
						k++;
					}
					//Port3 WAN port
					for (i=0; i<42; i++) {
						switch_cmd_p->statistics[k] = read_ar8xxx(0x1300+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x300+i, switch_cmd_p->all_regs[k]);
						k++;
					}
#else
					for (i=0; i<42; i++) {
						switch_cmd_p->statistics[k] = read_ar8xxx(0x20000+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						k++;
					}
					for (i=0; i<42; i++) {
						switch_cmd_p->statistics[k] = read_ar8xxx(0x20200+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x100+i, switch_cmd_p->all_regs[k]);
						k++;
					}
					for (i=0; i<42; i++) {
						switch_cmd_p->statistics[k] = read_ar8xxx(0x20300+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x300+i, switch_cmd_p->all_regs[k]);
						k++;
					}
#endif
					break;
				case SWITCH_CMD_WRITE:
					write_ar8xxx(switch_cmd_p->reg, switch_cmd_p->data);
					break;
				case SWITCH_CMD_VLAN_WRITE:
					printk(">>>>>>--ETH: SWITCH_CMD_VLAN_WRITE \n");
#ifdef CONFIG_SC14452_ATHEROS_AR8314

					printk("vlan_mode=[0x%08x], vid_phone=[%d], prio_phone=[%d], vid_pc=[%d], prio_pc=[%d]\n", 
						((switch_cmd_p->reg & 0xC0000000)>>30), (switch_cmd_p->data & 0xFFFF), (switch_cmd_p->reg & 0x7), 
						(switch_cmd_p->data1 & 0xFFFF), ((switch_cmd_p->reg & 0x00070000) >> 16));
					
					// AR8134 ONLY SUPPORTS TAGGED OR UNTAGGED
					// vlan_mode = 1 
					// ALLOW ONLY TAGGED FRAMES AT WAN PORT
					if ((switch_cmd_p->reg & 0xC0000000) == 0x40000000) {

						write_ar8xxx(0x40, 0x9); // Flush all previous VLAN entries

						// switch_regs.reg = (unsigned int) 0x108;
						// switch_regs.data = (unsigned int) ((1 << 30) | 0x80000 | (prio_pc << 27) | (vid_pc & 0xFFFF)); // 22/8/2011 added 1<<30 to fix problem when using same VLAN ids at both ports
						// port-based VLAN register
						// Port 0 CPU port
						write_ar8xxx(0x108, (1 << 30) | 0x80000 | (((switch_cmd_p->reg & 0x7) << 27) | (switch_cmd_p->data & 0xFFFF)));

						// switch_regs.reg = (unsigned int) 0x308;
						// switch_regs.data = (unsigned int) ((1 << 30) | 0x80000 | (prio_pc << 27) | (vid_pc & 0xFFFF)); // 22/8/2011 added 1<<30 to fix problem when using same VLAN ids at both ports
						// port-based VLAN register
						// Port 2 PC port
						write_ar8xxx(0x308, (1 << 30) | 0x80000 | ((((switch_cmd_p->reg & 0x00070000) >> 16) << 27) | (switch_cmd_p->data1 & 0xFFFF)));
				
						// switch_regs.reg = (unsigned int) 0x408;
						// switch_regs.data = (unsigned int) 0x40370001;
						// port-based VLAN register
						// Port 3 LAN/Phone port
						write_ar8xxx(0x408, 0x40370001);

						// switch_regs.reg = (unsigned int) 0x44;
						// switch_regs.data = (unsigned int) 0x809;
						// Write VLAN table 1. Port Members are Port 0 CPU Port and Port 3 LAN/Phone port
						write_ar8xxx(0x44, 0x809);

						// switch_regs.reg = (unsigned int) 0x40;
						// switch_regs.data = (unsigned int)(((vid_phone & 0xFFFF) << 16) | 0xa);
						// For 8314 at phone port keep VLAN incoming packet's priority. 
						// Write VLAN table 1
						write_ar8xxx(0x40, ((switch_cmd_p->data & 0xFFFF) << 16) | 0xa);
				
						// switch_regs.reg = (unsigned int) 0x44;
						// switch_regs.data = (unsigned int) 0x80c;
						// Write VLAN table 2
						// Write VLAN table 1. Port Members are Port 2 PC Port and Port 3 LAN/Phone port
						write_ar8xxx(0x44, 0x80c);
														
						// switch_regs.reg = (unsigned int) 0x40;
						// switch_regs.data = (unsigned int)((1 << 31) | ((prio_pc & 0x7) << 28) | ((vid_pc & 0xFFFF) << 16) | 0xa);
						// Write VLAN table 2
						write_ar8xxx(0x40, ((1 << 31) | ( ((switch_cmd_p->reg & 0x00070000) >> 16) << 28) | ((switch_cmd_p->data1 & 0xFFFF) << 16) | 0xa));

						// Enable Forward Mode, Learning and egress VLAN
						write_ar8xxx(0x104, 0x4904);
						write_ar8xxx(0x304, 0x4104);
						write_ar8xxx(0x404, 0x4204);
						// Enable VLAN and Port based priorities
						write_ar8xxx(0x110, 0xa001b);
						write_ar8xxx(0x310, 0xa001b);
						write_ar8xxx(0x410, 0xa001b);
					}


#elif defined CONFIG_SC14452_ATHEROS_AR8325
				// VTU_FUNC_REG1
				write_ar8xxx(0x0614, 0x80000009);	// Flush all previous VLAN entries

				printk("vlan_mode=[0x%08x], vid_phone=[%d], prio_phone=[%d], vid_pc=[%d], prio_pc=[%d]\n", 
					((switch_cmd_p->reg & 0xC0000000)>>30), (switch_cmd_p->data & 0xFFFF), (switch_cmd_p->reg & 0x7), 
					(switch_cmd_p->data1 & 0xFFFF), ((switch_cmd_p->reg & 0x00070000) >> 16));

				// 0x0660 CPU port0
				// 0x0678 PC port 2
				// 0x0684 WAN port 3
				// PORTX_LOOKUP_CTRL
				unset_ar8xxx(0x0660, 0x7F, 0);		
				set_ar8xxx(0x0660, 1, 3);				// CPU port0. PORT_VID_MEM=0001000. Sent packets only to WAN port3
				unset_ar8xxx(0x0660, 0x3, 8);	
				set_ar8xxx(0x0660, 2, 8);				// CPU port0. 802.1Q_MODE=10. Enable Check 802_1Q vlan mode
				unset_ar8xxx(0x0678, 0x7F, 0);		
				set_ar8xxx(0x0678, 1, 3);				// PC port2. PORT_VID_MEM=0001000. Sent packets only to WAN port3
				unset_ar8xxx(0x0678, 0x3, 8);	
				set_ar8xxx(0x0678, 2, 8);				// PC port2. 802.1Q_MODE=10. Enable Check 802_1Q vlan mode
				unset_ar8xxx(0x0684, 0x7F, 0);	
				set_ar8xxx(0x0684, 0x5, 0);				// WAN port3. PORT_VID_MEM=0000101. Sent packets only to port2 and port3
				unset_ar8xxx(0x0684, 0x3, 8);	
				set_ar8xxx(0x0684, 2, 8);				// WAN port3. 802.1Q_MODE=10. Enable Check 802_1Q vlan mode

				// PORT0_PRI_CTRL
				set_ar8xxx(0x0664, 1, 17);			// CPU port0, vlan priorities enable
				set_ar8xxx(0x067C, 1, 17);			// PC port2, vlan priorities enable
				set_ar8xxx(0x0688, 1, 17);			// WAN port3, vlan priorities enable

				// Write 0x420 CPU port 0 default VID and priotity.
				// ING_PORT_CPRI_0=prio_phone, PORT_DEFAULT_CVID_0=vid_phone
				write_ar8xxx(0x0420, (((switch_cmd_p->reg & 0x7) << 29) | ((switch_cmd_p->data & 0xFFFF) << 16) | 0x1));

				// Write 0x430 PC port 2 default VID and priotity.
				// ING_PORT_CPRI_2=prio_pc, PORT_DEFAULT_CVID_2=vid_pc
				write_ar8xxx(0x0430, ((((switch_cmd_p->reg & 0x00070000) >> 16) << 29) | ((switch_cmd_p->data1 & 0xFFFF) << 16) | 0x1));

				// Default value. No port based VID
				write_ar8xxx(0x0438, 0x10001);

				// PORTX_VLAN_CTRL1
				unset_ar8xxx(0x0424, 0x1, 8);		// FORCE_DEFAULT_VID_EN_0 = 0
				unset_ar8xxx(0x0424, 0x3, 12);	
				set_ar8xxx(0x0424, 1, 12);			// EG_VLAN_MODE=10 egress at CPU port0 without vlan
				unset_ar8xxx(0x0434, 0x1, 8);		// FORCE_DEFAULT_VID_EN_2 = 0
				unset_ar8xxx(0x0434, 0x3, 12);	
				set_ar8xxx(0x0434, 1, 12);			// EG_VLAN_MODE=01, egress at PC port2 without vlan
				unset_ar8xxx(0x043C, 0x1, 8);		// FORCE_DEFAULT_VID_EN_3 = 0
				unset_ar8xxx(0x043C, 0x3, 12);	
				set_ar8xxx(0x043C, 3, 12);			// EG_VLAN_MODE=11 untouched, egress at WAN port2 with vlan

				// vlan_mode = 1
				// ALLOW ONLY TAGGED FRAMES AT WAN PORT
				if ((switch_cmd_p->reg & 0xC0000000) == 0x40000000) {
					// if prio_phone > 0
					// Write Vlan table 1
					// Valid VTU table entry
					// VTU_EG_VLAN_MODE = 0x3F3D CPU port 0 untagged (01b), WAN port 3 unmofified (00b) and all other ports not members (11b).
					// Overwrite packets vlan priority at VTU_PRI if prio_phone is not zero one at VTU_PRI
					if (switch_cmd_p->reg & 0x7) {
						// 0x3F3D port members, VTU_VALID=1, VTU_PRI_OVER_EN=1, VTU_PRI=prio_phone
						write_ar8xxx(0x0610, ((1 << 20) | (1 << 19) | (0x3FBD << 4) | (1 << 3) | (switch_cmd_p->reg & 0x7)));
					}else {
						// 0x3F3D port members, VTU_VALID=1. If prio_phone=0 leave packet's default vlan priority
						write_ar8xxx(0x0610, ((1 << 20) | (1 << 19) | (0x3FBD << 4)));
					}
					// Write Vlan table 1
					// Load a VID entry (VTU_FUNC), set the VT_BUSY
					// programm the VID (vid_phone)
					// VTU_FUNC=010, VT_BUSY=1
					write_ar8xxx(0x0614, ((1 << 31) | ((switch_cmd_p->data & 0xFFFF) << 16) | 0x2));

					// Write Vlan table 2
					// Valid VTU table entry
					// VTU_EG_VLAN_MODE = 0x3F1F PC port 2 untagged (01b), WAN port 3 unmofified (00b) and all other ports not members (11b).
					// Overwrite packets vlan priority at VTU_PRI always
					// 0x3F3D port members, VTU_VALID=1, VTU_PRI_OVER_EN=1, VTU_PRI=prio_pc
					write_ar8xxx(0x0610, ((1 << 20) | (1 << 19) | (0x3F9F << 4) | (1 << 3) | ((switch_cmd_p->reg & 0x00070000) >> 16)));

					// Write Vlan table 2
					// Load a VID entry (VTU_FUNC), set the VT_BUSY
					// programm the VID (vid_phone)
					// VTU_FUNC=010, VT_BUSY=1
					write_ar8xxx(0x0614, ((1 << 31) | ((switch_cmd_p->data1 & 0xFFFF) << 16) | 0x2));

				// vlan_mode = 2
				// TAGGED FRAMES AT WAN PORT TO CPU PORT, UNTAGGED TO PC PORT
				} else if ((switch_cmd_p->reg & 0xC0000000) == 0x80000000) {

					// if prio_phone > 0
					// Write Vlan table 1
					// Valid VTU table entry
					// VTU_EG_VLAN_MODE = 0x3F3D CPU port 0 untagged (01b), WAN port 3 unmofified (00b) and all other ports not members (11b).
					// Overwrite packets vlan priority at VTU_PRI if prio_phone is not zero one at VTU_PRI
					if (switch_cmd_p->reg & 0x7) {
						// 0x3F3D port members, VTU_VALID=1, VTU_PRI_OVER_EN=1, VTU_PRI=prio_phone
						write_ar8xxx(0x0610, ((1 << 20) | (1 << 19) | (0x3FBD << 4) | (1 << 3) | (switch_cmd_p->reg & 0x7)));
					}else {
						// 0x3F3D port members, VTU_VALID=1. If prio_phone=0 leave packet's default vlan priority
						write_ar8xxx(0x0610, ((1 << 20) | (1 << 19) | (0x3FBD << 4)));
					}
					// Write Vlan table 1
					// Load a VID entry (VTU_FUNC), set the VT_BUSY
					// programm the VID (vid_phone)
					// VTU_FUNC=010, VT_BUSY=1
					write_ar8xxx(0x0614, ((1 << 31) | ((switch_cmd_p->data & 0xFFFF) << 16) | 0x2));

					// Write Vlan table 2
					// Valid VTU table entry
					// VTU_EG_VLAN_MODE = 0x3F1F PC port 2 untagged (01b), WAN port 3 unmofified (00b) and all other ports not members (11b).
					// Overwrite packets vlan priority at VTU_PRI always
					// 0x3F3D port members, VTU_VALID=1, VTU_PRI_OVER_EN=1, VTU_PRI=prio_pc
					write_ar8xxx(0x0610, ((1 << 20) | (1 << 19) | (0x3F5F << 4) | (1 << 3) | ((switch_cmd_p->reg & 0x00070000) >> 16)));

					// Write Vlan table 2
					// Load a VID entry (VTU_FUNC), set the VT_BUSY
					// programm the VID (vid_phone)
					// VTU_FUNC=010, VT_BUSY=1
					write_ar8xxx(0x0614, ((1 << 31) | ((switch_cmd_p->data1 & 0xFFFF) << 16) | 0x2));

					set_ar8xxx(0x0424, 0x1, 8);		// FORCE_DEFAULT_VID_EN_0 = 1
					set_ar8xxx(0x0434, 0x1, 8);		// FORCE_DEFAULT_VID_EN_2 = 1
				
					unset_ar8xxx(0x0684, 0x3, 8);	
					set_ar8xxx(0x0684, 1, 8);			// WAN port3. 802.1Q_MODE=01. Enable Fallback 802_1Q vlan mode
				
				// vlan_mode = 3
				// TAGGED FRAMES AT WAN PORT TO PC PORT, UNTAGGED TO CPU PORT
				} else if ((switch_cmd_p->reg & 0xC0000000) == 0xC0000000) {

					// if prio_phone > 0
					// Write Vlan table 1
					// Valid VTU table entry
					// VTU_EG_VLAN_MODE = 0x3F3D CPU port 0 untagged (01b), WAN port 3 unmofified (00b) and all other ports not members (11b).
					// Overwrite packets vlan priority at VTU_PRI if prio_phone is not zero one at VTU_PRI
					if (switch_cmd_p->reg & 0x7) {
						// 0x3F3D port members, VTU_VALID=1, VTU_PRI_OVER_EN=1, VTU_PRI=prio_phone
						write_ar8xxx(0x0610, ((1 << 20) | (1 << 19) | (0x3F7D << 4) | (1 << 3) | (switch_cmd_p->reg & 0x7)));
					}else {
						// 0x3F3D port members, VTU_VALID=1. If prio_phone=0 leave packet's default vlan priority
						write_ar8xxx(0x0610, ((1 << 20) | (1 << 19) | (0x3F7D << 4)));
					}
					// Write Vlan table 1
					// Load a VID entry (VTU_FUNC), set the VT_BUSY
					// programm the VID (vid_phone)
					// VTU_FUNC=010, VT_BUSY=1
					write_ar8xxx(0x0614, ((1 << 31) | ((switch_cmd_p->data & 0xFFFF) << 16) | 0x2));

					// Write Vlan table 2
					// Valid VTU table entry
					// VTU_EG_VLAN_MODE = 0x3F1F PC port 2 untagged (01b), WAN port 3 unmofified (00b) and all other ports not members (11b).
					// Overwrite packets vlan priority at VTU_PRI always
					// 0x3F3D port members, VTU_VALID=1, VTU_PRI_OVER_EN=1, VTU_PRI=prio_pc
					write_ar8xxx(0x0610, ((1 << 20) | (1 << 19) | (0x3F9F << 4) | (1 << 3) | ((switch_cmd_p->reg & 0x00070000) >> 16)));

					// Write Vlan table 2
					// Load a VID entry (VTU_FUNC), set the VT_BUSY
					// programm the VID (vid_phone)
					// VTU_FUNC=010, VT_BUSY=1
					write_ar8xxx(0x0614, ((1 << 31) | ((switch_cmd_p->data1 & 0xFFFF) << 16) | 0x2));

					unset_ar8xxx(0x0684, 0x3, 8);	
					set_ar8xxx(0x0684, 1, 8);			// WAN port3. 802.1Q_MODE=01. Enable Fallback 802_1Q vlan mode

					set_ar8xxx(0x0424, 0x1, 8);		// FORCE_DEFAULT_VID_EN_0 = 1
					set_ar8xxx(0x0434, 0x1, 8);		// FORCE_DEFAULT_VID_EN_2 = 1
				}

#elif defined CONFIG_SC14452_ATHEROS_AR8236
					// Enable VLAN
					write_ar8xxx(0x10c, 0x40100000);
					write_ar8xxx(0x30c, 0x40100000);
					write_ar8xxx(0x50c, 0x402f0000);
					// Enable Forward Mode, Learning and egress VLAN
					write_ar8xxx(0x104, 0x4904);
					write_ar8xxx(0x304, 0x4104);
					write_ar8xxx(0x504, 0x4204);
					// Enable VLAN and Port based priorities
					write_ar8xxx(0x114, 0xa001b);
					write_ar8xxx(0x314, 0xa001b);
					write_ar8xxx(0x514, 0xa001b);
#endif
					break;
				case SWITCH_CMD_VLAN_DEL:
					printk(">>>>>>--ETH: SWITCH_CMD_VLAN_DEL \n");
#ifdef CONFIG_SC14452_ATHEROS_AR8325
					// PORTX_VLAN_CTRL0
					unset_ar8xxx(0x420, 0x7, 29);			// ING_PORT_CPRI_0=0 
					unset_ar8xxx(0x420, 0xFFFF, 16);	
					set_ar8xxx(0x420, 1, 16);					// PORT_DEFAULT_CVID_0=1
					unset_ar8xxx(0x430, 0x7, 29);			// ING_PORT_CPRI_2=0 
					unset_ar8xxx(0x430, 0xFFFF, 16);	
					set_ar8xxx(0x430, 1, 16);					// PORT_DEFAULT_CVID_2=1
					unset_ar8xxx(0x438, 0x7, 29);			// ING_PORT_CPRI_3=0 
					unset_ar8xxx(0x438, 0xFFFF, 16);	
					set_ar8xxx(0x438, 1, 16);					// PORT_DEFAULT_CVID_3=1

					// PORTX_VLAN_CTRL1
					unset_ar8xxx(0x0424, 0x3, 12);		// EG_VLAN_MODE=11, egress at CPU port0 untouched
					set_ar8xxx(0x0424, 0x3, 12);			// EG_VLAN_MODE=11, egress at CPU port0 untouched
					unset_ar8xxx(0x0434, 0x3, 12);		// EG_VLAN_MODE=11, egress at PC port2 untouched
					set_ar8xxx(0x0434, 0x3, 12);			// EG_VLAN_MODE=11, egress at PC port2 untouched
					unset_ar8xxx(0x043C, 0x3, 12);		// EG_VLAN_MODE=11, egress at WAN port2 untouched
					set_ar8xxx(0x043C, 0x3, 12);			// EG_VLAN_MODE=11, egress at WAN port2 untouched
					
					// PORT0_LOOKUP_CTRL
					unset_ar8xxx(0x0660, 0x3, 8);			// CPU port0. 802.1Q_MODE=00. Disable 802.1Q vlan
					unset_ar8xxx(0x0660, 0x7F, 0);
					set_ar8xxx(0x0660, 0xC, 0);				// CPU port0. PORT_VID_MEM=0001100. Sent packets to ports 2 & 3
					unset_ar8xxx(0x0678, 0x3, 8);			// PC port2. 802.1Q_MODE=00. Disable 802.1Q vlan
					unset_ar8xxx(0x0678, 0x7F, 0);
					set_ar8xxx(0x0678, 0x9, 0);				// PC port2. PORT_VID_MEM=001001. Sent packets to ports 0 & 3
					unset_ar8xxx(0x0684, 0x3, 8);			// WAN port3. 802.1Q_MODE=00. Disable 802.1Q vlan
					unset_ar8xxx(0x0684, 0x7F, 0);
					set_ar8xxx(0x0684, 0x5, 0);				// WAN port3. PORT_VID_MEM=000101. Sent packets to all port 0 & 2
					
					// VTU_FUNC_REG1
					write_ar8xxx(0x0614, 0x80000009);	// Flash all VLAN entries

					// PORT0_PRI_CTRL
					unset_ar8xxx(0x0664, 1, 17);			// CPU port0, vlan priorities disable
					unset_ar8xxx(0x067C, 1, 17);			// PC port2, vlan priorities disable
					unset_ar8xxx(0x0688, 1, 17);			// WAN port3, vlan priorities disable

#elif defined CONFIG_SC14452_ATHEROS_AR8314
					write_ar8xxx(0x108, 0x003e0001);
					write_ar8xxx(0x308, 0x003b0001);
					write_ar8xxx(0x408, 0x00370001);
					write_ar8xxx(0x104, 0x4804);
					write_ar8xxx(0x304, 0x4004);
					write_ar8xxx(0x404, 0x4004);
#elif defined CONFIG_SC14452_ATHEROS_AR8236
					write_ar8xxx(0x108, 0x10000);
					write_ar8xxx(0x308, 0x10000);
					write_ar8xxx(0x508, 0x10000);
					write_ar8xxx(0x104, 0x4804);
					write_ar8xxx(0x304, 0x4004);
					write_ar8xxx(0x504, 0x4004);
					write_ar8xxx(0x10c, 0x3e0000);
					write_ar8xxx(0x30c, 0x3b0000);
					write_ar8xxx(0x50c, 0x2f0000);
#endif
					break;
				case SWITCH_CMD_TOS_ADD:
					break;
				case SWITCH_CMD_LIMIT_BW:
					break;
				case SWITCH_CMD_PRIORITIZE_RTP_PORT:
					// Disable prioritization on a single port.
					printk(">>>>>>--Trying to prioritize\n");
					if (switch_cmd_p->reg == 0) { //Disable priotitization
						for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) {
							if (ports_to_prioritize[i].local_rtp_port == switch_cmd_p->data) {
								ports_to_prioritize[i].local_rtp_port = 0;
								ports_to_prioritize[i].priotity_enable = 0;
#ifdef CONFIG_SC14452_ATHEROS_AR8314
								temp = read_ar8xxx(0x58428 + i*0x20);
								// find at which ACL address the rtp port to disable is set
								if (((temp & 0xffff0000) >> 16) == switch_cmd_p->data) {
									printk(">>>>>>--PORT [%d] FOUND. Disable Prioritization\n", (temp & 0xffff0000) >> 16);
									write_ar8xxx(0x58824 + i*0x20, 0);				// Addr 0
									write_ar8xxx(0x58820 + i*0x20, 0);				// Addr set to invalid
//#elif defined CONFIG_SC14452_ATHEROS_AR8325
								// ADD THE IF STATEMENT
//									write_ar8xxx(0x40C, 0x11);						// ACL byte [11:8]. Set UDP protocol and dest port to 00h
//									write_ar8xxx(0x400, 0x80000001 + i);			// ACL_Busy, Write command, Rule and Index to ind=(1+i)
//#endif
								}
#elif defined CONFIG_SC14452_ATHEROS_AR8325
								//ADD THE IF STATEMENT
								write_ar8xxx(0x40C, 0x11);							// ACL byte [11:8]. Set UDP protocol and dest port to 00h
								write_ar8xxx(0x400, 0x80000001 + i);				// ACL_Busy, Write command, Rule and Index to ind=(1+i)
#endif
								break;
							}
						}
						if (i == MAX_RTP_AUDIO_PORTS)
							printk("ETH WARNING: Port is not in prioritization list\n");

						// Get next free entry
						for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) {
							if (ports_to_prioritize[i].priotity_enable == 0) continue;
							else break;
						}
						// Enter here if both port prioritazation is disabled
						if (i == MAX_RTP_AUDIO_PORTS) {
							printk(">>>>>>--ETH: Port prioritization fully disable for both RTP ports\n");
#ifdef CONFIG_SC14452_ATHEROS_AR8314
							write_ar8xxx(0x3c, 0xc000000e);			// Disable ACL
							write_ar8xxx(0x11c, 0x7fff7fff);		// Set engress priority limits to max
							write_ar8xxx(0x120, 0x7fff7fff);		// Set engress priority limits to max
#elif defined CONFIG_SC14452_ATHEROS_AR8325
							// not need to disable ACL since they may be used for MAC filtering and other
							// just set the address of the specific ACL to invalid
							//unset_ar8xxx(0x30, 1, 1);				// Disable ACL
							unset_ar8xxx(0x8AC, 1, 3);				// Disable egress limits 
							write_ar8xxx(0x898, 0x7fff7fff);		// Set CPU port0 engress priority limits for queues 4&5 to max
							write_ar8xxx(0x894, 0x7fff7fff);		// Set CPU port0 engress priority limits for queues 2&3 to max
							write_ar8xxx(0x890, 0x7fff7fff);		// Set CPU port0 engress priority limits for queues 0&1 to max
#endif
						}
					}
					else {
						for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) {
							// port already exists and is enabled
							if ((ports_to_prioritize[i].local_rtp_port == switch_cmd_p->data) && (ports_to_prioritize[i].priotity_enable == 1)) {
								return 0;
							}
						}
						for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) {
							if (ports_to_prioritize[i].priotity_enable == 0) {
								ports_to_prioritize[i].local_rtp_port = switch_cmd_p->data;
								ports_to_prioritize[i].priotity_enable = 1;
								temp = (switch_cmd_p->data << 16) | 0x11;			// UDP destination port to prioritize and protocol. UDP=0x11, TCP=0x6
#ifdef CONFIG_SC14452_ATHEROS_AR8314
								printk(">>>>>>--1UDP PORT=[%x] \n", temp);
								write_ar8xxx(0x3c, 0xc020000e);						// Always enable ACLs
								write_ar8xxx(0x58428 + i*0x20, temp);
								write_ar8xxx(0x58824 + i*0x20, i+1);				// Addr 0
								write_ar8xxx(0x58820 + i*0x20, 1);					// Addr valid
								write_ar8xxx(0x11c, 0x00220022);					// Set engress priority limits
								write_ar8xxx(0x120, 0x00a00022);					// Set engress priority limits
#elif defined CONFIG_SC14452_ATHEROS_AR8325
								printk(">>>>>>--2UDP PORT=[%x] port=[%d]\n", temp, switch_cmd_p->data);
								set_ar8xxx(0x30, 1, 1);								// Always enable ACL
								write_ar8xxx(0x40C, temp);							// ACL byte [11:8]. Set UDP protocol and dest port
								write_ar8xxx(0x400, 0x80000001 + i);				// ACL_Busy, Write command, Rule and Index to ind=(1+i)
								set_ar8xxx(0x8AC, 1, 3);							// Enable egress limits 
								write_ar8xxx(0x898, 0x00220022);					// Set CPU port0 engress priority limits for queues 4&5 to max
								write_ar8xxx(0x894, 0x00a00022);					// Set CPU port0 engress priority limits for queues 2&3 to max
								write_ar8xxx(0x890, 0x00220022);					// Set CPU port0 engress priority limits for queues 0&1 to max
#endif

								break;
							}
						}
						if (i == MAX_RTP_AUDIO_PORTS)
							printk("ETH ERROR: No available place for audio RTP port prioritization \n");
					}
					return 0;
				case SWITCH_CMD_LINK_DETECT:
						printk(">>>>>>--ETH: SWITCH_CMD_LINK_DETECT \n");
						switch_cmd_p->data = lp->Link[0];
						switch_cmd_p->data1 = lp->Link[1];
					return 0;
				case SWITCH_CMD_OPERATION_SELECT:
					if (switch_cmd_p->reg == MODE_ROUTER) {
						printk(">>>>>>--ETH: SWITCH_CMD_OPERATION_SELECT MODE_ROUTER\n");
						lp->mode = MODE_ROUTER;
					}
					else if (switch_cmd_p->reg == MODE_SWITCH) {
						printk(">>>>>>--ETH: SWITCH_CMD_OPERATION_SELECT MODE_SWITCH\n");
						lp->mode = MODE_SWITCH;
					}
					return 0;
				case SWITCH_CMD_GET_PORT_SPEED:
					printk(">>>>>>--ETH: SWITCH_CMD_GET_PORT_SPEED \n");
					switch_cmd_p->data = ar8xxx_port_get_speed(0);
					switch_cmd_p->data1 = ar8xxx_port_get_speed(1);
					return 0;
				case SWITCH_CMD_SET_STORM_CNTRL:
					AR8XXX_storm_cntrl(switch_cmd_p->reg);
					return 0;
				case SWITCH_CMD_GET_PC_ETH_ADDR:
					AR8XXX_get_first_mac_PCport(switch_cmd_p->all_regs);
					printk(">>>>>>--ETH: SWITCH_CMD_GET_PC_ETH_ADDR \nMAC= %2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x\n", 
						switch_cmd_p->all_regs[5], switch_cmd_p->all_regs[4], switch_cmd_p->all_regs[3],
						switch_cmd_p->all_regs[2], switch_cmd_p->all_regs[1], switch_cmd_p->all_regs[0]);
					return 0;
				case SWITCH_CMD_BLOCK_8021X_atPC:
					printk(">>>>>>--ETH: SWITCH_CMD_BLOCK_8021X_atPC \n");
					if (switch_cmd_p->reg == 1) {
						AR8XXX_block_8021x_atPC(1);
						printk(">>>>>>--ETH: SWITCH_CMD_GET_PORT_SPEED Enable\n");
					}else {
						AR8XXX_block_8021x_atPC(0);
						printk(">>>>>>--ETH: SWITCH_CMD_GET_PORT_SPEED Disable\n");
					}
					return 0;
				default: break;
			}
			return 0;
			default: break;
		}
	return -EOPNOTSUPP;
}

/*********************************************************************************************
*  The timer handler is used to run the Ethernet address filtering mechanism. The timer ticks
*  every 1/2 sec. If the address filters are enabled, it checks whether the received unicast
*  or broadcast packets exceed the user defined threshold. In this case, the filtering is enabled.
*  For unicast filtering, the MMC counters are then checked at every tick and the number
*  of filtered packets are monitored against the threshold. If this nomber drops below the
*  threshold, the filtering is disabled. For broadcast trafic, after disabling the reception
*  the system enables again in 4 seconds to check whether the broadcast storm is over.
*********************************************************************************************/
static void emac_timer_handler(unsigned long data)
{
  struct net_device *dev = (struct net_device *)data;
  struct net_local *lp = (struct net_local *)dev->priv;
	int i;

	for(i=0; i<2; i++)
	{
		if(lp->mode == MODE_ROUTER)
		{
  		if(lp->port_state[i] == 1)
			{
				if(lp->Link[i] != ar8xxx_port_status(i)) // status changed....
				{
					if(lp->Link[i] == 0) // Link detected....
					{
						printk(" EMAC: eth%d Link Up !\n", i);
						lp->Link[i] = 1;
						netif_carrier_on(lp->devices[i]);
					}
					else
					{
						printk(" EMAC: eth%d Link Down !\n", i);
						lp->Link[i] = 0;
						netif_carrier_off(lp->devices[i]);
					}
				}
			}
		}
		else // MODE_SWITCH
		{
			if(ar8xxx_port_status(i)) //carrier found
			{
				if(lp->Link[i] == 0)
				{
					printk(" EMAC: Port%d Link Up !\n", i);
					lp->Link[i] = 1;
				}
			}
			else // NO CARRIER FOUND
			{
				if(lp->Link[i] == 1)
				{
					printk(" EMAC: Port%d Link Down !\n", i);
					lp->Link[i] = 0;
					write_ar8xxx(0x60C, 0x80000205);			// AT_BUSY: Start and AT operation (cleared by HW). 
																// Flush all PC port2 entries in the dynamic address learning table
				}
			}
		}
	}

#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
	unsigned long btotal,utotal;
	static int protect_slots = 0;
	unsigned long flags;

	spin_lock_irqsave(&lp->filter_lock,flags);
	btotal = EMAC_MMC_RXBROADCASTFRAMES_G_REG;
	utotal = EMAC_MMC_RXUNICASTFRAMES_G_REG;
	lp->filter_config.ups = lp->tmp_ups * 2;
	lp->filter_config.bps = lp->tmp_bps * 2;
	if(lp->filter_config.ulp_enable) {
		if(utotal > lp->filter_config.ulp_threshold){
			EMAC_MACR1_FRAME_FILTER_REG |= (0x1 << 9);
			lp->filter_config.ulp_protect = 1;
		}
		else {
			EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 9);
			lp->filter_config.ulp_protect = 0;
		}
		lp->filter_config.ulp_filtered = utotal - lp->filter_config.ups;
	}
	if(lp->filter_config.blp_enable) {
		if(lp->filter_config.blp_protect == 0) {
			if(btotal > lp->filter_config.blp_threshold) {
				EMAC_MACR1_FRAME_FILTER_REG |= (0x1 << 5);
				lp->filter_config.blp_protect = 1;
				protect_slots = 8;
			}
		}
		else {
			protect_slots --;
			if(protect_slots == 0) {
				EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 5);
				lp->filter_config.blp_protect = 0;
			}
		}
		lp->filter_config.blp_filtered = btotal - lp->filter_config.bps;
	}
	lp->tmp_ups = 0;
	lp->tmp_bps = 0;

	spin_unlock_irqrestore(&lp->filter_lock,flags);
#endif

	lp->emac_timer.expires = jiffies + TMOUT;
	add_timer(&lp->emac_timer);
}

static void emac_rx_tasklet_fn(unsigned long data)
{
  struct net_device *dev = (struct net_device *)data;
  struct net_local *lp = (struct net_local *) dev->priv;
	unsigned long status;
	int cur_rx_pos;
	unsigned long des0;
	unsigned short frame_len, Packet_Count, i;
	struct sk_buff *skb, *new_skb;
	unsigned short id = 0;
	char tmpbuf [12];

	status = GetDword (EMAC_DMAR5_STATUS_REG);

	Packet_Count = 0;
	cur_rx_pos = lp->CUR_RX_DESC;
	des0 = lp->RX_DESCS[cur_rx_pos].DES0;
	while(((des0 & 0x80000000) == 0) && (Packet_Count < 4))
	{
		Packet_Count ++;
		if(des0 & (0x1 << 15)) {
			lp->stats[0].rx_errors++;
			if(des0 & (0x1 << 1))
				lp->stats[0].rx_crc_errors++;
		}
		else
		{ /* GET THE PACKET AND forward it to the system */

			frame_len = ((des0 >> 16) & 0x3fff) - 8;

			flush_dcache_range(lp->RX_DESCS[cur_rx_pos].DES2, lp->RX_DESCS[cur_rx_pos].DES2+frame_len + 8);

			id = 1 - (lp->RX_RING[cur_rx_pos]->data[13] & 0x1);

		 	if((lp->mode == MODE_SWITCH) || (id > 1))
		 		id = 0;

			dev = lp->devices[id];

			skb = lp->RX_RING[cur_rx_pos];
			skb->dev = dev;
			if( (((des0 >> 5) & 0x1) == 1) && (((des0 >> 7) & 0x1) == 0) && (((des0) & 0x1) == 0))
				skb->ip_summed = CHECKSUM_UNNECESSARY;
			else
				skb->ip_summed = CHECKSUM_NONE;

#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
			if(skb->data[0] & 0x1)
				lp->tmp_bps ++;
			else
				lp->tmp_ups ++;
#endif

			skb_put(skb, frame_len+2);

			memcpy (tmpbuf, lp->RX_RING[cur_rx_pos]->data, 12);
			memcpy ((lp->RX_RING[cur_rx_pos]->data) + 2, tmpbuf, 12);

			skb_pull(skb, 2);

			skb->protocol = eth_type_trans(skb,dev);

			if(netif_rx(skb) == NET_RX_DROP)
				lp->stats[id].rx_dropped ++;
			lp->stats[id].rx_packets++;
      lp->stats[id].rx_bytes+= frame_len;
		}
		cur_rx_pos = (cur_rx_pos + 1) % MAX_RX_DESCS;
		des0 = lp->RX_DESCS[cur_rx_pos].DES0;
	}
	i = lp->CUR_RX_DESC;

	while(Packet_Count)
	{
		Packet_Count --;
		des0 = lp->RX_DESCS[i].DES0;
		if(des0 & 0x1<<15)
		{
		 	lp->RX_DESCS[i].DES0 = 0x80000000;
		}
		else
		{
			new_skb = dev_alloc_skb(MAX_PACKET_LEN);
			if(new_skb == NULL)
				printk("No memory for new SKB!\n");
			skb_reserve(new_skb, 4);
			lp->RX_RING[i] = new_skb;
			lp->RX_DESCS[i].DES2 = (volatile unsigned long int) new_skb->data;
     	lp->RX_DESCS[i].DES0 = 0x80000000; /* DMA OWNED... */
		}
		i = (i+1) % MAX_RX_DESCS;
	}
	lp->CUR_RX_DESC = cur_rx_pos;
}

static irqreturn_t emac_interrupt(int irq, void *dev_id, struct pt_regs * regs)
{
	struct net_device *dev = NULL;
	struct net_local *lp= dev->priv;
	unsigned long status;
	volatile unsigned short rx_over = 0;

	dev = (struct net_device *)dev_id;
	lp = (struct net_local *)dev->priv;

	status = GetDword(EMAC_DMAR5_STATUS_REG);

	if (status & 0x40)
	{
		EMAC_DMAR5_STATUS_REG = 0x40;
	}

	if (status & 0x10)
	{
		EMAC_DMAR5_STATUS_REG = 0x10;
		rx_over = 1;
	}

	if (status & 0x5/*0x1*/)
	{
		EMAC_DMAR5_STATUS_REG = 0x01;
	}

	EMAC_DMAR5_STATUS_REG = 0x1e7ae;

	if(status & GMI)
	{
		printk("EMAC counter interrupt\n");
	}

	if (status & (RU|RXINT|OVF))
	{
		tasklet_schedule(&lp->emac_rx_tasklet);
	}

	SetWord(RESET_INT_PENDING_REG,EMAC_INT_PEND);

	return IRQ_HANDLED;
}

static int emac_xmit(struct sk_buff *skb, struct net_device *dev)
{
	struct net_local *lp = (struct net_local *) dev->priv;
	short length = 0;
	unsigned long status, des0;
	unsigned long flags;
	unsigned short id, id_1;

  if(skb == NULL) {
		printk("xmit with empty skb\n");
		return 0;
  }
  if(netif_queue_stopped(dev)) {
		printk("xmit with stopped queue\n");
		return -EBUSY;
  }

	if((dev->name)[3] == '0') {
		id = PORT_0;
		id_1 = 0x8;
	}
	else {
		id = PORT_1;
		id_1 = 0x4;
	}

  if(lp->tx_ring_size == MAX_TX_DESCS)
  {
		status = -EBUSY;
  }
	else  //proceed with transmission...
	{
		char tmpbuf[12];

		if(lp->TX_RING[lp->TX_RING_HEAD] != NULL)
			printk("ERROR in TX: RING BUFFER HEAD NOT NULL\n");

		memcpy(tmpbuf, skb->data, 12);
		skb_push(skb, 2);
		memcpy(skb->data, tmpbuf, 12);

#ifdef CONFIG_SC14452_ATHEROS_AR8314
		if (lp->mode == MODE_SWITCH) {
			skb->data[12] = 0x80;
			skb->data[13] = 0xe0;
		}
		else {
			skb->data[12] = 0x80;
			skb->data[13] = 0x63 - id;
		}
#elif defined CONFIG_SC14452_ATHEROS_AR8325
		//TODO
		if (lp->mode == MODE_SWITCH) {
			skb->data[12] = 0x80;
			skb->data[13] = 0x8C;
		}
		else {
			skb->data[12] = 0x80;
			skb->data[13] = 0x80 | id_1;
		}

#elif defined CONFIG_SC14452_ATHEROS_AR8236
		if (lp->mode == MODE_SWITCH) {
			skb->data[12]= 0x80;
			skb->data[13]= 0x94;
		}
		else {
			// TBA
			//skb->data[12]= 0x80;
			//skb->data[13]= 0x63 - id;
			;
		}
#endif

		length = ETH_ZLEN < skb->len ? skb->len : ETH_ZLEN;

		lp->TX_RING[lp->TX_RING_HEAD] = skb;
		spin_lock_irqsave(&lp->lock, flags);
		lp->TX_DESCS[lp->TX_RING_HEAD].DES2 = (volatile unsigned long int) skb->data;
		lp->TX_DESCS[lp->TX_RING_HEAD].DES1 |= DWORD_SHIFT(0x1, 30) | DWORD_SHIFT(0x1, 29) | (length);
		lp->TX_DESCS[lp->TX_RING_HEAD].DES0 = 0x80000000;

		SetDword(EMAC_DMAR1_TX_POLL_DEMAND_REG, 0);

		spin_unlock_irqrestore(&lp->lock, flags);
		// advance tx_head
		lp->TX_RING_HEAD = (lp->TX_RING_HEAD + 1) % MAX_TX_DESCS;
		lp->tx_ring_size += 1;
		lp->max_tx_packets ++;
	}

	if((!(lp->max_tx_packets % 4)) || (lp->tx_ring_size == MAX_TX_DESCS))
	{
		while (lp->tx_ring_size && ((lp->TX_DESCS[lp->TX_RING_TAIL].DES0 & 0x80000000) == 0))
		{
			id = 1 - (((lp->TX_RING[lp->TX_RING_TAIL])->data[13]) & 0x1);

 		 	if((lp->mode == MODE_SWITCH) || (id > 1))
				id = 0;

			des0 = lp->TX_DESCS[lp->TX_RING_TAIL].DES0;
			if(des0 & 1 << 15) {
				lp->stats[id].tx_errors ++;
				if(des0 & (1<<10))
					lp->stats[id].tx_carrier_errors ++;
			}
			lp->stats[id].tx_packets ++;
			lp->stats[id].tx_bytes += length;
			lp->TX_DESCS[lp->TX_RING_TAIL].DES2 = 0;
			lp->TX_DESCS[lp->TX_RING_TAIL].DES1 &= DWORD_SHIFT(0x1, 25);
			dev_kfree_skb(lp->TX_RING[lp->TX_RING_TAIL]);
			lp->TX_RING[lp->TX_RING_TAIL] = NULL;

			lp->tx_ring_size --;
			lp->TX_RING_TAIL = (lp->TX_RING_TAIL +1) % MAX_TX_DESCS;
		}
	}
	return 0;
}

static struct net_device_stats *emac_get_stats(struct net_device *dev)
{
	struct net_local *lp = (struct net_local *)dev->priv;

	unsigned short id = 0;

	if((dev->name)[3] == '0')
		id = PORT_0;
	else
		id = PORT_1;

	lp->stats[id].multicast += EMAC_MMC_RXMULTICASTFRAMES_G_REG;
	lp->stats[id].collisions += EMAC_MMC_TXSINGLECOL_G_REG + EMAC_MMC_TXMULTICOL_G_REG;
	lp->stats[id].rx_length_errors += EMAC_MMC_RXUNDERSIZE_G_REG + EMAC_MMC_RXOVERSIZE_G_REG;
	lp->stats[id].rx_fifo_errors += EMAC_MMC_RXFIFOOVERFLOW_REG;
	lp->stats[id].tx_fifo_errors += EMAC_MMC_TXUNDERFLOWERROR_REG;
	return &(lp->stats[id]);
}

static void emac_set_multicast_list(struct net_device *dev)
{
}
/*******************************************************************************************************************/
