// File created by Gigaset Elements GmbH
// All rights reserved.
/* 
	==================================================================================
    ax88796b.c: A SAMSUNG S3C2440 Linux2.6.x Ethernet device driver for ASIX AX88796B chips.
 
    This program is free software; you can distrine_block_inputbute it and/or modify it under
    the terms of the GNU General Public License (Version 2) as published by the Free Software
    Foundation.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY 
	WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
	PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston MA 02111-1307, USA.

	This program is based on

	ne.c:		A general non-shared-memory NS8390 ethernet driver for linux
				Written 1992-94 by Donald Becker.

	8390.c:     A general NS8390 ethernet driver core for linux.
				Written 1992-94 by Donald Becker.

	Version history:

		12/09/2006	1.0.0.0 - Initial release.


  
	==================================================================================
	Driver Overview
	==================================================================================
	ASIX AX88796B 3-in-1 Local Bus 8/16-bit Fast Ethernet Linux Driver

	The AX88796B Ethernet controller is a high performance and highly integrated
	local CPU bus Ethernet controllers with embedded 10/100Mbps	PHY/Transceiver
	16K bytes SRAM and supports both 8-bit and 16-bit local CPU interfaces for any 
	embedded systems. 

	If you look for more details, please visit ASIX's web site (http://www.asix.com.tw).


	==================================================================================
	COMPILING DRIVER
	==================================================================================
	Prepare: 

		AX88796B Linux Driver.
		Linux Kernel source code.
		Cross-Compiler.

	Getting Start:

		1.Extracting the AX88796B source file by executing the following command.
			[root@localhost]# tar jxvf ax88796b-arm-linux2.4.tar.bz2

		2.Edit the makefile to specifie the path of target platform Linux Kernel source.
		  EX:
				KDIR	:= /work/linux-2.6.17.11

		3.Executing 'make' command to compiler AX88796B Driver.

		4.If the compilation well, the ax88796.ko will be created under the current directory.


	==================================================================================
	DRIVER PARAMETERS
	==================================================================================
	The following parameters can be set when using insmod.
	EX: [root@localhost ax88796b]# insmod  ax88796.ko  mem=0x08000000

	mem=0xNNNNNNNN 
		specifies the physical base address that AX88796B can be accessed.
		Default value '0x08000000'.

	irq=N
		specifies the irq number. Default value '0x27'.

	media=(0=auto, 1=100full, 2=100half, 3=10full, 4=10half)
		Media mode control. Default value 'auto'.		
*/

static const char version1[] = "AX88796B: v1.1.0\n";
static const char version2[] = "AX88796B: 09/20/06 (http://www.asix.com.tw)\n";

#define	DRV_NAME	"AX88796B"

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/types.h>
#include <linux/string.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/crc32.h>

#if defined(CONFIG_SC14450_ASIX_LOW_POWER)
#include <linux/inetdevice.h>
#endif

#include <asm/system.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/irq.h>

#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
#include <linux/if_vlan.h>
#endif

#include "ax88796b.h"


#if defined(CONFIG_SC14450_ASIX_LOW_POWER)

/*------ power saving -------- */
#define AX_IDLE_TICS_MAX 15
#define RL_SPEED100 0
#define RL_SPEED10  1

unsigned int ax_idle_tics=0; // 0...
unsigned int ax_pwr_status=0; // 0,1,2
unsigned int ax_power_up_cmd=0;
static unsigned int ax_reg_write(unsigned int , unsigned int , unsigned int , unsigned int );
static u8 ax_reg_read(unsigned int , unsigned int , unsigned int);
static void ax_wol_init(struct net_device *);

void         rl_smi_init(void);
unsigned int rl_p0_status=0; // 0,1
unsigned int rl_p1_status=0; // 0,1
unsigned int rl_cmd_delay=0;
static void  rl_reg_write(int, int , int );
static u_int rl_reg_read(int , int , int );
static void  rl_set_speed(int , int );
static u_int rl_port_status(int );

void asix_wol_init(struct net_device *);
u16 ax_state=0; 
u16 ax_state_prev=0;

#endif

/* ---- No user-serviceable parts below ---- */
static int ax_probe(struct net_device *dev);
static int ax_open(struct net_device *dev);
static int ax_close(struct net_device *dev);
static void ax_reset(struct net_device *dev);
static void ax_get_hdr(struct net_device *dev, struct ax_pkt_hdr *hdr, int ring_page);
static void ax_block_input(struct net_device *dev, int count, struct sk_buff *skb, int ring_offset);
static void ax_block_output(struct net_device *dev, const int count, const unsigned char *buf, const int start_page);
static int ethdev_init(struct net_device *dev);
static void ax_init(struct net_device *dev, int startp);
static irqreturn_t ax_interrupt(int irq, void *dev_id);
static void ax_tx_intr(struct net_device *dev);
static void ax_tx_err(struct net_device *dev);
static void ax_receive(struct net_device *dev);
static void ax_rx_overrun(struct net_device *dev);
static void ax_trigger_send(struct net_device *dev, unsigned int length, int start_page);
static void set_multicast_list(struct net_device *dev);
static void do_set_multicast_list(struct net_device *dev);
static void ax_watchdog(unsigned long arg);

#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
static void ax_vlan_rx_register(struct net_device *dev, struct vlan_group *grp);
static void ax_vlan_rx_kill_vid(struct net_device *dev, unsigned short vid);
#endif

static int mdio_read(struct net_device *dev, int phy_id, int loc);
static void mdio_write(struct net_device *dev, int phy_id, int loc, int value);

// KH  #define	PRINTK(flag, args...) if (flag & DEBUG_FLAGS) printk(args)
#define	PRINTK(flag, args...)  printk(args)

#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
#error
#define AX88796_VLAN_TAG_USED 1
#else
#define AX88796_VLAN_TAG_USED 0
#endif

#undef  DEBUG_GPIO	
//#define DEBUG_GPIO	

#ifdef DEBUG_GPIO
#define SET_GPIO	P2_SET_DATA_REG	= (1<<10)
#define CLEAR_GPIO 	P2_RESET_DATA_REG = (1<<10)
#endif
#define USE_MEMCPY	0    ////////////////KH: TOCHECK


#define INT6_CTRL	(0x0007)
#define INT7_CTRL	(0x0038)
#define INT8_CTRL	(0x01C0)
#define KEYB_INT_PRIO	(0x0070)

#ifdef CONFIG_SC14450_VoIP_RevB
# define ETH_INT	4
#elif defined CONFIG_450DEMO
# define ETH_INT	2
#endif

static unsigned int media = 0;

#ifdef MODULE

static struct net_device dev_ax;
static int mem;
static int irq;

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
MODULE_PARM(mem, "i");
MODULE_PARM(irq, "i");
MODULE_PARM(media, "i");
#else
module_param(mem, int, 0);
module_param(irq, int, 0);
module_param(media, int, 0);
#endif

MODULE_PARM_DESC(mem, "MEMORY base address(es),required");
MODULE_PARM_DESC(irq, "IRQ number(s)");
MODULE_PARM_DESC(media, "Media Mode(0=auto, 1=100full, 2=100half, 3=10full, 4=10half)");

MODULE_DESCRIPTION("ASIX AX88796B Fast Ethernet driver");
MODULE_LICENSE("GPL");


/*
 * ----------------------------------------------------------------------------
 * Function Name: init_module
 * Purpose: 
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
int init_module(void)
{
	struct net_device *dev = &dev_ax;

	dev->irq = irq;
	dev->base_addr = mem;
	dev->init = ax_probe;
	strncpy( dev->name, "eth%d", sizeof( dev->name ) - 1 ) ;
	if(register_netdev(dev) == 0)
		return 0;

	if (mem != 0) {
		PRINTK(WARNING_MSG, "AX88796B: No AX88796B card found at memory = %#x\n", mem);
	}
	else {
		PRINTK(WARNING_MSG, "AX88796B: You must supply \"mem=0xNNNNNNN\" value(s) for AX88796B.\n");
	}
	return -ENXIO;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: cleanup_module
 * Purpose: 
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void cleanup_module(void)
{
	struct net_device *dev = &dev_ax;
	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;

	free_irq(dev->irq, dev);
	iounmap(ax_base);
	release_mem_region( (unsigned long)ax_base, NE_IO_EXTENT ) ;
	unregister_netdev(dev);
	kfree(dev->priv);
	dev->priv = NULL;
}
#endif /* MODULE */


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_probe
 * Purpose: 
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static int ax_probe(struct net_device *dev)
{
	int i;
	unsigned char SA_prom[32];
#if 0
	int reg0, wordlength = 0;
#endif
	int start_page, stop_page;
	int ret;
	unsigned long base_addr;
	void	*address;
	struct ax_device *ax_local;
	char * tmp;

	PRINTK(INIT_MSG,  "AX88796B: probe start ..........\n");

	SET_MODULE_OWNER(dev);

	if(dev->base_addr == 0 )
		dev->base_addr = AX88796B_BASE;

	base_addr = dev->base_addr;

	//AI
	//if (check_mem_region(base_addr , NE_IO_EXTENT))
	//	return -ENODEV;
	if (! request_mem_region(base_addr, NE_IO_EXTENT, DRV_NAME))
		return -EBUSY;
	address=ioremap_nocache(base_addr, NE_IO_EXTENT);
	//KH
//	if(address != base_addr)
//		printk("address != base_address!!\n");
//	else
//		printk("DEVICE address = 0x%x\n",address);
	if (!address) {
		PRINTK(ERROR_MSG, "AX88796B:Unable to remap memory\n");
		return -EBUSY;
	}

#if 0
	reg0 = readb(address);
	if (reg0 == 0xFF) {
		printk("ETH: reg0 != 0xFF (%d)\n",reg0);
		ret = -ENODEV;
		goto err_out;
	}
	/* Do a preliminary verification that we have a 8390. */
	{
		int regd;
		writeb(E8390_NODMA+E8390_PAGE1+E8390_STOP, address + E8390_CMD);
		regd = readb(address + EN0_COUNTER0);
		writeb(0xff, address + EN0_COUNTER0);
		writeb(E8390_NODMA+E8390_PAGE0, address + E8390_CMD);
		readb(address + EN0_COUNTER0); /* Clear the counter by reading. */
		if (readb(address + EN0_COUNTER0) != 0) {
			printk("ETH: Verification of 8390 Failed\n");
			writeb(reg0, address);
			writeb(regd, address + EN0_COUNTER0);	/* Restore the old values. */
			ret = -ENODEV;
			goto err_out;
		}
	}
#endif

	PRINTK(DRIVER_MSG, "%s", version1);
	PRINTK(DRIVER_MSG, "%s", version2);

	/* Reset card. Who knows what dain-bramaged state it was left in. */
	{
		unsigned long reset_start_time = jiffies;

		/* DON'T change these to readb/writeb or reset will fail on clones. */
		writeb(readb(address + EN0_RESET),(address + EN0_RESET));
		
		while ((readb(address + EN0_ISR) & ENISR_RESET) == 0)  {
			if (jiffies - reset_start_time > 4*HZ/100) {
					PRINTK(ERROR_MSG, " not found (no reset ack).\n");
					ret = -ENODEV;
					goto err_out;
			}
		}
		writeb(0xff,(address + EN0_ISR));		/* Ack all intr. */
//		printk("ETH: CARD RESET!\n");
	}
#if 0
	/* Read the 16 bytes of station address PROM. */
	{
		struct {unsigned char value, offset; } program_seq[] =
		{
			{E8390_NODMA+E8390_PAGE0+E8390_STOP, E8390_CMD}, /* Select page 0*/
			{0x48,	EN0_DCFG},	/* Set byte-wide (0x48) access. */
			{0x00,	EN0_RCNTLO},	/* Clear the count regs. */
			{0x00,	EN0_RCNTHI},
			{0x00,	EN0_IMR},	/* Mask completion irq. */
			{0xFF,	EN0_ISR},
			{E8390_RXOFF, EN0_RXCR},	/* 0x20  Set to monitor */
			{E8390_TXOFF, EN0_TXCR},	/* 0x02  and loopback mode. */
			{32,	EN0_RCNTLO},
			{0x00,	EN0_RCNTHI},
			{0x00,	EN0_RSARLO},	/* DMA starting at 0x0000. */
			{0x00,	EN0_RSARHI},
			{E8390_RREAD+E8390_START, E8390_CMD},
		};

		for (i = 0; i < sizeof(program_seq)/sizeof(program_seq[0]); i++)
			writeb(program_seq[i].value, address + program_seq[i].offset);

		printk("ETH: PROM READ!\n");
	}

	for(i = 0; i < 32; i+=2) {
		SA_prom[i] = readb(address + EN0_DATAPORT);
		SA_prom[i+1] = readb(address + EN0_DATAPORT);
		if (SA_prom[i] != SA_prom[i+1])
			wordlength = 1;
	}

	for (i = 0; i < 16; i++)
		SA_prom[i] = SA_prom[i+i];
#endif
	writeb(0x49,(address + EN0_DCFG));
	start_page = NESM_START_PG;
	stop_page = NESM_STOP_PG;

	if(dev->irq == 0)
		dev->irq =KEYB_INT;

	/* Allocate dev->priv and fill in 8390 specific dev fields. */
	if (ethdev_init(dev))
	{
        	PRINTK (ERROR_MSG, "unable to get memory for dev->priv.\n");
        	ret = -ENOMEM;
		goto err_out;
	}

	ax_local = (struct ax_device *)dev->priv;

        tmp = strstr(saved_command_line,"ethaddr=");
        if(tmp)
        {
                sscanf(tmp,"ethaddr=%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
				&SA_prom[0],&SA_prom[1],&SA_prom[2],
				&SA_prom[3],&SA_prom[4],&SA_prom[5]);
        }
        else/* Support No EEPROM */ 
	{
		SA_prom[0] = 0x00;
		SA_prom[1] = 0x88;
		SA_prom[2] = 0x88;
		SA_prom[3] = 0x77;
		SA_prom[4] = 0x99;
		SA_prom[5] = 0x66;
	}

	PRINTK(DRIVER_MSG, "%s: MAC ADDRESS ",DRV_NAME);
	for(i = 0; i < ETHER_ADDR_LEN; i++) {
		PRINTK(DRIVER_MSG, " %2.2x", SA_prom[i]);
		dev->dev_addr[i] = SA_prom[i];
	}

	PRINTK(DRIVER_MSG, "\n%s: %s found at 0x%x, using IRQ %d.\n",
		dev->name, DRV_NAME, AX88796B_BASE, dev->irq);

	ax_local->name = DRV_NAME;
	ax_local->membase = address;
	ax_local->tx_start_page = NESM_START_PG;
	ax_local->rx_start_page = NESM_RX_START_PG;
	ax_local->stop_page = NESM_STOP_PG;

	ax_local->media = media;

	dev->open = &ax_open;
	dev->stop = &ax_close;

#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
	dev->features |= NETIF_F_HW_VLAN_TX | NETIF_F_HW_VLAN_RX;
	dev->vlan_rx_register = ax_vlan_rx_register;
	dev->vlan_rx_kill_vid = ax_vlan_rx_kill_vid;
#endif
	ax_init(dev, 0);
#ifndef MODULE
 	if(register_netdev(dev) == 0){
		printk("CANNOT REGISTER ETHERNET DEVICE....\n");
                return 0;
	}
#endif

	PRINTK(INIT_MSG,  "AX88796B: probe end ..........\n");
	return 0;

err_out:
	printk("ETH DEVICE: at ERR_OUT!!\n");
	iounmap(address);
	kfree(dev->priv);
	dev->priv = NULL;
	return ret;
}


#ifndef MODULE
/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_kprobe
 * Purpose: 
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
struct net_device * __init ax_kprobe(int unit)
{
	struct net_device *dev = alloc_ei_netdev();
	int err;

	if (!dev)
		return ERR_PTR(-ENOMEM);

	sprintf(dev->name, "eth%d", unit);
	printk("ETH: Probing device %s\n",dev->name);
	netdev_boot_setup_check(dev);

	err = ax_probe(dev);
	if (err)
		goto out;
	return dev;
out:
	printk("Device %s not OK\n",dev->name);
	free_netdev(dev);
	return ERR_PTR(err);
}
#endif


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_reset
 * Purpose: 
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void ax_reset(struct net_device *dev)
{
    struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;
	unsigned long reset_start_time = jiffies;

	/* DON'T change these to readb/writeb or reset will fail on clones. */
	writeb(readb(ax_base + EN0_RESET), ax_base + EN0_RESET);

	ax_local->dmaing = 0;

	/* This check _should_not_ be necessary, omit eventually. */
	while ((readb(ax_base+EN0_ISR) & ENISR_RESET) == 0)
		if (jiffies - reset_start_time > 2*HZ/100) {
			PRINTK(ERROR_MSG, "%s: ax_reset() did not complete.\n", dev->name);
			break;
		}
	writeb(ENISR_RESET, ax_base + EN0_ISR);	/* Ack intr. */
}



/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_get_hdr
 * Purpose: Grab the 796b specific header
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void ax_get_hdr(struct net_device *dev, struct ax_pkt_hdr *hdr, int ring_page)
{
    struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;
	u16 tmp;

	/* This *shouldn't* happen. If it does, it's the last thing you'll see */

	if (ax_local->dmaing)
	{
		PRINTK(ERROR_MSG, "%s: DMAing conflict in ne_get_8390_hdr "
			"[DMAstat:%d][irqlock:%d].\n",
			dev->name, ax_local->dmaing, ax_local->irqlock);
		return;
	}

	ax_local->dmaing |= 0x01;
	writeb(E8390_NODMA+E8390_PAGE0+E8390_START, ax_base+ E8390_CMD);
	writeb(sizeof(struct ax_pkt_hdr), ax_base + EN0_RCNTLO);
	writeb(0, ax_base + EN0_RCNTHI);
	writeb(0, ax_base + EN0_RSARLO);		/* On page boundary */
	writeb(ring_page, ax_base + EN0_RSARHI);
	writeb(E8390_RREAD+E8390_START, ax_base + E8390_CMD);

	while (( readb(ax_base+EN0_SR) & 0x20) ==0);

	for(tmp=0; tmp < (sizeof(struct ax_pkt_hdr)>>1); tmp++)
		*((u16 *)hdr + tmp)= readw(ax_base+EN0_DATAPORT);	

	writeb(ENISR_RDC, ax_base + EN0_ISR);	/* Ack intr. */
	ax_local->dmaing = 0;

	le16_to_cpus(&hdr->count);

}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_block_input
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void ax_block_input(struct net_device *dev, int count, struct sk_buff *skb, int ring_offset)
{
    struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;
	u8 *vlan_buf;

	/* This *shouldn't* happen. If it does, it's the last thing you'll see */
	if (ax_local->dmaing)
	{
		PRINTK(ERROR_MSG, "%s: DMAing conflict in ne_block_input "
			"[DMAstat:%d][irqlock:%d].\n",
			dev->name, ax_local->dmaing, ax_local->irqlock);
		return;
	}

	ax_local->dmaing |= 0x01;
	writeb(E8390_NODMA+E8390_PAGE0+E8390_START, ax_base+ E8390_CMD);
	writeb(count & 0xff, ax_base + EN0_RCNTLO);
	writeb(count >> 8, ax_base + EN0_RCNTHI);
	writeb(ring_offset & 0xff, ax_base + EN0_RSARLO);
	writeb(ring_offset >> 8, ax_base + EN0_RSARHI);
	writeb(E8390_RREAD+E8390_START, ax_base + E8390_CMD);

	while (( readb(ax_base+EN0_SR) & 0x20) ==0);

	vlan_buf = ax_local->packet_buf;

#if (USE_MEMCPY == 1)
	{
		u16	pkt_len_align;
		if ( ((count % 4)) != 0 )
			pkt_len_align = count + 4 - (count % 4);
		else
			pkt_len_align = count;
			int myi;
		for(myi=0;myi<pkt_len_align;myi++)
			*(vlan_buf+myi)=*((volatile unsigned char *)(ax_base)+0x20);	
//		memcpy(vlan_buf, (ax_base+EN0_DATA_ADDR), pkt_len_align);
//			printk("\n:%d",pkt_len_align);
//			for(myi=0;myi<36;myi++)
//				printk("%x ",vlan_buf[myi]);
//			printk("\n");
	
#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
#error
		if(ax_local->vlgrp) {
			memcpy((void *)(skb->data), vlan_buf, 12);
			memcpy((void *)(skb->data+12), vlan_buf, (count-4-12));
		}
		else
#endif
			memcpy((void *)(skb->data), vlan_buf, count);
	}
#else
	{
		u8 *buf = skb->data;
		u16 i,tmp;
		tmp = count >> 1;
		if(count%2){
			tmp++;
//		printk("skb->len: %d ",skb->len);
		}

#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
#error
		if(ax_local->vlgrp) {
			for(i=0; i < tmp; i++) {
				*((u16 *)vlan_buf + i) = readw(ax_base+EN0_DATA_ADDR);
			}
			if (count & 0x01)
				vlan_buf[count-1] = readb(ax_base + EN0_DATA_ADDR);
			memcpy((void *)(skb->data), vlan_buf, 12);
			memcpy((void *)(skb->data+12), vlan_buf, (count-4-12));
		}
		else
	#endif
		{
			for(i=0; i < tmp; i++) {
				*((u16 *)buf + i) = readw(ax_base+EN0_DATAPORT);
			}
//			if(count%2){
//				printk("last byte: 0x%x 0x%x\n",buf[count-1],buf[count]);
			//	buf[count-1] = buf[count];
//			}
		//	if (count & 0x01){
		//		printk(".");
		//		buf[count-1] = readb(ax_base + EN0_DATAPORT);
		//	}
		}
	}
#endif

	writeb(ENISR_RDC, ax_base + EN0_ISR);	/* Ack intr. */
	ax_local->dmaing = 0;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_block_output
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void ax_block_output(struct net_device *dev, int count, const unsigned char *buf, const int start_page)
{
    struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;
	unsigned long dma_start;

	/* This *shouldn't* happen. If it does, it's the last thing you'll see */
	if (ax_local->dmaing)
	{
		PRINTK(ERROR_MSG, "%s: DMAing conflict in ne_block_output."
			"[DMAstat:%d][irqlock:%d]\n",
			dev->name, ax_local->dmaing, ax_local->irqlock);
		return;
	}

	ax_local->dmaing |= 0x01;
	/* We should already be in page 0, but to be safe... */
	writeb(E8390_PAGE0+E8390_START+E8390_NODMA, ax_base + E8390_CMD);
	writeb(ENISR_RDC, ax_base + EN0_ISR);
	/* Now the normal output. */
	writeb(count & 0xff, ax_base + EN0_RCNTLO);
	writeb(count >> 8,   ax_base + EN0_RCNTHI);
	writeb(0x00, ax_base + EN0_RSARLO);
	writeb(start_page, ax_base + EN0_RSARHI);
	writeb(E8390_RWRITE+E8390_START, ax_base + E8390_CMD);

#if (USE_MEMCPY == 1)
	{
		memcpy((ax_base+EN0_DATA_ADDR), buf, (count+count%8));
	}
#else
	{
		u16 data, i;
//			if (count & 0x01)
//				printk(".");
		for (i=0; i<count; i+=2)
		{
			data = (u16)*(buf+i) + ( (u16)*(buf+i+1) << 8 );
			writew(data, ax_base + EN0_DATAPORT);
		}
	}
#endif
	dma_start = jiffies;
	while ((readb(ax_base + EN0_ISR) & 0x40) == 0) {
		if (jiffies - dma_start > 2*HZ/100) {		/* 20ms */
			PRINTK(ERROR_MSG, "%s: timeout waiting for Tx RDC.\n", dev->name);
			ax_reset(dev);
			ax_init(dev,1);
			break;
		}
	}
	writeb(ENISR_RDC, ax_base + EN0_ISR);	/* Ack intr. */
	ax_local->dmaing = 0;
	return;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_open
 * Purpose: Open/initialize 796b
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static int ax_open(struct net_device *dev)
{
	unsigned long flags;
	struct ax_device *ax_local = (struct ax_device *) dev->priv;
//	void *membase = ax_local->membase;
	int ret=0;

	PRINTK(DEBUG_MSG, "AX88796B: ax88796B ei_open beginning ..........\n");
//	PRINTK(DEBUG_MSG, "AX88796B: membase %p\n\r", membase);

	while( RESET_INT_PENDING_REG & 0x2 ) {
		KEY_STATUS_REG = ETH_INT ;
		RESET_INT_PENDING_REG = 0x2;
	}
	ax_local->bcast_dis = 0;
	ret = request_irq(dev->irq, &ax_interrupt, SA_SHIRQ, dev->name, dev);

	if (ret) {
		PRINTK (ERROR_MSG, "%s: unable to get IRQ %d (errno=%d).\n",dev->name, dev->irq, ret);
		return -ENXIO;
	}
//	PRINTK(DEBUG_MSG, "AX88796B: Request IRQ success !!\n\r");

	/* This can't happen unless somebody forgot to call ethdev_init(). */
	if (ax_local == NULL) 
	{
		PRINTK(ERROR_MSG, "%s: ei_open passed a non-existent device!\n", dev->name);
		return -ENXIO;
	}

	 ax_local->packet_buf = kmalloc(1536, GFP_KERNEL);
	 if(!ax_local->packet_buf)
		 return -ENOMEM;

	 memset(ax_local->packet_buf,0,1536);

	dev->tx_timeout = NULL;
	dev->watchdog_timeo = 0;

	init_timer(&ax_local->watchdog);

	ax_local->watchdog.function = &ax_watchdog;
	ax_local->watchdog.expires = jiffies + AX88796_WATCHDOG_PERIOD;
	ax_local->watchdog.data = (unsigned long) dev;
	add_timer(&ax_local->watchdog);
      
    spin_lock_irqsave(&ax_local->page_lock, flags);
	ax_reset(dev);
	ax_init(dev, 1);
	netif_start_queue(dev);
    spin_unlock_irqrestore(&ax_local->page_lock, flags);
	ax_local->irqlock = 0;
	PRINTK(DEBUG_MSG, "AX88796B: ax88796B ei_open end ..........\n");
#ifdef DEBUG_GPIO
	CLEAR_GPIO;
#endif
	return 0;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_close
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static int ax_close(struct net_device *dev)
{
	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	unsigned long flags;
 	PRINTK(DEBUG_MSG, "AX88796B: ax88796B ei_close beginning ..........\n");
   	spin_lock_irqsave(&ax_local->page_lock, flags);
	ax_init(dev, 0);

	free_irq(dev->irq, dev);
//	if(ax_local->watchdog != NULL)
		del_timer(&ax_local->watchdog);
   	spin_unlock_irqrestore(&ax_local->page_lock, flags);
	netif_stop_queue(dev);
	PRINTK(DEBUG_MSG, "AX88796B: ax88796B ei_close end ..........\n");
	return 0;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_start_xmit
 * Purpose: Add Vlan tab for packet transmission
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
static void add_vlan(struct net_device *dev, struct sk_buff *skb, int length)
{
	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	u8 *vlan_buf = ax_local->packet_buf;
	u16 vlan_tag = ax_local->vid;

	memcpy(&vlan_buf[0], skb->data, 12);
	vlan_buf[12]=0x81;
	vlan_buf[13]=0x00;
	vlan_buf[14]=(vlan_tag&0xf00)>>8;
	vlan_buf[15]=vlan_tag&0xff;
	memcpy(&vlan_buf[16],&skb->data[12],(length-12-4));
	return;
}
#endif


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_start_xmit
 * Purpose: begin packet transmission
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static int ax_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;
	int send_length;
	unsigned long flags;
	u8 ctepr=0, free_pages=0, need_pages;

	// printk("ETH: AT TRANSMIT!!!\n");

#if defined(CONFIG_SC14450_ASIX_LOW_POWER)
    u8 ax_status = 0x3 & ax_reg_read((unsigned int)ax_base, 3, 0xb);
    if (ax_status == 1) {
        ax_reg_write((unsigned int)ax_base, 3, 0xb, 0x00); // D1->D0
        printk("Exiting state D1\n");
        ax_idle_tics = 0;
    }
    else if (ax_status == 2) {
        ax_reg_write((unsigned int)ax_base, 3, 0xb, 0x00); // D1->D0
        ax_reg_write((unsigned int) ax_local->membase,3,0x1f,1); // exit from D2
        printk("Exiting state D2\n");
        ax_idle_tics = 0;
    }
#endif        

#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
	u8 *vlan_buf = ax_local->packet_buf;
	u16 vlan_tag = 0;
#endif

	/* check for link status */
	if (!(readb(ax_base + EN0_SR) & 0x01)) {
		dev_kfree_skb (skb);
		PRINTK(DEBUG_MSG, "drop transmit packet\n\r");
		return 0;
	}
//	if(skb->len % 2)
//		printk(".");

	send_length = ETH_ZLEN < skb->len ? skb->len : ETH_ZLEN;

#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
	if (vlan_tx_tag_present(skb)) {
		vlan_tag = vlan_tx_tag_get(skb);
		writeb((u8)(vlan_tag&0xff), ax_base + EN0_VID0);
		writeb((u8)((vlan_tag&0xf00)>>8), ax_base + EN0_VID1);
		ax_local->vid = vlan_tag;
	}
#endif
	spin_lock_irqsave(&ax_local->page_lock, flags);
		writeb(0x00, ax_base + EN0_IMR);
	spin_unlock_irqrestore(&ax_local->page_lock, flags);

	spin_lock(&ax_local->page_lock);
	ax_local->irqlock = 1;

	need_pages = (send_length -1)/256 +1;
	ctepr = readb(ax_base + EN0_CTEPR) & 0x7f;
		
	if(ctepr == 0) {
		if(ax_local->tx_curr_page == ax_local->tx_start_page && ax_local->tx_prev_ctepr == 0)
			free_pages = TX_PAGES;
		else
			free_pages = ax_local->tx_stop_page - ax_local->tx_curr_page;
	}
	else if(ctepr < ax_local->tx_curr_page - 1) {
		free_pages = ax_local->tx_stop_page - ax_local->tx_curr_page + 
					 ctepr - ax_local->tx_start_page + 1;
	}
	else if(ctepr > ax_local->tx_curr_page - 1) {
		free_pages = ctepr + 1 - ax_local->tx_curr_page;
	}
	else if(ctepr == ax_local->tx_curr_page - 1) {
		if(ax_local->tx_full)
			free_pages = 0;
		else
			free_pages = TX_PAGES;
	}		

	if(free_pages < need_pages) {
		PRINTK(DEBUG_MSG, "free_pages < need_pages\n\r");
		netif_stop_queue(dev);
		ax_local->tx_full = 1;
		ax_local->irqlock = 0;	
		spin_unlock(&ax_local->page_lock);
		spin_lock_irqsave(&ax_local->page_lock, flags);
		writeb(ENISR_ALL, ax_base + EN0_IMR);
		spin_unlock_irqrestore(&ax_local->page_lock, flags);
		return 1;
	}
#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
	if (vlan_tag) {
		send_length +=4;
		add_vlan(dev,skb, send_length);
		ax_block_output(dev, send_length, vlan_buf, ax_local->tx_curr_page);
	}
	else
#endif
	ax_block_output(dev, send_length, skb->data, ax_local->tx_curr_page);
	ax_trigger_send(dev, send_length, ax_local->tx_curr_page);
	if(free_pages == need_pages) {
		netif_stop_queue(dev);
		ax_local->tx_full = 1;
	}
	ax_local->tx_prev_ctepr = ctepr;
	ax_local->tx_curr_page = ax_local->tx_curr_page + need_pages < ax_local->tx_stop_page ? 
	ax_local->tx_curr_page + need_pages : 
	need_pages - (ax_local->tx_stop_page - ax_local->tx_curr_page) + ax_local->tx_start_page;

	dev_kfree_skb (skb);
	dev->trans_start = jiffies;
	ax_local->stat.tx_bytes += send_length;

	ax_local->irqlock = 0;	
	spin_unlock(&ax_local->page_lock);
	spin_lock_irqsave(&ax_local->page_lock, flags);
	writeb(ENISR_ALL, ax_base + EN0_IMR);
	spin_unlock_irqrestore(&ax_local->page_lock, flags);
	return 0;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_tx_intr
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void ax_tx_intr(struct net_device *dev)
{
    struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;
	int status = readb(ax_base + EN0_TSR);

	ax_local->tx_full = 0;
	if(netif_queue_stopped(dev))
		netif_wake_queue(dev);

	/* Minimize Tx latency: update the statistics after we restart TXing. */
	if (status & ENTSR_COL)
		ax_local->stat.collisions++;
	if (status & ENTSR_PTX)
		ax_local->stat.tx_packets++;
	else 
	{
		ax_local->stat.tx_errors++;
		if (status & ENTSR_ABT) 
		{
			ax_local->stat.tx_aborted_errors++;
			ax_local->stat.collisions += 16;
		}
		if (status & ENTSR_CRS) 
			ax_local->stat.tx_carrier_errors++;
		if (status & ENTSR_FU) 
			ax_local->stat.tx_fifo_errors++;
		if (status & ENTSR_CDH)
			ax_local->stat.tx_heartbeat_errors++;
		if (status & ENTSR_OWC)
			ax_local->stat.tx_window_errors++;
	}
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_interrupt
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
static void ax_interrupt(int irq, void *dev_id, struct pt_regs * regs)
#else
static irqreturn_t ax_interrupt(int irq, void *dev_id)
#endif
{
	struct net_device *dev = dev_id;
	int interrupts;
       struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;
	unsigned long flags;

	/* check if this interrupt is for us (irq line is being shared) */
	if( !( KEY_STATUS_REG & ETH_INT ) )
		return IRQ_NONE ;

#ifdef DEBUG_GPIO
	SET_GPIO;
#endif
	if (dev == NULL) 
	{
		PRINTK (ERROR_MSG, "net_interrupt(): irq %d for unknown device.\n", irq);
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
		return;
#else
		KEY_STATUS_REG = ETH_INT ;
		return 0;
#endif
	}

	writeb(E8390_NODMA+E8390_PAGE0, ax_base + E8390_CMD);

	spin_lock_irqsave(&ax_local->page_lock, flags);
		writeb(0x00, ax_base + EN0_IMR);
//		*(volatile unsigned short *) 0xFF49B6 = *(volatile unsigned short *) 0xFF49B6;
	spin_unlock_irqrestore(&ax_local->page_lock, flags);

	if (ax_local->irqlock) {
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
		return;
#else
	//	printk("ETH IRQ locked\n");
	
	RESET_INT_PENDING_REG = 0x2;
//	*(volatile unsigned short*)0xFF5402=0x2;
	//	*(volatile unsigned short *) 0xFF49B6 = *(volatile unsigned short *) 0xFF49B6;
		return 0;
#endif
	}

	spin_lock(&ax_local->page_lock);
	
	while (1)
	{

		if((interrupts = readb(ax_base + EN0_ISR)) == 0)
			break;
		writeb(interrupts, ax_base + EN0_ISR); /* Ack the interrupts */
		if (interrupts & ENISR_TX) 
        {
#if defined(CONFIG_SC14450_ASIX_LOW_POWER)
            ax_idle_tics = 0;
#endif		
        	ax_tx_intr(dev);
        }

		if (interrupts & ENISR_OVER)
			ax_rx_overrun(dev);
		
		if (interrupts & (ENISR_RX+ENISR_RX_ERR))
            ax_receive(dev);
        

		if (interrupts & ENISR_TX_ERR)
			ax_tx_err(dev);

		if (interrupts & ENISR_COUNTERS) 
		{   
			ax_local->stat.rx_frame_errors += readb(ax_base + EN0_COUNTER0);
			ax_local->stat.rx_crc_errors   += readb(ax_base + EN0_COUNTER1);
			ax_local->stat.rx_missed_errors+= readb(ax_base + EN0_COUNTER2); 
			writeb(ENISR_COUNTERS, ax_base + EN0_ISR); /* Ack intr. */
		}

		if (interrupts & ENISR_RDC)
			writeb(ENISR_RDC, ax_base + EN0_ISR);

		writeb(E8390_NODMA+E8390_PAGE0+E8390_START, ax_base + E8390_CMD);
	}

	spin_unlock(&ax_local->page_lock);
	spin_lock_irqsave(&ax_local->page_lock, flags);
		writeb(ENISR_ALL, ax_base + EN0_IMR);
	spin_unlock_irqrestore(&ax_local->page_lock, flags);

	writeb(E8390_NODMA+E8390_PAGE0+E8390_START, ax_base + E8390_CMD);
//	*(volatile unsigned short *) 0xFF49B6 = *(volatile unsigned short *) 0xFF49B6;
//	*(volatile unsigned short*)0xFF5402=0x2;
	RESET_INT_PENDING_REG = 0x2;

#ifdef DEBUG_GPIO
	CLEAR_GPIO;
#endif
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
	return;
#else
	//return IRQ_RETVAL(0);
	return IRQ_HANDLED;
#endif
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_tx_err
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void ax_tx_err(struct net_device *dev)
{
    struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;

	unsigned char txsr = readb(ax_base+EN0_TSR);
	unsigned char tx_was_aborted = txsr & (ENTSR_ABT+ENTSR_FU);

	if (tx_was_aborted)
		ax_tx_intr(dev);
	else 
	{
		ax_local->stat.tx_errors++;
		if (txsr & ENTSR_CRS) ax_local->stat.tx_carrier_errors++;
		if (txsr & ENTSR_CDH) ax_local->stat.tx_heartbeat_errors++;
		if (txsr & ENTSR_OWC) ax_local->stat.tx_window_errors++;
	}
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_receive
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void ax_receive(struct net_device *dev)
{
    struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;
	unsigned char rxing_page, this_frame, next_frame;
	unsigned short current_offset;
	struct ax_pkt_hdr rx_frame;
	int num_rx_pages = ax_local->stop_page - ax_local->rx_start_page;
	while(1)
	{
		int pkt_len, pkt_stat;

		/* Get the rx page (incoming packet pointer). */
		writeb(E8390_NODMA+E8390_PAGE1, ax_base + E8390_CMD);
		rxing_page = readb(ax_base + EN1_CURPAG);
		writeb(E8390_NODMA+E8390_PAGE0, ax_base + E8390_CMD);

		
		/* Remove one frame from the ring.  Boundary is always a page behind. */
		this_frame = readb(ax_base + EN0_BOUNDARY) + 1;
		if (this_frame >= ax_local->stop_page)
			this_frame = ax_local->rx_start_page;

		if (this_frame != ax_local->current_page && (this_frame!=0x0 || rxing_page!=0xFF))
			PRINTK(RX_MSG, "%s: mismatched read page pointers %2x vs %2x.\n",
				   dev->name, this_frame, ax_local->current_page);
		
		if (this_frame == rxing_page) {	/* Read all the frames? */
			break;				/* Done for now */
		}
		current_offset = this_frame << 8;
		ax_get_hdr(dev, &rx_frame, this_frame);
		
		pkt_len = rx_frame.count - sizeof(struct ax_pkt_hdr);
		pkt_stat = rx_frame.status;
		next_frame = this_frame + 1 + ((pkt_len+4)>>8);
		
		/* Check for bogosity warned by 3c503 book: the status byte is never
		   written.  This happened a lot during testing! This code should be
		   cleaned up someday. */
		if (rx_frame.next != next_frame
			&& rx_frame.next != next_frame + 1
			&& rx_frame.next != next_frame - num_rx_pages
			&& rx_frame.next != next_frame + 1 - num_rx_pages) {
			ax_local->current_page = rxing_page;
			writeb(ax_local->current_page-1, ax_base+EN0_BOUNDARY);
			ax_local->stat.rx_errors++;
			PRINTK(ERROR_MSG, "error occurred! Drop this packet!!\n");
			continue;
		}
//		printk("AT RX: len: %d\n",pkt_len);

		if (pkt_len < 60  ||  pkt_len > 1518)
		{
			PRINTK(RX_MSG, "%s: bogus packet size: %d, status=%#2x nxpg=%#2x.\n",
					   dev->name, rx_frame.count, rx_frame.status,
					   rx_frame.next);
			ax_local->stat.rx_errors++;
			ax_local->stat.rx_length_errors++;
		}
		else if ((pkt_stat & 0x0F) == ENRSR_RXOK) 
		{
			struct sk_buff *skb;
			skb = dev_alloc_skb(pkt_len+4);
			if (skb == NULL)
			{
				printk("%s: Couldn't allocate a sk_buff of size %d.\n", dev->name, pkt_len);
				ax_local->stat.rx_dropped++;
				break;
			}
			skb_reserve(skb,2);	/* IP headers on 16 byte boundaries */
			skb->dev = dev;
			//skb->ip_summed = CHECKSUM_UNNECESSARY;
			skb->ip_summed = CHECKSUM_NONE;
			skb_put(skb, pkt_len);	/* Make room */
			ax_block_input(dev, pkt_len, skb, current_offset + sizeof(rx_frame));

			if (skb->data[0] == 0xff)
				ax_local->bps++;
			if(ax_local->bps >=200)
			{
				if( ax_local->bcast_dis == 0){
					//printk("BCAST OFF!\n");
					writeb(E8390_RXCONFIG_NOBCAST, ax_base + EN0_RXCR);
					ax_local->bcast_dis = 1;
				}
			}
			skb->protocol=eth_type_trans(skb,dev);
#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
			if(ax_local->vlgrp)
				vlan_hwaccel_rx(skb, ax_local->vlgrp, ax_local->vid);
			else
#endif
				netif_rx(skb);
			
			dev->last_rx = jiffies;
			ax_local->stat.rx_packets++;
			ax_local->stat.rx_bytes += pkt_len;
			if (pkt_stat & ENRSR_PHY)
				ax_local->stat.multicast++;
		}
		else 
		{
			PRINTK(ERROR_MSG, "%s: bogus packet: status=%#2x nxpg=%#2x size=%d\n",
					   dev->name, rx_frame.status, rx_frame.next, rx_frame.count);
			ax_local->stat.rx_errors++;
			/* NB: The NIC counts CRC, frame and missed errors. */
			if (pkt_stat & ENRSR_FO)
				ax_local->stat.rx_fifo_errors++;
		}
		next_frame = rx_frame.next;
		
		/* This _should_ never happen: it's here for avoiding bad clones. */
		if (next_frame >= ax_local->stop_page) {
			PRINTK(ERROR_MSG, "%s: next frame inconsistency, %#2x\n", dev->name, next_frame);
			next_frame = ax_local->rx_start_page;
		}
		ax_local->current_page = next_frame;
		writeb(next_frame-1, ax_base+EN0_BOUNDARY);
	}

	return;
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_rx_overrun
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void ax_rx_overrun(struct net_device *dev)
{
    struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;
	unsigned char was_txing, must_resend = 0;

    
	/*
	 * Record whether a Tx was in progress and then issue the
	 * stop command.
	 */
	was_txing = readb(ax_base+E8390_CMD) & E8390_TRANS;
	writeb(E8390_NODMA+E8390_PAGE0+E8390_STOP, ax_base+E8390_CMD);

	PRINTK(RX_MSG, "%s: Receiver overrun.\n", dev->name);

	ax_local->stat.rx_over_errors++;

	udelay(2*1000);

	writeb(0x00, ax_base+EN0_RCNTLO);
	writeb(0x00, ax_base+EN0_RCNTHI);

	/*
	 * See if any Tx was interrupted or not. According to NS, this
	 * step is vital, and skipping it will cause no end of havoc.
	 */

	if (was_txing)
	{ 
		unsigned char tx_completed = readb(ax_base+EN0_ISR) & (ENISR_TX+ENISR_TX_ERR);
		if (!tx_completed)
			must_resend = 1;
	}

	/*
	 * Have to enter loopback mode and then restart the NIC before
	 * you are allowed to slurp packets up off the ring.
	 */
	writeb(E8390_TXOFF, ax_base + EN0_TXCR);
	writeb(E8390_NODMA + E8390_PAGE0 + E8390_START, ax_base + E8390_CMD);

	/*
	 * Clear the Rx ring of all the debris, and ack the interrupt.
	 */
	ax_receive(dev);

	/*
	 * Leave loopback mode, and resend any packet that got stopped.
	 */
	writeb(E8390_TXCONFIG, ax_base + EN0_TXCR); 
	if (must_resend)
    	writeb(E8390_NODMA + E8390_PAGE0 + E8390_START + E8390_TRANS, ax_base + E8390_CMD);

}


/*
 *	Collect the stats. This is called unlocked and from several contexts.
 */
static struct net_device_stats *get_stats(struct net_device *dev)
{
 	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase; 
	unsigned long flags;

	/* If the card is stopped, just return the present stats. */
	if (!netif_running(dev))
		return &ax_local->stat;

	spin_lock_irqsave(&ax_local->page_lock,flags);
	/* Read the counter registers, assuming we are in page 0. */
	ax_local->stat.rx_frame_errors += readb(ax_base + EN0_COUNTER0);
	ax_local->stat.rx_crc_errors   += readb(ax_base + EN0_COUNTER1);
	ax_local->stat.rx_missed_errors+= readb(ax_base + EN0_COUNTER2);

	spin_unlock_irqrestore(&ax_local->page_lock, flags);
	return &ax_local->stat;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: make_mc_bits
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static inline void make_mc_bits(u8 *bits, struct net_device *dev)
{
	struct dev_mc_list *dmi;
	for (dmi=dev->mc_list; dmi; dmi=dmi->next) 
	{
		u32 crc;
		if (dmi->dmi_addrlen != ETH_ALEN) 
		{
			PRINTK(INIT_MSG, "%s: invalid multicast address length given.\n", dev->name);
			continue;
		}
		crc = ether_crc(ETH_ALEN, dmi->dmi_addr);
		/* 
		 * The 8390 uses the 6 most significant bits of the
		 * CRC to index the multicast table.
		 */
		bits[crc>>29] |= (1<<((crc>>26)&7));
	}
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: do_set_multicast_list
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void do_set_multicast_list(struct net_device *dev)
{
 	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase; 
	int i;
	if (!(dev->flags&(IFF_PROMISC|IFF_ALLMULTI))) 
	{
		memset(ax_local->mcfilter, 0, 8);
		if (dev->mc_list)
			make_mc_bits(ax_local->mcfilter, dev);
	}
	else
		memset(ax_local->mcfilter, 0xFF, 8);	/* mcast set to accept-all */
	 
	if (netif_running(dev))
		writeb(E8390_RXCONFIG, ax_base + EN0_RXCR);
	writeb(E8390_NODMA + E8390_PAGE1, ax_base + E8390_CMD);
	for(i = 0; i < 8; i++) 
	{
		writeb(ax_local->mcfilter[i], ax_base + EN1_MULT_SHIFT(i));
	}
	writeb(E8390_NODMA + E8390_PAGE0, ax_base + E8390_CMD);

  	if(dev->flags&IFF_PROMISC)
  		writeb(E8390_RXCONFIG | 0x18, ax_base + EN0_RXCR);
	else if(dev->flags&IFF_ALLMULTI || dev->mc_list) 
  		writeb(E8390_RXCONFIG | 0x08, ax_base + EN0_RXCR);
	
	else 
  		writeb(E8390_RXCONFIG, ax_base + EN0_RXCR);
 }


/*
 * ----------------------------------------------------------------------------
 * Function Name: set_multicast_list
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void set_multicast_list(struct net_device *dev)
{
	unsigned long flags;
	struct ax_device *ax_local = (struct ax_device*)dev->priv;
	
	spin_lock_irqsave(&ax_local->page_lock, flags);
	do_set_multicast_list(dev);
	spin_unlock_irqrestore(&ax_local->page_lock, flags);
}	


/*
 * ----------------------------------------------------------------------------
 * Function Name: ethdev_init
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static int ethdev_init(struct net_device *dev)
{
  
	if (dev->priv == NULL) 
	{
		struct ax_device *ax_local;
		
		dev->priv = kmalloc(sizeof(struct ax_device), GFP_KERNEL);
		if (dev->priv == NULL)
			return -ENOMEM;
		memset(dev->priv, 0, sizeof(struct ax_device));
		ax_local = (struct ax_device *)dev->priv;
		spin_lock_init(&ax_local->page_lock);
	}
    
	dev->hard_start_xmit = &ax_start_xmit;
	dev->get_stats	= get_stats;
	dev->set_multicast_list = &set_multicast_list;

	ether_setup(dev);
        
	return 0;
}



/*
 * ----------------------------------------------------------------------------
 * Function Name: ax88796_PHY_init
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void ax88796_PHY_init(struct net_device *dev)
{
	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;
	u8 tmp_data;

	/* Enable AX88796B FOLW CONTROL */
	writeb(ENFLOW_ENABLE, ax_base+EN0_FLOW);

	/* Enable PHY PAUSE */
//	mdio_write(dev,0x10,0x04,(mdio_read(dev,0x10,0x04) | 0x400));
//	mdio_write(dev,0x10,0x00,0x1200);

	/* Enable AX88796B TQC */
	tmp_data = readb(ax_base+EN0_MCR);
	writeb( tmp_data | ENTQC_ENABLE, ax_base+EN0_MCR);

	/* Enable AX88796B Transmit Buffer Ring */
	writeb(E8390_NODMA+E8390_PAGE3+E8390_STOP, ax_base+E8390_CMD);
	writeb(ENTBR_ENABLE, ax_base+EN3_TBR);
	writeb(E8390_NODMA+E8390_PAGE0+E8390_STOP, ax_base+E8390_CMD);
	
	//KH!!!!
#if defined(CONFIG_SC14450_ASIX_LOW_POWER)
    ax_local->media = MEDIA_10FULL;
#endif	
    switch (ax_local->media) {
	default:
	case MEDIA_AUTO:
		mdio_write(dev,0x10,0x04,(mdio_read(dev,0x10,0x04) | 0x400));
     	 	mdio_write(dev,0x10,0x00,0x1200);
		PRINTK(DRIVER_MSG, "AX88796B: The media mode is autosense.\n");
		break;

	case MEDIA_100FULL:
		PRINTK(DRIVER_MSG, "AX88796B: The media mode is forced to 100full.\n");
		mdio_write(dev,0x10,0x04,0x0501);
		mdio_write(dev,0x10,0x00,0x1200);
		break;

	case MEDIA_100HALF:
		PRINTK(DRIVER_MSG, "AX88796B: The media mode is forced to 100half.\n");
		mdio_write(dev,0x10,0x04,0x0081);
		mdio_write(dev,0x10,0x00,0x1200);
		break;

	case MEDIA_10FULL:
		PRINTK(DRIVER_MSG, "AX88796B: The media mode is forced to 10full.\n");
		mdio_write(dev,0x10,0x04,0x0441);
		mdio_write(dev,0x10,0x00,0x1200);
		break;

	case MEDIA_10HALF:
		PRINTK(DRIVER_MSG, "AX88796B: The media mode is forced to 10half.\n");
		mdio_write(dev,0x10,0x04,0x0021);
		mdio_write(dev,0x10,0x00,0x1200);
		break;
	}

	ax_local->media_curr = readb(ax_base + EN0_SR);

}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_init
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void ax_init(struct net_device *dev, int startp)
{
	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;
	int i;
	
	/* Follow National Semi's recommendations for initing the DP83902. */
	writeb(E8390_NODMA+E8390_PAGE0+E8390_STOP, ax_base+E8390_CMD); /* 0x21 */
	writeb(0x49, ax_base + EN0_DCFG);	/* 0x48 or 0x49 */
	/* Clear the remote byte count registers. */
	writeb(0x00,  ax_base + EN0_RCNTLO);
	writeb(0x00,  ax_base + EN0_RCNTHI);
	/* Set to monitor and loopback mode -- this is vital!. */
	writeb(E8390_RXOFF, ax_base + EN0_RXCR); /* 0x20 */
	writeb(E8390_TXOFF, ax_base + EN0_TXCR); /* 0x02 */
	/* Set the transmit page and receive ring. */
	writeb(NESM_START_PG, ax_base + EN0_TPSR);
	writeb(NESM_RX_START_PG, ax_base + EN0_STARTPG);
	writeb(NESM_RX_START_PG, ax_base + EN0_BOUNDARY);

	ax_local->current_page = NESM_RX_START_PG + 1;		/* assert boundary+1 */
	writeb(NESM_STOP_PG, ax_base + EN0_STOPPG);

	ax_local->tx_prev_ctepr = 0;
	ax_local->tx_start_page = NESM_START_PG;
	ax_local->tx_curr_page = NESM_START_PG;
	ax_local->tx_stop_page = NESM_START_PG + TX_PAGES;

	/* Init interrupt priority levels and keyboard interrupt setup */
#ifndef CONFIG_CVM480_SPI_DECT_SUPPORT
	/* mask interrupt for now, we'll unmask it in open() */
	//SetBits( INT3_PRIORITY_REG, KEYB_INT_PRIO, 6 ) ;
	KEY_BOARD_INT_REG = 0x0;	
	KEY_DEBOUNCE_REG = 0x0;		
#endif	
#ifdef CONFIG_SC14450_VoIP_RevB
	/* int7 is used, it is set up by the bootloader */
#elif defined CONFIG_450DEMO
	P1_14_MODE_REG = 0x229;			// P1_14 is int6n	
	SetBits(KEY_GP_INT_REG, INT6_CTRL, 4);	// int6 is an active high signal
#endif
	KEY_STATUS_REG = ETH_INT ;

	/* set interrupt level to push-pull / active high */
	writeb(0x30, ax_base + EN0_BTCR);

	/* Clear the pending interrupts and mask. */
	writeb(0xFF, ax_base + EN0_ISR);
	writeb(0x00,  ax_base + EN0_IMR);

	/* Copy the station address into the DS8390 registers. */

	writeb(E8390_NODMA + E8390_PAGE1 + E8390_STOP, ax_base+E8390_CMD); /* 0x61 */
	writeb(NESM_START_PG + TX_PAGES + 1, ax_base + EN1_CURPAG);
	for(i = 0; i < 6; i++) 
	{
		writeb(dev->dev_addr[i], ax_base + EN1_PHYS_SHIFT(i));
		if(readb(ax_base + EN1_PHYS_SHIFT(i))!=dev->dev_addr[i])
			PRINTK(ERROR_MSG, "Hw. address read/write mismap %d\n",i);
	}
	writeb(E8390_NODMA+E8390_PAGE0+E8390_STOP, ax_base+E8390_CMD);

	if (startp) 
	{
#if defined(CONFIG_SC14450_ASIX_LOW_POWER)
        rl_smi_init();
#endif
		ax88796_PHY_init(dev);
		writeb(0xff,  ax_base + EN0_ISR);
		writeb(ENISR_ALL,  ax_base + EN0_IMR);
		writeb(E8390_NODMA+E8390_PAGE0+E8390_START, ax_base+E8390_CMD);
		writeb(E8390_TXCONFIG, ax_base + EN0_TXCR); /* xmit on. */
		/* 3c503 TechMan says rxconfig only after the NIC is started. */
		writeb(E8390_RXCONFIG, ax_base + EN0_RXCR); /* rx on,  */
		do_set_multicast_list(dev);	/* (re)load the mcast table */
	}

}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_trigger_send
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */

static void ax_trigger_send(struct net_device *dev, unsigned int length, int start_page)
{
 	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;
	writeb(E8390_NODMA+E8390_PAGE0, ax_base+E8390_CMD);
	writeb(length & 0xff, ax_base + EN0_TCNTLO);
	writeb(length >> 8, ax_base + EN0_TCNTHI);
	writeb(start_page, ax_base + EN0_TPSR);
	writeb((E8390_NODMA|E8390_TRANS|E8390_START), ax_base+E8390_CMD);
}


 
#if defined(CONFIG_SC14450_ASIX_LOW_POWER)

static u_int rl_port_status(int port){

    u_int smi_reg_val;

    smi_reg_val = rl_reg_read(port,1,0);

    if (smi_reg_val & 0x4) 
        return 1;
    else
        return 0;

}

static unsigned int ax_reg_write(unsigned int base, unsigned int page, unsigned int reg_no, unsigned int reg_val){

    unsigned int asix_base = base;
    unsigned int write_address = asix_base + (reg_no<<1);
    unsigned char ret_val;
    
    ret_val = *(unsigned char *)asix_base; // reg0
    ret_val = (ret_val & 0x3F) | ((page&0x3) << 6);
    *(unsigned char *)asix_base=ret_val;

    *(unsigned char *)write_address = reg_val&0xFF;
    
    // printk("ax_base = %X\n", base);
    
    return 0;

}

static u8 ax_reg_read(unsigned int base, unsigned int page, unsigned int reg_no){

    unsigned int asix_base = base;
    unsigned int read_address = asix_base + (reg_no<<1);
    u8 ret_val;
    
    ret_val = *(u8 *)asix_base; // reg0
    ret_val = (ret_val & 0x3F) | ((page&0x3) << 6);
    *(u8 *)asix_base=ret_val;

    ret_val = *(u8 *)read_address;

    return ret_val;

}


static void ax_wol_program(unsigned long arg){
    struct net_device *dev = (struct net_device *)(arg);
    
    ax_wol_init(dev);
    
}

#endif

/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_watchdog
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */

static void ax_watchdog(unsigned long arg)
{

	struct net_device *dev = (struct net_device *)(arg);
 	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;

	int status,duplex;
#if defined(CONFIG_SC14450_ASIX_LOW_POWER)
    
    int realtek_port0_change=0;
    int realtek_port1_change=0;
    int realtek_port1_new_status=0;
    int realtek_port0_new_status=0;

#endif

//	printk("IN WATCHDOG!!! Interrupt status 0x%x 0x%x 0x%x 0x%x\n",*(volatile unsigned short *)INT0_PRIORITY_REG,*(volatile unsigned short *)INT1_PRIORITY_REG,*(volatile unsigned short *)INT2_PRIORITY_REG,*(volatile unsigned short *)INT3_PRIORITY_REG);
//	printk("IN WATCHDOG!!! Keyboard Registers status 0x%x 0x%x 0x%x\n",*(volatile unsigned short *)0xFF49b0,*(volatile unsigned short *)0xff49b2,*(volatile unsigned short *)0xFF49b6);
//	printk("EN0_BTCR : 0x%x\n",readb(ax_base + EN0_BTCR));

	status = readb(ax_base + EN0_SR);

#if defined(CONFIG_SC14450_ASIX_LOW_POWER)

    ax_state = 0x3 & ax_reg_read((unsigned int)ax_base, 3, 0xb);

    if (ax_state_prev != ax_state) {
        printk("ax_state_change : %d -> %d\n",ax_state_prev,ax_state);
        ax_state_prev = ax_state;
    }

    if (ax_state == 0) {
    
        if (ax_idle_tics == AX_IDLE_TICS_MAX) { // 2*15 secs -> 30 secs
            ax_wol_program(arg); // program power save
            ax_idle_tics = AX_IDLE_TICS_MAX+1;
        } else {
            if (ax_idle_tics < AX_IDLE_TICS_MAX) 
                ax_idle_tics++; 
                // printk(".");
        }
    } else {
        ; // printk("ax_state = %d\n", ax_state);
    }
    
#endif    
    
	if(ax_local->media_curr != status) {
        
#if defined(CONFIG_SC14450_ASIX_LOW_POWER)  
        printk("ax_status  %X -> %X\n", ax_local->media_curr, status);
#endif
		if ((status & 0x01))
		{
			if ((status & 0x04)) {
				PRINTK(DRIVER_MSG, "%s Link mode : 100 Mb/s  ", dev->name);
			}
			else {
				PRINTK(DRIVER_MSG, "%s Link mode : 10 Mb/s  ", dev->name);
			}

			duplex = readb(ax_base + EN0_MCR);
			if ((duplex & 0x80)) {
				PRINTK(DRIVER_MSG, "Duplex mode.\n");
			}
			else {
				PRINTK(DRIVER_MSG, "Half mode.\n");	
			}
		}
		else {
			PRINTK(DRIVER_MSG, "%s Link down.\n", dev->name);
		}

		ax_local->media_curr = readb(ax_base + EN0_SR);
	}
    
#if defined(CONFIG_SC14450_ASIX_LOW_POWER)
   
    if (rl_cmd_delay == 0) {

        ax_power_up_cmd = 0;
        realtek_port0_new_status = rl_port_status(0);
        realtek_port1_new_status = rl_port_status(1);

        if (realtek_port0_new_status & ~rl_p0_status) 
            realtek_port0_change = 1; // rise
        else if (~realtek_port0_new_status & rl_p0_status) 
            realtek_port0_change = 2; // fall
        else
            realtek_port0_change = 0; // no change

        if (realtek_port1_new_status & ~rl_p1_status) 
            realtek_port1_change = 1; // rise
        else if (~realtek_port1_new_status & rl_p1_status) 
            realtek_port1_change = 2; // fall
        else
            realtek_port1_change = 0; // no change

        if (realtek_port0_change == 1) { // rise
            printk("realtek_port0_change = %d\n",realtek_port0_change);
            if (realtek_port1_new_status) {
                printk("Setting port 0 speed to 100\n");
                rl_set_speed(0,RL_SPEED100);
                printk("Setting port 1 speed to 100\n");
                rl_set_speed(1,RL_SPEED100);
            }
            ax_power_up_cmd = 1;
            rl_cmd_delay  = 5;
        } else if (realtek_port0_change == 2) { // fall
            printk("realtek_port0_change = %d\n",realtek_port0_change);
            printk("Setting port 0 speed to 10\n");
            rl_set_speed(0,RL_SPEED10);
            
            printk("Setting port 1 speed to 10\n");
            rl_set_speed(1,RL_SPEED10);
            
            #if 0
            printk("Powering Down ASIX....\n");
            ax_reg_write((unsigned int) ax_local->membase,3,0xb,0);
            ax_reg_write((unsigned int) ax_local->membase,3,0xb,2);
            #endif

            rl_cmd_delay = 5;
        } else {
            if (realtek_port1_change == 1) {
                printk("realtek_port1_change = %d\n",realtek_port1_change);
                if (realtek_port0_new_status) {
                    printk("Setting port 0 speed to 100\n");
                    rl_set_speed(0,RL_SPEED100);
                    printk("Setting port 1 speed to 100\n");
                    rl_set_speed(1,RL_SPEED100);
                    rl_cmd_delay = 2;
                }
            } else if (realtek_port1_change == 2) {
                printk("realtek_port1_change = %d\n",realtek_port1_change);
                
                printk("Setting port 0 speed to 10\n");
                rl_set_speed(0,RL_SPEED10);
                
                printk("Setting port 1 speed to 10\n");
                rl_set_speed(1,RL_SPEED10);
                
                rl_cmd_delay = 2;
            }
        }        
        rl_p1_status = realtek_port1_new_status;
        rl_p0_status = realtek_port0_new_status;
    
    
    } else {
        if (rl_cmd_delay == 3) {
            #if 0
            if (ax_power_up_cmd) {
                printk("Powering Up ASIX....\n");
                ax_reg_write((unsigned int) ax_local->membase,3,0x1f,1);
                ax_idle_tics=0;
            }
            #endif
        }

        rl_cmd_delay--;
        printk("** %d, p0:%d, p1:%d, \n", rl_cmd_delay,  rl_port_status(0), rl_port_status(1));
    }
#endif
    
	mod_timer(&ax_local->watchdog, jiffies + AX88796_WATCHDOG_PERIOD);

	if( ax_local->bcast_dis == 1)
	{
		ax_local->bcast_dis = 0;
		writeb(E8390_RXCONFIG, ax_base + EN0_RXCR);
	}
	ax_local->bps = 0;
	return ;
}


#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_vlan_rx_register
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void ax_vlan_rx_register(struct net_device *dev, struct vlan_group *grp)
{
 	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;

	writeb((readb(ax_base+EN0_MCR) | ENVLAN_ENABLE), ax_base+EN0_MCR);
	ax_local->vlgrp = grp;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_vlan_rx_kill_vid
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void ax_vlan_rx_kill_vid(struct net_device *dev, unsigned short vid)
{
 	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;

	writeb((readb(ax_base+EN0_MCR) & ~ENVLAN_ENABLE), ax_base+EN0_MCR);
	if (ax_local->vlgrp)
		ax_local->vlgrp->vlan_devices[vid] = NULL;
}
#endif


/*======================================================================
    MII interface support
======================================================================*/
#define MDIO_SHIFT_CLK      0x01
#define MDIO_DATA_WRITE0    0x00
#define MDIO_DATA_WRITE1    0x08
#define MDIO_DATA_READ		0x04
#define MDIO_MASK			0x0f
#define MDIO_ENB_IN			0x02

static void mdio_sync(struct net_device *dev)
{
	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;
    int bits;
    for (bits = 0; bits < 32; bits++) {
		writeb(MDIO_DATA_WRITE1, ax_base + AX88796_MII_EEPROM);
		writeb(MDIO_DATA_WRITE1 | MDIO_SHIFT_CLK, ax_base + AX88796_MII_EEPROM);
    }
}

static void mdio_clear(struct net_device *dev)
{
	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;

    int bits;
    for (bits = 0; bits < 16; bits++) {
		writeb(MDIO_DATA_WRITE0, ax_base + AX88796_MII_EEPROM);
		writeb(MDIO_DATA_WRITE0 | MDIO_SHIFT_CLK, ax_base + AX88796_MII_EEPROM);
    }
}

static int mdio_read(struct net_device *dev, int phy_id, int loc)
{
	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;

    u_int cmd = (0xf6<<10)|(phy_id<<5)|loc;
    int i, retval = 0;
	mdio_clear(dev);
    mdio_sync(dev);
    for (i = 14; i >= 0; i--) {
		int dat = (cmd&(1<<i)) ? MDIO_DATA_WRITE1 : MDIO_DATA_WRITE0;
		writeb(dat, ax_base + AX88796_MII_EEPROM);
		writeb(dat | MDIO_SHIFT_CLK, ax_base + AX88796_MII_EEPROM);
    }
    for (i = 19; i > 0; i--) {
		writeb(MDIO_ENB_IN, ax_base + AX88796_MII_EEPROM);
		retval = (retval << 1) | ((readb(ax_base + AX88796_MII_EEPROM) & MDIO_DATA_READ) != 0);
		writeb(MDIO_ENB_IN | MDIO_SHIFT_CLK, ax_base + AX88796_MII_EEPROM);
    }
    return (retval>>1) & 0xffff;
}

static void mdio_write(struct net_device *dev, int phy_id, int loc, int value)
{
	struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;

    u_int cmd = (0x05<<28)|(phy_id<<23)|(loc<<18)|(1<<17)|value;
    int i;
	mdio_clear(dev);
    mdio_sync(dev);
    for (i = 31; i >= 0; i--) {
	int dat = (cmd&(1<<i)) ? MDIO_DATA_WRITE1 : MDIO_DATA_WRITE0;
		writeb(dat, ax_base + AX88796_MII_EEPROM);
		writeb(dat | MDIO_SHIFT_CLK, ax_base + AX88796_MII_EEPROM);
    }
    for (i = 1; i >= 0; i--) {
		writeb(MDIO_ENB_IN, ax_base + AX88796_MII_EEPROM);
		writeb(MDIO_ENB_IN | MDIO_SHIFT_CLK, ax_base + AX88796_MII_EEPROM);
    }
}

#if defined(CONFIG_SC14450_ASIX_LOW_POWER)


#define _P2_DATA_REG                             (0xFF4890)  /* P2 Data input /out register */
#define _P2_SET_DATA_REG                         (0xFF4892)  /* P2 Set port pins register */
#define _P2_RESET_DATA_REG                       (0xFF4894)  /* P2 Reset port pins register */
#define _P2_07_MODE_REG                          (0xFF48AE)  /* P2y Mode Register */
#define _P2_10_MODE_REG                          (0xFF48B4)  /* P2y Mode Register */


#define MDC_BIT     7
#define MDIO_BIT    10

#define REALTEK_START 0x4000
#define REALTEK_READ  0x2000
#define REALTEK_WRITE 0x1000
#define REALTEK_TO    0x0002

#define MDC0 writew(1<<MDC_BIT, _P2_RESET_DATA_REG)
#define MDC1 writew(1<<MDC_BIT, _P2_SET_DATA_REG)

#define MDIO0 writew(1<<MDIO_BIT, _P2_RESET_DATA_REG)
#define MDIO1 writew(1<<MDIO_BIT, _P2_SET_DATA_REG)

#define PORT_INPUT          0
#define PORT_OUTPUT         3
#define PID_port            0	

#define GetWord             readw

static void realtek_byte_write(unsigned char value){
    int i;
    unsigned char mask=0x80;

    for (i=0;i<8;i++){
        
        MDC0;

        if (value&mask) {
            MDIO1; 
        } else  {
            MDIO0;
        }

        MDC1;
        MDC1;
        MDC0;
        mask = mask>>1;
    }     

}

static void rl_reg_write(int phy_id, int loc, int value)
{

    u_int address16;
    unsigned char a1,a0,b1,b0;


    address16 = REALTEK_START | REALTEK_WRITE | ((phy_id&0x1F)<<7) | ((loc&0x1F)<<2) | REALTEK_TO;
    a1 = ( address16 & 0xFF00 ) >> 8;
    a0 = ( address16 & 0x00FF ) >> 0;
    b1 = ( value  & 0xFF00 ) >> 8;
    b0 = ( value  & 0x00FF ) >> 0;
    
    realtek_byte_write(a1);
    realtek_byte_write(a0);
    realtek_byte_write(b1);
    realtek_byte_write(b0);

}

void realtek_smi_preamble(void){
    realtek_byte_write(0xFF);
    realtek_byte_write(0xFF);
    realtek_byte_write(0xFF);
    realtek_byte_write(0xFF);
}

void set_mdio_mode(int mode){
    writew(mode <<8 | PID_port, _P2_10_MODE_REG);
}


u_int rl_reg_read(int phy_id, int loc, int dbg_mode){
    u_int smi_address, i, ret_val;
    unsigned char a1,a0;
    u_int p2_data;

    smi_address = REALTEK_START | REALTEK_READ | ((phy_id&0x1F)<<7) | ((loc&0x1F)<<2) | REALTEK_TO;
    a1 = ( smi_address & 0xFF00 ) >> 8;
    a0 = ( smi_address & 0x00FF ) >> 0;
    
    realtek_byte_write(a1);
    realtek_byte_write(a0);
    
    set_mdio_mode(PORT_INPUT);
    ret_val=0;

    for(i=0;i<16;i++){
        p2_data=GetWord(_P2_DATA_REG);
        if (p2_data & (1<<MDIO_BIT))
            ret_val = (ret_val<<1)|1;
        else
            ret_val = (ret_val<<1)|0;
        MDC0;
        MDC1;
        MDC1;
        MDC0;
        MDC0;
    }
    set_mdio_mode(PORT_OUTPUT);
    
    if (dbg_mode) {
        printk("***rl_reg_read : Port %2d, Reg: %2d = %04X\n",phy_id, loc, ret_val);
    }

    return ret_val;

}


void rl_set_speed(int port, int speed){
     u_int smi_reg_val;

#if 0
    if (speed == SPEED100) {
        smi_reg_val = rl_reg_read(port,0,0); 
        smi_reg_val = smi_reg_val | ((1<<13)); // 1:100, 0:10
        smi_reg_val = smi_reg_val | ((1<<12)); // auto-negotiation
        rl_reg_write(port,0,smi_reg_val);
        // rl_reg_read(port,0,1); 

        smi_reg_val = rl_reg_read(port,0,0); 
        smi_reg_val = smi_reg_val | ( (1<<11)); // power down 0
        rl_reg_write(port,0,smi_reg_val);
        // rl_reg_read(port,0,1); 


        smi_reg_val = rl_reg_read(port,0,0); 
        smi_reg_val = smi_reg_val & ( ~(1<<11)); // power up 0
        rl_reg_write(port,0,smi_reg_val);
        // rl_reg_read(port,0,1);
    } else {
        smi_reg_val = rl_reg_read(port,0,0); 
        smi_reg_val = smi_reg_val & (~(1<<13)); // 1:100, 0:10
        smi_reg_val = smi_reg_val & (~(1<<12)); // auto-negotiation
        rl_reg_write(port,0,smi_reg_val);
        //rl_reg_read(port,0,1); 

        smi_reg_val = rl_reg_read(port,0,0); 
        smi_reg_val = smi_reg_val | ( (1<<11)); // power down 0
        rl_reg_write(port,0,smi_reg_val);
        //rl_reg_read(port,0,1); 


        smi_reg_val = rl_reg_read(port,0,0); 
        smi_reg_val = smi_reg_val & ( ~(1<<11)); // power up 0
        rl_reg_write(port,0,smi_reg_val);
        //rl_reg_read(port,0,1);
    }
#else
    if (speed == RL_SPEED100) {
        // program cpabilities register of port =.
        smi_reg_val = rl_reg_read(port,4,0); 
        smi_reg_val = smi_reg_val | ( (1<<8));
        smi_reg_val = smi_reg_val | ( (1<<7));
        rl_reg_write(port,4,smi_reg_val);
    } else {
        // program cpabilities register of port.
        smi_reg_val = rl_reg_read(port,4,0); 
        smi_reg_val = smi_reg_val & ( ~(1<<8));
        smi_reg_val = smi_reg_val & ( ~(1<<7));
        rl_reg_write(port,4,smi_reg_val);
    }
    // restart auto negotiation of port 4
    smi_reg_val = rl_reg_read(port,0,0); 
    smi_reg_val = smi_reg_val | ( (1<<9));
    rl_reg_write(port,0,smi_reg_val);

#endif

}

void rl_smi_init(void) {
    u_int smi_reg_val;

    smi_reg_val = readw(_P2_07_MODE_REG); printk("realtek_smi_init: %X = %04X\n", _P2_07_MODE_REG, smi_reg_val);
    writew(PORT_OUTPUT <<8 | PID_port, _P2_07_MODE_REG);
    smi_reg_val = GetWord(_P2_07_MODE_REG); printk("realtek_smi_init: %X = %04X\n", _P2_07_MODE_REG, smi_reg_val);

    smi_reg_val = GetWord(_P2_10_MODE_REG); printk("realtek_smi_init: _P2_10_MODE_REG = %04X\n", smi_reg_val);
    writew(PORT_OUTPUT <<8 | PID_port, _P2_10_MODE_REG);
    smi_reg_val = GetWord(_P2_10_MODE_REG); printk("realtek_smi_init: _P2_10_MODE_REG = %04X\n", smi_reg_val);

    realtek_smi_preamble();

    rl_set_speed(0,RL_SPEED10);
    rl_set_speed(1,RL_SPEED100);

#if 0
    smi_reg_val = rl_reg_read(0,0,1); 
    smi_reg_val = smi_reg_val & (~(1<<13)); // 1:100, 0:10
    smi_reg_val = smi_reg_val & (~(1<<12)); // auto-negotiation
    rl_reg_write(0,0,smi_reg_val);

//    smi_reg_val = rl_reg_read(0,22,1); 
//    smi_reg_val = smi_reg_val & (~(1<<5)); 

//    smi_reg_val = smi_reg_val | ( (1<<4));
//    rl_reg_write(0,22,smi_reg_val);

    smi_reg_val = rl_reg_read(0,0,1); 
    smi_reg_val = smi_reg_val | ( (1<<11)); // power down 0
    rl_reg_write(0,0,smi_reg_val);


    smi_reg_val = rl_reg_read(0,0,1); 
    smi_reg_val = smi_reg_val & ( ~(1<<11)); // power up 0
    rl_reg_write(0,0,smi_reg_val);
#endif

    smi_reg_val = rl_reg_read(2,0,1); 
    smi_reg_val = smi_reg_val | ( (1<<11)); // power down 2
    rl_reg_write(2,0,smi_reg_val);

    smi_reg_val = rl_reg_read(3,0,1); 
    smi_reg_val = smi_reg_val | ( (1<<11)); // power down 3
    rl_reg_write(3,0,smi_reg_val);
    
    // program cpabilities register of port 4.
    smi_reg_val = rl_reg_read(4,4,1); 
    smi_reg_val = smi_reg_val & ( ~(1<<8));
    smi_reg_val = smi_reg_val & ( ~(1<<7));
    rl_reg_write(4,4,smi_reg_val);

    // restart auto negotiation of port 4
    smi_reg_val = rl_reg_read(4,0,1); 
    smi_reg_val = smi_reg_val | ( (1<<9));
    rl_reg_write(4,0,smi_reg_val);


}



void ax_wol_init_1(struct net_device *dev){

    struct ax_device *ax_local = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;


    unsigned int bin;
    
    bin = readb(ax_base);
    printk("asix offset 0 = 0x%X\n", bin);

    printk("Selecting Page 3 ..\n");
	writeb((bin&0x3F)|0xC0,   ax_base + 0);

    printk("asix offset 0 = 0x%X\n", readb(ax_base));


	// mask
    writeb(0x03,  ax_base + EN3_BM0);
	writeb(0x00,  ax_base + EN3_BM0);
	writeb(0x00,  ax_base + EN3_BM0);
	writeb(0x3C,  ax_base + EN3_BM0);

	// crc
    writeb(0x65,  ax_base + EN3_BM10CRC);
	writeb(0x52,  ax_base + EN3_BM10CRC);
	writeb(0x00,  ax_base + EN3_BM10CRC);
	writeb(0x00,  ax_base + EN3_BM10CRC);

#if 1

	writeb(0x06,  ax_base + EN3_BMOFST);
	writeb(0x00,  ax_base + EN3_BMOFST);
	writeb(0x00,  ax_base + EN3_BMOFST);
	writeb(0x00,  ax_base + EN3_BMOFST);


	writeb(0x6e,  ax_base + EN3_LSTBYT);
	writeb(0x00,  ax_base + EN3_LSTBYT);
	writeb(0x00,  ax_base + EN3_LSTBYT);
	writeb(0x00,  ax_base + EN3_LSTBYT);

	writeb(0x01,  ax_base + EN3_BMCD);
	writeb(0x00,  ax_base + EN3_BMCD);
	writeb(0x00,  ax_base + EN3_BMCD);
	writeb(0x00,  ax_base + EN3_BMCD);


	writeb(0x03,  ax_base + EN3_WUCS);
	writeb(0x01,  ax_base + EN3_PMR);
#endif

    writeb(bin,   ax_base + 0);

    
}


#define CRC16_INIT_VALUE	0xffff
#define CRC16_XOR_VALUE		0x0000

static unsigned short crctable[256];


void make_crc_table( void ) {
	int i, j;
	unsigned long poly, c;
	/* terms of polynomial defining this crc (except x^16): */
	static const unsigned char p[] = {0,2,15};

	/* make exclusive-or pattern from polynomial (0x1021) */
	poly = 0L;
	for ( i = 0; i < sizeof( p ) / sizeof( unsigned char ); i++ ) {
		poly |= 1L << p[i];
	}

	for ( i = 0; i < 256; i++ ) {
		c = i << 8;
		for ( j = 0; j < 8; j++ ) {
			c = ( c & 0x8000 ) ? poly ^ ( c << 1 ) : ( c << 1 );
		}
		crctable[i] = (unsigned short) c;
	}
}

void CRC16_InitChecksum( unsigned short *crcvalue ) {
	*crcvalue = CRC16_INIT_VALUE;
}

void CRC16_Update( unsigned short *crcvalue, unsigned char data ) {
	*crcvalue = ( (*crcvalue )<< 8 ) ^ crctable[ ( *(crcvalue) >> 8 ) ^ data ];
}

void CRC16_UpdateChecksum( unsigned short *crcvalue, const void *data, int length ) {
	unsigned short crc;
	const unsigned char *buf = (const unsigned char *) data;

	crc = *crcvalue;
    printk("**** crc = %X\n",crc);

	while( length-- ) {
        
		crc = ( crc << 8 ) ^ crctable[ ( crc >> 8 ) ^ (*buf) ];
        printk("**** crc = %X, after %X, index=%d, val = %X \n",crc,*buf, ( crc >> 8 ) ^ (*buf) , crctable[ ( crc >> 8 ) ^ (*buf) ]);
        buf++;

	}
	*crcvalue = crc;
}

void CRC16_FinishChecksum( unsigned short *crcvalue ) {
	*crcvalue ^= CRC16_XOR_VALUE;
}

u16 CRC16_BlockChecksum( const void *data, int length ) {
	u16 crc;

	CRC16_InitChecksum( &crc );
    printk("*** crc = %X\n",crc);
	CRC16_UpdateChecksum( &crc, data, length );
    printk("*** crc = %X\n",crc);
	CRC16_FinishChecksum( &crc );
    printk("*** crc = %X\n",crc);
	return crc;
}

u8 reverse_bytes( u8 cin ) {
	unsigned char cout=0;
    int i;
    
    unsigned char mask = 1;
    for (i=0;i<8;i++){
        cout = cout << 1;
        if (cin&mask) cout |= 0x1;
        mask = mask << 1;
    }
    printk("%02x --> %02X\n",cin,cout);

	return cout;
}



void ax_program_filter_arp(struct net_device *dev) {
    u8 ip_addr[4];
    struct in_device *in_dev    = (struct in_device *) dev->ip_ptr;
    struct in_ifaddr *ifa_list  = (struct in_ifaddr *) in_dev->ifa_list;

    ip_addr[0] = 0xFF&(ifa_list->ifa_address>>0);
    ip_addr[1] = 0xFF&(ifa_list->ifa_address>>8);
    ip_addr[2] = 0xFF&(ifa_list->ifa_address>>16);
    ip_addr[3] = 0xFF&(ifa_list->ifa_address>>24);
    
    printk("ax_program_filter_arp: ifa = %d.%d.%d.%d\n",ip_addr[0], ip_addr[1], ip_addr[2], ip_addr[3]);
    
}

void ax_program_filter_MAC(struct net_device *dev) {
int i;
    for(i=0;i<6;i++) {
        printk("ax_program_filter_MAC : MAC %d = %02X\n",i,dev->dev_addr[i]);
    }

}


void ax_wol_init(struct net_device *dev){


    struct ax_device *ax_local  = (struct ax_device *) dev->priv;
	void *ax_base = ax_local->membase;

    u8 ip_addr[4];
    struct in_device *in_dev    = (struct in_device *) dev->ip_ptr;
    struct in_ifaddr *ifa_list  = (struct in_ifaddr *) in_dev->ifa_list;
    u16 crc16_out_arp, crc16_out_mac;
    u8 crc16_in[32];
    unsigned int bin;


    ip_addr[0] = 0xFF&(ifa_list->ifa_address>>0);
    ip_addr[1] = 0xFF&(ifa_list->ifa_address>>8);
    ip_addr[2] = 0xFF&(ifa_list->ifa_address>>16);
    ip_addr[3] = 0xFF&(ifa_list->ifa_address>>24);

    make_crc_table();
    
    crc16_in[0] = reverse_bytes(0x08);
    crc16_in[1] = reverse_bytes(0x06);
    crc16_in[2] = reverse_bytes(ip_addr[0]);
    crc16_in[3] = reverse_bytes(ip_addr[1]);
    crc16_in[4] = reverse_bytes(ip_addr[2]);
    crc16_in[5] = reverse_bytes(ip_addr[3]);
    crc16_out_arp = CRC16_BlockChecksum(crc16_in,6);
    printk("crc arp= 0x%X\n", crc16_out_arp );


    crc16_in[0] = reverse_bytes(dev->dev_addr[0]);
    crc16_in[1] = reverse_bytes(dev->dev_addr[1]);
    crc16_in[2] = reverse_bytes(dev->dev_addr[2]);
    crc16_in[3] = reverse_bytes(dev->dev_addr[3]);
    crc16_in[4] = reverse_bytes(dev->dev_addr[4]);
    crc16_in[5] = reverse_bytes(dev->dev_addr[5]);
    crc16_out_mac = CRC16_BlockChecksum(crc16_in,6);
    printk("crc mac= 0x%X\n", crc16_out_mac );
        
    bin = readb(ax_base);
    printk("asix offset 0 = 0x%X\n", bin);

    printk("Selecting Page 3 ..\n");
	writeb((bin&0x3F)|0xC0,   ax_base + 0);

    printk("asix offset 0 = 0x%X\n", readb(ax_base));

	// mask for arp request
    writeb(0x03,  ax_base + EN3_BM0);
	writeb(0x00,  ax_base + EN3_BM0);
	writeb(0x00,  ax_base + EN3_BM0);
	writeb(0x3C,  ax_base + EN3_BM0);

	// mask for mac match
    writeb(0x3f,  ax_base + EN3_BM1);
	writeb(0x00,  ax_base + EN3_BM1);
	writeb(0x00,  ax_base + EN3_BM1);
	writeb(0x00,  ax_base + EN3_BM1);

	// crc
    writeb(crc16_out_arp&0xFF,  ax_base + EN3_BM10CRC);
	writeb((crc16_out_arp>>8)&0xFF,  ax_base + EN3_BM10CRC);
	writeb(crc16_out_mac&0xFF,  ax_base + EN3_BM10CRC);
	writeb((crc16_out_mac>>8)&0xFF,  ax_base + EN3_BM10CRC);


	writeb(0x06,  ax_base + EN3_BMOFST);
	writeb(0x00,  ax_base + EN3_BMOFST);
	writeb(0x00,  ax_base + EN3_BMOFST);
	writeb(0x00,  ax_base + EN3_BMOFST);


	writeb(ip_addr[3],  ax_base + EN3_LSTBYT); // arp
	writeb(dev->dev_addr[5],  ax_base + EN3_LSTBYT); // mac
	writeb(0x00,  ax_base + EN3_LSTBYT);
	writeb(0x00,  ax_base + EN3_LSTBYT);

	writeb(0x11,  ax_base + EN3_BMCD);
	writeb(0x00,  ax_base + EN3_BMCD);
	writeb(0x00,  ax_base + EN3_BMCD);
	writeb(0x00,  ax_base + EN3_BMCD);

	writeb(0x03,  ax_base + EN3_WUCS);  // b1: wake-up frame enable, b0: link change
	writeb(0x01,  ax_base + EN3_PMR);   // enter D1 state (1), D2(2)

    writeb(bin,   ax_base + 0);

    
}


#endif
