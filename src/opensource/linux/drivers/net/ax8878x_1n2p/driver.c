/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
 /*============================================================================
 * Module Name: driver.c
 * Purpose:
 * Author:
 * Date:
 * Notes:
 * $Log: driver.c,v $
 * no message
 *
 *
 *=============================================================================
 */

/* INCLUDE FILE DECLARATIONS */
#include "driver.h"

#if defined(CONFIG_SC14450_AX8878x_1N2P_SRAM_LOW_POWER)
#include <linux/inetdevice.h>
#include <linux/crc32.h>
#endif

/* NAMING CONSTANT DECLARATIONS */

// These identify the driver base version and may not be removed.
#define DRV_NAME	"AX88782/AX88783"
#define ADP_NAME	"ASIX AX88783 1N2P Local Bus Ethernet Adapter"
#define DRV_VERSION	"1.3.0"
#define PFX DRV_NAME ": "


/* SC14450-specific stuff */
#define AX88783_BASE	0x1200000
#define AX88783_IRQ	KEYB_INT

#define INT6_CTRL       (0x0007)
#define INT7_CTRL       (0x0038)
#define INT8_CTRL       (0x01C0)
#define KEYB_INT_PRIO   (0x0070)

#define ETH_INT		4

extern struct net_device *__alloc_ei_netdev(int size);

static inline struct net_device *alloc_ei_netdev(void)
{
	return __alloc_ei_netdev(0);
}


/* GLOBAL VARIABLES DECLARATIONS */

/* LOCAL VARIABLES DECLARATIONS */
static char version[] __devinitdata =
KERN_INFO ADP_NAME ".c:v" DRV_VERSION " " __TIME__ " " __DATE__ "\n"
KERN_INFO "  http://www.asix.com.tw\n";

static struct net_device *global_dev = NULL;
static int mem = AX88783_BASE ;
static int irq = AX88783_IRQ ;
static int media0 = 0;
static int media1 = 0;

#if defined( CONFIG_SC14450_AX8878x_1N2P_SRAM_LOW_POWER ) 

#define PCR_POWER_DOWN1     (1<<17)

#define WOLCR_SLEEP_MODE    (1<<17)
#define WOLCR_WAKEUP_LEVEL  (1<<20)
#define WOLCR_WAKEUP_ACTIVE (1<<22)
#define WOLCR_RESET_PME     (1<<23)
#define WOLCR_PME           (1<<31)


#define ASIX_IDLE   0
#define ASIX_WOL0   1

#define CMD_NO      0x00
#define CMD_CHECK   0x01
#define CMD_OFF0    0x02
#define CMD_OFF1    0x04
#define CMD_D2      0x08
#define CMD_WOL0    0x10
#define CMD_MAGIC0  0x20
#define CMD_ON1     0x40

#define WCNT_VAL    15
unsigned int wcnt=WCNT_VAL; // 2*WCNT_VAL secs
unsigned short asix_state = ASIX_IDLE;
// // // unsigned short asix_cmd   = CMD_OFF0 | CMD_OFF1 | CMD_D2;
unsigned short asix_cmd   = CMD_OFF1 | CMD_WOL0;
// // unsigned short asix_cmd = CMD_OFF1 | CMD_WOL0;

u16 w_cnt = 0;
u32 crc_wol(struct net_device *, int, int);


#endif

MODULE_AUTHOR("ASIX");
MODULE_DESCRIPTION("ASIX AX88783 1N2P Local Bus Ethernet driver");
MODULE_LICENSE("GPL");

#if 0
MODULE_PARM(mem, "i");
#else
module_param( mem, int, AX88783_BASE ) ;
#endif
MODULE_PARM_DESC(mem, "MEMORY base address(s)");

#if 0
MODULE_PARM(irq, "i");
#else
module_param( irq, int, AX88783_IRQ ) ;
#endif
MODULE_PARM_DESC(irq, "IRQ number(s)");

/* LOCAL SUBPROGRAM DECLARATIONS */

#if defined( CONFIG_SC14450_AX8878x_1N2P_SRAM_LOW_POWER ) 


void SitelClearWakeUp(SWITCH *pSwitch) {
        NapaWriteRegister(pSwitch,WOLCR, WOLCR_RESET_PME);
        NapaClearWakeUp(pSwitch);
        NapaWriteRegister(pSwitch,WOLCR, WOLCR_WAKEUP_LEVEL | WOLCR_WAKEUP_ACTIVE | WOLCR_RESET_PME );
}


void SitelInitWakeUp(struct net_device *dev, SWITCH *pSwitch) {



        //NapaConfigLinkChange(pSwitch, Port_0, 1);
        //NapaConfigMagicPacket(pSwitch, Port_0, 1);
        
        printk("Entering D1 state\n");
        
        // NapaConfigWakeUpDaMatch(pSwitch, Port_0, 1);
        
        NapaConfigWakeUpMaskData(pSwitch,   Port_0, 0, 0x0000003F); 
        NapaConfigWakeUpOffset(pSwitch,     Port_0, 0, 0);
        NapaConfigWakeUpCrcData(pSwitch,    Port_0, 0, crc_wol(dev,0,6));
        NapaConfigWakeUpMask(pSwitch,       Port_0, 0, 1);

        NapaConfigWakeUpMaskData(pSwitch,   Port_0, 1, 0x3C000003); 
        NapaConfigWakeUpOffset(pSwitch,     Port_0, 1, 3);
        NapaConfigWakeUpCrcData(pSwitch,    Port_0, 1, crc_wol(dev,1,6));
        NapaConfigWakeUpMask(pSwitch,       Port_0, 1, 1);

        //NapaConfigWakeUpMaskData(pSwitch,   Port_0, 2, 0x3C000003); 
        //NapaConfigWakeUpOffset(pSwitch,     Port_0, 2, 3);
        //NapaConfigWakeUpCrcData(pSwitch,    Port_0, 2, crc_wol(dev,2));
        //NapaConfigWakeUpMask(pSwitch,       Port_0, 2, 1);


        NapaConfigWakeUpFrame(pSwitch, Port_0, 1);
        NapaConfigWakeUpFunc(pSwitch,1);
        

}

void SitelTriggerWOL(SWITCH *pSwitch, u16 delay_val) {

    SitelClearWakeUp(pSwitch);
    wcnt = delay_val;
    asix_cmd |= CMD_WOL0;

}
#endif

/*
 * ----------------------------------------------------------------------------
 * Function Name: mdio_read()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static int
mdio_read(struct net_device *dev, int phy_id, int location)
{
	PAX_PRIVATE		ax_local = (PAX_PRIVATE)(dev->priv);
	PHY_STURE		PhyData;

	PhyData.PhyID = (u8)phy_id;
	PhyData.PhyAddr = (u8)location;
	NapaMdioRead(ax_local->pSwitch, &PhyData);

	return PhyData.Value;
} /* End of mdio_read() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: mdio_write()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void
mdio_write(struct net_device *dev, int phy_id, int location, int value)
{
	PAX_PRIVATE		ax_local = (PAX_PRIVATE)(dev->priv);
	PHY_STURE		PhyData;

	PhyData.PhyID = (u8)phy_id;
	PhyData.PhyAddr = (u8)location;
	PhyData.Value = value;

	NapaMdioWrite(ax_local->pSwitch, &PhyData);

} /* End of mdio_write() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_start_xmit()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static int
ax_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
	PAX_PRIVATE          ax_local = (PAX_PRIVATE)(dev->priv);
	NAPA_STATUS          Status;


	SDBG(SDBG_TX, ("=====> ax_start_xmit\n"));

#if defined( CONFIG_SC14450_AX8878x_1N2P_SRAM_LOW_POWER ) 
	// printk("=====> ax_start_xmit\n");
    if (asix_state == ASIX_WOL0) {
        printk("exit from D1 state\n");
        SitelClearWakeUp(ax_local->pSwitch);
        asix_state = ASIX_IDLE;
    }
    asix_cmd |= CMD_WOL0;
    wcnt = WCNT_VAL ; // wait 2*WCNT_VAL seconds before going to D1 state
#endif

spin_lock_irq(&ax_local->lock);

	NapaDisableInterrupt(ax_local->pSwitch);

	Status = NapaLocalBusSendPacket(ax_local->pSwitch, skb->len, skb->data);

spin_unlock_irq(&ax_local->lock);

	NapaEnableInterrupt(ax_local->pSwitch);

	SDBG(SDBG_TX, ("<===== ax_start_xmit\n"));

	if(Status == NAPA_STATUS_SUCCESS) {
		ax_local->net_stats.tx_bytes += skb->len;
		ax_local->net_stats.tx_packets++;
		dev_kfree_skb(skb);
		dev->trans_start = jiffies;
		return 0;
	}
	else {
		return 1;
	}

} /* End of ax_start_xmit() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_pci_start_xmit()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
//static void
void
ax_rx(struct net_device *dev)
{
	PAX_PRIVATE		ax_local = (PAX_PRIVATE)(dev->priv);
	NAPA_STATUS		Status;
	struct sk_buff	*skb;
	unsigned short	PktLen;
	PORT			Port;

	SDBG(SDBG_RX, ("=====> ax_rx\n"));

//	printk("=====> ax_rx\n");
	while(1)
	{
		Status = NapaLocalBusGetRxPktLen(ax_local->pSwitch, &PktLen, &Port);
	
		if(Status == NAPA_STATUS_FAILURE)
		{
			ax_local->net_stats.rx_dropped++;
			NapaLocalBusCpoReset(ax_local->pSwitch);
			break;
		}

		skb = dev_alloc_skb(PktLen + 2);

		if(skb == NULL) {
			ax_local->net_stats.rx_dropped++;
			NapaLocalBusCpoReset(ax_local->pSwitch);
			break;
		}

		skb_reserve(skb,2);	/* IP headers on 16 byte boundaries */

		NapaLocalBusGetRxPkt(ax_local->pSwitch, skb_put(skb, PktLen), PktLen);
#if 0
		{
			u16 i;
			SDBG(SDBG_RX, ("Received Packet(%d)\n", PktLen));
			for(i = 0; i < PktLen; i++)
			{
				SDBG(SDBG_RX, "%2.2x ", *((u8 *)skb->data + i));
				if((i + 1) % 16 == 0)
					SDBG(SDBG_RX, "\n");
			
			}
			SDBG(SDBG_RX, "\n");
		
		}
#endif


		skb->dev = dev;
		skb->protocol = eth_type_trans(skb, dev);

		netif_rx(skb);

		dev->last_rx = jiffies;

		ax_local->net_stats.rx_packets++;
		ax_local->net_stats.rx_bytes += PktLen;

		break;
	}

	SDBG(SDBG_RX, ("<===== ax_rx\n"));
//	printk("<===== ax_rx\n");
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_link_change()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
ax_link_change(struct net_device *dev)
{
	PAX_PRIVATE ax_local = (PAX_PRIVATE)(dev->priv);
	LINK_STURE P0LinkStatus, P1LinkStatus;

	NapaGetLinkStatus(ax_local->pSwitch, Port_0, &P0LinkStatus);
	printk(KERN_INFO PFX "port0, Link %s. Speed %d Mbps, %s duplex, %s.\n",
		P0LinkStatus.Link == 1 ? "Up" : "Down",
		P0LinkStatus.Speed == 1 ? 100 : 10,
		P0LinkStatus.Duplex == 1 ? "full" : "half",
		P0LinkStatus.FlowCtrl == 1 ? "FlowCtrl Enabled" : "FlowCtrl Disabled");

	NapaGetLinkStatus(ax_local->pSwitch, Port_1, &P1LinkStatus);
	printk(KERN_INFO PFX "port1, Link %s. Speed %d Mbps, %s duplex, %s.\n",
		P1LinkStatus.Link == 1 ? "Up" : "Down",
		P1LinkStatus.Speed == 1 ? 100 : 10,
		P1LinkStatus.Duplex == 1 ? "full" : "half",
		P1LinkStatus.FlowCtrl == 1 ? "FlowCtrl Enabled" : "FlowCtrl Disabled");

	if(P0LinkStatus.Link == 0 && P1LinkStatus.Link == 0) {
		netif_carrier_off(dev);
			netif_stop_queue(dev);
	} else {
		netif_carrier_on(dev);
			netif_start_queue(dev);
	}

}

/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_pci_start_xmit()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static irqreturn_t
ax_interrupt (int irq, void *dev_id)
{
	struct net_device	*dev = (struct net_device *)dev_id;
	PAX_PRIVATE          ax_local = (PAX_PRIVATE)(dev->priv);
	u16                  Isr;

	/* check if this interrupt is for us (irq line is being shared) */
	if( !( KEY_STATUS_REG & ETH_INT ) )
		return IRQ_NONE ;

//	printk("=====> ax_interrupt\n");
	SDBG(SDBG_INT, ("=====> ax_interrupt\n"));

	spin_lock (&ax_local->lock);

	Isr = NapaReadInterrupt(ax_local->pSwitch);

//	printk("GOT INTERRUPTS: 0x%x\n", Isr);
	if (Isr == 0)
	{
		spin_unlock (&ax_local->lock);
		SDBG(SDBG_INT, ("No interrupt event.\n"));
		SDBG(SDBG_INT, ("<===== ax_interrupt\n"));
		KEY_STATUS_REG = ETH_INT ;
		RESET_INT_PENDING_REG = 2;//KEYB_INT ;
		return IRQ_HANDLED;
	}


	while (Isr) {

		NapaDisableAndAckInterrupt(ax_local->pSwitch, Isr);

		if (Isr & IMSR_INT_LINK_CHANGE) {
			SDBG(SDBG_INT, ("Link change interrupt\n"));
//			printk("Link change interrupt\n");
			ax_link_change(dev);
		}

		if (Isr & IMSR_INT_CPO_EMPTY) {
			SDBG(SDBG_INT, ("Rx interrupt\n"));
//			printk("Rx interrupt\n");
			ax_rx(dev);
		}

		if(Isr & IMSR_INT_CPI_ERR) {
			SDBG(SDBG_INT, ("CPI error interrupt\n"));
		//	printk("CPI error interrupt\n");
			NapaLocalBusCpiReset(ax_local->pSwitch);
		}

		if (Isr & IMSR_INT_CPI_FULL) {
			SDBG(SDBG_INT, ("Tx buffer full interrupt\n"));
		//	printk("Tx buffer full interrupt\n");
			netif_stop_queue(dev);
		}

		if (Isr & IMSR_INT_CPI_CLEAR) {
			SDBG(SDBG_INT, ("Tx buffer clean interrupt\n"));
		//	printk("Tx buffer clean interrupt\n");
			netif_wake_queue(dev);
		}

		Isr = NapaReadInterrupt(ax_local->pSwitch);
	}

	NapaEnableInterrupt(ax_local->pSwitch);

	KEY_STATUS_REG = ETH_INT ;
	RESET_INT_PENDING_REG = 2;//KEYB_INT ;

	spin_unlock (&ax_local->lock);

	SDBG(SDBG_INT, ("<===== ax_interrupt\n"));
//	printk("<===== ax_interrupt\n");

	return IRQ_HANDLED ;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_pci_start_xmit()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void
ax_set_multicast(struct net_device *dev)
{
	PAX_PRIVATE          ax_local = (PAX_PRIVATE)(dev->priv);
	struct dev_mc_list *mclist;
	int i;

	SDBG(SDBG_INIT, ("=====> ax_set_multicast\n"));

	if (dev->flags & IFF_PROMISC) {

		NapaSetRxFilter(ax_local->pSwitch, Port_0, RX_PROMISCUOUS);
		NapaSetRxFilter(ax_local->pSwitch, Port_1, RX_PROMISCUOUS);

	} else if((dev->flags & IFF_ALLMULTI) || (dev->mc_count > MAX_MULTICAST_LIST)) {
		NapaCleanMultiFilter(ax_local->pSwitch, MULTIFILTER);
	} else {

		mclist = dev->mc_list;
		for(i = 0; i < dev->mc_count; i++)
		{
			NapaSetMultiFilter(ax_local->pSwitch, Port_0, i, mclist->dmi_addr);
			NapaSetMultiFilter(ax_local->pSwitch, Port_1, i, mclist->dmi_addr);
			mclist = mclist->next;
		}
	}

	SDBG(SDBG_INIT, ("<===== ax_set_multicast\n"));
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_get_stats()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static struct net_device_stats * ax_get_stats(struct net_device *dev)
{
	PAX_PRIVATE		ax_local = (PAX_PRIVATE)(dev->priv);

	return &ax_local->net_stats;
} /* End of ax_get_stats() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_link()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
 
 



static void
ax_link(unsigned long arg)
{
	struct net_device *dev = (struct net_device *)arg;
	PAX_PRIVATE ax_local = (PAX_PRIVATE)(dev->priv);
	u16 BMCR, BMSR;
	u8  Port;
	u8  CurrLink;
	u8  CurrSpeed;
	u8  CurrDuplex;
    u32 areg;

    for(Port = 0; Port < 2; Port++)
	{
		BMSR = mdio_read(dev, ax_local->phyID[Port], MII_BMSR);
		BMCR = mdio_read(dev, ax_local->phyID[Port], MII_BMCR);
		CurrLink = (BMSR & BMSR_LSTATUS) == 0 ? LINK_DOWN : LINK_UP;
		CurrSpeed = (BMCR & BMCR_SPEED100) == 0 ? 0 : 1;
		CurrDuplex = (BMCR & BMCR_FULLDPLX) == 0 ? 0 : 1;
		
		if(CurrLink != ax_local->LinkStatus[Port].Link ||
		   CurrSpeed != ax_local->LinkStatus[Port].Speed ||
		   CurrDuplex != ax_local->LinkStatus[Port].Duplex)
		{
			ax_local->LinkStatus[Port].Link = CurrLink;
			ax_local->LinkStatus[Port].Speed = CurrSpeed;
			ax_local->LinkStatus[Port].Duplex = CurrDuplex;
			ax_local->LinkStatus[Port].FlowCtrl = 1;
			
			NapaSetLinkStatus(ax_local->pSwitch, Port, &ax_local->LinkStatus[Port]);

			printk(KERN_INFO PFX "port%d, Link %s. Speed %d Mbps, %s duplex, %s.\n",
				    Port,
	   				ax_local->LinkStatus[Port].Link == 1 ? "Up" : "Down",
					ax_local->LinkStatus[Port].Speed == 1 ? 100 : 10,
	 				ax_local->LinkStatus[Port].Duplex == 1 ? "full" : "half",
	  				ax_local->LinkStatus[Port].FlowCtrl == 1 ?
					"FlowCtrl Enabled" : "FlowCtrl Disabled");

			if(Port == 0) {
				
                printk("0x200 = 0x%lx\n",
					   NapaReadRegister(ax_local->pSwitch, 0x200));
#if defined( CONFIG_SC14450_AX8878x_1N2P_SRAM_LOW_POWER ) 
                if (ax_local->LinkStatus[Port].Link == 1) {
                    SitelTriggerWOL(ax_local->pSwitch,WCNT_VAL);
                }
#endif                
			} else
				printk("0x240 = 0x%lx\n",
					   NapaReadRegister(ax_local->pSwitch, 0x240));
		}
	}

	if(ax_local->LinkStatus[0].Link == LINK_DOWN &&
		  ax_local->LinkStatus[1].Link == LINK_DOWN)
	{
		netif_carrier_off(dev);
			netif_stop_queue(dev);
	}
	else
	{
		netif_carrier_on(dev);
			netif_start_queue(dev);
	}
    
#if defined( CONFIG_SC14450_AX8878x_1N2P_SRAM_LOW_POWER ) 
    if ((wcnt == 1)  && (asix_cmd & CMD_OFF0)){
        printk("*** Power Down Port 0\n");
        mdio_write(dev, ax_local->phyID[Port_0], MII_BMCR, BMCR_PDOWN);
        asix_cmd &= ~CMD_OFF0;
        wcnt = 2;
    }

    if ((wcnt == 1)  && (asix_cmd & CMD_OFF1)){
        printk("*** Power Down Port 1\n");
        // mdio_write(dev, ax_local->phyID[Port_1], MII_BMCR, BMCR_PDOWN);
        areg = NapaReadRegister(ax_local->pSwitch, PCR); NapaWriteRegister(ax_local->pSwitch, PCR, areg | PCR_POWER_DOWN1 ); 
        asix_cmd &= ~CMD_OFF1;
        wcnt = 2;
    }

    if ((wcnt == 1)  && (asix_cmd & CMD_ON1)){
        printk("*** Powering Up port 1\n");
        areg = NapaReadRegister(ax_local->pSwitch, PCR); NapaWriteRegister(ax_local->pSwitch,PCR, areg & ~PCR_POWER_DOWN1 );  // power up phy 1
        asix_cmd &= ~CMD_ON1;
        wcnt = 2;
    }

    if ((wcnt == 1)  && (asix_cmd & CMD_D2)){
        printk("*** Entering D2 state\n");
        areg = NapaReadRegister(ax_local->pSwitch, WOLCR); NapaWriteRegister(ax_local->pSwitch, WOLCR, areg | WOLCR_SLEEP_MODE );
        asix_cmd &= ~CMD_D2;
        wcnt = 2;
    }


    if ((wcnt == 1)  && (asix_cmd & CMD_WOL0)){
        printk("*** Entering D1 state for port 0 (WOL on Port 0)\n");

        SitelClearWakeUp(ax_local->pSwitch);
        SitelInitWakeUp(dev, ax_local->pSwitch);

        printk(" PCR   (%04X) = 0x%08lx\n",PCR, NapaReadRegister(ax_local->pSwitch, PCR));
        printk(" WOLCR (%04X) = 0x%08lx\n",WOLCR, NapaReadRegister(ax_local->pSwitch, WOLCR));

        asix_cmd &= ~CMD_WOL0;
        asix_state = ASIX_WOL0;
        wcnt = 2;

    }


#if 0
    if (asix_state == ASIX_WOL0) {
        
        printk("   WOLCR (%04X) = 0x%08lx\n",WOLCR  , NapaReadRegister(ax_local->pSwitch, WOLCR   ));
        printk("   WOLSR (%04X) = 0x%08lx\n\n",WOLSR  , NapaReadRegister(ax_local->pSwitch, WOLSR   ));

        printk("   PORT0MASK0 (%04X) = 0x%08lx\n",PORT0MASK0  , NapaReadRegister(ax_local->pSwitch, PORT0MASK0   ));
        printk("   PORT0CRC0 (%04X) = 0x%08lx\n\n",PORT0CRC0  , NapaReadRegister(ax_local->pSwitch, PORT0CRC0   ));
        
        printk("   PORT0MASK1 (%04X) = 0x%08lx\n",PORT0MASK1  , NapaReadRegister(ax_local->pSwitch, PORT0MASK1   ));
        printk("   PORT0CRC1 (%04X) = 0x%08lx\n\n",PORT0CRC1  , NapaReadRegister(ax_local->pSwitch, PORT0CRC1   ));


        printk("   PORT0MASK2 (%04X) = 0x%08lx\n",PORT0MASK2  , NapaReadRegister(ax_local->pSwitch, PORT0MASK2   ));
        printk("   PORT0CRC2 (%04X) = 0x%08lx\n\n\n\n",PORT0CRC2  , NapaReadRegister(ax_local->pSwitch, PORT0CRC2   ));

    }
#endif

    if (wcnt > 0) {
        wcnt--;
        if ((wcnt == 10) || (wcnt == 5)) {
            printk(" %d sec before D1 state ... ", 2*wcnt);
            printk(" WOLCR (%04X) = 0x%08lx",WOLCR, NapaReadRegister(ax_local->pSwitch, WOLCR)); printk(" WOLCR (%04X) = 0x%08lx\n",PCR, NapaReadRegister(ax_local->pSwitch, PCR));
            areg =  NapaReadRegister(ax_local->pSwitch, PCR); areg |= 1<<31; NapaWriteRegister(ax_local->pSwitch, PCR, areg);
        }
        // if ( (wcnt>0) && (wcnt<10) ) printk(" %d WCR (0x104) = 0x%08lx\n",wcnt, NapaReadRegister(ax_local->pSwitch, 0x104));
    }

    
    // monitor WOL flag
    areg = NapaReadRegister(ax_local->pSwitch, WOLCR);
    if (areg & WOLCR_PME) {
        printk("WOL event, Reprogramming WOL");
        SitelTriggerWOL(ax_local->pSwitch, WCNT_VAL);
    }
#endif
    
	mod_timer(&ax_local->watchdog, jiffies + 2 * HZ);
}
/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_media_init()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void
ax_media_init(struct net_device *dev, int media, u8 phyID)
{
	int advertise;

	advertise = mdio_read(dev, phyID, MII_ADVERTISE) & ~(ADVERTISE_ALL);
	if(media == 0)
	{
		advertise |= (ADVERTISE_ALL | 0x400);
	}
	else if(media == 1)
	{
		advertise |= (ADVERTISE_100FULL | 0x400);
	}
	else if(media == 2)
	{
		advertise |= ADVERTISE_100HALF;
	}
	else if(media == 3)
	{
		advertise |= (ADVERTISE_10FULL | 0x400);
	}
	else if(media == 4)
	{
		advertise |= ADVERTISE_10HALF;
	}
	mdio_write(dev, phyID, MII_ADVERTISE, advertise);
}



/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_phy_init()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void
ax_phy_init(struct net_device *dev)
{
	LED_STURE Led;
	int Data16;
	PAX_PRIVATE ax_local = (PAX_PRIVATE)(dev->priv);

    SDBG(SDBG_INIT, ("=====> ax_phy_init\n"));

	printk("=====> ax_phy_init\n");
	
    NapaEnablePhy(ax_local->pSwitch, Port_0);
	NapaEnablePhy(ax_local->pSwitch, Port_1);

	NapaDelayMs(100);

	SDBG(SDBG_INIT, ("Port 0 Phy ID %d\n", ax_local->phyID[Port_0]));
	SDBG(SDBG_INIT, ("Port 1 Phy ID %d\n", ax_local->phyID[Port_1]));
	NapaSetPhyId(ax_local->pSwitch, Port_0, ax_local->phyID[Port_0]);
	NapaSetPhyId(ax_local->pSwitch, Port_1, ax_local->phyID[Port_1]);

#if 0

	// Speed up PHY link speed
	Data16 = mdio_read(dev, ax_local->phyID[Port_0], 16);
	mdio_write(dev, ax_local->phyID[Port_0], 16, (Data16 | 0x1000));

	Data16 = mdio_read(dev, ax_local->phyID[Port_1], 16);
	mdio_write(dev, ax_local->phyID[Port_1], 16, (Data16 | 0x1000));

	Data16 = mdio_read(dev, ax_local->phyID[Port_0], 24);
	mdio_write(dev, ax_local->phyID[Port_0], 24, (Data16 | 4));

	Data16 = mdio_read(dev, ax_local->phyID[Port_1], 24);
	mdio_write(dev, ax_local->phyID[Port_1], 24, (Data16 | 4));

	// Enable Phy Paue capability
	Data16 = mdio_read(dev, ax_local->phyID[Port_0], MII_ADVERTISE);
	mdio_write(dev, ax_local->phyID[Port_0], 4, (Data16 | 0x400));

	Data16 = mdio_read(dev, ax_local->phyID[Port_1], MII_ADVERTISE);
	mdio_write(dev, ax_local->phyID[Port_1], 4, (Data16 | 0x400));

	printk("Port0 Phy Reg4 = 0x%x\n",
		   mdio_read(dev, ax_local->phyID[Port_0], MII_ADVERTISE));
	printk("Port1 Phy Reg4 = 0x%x\n",
		   mdio_read(dev, ax_local->phyID[Port_1], MII_ADVERTISE));
#endif
#if 1
	
#if(PORT0_DEFAULT_PHY_INTERFACE == PHY_MODE_RMII)
	SDBG(SDBG_INIT, ("Port 0 Phy interface : RMII\n"));
	NapaSetPhyMode(ax_local->pSwitch, Port_0, PHY_MODE_RMII);
#else
	SDBG(SDBG_INIT, ("Port 0 Phy interface : MII\n"));
	NapaSetPhyMode(ax_local->pSwitch, Port_0, PHY_MODE_MII);
#endif

#if(PORT1_DEFAULT_PHY_INTERFACE == PHY_MODE_RMII)
	SDBG(SDBG_INIT, ("Port 1 Phy interface : RMII\n"));
	NapaSetPhyMode(ax_local->pSwitch, Port_1, PHY_MODE_RMII);
#else
	SDBG(SDBG_INIT, ("Port 1 Phy interface : MII\n"));
	NapaSetPhyMode(ax_local->pSwitch, Port_1, PHY_MODE_MII);
#endif

#endif

	Led.Led0_Mode = DEFAULT_PHY_LED0;
	Led.Led1_Mode = DEFAULT_PHY_LED1;
	Led.Led2_Mode = DEFAULT_PHY_LED2;
	NapaSetLedMode(ax_local->pSwitch, Port_0, &Led);
	NapaSetLedMode(ax_local->pSwitch, Port_1, &Led);

#if 0 // 1: enable polling
	NapaConfigPhyPolling(ax_local->pSwitch, Port_0, ENABLE);
	NapaConfigPhyPolling(ax_local->pSwitch, Port_1, ENABLE);
#else // disable polling
	NapaConfigPhyPolling(ax_local->pSwitch, Port_0, 0);
	NapaConfigPhyPolling(ax_local->pSwitch, Port_1, 0);
#endif


#if 1


		// Speed up PHY link speed
	Data16 = mdio_read(dev, ax_local->phyID[Port_0], 16);
	mdio_write(dev, ax_local->phyID[Port_0], 16, (Data16 | 0x1000));

	Data16 = mdio_read(dev, ax_local->phyID[Port_1], 16);
	mdio_write(dev, ax_local->phyID[Port_1], 16, (Data16 | 0x1000));

	Data16 = mdio_read(dev, ax_local->phyID[Port_0], 24);
	mdio_write(dev, ax_local->phyID[Port_0], 24, (Data16 | 4));

	Data16 = mdio_read(dev, ax_local->phyID[Port_1], 24);
	mdio_write(dev, ax_local->phyID[Port_1], 24, (Data16 | 4));
#endif

#if 1   //  100
	ax_media_init(dev, media0, ax_local->phyID[Port_0]);
	ax_media_init(dev, media1, ax_local->phyID[Port_1]);
#else   //  10
    ax_media_init(dev, 3, ax_local->phyID[Port_0]);
	ax_media_init(dev, 3, ax_local->phyID[Port_1]);
#endif

	mdio_write(dev, ax_local->phyID[Port_0], MII_BMCR, (BMCR_ANRESTART | BMCR_ANENABLE));
	mdio_write(dev, ax_local->phyID[Port_1], MII_BMCR, (BMCR_ANRESTART | BMCR_ANENABLE));
 

	SDBG(SDBG_INIT, ("<===== ax_phy_init\n"));
} /* End of ax_phy_init() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_board_init()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void
ax_board_init(struct net_device *dev)
{
	PAX_PRIVATE ax_local = (PAX_PRIVATE)(dev->priv);


	ax_phy_init(dev);

	printk("OPEN : after phy init\n");
	NapaSetPktOrder(ax_local->pSwitch, BORDER_LITTLE);

	NapaSetInterruptMask(ax_local->pSwitch, IMSR_MASK_AGAIN | IMSR_MASK_NEW_LEARN);

	NapaConfigLocalBusCpio(ax_local->pSwitch, ENABLE);

} /* End of ax_board_init() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_open()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static int
ax_open(struct net_device *dev)
{
	int err ;
	PAX_PRIVATE          ax_local = (PAX_PRIVATE)(dev->priv);

	SDBG(SDBG_DRIVER, ("=====> ax_open\n"));
 
	/* Init interrupt priority levels and keyboard interrupt setup */
#if !defined( CONFIG_CVM480_SPI_DECT_SUPPORT )
	//SetBits( INT3_PRIORITY_REG, KEYB_INT_PRIO, 7 ) ;
	KEY_BOARD_INT_REG = 0x0;
	KEY_DEBOUNCE_REG = 0x0;
#endif

	NapaDisableInterrupt(ax_local->pSwitch);

	/* int7 is used, it is set up by the bootloader? */
	P1_15_MODE_REG = 0x12A;
	SetBits(KEY_GP_INT_REG, INT7_CTRL, 5);	// int7 is an active high signal
	KEY_STATUS_REG = ETH_INT ;

	while( RESET_INT_PENDING_REG & 0x2 ) {
		KEY_STATUS_REG = ETH_INT ;
		RESET_INT_PENDING_REG = 0x2;
	}


	err = request_irq(dev->irq, &ax_interrupt, SA_SHIRQ, DRV_NAME, dev);
	
	if (err) {
		printk(KERN_ERR PFX "Unable to get IRQ %d (errno=%d).\n", dev->irq, err);
		return err;
	}


	NapaSetMacAddress(ax_local->pSwitch, Port_0, dev->dev_addr);
	NapaSetMacAddress(ax_local->pSwitch, Port_1, dev->dev_addr);
	
	ax_board_init(dev);
	
	ax_local->LinkStatus[0].Link   = LINK_UNKNOW;
        ax_local->LinkStatus[1].Link   = LINK_UNKNOW;
		
NapaEnableInterrupt(ax_local->pSwitch);

//	netif_start_queue(dev);

#if 1
	netif_carrier_off(dev);

	ax_local->LinkStatus[0].Link   = LINK_UNKNOW;
	ax_local->LinkStatus[1].Link   = LINK_UNKNOW;
	
	init_timer(&ax_local->watchdog);
	ax_local->watchdog.function = ax_link;
	ax_local->watchdog.expires = jiffies + 2 * HZ;
	ax_local->watchdog.data = (unsigned long)dev;
	add_timer(&ax_local->watchdog);
#endif
	
	SDBG(SDBG_DRIVER, ("<===== ax_open\n"));

	return 0;
} /* End of ax_open() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_open()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static int
ax_close(struct net_device *dev)
{
	PAX_PRIVATE          ax_local = (PAX_PRIVATE)(dev->priv);

	SDBG(SDBG_DRIVER, ("=====> ax_close\n"));

	del_timer(&ax_local->watchdog);

	NapaDisableInterrupt(ax_local->pSwitch);

	NapaChipReset(ax_local->pSwitch);

	netif_stop_queue(dev);

	free_irq( dev->irq, dev ) ;

	SDBG(SDBG_DRIVER, ("<===== ax_close\n"));

	return 0;
} /* End of ax_open() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_ioctl()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static int ax_ioctl(struct net_device *dev, struct ifreq *rq, int cmd)
{
	int ret = 0;
#if 0
	PAX_PRIVATE          ax_local = (PAX_PRIVATE)(dev->priv);

	struct mii_ioctl_data *data = (struct mii_ioctl_data *) & rq->ifr_data;

	if (!netif_running(dev))
		return -EINVAL;

	spin_lock_irq(&ax_local->lock);
	ret = generic_mii_ioctl(&ax_local->mii_if, data, cmd, NULL);
	spin_unlock_irq(&ax_local->lock);
#endif

	return ret;
} /* End of ax_ioctl() */


extern int	set_external_irq(int irq, int edge, int pullup);

/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_probe()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static int __init ax_probe(struct net_device *dev)
{
	PAX_PRIVATE          ax_local = (PAX_PRIVATE)(dev->priv);
	volatile void       *mem_base;
	u8                   BusMode;
	char * 		     tmp;

	SET_MODULE_OWNER(dev);

	printk(version);

	if(dev->base_addr == 0 ) {
		dev->base_addr = AX88783_BASE ;
	}
	printk("ETH BASE: 0x%lx\n", dev->base_addr);

	SDBG(SDBG_DRIVER, ("Using 0x%lx base address\n", dev->base_addr));

	if (check_mem_region(dev->base_addr , NAPA_IO_EXTENT))
		return -ENODEV;

	if (!request_mem_region(dev->base_addr, NAPA_IO_EXTENT, DRV_NAME))
		return -EBUSY;

	mem_base = ioremap_nocache(dev->base_addr, NAPA_IO_EXTENT);

	if (!mem_base) {
		printk(KERN_ERR PFX "Unable to remap memory.\n");
		return -EBUSY;
	}

	ax_local->pSwitch->pMemBase = mem_base;

	BusMode = NapaGetChipMode(ax_local->pSwitch);

#if 0
	if(BusMode == CR_CHIP_MODE_8BIT) {
		BWSCON = BWSCON & 0xFFFFFFCF;
	} else if(BusMode == CR_CHIP_MODE_16BIT) {
		BWSCON = (BWSCON & 0xFFFFFFCF) | 0x00000010;
	} else if(BusMode == CR_CHIP_MODE_32BIT) {
		BWSCON |=  0x20;
	} else {
		return -ENODEV;
	}
#endif
	NapaAccessPortInit(BusMode);

	mdelay(1000);

        if (NapaCheckChipInitDone (ax_local->pSwitch) != NAPA_STATUS_SUCCESS) {
                printk ("Chip not ready !");
                iounmap((void *)ax_local->pSwitch->pMemBase);
                release_mem_region(global_dev->base_addr, NAPA_IO_EXTENT);
        }


	NapaChipReset(ax_local->pSwitch);


//	NapaReloadEEProm(ax_local->pSwitch);

#if 0
	CLKDIVN = CLKDIVN_50MHZ;
	CAMDIVN = CAMDIVN_50MHZ;
	UBRDIV0 = UBRDIV0_50MHZ;
#endif

	if (dev->irq == 0)
		dev->irq = AX88783_IRQ ;

#if 0
		set_external_irq(IRQ_EINT11, EXT_LOWLEVEL, GPIO_PULLUP_EN);
		EINTMASK &= ~EINT11_MASK;
		EXTINT1 &= ~0xF000;
		EXTINT1 |= FLTEN11_LOWLEVEL;
#endif


	NapaConfigInterruptLevel(ax_local->pSwitch, 0);

	NapaDisableInterrupt(ax_local->pSwitch);

	dev->hard_start_xmit     =  ax_start_xmit;
	dev->get_stats           =  ax_get_stats;
	dev->open                =  ax_open;
	dev->stop                =  ax_close;
	dev->do_ioctl            =  ax_ioctl;
	dev->set_multicast_list  =  ax_set_multicast;

//	NapaGetMacAddress(ax_local->pSwitch, Port_0, dev->dev_addr);

//	if(dev->dev_addr[0] == 0 && dev->dev_addr[1] == 0 && 
//		dev->dev_addr[2] == 0 && dev->dev_addr[3] == 0 &&
//		dev->dev_addr[4] == 0 &&
//	dev->dev_addr[5] == 0)
        tmp = strstr(saved_command_line,"ethaddr=");
        if(tmp)
        {
                sscanf(tmp,"ethaddr=%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
                                &dev->dev_addr[0],&dev->dev_addr[1],&dev->dev_addr[2],
                                &dev->dev_addr[3],&dev->dev_addr[4],&dev->dev_addr[5]);
        }
	else
	{
		dev->dev_addr[0] = 0x00;
		dev->dev_addr[1] = 0x12;
		dev->dev_addr[2] = 0x34;
		dev->dev_addr[3] = 0x56;
		dev->dev_addr[4] = 0x78;
		dev->dev_addr[5] = 0x9a;
	}

	printk(KERN_INFO PFX "addr 0x%lx, irq %d, "
		"MAC addr %02X:%02X:%02X:%02X:%02X:%02X\n",
		dev->base_addr, dev->irq,
		dev->dev_addr[0], dev->dev_addr[1], dev->dev_addr[2],
		dev->dev_addr[3], dev->dev_addr[4], dev->dev_addr[5]);

	ax_local->phyID[Port_0] = PORT0_DEFAULT_PHY_ID;
	ax_local->phyID[Port_1] = PORT1_DEFAULT_PHY_ID;

	ax_local->mii_if.phy_id = ax_local->phyID[Port_0];
	ax_local->mii_if.phy_id_mask = 0x1F;;
	ax_local->mii_if.reg_num_mask = 0x1F;;
	ax_local->mii_if.dev = dev;
	ax_local->mii_if.mdio_read = mdio_read;
	ax_local->mii_if.mdio_write = mdio_write;

	spin_lock_init(&ax_local->lock);

	ether_setup(dev);

	return 0;

} /* End of ax_probe() */


#ifndef MODULE
/*
 * ----------------------------------------------------------------------------
 * Function Name: ax_kprobe
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
struct net_device * __init ax8878x_1n2p_kprobe(int unit)
{
	struct net_device *dev; //= alloc_ei_netdev();
	int err;
	PSWITCH        pSwitch;
	PAX_PRIVATE    ax_local;

	if (unit >0)
		return 0;
	dev= alloc_ei_netdev();

	printk("Reaching ax8878x_1n2p_kprobe \n");
	if(!dev) {
		printk(KERN_ERR PFX "Cannot register net device.\n");
		return ERR_PTR(-ENOMEM);
	}

	dev->priv = kzalloc(sizeof(AX_PRIVATE), GFP_KERNEL);
	if(!dev->priv){
		printk(KERN_ERR PFX "Cannot allocate dev->priv\n"); 
		return ERR_PTR(-ENOMEM);
	}
	pSwitch = kmalloc(sizeof(SWITCH), GFP_KERNEL);
	if(!pSwitch) {
		printk(KERN_ERR PFX "Could not allocate memory for private data.\n");
		return ERR_PTR(-ENOMEM);
	}

	ax_local = (PAX_PRIVATE)dev->priv;
	ax_local->pSwitch = pSwitch;

	sprintf(dev->name, "eth%d", unit);
	printk("ETH: Probing device %s\n",dev->name);
	netdev_boot_setup_check(dev);

	err = ax_probe(dev);

	if(register_netdev(dev) != 0) {
		printk("Can not register ethdev\n");
		return 0;
	}

	if (err)
		goto out;
	return dev;
out:
	printk("Device %s not OK\n",dev->name);
	free_netdev(dev);
	return ERR_PTR(err);
}
#endif



/*
 * ----------------------------------------------------------------------------
 * Function Name: init_module()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
int
init_module(void)
{
	PSWITCH        pSwitch;
	PAX_PRIVATE    ax_local;

	if(global_dev) {
		printk(KERN_ERR PFX "already initialized.\n");
		return -EBUSY;
	}

	pSwitch = kmalloc(sizeof(SWITCH), GFP_KERNEL);
	if(!pSwitch) {
		printk(KERN_ERR PFX "Could not allocate memory for private data.\n");
		return -ENOMEM;
	}

	global_dev = alloc_etherdev(sizeof(AX_PRIVATE));

	if(!global_dev) {
		printk(KERN_ERR PFX "Cannot registerz net device.\n");
		return -ENOMEM;
	}

	global_dev->irq = irq;
	global_dev->base_addr = mem;
	global_dev->init = ax_probe;

	ax_local = (PAX_PRIVATE)global_dev->priv;
	ax_local->pSwitch = pSwitch;

	if(register_netdev(global_dev) == 0) {
		return 0;
	}

	kfree(pSwitch);
	kfree(global_dev);

	if (mem != 0) {
		printk(KERN_ERR PFX "No NAPA card found at memory = %#x\n\r", mem);
	}
	else {
		printk(KERN_ERR PFX "You must supply \"mem=0xNNNNNNN\" value(s) for NAPA.\n\r");
	}
	return -ENXIO;
} /* End of init_module() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: cleanup_module()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
cleanup_module(void)
{
	PAX_PRIVATE ax_local = (PAX_PRIVATE)(global_dev->priv);
	if(global_dev) {
		free_irq (global_dev->irq, global_dev);
		release_mem_region(global_dev->base_addr, NAPA_IO_EXTENT);
		unregister_netdev(global_dev);
		kfree(ax_local->pSwitch);
		kfree(global_dev);
	}

} /* End of cleanup_module() */


#if defined(CONFIG_SC14450_AX8878x_1N2P_SRAM_LOW_POWER)


#define CRCPOLY 0xEDB88320
// #define CRCPOLY    0x04C11DB7
#define INITXOR  0xFFFFFFFF
#define FINALXOR 0xFFFFFFFF


/**
 * Computes the CRC32 of the buffer of the given length using the
 * (slow) bit-oriented approach
 */
int crc32_bitoriented(unsigned char *buffer, int length) {
    int i, j;
    u32 crcreg = INITXOR;
    
    for (j = 0; j < length; ++j) {
        unsigned char b = buffer[j];
        for (i = 0; i < 8; ++i) {
            if ((crcreg ^ b) & 1) {
                crcreg = (crcreg >> 1) ^ CRCPOLY;
            } else {
                crcreg >>= 1;
            }
            b >>= 1;
        }
    }

    return crcreg ^ FINALXOR;
}

/**
 * Computes the CRC32 of the buffer of the given length 
 * using the supplied crc_table
 */
int crc32_tabledriven(unsigned char *buffer, 
                      int length, 
                      u32 *crc_table) 
{
    int i;
    u32 crcreg = INITXOR;
        
    for (i = 0; i < length; ++i) {
        crcreg = (crcreg >> 8) ^ crc_table[((crcreg ^ buffer[i]) & 0xFF)];
    }
    
    return crcreg ^ FINALXOR;
}

u8 reverse_bytes( u8 cin ) {
	unsigned char cout=0;
    int i;
    
    unsigned char mask = 1;
    for (i=0;i<8;i++){
        cout = cout << 1;
        if (cin&mask) cout |= 0x1;
        mask = mask << 1;
    }
    printk("%02x --> %02X\n",cin,cout);

	return cout;
}
u32 reverse_bytes32( u32 cin ) {
	u32 cout=0;
    int i;
    
    u32 mask = 1;
    for (i=0;i<32;i++){
        cout = cout << 1;
        if (cin&mask) cout |= 0x1;
        mask = mask << 1;
    }
    printk("%02x --> %02X\n",cin,cout);

	return cout;
}



u32 crc_wol(struct net_device *dev, int type, int nbytes) {

    u32 crc32;
    u8 masked_bytes[20];
    u16 i;

    u8 ip_addr[4];
    struct in_device *in_dev    = (struct in_device *) dev->ip_ptr;
    struct in_ifaddr *ifa_list  = (struct in_ifaddr *) in_dev->ifa_list;


    ip_addr[0] = 0xFF&(ifa_list->ifa_address>>0);
    ip_addr[1] = 0xFF&(ifa_list->ifa_address>>8);
    ip_addr[2] = 0xFF&(ifa_list->ifa_address>>16);
    ip_addr[3] = 0xFF&(ifa_list->ifa_address>>24);


    if (type == 0) { // da mac match
        masked_bytes[0] = dev->dev_addr[0];
        masked_bytes[1] = dev->dev_addr[1];
        masked_bytes[2] = dev->dev_addr[2];
        masked_bytes[3] = dev->dev_addr[3];
        masked_bytes[4] = dev->dev_addr[4];
        masked_bytes[5] = dev->dev_addr[5];

        printk("mac bytes: "); for (i=0;i<nbytes;i++) printk(" %d ",masked_bytes[i]);
        crc32 = reverse_bytes32(crc32_bitoriented(masked_bytes,nbytes));
        printk("crc of mac bytes: %08X\n", crc32);
    } else if (type == 1) { // arp request
        masked_bytes[0] = (0x08);
        masked_bytes[1] = (0x06);
        masked_bytes[2] = (ip_addr[0]);
        masked_bytes[3] = (ip_addr[1]);
        masked_bytes[4] = (ip_addr[2]);
        masked_bytes[5] = (ip_addr[3]);    

        printk("arp bytes: "); for (i=0;i<nbytes;i++) printk(" %d ",masked_bytes[i]);
        crc32 = reverse_bytes32(crc32_bitoriented(masked_bytes,nbytes));
        printk("crc of arp bytes: %08X\n", crc32);

    } else if (type == 2) { // test
        masked_bytes[0] =  0xF0;
        masked_bytes[1] =  0xFF;
        masked_bytes[2] =  0xFF;
        masked_bytes[3] =  0xFF;
        masked_bytes[4] =  0xFF;
        masked_bytes[5] =  0xFF;

        printk("arp mask bytes: "); for (i=0;i<nbytes;i++) printk(" %d ",masked_bytes[i]);
        crc32 = reverse_bytes32(crc32_bitoriented(masked_bytes,nbytes));
        printk("crc of example_packet: %08X\n", crc32);
        
    }

    return crc32;

}

#endif
