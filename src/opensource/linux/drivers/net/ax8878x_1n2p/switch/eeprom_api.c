/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
 /*============================================================================
 * Module Name: eeprom_api.c
 * Purpose:
 * Author:
 * Date:
 * Notes:
 * $Log: eeprom_api.c,v $
 * no message
 *
 *
 *=============================================================================
 */

/* INCLUDE FILE DECLARATIONS */
#include "switch.h"

/* NAMING CONSTANT DECLARATIONS */

/* GLOBAL VARIABLES DECLARATIONS */

/* LOCAL VARIABLES DECLARATIONS */

/* LOCAL SUBPROGRAM DECLARATIONS */
static void eeprom_write (SWITCH *pSwitch, unsigned char Mode,
			unsigned long OpCode, unsigned long Addr, unsigned char Data);

static NAPA_STATUS eeprom_check (SWITCH *pSwitch);

/*
 * ----------------------------------------------------------------------------
 * Function Name: eeprom_write()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaReloadEEProm (SWITCH *pSwitch)
{

	NapaWriteRegister (pSwitch, BLCR, BLCR_RELOAD);

	while((NapaReadRegister (pSwitch, BLCR) & BLCR_DONE) == 0);

} /* End of eeprom_write() */

/*
 * ----------------------------------------------------------------------------
 * Function Name: eeprom_write()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void
eeprom_write (SWITCH *pSwitch, unsigned char Mode, unsigned long OpCode,
			 unsigned long Addr, unsigned char Data)
{
	unsigned long TmpLong;

	TmpLong = 0;

	if((OpCode == EEPROM_ERASE) || (OpCode == EEPROM_WRITE))
	{
		TmpLong = EECR_CHK;
	}

	if (Mode == EEPROM_MODE_93C46)
	{
		TmpLong |= EECR_REQ | EEPROM_93C46_STARTBIT | EEPROM_93C46_OPCODE(OpCode) |
			  EECR_ADDR(Addr) | Data | EECR_MODE(EEPROM_MODE_93C46);
	}
	else if (Mode == EEPROM_MODE_93C56)
	{
		TmpLong = EECR_REQ | EEPROM_93C56_STARTBIT | EEPROM_93C56_OPCODE(OpCode) |
			  EECR_ADDR(Addr) | Data | EECR_MODE(EEPROM_MODE_93C56);

	}
	else if (Mode == EEPROM_MODE_93C66)
	{
		TmpLong |= EECR_REQ | EEPROM_93C66_STARTBIT | EEPROM_93C66_OPCODE(OpCode) |
			  EECR_ADDR(Addr) | Data | EECR_MODE(EEPROM_MODE_93C66);
	}
	else if (Mode == EEPROM_MODE_93C76)
	{
		TmpLong |= EECR_REQ | EEPROM_93C76_STARTBIT | EEPROM_93C76_OPCODE(OpCode) |
			  EECR_ADDR(Addr) | Data | EECR_MODE(EEPROM_MODE_93C76);
	}
	else
	{
		TmpLong |= EECR_REQ | EEPROM_93C86_STARTBIT | EEPROM_93C86_OPCODE(OpCode) |
			  EECR_ADDR(Addr) | Data | EECR_MODE(EEPROM_MODE_93C86);
	}

	NapaWriteRegister (pSwitch, EECR, TmpLong);

} /* End of eeprom_write() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: eeprom_check()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static NAPA_STATUS
eeprom_check (SWITCH *pSwitch)
{
	unsigned long TmpLong;
	unsigned long Count = 0;

	do {

		if(++Count == 100000)
		{
			SDBG(SDBG_EEPROM, ("eeprom_check : EEPROM not valid, aborting...\n\r"));
			return NAPA_STATUS_FAILURE;
		}

		TmpLong = NapaReadRegister (pSwitch, EECR);

		if((Count % 10000) == 0)
			NapaDelayMs (1);

	} while((TmpLong & EECR_VALID) == 0);

	return NAPA_STATUS_SUCCESS;

} /* End of eeprom_check() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadEEPromData()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_STATUS
NapaReadEEPromData (SWITCH *pSwitch, unsigned char Mode, EEPROM_STURE *pData)
{
	unsigned long   TmpLong;
	unsigned long   Value;
	unsigned long   i;
	unsigned short  MaxNum;
	unsigned short  Loop;

	SDBG(SDBG_EEPROM, ("=====> NapaReadEEPromData\n\r"));

	SDBG(SDBG_EEPROM, ("EEPROM Type %d\n", Mode));

	MaxNum = EEPROM_MAX_NUM(Mode);
	Loop = MaxNum * 5;
	pData->Num = MaxNum;
	SDBG(SDBG_EEPROM, ("MaxNum %d\n", MaxNum));

	Value = 0;

	for (i = 0; i < Loop; i++)
	{
		eeprom_write (pSwitch, Mode, EEPROM_READ, i, 0);

		if (eeprom_check (pSwitch) != NAPA_STATUS_SUCCESS)
		{
			SDBG(SDBG_EEPROM, ("Timed out on waiting for EEPROM data valid\n"));
			SDBG(SDBG_EEPROM, ("<===== NapaReadEEPromData\n\r"));
			return NAPA_STATUS_FAILURE;
		}

		TmpLong = NapaReadRegister (pSwitch, EECR);

		if ( (i % 5) == 0 )
		{
			unsigned short	TmpAddr = 0;
			TmpAddr |= (unsigned char)TmpLong;
			TmpAddr = TmpAddr << 2;

			pData->Addr[i / 5] = TmpAddr;
			Value = 0;
			SDBG(SDBG_EEPROM, ("EEPROM%4.4d ", TmpAddr));
		} else if ( (i % 5) == 1 ) {
			Value = (TmpLong << 24) & 0xFF000000;
		} else if ( (i % 5) == 2 ) {
			Value |= (TmpLong << 16) & 0x00FF0000;
		} else if ( (i % 5) == 3 ) {
			Value |= (TmpLong << 8) & 0x0000FF00;
		} else if ( (i % 5) == 4 ) {
			Value |= TmpLong & 0x000000FF;
			pData->Data[i / 5] = Value;
			SDBG(SDBG_EEPROM, ("Data = %8.8lx", Value));
		}
	}

	SDBG(SDBG_EEPROM, ("<===== NapaReadEEPromData\n\r"));
	return NAPA_STATUS_SUCCESS;

} /* End of NapaReadEEPromData() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaWriteEEPromData()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_STATUS
NapaWriteEEPromData (SWITCH *pSwitch, unsigned char Mode, EEPROM_STURE *pData)
{
	unsigned char    Value;
	unsigned short   Loop;
	unsigned long    i;
	unsigned long    EWen_Addr, EWds_Addr;

	SDBG(SDBG_EEPROM, ("=====> NapaWriteEEPromData\n"));

	Loop = EEPROM_MAX_NUM(Mode) * 5;

	if (Mode == EEPROM_MODE_93C46)
	{
		EWen_Addr = EEPROM_EWEN_93C46_ADDR;
		EWds_Addr = EEPROM_EWDS_93C46_ADDR;
		SDBG(SDBG_EEPROM, ("EEPROM type 93C46\n"));
	}
	else if (Mode == EEPROM_MODE_93C56)
	{
		EWen_Addr = EEPROM_EWEN_93C56_ADDR;
		EWds_Addr = EEPROM_EWDS_93C56_ADDR;
		SDBG(SDBG_EEPROM, ("EEPROM type 93C56\n"));
	}
	else if (Mode == EEPROM_MODE_93C66)
	{
		EWen_Addr = EEPROM_EWEN_93C66_ADDR;
		EWds_Addr = EEPROM_EWDS_93C66_ADDR;
		SDBG(SDBG_EEPROM, ("EEPROM type 93C66\n"));
	}
	else if (Mode == EEPROM_MODE_93C76)
	{
		EWen_Addr = EEPROM_EWEN_93C76_ADDR;
		EWds_Addr = EEPROM_EWDS_93C76_ADDR;
		SDBG(SDBG_EEPROM, ("EEPROM type 93C76\n"));
	}
	else if (Mode == EEPROM_MODE_93C86)
	{
		EWen_Addr = EEPROM_EWEN_93C86_ADDR;
		EWds_Addr = EEPROM_EWDS_93C86_ADDR;
		SDBG(SDBG_EEPROM, ("EEPROM type 93C86\n"));
	}
	else
	{
		SDBG(SDBG_EEPROM, ("NAPA doesn't support this EEPROM module %2.2d\n\r", Mode));
		SDBG(SDBG_EEPROM, ("<===== NapaWriteEEPromData\n\r"));
		return NAPA_STATUS_NOT_SUPPORT;
	}

	// Enable EEPROM write
	eeprom_write (pSwitch, Mode, EEPROM_EWEN, EWen_Addr, 0);
	if (eeprom_check (pSwitch) != NAPA_STATUS_SUCCESS)
	{
		SDBG(SDBG_EEPROM, ("<===== NapaWriteEEPromData\n\r"));
		return NAPA_STATUS_FAILURE;
	}

	for(i = 0; i < Loop; i++)
	{	
		if ( (i % 5) == 0 ) 
		{
			Value = (unsigned char)(pData->Addr[i / 5] >> 2);
			eeprom_write (pSwitch, Mode, EEPROM_WRITE, i, Value);
			if (eeprom_check (pSwitch) != NAPA_STATUS_SUCCESS)
				return NAPA_STATUS_FAILURE;
			SDBG(SDBG_EEPROM, ("NO-%2.2ld, addr:%4.4x, Data:", (i/5), Value));

		} else if ( (i % 5) == 1 ) {
			Value = (unsigned char)(pData->Data[i / 5] >> 24);
			eeprom_write (pSwitch, Mode, EEPROM_WRITE, i, Value);
			if (eeprom_check (pSwitch) != NAPA_STATUS_SUCCESS)
			{
				SDBG(SDBG_EEPROM, ("<===== NapaWriteEEPromData\n\r"));
				return NAPA_STATUS_FAILURE;
			}
			SDBG(SDBG_EEPROM, ("%2.2x ", Value));

		} else if ( (i % 5) == 2 ) {
			Value = (unsigned char)(pData->Data[i / 5] >> 16);
			eeprom_write (pSwitch, Mode, EEPROM_WRITE, i, Value);
			if (eeprom_check (pSwitch) != NAPA_STATUS_SUCCESS)
			{
				SDBG(SDBG_EEPROM, ("<===== NapaWriteEEPromData\n\r"));
				return NAPA_STATUS_FAILURE;
			}
			SDBG(SDBG_EEPROM, ("%2.2x ", Value));

		} else if ( (i % 5) == 3 ) {
			Value = (unsigned char)(pData->Data[i / 5] >> 8);
			eeprom_write (pSwitch, Mode, EEPROM_WRITE, i, Value);
			if (eeprom_check (pSwitch) != NAPA_STATUS_SUCCESS)
			{
				SDBG(SDBG_EEPROM, ("<===== NapaWriteEEPromData\n\r"));
				return NAPA_STATUS_FAILURE;
			}
			SDBG(SDBG_EEPROM, ("%2.2x ", Value));

		} else {
			Value = (unsigned char)(pData->Data[i / 5]);
			eeprom_write (pSwitch, Mode, EEPROM_WRITE, i, Value);
			if (eeprom_check (pSwitch) != NAPA_STATUS_SUCCESS)
			{
				SDBG(SDBG_EEPROM, ("<===== NapaWriteEEPromData\n\r"));
				return NAPA_STATUS_FAILURE;
			}
			SDBG(SDBG_EEPROM, ("%2.2x \n\r", Value));

		}
	}

	// Disable EEPROM write
	eeprom_write (pSwitch, Mode, EEPROM_EWDS, EWds_Addr, 0);

	SDBG(SDBG_EEPROM, ("<===== NapaWriteEEPromData\n\r"));

	return NAPA_STATUS_SUCCESS;

} /* End of NapaWriteEEPromData() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaEreaseEEProm()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_STATUS
NapaEreaseEEProm (SWITCH *pSwitch, unsigned char Mode)
{
	unsigned long    EWen_Addr, EWds_Addr, ERAll_Addr;

	SDBG(SDBG_EEPROM, ("=====> NapaEreaseEEProm\n\r"));

	if (Mode == EEPROM_MODE_93C46)
	{
		EWen_Addr = EEPROM_EWEN_93C46_ADDR;
		EWds_Addr = EEPROM_EWDS_93C46_ADDR;
		ERAll_Addr = EEPROM_ERALL_93C46_ADDR;
	}
	else if (Mode == EEPROM_MODE_93C56)
	{
		EWen_Addr = EEPROM_EWEN_93C56_ADDR;
		EWds_Addr = EEPROM_EWDS_93C56_ADDR;
		ERAll_Addr = EEPROM_ERALL_93C56_ADDR;
	}
	else if (Mode == EEPROM_MODE_93C66)
	{
		EWen_Addr = EEPROM_EWEN_93C66_ADDR;
		EWds_Addr = EEPROM_EWDS_93C66_ADDR;
		ERAll_Addr = EEPROM_ERALL_93C66_ADDR;
	}
	else if (Mode == EEPROM_MODE_93C76)
	{
		EWen_Addr = EEPROM_EWEN_93C76_ADDR;
		EWds_Addr = EEPROM_EWDS_93C76_ADDR;
		ERAll_Addr = EEPROM_ERALL_93C76_ADDR;
	}
	else if (Mode == EEPROM_MODE_93C86)
	{
		EWen_Addr = EEPROM_EWEN_93C86_ADDR;
		EWds_Addr = EEPROM_EWDS_93C86_ADDR;
		ERAll_Addr = EEPROM_ERALL_93C86_ADDR;
	}
	else
	{
		SDBG(SDBG_EEPROM, ("NAPA doesn't support this EEPROM module %2.2d\n\r", Mode));
		SDBG(SDBG_EEPROM, ("<===== NapaEreaseEEProm\n\r"));
		return NAPA_STATUS_NOT_SUPPORT;
	}

	// Enable EEPROM write
	eeprom_write (pSwitch, Mode, EEPROM_EWEN, EWen_Addr, 0);
	if (eeprom_check (pSwitch) != NAPA_STATUS_SUCCESS)
	{
		SDBG(SDBG_EEPROM, ("<===== NapaEreaseEEProm\n\r"));
		return NAPA_STATUS_FAILURE;
	}

	// Erease EEPROM data
	eeprom_write (pSwitch, Mode, EEPROM_ERALL, ERAll_Addr, 0);
	if (eeprom_check (pSwitch) != NAPA_STATUS_SUCCESS)
	{
		SDBG(SDBG_EEPROM, ("<===== NapaEreaseEEProm\n\r"));
		return NAPA_STATUS_FAILURE;
	}

	// Disable EEPROM write
	eeprom_write (pSwitch, Mode, EEPROM_EWDS, EWds_Addr, 0);

	SDBG(SDBG_EEPROM, ("<===== NapaEreaseEEProm\n\r"));

	return NAPA_STATUS_SUCCESS;

}  /* End of NapaEreaseEEProm() */
