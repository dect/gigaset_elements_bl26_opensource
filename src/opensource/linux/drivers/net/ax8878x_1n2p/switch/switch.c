/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
 /*============================================================================
 * Module Name: switch.c
 * Purpose:
 * Author:
 * Date:
 * Notes:
 * $Log: switch.c,v $
 * no message
 *
 *
 *=============================================================================
 */

/* INCLUDE FILE DECLARATIONS */
#include "switch.h"

/* NAMING CONSTANT DECLARATIONS */

/* GLOBAL VARIABLES DECLARATIONS */

/* LOCAL VARIABLES DECLARATIONS */

/* LOCAL SUBPROGRAM DECLARATIONS */

// Initialize APIs
/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaGetMacAddress()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaGetMacAddress(SWITCH *pSwitch, PORT Port, unsigned char *pMac)
{
	unsigned char  MacTmp[4];
	unsigned long *pTmpLong = (unsigned long *)MacTmp;
	unsigned short Offset;
	
	Offset = (Port == Port_0) ? P0MAC0 : P1MAC0;
	
	*pTmpLong = NapaReadRegister(pSwitch, Offset);

	pMac[5] = MacTmp[0];
	pMac[4] = MacTmp[1];
	pMac[3] = MacTmp[2];
	pMac[2] = MacTmp[3];

	Offset += 4;
	*pTmpLong = NapaReadRegister(pSwitch, Offset);

	pMac[1] = MacTmp[0];
	pMac[0] = MacTmp[1];

	SDBG(SDBG_INIT, ("Port%d MAC[%x-%x-%x-%x-%x-%x]\n\r", Port,
		pMac[0], pMac[1], pMac[2], pMac[3], pMac[4], pMac[5]));

} /* End of NapaGetMacAddress() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaSetMacAddress()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaSetMacAddress(SWITCH *pSwitch, PORT Port, unsigned char *pMac)
{
	unsigned char  MacTmp[4];
	unsigned long *pTmpLong = (unsigned long *)MacTmp;
	unsigned short Offset;
	FORWARDING_TABLE_STRUE FtMac;

	Offset = Port == Port_0 ? P0MAC0 : P1MAC0;

	// set Port0 MAC address
	MacTmp[0] = pMac[5];
	MacTmp[1] = pMac[4];
	MacTmp[2] = pMac[3];
	MacTmp[3] = pMac[2];

	NapaWriteRegister(pSwitch, Offset, *pTmpLong);

	Offset += 4;
	MacTmp[0] = pMac[1];
	MacTmp[1] = pMac[0];

	NapaWriteRegister(pSwitch, Offset, *pTmpLong);

	SDBG(SDBG_INIT, ("Write Port%d MAC[%02x-%02x-%02x-%02x-%02x-%02x]\n\r",
		Port, pMac[0], pMac[1], pMac[2], pMac[3], pMac[4], pMac[5]));

	FtMac.FilterDa = DISABLE;
	FtMac.FilterSa = DISABLE;
	FtMac.IsStatic = ENABLE;
	FtMac.Page = 0;
	FtMac.SrcPort = Port_2;
	FtMac.Mac[0] = pMac[0];
	FtMac.Mac[1] = pMac[1];
	FtMac.Mac[2] = pMac[2];
	FtMac.Mac[3] = pMac[3];
	FtMac.Mac[4] = pMac[4];
	FtMac.Mac[5] = pMac[5];

	NapaWriteForwardingEntry(pSwitch, &FtMac);

} /* End of NapaSetMacAddress() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaGetChipMode()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned char
NapaGetChipMode(SWITCH *pSwitch)
{
	unsigned char Tmp;

	Tmp = *((unsigned char *)pSwitch->pMemBase);

	return Tmp & CR_CHIP_MODE_MASK;
	

} /* End of NapaGetChipMode() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaGetChipRevision ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned char
NapaGetChipRevision (SWITCH *pSwitch)
{
	unsigned char ChipRevision;

	ChipRevision = (unsigned char)((NapaReadRegister (pSwitch, CR) & CR_CHIP_REVISION_MASK) >> 8);

	return ChipRevision;
	

} /* End of NapaGetChipRevision() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaChipReset()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaChipReset(SWITCH *pSwitch)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, CR);

	NapaWriteRegister(pSwitch, CR, (TmpLong & ~CR_CHIP_RESET));

	NapaDelayMs(1);

	NapaWriteRegister(pSwitch, CR, (TmpLong | CR_CHIP_RESET));

} /* End of NapaChipReset() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaLocalBusCpiReset()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaLocalBusCpiReset(SWITCH *pSwitch)
{
	unsigned long  TmpLong;

	TmpLong = NapaReadRegister(pSwitch, CR);

	TmpLong &= ~CR_CPI_RESET;
	
	NapaWriteRegister(pSwitch, CR, TmpLong);

} /* End of NapaLocalBusCpiReset() */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaCheckChipInitDone()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_STATUS
NapaCheckChipInitDone (SWITCH *pSwitch)
{
	unsigned long  TmpLong;

	TmpLong = NapaReadRegister(pSwitch, CR);

	if (TmpLong & CR_CHIP_INIT_DONE)
		return NAPA_STATUS_SUCCESS;
	else
		return NAPA_STATUS_FAILURE;

} /* End of NapaCheckChipInitDone() */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaLocalBusCpoReset()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaLocalBusCpoReset(SWITCH *pSwitch)
{
	unsigned long  TmpLong;

	TmpLong = NapaReadRegister(pSwitch, CR);

	TmpLong &= ~CR_CPO_RESET;
	
	NapaWriteRegister(pSwitch, CR, TmpLong);

} /* End of NapaLocalBusCpoReset() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigLocalBusCpi()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigLocalBusCpio(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long  TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	
	if(Set)
		TmpLong |= L2PSR_CPIO_ON;
	else
		TmpLong &= ~L2PSR_CPIO_ON;
		
	NapaWriteRegister(pSwitch, L2PSR, TmpLong);

} /* End of NapaConfigLocalBusCpi() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigBroadcastPortMap ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigBroadcastPortMap (SWITCH *pSwitch, PORTMAP_STURE *pPortMap)
{
	unsigned long  TmpLong;

	TmpLong = NapaReadRegister (pSwitch, L2FCR);

	if (pPortMap->Port0)
		TmpLong |= L2FCR_BROADCAST_ON (0);
	else
		TmpLong &= ~L2FCR_BROADCAST_ON (0);

	if (pPortMap->Port1)
		TmpLong |= L2FCR_BROADCAST_ON (1);
	else
		TmpLong &= ~L2FCR_BROADCAST_ON (1);

	if (pPortMap->Port2)
		TmpLong |= L2FCR_BROADCAST_ON (2);
	else
		TmpLong &= ~L2FCR_BROADCAST_ON (2);

	NapaWriteRegister (pSwitch, L2FCR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigFloodPortMap ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigFloodPortMap (SWITCH *pSwitch, PORTMAP_STURE *pPortMap)
{
	unsigned long  TmpLong;

	TmpLong = NapaReadRegister (pSwitch, L2FCR);

	if (pPortMap->Port0)
		TmpLong |= L2FCR_FLOOD_ON (0);
	else
		TmpLong &= ~L2FCR_FLOOD_ON (0);

	if (pPortMap->Port1)
		TmpLong |= L2FCR_FLOOD_ON (1);
	else
		TmpLong &= ~L2FCR_FLOOD_ON (1);

	if (pPortMap->Port2)
		TmpLong |= L2FCR_FLOOD_ON (2);
	else
		TmpLong &= ~L2FCR_FLOOD_ON (2);

	NapaWriteRegister (pSwitch, L2FCR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigMulticastPortMap ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigMulticastPortMap (SWITCH *pSwitch, PORTMAP_STURE *pPortMap)
{
	unsigned long  TmpLong;

	TmpLong = NapaReadRegister (pSwitch, L2FCR);

	if (pPortMap->Port0)
		TmpLong |= L2FCR_MULTICAST_ON (0);
	else
		TmpLong &= ~L2FCR_MULTICAST_ON (0);

	if (pPortMap->Port1)
		TmpLong |= L2FCR_MULTICAST_ON (1);
	else
		TmpLong &= ~L2FCR_MULTICAST_ON (1);

	if (pPortMap->Port2)
		TmpLong |= L2FCR_MULTICAST_ON (2);
	else
		TmpLong &= ~L2FCR_MULTICAST_ON (2);

	NapaWriteRegister (pSwitch, L2FCR, TmpLong);
}

#if 1
/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaSetRxFilter ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaSetRxFilter (SWITCH *pSwitch, PORT Port, unsigned char Filter)
{
	unsigned long  TmpLong;
	unsigned short Offset;


	Offset = Port == Port_0 ? P0MCR : P1MCR;

	TmpLong = NapaReadRegister(pSwitch, Offset) | MCR_FLOWCTRL_ON | MCR_CRC_CHECK;

	if(Filter == RX_PROMISCUOUS)
		TmpLong &= ~MCR_DA_MATCH;
	else
		TmpLong |= MCR_DA_MATCH;

	NapaWriteRegister(pSwitch, Offset, TmpLong);
} /* End of NapaSetRxFilter() */
#endif

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigSwitchCore()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigSwitchCore(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long  TmpLong;

	TmpLong = NapaReadRegister(pSwitch, CR);

	/* Switch Core reset active low */
	if(Set)
		TmpLong |= CR_SWITCH_CORE_RESET;
	else
		TmpLong &= ~CR_SWITCH_CORE_RESET;

	NapaWriteRegister(pSwitch, CR, TmpLong);

	if (Set)
	{
		while (1)
		{
			TmpLong = NapaReadRegister(pSwitch, CR);
			if (TmpLong & CR_CHIP_INIT_DONE)
				break;
		}
	}
} /* End of NapaConfigSwitchCore() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigMac()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigMac(SWITCH *pSwitch, PORT Port, NAPA_CONFIG Set)
{
	unsigned long  TmpLong;
	unsigned short Offset;

	if (Port == Port_0)
		Offset = P0MCR;
	else if (Port == Port_1)
		Offset = P1MCR;
	else
		Offset = P2MCR;

	/* Always turn on the flow control */
	TmpLong = NapaReadRegister(pSwitch, Offset) | MCR_FLOWCTRL_ON | MCR_CRC_CHECK;

	if(Set)
		TmpLong &= ~MCR_MAC_ON;
	else
		TmpLong |= MCR_MAC_ON;

	NapaWriteRegister(pSwitch, Offset, TmpLong);

} /* End of NapaConfigMac() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigMacRx()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigMacRx(SWITCH *pSwitch, PORT Port, NAPA_CONFIG Set)
{
	unsigned long  TmpLong;
	unsigned short Offset;

	if (Port == Port_0)
		Offset = P0MCR;
	else if (Port == Port_1)
		Offset = P1MCR;
	else
		Offset = P2MCR;

	/* Always turn on the flow control */
	TmpLong = NapaReadRegister(pSwitch, Offset) | MCR_FLOWCTRL_ON | MCR_CRC_CHECK;

	if(Set)
		TmpLong &= ~MCR_MCR_RX_STOP;
	else
		TmpLong |= MCR_MCR_RX_STOP;

	NapaWriteRegister(pSwitch, Offset, TmpLong);

} /* End of NapaConfigMacRx() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigMacTx()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigMacTx(SWITCH *pSwitch, PORT Port, NAPA_CONFIG Set)
{
	unsigned long  TmpLong;
	unsigned short Offset;

	if (Port == Port_0)
		Offset = P0MCR;
	else if (Port == Port_1)
		Offset = P1MCR;
	else
		Offset = P2MCR;

	/* Always turn on the flow control */
	TmpLong = NapaReadRegister(pSwitch, Offset) | MCR_FLOWCTRL_ON | MCR_CRC_CHECK;

	if(Set)
		TmpLong &= ~MCR_MCR_TX_STOP;
	else
		TmpLong |= MCR_MCR_TX_STOP;

	NapaWriteRegister(pSwitch, Offset, TmpLong);

} /* End of NapaConfigMacTx() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigPciFifo()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigPciFifo(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long  TmpLong;

	TmpLong = NapaReadRegister(pSwitch, GMCR);
	
	if(Set)
		TmpLong |= GMCR_PCI_FIFO_ON;
	else
		TmpLong &= ~GMCR_PCI_FIFO_ON;
	
	NapaWriteRegister(pSwitch, GMCR, TmpLong);

} /* End of NapaConfigPciFifo() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaPciSetTxDesc()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaPciSetTxDesc(SWITCH *pSwitch, unsigned long PhyAddr)
{

	NapaWriteRegister(pSwitch, DMATXDESCPA, PhyAddr);

} /* End of NapaPciSetTxDesc() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaPciSetRxDesc()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaPciSetRxDesc(SWITCH *pSwitch, unsigned long  PhyAddr)
{

	NapaWriteRegister(pSwitch, DMARXDESCPA, PhyAddr);

} /* End of NapaPciSetRxDesc() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaPciReloadTxDesc()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaPciReloadTxDesc(SWITCH *pSwitch, unsigned long PhyAddr)
{

	NapaWriteRegister(pSwitch, DMATXCR, DMACR_STOP);
	NapaWriteRegister(pSwitch, DMATXDESCPA, PhyAddr);
	NapaWriteRegister(pSwitch, DMATXCR, DMACR_STOP | DMACR_RELOAD);

} /* End of NapaPciReloadTxDesc() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaPciReloadRxDesc()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaPciReloadRxDesc(SWITCH *pSwitch, unsigned long PhyAddr)
{

	NapaWriteRegister(pSwitch, DMARXCR, DMACR_STOP);
	NapaWriteRegister(pSwitch, DMARXDESCPA, PhyAddr);
	NapaWriteRegister(pSwitch, DMARXCR, DMACR_STOP | DMACR_RELOAD);

} /* End of NapaPciReloadRxDesc() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaPciTxStart()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaPciTxStart(SWITCH *pSwitch)
{

	NapaWriteRegister(pSwitch, DMATXCR, DMACR_START);

} /* End of NapaPciTxStart() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaPciTxStop()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaPciTxStop(SWITCH *pSwitch)
{
	unsigned long  TmpLong;

	NapaWriteRegister(pSwitch, DMATXCR, DMACR_STOP);

	while (1) {
		TmpLong = NapaReadRegister(pSwitch, DMATXCR);
		if ((TmpLong & DMACR_PKT_NBUSY) || (TmpLong & DMACR_IDLE))
			break;
	}

} /* End of NapaPciTxStop() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaPciRxStart()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void 
NapaPciRxStart(SWITCH *pSwitch)
{

	NapaWriteRegister(pSwitch, DMARXCR, DMACR_START);

} /* End of NapaPciRxStart() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaPciRxStop()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaPciRxStop(SWITCH *pSwitch)
{

	unsigned long  TmpLong;

	NapaWriteRegister(pSwitch, DMARXCR, DMACR_STOP);

	while (1) {
		TmpLong = NapaReadRegister(pSwitch, DMARXCR);
		if ((TmpLong & DMACR_PKT_NBUSY) || (TmpLong & DMACR_IDLE))
			break;
	}

} /* End of NapaPciRxStop() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigTcpOffload()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void 
NapaConfigTcpOffload(SWITCH *pSwitch, NAPA_CONFIG Inject, NAPA_CONFIG Drop)
{
	unsigned long  TmpLong;

	TmpLong = NapaReadRegister(pSwitch, TOCR);
	
	if(Inject)
		TmpLong |= TOCR_INJECT_TCP_CKECKSUM;
	else
		TmpLong &= ~TOCR_INJECT_TCP_CKECKSUM;
	
	if(Drop)
		TmpLong |= TOCR_DROP_ERR_TCP_CHECKSUM;
	else
		TmpLong &= ~TOCR_DROP_ERR_TCP_CHECKSUM;

	NapaWriteRegister(pSwitch, TOCR, TmpLong);

} /* End of NapaConfigTcpOffload() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigIpOffload()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void 
NapaConfigIpOffload(SWITCH *pSwitch, NAPA_CONFIG Inject, NAPA_CONFIG Drop)
{
	unsigned long  TmpLong;

	TmpLong = NapaReadRegister(pSwitch, TOCR);
	
	if(Inject)
		TmpLong |= TOCR_INJECT_IP_CKECKSUM;
	else
		TmpLong &= ~TOCR_INJECT_IP_CKECKSUM;
	
	if(Drop)
		TmpLong |= TOCR_DROP_ERR_IP_CHECKSUM;
	else
		TmpLong &= ~TOCR_DROP_ERR_IP_CHECKSUM;

	NapaWriteRegister(pSwitch, TOCR, TmpLong);

} /* End of NapaConfigIpOffload() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigUdpOffload()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigUdpOffload(SWITCH *pSwitch, NAPA_CONFIG Inject, NAPA_CONFIG Drop)
{
	unsigned long  TmpLong;

	TmpLong = NapaReadRegister(pSwitch, TOCR);
	
	if(Inject)
		TmpLong |= TOCR_INJECT_UDP_CKECKSUM;
	else
		TmpLong &= ~TOCR_INJECT_UDP_CKECKSUM;
	
	if(Drop)
		TmpLong |= TOCR_DROP_ERR_UDP_CHECKSUM;
	else
		TmpLong &= ~TOCR_DROP_ERR_UDP_CHECKSUM;

	NapaWriteRegister(pSwitch, TOCR, TmpLong);

} /* End of NapaConfigUdpOffload() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigIcmpOffload()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigIcmpOffload(SWITCH *pSwitch, NAPA_CONFIG Inject, NAPA_CONFIG Drop)
{
	unsigned long  TmpLong;

	TmpLong = NapaReadRegister(pSwitch, TOCR);
	
	if(Inject)
		TmpLong |= TOCR_INJECT_ICMP_CKECKSUM;
	else
		TmpLong &= ~TOCR_INJECT_ICMP_CKECKSUM;
	
	if(Drop)
		TmpLong |= TOCR_DROP_ERR_ICMP_CHECKSUM;
	else
		TmpLong &= ~TOCR_DROP_ERR_ICMP_CHECKSUM;

	NapaWriteRegister(pSwitch, TOCR, TmpLong);

} /* End of NapaConfigIcmpOffload() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigIgmpOffload()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigIgmpOffload(SWITCH *pSwitch, NAPA_CONFIG Inject, NAPA_CONFIG Drop)
{
	unsigned long  TmpLong;

	TmpLong = NapaReadRegister(pSwitch, TOCR);
	
	if(Inject)
		TmpLong |= TOCR_INJECT_IGMP_CKECKSUM;
	else
		TmpLong &= ~TOCR_INJECT_IGMP_CKECKSUM;
	
	if(Drop)
		TmpLong |= TOCR_DROP_ERR_IGMP_CHECKSUM;
	else
		TmpLong &= ~TOCR_DROP_ERR_IGMP_CHECKSUM;

	NapaWriteRegister(pSwitch, TOCR, TmpLong);

} /* End of NapaConfigIgmpOffload() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigPPPoEOffload()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigPPPoEOffload(SWITCH *pSwitch, NAPA_CONFIG Inject, NAPA_CONFIG Drop)
{
	unsigned long  TmpLong;

	TmpLong = NapaReadRegister(pSwitch, TOCR);
	
	if(Inject)
		TmpLong |= TOCR_INJECT_PPPOE_CKECKSUM;
	else
		TmpLong &= ~TOCR_INJECT_PPPOE_CKECKSUM;
	
	if(Drop)
		TmpLong |= TOCR_DROP_ERR_PPPOE_CHECKSUM;
	else
		TmpLong &= ~TOCR_DROP_ERR_PPPOE_CHECKSUM;

	NapaWriteRegister(pSwitch, TOCR, TmpLong);

} /* End of NapaConfigPPPoEOffload() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaSetPktOrder()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaSetPktOrder(SWITCH *pSwitch, unsigned char Order)
{

	NapaWriteRegister(pSwitch, BORDER, Order);

} /* End of NapaSetPktOrder() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaSetMultiFilter()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaSetMultiFilter(SWITCH  *pSwitch, PORT Port, int Num, unsigned char *Mac)
{
	unsigned char  MacTmp[4];
	unsigned long  *pTmpLong = (unsigned long *)MacTmp;

	unsigned short Offset;

	Offset = Port == Port_0 ? MULTIFILTER : MULTIFILTER + 32;
	Offset = Offset + Num * 4;
		
	*pTmpLong = 0;
	SDBG(SDBG_INIT, ("Set multicast addr"
		"[%d] MAC[%02x-%02x-%02x-%02x-%02x-%02x]\n\r",
		Num, Mac[0], Mac[1], Mac[2], Mac[3], Mac[4], Mac[5]));

		MacTmp[0] = Mac[5];
		MacTmp[1] = Mac[4];
		MacTmp[2] = Mac[3];

	*pTmpLong |= (1 << 25);
	*pTmpLong |= Port == 0 ? (1 << 23) : (1 << 24);

	NapaWriteRegister(pSwitch, Offset, *pTmpLong);

}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaCleanMultiFilter()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaCleanMultiFilter(SWITCH  *pSwitch, PORT Port)
{
	unsigned short Offset;
	int i;

	Offset = Port == Port_0 ? MULTIFILTER : MULTIFILTER + 32;

	for(i = 0; i < 8; i++) {
		NapaWriteRegister(pSwitch, Offset, 0);
		Offset += 4;
	}
}
//End of Initialize APIs


//Interrupt APIs
/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaDisableInterrupt()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaDisableInterrupt(SWITCH *pSwitch)
{
	NapaWriteRegister(pSwitch, IMSR, IMSR_MASK_ALL);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaEnableInterrupt()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaEnableInterrupt(SWITCH *pSwitch)
{
	NapaWriteRegister(pSwitch, IMSR, pSwitch->INT_MASK);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigInterruptLevel()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigInterruptLevel(SWITCH *pSwitch, unsigned char Level)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, GMCR);

	if(Level)
		TmpLong |= GMCR_INT_ACTIVE_HIGH;
	else
		TmpLong &= ~GMCR_INT_ACTIVE_HIGH;
		
	NapaWriteRegister(pSwitch, GMCR, TmpLong);

} /* End of NapaConfigInterruptLevel() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadInterruptMask()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned long
NapaReadInterruptMask(SWITCH *pSwitch)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, IMSR) & 0xFFFF0000;

	SDBG(SDBG_INT, ("Current Mask 0x%lx\n", TmpLong));

	return TmpLong;
} /* End of NapaReadInterruptMask() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaSetInterruptMask()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaSetInterruptMask(SWITCH *pSwitch, unsigned long Mask)
{

	SDBG(SDBG_INT, ("Set Mask 0x%lx\n", Mask));

	pSwitch->INT_MASK = Mask;
	
	NapaWriteRegister(pSwitch, IMSR, Mask);

} /* End of NapaSetInterruptMask() */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaAddInterruptMask()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaAddInterruptMask(SWITCH *pSwitch, unsigned long Mask)
{

	SDBG(SDBG_INT, ("Add Mask 0x%lx\n", Mask));

	pSwitch->INT_MASK |= Mask;

	NapaWriteRegister(pSwitch, IMSR, pSwitch->INT_MASK);

} /* End of NapaAddInterruptMask() */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaRemoveInterruptMask()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaRemoveInterruptMask(SWITCH *pSwitch, unsigned long Mask)
{

	SDBG(SDBG_INT, ("Remove Mask 0x%lx\n", Mask));

	pSwitch->INT_MASK &= ~Mask;

	NapaWriteRegister(pSwitch, IMSR, pSwitch->INT_MASK);

} /* End of NapaRemoveInterruptMask() */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadInterrupt()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned short
NapaReadInterrupt(SWITCH *pSwitch)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, IMSR);

	return (unsigned short)TmpLong;
	
} /* End of NapaReadInterrupt() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaDisableAndAckInterrupt()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaDisableAndAckInterrupt(SWITCH *pSwitch, unsigned short Isr)
{
	unsigned long TmpLong;

	TmpLong = IMSR_MASK_ALL | Isr;

	NapaWriteRegister(pSwitch, IMSR, TmpLong);

} /* End of NapaDisableAndAckInterrupt() */
//End of Interrupt APIs


//
// Tx/Rx APIs
//
/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaLocalBusConfigInsPort()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaLocalBusConfigInsPort(SWITCH *pSwitch, NAPA_CONFIG Insert)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, GMCR);
	if(Insert)
	{
		TmpLong |= GMCR_INS_SRC_FROM_CPU;
	}
	else
	{
		TmpLong &= ~GMCR_INS_SRC_FROM_CPU;
	}
	NapaWriteRegister(pSwitch, GMCR, TmpLong);

} /* End of NapaLocalBusConfigInsPort() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaLocalBusSendPacket2Port()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_STATUS
NapaLocalBusSendPacket2Port(SWITCH *pSwitch, unsigned short Len, unsigned char *pBuf, PORT Port)
{
	unsigned long	PktHeader;
 	unsigned long	TmpLong;
	unsigned short	i;

	SDBG(SDBG_TX, ("=====> NapaLocalBusSendPacket2Port\n"));

	if(Len > MAX_ETHERNET_FRAME_SIZE)
		return NAPA_STATUS_FAILURE;

	TmpLong = Len | CPI_INSERT_PORT_ON | CPI_INSERT_PORT_NUM(Port);
	TmpLong = (~TmpLong << 16) | TmpLong;
	PktHeader = ( (TmpLong & 0xFF000000) >> 24 ) |
							( (TmpLong & 0x00FF0000) >> 8 ) |
							( (TmpLong & 0x0000FF00) << 8 ) |
							( (TmpLong & 0x000000FF) << 24 );
	SDBG(SDBG_TX, ("Write Packet header 0x%8.8lx\n", (~TmpLong << 16) | TmpLong));

	NapaWriteRegister(pSwitch, CSCR, CSCR_CPI_START);

	/* Write packet haeder to CPIO */
	NapaWriteRegister(pSwitch, DATAPORT, PktHeader);

	for (i = 0; i < (Len + 1); i += 4) {
		TmpLong = (unsigned long)*(pBuf + i) |
				(((unsigned long)*(pBuf + i + 1)) << 8) |
				(((unsigned long)*(pBuf + i + 2)) << 16) |
				(((unsigned long)*(pBuf + i + 3)) << 24);
		NapaWriteRegister(pSwitch, DATAPORT, TmpLong);
	}

	SDBG(SDBG_TX, ("<===== NapaLocalBusSendPacket2Port\n"));

	return NAPA_STATUS_SUCCESS;
} /* End of NapaLocalBusSendPacket2Port() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaLocalBusSendPacket()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_STATUS
NapaLocalBusSendPacket(SWITCH *pSwitch, unsigned short Len, unsigned char *pBuf)
{

	unsigned long PktHeader;
 	unsigned long TmpLong;
	unsigned short i;

	SDBG(SDBG_TX, ("=====> NapaLocalBusSendPacket\n"));

	if(Len > MAX_ETHERNET_FRAME_SIZE)
		return NAPA_STATUS_FAILURE;

	TmpLong = Len;
	TmpLong = (~TmpLong << 16) | TmpLong;
	PktHeader = ( (TmpLong & 0xFF000000) >> 24 ) |
							( (TmpLong & 0x00FF0000) >> 8 ) |
							( (TmpLong & 0x0000FF00) << 8 ) |
							( (TmpLong & 0x000000FF) << 24 );
	SDBG(SDBG_TX, ("Write Packet header 0x%8.8lx\n", (~TmpLong << 16) | TmpLong));

	NapaWriteRegister(pSwitch, CSCR, CSCR_CPI_START);

	/* Write packet haeder to CPIO */
	NapaWriteRegister(pSwitch, DATAPORT, PktHeader);

	for (i = 0; i < Len; i += 4) {
		TmpLong = (unsigned long)*(pBuf + i) |
				(((unsigned long)*(pBuf + i + 1)) << 8) |
				(((unsigned long)*(pBuf + i + 2)) << 16) |
				(((unsigned long)*(pBuf + i + 3)) << 24);
		NapaWriteRegister(pSwitch, DATAPORT, TmpLong);
	}

	SDBG(SDBG_TX, ("<===== NapaLocalBusSendPacket\n"));

	return NAPA_STATUS_SUCCESS;
} /* End of NapaLocalBusSendPacket() */



/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaLocalBusGetRxPktLen()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_STATUS
NapaLocalBusGetRxPktLen(SWITCH *pSwitch, unsigned short *Len, PORT *Port)
{
	unsigned long TmpLong;
	unsigned short PktLen;

	SDBG(SDBG_RX, ("=====> NapaLocalBusGetRxPktLen\n"));

	NapaWriteRegister(pSwitch, CSCR, CSCR_CPO_START);

	TmpLong = NapaReadRegister(pSwitch, DATAPORT);
	TmpLong = ( (TmpLong & 0xFF000000) >> 24 ) |
						( (TmpLong & 0x00FF0000) >> 8 ) |
						( (TmpLong & 0x0000FF00) << 8 ) |
						( (TmpLong & 0x000000FF) << 24 );

	SDBG(SDBG_RX, ("Packet header 0x%8.8lx\n", TmpLong));

	PktLen = (unsigned short)TmpLong;

	SDBG(SDBG_RX, ("Packet length 0x%x\n", PktLen));

	if ( PktLen != (unsigned short)(~(TmpLong >> 16)) )
	{
		SDBG(SDBG_RX, ("Packet header error!\n"));
		SDBG(SDBG_RX, ("<===== NapaLocalBusGetRxPktLen\n"));
		return NAPA_STATUS_FAILURE;
	}

	*Len = PktLen & 0x7FF;
	*Port = CPO_PORT_NUM(PktLen);

	SDBG(SDBG_RX, ("<===== NapaLocalBusGetRxPktLen\n"));

	return NAPA_STATUS_SUCCESS;
} /* End of NapaLocalBusGetRxPktLen() */



/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaLocalBusGetRxPkt()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaLocalBusGetRxPkt(SWITCH *pSwitch, unsigned char *pBuf, unsigned short PktLen)
{
	unsigned short PktLenAlign;
	unsigned short i;
	unsigned long TmpLong;

	PktLenAlign = ((PktLen + 3) / 4) * 4;

	for(i = 0; i < PktLenAlign; i += 4) {
		TmpLong = NapaReadRegister(pSwitch, DATAPORT);
		*(pBuf + i) = (unsigned char)TmpLong;
		*(pBuf + i + 1) = (unsigned char)(TmpLong >> 8);
		*(pBuf + i + 2) = (unsigned char)(TmpLong >> 16);
		*(pBuf + i + 3) = (unsigned char)(TmpLong >> 24);
	}

} /* End of NapaLocalBusGetRxPkt() */
// End of Tx/Rx APIs


// PHY APIs
/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaGetPhyId()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned char
NapaGetPhyId(SWITCH *pSwitch, PORT Port)
{
	unsigned long TmpLong;
	unsigned char PhyID;
	
	TmpLong = NapaReadRegister(pSwitch, POLLCR);

	PhyID = (unsigned char)((TmpLong >> (Port * 8)) & 0x1F);

	return PhyID;
} /* End of NapaGetPhyId() */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaSetPhyId()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaSetPhyId(SWITCH *pSwitch, PORT Port,unsigned char Phyid)
{
	unsigned long TmpLong;
	unsigned long ID = Phyid;

	TmpLong = NapaReadRegister(pSwitch, POLLCR);

	if(Port == Port_0)
	{
		TmpLong &= POLLCR_PORT0_PHYID_MASK;
		TmpLong |= POLLCR_PORT0_PHYID(ID);
	}
	else if(Port == Port_1)
	{
		TmpLong &= POLLCR_PORT1_PHYID_MASK;
		TmpLong |= POLLCR_PORT1_PHYID(ID);
	}

	NapaWriteRegister(pSwitch, POLLCR, TmpLong);

} /* End of NapaConfigPortState() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaSetPhyMode()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void 
NapaSetPhyMode(SWITCH *pSwitch, PORT Port, unsigned char Mode)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, MIICR) & 0xFF;

	if(Mode == PHY_MODE_RMII)
	{
		TmpLong |= Port == Port_0 ? MIICR_PORT0_MII_CLK_GEN : MIICR_PORT1_MII_CLK_GEN;

		NapaWriteRegister(pSwitch, MIICR, TmpLong);

		NapaDelayMs(10);

		TmpLong |= Port == Port_0 ? MIICR_PORT0_PHY_RMII : MIICR_PORT1_PHY_RMII;
	}
	else
	{
		TmpLong &= Port == Port_0 ? ~MIICR_PORT0_MII_CLK_GEN : ~MIICR_PORT1_MII_CLK_GEN;
		TmpLong &= Port == Port_0 ? ~MIICR_PORT0_PHY_RMII : ~MIICR_PORT1_PHY_RMII;
	}

	NapaWriteRegister(pSwitch, MIICR, TmpLong);

} /* End of NapaSetPhyMode() */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaEnablePhy()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void 
NapaEnablePhy(SWITCH *pSwitch, PORT Port)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, PCR);

	if(Port == Port_0)
		TmpLong |= PCR_PHY0_RESET_CLEAR;
	else if(Port == Port_1)
		TmpLong |= PCR_PHY1_RESET_CLEAR;

	NapaWriteRegister(pSwitch, PCR, TmpLong);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigPhyPolling()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigPhyPolling(SWITCH *pSwitch, PORT Port, NAPA_CONFIG Set)
{
	unsigned long TmpLong;
	
	TmpLong = NapaReadRegister(pSwitch, POLLCR);

	if(Set)
	{
		TmpLong |= (Port == Port_0) ? POLLCR_PORT0_AUTO_POOLING : POLLCR_PORT1_AUTO_POOLING;
	}
	else
	{
		TmpLong &= (Port == Port_0) ? POLLCR_PORT0_AUTO_POOLING_MASK : POLLCR_PORT1_AUTO_POOLING_MASK;
	}

	NapaWriteRegister(pSwitch, POLLCR, TmpLong);
		
} /* End of NapaConfigPhyPolling() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaSetLedMode()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void 
NapaSetLedMode(SWITCH *pSwitch, PORT Port, LED_STURE *pLed)
{
	unsigned long TmpLong;
	unsigned long Led0, Led1, Led2;

	Led0 = pLed->Led0_Mode;
	Led1 = pLed->Led1_Mode;
	Led2 = pLed->Led2_Mode;

	TmpLong = NapaReadRegister(pSwitch, LEDCR);

	TmpLong |= LEDCR_PORT_LED_ON(Port) | LEDCR_LED0(Led0) | LEDCR_LED1(Led1) | LEDCR_LED2(Led2);

	NapaWriteRegister(pSwitch, LEDCR, TmpLong);

} /* End of NapaSetLedMode() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaGetLinkStatus()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaGetLinkStatus(SWITCH *pSwitch, PORT Port, LINK_STURE *pLinkStatus)
{
	unsigned long TmpLong;
	unsigned short Offset;
	
	Offset = Port == Port_0 ? P0MCR : P1MCR;
	
	TmpLong = NapaReadRegister(pSwitch, Offset);

	pLinkStatus->Link = (TmpLong & MCR_MAC_ON) == 0x01 ? 1 : 0;
	pLinkStatus->Speed = (TmpLong & MCR_SPEED100) == 0x08 ? 1 : 0;
	pLinkStatus->Duplex = (TmpLong & MCR_DUPLEX_FULL) == 0x10 ? 1 : 0;
	pLinkStatus->FlowCtrl = (TmpLong & MCR_FLOWCTRL_ON) == 0x80 ? 1 : 0;

} /* End of NapaGetLinkStatus() */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaSetLinkStatus()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaSetLinkStatus(SWITCH *pSwitch, PORT Port, LINK_STURE *pLinkStatus)
{
	unsigned long TmpLong;
	unsigned short Offset;
	
	Offset = Port == Port_0 ? P0MCR : P1MCR;
	
	TmpLong = NapaReadRegister(pSwitch, Offset);

	if(pLinkStatus->Link)
		TmpLong |= MCR_MAC_ON;
	else
		TmpLong &= ~MCR_MAC_ON;

	if(pLinkStatus->Speed)
		TmpLong |= MCR_SPEED100;
	else
		TmpLong &= ~MCR_SPEED100;

	if(pLinkStatus->Duplex)
		TmpLong |= MCR_DUPLEX_FULL;
	else
		TmpLong &= ~MCR_DUPLEX_FULL;

	if(pLinkStatus->FlowCtrl)
		TmpLong |= MCR_FLOWCTRL_ON;
	else
		TmpLong &= ~MCR_FLOWCTRL_ON;

	NapaWriteRegister(pSwitch, Offset, TmpLong);

} /* End of NapaSetLinkStatus() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaMdioRead()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaMdioRead(SWITCH *pSwitch, PHY_STURE *pPhy)
{
	unsigned long TmpLong;
	unsigned long PhyID, PhyReg;

	PhyID = pPhy->PhyID;
	PhyReg = pPhy->PhyAddr;

	TmpLong = MDCR_READ | MDCR_PHY_ID(PhyID) | MDCR_PHY_REG(PhyReg);

	NapaWriteRegister(pSwitch, MDCR, TmpLong);

	while(1)
	{
		TmpLong = NapaReadRegister(pSwitch, MDCR);
		if(TmpLong & MDCR_VALID)
			break;
	}

	pPhy->Value = (unsigned short)NapaReadRegister(pSwitch, MDCR) & MDCR_VALUE_MASK;

	SDBG(SDBG_DRIVER, ("Read PhyID %ld : register %ld = 0x%x\n", PhyID, PhyReg, pPhy->Value));

} /* End of NapaMdioRead() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaMdioWrite()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaMdioWrite(SWITCH *pSwitch, PHY_STURE *pPhy)
{
	unsigned long TmpLong;
	unsigned long PhyID, PhyReg, Value;

	PhyID = pPhy->PhyID;
	PhyReg = pPhy->PhyAddr;
	Value = pPhy->Value;

	TmpLong = Value | MDCR_WRITE | MDCR_PHY_ID(PhyID) | MDCR_PHY_REG(PhyReg);

	NapaWriteRegister(pSwitch, MDCR, TmpLong);

	while(1)
	{
		TmpLong = NapaReadRegister(pSwitch, MDCR);
		if(TmpLong & MDCR_VALID)
			break;
	}

} /* End of NapaMdioWrite() */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadCRCConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_CONFIG
NapaReadCRCConfig(SWITCH *pSwitch, PORT Port)
{
	unsigned long TmpLong;
	unsigned short Offset;
	
	Offset = Port == Port_0 ? P0MCR : P1MCR;
	
	TmpLong = NapaReadRegister(pSwitch, Offset);

	return (TmpLong & MCR_CRC_CHECK) != 0 ? ENABLE : DISABLE; 

} /* End of NapaReadCRCConfig() */
// End of PHY APIs

// WakeUp APIs
/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigWakeUpFunc()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigWakeUpFunc(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, WOLCR);

	if(Set)
		TmpLong |= WOLCR_WAKEUP_START;
	else
		TmpLong &= ~WOLCR_WAKEUP_START;

	NapaWriteRegister(pSwitch, WOLCR, TmpLong);

}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaClearWakeUp()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaClearWakeUp(SWITCH *pSwitch)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, WOLCR);

	NapaWriteRegister(pSwitch, WOLCR, TmpLong | WOLCR_WAKEUP_CLEAR);

}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaClearSleep()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigSleep(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, WOLCR);

	if (Set)
		NapaWriteRegister(pSwitch, WOLCR, TmpLong | WOLCR_SLEEP_START);
	else
		NapaWriteRegister(pSwitch, WOLCR, TmpLong & ~WOLCR_SLEEP_START);

}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaClearSleep()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaClearSleep(SWITCH *pSwitch)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, WOLCR);

	NapaWriteRegister(pSwitch, WOLCR, TmpLong | WOLCR_SLEEP_CLEAR);

}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigWakeUpFrame()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigWakeUpFrame(SWITCH *pSwitch, PORT Port, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, WOLCR);

	if(Set)
	{
		if(Port == Port_0)
			TmpLong |= WOLCR_PORT0_WAKEUP_FRAME;
		else if(Port == Port_1)
			TmpLong |= WOLCR_PORT1_WAKEUP_FRAME;
	}
	else
	{
		if(Port == Port_0)
			TmpLong &= ~WOLCR_PORT0_WAKEUP_FRAME;
		else if(Port == Port_1)
			TmpLong &= ~WOLCR_PORT1_WAKEUP_FRAME;
	}

	NapaWriteRegister(pSwitch, WOLCR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigLinkChange()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigLinkChange(SWITCH *pSwitch, PORT Port, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, WOLCR);

	if(Set)
	{
		if(Port == Port_0)
			TmpLong |= WOLCR_PORT0_LINK_CHANGE;
		else if(Port == Port_1)
			TmpLong |= WOLCR_PORT1_LINK_CHANGE;
	}
	else
	{
		if(Port == Port_0)
			TmpLong &= ~WOLCR_PORT0_LINK_CHANGE;
		else if(Port == Port_1)
			TmpLong &= ~WOLCR_PORT1_LINK_CHANGE;
	}

	NapaWriteRegister(pSwitch, WOLCR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigMagicPacket()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigMagicPacket(SWITCH *pSwitch, PORT Port, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, WOLCR);

	if(Set)
	{
		if(Port == Port_0)
			TmpLong |= WOLCR_PORT0_MAGIC_PACKET;
		else if(Port == Port_1)
			TmpLong |= WOLCR_PORT1_MAGIC_PACKET;
	}
	else
	{
		if(Port == Port_0)
			TmpLong &= ~WOLCR_PORT0_MAGIC_PACKET;
		else if(Port == Port_1)
			TmpLong &= ~WOLCR_PORT1_MAGIC_PACKET;
	}

	NapaWriteRegister(pSwitch, WOLCR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigWakeUpDaMatch()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigWakeUpDaMatch(SWITCH *pSwitch, PORT Port, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, WOLSR);

	if(Port == Port_0)
	{
		if(Set)
			TmpLong |= WOLSR_PORT0_DA_MATCH;
		else
			TmpLong &= ~WOLSR_PORT0_DA_MATCH;
	}
	else if(Port == Port_1)
	{
		if(Set)
			TmpLong |= WOLSR_PORT1_DA_MATCH;
		else
			TmpLong &= ~WOLSR_PORT1_DA_MATCH;
	}

	NapaWriteRegister(pSwitch, WOLSR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigWakeUpMask()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigWakeUpMask(SWITCH *pSwitch, PORT Port, unsigned char MaskNum, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, WOLSR);

	if(Set)
	{
		if(Port == Port_0)
		{
			if(MaskNum == 0)
				TmpLong |= WOLSR_PORT0_MASK0_ON;
			else if(MaskNum == 1)
				TmpLong |= WOLSR_PORT0_MASK1_ON;
			else if(MaskNum == 2)
				TmpLong |= WOLSR_PORT0_MASK2_ON;
		}
		else if(Port == Port_1)
		{
			if(MaskNum == 0)
				TmpLong |= WOLSR_PORT1_MASK0_ON;
			else if(MaskNum == 1)
				TmpLong |= WOLSR_PORT1_MASK1_ON;
			else if(MaskNum == 2)
				TmpLong |= WOLSR_PORT1_MASK2_ON;
		}
	}
	else
	{
		if(Port == Port_0)
		{
			if(MaskNum == 0)
				TmpLong &= ~WOLSR_PORT0_MASK0_ON;
			else if(MaskNum == 1)
				TmpLong &= ~WOLSR_PORT0_MASK1_ON;
			else if(MaskNum == 2)
				TmpLong &= ~WOLSR_PORT0_MASK2_ON;
		}
		else if(Port == Port_1)
		{
			if(MaskNum == 0)
				TmpLong &= ~WOLSR_PORT1_MASK0_ON;
			else if(MaskNum == 1)
				TmpLong &= ~WOLSR_PORT1_MASK1_ON;
			else if(MaskNum == 2)
				TmpLong &= ~WOLSR_PORT1_MASK2_ON;
		}
	}

	NapaWriteRegister(pSwitch, WOLSR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigWakeUpOffset()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigWakeUpOffset(SWITCH *pSwitch, PORT Port, unsigned char OffsetNum, unsigned char Offset)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, WOLSR);

	if(Port == Port_0)
	{
		if(OffsetNum == 0)
		{
			TmpLong &= ~WOLSR_PORT0_OFFSET0_MASK;
			TmpLong |= WOLSR_PORT0_OFFSET0((unsigned long)Offset);
		}
		else if(OffsetNum == 1)
		{
			TmpLong &= ~WOLSR_PORT0_OFFSET1_MASK;
			TmpLong |= WOLSR_PORT0_OFFSET1((unsigned long)Offset);
		}
		else if(OffsetNum == 2)
		{
			TmpLong &= ~WOLSR_PORT0_OFFSET2_MASK;
			TmpLong |= WOLSR_PORT0_OFFSET2((unsigned long)Offset);
		}
	}
	else if(Port == Port_1)
	{
		if(OffsetNum == 0)
		{
			TmpLong &= ~WOLSR_PORT1_OFFSET0_MASK;
			TmpLong |= WOLSR_PORT1_OFFSET0((unsigned long)Offset);
		}
		else if(OffsetNum == 1)
		{
			TmpLong &= ~WOLSR_PORT1_OFFSET1_MASK;
			TmpLong |= WOLSR_PORT1_OFFSET1((unsigned long)Offset);
		}
		else if(OffsetNum == 2)
		{
			TmpLong &= ~WOLSR_PORT1_OFFSET2_MASK;
			TmpLong |= WOLSR_PORT1_OFFSET2((unsigned long)Offset);
		}	
	}

	NapaWriteRegister(pSwitch, WOLSR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigWakeUpCascade()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigWakeUpCascade(SWITCH *pSwitch, PORT Port, unsigned char CascadeNum, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, WOLCR);

	if(Set)
	{
		if(Port == Port_0)
		{
			if(CascadeNum == 0)
				TmpLong |= WOLCR_PORT0_CASCADE0;
			else if(CascadeNum == 1)
				TmpLong |= WOLCR_PORT0_CASCADE1;
		}
		else if(Port == Port_1)
		{
			if(CascadeNum == 0)
				TmpLong |= WOLCR_PORT1_CASCADE0;
			else if(CascadeNum == 1)
				TmpLong |= WOLCR_PORT1_CASCADE1;
		}
	}
	else
	{
		if(Port == Port_0)
		{
			if(CascadeNum == 0)
				TmpLong &= ~WOLCR_PORT0_CASCADE0;
			else if(CascadeNum == 1)
				TmpLong &= ~WOLCR_PORT0_CASCADE1;
		}
		else if(Port == Port_1)
		{
			if(CascadeNum == 0)
				TmpLong &= ~WOLCR_PORT1_CASCADE0;
			else if(CascadeNum == 1)
				TmpLong &= ~WOLCR_PORT1_CASCADE1;
		}	
	}

	NapaWriteRegister(pSwitch, WOLCR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigWakeUpMaskData()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigWakeUpMaskData(SWITCH *pSwitch, PORT Port, unsigned char MaskNum, unsigned long Value)
{
	unsigned short Offset;

	if(Port == Port_0)
	{
		if(MaskNum == 0)
			Offset = PORT0MASK0;
		else if(MaskNum == 1)
			Offset = PORT0MASK1;
		else if(MaskNum == 2)
			Offset = PORT0MASK2;
		else
			return;
	}
	else if(Port == Port_1)
	{
		if(MaskNum == 0)
			Offset = PORT1MASK0;
		else if(MaskNum == 1)
			Offset = PORT1MASK1;
		else if(MaskNum == 2)
			Offset = PORT1MASK2;
		else
			return;
	}
	else
		return;

	NapaWriteRegister(pSwitch, Offset, Value);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigWakeUpCrcData()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigWakeUpCrcData(SWITCH *pSwitch, PORT Port, unsigned char CrcNum, unsigned long Value)
{
	unsigned short Offset;

	if(Port == Port_0)
	{
		if(CrcNum == 0)
			Offset = PORT0CRC0;
		else if(CrcNum == 1)
			Offset = PORT0CRC1;
		else if(CrcNum == 2)
			Offset = PORT0CRC2;
		else
			return;
	}
	else if(Port == Port_1)
	{
		if(CrcNum == 0)
			Offset = PORT1CRC0;
		else if(CrcNum == 1)
			Offset = PORT1CRC1;
		else if(CrcNum == 2)
			Offset = PORT1CRC2;
		else
			return;
	}
	else
		return;

	NapaWriteRegister(pSwitch, Offset, Value);
}
// End of WakeUp APIs

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadMaxBroadcast()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned char
NapaReadMaxBroadcast(SWITCH *pSwitch)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, GMCR);
	TmpLong = (TmpLong >> 14) & 0x03;
	return (unsigned char)TmpLong;
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaWriteMaxBroadcast()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaWriteMaxBroadcast(SWITCH *pSwitch, unsigned char value)
{
	unsigned long TmpLong;
	
	TmpLong = NapaReadRegister(pSwitch, GMCR);
	TmpLong &= ~GMCR_MAX_STORM(0x03);
	TmpLong |= GMCR_MAX_STORM(value);
	NapaWriteRegister(pSwitch, GMCR, TmpLong);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadARPToCPUConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_CONFIG
NapaReadARPToCPUConfig(SWITCH *pSwitch)
{
	unsigned long TmpLong;
	NAPA_CONFIG Set;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	if (TmpLong & L2PSR_ARP_TO_CPU)
		Set = ENABLE;
	else
		Set = DISABLE;
	return Set;
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaWriteARPToCPUConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaWriteARPToCPUConfig(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	if (Set)
		TmpLong |= L2PSR_ARP_TO_CPU;
	else
		TmpLong &= ~L2PSR_ARP_TO_CPU;
	NapaWriteRegister(pSwitch, L2PSR, TmpLong);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadMCoverVLANConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_CONFIG
NapaReadMCoverVLANConfig(SWITCH *pSwitch)
{
	unsigned long TmpLong;
	NAPA_CONFIG Set;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	if (TmpLong & L2PSR_MC_OVER_VLAN)
		Set = ENABLE;
	else
		Set = DISABLE;
	return Set;
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaWriteMCoverVLANConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaWriteMCoverVLANConfig(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	if (Set)
		TmpLong |= L2PSR_MC_OVER_VLAN;
	else
		TmpLong &= ~L2PSR_MC_OVER_VLAN;
	NapaWriteRegister(pSwitch, L2PSR, TmpLong);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadCtrlToCPUConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_CONFIG
NapaReadCtrlToCPUConfig(SWITCH *pSwitch)
{
	unsigned long TmpLong;
	NAPA_CONFIG Set;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	if (TmpLong & L2PSR_CTRL_PKT_TO_CPU)
		Set = ENABLE;
	else
		Set = DISABLE;
	return Set;
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaWriteCtrlToCPUConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaWriteCtrlToCPUConfig(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	if (Set)
		TmpLong |= L2PSR_CTRL_PKT_TO_CPU;
	else
		TmpLong &= ~L2PSR_CTRL_PKT_TO_CPU;
	NapaWriteRegister(pSwitch, L2PSR, TmpLong);
}
