/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
/*=============================================================================
 * Module Name:access_port.h
 * Purpose:
 * Author:
 * Date:
 * Notes:
 * $Log: access_port,v $
 * Revision 0.0.0.1
 * no message
 *
 *
 *=============================================================================
 */
#ifndef __ACCESS_PORT_H__
#define __ACCESS_PORT_H__

/* INCLUDE FILE DECLARATIONS */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/pci.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/ethtool.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/delay.h>
#include <linux/random.h>
#include <linux/mii.h>

#include <linux/in.h>

#include <asm/system.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/uaccess.h>

/* NAMING CONSTANT AND TYPE DECLARATIONS */

#define SDBG_DRIVER                  (1 << 0)
#define SDBG_ACCESS                  (1 << 1)
#define SDBG_INIT                    (1 << 2)
#define SDBG_RX                      (1 << 3)
#define SDBG_TX                      (1 << 4)
#define SDBG_INT                     (1 << 5)
#define SDBG_FDTABLE                 (1 << 6)
#define SDBG_VLAN                    (1 << 7)
#define SDBG_RMON                    (1 << 8)
#define SDBG_EEPROM                  (1 << 9)
#define SDBG_STP                     (1 << 10)
#define SDBG_OTHER                   (1 << 11)
#define SDBG_DEBUG                   (1 << 12)
#define SDBG_FLAG                    (0)

#define SDBG(Flag, Msg)   	//   if(Flag & SDBG_FLAG) printk Msg

#define NAPA_BASE_ADDRESS           (0x08000000)
#define NAPA_IO_EXTENT              (0xFFF)
#define NAPA_IRQ                    (IRQ_EINT11)

#define PORT0_DEFAULT_PHY_INTERFACE (PHY_MODE_MII)
#define PORT1_DEFAULT_PHY_INTERFACE (PHY_MODE_MII)

#define PORT0_DEFAULT_PHY_ID        (0x10)
#define PORT1_DEFAULT_PHY_ID        (0x11)

#define DEFAULT_PHY_LED0            (PHY_LED_LINK)
#define DEFAULT_PHY_LED1            (PHY_LED_DUPLEX | PHY_LED_COLLISION)
#define DEFAULT_PHY_LED2            (PHY_LED_SPEED)

typedef struct _SWITCH {

	// Memory base address
	volatile void				*pMemBase;

	// used by Forwarding APIs
	unsigned long				FTPage;
	unsigned char				FTEnd;
	
	// interrupt related
	unsigned long				INT_MASK;

} SWITCH, *PSWITCH;

/* EXPORTED SUBPROGRAM SPECIFICATIONS */
// Access APIs
unsigned long
NapaReadRegister(
        SWITCH *pSwitch,
        unsigned short RegAddr
        );

void
NapaWriteRegister(
        SWITCH *pSwitch,
        unsigned short RegAddr,
        unsigned long Value
        );

void
NapaDelayMs(
        unsigned long Time
        );

unsigned long
NapaGetCurrentTime(
        void
        );

void
NapaAccessPortInit(
		u8 mode
		);
// End of Access APIs

#endif
