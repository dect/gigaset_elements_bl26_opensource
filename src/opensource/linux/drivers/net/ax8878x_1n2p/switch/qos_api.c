/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
 /*============================================================================
 * Module Name: qos_api.c
 * Purpose:
 * Author:
 * Date:
 * Notes:
 *
 *
 *=============================================================================
 */

/* INCLUDE FILE DECLARATIONS */
#include "switch.h"

/* NAMING CONSTANT DECLARATIONS */

/* GLOBAL VARIABLES DECLARATIONS */

/* LOCAL VARIABLES DECLARATIONS */

/* LOCAL SUBPROGRAM DECLARATIONS */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWriteRxRateLimitConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWriteRxRateLimitConfig(SWITCH *pSwitch, QOS_RATE_CONFIG *pSet)
{
	unsigned long TmpLong;
	unsigned short Offset = pSet->Port * 0x40 + P0MCR;

	TmpLong = NapaReadRegister(pSwitch, Offset);
	
	if(pSet->Config)
		TmpLong |= MCR_RX_RATE_ON;
	else
		TmpLong &= ~MCR_RX_RATE_ON;
	NapaWriteRegister(pSwitch, Offset, TmpLong);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWriteTxRateLimitConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWriteTxRateLimitConfig(SWITCH *pSwitch, QOS_RATE_CONFIG *pSet)
{
	unsigned long TmpLong;
	unsigned short Offset = pSet->Port * 0x40 + P0MCR;

	TmpLong = NapaReadRegister(pSwitch, Offset);
	
	if(pSet->Config)
		TmpLong |= MCR_TX_RATE_ON;
	else
		TmpLong &= ~MCR_TX_RATE_ON;
	NapaWriteRegister(pSwitch, Offset, TmpLong);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWrite8021pRemapConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWrite8021pRemapConfig(SWITCH *pSwitch, QOS_1P_TX_CONFIG *pSet)
{
	unsigned long TmpLong;
	unsigned short Offset = pSet->Port * 0x40 + P0QMTBL;

	TmpLong = NapaReadRegister(pSwitch, Offset);
	if(pSet->Config)
		TmpLong |= QMTBL_TX_ON;
	else
		TmpLong &= ~QMTBL_TX_ON;
	NapaWriteRegister(pSwitch, Offset, TmpLong);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWrite8021pConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWrite8021pConfig(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	if(Set)
		TmpLong |= L2PSR_1P_ON;
	else
		TmpLong &= ~L2PSR_1P_ON;
	NapaWriteRegister(pSwitch, L2PSR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWrite8021pConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWriteTosConfig(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	if(Set)
		TmpLong |= L2PSR_TOS_ON;
	else
		TmpLong &= ~L2PSR_TOS_ON;
	NapaWriteRegister(pSwitch, L2PSR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWrite8021pConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWriteCosConfig(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	if(Set)
		TmpLong |= L2PSR_COS_ON;
	else
		TmpLong &= ~L2PSR_COS_ON;
	NapaWriteRegister(pSwitch, L2PSR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWriteDSCPConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWriteDSCPConfig (SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	if(Set)
		TmpLong |= L2PSR_DSCP_ON;
	else
		TmpLong &= ~L2PSR_DSCP_ON;
	NapaWriteRegister(pSwitch, L2PSR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadRead8021pMapTable()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadRead8021pMapTable(SWITCH *pSwitch, QOS_1P_MAP *pMap)
{
	unsigned long TmpLong;
	unsigned short Offset;
	unsigned char i;

	Offset = pMap->Port * 0x40 + P0QMTBL;
	TmpLong = NapaReadRegister(pSwitch, Offset);

	for(i = 0; i < 8; i++)
	{
		pMap->Priority[i] = (unsigned char)(TmpLong >> (2 * i)) & QMTBL_PRIORITY_MASK;
	}
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWrite8021pMapTable()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWrite8021pMapTable(SWITCH *pSwitch, QOS_1P_MAP *pMap)
{
	unsigned long TmpLong;
	unsigned short Offset;
	unsigned char i;

	Offset = pMap->Port * 0x40 + P0QMTBL;
	TmpLong = NapaReadRegister(pSwitch, Offset);
	TmpLong &= 0xFFFF0000;

	for(i = 0; i < 8; i++)
	{
		TmpLong |= ((unsigned long)(pMap->Priority[i]) << (2 * i));
	}

	NapaWriteRegister(pSwitch, Offset, TmpLong);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWrite8021pRemapTable()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void NapadRead8021pRemapTable(SWITCH *pSwitch, QOS_1P_REMAP *pMap)
{
	unsigned long TmpLong;
	unsigned short Offset;
	unsigned char i;

	Offset = pMap->Port * 0x40 + P0QMTBL;
	TmpLong = NapaReadRegister(pSwitch, Offset);

	for(i = 0; i < 4; i++)
	{
		pMap->Priority[i] = 
			(unsigned char)((TmpLong >> 16) >> (4 * i)) 
			& QMTBL_REMAP_MASK;
	}
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWrite8021pRemapTable()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void NapadWrite8021pRemapTable(SWITCH *pSwitch, QOS_1P_REMAP *pMap)
{
	unsigned long TmpLong;
	unsigned short Offset;
	unsigned char i;

	Offset = pMap->Port * 0x40 + P0QMTBL;
	TmpLong = NapaReadRegister(pSwitch, Offset);
	TmpLong &= 0x8000FFFF;

	for(i = 0; i < 4; i++)
	{
		TmpLong |= ((((unsigned long)(pMap->Priority[i])) << 16) 
			<< (4 * i));
	}

	NapaWriteRegister(pSwitch, Offset, TmpLong);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadReadCosMapTable()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadReadTosMapTable(SWITCH *pSwitch, QOS_COSTOS_MAP *pMap)
{
	unsigned long TmpLong;
	unsigned char i;
	
	TmpLong = NapaReadRegister(pSwitch, PMTR);

	for(i = 0; i < 8; i++)
	{
		pMap->Priority[i] = (unsigned char)(TmpLong >> (i * 2)) & PMTR_PRIORITY_MASK;
	}
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWriteCosMapTable()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWriteTosMapTable(SWITCH *pSwitch, QOS_COSTOS_MAP *pMap)
{
	unsigned long TmpLong;
	unsigned char i;
	
	TmpLong = NapaReadRegister(pSwitch, PMTR) & ~PMTR_TOS_MASK;

	for(i = 0; i < 8; i++)
	{
		TmpLong |= (unsigned long)pMap->Priority[i] << (i * 2);
	}

	NapaWriteRegister(pSwitch, PMTR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadReadCosMapTable()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadReadCosMapTable(SWITCH *pSwitch, QOS_COSTOS_MAP *pMap)
{
	unsigned long TmpLong;
	unsigned char i;
	
	TmpLong = NapaReadRegister(pSwitch, PMTR);

	for(i = 0; i < 8; i++)
	{
		pMap->Priority[i] = (unsigned char)(TmpLong >> (16 + i * 2)) & PMTR_PRIORITY_MASK;
	}
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWriteCosMapTable()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWriteCosMapTable(SWITCH *pSwitch, QOS_COSTOS_MAP *pMap)
{
	unsigned long TmpLong;
	unsigned char i;
	
	TmpLong = NapaReadRegister(pSwitch, PMTR) & ~PMTR_COS_MASK;

	for(i = 0; i < 8; i++)
	{
		TmpLong |= (unsigned long)pMap->Priority[i] << (16 + i * 2);
	}

	NapaWriteRegister(pSwitch, PMTR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadReadDSCPMapTable ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadReadDSCPMapTable (SWITCH *pSwitch, QOS_DSCP_MAP *pMap)
{
	unsigned long TmpLong;
	unsigned short Offset;
	unsigned char i, j;

	for (i = 0; i < 4; i++) {

		Offset = (DSCPTBL0 + i * 4);
		TmpLong = NapaReadRegister(pSwitch, Offset);

		for (j = 0; j < 16; j++) {
			pMap->Priority[i * 16 + j] = (unsigned char)((TmpLong >> (j * 2)) & 0x03);
		}
	}
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWriteDSCPMapTable ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWriteDSCPMapTable (SWITCH *pSwitch, QOS_DSCP_MAP *pMap)
{
	unsigned long TmpLong;
	unsigned long Pri;
	unsigned short Offset;
	unsigned char i, j;

	for (i = 0; i < 4; i++) {

		Offset = (DSCPTBL0 + i * 4);
		TmpLong = 0;

		for (j = 0; j < 16; j++) {
			Pri = pMap->Priority[i * 16 + j] & 0x03;
			TmpLong |= (Pri << (j * 2));
		}

		NapaWriteRegister(pSwitch, Offset, TmpLong);
	}
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWritePortTxRate ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWritePortTxRate (SWITCH *pSwitch, PORT Port, unsigned long Rate)
{
	unsigned short Offset = (P0RL + 0x40 * Port);
	unsigned long TmpLong;

	TmpLong = (NapaReadRegister (pSwitch, Offset) & P0RL_TX_RATE_MASK) |
			((Rate / (8 * 4096)) << 16);

	NapaWriteRegister (pSwitch, Offset, TmpLong);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWritePortTxRate ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned long
NapadReadPortTxRate (SWITCH *pSwitch, PORT Port)
{
	unsigned short Offset = (P0RL + 0x40 * Port);
	unsigned long TmpLong;

	TmpLong = ((NapaReadRegister (pSwitch, Offset) >> 16) & 0xFFF) *
			(8 * 4096);

	return TmpLong;
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWritePortRxRate ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWritePortRxRate (SWITCH *pSwitch, PORT Port, unsigned long Rate)
{
	unsigned short Offset = (P0RL + 0x40 * Port);
	unsigned long TmpLong;

	TmpLong = (NapaReadRegister (pSwitch, Offset) & P0RL_RX_RATE_MASK) |
			(Rate / (8 * 4096));

	NapaWriteRegister (pSwitch, Offset, TmpLong);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadReadPortRxRate ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned long
NapadReadPortRxRate (SWITCH *pSwitch, PORT Port)
{
	unsigned short Offset = (P0RL + 0x40 * Port);
	unsigned long TmpLong;

	TmpLong = (NapaReadRegister (pSwitch, Offset) & 0xFFF) *
			(8 * 4096);

	return TmpLong;
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWriteQueueTxRate ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWriteQueueTxRate (SWITCH *pSwitch, PORT Port, unsigned char Queue, unsigned long Rate)
{
	unsigned short Offset = (P0TQRL0 + 0x40 * Port) + 4 * (Queue / 2);
	unsigned long TmpLong;
	unsigned long mask = (Queue % 2) == 1 ? 0x00000FFF : 0x0FFF0000;
	unsigned char bit_s = (Queue % 2) == 1 ? 16 : 0;

	TmpLong = (NapaReadRegister (pSwitch, Offset) & mask) |
			((Rate / (8 * 4096)) << bit_s);

	NapaWriteRegister (pSwitch, Offset, TmpLong);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadReadQueueTxRate ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned long
NapadReadQueueTxRate (SWITCH *pSwitch, PORT Port, unsigned char Queue)
{
	unsigned short Offset = (P0TQRL0 + 0x40 * Port) + 4 * (Queue / 2);
	unsigned long TmpLong;
	unsigned char bit_s = (Queue % 2) == 1 ? 16 : 0;

	TmpLong = (NapaReadRegister (pSwitch, Offset) >> bit_s) & 0xFFF;

	return (TmpLong * 8 * 4096);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWriteQueueRxRate ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWriteQueueRxRate (SWITCH *pSwitch, PORT Port, unsigned char Queue, unsigned long Rate)
{
	unsigned short Offset = (P0RQRL0 + 0x40 * Port) + 4 * (Queue / 2);
	unsigned long TmpLong;
	unsigned long mask = (Queue % 2) == 1 ? 0x00000FFF : 0x0FFF0000;
	unsigned char bit_s = (Queue % 2) == 1 ? 16 : 0;

	TmpLong = (NapaReadRegister (pSwitch, Offset) & mask) |
			((Rate / (8 * 4096)) << bit_s);

	NapaWriteRegister (pSwitch, Offset, TmpLong);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadReadQueueRxRate ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned long
NapadReadQueueRxRate (SWITCH *pSwitch, PORT Port, unsigned char Queue)
{
	unsigned short Offset = (P0RQRL0 + 0x40 * Port) + 4 * (Queue / 2);
	unsigned long TmpLong;
	unsigned char bit_s = (Queue % 2) == 1 ? 16 : 0;

	TmpLong = (NapaReadRegister (pSwitch, Offset) >> bit_s) & 0xFFF;

	return (TmpLong * 8 * 4096);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadWriteQueueWeight ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapadWriteQueueWeight (SWITCH *pSwitch, PORT Port, unsigned char Queue, unsigned char Weight)
{
	unsigned short Offset = P0BMU + 0x40 * Port;
	unsigned long TmpLong;
	
	TmpLong = (unsigned long)Weight << (4 * Queue);
	TmpLong |= (NapaReadRegister (pSwitch, Offset) &
			 ~BMU_QUEUE_MASK(Queue));

	NapaWriteRegister (pSwitch, Offset, TmpLong);
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapadReadQueueWeight ()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned char
NapadReadQueueWeight (SWITCH *pSwitch, PORT Port, unsigned char Queue)
{
	unsigned short Offset = P0BMU + 0x40 * Port;
	unsigned long TmpLong;

	TmpLong = NapaReadRegister (pSwitch, Offset);
	TmpLong &= BMU_QUEUE_MASK(Queue);
	TmpLong = TmpLong >> (4 * Queue);
	return (unsigned char)TmpLong;
}