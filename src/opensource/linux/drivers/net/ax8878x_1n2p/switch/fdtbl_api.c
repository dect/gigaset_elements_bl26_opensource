/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
 /*============================================================================
 * Module Name: fdtbl_api.c
 * Purpose:
 * Author:
 * Date:
 * Notes:
 * $Log: fdtbl_api.c,v $
 * no message
 *
 *
 *=============================================================================
 */

/* INCLUDE FILE DECLARATIONS */
//#include "fdtbl_api.h"
#include "switch.h"

/* NAMING CONSTANT DECLARATIONS */

/* GLOBAL VARIABLES DECLARATIONS */

/* LOCAL VARIABLES DECLARATIONS */

/* LOCAL SUBPROGRAM DECLARATIONS */

/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadNextForwardingEntry()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_STATUS
NapaReadNextForwardingEntry (SWITCH *pSwitch, FORWARDING_TABLE_STRUE *pMac)
{
	unsigned long  Data1, Data2;
	unsigned long  Valid, End;
	unsigned long  TmpLong;

	SDBG(SDBG_FDTABLE, ("=====> NapaReadNextForwardingEntry\n\r"));

	if(pSwitch->FTEnd)
	{
		TmpLong = 0;
		NapaWriteRegister(pSwitch, FTCMD, TmpLong);
		SDBG(SDBG_FDTABLE, ("Forwarding Table search done.\n\r"));
		SDBG(SDBG_FDTABLE, ("<===== NapaReadNextForwardingEntry\n\r"));
		return NAPA_STATUS_NO_DATA;
	}

	while(1)
	{
		TmpLong = FTCMD_READ_FT | FTCMD_CONTI_FT | pSwitch->FTPage;
		NapaWriteRegister(pSwitch, FTCMD, TmpLong);

		do
		{	
			TmpLong = NapaReadRegister(pSwitch, FTCMD);
		} while((TmpLong  & FTCMD_FT_VALID) == 0 && (TmpLong  & FTCMD_FT_END) == 0);

		Valid = TmpLong & FTCMD_FT_VALID;
		End = TmpLong  & FTCMD_FT_END;

		TmpLong = FTCMD_CONTI_FT | pSwitch->FTPage;
		NapaWriteRegister(pSwitch, FTCMD, TmpLong);

		if (Valid)
		{

			TmpLong = NapaReadRegister(pSwitch, FTCMD);
			Data1 = TmpLong & 0x00FFFFFF;
			TmpLong = NapaReadRegister(pSwitch, FTDATA);
			Data2 = TmpLong;

			pMac->Mac[0] = (unsigned char)Data2;
			pMac->Mac[1] = (unsigned char)(Data2 >> 8);
			pMac->Mac[2] = (unsigned char)(Data2 >> 16);
			pMac->Mac[3] = (unsigned char)(Data2 >> 24);
			pMac->Mac[4] = (unsigned char)Data1;
			pMac->Mac[5] = (unsigned char)(Data1 >> 8);

			if(Data1 & FTCMD_FT_STATIC)
				pMac->IsStatic = 1;
			else
				pMac->IsStatic = 0;

			pMac->SrcPort = (unsigned char)((Data1 >> 16) & 0x03);

			if(Data1 & FTCMD_FT_FILTER_DA)
				pMac->FilterDa = 1;
			else
				pMac->FilterDa = 0;

			if(Data1 & FTCMD_FT_FILTER_SA)
				pMac->FilterSa = 1;
			else
				pMac->FilterSa = 0;

			if(pSwitch->FTPage == FTCMD_FT_PAGE0)
				pMac->Page = 0;
			else
				pMac->Page = 1;

			SDBG(SDBG_FDTABLE, ("Valid MAC :: %2.2x-%2.2x-%2.2x-%2.2x-%2.2x-%2.2x\n\r", 
				pMac->Mac[5], pMac->Mac[4], pMac->Mac[3],
				pMac->Mac[2], pMac->Mac[1], pMac->Mac[0]));

			TmpLong = FTCMD_CONTI_FT;
			NapaWriteRegister(pSwitch, FTCMD, TmpLong);

			if (End)
			{
				if ( pSwitch->FTPage == FTCMD_FT_PAGE0 )
				{
					pSwitch->FTPage = FTCMD_FT_PAGE1;
					SDBG(SDBG_FDTABLE, ("PAGE 0 search end\n\r"));
				} else {
					pSwitch->FTEnd = 1;
					SDBG(SDBG_FDTABLE, ("PAGE 1 search end\n\r"));
				}
			}

			break;

		}
		else
		{
			if ( pSwitch->FTPage == FTCMD_FT_PAGE0 )
			{
				SDBG(SDBG_FDTABLE, ("PAGE 0 search end\n\r"));
				pSwitch->FTPage = FTCMD_FT_PAGE1;
			}
			else
			{
				pSwitch->FTEnd = 1;

				TmpLong = 0;
				NapaWriteRegister(pSwitch, FTCMD, TmpLong);
				SDBG(SDBG_FDTABLE, ("PAGE 1 search end\n\r"));
				SDBG(SDBG_FDTABLE, ("<===== NapaReadNextForwardingEntry\n\r"));
				return NAPA_STATUS_NO_DATA;
			}
		}
	}

	SDBG(SDBG_FDTABLE, ("<===== NapaReadNextForwardingEntry\n\r"));
	return NAPA_STATUS_SUCCESS;

} /* End of NapaReadNextForwardingEntry() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadForwardingEntry()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_STATUS
NapaReadForwardingEntry (SWITCH *pSwitch, FORWARDING_TABLE_STRUE *pMac)
{
	unsigned long TmpLong = 0;

	pSwitch->FTPage = 0;
	pSwitch->FTEnd = 0;

	NapaWriteRegister(pSwitch, FTCMD, TmpLong);

	return NapaReadNextForwardingEntry(pSwitch, pMac);

} /* End of NapaReadForwardingEntry() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaFlushForwardingEntry()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaFlushForwardingEntry(SWITCH *pSwitch)
{
	unsigned long TmpLong;

	TmpLong = FTCMD_FLUSH_FT;
	NapaWriteRegister(pSwitch, FTCMD, TmpLong);

	do
	{
		TmpLong = NapaReadRegister(pSwitch, FTCMD);
	} while((TmpLong & FTCMD_FT_FLUSH_DOWN) == 0);

	TmpLong = 0;
	NapaWriteRegister(pSwitch, FTCMD, TmpLong);

} /* End of NapaFlushForwardingEntry() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaWriteForwardingEntry()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaWriteForwardingEntry(SWITCH *pSwitch, FORWARDING_TABLE_STRUE *pMac)
{
	unsigned long TmpLong;

	SDBG(SDBG_FDTABLE, ("=====> NapaWriteForwardingEntry\n\r"));

	TmpLong = (unsigned long)pMac->Mac[0] | (unsigned long)pMac->Mac[1] << 8 |
		(unsigned long)pMac->Mac[2] << 16 | (unsigned long)pMac->Mac[3] << 24;

	NapaWriteRegister(pSwitch, FTDATA, TmpLong);

	TmpLong =  FTCMD_WRITE_FT | (unsigned long)pMac->Mac[4] | 
		((unsigned long)pMac->Mac[5] << 8) |
		(FTCMD_FT_PORT((unsigned long)pMac->SrcPort));

	TmpLong |= pMac->IsStatic == 1 ? FTCMD_FT_STATIC : 0;

	TmpLong |= pMac->FilterDa == 1 ? FTCMD_FT_FILTER_DA : 0;

	TmpLong |= pMac->FilterSa == 1 ? FTCMD_FT_FILTER_SA : 0;

	NapaWriteRegister(pSwitch, FTCMD, TmpLong);

	SDBG(SDBG_FDTABLE, ("Write MAC :: %2.2x-%2.2x-%2.2x-%2.2x-%2.2x-%2.2x\n\r",
	pMac->Mac[5], pMac->Mac[4], pMac->Mac[3],
	pMac->Mac[2], pMac->Mac[1], pMac->Mac[0]));

//	TmpLong = 0;
//	NapaWriteRegister(pSwitch, FTCMD, TmpLong);

	SDBG(SDBG_FDTABLE, ("<===== NapaWriteForwardingEntry\n\r"));

} /* End of NapaFlushForwardingEntry() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigForwardingLearn()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_CONFIG
NapaReadForwardingLearnConfig(SWITCH *pSwitch)
{
	unsigned long TmpLong;
	TmpLong = NapaReadRegister(pSwitch, L2PSR);

	if(TmpLong & L2PSR_STOP_LEARNING)
		return ENABLE;
	else
		return DISABLE;

} /* End of NapaConfigForwardingLearn() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaConfigForwardingLearn()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaConfigForwardingLearnStop(SWITCH *pSwitch, NAPA_CONFIG Set)
{
	unsigned long TmpLong;
	TmpLong = NapaReadRegister(pSwitch, L2PSR);

	if(Set)
		TmpLong |= L2PSR_STOP_LEARNING;
	else
		TmpLong &= ~L2PSR_STOP_LEARNING;

	NapaWriteRegister(pSwitch, L2PSR, TmpLong);
} /* End of NapaConfigForwardingLearn() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadAgingConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned short
NapaReadAgingConfig(SWITCH *pSwitch)
{
	unsigned long TmpLong;
	unsigned short TmpShort;
	
	TmpLong = NapaReadRegister(pSwitch, L2FCR);

	TmpShort = (unsigned short)((TmpLong >> 16) & 0x1FF);

	return TmpShort;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaWriteAgingConfig()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaWriteAgingConfig(SWITCH *pSwitch, unsigned long ConfigTime)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2FCR);

	// Clear first
	TmpLong &= 0xFE00FFFF;

	TmpLong |= L2FCR_AGING_TIME(ConfigTime);

	NapaWriteRegister(pSwitch, L2FCR, TmpLong);

}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaWriteLearningMethod()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaWriteLearningMethod(SWITCH *pSwitch, NAPA_CONFIG Method)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	if(Method == FORWARDING_LINEAR_LEARNING)
		TmpLong &= ~L2PSR_HASH_MODE_HASH;
	else
		TmpLong |= L2PSR_HASH_MODE_HASH;

	NapaWriteRegister(pSwitch, L2PSR, TmpLong);
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadLearningMethod()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
NAPA_CONFIG
NapaReadLearningMethod(SWITCH *pSwitch)
{
	unsigned long TmpLong;

	TmpLong = NapaReadRegister(pSwitch, L2PSR);
	if(TmpLong & L2PSR_HASH_MODE_HASH)
		return FORWARDING_HASH_LEARNING;
	else
		return FORWARDING_LINEAR_LEARNING;		
}
