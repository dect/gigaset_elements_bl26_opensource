/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
 /*============================================================================
 * Module Name: access_port.c
 * Purpose:
 * Author:
 * Date:
 * Notes:
 * $Log: access_port.c,v $
 * no message
 *
 *
 *=============================================================================
 */

/* INCLUDE FILE DECLARATIONS */
#include "access_port.h"
#include "switch.h"

#define swap_addr_bit(x) (x&~0x180)|((x&0x100)>>1)|((x&0x80)<<1)



/* NAMING CONSTANT DECLARATIONS */

/* GLOBAL VARIABLES DECLARATIONS */

/* LOCAL VARIABLES DECLARATIONS */
typedef void (*Write_Reg)(unsigned long value, void *membase);
typedef unsigned long(*Read_Reg)(void *membase);

Write_Reg	pWrite_Reg;
Read_Reg	pRead_Reg;

/* LOCAL SUBPROGRAM DECLARATIONS */
void inline
Write_Register_32bit(unsigned long value, void *membase)
{
	writel( value, membase);
}

void inline
Write_Register_16bit(unsigned long value, void *membase)
{
/*
	unsigned templ,temph;

	templ = swap_addr_bit((unsigned)membase);
	temph = swap_addr_bit((unsigned)membase+4);

	writew( (unsigned short)value, templ);
	writew( (unsigned short)(value >> 16), temph);
*/
	writew( (unsigned short)value, membase);
	writew( (unsigned short)(value >> 16), membase+4/*2*/);
}

void inline
Write_Register_8bit(unsigned long value, void *membase)
{
	writeb( (unsigned char)value, membase);
	writeb( (unsigned char)(value >> 8), membase+1);
	writeb( (unsigned char)(value >> 16), membase+2);
	writeb( (unsigned char)(value >> 24), membase+3);
}

unsigned long inline
Read_Register_32bit(void *membase)
{
	unsigned long data;
	data = readl(membase);
	return data;
}

unsigned long inline
Read_Register_16bit(void *membase)
{
//	unsigned templ,temph;

	unsigned long data;
	unsigned short *tmp = (unsigned short *)&data;
	
/*
	templ = swap_addr_bit((unsigned)membase);
	temph = swap_addr_bit((unsigned)membase+4);

	*tmp = readw(templ);
	*(tmp+1) = readw(temph);
*/

	*tmp = readw(membase);
	*(tmp+1) = readw(membase+4/*2*/);

	return data;
}

unsigned long inline
Read_Register_8bit(void *membase)
{
	unsigned long data;
	unsigned char *tmp = (unsigned char *)&data;
	*tmp = readb(membase);
	*(tmp+1) = readb(membase+1);
	*(tmp+2) = readb(membase+2);
	*(tmp+3) = readb(membase+3);
	return data;
}


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaAccessPortInit()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaAccessPortInit(u8 mode)
{
	switch(mode) {
	case CR_CHIP_MODE_8BIT:
		pWrite_Reg = Write_Register_8bit;
		pRead_Reg = Read_Register_8bit;
		printk("NapaAccessPortInit(): 8-bit mode\n");
		break;
	case CR_CHIP_MODE_16BIT:
		pWrite_Reg = Write_Register_16bit;
		pRead_Reg = Read_Register_16bit;
		printk("NapaAccessPortInit(): 16-bit mode\n");
		break;
	case CR_CHIP_MODE_32BIT:
		pWrite_Reg = Write_Register_32bit;
		pRead_Reg = Read_Register_32bit;
		printk("NapaAccessPortInit(): 32-bit mode\n");
		break;
	default:
		printk("NapaAccessPortInit(): unknown mode %hu\n", mode);
		break;
	}

} /* End of NapaAccessPortInit() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaReadRegister()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned long
NapaReadRegister(SWITCH *pSwitch, unsigned short RegAddr)
{
	unsigned long	TmpLong;
	unsigned short temp = 2* RegAddr;

//	printk("Register: 0x%x\n",RegAddr);
	TmpLong = pRead_Reg((unsigned char *)pSwitch->pMemBase + temp);

//	printk("In Read Add %X \n",(unsigned char *)pSwitch->pMemBase + temp);

	SDBG(SDBG_ACCESS, ("Read register 0x%x : 0x%lx\n\r", RegAddr, TmpLong));

	return TmpLong;

} /* End of NapaReadRegister() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaWriteRegister()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaWriteRegister(SWITCH *pSwitch, unsigned short RegAddr, unsigned long Value)
{

	unsigned short temp = 2* RegAddr;

	pWrite_Reg(Value, (unsigned char *)pSwitch->pMemBase + temp);

	SDBG(SDBG_ACCESS, ("Write register 0x%x : 0x%lx\n\r", RegAddr, Value));

} /* End of NapaWriteRegister() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaDelayMs()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void
NapaDelayMs(unsigned long Time)
{

	mdelay(Time);

} /* End of NapaDelayMs() */


/*
 * ----------------------------------------------------------------------------
 * Function Name: NapaGetCurrentTime()
 * Purpose:
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
unsigned long
NapaGetCurrentTime(void)
{

	return jiffies;

} /* End of NapaGetCurrentTime() */
