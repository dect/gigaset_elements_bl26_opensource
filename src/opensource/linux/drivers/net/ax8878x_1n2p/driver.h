/*
 ******************************************************************************
 *     Copyright (c) 2007	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
 /*============================================================================
 * Module Name: driver.h
 * Purpose:
 * Author:
 * Date:
 * Notes:
 * $Log: driver.h,v $
 * no message
 *
 *
 *=============================================================================
 */


#ifndef _DRIVER_H
#define _DRIVER_H

/* INCLUDE FILE DECLARATIONS */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/pci.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/ethtool.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/delay.h>
#include <linux/random.h>
#include <linux/mii.h>

#include <linux/in.h>

#include <asm/system.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/uaccess.h>

#include "switch/switch.h"

/* NAMING CONSTANT AND TYPE DECLARATIONS */
#define LINK_DOWN			0
#define LINK_UP				1
#define LINK_UNKNOW			2

typedef struct _ax_private {

	PSWITCH                 pSwitch;

	struct net_device      *dev;

	spinlock_t              lock;

	struct net_device_stats net_stats;

	struct mii_if_info      mii_if;

	u8						phyID[2];
	
	struct timer_list		watchdog;

	LINK_STURE				LinkStatus[2];
	
} AX_PRIVATE, *PAX_PRIVATE;

/* SMDK2440 Registers Definition */
/* SMDK2440 default clocks: FCLK=400MHZ, HCLK=125MHZ, PCLK=62.5MHZ */

#define CLKDIVN_125MHZ              (0x0000000F)
/* Set HCLK=FCLK/3, PCLK=HCLK/2 when CAMDIVN[8]=0 */

#define CAMDIVN_125MHZ              (0x00000000)
/* Set HCLK=FCLK/3, PCLK=HCLK/2 when CAMDIVN[8]=0 */

#define UBRDIV0_125MHZ              (0x00000023)
/* Set UART Baud Rate divisor for 125MHZ HCLK */

#define CLKDIVN_100MHZ              (0x0000000D)
/* Set HCLK=FCLK/4, PCLK=HCLK/2 when CAMDIVN[9]=0 */

#define CAMDIVN_100MHZ              (0x00000000)
/* Set HCLK=FCLK/4, PCLK=HCLK/2 when CAMDIVN[9]=0 */

#define UBRDIV0_100MHZ              (0x0000001B)
/* Set UART Baud Rate divisor for 100MHZ HCLK */

#define CLKDIVN_50MHZ               (0x0000000D)
/* Set HCLK=FCLK/8, PCLK=HCLK/2 when CAMDIVN[9]=1 */

#define CAMDIVN_50MHZ               (0x00000200)
/* Set HCLK=FCLK/8, PCLK=HCLK/2 when CAMDIVN[9]=1 */

#define UBRDIV0_50MHZ               (0x0000000D)
/* Set UART Baud Rate divisor for 50MHZ HCLK */

#define NAPA_DEFAULT_BANKCON1       (0x00000520)    /* Set Bank 1 access timing 8 clocks */
#define NAPA_BURST_BANKCON1         (0x0000062F)

/* EINTMASK Register Bit Definition */
#define EINT11_MASK                 (0x00000800)    /* Clear this bit to enable EINT11 interrupt */

/* EXTINT1 Register Bit Definition */
#define FLTEN11_HIGHLEVEL           (0x00009000)    /* Set EINT11 High Level trigger with noise filter */
#define FLTEN11_LOWLEVEL            (0x00008000)    /* Set EINT11 Low Level trigger with noise filter */
/* End of SMDK2440 Registers Definition */


#endif
