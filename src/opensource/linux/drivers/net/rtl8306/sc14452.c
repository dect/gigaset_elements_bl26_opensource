/*#################################################################################################
#
#  SiTel Semiconductor BV
#  Het Zuiderkruis 53
#  5215 MV 's-Hertogenbosch
#  The Netherlands
#
#  Project: VoIP
#  RHEA SOFTWARE PLATFORM
#  SC14452 EMAC LINUX DRIVER
#
#  File sc14452.c
#################################################################################################*/

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ioport.h>
#include <linux/init.h>
#include <linux/dma-mapping.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <asm/io.h>
#include <asm/system.h>

#include "sc14452.h"
#include "../asm.h"

static int emac_tx_timeout(struct net_device *dev);
struct net_device * __init sc14452_emac_probe(int unit);
#ifdef ROUTER_MODE
static int __init emac_init(struct net_device *dev, int unit);
#else
static int __init emac_init(struct net_device *dev/*, unsigned long ioaddr*/);
#endif
static int emac_tx_timeout(struct net_device *dev);
static int emac_open(struct net_device *dev);
static int emac_close(struct net_device *dev);
static void emac_timer_handler(unsigned long data);
static irqreturn_t emac_interrupt(int irq, void *dev_id, struct pt_regs * regs);
static irqreturn_t rmii_interrupt(int irq, void *dev_id, struct pt_regs * regs);
static int sc14452_ioctl(struct net_device *dev, struct ifreq *ifr, int cmd);
static int emac_xmit(struct sk_buff *skb, struct net_device *dev);
static struct net_device_stats *emac_get_stats(struct net_device *dev);
static void emac_set_multicast_list(struct net_device *dev);
static void emac_rx_tasklet_fn(unsigned long data);

#ifdef ROUTER_MODE
#define PORT_1  1
#define PORT_0  0

typedef struct _TOS_entry {
	unsigned char TOS_reg;
	unsigned char	TOS_DiffServ;
	unsigned char TOS_reg_val;
}TOS_entry;

#define MAX_TOS_CLASS_VALUES	64

TOS_entry TOS_table[MAX_TOS_CLASS_VALUES] =
{
	{0x60, 0x00, 0x03},
	{0x60, 0x04, 0x0C},
	{0x60, 0x08, 0x30},
	{0x60, 0x0C, 0xC0},
	{0x61, 0x10, 0x03},
	{0x61, 0x14, 0x0C},
	{0x61, 0x18, 0x30},
	{0x61, 0x1C, 0xC0},
	{0x62, 0x20, 0x03},
	{0x62, 0x24, 0x0C},
	{0x62, 0x28, 0x30},
	{0x62, 0x2C, 0xC0},
	{0x63, 0x30, 0x03},
	{0x63, 0x34, 0x0C},
	{0x63, 0x38, 0x30},
	{0x63, 0x3C, 0xC0},
	{0x64, 0x40, 0x03},
	{0x64, 0x44, 0x0C},
	{0x64, 0x48, 0x30},
	{0x64, 0x4C, 0xC0},
	{0x65, 0x50, 0x03},
	{0x65, 0x54, 0x0C},
	{0x65, 0x58, 0x30},
	{0x65, 0x5C, 0xC0},
	{0x66, 0x60, 0x03},
	{0x66, 0x64, 0x0C},
	{0x66, 0x68, 0x30},
	{0x66, 0x6C, 0xC0},
	{0x67, 0x70, 0x03},
	{0x67, 0x74, 0x0C},
	{0x67, 0x78, 0x30},
	{0x67, 0x7C, 0xC0},
	{0x68, 0x80, 0x03},
	{0x68, 0x84, 0x0C},
	{0x68, 0x88, 0x30},
	{0x68, 0x8C, 0xC0},
	{0x69, 0x90, 0x03},
	{0x69, 0x94, 0x0C},
	{0x69, 0x98, 0x30},
	{0x69, 0x9C, 0xC0},
	{0x6A, 0xA0, 0x03},
	{0x6A, 0xA4, 0x0C},
	{0x6A, 0xA8, 0x30},
	{0x6A, 0xAC, 0xC0},
	{0x6B, 0xB0, 0x03},
	{0x6B, 0xB4, 0x0C},
	{0x6B, 0xB8, 0x30},
	{0x6B, 0xBC, 0xC0},
	{0x6C, 0xC0, 0x03},
	{0x6C, 0xC4, 0x0C},
	{0x6C, 0xC8, 0x30},
	{0x6C, 0xCC, 0xC0},
	{0x6D, 0xD0, 0x03},
	{0x6D, 0xD4, 0x0C},
	{0x6D, 0xD8, 0x30},
	{0x6D, 0xDC, 0xC0},
	{0x6E, 0xE0, 0x03},
	{0x6E, 0xE4, 0x0C},
	{0x6E, 0xE8, 0x30},
	{0x6E, 0xEC, 0xC0},
	{0x6F, 0xF0, 0x03},
	{0x6F, 0xF4, 0x0C},
	{0x6F, 0xF8, 0x30},
	{0x6F, 0xFC, 0xC0}
};

#define MAX_REG_NUMBER 167

struct SWITCH_REGS_CMD {
	unsigned char cmd;
	unsigned int reg;
	unsigned int data;
	unsigned int data1;
	unsigned int all_regs[MAX_REG_NUMBER];
	unsigned int statistics[126];
};

struct audio_rtp_port {
	unsigned short local_rtp_port;
	unsigned char priotity_enable;
};

struct audio_rtp_port ports_to_prioritize[MAX_RTP_AUDIO_PORTS];

#define SWITCH_CMD_READ										0
#define SWITCH_CMD_WRITE									1
#define SWITCH_CMD_ST_MAC_READ						2
#define SWITCH_CMD_ST_MAC_WRITE						3
#define SWITCH_CMD_ST_MAC_DEL 						4
#define SWITCH_CMD_VLAN_WRITE							5
#define SWITCH_CMD_VLAN_DEL								6
#define SWITCH_CMD_LINK_DETECT						7
#define SWITCH_CMD_OPERATION_SELECT				8
#define SWITCH_CMD_TOS_ADD								9
#define SWITCH_CMD_LIMIT_BW								10
#define SWITCH_CMD_PRIORITIZE_RTP_PORT		11
#define SWITCH_CMD_READ_ALL								12
#define SWITCH_CMD_GET_STATISTICS					13
#define SWITCH_CMD_GET_PORT_SPEED         14
#define SWITCH_CMD_SET_STORM_CNTRL				15

extern unsigned int rtl8306_port_status(int port);
#endif


#define TMOUT 	(HZ >> 1)   // Timeout is at 1/2 sec

#ifndef ROUTER_MODE
#define EMAC_SPEED_100 SIOCDEVPRIVATE+1
#define EMAC_SPEED_10  SIOCDEVPRIVATE+2
#define EMAC_OPEN_SW   SIOCDEVPRIVATE+3
#define EMAC_CLOSE_SW  SIOCDEVPRIVATE+4


#ifdef CONFIG_SC14452_ES2
#define RMII_INT 2
extern void rmii_patch(void);
volatile unsigned short cache_disabled = 0;
volatile unsigned short xmit_enabled = 0;
volatile unsigned short rec_enabled = 0;
volatile unsigned short temp_rec_enabled = 0;

// The ES2 requires special cache handling.
void cache_enable(void)
{
	unsigned short temp;

	CACHE_CTRL_REG = 0x16;
	_spr_("cfg",temp);
	temp |= 0x14;	 //set bit 4
	_lpr_("cfg",temp);

	cache_disabled = 0;
}

void cache_disable(void)
{
	unsigned short temp;

	_spr_("cfg",temp);
	temp &= ~0x14;	 //reset bit 4
	_lpr_("cfg",temp);

	CACHE_CTRL_REG = 0;

	// use DMA to speed up the init part
	dma_meminit(0x9c00, 256, 2, 2, 0, 0);
	dma_wait(0);
	cache_disabled = 1;

}


void data_cache_inv(unsigned base, unsigned len)
{
	unsigned short temp;

	unsigned addr1,addr2,len1,len2 = 0;

	unsigned offset = (((base >> 4) & 0x1fe) *2);
	unsigned tags = (len>>5) +2;

	addr1 = 0x9c00 + offset;

	if ((offset + tags *4) > 1024)
	{
		len1 =  (1024 - offset) >> 2;
		addr2 = 0x9c00;
		len2 =  tags -len1;
	}
	else
	{
		len1 = tags;
	}

	_spr_("cfg",temp);
	temp &= ~0x14;	 //reset bit 4
	_lpr_("cfg",temp);
	CACHE_CTRL_REG = 0;
	DMA0_B_STARTH_REG = 0;
	DMA0_B_STARTL_REG = 0x9c00;
	DMA0_LEN_REG = 0x100;
	DMA0_CTRL_REG = 0x1825;
	while (DMA0_CTRL_REG & DMA_ON);

	cache_disabled = 1;
}
#endif
#endif

struct net_device * __init sc14452_probe(int unit)
{
	struct net_device *dev;

#ifdef ROUTER_MODE
	if(unit >1)
#else
	if(unit !=0)
#endif
		return NULL;
	dev = alloc_netdev(sizeof(struct net_local), "eth%d", ether_setup);
	register_netdev(dev);

	if (dev == NULL)
		return (struct net_device *)-ENOMEM;
#ifdef ROUTER_MODE
	emac_init(dev, unit);
#else
	emac_init(dev);
	#endif
#ifdef ROUTER_MODE
	printk("SC14452 EMAC  %s\n", dev->name);
#else
	printk("SC14452 EMAC dev_irq: %d\n", dev->irq);
#endif

	return dev;

}


#ifdef ROUTER_MODE
static int __init emac_init(struct net_device *dev, int unit)
#else
static int __init emac_init(struct net_device *dev/*, unsigned long ioaddr*/)
#endif

{
	static unsigned version_printed = 0;
	int i;
	struct net_local *lp;

	char * tmp;

	/* Initialize the device structure. */
	#ifdef ROUTER_MODE
	if (unit == 0) {
	#endif
	dev->priv = (struct net_local*)  kmalloc(sizeof(struct net_local), GFP_KERNEL);
	if (dev->priv == NULL)
		return -ENOMEM;

	memset(dev->priv, 0, sizeof(struct net_local));

	dev->base_addr = 0xFF2000;

	lp = (struct net_local *) dev->priv;

	lp->state= STATE_OFF;
	#ifdef ROUTER_MODE
	lp->mode = MODE_ROUTER;
	lp->devices[PORT_0] = dev;
	}
	else {
		dev->priv = (dev_get_by_name("eth0"))->priv;
		lp = (struct net_local *) dev->priv;
		lp->devices[PORT_1] = dev;
	}
	#endif
	printk(KERN_INFO "%s: %s found at %lx, ", dev->name, cardname, dev->base_addr);
#ifdef ROUTER_MODE
	if (unit == 0) {
#endif
	tmp = strstr(saved_command_line,"ethaddr=");
        if(tmp)  {
                sscanf(tmp,"ethaddr=%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
                                &(dev->dev_addr[0]),&(dev->dev_addr[1]),&(dev->dev_addr[2]),
                                &(dev->dev_addr[3]),&(dev->dev_addr[4]),&(dev->dev_addr[5]));
        }
        else  {
	/* 	Some error occured during system startup and the ethernet
		address was not passed from bootloader. Assign a fixed predefined MAC address
		to make the device accessible and debugable.*/
                dev->dev_addr[0] = 0x00;
                dev->dev_addr[1] = 0x88;
                dev->dev_addr[2] = 0x88;
                dev->dev_addr[3] = 0x77;
                dev->dev_addr[4] = 0x99;
                dev->dev_addr[5] = 0x66;
        }
#ifdef ROUTER_MODE

}
	else if (unit == 1) {
 		tmp = strstr(saved_command_line, "eth2addr=");

    if(tmp) {
			sscanf(tmp,"eth2addr=%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
			&(dev->dev_addr[0]),&(dev->dev_addr[1]),&(dev->dev_addr[2]),
			&(dev->dev_addr[3]),&(dev->dev_addr[4]),&(dev->dev_addr[5]));
		}
    else {
			/*Some error occured during system startup and the ethernet
			address was not passed from bootloader. Assign a fixed predefined MAC address
			to make the device accessible and debugable.
			*/
			//FATAL ERROR
 			dev->dev_addr[0] = 0x00; dev->dev_addr[1] = 0x88; dev->dev_addr[2] = 0x88;
			dev->dev_addr[3] = 0x77; dev->dev_addr[4] = 0x99; dev->dev_addr[5] = 0x67;
		}
	}
#endif
	/* Retrieve and print the ethernet address. */
	#ifdef ROUTER_MODE
	printk ("\n EMAC DEVICE ADDRESS (port:%d): ", unit);
	#else
	printk ("\n EMAC DEVICE ADDRESS: ");
	#endif
	for (i = 0; i < 6; i++)
		printk(" %2.2x", dev->dev_addr[i]);
	printk("\n");

	/* assign handlers */
	dev->open		= emac_open;
	dev->stop		= emac_close;
	dev->hard_start_xmit 	= emac_xmit;
	dev->get_stats		= emac_get_stats;
	dev->set_multicast_list = &emac_set_multicast_list;
	dev->tx_timeout = (void *)emac_tx_timeout;
	dev->watchdog_timeo	= 30*HZ;
	dev->tx_queue_len       = 100;
	dev->do_ioctl = sc14452_ioctl;

	dev->irq = EMAC_INT;

	/* Fill in the fields of the device structure with ethernet values. */
	ether_setup(dev);
#ifdef ROUTER_MODE
	if(unit == 0)
#endif
	eth_init(dev);

	SET_MODULE_OWNER(dev);
	return 0;
}


static int emac_tx_timeout(struct net_device *dev)
{
	return 0;
}

static int emac_open(struct net_device *dev)
{
	unsigned long phy_status = 0;
        struct net_local *lp = (struct net_local *)dev->priv;
	unsigned short i;
	int result;
	struct sk_buff * skb;
	unsigned long flags;
#ifdef ROUTER_MODE
	unsigned short id;
#endif
	/* Prepare descriptors and skbs */
	/*Prepare tx descriptors and ring buffer */
#ifdef ROUTER_MODE
if(lp->mode == MODE_UNDEFINED) {
		lp->mode = MODE_ROUTER;
	}
		if(lp->mode == MODE_SWITCH)
		eth_init(dev);
#endif
	spin_lock_init(&lp->lock);
#ifdef ROUTER_MODE
	if((dev->name)[3] == '0')
		id = PORT_0;
	else
		id = PORT_1;

	lp->port_state[id] = 1;

#endif
#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
	spin_lock_init(&lp->filter_lock);
#endif
#ifdef ROUTER_MODE
	if(lp->state == STATE_OFF) {
#endif
	lp->tx_ring_size = 0;
	lp->TX_RING_TAIL = 0;
	lp->TX_RING_HEAD = 0;

	for (i=0;i<MAX_TX_DESCS;i++)
	{
		lp->TX_RING[i] = NULL;
	}

	/*prepare rx descriptors and ring buffer*/
	for(i=0;i<MAX_RX_DESCS;i++)
	{
		skb = dev_alloc_skb(MAX_PACKET_LEN);
		if( unlikely( (unsigned)skb->data >= 0x00FED000 ) )
			printk( "%s: skb=%p!!!\n", __FUNCTION__, skb ) ;
		if (!skb)
		{
			printk("Could not allocate skb at open. \n");
			goto out;
		}
		#ifdef ROUTER_MODE
			skb_reserve(skb, 6);
			lp->RX_RING[i] = skb;
			lp->RX_DESCS[i].DES2 = (volatile unsigned long int) skb->data;

		#else
		skb_reserve(skb,2);
		lp->RX_RING[i] = skb;
		lp->RX_DESCS[i].DES2 = skb->data;
		#endif
		lp->RX_DESCS[i].DES0 = 0x80000000; /* DMA OWNED... */
	}

	lp->CUR_RX_DESC = 0;
	lp->max_tx_packets = 0;

	tasklet_init(&lp->emac_rx_tasklet, emac_rx_tasklet_fn, (unsigned long)dev);


#ifdef CONFIG_SC14452_ES2
	KEY_GP_INT_REG = 6;
	SetPort(P2_08_MODE_REG, PORT_PULL_DOWN, 41);    // P2_08 is int6

	while( RESET_INT_PENDING_REG & 0x2 ) {
		KEY_STATUS_REG = RMII_INT ;
		RESET_INT_PENDING_REG = 0x2;
	}

spin_lock_irqsave(&lp->lock,flags);
cache_disable();
#endif
	emac_cfg(dev->dev_addr, lp);

#ifdef CONFIG_SC14452_ES2
cache_enable();
spin_unlock_irqrestore(&lp->lock,flags);
#endif

	/* Acquire the IRQ before any is generated */
	#ifdef ROUTER_MODE
		result = request_irq(dev->irq, (void *)&emac_interrupt, SA_INTERRUPT, "emac_irq", lp->devices[0]);
	#else
		result = request_irq(dev->irq, &emac_interrupt, SA_INTERRUPT, "emac_irq", dev);
	#endif
	if (result)
	{
		printk("Could not acquire EMAC IRQ %d \n",dev->irq);
		goto out;
	}
#ifdef CONFIG_SC14452_ES2
	/* Acquire the IRQ before any is generated */
	result = request_irq(KEYB_INT, &rmii_interrupt, SA_INTERRUPT, "rmii_irq", dev);
	if (result)
	{
		printk("Could not acquire RMII IRQ %d \n",KEYB_INT);
		goto out;
	}
#endif


	init_timer(&lp->emac_timer);
	#ifdef ROUTER_MODE
	lp->emac_timer.expires = jiffies + (2* TMOUT);
	#else
	lp->emac_timer.expires = jiffies + TMOUT;
	#endif
	lp->emac_timer.data = (unsigned long)dev;
	lp->emac_timer.function = &emac_timer_handler;
	add_timer(&lp->emac_timer);

#if 0
#if defined( CONFIG_SC1445x_POWER_DRIVER )
    {
        u16 creg, phy_addr, phy_reg;

        for ( phy_addr = 1; phy_addr < 5; phy_addr++ ) {
            phy_reg = 0;
            creg = EMAC_MACR4_MII_ADDR_REG;
            while ((creg & 0x1) == 1) creg = EMAC_MACR4_MII_ADDR_REG;
            creg &= ~( 0x1f<<6 | 0x1f<<11);
            creg |=  (phy_addr << 11) | (phy_reg << 6) | 0x3;
            EMAC_MACR5_MII_DATA_REG = 0x800;
            EMAC_MACR4_MII_ADDR_REG = creg;

        }

    }
#endif
#endif
#ifdef ROUTER_MODE
		lp->state = STATE_ON;
	}
	if(lp->mode == MODE_ROUTER) {
		if (rtl8306_port_status(id)) {
			printk("EMAC: eth%d Link detected\n", id);
			lp->Link[id] = 1;
			netif_carrier_on(dev);
#endif
	netif_start_queue(dev);
#ifdef ROUTER_MODE
		}
		else{
			printk("EMAC: eth%d Link NOT detected\n", id);
			lp->Link[id] = 0;
			netif_carrier_off(dev);
		}
	}
	else //MODE SWITCH
	{
		if (rtl8306_port_status(id))
		{
			printk("EMAC: Port%d Link detected\n", id);
			lp->Link[id] = 1;
		}
		else
		{
			printk("EMAC: Port%d Link NOT detected\n", id);
			lp->Link[id] = 0;
		}

		if (rtl8306_port_status(1 - id))
		{
			printk("EMAC: Port%d Link detected\n", 1 - id);
			lp->Link[1 - id] = 1;
		}
		else
		{
			printk("EMAC: Port%d Link NOT detected\n", 1 - id);
			lp->Link[id] = 0;
		}

		netif_carrier_on(dev);
		netif_start_queue(dev);
	}
#endif
	return 0;
out:
	/* Clean any gathered SKBs */
	for(i=0;i<MAX_RX_DESCS;i++)
	{
		if(lp->RX_RING[i])
			dev_kfree_skb(lp->RX_RING[i]);
	}
	return -ENOMEM;
}

static int emac_close(struct net_device *dev)
{
    struct net_local *lp = (struct net_local *)dev->priv;
    #ifdef ROUTER_MODE
    unsigned short i, id;

    if((dev->name)[3] == '0')
		id = PORT_0;
	else
		id = PORT_1;

	// Re-Initialize statistics
	memset(&lp->stats[id], 0, sizeof(struct net_device_stats));

	lp->port_state[id] = 0;
	lp->Link[id] = 0;
    #else
	unsigned short i;
	#endif

	#ifdef ROUTER_MODE
		netif_stop_queue(dev);
	if(lp->port_state[1-id] == 0) { // Both interfaces down ....
	#endif
	lp->state= STATE_OFF;
	del_timer(&lp->emac_timer);
	#ifdef ROUTER_MODE
	free_irq(dev->irq, lp->devices[0]);
	#else
	netif_stop_queue(dev);
	free_irq(dev->irq, dev);
	#endif
#ifdef CONFIG_SC14452_ES2
	free_irq(KEYB_INT, dev);
#endif
	for(i=0;i<MAX_RX_DESCS;i++)
		if(lp->RX_RING[i])
			dev_kfree_skb(lp->RX_RING[i]);

	for(i=0;i<MAX_TX_DESCS;i++)
		if(lp->TX_RING[i])
			dev_kfree_skb(lp->TX_RING[i]);

	tasklet_kill(&lp->emac_rx_tasklet);

	EMAC_MACR0_CONFIG_REG &= ~0x3;

	emac_init_tx_desc(lp->TX_DESCS);
	emac_init_rx_desc(lp->RX_DESCS);

	emac_soft_rst();

#ifdef CONFIG_SC14452_ES2
	rec_enabled = 1;
#endif
	#ifdef ROUTER_MODE
		} // Both interfaces down...
	#endif
	return 0;
}

/* ********************************************************************************************************
*  The ioctl function is used to pass address filtering information between the driver and the application.
*  The SC14452 can protect up to 4 Ethernet addresses. This mechanism is used to ensure phone quality in
*  	malicious network load conditions.
*  The SIOCDEVPRIVATE slot is used for this purpose.
*  The commands exchanged are:
*
*	EMAC_LF_STATUS          0
*	EMAC_LF_ADD             1
*	EMAC_LF_REMOVE          2
*	EMAC_LF_REMOVE_ALL      3
*	EMAC_LF_START_ALL       4
*	EMAC_LF_START_BCAST     5
*	EMAC_LF_START_UCAST     6
*	EMAC_LF_STOP_ALL        7
*	EMAC_LF_STOP_BCAST      8
*	EMAC_LF_STOP_UCAST      9
*****************************************************************************************************/

#ifndef ROUTER_MODE
static void open_switch_ports(void)
{
	u16 creg, phy_addr, phy_reg;
	unsigned int temp;

  for ( phy_addr = 1; phy_addr < 5; phy_addr++ ) {

    	temp = emac_md_read (phy_addr, 0, EMAC_MDC_DIV_42);
     	temp = temp & ~0x800;
     	emac_md_write (phy_addr, 0, EMAC_MDC_DIV_42,temp);

/*        	phy_reg = 0;
      creg = EMAC_MACR4_MII_ADDR_REG;
      while ((creg & 0x1) == 1) creg = EMAC_MACR4_MII_ADDR_REG;
      creg &= ~( 0x1f<<6 | 0x1f<<11);
      creg |=  (phy_addr << 11) | (phy_reg << 6) | 0x3;
      EMAC_MACR5_MII_DATA_REG = 0x8200;
      EMAC_MACR4_MII_ADDR_REG = creg;
*/
  }

}

static void close_switch_ports(void)
{
  u16 creg, phy_addr, phy_reg;
	unsigned int temp;

  for ( phy_addr = 1; phy_addr < 5; phy_addr++ ) {

    	temp = emac_md_read (phy_addr, 0, EMAC_MDC_DIV_42);
     	temp = temp | 0x800;
     	emac_md_write (phy_addr, 0, EMAC_MDC_DIV_42,temp);
/*
      phy_reg = 0;
      creg = EMAC_MACR4_MII_ADDR_REG;
      while ((creg & 0x1) == 1) creg = EMAC_MACR4_MII_ADDR_REG;
      creg &= ~( 0x1f<<6 | 0x1f<<11);
      creg |=  (phy_addr << 11) | (phy_reg << 6) | 0x3;
      EMAC_MACR5_MII_DATA_REG = 0x800;
      EMAC_MACR4_MII_ADDR_REG = creg;
*/
  }

}
#endif

static int sc14452_ioctl(struct net_device *dev, struct ifreq *ifr, int cmd)
{
        struct net_local *lp = (struct net_local *)dev->priv;
   #ifdef ROUTER_MODE
        struct SWITCH_REGS_CMD *switch_cmd_p;
		unsigned int i, k = 0;
		unsigned int temp = 0;
   #endif
   #ifndef ROUTER_MODE
	#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
		struct addr_filter_ioctl * command_struct_p;
	#else
		struct mii_struct * command_struct_p;
	#endif
	int i;
	int count = 0;
	int found = 0;
	unsigned long flags;


        if (!netif_running(dev))
                return -ENODEV;
	#endif
        switch (cmd) {
        #ifndef ROUTER_MODE
        case EMAC_CLOSE_SW:
        	spin_lock_irqsave(&lp->lock,flags); /* protect from acces from other ptocesses */
        	close_switch_ports();
        	spin_unlock_irqrestore(&lp->lock,flags);
        	return 0;
        	break;
        case EMAC_OPEN_SW:
        	spin_lock_irqsave(&lp->lock,flags); /* protect from acces from other ptocesses */
        	open_switch_ports();
        	spin_unlock_irqrestore(&lp->lock,flags);
        	return 0;
        	break;
        case EMAC_SPEED_10:
//        	printk("emac_ioctl_10\n");
			spin_lock_irqsave(&lp->lock,flags); /* protect from acces from other ptocesses */
			emac_init_speed(10);

			lp->tx_ring_size = 0;
			lp->TX_RING_TAIL = 0;
			lp->TX_RING_HEAD = 0;

			for (i=0;i<MAX_TX_DESCS;i++)
			{
				lp->TX_RING[i] = NULL;
			}

			/*prepare rx descriptors and ring buffer*/
			for(i=0;i<MAX_RX_DESCS;i++)
			{
				lp->RX_DESCS[i].DES0 = 0x80000000; /* DMA OWNED... */
			}

			lp->CUR_RX_DESC = 0;
			lp->max_tx_packets = 0;



			emac_cfg(dev->dev_addr, lp);
			spin_unlock_irqrestore(&lp->lock,flags);
			return 0;
        	break;
        case EMAC_SPEED_100:
 //       	printk("emac_ioctl_100\n");
			spin_lock_irqsave(&lp->lock,flags); /* protect from acces from other ptocesses */
			emac_init_speed(100);

			lp->tx_ring_size = 0;
			lp->TX_RING_TAIL = 0;
			lp->TX_RING_HEAD = 0;

			for (i=0;i<MAX_TX_DESCS;i++)
			{
				lp->TX_RING[i] = NULL;
			}

			/*prepare rx descriptors and ring buffer*/
			for(i=0;i<MAX_RX_DESCS;i++)
			{
				lp->RX_DESCS[i].DES0 = 0x80000000; /* DMA OWNED... */
			}

			lp->CUR_RX_DESC = 0;
			lp->max_tx_packets = 0;


			emac_cfg(dev->dev_addr, lp);
			spin_unlock_irqrestore(&lp->lock,flags);
			return 0;
        	break;
        case SIOCDEVPRIVATE:
		command_struct_p = ( struct mii_struct *) ifr->ifr_data;
		if (command_struct_p->command > 500)
		{
			switch (command_struct_p->command){
				case EMAC_MII_READ:
			 		command_struct_p->mii_data = emac_md_read ((unsigned char)(command_struct_p->mii_id), (unsigned char)(command_struct_p->mii_register),EMAC_MDC_DIV_42);
					break;
				case EMAC_MII_WRITE:
					emac_md_write ((unsigned char)(command_struct_p->mii_id), (unsigned char) (command_struct_p->mii_register), EMAC_MDC_DIV_42, command_struct_p->mii_data);
					break;
				default:
					return -EOPNOTSUPP;
			}
			return 0;
		}
		#endif
#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
        case SIOCDEVPRIVATE:
        spin_lock_irqsave(&lp->filter_lock,flags); /* protect access from other processes */
		command_struct_p = ( struct addr_filter_ioctl *) ifr->ifr_data;
		switch (command_struct_p->command)
		{
			case EMAC_LF_STATUS:
					command_struct_p ->command = EMAC_LF_COMMAND_SUCCESS;
					break;
			case EMAC_LF_ADD:
					/* Broadcast addresses are not allowed ...*/
					if ( command_struct_p->status.address_filters[0].MAC[0] & 0x1){
						command_struct_p->command = EMAC_LF_COMMAND_FAIL_BCAST;
						break;
					}
					for (i=0;i<4;i++)
					{	/* look for an empty slot */
						if(lp->filter_config.address_filters[i].enable){
							count++;
						}
						else
						{
							lp->filter_config.address_filters[i].enable = 1;
							lp->filter_config.address_filters[i].pid = command_struct_p->pid;
							memcpy(lp->filter_config.address_filters[i].MAC ,command_struct_p->status.address_filters[0].MAC , 6);
							/* It is important here that the second address register is writen last. Otherwise the HW will not
							   recognize the MAC address correctly */
							*(unsigned long *)(0xff2048 +(i*8))= (0xc000 <<16)| (lp->filter_config.address_filters[i].MAC[5] <<8 )| (lp->filter_config.address_filters[i].MAC[4] )	;
							*(unsigned long *)(0xff204c +(i*8))= (lp->filter_config.address_filters[i].MAC[3] <<24 )| (lp->filter_config.address_filters[i].MAC[2] << 16 ) | (lp->filter_config.address_filters[i].MAC[1] <<8 )| (lp->filter_config.address_filters[i].MAC[0]  );
							command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
							break;
						}
					}
					if(count == 4)
						command_struct_p->command = EMAC_LF_COMMAND_FAIL_FULL;
					break;
			case EMAC_LF_REMOVE:
					found = 0;
					for (i=0;i<4;i++)
					{
						if (lp->filter_config.address_filters[i].enable == 1)
						{
							if(!memcmp(lp->filter_config.address_filters[i].MAC , command_struct_p->status.address_filters[i].MAC,6))
							{
								found = 1;
								lp->filter_config.address_filters[i].enable = 0;
								*(unsigned long *)(0xff2048 +(i*8))= 0x0;
								*(unsigned long *)(0xff204c +(i*8))= 0x0;
								command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
								break;
							}
						}
					}
					if(!found)
					{
						command_struct_p->command = EMAC_LF_COMMAND_FAIL_NFOUND;
					}
					break;
			case EMAC_LF_REMOVE_ALL:
					break;
			case EMAC_LF_START_ALL:
					break;
			case EMAC_LF_START_BCAST:
					lp->filter_config.blp_enable = 1;
					lp->filter_config.blp_threshold = command_struct_p->status.blp_threshold;
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
			case EMAC_LF_START_UCAST:
					lp->filter_config.ulp_enable = 1;
					lp->filter_config.ulp_threshold = command_struct_p->status.ulp_threshold;
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
			case EMAC_LF_STOP_ALL:
					lp->filter_config.ulp_enable = 0;
					lp->filter_config.blp_enable = 0;
					lp->filter_config.blp_threshold = 0;
					lp->filter_config.ulp_threshold = 0;
					lp->filter_config.blp_protect = 0;
					lp->filter_config.ulp_protect = 0;
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 5) ;
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 9) ;
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
			case EMAC_LF_STOP_BCAST:
					lp->filter_config.blp_enable = 0;
					lp->filter_config.blp_threshold = 0;
					lp->filter_config.blp_protect = 0;
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 5) ;
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
			case EMAC_LF_STOP_UCAST:
					lp->filter_config.ulp_enable = 0;
					lp->filter_config.ulp_threshold = 0;
					lp->filter_config.ulp_protect = 0;
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 9) ;
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
			default:
				return -EOPNOTSUPP;
		}
		spin_unlock_irqrestore(&lp->filter_lock,flags);
		/* Always copy the status structure to provide status info to the application */
		memcpy(&(command_struct_p->status),&(lp->filter_config),sizeof(struct addr_filter_status));
                return 0;
#endif
#ifdef ROUTER_MODE

///TODO

#if 0
		case SIOCDEVPRIVATE:
			switch_cmd_p = ( struct SWITCH_REGS_CMD *) ifr->ifr_data;
			switch (switch_cmd_p->cmd) {
				case SWITCH_CMD_READ:
					//printk("reading switch_cmd_p->reg=[%x]\n", switch_cmd_p->reg);
					switch_cmd_p->data = read_rtl8306(switch_cmd_p->reg);
					//printk("reading 0x%04x = 0x%08x \n", switch_cmd_p->reg, switch_cmd_p->data);
					break;
				case SWITCH_CMD_READ_ALL:
					k = 0;
					for (i=0; i<47; i++) {
						switch_cmd_p->all_regs[k] = read_rtl8306(i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}

					for (i=0; i<9; i++) {
						switch_cmd_p->all_regs[k] = read_rtl8306(0x100+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x100+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}
#ifdef CONFIG_SC14452_ATHEROS_AR8236
					for (i=0; i<9; i++) {
						switch_cmd_p->all_regs[k] = read_rtl8306(0x200+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x100+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}
#endif
					for (i=0; i<9; i++) {
						switch_cmd_p->all_regs[k] = read_rtl8306(0x300+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x300+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}
					for (i=0; i<9; i++) {
						switch_cmd_p->all_regs[k] = read_rtl8306(0x400+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x400+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}

#ifdef CONFIG_SC14452_ATHEROS_AR8236
					for (i=0; i<9; i++) {
						switch_cmd_p->all_regs[k] = read_ar8xxx(0x500+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x100+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}
#endif
					for (i=0; i<9; i++) {
						switch_cmd_p->all_regs[k] = read_rtl8306(0x600+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x600+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}

					for (i=0; i<32; i++) {
						switch_cmd_p->all_regs[k] = emac_md_read (1, i, EMAC_MDC_DIV_42);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x600+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}

					for (i=0; i<32; i++) {
						switch_cmd_p->all_regs[k] = emac_md_read (2, i, EMAC_MDC_DIV_42);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x600+i, switch_cmd_p->all_regs[k]);
						if (k < MAX_REG_NUMBER) k++;
						else printk("ETH ERROR: Read register out of range\n");
					}
					break;

				case SWITCH_CMD_GET_STATISTICS:
					k = 0;
					for (i=0; i<42; i++) {
						switch_cmd_p->statistics[k] = rtl8306(0x20000+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k,  i, switch_cmd_p->all_regs[k]);
						k++;
					}
					for (i=0; i<42; i++) {
						switch_cmd_p->statistics[k] = rtl8306(0x20200+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x100+i, switch_cmd_p->all_regs[k]);
						k++;
					}
					for (i=0; i<42; i++) {
						switch_cmd_p->statistics[k] = rtl8306(0x20300+i*4);
						//printk("k=[%d] reading 0x%04x = 0x%08x \n", k, 0x300+i, switch_cmd_p->all_regs[k]);
						k++;
					}
					break;
				case SWITCH_CMD_WRITE:
					write_ar8xxx(switch_cmd_p->reg, switch_cmd_p->data);
					break;
				case SWITCH_CMD_VLAN_WRITE:
#ifdef CONFIG_SC14452_ATHEROS_AR8314
					// Enable Forward Mode, Learning and egress VLAN
					write_ar8xxx(0x104, 0x4904);
					write_ar8xxx(0x304, 0x4104);
					write_ar8xxx(0x404, 0x4204);
					// Enable VLAN and Port based priorities
					write_ar8xxx(0x110, 0xa001b);
					write_ar8xxx(0x310, 0xa001b);
					write_ar8xxx(0x410, 0xa001b);
#elif defined CONFIG_SC14452_ATHEROS_AR8236
					// Enable VLAN
					write_ar8xxx(0x10c, 0x40100000);
					write_ar8xxx(0x30c, 0x40100000);
					write_ar8xxx(0x50c, 0x402f0000);
					// Enable Forward Mode, Learning and egress VLAN
					write_ar8xxx(0x104, 0x4904);
					write_ar8xxx(0x304, 0x4104);
					write_ar8xxx(0x504, 0x4204);
					// Enable VLAN and Port based priorities
					write_ar8xxx(0x114, 0xa001b);
					write_ar8xxx(0x314, 0xa001b);
					write_ar8xxx(0x514, 0xa001b);
#endif
					break;
				case SWITCH_CMD_VLAN_DEL:
#ifdef CONFIG_SC14452_ATHEROS_AR8314
					write_ar8xxx(0x108, 0x003e0001);
					write_ar8xxx(0x308, 0x003b0001);
					write_ar8xxx(0x408, 0x00370001);
					write_ar8xxx(0x104, 0x4804);
					write_ar8xxx(0x304, 0x4004);
					write_ar8xxx(0x404, 0x4004);
#elif defined CONFIG_SC14452_ATHEROS_AR8236
					write_ar8xxx(0x108, 0x10000);
					write_ar8xxx(0x308, 0x10000);
					write_ar8xxx(0x508, 0x10000);
					write_ar8xxx(0x104, 0x4804);
					write_ar8xxx(0x304, 0x4004);
					write_ar8xxx(0x504, 0x4004);
					write_ar8xxx(0x10c, 0x3e0000);
					write_ar8xxx(0x30c, 0x3b0000);
					write_ar8xxx(0x50c, 0x2f0000);
#endif
					break;
				case SWITCH_CMD_TOS_ADD:
					break;
				case SWITCH_CMD_LIMIT_BW:
					break;
				case SWITCH_CMD_PRIORITIZE_RTP_PORT:
					// Disable prioritization on a single port.
					if (switch_cmd_p->reg == 0) {
						for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) {
							if (ports_to_prioritize[i].local_rtp_port == switch_cmd_p->data) {
								ports_to_prioritize[i].local_rtp_port = 0;
								ports_to_prioritize[i].priotity_enable = 0;
								temp = read_ar8xxx(0x58428 + i*0x20);
								if (((temp & 0xffff0000) >> 16) == switch_cmd_p->data) {
									//printk("PORT [%d] FOUND. Disable Prioritization\n", (temp & 0xffff0000) >> 16);
									write_ar8xxx(0x58824 + i*0x20, 0);					// Addr 0
									write_ar8xxx(0x58820 + i*0x20, 0);					// Addr valid
								}
								break;
							}
						}
						if (i == MAX_RTP_AUDIO_PORTS)
							printk("ETH WARNING: Port is not in prioritization list\n");

						for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) {
							if (ports_to_prioritize[i].priotity_enable == 0) continue;
							else break;
						}
						if (i == MAX_RTP_AUDIO_PORTS) {
							//printk("ETH: Port prioritization fully disable\n");
							write_ar8xxx(0x3c, 0xc000000e);			// Disable ACL
							write_ar8xxx(0x11c, 0x7fff7fff);		// Set engress priority limits to max
							write_ar8xxx(0x120, 0x7fff7fff);		// Set engress priority limits to max
						}
					}
					else {
						for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) {
							//port already exists and is enabled
							if ((ports_to_prioritize[i].local_rtp_port == switch_cmd_p->data) && (ports_to_prioritize[i].priotity_enable == 1)) {
								return 0;
							}
						}
						for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) {
							if (ports_to_prioritize[i].priotity_enable == 0) {
								ports_to_prioritize[i].local_rtp_port = switch_cmd_p->data;
								ports_to_prioritize[i].priotity_enable = 1;
								write_ar8xxx(0x3c, 0xc020000e);
								temp = (switch_cmd_p->data << 16) | 0x11; // UDP destination port to prioritize and protocol. UDP=0x11, TCP=0x6
								//printk("UDP PORT=[%x] \n", temp);
								write_ar8xxx(0x58428 + i*0x20, temp);
								write_ar8xxx(0x58824 + i*0x20, i+1);					// Addr 0
								write_ar8xxx(0x58820 + i*0x20, 1);					// Addr valid
								write_ar8xxx(0x11c, 0x00220022);		// Set engress priority limits
								write_ar8xxx(0x120, 0x00a00022);		// Set engress priority limits
								break;
							}
						}
						if (i == MAX_RTP_AUDIO_PORTS)
							printk("ETH ERROR: No available place for audio RTP port prioritization \n");
					}
					return 0;
#endif

				case SWITCH_CMD_LINK_DETECT:
						switch_cmd_p->data = lp->Link[0];
						switch_cmd_p->data1 = lp->Link[1];
					return 0;
				case SWITCH_CMD_OPERATION_SELECT:
					if (switch_cmd_p->reg == MODE_ROUTER) {
						lp->mode = MODE_ROUTER;
					}
					else if (switch_cmd_p->reg == MODE_SWITCH) {
						lp->mode = MODE_SWITCH;
					}
					return 0;

				case SWITCH_CMD_GET_PORT_SPEED:
					switch_cmd_p->data = rtl8306_port_get_speed(0);
					switch_cmd_p->data1 = rtl8306_port_get_speed(1);
					return 0;
#if 0
				case SWITCH_CMD_SET_STORM_CNTRL:
					if (switch_cmd_p->reg == 1) {
#ifdef CONFIG_SC14452_ATHEROS_AR8314
						// WAN port storm control
						write_ar8xxx(0x414, 0x702);
#elif defined CONFIG_SC14452_ATHEROS_AR8236
						// WAN port storm control
						write_ar8xxx(0x518, 0x702);
#endif
						// PC port storm control
						write_ar8xxx(0x314, 0x702);

					}else {
#ifdef CONFIG_SC14452_ATHEROS_AR8314
						// WAN port storm control
						write_ar8xxx(0x414, 0x0);
#elif defined CONFIG_SC14452_ATHEROS_AR8236
						// WAN port storm control
						write_ar8xxx(0x518, 0x0);
#endif
						// PC port storm control
						write_ar8xxx(0x314, 0x0);
					}
					return 0;
				default: break;
			}
			return 0;
#endif
			default: break;


#endif
        }
        return -EOPNOTSUPP;
}

/*********************************************************************************************
*  The timer handler is used to run the Ethernet address filtering mechanism. The timer ticks
*  every 1/2 sec. If the address filters are enabled, it checks whether the received unicast
*  or broadcast packets exceed the user defined threshold. In this case, the filtering is enabled.
*  For unicast filtering, the MMC counters are then checked at every tick and the number
*  of filtered packets are monitored against the threshold. If this nomber drops below the
*  threshold, the filtering is disabled. For broadcast trafic, after disabling the reception
*  the system enables again in 4 seconds to check whether the broadcast storm is over.
*********************************************************************************************/
static void emac_timer_handler(unsigned long data)
{
        struct net_device *dev = (struct net_device *)data;
        struct net_local *lp = (struct net_local *)dev->priv;
    #ifndef ROUTER_MODE
	unsigned long phy_status = 0;
	#else
	int i;

	for(i=0; i<2; i++)
	{
		if(lp->mode == MODE_ROUTER)
		{
  		if(lp->port_state[i] == 1)
			{
				if(lp->Link[i] != rtl8306_port_status(i)) // status changed....
				{
					if(lp->Link[i] == 0) // Link detected....
					{
						printk(" EMAC: eth%d Link Up !\n", i);
						lp->Link[i] = 1;
						netif_carrier_on(lp->devices[i]);
					}
					else
					{
						printk(" EMAC: eth%d Link Down !\n", i);
						lp->Link[i] = 0;
						netif_carrier_off(lp->devices[i]);

					}
				}
			}
		}
		else // MODE_SWITCH
		{
			if(rtl8306_port_status(i)) //carrier found
			{
				if(lp->Link[i] == 0)
				{
					printk(" EMAC: Port%d Link Up !\n", i);
					lp->Link[i] = 1;
				}
			}
			else // NO CARRIER FOUND
			{
				if(lp->Link[i] == 1)
				{
					printk(" EMAC: Port%d Link Down !\n", i);
					lp->Link[i] = 0;
				}
			}
		}
	}

	#endif
#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
	unsigned long btotal,utotal;
	static int protect_slots = 0;
	unsigned long flags;

	spin_lock_irqsave(&lp->filter_lock,flags);
	btotal = EMAC_MMC_RXBROADCASTFRAMES_G_REG;
	utotal = EMAC_MMC_RXUNICASTFRAMES_G_REG;
	lp->filter_config.ups = lp->tmp_ups * 2;
	lp->filter_config.bps = lp->tmp_bps * 2;
	if(lp->filter_config.ulp_enable){
		if(utotal > lp->filter_config.ulp_threshold){
			EMAC_MACR1_FRAME_FILTER_REG |= (0x1 << 9);
			lp->filter_config.ulp_protect = 1;
		}
		else{
			EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 9) ;
			lp->filter_config.ulp_protect = 0;
		}
		lp->filter_config.ulp_filtered = utotal - lp->filter_config.ups;
	}
	if(lp->filter_config.blp_enable){
		if(lp->filter_config.blp_protect == 0){
			if(btotal > lp->filter_config.blp_threshold){
				EMAC_MACR1_FRAME_FILTER_REG |= (0x1 << 5);
				lp->filter_config.blp_protect = 1;
				protect_slots = 8;
			}
		}
		else{
			protect_slots --;
			if(protect_slots == 0){
				EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 5) ;
				lp->filter_config.blp_protect = 0;
			}
		}
		lp->filter_config.blp_filtered = btotal - lp->filter_config.bps;
	}
	lp->tmp_ups = 0;
	lp->tmp_bps = 0;

	spin_unlock_irqrestore(&lp->filter_lock,flags);
#endif
	lp->emac_timer.expires = jiffies + TMOUT;
	add_timer(&lp->emac_timer);
}

static void emac_rx_tasklet_fn(unsigned long data)
{
        struct net_device *dev = (struct net_device *)data;
        struct net_local *lp = (struct net_local *) dev->priv;
	unsigned long status;
	int cur_rx_pos;
	unsigned int ik;
	unsigned long des0;
	#ifndef ROUTER_MODE
	unsigned short frame_len,Packet_Count,i,loopcount = 8;
	#else
	unsigned short frame_len, Packet_Count, i;
	#endif

	struct sk_buff *skb, *new_skb;
	#ifndef ROUTER_MODE
	unsigned temp;
	unsigned short temp1;
	unsigned long flags;
	#else
	unsigned short id = 0;
	char tmpbuf [12];
	#endif


	status = GetDword (EMAC_DMAR5_STATUS_REG);

		Packet_Count = 0;
		cur_rx_pos = lp->CUR_RX_DESC;
		des0 = lp->RX_DESCS[cur_rx_pos].DES0;
		while(((des0 & 0x80000000) == 0) && (Packet_Count < 4) )
		{
			Packet_Count ++;
			if(des0 & (0x1<<15) ){
			#ifdef ROUTER_MODE
			lp->stats[0].rx_errors++;
			#else
				lp->stats.rx_errors++;
			#endif
				if(des0 & (0x1<<1))
				#ifdef ROUTER_MODE
					lp->stats[0].rx_crc_errors++;
				#else
					lp->stats.rx_crc_errors++;
				#endif
			}
			else
			{ /* GET THE PACKET AND forward it to the system */
			#ifdef ROUTER_MODE

#if 0
				printk ("D: %2.2x,%2.2x,%2.2x,%2.2x\r\n",lp->RX_RING[cur_rx_pos]->data[12] \
														,lp->RX_RING[cur_rx_pos]->data[13] \
														,lp->RX_RING[cur_rx_pos]->data[14] \
														,lp->RX_RING[cur_rx_pos]->data[15] );
#endif
				frame_len = ((des0 >> 16) & 0x3fff) - 10;

				flush_dcache_range(lp->RX_DESCS[cur_rx_pos].DES2, lp->RX_DESCS[cur_rx_pos].DES2+frame_len + 10);

				id = (lp->RX_RING[cur_rx_pos]->data[15] & 0x3);

				if (id==2)
					id=1;
				else
					id=0;

			 	if((lp->mode == MODE_SWITCH) || (id > 1))
			 		id = 0;

				dev = lp->devices[id];
			#else
				frame_len = ((des0 >> 16) & 0x3fff)-6;
			#endif

				skb = lp->RX_RING[cur_rx_pos];
				skb->dev = dev;
				if( (((des0 >> 5)&0x1) == 1) && (((des0 >> 7)&0x1) ==0) && (((des0 )&0x1)== 0))
					skb->ip_summed = CHECKSUM_UNNECESSARY;
				else
					skb->ip_summed = CHECKSUM_NONE;
		#ifndef ROUTER_MODE
			#ifndef CONFIG_SC14452_ES2
				flush_dcache_range(lp->RX_DESCS[cur_rx_pos].DES2,lp->RX_DESCS[cur_rx_pos].DES2+frame_len + 6);
			#endif
		#endif
#ifdef CONFIG_SC14452_NETLOAD_PROTECTION

				if(skb->data[0] & 0x1)
					lp->tmp_bps ++;
				else
					lp->tmp_ups ++;
#endif
	#ifndef ROUTER_MODE
				skb_put(skb,frame_len);
	#else
				skb_put(skb, frame_len+4);	//TODO CHECK IF THIS IS NEEDED

				memcpy (tmpbuf, lp->RX_RING[cur_rx_pos]->data, 12);
				memcpy ((lp->RX_RING[cur_rx_pos]->data) + 4, tmpbuf, 12);

				skb_pull(skb, 4);

#if 0
				if  (id==1)
				{
					printk("%2.2x-",id);
					for (ik=0;ik<12;ik++)
					{
						printk("%2.2x ",lp->RX_RING[cur_rx_pos]->data[ik]);
					}
					printk ("\r\n");
				}
#endif

	#endif
				skb->protocol = eth_type_trans(skb,dev);

				if( netif_rx(skb)==NET_RX_DROP)
		#ifdef ROUTER_MODE
				{
					lp->stats[id].rx_dropped ++;
				}

				lp->stats[id].rx_packets++;
    			lp->stats[id].rx_bytes+= frame_len;
		#else
					lp->stats.rx_dropped ++;
				lp->stats.rx_packets++;
                lp->stats.rx_bytes+= frame_len;

         #endif
			}

			cur_rx_pos = (cur_rx_pos + 1)% MAX_RX_DESCS;
			des0 = lp->RX_DESCS[cur_rx_pos].DES0;
		}

		i = lp->CUR_RX_DESC;
		while(Packet_Count)
		{
			Packet_Count --;
			des0 = lp->RX_DESCS[i].DES0;
			if( des0 & 0x1<<15)
			{
			 	lp->RX_DESCS[i].DES0 = 0x80000000;
			}
			else
			{
				new_skb = dev_alloc_skb(MAX_PACKET_LEN);
				if( unlikely( (unsigned)new_skb->data >= 0x00FED000 ) )
					printk( "%s: skb=%p!!!\n", __FUNCTION__, new_skb ) ;
				if(new_skb == NULL)
					printk("No memory for new SKB!\n");
				#ifdef ROUTER_MODE
				skb_reserve(new_skb, 6);
				#else
				skb_reserve(new_skb,2);
				#endif
				lp->RX_RING[i] = new_skb;
				lp->RX_DESCS[i].DES2 = (volatile unsigned long int) new_skb->data;
                		lp->RX_DESCS[i].DES0 = 0x80000000; /* DMA OWNED... */

			}

			i = (i+1) % MAX_RX_DESCS;
		}

		lp->CUR_RX_DESC = cur_rx_pos;
}


#ifdef CONFIG_SC14452_ES2

static irqreturn_t rmii_interrupt(int irq, void *dev_id, struct pt_regs * regs)
{
	struct net_device *dev ;
	struct net_local *lp= dev->priv;

	unsigned temp;


	KEY_STATUS_REG = RMII_INT ;
	RESET_INT_PENDING_REG = 0x2;

	cache_disable();

	temp = GetDword (EMAC_DMAR6_OPERATION_MODE_REG);
	SetDword(EMAC_DMAR6_OPERATION_MODE_REG,temp | 2);

	SetDword(EMAC_DMAR2_RX_POLL_DEMAND_REG, 0);


	rec_enabled = 1;

	return IRQ_HANDLED;

}

void rmii_patch(void)
{

	unsigned temp;
	if (!rec_enabled)
	{

		if (P2_DATA_REG & 0x100)
		{

			KEY_STATUS_REG = RMII_INT ;
			RESET_INT_PENDING_REG = 0x2;

			cache_disable();

			temp = GetDword (EMAC_DMAR6_OPERATION_MODE_REG);
			SetDword(EMAC_DMAR6_OPERATION_MODE_REG,temp | 2);

			SetDword(EMAC_DMAR2_RX_POLL_DEMAND_REG, 0);
			rec_enabled = 1;
		}
	}
}
#endif



#ifndef CONFIG_SC14452_ES2

static irqreturn_t emac_interrupt(int irq, void *dev_id, struct pt_regs * regs)
{
	struct net_device *dev = NULL;
	struct net_local *lp= dev->priv;
	unsigned long status;
	#ifndef ROUTER_MODE
	int cur_rx_pos;
	unsigned long des0;
	unsigned short frame_len,Packet_Count,i,loopcount = 8;
	struct sk_buff *skb, *new_skb;

	unsigned temp;
	#endif
	volatile unsigned short rx_over = 0;

	dev = (struct net_device *)dev_id;
	lp = (struct net_local *) dev->priv;

	status = GetDword (EMAC_DMAR5_STATUS_REG);

	if (status & 0x40)
	{
		EMAC_DMAR5_STATUS_REG =0x40;

	}

	if (status & 0x10)
	{
		EMAC_DMAR5_STATUS_REG =0x10;

		rx_over = 1;
	}

	if (status & 0x5/*0x1*/)
	{
		EMAC_DMAR5_STATUS_REG =0x01;

	}

	EMAC_DMAR5_STATUS_REG = 0x1e7ae;

	if(status & GMI)
	{
		printk("EMAC counter interrupt\n");
	}



	if (status & (RU|RXINT|OVF))
	{
		tasklet_schedule(&lp->emac_rx_tasklet);
	}
	SetWord(RESET_INT_PENDING_REG,EMAC_INT_PEND);
	return IRQ_HANDLED;
}
#endif

#ifdef CONFIG_SC14452_ES2
static irqreturn_t emac_interrupt(int irq, void *dev_id, struct pt_regs * regs)
{
	struct net_device *dev ;
	struct net_local *lp= dev->priv;
	unsigned long status;
	int cur_rx_pos;
	unsigned long des0;
	unsigned short frame_len,Packet_Count,i,loopcount = 8;
	struct sk_buff *skb, *new_skb;

	unsigned temp;
	volatile unsigned short rx_over = 0;

	dev = (struct net_device *)dev_id;
	lp = (struct net_local *) dev->priv;

	status = GetDword (EMAC_DMAR5_STATUS_REG);

	if (status & 0x40)
	{
		EMAC_DMAR5_STATUS_REG =0x40;
		if  ((P2_DATA_REG & 0x100 ) == 0)
		{
				temp = GetDword (EMAC_DMAR6_OPERATION_MODE_REG);
				SetDword(EMAC_DMAR6_OPERATION_MODE_REG,temp & ~2);
				rec_enabled = 0;

		}
	}

	if (status & 0x10)
	{
		EMAC_DMAR5_STATUS_REG =0x10;

		temp = GetDword (EMAC_DMAR6_OPERATION_MODE_REG);
		SetDword(EMAC_DMAR6_OPERATION_MODE_REG,temp & ~2);
		rec_enabled = 0;
		rx_over = 1;
	}

	if (status & 0x5/*0x1*/)
	{
		unsigned short i;

		EMAC_DMAR5_STATUS_REG =0x01;

		unsigned short tx_in_progress = 0;

		for(i=0;i<MAX_TX_DESCS;i++)
			if(lp->TX_DESCS[i].DES0 & 0x80000000)
			{
				tx_in_progress = 1;
				break;
			}

		if (!tx_in_progress)
		{
			temp = GetDword (EMAC_DMAR6_OPERATION_MODE_REG);
			SetDword(EMAC_DMAR6_OPERATION_MODE_REG,temp & ~0x2000);
			xmit_enabled = 0;
		}
	}

	EMAC_DMAR5_STATUS_REG = 0x1e7ae;

	if(status & GMI)
	{
		printk("EMAC counter interrupt\n");
	}

	if (!rec_enabled && temp_rec_enabled)
	{
		temp = GetDword (EMAC_DMAR6_OPERATION_MODE_REG);
		SetDword(EMAC_DMAR6_OPERATION_MODE_REG,temp & ~2);

		temp_rec_enabled = 0;
	}

	if (!xmit_enabled)
	{


		if ((rx_over) || (!rec_enabled)&&((P2_DATA_REG & 0x100 ) == 0))
		{
			while ( ((EMAC_DMAR5_STATUS_REG & 0xe0000) == 0xe0000)|| ((EMAC_DMAR5_STATUS_REG & 0xe0000) == 0x20000)||((EMAC_DMAR5_STATUS_REG & 0xe0000) == 0xa0000)) ;

			cache_enable();
		}
	}
	if (status & (RU|RXINT|OVF))
	{
		tasklet_schedule(&lp->emac_rx_tasklet);
	}
//	INT3_PRIORITY_REG &= ~EMAC_INT_PRIO;
	SetWord(RESET_INT_PENDING_REG,EMAC_INT_PEND);
	return IRQ_HANDLED;
}
#endif


static int emac_xmit(struct sk_buff *skb, struct net_device *dev)
{
	struct net_local *lp = (struct net_local *) dev->priv;
	short length = 0;
	unsigned long status,des0;
	unsigned long flags;
	unsigned int i;
	#ifdef ROUTER_MODE
	unsigned short id;
	#else
	unsigned temp;
	#endif

        if(skb == NULL) {
                printk("xmit with empty skb\n");
                return 0;
        }
        if (netif_queue_stopped(dev)) {
                printk("xmit with stopped queue\n");
                return -EBUSY;
        }

	#ifdef ROUTER_MODE
	if((dev->name)[3] == '0')
		id = PORT_0;
	else
		id = PORT_1;
	#endif


        if(lp->tx_ring_size == MAX_TX_DESCS)
        {
                status = -EBUSY;
        }
	else  //proceed with transmission...
	{
	#ifdef ROUTER_MODE
	char tmpbuf[12];
	#endif
		if(lp->TX_RING[lp->TX_RING_HEAD] != NULL)
			printk("ERROR in TX: RING BUFFER HEAD NOT NULL\n");
	#ifdef ROUTER_MODE
			memcpy(tmpbuf, skb->data, 12);

		skb_push(skb, 4);
		memcpy(skb->data, tmpbuf, 12);

		if (lp->mode == MODE_SWITCH) {
			skb->data[12]= 0x88;
			skb->data[13]= 0x99;
			skb->data[14]= 0x93;
			skb->data[15]= 0xc3;
		}
		else {
			skb->data[12]= 0x88;
			skb->data[13]= 0x99;
			skb->data[14]= 0x93;
			skb->data[15]= 0xc0+(1<<id);
		}
	#endif
#if 0
		for (i=0;i<skb->len;i++)
			printk("%2.2x",skb->data[i]);

		printk("\r\n");
#endif

		length =  ETH_ZLEN < skb->len ? skb->len : ETH_ZLEN;

		lp->TX_RING[lp->TX_RING_HEAD] = skb;
		spin_lock_irqsave(&lp->lock,flags);
		lp->TX_DESCS[lp->TX_RING_HEAD].DES2 = (volatile unsigned long int) skb->data;
		lp->TX_DESCS[lp->TX_RING_HEAD].DES1 |= DWORD_SHIFT(0x1, 30) | DWORD_SHIFT(0x1, 29) | (length);
		lp->TX_DESCS[lp->TX_RING_HEAD].DES0 = 0x80000000;

#ifdef CONFIG_SC14452_ES2
		xmit_enabled = 1;
		cache_disable();
		temp = GetDword (EMAC_DMAR6_OPERATION_MODE_REG);
		SetDword(EMAC_DMAR6_OPERATION_MODE_REG,temp | 0x2002);
		SetDword(EMAC_DMAR2_RX_POLL_DEMAND_REG, 0);
#endif
		SetDword(EMAC_DMAR1_TX_POLL_DEMAND_REG, 0);

#ifdef CONFIG_SC14452_ES2
		temp_rec_enabled = 1;
#endif
	spin_unlock_irqrestore(&lp->lock,flags);
		//advance tx_head
		lp->TX_RING_HEAD = (lp->TX_RING_HEAD+1) % MAX_TX_DESCS;
		lp->tx_ring_size += 1;
		lp->max_tx_packets ++;
	}



	if((!(lp->max_tx_packets % 4)) || (lp->tx_ring_size == MAX_TX_DESCS))
	{
		while (lp->tx_ring_size &&  ((lp->TX_DESCS[lp->TX_RING_TAIL].DES0 & 0x80000000) == 0))
		{
		#ifdef ROUTER_MODE
			id = (((lp->TX_RING[lp->TX_RING_TAIL])->data[15]) & 0x3);
			if (id==2)
				id=1;
			else
				id=0;

 		 	if((lp->mode == MODE_SWITCH) || (id > 1))
				id = 0;

		#endif
			des0 = lp->TX_DESCS[lp->TX_RING_TAIL].DES0;
			if(des0 & 1 << 15){
				#ifdef ROUTER_MODE
				lp->stats[id].tx_errors ++;
				#else
				lp->stats.tx_errors ++;
				#endif
				if(des0 & (1<<10))
				#ifdef ROUTER_MODE
					lp->stats[id].tx_carrier_errors ++;
				#else
					lp->stats.tx_carrier_errors ++;
				#endif

			}
			#ifdef ROUTER_MODE
				lp->stats[id].tx_packets ++;
				lp->stats[id].tx_bytes += length;
			#else
				lp->stats.tx_packets ++;
				lp->stats.tx_bytes += length;
			#endif
			lp->TX_DESCS[lp->TX_RING_TAIL].DES2 = 0;
			lp->TX_DESCS[lp->TX_RING_TAIL].DES1 &=  DWORD_SHIFT(0x1,25) ;
			dev_kfree_skb(lp->TX_RING[lp->TX_RING_TAIL]);
			lp->TX_RING[lp->TX_RING_TAIL] = NULL;

			lp->tx_ring_size --;
			lp->TX_RING_TAIL = (lp->TX_RING_TAIL +1)% MAX_TX_DESCS;
		}
	}

	return 0;
}



static struct net_device_stats *emac_get_stats(struct net_device *dev)
{
	struct net_local *lp = (struct net_local *)dev->priv;
#ifdef ROUTER_MODE
	unsigned short id = 0;

	if((dev->name)[3] == '0')
		id = PORT_0;
	else
		id = PORT_1;

	lp->stats[id].multicast += EMAC_MMC_RXMULTICASTFRAMES_G_REG;
	lp->stats[id].collisions += EMAC_MMC_TXSINGLECOL_G_REG + EMAC_MMC_TXMULTICOL_G_REG;
	lp->stats[id].rx_length_errors += EMAC_MMC_RXUNDERSIZE_G_REG + EMAC_MMC_RXOVERSIZE_G_REG;
	lp->stats[id].rx_fifo_errors += EMAC_MMC_RXFIFOOVERFLOW_REG;
	lp->stats[id].tx_fifo_errors += EMAC_MMC_TXUNDERFLOWERROR_REG;
	return &(lp->stats[id]);

#else
	lp->stats.multicast += EMAC_MMC_RXMULTICASTFRAMES_G_REG;
	lp->stats.collisions += EMAC_MMC_TXSINGLECOL_G_REG + EMAC_MMC_TXMULTICOL_G_REG;
	lp->stats.rx_length_errors += EMAC_MMC_RXUNDERSIZE_G_REG + EMAC_MMC_RXOVERSIZE_G_REG;
	lp->stats.rx_fifo_errors += EMAC_MMC_RXFIFOOVERFLOW_REG;
	lp->stats.tx_fifo_errors += EMAC_MMC_TXUNDERFLOWERROR_REG;
	return &lp->stats;

#endif
}


static void emac_set_multicast_list(struct net_device *dev)
{


}
/*******************************************************************************************************************/
