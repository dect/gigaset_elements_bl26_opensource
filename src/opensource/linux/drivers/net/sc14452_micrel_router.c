// File created by Gigaset Elements GmbH
// All rights reserved.
/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 *
 * Project: VoIP
 * RHEA SOFTWARE PLATFORM
 * SC14452 EMAC LINUX DRIVER
 *
 * File sc14452_micrel_router.c
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ioport.h>
#include <linux/init.h>
#include <linux/dma-mapping.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <asm/io.h>
#include <asm/system.h>

#include "sc14452_micrel_router.h"
#include "asm.h"

static int emac_tx_timeout(struct net_device *dev);
struct net_device * __init sc14452_emac_probe(int unit);
static int __init emac_init(struct net_device *dev, int unit);
static int emac_tx_timeout(struct net_device *dev);
static int emac_open(struct net_device *dev);
static int emac_close(struct net_device *dev);
static void emac_timer_handler(unsigned long data);
static irqreturn_t emac_interrupt(int irq, void *dev_id, struct pt_regs * regs);
static int sc14452_ioctl(struct net_device *dev, struct ifreq *ifr, int cmd);
static int emac_xmit(struct sk_buff *skb, struct net_device *dev);
static struct net_device_stats *emac_get_stats(struct net_device *dev);
static void emac_set_multicast_list(struct net_device *dev);
static void emac_rx_tasklet_fn(unsigned long data);

#define PORT_1  1
#define PORT_0  0
//#define ASSIGN_ETH0_ON_PORT2 1

//#define SWITCH_TEST	1

#ifdef CONFIG_MICREL_ROUTER
extern void init_micrel_KSZ8873_Switch(void);
extern unsigned int I2C_8Bit_md_read (unsigned char reg_address);
extern void I2C_8Bit_md_write (unsigned char reg_address, unsigned int wdata);
extern unsigned short emac_link_detect(unsigned short);

typedef struct _TOS_entry {
	unsigned char TOS_reg;
	unsigned char TOS_DiffServ;
	unsigned char TOS_reg_val;
}TOS_entry;

#define MAX_TOS_CLASS_VALUES	64

TOS_entry TOS_table[MAX_TOS_CLASS_VALUES] =
{
	{0x60, 0x00, 0x03},
	{0x60, 0x04, 0x0C},
	{0x60, 0x08, 0x30},
	{0x60, 0x0C, 0xC0},
	{0x61, 0x10, 0x03},
	{0x61, 0x14, 0x0C},
	{0x61, 0x18, 0x30},
	{0x61, 0x1C, 0xC0},
	{0x62, 0x20, 0x03},
	{0x62, 0x24, 0x0C},
	{0x62, 0x28, 0x30},
	{0x62, 0x2C, 0xC0},
	{0x63, 0x30, 0x03},
	{0x63, 0x34, 0x0C},
	{0x63, 0x38, 0x30},
	{0x63, 0x3C, 0xC0},
	{0x64, 0x40, 0x03},
	{0x64, 0x44, 0x0C},
	{0x64, 0x48, 0x30},
	{0x64, 0x4C, 0xC0},
	{0x65, 0x50, 0x03},
	{0x65, 0x54, 0x0C},
	{0x65, 0x58, 0x30},
	{0x65, 0x5C, 0xC0},
	{0x66, 0x60, 0x03},
	{0x66, 0x64, 0x0C},
	{0x66, 0x68, 0x30},
	{0x66, 0x6C, 0xC0},
	{0x67, 0x70, 0x03},
	{0x67, 0x74, 0x0C},
	{0x67, 0x78, 0x30},
	{0x67, 0x7C, 0xC0},
	{0x68, 0x80, 0x03},
	{0x68, 0x84, 0x0C},
	{0x68, 0x88, 0x30},
	{0x68, 0x8C, 0xC0},
	{0x69, 0x90, 0x03},
	{0x69, 0x94, 0x0C},
	{0x69, 0x98, 0x30},
	{0x69, 0x9C, 0xC0},
	{0x6A, 0xA0, 0x03},
	{0x6A, 0xA4, 0x0C},
	{0x6A, 0xA8, 0x30},
	{0x6A, 0xAC, 0xC0},
	{0x6B, 0xB0, 0x03},
	{0x6B, 0xB4, 0x0C},
	{0x6B, 0xB8, 0x30},
	{0x6B, 0xBC, 0xC0},
	{0x6C, 0xC0, 0x03},
	{0x6C, 0xC4, 0x0C},
	{0x6C, 0xC8, 0x30},
	{0x6C, 0xCC, 0xC0},
	{0x6D, 0xD0, 0x03},
	{0x6D, 0xD4, 0x0C},
	{0x6D, 0xD8, 0x30},
	{0x6D, 0xDC, 0xC0},
	{0x6E, 0xE0, 0x03},
	{0x6E, 0xE4, 0x0C},
	{0x6E, 0xE8, 0x30},
	{0x6E, 0xEC, 0xC0},
	{0x6F, 0xF0, 0x03},
	{0x6F, 0xF4, 0x0C},
	{0x6F, 0xF8, 0x30},
	{0x6F, 0xFC, 0xC0}
};


struct SWITCH_REGS_CMD {
	unsigned char cmd;
	unsigned char reg;
	unsigned char data;
	unsigned char data1;
	unsigned char all_regs[200];
	unsigned char static_mac[16][8];
	unsigned char dynamic_mac[16][8];
};

struct audio_rtp_port {
	unsigned short local_rtp_port;
	unsigned char priotity_enable;
};

static char cnt_no_rtp_pkts = 0;

#define MAX_RTP_AUDIO_PORTS 2

struct audio_rtp_port ports_to_prioritize[MAX_RTP_AUDIO_PORTS];

#define SWITCH_CMD_READ									0
#define SWITCH_CMD_WRITE								1
#define SWITCH_CMD_ST_MAC_READ							2
#define SWITCH_CMD_ST_MAC_WRITE							3
#define SWITCH_CMD_ST_MAC_DEL 							4
#define SWITCH_CMD_VLAN_WRITE							5
#define SWITCH_CMD_VLAN_DEL								6
#define SWITCH_CMD_LINK_DETECT							7
#define SWITCH_CMD_OPERATION_SELECT						8
#define SWITCH_CMD_TOS_ADD								9
#define SWITCH_CMD_LIMIT_BW								10
#define SWITCH_CMD_PRIORITIZE_RTP_PORT					11
#define SWITCH_CMD_GET_PC_ETH_ADDR						12

/* 201009 Patch */
static unsigned char tx_underflow_flag = 0;
static unsigned int txcnt = 0;
#endif

#define TMOUT (HZ >> 1)   // Timeout is at 1/2 sec


struct net_device * __init sc14452_probe(int unit)
{
	struct net_device *dev;
	if(unit > 1)
		return NULL;	
	dev = alloc_netdev(sizeof(struct net_local), "eth%d", ether_setup);	
	register_netdev(dev);
	
	if (dev == NULL)
		return (struct net_device *)-ENOMEM;

	emac_init(dev,unit);

	printk("SC14452 EMAC  %s\n", dev->name);

	return dev;	
}


static int __init emac_init(struct net_device *dev,int unit)
{
	int i;
	struct net_local *lp;

	char * tmp;
	/* Initialize the device structure. */
	if(unit == 0) {
		dev->priv = (struct net_local*) kmalloc(sizeof(struct net_local), GFP_KERNEL);
		if (dev->priv == NULL)
			return -ENOMEM;
		memset(dev->priv, 0, sizeof(struct net_local));
		dev->base_addr = 0xFF2000;
		lp = (struct net_local *) dev->priv;

		lp->state = STATE_OFF;
		lp->mode = MODE_ROUTER;
//#ifdef ASSIGN_ETH0_ON_PORT2
//		lp->devices[PORT_1] = dev;
	//	printk("DEVICE 0: %x\n",lp->devices[PORT_1]);
//#else
		lp->devices[PORT_0] = dev;
//#endif
	}
	else {
		dev->priv = (dev_get_by_name("eth0"))->priv;
		lp = (struct net_local *) dev->priv;

//#ifdef ASSIGN_ETH0_ON_PORT2
	//	lp->devices[PORT_0] = dev;
//		printk("DEVICE 1: %x\n",lp->devices[PORT_0]);
//#else
		lp->devices[PORT_1] = dev;
//#endif
	}
	printk(KERN_INFO "%s : %s found at %lx, ", dev->name,cardname, dev->base_addr);
 	if (unit == 0) 
	{
		tmp = strstr(saved_command_line,"ethaddr=");
 
		if(tmp) {
			sscanf(tmp, "ethaddr=%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
			&(dev->dev_addr[0]),&(dev->dev_addr[1]),&(dev->dev_addr[2]),
			&(dev->dev_addr[3]),&(dev->dev_addr[4]),&(dev->dev_addr[5]));
		}
    else {
			/* Some error occured during system startup and the ethernet 
			address was not passed from bootloader. Assign a fixed predefined MAC address 
			to make the device accessible and debugable.
			*/
			//FATAL ERROR
 			dev->dev_addr[0] = 0x00; dev->dev_addr[1] = 0x88; dev->dev_addr[2] = 0x88;
				dev->dev_addr[3] = 0x77; dev->dev_addr[4] = 0x99; dev->dev_addr[5] = 0x66;
		}
	}
	else if(unit == 1) {

 		tmp = strstr(saved_command_line, "eth2addr=");
	 
    if(tmp) {
			sscanf(tmp,"eth2addr=%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
			&(dev->dev_addr[0]),&(dev->dev_addr[1]),&(dev->dev_addr[2]),
			&(dev->dev_addr[3]),&(dev->dev_addr[4]),&(dev->dev_addr[5]));
		}
    else {
			/* Some error occured during system startup and the ethernet 
			address was not passed from bootloader. Assign a fixed predefined MAC address 
			to make the device accessible and debugable.
			*/
			//FATAL ERROR
 			dev->dev_addr[0] = 0x00; dev->dev_addr[1] = 0x88; dev->dev_addr[2] = 0x88;
			dev->dev_addr[3] = 0x77; dev->dev_addr[4] = 0x99; dev->dev_addr[5] = 0x67;
		}
	}
 	/* Retrieve and print the ethernet address. */
	printk ("\n EMAC DEVICE ADDRESS (port:%d): ", unit);
	for (i = 0; i < 6; i++)
		printk(" %2.2x", dev->dev_addr[i]);
	printk("\n");

	for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) {
		ports_to_prioritize[i].local_rtp_port = 0;
		ports_to_prioritize[i].priotity_enable = 0;
	}

	/* assign handlers */
	dev->open	= emac_open;
	dev->stop	= emac_close;
	dev->hard_start_xmit = emac_xmit;
	dev->get_stats = emac_get_stats;
	dev->set_multicast_list = &emac_set_multicast_list;
	dev->tx_timeout = (void *)emac_tx_timeout;
	dev->watchdog_timeo	= 30*HZ;
	dev->tx_queue_len = 100;
	dev->do_ioctl = sc14452_ioctl;
 	dev->irq = EMAC_INT;
 	/* Fill in the fields of the device structure with ethernet values. */
	ether_setup(dev);
 	if(unit == 0)
		eth_init(dev);
 	SET_MODULE_OWNER(dev);
	return 0;
}

static int emac_tx_timeout(struct net_device *dev)
{
	return 0;
}

static int emac_open(struct net_device *dev)
{
  struct net_local *lp = (struct net_local *)dev->priv;
	unsigned short i;
	int result;
	struct sk_buff * skb;
 	unsigned short id;

	/* Prepare descriptors and skbs */
	/*Prepare tx descriptors and ring buffer */

	if(lp->mode == MODE_UNDEFINED)
		lp->mode = MODE_ROUTER;
		
	if(lp->mode == MODE_SWITCH)
		init_micrel_KSZ8873_Switch();

	spin_lock_init(&lp->lock);	

	if((dev->name)[3] == '0')
		id = PORT_0;
	else
		id = PORT_1;

	if (lp->mode == MODE_ROUTER)
	{
		#ifdef ASSIGN_ETH0_ON_PORT2
			if((dev->name)[3] == '0')
				id = PORT_1;
			else
				id = PORT_0;
		#endif
	}

	lp->port_state[id] = 1;
 
	//printk("STARTING device %d  lp state is %d\n",id, lp->state);
#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
	spin_lock_init(&lp->filter_lock);	
#endif
	if(lp->state == STATE_OFF) {
		lp->tx_ring_size = 0;
		lp->TX_RING_TAIL = 0;
		lp->TX_RING_HEAD = 0;

		for (i=0;i<MAX_TX_DESCS;i++)
		{
			lp->TX_RING[i] = NULL;
		}

		/*prepare rx descriptors and ring buffer*/
		for(i=0;i<MAX_RX_DESCS;i++)
		{
			skb = dev_alloc_skb(MAX_PACKET_LEN);
			if (!skb)
			{
				printk("Could not allocate skb at open. \n");
				goto out;
			}	
			skb_reserve(skb, 2);
			lp->RX_RING[i] = skb;
			lp->RX_DESCS[i].DES2 = (volatile unsigned long int) skb->data;
			lp->RX_DESCS[i].DES0 = 0x80000000; /* DMA OWNED... */
		}

		lp->CUR_RX_DESC = 0;
		lp->max_tx_packets = 0;

		tasklet_init(&lp->emac_rx_tasklet, emac_rx_tasklet_fn, (unsigned long)dev);

		emac_cfg(dev->dev_addr, lp);

		/* Acquire the IRQ before any is generated */
		result = request_irq(dev->irq, (void *)&emac_interrupt, SA_INTERRUPT, "emac_irq", lp->devices[0]);	
		if (result)
		{
			printk("Could not acquire EMAC IRQ %d \n",dev->irq);
			goto out;
		}

		init_timer(&lp->emac_timer);
		lp->emac_timer.expires = jiffies + (2* TMOUT);
		lp->emac_timer.data = (unsigned long)dev;
		lp->emac_timer.function = &emac_timer_handler;
		add_timer(&lp->emac_timer);

#if defined (CONFIG_SC1445x_POWER_DRIVER)
    {
      u16 creg, phy_addr, phy_reg;
      
      for ( phy_addr = 1; phy_addr < 5; phy_addr++ ) {
          phy_reg = 0; 
          creg = EMAC_MACR4_MII_ADDR_REG;
          while ((creg & 0x1) == 1) creg = EMAC_MACR4_MII_ADDR_REG;
          creg &= ~( 0x1f<<6 | 0x1f<<11);
          creg |=  (phy_addr << 11) | (phy_reg << 6) | 0x3;
          EMAC_MACR5_MII_DATA_REG = 0x800;
          EMAC_MACR4_MII_ADDR_REG = creg;
       }    
    }
#endif    
		lp->state = STATE_ON;
	} 

	if(lp->mode == MODE_ROUTER) {
		if (emac_link_detect(id)){
			printk("EMAC: eth%d Link detected\n",id);
			lp->Link[id] = 1;
			netif_carrier_on(dev);
			netif_start_queue(dev);
		}
		else{
			printk("EMAC: eth%d Link NOT detected\n",id);
			lp->Link[id] = 0;
			netif_carrier_off(dev);
		}
	}
	else //MODE SWITCH
	{		
		if (emac_link_detect(id))
		{
			printk("EMAC: Port%d Link detected\n",id);
			lp->Link[id] = 1;
		}
		else
		{
			printk("EMAC: Port%d Link NOT detected\n",id);
			lp->Link[id] = 0;
		}

		if (emac_link_detect( 1- id))
		{
			printk("EMAC: Port%d Link detected\n",1 - id);
			lp->Link[1 - id] = 1;
		}
		else
		{
			printk("EMAC: Port%d Link NOT detected\n",1 - id);
			lp->Link[id] = 0;
		}

		netif_carrier_on(dev);
		netif_start_queue(dev);
	}
	return 0;

out:
	/* Clean any gathered SKBs */
	for(i=0;i<MAX_RX_DESCS;i++)
	{
		if(lp->RX_RING[i])
			dev_kfree_skb(lp->RX_RING[i]);
	}	
	return -ENOMEM;
}

static int emac_close(struct net_device *dev)
{
  struct net_local *lp = (struct net_local *)dev->priv;
	unsigned short i, id;
	 
	if((dev->name)[3] == '0')
		id = PORT_0;
	else
		id = PORT_1;

	// Re-Initialize statistics
	memset(&lp->stats[id], 0, sizeof(struct net_device_stats));

	if (lp->mode == MODE_ROUTER)
	{
		#ifdef ASSIGN_ETH0_ON_PORT2
			if((dev->name)[3] == '0')
				id = PORT_1;
			else
				id = PORT_0;	
		#endif
	}
 
	lp->port_state[id] = 0;
	lp->Link[id] = 0; 
	 
	netif_stop_queue(dev);
	if(lp->port_state[1-id] == 0) { // Both interfaces down .... 

		lp->state= STATE_OFF;
 		del_timer(&lp->emac_timer);
 
		free_irq(dev->irq, lp->devices[0]);
		for(i=0;i<MAX_RX_DESCS;i++)
			if(lp->RX_RING[i])
				dev_kfree_skb(lp->RX_RING[i]);

		for(i=0;i<MAX_TX_DESCS;i++)
			if(lp->TX_RING[i])
				dev_kfree_skb(lp->TX_RING[i]);

		tasklet_kill(&lp->emac_rx_tasklet);

		EMAC_MACR0_CONFIG_REG &= ~0x3;

		emac_init_tx_desc(lp->TX_DESCS);
		emac_init_rx_desc(lp->RX_DESCS);

		emac_soft_rst();

	} // Both interfaces down...

	return 0;
}

/* ********************************************************************************************************
*  The ioctl function is used to pass address filtering information between the driver and the application.
*  The SC14452 can protect up to 4 Ethernet addresses. This mechanism is used to ensure phone quality in 
*  	malicious network load conditions.
*  The SIOCDEVPRIVATE slot is used for this purpose. 
*  The commands exchanged are: 
*
*	EMAC_LF_STATUS          0
*	EMAC_LF_ADD             1
*	EMAC_LF_REMOVE          2
*	EMAC_LF_REMOVE_ALL      3
*	EMAC_LF_START_ALL       4
*	EMAC_LF_START_BCAST     5
*	EMAC_LF_START_UCAST     6
*	EMAC_LF_STOP_ALL        7
*	EMAC_LF_STOP_BCAST      8
*	EMAC_LF_STOP_UCAST      9
*****************************************************************************************************/
static int sc14452_ioctl(struct net_device *dev, struct ifreq *ifr, int cmd)
{
  struct net_local *lp = (struct net_local *)dev->priv;
	struct SWITCH_REGS_CMD *switch_cmd_p;
	int i, j;
  	
  //if (!netif_running(dev))
		//return -ENODEV;

	switch (cmd) {
#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
    case SIOCDEVPRIVATE:
			spin_lock_irqsave(&lp->filter_lock,flags); /* protect access from other processes */
			command_struct_p = ( struct addr_filter_ioctl *) ifr->ifr_data;
			switch (command_struct_p->command) 
			{
				case EMAC_LF_STATUS:
					command_struct_p ->command = EMAC_LF_COMMAND_SUCCESS;
					break;
				case EMAC_LF_ADD:
					/* Broadcast addresses are not allowed ...*/
					if ( command_struct_p->status.address_filters[0].MAC[0] & 0x1){
						command_struct_p->command = EMAC_LF_COMMAND_FAIL_BCAST;
						break;
					}	
					for (i=0;i<4;i++)
					{	/* look for an empty slot */
						if(lp->filter_config.address_filters[i].enable){
							count++;
						}
						else
						{
							lp->filter_config.address_filters[i].enable = 1;
							lp->filter_config.address_filters[i].pid = command_struct_p->pid;
							memcpy(lp->filter_config.address_filters[i].MAC ,command_struct_p->status.address_filters[0].MAC , 6);
							/* It is important here that the second address register is writen last. Otherwise the HW will not
								 recognize the MAC address correctly */
							*(unsigned long *)(0xff2048 +(i*8))= (0xc000 <<16)| (lp->filter_config.address_filters[i].MAC[5] <<8 )| (lp->filter_config.address_filters[i].MAC[4] )	;
							*(unsigned long *)(0xff204c +(i*8))= (lp->filter_config.address_filters[i].MAC[3] <<24 )| (lp->filter_config.address_filters[i].MAC[2] << 16 ) | (lp->filter_config.address_filters[i].MAC[1] <<8 )| (lp->filter_config.address_filters[i].MAC[0]  );
							command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
							break;
						}
					}
					if(count == 4)
						command_struct_p->command = EMAC_LF_COMMAND_FAIL_FULL;
					break;
				case EMAC_LF_REMOVE:
					found = 0;
					for (i=0;i<4;i++)
					{
						if (lp->filter_config.address_filters[i].enable == 1)
						{
							if(!memcmp(lp->filter_config.address_filters[i].MAC , command_struct_p->status.address_filters[i].MAC,6))
							{
								found = 1;
								lp->filter_config.address_filters[i].enable = 0;
								*(unsigned long *)(0xff2048 +(i*8)) = 0x0;
								*(unsigned long *)(0xff204c +(i*8)) = 0x0;
								command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
								break;
							}
						}
					}
					if(!found)
					{
						command_struct_p->command = EMAC_LF_COMMAND_FAIL_NFOUND;
					}
					break;
				case EMAC_LF_REMOVE_ALL:
					break;
				case EMAC_LF_START_ALL:
					break;
				case EMAC_LF_START_BCAST:
					lp->filter_config.blp_enable = 1;
					lp->filter_config.blp_threshold = command_struct_p->status.blp_threshold;
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
				case EMAC_LF_START_UCAST:
					lp->filter_config.ulp_enable = 1;
					lp->filter_config.ulp_threshold = command_struct_p->status.ulp_threshold;
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
				case EMAC_LF_STOP_ALL:
					lp->filter_config.ulp_enable = 0;
					lp->filter_config.blp_enable = 0;
					lp->filter_config.blp_threshold = 0;
					lp->filter_config.ulp_threshold = 0;
					lp->filter_config.blp_protect = 0;
					lp->filter_config.ulp_protect = 0;
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 5);
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 9);
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
				case EMAC_LF_STOP_BCAST:
					lp->filter_config.blp_enable = 0;
					lp->filter_config.blp_threshold = 0;
					lp->filter_config.blp_protect = 0;
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 5);
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
				case EMAC_LF_STOP_UCAST:
					lp->filter_config.ulp_enable = 0;
					lp->filter_config.ulp_threshold = 0;
					lp->filter_config.ulp_protect = 0;
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 9);
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
				default: 
					return -EOPNOTSUPP;
			}
			spin_unlock_irqrestore(&lp->filter_lock, flags);
			/* Always copy the status structure to provide status info to the application */
			memcpy(&(command_struct_p->status),&(lp->filter_config),sizeof(struct addr_filter_status));
			return 0;
#endif

		case SIOCDEVPRIVATE:
			switch_cmd_p = ( struct SWITCH_REGS_CMD *) ifr->ifr_data;    	
			switch (switch_cmd_p->cmd) {
				case SWITCH_CMD_READ:
					switch_cmd_p->data = I2C_8Bit_md_read(switch_cmd_p->reg);
					break;				
				case SWITCH_CMD_WRITE:
					I2C_8Bit_md_write (switch_cmd_p->reg, switch_cmd_p->data);
					break;
				case SWITCH_CMD_ST_MAC_WRITE:
					I2C_8Bit_md_write (131, switch_cmd_p->static_mac[0][0]);	
					I2C_8Bit_md_write (130, switch_cmd_p->static_mac[0][1]);	
					I2C_8Bit_md_write (129, switch_cmd_p->static_mac[0][2]);	
					I2C_8Bit_md_write (128, switch_cmd_p->static_mac[0][3]);	
					I2C_8Bit_md_write (127, switch_cmd_p->static_mac[0][4]);	
					I2C_8Bit_md_write (126, switch_cmd_p->static_mac[0][5]);	
					I2C_8Bit_md_write (125, switch_cmd_p->static_mac[0][6]);	
					I2C_8Bit_md_write (124, switch_cmd_p->static_mac[0][7]);	

					for(i=0;i<8;i++)
						printk("%2x  %2x\n", I2C_8Bit_md_read(131-i), switch_cmd_p->static_mac[0][i]);
					printk("\n");
					I2C_8Bit_md_write(121, 0x00);
					I2C_8Bit_md_write(122, switch_cmd_p->reg);
					break;
				case SWITCH_CMD_ST_MAC_DEL:
					I2C_8Bit_md_write (131, 0);	
					I2C_8Bit_md_write (130, 0);	
					I2C_8Bit_md_write (129, 0);	
					I2C_8Bit_md_write (128, 0);	
					I2C_8Bit_md_write (127, 0);	
					I2C_8Bit_md_write (126, 0);	
					I2C_8Bit_md_write (125, 0);	
					I2C_8Bit_md_write (124, 0);	
					I2C_8Bit_md_write (121, 0x00);
					I2C_8Bit_md_write (122, switch_cmd_p->reg);
					break;
				 case SWITCH_CMD_VLAN_WRITE:
					I2C_8Bit_md_write (131, switch_cmd_p->static_mac[0][7]);	
					I2C_8Bit_md_write (130, switch_cmd_p->static_mac[0][6]);	
					I2C_8Bit_md_write (129, switch_cmd_p->static_mac[0][5]);	
					I2C_8Bit_md_write (121, 0x04);
					I2C_8Bit_md_write (122, switch_cmd_p->reg);
					break;
				case SWITCH_CMD_VLAN_DEL:
					I2C_8Bit_md_write (131, 0);	
					I2C_8Bit_md_write (130, 0);	
					I2C_8Bit_md_write (129, 0);	
					I2C_8Bit_md_write (121, 0x04);
					I2C_8Bit_md_write (122, switch_cmd_p->reg);
					break;
				case SWITCH_CMD_TOS_ADD:
					// Zero all TOS registers
					for (i=0; i<16; i++) {
						I2C_8Bit_md_write(0x60 + i, 0x00);	
					}

					// Write TOS Value to the specific micrel register
					for (i=0; i<MAX_TOS_CLASS_VALUES; i++) {
						if (switch_cmd_p->data == TOS_table[i].TOS_DiffServ) {
							I2C_8Bit_md_write(TOS_table[i].TOS_reg, TOS_table[i].TOS_reg_val);
							break;
						}
					}
					if (i == MAX_TOS_CLASS_VALUES)
						printk("ETH Error: Wrong TOS Class Value. Found %02x. Should be 0x00 to 0xFC at a multiple of 4 \n", switch_cmd_p->data);

					// TOS Priority enable at ports 1, 2, 3
					I2C_8Bit_md_write(0x10, 0xc1);
					I2C_8Bit_md_write(0x20, 0xc1);
					I2C_8Bit_md_write(0x30, 0xc1);
					break;
				case SWITCH_CMD_LIMIT_BW:
					if (switch_cmd_p->data == 0) {
						printk("BW limit OFF \n");
						// ingress data rate on port 2 set to 100MB or 10MB
						I2C_8Bit_md_write(0x26, 0x00);
						I2C_8Bit_md_write(0x27, 0x00);
						I2C_8Bit_md_write(0x28, 0x00);
						I2C_8Bit_md_write(0x29, 0x00);

						// egress data rate on port 2 set to 100MB or 10MB 
						I2C_8Bit_md_write(0x9E, 0x00);
						I2C_8Bit_md_write(0x9F, 0x00);
						I2C_8Bit_md_write(0xA0, 0x00);
						I2C_8Bit_md_write(0xA1, 0x00);

						// ingress data rate on port 1 set to 100MB or 10MB 
						I2C_8Bit_md_write(0x16, 0x00);
						I2C_8Bit_md_write(0x17, 0x00);
						I2C_8Bit_md_write(0x18, 0x00);
						I2C_8Bit_md_write(0x19, 0x00);

						// egress data rate on port 1 set to 100MB or 10MB
						I2C_8Bit_md_write(0x9A, 0x00);
						I2C_8Bit_md_write(0x9B, 0x00);
						I2C_8Bit_md_write(0x9C, 0x00);
						I2C_8Bit_md_write(0x9D, 0x00);

					}else {
						printk("BW limit ON \n");
						// ingress data rate on port 2 limit to 1MB 
						I2C_8Bit_md_write(0x26, 0x01);
						I2C_8Bit_md_write(0x27, 0x01);
						I2C_8Bit_md_write(0x28, 0x01);
						I2C_8Bit_md_write(0x29, 0x01);

						// egress data rate on port 2 limit to 1MB 
						I2C_8Bit_md_write(0x9E, 0x81);
						I2C_8Bit_md_write(0x9F, 0x01);
						I2C_8Bit_md_write(0xA0, 0x01);
						I2C_8Bit_md_write(0xA1, 0x01);
					
						// ingress data rate on port 1 limit to 1MB 
						I2C_8Bit_md_write(0x16, 0x01);
						I2C_8Bit_md_write(0x17, 0x01);
						I2C_8Bit_md_write(0x18, 0x01);
						I2C_8Bit_md_write(0x19, 0x01);

						// egress data rate on port 1 limit to 1MB
						I2C_8Bit_md_write(0x9A, 0x81);
						I2C_8Bit_md_write(0x9B, 0x01);
						I2C_8Bit_md_write(0x9C, 0x01);
						I2C_8Bit_md_write(0x9D, 0x01);
						/*I2C_8Bit_md_write(0x9A, 0x82);
						I2C_8Bit_md_write(0x9B, 0x02);
						I2C_8Bit_md_write(0x9C, 0x02);
						I2C_8Bit_md_write(0x9D, 0x02);*/
					}
					break;
				case SWITCH_CMD_PRIORITIZE_RTP_PORT:
					printk("ETH: SWITCH_CMD_PRIORITIZE_RTP_PORT port=[%d] enable=[%d] \n", ((switch_cmd_p->data1 << 8) | switch_cmd_p->data), switch_cmd_p->reg); 
					if (switch_cmd_p->reg == 0) {
           				// find if this port exists. If yes, disable it. 
						for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) {
					if (ports_to_prioritize[i].local_rtp_port == ((switch_cmd_p->data1 << 8) | switch_cmd_p->data)) { 
								printk("ETH: SWITCH_CMD_PRIORITIZE_RTP_PORT port=[%d] DISABLED\n", ports_to_prioritize[i].local_rtp_port); 
								ports_to_prioritize[i].local_rtp_port = 0;
								ports_to_prioritize[i].priotity_enable = 0;
                				break; 
							}
						}
						if (i == MAX_RTP_AUDIO_PORTS) 
							printk("ETH: Port=[%d] prioritization is not enabled. Will not disable it. \n", ((switch_cmd_p->data1 << 8) | switch_cmd_p->data)); 

						cnt_no_rtp_pkts = 0; 
					}

					else {
						for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) {
							// if port already exists and is already enabled do nothing 
							if ((ports_to_prioritize[i].local_rtp_port == ((switch_cmd_p->data1 << 8) | switch_cmd_p->data)) && 
								ports_to_prioritize[i].priotity_enable == 1) { 
								printk("ETH: SWITCH_CMD_PRIORITIZE_RTP_PORT port=[%d] ALREADY ENABLED\n", ports_to_prioritize[i].local_rtp_port); 
								return 0; 
							}
						}
						for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) {
							if (ports_to_prioritize[i].priotity_enable == 0) {
								ports_to_prioritize[i].local_rtp_port = ((switch_cmd_p->data1 << 8) | switch_cmd_p->data);
								ports_to_prioritize[i].priotity_enable = 1;
								break;
							}
						}
						if (i == MAX_RTP_AUDIO_PORTS)
							printk("ETH ERROR: No available place for audio RTP port prioritization \n");
					}
					break;
				case SWITCH_CMD_LINK_DETECT:
					if (lp->mode == MODE_SWITCH)
					{
						#ifdef ASSIGN_ETH0_ON_PORT2
							switch_cmd_p->data = lp->Link[0];
							switch_cmd_p->data1 = lp->Link[1];
						#else
							switch_cmd_p->data = lp->Link[1];
							switch_cmd_p->data1 = lp->Link[0];
						#endif

					}else {
						switch_cmd_p->data = lp->Link[1];
						switch_cmd_p->data1 = lp->Link[0];
					}
					return 0;
				case SWITCH_CMD_OPERATION_SELECT:
					if (switch_cmd_p->reg == MODE_ROUTER) {
						printk("MODE_ROUTER \n");
						lp->mode = MODE_ROUTER;
					}
					else if (switch_cmd_p->reg == MODE_SWITCH) {
						printk("MODE_SWITCH \n");
						lp->mode = MODE_SWITCH;
					}
					return 0;
				case SWITCH_CMD_GET_PC_ETH_ADDR:
					micrel_KSZ8873_get_first_mac_PCport(switch_cmd_p->all_regs);
					printk("\n\n>>>>>>--ETH: SWITCH_CMD_GET_PC_ETH_ADDR \n MAC= %2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x\n", 
						switch_cmd_p->all_regs[5], switch_cmd_p->all_regs[4], switch_cmd_p->all_regs[3],
						switch_cmd_p->all_regs[2], switch_cmd_p->all_regs[1], switch_cmd_p->all_regs[0]);
					return 0;
				default: break;
			}

#ifdef SWITCH_TEST

		for(i=0; i<199; i++) {
			switch_cmd_p->all_regs[i] = I2C_8Bit_md_read(i);
		}	

		for(i=0;i<16;i++) {
				I2C_8Bit_md_write(121, 0x14);
				I2C_8Bit_md_write(122, i);
				for(j=1000;j!=0;j--);
				switch_cmd_p->dynamic_mac[i][0] = I2C_8Bit_md_read(131);
				switch_cmd_p->dynamic_mac[i][1] = I2C_8Bit_md_read(130);
				switch_cmd_p->dynamic_mac[i][2] = I2C_8Bit_md_read(129);
				switch_cmd_p->dynamic_mac[i][3] = 0;
				switch_cmd_p->dynamic_mac[i][4] = 0;
				switch_cmd_p->dynamic_mac[i][5] = 0;
				switch_cmd_p->dynamic_mac[i][6] = 0;
				switch_cmd_p->dynamic_mac[i][7] = 0;
			}
#endif 

			return 0;
		default: break;
	}
	return -EOPNOTSUPP;
}

/*********************************************************************************************
*  The timer handler is used to run the Ethernet address filtering mechanism. The timer ticks 
*  every 1/2 sec. If the address filters are enabled, it checks whether the received unicast 
*  or broadcast packets exceed the user defined threshold. In this case, the filtering is enabled. 
*  For unicast filtering, the MMC counters are then checked at every tick and the number
*  of filtered packets are monitored against the threshold. If this nomber drops below the 
*  threshold, the filtering is disabled. For broadcast trafic, after disabling the reception
*  the system enables again in 4 seconds to check whether the broadcast storm is over. 
*********************************************************************************************/

static void emac_timer_handler(unsigned long data)
{
	struct net_device *dev = (struct net_device *)data;
	struct net_local *lp = (struct net_local *)dev->priv;
	int i, k;

	for(k=0;k<2;k++)
	{
		i=k;
		if (lp->mode == MODE_ROUTER)
		{
			#ifdef ASSIGN_ETH0_ON_PORT2
				i=!k;
			#endif
		}
		if(lp->mode == MODE_ROUTER) {
  			if(lp->port_state[i] == 1)
			{
				if(lp->Link[i] != emac_link_detect(i)) // status changed....
				{
					if(lp->Link[i] == 0) // Link detected.... 
					{
						printk(" EMAC: eth%d Link Up !\n", k);
						lp->Link[i] = 1;
						netif_carrier_on(lp->devices[i]);
						//netif_start_queue(lp->devices[i]);		
					}
					else
					{
						printk(" EMAC: eth%d Link Down !\n", k);
						lp->Link[i] = 0;
						netif_carrier_off(lp->devices[i]);
						// Added for PC MAC quick update
						// Disable Learning in PC Port1
						I2C_8Bit_md_write(0x12, 0x7);
						// Disable Learning in LAN Port2
						I2C_8Bit_md_write(0x22, 0x7);
						// Flash dynamic table
						I2C_8Bit_md_write(0x2, 0x20);
						// Enable Learning in PC Port1
						I2C_8Bit_md_write(0x12, 0x6);
						// Enable Learning in LAN Port2
						I2C_8Bit_md_write(0x22, 0x6);

						//netif_stop_queue(lp->devices[i]);
					}
				}
			} 
		}

		else // MODE_SWITCH
		{
			if(emac_link_detect(i)) //carrier found
			{
				if(lp->Link[i] == 0)
				{
					printk(" EMAC: Port%d Link Up !\n", k);
					lp->Link[i] = 1;
				}
			}
			else // NO CARRIER FOUND
			{
				if(lp->Link[i] == 1)
				{
					printk(" EMAC: Port%d Link Down !\n", k);
					lp->Link[i] = 0;
					// Added for PC MAC quick update
					// Disable Learning in PC Port1
					I2C_8Bit_md_write(0x12, 0x7);
					// Disable Learning in LAN Port2
					I2C_8Bit_md_write(0x22, 0x7);
					// Flash dynamic table
					I2C_8Bit_md_write(0x2, 0x20);
					// Enable Learning in PC Port1
					I2C_8Bit_md_write(0x12, 0x6);
					// Enable Learning in LAN Port2
					I2C_8Bit_md_write(0x22, 0x6);
				}

			}
		}
	}

#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
	unsigned long btotal,utotal;
	static int protect_slots = 0; 
	unsigned long flags;
		
	spin_lock_irqsave(&lp->filter_lock,flags);
	btotal = EMAC_MMC_RXBROADCASTFRAMES_G_REG;
	utotal = EMAC_MMC_RXUNICASTFRAMES_G_REG;
	lp->filter_config.ups = lp->tmp_ups * 2;
	lp->filter_config.bps = lp->tmp_bps * 2;
	if(lp->filter_config.ulp_enable) {
		if(utotal > lp->filter_config.ulp_threshold){
			EMAC_MACR1_FRAME_FILTER_REG |= (0x1 << 9);
			lp->filter_config.ulp_protect = 1;	
		}
		else {
			EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 9);
			lp->filter_config.ulp_protect = 0;	
		}
		lp->filter_config.ulp_filtered = utotal - lp->filter_config.ups;
	}
	if(lp->filter_config.blp_enable) {
		if(lp->filter_config.blp_protect == 0){
			if(btotal > lp->filter_config.blp_threshold){
				EMAC_MACR1_FRAME_FILTER_REG |= (0x1 << 5);
				lp->filter_config.blp_protect = 1;	
				protect_slots = 8;
			}
		}
		else {
			protect_slots --;
			if(protect_slots == 0) {
				EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 5);
				lp->filter_config.blp_protect = 0;	
			}
		}
		lp->filter_config.blp_filtered = btotal - lp->filter_config.bps;
	}	
	lp->tmp_ups = 0;
	lp->tmp_bps = 0;

	spin_unlock_irqrestore(&lp->filter_lock,flags);
#endif

	lp->emac_timer.expires = jiffies + TMOUT;
	add_timer(&lp->emac_timer);	
}

static void emac_rx_tasklet_fn(unsigned long data)
{
  struct net_device *dev = (struct net_device *)data;
  struct net_local *lp = (struct net_local *) dev->priv;
	unsigned long status;
	int cur_rx_pos;
	unsigned long des0;
	unsigned short frame_len,Packet_Count,i;
	struct sk_buff *skb, *new_skb;
	unsigned short id = 0;

	status = GetDword (EMAC_DMAR5_STATUS_REG);

		Packet_Count = 0;
		cur_rx_pos = lp->CUR_RX_DESC;
		des0 = lp->RX_DESCS[cur_rx_pos].DES0;
		while(((des0 & 0x80000000) == 0) && (Packet_Count < 4))
		{
			Packet_Count ++;
			if(des0 & (0x1 << 15) ){
				lp->stats[0].rx_errors++;
				if(des0 & (0x1 << 1))
					lp->stats[0].rx_crc_errors++;
			}	
			else
			{ /* GET THE PACKET AND forward it to the system */
#if 0
				char dont_sent = 0;
				unsigned short dest_port = 0;
	
				flush_dcache_range(lp->RX_RING[cur_rx_pos]->data[20], lp->RX_RING[cur_rx_pos]->data[42]);

				/* Check packet for VLAN ID. Then find destination port */
				if (((lp->RX_RING[cur_rx_pos]->data[20] & 0xFF) == 0x81) && ((lp->RX_RING[cur_rx_pos]->data[21] & 0xFF) == 0x00))
					dest_port = ((lp->RX_RING[cur_rx_pos]->data[40] & 0xFF) << 8) | (lp->RX_RING[cur_rx_pos]->data[41] & 0xFF); 	
				else
					dest_port = ((lp->RX_RING[cur_rx_pos]->data[36] & 0xFF) << 8) | (lp->RX_RING[cur_rx_pos]->data[37] & 0xFF); 

				for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) {
					if (ports_to_prioritize[i].priotity_enable == 1) { 
						/* packet does not match an rtp audio */
						if (!(dest_port == ports_to_prioritize[i].local_rtp_port)) {
							if (cnt_no_rtp_pkts > 40) { 
								dont_sent = 1;
								cnt_no_rtp_pkts = 0;
								lp->stats[id].rx_dropped ++;
								lp->RX_DESCS[cur_rx_pos].DES0 |= (0x1<<15);
							}else {
								dont_sent = 0;		
								cnt_no_rtp_pkts++;
							}
							break;
						}
					}
				}
				if (dont_sent == 1) 
				{
					cur_rx_pos = (cur_rx_pos + 1) % MAX_RX_DESCS;
					des0 = lp->RX_DESCS[cur_rx_pos].DES0;	
					continue;
				}
#endif
				frame_len = ((des0 >> 16) & 0x3fff)-7 ;
				
				flush_dcache_range(lp->RX_DESCS[cur_rx_pos].DES2, lp->RX_DESCS[cur_rx_pos].DES2+frame_len + 7);

				id = (lp->RX_RING[cur_rx_pos]->data[frame_len] & 0x3);

				#ifdef ASSIGN_ETH0_ON_PORT2
					if (lp->mode == MODE_ROUTER)
					{
						if (id) id=0;
						if (!id) id=1;
					}
				#endif 
			 
			 	if((lp->mode == MODE_SWITCH)||(id > 1))
			 		id = 0;
				dev = lp->devices[id];

				skb = lp->RX_RING[cur_rx_pos];
				skb->dev = dev;
				if( (((des0 >> 5) & 0x1) == 1) && (((des0 >> 7) & 0x1) == 0) && (((des0) & 0x1) == 0))
					skb->ip_summed = CHECKSUM_UNNECESSARY;
				else
					skb->ip_summed = CHECKSUM_NONE;
#ifdef CONFIG_SC14452_NETLOAD_PROTECTION

				if(skb->data[0] & 0x1)
					lp->tmp_bps ++;
				else
					lp->tmp_ups ++;
#endif
				skb_put(skb, frame_len);
				skb->protocol = eth_type_trans(skb,dev);
				if(netif_rx(skb)==NET_RX_DROP) 
					lp->stats[id].rx_dropped ++;
				lp->stats[id].rx_packets++;
        lp->stats[id].rx_bytes+= frame_len;
			}
			cur_rx_pos = (cur_rx_pos + 1) % MAX_RX_DESCS;
			des0 = lp->RX_DESCS[cur_rx_pos].DES0;	
		}
		
		i = lp->CUR_RX_DESC;
		while(Packet_Count)
		{
			Packet_Count --;
			des0 = lp->RX_DESCS[i].DES0;
			if(des0 & 0x1<<15)
			{
			 	lp->RX_DESCS[i].DES0 = 0x80000000;	
			}
			else
			{
				new_skb = dev_alloc_skb(MAX_PACKET_LEN);
				if(new_skb == NULL)
					printk("No memory for new SKB!\n");	
				skb_reserve(new_skb, 2);
				lp->RX_RING[i] = new_skb;
				lp->RX_DESCS[i].DES2 = (volatile unsigned long int) new_skb->data;
        lp->RX_DESCS[i].DES0 = 0x80000000; /* DMA OWNED... */
			}
			i = (i+1) % MAX_RX_DESCS;
		}
	lp->CUR_RX_DESC = cur_rx_pos;
}


static irqreturn_t emac_interrupt(int irq, void *dev_id, struct pt_regs * regs)
{
	struct net_device *dev = NULL;
	struct net_local *lp= dev->priv;
	unsigned long status ,i, erron, status2;
	volatile unsigned int j,k;
	volatile unsigned short rx_over = 0;

	dev = (struct net_device *)dev_id;
	lp = (struct net_local *)dev->priv;

	status = GetDword(EMAC_DMAR5_STATUS_REG);

	if (status & 0x40)
	{
		EMAC_DMAR5_STATUS_REG = 0x40;
	}

	if (status & 0x10)
	{
		EMAC_DMAR5_STATUS_REG = 0x10;

		rx_over = 1;
	}

	if (status & 0x5/*0x1*/)
	{
		EMAC_DMAR5_STATUS_REG = 0x01;
	}


	if(status & GMI)
	{
		printk("EMAC counter interrupt\n");
	}

//-- TX UNDERFLOW HANDLING
#if 1
		if(status & UNF)
			{

					struct s_descriptor* ERR_TX_DESC = GetDword (EMAC_DMAR18_CUR_HOST_TX_DESCRIPTOR_REG);
					printk(KERN_ERR"EMAC TX UNDERFLOW interrupt %lx %lx %lx \r\n",ERR_TX_DESC->DES0,GetDword (EMAC_DMAR18_CUR_HOST_TX_DESCRIPTOR_REG),status);

					SetDword(EMAC_DMAR5_STATUS_REG,(UNF|AIS));

					tx_underflow_flag=1;

					//SetDword(EMAC_DMAR5_STATUS_REG,(UNF|AIS));
			}
#endif

	EMAC_DMAR5_STATUS_REG = (0x1e7ae);

	if (status & (RU|RXINT|OVF))
	{
		tasklet_schedule(&lp->emac_rx_tasklet);
	}

	SetWord(RESET_INT_PENDING_REG,EMAC_INT_PEND);

	return IRQ_HANDLED;
}


static int emac_xmit(struct sk_buff *skb, struct net_device *dev)
{
	struct net_local *lp = (struct net_local *) dev->priv;
	short length = 0;
	unsigned long status, des0, status2;
	unsigned long flags;
	unsigned short id;

	unsigned int i;
	struct sk_buff * myskb;
	//volatile unsigned int port1, port0;

/* 201009 LG Twin Patch */
	if (tx_underflow_flag)
	{
		//printk ("Resetting the interface \r\n");

		spin_lock_irqsave(&lp->lock,flags);
		emac_soft_rst();

		for(i=0;i<MAX_RX_DESCS;i++)
			if(lp->RX_RING[i])
				dev_kfree_skb(lp->RX_RING[i]);

		for(i=0;i<MAX_TX_DESCS;i++)
			if(lp->TX_RING[i])
				dev_kfree_skb(lp->TX_RING[i]);

		EMAC_MACR0_CONFIG_REG &= ~0x3;

		emac_init_tx_desc(lp->TX_DESCS);
		emac_init_rx_desc(lp->RX_DESCS);

		emac_soft_rst();

		lp->tx_ring_size = 0;
		lp->TX_RING_TAIL = 0;
		lp->TX_RING_HEAD = 0;

		for (i=0;i<MAX_TX_DESCS;i++)
		{
			lp->TX_RING[i] = NULL;
		}

			/*prepare rx descriptors and ring buffer*/
		for(i=0;i<MAX_RX_DESCS;i++)
		{
			myskb = dev_alloc_skb(MAX_PACKET_LEN);
			skb_reserve(myskb, 2);
			lp->RX_RING[i] = myskb;
			lp->RX_DESCS[i].DES2 = myskb->data;
			lp->RX_DESCS[i].DES0 = 0x80000000; /* DMA OWNED... */
		}

			lp->CUR_RX_DESC = 0;
			lp->max_tx_packets = 0;

			emac_cfg(dev->dev_addr, lp);

			spin_unlock_irqrestore(&lp->lock, flags);

		tx_underflow_flag=0;
	}


	//printk("xmit dev: %x, len: %d  ",dev , skb->len);
  if(skb == NULL) {
		printk("xmit with empty skb\n");
		return 0;
  }
  if(netif_queue_stopped(dev)) {
		printk("xmit with stopped queue\n");
		return -EBUSY;
  }

	if((dev->name)[3] == '0')
		id = PORT_0;
	else
		id = PORT_1;

	if(lp->mode == MODE_ROUTER)
	{
		#ifdef ASSIGN_ETH0_ON_PORT2
			if((dev->name)[3] == '0')
				id = PORT_1;
			else
				id = PORT_0;
		#endif
	}

  if(lp->tx_ring_size == MAX_TX_DESCS)
  {
          status = -EBUSY;
  }
	else  //proceed with transmission...
	{
		if(lp->TX_RING[lp->TX_RING_HEAD] != NULL)
			printk("ERROR in TX: RING BUFFER HEAD NOT NULL\n");

		length =  ETH_ZLEN < skb->len ? skb->len : ETH_ZLEN;

		if (lp->mode == MODE_SWITCH)
			skb->data[length] = 0;
		else skb->data[length]= id + 1;

		lp->TX_RING[lp->TX_RING_HEAD] = skb;
		spin_lock_irqsave(&lp->lock,flags);
		lp->TX_DESCS[lp->TX_RING_HEAD].DES2 = (volatile unsigned long int) skb->data;
		lp->TX_DESCS[lp->TX_RING_HEAD].DES1 |= 	DWORD_SHIFT(0x1, 30) | DWORD_SHIFT(0x1, 29) | (length+1);
		lp->TX_DESCS[lp->TX_RING_HEAD].DES0 = 0x80000000;

		status2 = GetDword(EMAC_DMAR5_STATUS_REG);

		if (status2 & UNF)
		{
			printk ("Clearing TX_Underflow within xmit \r\n");

			SetDword(EMAC_DMAR5_STATUS_REG,(UNF|AIS));
			tx_underflow_flag=1;
		}

		SetDword(EMAC_DMAR1_TX_POLL_DEMAND_REG, 0);

		spin_unlock_irqrestore(&lp->lock, flags);
		//advance tx_head
		lp->TX_RING_HEAD = (lp->TX_RING_HEAD + 1) % MAX_TX_DESCS;
		lp->tx_ring_size += 1;
		lp->max_tx_packets ++;
	}

	if((!(lp->max_tx_packets % 4)) || (lp->tx_ring_size == MAX_TX_DESCS))
	{
		while (lp->tx_ring_size &&  ((lp->TX_DESCS[lp->TX_RING_TAIL].DES0 & 0x80000000) == 0))
		{
      id = (((lp->TX_RING[lp->TX_RING_TAIL])->data[(lp->TX_RING[lp->TX_RING_TAIL])->len]) & 0x3) -1; 
 			if( id >1)
				id = 0; 

			des0 = lp->TX_DESCS[lp->TX_RING_TAIL].DES0;
			if(des0 & 1 << 15) {
				lp->stats[id].tx_errors ++;
				if(des0 & (1<<10))
					lp->stats[id].tx_carrier_errors ++;
			}
			lp->stats[id].tx_packets ++;
			lp->stats[id].tx_bytes += length;
			lp->TX_DESCS[lp->TX_RING_TAIL].DES2 = 0;
			lp->TX_DESCS[lp->TX_RING_TAIL].DES1 &= DWORD_SHIFT(0x1, 25);
			dev_kfree_skb(lp->TX_RING[lp->TX_RING_TAIL]);
			lp->TX_RING[lp->TX_RING_TAIL] = NULL;

			lp->tx_ring_size --;
			lp->TX_RING_TAIL = (lp->TX_RING_TAIL +1)% MAX_TX_DESCS;
		}  
	}	

	return 0; 
}


static struct net_device_stats *emac_get_stats(struct net_device *dev)
{
	struct net_local *lp = (struct net_local *)dev->priv;

	unsigned short id = 0; 

	if((dev->name)[3] == '0')
		id = PORT_0;
	else
		id = PORT_1;

	if (lp->mode == MODE_ROUTER)
	{
		#ifdef ASSIGN_ETH0_ON_PORT2
			if((dev->name)[3] == '0')
				id = PORT_1;
			else
				id = PORT_0;
		#endif
	}

	lp->stats[id].multicast += EMAC_MMC_RXMULTICASTFRAMES_G_REG;
	lp->stats[id].collisions += EMAC_MMC_TXSINGLECOL_G_REG + EMAC_MMC_TXMULTICOL_G_REG;
	lp->stats[id].rx_length_errors += EMAC_MMC_RXUNDERSIZE_G_REG + EMAC_MMC_RXOVERSIZE_G_REG;
	lp->stats[id].rx_fifo_errors += EMAC_MMC_RXFIFOOVERFLOW_REG;
	lp->stats[id].tx_fifo_errors += EMAC_MMC_TXUNDERFLOWERROR_REG;
	return &(lp->stats[id]);
}


static void emac_set_multicast_list(struct net_device *dev)
{

}
/*************************************************************************************************/



