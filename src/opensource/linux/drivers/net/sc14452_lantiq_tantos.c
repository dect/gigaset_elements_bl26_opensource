// File created by Gigaset Elements GmbH
// All rights reserved.
/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 *
 * Project: VoIP
 * RHEA SOFTWARE PLATFORM
 * SC14452 EMAC LINUX DRIVER
 *
 * File sc14452_lantiq_tantos.c
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ioport.h>
#include <linux/init.h>
#include <linux/dma-mapping.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <asm/io.h>
#include <asm/system.h>

#include "sc14452_lantiq_tantos.h"
#include "asm.h"

static int emac_tx_timeout(struct net_device *dev);
struct net_device * __init sc14452_emac_probe(int unit);
static int __init emac_init(struct net_device *dev/*, unsigned long ioaddr*/);
static int emac_tx_timeout(struct net_device *dev);
static int emac_open(struct net_device *dev);
static int emac_close(struct net_device *dev);
static void emac_timer_handler(unsigned long data);
static irqreturn_t emac_interrupt(int irq, void *dev_id, struct pt_regs * regs);
static irqreturn_t rmii_interrupt(int irq, void *dev_id, struct pt_regs * regs);

static int sc14452_ioctl(struct net_device *dev, struct ifreq *ifr, int cmd);
static int emac_xmit(struct sk_buff *skb, struct net_device *dev);
static struct net_device_stats *emac_get_stats(struct net_device *dev);
static void emac_set_multicast_list(struct net_device *dev);
static void emac_rx_tasklet_fn(unsigned long data);


#define TMOUT 	(HZ >> 1)   // Timeout is at 1/2 sec


#define EMAC_SPEED_100 SIOCDEVPRIVATE+1
#define EMAC_SPEED_10  SIOCDEVPRIVATE+2
#define EMAC_OPEN_SW   SIOCDEVPRIVATE+3
#define EMAC_CLOSE_SW  SIOCDEVPRIVATE+4


#ifdef CONFIG_SC14452_ES2
#define RMII_INT 2 
extern void rmii_patch(void);
volatile unsigned short cache_disabled = 0;
volatile unsigned short xmit_enabled = 0;
volatile unsigned short rec_enabled = 0;
volatile unsigned short temp_rec_enabled = 0;

// The ES2 requires special cache handling. 
void cache_enable(void)
{
	unsigned short temp;

	CACHE_CTRL_REG = 0x16;	
	_spr_("cfg",temp);
	temp |= 0x14;	 //set bit 4
	_lpr_("cfg",temp);

	cache_disabled = 0;
}

void cache_disable(void)
{
	unsigned short temp;

	_spr_("cfg",temp);
	temp &= ~0x14;	 //reset bit 4
	_lpr_("cfg",temp);

	CACHE_CTRL_REG = 0;

	// use DMA to speed up the init part
	dma_meminit(0x9c00, 256, 2, 2, 0, 0);
	dma_wait(0);	
	cache_disabled = 1;

}


void data_cache_inv(unsigned base, unsigned len)
{
	unsigned short temp;

	unsigned addr1,addr2,len1,len2 = 0;

	unsigned offset = (((base >> 4) & 0x1fe) *2);
	unsigned tags = (len>>5) +2;

	addr1 = 0x9c00 + offset;

	if ((offset + tags *4) > 1024)
	{
		len1 =  (1024 - offset) >> 2;
		addr2 = 0x9c00;
		len2 =  tags -len1;
	}
	else
	{
		len1 = tags;
	}

	_spr_("cfg",temp);
	temp &= ~0x14;	 //reset bit 4
	_lpr_("cfg",temp);
	CACHE_CTRL_REG = 0;
	DMA0_B_STARTH_REG = 0;
	DMA0_B_STARTL_REG = 0x9c00;
	DMA0_LEN_REG = 0x100;
	DMA0_CTRL_REG = 0x1825;
	while (DMA0_CTRL_REG & DMA_ON);

	cache_disabled = 1;
}
#endif


struct net_device * __init sc14452_probe(int unit)
{
	struct net_device *dev;

	if(unit !=0)
		return NULL;	
	dev = alloc_netdev(sizeof(struct net_local), "eth%d", ether_setup);	
	register_netdev(dev);
	
	if (dev == NULL)
		return (struct net_device *)-ENOMEM;

	emac_init(dev);

	printk("SC14452 EMAC dev_irq: %d\n", dev->irq);

	return dev;	
	
}


static int __init emac_init(struct net_device *dev/*, unsigned long ioaddr*/)
{
	static unsigned version_printed = 0;
	int i;
	struct net_local *lp;	
	
	char * tmp;

	/* Initialize the device structure. */
	dev->priv = (struct net_local*)  kmalloc(sizeof(struct net_local), GFP_KERNEL);
	if (dev->priv == NULL)
		return -ENOMEM;

	memset(dev->priv, 0, sizeof(struct net_local));

	dev->base_addr = 0xFF2000;

	lp = (struct net_local *) dev->priv;

	lp->state= STATE_OFF;
	printk(KERN_INFO "%s: %s found at %lx, ", dev->name, cardname, dev->base_addr);

	tmp = strstr(saved_command_line,"ethaddr=");
        if(tmp)
        {
                sscanf(tmp,"ethaddr=%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
                                &(dev->dev_addr[0]),&(dev->dev_addr[1]),&(dev->dev_addr[2]),
                                &(dev->dev_addr[3]),&(dev->dev_addr[4]),&(dev->dev_addr[5]));
        }
        else
       {
	/* 	Some error occured during system startup and the ethernet 
		address was not passed from bootloader. Assign a fixed predefined MAC address 
		to make the device accessible and debugable.*/
                dev->dev_addr[0] = 0x00;
                dev->dev_addr[1] = 0x88;
                dev->dev_addr[2] = 0x88;
                dev->dev_addr[3] = 0x77;
                dev->dev_addr[4] = 0x99;
                dev->dev_addr[5] = 0x66;
        }

	/* Retrieve and print the ethernet address. */
	printk ("\n EMAC DEVICE ADDRESS: ");
	for (i = 0; i < 6; i++)
		printk(" %2.2x", dev->dev_addr[i]);
	printk("\n");

	/* assign handlers */
	dev->open		= emac_open;
	dev->stop		= emac_close;
	dev->hard_start_xmit 	= emac_xmit;
	dev->get_stats		= emac_get_stats;
	dev->set_multicast_list = &emac_set_multicast_list;
	dev->tx_timeout		= emac_tx_timeout;
	dev->watchdog_timeo	= 30*HZ;
	dev->tx_queue_len       = 100;
	dev->do_ioctl = sc14452_ioctl;

	dev->irq = EMAC_INT;

	/* Fill in the fields of the device structure with ethernet values. */
	ether_setup(dev);

	eth_init(dev);

	SET_MODULE_OWNER(dev);
	return 0;
}


static int emac_tx_timeout(struct net_device *dev)
{
}

static int emac_open(struct net_device *dev)
{
	unsigned long phy_status = 0;
        struct net_local *lp = (struct net_local *)dev->priv;
	unsigned short i;
	int result;
	struct sk_buff * skb;

	unsigned long flags;

	/* Prepare descriptors and skbs */
	/*Prepare tx descriptors and ring buffer */

	spin_lock_init(&lp->lock);	

#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
	spin_lock_init(&lp->filter_lock);	
#endif
	lp->tx_ring_size = 0;
	lp->TX_RING_TAIL = 0;
	lp->TX_RING_HEAD = 0;

	for (i=0;i<MAX_TX_DESCS;i++)
	{
		lp->TX_RING[i] = NULL;
	}

	/*prepare rx descriptors and ring buffer*/
	for(i=0;i<MAX_RX_DESCS;i++)
	{
		skb = dev_alloc_skb(MAX_PACKET_LEN);
		if( unlikely( (unsigned)skb->data >= 0x00FED000 ) )
			printk( "%s: skb=%p!!!\n", __FUNCTION__, skb ) ;
		if (!skb)
		{
			printk("Could not allocate skb at open. \n");
			goto out;
		}
		skb_reserve(skb,2);
		lp->RX_RING[i] = skb;
		lp->RX_DESCS[i].DES2 = skb->data;
		lp->RX_DESCS[i].DES0 = 0x80000000; /* DMA OWNED... */
	}

	lp->CUR_RX_DESC = 0;
	lp->max_tx_packets = 0;

	tasklet_init(&lp->emac_rx_tasklet, emac_rx_tasklet_fn, (unsigned long)dev);


#ifdef CONFIG_SC14452_ES2
	KEY_GP_INT_REG = 6;
	SetPort(P2_08_MODE_REG, PORT_PULL_DOWN, 41);    // P2_08 is int6

	while( RESET_INT_PENDING_REG & 0x2 ) {
		KEY_STATUS_REG = RMII_INT ;
		RESET_INT_PENDING_REG = 0x2;
	}

spin_lock_irqsave(&lp->lock,flags);
cache_disable();
#endif
	emac_cfg(dev->dev_addr, lp);

#ifdef CONFIG_SC14452_ES2
cache_enable();
spin_unlock_irqrestore(&lp->lock,flags);
#endif

	/* Acquire the IRQ before any is generated */
	result = request_irq(dev->irq, &emac_interrupt, SA_INTERRUPT, "emac_irq", dev);	
	if (result)
	{
		printk("Could not acquire EMAC IRQ %d \n",dev->irq);
		goto out;
	}
#ifdef CONFIG_SC14452_ES2
	/* Acquire the IRQ before any is generated */
	result = request_irq(KEYB_INT, &rmii_interrupt, SA_INTERRUPT, "rmii_irq", dev);	
	if (result)
	{
		printk("Could not acquire RMII IRQ %d \n",KEYB_INT);
		goto out;
	}	
#endif


	init_timer(&lp->emac_timer);
	lp->emac_timer.expires = jiffies + TMOUT;
	lp->emac_timer.data = (unsigned long)dev;
	lp->emac_timer.function = &emac_timer_handler;
	add_timer(&lp->emac_timer);

#if 0
#if defined( CONFIG_SC1445x_POWER_DRIVER )
    {
        u16 creg, phy_addr, phy_reg;
        
        for ( phy_addr = 1; phy_addr < 5; phy_addr++ ) {
            phy_reg = 0; 
            creg = EMAC_MACR4_MII_ADDR_REG;
            while ((creg & 0x1) == 1) creg = EMAC_MACR4_MII_ADDR_REG;
            creg &= ~( 0x1f<<6 | 0x1f<<11);
            creg |=  (phy_addr << 11) | (phy_reg << 6) | 0x3;
            EMAC_MACR5_MII_DATA_REG = 0x800;
            EMAC_MACR4_MII_ADDR_REG = creg;

        }    
    
    }
#endif    
#endif

	netif_start_queue(dev);

	return 0;
out:
	/* Clean any gathered SKBs */
	for(i=0;i<MAX_RX_DESCS;i++)
	{
		if(lp->RX_RING[i])
			dev_kfree_skb(lp->RX_RING[i]);
	}	
	return -ENOMEM;
}

static int emac_close(struct net_device *dev)
{
        struct net_local *lp = (struct net_local *)dev->priv;
	unsigned short i;
	
	
	lp->state= STATE_OFF;
	
	del_timer(&lp->emac_timer);

	netif_stop_queue(dev);
	free_irq(dev->irq, dev);
#ifdef CONFIG_SC14452_ES2
	free_irq(KEYB_INT, dev);
#endif
	for(i=0;i<MAX_RX_DESCS;i++)
		if(lp->RX_RING[i])
			dev_kfree_skb(lp->RX_RING[i]);

	for(i=0;i<MAX_TX_DESCS;i++)
		if(lp->TX_RING[i])
			dev_kfree_skb(lp->TX_RING[i]);

	tasklet_kill(&lp->emac_rx_tasklet);

	EMAC_MACR0_CONFIG_REG &= ~0x3;

	emac_init_tx_desc(lp->TX_DESCS);
	emac_init_rx_desc(lp->RX_DESCS);

	emac_soft_rst();

#ifdef CONFIG_SC14452_ES2
	rec_enabled = 1;
#endif
}

/* ********************************************************************************************************
*  The ioctl function is used to pass address filtering information between the driver and the application.
*  The SC14452 can protect up to 4 Ethernet addresses. This mechanism is used to ensure phone quality in 
*  	malicious network load conditions.
*  The SIOCDEVPRIVATE slot is used for this purpose. 
*  The commands exchanged are: 
*
*	EMAC_LF_STATUS          0
*	EMAC_LF_ADD             1
*	EMAC_LF_REMOVE          2
*	EMAC_LF_REMOVE_ALL      3
*	EMAC_LF_START_ALL       4
*	EMAC_LF_START_BCAST     5
*	EMAC_LF_START_UCAST     6
*	EMAC_LF_STOP_ALL        7
*	EMAC_LF_STOP_BCAST      8
*	EMAC_LF_STOP_UCAST      9
*****************************************************************************************************/


static void open_switch_ports(void)
{
	u16 creg, phy_addr, phy_reg;
	unsigned int temp;
return;
  for ( phy_addr = 1; phy_addr < 5; phy_addr++ ) {

    	temp = emac_md_read (phy_addr, 0, EMAC_MDC_DIV_42);
     	temp = temp & ~0x800;
     	emac_md_write (phy_addr, 0, EMAC_MDC_DIV_42,temp);

/*        	phy_reg = 0;
      creg = EMAC_MACR4_MII_ADDR_REG;
      while ((creg & 0x1) == 1) creg = EMAC_MACR4_MII_ADDR_REG;
      creg &= ~( 0x1f<<6 | 0x1f<<11);
      creg |=  (phy_addr << 11) | (phy_reg << 6) | 0x3;
      EMAC_MACR5_MII_DATA_REG = 0x8200;
      EMAC_MACR4_MII_ADDR_REG = creg;
*/
  }

}

static void close_switch_ports(void)
{
  u16 creg, phy_addr, phy_reg;
	unsigned int temp;
return;
  for ( phy_addr = 1; phy_addr < 5; phy_addr++ ) {

    	temp = emac_md_read (phy_addr, 0, EMAC_MDC_DIV_42);
     	temp = temp | 0x800;
     	emac_md_write (phy_addr, 0, EMAC_MDC_DIV_42,temp);
/*
      phy_reg = 0;
      creg = EMAC_MACR4_MII_ADDR_REG;
      while ((creg & 0x1) == 1) creg = EMAC_MACR4_MII_ADDR_REG;
      creg &= ~( 0x1f<<6 | 0x1f<<11);
      creg |=  (phy_addr << 11) | (phy_reg << 6) | 0x3;
      EMAC_MACR5_MII_DATA_REG = 0x800;
      EMAC_MACR4_MII_ADDR_REG = creg;
*/
  }

}


static int sc14452_ioctl(struct net_device *dev, struct ifreq *ifr, int cmd)
{
        struct net_local *lp = (struct net_local *)dev->priv;
#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
	struct addr_filter_ioctl * command_struct_p;
#else
	struct mii_struct * command_struct_p;
#endif	
	int i;
	int count = 0;
	int found = 0;
	unsigned long flags;
	
return;
        if (!netif_running(dev))
                return -ENODEV;

        switch (cmd) {
        case EMAC_CLOSE_SW:
        	spin_lock_irqsave(&lp->lock,flags); /* protect from acces from other ptocesses */
        	close_switch_ports();
        	spin_unlock_irqrestore(&lp->lock,flags);
        	return 0;
        	break;
        case EMAC_OPEN_SW:
        	spin_lock_irqsave(&lp->lock,flags); /* protect from acces from other ptocesses */
        	open_switch_ports();
        	spin_unlock_irqrestore(&lp->lock,flags);
        	return 0;
        	break;
        case EMAC_SPEED_10:
//        	printk("emac_ioctl_10\n");
			spin_lock_irqsave(&lp->lock,flags); /* protect from acces from other ptocesses */
			emac_init_speed(10);

			lp->tx_ring_size = 0;
			lp->TX_RING_TAIL = 0;
			lp->TX_RING_HEAD = 0;

			for (i=0;i<MAX_TX_DESCS;i++)
			{
				lp->TX_RING[i] = NULL;
			}

			/*prepare rx descriptors and ring buffer*/
			for(i=0;i<MAX_RX_DESCS;i++)
			{
				lp->RX_DESCS[i].DES0 = 0x80000000; /* DMA OWNED... */
			}

			lp->CUR_RX_DESC = 0;
			lp->max_tx_packets = 0;



			emac_cfg(dev->dev_addr, lp);
			spin_unlock_irqrestore(&lp->lock,flags);
			return 0;
        	break;
        case EMAC_SPEED_100:
 //       	printk("emac_ioctl_100\n");
			spin_lock_irqsave(&lp->lock,flags); /* protect from acces from other ptocesses */
			emac_init_speed(100);

			lp->tx_ring_size = 0;
			lp->TX_RING_TAIL = 0;
			lp->TX_RING_HEAD = 0;

			for (i=0;i<MAX_TX_DESCS;i++)
			{
				lp->TX_RING[i] = NULL;
			}

			/*prepare rx descriptors and ring buffer*/
			for(i=0;i<MAX_RX_DESCS;i++)
			{
				lp->RX_DESCS[i].DES0 = 0x80000000; /* DMA OWNED... */
			}

			lp->CUR_RX_DESC = 0;
			lp->max_tx_packets = 0;


			emac_cfg(dev->dev_addr, lp);
			spin_unlock_irqrestore(&lp->lock,flags);
			return 0;
        	break;
        case SIOCDEVPRIVATE:
		command_struct_p = ( struct mii_struct *) ifr->ifr_data;
		if (command_struct_p->command > 500)
		{
			switch (command_struct_p->command){
				case EMAC_MII_READ:
			 		command_struct_p->mii_data = emac_md_read ((unsigned char)(command_struct_p->mii_id), (unsigned char)(command_struct_p->mii_register),EMAC_MDC_DIV_42);
					break;
				case EMAC_MII_WRITE:
					emac_md_write ((unsigned char)(command_struct_p->mii_id), (unsigned char) (command_struct_p->mii_register), EMAC_MDC_DIV_42, command_struct_p->mii_data); 
					break;
				default:
					return -EOPNOTSUPP;
			}
			return 0;
		}
#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
        case SIOCDEVPRIVATE:
		spin_lock_irqsave(&lp->filter_lock,flags); /* protect from acces from other ptocesses */
		command_struct_p = ( struct addr_filter_ioctl *) ifr->ifr_data;
		switch (command_struct_p->command){
			case EMAC_LF_STATUS:
					command_struct_p ->command = EMAC_LF_COMMAND_SUCCESS;
					break;
			case EMAC_LF_ADD:
					/* Broadcast addresses are not allowed ...*/
					if ( command_struct_p->status.address_filters[0].MAC[0] & 0x1){
						command_struct_p->command = EMAC_LF_COMMAND_FAIL_BCAST;
						break;
					}	
					for (i=0;i<4;i++)
					{	/* look for an empty slot */
						if(lp->filter_config.address_filters[i].enable){
							count++;
						}
						else
						{
							lp->filter_config.address_filters[i].enable = 1;
							lp->filter_config.address_filters[i].pid = command_struct_p->pid;
							memcpy(lp->filter_config.address_filters[i].MAC ,command_struct_p->status.address_filters[0].MAC , 6);
							/* It is important here that the second address register is writen last. Otherwise the HW will not
							   recognize the MAC address correctly */
							*(unsigned long *)(0xff2048 +(i*8))= (0xc000 <<16)| (lp->filter_config.address_filters[i].MAC[5] <<8 )| (lp->filter_config.address_filters[i].MAC[4] )	;
							*(unsigned long *)(0xff204c +(i*8))= (lp->filter_config.address_filters[i].MAC[3] <<24 )| (lp->filter_config.address_filters[i].MAC[2] << 16 ) | (lp->filter_config.address_filters[i].MAC[1] <<8 )| (lp->filter_config.address_filters[i].MAC[0]  );
							command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
							break;
						}
					}
					if(count == 4)
						command_struct_p->command = EMAC_LF_COMMAND_FAIL_FULL;
					break;
			case EMAC_LF_REMOVE:
					found = 0;
					for (i=0;i<4;i++)
					{
						if (lp->filter_config.address_filters[i].enable == 1)
						{
							if(!memcmp(lp->filter_config.address_filters[i].MAC , command_struct_p->status.address_filters[i].MAC,6))
							{
								found = 1;
								lp->filter_config.address_filters[i].enable = 0;
								*(unsigned long *)(0xff2048 +(i*8))= 0x0;
								*(unsigned long *)(0xff204c +(i*8))= 0x0;
								command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
								break;
							}
						}
					}
					if(!found)
					{
						command_struct_p->command = EMAC_LF_COMMAND_FAIL_NFOUND;
					}
					break;
			case EMAC_LF_REMOVE_ALL:
					break;
			case EMAC_LF_START_ALL:
					break;
			case EMAC_LF_START_BCAST:
					lp->filter_config.blp_enable = 1;
					lp->filter_config.blp_threshold = command_struct_p->status.blp_threshold;
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
			case EMAC_LF_START_UCAST:
					lp->filter_config.ulp_enable = 1;
					lp->filter_config.ulp_threshold = command_struct_p->status.ulp_threshold;
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
			case EMAC_LF_STOP_ALL:
					lp->filter_config.ulp_enable = 0;
					lp->filter_config.blp_enable = 0;
					lp->filter_config.blp_threshold = 0;
					lp->filter_config.ulp_threshold = 0;
					lp->filter_config.blp_protect = 0;
					lp->filter_config.ulp_protect = 0;
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 5) ;
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 9) ;
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
			case EMAC_LF_STOP_BCAST:
					lp->filter_config.blp_enable = 0;
					lp->filter_config.blp_threshold = 0;
					lp->filter_config.blp_protect = 0;
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 5) ;
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
			case EMAC_LF_STOP_UCAST:
					lp->filter_config.ulp_enable = 0;
					lp->filter_config.ulp_threshold = 0;
					lp->filter_config.ulp_protect = 0;
					EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 9) ;
					command_struct_p->command = EMAC_LF_COMMAND_SUCCESS;
					break;
			default: 
				return -EOPNOTSUPP;
		}
		spin_unlock_irqrestore(&lp->filter_lock,flags);
		/* Always copy the status structure to provide status info to the application */
		memcpy(&(command_struct_p->status),&(lp->filter_config),sizeof(struct addr_filter_status));
                return 0;
#endif
        }
        return -EOPNOTSUPP;
}

/*********************************************************************************************
*  The timer handler is used to run the Ethernet address filtering mechanism. The timer ticks 
*  every 1/2 sec. If the address filters are enabled, it checks whether the received unicast 
*  or broadcast packets exceed the user defined threshold. In this case, the filtering is enabled. 
*  For unicast filtering, the MMC counters are then checked at every tick and the number
*  of filtered packets are monitored against the threshold. If this nomber drops below the 
*  threshold, the filtering is disabled. For broadcast trafic, after disabling the reception
*  the system enables again in 4 seconds to check whether the broadcast storm is over. 
*********************************************************************************************/
static void emac_timer_handler(unsigned long data)
{
        struct net_device *dev = (struct net_device *)data;
        struct net_local *lp = (struct net_local *)dev->priv;
	unsigned long phy_status = 0;
#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
	unsigned long btotal,utotal;
	static int protect_slots = 0; 
	unsigned long flags;
		
	spin_lock_irqsave(&lp->filter_lock,flags);
	btotal = EMAC_MMC_RXBROADCASTFRAMES_G_REG;
	utotal = EMAC_MMC_RXUNICASTFRAMES_G_REG;
	lp->filter_config.ups = lp->tmp_ups * 2;
	lp->filter_config.bps = lp->tmp_bps * 2;
	if(lp->filter_config.ulp_enable){
		if(utotal > lp->filter_config.ulp_threshold){
			EMAC_MACR1_FRAME_FILTER_REG |= 0x1 << 9 ;
			lp->filter_config.ulp_protect = 1;	
		}
		else{
			EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 9) ;
			lp->filter_config.ulp_protect = 0;	
		}
		lp->filter_config.ulp_filtered = utotal - lp->filter_config.ups;
	}
	if(lp->filter_config.blp_enable){
		if(lp->filter_config.blp_protect == 0){
			if(btotal > lp->filter_config.blp_threshold){
				EMAC_MACR1_FRAME_FILTER_REG |= 0x1 << 5 ;
				lp->filter_config.blp_protect = 1;	
				protect_slots = 8;
			}
		}
		else{
			protect_slots --;
			if(protect_slots == 0){
				EMAC_MACR1_FRAME_FILTER_REG &= ~(0x1 << 5) ;
				lp->filter_config.blp_protect = 0;	
			}
		}
		lp->filter_config.blp_filtered = btotal - lp->filter_config.bps;
	}	
	lp->tmp_ups = 0;
	lp->tmp_bps = 0;

	spin_unlock_irqrestore(&lp->filter_lock,flags);
#endif
	lp->emac_timer.expires = jiffies + TMOUT;
	add_timer(&lp->emac_timer);	
}

static void emac_rx_tasklet_fn(unsigned long data)
{
        struct net_device *dev = (struct net_device *)data;
        struct net_local *lp = (struct net_local *) dev->priv;
	unsigned long status;
	int cur_rx_pos;
	unsigned long des0;
	unsigned short frame_len,Packet_Count,i,loopcount = 8;
	struct sk_buff *skb, *new_skb;
	unsigned temp;
	unsigned short temp1;

	unsigned long flags;
	

	status = GetDword (EMAC_DMAR5_STATUS_REG);

		Packet_Count = 0;
		cur_rx_pos = lp->CUR_RX_DESC;
		des0 = lp->RX_DESCS[cur_rx_pos].DES0;
		while(((des0 & 0x80000000) == 0) && (Packet_Count < 4) )
		{
			Packet_Count ++;
			if(des0 & (0x1<<15) ){
				lp->stats.rx_errors++;
				if(des0 & (0x1<<1))
					lp->stats.rx_crc_errors++;
			}	
			else
			{ /* GET THE PACKET AND forward it to the system */
				frame_len = ((des0 >> 16) & 0x3fff)-6;

				skb = lp->RX_RING[cur_rx_pos];
				skb->dev = dev;
				if( (((des0 >> 5)&0x1) == 1) && (((des0 >> 7)&0x1) ==0) && (((des0 )&0x1)== 0))
					skb->ip_summed = CHECKSUM_UNNECESSARY;
				else
					skb->ip_summed = CHECKSUM_NONE;
#ifndef CONFIG_SC14452_ES2
				flush_dcache_range(lp->RX_DESCS[cur_rx_pos].DES2,lp->RX_DESCS[cur_rx_pos].DES2+frame_len + 6);
#endif
#ifdef CONFIG_SC14452_NETLOAD_PROTECTION

				if(skb->data[0] & 0x1)
					lp->tmp_bps ++;
				else
					lp->tmp_ups ++;
#endif
				skb_put(skb,frame_len);
				skb->protocol = eth_type_trans(skb,dev);
				if( netif_rx(skb)==NET_RX_DROP) 
					lp->stats.rx_dropped ++;
				lp->stats.rx_packets++;
                                lp->stats.rx_bytes+= frame_len;
			}

			cur_rx_pos = (cur_rx_pos + 1)% MAX_RX_DESCS;
			des0 = lp->RX_DESCS[cur_rx_pos].DES0;	
		}
		
		i = lp->CUR_RX_DESC;
		while(Packet_Count)
		{
			Packet_Count --;
			des0 = lp->RX_DESCS[i].DES0;
			if( des0 & 0x1<<15)
			{
			 	lp->RX_DESCS[i].DES0 = 0x80000000;	
			}
			else
			{
				new_skb = dev_alloc_skb(MAX_PACKET_LEN);
				if( unlikely( (unsigned)new_skb->data >= 0x00FED000 ) )
					printk( "%s: skb=%p!!!\n", __FUNCTION__, new_skb ) ;
				if(new_skb == NULL)
					printk("No memory for new SKB!\n");	
				skb_reserve(new_skb,2);
				lp->RX_RING[i] = new_skb;
				lp->RX_DESCS[i].DES2 = new_skb->data;
                		lp->RX_DESCS[i].DES0 = 0x80000000; /* DMA OWNED... */
	
			}

			i = (i+1) % MAX_RX_DESCS;
		}
		
		lp->CUR_RX_DESC = cur_rx_pos;
}


#ifdef CONFIG_SC14452_ES2

static irqreturn_t rmii_interrupt(int irq, void *dev_id, struct pt_regs * regs)
{
	struct net_device *dev ;
	struct net_local *lp= dev->priv;

	unsigned temp;


	KEY_STATUS_REG = RMII_INT ;
	RESET_INT_PENDING_REG = 0x2;

	cache_disable();

	temp = GetDword (EMAC_DMAR6_OPERATION_MODE_REG);
	SetDword(EMAC_DMAR6_OPERATION_MODE_REG,temp | 2);

	SetDword(EMAC_DMAR2_RX_POLL_DEMAND_REG, 0);


	rec_enabled = 1;

	return IRQ_HANDLED;
	
}

void rmii_patch(void)
{

	unsigned temp;
	if (!rec_enabled)
	{

		if (P2_DATA_REG & 0x100)
		{

			KEY_STATUS_REG = RMII_INT ;
			RESET_INT_PENDING_REG = 0x2;

			cache_disable();

			temp = GetDword (EMAC_DMAR6_OPERATION_MODE_REG);
			SetDword(EMAC_DMAR6_OPERATION_MODE_REG,temp | 2);

			SetDword(EMAC_DMAR2_RX_POLL_DEMAND_REG, 0);
			rec_enabled = 1;
		}
	}
}


static irqreturn_t emac_interrupt(int irq, void *dev_id, struct pt_regs * regs)
{
	struct net_device *dev ;
	struct net_local *lp= dev->priv;
	unsigned long status;
	int cur_rx_pos;
	unsigned long des0;
	unsigned short frame_len,Packet_Count,i,loopcount = 8;
	struct sk_buff *skb, *new_skb;

	unsigned temp;
	volatile unsigned short rx_over = 0;

	dev = (struct net_device *)dev_id;
	lp = (struct net_local *) dev->priv;

	status = GetDword (EMAC_DMAR5_STATUS_REG);

	if (status & 0x40)
	{
		EMAC_DMAR5_STATUS_REG =0x40;
		if  ((P2_DATA_REG & 0x100 ) == 0)
		{
				temp = GetDword (EMAC_DMAR6_OPERATION_MODE_REG);
				SetDword(EMAC_DMAR6_OPERATION_MODE_REG,temp & ~2);
				rec_enabled = 0;

		}
	}

	if (status & 0x10)
	{
		EMAC_DMAR5_STATUS_REG =0x10;

		temp = GetDword (EMAC_DMAR6_OPERATION_MODE_REG);
		SetDword(EMAC_DMAR6_OPERATION_MODE_REG,temp & ~2);
		rec_enabled = 0;
		rx_over = 1;
	}

	if (status & 0x5/*0x1*/)
	{
		unsigned short i;

		EMAC_DMAR5_STATUS_REG =0x01;

		unsigned short tx_in_progress = 0;

		for(i=0;i<MAX_TX_DESCS;i++)
			if(lp->TX_DESCS[i].DES0 & 0x80000000)
			{
				tx_in_progress = 1;
				break;
			}

		if (!tx_in_progress)
		{
			temp = GetDword (EMAC_DMAR6_OPERATION_MODE_REG);
			SetDword(EMAC_DMAR6_OPERATION_MODE_REG,temp & ~0x2000);
			xmit_enabled = 0;
		}
	}

	EMAC_DMAR5_STATUS_REG = 0x1e7ae;
	
	if(status & GMI)
	{
		printk("EMAC counter interrupt\n");
	}

	if (!rec_enabled && temp_rec_enabled)
	{
		temp = GetDword (EMAC_DMAR6_OPERATION_MODE_REG);
		SetDword(EMAC_DMAR6_OPERATION_MODE_REG,temp & ~2);

		temp_rec_enabled = 0;
	}

	if (!xmit_enabled)
	{
		

		if ((rx_over) || (!rec_enabled)&&((P2_DATA_REG & 0x100 ) == 0))
		{
			while ( ((EMAC_DMAR5_STATUS_REG & 0xe0000) == 0xe0000)|| ((EMAC_DMAR5_STATUS_REG & 0xe0000) == 0x20000)||((EMAC_DMAR5_STATUS_REG & 0xe0000) == 0xa0000)) ;
		
			cache_enable();
		}
	}
	if (status & (RU|RXINT|OVF))
	{
		tasklet_schedule(&lp->emac_rx_tasklet);
	}
//	INT3_PRIORITY_REG &= ~EMAC_INT_PRIO; 
	SetWord(RESET_INT_PENDING_REG,EMAC_INT_PEND);
	return IRQ_HANDLED;
}

#else

static irqreturn_t emac_interrupt(int irq, void *dev_id, struct pt_regs * regs)
{
	struct net_device *dev ;
	struct net_local *lp= dev->priv;
	unsigned long status;
	int cur_rx_pos;
	unsigned long des0;
	unsigned short frame_len,Packet_Count,i,loopcount = 8;
	struct sk_buff *skb, *new_skb;

	unsigned temp;
	volatile unsigned short rx_over = 0;

	dev = (struct net_device *)dev_id;
	lp = (struct net_local *) dev->priv;

	status = GetDword (EMAC_DMAR5_STATUS_REG);

	if (status & 0x40)
	{
		EMAC_DMAR5_STATUS_REG =0x40;
		
	}

	if (status & 0x10)
	{
		EMAC_DMAR5_STATUS_REG =0x10;

		rx_over = 1;
	}

	if (status & 0x5/*0x1*/)
	{
		unsigned short i;

		EMAC_DMAR5_STATUS_REG =0x01;

	}

	EMAC_DMAR5_STATUS_REG = 0x1e7ae;
	
	if(status & GMI)
	{
		printk("EMAC counter interrupt\n");
	}

	
	
	if (status & (RU|RXINT|OVF))
	{
		tasklet_schedule(&lp->emac_rx_tasklet);
	}
	SetWord(RESET_INT_PENDING_REG,EMAC_INT_PEND);
	return IRQ_HANDLED;
}


#endif


static int emac_xmit(struct sk_buff *skb, struct net_device *dev)
{
	struct net_local *lp = (struct net_local *) dev->priv;
	short length = 0;
	unsigned long status,des0;
	unsigned long flags;
	unsigned temp;

        if(skb == NULL) {
                printk("xmit with empty skb\n");
                return 0;
        }
        if (netif_queue_stopped(dev)) {
                printk("xmit with stopped queue\n");
                return -EBUSY;
        }

	

        if(lp->tx_ring_size == MAX_TX_DESCS)
        {
                status = -EBUSY;
        }
	else  //proceed with transmission...
	{
		if(lp->TX_RING[lp->TX_RING_HEAD] != NULL)
			printk("ERROR in TX: RING BUFFER HEAD NOT NULL\n");

		length =  ETH_ZLEN < skb->len ? skb->len : ETH_ZLEN;

		lp->TX_RING[lp->TX_RING_HEAD] = skb;
		spin_lock_irqsave(&lp->lock,flags);
		lp->TX_DESCS[lp->TX_RING_HEAD].DES2 = skb->data;
		lp->TX_DESCS[lp->TX_RING_HEAD].DES1 |= 	DWORD_SHIFT(0x1 , 30) |
							DWORD_SHIFT(0x1 , 29) |
							length;
		lp->TX_DESCS[lp->TX_RING_HEAD].DES0 = 0x80000000;

#ifdef CONFIG_SC14452_ES2
		xmit_enabled = 1;
		cache_disable();
		temp = GetDword (EMAC_DMAR6_OPERATION_MODE_REG);
		SetDword(EMAC_DMAR6_OPERATION_MODE_REG,temp | 0x2002);
		SetDword(EMAC_DMAR2_RX_POLL_DEMAND_REG, 0);
#endif
		SetDword(EMAC_DMAR1_TX_POLL_DEMAND_REG, 0);

#ifdef CONFIG_SC14452_ES2
		temp_rec_enabled = 1;
#endif
	spin_unlock_irqrestore(&lp->lock,flags);
		//advance tx_head
		lp->TX_RING_HEAD = (lp->TX_RING_HEAD+1) % MAX_TX_DESCS;
		lp->tx_ring_size += 1;
		lp->max_tx_packets ++;
	}



	if((!(lp->max_tx_packets % 4)) || (lp->tx_ring_size == MAX_TX_DESCS))
	{
		while (lp->tx_ring_size &&  ((lp->TX_DESCS[lp->TX_RING_TAIL].DES0 & 0x80000000) == 0))
		{
			des0 = lp->TX_DESCS[lp->TX_RING_TAIL].DES0;
			if(des0 & 1 << 15){
				lp->stats.tx_errors ++;
				if(des0 & (1<<10))
					lp->stats.tx_carrier_errors ++;
			}
			lp->stats.tx_packets ++;
			lp->stats.tx_bytes += length;
			lp->TX_DESCS[lp->TX_RING_TAIL].DES2 = 0;
			lp->TX_DESCS[lp->TX_RING_TAIL].DES1 &=  DWORD_SHIFT(0x1,25) ;
			dev_kfree_skb(lp->TX_RING[lp->TX_RING_TAIL]);
			lp->TX_RING[lp->TX_RING_TAIL] = NULL;

			lp->tx_ring_size --;
			lp->TX_RING_TAIL = (lp->TX_RING_TAIL +1)% MAX_TX_DESCS;
		}  
	}	

	return 0; 
}



static struct net_device_stats *emac_get_stats(struct net_device *dev)
{
	struct net_local *lp = (struct net_local *)dev->priv;

	lp->stats.multicast += EMAC_MMC_RXMULTICASTFRAMES_G_REG;
	lp->stats.collisions += EMAC_MMC_TXSINGLECOL_G_REG + EMAC_MMC_TXMULTICOL_G_REG;
	lp->stats.rx_length_errors += EMAC_MMC_RXUNDERSIZE_G_REG + EMAC_MMC_RXOVERSIZE_G_REG;
	lp->stats.rx_fifo_errors += EMAC_MMC_RXFIFOOVERFLOW_REG;
	lp->stats.tx_fifo_errors += EMAC_MMC_TXUNDERFLOWERROR_REG;
	return &lp->stats;
}


static void emac_set_multicast_list(struct net_device *dev)
{


}
