// File created by Gigaset Elements GmbH
// All rights reserved.
/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 *
 * Project: VoIP
 * RHEA SOFTWARE PLATFORM
 * SC14452 EMAC LINUX DRIVER
 *
 * File sc14452_lantiq_tantos.h
 */

#ifndef SC14452_H
#define SC14452_H

#include <linux/if_ether.h>
#include <linux/ioport.h>
#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <asm-cr16/regs.h>
static const char *version =
        "sc14452.c: v1.0 Sitel Semiconductor\n";

#define MAX_PACKET_LEN 0x700


/* The OS name of the card. Is used for messages and in the requests for*/
/* io regions, irqs and dma channels                                    */
static const char* cardname = "SC14452_EMU";

/*----------------------------*/
/*  IO configurations         */
/*----------------------------*/
//#define PORT_INPUT          0
//#define PORT_PULL_UP        1
//#define PORT_PULL_DOWN      2
//#define PORT_OUTPUT         3


#define 	STATE_OFF	0
#define		STATE_ON	1

#define MAX_RX_DESCS	16
#define MAX_TX_DESCS	16


#define TX_DES_BASE  0x0020000
#define RX_DES_BASE  0x0021000

#define EMAC_MII_READ           1000
#define EMAC_MII_WRITE          1001

struct mii_struct{
        unsigned long command;
        unsigned long mii_id;
        unsigned long mii_register;
        unsigned long mii_data;
        };

void tantos_init(void);
void write_smi_tantos(unsigned int reg_address, unsigned int reg_data);

#ifdef CONFIG_SC14452_NETLOAD_PROTECTION

#define ULP_THRESHHOLD	500	// Default values for unicast frames per second threshold
#define BLP_THRESHHOLD	200	// Default values for broadcast frames per second threshold

/* EMAC_LOADFILTER Message type definitions */
#define EMAC_LF_STATUS          0
#define EMAC_LF_ADD             1
#define EMAC_LF_REMOVE          2
#define EMAC_LF_REMOVE_ALL      3
#define EMAC_LF_START_ALL       4
#define EMAC_LF_START_BCAST     5
#define EMAC_LF_START_UCAST     6
#define EMAC_LF_STOP_ALL        7
#define EMAC_LF_STOP_BCAST      8
#define EMAC_LF_STOP_UCAST      9

#define EMAC_LF_COMMAND_SUCCESS					100
#define EMAC_LF_COMMAND_FAIL						200
#define EMAC_LF_COMMAND_FAIL_BCAST      201
#define EMAC_LF_COMMAND_FAIL_FULL       202
#define EMAC_LF_COMMAND_FAIL_NFOUND     203


struct addr_filter {
	unsigned short enable;
	unsigned short pid;
	unsigned char MAC[6];
}; 	

struct addr_filter_status {
	unsigned short ulp_enable;	// Unicast load protection enable
	unsigned short blp_enable;	// Broadcast load protection Enable
	unsigned short ulp_threshold;	// Unicast load threshold
	unsigned short blp_threshold;  // Broadcast load threshold
	unsigned long ups; 		// Unicast packets per second
	unsigned long bps;		// Broadcast packets per second
	unsigned short blp_filtered;	//
	unsigned short ulp_filtered;	//
	unsigned short blp_protect;	// Indicates whether broadcast traffic protection is activated
	unsigned short ulp_protect;	// Indicates whether Unicast traffic protection is activated
	struct addr_filter address_filters[4];		// Filter addresses and enable flags
};

struct addr_filter_ioctl {
	unsigned short command;
	unsigned short pid;
	struct addr_filter_status status;
}; 
#endif

/***
 *** Tx/Rx descriptors
 ***/
struct s_descriptor {
   volatile unsigned long int DES0;
   volatile unsigned long int DES1;
   volatile unsigned long int DES2;
   volatile unsigned long int DES3;   
};

#define ULP_THRESHOLD		500
#define BLP_THRESHOLD		100

struct net_local {
        struct net_device_stats stats;  /* uClinux ethernet stats struct */
        int state;                      /* State of driver */
        int Link;                       /* Link Indication */
        int FullDuplex;                 /* Full Duplex link */
        int LinkSpeed100Mbps;           /* Link Speed Indication */
        struct timer_list emac_timer;    /* link detection timer */
#ifdef CONFIG_SC14452_ES2
	struct s_descriptor RX_DESCS[MAX_RX_DESCS];  
#else
        struct s_descriptor* RX_DESCS;  
#endif
        struct sk_buff * RX_RING[MAX_RX_DESCS];
        char  * RX_BUFFERS;
        int CUR_RX_DESC;
#ifdef CONFIG_SC14452_ES2
	struct s_descriptor  TX_DESCS[MAX_TX_DESCS];  
#else
        struct s_descriptor*  TX_DESCS;  
#endif
        struct sk_buff * TX_RING[MAX_TX_DESCS];
        int tx_ring_size;
        int TX_RING_HEAD;
        int TX_RING_TAIL;
        int INT_STATS[14];
        struct tasklet_struct emac_rx_tasklet;
        struct tasklet_struct emac_tx_tasklet;
        int Frame_end;
        int Frame_start;
        int tx_disabled;
	unsigned short max_tx_packets;
	spinlock_t              lock;
#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
	spinlock_t              filter_lock;
	struct addr_filter_status filter_config;
	unsigned long tmp_ups;
	unsigned long tmp_bps;
#endif
};

struct emac_dev {
		struct s_descriptor  *tx_descriptor;
		unsigned long int    *tx_buf_base;
		unsigned long int    tx_buf_size;
		unsigned char        num_of_tx_des;		
		unsigned char        tx_ptr;
		struct s_descriptor  *rx_descriptor;
		unsigned long int    *rx_buf_base;
		unsigned long int    rx_buf_size;
		unsigned char        num_of_rx_des;
		unsigned char        rx_ptr;
};

struct emac_dev emacDev;



#endif 

#ifdef  DEBUG
#define PRINTK(args...) printk(args)
#else
#define PRINTK(args...)
#endif

//#define SetPort(a,b,c) (a = ((b << 8) | c))
//-----------------------------------------------------------------------
//              Mathematics
//-----------------------------------------------------------------------

#define DWORD_SHIFT(DAT,BITS) ((unsigned long int) (DAT) << (BITS))

#define NUM_OF_DWORDS(x) (((x % 4) == 0) ? (x >> 2) : ((x >>2 ) + 1))


//-----------------------------------------------------------------------
//              Definitions for memory allocation
//-----------------------------------------------------------------------


#define TX_BUF_GAP  0      // space between two sequential tx buffers (bytes)
#define TX_BUF_SIZE MAX_PACKET_LEN    // the size of one tx buffer (bytes)

#define RX_BUF_GAP  0      // space between two sequential rx buffers (bytes)
#define RX_BUF_SIZE MAX_PACKET_LEN    // the size of one rx buffer (bytes)
//-----------------------------------------------------------------------
//              Definitions for the SMI interface (MDC, MDIO)
//-----------------------------------------------------------------------
#define MD_PHY_ADDR(x) DWORD_SHIFT((x & 0x1f),11)
#define MD_REG_ADDR(x) DWORD_SHIFT((x & 0x1f),6)
#define MD_CLK(x)      DWORD_SHIFT((x & 0x7),2)
#define MD_WRITE       DWORD_SHIFT(1,1)
#define MD_BUSY        0x1

#define EMAC_MDC_DIV_42  0x0
#define EMAC_MDC_DIV_62  0x1
#define EMAC_MDC_DIV_16  0x2
#define EMAC_MDC_DIV_26  0x3
#define EMAC_MDC_DIV_102 0x4
#define EMAC_MDC_DIV_122 0x5

//-----------------------------------------------------------------------
//              Emac peripheral ID
//-----------------------------------------------------------------------

#define PID_EMAC_PIN  57

//-----------------------------------------------------------------------
//  The dma channel used in order to accelerate the process of memory copy.
//-----------------------------------------------------------------------

#define EMAC_USE_DMA_CHANNEL 0

//-----------------------------------------------------------------------
//              Structures and variables related with the emac.
//-----------------------------------------------------------------------

char TxEnd;

#define MAX_RX_FRAMES 4
#define RX_FRAME_SIZE 2000   // Multiple of 4 only!!!!

unsigned char *RxFrameBuf=NULL;

char RxFrames=0;

/*----------------------------------------------------------------------------*/
/*                               Clock Enable                                 */
/*----------------------------------------------------------------------------*/
void emac_clk_enable(void) {
	SetBits(CLK_AUX2_REG, SW_EMAC_EN, 1);	
}

/*----------------------------------------------------------------------------*/
/*                               Clock Disable                                */
/*----------------------------------------------------------------------------*/
void emac_clk_disable(void) {
	SetBits(CLK_AUX2_REG, SW_EMAC_EN, 0);
}

/*----------------------------------------------------------------------------*/
/*                   Sets the interface speed to 10Mpbs                       */
/*----------------------------------------------------------------------------*/
void emac_10mbps(void) {
	SetBits(CLK_AUX2_REG, SW_ETH_SEL_SPEED, 0);
}

/*----------------------------------------------------------------------------*/
/*                   Sets the interface speed to 100Mpbs                      */
/*----------------------------------------------------------------------------*/
void emac_100mbps(void) {
	SetBits(CLK_AUX2_REG, SW_ETH_SEL_SPEED, 1);
}

/*----------------------------------------------------------------------------*/
/*                           Init PADs for RMII mode                          */
/*----------------------------------------------------------------------------*/

void emac_rmii_pads (void)
{
	SetPort(P3_02_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // port3(2) -> emac ref_clk

	SetPort(P3_03_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxEn output
	SetPort(P3_07_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxD[0] output
	SetPort(P3_08_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxD[1] output
	SetPort(P3_04_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxDV input
	SetPort(P3_05_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxD[0] input
	SetPort(P3_06_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxD[1] input

}

/*----------------------------------------------------------------------------*/
/*                           Init PADs for MII mode                          */
/*----------------------------------------------------------------------------*/

void emac_mii_pads (void)
{
	//SetPort(P0_00_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // COL input
	//SetPort(P0_01_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // CSR input
	//SetPort(P0_02_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RXER input

	SetPort(P0_04_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // TXCLK
	SetPort(P0_05_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RXCLK

	SetPort(P3_03_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxEn output
	SetPort(P3_07_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxD[0] output
	SetPort(P3_08_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxD[1] output
	SetPort(P0_07_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxD[2] output
	SetPort(P0_08_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxD[3] output

	SetPort(P3_04_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxDV input
	SetPort(P3_05_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxD[0] input
	SetPort(P3_06_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxD[1] input
	SetPort(P0_09_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxD[2] input
	SetPort(P0_10_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxD[3] input
}

/*----------------------------------------------------------------------------*/
/*                           Init MD PADs                                     */
/*----------------------------------------------------------------------------*/

void emac_md_pads(void)
{
	SetPort(P3_00_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);    // MDC output
	SetPort(P3_01_MODE_REG, GPIO_PUPD_IN_NONE,  PID_EMAC_PIN);    // MDIO
}


/*----------------------------------------------------------------------------*/
/*                   Set RMII mode and Internal Clocks                        */
/*----------------------------------------------------------------------------*/
void emac_rmii_clk_int(void)
{
	SetBits(CLK_AUX2_REG, SW_RMII_EN,1);        // Set RMII Mode
	SetBits(CLK_AUX2_REG, SW_RMII_CLK_INT, 1);  // Internal source for RMII ref clock
	SetBits(GPRG_R0_REG, 0x0100, 0);            // REF_CLK output
}


/*----------------------------------------------------------------------------*/
/*                   Set RMII mode and External Clocks                        */
/*----------------------------------------------------------------------------*/
void emac_rmii_clk_ext (void)
{
	SetBits(CLK_AUX2_REG, SW_RMII_EN,1);        // Set RMII Mode
	SetBits(GPRG_R0_REG, 0x0100, 1);            // REF_CLK input
	SetBits(CLK_AUX2_REG, SW_RMII_CLK_INT, 0);  // External source for RMII ref clock
}


/*----------------------------------------------------------------------------*/
/*                   Set MII mode and External  Clocks                        */
/*----------------------------------------------------------------------------*/

void emac_mii_clk_ext(void)
{
	SetBits(CLK_AUX2_REG, SW_RMII_EN,0);                  // Set MII Mode
	SetPort(P0_04_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // TXCLK input
	SetPort(P0_05_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RXCLK input
}

/*----------------------------------------------------------------------------*/
/*                          MD Interface - Read Function                      */
/*----------------------------------------------------------------------------*/
unsigned int emac_md_read (unsigned char phy_address, unsigned char reg_address, unsigned char mdc_div)
{
	// Check the BUSY bit before write to the EMAC_MACR4_MII_ADDR_REG
	while (GetDword (EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);

	// Write the read request
	SetDword (EMAC_MACR4_MII_ADDR_REG, MD_PHY_ADDR(phy_address) | 
	                                   MD_REG_ADDR(reg_address) |
					   MD_CLK(mdc_div) |
					   MD_BUSY);
		
	// Wait Until to receive the requested data
	while (GetDword (EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);
		
	return (GetDword (EMAC_MACR5_MII_DATA_REG));
}



/*----------------------------------------------------------------------------*/
/*                         MD Interface - Write Function                      */
/*----------------------------------------------------------------------------*/
void emac_md_write (unsigned char phy_address, unsigned char reg_address, unsigned char mdc_div, unsigned int wdata)
{
	// Check the BUSY bit before write to the EMAC_MACR4_MII_ADDR_REG & EMAC_MACR5_MII_ADDR_REG
	while (GetDword (EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);
	
	SetDword (EMAC_MACR5_MII_DATA_REG, wdata);

	SetDword (EMAC_MACR4_MII_ADDR_REG, MD_PHY_ADDR(phy_address) | 
	                                   MD_REG_ADDR(reg_address) |
	                                   MD_CLK(mdc_div) |
	                                   MD_WRITE  |
	                                   MD_BUSY);

	// Wait Until  the end of write operation
	while (GetDword(EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);

}

/*----------------------------------------------------------------------------*/
/*                     Initialize the external switch lantiq tantos	          */
/*----------------------------------------------------------------------------*/
void write_smi_tantos(unsigned int reg_address, unsigned int reg_data)
{
	emac_md_write((reg_address>>5), (reg_address&0x1f), EMAC_MDC_DIV_42, reg_data);
}

void tantos_init(void)
{
	printk("[%s]\n", __FUNCTION__);
	// MII port 5 and 6 100Mbit speed full duplex
	write_smi_tantos(0xf5, 0x773);
	// force MII port 5 up
	write_smi_tantos(0xa1, 0x4);
	// force MII port 6 up
	write_smi_tantos(0xc1, 0x4);
}

/*----------------------------------------------------------------------------*/
/*                           Init Tx Descriptor                               */
/*----------------------------------------------------------------------------*/

void emac_init_tx_desc (struct s_descriptor* TX_DESCS)
{
	unsigned char i;
	long int temp;
    
	temp = DWORD_SHIFT(0x1 , 31) | // [31]    Interrupt Completion
	       DWORD_SHIFT(0x0 , 30) | // [30]    Last Segment
	       DWORD_SHIFT(0x0 , 29) | // [29]    First Segment
	       DWORD_SHIFT(0x0 , 27) | // [28:27] Checksum Insertion Control
	       DWORD_SHIFT(0x0 , 26) | // [26]    Disable CRC
	       DWORD_SHIFT(0x0 , 25) | // [25]    Transmit End Of Ring
	       DWORD_SHIFT(0x0 , 24) | // [24]    Second address Chained
	       DWORD_SHIFT(0x0 , 23) | // [23]    Disable padding
	       DWORD_SHIFT(0x0 , 11) | // [21:11] Trasmit Buffer 2 Size
	                   0x0;        // [10:0]  Trasmit Buffer 1 Size
	   
	for (i=0; i<MAX_TX_DESCS; i++) {
		TX_DESCS[i].DES0 = 0x00000000; // The software is the owner of the descriptor
		TX_DESCS[i].DES1 = temp;
	
		TX_DESCS[i].DES2 = 0; 
		TX_DESCS[i].DES3 = 0; 
	}
    
	// Set Transmit End of Ring in the Last descriptor
	TX_DESCS[MAX_TX_DESCS-1].DES1 = TX_DESCS[MAX_TX_DESCS-1].DES1 | DWORD_SHIFT(0x1,25); // [25] Transmit End Of Ring;
}


/*----------------------------------------------------------------------------*/
/*                           Init Rx Descriptor                               */
/*----------------------------------------------------------------------------*/

void emac_init_rx_desc (struct s_descriptor* RX_DESCS)
{
	unsigned char i;
	long int temp;

	temp = DWORD_SHIFT(                0x0 , 31) | // [31]    Disable Interrupt On Completion
	       DWORD_SHIFT(                0x0 , 25) | // [25]    Receive End Of Ring
	       DWORD_SHIFT(                0x0 , 24) | // [24]    Second address Chained
	       DWORD_SHIFT(0 , 11) | 		       // [21:11] Trasmit Buffer 2 Size
	                   MAX_PACKET_LEN;        // [10:0]  Trasmit Buffer 1 Size

	for (i=0; i < MAX_RX_DESCS; i++) {
		RX_DESCS[i].DES0 = 0x00000000;    	
		RX_DESCS[i].DES1 = temp;

		RX_DESCS[i].DES2 = 0;
		RX_DESCS[i].DES3 = 0;
	}    

	// Set Receive End of Ring in the Last descriptor
	RX_DESCS[MAX_RX_DESCS-1].DES1 = RX_DESCS[MAX_RX_DESCS-1].DES1 | DWORD_SHIFT(0x1,25); // [25] Receive End Of Ring;
}

/*----------------------------------------------------------------------------*/
/*                       Prepare Rx Descriptors                               */
/*----------------------------------------------------------------------------*/
void emac_rx_des_prepare(void) 
{
	unsigned char i;

	for (i=0; i<emacDev.num_of_rx_des; i++) 
		emacDev.rx_descriptor[i].DES0 = 0x80000000; // Set OWN bit. The descriptor is owned by the DMA of the EMAC.
	
}

/*----------------------------------------------------------------------------*/
/*                        Emac Soft Reset                                     */
/*----------------------------------------------------------------------------*/

void emac_soft_rst(void)
{
	// Set Software reset bit. The new PHY configuration will be read.
	SetDword (EMAC_DMAR0_BUS_MODE_REG, 0x1); 

	// Wait End of Reset
	while (GetDword (EMAC_DMAR0_BUS_MODE_REG) & 0x1); 
}

/*----------------------------------------------------------------------------*/
/*                  The Emac start using the tx descriptors                   */
/*----------------------------------------------------------------------------*/

void emac_read_tx_des(void)
{
    SetDword(EMAC_DMAR1_TX_POLL_DEMAND_REG, 0x0);
}

/*----------------------------------------------------------------------------*/
/*                  The Emac start using the rx descriptors                   */
/*----------------------------------------------------------------------------*/

void emac_read_rx_des(void)
{
    SetDword(EMAC_DMAR2_RX_POLL_DEMAND_REG, 0x0);
}


/*----------------------------------------------------------------------------*/
/*                        Wait End of Frame Reception                         */
/*----------------------------------------------------------------------------*/

void emac_wait_frame_reception(void) {
    // Polling
    while (!(GetDword (EMAC_DMAR5_STATUS_REG) & (DWORD_SHIFT(0x1,6))));

    // Clear Receive Interrupt
    SetDword (EMAC_DMAR5_STATUS_REG, DWORD_SHIFT(0x1,6));
}


/*----------------------------------------------------------------------------*/
/*                        Wait End of Frame Transmission                       */
/*----------------------------------------------------------------------------*/

void emac_wait_frame_transmission(void) {
    // Polling
    while (!(GetDword (EMAC_DMAR5_STATUS_REG) & 0x1));

    // Clear Receive Interrupt
    SetDword (EMAC_DMAR5_STATUS_REG, 0x1);
}

    

/*----------------------------------------------------------------------------*/
/*                                   Init Emac                                */
/*----------------------------------------------------------------------------*/

void emac_cfg (unsigned char *mac_address, struct net_local * lp)
{
	long int temp;
	
	/***
	 *** Init: (see EthMAC datasheet paragraph 3.3.1,pp.46)
	 ***/
	 
	/* a) DMA Register0 = ...
	 *    Bus Mode Register
	 *    Details:
	 *    -> DMA Register0.DA = 1; // set round-robin arbitration with priority at Rx during collisions
	 *    -> Set fixed bursts
	 *       DMA Register0, bit FB 
	 *       and set maximum burst length (is necessary !!!!!):
	 *       Register0[13:8]=PBL field
	 */
	temp = DWORD_SHIFT( 0x1  , 25) |  // [25]    Address - Aligned Beats
	       DWORD_SHIFT( 0x0  , 24) |  // [24]    4xPBL Mode
	       DWORD_SHIFT( 0x1  , 23) |  // [23]    Use Separate PBL
	       DWORD_SHIFT( 0x04 , 17) |  // [22:17] RxDMA PBL
	       DWORD_SHIFT( 0x1  , 16) |  // [16]    Fixed Burst
	       DWORD_SHIFT( 0x1  , 14) |  // [15:14] Rx:Tx priority ratio
	       DWORD_SHIFT( 0x10 ,  8) |  // [13:8]  Programmable Burst Length
	       DWORD_SHIFT( 0x0  ,  2) |  // [6:2]   DSL : Descriptor Skip Length
	       DWORD_SHIFT( 0x1  ,  1) |  // [1]	  DMA Arbitration scheme
	                    0x0;          // [0]    Software Reset
     
	SetDword (EMAC_DMAR0_BUS_MODE_REG, temp); 
  
	/* b) DMA Register7 = ...
	 *    Interrupt Enable Register
	 */
	temp = DWORD_SHIFT( 0x1 , 16) | // [16] Normal Interrupt Summary Enable
	       DWORD_SHIFT( 0x1 , 15) | // [15] Abnormal Interrupt Summary Enable
	       DWORD_SHIFT( 0x1 , 14) | // [14] Early Receive Interrupt Enable
	       DWORD_SHIFT( 0x1 , 13) | // [13] Fatal Bus Error Enable
	       DWORD_SHIFT( 0x0 , 10) | // [10] Early Transmit Interrupt Enable
	       DWORD_SHIFT( 0x1 ,  9) | // [ 9] Receive Watchdog Timeout Enable
	       DWORD_SHIFT( 0x1 ,  8) | // [ 8] Receive Stopped Enable
	       DWORD_SHIFT( 0x1 ,  7) | // [ 7] Receive Buffer Unavailable Enable
	       DWORD_SHIFT( 0x1 ,  6) | // [ 6] Receive Interrupt Enable
	       DWORD_SHIFT( 0x1 ,  5) | // [ 5] Underflow Interrupt Enable
	       DWORD_SHIFT( 0x1 ,  4) | // [ 4] Overflow Interrupt Enable
	       DWORD_SHIFT( 0x1 ,  3) | // [ 3] Transmit Jabber Timeout Enable
	       DWORD_SHIFT( 0x1 ,  2) | // [ 2] Transmit Buffer Unavailable Enable
	       DWORD_SHIFT( 0x1 ,  1) | // [ 1] Transmit Stopped Enable
	                    0x1;        // [ 0] Transmit Interrupt Enable

	SetDword (EMAC_DMAR7_INT_ENABLE_REG,temp);	    
	        
	/* c) Prepare Tx/Rx buffers
	 *    and update DMA Register3 ( Receive Descript List Address Register) 
	 *               DMA Register4 ( Transmit Descriptor List Address Regsiter)
	 */
	SetDword(EMAC_DMAR3_RX_DESCRIPTOR_LIST_ADDR_REG, (unsigned long int) lp->RX_DESCS);
	SetDword(EMAC_DMAR4_TX_DESCRIPTOR_LIST_ADDR_REG, (unsigned long int) lp->TX_DESCS); 
	 
	 
	/* d) set filtering options at
	 *    MAC Register1 (Frame Filter)
	 *    MAC Register2 (Reserved - Hash Disabled)
	 *    MAC Register3 (Reserved - Hash Disabled)
	 *    MAC Register 16 & 17 (MAC Address 0)
	 */
     
	temp = DWORD_SHIFT(  0x0 , 31) | // [31]  Receive All
	       DWORD_SHIFT(  0x0 ,  9) | // [ 9]  Source Address Filter Enable
	       DWORD_SHIFT(  0x0 ,  8) | // [ 8]  SA Inverse Filtering
	       DWORD_SHIFT(  0x0 ,  6) | // [7:6] Pass Control Frames ( Forwards control frames that pass address filter)
	       DWORD_SHIFT(  0x0 ,  5) | // [ 5] Disable Broadcast Frames
	       DWORD_SHIFT(  0x1 ,  4) | // [ 4] Pass All Multicast
	       DWORD_SHIFT(  0x0 ,  3) | // [ 3] DA Inverse Filtering
                         0x0;         // [ 0] Promiscuous Mode
     
	SetDword(EMAC_MACR1_FRAME_FILTER_REG, temp);
	 
	SetDword(EMAC_MACR16_MAC_ADDR0_HIGH_REG, DWORD_SHIFT(mac_address[5], 8)| mac_address[4]);
	
	SetDword(EMAC_MACR17_MAC_ADDR0_LOW_REG, DWORD_SHIFT(mac_address[3],24) | 
                                            DWORD_SHIFT(mac_address[2],16) | 
	                                        DWORD_SHIFT(mac_address[1], 8) | mac_address[0]);
      
	/* e) configure and enable Tx/Rx operating modes
	 *    MAC Register0
	 *    (PS and DM bits are set based on the auto-negotion result read from the PHY)
	 */
	 
	temp = DWORD_SHIFT( 0x0 , 31) | // [31]	Transmit Configuration in RGMII/SGMII
	       DWORD_SHIFT( 0x1 , 23) | // [23]	Watchdog Disable
	       DWORD_SHIFT( 0x0 , 22) | // [22]	Jabber Disable
	       DWORD_SHIFT( 0x0 , 21) | // [21]	Frame Burst Enable
	       DWORD_SHIFT( 0x0 , 20) | // [20]	Jombo Frame Enable
	       DWORD_SHIFT( 0x0 , 17) | // [19:17] Inter Frame Gap ( 96 bit time)
	       DWORD_SHIFT( 0x0 , 16) | // [16]	Disable Carrier Sense During Transmission
	       DWORD_SHIFT( 0x1 , 15) | // [15]	Port Select ( 10/100Mbps)
#ifdef CONFIG_SC14452_ES2
	       DWORD_SHIFT( 0x0 , 14) | // [14]	Speed (100Mbps) //vm
#else
		   DWORD_SHIFT( 0x1 , 14) | // [14]	Speed (100Mbps) //vm
#endif
	       DWORD_SHIFT( 0x1 , 13) | // [13]	Disable Receive Own
	       DWORD_SHIFT( 0x0 , 12) | // [12]	Loop Back Mode
	       DWORD_SHIFT( 0x1 , 11) | // [11]	Duplex Mode
	       DWORD_SHIFT( 0x1 , 10) | // [10]	Checksum offload
	       DWORD_SHIFT( 0x0 ,  9) | // [ 9]	Disable Retry
	       DWORD_SHIFT( 0x1 ,  8) | // [ 8]	Link Up/Down
	       DWORD_SHIFT( 0x1 ,  7) | // [ 7]	Automatic Pad/CRC Stripping
	       DWORD_SHIFT( 0x0 ,  5) | // [6:5]	Back-Off Limit
	       DWORD_SHIFT( 0x0 ,  4) | // [ 4]	Deferral Check
	       DWORD_SHIFT( 0x1 ,  3) | // [ 3]	Transmitter Enable
	       DWORD_SHIFT( 0x1 ,  2) ; // [ 2]	Receiver Enable

	SetDword(EMAC_MACR0_CONFIG_REG, temp);
    
     
	/* Block MMC interrupts  and reset counters*/
	EMAC_MMC_INTR_MASK_RX_REG = 0xffffffff;
	EMAC_MMC_INTR_MASK_TX_REG = 0xffffffff;

	EMAC_MMC_CNTRL_REG = 0x5; 	//Reset at Read
     
	/* g) Start Tx/Rx 
	 *    Operation Mode
	 *    DMA Register6, bits 13 and 1
	 *    Note: if bit ST (13) = 1 then EthMAC polls the Tx descriptor
	 */
    
	temp = DWORD_SHIFT( 0x0 , 26) | // [26]	Disable Dropping of TCP/IP checksum Error Frames 
	       DWORD_SHIFT( 0x0 , 25) | // [25]	Receive Store and Forward 
	       DWORD_SHIFT( 0x0 , 24) | // [24]	Disable Flushing of Received Frames
	       DWORD_SHIFT( 0x0 , 21) | // [21]	Transmit Store and Forward
	       DWORD_SHIFT( 0x0 , 20) | // [20]	Flush Transmit FIFO
	       DWORD_SHIFT( 0x0 , 14) | // [16:14] Transmit Threshold Control (64 bytes)
#ifdef CONFIG_SC14452_ES2
		   DWORD_SHIFT( 0x0 , 13) | // [13]	Start/Stop Transmission Command //vm
#else
	       DWORD_SHIFT( 0x1 , 13) | // [13]	Start/Stop Transmission Command //vm
#endif
	       DWORD_SHIFT( 0x0 ,  7) | // [ 7]	Forward Error Frames
	       DWORD_SHIFT( 0x0 ,  6) | // [ 6]	Forward Undersized Good Frames
	       DWORD_SHIFT( 0x1 ,  3) | // [4:3]	Receive Threshold Control (32 bytes)
	       DWORD_SHIFT( 0x0 ,  2) | // [ 2]	Operate on Second Frame
#ifdef CONFIG_SC14452_ES2
	       DWORD_SHIFT( 0x0 ,  1) ; // [ 1]	Start/Stop Receive //vm
#else
		   DWORD_SHIFT( 0x1 ,  1) ; // [ 1]	Start/Stop Receive //vm
#endif   
	SetDword(EMAC_DMAR6_OPERATION_MODE_REG, temp);
}

#ifdef EMAC_TEST_MODE


/*-----------------------------------------------------------------------------------------*/
/* Reads the Length of the received frame and checks if the descriptor structure is valid. */
/* They don't make any change to the descriptors.                                          */
/*-----------------------------------------------------------------------------------------*/
long int emac_get_rx_len(void) 
{
	unsigned char i = 0;
	unsigned long int frame_buf_pos = 0;
	unsigned long int frame_len = 0;
	long int des0;
    
	while (i<emacDev.num_of_rx_des) {
	
		des0 = emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES0;

		// Check Own bit
		if (des0 & DWORD_SHIFT(0x1,31))
			return (-1);
    
		// Check First Descriptor bit
		if (i==0 && !(des0 & DWORD_SHIFT(0x1,9)))
			return (-1);
	
		// Check Last Descriptor bit
		if (des0 & DWORD_SHIFT(0x1,8)) {
		
			frame_len = (des0 >> 16) & 0x3fff;
			
			// Check Error Summary
			if (des0  & DWORD_SHIFT(0x1,15))
				return(-1);
			
			if ( (frame_len < frame_buf_pos) || 
			    ((frame_len - frame_buf_pos) > 2*emacDev.rx_buf_size))
				return (-1);

				
			return (frame_len);
		} else
			frame_buf_pos += 2*emacDev.rx_buf_size;

		i++;
	}
    return (-1);
}

/*----------------------------------------------------------------------------*/
/*  Copy the received data to a new buffer and release the Rx Descriptors     */
/*----------------------------------------------------------------------------*/
unsigned long int emac_get_rx_data(unsigned long int *frame_buf) 
{
	unsigned char i = 0;
    
	long int des0;
	unsigned long int frame_buf_pos = 0;
	unsigned long int frame_len = 0;
	unsigned int dma_len =0;
    
	while (i<emacDev.num_of_rx_des) {
    
		des0 = emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES0;
	
		// Set Own bit
		emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES0 = 0x80000000;
	
		// Check Last Descriptor bit
		if (des0 & DWORD_SHIFT(0x1,8)) {
		
			frame_len = (des0 >> 16) & 0x3fff;
			
			// Copy the data from buffer 1 to the frame buffer using the dma engine.      
			if ((frame_len - frame_buf_pos) >= emacDev.rx_buf_size)
				dma_len = emacDev.rx_buf_size;
			else
				dma_len = (frame_len - frame_buf_pos);	
			
			frame_buf_pos += dma_len;
			
			if (frame_len > frame_buf_pos) {
				// Copy the data from buffer 2 to the frame buffer using the dma engine. 
        		
				dma_len = (frame_len - frame_buf_pos);
        		
				frame_buf_pos += dma_len;
			}
        		
			emacDev.rx_ptr = (emacDev.rx_ptr + i + 1) % emacDev.num_of_rx_des;
			
			return (frame_len);
		} else {
			// Copy the data from buffer 1 to the frame buffer using the dma engine. 
        	
			dma_len = emacDev.rx_buf_size;
        	
			frame_buf_pos += dma_len;
        	
			frame_buf_pos += dma_len;
        	
			i++;
		}
	}

	return (frame_buf_pos);

}

/*----------------------------------------------------------------------------*/
/*            Calculates the number of the free tx descriptors                */
/*----------------------------------------------------------------------------*/
unsigned char emac_tx_des_calc (void) {
	unsigned char i; 
	
	for (i=0; i<emacDev.num_of_tx_des; i++)
		if (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES0 & 0x80000000)
			return (i);
	
	return (emacDev.num_of_tx_des);
}

/*----------------------------------------------------------------------------*/
/*            Calculates the total size of the free tx buffers                */
/*----------------------------------------------------------------------------*/
unsigned long int emac_tx_buf_calc (void) {
	return (2* emac_tx_des_calc() * emacDev.tx_buf_size);
}



/*----------------------------------------------------------------------------*/
/*            Write a Frame to the Tx Descriptors                             */
/*----------------------------------------------------------------------------*/
unsigned long int emac_send_tx_data(unsigned long int frame_size, unsigned long int *frame_buf)
{
	unsigned char i=0,k;
	unsigned long int frame_buf_pos = 0;
	unsigned int dma_len =0;

	while (i<emacDev.num_of_tx_des) {
		
		if (i==0)
			// This is the first Descriptor. Sets the First Segment bit and clears the Last Segment bit
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 | DWORD_SHIFT(0x1,29) ) & (~DWORD_SHIFT(0x1,30));
		else
			// Clear the First Segment bit and the Last Segment bit
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & ~(DWORD_SHIFT(0x1,29) | DWORD_SHIFT(0x1,30));		
		
		// Copy the data from frame buffer to the buffer 1 using the dma engine.
		if ((frame_size - frame_buf_pos) >= emacDev.tx_buf_size)
			dma_len = emacDev.tx_buf_size;
		else
			dma_len = frame_size - frame_buf_pos;
       	
		// Write the size of buffer 1
		emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & (~0x7ff)) | dma_len;
			
		frame_buf_pos += dma_len;
	
		// Copy the data from frame buffer to the buffer 2 using the dma engine if is necessary.
		if (frame_size > frame_buf_pos) {
       			if ((frame_size -  frame_buf_pos) >= emacDev.tx_buf_size)
				dma_len = emacDev.tx_buf_size;
			else
				dma_len = frame_size - frame_buf_pos;
			
			// Write the size of buffer 2
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & (~0x3ff800)) | DWORD_SHIFT (dma_len, 11);
				
			frame_buf_pos += dma_len;
		} else 
			// Clear the size of buffer 2
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & (~0x3ff800);
		
		if (frame_size == frame_buf_pos) {
			// Set the Last Segment bit to the Last descriptor
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 | DWORD_SHIFT(0x1,30);
	
			// -------------------- Set Own Bit ------------------------------------//
			for (k=0; k<=i; k++) {
				emacDev.tx_descriptor[(emacDev.tx_ptr + k) % emacDev.num_of_tx_des].DES0 = 0x80000000;
			}
			
			// -------------------- Update TxPtr Value -----------------------------//
			emacDev.tx_ptr = (emacDev.tx_ptr + i + 1) % emacDev.num_of_tx_des;	
			
			return (frame_buf_pos);
		}
		i++;
	}
	
	return (frame_buf_pos);
	
}


void emac_rxevent(void) {
	long int len;
	
	while ( (len = emac_get_rx_len()) > 0) {
		
		if (len>=RX_FRAME_SIZE) {
			printk("EMAC452: packet too big\n");
			return;
		}
		if (RxFrames >= MAX_RX_FRAMES) {
			return;
		}
//		RxFrameLen[RxFrames] = (int ) emac_get_rx_data((unsigned long int *) &RxFrameBuf[RxFrames * RX_FRAME_SIZE]);
		RxFrames++;		
	}
}

void emac_rxoverflow(void)
{
	printk("losing packets in rx\n");
	
	// Take all the good frames
	emac_rxevent();
	
	// Clear all the rx descriptors 
	emac_rx_des_prepare();
	
	// Force the emac to read again the rx descriptors
	emac_read_rx_des();
	
}


void emac_txevent(void) {
	TxEnd = 1;
}

void emac_poll(void) {
	long int status;
	
	while ((status = (GetDword (EMAC_DMAR5_STATUS_REG) & 0x1ffff)) !=0) {

		// Receive Overflow
		if (status & DWORD_SHIFT(0x1,4)) {
			emac_rxoverflow();
			SetDword (EMAC_DMAR5_STATUS_REG, DWORD_SHIFT(0x1,4));	
		}


		// End of frame transmission
		if (status & 0x1) {
			emac_txevent();
			SetDword (EMAC_DMAR5_STATUS_REG,0x1);
		}
	
		// End of frame reception
		if (status & DWORD_SHIFT(0x1,6)) {
			emac_rxevent();
			SetDword (EMAC_DMAR5_STATUS_REG, DWORD_SHIFT(0x1,6));
		}

		// Clear all other interrupts
		SetDword (EMAC_DMAR5_STATUS_REG, DWORD_SHIFT(0x1,16) |
						 DWORD_SHIFT(0x1,15) |
						 DWORD_SHIFT(0x1,14) | 
		                                 DWORD_SHIFT(0x1,13) |
						 DWORD_SHIFT(0x1,10) |
						 DWORD_SHIFT(0x1,9)  |
						 DWORD_SHIFT(0x1,8)  |
						 DWORD_SHIFT(0x1,7)  |
						 DWORD_SHIFT(0x1,5)  |
						 DWORD_SHIFT(0x1,3)  |
						 DWORD_SHIFT(0x1,2)  |
						 DWORD_SHIFT(0x1,1)   );
	}
	
}

void eth_halt()
{
}

int eth_rx()
{
	int j, tmo;

	PRINTK("### eth_rx\n");

	while(1) {
		emac_poll();
		if (RxFrames > 0) {
			PRINTK("RxFrames %02x \n",RxFrames);
			RxFrames = 0;
			return 1;
		}
	}
	return 0;
}


int eth_send(volatile void *packet, int length)
{
	int tmo;

	PRINTK("### eth_send\n");

	TxEnd = -1;

	emac_send_tx_data(length, (unsigned long int *) packet);
	
	emac_read_tx_des();
	
	while(1) {
		emac_poll();
		if (TxEnd != -1) {
			PRINTK("Packet sucesfully sent\n");
			return 0;
		}
		
	}
	return 0;
}
#endif //EMAC_TEST_MODE


int emac_init_speed(unsigned short speed)
{
	printk("[%s]\n", __FUNCTION__);
	emac_clk_disable();
	//#ifdef CONFIG_SC14452_ES2
		//emac_10mbps();
	//#else
		if (speed == 10)
			emac_10mbps();
		else
			emac_100mbps();
	//#endif

	// Enable EMAC clocks
	emac_clk_enable();

	// RTL8306 init
	tantos_init();

	// Software reset to read the PHY configuration
	emac_soft_rst();
}

int eth_init(struct net_device *dev)
{
	struct net_local *lp = dev->priv;

	printk("[%s]\n", __FUNCTION__);

	// allocate memory for the rx and tx descriptors
#ifndef CONFIG_SC14452_ES2
	lp->TX_DESCS = TX_DES_BASE;
	lp->RX_DESCS = RX_DES_BASE;
#endif
		
	// Init the descriptors structures
	emac_init_tx_desc (lp->TX_DESCS);
	emac_init_rx_desc (lp->RX_DESCS);

	// Configure the pads for MII interface
	emac_mii_pads();

	// Configure the clock for MII interface and external clock
	emac_mii_clk_ext();

	// Configure the pads for MDC interface
	emac_md_pads();

	emac_clk_disable();

	// Set 100mbps operation
	emac_100mbps();

	// Enable EMAC clocks
	emac_clk_enable();
		
	// init tantos switch
	tantos_init();

	// Software reset to read the PHY configuration
	emac_soft_rst();
}
