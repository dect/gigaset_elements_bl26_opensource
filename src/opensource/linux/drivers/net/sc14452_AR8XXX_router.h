// File created by Gigaset Elements GmbH
// All rights reserved.
/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 *
 * RHEA SOFTWARE PLATFORM
 * SC14452 EMAC LINUX DRIVER
 * File sc14452_AR8XXX_router.h
 */

#ifndef SC14452_ATHEROS_ROUTER_H
#define SC14452_ATHEROS_ROUTER_H

#include <linux/if_ether.h>
#include <linux/ioport.h>
#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <asm-cr16/regs.h>

void init_AR8XXX(unsigned short rate);
void init_AR8XXX_Switch(unsigned short rate);
unsigned int ar8xxx_port_status(int port);
void init_AR8XXX_ACL_Rules(void);
void disable_AR8XXX_PAUSE(void);
void enable_AR8XXX_PAUSE(void);
unsigned int read_ar8xxx(unsigned int reg_address);
void write_ar8xxx(unsigned int reg_address, unsigned int reg_data);
void set_ar8xxx(unsigned int reg_address, unsigned int reg_data, int bit);
void unset_ar8xxx(unsigned int reg_address, unsigned int reg_data, int bit);
void AR8XXX_storm_cntrl(int enable);
void AR8XXX_WAN_port_ingress_rate(int enable);
void AR8XXX_PC_port_ingress_rate(int enable);
void AR8XXX_LLDP_noVLAN(void);
void AR8XXX_block_8021x_atPC(int enable);
int AR8XXX_get_first_mac_PCport(unsigned int *mac);

#define MAX_RTP_AUDIO_PORTS		2
#define ACL_LLDP_noVLAN_POS_RX	MAX_RTP_AUDIO_PORTS + 1
#define ACL_LLDP_noVLAN_POS_TX	ACL_LLDP_noVLAN_POS_RX + 1
#define ACL_8021X_noPC_POS		ACL_LLDP_noVLAN_POS_TX + 1

//static const char *version = "sc14452_AR8XXX_router.c: v1.0 Sitel Semiconductor\n";

#define MAX_PACKET_LEN 0x700

/* The OS name of the card. Is used for messages and in the requests for*/
/* io regions, irqs and dma channels                                    */
static const char* cardname = "SC14452_EMAC";

/*----------------------------*/
/*  IO configurations         */
/*----------------------------*/
#define PORT_INPUT          0
#define PORT_PULL_UP        1
#define PORT_PULL_DOWN      2
#define PORT_OUTPUT         3

#define STATE_OFF						0
#define	STATE_ON						1

#define MODE_ROUTER					0
#define MODE_SWITCH					1
#define MODE_UNDEFINED			0xff

#define MAX_RX_DESCS				64
#define MAX_TX_DESCS				128

/* Place RX and TX descriptors in internal memory to avoid cache coherency issues */
#define TX_DES_BASE					0x0020000
#define RX_DES_BASE					0x0021000

#ifdef CONFIG_SC14452_NETLOAD_PROTECTION

#define ULP_THRESHOLD						500	// Default values for unicast frames per second threshold
#define BLP_THRESHOLD						100	// Default values for broadcast frames per second threshold

/* EMAC_LOADFILTER Message type definitions */
#define EMAC_LF_STATUS          0
#define EMAC_LF_ADD             1
#define EMAC_LF_REMOVE          2
#define EMAC_LF_REMOVE_ALL      3
#define EMAC_LF_START_ALL       4
#define EMAC_LF_START_BCAST     5
#define EMAC_LF_START_UCAST     6
#define EMAC_LF_STOP_ALL        7
#define EMAC_LF_STOP_BCAST      8
#define EMAC_LF_STOP_UCAST      9

#define EMAC_LF_COMMAND_SUCCESS					100
#define EMAC_LF_COMMAND_FAIL					200
#define EMAC_LF_COMMAND_FAIL_BCAST      201
#define EMAC_LF_COMMAND_FAIL_FULL       202
#define EMAC_LF_COMMAND_FAIL_NFOUND     203

struct addr_filter 
{
	unsigned short enable;
	unsigned short pid;
	unsigned char MAC[6];
}; 	

struct addr_filter_status 
{
	unsigned short ulp_enable;							// Unicast load protection enable
	unsigned short blp_enable;							// Broadcast load protection Enable
	unsigned short ulp_threshold;						// Unicast load threshold
	unsigned short blp_threshold;						// Broadcast load threshold
	unsigned long ups; 											// Unicast packets per second
	unsigned long bps;											// Broadcast packets per second
	unsigned short blp_filtered;						//
	unsigned short ulp_filtered;						//
	unsigned short blp_protect;							// Indicates whether broadcast traffic protection is activated
	unsigned short ulp_protect;							// Indicates whether Unicast traffic protection is activated
	struct addr_filter address_filters[4];	// Filter addresses and enable flags
};

struct addr_filter_ioctl 
{
	unsigned short command;
	unsigned short pid;
	struct addr_filter_status status;
}; 
#endif

/***
 *** Tx/Rx descriptors
 ***/
struct s_descriptor 
{
   volatile unsigned long int DES0;
   volatile unsigned long int DES1;
   volatile unsigned long int DES2;
   volatile unsigned long int DES3;   
};


struct net_local 
{
	struct net_device * devices[2];
	struct net_device_stats stats[2];					/* uClinux ethernet stats struct */
	int state;											/* State of driver */
	int mode;
	unsigned char Link[2];								/* Link Indication */
	unsigned char port_state[2];						/* state of each one of the two devices, i.e. UP or DOWN */
	int FullDuplex;										/* Full Duplex link */
	int LinkSpeed100Mbps;								/* Link Speed Indication */
	struct timer_list emac_timer;						/* link detection timer */
#ifdef CONFIG_SC14452_ES2
	struct s_descriptor RX_DESCS[MAX_RX_DESCS];  
#else
	struct s_descriptor *RX_DESCS;  
#endif
	struct sk_buff *RX_RING[MAX_RX_DESCS];
	char *RX_BUFFERS;
	int CUR_RX_DESC;
#ifdef CONFIG_SC14452_ES2
	struct s_descriptor  TX_DESCS[MAX_TX_DESCS];  
#else
	struct s_descriptor *TX_DESCS;  
#endif
	struct sk_buff *TX_RING[MAX_TX_DESCS];
	int tx_ring_size;
	int TX_RING_HEAD;
	int TX_RING_TAIL;
	int INT_STATS[14];
	struct tasklet_struct emac_rx_tasklet;
	struct tasklet_struct emac_tx_tasklet;
	int Frame_end;
	int Frame_start;
	int tx_disabled;
	unsigned short max_tx_packets;
	spinlock_t lock;
#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
	spinlock_t filter_lock;
	struct addr_filter_status filter_config;
	unsigned long tmp_ups;
	unsigned long tmp_bps;
#endif
	unsigned short id;
};


#ifdef  DEBUG
#define PRINTK(args...) printk(args)
#else
#define PRINTK(args...)
#endif

//-----------------------------------------------------------------------
//              Mathematics
//-----------------------------------------------------------------------
#define DWORD_SHIFT(DAT,BITS) ((unsigned long int) (DAT) << (BITS))
#define NUM_OF_DWORDS(x) (((x % 4) == 0) ? (x >> 2) : ((x >>2 ) + 1))

//-----------------------------------------------------------------------
//              Definitions for memory allocation
//-----------------------------------------------------------------------
#define TX_BUF_GAP  0						// space between two sequential tx buffers (bytes)
#define TX_BUF_SIZE MAX_PACKET_LEN			// the size of one tx buffer (bytes)

#define RX_BUF_GAP  0						// space between two sequential rx buffers (bytes)
#define RX_BUF_SIZE MAX_PACKET_LEN			// the size of one rx buffer (bytes)

//-----------------------------------------------------------------------
//              Definitions for the SMI interface (MDC, MDIO)
//-----------------------------------------------------------------------
#define MD_PHY_ADDR(x) DWORD_SHIFT((x & 0x1f),11)
#define MD_REG_ADDR(x) DWORD_SHIFT((x & 0x1f),6)
#define MD_CLK(x)      DWORD_SHIFT((x & 0x7),2)
#define MD_WRITE       DWORD_SHIFT(1,1)
#define MD_BUSY        0x1

#define EMAC_MDC_DIV_42  0x0
#define EMAC_MDC_DIV_62  0x1
#define EMAC_MDC_DIV_16  0x2
#define EMAC_MDC_DIV_26  0x3
#define EMAC_MDC_DIV_102 0x4
#define EMAC_MDC_DIV_122 0x5

//-----------------------------------------------------------------------
//              Emac peripheral ID
//-----------------------------------------------------------------------
#define PID_EMAC_PIN  57

//-----------------------------------------------------------------------
//  The dma channel used in order to accelerate the process of memory copy.
//-----------------------------------------------------------------------
#define EMAC_USE_DMA_CHANNEL 0

//-----------------------------------------------------------------------
//              Structures and variables related with the emac.
//-----------------------------------------------------------------------
char TxEnd;

#define MAX_RX_FRAMES 4
#define RX_FRAME_SIZE 2000					// Multiple of 4 only!!!!

#ifdef EMAC_TEST_MODE
unsigned char *RxFrameBuf = NULL;
char RxFrames = 0;
#endif

/*----------------------------------------------------------------------------*/
/*                               Clock Enable                                 */
/*----------------------------------------------------------------------------*/
void emac_clk_enable(void) 
{
	SetBits(CLK_AUX2_REG, SW_EMAC_EN, 1);	
}

/*----------------------------------------------------------------------------*/
/*                               Clock Disable                                */
/*----------------------------------------------------------------------------*/
void emac_clk_disable(void) 
{
	SetBits(CLK_AUX2_REG, SW_EMAC_EN, 0);
}

/*----------------------------------------------------------------------------*/
/*                   Sets the interface speed to 10Mpbs                       */
/*----------------------------------------------------------------------------*/
void emac_10mbps(void) 
{
	SetBits(CLK_AUX2_REG, SW_ETH_SEL_SPEED, 0);
}

/*----------------------------------------------------------------------------*/
/*                   Sets the interface speed to 100Mpbs                      */
/*----------------------------------------------------------------------------*/
void emac_100mbps(void) 
{
	SetBits(CLK_AUX2_REG, SW_ETH_SEL_SPEED, 1);
}

/*----------------------------------------------------------------------------*/
/*                           Init PADs for RMII mode                          */
/*----------------------------------------------------------------------------*/
void emac_rmii_pads (void)
{
	SetPort(P3_02_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // port3(2) -> emac ref_clk
	SetPort(P3_03_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxEn output
	SetPort(P3_07_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxD[0] output
	SetPort(P3_08_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxD[1] output
	SetPort(P3_04_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxDV input
	SetPort(P3_05_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxD[0] input
	SetPort(P3_06_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxD[1] input
}

/*----------------------------------------------------------------------------*/
/*                           Init PADs for MII mode                          */
/*----------------------------------------------------------------------------*/
void emac_mii_pads (void)
{
	SetPort(P0_04_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // TXCLK
	SetPort(P0_05_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RXCLK

	SetPort(P3_03_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxEn output
	SetPort(P3_07_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxD[0] output
	SetPort(P3_08_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxD[1] output
	SetPort(P0_07_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxD[2] output
	SetPort(P0_08_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);   // TxD[3] output

	SetPort(P3_04_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxDV input
	SetPort(P3_05_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxD[0] input
	SetPort(P3_06_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxD[1] input
	SetPort(P0_09_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxD[2] input
	SetPort(P0_10_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RxD[3] input
}

/*----------------------------------------------------------------------------*/
/*                           Init MD PADs                                     */
/*----------------------------------------------------------------------------*/
void emac_md_pads(void)
{
	SetPort(P3_00_MODE_REG, GPIO_PUPD_OUT_NONE, PID_EMAC_PIN);    // MDC output
	SetPort(P3_01_MODE_REG, GPIO_PUPD_IN_NONE,  PID_EMAC_PIN);    // MDIO
}

/*----------------------------------------------------------------------------*/
/*                   Set RMII mode and Internal Clocks                        */
/*----------------------------------------------------------------------------*/
void emac_rmii_clk_int(void)
{
	SetBits(CLK_AUX2_REG, SW_RMII_EN, 1);				// Set RMII Mode
	SetBits(CLK_AUX2_REG, SW_RMII_CLK_INT, 1);	// Internal source for RMII ref clock
	SetBits(GPRG_R0_REG, 0x0100, 0);						// REF_CLK output
}

/*----------------------------------------------------------------------------*/
/*                   Set RMII mode and External Clocks                        */
/*----------------------------------------------------------------------------*/
void emac_rmii_clk_ext (void)
{
	SetBits(CLK_AUX2_REG, SW_RMII_EN,1);        // Set RMII Mode
	SetBits(GPRG_R0_REG, 0x0100, 1);            // REF_CLK input
	SetBits(CLK_AUX2_REG, SW_RMII_CLK_INT, 0);  // External source for RMII ref clock
}

/*----------------------------------------------------------------------------*/
/*                   Set MII mode and External  Clocks                        */
/*----------------------------------------------------------------------------*/
void emac_mii_clk_ext(void)
{
	SetBits(CLK_AUX2_REG, SW_RMII_EN,0);                  // Set MII Mode
	SetPort(P0_04_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // TXCLK input
	SetPort(P0_05_MODE_REG, GPIO_PUPD_IN_NONE, PID_EMAC_PIN);    // RXCLK input
}

/*----------------------------------------------------------------------------*/
/*                          MD Interface - Read Function                      */
/*----------------------------------------------------------------------------*/
unsigned int emac_md_read(unsigned char phy_address, unsigned char reg_address, unsigned char mdc_div)
{
	// Check the BUSY bit before write to the EMAC_MACR4_MII_ADDR_REG
	while (GetDword(EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);

	// Write the read request
	SetDword(EMAC_MACR4_MII_ADDR_REG, MD_PHY_ADDR(phy_address) |
	                                  MD_REG_ADDR(reg_address) |
									MD_CLK(mdc_div) |
																		MD_BUSY);
		
	// Wait Until to receive the requested data
	while (GetDword(EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);
		
	return (GetDword(EMAC_MACR5_MII_DATA_REG));
}

/*----------------------------------------------------------------------------*/
/*                         MD Interface - Write Function                      */
/*----------------------------------------------------------------------------*/
void emac_md_write(unsigned char phy_address, unsigned char reg_address, unsigned char mdc_div, unsigned int wdata)
{
	// Check the BUSY bit before write to the EMAC_MACR4_MII_ADDR_REG & EMAC_MACR5_MII_ADDR_REG
	while (GetDword(EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);
	
	SetDword(EMAC_MACR5_MII_DATA_REG, wdata);

	SetDword(EMAC_MACR4_MII_ADDR_REG, MD_PHY_ADDR(phy_address)	| 
	                                  MD_REG_ADDR(reg_address) |
	                                  MD_CLK(mdc_div) |
	                                  MD_WRITE  |
	                                  MD_BUSY);

	// Wait Until  the end of write operation
	while (GetDword(EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);
}

unsigned int read_ar8xxx(unsigned int reg_address)
{
	unsigned int temp;
	unsigned int ret;

	// First access on AR8XXX
	emac_md_write (0x18, 0x0, EMAC_MDC_DIV_42, (reg_address>>9)); // write the first word where the High Address is located on the data field

	// Second access on AR8XXX
	temp = (reg_address & 0x1FC)>>1;
	temp = temp | 0x200;
	ret = emac_md_read ((temp>>5), (temp&0x1F), EMAC_MDC_DIV_42);

	// Third access on AR8XXX
	temp = (reg_address & 0x1FE)>>1;
	temp = temp | 0x201;
	ret |= (emac_md_read ((temp>>5), (temp&0x1F), EMAC_MDC_DIV_42) << 16);

	return ret;
}

void write_ar8xxx(unsigned int reg_address, unsigned int reg_data)
{	
	unsigned int temp;

	// First access on AR8XXX
	emac_md_write (0x18, 0x0, EMAC_MDC_DIV_42, (reg_address>>9));     // write the first word where the High Address is located on the data field

#ifdef CONFIG_SC14452_ATHEROS_AR8325
	// Second access on AR8XXX
	temp = (reg_address & 0x1FE)>>1;
	temp = temp | 0x200;
	emac_md_write ((temp>>5), (temp&0x1F), EMAC_MDC_DIV_42, (reg_data & 0xFFFF));

	// Third access on AR8XXX
	temp = (reg_address & 0x1FE)>>1;
	temp = temp | 0x201;
	emac_md_write ((temp>>5), (temp&0x1F), EMAC_MDC_DIV_42, (reg_data >> 16));
#else
	// third access on AR8XXX
	temp = (reg_address & 0x1FE)>>1;
	temp = temp | 0x201;
	emac_md_write ((temp>>5), (temp&0x1F), EMAC_MDC_DIV_42, (reg_data>>16));

	//Second access on AR8XXX
	temp = (reg_address & 0x1FC)>>1;
	temp = temp | 0x200;
	emac_md_write ((temp>>5), (temp&0x1F), EMAC_MDC_DIV_42, (reg_data & 0xFFFF));
#endif
}

// read modify set ar8xxx function
void set_ar8xxx(unsigned int reg_address, unsigned int reg_data, int bit)
{	
	unsigned int temp;

	if (!reg_data) return;

	temp = read_ar8xxx(reg_address);
	temp |= (reg_data<<bit);
	write_ar8xxx(reg_address, temp);
}

// read modify unset ar8xxx function
void unset_ar8xxx(unsigned int reg_address, unsigned int reg_data, int bit)
{	
	unsigned int temp;

	if (!reg_data) return;

	temp = read_ar8xxx(reg_address);
	temp &= ~(reg_data<<bit);
	write_ar8xxx(reg_address, temp);
}

unsigned int ar8xxx_port_status(int port)
{
#ifdef CONFIG_SC14452_ATHEROS_AR8314 
  if (port == 0)
		return ((0x100 & read_ar8xxx(0x400)) >> 8);
  else
    return ((0x100 & read_ar8xxx(0x300)) >> 8);
#elif defined CONFIG_SC14452_ATHEROS_AR8236
	if (port == 0)
		return ((0x100 & read_ar8xxx(0x500)) >> 8);
  else
    return ((0x100 & read_ar8xxx(0x300)) >> 8);
#elif defined CONFIG_SC14452_ATHEROS_AR8325
	if (port == 0)
		return ((0x100 & read_ar8xxx(0x88)) >> 8);
  else
    return ((0x100 & read_ar8xxx(0x84)) >> 8);
#endif
}

unsigned int ar8xxx_port_get_speed(int port)
{
#ifdef CONFIG_SC14452_ATHEROS_AR8314
	if(port == 0)
	 	return (0x0003 & read_ar8xxx(0x400));
	else
	 	return (0x0003 & read_ar8xxx(0x300));
#elif defined CONFIG_SC14452_ATHEROS_AR8236
	if(port == 0)
	 	return (0x0003 & read_ar8xxx(0x500));
	else
	 	return (0x0003 & read_ar8xxx(0x300));
#elif defined CONFIG_SC14452_ATHEROS_AR8325
	if(port == 0)
	 	return (0x0003 & read_ar8xxx(0x88));
	else
	 	return (0x0003 & read_ar8xxx(0x84));
#endif
}


/*------------------------------------------------------------------------------*/
/*			Initialize the external switch										*/
/*			in Switch Mode														*/
/*------------------------------------------------------------------------------*/
void init_AR8XXX_Switch(unsigned short rate)
{
	unsigned int temp = 0;
#ifndef CONFIG_SC14452_ATHEROS_AR8325 // AR8314 & AR8236
	if (rate == 10)
		write_ar8xxx(0x100, 0x007c);
  else
		write_ar8xxx(0x100, 0x007d);

	// Enable MAC0. Disable MAC5
	write_ar8xxx(0x8, 0x01061b61);

	// bit 26 = 1. Enable broadcast to CPU
	temp = read_ar8xxx(0x2c);
	temp |= (1<<26);
	write_ar8xxx(0x2c, temp);

#else // CONFIG_SC14452_ATHEROS_AR8325
	printk("[%s] \n", __FUNCTION__);
	// PORT0_STATUS
	unset_ar8xxx(0x7C, 0x1fff, 0);
	if (rate == 10)
		set_ar8xxx(0x7C, 0x4c, 0);			// 10M
	else
		set_ar8xxx(0x7C, 0x4d, 0);			// 100M

	// GLOBAL_FW_CTRL1
	write_ar8xxx(0x624, 0x7f7f7f);		// send unknown packets to all ports
	write_ar8xxx(0x4, 0x700);					// mac0 connected to cpu through MII interface, phy mode 
																		// select invert clock output for port0 phymode, MII interface txclk, rxclk
  write_ar8xxx(0x660, 0x0014000c);	// PORT_VID_MEM_0 members ports 2 & 3 only.
	write_ar8xxx(0x678, 0x00140009);	// PORT_VID_MEM_2 members ports 0 & 3 only.
	write_ar8xxx(0x684, 0x00140005);	// PORT_VID_MEM_3 members ports 0 & 2 only.

#endif

#ifdef CONFIG_SC14452_ATHEROS_AR8314
	// Set WAN port to normal mode. It was disabled at bootloader
	temp = read_ar8xxx(0x404);
	temp &= 0xfffffff8;
	temp |= 4;
	write_ar8xxx(0x404, temp);

#elif defined CONFIG_SC14452_ATHEROS_AR8325
	// PORT_STATE_3 = 100 forward mode. Set WAN port to normal mode. It was disabled at bootloader
	unset_ar8xxx(0x0684, 0x7, 16);
	set_ar8xxx(0x0684, 0x4, 16);

#elif defined CONFIG_SC14452_ATHEROS_AR8236

	emac_md_write (0x14, 0x1, EMAC_MDC_DIV_42, 0x0000);
	emac_md_write (0x10, 0x2, EMAC_MDC_DIV_42, 0x0000);
	emac_md_write (0x10, 0x3, EMAC_MDC_DIV_42, 0x000e);
	emac_md_write (0x10, 0x16, EMAC_MDC_DIV_42, 0x003f);
	emac_md_write (0x10, 0x17, EMAC_MDC_DIV_42, 0x7e3f);
	
	// Set WAN port to normal mode. It was disabled at bootloader
	temp = read_ar8xxx(0x504);
	temp &= 0xfffffff8;
	temp |= 4;
	write_ar8xxx(0x504, temp);
#endif

#ifndef CONFIG_SC14452_ATHEROS_AR8325 // AR8314 & AR8236
	// Enable CPU port
	write_ar8xxx(0x78, 0x1f0);

	// Set PC port to normal mode. It was disabled at bootloader
	temp = read_ar8xxx(0x304);
	temp &= 0xfffffff8;
	temp |= 4;
	write_ar8xxx(0x304, temp);

	// Enable Atheros Header
	temp = read_ar8xxx(0x104);
	temp |= 1<<11;
	write_ar8xxx(0x104, temp);

#else // CONFIG_SC14452_ATHEROS_AR8325
	// GLOBAL_FW_CTRL1
	write_ar8xxx(0x624, 0x7f7f7f);		// sent unknown packets to all ports
	
	// GLOBAL_FW_CTRL0
	set_ar8xxx(0x620, 0x1, 10);				// CPU_PORT_EN=1

	// PORT2_LOOKUP_CTRL
	unset_ar8xxx(0x0678, 0x7, 16);		
	set_ar8xxx(0x0678, 0x4, 16);			// Normal forward port state

	// ATHEROS HEADER ENABLE
	// PORT0_HEADER_CTRL	
	write_ar8xxx(0x9C, 0xa);					// All frames with Atheros header

#endif
}


/*------------------------------------------------------------------------------*/
/*        Initialize the external switch										*/
/*			 	in Router Mode													*/
/*------------------------------------------------------------------------------*/
// Supported for AR8314 and AR8325
void init_AR8XXX(unsigned short rate)
{
	unsigned int temp = 0;

#ifdef CONFIG_SC14452_ATHEROS_AR8314
  if (rate == 10)
		write_ar8xxx(0x100, 0x007c);
  else
		write_ar8xxx(0x100, 0x007d);

	//Enable MAC0. Disable MAC5
	write_ar8xxx(0x8, 0x01061b61);

	// bit 26 = 1. Enable broadcast to CPU
	temp = read_ar8xxx(0x2c);
	temp |= (1<<26);
	write_ar8xxx(0x2c, temp);

	// Enable CPU port
	write_ar8xxx(0x78, 0x1f0);

	// Enable Atheros Header
	temp = read_ar8xxx(0x104);
	temp |= 1<<11;
	write_ar8xxx(0x104, temp);

	// Set PC port to normal mode. It was disabled at bootloader
	temp = read_ar8xxx(0x304);
	temp &= 0xfffffff8;
	temp |= 4;
	write_ar8xxx(0x304, temp);

	// Set WAN port to normal mode. It was disabled at bootloader
	temp = read_ar8xxx(0x404);
	temp &= 0xfffffff8;
	temp |= 4;
	write_ar8xxx(0x404, temp);

	// ROUTER SPECIFIC FUNCTION
	// Set MAC0 to 802.1Q mode, 
	write_ar8xxx(0x108, 0x003e0001);
	// Set MAC3 PVID=1, 
	write_ar8xxx(0x308, 0x00010001);
	// Set MAC4 PVID=2, 
	write_ar8xxx(0x408, 0x00010001);

	init_AR8XXX_ACL_Rules();

#elif defined CONFIG_SC14452_ATHEROS_AR8325
	printk("[%s] \n", __FUNCTION__);	
	// set speed and disable flow control
	unset_ar8xxx(0x7C, 0x1fff, 0);
	if (rate == 10)
		set_ar8xxx(0x7C, 0x4c, 0);			// 10M
	else
		set_ar8xxx(0x7C, 0x4d, 0);			// 100M

	// GLOBAL_FW_CTRL1
	write_ar8xxx(0x624, 0x7f7f7f);		// send unknown packets to all ports
	write_ar8xxx(0x4, 0x700);					// mac0 connected to cpu through MII interface, phy mode 
																		// select invert clock output for port0 phymode, MII interface txclk, rxclk
	// VTU_FUNC_REG1
	write_ar8xxx(0x0614, 0x80000009);	// Flash all previous VLAN entries
	
	// set port membership and enable PC port3.
	// PORT_STATE_X = 100 forward mode. Set all ports to normal mode. PC port3 was disabled at bootloader
	write_ar8xxx(0x660, 0x0014010c);	// PORT_VID_MEM_0 members ports 2 & 3 only. 
	write_ar8xxx(0x678, 0x00140101);	// PORT_VID_MEM_2 members port 0 only.
	write_ar8xxx(0x684, 0x00140101);	// PORT_VID_MEM_3 members port 0 only.

	// GLOBAL_FW_CTRL1
	write_ar8xxx(0x624, 0x7f7f7f);		// send unknown packets to all ports
	
	// GLOBAL_FW_CTRL0
	set_ar8xxx(0x620, 0x1, 10);				// CPU_PORT_EN=1

	// Aging at 14 seconds. Not needed anymore since we flush arl at link down
	//unset_ar8xxx(0x618, 0xFF, 0);
	//set_ar8xxx(0x618, 0x2, 0);	

	// ATHEROS HEADER ENABLE
	// PORT0_HEADER_CTRL
	write_ar8xxx(0x9C, 0xa);					// All frames with Atheros header

	init_AR8XXX_ACL_Rules();

	AR8XXX_LLDP_noVLAN();

#else

  if (rate == 10)
		write_ar8xxx(0x100, 0x007c);
  else
		write_ar8xxx(0x100, 0x007d);

	// Enable MAC0. Disable MAC5
	// write_ar8xxx(0x8, 0x01061b61);

	// bit 26 = 1. Enable broadcast to CPU
	temp = read_ar8xxx(0x2c);
	temp |= (1<<26);
	write_ar8xxx(0x2c, temp);

	// Enable CPU port
	write_ar8xxx(0x78, 0x1f0);

	// Enable Atheros Header
	temp = read_ar8xxx(0x104);
	temp |= 1<<11;
	write_ar8xxx(0x104, temp);

	// Set PC port to normal mode. It was disabled at bootloader
	temp = read_ar8xxx(0x304);
	temp &= 0xfffffff8;
	temp |= 4;
	write_ar8xxx(0x304, temp);

	// Set WAN port to normal mode. It was disabled at bootloader
	temp = read_ar8xxx(0x404);
	temp &= 0xfffffff8;
	temp |= 4;
	write_ar8xxx(0x404, temp);

	// ROUTER SPECIFIC FUNCTION
	// Set MAC0 to 802.1Q mode, 
	write_ar8xxx(0x108, 0x003e0001);
	// Set MAC3 PVID=1, 
	write_ar8xxx(0x308, 0x00010001);
	// Set MAC4 PVID=2, 
	write_ar8xxx(0x408, 0x00010001);

	init_AR8XXX_ACL_Rules();
#endif
}


void init_AR8XXX_ACL_Rules(void)
{
	int i = 0;

#ifdef CONFIG_SC14452_ATHEROS_AR8314
	write_ar8xxx(0x3c, 0xc000000e);			// Disable ACL

	for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) {
		// Rule Control
		write_ar8xxx(0x58824 + i*0x20, 1+i);					// Addr 0
		write_ar8xxx(0x58828 + i*0x20, 0);						// Addr 1
		write_ar8xxx(0x5882c + i*0x20, 0);						// Addr 2
		write_ar8xxx(0x58830 + i*0x20, 0);						// Addr 3
		write_ar8xxx(0x58820 + i*0x20, 1);						// Addr valid
		write_ar8xxx(0x58838 + i*0x20, 0);						// Rule Length

		// Rule table
		write_ar8xxx(0x58420 + i*0x20, 0x0);  //0-3
		write_ar8xxx(0x58424 + i*0x20, 0x0);  //4-7
		write_ar8xxx(0x58428 + i*0x20, 0x0);	//8-11			// UDP destination port to prioritize. Init to 0
		write_ar8xxx(0x5842c + i*0x20, 0x0);	//12-15
		write_ar8xxx(0x58430 + i*0x20, 0xf);	//16				// Set rule at port 3, WAN port, bit0=P0, bit1=P1,��
		// Mask table
		write_ar8xxx(0x58c20 + i*0x20, 0x0);	//0-3
		write_ar8xxx(0x58c24 + i*0x20, 0x0);	//4-7
		write_ar8xxx(0x58c28 + i*0x20, 0xffff00ff); //8-11		// mask of udp dest port and IP protocol
		write_ar8xxx(0x58c2c + i*0x20, 0x00030000);	//12-15		// use mask or range. This is mask 
		// Rule type select
		write_ar8xxx(0x5883c + i*0x20, 0x00000002);		// Select IPv4 rule				
		// Rule result
		write_ar8xxx(0x58020 + i*0x20, 0x0008c000);		// Priority Queue enable and set to max 
	}
#elif defined CONFIG_SC14452_ATHEROS_AR8325
	unset_ar8xxx(0x30, 1, 1);						// Disable ACL
	unset_ar8xxx(0x8AC, 1, 1);						// Disable egress limits 

	for (i=0; i<MAX_RTP_AUDIO_PORTS; i++) {
		// IPv4 Pattern. See Table 2.9 page 32 Atheros 8325 datasheet
		write_ar8xxx(0x404, 0);						// ACL byte [3:0]
		write_ar8xxx(0x408, 0);						// ACL byte [7:4]
		write_ar8xxx(0x40C, 0x7e40011);				// ACL byte [11:8]. Set UDP protocol and dest port init at 00h
		write_ar8xxx(0x410, 0x00);					// ACL byte [15:12]. 
		write_ar8xxx(0x414, 0xf);					// ACL byte [16]. Set source port as WAN port3, PC port2 and CPU port0
		write_ar8xxx(0x400, 0x80000001 + i);		// ACL_Busy, Write command, Rule and Index to ind=(1+i)

		// IPv4 Mask. See Table 2.10 page 33 Atheros 8325 datasheet
		write_ar8xxx(0x404, 0);						// ACL byte [3:0]
		write_ar8xxx(0x408, 0);						// ACL byte [7:4]
		write_ar8xxx(0x40C, 0xffff00ff);			// ACL byte [11:8]. Set IP protocol mask and dest port mask
		write_ar8xxx(0x410, 0x00030000);	 		// ACL byte [15:12]. Use mask or range. This is mask
		write_ar8xxx(0x414, 0x000000c2);	 		// ACL byte [16]. Start & end (only ACL rule, so it is start and end as well), IPv4 rule type
		write_ar8xxx(0x400, 0x80000101 + i);		// ACL_Busy, Write command, Mask and Index to ind=(1+i)

		// IPv4 Result. See Table 2.10 page 33 Atheros 8325 datasheet
		write_ar8xxx(0x404, 0);						// ACL byte [3:0]
		write_ar8xxx(0x408, 0x1c000000);			// ACL byte [7:4]. Map result to Queue 3 bits 56-63
		write_ar8xxx(0x40C, 0);						// ACL byte [11:8].
		write_ar8xxx(0x410, 0);	 					// ACL byte [15:12].
		write_ar8xxx(0x414, 0);	 					// ACL byte [16]. 
		write_ar8xxx(0x400, 0x80000201 + i);		// ACL_Busy, Write command, Result and Index to ind=(1+i)
	}
#endif
}


/* Returns the last MAC address found at PC port 2 */
int AR8XXX_get_first_mac_PCport(unsigned int *mac)
{

#if ((defined CONFIG_SC14452_ATHEROS_AR8325) || (defined CONFIG_SC14452_ATHEROS_AR8314))
	int i = 1, k = 0, l = 0;
	unsigned int tmp1 = 0x0, tmp2 = 0x0, tmp3 = 0x0, test = 0;
	unsigned int tmp_mac[6];
	unsigned int pc_mac[10][6];
	unsigned int pc_reg[10];

#ifdef CONFIG_SC14452_ATHEROS_AR8325
	// WAN port3, PC port2
	write_ar8xxx(0x600, 0x0);
	write_ar8xxx(0x604, 0x0);
	write_ar8xxx(0x608, 0x0);
	write_ar8xxx(0x60C, 0x80000006);					// AT_BUSY: Start and AT operation (cleared by HW). 
														// Get next valid MAC ARL entry
#elif (defined CONFIG_SC14452_ATHEROS_AR8314)
	//write_ar8xxx(0x58, 0x0);
	// complete AT operation
	// WAN port3, PC port2
	//write_ar8xxx(0x50, 0xe);							// AT_BUSY: Start and AT operation (cleared by HW). 
														// Get next valid MAC ARL entry
#endif
	
	memset(tmp_mac, 0, sizeof(tmp_mac));
	memset(mac, 0, sizeof(tmp_mac));
	memset(pc_mac, 0, sizeof(pc_mac));
	memset(pc_reg, 0, sizeof(pc_reg));

	while(i<1000)
	{
#ifdef CONFIG_SC14452_ATHEROS_AR8325
		tmp1 = read_ar8xxx(0x600);						// Read MAC Address 31:0
#elif (defined CONFIG_SC14452_ATHEROS_AR8314)
		write_ar8xxx(0x50, 0xe);
		tmp1 = read_ar8xxx(0x54);
#endif
		tmp_mac[0] = tmp1 & 0xff;
		tmp_mac[1] = (tmp1 & 0xff00) >> 8;
		tmp_mac[2] = (tmp1 & 0xff0000) >> 16;
		tmp_mac[3] = (tmp1 & 0xff000000) >> 24;
#ifdef CONFIG_SC14452_ATHEROS_AR8325
		tmp2 = read_ar8xxx(0x604);						// Read MAC Address 47:32
		tmp_mac[4] = tmp2 & 0xff;
		tmp_mac[5] = (tmp2 & 0xff00) >> 8;
#elif (defined CONFIG_SC14452_ATHEROS_AR8314)
		tmp2 = read_ar8xxx(0x50);						// Read MAC Address 47:32
		tmp_mac[0] = (tmp2 & 0xff0000) >> 16;
		tmp_mac[1] = (tmp2 & 0xff000000) >> 24;
#endif

#ifdef CONFIG_SC14452_ATHEROS_AR8325
		tmp3 = read_ar8xxx(0x608);
#elif (defined CONFIG_SC14452_ATHEROS_AR8314)
		tmp3 = read_ar8xxx(0x58);
#endif

#ifdef CONFIG_SC14452_ATHEROS_AR8325
		//printk("<%d> mac= %2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x at 0x604=[0x%08x] 0x608=[0x%08x], tmp1=[0x%08x]\n", 
			//i, tmp_mac[5], tmp_mac[4], tmp_mac[3], tmp_mac[2], tmp_mac[1], tmp_mac[0], tmp2, tmp3, tmp1);
#elif (defined CONFIG_SC14452_ATHEROS_AR8314)
		//printk("<%d>\t mac= %2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x, 0x58=[0x%08x], 0x50=[0x%08x], 0x54=[0x%08x]   \n", 
			//i, tmp_mac[5], tmp_mac[4], tmp_mac[3], tmp_mac[2], tmp_mac[1], tmp_mac[0], tmp3, tmp2, tmp1);
#endif
		
		// if table is not empty, then store all entries that exist in PC port2
#ifdef CONFIG_SC14452_ATHEROS_AR8325
		// PC port2 (bit 18)
		if ((tmp2 & 0x40000) == 0x40000) {
#elif (defined CONFIG_SC14452_ATHEROS_AR8314)
		// PC port2 (bit 2)
		if ((tmp3 & 0x4) == 0x4) {
#endif
			memcpy(&pc_mac[k], tmp_mac, sizeof(tmp_mac));
#ifdef CONFIG_SC14452_ATHEROS_AR8325
			pc_reg[k] = tmp3;
#elif (defined CONFIG_SC14452_ATHEROS_AR8314)
			pc_reg[k] = (tmp3 & 0xf0000) >> 16;		// Store AT_STATUS bits
#endif

#ifdef CONFIG_SC14452_ATHEROS_AR8325
			//printk("PC mac= %2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x 0x608=[0x%08x]\n",	
				//pc_mac[k][5], pc_mac[k][4], pc_mac[k][3], pc_mac[k][2], pc_mac[k][1], pc_mac[k][0], pc_reg[k]);
#elif (defined CONFIG_SC14452_ATHEROS_AR8314)
			//printk("PC mac= %2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x AT_STATUS=[0x%x]\n",	
				//pc_mac[k][5], pc_mac[k][4], pc_mac[k][3], pc_mac[k][2], pc_mac[k][1], pc_mac[k][0], pc_reg[k]);
#endif
			k++;
			if (k>9) k=0;
		}

#ifdef CONFIG_SC14452_ATHEROS_AR8325
		// If 0x608 bits AT_STATUS 3:0=0000 then entry is empty
		// Then find form the list the last MAC entry by comparing the ATU_STATUS bits of 0x608 register
		if (((tmp3 & 0xf) == 0x0) && (tmp1 == 0x0)) {	
#elif (defined CONFIG_SC14452_ATHEROS_AR8314)
		// If 0x58 bits AT_STATUS 19:16=0000 then entry is empty
		// Then find form the list the last MAC entry by comparing the ATU_STATUS bits of 0x608 register
		if (((tmp3 & 0xf0000) == 0x0) && (tmp1 == 0x0)) {
#endif
			//printk("[%s] MAC TABLE EMPTY \n", __FUNCTION__);
			if (k==1) memcpy(mac, &pc_mac[0], sizeof(pc_mac));
			else {
				memcpy(mac, &pc_mac[0], sizeof(pc_mac));
				for (l=0; l<k; l++) {
					test = pc_reg[l] & 0xf;
					for (l=1; l<k; l++) {
						if (test <= (pc_reg[l] & 0xf)) {
							test = pc_reg[l] & 0xf;
							memcpy(mac, &pc_mac[l], sizeof(pc_mac));
							//printk("PC mac= %2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x\n",	
								//mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);
						}
					}
				}
			}
			break;
		}

#ifdef CONFIG_SC14452_ATHEROS_AR8325
		// get next entry
		write_ar8xxx(0x60C, 0x80000006);
#elif (defined CONFIG_SC14452_ATHEROS_AR8314)		
		// WAN port3, PC port2
		//write_ar8xxx(0x50, 0x0);
		//write_ar8xxx(0x50, 0xe);						// AT_BUSY: Start and AT operation (cleared by HW). 
														// Get next valid MAC ARL entry
#endif
		i++;
	}

#endif
	return 0;
}


/*
1. ACL classifies the received packets of MAC0 and MAC2
2. ACL will check EtherType=0x888e for 802.1.x
3. If matched, CTAG to 1 is changed
4. If the CTAG is equal to egress port PVID, the tag should be removed
*/
void AR8XXX_block_8021x_atPC(int enable)
{
	set_ar8xxx(0x30, 1, 1);										// Enable ACLs

	// RX
	// MAC Pattern. See Table 2.7 page 31 Atheros 8325 datasheet
	write_ar8xxx(0x404, 0);										// ACL byte [3:0]
	write_ar8xxx(0x408, 0);										// ACL byte [7:4]
	write_ar8xxx(0x40C, 0);										// ACL byte [11:8]
	write_ar8xxx(0x410, 0x888e0000);							// ACL byte [15:12]. Bytes 15:14 Match 0x88xx LLDP Eth type. Page 31 datasheet.
	write_ar8xxx(0x414, 0x9);									// ACL byte [16]. Set source port as WAN port3, CPU port0
	write_ar8xxx(0x400, 0x80000001 + ACL_8021X_noPC_POS);		// ACL_Busy, Write command, Rule and Index to ind=(1+i)

	// MAC Mask. See Table 2.8 page 31 Atheros 8325 datasheet
	write_ar8xxx(0x404, 0);										// ACL byte [3:0]
	write_ar8xxx(0x408, 0);										// ACL byte [7:4]
	write_ar8xxx(0x40C, 0);										// ACL byte [11:8]. 
	write_ar8xxx(0x410, 0xFFFF0000);	 						// ACL byte [15:12]. Bytes 15:14 Mask at Eth type. Page 32 datasheet.
	write_ar8xxx(0x414, 0x000000c9);	 						// ACL byte [16]. Start & end (only ACL rule, so it is start and end as well), MAC rule type [2:0]=1
	write_ar8xxx(0x400, 0x80000101 + ACL_8021X_noPC_POS);		// ACL_Busy, Write command, Mask and Index to ind=(1+i)

	// MAC Result. See Table 2.6 page 30 Atheros 8325 datasheet
	write_ar8xxx(0x404, 0x00000000);							// 31:0 ACL byte [3:0].
	if (enable) {
		write_ar8xxx(0x408, 0x20000000);							// 63:32 ACL byte [7:4]. DES_PORT[67:61]=0001001 (CPU port0 and WAN port3)
		write_ar8xxx(0x40C, 0x00000011);							// 80:64 ACL byte [11:8]. DES_PORT_OVER_EN[68]=1 DES_PORT[67:61]=0001001 (CPU port0 and WAN port3)
	}else {
		write_ar8xxx(0x408, 0x00000000);							// 63:32 ACL byte [7:4]. DES_PORT[67:61]=0001001 (CPU port0 and WAN port3)
		write_ar8xxx(0x40C, 0x00000000);							// 80:64 ACL byte [11:8]. DES_PORT_OVER_EN[68]=1 DES_PORT[67:61]=0001001 (CPU port0 and WAN port3)
	}
	write_ar8xxx(0x410, 0);	 									// ACL byte [15:12].
	write_ar8xxx(0x414, 0);	 									// ACL byte [16]. 
	write_ar8xxx(0x400, 0x80000201 + ACL_8021X_noPC_POS);		// ACL_Busy, Write command, Result and Index to ind=(1+i)
}


/*
1. ACL classifies the received packets of MAC0 and MAC2
2. ACL will check EtherType=0x88cc
3. If matched, CTAG to 1 is changed
4. If the CTAG is equal to egress port PVID, the tag should be removed
*/
void AR8XXX_LLDP_noVLAN(void)
{
	set_ar8xxx(0x30, 1, 1);										// Enable ACLs

	// RX
	// MAC Pattern. See Table 2.7 page 31 Atheros 8325 datasheet
	write_ar8xxx(0x404, 0);										// ACL byte [3:0]
	write_ar8xxx(0x408, 0);										// ACL byte [7:4]
	write_ar8xxx(0x40C, 0);										// ACL byte [11:8]
	write_ar8xxx(0x410, 0x88cc0000);							// ACL byte [15:12]. Bytes 15:14 Match 0x88xx LLDP Eth type. Page 31 datasheet.
	write_ar8xxx(0x414, 0xc);									// ACL byte [16]. Set source port as WAN port3, PC port2
	write_ar8xxx(0x400, 0x80000001 + ACL_LLDP_noVLAN_POS_RX);		// ACL_Busy, Write command, Rule and Index to ind=(1+i)

	// MAC Mask. See Table 2.8 page 31 Atheros 8325 datasheet
	write_ar8xxx(0x404, 0);										// ACL byte [3:0]
	write_ar8xxx(0x408, 0);										// ACL byte [7:4]
	write_ar8xxx(0x40C, 0);										// ACL byte [11:8]. 
	write_ar8xxx(0x410, 0xFFFF0000);	 						// ACL byte [15:12]. Bytes 15:14 Mask at Eth type. Page 32 datasheet.
	write_ar8xxx(0x414, 0x000000c9);	 						// ACL byte [16]. Start & end (only ACL rule, so it is start and end as well), MAC rule type and VID MASK=1. FRAME_WITH_TAG=1
	write_ar8xxx(0x400, 0x80000101 + ACL_LLDP_noVLAN_POS_RX);		// ACL_Busy, Write command, Mask and Index to ind=(1+i)

	// MAC Result. See Table 2.6 page 30 Atheros 8325 datasheet
	write_ar8xxx(0x404, 0x00010000);							// 31:0 ACL byte [3:0]. Change CTAG to 1
	write_ar8xxx(0x408, 0x20001000);							// 63:32 ACL byte [7:4]. DES_PORT=P0, TRANS_CTAG_CHANGE_EN=1
	write_ar8xxx(0x40C, 0x00000010);							// 80:64 ACL byte [11:8]. DES_PORT_OVER_EN=1
	write_ar8xxx(0x410, 0);	 									// ACL byte [15:12].
	write_ar8xxx(0x414, 0);	 									// ACL byte [16]. 
	write_ar8xxx(0x400, 0x80000201 + ACL_LLDP_noVLAN_POS_RX);		// ACL_Busy, Write command, Result and Index to ind=(1+i)

	// TX
	// MAC Pattern. See Table 2.7 page 31 Atheros 8325 datasheet
	write_ar8xxx(0x404, 0);										// ACL byte [3:0]
	write_ar8xxx(0x408, 0);										// ACL byte [7:4]
	write_ar8xxx(0x40C, 0);										// ACL byte [11:8]
	write_ar8xxx(0x410, 0x88cc0000);							// ACL byte [15:12]. Bytes 15:14 Match 0x88xx LLDP Eth type. Page 31 datasheet.
	write_ar8xxx(0x414, 0x9);									// ACL byte [16]. Set source port as CPU port0
	write_ar8xxx(0x400, 0x80000001 + ACL_LLDP_noVLAN_POS_TX);	// ACL_Busy, Write command, Rule and Index to ind=(1+i)

	// MAC Mask. See Table 2.8 page 31 Atheros 8325 datasheet
	write_ar8xxx(0x404, 0);										// ACL byte [3:0]
	write_ar8xxx(0x408, 0);										// ACL byte [7:4]
	write_ar8xxx(0x40C, 0);										// ACL byte [11:8]. 
	write_ar8xxx(0x410, 0xFFFF0000);	 						// ACL byte [15:12]. Bytes 15:14 Mask at Eth type. Page 32 datasheet.
	write_ar8xxx(0x414, 0x000000c9);	 						// ACL byte [16]. Start & end (only ACL rule, so it is start and end as well), MAC rule type and VID MASK=1. 
	write_ar8xxx(0x400, 0x80000101 + ACL_LLDP_noVLAN_POS_TX);	// ACL_Busy, Write command, Mask and Index to ind=(1+i)

	// MAC Result. See Table 2.6 page 30 Atheros 8325 datasheet
	write_ar8xxx(0x404, 0x00000000);							// 31:0 ACL byte [3:0]. Change CTAG to 1
	write_ar8xxx(0x408, 0x00002000);							// 63:32 ACL byte [7:4]. TRANS_CTAG_CHANGE_EN=1
	write_ar8xxx(0x40C, 0x00000011);							// 80:64 ACL byte [11:8]. DES_PORT_OVER_EN=1, DES_PORT=P3
	write_ar8xxx(0x410, 0);	 									// ACL byte [15:12].
	write_ar8xxx(0x414, 0);	 									// ACL byte [16]. 
	write_ar8xxx(0x400, 0x80000201 + ACL_LLDP_noVLAN_POS_TX);	// ACL_Busy, Write command, Result and Index to ind=(1+i)
}


void AR8XXX_storm_cntrl(int enable)
{
	if (enable == 1) {
#ifdef CONFIG_SC14452_ATHEROS_AR8314
		// WAN port storm control
		write_ar8xxx(0x414, 0x703);
		// PC port storm control
		write_ar8xxx(0x314, 0x703);
#elif defined CONFIG_SC14452_ATHEROS_AR8236
		// WAN port storm control
		write_ar8xxx(0x518, 0x703);
		// PC port storm control
		write_ar8xxx(0x314, 0x703);
#elif defined CONFIG_SC14452_ATHEROS_AR8325
		// PC port2 storm control excess counter
		write_ar8xxx(0xB24, 0x00050003);
		unset_ar8xxx(0xB28, 0xff, 0);
		// broadcast, unknown unicast, unknown multicast
		set_ar8xxx(0xB28, 0x38, 0);
		// WAN port3 storm control excess counter
		write_ar8xxx(0xB34, 0x00050003);
		// broadcast, unknown unicast, unknown multicast
		unset_ar8xxx(0xB38, 0xff, 0);
		set_ar8xxx(0xB38, 0x38, 0);
#endif

	}else {
#ifdef CONFIG_SC14452_ATHEROS_AR8314
		// WAN port storm control
		write_ar8xxx(0x414, 0x0);
		// PC port storm control
		write_ar8xxx(0x314, 0x0);
#elif defined CONFIG_SC14452_ATHEROS_AR8236
		// WAN port storm control
		write_ar8xxx(0x518, 0x0);
		// PC port storm control
		write_ar8xxx(0x314, 0x0);
#elif defined CONFIG_SC14452_ATHEROS_AR8325
		// PC port2 storm control disable
		write_ar8xxx(0xB24, 0x00047fff);
		unset_ar8xxx(0xB28, 0xff, 0);
		// WAN port3 storm control disable
		write_ar8xxx(0xB34, 0x00047fff);
		unset_ar8xxx(0xB38, 0xff, 0);
#endif
	}
}

void AR8XXX_WAN_port_ingress_rate(int enable)
{
	if (enable == 1) {	
#ifdef CONFIG_SC14452_ATHEROS_AR8325
		// WAN port3 ingress rate control 8Mbps
		write_ar8xxx(0xB30, 0x18410060);
		unset_ar8xxx(0xB38, 0xff, 8);
		// all frames
		set_ar8xxx(0xB38, 0xe7, 8);
#endif

	}else {
#ifdef CONFIG_SC14452_ATHEROS_AR8325
		// WAN port3 ingress rate control 8Mbps
		write_ar8xxx(0xB30, 0x18407fff);
		unset_ar8xxx(0xB38, 0xff, 8);	
#endif
	}
}

void AR8XXX_PC_port_ingress_rate(int enable)
{
	if (enable == 1) {	
#if defined CONFIG_SC14452_ATHEROS_AR8325
		// WAN port3 ingress rate control 8Mbps
		write_ar8xxx(0xB20, 0x18410060);
		unset_ar8xxx(0xB28, 0xff, 8);
		// all frames
		set_ar8xxx(0xB28, 0xe7, 8);
#endif

	}else {
#if defined CONFIG_SC14452_ATHEROS_AR8325
		// WAN port3 ingress rate control 8Mbps
		write_ar8xxx(0xB20, 0x18407fff);
		unset_ar8xxx(0xB28, 0xff, 8);	
#endif
	}
}

void disable_AR8xxx_VLAN(void)
{
	printk("[%s]\n", __FUNCTION__);
#ifdef CONFIG_SC14452_ATHEROS_AR8325
	// PORTX_VLAN_CTRL0
	unset_ar8xxx(0x420, 0x7, 29);			// ING_PORT_CPRI_0=0 
	unset_ar8xxx(0x420, 0xFFFF, 16);	
	set_ar8xxx(0x420, 1, 16);					// PORT_DEFAULT_CVID_0=1
	unset_ar8xxx(0x430, 0x7, 29);			// ING_PORT_CPRI_2=0 
	unset_ar8xxx(0x430, 0xFFFF, 16);	
	set_ar8xxx(0x430, 1, 16);					// PORT_DEFAULT_CVID_2=1
	unset_ar8xxx(0x438, 0x7, 29);			// ING_PORT_CPRI_3=0 
	unset_ar8xxx(0x438, 0xFFFF, 16);	
	set_ar8xxx(0x438, 1, 16);					// PORT_DEFAULT_CVID_3=1

	// PORTX_VLAN_CTRL1
	unset_ar8xxx(0x0424, 0x3, 12);		// EG_VLAN_MODE=11, egress at CPU port0 untouched
	set_ar8xxx(0x0424, 0x3, 12);			// EG_VLAN_MODE=11, egress at CPU port0 untouched
	unset_ar8xxx(0x0434, 0x3, 12);		// EG_VLAN_MODE=11, egress at PC port2 untouched
	set_ar8xxx(0x0434, 0x3, 12);			// EG_VLAN_MODE=11, egress at PC port2 untouched
	unset_ar8xxx(0x043C, 0x3, 12);		// EG_VLAN_MODE=11, egress at WAN port2 untouched
	set_ar8xxx(0x043C, 0x3, 12);			// EG_VLAN_MODE=11, egress at WAN port2 untouched
	
	// PORT0_LOOKUP_CTRL
	unset_ar8xxx(0x0660, 0x3, 8);			// CPU port0. 802.1Q_MODE=00. Disable 802.1Q vlan
	unset_ar8xxx(0x0660, 0x7F, 0);
	set_ar8xxx(0x0660, 0xC, 0);				// CPU port0. PORT_VID_MEM=0001100. Sent packets to ports 2 & 3
	unset_ar8xxx(0x0678, 0x3, 8);			// PC port2. 802.1Q_MODE=00. Disable 802.1Q vlan
	unset_ar8xxx(0x0678, 0x7F, 0);
	set_ar8xxx(0x0678, 0x9, 0);				// PC port2. PORT_VID_MEM=001001. Sent packets to ports 0 & 3
	unset_ar8xxx(0x0684, 0x3, 8);			// WAN port3. 802.1Q_MODE=00. Disable 802.1Q vlan
	unset_ar8xxx(0x0684, 0x7F, 0);
	set_ar8xxx(0x0684, 0x5, 0);				// WAN port3. PORT_VID_MEM=000101. Sent packets to all port 0 & 2
	
	// VTU_FUNC_REG1
	write_ar8xxx(0x0614, 0x80000009);	// Flash all VLAN entries

	// PORT0_PRI_CTRL
	unset_ar8xxx(0x0664, 1, 17);			// CPU port0, vlan priorities disable
	unset_ar8xxx(0x067C, 1, 17);			// PC port2, vlan priorities disable
	unset_ar8xxx(0x0688, 1, 17);			// WAN port3, vlan priorities disable

#elif defined CONFIG_SC14452_ATHEROS_AR8314
#if 0
	write_ar8xxx(0x108, 0x003e0001);
	write_ar8xxx(0x308, 0x003b0001);
	write_ar8xxx(0x408, 0x00370001);
	write_ar8xxx(0x104, 0x4804);
	write_ar8xxx(0x304, 0x4004);
	write_ar8xxx(0x404, 0x4004);
#endif
#elif defined CONFIG_SC14452_ATHEROS_AR8236
#if 0
	write_ar8xxx(0x108, 0x10000);
	write_ar8xxx(0x308, 0x10000);
	write_ar8xxx(0x508, 0x10000);
	write_ar8xxx(0x104, 0x4804);
	write_ar8xxx(0x304, 0x4004);
	write_ar8xxx(0x504, 0x4004);
	write_ar8xxx(0x10c, 0x3e0000);
	write_ar8xxx(0x30c, 0x3b0000);
	write_ar8xxx(0x50c, 0x2f0000);
#endif
#endif
}


void enable_AR8XXX_PAUSE(void)
{
#ifdef CONFIG_SC14452_ATHEROS_AR8325
	// PORT0_STATUS
	set_ar8xxx(0x007C, 0x3, 4);					// TX_FLOW_EN_0 = 0, RX_FLOW_EN_0 = 0
	unset_ar8xxx(0x007C, 0x1, 12);			// mac flow can be config by software, by bits 4&5
	// PORT2_STATUS
	set_ar8xxx(0x0084, 0x3, 4);					// TX_FLOW_EN_0 = 0, RX_FLOW_EN_0 = 0
	unset_ar8xxx(0x0084, 0x1, 12);			// mac flow can be config by software, by bits 4&5
	// PORT3_STATUS
	set_ar8xxx(0x0088, 0x3, 4);					// TX_FLOW_EN_0 = 0, RX_FLOW_EN_0 = 0
	unset_ar8xxx(0x0088, 0x1, 12);			// mac flow can be config by software, by bits 4&5
#endif
}

void disable_AR8XXX_PAUSE(void)
{
#ifdef CONFIG_SC14452_ATHEROS_AR8325
	// PORT0_STATUS
	unset_ar8xxx(0x007C, 0x3, 4);					// TX_FLOW_EN_0 = 0, RX_FLOW_EN_0 = 0
	unset_ar8xxx(0x007C, 0x1, 12);				// mac flow can be config by software, by bits 4&5
	// PORT2_STATUS
	unset_ar8xxx(0x0084, 0x3, 4);					// TX_FLOW_EN_0 = 0, RX_FLOW_EN_0 = 0
	unset_ar8xxx(0x0084, 0x1, 12);				// mac flow can be config by software, by bits 4&5
	// PORT3_STATUS
	unset_ar8xxx(0x0088, 0x3, 4);					// TX_FLOW_EN_0 = 0, RX_FLOW_EN_0 = 0
	unset_ar8xxx(0x0088, 0x1, 12);				// mac flow can be config by software, by bits 4&5

#elif defined CONFIG_SC14452_ATHEROS_AR8314
	unsigned int temp = 0;
	int i = 0;
	// Read PHY 2 (WAN port) register 4
	// Disable PAUSE functionality
	temp = emac_md_read (2, 4, EMAC_MDC_DIV_42);
	temp &= 0xFFFFF3FF;
	emac_md_write (2, 4, EMAC_MDC_DIV_42, temp);

	// Read PHY 1 (PC port) register 4
	// Disable PAUSE functionality
	temp = emac_md_read (1, 4, EMAC_MDC_DIV_42);
	temp &= 0xFFFFF3FF;
	emac_md_write (1, 4, EMAC_MDC_DIV_42, temp);

	// soft reset
	write_ar8xxx(0x0, 0x80000000);

	while (1) {
		i++;
		temp = read_ar8xxx(0x0);
		if (i==50) break;
		if ((temp & 0x80000000) == 0x80000000)
			continue;
		else break;
	}
	// Enable port status auto learn
	temp = read_ar8xxx(0x300);
	temp |= 1<<9;
	write_ar8xxx(0x300, temp);

	// Enable port status auto learn
	temp = read_ar8xxx(0x400);
	temp |= 1<<9;
	write_ar8xxx(0x400, temp);
#endif
}

/*----------------------------------------------------------------------------*/
/*                           Init Tx Descriptor                               */
/*----------------------------------------------------------------------------*/
void emac_init_tx_desc (struct s_descriptor* TX_DESCS)
{
	unsigned char i;
	long int temp;
    
	temp = DWORD_SHIFT(0x1, 31) | // [31]    Interrupt Completion
	       DWORD_SHIFT(0x0, 30) | // [30]    Last Segment
	       DWORD_SHIFT(0x0, 29) | // [29]    First Segment
	       DWORD_SHIFT(0x0, 27) | // [28:27] Checksum Insertion Control
	       DWORD_SHIFT(0x0, 26) | // [26]    Disable CRC
	       DWORD_SHIFT(0x0, 25) | // [25]    Transmit End Of Ring
	       DWORD_SHIFT(0x0, 24) | // [24]    Second address Chained
	       DWORD_SHIFT(0x0, 23) | // [23]    Disable padding
	       DWORD_SHIFT(0x0, 11) | // [21:11] Trasmit Buffer 2 Size
	                   0x0;        // [10:0]  Trasmit Buffer 1 Size
	   
	for (i=0; i<MAX_TX_DESCS; i++) {
		TX_DESCS[i].DES0 = 0x00000000; // The software is the owner of the descriptor
		TX_DESCS[i].DES1 = temp;
	
		TX_DESCS[i].DES2 = 0; 
		TX_DESCS[i].DES3 = 0; 
	}
    
	// Set Transmit End of Ring in the Last descriptor
	TX_DESCS[MAX_TX_DESCS-1].DES1 = TX_DESCS[MAX_TX_DESCS-1].DES1 | DWORD_SHIFT(0x1,25); // [25] Transmit End Of Ring;
}

/*----------------------------------------------------------------------------*/
/*                           Init Rx Descriptor                               */
/*----------------------------------------------------------------------------*/
void emac_init_rx_desc (struct s_descriptor* RX_DESCS)
{
	unsigned char i;
	long int temp;

	temp = DWORD_SHIFT(0x0, 31) | // [31]    Disable Interrupt On Completion
	       DWORD_SHIFT(0x0, 25) | // [25]    Receive End Of Ring
	       DWORD_SHIFT(0x0, 24) | // [24]    Second address Chained
	       DWORD_SHIFT(0, 11)		| // [21:11] Trasmit Buffer 2 Size
	       MAX_PACKET_LEN;        // [10:0]  Trasmit Buffer 1 Size

	for (i=0; i < MAX_RX_DESCS; i++) {
		RX_DESCS[i].DES0 = 0x00000000;    	
		RX_DESCS[i].DES1 = temp;

		RX_DESCS[i].DES2 = 0;
		RX_DESCS[i].DES3 = 0;
	}    

	// Set Receive End of Ring in the Last descriptor
	RX_DESCS[MAX_RX_DESCS-1].DES1 = RX_DESCS[MAX_RX_DESCS-1].DES1 | DWORD_SHIFT(0x1,25); // [25] Receive End Of Ring;
}

/*----------------------------------------------------------------------------*/
/*                        Emac Soft Reset                                     */
/*----------------------------------------------------------------------------*/
void emac_soft_rst(void)
{
	// Set Software reset bit. The new PHY configuration will be read.
	SetDword (EMAC_DMAR0_BUS_MODE_REG, 0x1); 

	// Wait End of Reset
	while (GetDword (EMAC_DMAR0_BUS_MODE_REG) & 0x1); 
}

/*----------------------------------------------------------------------------*/
/*                  The Emac start using the tx descriptors                   */
/*----------------------------------------------------------------------------*/
void emac_read_tx_des(void)
{
	SetDword(EMAC_DMAR1_TX_POLL_DEMAND_REG, 0x0);
}

/*----------------------------------------------------------------------------*/
/*                  The Emac start using the rx descriptors                   */
/*----------------------------------------------------------------------------*/
void emac_read_rx_des(void)
{
  SetDword(EMAC_DMAR2_RX_POLL_DEMAND_REG, 0x0);
}

/*----------------------------------------------------------------------------*/
/*                        Wait End of Frame Reception                         */
/*----------------------------------------------------------------------------*/
void emac_wait_frame_reception(void) 
{
  // Polling
  while (!(GetDword(EMAC_DMAR5_STATUS_REG) & (DWORD_SHIFT(0x1, 6))));

  // Clear Receive Interrupt
  SetDword(EMAC_DMAR5_STATUS_REG, DWORD_SHIFT(0x1, 6));
}

/*----------------------------------------------------------------------------*/
/*                        Wait End of Frame Transmission                       */
/*----------------------------------------------------------------------------*/
void emac_wait_frame_transmission(void) 
{
  // Polling
  while (!(GetDword(EMAC_DMAR5_STATUS_REG) & 0x1));

  // Clear Receive Interrupt
  SetDword(EMAC_DMAR5_STATUS_REG, 0x1);
}

/*----------------------------------------------------------------------------*/
/*                                   Init Emac                                */
/*----------------------------------------------------------------------------*/
void emac_cfg (unsigned char *mac_address, struct net_local *lp)
{
	long int temp;
	
	/***
	 *** Init: (see EthMAC datasheet paragraph 3.3.1,pp.46)
	 ***/
	 
	/* a) DMA Register0 = ...
	 *    Bus Mode Register
	 *    Details:
	 *    -> DMA Register0.DA = 1; // set round-robin arbitration with priority at Rx during collisions
	 *    -> Set fixed bursts
	 *       DMA Register0, bit FB 
	 *       and set maximum burst length (is necessary !!!!!):
	 *       Register0[13:8]=PBL field
	 */
	temp = DWORD_SHIFT(0x1,		25) | // [25]    Address - Aligned Beats
	       DWORD_SHIFT(0x0,		24) | // [24]    4xPBL Mode
	       DWORD_SHIFT(0x1,		23) | // [23]    Use Separate PBL
	       DWORD_SHIFT(0x04,	17) | // [22:17] RxDMA PBL
	       DWORD_SHIFT(0x1,		16) | // [16]    Fixed Burst
	       DWORD_SHIFT(0x1,		14) | // [15:14] Rx:Tx priority ratio
	       DWORD_SHIFT(0x10,   8) | // [13:8]  Programmable Burst Length
	       DWORD_SHIFT(0x0,		 2) | // [6:2]   DSL : Descriptor Skip Length
	       DWORD_SHIFT(0x1,		 1) | // [1]	   DMA Arbitration scheme
	                   0x0;         // [0]     Software Reset
  
	SetDword(EMAC_DMAR0_BUS_MODE_REG, temp); 
  
	/* b) DMA Register7 = ...
	 *    Interrupt Enable Register
	 */
	temp = DWORD_SHIFT(0x1, 16) | // [16] Normal Interrupt Summary Enable
	       DWORD_SHIFT(0x1, 15) | // [15] Abnormal Interrupt Summary Enable
	       DWORD_SHIFT(0x1, 14) | // [14] Early Receive Interrupt Enable
	       DWORD_SHIFT(0x1, 13) | // [13] Fatal Bus Error Enable
	       DWORD_SHIFT(0x0, 10) | // [10] Early Transmit Interrupt Enable
	       DWORD_SHIFT(0x1,  9) | // [ 9] Receive Watchdog Timeout Enable
	       DWORD_SHIFT(0x1,  8) | // [ 8] Receive Stopped Enable
	       DWORD_SHIFT(0x1,  7) | // [ 7] Receive Buffer Unavailable Enable
	       DWORD_SHIFT(0x1,  6) | // [ 6] Receive Interrupt Enable
	       DWORD_SHIFT(0x1,  5) | // [ 5] Underflow Interrupt Enable
	       DWORD_SHIFT(0x1,  4) | // [ 4] Overflow Interrupt Enable
	       DWORD_SHIFT(0x1,  3) | // [ 3] Transmit Jabber Timeout Enable
	       DWORD_SHIFT(0x1,  2) | // [ 2] Transmit Buffer Unavailable Enable
	       DWORD_SHIFT(0x1,  1) | // [ 1] Transmit Stopped Enable
										 0x1;       // [ 0] Transmit Interrupt Enable

	SetDword(EMAC_DMAR7_INT_ENABLE_REG, temp);	    
	        
	/* c) Prepare Tx/Rx buffers
	 *    and update DMA Register3 ( Receive Descript List Address Register) 
	 *               DMA Register4 ( Transmit Descriptor List Address Regsiter)
	 */
	SetDword(EMAC_DMAR3_RX_DESCRIPTOR_LIST_ADDR_REG, (unsigned long int)lp->RX_DESCS);
	SetDword(EMAC_DMAR4_TX_DESCRIPTOR_LIST_ADDR_REG, (unsigned long int)lp->TX_DESCS); 
	 
	/* d) set filtering options at
	 *    MAC Register1 (Frame Filter)
	 *    MAC Register2 (Reserved - Hash Disabled)
	 *    MAC Register3 (Reserved - Hash Disabled)
	 *    MAC Register 16 & 17 (MAC Address 0)
	 */
	temp = DWORD_SHIFT(0x0, 31) | // [31]  Receive All
	       DWORD_SHIFT(0x0,  9) | // [ 9]  Source Address Filter Enable
	       DWORD_SHIFT(0x0,  8) | // [ 8]  SA Inverse Filtering
	       DWORD_SHIFT(0x0,  6) | // [7:6] Pass Control Frames ( Forwards control frames that pass address filter)
	       DWORD_SHIFT(0x0,  5) | // [ 5] Disable Broadcast Frames
	       DWORD_SHIFT(0x1,  4) | // [ 4] Pass All Multicast
	       DWORD_SHIFT(0x0,  3) | // [ 3] DA Inverse Filtering
                     0x0;       // [ 0] Promiscuous Mode
     
	SetDword(EMAC_MACR1_FRAME_FILTER_REG, temp);
	 
	SetDword(EMAC_MACR16_MAC_ADDR0_HIGH_REG, DWORD_SHIFT(mac_address[5], 8) | mac_address[4]);
	
	SetDword(EMAC_MACR17_MAC_ADDR0_LOW_REG, DWORD_SHIFT(mac_address[3], 24) | 
                                          DWORD_SHIFT(mac_address[2], 16) | 
	                                        DWORD_SHIFT(mac_address[1], 8)	| mac_address[0]);
      
	/* e) configure and enable Tx/Rx operating modes
	 *    MAC Register0
	 *    (PS and DM bits are set based on the auto-negotion result read from the PHY)
	 */
	temp = DWORD_SHIFT(0x0, 31) | // [31]	Transmit Configuration in RGMII/SGMII
	       DWORD_SHIFT(0x1, 23) | // [23]	Watchdog Disable
	       DWORD_SHIFT(0x0, 22) | // [22]	Jabber Disable
	       DWORD_SHIFT(0x0, 21) | // [21]	Frame Burst Enable
	       DWORD_SHIFT(0x0, 20) | // [20]	Jombo Frame Enable
	       DWORD_SHIFT(0x0, 17) | // [19:17] Inter Frame Gap ( 96 bit time)
	       DWORD_SHIFT(0x0, 16) | // [16]	Disable Carrier Sense During Transmission
	       DWORD_SHIFT(0x1, 15) | // [15]	Port Select ( 10/100Mbps)
#ifdef CONFIG_SC14452_ES2
	       DWORD_SHIFT(0x0, 14) | // [14]	Speed (100Mbps) //vm
#else
				 DWORD_SHIFT(0x1, 14) | // [14]	Speed (100Mbps) //vm
#endif
	       DWORD_SHIFT(0x1, 13) | // [13]	Disable Receive Own
	       DWORD_SHIFT(0x0, 12) | // [12]	Loop Back Mode
	       DWORD_SHIFT(0x1, 11) | // [11]	Duplex Mode
	       DWORD_SHIFT(0x1, 10) | // [10]	Checksum offload
	       DWORD_SHIFT(0x0,  9) | // [ 9]	Disable Retry
	       DWORD_SHIFT(0x1,  8) | // [ 8]	Link Up/Down
	       DWORD_SHIFT(0x1,  7) | // [ 7]	Automatic Pad/CRC Stripping
	       DWORD_SHIFT(0x0,  5) | // [6:5]	Back-Off Limit
	       DWORD_SHIFT(0x0,  4) | // [ 4]	Deferral Check
	       DWORD_SHIFT(0x1,  3) | // [ 3]	Transmitter Enable
	       DWORD_SHIFT(0x1,  2) ; // [ 2]	Receiver Enable

	SetDword(EMAC_MACR0_CONFIG_REG, temp);
    
	/* Block MMC interrupts  and reset counters*/
	EMAC_MMC_INTR_MASK_RX_REG = 0xffffffff;
	EMAC_MMC_INTR_MASK_TX_REG = 0xffffffff;

	EMAC_MMC_CNTRL_REG = 0x5; 	//Reset at Read
     
	/* g) Start Tx/Rx 
	 *    Operation Mode
	 *    DMA Register6, bits 13 and 1
	 *    Note: if bit ST (13) = 1 then EthMAC polls the Tx descriptor
	 */
	temp = DWORD_SHIFT(0x0, 26) | // [26]	Disable Dropping of TCP/IP checksum Error Frames 
	       DWORD_SHIFT(0x0, 25) | // [25]	Receive Store and Forward 
	       DWORD_SHIFT(0x0, 24) | // [24]	Disable Flushing of Received Frames
	       DWORD_SHIFT(0x0, 21) | // [21]	Transmit Store and Forward
	       DWORD_SHIFT(0x0, 20) | // [20]	Flush Transmit FIFO
	       DWORD_SHIFT(0x0, 14) | // [16:14] Transmit Threshold Control (64 bytes)
#ifdef CONFIG_SC14452_ES2
				 DWORD_SHIFT(0x0, 13) | // [13]	Start/Stop Transmission Command //vm
#else
	       DWORD_SHIFT(0x1, 13) | // [13]	Start/Stop Transmission Command //vm
#endif
	       DWORD_SHIFT(0x0,  7) | // [ 7]	Forward Error Frames
	       DWORD_SHIFT(0x0,  6) | // [ 6]	Forward Undersized Good Frames
	       DWORD_SHIFT(0x1,  3) | // [4:3]	Receive Threshold Control (32 bytes)
	       DWORD_SHIFT(0x0,  2) | // [ 2]	Operate on Second Frame
#ifdef CONFIG_SC14452_ES2
	       DWORD_SHIFT(0x0,  1) ; // [ 1]	Start/Stop Receive //vm
#else
				 DWORD_SHIFT(0x1,  1) ; // [ 1]	Start/Stop Receive //vm
#endif   

	SetDword(EMAC_DMAR6_OPERATION_MODE_REG, temp);
}

int emac_init_speed(unsigned short speed, int mode)
{
	emac_clk_disable();
	#ifdef CONFIG_SC14452_ES2
		emac_10mbps();
	#else
		if (speed == 10)
			emac_10mbps();
		else
			emac_100mbps();
	#endif

	// Enable EMAC clocks
	emac_clk_enable();
	if (mode != MODE_SWITCH) 
	{
#ifdef CONFIG_SC14452_ATHEROS_AR8314
		printk("AR8314 IN ROUTER MODE\n");
#elif defined CONFIG_SC14452_ATHEROS_AR8325
		printk("AR8325 IN ROUTER MODE\n");
#elif defined CONFIG_SC14452_ATHEROS_AR8236
		printk("AR8236 IN ROUTER MODE\n");
#endif
		init_AR8XXX(speed);
	}
	else
	{
#ifdef CONFIG_SC14452_ATHEROS_AR8314
		printk("AR8314 IN SWITCH MODE \n");
#elif defined CONFIG_SC14452_ATHEROS_AR8325
		printk("AR8325 IN SWITCH MODE \n");
#elif defined CONFIG_SC14452_ATHEROS_AR8236
		printk("AR8236 IN SWITCH MODE \n");
#endif
		init_AR8XXX_Switch(speed);
	}
	emac_soft_rst();

	return 0;
}

int eth_init(struct net_device *dev)
{
	struct net_local *lp = dev->priv;
	printk("[%s] \n", __FUNCTION__);

	// allocate memory for the rx and tx descriptors
#ifndef CONFIG_SC14452_ES2
	lp->TX_DESCS = (struct s_descriptor *) TX_DES_BASE;
	lp->RX_DESCS = (struct s_descriptor *) RX_DES_BASE;
#endif
		
	// Init the descriptors structures
	emac_init_tx_desc(lp->TX_DESCS);
	emac_init_rx_desc(lp->RX_DESCS);

#if defined (CONFIG_SC14452_ATHEROS_AR8314) || defined (CONFIG_SC14452_ATHEROS_AR8325)
  // Configure the pads for MII interface
  emac_mii_pads();
  // Configure the clock for MII interface and external clock
  emac_mii_clk_ext();
#elif defined CONFIG_SC14452_ATHEROS_AR8236
	// Configure the pads for RMII interface
	emac_rmii_pads();
	// Configure the clock for RMII interface and external clock
	emac_rmii_clk_ext();
#endif

	// Configure the pads for MDC interface
	emac_md_pads();

	// Disable flow control
	disable_AR8XXX_PAUSE();
	// Enable broadcast, unknown unicast and multicast storm control
	AR8XXX_storm_cntrl(1);
	// ingress rate limit at 8Mbps
	AR8XXX_WAN_port_ingress_rate(0);
	AR8XXX_PC_port_ingress_rate(0);

	if(lp->mode != MODE_SWITCH)
		emac_init_speed(100, MODE_ROUTER);
	else	
		emac_init_speed(100, MODE_SWITCH);

#if 0
	// Associate MAC address entry to ports 0, 3 and 4
	write_ar8xxx(0x58, 0x19);
	write_ar8xxx(0x54, ((dev->dev_addr[0] & 0xFF) << 24) |
										((dev->dev_addr[1] & 0xFF) << 16) |
										((dev->dev_addr[2] & 0xFF) << 8) |
										(dev->dev_addr[3] & 0xFF));

	write_ar8xxx(0x50, ((dev->dev_addr[4] & 0xFF) << 24) |
										((dev->dev_addr[5] & 0xFF) << 16) | 0xa);

	// Set Global MAC Address
	write_ar8xxx(0x24, ((dev->dev_addr[0] & 0xFF) << 24) |
										((dev->dev_addr[1] & 0xFF) << 16) |
										((dev->dev_addr[2] & 0xFF) << 8) |
										(dev->dev_addr[3] & 0xFF));

	write_ar8xxx(0x20, ((dev->dev_addr[4] & 0xFF) << 8) | (dev->dev_addr[5] & 0xFF));
#endif

	return 0;
}


#ifdef EMAC_TEST_MODE
struct emac_dev 
{
		struct s_descriptor  *tx_descriptor;
		unsigned long int    *tx_buf_base;
		unsigned long int    tx_buf_size;
		unsigned char        num_of_tx_des;		
		unsigned char        tx_ptr;
		struct s_descriptor  *rx_descriptor;
		unsigned long int    *rx_buf_base;
		unsigned long int    rx_buf_size;
		unsigned char        num_of_rx_des;
		unsigned char        rx_ptr;
};

struct emac_dev emacDev;

/*----------------------------------------------------------------------------*/
/*                       Prepare Rx Descriptors                               */
/*----------------------------------------------------------------------------*/
void emac_rx_des_prepare(void) 
{
	unsigned char i;

	for (i=0; i<emacDev.num_of_rx_des; i++) 
		emacDev.rx_descriptor[i].DES0 = 0x80000000; // Set OWN bit. The descriptor is owned by the DMA of the EMAC.
}

/*-----------------------------------------------------------------------------------------*/
/* Reads the Length of the received frame and checks if the descriptor structure is valid. */
/* They don't make any change to the descriptors.                                          */
/*-----------------------------------------------------------------------------------------*/
long int emac_get_rx_len(void) 
{
	unsigned char i = 0;
	unsigned long int frame_buf_pos = 0;
	unsigned long int frame_len = 0;
	long int des0;
    
	while (i<emacDev.num_of_rx_des) {
	
		des0 = emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES0;

		// Check Own bit
		if (des0 & DWORD_SHIFT(0x1,31))
			return (-1);
    
		// Check First Descriptor bit
		if (i==0 && !(des0 & DWORD_SHIFT(0x1,9)))
			return (-1);
	
		// Check Last Descriptor bit
		if (des0 & DWORD_SHIFT(0x1,8)) {
		
			frame_len = (des0 >> 16) & 0x3fff;
			
			// Check Error Summary
			if (des0  & DWORD_SHIFT(0x1,15))
				return(-1);
			
			if ( (frame_len < frame_buf_pos) || 
			    ((frame_len - frame_buf_pos) > 2*emacDev.rx_buf_size))
				return (-1);

				
			return (frame_len);
		} else
			frame_buf_pos += 2*emacDev.rx_buf_size;

		i++;
	}
    return (-1);
}

/*----------------------------------------------------------------------------*/
/*  Copy the received data to a new buffer and release the Rx Descriptors     */
/*----------------------------------------------------------------------------*/
unsigned long int emac_get_rx_data(unsigned long int *frame_buf) 
{
	unsigned char i = 0;
    
	long int des0;
	unsigned long int frame_buf_pos = 0;
	unsigned long int frame_len = 0;
	unsigned int dma_len =0;
    
	while (i<emacDev.num_of_rx_des) {
    
		des0 = emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES0;
	
		// Set Own bit
		emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES0 = 0x80000000;
	
		// Check Last Descriptor bit
		if (des0 & DWORD_SHIFT(0x1,8)) {
		
			frame_len = (des0 >> 16) & 0x3fff;
			
			// Copy the data from buffer 1 to the frame buffer using the dma engine.      
			if ((frame_len - frame_buf_pos) >= emacDev.rx_buf_size)
				dma_len = emacDev.rx_buf_size;
			else
				dma_len = (frame_len - frame_buf_pos);	
			
			frame_buf_pos += dma_len;
			
			if (frame_len > frame_buf_pos) {
				// Copy the data from buffer 2 to the frame buffer using the dma engine. 
        		
				dma_len = (frame_len - frame_buf_pos);
        		
				frame_buf_pos += dma_len;
			}
        		
			emacDev.rx_ptr = (emacDev.rx_ptr + i + 1) % emacDev.num_of_rx_des;
			
			return (frame_len);
		} else {
			// Copy the data from buffer 1 to the frame buffer using the dma engine. 
        	
			dma_len = emacDev.rx_buf_size;
        	
			frame_buf_pos += dma_len;
        	
			frame_buf_pos += dma_len;
        	
			i++;
		}
	}

	return (frame_buf_pos);
}

/*----------------------------------------------------------------------------*/
/*            Calculates the number of the free tx descriptors                */
/*----------------------------------------------------------------------------*/
unsigned char emac_tx_des_calc (void) 
{
	unsigned char i; 
	
	for (i=0; i<emacDev.num_of_tx_des; i++)
		if (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES0 & 0x80000000)
			return (i);
	
	return (emacDev.num_of_tx_des);
}

/*----------------------------------------------------------------------------*/
/*            Calculates the total size of the free tx buffers                */
/*----------------------------------------------------------------------------*/
unsigned long int emac_tx_buf_calc (void) 
{
	return (2* emac_tx_des_calc() * emacDev.tx_buf_size);
}

/*----------------------------------------------------------------------------*/
/*            Write a Frame to the Tx Descriptors                             */
/*----------------------------------------------------------------------------*/
unsigned long int emac_send_tx_data(unsigned long int frame_size, unsigned long int *frame_buf)
{
	unsigned char i = 0, k;
	unsigned long int frame_buf_pos = 0;
	unsigned int dma_len = 0;

	while (i < emacDev.num_of_tx_des) {
		
		if (i==0)
			// This is the first Descriptor. Sets the First Segment bit and clears the Last Segment bit
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 | DWORD_SHIFT(0x1,29) ) & (~DWORD_SHIFT(0x1,30));
		else
			// Clear the First Segment bit and the Last Segment bit
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & ~(DWORD_SHIFT(0x1,29) | DWORD_SHIFT(0x1,30));		
		
		// Copy the data from frame buffer to the buffer 1 using the dma engine.
		if ((frame_size - frame_buf_pos) >= emacDev.tx_buf_size)
			dma_len = emacDev.tx_buf_size;
		else
			dma_len = frame_size - frame_buf_pos;
       	
		// Write the size of buffer 1
		emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & (~0x7ff)) | dma_len;
			
		frame_buf_pos += dma_len;
	
		// Copy the data from frame buffer to the buffer 2 using the dma engine if is necessary.
		if (frame_size > frame_buf_pos) {
       			if ((frame_size -  frame_buf_pos) >= emacDev.tx_buf_size)
				dma_len = emacDev.tx_buf_size;
			else
				dma_len = frame_size - frame_buf_pos;
			
			// Write the size of buffer 2
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & (~0x3ff800)) | DWORD_SHIFT (dma_len, 11);
				
			frame_buf_pos += dma_len;
		} else 
			// Clear the size of buffer 2
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & (~0x3ff800);
		
		if (frame_size == frame_buf_pos) {
			// Set the Last Segment bit to the Last descriptor
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 | DWORD_SHIFT(0x1,30);
	
			// -------------------- Set Own Bit ------------------------------------//
			for (k=0; k<=i; k++) {
				emacDev.tx_descriptor[(emacDev.tx_ptr + k) % emacDev.num_of_tx_des].DES0 = 0x80000000;
			}
			
			// -------------------- Update TxPtr Value -----------------------------//
			emacDev.tx_ptr = (emacDev.tx_ptr + i + 1) % emacDev.num_of_tx_des;	
			
			return (frame_buf_pos);
		}
		i++;
	}
	return (frame_buf_pos);
	
}

void emac_rxevent(void)
{
	long int len;
	
	while ((len = emac_get_rx_len()) > 0) {

		if (len >= RX_FRAME_SIZE) {
			printk("EMAC452: packet too big\n");
			return;
		}
		if (RxFrames >= MAX_RX_FRAMES) {
			return;
		}
		RxFrames++;		
	}
}

void emac_rxoverflow(void)
{
	printk("losing packets in rx\n");
	
	// Take all the good frames
	emac_rxevent();
	
	// Clear all the rx descriptors 
	emac_rx_des_prepare();
	
	// Force the emac to read again the rx descriptors
	emac_read_rx_des();
	
}


void emac_txevent(void) 
{
	TxEnd = 1;
}

void emac_poll(void) 
{
	long int status;
	
	while ((status = (GetDword(EMAC_DMAR5_STATUS_REG) & 0x1ffff)) != 0) {

		// Receive Overflow
		if (status & DWORD_SHIFT(0x1, 4)) {
			emac_rxoverflow();
			SetDword(EMAC_DMAR5_STATUS_REG, DWORD_SHIFT(0x1, 4));	
		}

		// End of frame transmission
		if (status & 0x1) {
			emac_txevent();
			SetDword(EMAC_DMAR5_STATUS_REG, 0x1);
		}
	
		// End of frame reception
		if (status & DWORD_SHIFT(0x1, 6)) {
			emac_rxevent();
			SetDword(EMAC_DMAR5_STATUS_REG, DWORD_SHIFT(0x1, 6));
		}

		// Clear all other interrupts
		SetDword (EMAC_DMAR5_STATUS_REG, DWORD_SHIFT(0x1,16) |
						 DWORD_SHIFT(0x1,15) |
						 DWORD_SHIFT(0x1,14) | 
		                                 DWORD_SHIFT(0x1,13) |
						 DWORD_SHIFT(0x1,10) |
						 DWORD_SHIFT(0x1,9)  |
						 DWORD_SHIFT(0x1,8)  |
						 DWORD_SHIFT(0x1,7)  |
						 DWORD_SHIFT(0x1,5)  |
						 DWORD_SHIFT(0x1,3)  |
						 DWORD_SHIFT(0x1,2)  |
						 DWORD_SHIFT(0x1,1)   );
	}
}

void eth_halt()
{
}

int eth_rx()
{
	int j, tmo;

	PRINTK("### eth_rx\n");

	while(1) {
		emac_poll();
		if (RxFrames > 0) {
			PRINTK("RxFrames %02x \n", RxFrames);
			RxFrames = 0;
			return 1;
		}
	}
	return 0;
}

int eth_send(volatile void *packet, int length)
{
	int tmo;

	PRINTK("### eth_send\n");

	TxEnd = -1;

	emac_send_tx_data(length, (unsigned long int *) packet);
	
	emac_read_tx_des();
	
	while(1) {
		emac_poll();
		if (TxEnd != -1) {
			PRINTK("Packet sucesfully sent\n");
			return 0;
		}
	}
	return 0;
}
#endif // EMAC_TEST_MODE

#endif
