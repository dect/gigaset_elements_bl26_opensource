// File created by Gigaset Elements GmbH
// All rights reserved.
/*#################################################################################################
#
#  SiTel Semiconductor BV
#  Het Zuiderkruis 53
#  5215 MV 's-Hertogenbosch
#  The Netherlands
#
#  Project: VoIP
#  RHEA SOFTWARE PLATFORM
#  SC14452 EMAC LINUX DRIVER
#
#  File sc14452.h
#################################################################################################*/

#ifndef SC14452_MICREL_ROUTER_H
#define SC14452_MICREL_ROUTER_H

#include <linux/if_ether.h>
#include <linux/ioport.h>
#include <linux/skbuff.h>
#include <linux/netdevice.h>

#ifdef CONFIG_MICREL_ROUTER
unsigned int I2C_8Bit_md_read(unsigned char reg_address);
void I2C_8Bit_md_write(unsigned char reg_address, unsigned int wdata);
void ACCESS1_Init(void);
void init_micrel_KSZ8873(void);
void init_micrel_KSZ8873_Switch(void);
void StopACCESSBus(void);
void StartACCESSBus(void);
int micrel_KSZ8873_get_first_mac_PCport(unsigned char *mac);
#endif

static const char *version =
        "sc14452.c: v1.0 Sitel Semiconductor\n";

#define MAX_PACKET_LEN 0x700

/* The OS name of the card. Is used for messages and in the requests for*/
/* io regions, irqs and dma channels                                    */
static const char* cardname = "SC14452_EMAC";

/*----------------------------*/
/*  IO configurations         */
/*----------------------------*/
#define PORT_INPUT          0
#define PORT_PULL_UP        1
#define PORT_PULL_DOWN      2
#define PORT_OUTPUT         3

#define STATE_OFF	0
#define	STATE_ON	1

#define MODE_ROUTER			0
#define MODE_SWITCH			1
#define MODE_UNDEFINED	0xff

#define MAX_RX_DESCS				64
#define MAX_TX_DESCS				128

/* Place RX and TX descriptors in internal memory to avoid cache coherency issues */
#define TX_DES_BASE  0x0020000
#define RX_DES_BASE  0x0021000

#ifdef CONFIG_SC14452_NETLOAD_PROTECTION

#define ULP_THRESHOLD			500	// Default values for unicast frames per second threshold
#define BLP_THRESHOLD			100	// Default values for broadcast frames per second threshold

/* EMAC_LOADFILTER Message type definitions */
#define EMAC_LF_STATUS          0
#define EMAC_LF_ADD             1
#define EMAC_LF_REMOVE          2
#define EMAC_LF_REMOVE_ALL      3
#define EMAC_LF_START_ALL       4
#define EMAC_LF_START_BCAST     5
#define EMAC_LF_START_UCAST     6
#define EMAC_LF_STOP_ALL        7
#define EMAC_LF_STOP_BCAST      8
#define EMAC_LF_STOP_UCAST      9

#define EMAC_LF_COMMAND_SUCCESS 100
#define EMAC_LF_COMMAND_FAIL    200
#define EMAC_LF_COMMAND_FAIL_BCAST      201
#define EMAC_LF_COMMAND_FAIL_FULL       202
#define EMAC_LF_COMMAND_FAIL_NFOUND     203

struct addr_filter
{
	unsigned short enable;
	unsigned short pid;
	unsigned char MAC[6];
};

struct addr_filter_status
{
	unsigned short ulp_enable;	// Unicast load protection enable
	unsigned short blp_enable;	// Broadcast load protection Enable
	unsigned short ulp_threshold;	// Unicast load threshold
	unsigned short blp_threshold;  // Broadcast load threshold
	unsigned long ups; 		// Unicast packets per second
	unsigned long bps;		// Broadcast packets per second
	unsigned short blp_filtered;	//
	unsigned short ulp_filtered;	//
	unsigned short blp_protect;	// Indicates whether broadcast traffic protection is activated
	unsigned short ulp_protect;	// Indicates whether Unicast traffic protection is activated
	struct addr_filter address_filters[4];		// Filter addresses and enable flags
};

struct addr_filter_ioctl
{
	unsigned short command;
	unsigned short pid;
	struct addr_filter_status status;
};
#endif

/***
 *** Tx/Rx descriptors
 ***/
struct s_descriptor
{
   volatile unsigned long int DES0;
   volatile unsigned long int DES1;
   volatile unsigned long int DES2;
   volatile unsigned long int DES3;
};


struct net_local
{
	struct net_device * devices[2];
        struct net_device_stats stats[2];  /* uClinux ethernet stats struct */
        int state;                      /* State of driver */
	int mode;
#ifdef CONFIG_MICREL_ROUTER
        unsigned char Link[2];                       /* Link Indication */
	unsigned char port_state[2];				/* state of each one of the two devices, i.e. UP or DOWN */
#else
        int Link;                       /* Link Indication */
#endif
        int FullDuplex;                 /* Full Duplex link */
        int LinkSpeed100Mbps;           /* Link Speed Indication */
        struct timer_list emac_timer;    /* link detection timer */
#ifdef CONFIG_SC14452_ES2
	struct s_descriptor RX_DESCS[MAX_RX_DESCS];
#else
  struct s_descriptor *RX_DESCS;
#endif
  struct sk_buff *RX_RING[MAX_RX_DESCS];
  char *RX_BUFFERS;
        int CUR_RX_DESC;
#ifdef CONFIG_SC14452_ES2
	struct s_descriptor  TX_DESCS[MAX_TX_DESCS];
#else
  struct s_descriptor *TX_DESCS;
#endif
  struct sk_buff *TX_RING[MAX_TX_DESCS];
        int tx_ring_size;
        int TX_RING_HEAD;
        int TX_RING_TAIL;
        int INT_STATS[14];
        struct tasklet_struct emac_rx_tasklet;
        struct tasklet_struct emac_tx_tasklet;
        int Frame_end;
        int Frame_start;
        int tx_disabled;
	unsigned short max_tx_packets;
	spinlock_t              lock;
#ifdef CONFIG_SC14452_NETLOAD_PROTECTION
	spinlock_t              filter_lock;
	struct addr_filter_status filter_config;
	unsigned long tmp_ups;
	unsigned long tmp_bps;
#endif
	unsigned short id;
};

#ifdef EMAC_TEST_MODE
struct emac_dev
{
		struct s_descriptor  *tx_descriptor;
		unsigned long int    *tx_buf_base;
		unsigned long int    tx_buf_size;
		unsigned char        num_of_tx_des;
		unsigned char        tx_ptr;
		struct s_descriptor  *rx_descriptor;
		unsigned long int    *rx_buf_base;
		unsigned long int    rx_buf_size;
		unsigned char        num_of_rx_des;
		unsigned char        rx_ptr;
};

struct emac_dev emacDev;

#endif

#ifdef  DEBUG
#define PRINTK(args...) printk(args)
#else
#define PRINTK(args...)
#endif

#define SetPort(a, b, c) (a = ((b << 8) | c))

//-----------------------------------------------------------------------
//              Mathematics
//-----------------------------------------------------------------------
#define DWORD_SHIFT(DAT,BITS) ((unsigned long int) (DAT) << (BITS))
#define NUM_OF_DWORDS(x) (((x % 4) == 0) ? (x >> 2) : ((x >>2 ) + 1))

//-----------------------------------------------------------------------
//              Definitions for memory allocation
//-----------------------------------------------------------------------
#define TX_BUF_GAP  0      // space between two sequential tx buffers (bytes)
#define TX_BUF_SIZE MAX_PACKET_LEN    // the size of one tx buffer (bytes)

#define RX_BUF_GAP  0      // space between two sequential rx buffers (bytes)
#define RX_BUF_SIZE MAX_PACKET_LEN    // the size of one rx buffer (bytes)

//-----------------------------------------------------------------------
//              Definitions for the SMI interface (MDC, MDIO)
//-----------------------------------------------------------------------
#define MD_PHY_ADDR(x) DWORD_SHIFT((x & 0x1f),11)
#define MD_REG_ADDR(x) DWORD_SHIFT((x & 0x1f),6)
#define MD_CLK(x)      DWORD_SHIFT((x & 0x7),2)
#define MD_WRITE       DWORD_SHIFT(1,1)
#define MD_BUSY        0x1

#define EMAC_MDC_DIV_42  0x0
#define EMAC_MDC_DIV_62  0x1
#define EMAC_MDC_DIV_16  0x2
#define EMAC_MDC_DIV_26  0x3
#define EMAC_MDC_DIV_102 0x4
#define EMAC_MDC_DIV_122 0x5

//-----------------------------------------------------------------------
//              Emac peripheral ID
//-----------------------------------------------------------------------
#define PID_EMAC_PIN  57

//-----------------------------------------------------------------------
//  The dma channel used in order to accelerate the process of memory copy.
//-----------------------------------------------------------------------
#define EMAC_USE_DMA_CHANNEL 0

//-----------------------------------------------------------------------
//              Structures and variables related with the emac.
//-----------------------------------------------------------------------
char TxEnd;

#define MAX_RX_FRAMES 4
#define RX_FRAME_SIZE 2000   // Multiple of 4 only!!!!

#ifdef EMAC_TEST_MODE
unsigned char *RxFrameBuf=NULL;

char RxFrames=0;
#endif

unsigned short emac_link_detect(unsigned short id)
{
	return ((unsigned short)(((I2C_8Bit_md_read(30 + (id*16))) & 0x20) >> 5));
}
/*----------------------------------------------------------------------------*/
/*                               Clock Enable                                 */
/*----------------------------------------------------------------------------*/
void emac_clk_enable(void)
{
	SetBits(CLK_AUX2_REG, SW_EMAC_EN, 1);
}

/*----------------------------------------------------------------------------*/
/*                               Clock Disable                                */
/*----------------------------------------------------------------------------*/
void emac_clk_disable(void)
{
	SetBits(CLK_AUX2_REG, SW_EMAC_EN, 0);
}

/*----------------------------------------------------------------------------*/
/*                   Sets the interface speed to 10Mpbs                       */
/*----------------------------------------------------------------------------*/
void emac_10mbps(void)
{
	SetBits(CLK_AUX2_REG, SW_ETH_SEL_SPEED, 0);
}

/*----------------------------------------------------------------------------*/
/*                   Sets the interface speed to 100Mpbs                      */
/*----------------------------------------------------------------------------*/
void emac_100mbps(void)
{
	SetBits(CLK_AUX2_REG, SW_ETH_SEL_SPEED, 1);
}

/*----------------------------------------------------------------------------*/
/*                           Init PADs for RMII mode                          */
/*----------------------------------------------------------------------------*/
void emac_rmii_pads (void)
{
	SetPort(P3_02_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // port3(2) -> emac ref_clk

	SetPort(P3_03_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxEn output
	SetPort(P3_07_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxD[0] output
	SetPort(P3_08_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxD[1] output
	SetPort(P3_04_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxDV input
	SetPort(P3_05_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxD[0] input
	SetPort(P3_06_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxD[1] input
}

/*----------------------------------------------------------------------------*/
/*                           Init PADs for MII mode                          */
/*----------------------------------------------------------------------------*/
void emac_mii_pads (void)
{
	SetPort(P0_00_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // COL input
	SetPort(P0_01_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // CSR input
	SetPort(P0_02_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RXER input

	SetPort(P0_04_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // TXCLK
	SetPort(P0_05_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RXCLK

	SetPort(P3_03_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxEn output
	SetPort(P3_07_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxD[0] output
	SetPort(P3_08_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxD[1] output
	SetPort(P0_07_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxD[2] output
	SetPort(P0_08_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);   // TxD[3] output

	SetPort(P3_04_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxDV input
	SetPort(P3_05_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxD[0] input
	SetPort(P3_06_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxD[1] input
	SetPort(P0_09_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxD[2] input
	SetPort(P0_10_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RxD[3] input
}

/*----------------------------------------------------------------------------*/
/*                           Init MD PADs                                     */
/*----------------------------------------------------------------------------*/
void emac_md_pads(void)
{
	SetPort(P3_00_MODE_REG, PORT_OUTPUT, PID_EMAC_PIN);    // MDC output
	SetPort(P3_01_MODE_REG, PORT_INPUT,  PID_EMAC_PIN);    // MDIO
}


/*----------------------------------------------------------------------------*/
/*                   Set RMII mode and Internal Clocks                        */
/*----------------------------------------------------------------------------*/
void emac_rmii_clk_int(void)
{
	SetBits(CLK_AUX2_REG, SW_RMII_EN, 1);        // Set RMII Mode
	SetBits(CLK_AUX2_REG, SW_RMII_CLK_INT, 1);  // Internal source for RMII ref clock
	SetBits(GPRG_R0_REG, 0x0100, 0);            // REF_CLK output
}

/*----------------------------------------------------------------------------*/
/*                   Set RMII mode and External Clocks                        */
/*----------------------------------------------------------------------------*/
void emac_rmii_clk_ext (void)
{
    SetBits(CLK_AUX2_REG, SW_RMII_EN,1);        // Set RMII Mode
#ifdef CONFIG_IN_BOARDS
	printk(".............................. emac_rmii_clk_ext \n");
    SetBits(GPRG_R0_REG, 0x3C00, 0xF);            // SDRAM control and Address
    SetBits(GPRG_R1_REG, 0xF000, 0xF);            // SDRAM Data lines
#endif
    SetBits(GPRG_R0_REG, 0x0100, 1);            // REF_CLK input
    SetBits(CLK_AUX2_REG, SW_RMII_CLK_INT, 0);  // External source for RMII ref clock
}

/*----------------------------------------------------------------------------*/
/*                   Set MII mode and External  Clocks                        */
/*----------------------------------------------------------------------------*/
void emac_mii_clk_ext(void)
{
	SetBits(CLK_AUX2_REG, SW_RMII_EN, 0);                  // Set MII Mode
	SetPort(P0_04_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // TXCLK input
	SetPort(P0_05_MODE_REG, PORT_INPUT, PID_EMAC_PIN);    // RXCLK input
}

/*----------------------------------------------------------------------------*/
/*                          MD Interface - Read Function                      */
/*----------------------------------------------------------------------------*/
unsigned int emac_md_read(unsigned char phy_address, unsigned char reg_address, unsigned char mdc_div)
{
	// Check the BUSY bit before write to the EMAC_MACR4_MII_ADDR_REG
	while (GetDword(EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);

	// Write the read request
	SetDword(EMAC_MACR4_MII_ADDR_REG,
					 MD_PHY_ADDR(phy_address) |
	                                   MD_REG_ADDR(reg_address) |
					   MD_CLK(mdc_div) |
					   MD_BUSY);

	// Wait Until to receive the requested data
	while (GetDword(EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);

	return (GetDword(EMAC_MACR5_MII_DATA_REG));
}

/*----------------------------------------------------------------------------*/
/*                         MD Interface - Write Function                      */
/*----------------------------------------------------------------------------*/
void emac_md_write (unsigned char phy_address, unsigned char reg_address, unsigned char mdc_div, unsigned int wdata)
{
	// Check the BUSY bit before write to the EMAC_MACR4_MII_ADDR_REG & EMAC_MACR5_MII_ADDR_REG
	while (GetDword(EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);

	SetDword(EMAC_MACR5_MII_DATA_REG, wdata);

	SetDword(EMAC_MACR4_MII_ADDR_REG, MD_PHY_ADDR(phy_address)	|
	                                   MD_REG_ADDR(reg_address) |
	                                   MD_CLK(mdc_div) |
	                                   MD_WRITE  |
	                                   MD_BUSY);

	// Wait Until  the end of write operation
	while (GetDword(EMAC_MACR4_MII_ADDR_REG) & MD_BUSY);
}

/*----------------------------------------------------------------------------*/
/*                     Disable specific port				      */
/*----------------------------------------------------------------------------*/
void inline rtl8306_disable_port(int port)
{
	emac_md_write(port, 0, EMAC_MDC_DIV_42,11);
}

/*----------------------------------------------------------------------------*/
/*                     Enable specific port				      */
/*----------------------------------------------------------------------------*/
void inline rtl8306_enable_port(int port)
{
	emac_md_write(port, 0, EMAC_MDC_DIV_42,0x3300);
}

/*----------------------------------------------------------------------------*/
/*                     Initialize the external switch                         */
/*----------------------------------------------------------------------------*/
void rtl8306_init(void)
{
	unsigned int temp;

	// Activate the link on MAC 5.
	temp = emac_md_read(6, 22, EMAC_MDC_DIV_42);

	temp = temp | 0x8000;

	emac_md_write(6, 22, EMAC_MDC_DIV_42,temp);

//vm patch 10mbps
#ifdef CONFIG_SC14452_ES2
	emac_md_write(6, 0, EMAC_MDC_DIV_42,0x100);
#endif
}

/*----------------------------------------------------------------------------*/
/*      Initialize the external switch										  */
/*			 	in Router Mode											      */
/*----------------------------------------------------------------------------*/
void init_micrel_KSZ8873(void)
{
	 unsigned int temp;

	// Initialize I2C Bus
	ACCESS1_Init();

	/* Setup for routing enabled switch configuration */
/* this applys only to KSZ 8873 */

	/* Set VLAN ports*/
	//	Reg 0x11 bits 2:0 = 101
	//	Reg 0x21 bits 2:0 = 110
	//	Reg 0x31 bits 2:0 = 111

	// Soft Reset
	I2C_8Bit_md_write(0x43, 0x10);

	// 16mA I/O pad drive strength
	temp = I2C_8Bit_md_read(14);
	temp = temp | 0x40;
	I2C_8Bit_md_write(14, temp);

	// VLAN membership for router mode
	I2C_8Bit_md_write(0x11, 0x5);
	I2C_8Bit_md_write(0x21, 0x6);
	I2C_8Bit_md_write(0x31, 0x7);

	// TOS Priority and Broadcasto storm enable at ports 1,2,3
	I2C_8Bit_md_write(0x30, 0xc1);
	I2C_8Bit_md_write(0x10, 0xc1);
	I2C_8Bit_md_write(0x20, 0xc1);

#ifdef CONFIG_IN_BOARDS 
	I2C_8Bit_md_write(0xA2, 0x85);
	I2C_8Bit_md_write(0xA3, 0x05);
	I2C_8Bit_md_write(0xA4, 0x05);
	I2C_8Bit_md_write(0xA5, 0x05);
#else //oth_20100906 add for limiting egress data rate on port 3 to 5Mbps
	I2C_8Bit_md_write(0xA2, 0x83);
	I2C_8Bit_md_write(0xA3, 0x03);
	I2C_8Bit_md_write(0xA4, 0x03);
	I2C_8Bit_md_write(0xA5, 0x03);
#endif

	/* VID A Port membership = 110 (ports 3 & 2 only) */
	I2C_8Bit_md_write(131, 0xa);
  	I2C_8Bit_md_write(130, 0x0);
  	I2C_8Bit_md_write(129, 0xe);

 	I2C_8Bit_md_write(121, 0x04);
  	I2C_8Bit_md_write(122, 0x0);

	/* Enable VLAN, Reg 0x05 bit 7 = 1 */
	I2C_8Bit_md_write(0x5, 0x80);

	/* Disable Learning Reg 0x12 bit 0 = 1 Reg 0x22 bit 0 = 1 */
	I2C_8Bit_md_write(0x12, 0x07);
	I2C_8Bit_md_write(0x22, 0x07);

	/* Fast-Age enabled when link is off */
	I2C_8Bit_md_write(0x3, 0x74);

	// PAUSE disable
	temp = I2C_8Bit_md_read(0x3);
	temp = temp & ~0x30;
	I2C_8Bit_md_write(0x3, temp);

	//enable phy ports 1 & 2
	temp = I2C_8Bit_md_read(29);
	temp = temp & ~0x8;
	I2C_8Bit_md_write(29, temp);

	//restart auto negotiation
	temp = I2C_8Bit_md_read(45);
	temp = temp & ~0x8;
	I2C_8Bit_md_write(45, temp);

	I2C_8Bit_md_write(28, 0x9f);
	I2C_8Bit_md_write(44, 0x9f);

	temp = I2C_8Bit_md_read(29);
	temp = temp | 0x20;
	I2C_8Bit_md_write(29, temp);

	temp = I2C_8Bit_md_read(45);
	temp = temp | 0x20;
	I2C_8Bit_md_write(45, temp);
}

#if 0
// 01:80:c2:00:00:0e
// PORT 3 CPU
// PORT 2 WAN
// PORT 1 PC
void micrel_KSZ8873_LLDP_Bypass(void)
{
	I2C_8Bit_md_write(124, 0x00);								// 57 and 56 FID

	I2C_8Bit_md_write(125, 0x0B);								// 55:48 50-48 Forward Ports -> PORT 3 & PORT 2 
																							// 51 Valid Entry = 1
																							// 52 Override Port 
																							// 53 Use FID
																						  // 54-55 FID
	I2C_8Bit_md_write(126, 0x0e);								// 47:40 MAC Address
	I2C_8Bit_md_write(127, 0x00);								// 39:32 MAC Address
	I2C_8Bit_md_write(128, 0x00);								// 31:24 MAC Address
	I2C_8Bit_md_write(129, 0xc2);								// 23:16 MAC Address
	I2C_8Bit_md_write(130, 0x80);								// 15:8 MAC Address
	I2C_8Bit_md_write(131, 0x01);								// 7:0 MAC Address
	
	I2C_8Bit_md_write(121, 0x00);								// write static table 
	I2C_8Bit_md_write(122, 0x4);								// trigger the write operation at 4th position

	I2C_8Bit_md_write(124, 0x00);								// 57 and 56 FID

	I2C_8Bit_md_write(125, 0x0B);								// 55:48 50-48 Forward Ports -> PORT 3 & PORT 2 
																							// 51 Valid Entry = 1
																							// 52 Override Port 
																							// 53 Use FID
																						  // 54-55 FID
	I2C_8Bit_md_write(126, 0x01);								// 47:40 MAC Address
	I2C_8Bit_md_write(127, 0x80);								// 39:32 MAC Address
	I2C_8Bit_md_write(128, 0xc2);								// 31:24 MAC Address
	I2C_8Bit_md_write(129, 0x00);								// 23:16 MAC Address
	I2C_8Bit_md_write(130, 0x00);								// 15:8 MAC Address
	I2C_8Bit_md_write(131, 0x0e);								// 7:0 MAC Address
	
	I2C_8Bit_md_write(121, 0x00);								// write static table 
	I2C_8Bit_md_write(122, 0x5);								// trigger the write operation at 5th position
}
#endif

/* Returns the last MAC address found at PC port 2 */
int micrel_KSZ8873_get_first_mac_PCport(unsigned char *mac)
{
	int i = 0, k = 0, j = 0;
	unsigned int tmp1 = 0x0, tmp2 = 0x0, tmp3 = 0x0, test = 0;
	unsigned char tmp_mac[6];
	unsigned char pc_mac[10][6];
	unsigned char aging[10];
	int valid_entries = 0;
	
	memset(tmp_mac, 0, sizeof(tmp_mac));
	memset(mac, 0, sizeof(tmp_mac));
	memset(pc_mac, 0, sizeof(pc_mac));
	memset(aging, 0, sizeof(aging));

	// WAN port3, PC port2
	I2C_8Bit_md_write(0x79, 0x18 | ((i&0x300)>>7));				// Read MAC table dynamic table entry
	I2C_8Bit_md_write(0x7A, i&0xff);							// Trigger the read operation

	tmp1 = I2C_8Bit_md_read(0x7B);								// [66:64] 66: MAC Empty
																// 65-64 no of valid entries
	tmp2 = I2C_8Bit_md_read(0x7C);								// [65:56] no of valid entries

	valid_entries = ((tmp1 & 0x3) << 7) | (tmp2 & 0xff);
	//printk("[%s] valid_entries=[%d]\n", __FUNCTION__, valid_entries);
	
	if (valid_entries > 1023) valid_entries = 1023;

	// max MAC table entries are 1024
	while(i<valid_entries+1)
	{
		// WAN port3, PC port2
		I2C_8Bit_md_write(0x79, 0x18 | ((i&0x300)>>7));				// Read MAC table dynamic table entry
		I2C_8Bit_md_write(0x7A, i&0xff);							// Trigger the read operation
	
		tmp1 = I2C_8Bit_md_read(0x7B);								// [66:64] 66: MAC Empty
																	// 65-64 no of valid entries			
		if ((tmp1 & 0x80) == 0x80) continue;						// read operation still in progress
		if ((tmp1 & 0x4) == 0x4) break;								// no valid enrties in the table

		tmp2 = I2C_8Bit_md_read(0x7C);								// [65:56] no of valid entries

		// PORT 3 CPU
		// PORT 2 WAN
		// PORT 1 PC
		tmp3 = I2C_8Bit_md_read(0x7D);								// [55:48] 55-54: Aging counter
																	// 53-52: source port 00: port1, 01: port2, 10: port3
		tmp_mac[5] = I2C_8Bit_md_read(0x7E) & 0xFF;						 
		tmp_mac[4] = I2C_8Bit_md_read(0x7F) & 0xFF;		
		tmp_mac[3] = I2C_8Bit_md_read(0x80) & 0xFF;
		tmp_mac[2] = I2C_8Bit_md_read(0x81) & 0xFF;
		tmp_mac[1] = I2C_8Bit_md_read(0x82) & 0xFF;
		tmp_mac[0] = I2C_8Bit_md_read(0x83) & 0xFF;

		//printk("[%d]	mac = %2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x, tmp3=[0x%04x], tmp2=[0x%04x], tmp1=[0x%04x] \n", 
			//i, tmp_mac[5], tmp_mac[4], tmp_mac[3], tmp_mac[2], tmp_mac[1], tmp_mac[0], tmp3, tmp2, tmp1);

		// Store all entries that exist in PC port1
		// tmp3 is bits [55:48]. Source port is 53-52
		// PC port2 (bits 53-52)
		if ((tmp3 & 0x30) == 0x0) {

			memcpy(&pc_mac[k], tmp_mac, sizeof(tmp_mac));
			aging[k] = (tmp3 & 0xC0) >> 6;
		
			//printk("<%d> ######### PC mac= %2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x aging=[%d]\n",	
				//k, pc_mac[k][5], pc_mac[k][4], pc_mac[k][3], pc_mac[k][2], pc_mac[k][1], pc_mac[k][0], aging[k]);

			k++;
			// just 10 entries are supported
			if (k>9) break; // or break..?
		}

		i++;
	}
		
	if (k==1) memcpy(mac, &pc_mac[0], sizeof(pc_mac));
	else {
		memcpy(mac, &pc_mac[0], sizeof(pc_mac));
		for (j=0; j<k; j++) {
			test = aging[j];
			for (j=1; j<k; j++) {
				if (test <= aging[j]) {
					test =  aging[j];
					memcpy(mac, &pc_mac[j], sizeof(pc_mac));
					//printk("\n\n######### PC mac= %2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x #########\n\n",	
						//mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);
				}
			}
		}
	}
		
	return 0;
}

/*----------------------------------------------------------------------------*/
/*                     Initialize the external switch                         */
/*			 	in Switch Mode				      */
/*----------------------------------------------------------------------------*/
void init_micrel_KSZ8873_Switch(void)
{
	 unsigned int temp;
/* Test setup for switching enabled switch configuration */
/* At this point we should have the system already configured for routing at system init */
/* Therefore we just set the switch to default settings...  */
/* this applys only to KSZ 8873 */

	/* Set VLAN ports*/
	//	Reg 0x11 bits 2:0 = 111
	//	Reg 0x21 bits 2:0 = 111
	//	Reg 0x31 bits 2:0 = 111

	// 8mA I/O pad drive strength
	temp = I2C_8Bit_md_read(14);
	temp = temp & ~(0x40);
	I2C_8Bit_md_write(14, temp);

	I2C_8Bit_md_write(0x11, 0x7);
	I2C_8Bit_md_write(0x21, 0x7);
	I2C_8Bit_md_write(0x31, 0x7);

	// TOS Priority enable at ports 1,2,3
	temp = I2C_8Bit_md_read(0x30);
	temp = temp | 0xc1;
	I2C_8Bit_md_write(0x30, temp);
	temp = I2C_8Bit_md_read(0x10);
	temp = temp | 0xc1;
	I2C_8Bit_md_write(0x10, temp);
	temp = I2C_8Bit_md_read(0x20);
	temp = temp | 0xc1;
	I2C_8Bit_md_write(0x20, temp);

	/* VID A Port membership = 110 (ports 3 & 2 only) */
	I2C_8Bit_md_write(131, 0xa);
	I2C_8Bit_md_write(130, 0x0);
	I2C_8Bit_md_write(129, 0xe);

	I2C_8Bit_md_write(121, 0x04);
	I2C_8Bit_md_write(122, 0x0);
	
	/* Disable VLAN, Reg 0x05 bit 7 = 0 */
	//I2C_8Bit_md_write( 0x5, 0x00); 

	/* Enable Learning Reg 0x12 bit 0 = 0 Reg 0x22 bit 0 = 0 */
	I2C_8Bit_md_write(0x12, 0x06);
	I2C_8Bit_md_write(0x22, 0x06);

	/* Keep Tail Tag to simplify Reg 0x03 bit 6 = 1 */
	I2C_8Bit_md_write(0x3, 0x74);

	// PAUSE disable
	temp = I2C_8Bit_md_read(0x3);
	temp = temp & ~0x30;
	I2C_8Bit_md_write(0x3, temp);

	//enable phy ports 1 & 2
	temp = I2C_8Bit_md_read(29);
	temp = temp & ~0x8;
	I2C_8Bit_md_write(29, temp);

	temp = I2C_8Bit_md_read(45);
	temp = temp & ~0x8;

	//restart auto negotiation
	temp = I2C_8Bit_md_read(45);
	temp = temp & ~0x8;
	I2C_8Bit_md_write(45, temp);

	I2C_8Bit_md_write(28, 0x9f);
	I2C_8Bit_md_write(44, 0x9f);

	temp = I2C_8Bit_md_read(29);
	temp = temp | 0x20;
	I2C_8Bit_md_write(29, temp);

	temp = I2C_8Bit_md_read(45);
	temp = temp | 0x20;
	I2C_8Bit_md_write(45, temp);
}

/*----------------------------------------------------------------------------*/
/*                           Init Tx Descriptor                               */
/*----------------------------------------------------------------------------*/
void emac_init_tx_desc (struct s_descriptor* TX_DESCS)
{
	unsigned char i;
	long int temp;

	temp = DWORD_SHIFT(0x1, 31) | // [31]    Interrupt Completion
	       DWORD_SHIFT(0x0, 30) | // [30]    Last Segment
	       DWORD_SHIFT(0x0, 29) | // [29]    First Segment
	       DWORD_SHIFT(0x0, 27) | // [28:27] Checksum Insertion Control
	       DWORD_SHIFT(0x0, 26) | // [26]    Disable CRC
	       DWORD_SHIFT(0x0, 25) | // [25]    Transmit End Of Ring
	       DWORD_SHIFT(0x0, 24) | // [24]    Second address Chained
	       DWORD_SHIFT(0x0, 23) | // [23]    Disable padding
	       DWORD_SHIFT(0x0, 11) | // [21:11] Trasmit Buffer 2 Size
	                   0x0;        // [10:0]  Trasmit Buffer 1 Size

	for (i=0; i<MAX_TX_DESCS; i++) {
		TX_DESCS[i].DES0 = 0x00000000; // The software is the owner of the descriptor
		TX_DESCS[i].DES1 = temp;

		TX_DESCS[i].DES2 = 0;
		TX_DESCS[i].DES3 = 0;
	}

	// Set Transmit End of Ring in the Last descriptor
	TX_DESCS[MAX_TX_DESCS-1].DES1 = TX_DESCS[MAX_TX_DESCS-1].DES1 | DWORD_SHIFT(0x1,25); // [25] Transmit End Of Ring;
}

/*----------------------------------------------------------------------------*/
/*                           Init Rx Descriptor                               */
/*----------------------------------------------------------------------------*/
void emac_init_rx_desc (struct s_descriptor* RX_DESCS)
{
	unsigned char i;
	long int temp;

	temp = DWORD_SHIFT(0x0, 31) | // [31]    Disable Interrupt On Completion
	       DWORD_SHIFT(0x0, 25) | // [25]    Receive End Of Ring
	       DWORD_SHIFT(0x0, 24) | // [24]    Second address Chained
	       DWORD_SHIFT(0, 11)		| // [21:11] Trasmit Buffer 2 Size
	                   MAX_PACKET_LEN;        // [10:0]  Trasmit Buffer 1 Size

	for (i=0; i < MAX_RX_DESCS; i++) {
		RX_DESCS[i].DES0 = 0x00000000;
		RX_DESCS[i].DES1 = temp;

		RX_DESCS[i].DES2 = 0;
		RX_DESCS[i].DES3 = 0;
	}

	// Set Receive End of Ring in the Last descriptor
	RX_DESCS[MAX_RX_DESCS-1].DES1 = RX_DESCS[MAX_RX_DESCS-1].DES1 | DWORD_SHIFT(0x1,25); // [25] Receive End Of Ring;
}


/*----------------------------------------------------------------------------*/
/*                        Emac Soft Reset                                     */
/*----------------------------------------------------------------------------*/
void emac_soft_rst(void)
{
	// Set Software reset bit. The new PHY configuration will be read.
	SetDword (EMAC_DMAR0_BUS_MODE_REG, 0x1);

	// Wait End of Reset
	while (GetDword (EMAC_DMAR0_BUS_MODE_REG) & 0x1);
}

/*----------------------------------------------------------------------------*/
/*                  The Emac start using the tx descriptors                   */
/*----------------------------------------------------------------------------*/
void emac_read_tx_des(void)
{
    SetDword(EMAC_DMAR1_TX_POLL_DEMAND_REG, 0x0);
}

/*----------------------------------------------------------------------------*/
/*                  The Emac start using the rx descriptors                   */
/*----------------------------------------------------------------------------*/
void emac_read_rx_des(void)
{
    SetDword(EMAC_DMAR2_RX_POLL_DEMAND_REG, 0x0);
}

/*----------------------------------------------------------------------------*/
/*                        Wait End of Frame Reception                         */
/*----------------------------------------------------------------------------*/
void emac_wait_frame_reception(void)
{
    // Polling
  while (!(GetDword(EMAC_DMAR5_STATUS_REG) & (DWORD_SHIFT(0x1, 6))));

    // Clear Receive Interrupt
  SetDword(EMAC_DMAR5_STATUS_REG, DWORD_SHIFT(0x1, 6));
}

/*----------------------------------------------------------------------------*/
/*                        Wait End of Frame Transmission                       */
/*----------------------------------------------------------------------------*/
void emac_wait_frame_transmission(void)
{
    // Polling
  while (!(GetDword(EMAC_DMAR5_STATUS_REG) & 0x1));

    // Clear Receive Interrupt
  SetDword(EMAC_DMAR5_STATUS_REG, 0x1);
}

/*----------------------------------------------------------------------------*/
/*                                   Init Emac                                */
/*----------------------------------------------------------------------------*/
void emac_cfg (unsigned char *mac_address, struct net_local *lp)
{
	long int temp;

	/***
	 *** Init: (see EthMAC datasheet paragraph 3.3.1,pp.46)
	 ***/

	/* a) DMA Register0 = ...
	 *    Bus Mode Register
	 *    Details:
	 *    -> DMA Register0.DA = 1; // set round-robin arbitration with priority at Rx during collisions
	 *    -> Set fixed bursts
	 *       DMA Register0, bit FB
	 *       and set maximum burst length (is necessary !!!!!):
	 *       Register0[13:8]=PBL field
	 */
	temp = DWORD_SHIFT(0x01, 25) |  // [25]    Address - Aligned Beats
	       DWORD_SHIFT(0x00, 24) |  // [24]    4xPBL Mode
	       DWORD_SHIFT(0x01, 23) |  // [23]    Use Separate PBL
	      // DWORD_SHIFT(0x04, 17) |  // [22:17] RxDMA PBL
	       DWORD_SHIFT(0x4, 17) |  // [22:17] RxDMA PBL
	       DWORD_SHIFT(0x01, 16) |  // [16]    Fixed Burst
	       DWORD_SHIFT(0x01, 14) |  // [15:14] Rx:Tx priority ratio
	       DWORD_SHIFT(0x10,  8) |  // [13:8]  Programmable Burst Length
	       DWORD_SHIFT(0x00,  2) |  // [6:2]   DSL : Descriptor Skip Length
	       DWORD_SHIFT(0x01,  1) |  // [1]	  DMA Arbitration scheme
	                   0x0;         // [0]    Software Reset
     
	SetDword(EMAC_DMAR0_BUS_MODE_REG, temp); 
  
	/* b) DMA Register7 = ...
	 *    Interrupt Enable Register
	 */
	temp = DWORD_SHIFT(0x1, 16) | // [16] Normal Interrupt Summary Enable
	       DWORD_SHIFT(0x1, 15) | // [15] Abnormal Interrupt Summary Enable
	       DWORD_SHIFT(0x1, 14) | // [14] Early Receive Interrupt Enable
	       DWORD_SHIFT(0x1, 13) | // [13] Fatal Bus Error Enable
	       DWORD_SHIFT(0x0, 10) | // [10] Early Transmit Interrupt Enable
	       DWORD_SHIFT(0x1,  9) | // [ 9] Receive Watchdog Timeout Enable
	       DWORD_SHIFT(0x1,  8) | // [ 8] Receive Stopped Enable
	       DWORD_SHIFT(0x1,  7) | // [ 7] Receive Buffer Unavailable Enable
	       DWORD_SHIFT(0x1,  6) | // [ 6] Receive Interrupt Enable
	       DWORD_SHIFT(0x1,  5) | // [ 5] Underflow Interrupt Enable
	       DWORD_SHIFT(0x1,  4) | // [ 4] Overflow Interrupt Enable
	       DWORD_SHIFT(0x1,  3) | // [ 3] Transmit Jabber Timeout Enable
	       DWORD_SHIFT(0x1,  2) | // [ 2] Transmit Buffer Unavailable Enable
	       DWORD_SHIFT(0x1,  1) | // [ 1] Transmit Stopped Enable
	                    0x1;        // [ 0] Transmit Interrupt Enable

	SetDword(EMAC_DMAR7_INT_ENABLE_REG, temp);

	/* c) Prepare Tx/Rx buffers
	 *    and update DMA Register3 ( Receive Descript List Address Register)
	 *               DMA Register4 ( Transmit Descriptor List Address Regsiter)
	 */
	SetDword(EMAC_DMAR3_RX_DESCRIPTOR_LIST_ADDR_REG, (unsigned long int)lp->RX_DESCS);
	SetDword(EMAC_DMAR4_TX_DESCRIPTOR_LIST_ADDR_REG, (unsigned long int)lp->TX_DESCS);

	/* d) set filtering options at
	 *    MAC Register1 (Frame Filter)
	 *    MAC Register2 (Reserved - Hash Disabled)
	 *    MAC Register3 (Reserved - Hash Disabled)
	 *    MAC Register 16 & 17 (MAC Address 0)
	 */
	temp = DWORD_SHIFT(0x0, 31) | // [31]  Receive All
	       DWORD_SHIFT(0x0,  9) | // [ 9]  Source Address Filter Enable
	       DWORD_SHIFT(0x0,  8) | // [ 8]  SA Inverse Filtering
	       DWORD_SHIFT(0x0,  6) | // [7:6] Pass Control Frames ( Forwards control frames that pass address filter)
	       DWORD_SHIFT(0x0,  5) | // [ 5] Disable Broadcast Frames
	       DWORD_SHIFT(0x1,  4) | // [ 4] Pass All Multicast
	       DWORD_SHIFT(0x0,  3) | // [ 3] DA Inverse Filtering
                     0x0;       // [ 0] Promiscuous Mode

	SetDword(EMAC_MACR1_FRAME_FILTER_REG, temp);

	SetDword(EMAC_MACR16_MAC_ADDR0_HIGH_REG, DWORD_SHIFT(mac_address[5], 8) | mac_address[4]);

	SetDword(EMAC_MACR17_MAC_ADDR0_LOW_REG, DWORD_SHIFT(mac_address[3], 24) |
                                          DWORD_SHIFT(mac_address[2], 16) |
	                                        DWORD_SHIFT(mac_address[1], 8) | mac_address[0]);

	/* e) configure and enable Tx/Rx operating modes
	 *    MAC Register0
	 *    (PS and DM bits are set based on the auto-negotion result read from the PHY)
	 */
	temp = DWORD_SHIFT(0x0, 31) | // [31]	Transmit Configuration in RGMII/SGMII
	       DWORD_SHIFT(0x1, 23) | // [23]	Watchdog Disable
	       DWORD_SHIFT(0x0, 22) | // [22]	Jabber Disable
	       DWORD_SHIFT(0x0, 21) | // [21]	Frame Burst Enable
	       DWORD_SHIFT(0x0, 20) | // [20]	Jombo Frame Enable
	       DWORD_SHIFT(0x0, 17) | // [19:17] Inter Frame Gap ( 96 bit time)
	       DWORD_SHIFT(0x0, 16) | // [16]	Disable Carrier Sense During Transmission
	       DWORD_SHIFT(0x1, 15) | // [15]	Port Select ( 10/100Mbps)
#ifdef CONFIG_SC14452_ES2
	       DWORD_SHIFT(0x0, 14) | // [14]	Speed (100Mbps) //vm
#else
				 DWORD_SHIFT(0x1, 14) | // [14]	Speed (100Mbps) //vm
#endif
	       DWORD_SHIFT(0x1, 13) | // [13]	Disable Receive Own
	       DWORD_SHIFT(0x0, 12) | // [12]	Loop Back Mode
	       DWORD_SHIFT(0x1, 11) | // [11]	Duplex Mode
	       DWORD_SHIFT(0x1, 10) | // [10]	Checksum offload
	       DWORD_SHIFT(0x0,  9) | // [ 9]	Disable Retry
	       DWORD_SHIFT(0x1,  8) | // [ 8]	Link Up/Down
	       DWORD_SHIFT(0x1,  7) | // [ 7]	Automatic Pad/CRC Stripping
	       DWORD_SHIFT(0x0,  5) | // [6:5]	Back-Off Limit
	       DWORD_SHIFT(0x0,  4) | // [ 4]	Deferral Check
	       DWORD_SHIFT(0x1,  3) | // [ 3]	Transmitter Enable
	       DWORD_SHIFT(0x1,  2) ; // [ 2]	Receiver Enable

	SetDword(EMAC_MACR0_CONFIG_REG, temp);

	/* Block MMC interrupts  and reset counters*/
	EMAC_MMC_INTR_MASK_RX_REG = 0xffffffff;
	EMAC_MMC_INTR_MASK_TX_REG = 0xffffffff;

	EMAC_MMC_CNTRL_REG = 0x5; 	//Reset at Read

	/* g) Start Tx/Rx
	 *    Operation Mode
	 *    DMA Register6, bits 13 and 1
	 *    Note: if bit ST (13) = 1 then EthMAC polls the Tx descriptor
	 */
	temp = DWORD_SHIFT(0x0, 26) | // [26]	Disable Dropping of TCP/IP checksum Error Frames
	       DWORD_SHIFT(0x0, 25) | // [25]	Receive Store and Forward
	       DWORD_SHIFT(0x0, 24) | // [24]	Disable Flushing of Received Frames
	       DWORD_SHIFT(0x0, 21) | // [21]	Transmit Store and Forward
	       DWORD_SHIFT(0x0, 20) | // [20]	Flush Transmit FIFO
	      // DWORD_SHIFT(0x0, 14) | // [16:14] Transmit Threshold Control (64 bytes)
	       DWORD_SHIFT(0x0, 14) | // [16:14] Transmit Threshold Control (64 bytes)
#ifdef CONFIG_SC14452_ES2
				 DWORD_SHIFT(0x0, 13) | // [13]	Start/Stop Transmission Command //vm
#else
	       DWORD_SHIFT(0x1, 13) | // [13]	Start/Stop Transmission Command //vm
#endif
	       DWORD_SHIFT(0x0,  7) | // [ 7]	Forward Error Frames
	       DWORD_SHIFT(0x0,  6) | // [ 6]	Forward Undersized Good Frames
	       //DWORD_SHIFT(0x1,  3) | // [4:3]	Receive Threshold Control (32 bytes)
	       DWORD_SHIFT(0x1,  3) | // [4:3]	Receive Threshold Control (32 bytes)
	       DWORD_SHIFT(0x1,  2) | // [ 2]	Operate on Second Frame /*201009 LGTwin Patch */
#ifdef CONFIG_SC14452_ES2
	       DWORD_SHIFT(0x0,  1) ; // [ 1]	Start/Stop Receive //vm
#else
				 DWORD_SHIFT(0x1,  1) ; // [ 1]	Start/Stop Receive //vm
#endif

	SetDword(EMAC_DMAR6_OPERATION_MODE_REG, temp);
}

int eth_init(struct net_device *dev)
{
	struct net_local *lp=dev->priv;

	// allocate memory for the rx and tx descriptors

#ifndef CONFIG_SC14452_ES2
	lp->TX_DESCS = (struct s_descriptor *) TX_DES_BASE;
	lp->RX_DESCS = (struct s_descriptor *) RX_DES_BASE;
#endif

	// Init the descriptors structures
	emac_init_tx_desc(lp->TX_DESCS);
	emac_init_rx_desc(lp->RX_DESCS);

	// Configure the pads for RMII interface
	emac_rmii_pads();

#ifdef CONFIG_SC14452_ES2
	emac_10mbps();
#else
	emac_100mbps();
#endif

	// Configure the clock for RMII interface and external clock
	emac_rmii_clk_ext();

	// Configure the pads for MDC interface
	emac_md_pads();

	// Enable EMAC clocks
	emac_clk_enable();

	// Software reset to read the PHY configuration
	emac_soft_rst();

#ifdef CONFIG_MICREL_ROUTER
	init_micrel_KSZ8873();
#else
	// RTL8306 init
	rtl8306_init();
#endif
	return 0;
}


#ifdef CONFIG_MICREL_ROUTER
//*********************************************************************/
//                      ACCESS1_Init																	//
//********************************************************************//
void ACCESS1_Init(void)
{
	SetBits(CLK_GPIO5_REG, SW_AB1_EN, 1);												// enable access1 bus Clock
	SetBits(CLK_GPIO5_REG, SW_AB1_DIV, 16);											// 10,368MHz/8
		
#if defined(CONFIG_L_V1_BOARD)
	SetPort(P1_01_MODE_REG, PORT_OUTPUT, GPIO_PID_SCL1);  			// MDC
	SetPort(P1_00_MODE_REG, PORT_OUTPUT, GPIO_PID_SDA1);				// MDIO
#elif  defined (CONFIG_L_V3_BOARD)
	SetPort(P0_09_MODE_REG, PORT_OUTPUT, GPIO_PID_SCL1);  			// MDC
	SetPort(P0_10_MODE_REG, PORT_OUTPUT, GPIO_PID_SDA1);				// MDIO
#elif  defined (CONFIG_L_V3_BOARD_CNX)
	SetPort(P0_09_MODE_REG, PORT_OUTPUT, GPIO_PID_SCL1);  			// MDC
	SetPort(P0_10_MODE_REG, PORT_OUTPUT, GPIO_PID_SDA1);				// MDIO
#elif  defined (CONFIG_L_V6_BOARD)
	SetPort(P0_09_MODE_REG, PORT_OUTPUT, GPIO_PID_SCL1);  			// MDC
	SetPort(P0_10_MODE_REG, PORT_OUTPUT, GPIO_PID_SDA1);				// MDIO
#endif				// MDIO
	
	SetBits(ACCESS1_CTRL_REG, SCK_SEL, 0x1);										// Further division of the SCL
	SetBits(ACCESS1_CTRL_REG, SCK_NUM, 1);											// 9 bits without ACK bits are generated, SDAx Open Drain
	SetBits(ACCESS1_CTRL_REG, SCL_OD,  0);											// Set ACCESS Bus SCL Push Pull
	SetBits(ACCESS1_CTRL_REG, SDA_OD,  1);											// Set ACCESS Bus SDA Open Drain
	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_INT, 1);								// enable interrupt for the access1 bus

	SetWord(ACCESS1_CLEAR_INT_REG, 1);     											/* Clear ACCESS1 interrupt in ctrl register*/

	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 0);								/* ---  Enable ACCESS1 BUS Procedure  --- */
	SetBits(ACCESS1_CTRL_REG, SDA_VAL, 1);											// SDA1 = 1
	SetBits(ACCESS1_CTRL_REG, SCL_VAL, 1);											// SCL1 = 1
	SetBits(ACCESS1_CTRL_REG, SDA_VAL, 0);											// SDA1 = 0
	SetBits(ACCESS1_CTRL_REG, SCL_VAL, 1);											// SCL1 = 1
	SetBits(ACCESS1_CTRL_REG, SDA_VAL, 0);											// SDA1 = 0
	SetBits(ACCESS1_CTRL_REG, SCL_VAL, 0);											// SCL1 = 0
	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 1);								// ENABLE Access1 BUS
}

//*********************************************************************/
//                      Stop Access BUS																//
//********************************************************************//
void StopACCESSBus(void)
{
	 /* Stop Condition */
	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 0);

	//Disable Access Bus
	SetBits(ACCESS1_CTRL_REG,SDA_VAL, 0);											// SDA1 = 0
	SetBits(ACCESS1_CTRL_REG,SCL_VAL, 0);											// SCL1 = 0
	SetBits(ACCESS1_CTRL_REG,SDA_VAL, 0);											// SDA1 = 0
	SetBits(ACCESS1_CTRL_REG,SCL_VAL, 1);											// SCL1 = 1
	SetBits(ACCESS1_CTRL_REG,SDA_VAL, 1);											// SDA1 = 1
	SetBits(ACCESS1_CTRL_REG,SCL_VAL, 1);											// SCL1 = 1
}

//*********************************************************************/
//                      Start Access BUS															//
//********************************************************************//
void StartACCESSBus(void)
{
	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 0);							/* ---  Enable ACCESS1 BUS Procedure  --- */
	SetBits(ACCESS1_CTRL_REG, SDA_VAL, 1);										// SDA1 = 1
	SetBits(ACCESS1_CTRL_REG, SCL_VAL, 1);										// SCL1 = 1
	SetBits(ACCESS1_CTRL_REG, SDA_VAL, 0);										// SDA1 = 0
	SetBits(ACCESS1_CTRL_REG, SCL_VAL, 1);										// SCL1 = 1
	SetBits(ACCESS1_CTRL_REG, SDA_VAL, 0);										// SDA1 = 0
	SetBits(ACCESS1_CTRL_REG, SCL_VAL, 0);										// SCL1 = 0
	SetBits(ACCESS1_CTRL_REG, EN_ACCESS_BUS, 1);							// ENABLE Access1 BUS

	// Clear any interrupt
	SetWord(ACCESS1_CLEAR_INT_REG, 0x1);
}

//******************************************************************//
//                      I2C_8Bit_md_write														//
//******************************************************************//
void I2C_8Bit_md_write(unsigned char reg_address, unsigned int wdata)
{
	StartACCESSBus();

	SetWord(ACCESS1_IN_OUT_REG, 0xBE);												// Write Command
  while(GetBits(ACCESS1_CTRL_REG, ACCESS_INT)==0) 	{}
  SetWord(ACCESS1_CLEAR_INT_REG, 0x00a1);

  SetWord(ACCESS1_IN_OUT_REG, reg_address);									// Write Register Address
  while(GetBits(ACCESS1_CTRL_REG, ACCESS_INT)==0) 	{}
  SetWord(ACCESS1_CLEAR_INT_REG, 0x00a1);

  SetWord(ACCESS1_IN_OUT_REG, wdata);												// Write Register Data
  while(GetBits(ACCESS1_CTRL_REG, ACCESS_INT)==0) 	{}
  SetWord(ACCESS1_CLEAR_INT_REG, 0x00a1);

	StopACCESSBus();
}

//******************************************************************//
//                      I2C_8Bit_md_read														//
//******************************************************************//
unsigned int I2C_8Bit_md_read(unsigned char reg_address)
{
	unsigned int tempr;

	StartACCESSBus();

  SetWord(ACCESS1_IN_OUT_REG, 0xBE);													// Write Command
  while(GetBits(ACCESS1_CTRL_REG, ACCESS_INT)==0){}
  SetWord(ACCESS1_CLEAR_INT_REG, 0x00a1);

  SetByte(ACCESS1_IN_OUT_REG, reg_address);										// Write Register Address
  while(GetBits(ACCESS1_CTRL_REG, ACCESS_INT)==0){}
  SetWord(ACCESS1_CLEAR_INT_REG, 0x00a1);

	StartACCESSBus();

	SetByte(ACCESS1_IN_OUT_REG, 0xBF);													// Read Command
	while(GetBits(ACCESS1_CTRL_REG, ACCESS_INT)==0) {}
  SetWord(ACCESS1_CLEAR_INT_REG, 0x00a1);

	// Enable transfer of nACK from SC14452 to Switch
	SetBits(ACCESS1_CTRL_REG, ACKn, 1);

	while(GetBits(ACCESS1_CTRL_REG, ACCESS_INT)==0) {}					// Read the Register
  tempr = GetWord(ACCESS1_IN_OUT_REG);
  SetWord(ACCESS1_CLEAR_INT_REG, 0x00a1);

	StopACCESSBus();

	return (tempr);
}
#endif


#ifdef EMAC_TEST_MODE
/*----------------------------------------------------------------------------*/
/*                       Prepare Rx Descriptors                               */
/*----------------------------------------------------------------------------*/
void emac_rx_des_prepare(void)
{
	unsigned char i;

	for (i=0; i<emacDev.num_of_rx_des; i++)
		emacDev.rx_descriptor[i].DES0 = 0x80000000; // Set OWN bit. The descriptor is owned by the DMA of the EMAC.

}

/*-----------------------------------------------------------------------------------------*/
/* Reads the Length of the received frame and checks if the descriptor structure is valid. */
/* They don't make any change to the descriptors.                                          */
/*-----------------------------------------------------------------------------------------*/
long int emac_get_rx_len(void)
{
	unsigned char i = 0;
	unsigned long int frame_buf_pos = 0;
	unsigned long int frame_len = 0;
	long int des0;

	while (i<emacDev.num_of_rx_des) {

		des0 = emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES0;

		// Check Own bit
		if (des0 & DWORD_SHIFT(0x1,31))
			return (-1);

		// Check First Descriptor bit
		if (i==0 && !(des0 & DWORD_SHIFT(0x1,9)))
			return (-1);

		// Check Last Descriptor bit
		if (des0 & DWORD_SHIFT(0x1,8)) {

			frame_len = (des0 >> 16) & 0x3fff;

			// Check Error Summary
			if (des0  & DWORD_SHIFT(0x1,15))
				return(-1);

			if ( (frame_len < frame_buf_pos) ||
			    ((frame_len - frame_buf_pos) > 2*emacDev.rx_buf_size))
				return (-1);


			return (frame_len);
		} else
			frame_buf_pos += 2*emacDev.rx_buf_size;

		i++;
	}
    return (-1);
}

/*----------------------------------------------------------------------------*/
/*  Copy the received data to a new buffer and release the Rx Descriptors     */
/*----------------------------------------------------------------------------*/
unsigned long int emac_get_rx_data(unsigned long int *frame_buf)
{
	unsigned char i = 0;

	long int des0;
	unsigned long int frame_buf_pos = 0;
	unsigned long int frame_len = 0;
	unsigned int dma_len =0;

	while (i<emacDev.num_of_rx_des) {

		des0 = emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES0;

		// Set Own bit
		emacDev.rx_descriptor[(emacDev.rx_ptr + i) % emacDev.num_of_rx_des].DES0 = 0x80000000;

		// Check Last Descriptor bit
		if (des0 & DWORD_SHIFT(0x1,8)) {

			frame_len = (des0 >> 16) & 0x3fff;

			// Copy the data from buffer 1 to the frame buffer using the dma engine.
			if ((frame_len - frame_buf_pos) >= emacDev.rx_buf_size)
				dma_len = emacDev.rx_buf_size;
			else
				dma_len = (frame_len - frame_buf_pos);

			frame_buf_pos += dma_len;

			if (frame_len > frame_buf_pos) {
				// Copy the data from buffer 2 to the frame buffer using the dma engine.

				dma_len = (frame_len - frame_buf_pos);

				frame_buf_pos += dma_len;
			}

			emacDev.rx_ptr = (emacDev.rx_ptr + i + 1) % emacDev.num_of_rx_des;

			return (frame_len);
		} else {
			// Copy the data from buffer 1 to the frame buffer using the dma engine.

			dma_len = emacDev.rx_buf_size;

			frame_buf_pos += dma_len;

			frame_buf_pos += dma_len;

			i++;
		}
	}

	return (frame_buf_pos);
}

/*----------------------------------------------------------------------------*/
/*            Calculates the number of the free tx descriptors                */
/*----------------------------------------------------------------------------*/
unsigned char emac_tx_des_calc (void)
{
	unsigned char i;

	for (i=0; i<emacDev.num_of_tx_des; i++)
		if (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES0 & 0x80000000)
			return (i);

	return (emacDev.num_of_tx_des);
}

/*----------------------------------------------------------------------------*/
/*            Calculates the total size of the free tx buffers                */
/*----------------------------------------------------------------------------*/
unsigned long int emac_tx_buf_calc (void)
{
	return (2* emac_tx_des_calc() * emacDev.tx_buf_size);
}

/*----------------------------------------------------------------------------*/
/*            Write a Frame to the Tx Descriptors                             */
/*----------------------------------------------------------------------------*/
unsigned long int emac_send_tx_data(unsigned long int frame_size, unsigned long int *frame_buf)
{
	unsigned char i = 0, k;
	unsigned long int frame_buf_pos = 0;
	unsigned int dma_len = 0;

	while (i < emacDev.num_of_tx_des) {

		if (i==0)
			// This is the first Descriptor. Sets the First Segment bit and clears the Last Segment bit
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 | DWORD_SHIFT(0x1,29) ) & (~DWORD_SHIFT(0x1,30));
		else
			// Clear the First Segment bit and the Last Segment bit
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & ~(DWORD_SHIFT(0x1,29) | DWORD_SHIFT(0x1,30));

		// Copy the data from frame buffer to the buffer 1 using the dma engine.
		if ((frame_size - frame_buf_pos) >= emacDev.tx_buf_size)
			dma_len = emacDev.tx_buf_size;
		else
			dma_len = frame_size - frame_buf_pos;

		// Write the size of buffer 1
		emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & (~0x7ff)) | dma_len;

		frame_buf_pos += dma_len;

		// Copy the data from frame buffer to the buffer 2 using the dma engine if is necessary.
		if (frame_size > frame_buf_pos) {
       			if ((frame_size -  frame_buf_pos) >= emacDev.tx_buf_size)
				dma_len = emacDev.tx_buf_size;
			else
				dma_len = frame_size - frame_buf_pos;

			// Write the size of buffer 2
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = (emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & (~0x3ff800)) | DWORD_SHIFT (dma_len, 11);

			frame_buf_pos += dma_len;
		} else
			// Clear the size of buffer 2
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 & (~0x3ff800);

		if (frame_size == frame_buf_pos) {
			// Set the Last Segment bit to the Last descriptor
			emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 = emacDev.tx_descriptor[(emacDev.tx_ptr + i) % emacDev.num_of_tx_des].DES1 | DWORD_SHIFT(0x1,30);

			// -------------------- Set Own Bit ------------------------------------//
			for (k=0; k<=i; k++) {
				emacDev.tx_descriptor[(emacDev.tx_ptr + k) % emacDev.num_of_tx_des].DES0 = 0x80000000;
			}

			// -------------------- Update TxPtr Value -----------------------------//
			emacDev.tx_ptr = (emacDev.tx_ptr + i + 1) % emacDev.num_of_tx_des;

			return (frame_buf_pos);
		}
		i++;
	}
	return (frame_buf_pos);

}

void emac_rxevent(void)
{
	long int len;

	while ( (len = emac_get_rx_len()) > 0) {

		if (len>=RX_FRAME_SIZE) {
			printk("EMAC452: packet too big\n");
			return;
		}
		if (RxFrames >= MAX_RX_FRAMES) {
			return;
		}
//		RxFrameLen[RxFrames] = (int ) emac_get_rx_data((unsigned long int *) &RxFrameBuf[RxFrames * RX_FRAME_SIZE]);
		RxFrames++;
	}
}

void emac_rxoverflow(void)
{
	printk("losing packets in rx\n");

	// Take all the good frames
	emac_rxevent();

	// Clear all the rx descriptors
	emac_rx_des_prepare();

	// Force the emac to read again the rx descriptors
	emac_read_rx_des();

}


void emac_txevent(void)
{
	TxEnd = 1;
}

void emac_poll(void)
{
	long int status;

	while ((status = (GetDword(EMAC_DMAR5_STATUS_REG) & 0x1ffff)) != 0) {

		// Receive Overflow
		if (status & DWORD_SHIFT(0x1, 4)) {
			emac_rxoverflow();
			SetDword(EMAC_DMAR5_STATUS_REG, DWORD_SHIFT(0x1, 4));
		}

		// End of frame transmission
		if (status & 0x1) {
			emac_txevent();
			SetDword(EMAC_DMAR5_STATUS_REG, 0x1);
		}

		// End of frame reception
		if (status & DWORD_SHIFT(0x1, 6)) {
			emac_rxevent();
			SetDword(EMAC_DMAR5_STATUS_REG, DWORD_SHIFT(0x1, 6));
		}

		// Clear all other interrupts
		SetDword(EMAC_DMAR5_STATUS_REG, 
						 DWORD_SHIFT(0x1, 16) |
						 DWORD_SHIFT(0x1, 15) |
						 DWORD_SHIFT(0x1, 14) | 
		         DWORD_SHIFT(0x1, 13) |
						 DWORD_SHIFT(0x1, 10) |
						 DWORD_SHIFT(0x1, 9)  |
						 DWORD_SHIFT(0x1, 8)  |
						 DWORD_SHIFT(0x1, 7)  |
						 DWORD_SHIFT(0x1, 5)  |
						 DWORD_SHIFT(0x1, 3)  |
						 DWORD_SHIFT(0x1, 2)  |
						 DWORD_SHIFT(0x1, 1)  );
	}
}

void eth_halt()
{
}

int eth_rx()
{
	int j, tmo;

	PRINTK("### eth_rx\n");

	while(1) {
		emac_poll();
		if (RxFrames > 0) {
			PRINTK("RxFrames %02x \n", RxFrames);
			RxFrames = 0;
			return 1;
		}
	}
	return 0;
}

int eth_send(volatile void *packet, int length)
{
	int tmo;

	PRINTK("### eth_send\n");

	TxEnd = -1;

	emac_send_tx_data(length, (unsigned long int *) packet);

	emac_read_tx_des();

	while(1) {
		emac_poll();
		if (TxEnd != -1) {
			PRINTK("Packet sucesfully sent\n");
			return 0;
		}
	}
	return 0;
}
#endif // EMAC_TEST_MODE

#endif // SC14452_MICREL_ROUTER_H