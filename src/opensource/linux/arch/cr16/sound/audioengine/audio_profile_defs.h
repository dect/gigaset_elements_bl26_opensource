/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

#if !defined( __AUDIO_PROFILES_DEFS_H__ )
#define __AUDIO_PROFILES_DEFS_H__


#define AUDIO_PROFILE_MAGIC_AP10	0x30315041	/* ASCII: "AP10" */
#define AUDIO_PROFILE_MAGIC_AP11	0x31315041	/* ASCII: "AP11" */
#define AUDIO_PROFILE_MAGIC_AP12	0x32315041	/* ASCII: "AP12" */
#define AUDIO_PROFILE_MAGIC		AUDIO_PROFILE_MAGIC_AP12

#define FILTER_SIZE 7


typedef struct sc1445x_audio_profile_header_t {
	unsigned magic ;
	unsigned short vmic_vspk_count ;
	unsigned short filter_size ;
	unsigned features ;
} sc1445x_audio_profile_header ;

#define DECLARE_DEFAULT_AP_HEADER_WITH_MAGIC( name, _magic ) 		\
	sc1445x_audio_profile_header name = {				\
		.magic =		_magic,				\
		.vmic_vspk_count =	SC1445x_AE_IFACE_MODE_COUNT_IN_AP,\
		.filter_size =		FILTER_SIZE,			\
		.features =		0				\
	}

#define DECLARE_DEFAULT_AP_HEADER( name ) \
	DECLARE_DEFAULT_AP_HEADER_WITH_MAGIC( name, AUDIO_PROFILE_MAGIC )

#if defined( CONFIG_SND_SC1445x_USE_PAEC )
/* uncomment if the DSP1 fw uses a PAEC suppressor; comment if it uses NLP */
#  define HAVE_PAEC_SUPPRESSOR
#endif

/* custom parameters for the vspks */
#define DECLARE_VSPK_CUSTOM_PARAMS_STRUCTS( COUNT )				\
struct vspk_custom_param {							\
	/* enable flag */							\
	unsigned short use ;							\
										\
	/*									\
	 * DSP address or DSP cmd to use to set the parameter:			\
	 *									\
	 * addr <  0x10000 --> use addr as DSP cmd				\
	 * addr >= 0x10000 --> use addr as absolute address			\
	 *									\
	 */									\
	unsigned addr ;								\
										\
	/* parameter values */							\
	unsigned short values[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;		\
} ;										\
										\
struct vspk_custom_params {							\
	/* number of custom parameters */					\
	/*unsigned short count ;*/							\
										\
	/* the custom parameters [0 ... (COUNT - 1)] */				\
	struct vspk_custom_param params[COUNT] ;				\
} 

/* enable and 16-bit volume value for each of the speaker volume knobs */
#define DECLARE_VSPK_VOLUMES_STRUCT 						\
struct vspk_volumes {								\
	/* enable flags */							\
	unsigned int use_analog		: 1 ;	/* CODEC_LSR_REG.LSRATT */	\
	unsigned int use_rx_path_att	: 1 ;	/* DSP cmd 0x0008, 0-0x7fff */	\
	unsigned int use_rx_path_shift	: 1 ;	/* DSP cmd 0x0020, 0-4 */	\
	unsigned int use_sidetone_att	: 1 ;	/* DSP cmd 0x000a, 0-0x7fff */	\
	unsigned int use_ext_spk_att	: 1 ;	/* DSP cmd 0x0013, 0-0x7fff */	\
	unsigned int use_shift_ext_spk	: 1 ;	/* DSP cmd 0x0017, 0-4 */	\
	unsigned int use_tone_vol	: 1 ;	/* DSP cmd 0x9/0x7, 0-0x7fff */	\
	unsigned int use_classd_vout	: 1 ;	/* CLASSD_CTRL_REG.CLASSD_VOUT */ \
	unsigned int use_pcm_gain_pre	: 1 ;	/* DSP cmd 0x002a, 0-0x7fff */	\
	unsigned int use_pcm_gain_shift	: 1 ;	/* DSP cmd 0x002c, signed */	\
	/*unsigned int use_pcm_gain_post	: 1 ;*/	/* DSP cmd 0x002b, 0-0x7fff */	\
	unsigned int use_loopgain_rxpl	: 1 ;	/* DSP cmd 0x0033, 0-0x7fff */	\
	unsigned int use_ng_threshold	: 1 ;	/* DSP cmd 0x0036, 0-0x7fff */	\
										\
	/* volume values (for all available levels) */				\
	unsigned short analog_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;		\
	unsigned short rx_path_att_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;	\
	unsigned short rx_path_shift_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;	\
	unsigned short sidetone_att_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;	\
	unsigned short ext_spk_att_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;	\
	/*unsigned short ext_spk_att_ring_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;*/\
	unsigned short ring_playback_vol_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;\
	unsigned short shift_ext_spk_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;	\
	/*unsigned short tone_vol_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;*/	\
	unsigned short ring_tone_vol_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;	\
	unsigned short call_tone_codec_vol_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;\
	unsigned short call_tone_classd_vol_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;\
	unsigned short dtmf_tone_codec_vol_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;\
	unsigned short dtmf_tone_classd_vol_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;\
	unsigned short classd_vout[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;		\
	unsigned short pcm_gain_pre[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;		\
	unsigned short pcm_gain_shift[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;	\
	/*unsigned short pcm_gain_post[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;*/		\
	unsigned short pcm_gain_pre_ringtone[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;	\
	unsigned short pcm_gain_shift_ringtone[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;\
	unsigned short loopgain_rxpl_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;	\
	unsigned short ng_threshold_levels[SC1445x_AE_VSPK_VOL_LEVEL_COUNT] ;	\
										\
	/* custom parameters */							\
	struct vspk_custom_params cp ;						\
}

/* enable and 16-bit volume value for each of the microphone gain knobs */
#if defined( CONFIG_SND_SC1445x_USE_PAEC )

/* custom parameters for the vmics */
#define DECLARE_VMIC_CUSTOM_PARAMS_STRUCTS( COUNT )				\
struct vmic_custom_param {							\
	/* enable flag */							\
	unsigned short use ;							\
										\
	/*									\
	 * DSP address or DSP cmd to use to set the parameter:			\
	 *									\
	 * addr <  0x10000 --> use addr as DSP cmd				\
	 * addr >= 0x10000 --> use addr as absolute address			\
	 *									\
	 */									\
	unsigned addr ;								\
										\
	/* parameter values */							\
	unsigned short values[SC1445x_AE_VMIC_GAIN_LEVEL_COUNT] ;		\
} ;										\
										\
struct vmic_custom_params {							\
	/* number of custom parameters */					\
	/*unsigned short count ;*/							\
										\
	/* the custom parameters [0 ... (COUNT - 1)] */				\
	struct vmic_custom_param params[COUNT] ;				\
} 

#define DECLARE_VMIC_GAINS_STRUCT						\
struct vmic_gains {								\
	/* enable flags */							\
	unsigned int use_analog		: 1 ;	/* CODEC_MIC_REG.MIC_GAIN */	\
	unsigned int use_shift_paec_out : 1 ;	/* DSP cmd 0x001A, signed */	\
	unsigned int use_paec_tx_att	: 1 ;	/* DSP cmd 0x001B, 0-0x7fff */	\
	unsigned int use_attlimit	: 1 ;	/* @0x10770, 0-0x7fff */	\
	unsigned int use_supmin		: 1 ;	/* @0x12b86, 0-0x7fff */	\
	unsigned int use_noiseattlimit	: 1 ;	/* @0x10784, 0-0x7fff */	\
										\
	/* gain values (for all available levels) */				\
	unsigned short analog_levels[SC1445x_AE_VMIC_GAIN_LEVEL_COUNT] ;	\
	unsigned short shift_paec_out_levels[SC1445x_AE_VMIC_GAIN_LEVEL_COUNT] ;\
	unsigned short shift_paec_out_levels_BT[SC1445x_AE_VMIC_GAIN_LEVEL_COUNT] ;\
	unsigned short paec_tx_att_levels[SC1445x_AE_VMIC_GAIN_LEVEL_COUNT] ;	\
	unsigned short paec_tx_att_levels_BT[SC1445x_AE_VMIC_GAIN_LEVEL_COUNT] ;\
	unsigned short attlimit[SC1445x_AE_VMIC_GAIN_LEVEL_COUNT] ;		\
	unsigned short supmin[SC1445x_AE_VMIC_GAIN_LEVEL_COUNT] ;		\
	unsigned short noiseattlimit[SC1445x_AE_VMIC_GAIN_LEVEL_COUNT] ;	\
										\
	/* custom parameters */							\
	struct vmic_custom_params cp ;						\
}

#else

#define DECLARE_VMIC_GAINS_STRUCT						\
struct vmic_gains {								\
	/* enable flags */							\
	unsigned int use_analog		: 1 ;	/* CODEC_MIC_REG.MIC_GAIN */	\
										\
	/* gain values (for all available levels) */				\
	unsigned short analog_levels[SC1445x_AE_VMIC_GAIN_LEVEL_COUNT] ;	\
}

#endif


#endif  /* __AUDIO_PROFILES_DEFS_H__ */

