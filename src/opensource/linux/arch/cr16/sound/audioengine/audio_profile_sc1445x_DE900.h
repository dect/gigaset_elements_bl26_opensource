/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

#if !defined( __AUDIO_PROFILE_SC1445x_DE900_H__ )
#define __AUDIO_PROFILE_SC1445x_DE900_H__
#define PAECv4


//Flamis 290910 equalizing mic gain for WB and NB


/* the number of available volume levels for virtual speakers */
#define SC1445x_AE_VSPK_VOL_LEVEL_COUNT		10

/* the number of available gain levels for virtual microphones */
#define SC1445x_AE_VMIC_GAIN_LEVEL_COUNT	10

DECLARE_VSPK_CUSTOM_PARAMS_STRUCTS( 0 ) ;
DECLARE_VSPK_VOLUMES_STRUCT ;
DECLARE_VMIC_CUSTOM_PARAMS_STRUCTS( 0 ) ;
DECLARE_VMIC_GAINS_STRUCT ;


/****************************************************************************************************/
/*************************** NARROWBAND RECEIVE DIRECTION VOLUME LEVELS *****************************/
/****************************************************************************************************/
/* There are five modes of operation available. The proper path of the SC14450 Gen2DSP and the 		*/
/* analog audio circuities are enabled according to the HANDSET, OPEN_LISTINING, HANDsFREE,	HEADSET	*/
/* and WIRELESS_HANDSET. Eight programmable gain steps are available for volume control	of each 	*/
/* mode. Amplification as well as attenuation units form the volume control section in order to 	*/
/* offer the best tuning flexibility. The following units are controlled through this process:		*/
/* 1) analog		--> control the receiver analog gain through CODEC_LSR_REG.LSRATT				*/
/* 2) rx_path_att	--> control the receiver digital attenuation through DSP cmd 0x0008, 0-0x7fff	*/
/* 3) rx_path_shift	--> control the receiver digital gain through DSP cmd 0x0020, 0-4				*/
/* 4) sidetone_att	--> control the sidetone attenuation through DSP cmd 0x000A, 0-0x7fff			*/
/* 5) ext_spk_att	--> control the speaker digital attenuation through DSP cmd 0x0013, 0-0x7fff	*/
/* 6) shift_ext_spk --> control the speaker digital gain through DSP cmd 0x0017, 0-4				*/
/****************************************************************************************************/

static struct vspk_volumes
profile_vspk_levels_narrowband[SC1445x_AE_IFACE_MODE_COUNT_IN_AP] = {
	
/********************************** HANDSET *********************************************************/
	{
		.use_analog 		=	1,
		.use_rx_path_att 	=	1,
		.use_rx_path_shift 	=	1,
		.use_sidetone_att 	= 	1,
		.use_ext_spk_att 	=	0,
		.use_shift_ext_spk 	=	0,
		.use_tone_vol 		=	1,
		.use_classd_vout	=	0,
		.use_pcm_gain_pre 	= 0,
		.use_pcm_gain_shift = 0,
		.use_loopgain_rxpl  = 0, 
		.use_ng_threshold	= 0,

		.analog_levels					= { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 },
		.rx_path_att_levels 			= { 0x18FC, 0x229E, 0x2FF9, 0x427A,
											0x5999, 0x7FFF, 0x5999, 0x7FFF,
											0x5999, 0x7FFF	},
		.rx_path_shift_levels 			= { 0, 0, 0, 0, 0, 0, 1, 1, 2, 2 },
		.sidetone_att_levels 			= { 0x1000, 0x1000, 0x1000, 0x1000, 
											0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000	},	
		.call_tone_codec_vol_levels 	= { 0x12EC, 0x1764, 0x1CE8, 0x23BB,
											0x2C29, 0x3694, 0x4375, 0x5360,
											0x670C, 0x7F5D },
		.dtmf_tone_codec_vol_levels 	= { 0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000 },
		.pcm_gain_pre 					= { 0x1000, 0x2000, 0x3000, 0x4000, 0x5000, 0x6000, 0x7000, 0x7fff,
											0x5000, 0x6000},
		.pcm_gain_shift 				= { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1},
		.pcm_gain_pre_ringtone 			= { 0x0100, 0x0200, 0x02e0,0x03F0, 0x058A, 0x0800, 0x0BD9, 0x1214,  
											0x171A, 0x1FDB },
		.pcm_gain_shift_ringtone 		= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		.loopgain_rxpl_levels			= { 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000,
											0x5000, 0x5000 },
		.ng_threshold_levels			= { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
											0x0000, 0x0000 },									
	},
/********************************** OPEN_LISTINING **************************************************/
	{
		.use_analog 		=	1,
		.use_rx_path_att 	=	1,
		.use_rx_path_shift 	=	1,
		.use_sidetone_att 	= 	1,
		.use_ext_spk_att 	=	1,
		.use_shift_ext_spk 	=	1,
		.use_tone_vol 		=	1,
		.use_classd_vout	=	1,
		.use_pcm_gain_pre 	= 0,
		.use_pcm_gain_shift = 0,
		.use_loopgain_rxpl  = 0, 
		.use_ng_threshold	= 0,

		.analog_levels					= { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 },
		.rx_path_att_levels 			= { 0x18FC, 0x229E, 0x2FF9, 0x427A,
											0x5999, 0x7FFF, 0x5999, 0x7FFF,
											0x5999, 0x7FFF	},
		.rx_path_shift_levels 			= { 0, 0, 0, 0, 0, 0, 1, 1, 2, 2 },
		.sidetone_att_levels 			= { 0x1000, 0x1000, 0x1000, 0x1000, 
											0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000	},											
		.ext_spk_att_levels				= { 0x2FF9, 0x427A, 0x5AAA, 0x7FFF,
											0x5AAA, 0x7FFF, 0x5AAA, 0x7FFF,
											0x5AAA, 0x7FFF },
		.shift_ext_spk_levels 			= { 0, 0, 0, 0, 1, 1, 2, 2, 3, 3 },		
		.ring_playback_vol_levels 		= { 0x2FF9, 0x427A, 0x5AAA, 0x7FFF,
											0x5AAA, 0x7FFF, 0x5AAA, 0x7FFF,
											0x5AAA, 0x7FFF },
		.ring_tone_vol_levels 			= { 0x12EC, 0x1764, 0x1CE8, 0x23BB,
											0x2C29, 0x3694, 0x4375, 0x5360,
											0x670C, 0x7F5D },
		.call_tone_codec_vol_levels 	= { 0x12EC, 0x1764, 0x1CE8, 0x23BB,
											0x2C29, 0x3694, 0x4375, 0x5360,
											0x670C, 0x7F5D },
		.call_tone_classd_vol_levels 	= { 0x12EC, 0x1764, 0x1CE8, 0x23BB,
											0x2C29, 0x3694, 0x4375, 0x5360,
											0x670C, 0x7F5D },
		.dtmf_tone_codec_vol_levels 	= { 0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000 },
		.dtmf_tone_classd_vol_levels 	= { 0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000 },
		.classd_vout					= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	},
/********************************** HANDsFREE *******************************************************/
	{
		.use_analog 		=	0,
		.use_rx_path_att 	=	0,
		.use_rx_path_shift 	=	0,
		.use_sidetone_att 	= 	0,
		.use_ext_spk_att 	=	1,
		.use_shift_ext_spk 	=	1,
		.use_tone_vol 		=	1,
		.use_classd_vout	=	1,
		.use_pcm_gain_pre 	= 0,
		.use_pcm_gain_shift = 0,
		.use_loopgain_rxpl  = 0, 
		.use_ng_threshold	= 0,

		.ext_spk_att_levels				= { 0x2FF9, 0x427A, 0x5AAA, 0x7FFF,
											0x5AAA, 0x7FFF, 0x5AAA, 0x7FFF,
											0x5AAA, 0x7FFF },
		.shift_ext_spk_levels 			= { 0, 0, 0, 0, 1, 1, 2, 2, 3, 3 },											
		.ring_playback_vol_levels 		= { 0x2FF9, 0x427A, 0x5AAA, 0x7FFF,
											0x5AAA, 0x7FFF, 0x5AAA, 0x7FFF,
											0x5AAA, 0x7FFF },
		.ring_tone_vol_levels 			= { 0x12EC, 0x1764, 0x1CE8, 0x23BB,
											0x2C29, 0x3694, 0x4375, 0x5360,
											0x670C, 0x7F5D },
		.call_tone_classd_vol_levels 	= { 0x12EC, 0x1764, 0x1CE8, 0x23BB,
											0x2C29, 0x3694, 0x4375, 0x5360,
											0x670C, 0x7F5D },
		.dtmf_tone_classd_vol_levels 	= { 0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000 },
		.classd_vout					= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },									
	},
/********************************** HEADSET *******************************************************/
/* NOTE: It has not been tuned */
	{
		.use_analog 		=	1,
		.use_rx_path_att 	=	1,
		.use_rx_path_shift 	=	1,
		.use_sidetone_att 	= 	1,
		.use_ext_spk_att 	=	0,
		.use_shift_ext_spk 	=	0,
		.use_tone_vol 		=	1,
		.use_classd_vout	=	0,
		.use_pcm_gain_pre 	= 0,
		.use_pcm_gain_shift = 0,
		.use_loopgain_rxpl  = 0, 
		.use_ng_threshold	= 0,

		.analog_levels					= { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 },
		.rx_path_att_levels 			= { 0x18FC, 0x229E, 0x2FF9, 0x427A,
											0x5999, 0x7FFF, 0x5999, 0x7FFF,
											0x5999, 0x7FFF	},
		.rx_path_shift_levels 			= { 0, 0, 0, 0, 0, 0, 1, 1, 2, 2 },
		.sidetone_att_levels 			= { 0x1000, 0x1000, 0x1000, 0x1000, 
											0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000	},	
		.call_tone_codec_vol_levels 	= { 0x12EC, 0x1764, 0x1CE8, 0x23BB,
											0x2C29, 0x3694, 0x4375, 0x5360,
											0x670C, 0x7F5D },
		.dtmf_tone_codec_vol_levels 	= { 0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000 },
	},
/********************************** WIRELESS_HANDSET ************************************************/
/* NOTE: This is a dummy mode that disables any volume control since it is handled from the			*/
/* wireless handset device																			*/
	{
		.use_analog 		=	0,
		.use_rx_path_att 	=	0,
		.use_rx_path_shift 	=	0,
		.use_sidetone_att 	= 	0,
		.use_ext_spk_att 	=	0,
		.use_shift_ext_spk 	=	0,
		.use_tone_vol 		=	1,
		.use_classd_vout	=	0,
		.use_pcm_gain_pre 	= 0,
		.use_pcm_gain_shift = 0,
		.use_loopgain_rxpl  = 0, 
		.use_ng_threshold	= 0,

		.call_tone_codec_vol_levels 	= { 0x3000, 0x3000, 0x3000, 0x3000,
											0x3000, 0x3000, 0x3000, 0x3000,
											0x3000, 0x3000 },
		.dtmf_tone_codec_vol_levels 	= { 0x3000, 0x3000, 0x3000, 0x3000,
											0x3000, 0x3000, 0x3000, 0x3000,
											0x3000, 0x3000 },
	}	
} ;

/****************************************************************************************************/
/**************************** WIDEBAND RECEIVE DIRECTION VOLUME LEVELS ******************************/
/****************************************************************************************************/
/* There are five modes of operation available. The proper path of the SC14450 Gen2DSP and the 		*/
/* analog audio circuities are enabled according to the HANDSET, OPEN_LISTINING, HANDsFREE,	HEADSET	*/
/* and WIRELESS_HANDSET. Eight programmable gain steps are available for volume control	of each 	*/
/* mode. Amplification as well as attenuation units form the volume control section in order to 	*/
/* offer the best tuning flexibility. The following units are controlled through this process:		*/
/* 1) analog		--> control the receiver analog gain through CODEC_LSR_REG.LSRATT				*/
/* 2) rx_path_att	--> control the receiver digital attenuation through DSP cmd 0x0008, 0-0x7fff	*/
/* 3) rx_path_shift	--> control the receiver digital gain through DSP cmd 0x0020, 0-4				*/
/* 4) sidetone_att	--> control the sidetone attenuation through DSP cmd 0x000A, 0-0x7fff			*/
/* 5) ext_spk_att	--> control the speaker digital attenuation through DSP cmd 0x0013, 0-0x7fff	*/
/* 6) shift_ext_spk --> control the speaker digital gain through DSP cmd 0x0017, 0-4				*/
/****************************************************************************************************/

static struct vspk_volumes
profile_vspk_levels_wideband[SC1445x_AE_IFACE_MODE_COUNT_IN_AP] = {

/********************************** HANDSET *********************************************************/
	{
		.use_analog 		=	1,
		.use_rx_path_att 	=	1,
		.use_rx_path_shift 	=	1,
		.use_sidetone_att 	= 	1,
		.use_ext_spk_att 	=	0,
		.use_shift_ext_spk 	=	0,
		.use_tone_vol 		=	1,
		.use_classd_vout	=	0,
		.use_pcm_gain_pre 	= 0,
		.use_pcm_gain_shift = 0,
		.use_loopgain_rxpl  = 0, 
		.use_ng_threshold	= 0,
	
		.analog_levels 				= { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
		.rx_path_att_levels 		= { 0x0CC0, 0x1207, 0x18FC, 0x229E, 
										0x2FF9, 0x427A, 0x5999, 0x7FFF,
										0x5999, 0x7FFF	 },
		.rx_path_shift_levels 		= { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 },
		.sidetone_att_levels 		= { 0x1000, 0x1000, 0x1000, 0x1000,
										0x1000, 0x1000, 0x1000, 0x1000,
										0x1000, 0x1000	},
		.call_tone_codec_vol_levels	= { 0x12EC, 0x1764, 0x1CE8, 0x23BB,
										0x2C29, 0x3694, 0x4375, 0x5360,
										0x670C, 0x7F5D },
		.dtmf_tone_codec_vol_levels = { 0x1000, 0x1000, 0x1000, 0x1000,
										0x1000, 0x1000, 0x1000, 0x1000,
										0x1000, 0x1000 },
	},
/********************************** OPEN_LISTINING **************************************************/
	{
		.use_analog 		=	1,
		.use_rx_path_att 	=	1,
		.use_rx_path_shift 	=	1,
		.use_sidetone_att 	= 	1,
		.use_ext_spk_att 	=	1,
		.use_shift_ext_spk 	=	1,
		.use_tone_vol 		=	1,
		.use_classd_vout	=	1,
		.use_pcm_gain_pre 	= 0,
		.use_pcm_gain_shift = 0,
		.use_loopgain_rxpl  = 0, 
		.use_ng_threshold	= 0,

		.analog_levels 					= { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
		.rx_path_att_levels 			= { 0x0CC0, 0x1207, 0x18FC, 0x229E, 
											0x2FF9, 0x427A, 0x5999, 0x7FFF,
											0x5999, 0x7FFF	 },
		.rx_path_shift_levels 			= { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 },
		.sidetone_att_levels 			= { 0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000	},
		.ext_spk_att_levels 			= { 0x1207, 0x18FC, 0x229E, 0x2FF9,
											0x427A, 0x5AAA, 0x7FFF, 0x5AAA,
											0x7FFF, 0x5AAA	},
		.shift_ext_spk_levels 			= { 0, 0, 0, 0, 0, 0, 0, 1, 1, 2 },
		.ring_playback_vol_levels		= { 0x1207, 0x18FC, 0x229E, 0x2FF9,
											0x427A, 0x5AAA, 0x7FFF, 0x5AAA,
											0x7FFF, 0x5AAA	},
		.ring_tone_vol_levels 			= { 0x12EC, 0x1764, 0x1CE8, 0x23BB,
											0x2C29, 0x3694, 0x4375, 0x5360,
											0x670C, 0x7F5D },
		.call_tone_codec_vol_levels 	= { 0x12EC, 0x1764, 0x1CE8, 0x23BB,
											0x2C29, 0x3694, 0x4375, 0x5360,
											0x670C, 0x7F5D },
		.call_tone_classd_vol_levels	= { 0x12EC, 0x1764, 0x1CE8, 0x23BB,
											0x2C29, 0x3694, 0x4375, 0x5360,
											0x670C, 0x7F5D },
		.dtmf_tone_codec_vol_levels 	= { 0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000 },
		.dtmf_tone_classd_vol_levels 	= { 0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000 },
		.classd_vout					= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	},

/********************************** HANDsFREE *******************************************************/
	{
		.use_analog 		=	0,
		.use_rx_path_att 	=	0,
		.use_rx_path_shift 	=	0,
		.use_sidetone_att 	= 	0,
		.use_ext_spk_att 	=	1,
		.use_shift_ext_spk 	=	1,
		.use_tone_vol 		=	1,
		.use_classd_vout	=	1,
		.use_pcm_gain_pre 	= 0,
		.use_pcm_gain_shift = 0,
		.use_loopgain_rxpl  = 0, 
		.use_ng_threshold	= 0,

		.ext_spk_att_levels 			= { 0x1207, 0x18FC, 0x229E, 0x2FF9,
											0x427A, 0x5AAA, 0x7FFF, 0x5AAA,
											0x7FFF, 0x5AAA	},
		.shift_ext_spk_levels 			= { 0, 0, 0, 0, 0, 0, 0, 1, 1, 2 },
		.ring_playback_vol_levels		= { 0x1207, 0x18FC, 0x229E, 0x2FF9,
											0x427A, 0x5AAA, 0x7FFF, 0x5AAA,
											0x7FFF, 0x5AAA	},
		.ring_tone_vol_levels 			= { 0x12EC, 0x1764, 0x1CE8, 0x23BB,
											0x2C29, 0x3694, 0x4375, 0x5360,
											0x670C, 0x7F5D },
		.call_tone_classd_vol_levels	= { 0x12EC, 0x1764, 0x1CE8, 0x23BB,
											0x2C29, 0x3694, 0x4375, 0x5360,
											0x670C, 0x7F5D },
		.dtmf_tone_classd_vol_levels 	= { 0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000, 0x1000, 0x1000,
											0x1000, 0x1000 },
		.classd_vout					= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },									
	},

/********************************** HEADSET *******************************************************/
/* NOTE: It has not been tuned */
	{
		.use_analog 		=	1,
		.use_rx_path_att 	=	1,
		.use_rx_path_shift 	=	1,
		.use_sidetone_att 	= 	1,
		.use_ext_spk_att 	=	0,
		.use_shift_ext_spk 	=	0,
		.use_tone_vol 		=	1,
		.use_classd_vout	=	0,
		.use_pcm_gain_pre 	= 0,
		.use_pcm_gain_shift = 0,
		.use_loopgain_rxpl  = 0, 
		.use_ng_threshold	= 0,
	
		.analog_levels 				= { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
		.rx_path_att_levels 		= { 0x0CC0, 0x1207, 0x18FC, 0x229E, 
										0x2FF9, 0x427A, 0x5999, 0x7FFF,
										0x5999, 0x7FFF	 },
		.rx_path_shift_levels 		= { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 },
		.sidetone_att_levels 		= { 0x1000, 0x1000, 0x1000, 0x1000,
										0x1000, 0x1000, 0x1000, 0x1000,
										0x1000, 0x1000	},
		.call_tone_codec_vol_levels	= { 0x12EC, 0x1764, 0x1CE8, 0x23BB,
										0x2C29, 0x3694, 0x4375, 0x5360,
										0x670C, 0x7F5D },
		.dtmf_tone_codec_vol_levels = { 0x1000, 0x1000, 0x1000, 0x1000,
										0x1000, 0x1000, 0x1000, 0x1000,
										0x1000, 0x1000 },
	},
/********************************** WIRELESS_HANDSET ************************************************/
/* NOTE: This is a dummy mode that disables any volume control since it is handled from the			*/
/* wireless handset device																			*/
	{
		.use_analog 		=	0,
		.use_rx_path_att 	=	0,
		.use_rx_path_shift 	=	0,
		.use_sidetone_att 	= 	0,
		.use_ext_spk_att 	=	0,
		.use_shift_ext_spk 	=	0,
		.use_tone_vol 		=	1,
		.use_classd_vout	=	0,
		.use_pcm_gain_pre 	= 0,
		.use_pcm_gain_shift = 0,
		.use_loopgain_rxpl  = 0, 
		.use_ng_threshold	= 0,


		.call_tone_codec_vol_levels 	= { 0x3000, 0x3000, 0x3000, 0x3000,
											0x3000, 0x3000, 0x3000, 0x3000,
											0x3000, 0x3000 },
		.dtmf_tone_codec_vol_levels 	= { 0x3000, 0x3000, 0x3000, 0x3000,
											0x3000, 0x3000, 0x3000, 0x3000,
											0x3000, 0x3000 },
	}	
} ;

/****************************************************************************************************/
/*************************** NARROWBAND TRANSMIT DIRECTION VOLUME LEVELS *****************************/
/****************************************************************************************************/
/* There are five modes of operation available. The proper path of the SC14450 Gen2DSP and the 		*/
/* analog audio circuities are enabled according to the HANDSET, OPEN_LISTINING, HANDsFREE,	HEADSET	*/
/* and WIRELESS_HANDSET. Eight programmable gain steps are available for volume control	of each 	*/
/* mode. Amplification as well as attenuation units form the volume control section in order to 	*/
/* offer the best tuning flexibility. The following units are controlled through this process:		*/
/* 1) analog		 --> control the transmit analog gain through CODEC_MIC_REG.MIC_GAIN			*/
/* 2) shift_paec_out --> control the transmit digital amplification through DSP cmd 0x001A, 0-0x7fff*/
/* 3) paec_tx_att	 --> control the transmit digital attenuation through DSP cmd 0x001B, 0-0x7fff	*/
/****************************************************************************************************/

static struct vmic_gains
profile_vmic_levels_narrowband[SC1445x_AE_IFACE_MODE_COUNT_IN_AP] = {

/********************************** HANDSET *********************************************************/
	{
		.use_analog =			1,
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
	#endif
		.use_attlimit	=	1,
		.use_supmin	=	1,
		.use_noiseattlimit	=	1,

		.analog_levels 			= { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },				
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels 	= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },				
		.paec_tx_att_levels 	= { 0x3200, 0x3800, 0x3E00, 0x4500,
									0x4C00, 0x5400, 0x5D00, 0x6700,
									0x7200, 0x7fff },	
		.paec_tx_att_levels_BT 	= { 0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000 },		
	#endif
		.attlimit				= { 0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800 },
		.supmin					= { 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff },
		.noiseattlimit			= { 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 
									0x2900, 0x2900 },							
	},
/********************************** OPEN_LISTINING **************************************************/
	{
		.use_analog 		= 	1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att 	=	1,
#endif
		.use_attlimit		=	1,
		.use_supmin			=	0,
		.use_noiseattlimit	=	1,

		.analog_levels 			= { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },				
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels 	= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },				
		.paec_tx_att_levels 	= { 0x3200, 0x3800, 0x3E00, 0x4500,
									0x4C00, 0x5400, 0x5D00, 0x6700,
									0x7200, 0x7fff },	
		.paec_tx_att_levels_BT 	= { 0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000 },		
#endif
		.attlimit				= { 0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800 },
		.supmin					= { 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff },
		.noiseattlimit			= { 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 
									0x2900, 0x2900 },							
	},
/********************************** HANDsFREE *******************************************************/
	{
		.use_analog 		= 	1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att 	=	1,
#endif
		.use_attlimit	=	1,
		.use_supmin	=	1,
		.use_noiseattlimit	=	1,

		.analog_levels 			= { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },		
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels 	= { 2, 2, 2, 2, 2, 3, 3, 3, 3, 3 },		
		.paec_tx_att_levels 	= { 0x5400, 0x5D00, 0x6700,	0x7200, 
									0x7fff, 0x4500,	0x4C00, 0x5400, 
									0x5D00, 0x6700 },
		.paec_tx_att_levels_BT 	= { 0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000 },		
#endif
		.attlimit				= { 0x003f, 0x003f, 0x003f, 0x003f,
									0x003f, 0x003f, 0x003f, 0x003f,
									0x003f, 0x003f },
		.supmin					= { 0x4333, 0x4333, 0x4333, 0x4333,
									0x4333, 0x4333, 0x4333, 0x4333,
									0x4333, 0x4333 },
		.noiseattlimit			= { 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 
									0x2000, 0x2000 },							
	},
/********************************** HEADSET *******************************************************/
/* NOTE: It has not been tuned */
	{
		.use_analog 		= 	1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att 	=	1,
	#endif
		.use_attlimit	=	1,
		.use_supmin	=	1,
		.use_noiseattlimit	=	1,

		.analog_levels 			= { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },				
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels 	= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },				
		.paec_tx_att_levels 	= { 0x3200, 0x3800, 0x3E00, 0x4500,
									0x4C00, 0x5400, 0x5D00, 0x6700,
									0x7200, 0x7fff },	
		.paec_tx_att_levels_BT 	= { 0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000 },	
#endif
		.attlimit				= { 0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800 },
		.supmin					= { 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff },
		.noiseattlimit			= { 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 
									0x2900, 0x2900 },							
	},
/********************************** WIRELESS_HANDSET ************************************************/
/* NOTE: This is a dummy mode that disables any volume control since it is handled from the			*/
/* wireless handset device																			*/
	{
		.use_analog 		=	0,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	0,
		.use_paec_tx_att 	=	0,
#endif
		.use_attlimit	=	1,
		.use_supmin	=	1,

		.analog_levels 			= { },
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels 	= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		.paec_tx_att_levels 	= { 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff },
		.paec_tx_att_levels_BT 	= { 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff },
#endif
		.attlimit				= { 0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800 },
		.supmin					= { 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff },
	}	
} ;

/****************************************************************************************************/
/**************************** WIDEBAND TRANSMIT DIRECTION VOLUME LEVELS *****************************/
/****************************************************************************************************/
/* There are five modes of operation available. The proper path of the SC14450 Gen2DSP and the 		*/
/* analog audio circuities are enabled according to the HANDSET, OPEN_LISTINING, HANDsFREE,	HEADSET	*/
/* and WIRELESS_HANDSET. Eight programmable gain steps are available for volume control	of each 	*/
/* mode. Amplification as well as attenuation units form the volume control section in order to 	*/
/* offer the best tuning flexibility. The following units are controlled through this process:		*/
/* 1) analog		 --> control the transmit analog gain through CODEC_MIC_REG.MIC_GAIN			*/
/* 2) shift_paec_out --> control the transmit digital amplification through DSP cmd 0x001A, 0-0x7fff*/
/* 3) paec_tx_att	 --> control the transmit digital attenuation through DSP cmd 0x001B, 0-0x7fff	*/
/****************************************************************************************************/

static struct vmic_gains
profile_vmic_levels_wideband[SC1445x_AE_IFACE_MODE_COUNT_IN_AP] = {

/********************************** HANDSET *********************************************************/
	{
		.use_analog 		= 	1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att 	=	1,
#endif
		.use_attlimit	=	1,
		.use_supmin	=	1,
		.use_noiseattlimit	=	1,

		.analog_levels 			= { 7, 7, 7, 7, 7, 7, 7, 7, 7, 7 },				
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels 	= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },				
		.paec_tx_att_levels 	= { 0x3200, 0x3800, 0x3E00, 0x4500,
									0x4C00, 0x5400, 0x5D00, 0x6700,
									0x7200, 0x7fff },	
		.paec_tx_att_levels_BT 	= { 0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000 },		
#endif
		.attlimit				= { 0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800 },
		.supmin					= { 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff },
		.noiseattlimit			= { 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 
									0x2900, 0x2900 },							
	},
/********************************** OPEN_LISTINING **************************************************/
	{
		.use_analog 		= 	1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att 	=	1,
#endif
		.use_attlimit	=	1,
		.use_supmin	=	1,
		.use_noiseattlimit	=	1,

		.analog_levels 			= { 7, 7, 7, 7, 7, 7, 7, 7, 7, 7 },				
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels 	= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },				
		.paec_tx_att_levels 	= { 0x3200, 0x3800, 0x3E00, 0x4500,
									0x4C00, 0x5400, 0x5D00, 0x6700,
									0x7200, 0x7fff },	
		.paec_tx_att_levels_BT 	= { 0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000 },		
#endif
		.attlimit				= { 0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800 },
		.supmin					= { 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff },
		.noiseattlimit			= { 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 
									0x2900, 0x2900 },							
	},
/********************************** HANDsFREE *******************************************************/
	{
		.use_analog 		= 	1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att 	=	1,
#endif
		.use_attlimit	=	1,
		.use_supmin	=	1,
		.use_noiseattlimit	=	1,

		.analog_levels 			= { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },		
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels 	= { 2, 2, 2, 2, 2, 3, 3, 3, 3, 3 },		
		.paec_tx_att_levels 	= { 0x5400, 0x5D00, 0x6700,	0x7200, 
									0x7fff, 0x4500,	0x4C00, 0x5400, 
									0x5D00, 0x6700 },
		.paec_tx_att_levels_BT 	= { 0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000 },	
#endif
		.attlimit				= { 0x003f, 0x003f, 0x003f, 0x003f,
									0x003f, 0x003f, 0x003f, 0x003f,
									0x003f, 0x003f },
		.supmin					= { 0x4333, 0x4333, 0x4333, 0x4333,
									0x4333, 0x4333, 0x4333, 0x4333,
									0x4333, 0x4333 },
		.noiseattlimit			= { 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 
									0x2000, 0x2000 },							
	},
/********************************** HEADSET *******************************************************/
/* NOTE: It has not been tuned */
	{
		.use_analog 		= 	1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att 	=	1,
#endif
		.use_attlimit	=	1,
		.use_supmin	=	1,
		.use_noiseattlimit	=	1,

		.analog_levels 			= { 7, 7, 7, 7, 7, 7, 7, 7, 7, 7 },				
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels 	= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },				
		.paec_tx_att_levels 	= { 0x3200, 0x3800, 0x3E00, 0x4500,
									0x4C00, 0x5400, 0x5D00, 0x6700,
									0x7200, 0x7fff },	
		.paec_tx_att_levels_BT 	= { 0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000, 0x1000, 0x1000,
									0x1000, 0x1000 },			
#endif
		.attlimit				= { 0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800 },
		.supmin					= { 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff },
		.noiseattlimit			= { 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 
									0x2900, 0x2900 },							
	},
/********************************** WIRELESS_HANDSET ************************************************/
/* NOTE: This is a dummy mode that disables any volume control since it is handled from the			*/
/* wireless handset device																			*/
	{
		.use_analog 		=	0,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att 	=	1,
#endif
		.use_attlimit	=	1,
		.use_supmin	=	1,

		.analog_levels 			= { },
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels 	= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		.paec_tx_att_levels 	= { 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff },
		.paec_tx_att_levels_BT 	= { 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff },
#endif
		.attlimit				= { 0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800, 0x1800, 0x1800,
									0x1800, 0x1800 },
		.supmin					= { 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff },
	}	
} ;

/****************************************************************************************************/
/****************************** NARROWBAND FREQUENCY RESPONSE FILTERS *******************************/
/****************************************************************************************************/
/* The available filters are responsible of the frequency mask shape for each direction.			*/
/* They consist of two cascaded IIR filter that construct a bandpass filter. Their coefficients are	*/
/* separated in three categories:	i) "handset" for the handset/headset acoustic components 		*/
/*								   ii) "external" for the handsfree acoustic components				*/
/*								  iii) "headset" for the corded headset acoustic components			*/
/****************************************************************************************************/

/****************************** TRANSMIT DIRECTION FILTER COEFFICIENTS *******************************/
static const unsigned short profile_narrowband_filters_TX1[3][FILTER_SIZE] = {
	/* handset */
	{ 0x4000, 0x83BA, 0x3C64, 0x6E59, 0xCEC5, 0x3746, 0x5000 },		
	/* external */
	{ 0x4000, 0xEB13, 0xF990, 0x292C, 0xFA28, 0x4666, 0x4000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
} ;
static const unsigned short profile_narrowband_filters_TX2[3][FILTER_SIZE] = {
	/* handset */
	{ 0x4000, 0x7E12, 0x3E64, 0x90A3, 0xCDCE, 0x3746, 0x5000 },		
	/* external */
	{ 0x4000, 0x3411, 0x1C5E, 0xD2E7, 0xE29B, 0x7FFF, 0x4000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
} ;
static const unsigned short profile_narrowband_filters_TX3[3][FILTER_SIZE] = { //
	/* handset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },	
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
} ;
/****************************** RECEIVE DIRECTION FILTER COEFFICIENTS *******************************/
static const unsigned short profile_narrowband_filters_RX1[3][FILTER_SIZE] = {
	/* handset */
	{ 0x4000, 0x83BA, 0x3C64, 0x6E59, 0xCEC5, 0x3746, 0x5000 },		
	/* external */
	{ 0x4000, 0x52F6, 0x1963, 0x0288, 0x270C, 0x0CCD, 0x4000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
} ;
static const unsigned short profile_narrowband_filters_RX2[3][FILTER_SIZE] = {
	/* handset */
	{ 0x4000, 0x7E12, 0x3E64, 0x90A3, 0xCDCE, 0x3746, 0x5000 },		
	/* external */
	{ 0x4000, 0xA04A, 0x2998, 0xD90B, 0xE946, 0x6000, 0x6000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
} ;
static const unsigned short profile_narrowband_filters_RX3[3][FILTER_SIZE] = {
	/* handset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },	
	/* external */
	{ 0x4000, 0x019B, 0x2890, 0x2A79, 0xE535, 0x699A, 0x5000 },	
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }		
} ;

/****************************************************************************************************/
/******************************* WIDEBAND FREQUENCY RESPONSE FILTERS ********************************/
/****************************************************************************************************/
/* The available filters are responsible of the frequency mask shape for each direction.			*/
/* They consist of two cascaded IIR filter that construct a bandpass filter. Their coefficients are	*/
/* separated in three categories:	i) "handset" for the handset/headset acoustic components 		*/
/*								   ii) "external" for the handsfree acoustic components				*/
/*								  iii) "headset" for the corded headset acoustic components			*/
/****************************************************************************************************/

/****************************** TRANSMIT DIRECTION FILTER COEFFICIENTS *******************************/
static const unsigned short profile_wideband_filters_TX1[3][FILTER_SIZE] = {
	/* handset */
	{ 0x4000, 0x631D, 0x26A1, 0x9E6C, 0xDA21, 0x2CCD, 0x5000 },		
	/* external */
	{ 0x4000, 0xE0D2, 0x0F2E, 0x4DEA, 0xE2DA, 0x2000, 0x4000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
} ;
static const unsigned short profile_wideband_filters_TX2[3][FILTER_SIZE] = {
	/* handset */
	{ 0x4000, 0x959B, 0x2D38, 0x6626, 0xD574, 0x4000, 0x5000 },		
	/* external */
	{ 0x4000, 0xC0DB, 0x2E32, 0x3BEE, 0xD4D7, 0x7FFF, 0x4000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
} ;
static const unsigned short profile_wideband_filters_TX3[3][FILTER_SIZE] = {
	/* handset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
} ;
/****************************** RECEIVE DIRECTION FILTER COEFFICIENTS *******************************/
static const unsigned short profile_wideband_filters_RX1[3][FILTER_SIZE] = {
	/* handset */
	{ 0x4000, 0x1F0E, 0x17D0, 0xDE55, 0xE7D1, 0x1CCD, 0x4000 },		
	/* external */
	{ 0x4000, 0x8593, 0x3BF3, 0x75CF, 0xC8B9, 0x2AE1, 0x5000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
} ;
static const unsigned short profile_wideband_filters_RX2[3][FILTER_SIZE] = {
	/* handset */
	{ 0x4000, 0x964D, 0x331A, 0x66A3, 0xD683, 0x7FFF, 0x5000 },		
	/* external */
	{ 0x4000, 0x441C, 0x0FAF, 0xFBD3, 0x1949, 0x251F, 0x4000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
} ;
static const unsigned short profile_wideband_filters_RX3[3][FILTER_SIZE] = {
	/* handset */
	{ 0x4000, 0xADA4, 0x34D1, 0x4A72, 0xD379, 0x7FFF, 0x5000 },		
	/* external */
	{ 0x4000, 0xC1DE, 0x2239, 0x11CD, 0xF362, 0x63D7, 0x5000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
} ;
/****************************************************************************************************/
/************************************ Acoustic Echo Canceller ***************************************/
/****************************************************************************************************/
/* The following values are the narrowband / wideband settings of the same parameters as well as	*/
/* the parameters that can be adjusted toward the fine tuning of the PAEC and the SUPPRESSOR		*/
/* algorithms.																						*/
/****************************************************************************************************/

#if defined( CONFIG_SND_SC1445x_USE_PAEC )
/* PAEC state array setup */
#ifdef PAECv4

static const unsigned short profile_paec_band_loc[] = { 
	0,   26,   52,   78,   104,  130,  156,  182,  208,  234,  260,  286,  312,  338,  364,		//part
} ;
static const unsigned short profile_paec_band_narrowband[] = { 
	0x0002, 0x0004, 0x0006, 0x0008, 0x000a, 0x000e, 0x0013, 0x001a, 0x0022, 0x002d, 0x0036, 0x0041, 0x0000, 0x0000, 0x0000,
} ;
static const unsigned short profile_paec_band_wideband[] = {
	0x0002, 0x0003, 0x0004, 0x0005, 0x0006, 0x0008, 0x000a, 0x000d, 0x0011, 0x0016, 0x001c, 0x0023, 0x002c, 0x0036, 0x0041,
} ;

#else

static const unsigned short profile_paec_band_loc[] = {
	  0,   21,   42,   63,   84,  105,  126,  147,  168,  189,  210,  231,  252,  273,  294,		//part
	  1,   22,   43,   64,   85,  106,  127,  148,  169,  190,  211,  232,  253,  274,  295,		//var_y
	  2,   23,   44,   65,   86,  107,  128,  149,  170,  191,  212,  233,  254,  275,	296,		//var_e
	  3,   24,   45,   66,   87,  108,  129,  150,  171,  192,  213,  234,  255,  276,  297,		//var_e_af
	 12,   33,   54,   75,   96,  117,  138,  159,  180,  201,  222,  243,  264,  285,  306			//noiseest
} ;

static const unsigned short profile_paec_band_narrowband[] = {
	0x0002, 0x0004, 0x0006, 0x0008, 0x000a, 0x000e, 0x0013, 0x001a, 0x0022, 0x002d, 0x0036, 0x0041, 0x0000, 0x0000, 0x0000,
	0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001,
	0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001,
	0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001,
	0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080
} ;
static const unsigned short profile_paec_band_wideband[] = {
	0x0002, 0x0003, 0x0004, 0x0005, 0x0006, 0x0008, 0x000a, 0x000d, 0x0011, 0x0016, 0x001c, 0x0023, 0x002c, 0x0036, 0x0041,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
	0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001
} ;

#endif

/* PAEC parameters setup */
#ifdef PAECv4
	
	static const unsigned short profile_paec_data_loc[] = { 
	23/*npart*/,		30/*init*/,		31/*init2*/,	33/*dtdcount_init*/,	35/*minframe*/,	38/*cctc*/,		44/*dtdsmooth*/,	48/*noiseest1*/, //161209GF Loc numbers have been changed to PAECv4 w/smoothed LMS (OldLoc-2).
	49/*noiseest2*/,	53/*fevadsm1*/,	54/*fevadsm2*/,	55/*nevadsm1*/,			56/*nevadsm2*/,	60/*reverbtc*/,	61/*smoothlow*/,	62/*smoothtop*/,	93/*lmsattack*/,	94/*lmsrelease*/
	} ;
	static const unsigned short profile_paec_data_narrowband[] = { 
	0x000C,		0xFF83,		0xFE0C,		0x0007,		0x007D,		0x00D9,		0x0368,		0x000B, 
	0x020C,		0x7FFF,		0x028D,		0x147A,		0x028D,		0x1000,		0x0CCC,		0x6666,		0x051F,		0x051F
	} ;
	static const unsigned short profile_paec_data_wideband[] = { 
	0x000F,		0xFF06,		0xFC18,		0x000F,		0x00FA,		0x006C,		0x01B4,		0x0005, 
	0x0106,		0x6666,		0x0146,		0x0A3D,		0x0146,		0x0800,		0x0666,		0x3333,		0x028F,		0x028F
	} ;

#else

	static const unsigned short profile_paec_data_loc[] = {
		22, 29, 34, 36, 40, 44, 45, 
		48, 49, 52, 53, 54, 55, 56
	} ;
	static const unsigned short profile_paec_data_narrowband[] = {
		0x000c, 0xffb0, 0x0106, 0x0040, 0x036A, 0x000d, 0x020c, 
		0x6666, 0x0ea1, 0x1000, 0x3000, 0x2000, 0x0666, 0x7000
	} ;
	static const unsigned short profile_paec_data_wideband[] = {		
		0x000f, 0xff60, 0x0083, 0x0040, 0x01b5, 0x0007, 0x0106, 
		0x3333, 0x0750, 0x1000, 0x6000, 0x3000, 0x0333, 0x6000
	} ;

#endif


#  if defined( HAVE_PAEC_SUPPRESSOR )
/* Suppressor Setup*/
static unsigned short* const profile_supp_params_addr[] = {
/*	(void*)0x10718, (void*)0x1071a,		//plevdet_data_spk fall / rise time
	(void*)0x11786, (void*)0x11788,		//plevdet_data_spk1 fall / rise time
	(void*)0x107f2, (void*)0x107f4,		//plevdet_data_paec_out fall / rise time 
	(void*)0x11a7e, (void*)0x11a82,		//pnlev_tx_data fall / rise time
	(void*)0x109B4,						//centclip_att
	(void*)0x109b8,						//c2_thresh
	(void*)0x109ba,						//cntrini 
	(void*)0x109bc,						//cntrmin 
	(void*)0x109c4, (void*)0x109be,		//rbetaoff / off 
	(void*)0x109c6, (void*)0x109c0,		//rbetaon / on	*/

#if 0  /* don't use Tx Peak Limiter for now */
/* TxPeakLimit */						
	(void*)0x1056e,						//HagcTx toff_scale
	(void*)0x10574,						//HagcTx toff
	(void*)0x10576,						//HagcTx ton	
	(void*)0x10586, (void*)0x10588,		//PLevDet2TxComp1 fall / rise time
	(void*)0x1058E, (void*)0x10590,		//PLevDet2TxComp2 fall / rise time
#endif

/* RxPeakLimit */	
	(void*)0x105ba,						//HagcRx toff_scale
	(void*)0x105C0,						//HagcRx toff
	(void*)0x105C2,						//HagcRx ton
	(void*)0x10624, (void*)0x10626,		//PLevDet2RxComp1 fall / rise time
	(void*)0x1062C, (void*)0x1062E,		//PLevDet2RxComp2 fall / rise time
/* Hfree */
	(void*)0x11fca, (void*)0x11fcc,		//hfree_pl_rxin fall / rise	
	(void*)0x11fd4, (void*)0x11fd8,		//hfree_pn_rxin fall / rise	
	(void*)0x11fe0, (void*)0x11fe2,		//hfree_pa_txin fall / rise	
	(void*)0x11fea, (void*)0x11fee,		//hfree_pn_txin fall / rise	
	(void*)0x12B72,						//hfree idlecntini
	(void*)0x12B74,						//hfree idlecntini2
	(void*)0x12B82,						//rx ratio
	(void*)0x12B84,						//hfree rxtxcntini
	(void*)0x12B88,						//hfree suptidleoff
	(void*)0x12B8a,						//hfree suptidleon
	(void*)0x12B8c,						//hfree suptoff
	(void*)0x12B8e,						//hfree supton
	(void*)0x11ff6, (void*)0x11ff8,		//hfree_pl_txin fall / rise	
	(void*)0x11f2c, (void*)0x11f2e,		//hfree_pa_rxin fall / rise
/* CNG */	
	(void*)0x11fc4						//comfnoiselevel
} ;

static const unsigned short profile_supp_params_data_narrowband[] = {
/*	0x7b81, 0x783d,						//plevdet_data_spk fall / rise time
	0x7dbc, 0x7ce0,						//plevdet_data_spk1 fall / rise time
	0x7b81, 0x783d,						//plevdet_data_paec_out fall / rise time
	0x7c0e, 0x7ff1,						//pnlev_tx_data fall / rise time
	0x7000,								//centclip_att
	0xE400,								//c2_thresh		
	0x0030,								//cntrini		
	0x7000,								//cntrmin		
	0x783d, 0x07c2,						//rbetaoff / off 
	0x70f4, 0x0AAA,						//rbetaon / on	*/

#if 0  /* don't use Tx Peak Limiter for now */
/* TxPeakLimit */							
	0x0015,								//HagcTx toff_scale
	0x00ee,								//HagcTx toff
	0x7e52,								//HagcTx ton	
	0x4da3, 0x783f,						//PLevDet2TxComp1 fall / rise time
	0x7e69, 0x70f6,						//PLevDet2TxComp2 fall / rise time
#endif

/* RxPeakLimit */	
	0x0015,								//HagcRx toff_scale
	0x00ee,								//HagcRx toff
	0x7e52,								//HagcRx ton					
	0x4da3, 0x783f,						//PLevDet2RxComp1 fall / rise time
	0x7e69, 0x70f6,						//PLevDet2RxComp2 fall / rise time
/* Hfree */
	0x7f3d, 0x7eab,						//hfree_pl_rxin fall / rise	
	0x7c0e, 0x7c0e,						//hfree_pn_rxin fall / rise	
	0x7f3d, 0x7eab,						//hfree_pa_txin fall / rise	
	0x7c0e, 0x7c0e,						//hfree_pn_txin fall / rise	
	0x0050,								//hfree idlecntini
	0x0008,								//hfree idlecntini2
	0xF800,								//rx ratio
	0x0050,								//hfree rxtxcntini
	0x0076,								//hfree suptidleoff
	0x7f89,								//hfree suptidleon
	0x05a5,								//hfree suptoff
	0x7a96,								//hfree supton
	0x7f3d, 0x7eab,						//hfree_pl_txin fall / rise	
	0x7f3d, 0x7eab,						//hfree_pa_rxin fall / rise
/* CNG */	
	0x0012								//comfnoiselevel	
} ;


static const unsigned short profile_supp_params_data_wideband[] = {		
/*	0x7DBB, 0x7C0F,						//plevdet_data_spk fall / rise time
	0x7EDC, 0x7E03,						//plevdet_data_spk1 fall / rise time
	0x7DBB, 0x7C0F,						//plevdet_data_paec_out fall / rise time
	0x7e03, 0x7fbb,						//pnlev_tx_data fall / rise time
	0x7000,								//centclip_att
	0xE400,								//c2_thresh
	0x0060,								//cntrini
	0x7000,								//cntrmin
	0x7C0F, 0x03F0,						//rbetaoff / off 
	0x783E, 0x0590,						//rbetaon / on	*/

#if 0  /* don't use Tx Peak Limiter for now */
/* TxPeakLimit */						
	0x0009,								//HagcTx toff_scale
	0x0039,								//HagcTx toff
	0x7f28,								//HagcTx ton					
	0x63b0, 0x7c10,						//PLevDet2TxComp1 fall / rise time
	0x7f34, 0x783f,						//PLevDet2TxComp2 fall / rise time
#endif

/* RxPeakLimit */	
	0x0009,								//HagcRx toff_scale
	0x0039,								//HagcRx toff
	0x7f28,								//HagcRx ton					
	0x63b0, 0x7c10,						//PLevDet2RxComp1 fall / rise time
	0x7f34, 0x783f,						//PLevDet2RxComp2 fall / rise time
/* Hfree */
	0x7f9e, 0x7f55,						//hfree_pl_rxin fall / rise	
	0x7e03, 0x7e03,						//hfree_pn_rxin fall / rise	
	0x7f9e, 0x7f55,						//hfree_pa_txin fall / rise	
	0x7e03, 0x7e03,						//hfree_pn_txin fall / rise	
	0x00A0,								//hfree idlecntini
	0x0010,								//hfree idlecntini2
	0xEFFF,								//rx ratio
	0x00A0,								//hfree rxtxcntini
	0x003B,								//hfree suptidleoff
	0x7FC5,								//hfree suptidleon
	0x02d2,								//hfree suptoff
	0x7D2E,								//hfree supton
	0x7f9e, 0x7f55,						//hfree_pl_txin fall / rise	
	0x7f9e, 0x7f55,						//hfree_pa_rxin fall / rise
/* CNG */	
	0x0012								//comfnoiselevel	
} ;
#  endif
#endif



#if defined( CONFIG_SND_SC1445x_USE_AEC )
/* AEC setup */
static const unsigned short profile_aec_params_data[] = {
	0x4000, 0x0100, 0x7FFF, 0x0020, 0x0666, 0x1000, 0x0200, 0x2000,
	0x3000, 0x0200, 0x0020, 0x0020, 0x0100, 0x0100, 0x0040, 0x0040,
	0x0020, 0x0020, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x07D0, 0x0069, 0x8000, 0x8000, 0x0100, 0x0200,
	0x7DBC, 0x7C10, 0x7DBC, 0x7C10, 0x7DBC, 0x7C10, 0x7FDF, 0x7FFF,
	0x7DBC, 0x7C10, 0x0014, 0x7C10, 0x0148, 0x7FF2, 0x0014, 0x7C10,
	0x01CF, 0x7FF2, 0x7DBC, 0x7C10, 0x7DBC, 0x7C10, 0x7DBC, 0x7C10,
	0x4000, 0x0000, 0xF333, 0x0200, 0x0000, 0x7FFF, 0x4000, 0x7FFF,
	0x4000, 0x0247, 0x7FFF, 0x7FFF, 0x287A, 0x0247, 0x01DB, 0x0032,
	0x7F15, 0x06BE, 0x7C5E, 0x4000, 0x0000, 0xA562, 0x0000, 0x0000,
	0xFA97, 0xAA74, 0x051E, 0x061E, 0x0278, 0x0298, 0x0000, 0x0000,
	0x0000, 0x0002, 0x0000, 0x0000, 0x0000, 0x0000, 0x101D, 0x101D,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0244, 0x03F0, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0207, 0x0207, 0x0207, 0x0207, 0x0000, 0x0000, 0x0000
} ;
#endif

#endif  /* __AUDIO_PROFILE_SC1445x_DE900_H__ */


