/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 * Some platform dependent macros.
 */

#if !defined( __AUDIOENGINE_PLATFORM_H__ )
#define __AUDIOENGINE_PLATFORM_H__

#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <asm/config.h>

/* define this if compiling for a minimal platform, where there is no output */
//#define AUDIO_ENGINE_NO_OUTPUT

#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT )
#  define SC1445x_AE_ATA_SUPPORT
#endif

#if defined( CONFIG_CVM480_DECT_SUPPORT ) || defined( CONFIG_LMX418x_DECT_SUPPORT ) \
		|| defined( CONFIG_LMX418x_DECT_SUPPORT_MODULE )
#  define SC1445x_AE_DECT_SUPPORT
#endif

#if defined( CONFIG_LMX418x_DECT_SUPPORT ) \
		|| defined( CONFIG_LMX418x_DECT_SUPPORT_MODULE )
#  define SC1445x_AE_NATIVE_DECT_SUPPORT
#endif

#if defined( CONFIG_SC1445x_DECT_HEADSET_SUPPORT ) || \
		( defined( CONFIG_SC1445x_KBD_SUPPORT ) && \
		  	defined( CONFIG_SC1445x_DECT_HANDSET_SUPPORT ) )
/* VoIP phone with DECT earpiece or DECT handsets */
#  define SC1445x_AE_PHONE_DECT

/* reserve "line" 0 for local iface */
#  define SC1445x_AE_NATIVE_DECT_LINE_OFFSET	1
#endif

#ifndef SC1445x_AE_NATIVE_DECT_LINE_OFFSET
#  define SC1445x_AE_NATIVE_DECT_LINE_OFFSET	ATA_DEV_CNT
#endif

#if defined( CONFIG_SC1445x_BT_MODULE )
#  define SC1445x_AE_BT
#endif

/* see if we are building for an ATA and or DECT platform */
#if defined( SC1445x_AE_ATA_SUPPORT ) || \
		( defined( SC1445x_AE_DECT_SUPPORT )  && \
			!defined( SC1445x_AE_PHONE_DECT ) )
#  define SC1445x_AE_PCM_LINES_SUPPORT
#endif

#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_PCM_LINES_SUPPORT )
#  define SC1445x_AE_USE_CONVERSATIONS
#endif


#if defined( AUDIO_ENGINE_NO_OUTPUT )

#  define PRINT( ... )		do { } while( 0 )

#  define DPRINT( ... )		do { } while( 0 )

#  define PRINTINFO( ... )	do { } while( 0 )

#else	

#  if !defined( SC1445x_AE_NATIVE_DECT_SUPPORT )
#	define PRINT_LEVEL			/* use whatever is default */
#  else
#	define PRINT_LEVEL	KERN_DEBUG	/* don't print on the console */
#  endif

#  define PRINT( ... )		printk( __VA_ARGS__ )

#  define DPRINT( ... )		pr_debug( __VA_ARGS__ )

#  define PRINTINFO( ... )	pr_info( __VA_ARGS__ )

#endif  /* AUDIO_ENGINE_NO_OUTPUT */

#define MALLOC( n )		kmalloc( n, GFP_KERNEL )

#define MALLOC_ATOMIC( n )	kmalloc( n, GFP_ATOMIC )

#define FREE( p )		kfree( p )


#endif	/* __AUDIOENGINE_PLATFORM_H__ */

