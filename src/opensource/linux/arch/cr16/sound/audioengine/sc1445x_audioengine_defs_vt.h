/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 * Structures used by the audio engine.
 */

#if !defined( __AUDIOENGINE_DEFS_H__ )
#define __AUDIOENGINE_DEFS_H__

#include "sc1445x_audioengine_platform_vt.h"
#include "audio_profile_defs_vt.h"


#if 0
/* define when using wideband codecs by default */
#define SC1445x_AE_WIDEBAND
#endif


#if 0
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_PCM_LINES_SUPPORT )
/* temporarily, support 3 channels on 452 */
#  define SC1445x_AE_USE_3_CHANNELS
#endif
#endif

#if 0
#  define SC1445x_AE_USE_FADE_IN_TIMER
#endif

#if defined( SC1445x_AE_ATA_SUPPORT ) && defined( CONFIG_SC14452 )
/* uncomment to add support for fax */
#  define SC1445x_AE_SUPPORT_FAX
#endif

#if defined( SC1445x_AE_SUPPORT_FAX )
//#  define SC1445x_AE_USE_FAX_TIMER
#endif


#define SC1445x_AE_TONEGEN_DEFAULT_VOLUME_TX	0x3000


/* define if you want to turn on statistics collection */
#define SC1445x_AE_COLLECT_STATISTICS


/* the number of bytes used for an audio packet in the audio engine buffers */
#define SC1445x_AE_BYTES_PER_AUDIO_PACKET		80

/* the number of bytes used as header for an audio packet, preceeding it */
#define SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER	2


#if defined( SC1445x_AE_SUPPORT_FAX )
/* the number of bytes used for a fax packet in the audio engine buffers */
#  define SC1445x_AE_BYTES_PER_FAX_PACKET			80

/* the number of bytes used as header for a fax packet, preceeding it */
#  define SC1445x_AE_BYTES_PER_FAX_PACKET_HEADER		2
#endif


#if 0
/* MOVED TO AUDIO PROFILES -- each profile may set level count accordingly */

/* the number of available volume levels for virtual speakers */
#define SC1445x_AE_VSPK_VOL_LEVEL_COUNT		8

/* the number of available gain levels for virtual microphones */
#define SC1445x_AE_VMIC_GAIN_LEVEL_COUNT	8
#endif

/* the memory limits of dsp memories for each chip */
#if defined( CONFIG_SC14452 )

#  define DSP1_PROG_MEM_LIMIT	(unsigned short)0x2800 //10 kWords
#  define DSP1_DATA_MEM_LIMIT	(unsigned short)0x2000 //8  kWords

#  define DSP2_PROG_MEM_LIMIT	(unsigned short)0x2000 //8kWords
#  define DSP2_DATA_MEM_LIMIT	(unsigned short)0x2800 //10kWords

#elif defined( CONFIG_SC14450 )

#  define DSP1_PROG_MEM_LIMIT	(unsigned short)0x2000 //8 kWords
#  define DSP1_DATA_MEM_LIMIT	(unsigned short)0x1800 //6  kWords

#  define DSP2_PROG_MEM_LIMIT	(unsigned short)0x2000 //8kWords
#  define DSP2_DATA_MEM_LIMIT	(unsigned short)0x2000 //8kWords

#endif


/* available flavors of audio codecs */
typedef enum sc1445x_ae_codec_type_t {
	SC1445x_AE_CODEC_UNDEFINED = 0,

	/* G711 flavors */
	SC1445x_AE_CODEC_G711_ALAW,
	SC1445x_AE_CODEC_G711_ULAW,
	SC1445x_AE_CODEC_G711_ALAW_VAD,
	SC1445x_AE_CODEC_G711_ULAW_VAD,

	/* G726 flavors */
	SC1445x_AE_CODEC_G726,
	SC1445x_AE_CODEC_G726_VAD,

	/* G729 flavors */
	SC1445x_AE_CODEC_G729,
	SC1445x_AE_CODEC_G729_VAD,

	/* G722 (wideband) */
	SC1445x_AE_CODEC_G722,

	/*iLBC flavors (narrowband) */
	SC1445x_AE_CODEC_iLBC_20ms,
	SC1445x_AE_CODEC_iLBC_30ms,

	/* G722 (wideband) alternate modes */
	SC1445x_AE_CODEC_G722_MODE2,
	SC1445x_AE_CODEC_G722_MODE3,

	SC1445x_AE_CODEC_INVALID
} sc1445x_ae_codec_type ;



/* available tone frequencies */
typedef enum sc1445x_ae_tone_t {
	SC1445x_AE_TONE_F0 = 0,	/* idle */
	SC1445x_AE_TONE_F697,	/*  697 Hz; tone1 for 1, 2, 3, A */
	SC1445x_AE_TONE_F770,	/*  770 Hz; tone1 for 4, 5, 6, B */
	SC1445x_AE_TONE_F852,	/*  852 Hz; tone1 for 7, 8, 9, C */
	SC1445x_AE_TONE_F941,	/*  941 Hz; tone1 for *, 0, #, D */
	SC1445x_AE_TONE_F1209,	/* 1209 Hz; tone2 for 1, 4, 7, * */
	SC1445x_AE_TONE_F1336,	/* 1336 Hz; tone2 for 2, 5, 8, 0 */
	SC1445x_AE_TONE_F1477,	/* 1477 Hz; tone2 for 3, 6, 9, # */
	SC1445x_AE_TONE_F1633,	/* 1633 Hz; tone2 for A, B, C, D */
	SC1445x_AE_TONE_F425,	/*  425 Hz; single tone for busy etc */
	SC1445x_AE_TONE_F440,	/*  440 Hz */
	SC1445x_AE_TONE_F1000,	/* 1000 Hz */
	SC1445x_AE_TONE_F400,	/*  400 Hz */
	SC1445x_AE_TONE_F100,	/*  100 Hz */
	SC1445x_AE_TONE_F250,	/*  250 Hz */
	SC1445x_AE_TONE_F350,	/*  350 Hz */
	SC1445x_AE_TONE_F480,	/*  480 Hz */
	SC1445x_AE_TONE_F600,	/*  600 Hz */
	SC1445x_AE_TONE_F620,	/*  620 Hz */
	SC1445x_AE_TONE_F680,	/*  680 Hz */
	SC1445x_AE_TONE_F950,	/*  950 Hz */
	SC1445x_AE_TONE_F1200,	/* 1200 Hz */
	SC1445x_AE_TONE_F1400,	/* 1400 Hz */
	SC1445x_AE_TONE_F1800,	/* 1800 Hz */
	SC1445x_AE_TONE_F2060,	/* 2060 Hz */
	SC1445x_AE_TONE_F2130,	/* 2130 Hz */
	SC1445x_AE_TONE_F2450,	/* 2450 Hz */
	SC1445x_AE_TONE_F2600,	/* 2600 Hz */
	SC1445x_AE_TONE_F2750,	/* 2750 Hz */

	SC1445x_AE_TONE_INVALID
} sc1445x_ae_tone ;


/* standard DTMF and other tones */
typedef enum sc1445x_ae_std_tone_t {
	SC1445x_AE_STD_TONE_0,		/* digit 0 */
	SC1445x_AE_STD_TONE_1,		/* digit 1 */
	SC1445x_AE_STD_TONE_2,		/* digit 2 */
	SC1445x_AE_STD_TONE_3,		/* digit 3 */
	SC1445x_AE_STD_TONE_4,		/* digit 4 */
	SC1445x_AE_STD_TONE_5,		/* digit 5 */
	SC1445x_AE_STD_TONE_6,		/* digit 6 */
	SC1445x_AE_STD_TONE_7,		/* digit 7 */
	SC1445x_AE_STD_TONE_8,		/* digit 8 */
	SC1445x_AE_STD_TONE_9,		/* digit 9 */
	SC1445x_AE_STD_TONE_STAR,	/* key * */
	SC1445x_AE_STD_TONE_HASH,	/* key # */
	SC1445x_AE_STD_TONE_A,		/* key A */
	SC1445x_AE_STD_TONE_B,		/* key B */
	SC1445x_AE_STD_TONE_C,		/* key C */
	SC1445x_AE_STD_TONE_D,		/* key D */
	/* call progress tones heard only locally */
	SC1445x_AE_STD_TONE_BUSY,
	SC1445x_AE_STD_TONE_CONGESTION,
	SC1445x_AE_STD_TONE_DIAL,
	SC1445x_AE_STD_TONE_DISCONNECT1,
	SC1445x_AE_STD_TONE_DISCONNECT2,
	SC1445x_AE_STD_TONE_RINGING,
	SC1445x_AE_STD_TONE_RING_BACK,
	SC1445x_AE_STD_TONE_SPECIAL_DIAL,
	SC1445x_AE_STD_TONE_WAITING,
	SC1445x_AE_STD_TONE_DIAL2,
	SC1445x_AE_STD_TONE_DIAL3,
	SC1445x_AE_STD_TONE_STUTTER_DIAL,
	SC1445x_AE_STD_TONE_CALL_WAITING,
	SC1445x_AE_STD_TONE_CALL_WAITING2,
	SC1445x_AE_STD_TONE_CALL_WAITING3,
	SC1445x_AE_STD_TONE_CALL_WAITING4,
	SC1445x_AE_STD_TONE_CIDCW_CAS,
	SC1445x_AE_STD_TONE_OUT_OF_SERVICE,
	SC1445x_AE_STD_TONE_OFF_HOOK_WARNING,
	SC1445x_AE_STD_TONE_ADDR_ACK,
	SC1445x_AE_STD_TONE_KEYPAD_ECHO1,
	SC1445x_AE_STD_TONE_KEYPAD_ECHO2,
	/* call progress tones heard by both the local and the remote party */
	SC1445x_AE_STD_TONE_BUSY_LR,
	SC1445x_AE_STD_TONE_CONGESTION_LR,
	SC1445x_AE_STD_TONE_DIAL_LR,
	SC1445x_AE_STD_TONE_DISCONNECT1_LR,
	SC1445x_AE_STD_TONE_DISCONNECT2_LR,
	SC1445x_AE_STD_TONE_RINGING_LR,
	SC1445x_AE_STD_TONE_RING_BACK_LR,
	SC1445x_AE_STD_TONE_SPECIAL_DIAL_LR,
	SC1445x_AE_STD_TONE_WAITING_LR,
	SC1445x_AE_STD_TONE_DIAL2_LR,
	SC1445x_AE_STD_TONE_DIAL3_LR,
	SC1445x_AE_STD_TONE_STUTTER_DIAL_LR,
	SC1445x_AE_STD_TONE_CALL_WAITING_LR,
	SC1445x_AE_STD_TONE_CALL_WAITING2_LR,
	SC1445x_AE_STD_TONE_CALL_WAITING3_LR,
	SC1445x_AE_STD_TONE_CALL_WAITING4_LR,
	SC1445x_AE_STD_TONE_CIDCW_CAS_LR,
	SC1445x_AE_STD_TONE_OUT_OF_SERVICE_LR,
	SC1445x_AE_STD_TONE_OFF_HOOK_WARNING_LR,
	SC1445x_AE_STD_TONE_ADDR_ACK_LR,
	SC1445x_AE_STD_TONE_KEYPAD_ECHO1_LR,
	SC1445x_AE_STD_TONE_KEYPAD_ECHO2_LR,

	SC1445x_AE_STD_TONE_INVALID
} sc1445x_ae_std_tone ;

#define SC1445x_AE_CP_TONE_LR_OFFSET \
	(SC1445x_AE_STD_TONE_BUSY_LR - SC1445x_AE_STD_TONE_BUSY)


/* status of the tone generation module */
typedef enum sc1445x_ae_tonegen_status_t {
	SC1445x_AE_TONEGEN_IDLE = 0,
	SC1445x_AE_TONEGEN_PLAYING,
	SC1445x_AE_TONEGEN_STOPPING
} sc1445x_ae_tonegen_status ;


/* parameters for generating a custom tone */
typedef struct sc1445x_ae_custom_tone_params_t {
	/* ARGCOS for the 3 frequencies, narrowband */
	unsigned short narrow_argcos[3] ;

	/* ARGCOS for the 3 frequencies, narrowband */
	unsigned short narrow_argsin[3] ;

	/* ARGCOS for the 3 frequencies, wideband */
	unsigned short wide_argcos[3] ;

	/* ARGCOS for the 3 frequencies, wideband */
	unsigned short wide_argsin[3] ;

	/* amplitudes for the 3 frequencies */
	unsigned short amplitude[3] ;
} sc1445x_ae_custom_tone_params ;


/* one-tone part of a tone sequence */
typedef struct sc1445x_ae_tone_sequence_part_t {
	sc1445x_ae_custom_tone_params tone_params ;
	unsigned short on_duration ;
	unsigned short off_duration ;
} sc1445x_ae_tone_sequence_part ;


/* container for tone sequence queue */
typedef struct sc1445x_ae_tone_seq_container_t {
	sc1445x_ae_tone_sequence_part data ;
	struct sc1445x_ae_tone_seq_container_t* next ;
} sc1445x_ae_tone_seq_container ;

/* tone sequence repeat status */
typedef enum sc1445x_ae_tone_seq_repeat_t {
	/* do not repeat tone sequence */
	SC1445x_AE_TONE_SEQ_NO_REPEAT = 0,
	/* repeat whole sequence */
	SC1445x_AE_TONE_SEQ_REPEAT_ALL,
	/* repeat only the last part of the sequence */
	SC1445x_AE_TONE_SEQ_REPEAT_LAST,
} sc1445x_ae_tone_seq_repeat ;

/* tonegen state */
typedef struct sc1445x_ae_tonegen_state_t {
	volatile sc1445x_ae_tonegen_status status ;
	sc1445x_ae_tone tone1 ;
	sc1445x_ae_tone tone2 ;
	sc1445x_ae_tone tone3 ;
	sc1445x_ae_tone tone4 ;

	sc1445x_ae_custom_tone_params custom_tone ;

	unsigned short on_duration ;	/* for DTMF */
	unsigned short off_duration ;	/* for DTMF */
	short repeat_tone ;  		/* whether we repeat the tone */
					/* after it ends */
	unsigned short volume ;

	struct timer_list timer ;
	struct timer_list auto_stop_timer ;
#if defined( SC1445x_AE_USE_FAX_TIMER )
	struct timer_list fax_timer ;
#endif

	struct sc1445x_ae_state_t* my_ae_state ;

	/* tone sequence stuff */
	sc1445x_ae_tone_seq_container* tone_seq_start ;
	sc1445x_ae_tone_seq_container* tone_seq_curr ;
	sc1445x_ae_tone_seq_repeat repeat_tone_seq ;

	/* flag to show whether we are currently playing a ringing tone */
	unsigned short is_ringing 	: 1 ;
	/* flag to show whether we are currently playing a DTMF */
	unsigned short is_playing_dtmf 	: 1 ;

#if defined( SC1445x_AE_USE_FAX_TIMER )
	/* flag to show whether CED detection is disabled */
	unsigned short is_CED_detection_off	: 1 ;
	/* flag to show whether CNG detection is disabled */
	unsigned short is_CNG_detection_off	: 1 ;
#endif

	/*
	 * flag to show whether this is a custom or a predefined tone
	 * for custom tones, the custom_tone field is valid
	 */
	unsigned short is_custom_tone		: 1 ;
	/*
	 * flag to show whether the tonegen is beeing reprogrammed
	 * (due to operating frequency change)
	 */
	unsigned short is_being_reprogrammed	: 1 ;
} sc1445x_ae_tonegen_state ;


/* raw PCM encoding rates */
typedef enum sc1445x_ae_raw_pcm_rate_t {
	SC1445x_AE_RAW_PCM_RATE_8000 = 0,
	SC1445x_AE_RAW_PCM_RATE_16000,
	SC1445x_AE_RAW_PCM_RATE_32000,
	SC1445x_AE_RAW_PCM_RATE_INVALID
} sc1445x_ae_raw_pcm_rate ;


/* raw PCM state */
typedef struct sc1445x_ae_raw_pcm_state_t {
	unsigned short* const buffer ;	/* where the raw PCM blocks start */
	unsigned short* const block_status ;	/* an array of the status */
						/* of each block */
	const unsigned short block_size ;	/* the size of each block, */
						/* in 16-bit samples */
	const unsigned short block_count ;	/* the number of blocks */
	const sc1445x_ae_raw_pcm_rate rate ;	/* the encoding rate */
	unsigned short next_block_index ;	/* the index of the next */
						/* block to fill with data */
	unsigned partial ;			/* used when a buffer is */
						/* partially filled */
} sc1445x_ae_raw_pcm_state ;



/* speaker state */
typedef struct sc1445x_ae_spk_state_t {
	unsigned short is_muted ;
} sc1445x_ae_spk_state ;


/* microphone state */
typedef struct sc1445x_ae_mic_state_t {
	unsigned short is_muted ;
} sc1445x_ae_mic_state ;


/* codec settings */
typedef struct sc1445x_ae_codec_settings_t {
	const unsigned short type ;
	unsigned int rate ;
	const unsigned short sample_bits ;
} sc1445x_ae_codec_settings ;


/* audio statistics */
typedef struct sc1445x_ae_audio_stats_t {
	/* frame type 0 (no packet) */
	unsigned empty_to_dsp ;
	unsigned empty_from_dsp ;
	unsigned empty_to_os ;
	unsigned empty_from_os ;

	/* frame type 1 (normal) */
	unsigned normal_to_dsp ;
	unsigned normal_from_dsp ;
	unsigned normal_to_os ;
	unsigned normal_from_os ;

	/* frame type 2 (silence) */
	unsigned sid_to_dsp ;
	unsigned sid_from_dsp ;
	unsigned sid_to_os ;
	unsigned sid_from_os ;
} sc1445x_ae_audio_stats ;


/* audio channel state */
typedef struct sc1445x_ae_channel_state_t {
	unsigned short is_active ;
	unsigned char* const playback_buffer ;	/* where to place an audio */
						/* packet for playback */
	unsigned char* const capture_buffer ;	/* where to get a captured */
						/* audio packet from */
	sc1445x_ae_codec_settings enc_codec ;	/* encoding codec (to net) */
	sc1445x_ae_codec_settings dec_codec ;	/* decoding codec (from net) */

#if defined( SC1445x_AE_COLLECT_STATISTICS )
	sc1445x_ae_audio_stats stats ;
#endif
#if defined( SC1445x_AE_SUPPORT_FAX )
	unsigned short is_fax ;			/* non-zero when transferring */
						/* fax data */
#endif

	unsigned short playback_counter ;	/* counters used by some */
	unsigned short capture_counter ;	/* codecs (e.g. iLBC) */

	const unsigned short playback_cnt_lim ;	/* and the limits for */
	const unsigned short capture_cnt_lim ;	/* these counters */

	unsigned short playback_sync ;		/* sync flag used by some */
	unsigned short capture_sync ;		/* codecs (e.g. iLBC) */

	unsigned short playback_tmp_buf[160] ;	/* temp buffers used by some */
	unsigned short capture_tmp_buf[160] ;	/* codecs (e.g. iLBC) */

#if defined( SC1445x_AE_USE_FADE_IN_TIMER )
	struct timer_list fade_in_timer ;
#endif
	unsigned short vol_level ;

	struct sc1445x_ae_state_t* my_ae_state ;
} sc1445x_ae_channel_state ;


#if defined( SC1445x_AE_USE_CONVERSATIONS )

/* conversation state */
typedef struct sc1445x_ae_conversation_state_t {
	unsigned short is_started ;

	/* bit-field that shows who participates in the conversation */
	unsigned short members ;

#  if !defined( SC1445x_AE_PHONE_DECT )
	unsigned short is_default ;
#  endif
} sc1445x_ae_conversation_state ;

#endif


/* audio engine operating mode */
typedef enum sc1445x_ae_mode_t {
	SC1445x_AE_MODE_NORMAL,			/* normal operation */
	SC1445x_AE_MODE_LOOPBACK,		/* loopback */
	SC1445x_AE_MODE_AUTOPLAY,		/* playback of stored audio */
	SC1445x_AE_MODE_RAW_PCM,		/* playback of raw PCM audio */

	SC1445x_AE_MODE_INVALID
} sc1445x_ae_mode ;


/* virtual speaker state */
typedef struct sc1445x_ae_vspk_state_t {
	unsigned short vol_level ;   /* 0 to SC1445x_AE_VSPK_VOL_LEVEL_COUNT */
	unsigned short dtmf_level ;  /* 0 to SC1445x_AE_VSPK_VOL_LEVEL_COUNT */
} sc1445x_ae_vspk_state ;


/* virtual microphone state */
typedef struct sc1445x_ae_vmic_state_t {
	unsigned short gain_level ;  /* 0 to SC1445x_AE_VMIC_GAIN_LEVEL_COUNT */
	unsigned short is_muted ;
} sc1445x_ae_vmic_state ;


/* audio engine interface mode */
typedef enum sc1445x_ae_iface_mode_t {
	SC1445x_AE_IFACE_MODE_HANDSET,		/* use handset only */
	SC1445x_AE_IFACE_MODE_OPEN_LISTENING,	/* use ext spk and handset */
	SC1445x_AE_IFACE_MODE_HANDS_FREE,	/* use ext spk and ext mic */
	SC1445x_AE_IFACE_MODE_HEADSET,		/* use headset only */
	SC1445x_AE_IFACE_MODE_WIRELESS,		/* use wireless headset */
	SC1445x_AE_IFACE_MODE_BT_GSM,		/* use BT module for GSM */
	SC1445x_AE_IFACE_MODE_BT_HEADSET,	/* use BT module for headset */

	SC1445x_AE_IFACE_MODE_INVALID
} sc1445x_ae_iface_mode ;

/* this is the number of virtual mics and spks */
#define SC1445x_AE_VMIC_VSPK_COUNT		SC1445x_AE_IFACE_MODE_INVALID


#if defined( SC1445x_AE_PCM_LINES_SUPPORT )

/* ATA/DECT inteconnection matrix */
/* There is an entry for each audio (VoIP) channel and each audio stream. */
/* Each entry is a bitfield that shows who this channel/stream connects to. */
/* The bitfield is implemented as 16-bit unsigned; the LSByte corresponds */
/* to audio streams; the MSByte corresponds to VoIP channels. */
typedef unsigned short sc1445x_ae_xmatrix_entry ;

typedef struct sc1445x_ae_xmatrix_t {
	/* who the VoIP channels listen to */
	sc1445x_ae_xmatrix_entry* voip_channels ;

	/* who the audio streams listen to */
	sc1445x_ae_xmatrix_entry* audio_streams ;
} sc1445x_ae_xmatrix ;

#endif

/* ATA/DECT line type */
/* (not protected by SC1445x_AE_PCM_LINES_SUPPORT ifdef */
/* because it is used by ioctls) */
typedef enum sc1445x_ae_line_type_t {
	SC1445x_AE_LINE_TYPE_ATA = 0,			/* ATA */

	SC1445x_AE_LINE_TYPE_CVM_DECT_NARROW,		/* narrowband */
							/* CVM DECT */

	SC1445x_AE_LINE_TYPE_CVM_DECT_WIDE_8KHZ_ALAW,	/* wideband CVM DECT */
							/* a-law compressed */
							/* to 8kHz */

	SC1445x_AE_LINE_TYPE_CVM_DECT_WIDE_8KHZ_ULAW,	/* wideband CVM DECT */
							/* u-law compressed */
							/* to 8kHz */

	SC1445x_AE_LINE_TYPE_CVM_DECT_WIDE_16KHZ,	/* wideband CVM DECT */
							/* at 16kHz */

	SC1445x_AE_LINE_TYPE_NATIVE_DECT_NARROW,	/* narrowband */
							/* native DECT */

	SC1445x_AE_LINE_TYPE_NATIVE_DECT_WIDE,		/* wideband */
							/* native DECT */

	SC1445x_AE_LINE_TYPE_INVALID
} sc1445x_ae_line_type ;


/* the PCM slot that a PCM device is using */
typedef enum sc1445x_ae_pcm_slot_id_t {
	SC1445x_AE_PCM_SLOT_ID_0 = 0,
	SC1445x_AE_PCM_SLOT_ID_1,
	SC1445x_AE_PCM_SLOT_ID_2,
	SC1445x_AE_PCM_SLOT_ID_3,

	SC1445x_AE_PCM_SLOT_ID_INVALID
} sc1445x_ae_pcm_slot_id ;

/* the local "endpoint" that is directly connected to a PCM device */
typedef enum sc1445x_ae_pcm_local_endpoint_t {
	SC1445x_AE_PCM_LOCAL_EP_HANDSET,	/* handset spk/mic */
	SC1445x_AE_PCM_LOCAL_EP_HANDS_FREE,	/* ext spk and ext mic */
	SC1445x_AE_PCM_LOCAL_EP_OPEN_LISTENING,	/* ext spk and handset*/

	SC1445x_AE_PCM_LOCAL_EP_INVALID
} sc1445x_ae_pcm_local_endpoint ;

/* the PCM sampling frequency that a PCM device is using */
typedef enum sc1445x_ae_pcm_freq_t {
	SC1445x_AE_PCM_FREQ_8KHZ,

	SC1445x_AE_PCM_FREQ_INVALID
} sc1445x_ae_pcm_freq ;

/* the PCM sample width that a PCM device is using */
typedef enum sc1445x_ae_pcm_sample_width_t {
	SC1445x_AE_PCM_SAMPLE_WIDTH_16BITS,

	SC1445x_AE_PCM_SAMPLE_WIDTH_INVALID
} sc1445x_ae_pcm_sample_width ;


/* the number of PCM devices that we can directly attach to, concurrently */
#define SC1445x_AE_PCM_DEV_COUNT	1

/* attached-to PCM device state */
typedef struct sc1445x_ae_pcm_dev_state_t {
	unsigned short attached ;

	sc1445x_ae_pcm_slot_id slot ;
	sc1445x_ae_pcm_local_endpoint lep ;
	sc1445x_ae_pcm_freq freq ;
	sc1445x_ae_pcm_sample_width width ;
} sc1445x_ae_pcm_dev_state ;

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
/* the VoIP-phone operation mode */
typedef struct sc1445x_ae_phone_op_mode_t {
	unsigned int voip_x_codec_classd : 1 ;	/* VoIP <--> CODEC or CLASSD */
	unsigned int voip_x_ramio_pcm    : 1 ;	/* VoIP <--> RAMIO or PCM */
	unsigned int pcm_x_codec_classd  : 1 ;	/* PCM  <--> CODEC or CLASSD */
} sc1445x_ae_phone_op_mode ;
#endif


/*** audio profile stuff ***/

/* enable and 16-bit volume value for each of the speaker volume knobs */
typedef struct sc1445x_ae_vspk_volumes_t {
	/* enable flags */
	unsigned int use_analog		: 1 ;	/* CODEC_LSR_REG.LSRATT */
	unsigned int use_rx_path_att	: 1 ;	/* DSP cmd 0x0008, 0-0x7fff */
	unsigned int use_rx_path_shift	: 1 ;	/* DSP cmd 0x0020, 0-4 */
	unsigned int use_sidetone_att	: 1 ;	/* DSP cmd 0x000a, 0-0x7fff */
	unsigned int use_ext_spk_att	: 1 ;	/* DSP cmd 0x0013, 0-0x7fff */
	unsigned int use_shift_ext_spk	: 1 ;	/* DSP cmd 0x0017, 0-4 */
	unsigned int use_tone_vol	: 1 ;	/* DSP cmd 0x9/0x7, 0-0x7fff */
	unsigned int use_classd_vout	: 1 ;	/* CLASSD_CTRL_REG.CLASSD_VOUT */

	/* volume values (for all available levels) */
	unsigned short* analog_levels ;
	unsigned short* rx_path_att_levels ;
	unsigned short* rx_path_shift_levels ;
	unsigned short* sidetone_att_levels ;
	unsigned short* ext_spk_att_levels ;
	/*unsigned short* ext_spk_att_ring_levels ;*/
	unsigned short* ring_playback_vol_levels ;
	unsigned short* shift_ext_spk_levels ;
	/*unsigned short* tone_vol_levels ;*/
	unsigned short* ring_tone_vol_levels ;
	unsigned short* call_tone_codec_vol_levels ;
	unsigned short* call_tone_classd_vol_levels ;
	unsigned short* dtmf_tone_codec_vol_levels ;
	unsigned short* dtmf_tone_classd_vol_levels ;
	unsigned short* classd_vout ;
} sc1445x_ae_vspk_volumes ;
#define sc1445x_ae_vspk_volumes_FIELDS_COUNT	13

/* enable and 16-bit volume value for each of the microphone gain knobs */
typedef struct sc1445x_ae_vmic_gains_t {
#if defined( CONFIG_SND_SC1445x_USE_PAEC )

	/* enable flags */
	unsigned int use_analog		: 1 ;	/* CODEC_MIC_REG.MIC_GAIN */
	unsigned int use_shift_paec_out : 1 ;	/* DSP cmd 0x001A, signed */
	unsigned int use_paec_tx_att	: 1 ;	/* DSP cmd 0x001B, 0-0x7fff */
	unsigned int use_attlimit	: 1 ;	/* @0x10770, 0-0x7fff */
	unsigned int use_supmin		: 1 ;	/* @0x12b86, 0-0x7fff */

	/* gain values (for all available levels) */
	unsigned short* analog_levels ;
	unsigned short* shift_paec_out_levels ;
	unsigned short* paec_tx_att_levels ;
	unsigned short* paec_tx_att_levels_BT ;
	unsigned short* attlimit ;
	unsigned short* supmin ;

#else  /* CONFIG_SND_SC1445x_USE_PAEC */

	/* enable flags */
	unsigned int use_analog		: 1 ;	/* CODEC_MIC_REG.MIC_GAIN */

	/* gain values (for all available levels) */
	unsigned short* analog_levels ;

#endif  /* CONFIG_SND_SC1445x_USE_PAEC */
} sc1445x_ae_vmic_gains ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
#  define sc1445x_ae_vmic_gains_FIELDS_COUNT	6
#else
#  define sc1445x_ae_vmic_gains_FIELDS_COUNT	1
#endif

/* audio profile */
typedef struct sc1445x_ae_audio_profile_t {
	const unsigned short vspk_lvl_count ;
	sc1445x_ae_vspk_volumes vspk_lvl_narrow[SC1445x_AE_VMIC_VSPK_COUNT] ;
	sc1445x_ae_vspk_volumes vspk_lvl_wide[SC1445x_AE_VMIC_VSPK_COUNT] ;

	const unsigned short  vmic_lvl_count ;
	sc1445x_ae_vmic_gains vmic_lvl_narrow[SC1445x_AE_VMIC_VSPK_COUNT] ;
	sc1445x_ae_vmic_gains vmic_lvl_wide[SC1445x_AE_VMIC_VSPK_COUNT] ;

	unsigned short narrowband_filters_TX1[3][FILTER_SIZE] ;
	unsigned short narrowband_filters_TX2[3][FILTER_SIZE] ;
	unsigned short narrowband_filters_TX3[3][FILTER_SIZE] ;
	unsigned short wideband_filters_TX1[3][FILTER_SIZE] ;
	unsigned short wideband_filters_TX2[3][FILTER_SIZE] ;
	unsigned short wideband_filters_TX3[3][FILTER_SIZE] ;
	unsigned short narrowband_filters_RX1[3][FILTER_SIZE] ;
	unsigned short narrowband_filters_RX2[3][FILTER_SIZE] ;
	unsigned short narrowband_filters_RX3[3][FILTER_SIZE] ;
	unsigned short wideband_filters_RX1[3][FILTER_SIZE] ;
	unsigned short wideband_filters_RX2[3][FILTER_SIZE] ;
	unsigned short wideband_filters_RX3[3][FILTER_SIZE] ;

#if defined( CONFIG_SND_SC1445x_USE_PAEC )
	/* PAEC state array setup */
	const unsigned short paec_band_count ;
	unsigned short* paec_band_loc ;
	unsigned short* paec_band_narrow ;
	unsigned short* paec_band_wide ;

	/* PAEC parameters setup */
	const unsigned short paec_data_count ;
	unsigned short* paec_data_loc ;
	unsigned short* paec_data_narrow ;
	unsigned short* paec_data_wide ;

#  if defined( HAVE_PAEC_SUPPRESSOR )
	/* Suppressor Setup*/
	const unsigned short supp_params_count ;
	unsigned short** supp_params_addr ;
	unsigned short* supp_params_data_narrow ;
	unsigned short* supp_params_data_wide ;
#  endif
#endif

#if defined( CONFIG_SND_SC1445x_USE_AEC )
	/* AEC setup */
	const unsigned short aec_params_count ;
	unsigned short* aec_params_data ;
#endif

	/*
	 * memory block used for the data of the dynamic-size arrays
	 * i.e. all of the above arrays (except for the *_filters_* arrays)
	 * actually point in here
	 */
	void* data ;
	
	/* size of data in bytes */
	unsigned data_nbytes ;
} sc1445x_ae_audio_profile ;


/* audio engine state */
typedef struct sc1445x_ae_state_t {
	/* settings shared by all audio channels */
	const unsigned short spk_count ;  /* how many speakers are there? */
	unsigned short spk_in_use ;	  /* which speaker are we using? */
	sc1445x_ae_spk_state* spks ;	  /* the state of each speaker */

	const unsigned short mic_count ;  /* how many microphones are there? */
	unsigned short mic_in_use ;	  /* which microphone are we using? */
	sc1445x_ae_mic_state* mics ;	  /* the state of each microphone */

	unsigned short AEC_enabled ;

	sc1445x_ae_mode mode ;		  /* operating mode */

	/* settings for each audio channel independently */
	const unsigned short audio_channels_count ;  /* max number of active */
						     /* audio channels */
	sc1445x_ae_channel_state* audio_channels ;	/* the state of each */
							/* audio channel */

#if defined( SC1445x_AE_USE_CONVERSATIONS )
	/* the state of each conversation */
	sc1445x_ae_conversation_state* conversations ;
#endif

#if defined( CONFIG_SC14452 )
	unsigned short ilbc_codec_count ;
#endif

	unsigned short sidetone_volume ;

	/* interface mode */
	sc1445x_ae_iface_mode iface_mode ;

	/* virtual mics and spks */
	sc1445x_ae_vmic_state vmics[SC1445x_AE_VMIC_VSPK_COUNT] ;
	sc1445x_ae_vspk_state vspks[SC1445x_AE_VMIC_VSPK_COUNT] ;

	/* tone generation */
	sc1445x_ae_tonegen_state* tonegen ;

	/* raw PCM */
	sc1445x_ae_raw_pcm_state raw_pcm ;

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	/* interconnection matrix */
	sc1445x_ae_xmatrix xmatrix ;
#endif

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	/* line types (ATA, DECT etc) */
	sc1445x_ae_line_type* line_types ;

	/* line mappings (line -> stream) */
	short* line_mappings ;
#else
#  if defined( SC1445x_AE_PHONE_DECT )
#    if 0
	/* type of DECT headset */
	sc1445x_ae_line_type dect_headset_type ;
#    endif

	/* types of DECT handsets */
	sc1445x_ae_line_type* dect_types ;
#  endif
	/* phone operation mode */
	union {
		sc1445x_ae_phone_op_mode bits ;
		unsigned int val ;
	} op_mode ;
#endif

	/* values for turning off/on CLASSD */
	unsigned short classd_ctrl_val[2] ;

	/* PCM devices that we are directly attached to */
	sc1445x_ae_pcm_dev_state pcm_dev[SC1445x_AE_PCM_DEV_COUNT] ;

	/* flag that shows if RX/TX filters are being updated */
	short updating_filters ;

	sc1445x_ae_audio_profile ap ;

	unsigned short do_disable_codec		: 1 ;
	unsigned short do_disable_classd	: 1 ;

	wait_queue_head_t fadeoutq ;

	void* private_data ;
} sc1445x_ae_state ;


#endif	/* __AUDIOENGINE_DEFS_H__ */
