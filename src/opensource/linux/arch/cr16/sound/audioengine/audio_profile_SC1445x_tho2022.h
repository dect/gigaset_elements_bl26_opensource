/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

#if !defined( __AUDIO_PROFILE_SC1445x_THO2022_H__ )
#define __AUDIO_PROFILE_SC1445x_THO2022_H__


/*	define values for spk and mic levels presented to vspk/vmic user */
/*	speaker volume levels should be defined in profile_vspk_levels */
/*	mic gain levels should be defined in profile_vmic_levels */
/*	also define wideband/narrowband filters for TX/RX */
/*	these should be defined in: */
/* 	profile_narrowband_filters_TX1, profile_narrowband_filters_TX2, */
/* 	profile_narrowband_filters_RX1, profile_narrowband_filters_RX2, */
/* 	profile_wideband_filters_TX1, profile_wideband_filters_TX2, */
/* 	profile_wideband_filters_RX1, profile_wideband_filters_RX2 */
/*	also define any AEC/PAEC parameters */
/*	paec mic digital gain moved to microphone section, comments added to all sections */

/* the number of available volume levels for virtual speakers */
#define SC1445x_AE_VSPK_VOL_LEVEL_COUNT		8

/* the number of available gain levels for virtual microphones */
#define SC1445x_AE_VMIC_GAIN_LEVEL_COUNT	8

DECLARE_VSPK_VOLUMES_STRUCT ;
DECLARE_VMIC_GAINS_STRUCT ;

/****************************************************************************************************/
/*************************** NARROWBAND RECEIVE DIRECTION VOLUME LEVELS *****************************/
/****************************************************************************************************/
/* There are five modes of operation available. The proper path of the SC14450 Gen2DSP and the 		*/
/* analog audio circuities are enabled according to the HANDSET, OPEN_LISTINING, HANDsFREE,	HEADSET	*/
/* and WIRELESS_HANDSET. Eight programmable gain steps are available for volume control	of each 	*/
/* mode. Amplification as well as attenuation units form the volume control section in order to 	*/
/* offer the best tuning flexibility. The following units are controlled through this process:		*/
/* 1) analog		--> control the receiver analog gain through CODEC_LSR_REG.LSRATT				*/
/* 2) rx_path_att	--> control the receiver digital attenuation through DSP cmd 0x0008, 0-0x7fff	*/
/* 3) ext_spk_att	--> control the speaker digital attenuation through DSP cmd 0x0013, 0-0x7fff	*/
/* 4) shift_ext_spk --> control the speaker digital gain through DSP cmd 0x0017, 0-4				*/
/****************************************************************************************************/

static struct vspk_volumes
profile_vspk_levels_narrowband[SC1445x_AE_IFACE_MODE_COUNT_IN_AP] = {
	
/********************************** HANDSET *********************************************************/
	{
		.use_analog =			1,
		.use_rx_path_att =		1,
		.use_ext_spk_att =		0,
		.use_shift_ext_spk =	0,
		.use_tone_vol =		1,


		.analog_levels =		{ 5, 5, 5, 5, 5, 5, 5, 5 },									
		.rx_path_att_levels =	{ 0x0140, 0x0fb0, 0x1e20, 0x2C90,  
								0x3B00, 0x4900, 0x6000, 0x6fc6 },		
		.ext_spk_att_levels =	{ },
		.shift_ext_spk_levels =	{ },
		.call_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.dtmf_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
	},
/********************************** OPEN_LISTINING **************************************************/
	{
		.use_analog =			1,
		.use_rx_path_att =		1,
		.use_ext_spk_att =		1,
		.use_shift_ext_spk =	1,
		.use_tone_vol =		1,

		.analog_levels =		{ 5, 5, 5, 5, 5, 5, 5, 5 },								
		.rx_path_att_levels =	{ 0x0140, 0x0fb0, 0x1e20, 0x2C90,  
								0x3B00, 0x4900, 0x6000, 0x6fc6 },		
		.ext_spk_att_levels =	{ 0x0f92, 0x1f60, 0x2f2e, 0x3efc,
								0x4eca, 0x5e98, 0x6e66, 0x7e34 },	//gflamis091208 Thomson_ST2022	
		.ring_playback_vol_levels =	{ 0x0f92, 0x1f60, 0x2f2e, 0x3efc,
								0x4eca, 0x5e98, 0x6e66, 0x7e34 },	//gflamis091208 Thomson_ST2022	
		.shift_ext_spk_levels =	{ 2, 2, 2, 2, 2, 2, 2, 2 },			//gflamis231208 Thomson_ST2022
		.ring_tone_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.call_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.call_tone_classd_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.dtmf_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.dtmf_tone_classd_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
	},
/********************************** HANDsFREE *******************************************************/
	{
		.use_analog =			0,
		.use_rx_path_att =		0,
		.use_ext_spk_att =		1,
		.use_shift_ext_spk =	1,
		.use_tone_vol =		1,

		.analog_levels =		{ },
		.rx_path_att_levels =	{ },
		.ext_spk_att_levels =	{ 0x0f92, 0x1f60, 0x2f2e, 0x3efc,
								0x4eca, 0x5e98, 0x6e66, 0x7e34 },	//gflamis091208 Thomson_ST2022
		.ring_playback_vol_levels =	{ 0x0f92, 0x1f60, 0x2f2e, 0x3efc,
								0x4eca, 0x5e98, 0x6e66, 0x7e34 },	//gflamis091208 Thomson_ST2022
		.shift_ext_spk_levels =	{ 2, 2, 2, 2, 2, 2, 2, 2 },			//gflamis231208 Thomson_ST2022
		.ring_tone_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.call_tone_classd_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.dtmf_tone_classd_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
	},
/********************************** HEADSET *******************************************************/
/* NOTE: It has not been tuned */
	{
		.use_analog =			1,
		.use_rx_path_att =		1,
		.use_ext_spk_att =		0,
		.use_shift_ext_spk =	0,
		.use_tone_vol =		1,

		.analog_levels =		{ 7, 6, 5, 4, 3, 2, 1, 0 },					
		.rx_path_att_levels =	{ 0x5ADC, 0x5ADC, 0x5ADC, 0x5ADC, 
								0x5ADC, 0x5ADC, 0x5ADC, 0x5ADC },
		.ext_spk_att_levels =	{ },
		.shift_ext_spk_levels =	{ },
		.call_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.dtmf_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
	},
/********************************** WIRELESS_HANDSET ************************************************/
/* NOTE: This is a dummy mode that disables any volume control since it is handled from the			*/
/* wireless handset device																			*/
	{
		.use_analog =			0,
		.use_rx_path_att =		0,
		.use_ext_spk_att =		0,
		.use_shift_ext_spk =	0,
		.use_tone_vol =		1,

		.analog_levels =		{ },
		.rx_path_att_levels =	{ },
		.ext_spk_att_levels =	{ },
		.shift_ext_spk_levels =	{ },
		.call_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.dtmf_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
	}	
} ;

/****************************************************************************************************/
/**************************** WIDEBAND RECEIVE DIRECTION VOLUME LEVELS ******************************/
/****************************************************************************************************/
/* There are five modes of operation available. The proper path of the SC14450 Gen2DSP and the 		*/
/* analog audio circuities are enabled according to the HANDSET, OPEN_LISTINING, HANDsFREE,	HEADSET	*/
/* and WIRELESS_HANDSET. Eight programmable gain steps are available for volume control	of each 	*/
/* mode. Amplification as well as attenuation units form the volume control section in order to 	*/
/* offer the best tuning flexibility. The following units are controlled through this process:		*/
/* 1) analog		--> control the receiver analog gain through CODEC_LSR_REG.LSRATT				*/
/* 2) rx_path_att	--> control the receiver digital attenuation through DSP cmd 0x0008, 0-0x7fff	*/
/* 3) ext_spk_att	--> control the speaker digital attenuation through DSP cmd 0x0013, 0-0x7fff	*/
/* 4) shift_ext_spk --> control the speaker digital gain through DSP cmd 0x0017, 0-4				*/
/****************************************************************************************************/

static struct vspk_volumes
profile_vspk_levels_wideband[SC1445x_AE_IFACE_MODE_COUNT_IN_AP] = {

/********************************** HANDSET *********************************************************/
	{
		.use_analog =			1,
		.use_rx_path_att =		1,
		.use_ext_spk_att =		0,
		.use_shift_ext_spk =	0,
		.use_tone_vol =		1,
	
		.analog_levels =		{ 5, 5, 5, 5, 5, 5, 5, 5 },								
		.rx_path_att_levels =	{ 0x0140, 0x0fb0, 0x1e20, 0x2C90,  
								0x3B00, 0x4900, 0x6000, 0x6fc6 },		
		.ext_spk_att_levels =	{ },
		.shift_ext_spk_levels =	{ },
		.call_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.dtmf_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
	},
/********************************** OPEN_LISTINING **************************************************/
	{
		.use_analog =			1,
		.use_rx_path_att =		1,
		.use_ext_spk_att =		1,
		.use_shift_ext_spk =	1,
		.use_tone_vol =		1,

		.analog_levels =		{ 5, 5, 5, 5, 5, 5, 5, 5 },								
		.rx_path_att_levels =	{ 0x0140, 0x0fb0, 0x1e20, 0x2C90,  
								0x3B00, 0x4900, 0x6000, 0x6fc6 },	
		.ext_spk_att_levels =	{ 0x0f92, 0x1f60, 0x2f2e, 0x3efc,
								0x4eca, 0x5e98, 0x6e66, 0x7e34 },	//gflamis091208 Thomson_ST2022
		.ring_playback_vol_levels =	{ 0x0f92, 0x1f60, 0x2f2e, 0x3efc,
								0x4eca, 0x5e98, 0x6e66, 0x7e34 },	//gflamis091208 Thomson_ST2022
		.shift_ext_spk_levels =	{ 2, 2, 2, 2, 2, 2, 2, 2 },			//gflamis231208 Thomson_ST2022
		.ring_tone_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.call_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.call_tone_classd_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.dtmf_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.dtmf_tone_classd_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
	},

/********************************** HANDsFREE *******************************************************/
	{
		.use_analog =			0,
		.use_rx_path_att =		0,
		.use_ext_spk_att =		1,
		.use_shift_ext_spk =	1,
		.use_tone_vol =		1,

		.analog_levels =		{ },
		.rx_path_att_levels =	{ },
		.ext_spk_att_levels =	{ 0x0f92, 0x1f60, 0x2f2e, 0x3efc,
								0x4eca, 0x5e98, 0x6e66, 0x7e34 },	//gflamis091208 Thomson_ST2022
		.ring_playback_vol_levels =	{ 0x0f92, 0x1f60, 0x2f2e, 0x3efc,
								0x4eca, 0x5e98, 0x6e66, 0x7e34 },	//gflamis091208 Thomson_ST2022
		.shift_ext_spk_levels =	{ 2, 2, 2, 2, 2, 2, 2, 2 },			//gflamis231208 Thomson_ST2022
		.ring_tone_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.call_tone_classd_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.dtmf_tone_classd_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
	},

/********************************** HEADSET *******************************************************/
/* NOTE: It has not been tuned */
	{
		.use_analog =			1,
		.use_rx_path_att =		1,
		.use_ext_spk_att =		0,
		.use_shift_ext_spk =	0,
		.use_tone_vol =		1,


		.analog_levels =		{ 7, 6, 5, 4, 3, 2, 1, 0 },
		.rx_path_att_levels =	{ 0x6DFA, 0x6DFA, 0x6DFA, 0x6DFA, 
								0x6DFA, 0x6DFA, 0x6DFA, 0x6DFA },
		.ext_spk_att_levels =	{ },
		.shift_ext_spk_levels =	{ },
		.call_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.dtmf_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
	},
/********************************** WIRELESS_HANDSET ************************************************/
/* NOTE: This is a dummy mode that disables any volume control since it is handled from the			*/
/* wireless handset device																			*/
	{
		.use_analog =			0,
		.use_rx_path_att =		0,
		.use_ext_spk_att =		0,
		.use_shift_ext_spk =	0,
		.use_tone_vol =		1,


		.analog_levels =		{ },
		.rx_path_att_levels =	{ },
		.ext_spk_att_levels =	{ },
		.shift_ext_spk_levels =	{ },
		.call_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
		.dtmf_tone_codec_vol_levels =	{ 0x3000, 0x3000, 0x3000, 0x3000,
								0x3000, 0x3000, 0x3000, 0x3000 },
	}	
} ;

/****************************************************************************************************/
/*************************** NARROWBAND TRANSMIT DIRECTION VOLUME LEVELS *****************************/
/****************************************************************************************************/
/* There are five modes of operation available. The proper path of the SC14450 Gen2DSP and the 		*/
/* analog audio circuities are enabled according to the HANDSET, OPEN_LISTINING, HANDsFREE,	HEADSET	*/
/* and WIRELESS_HANDSET. Eight programmable gain steps are available for volume control	of each 	*/
/* mode. Amplification as well as attenuation units form the volume control section in order to 	*/
/* offer the best tuning flexibility. The following units are controlled through this process:		*/
/* 1) analog		 --> control the transmit analog gain through CODEC_MIC_REG.MIC_GAIN			*/
/* 2) shift_paec_out --> control the transmit digital amplification through DSP cmd 0x001A, 0-4		*/
/* 3) paec_tx_att	 --> control the transmit digital attenuation through DSP cmd 0x001B, 0-0x7fff	*/
/****************************************************************************************************/

static struct vmic_gains
profile_vmic_levels_narrowband[SC1445x_AE_IFACE_MODE_COUNT_IN_AP] = {

/********************************** HANDSET *********************************************************/
	{
		.use_analog =			1,
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
	#endif

		.analog_levels =		{ 4, 4, 4, 4, 4, 4, 4, 4 },				//gflamis271008 Thomson_ST2022
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels ={ 0, 0, 0, 0, 0, 0, 0, 0 },				//gflamis230908 SiTel_Handset
		.paec_tx_att_levels =	{ 0x7fff, 0x7fff, 0x7fff, 0x7fff,
								0x7fff, 0x7fff, 0x7fff, 0x7fff },		//gflamis230908 SiTel_Handset
	#endif
	},
/********************************** OPEN_LISTINING **************************************************/
	{
		.use_analog =			1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
#endif

		.analog_levels =		{ 4, 4, 4, 4, 4, 4, 4, 4 },				//gflamis271008 Thomson_ST2022
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels ={ 0, 0, 0, 0, 0, 0, 0, 0 },			
		.paec_tx_att_levels =	{ 0x7fff, 0x7fff, 0x7fff, 0x7fff,
								0x7fff, 0x7fff, 0x7fff, 0x7fff },	
#endif
	},
/********************************** HANDsFREE *******************************************************/
	{
		.use_analog =			1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
#endif

		.analog_levels =		{ 10, 10, 10, 10, 10, 10, 10, 10 },			//gflamis091208 Thomson_ST2022
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels ={ 1, 1, 1, 1, 1, 1, 1, 1 },					//gflamis271008 Thomson_ST2022
		.paec_tx_att_levels =	{ 0x7FFF, 0x7FFF, 0x7FFF, 0x7FFF,
								0x7FFF, 0x7FFF, 0x7FFF, 0x7FFF },
#endif
	},
/********************************** HEADSET *******************************************************/
/* NOTE: It has not been tuned */
	{
		.use_analog =			1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
#endif

		.analog_levels =		{ 9, 9, 9, 9, 9, 9, 9, 9 },		
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels ={ 0, 0, 0, 0, 0, 0, 0, 0 },
		.paec_tx_att_levels =	{ 0x7FFF, 0x7FFF, 0x7FFF, 0x7FFF,
								0x7FFF, 0x7FFF, 0x7FFF, 0x7FFF },
#endif
	},
/********************************** WIRELESS_HANDSET ************************************************/
/* NOTE: This is a dummy mode that disables any volume control since it is handled from the			*/
/* wireless handset device																			*/
	{
		.use_analog =			0,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	0,
		.use_paec_tx_att =		0,
#endif

		.analog_levels =		{ },
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels ={ },
		.paec_tx_att_levels =	{ },
#endif
	}	
} ;

/****************************************************************************************************/
/**************************** WIDEBAND TRANSMIT DIRECTION VOLUME LEVELS *****************************/
/****************************************************************************************************/
/* There are five modes of operation available. The proper path of the SC14450 Gen2DSP and the 		*/
/* analog audio circuities are enabled according to the HANDSET, OPEN_LISTINING, HANDsFREE,	HEADSET	*/
/* and WIRELESS_HANDSET. Eight programmable gain steps are available for volume control	of each 	*/
/* mode. Amplification as well as attenuation units form the volume control section in order to 	*/
/* offer the best tuning flexibility. The following units are controlled through this process:		*/
/* 1) analog		 --> control the transmit analog gain through CODEC_MIC_REG.MIC_GAIN			*/
/* 2) shift_paec_out --> control the transmit digital amplification through DSP cmd 0x001A, 0-4		*/
/* 3) paec_tx_att	 --> control the transmit digital attenuation through DSP cmd 0x001B, 0-0x7fff	*/
/****************************************************************************************************/

static struct vmic_gains
profile_vmic_levels_wideband[SC1445x_AE_IFACE_MODE_COUNT_IN_AP] = {

/********************************** HANDSET *********************************************************/
	{
		.use_analog =			1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
#endif

		.analog_levels =	{ 4, 4, 4, 4, 4, 4, 4, 4 },				//gflamis271008 Thomson_ST2022
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels ={ 0, 0, 0, 0, 0, 0, 0, 0 },			//gflamis230908 SiTel_Handset
		.paec_tx_att_levels =	{ 0x7fff, 0x7fff, 0x7fff, 0x7fff,
								0x7fff, 0x7fff, 0x7fff, 0x7fff },	//gflamis230908 SiTel_Handset
#endif
	},
/********************************** OPEN_LISTINING **************************************************/
	{
		.use_analog =			1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
#endif

		.analog_levels =		{ 4, 4, 4, 4, 4, 4, 4, 4 },				//gflamis271008 Thomson_ST2022
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels ={ 0, 0, 0, 0, 0, 0, 0, 0 },				//gflamis230908 SiTel_Handset
		.paec_tx_att_levels =	{ 0x7fff, 0x7fff, 0x7fff, 0x7fff,
								0x7fff, 0x7fff, 0x7fff, 0x7fff },		//gflamis230908 SiTel_Handset
#endif
	},
/********************************** HANDsFREE *******************************************************/
	{
		.use_analog =			1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
#endif

		.analog_levels =		{ 10, 10, 10, 10, 10, 10, 10, 10 },			//gflamis091208 Thomson_ST2022
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels ={ 1, 1, 1, 1, 1, 1, 1, 1 },			//gflamis271008 Thomson_ST2022
		.paec_tx_att_levels =	{ 0x7FFF, 0x7FFF, 0x7FFF, 0x7FFF,
								0x7FFF, 0x7FFF, 0x7FFF, 0x7FFF },
#endif
	},
/********************************** HEADSET *******************************************************/
/* NOTE: It has not been tuned */
	{
		.use_analog =			1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
#endif

		.analog_levels =		{ 6, 6, 6, 6, 6, 6, 6, 6 },		
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels ={ 0, 0, 0, 0, 0, 0, 0, 0 },
		.paec_tx_att_levels =	{ 0x7FFF, 0x7FFF, 0x7FFF, 0x7FFF,
								0x7FFF, 0x7FFF, 0x7FFF, 0x7FFF },
#endif
	},
/********************************** WIRELESS_HANDSET ************************************************/
/* NOTE: This is a dummy mode that disables any volume control since it is handled from the			*/
/* wireless handset device																			*/
	{
		.use_analog =			0,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	0,
		.use_paec_tx_att =		0,
#endif

		.analog_levels =		{ },
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels ={ },
		.paec_tx_att_levels =	{ },
#endif
	}	
} ;

/****************************************************************************************************/
/****************************** NARROWBAND FREQUENCY RESPONSE FILTERS *******************************/
/****************************************************************************************************/
/* The available filters are responsible of the frequency mask shape for each direction.			*/
/* They consist of two cascaded IIR filter that construct a bandpass filter. Their coefficients are	*/
/* separated in two categories:	i) "handset/headset" for the handset/headset acoustic components 	*/
/*							   ii) "external" for the handsfree acoustic components					*/
/****************************************************************************************************/

/****************************** TRANSMIT DIRECTION FILTER COEFFICIENTS *******************************/
static const unsigned short profile_narrowband_filters_TX1[2][FILTER_SIZE] = {
	/* handset/headset */
	{ 0x4000, 0xA3A3, 0x1C60, 0x5054, 0xE9C6, 0x7FFF, 0x4000 },
	/* external */
	{ 0x4000, 0x9C64, 0x23C2, 0x79FE, 0xFF04, 0x2DF7, 0x2FE0 }
} ;
static const unsigned short profile_narrowband_filters_TX2[2][FILTER_SIZE] = {
	/* handset/headset */
	{ 0x4000, 0x7790, 0x37DB, 0xA467, 0xD76B, 0x6666, 0x4000 },
	/* external */
	{ 0x4000, 0x279A, 0x0006, 0xD482, 0xFBF7, 0x3BA3, 0x2FE0 }
} ;
static const unsigned short profile_narrowband_filters_TX3[3][FILTER_SIZE] = { //
	/* handset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
};
/****************************** RECEIVE DIRECTION FILTER COEFFICIENTS *******************************/
static const unsigned short profile_narrowband_filters_RX1[2][FILTER_SIZE] = {
	/* handset/headset */
	{ 0x4000, 0x82D3, 0x3DA7, 0x7D0B, 0xC24D, 0x7332, 0x4000 },
	/* external */
	{ 0x4000, 0x8135, 0x3EE7, 0x7532, 0xC9BA, 0x3A98, 0x5053 }
} ;
static const unsigned short profile_narrowband_filters_RX2[2][FILTER_SIZE] = {
	/* handset/headset */
	{ 0x4000, 0xC0AF, 0x0191, 0x7144, 0xF486, 0x7FFF, 0x1000 },
	/* external */
	{ 0x4000, 0x3CB0, 0x058F, 0xA613, 0xFFE5, 0x1B58, 0x3B3F }
} ;
static const unsigned short profile_narrowband_filters_RX3[3][FILTER_SIZE] = { //
	/* handset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
};

/****************************************************************************************************/
/******************************* WIDEBAND FREQUENCY RESPONSE FILTERS ********************************/
/****************************************************************************************************/
/* The available filters are responsible of the frequency mask shape for each direction.			*/
/* They consist of two cascaded IIR filter that construct a bandpass filter. Their coefficients are	*/
/* separated in two categories:	i) "handset/headset" for the handset/headset acoustic components 	*/
/*							   ii) "external" for the handsfree acoustic components					*/
/****************************************************************************************************/

/****************************** TRANSMIT DIRECTION FILTER COEFFICIENTS *******************************/
static const unsigned short profile_wideband_filters_TX1[2][FILTER_SIZE] = {
	/* handset/headset */
	{ 0x4000, 0x8175, 0x3E8E, 0x7D22, 0xC2C8, 0x3E12, 0x5F40 },
	/* external */
	{ 0x4000, 0xA772, 0x1947, 0x4C40, 0xF1F0, 0x3AF8, 0x5388 }
//	{ 0x4000, 0x852E, 0x3B01, 0x7A4F, 0xC530, 0x3448, 0x5000 }		//gflamis_220908 low cut (wideband stationary noise reduction)
} ;
static const unsigned short profile_wideband_filters_TX2[2][FILTER_SIZE] = {
	/* handset/headset */
	{ 0x4000, 0xBB46, 0x123D, 0x41D1, 0xEE3F, 0x6F90, 0x4FA0 },
	/* external */
	{ 0x4000, 0x3D1B, 0x0F64, 0xAAF1, 0xDB98, 0x3710, 0x2AF8 }
//	{ 0x4000, 0xCEB5, 0x006E, 0x714B, 0xE9C8, 0x3448, 0x2000 }		//gflamis_220908 low cut (wideband stationary noise reduction)
} ;
static const unsigned short profile_wideband_filters_TX3[3][FILTER_SIZE] = { //
	/* handset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
};
/****************************** RECEIVE DIRECTION FILTER COEFFICIENTS *******************************/
static const unsigned short profile_wideband_filters_RX1[2][FILTER_SIZE] = {
	/* handset/headset */
	{ 0x4000, 0x81EA, 0x3E1A, 0x7A60, 0xC561, 0x3EE0, 0x5770 },
	/* external */
	{ 0x4000, 0xB6F3, 0x0CB2, 0x515F, 0xEB37, 0x71A8, 0x4000 }
} ;
static const unsigned short profile_wideband_filters_RX2[2][FILTER_SIZE] = {
	/* handset/headset */
	{ 0x4000, 0x10AC, 0x000A, 0xE645, 0xEA4C, 0x36F3, 0x2FEB },
	/* external */
	{ 0x4000, 0x29F4, 0x0A5E, 0xC1FA, 0xD9A3, 0x1BB8, 0x36B0 }
} ;
static const unsigned short profile_wideband_filters_RX3[3][FILTER_SIZE] = { //
	/* handset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
};

/****************************************************************************************************/
/************************************ Acoustic Echo Canceller ***************************************/
/****************************************************************************************************/
/* The following values are the narrowband / wideband settings of the same parameters as well as	*/
/* the parameters that can be adjusted toward the fine tuning of the PAEC and the SUPPRESSOR		*/
/* algorithms.																						*/
/****************************************************************************************************/

#if defined( CONFIG_SND_SC1445x_USE_PAEC )
/* PAEC state array setup */
static const unsigned short profile_paec_band_loc[] = {
	  0,   21,   42,   63,   84,  105,  126,  147,  168,  189,  210,  231,  252,  273,  294,
	  1,   22,   43,   64,   85,  106,  127,  148,  169,  190,  211,  232,  253,  274,  295,
	  2,   23,   44,   65,   86,  107,  128,  149,  170,  191,  212,  233,  254,  275,	296,
	  3,   24,   45,   66,   87,  108,  129,  150,  171,  192,  213,  234,  255,  276,  297,
	 12,   33,   54,   75,   96,  117,  138,  159,  180,  201,  222,  243,  264,  285,  306
} ;

static const unsigned short profile_paec_band_narrowband[] = {
	0x0002, 0x0004, 0x0006, 0x0008, 0x000a, 0x000e, 0x0013, 0x001a, 0x0022, 0x002d, 0x0036, 0x0041, 0x0000, 0x0000, 0x0000,
	0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001,
	0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001,
	0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001,
	0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080, 0x0080
} ;
static const unsigned short profile_paec_band_wideband[] = {
	0x0002, 0x0003, 0x0004, 0x0005, 0x0006, 0x0008, 0x000a, 0x000d, 0x0011, 0x0016, 0x001c, 0x0023, 0x002c, 0x0036, 0x0041,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
	0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001
} ;

/* PAEC parameters setup */
static const unsigned short profile_paec_data_loc[] = {
	22, 29, 34, 36, 40, 44, 45,			//attlimit added
	48, 49, 53, 54, 55, 56
} ;
static const unsigned short profile_paec_data_narrowband[] = {
	0x000c, 0xffb0, 0x0106, 0x0058, 0x036A, 0x000d, 0x020c, 
	0x6666, 0x0ea1, 0x1800, 0x2000, 0x0666, 0x7000		
} ;
static const unsigned short profile_paec_data_wideband[] = {		
	0x000f, 0xff60, 0x0083, 0x0058, 0x01b5, 0x0007, 0x0106, 
	0x3333, 0x0750, 0x4000, 0x3000, 0x0333, 0x6000
} ;


#  if defined( HAVE_PAEC_SUPPRESSOR )
/* Suppressor Setup*/
static unsigned short* const profile_supp_params_addr[] = {
	(void*)0x10718, (void*)0x1071a,		//plevdet_data_spk fall / rise time
	(void*)0x107f2, (void*)0x107f4,		//plevdet_data_paec_out fall / rise time 
	(void*)0x11a7e, (void*)0x11a82,		//pnlev_tx_data fall / rise time
	(void*)0x109b8,						//c2_thresh
	(void*)0x109ba,						//cntrini 
	(void*)0x109c4, (void*)0x109be,		//rbetaoff / off 
	(void*)0x109c6, (void*)0x109c0		//rbetaon / on
} ;

static const unsigned short profile_supp_params_data_narrowband[] = {
	0x7dbc, 0x7ce0,						//plevdet_data_spk fall / rise time
	0x7dbc, 0x7ce0,						//plevdet_data_paec_out fall / rise time
	0x7c0e, 0x7f76,						//pnlev_tx_data fall / rise time
	0xf100,								//c2_thresh
	0x0080,								//cntrini 
	0x70f4, 0x0f0b,						//rbetaoff / off 
	0x70f4, 0x0555						//rbetaon / on
} ;

static const unsigned short profile_supp_params_data_wideband[] = {
	0x7edc, 0x7e03,						//plevdet_data_spk fall / rise time
	0x7edc, 0x7e03,						//plevdet_data_paec_out fall / rise time
	0x7e03, 0x7fbb,						//pnlev_tx_data fall / rise time
	0xf100,								//c2_thresh
	0x0080,								//cntrini PAEC3 281108
	0x783f, 0x07c1,						//rbetaoff / off 
	0x783f, 0x02c0						//rbetaon / on
} ;
#  endif
#endif



#if defined( CONFIG_SND_SC1445x_USE_AEC )
/* AEC setup */
static const unsigned short profile_aec_params_data[] = {
	0x4000, 0x0100, 0x7FFF, 0x0020, 0x0666, 0x1000, 0x0200, 0x2000,
	0x3000, 0x0200, 0x0020, 0x0020, 0x0100, 0x0100, 0x0040, 0x0040,
	0x0020, 0x0020, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x07D0, 0x0069, 0x8000, 0x8000, 0x0100, 0x0200,
	0x7DBC, 0x7C10, 0x7DBC, 0x7C10, 0x7DBC, 0x7C10, 0x7FDF, 0x7FFF,
	0x7DBC, 0x7C10, 0x0014, 0x7C10, 0x0148, 0x7FF2, 0x0014, 0x7C10,
	0x01CF, 0x7FF2, 0x7DBC, 0x7C10, 0x7DBC, 0x7C10, 0x7DBC, 0x7C10,
	0x4000, 0x0000, 0xF333, 0x0200, 0x0000, 0x7FFF, 0x4000, 0x7FFF,
	0x4000, 0x0247, 0x7FFF, 0x7FFF, 0x287A, 0x0247, 0x01DB, 0x0032,
	0x7F15, 0x06BE, 0x7C5E, 0x4000, 0x0000, 0xA562, 0x0000, 0x0000,
	0xFA97, 0xAA74, 0x051E, 0x061E, 0x0278, 0x0298, 0x0000, 0x0000,
	0x0000, 0x0002, 0x0000, 0x0000, 0x0000, 0x0000, 0x101D, 0x101D,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0244, 0x03F0, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0207, 0x0207, 0x0207, 0x0207, 0x0000, 0x0000, 0x0000
} ;
#endif

#endif  /* __AUDIO_PROFILE_SC1445x_DEMO_V2_DK_SHORT4_H__ */


