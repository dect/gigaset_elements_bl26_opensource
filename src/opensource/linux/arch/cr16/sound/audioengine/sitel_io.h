/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * SW and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */
#ifndef SITEL_IO_H
#define SITEL_IO_H

#if 0
typedef unsigned char      uint8;
typedef char               int8;
typedef unsigned short     uint16;
typedef short              int16;
typedef unsigned long int  uint32;
typedef long int           int32;

typedef unsigned char      bool;

typedef enum
    {
        FALSE  = 0,
        TRUE   = 1
    } BOOLEAN, BOOL;

#ifndef NULL
#define NULL (uint8*)(-1)
#endif

#define __data24
#endif


typedef unsigned char      BYTE;
typedef unsigned short     WORD;
typedef unsigned long int  DWORD;


#define __far
    
#include "sc14450.h"
#endif
