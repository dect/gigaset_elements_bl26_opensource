/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

#if !defined( __AUDIO_PROFILE_SC1445x_F_G2_BOARD_H__ )
#define __AUDIO_PROFILE_SC1445x_F_G2_BOARD_H__
#define PAECv4



/*	define values for spk and mic levels presented to vspk/vmic user */
/*	speaker volume levels should be defined in profile_vspk_levels */
/*	mic gain levels should be defined in profile_vmic_levels */
/*	also define wideband/narrowband filters for TX/RX */
/*	these should be defined in: */
/* 	profile_narrowband_filters_TX1, profile_narrowband_filters_TX2, */
/* 	profile_narrowband_filters_RX1, profile_narrowband_filters_RX2, */
/* 	profile_wideband_filters_TX1, profile_wideband_filters_TX2, */
/* 	profile_wideband_filters_RX1, profile_wideband_filters_RX2 */
/*	also define any AEC/PAEC parameters */
/*	paec mic digital gain moved to microphone section, comments added to all sections */
//	gflamis240409  	--> Implementation of the RLR/SLR and frequency response according to the measurements took place in Taiwan.
//	gflamis190609	--> Volume control spanned at 10 steps  

/* the number of available volume levels for virtual speakers */
#define SC1445x_AE_VSPK_VOL_LEVEL_COUNT		15   //Allen

/* the number of available gain levels for virtual microphones */
#define SC1445x_AE_VMIC_GAIN_LEVEL_COUNT	15   //Allen

DECLARE_VSPK_CUSTOM_PARAMS_STRUCTS( 0 ) ;
DECLARE_VSPK_VOLUMES_STRUCT ;
DECLARE_VMIC_CUSTOM_PARAMS_STRUCTS( 0 ) ;
DECLARE_VMIC_GAINS_STRUCT ;


/****************************************************************************************************/
/*************************** NARROWBAND RECEIVE DIRECTION VOLUME LEVELS *****************************/
/****************************************************************************************************/
/* There are five modes of operation available. The proper path of the SC14452 Gen2DSP and the 		*/
/* analog audio circuities are enabled according to the HANDSET, OPEN_LISTINING, HANDsFREE,	HEADSET	*/
/* and WIRELESS_HANDSET.The number of gain steps is programmable for volume control	of each mode.	*/
/* Amplification as well as attenuation units from the volume control section in order to offer	the */
/* best tuning flexibility. The following units are controlled through this process:				*/
/* 1) analog		 --> control the receiver analog gain through CODEC_LSR_REG.LSRATT				*/
/* 2) rx_path_att	 --> control the receiver digital attenuation through DSP cmd 0x0008, 0-0x7fff	*/
/* 3) rx_path_shift	 --> control the receiver digital gain through DSP cmd 0x0020, 0-4				*/
/* 4) sidetone_att	 --> control the sidetone attenuation through DSP cmd 0x000A, 0-0x7fff			*/
/* 5) ext_spk_att	 --> control the speaker digital attenuation through DSP cmd 0x0013, 0-0x7fff	*/
/* 6) shift_ext_spk  --> control the speaker digital gain through DSP cmd 0x0017, 0-4				*/
/* 7) tone_vol		 --> dtmf/key/ring tones loudness control (from tonegens)						*/
/* 8) pcm_gain_pre	 --> playback ring/sounds digital attenuation through DSP cmd 0x002A, 0-0x7fff	*/
/* 9) pcm_gain_shift --> playback ring/sounds digital gain through DSP cmd 0x002A, 0-4				*/
/****************************************************************************************************/

static struct vspk_volumes
profile_vspk_levels_narrowband[SC1445x_AE_IFACE_MODE_COUNT_IN_AP] = {
	
/********************************** HANDSET *********************************************************/
	{
		.use_analog 		= 1,
		.use_rx_path_att 	= 1,
		.use_rx_path_shift	= 1,
		.use_sidetone_att 	= 1,
		.use_ext_spk_att 	= 1,
		.use_shift_ext_spk 	= 1,
		.use_tone_vol 		= 1,
		.use_classd_vout	= 1,
		.use_pcm_gain_pre 	= 1,
		.use_pcm_gain_shift = 1,
		.use_loopgain_rxpl  = 1, 
		.use_ng_threshold	= 1,
		
		.analog_levels 					= { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 },	//160211GF
		.rx_path_att_levels 			= { 0x01A0, 0x0350, 0x0500, 0x0750, 0x0AB0, 0x08CF, 0x0E50,  
											0x1300, 0x1A00, 0x2000, 0x2C00, 0x3B00, 0x4D00, 0x6200, 0x7FFF },  //250511Annie
		//					  { 0x01A0, 0x0230, 0x02EE, 0x0520, 0x0820, 0x0D00, 0x13F0,  
		//									0x1E00, 0x1F00, 0x2000, 0x2E00, 0x2200, 0x3000, 0x4400, 0x6300 },  //220411Annie
		.rx_path_shift_levels 			= { 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 }, 	//250511Annie
		//					  { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2 }, 	//100311Annie
		.sidetone_att_levels 			= { 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300,
											0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300 },
		.call_tone_codec_vol_levels 	= { 0x0250, 0x0350, 0x0500, 0x0650, 0x0800, 0x0b00, 0x1000, 
											0x1600, 0x1b00, 0x2000, 0x2600, 0x2A00, 0x3600, 0x4300, 0x5500 }, 
		.dtmf_tone_codec_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		.pcm_gain_pre 					= { 0x1000, 0x2000, 0x3000, 0x4000, 0x5000, 0x6000, 0x7000, 0x7fff,
											0x5000, 0x6000, 0x7000, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.pcm_gain_shift 				= { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1 },
		.pcm_gain_pre_ringtone 			= { 0x0100, 0x0200, 0x02e0,0x03F0, 0x058A, 0x0800, 0x0BD9, 0x1214,  
											0x171A, 0x1FDB, 0x2AC6, 0x357B, 0x3F0D, 0x47FA, 0x2ADf },
		.pcm_gain_shift_ringtone 		= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1   },
		.ext_spk_att_levels 			= { 0x0900, 0x1A00, 0x2B00, 0x3F00, 0x5200 ,0x5500, 0x7200, 0x4E00,  
											0x5600, 0x6000, 0x7E00, 0x5C00, 0x7200 ,0x4D00, 0x6F00 },	//250511Annie	
		//		                          { 0x090f, 0x1200, 0x2000, 0x2E00, 0x4200 ,0x5500, 0x6500, 0x7FFF,  
		//									0x4E00, 0x6000, 0x7E00, 0x4A00, 0x5C00 ,0x7200, 0x4D00 },	//230511Annie	
		.shift_ext_spk_levels 			= { 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3  },	//250511Annie	
		//					  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 2  },	//100311Annie					
		.ring_playback_vol_levels 		= { 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff ,0x7fff, 
											0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },	
		.ring_tone_vol_levels 			= { 0x1000, 0x1500, 0x1c00, 0x2500, 0x3000, 0x4700, 0x5b00 ,0x7500, 
											0x7FFF, 0x4900, 0x5500, 0x6500, 0x7200, 0x7fff, 0x5100 },				
		.call_tone_classd_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
        .dtmf_tone_classd_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		.classd_vout 					= { 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 },	
		.loopgain_rxpl_levels			= { 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000,
											0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x4000},	//230511Annie
		.ng_threshold_levels			= { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
											0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000},									

	},
/********************************** OPEN_LISTINING **************************************************/
	{
		.use_analog 		= 1,
		.use_rx_path_att 	= 1,
		.use_rx_path_shift	= 1,
		.use_sidetone_att 	= 1,
		.use_ext_spk_att 	= 1,
		.use_shift_ext_spk 	= 1,
		.use_tone_vol 		= 1,
		.use_classd_vout	= 1,
		.use_pcm_gain_pre 	= 1,
		.use_pcm_gain_shift = 1,
		.use_loopgain_rxpl        = 1, 
		.use_ng_threshold	= 1,
		
		.analog_levels 					= { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 },	//160211GF
		.rx_path_att_levels 			= { 0x01A0, 0x0350, 0x0500, 0x0750, 0x0AB0, 0x08CF, 0x0E50,  
											0x1300, 0x1A00, 0x2000, 0x2C00, 0x3B00, 0x4D00, 0x6200, 0x7FFF },  //250511Annie
		//				  	  { 0x01A0, 0x0230, 0x02EE, 0x0520, 0x0820, 0x0D00, 0x13F0,  
		//									0x1E00, 0x1F00, 0x2000, 0x2E00, 0x2200, 0x3000, 0x4400, 0x6300 },  //220411Annie
		.rx_path_shift_levels 			= { 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 }, 	//250511Annie
		.sidetone_att_levels 			= { 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300,
											0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300 },
		.call_tone_codec_vol_levels 	= { 0x0250, 0x0350, 0x0500, 0x0650, 0x0800, 0x0b00, 0x1000, 
											0x1600, 0x1b00, 0x2000, 0x2600, 0x2A00, 0x3600, 0x4300, 0x5500 }, 
		.dtmf_tone_codec_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		.pcm_gain_pre 					= { 0x1000, 0x2000, 0x3000, 0x4000, 0x5000, 0x6000, 0x7000, 0x7fff,
											0x5000, 0x6000, 0x7000, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.pcm_gain_shift 				= { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1 },
		.pcm_gain_pre_ringtone 			= { 0x0100, 0x0200, 0x02e0,0x03F0, 0x058A, 0x0800, 0x0BD9, 0x1214,  
											0x171A, 0x1FDB, 0x2AC6, 0x357B, 0x3F0D, 0x47FA, 0x2ADf },
		.pcm_gain_shift_ringtone 		= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1   },
		.ext_spk_att_levels 			= { 0x0900, 0x1A00, 0x2B00, 0x3F00, 0x5200 ,0x5500, 0x7200, 0x4E00,  
											0x5600, 0x6000, 0x7E00, 0x5C00, 0x7200 ,0x4D00, 0x6F00 },	//250511Annie
		//					  { 0x090f, 0x1200, 0x2000, 0x2E00, 0x4200 ,0x5500, 0x6500, 0x7FFF,  
		//									0x4E00, 0x6000, 0x7E00, 0x4A00, 0x5C00 ,0x7200, 0x4D00 },	//230511Annie
		.shift_ext_spk_levels 			= { 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3  },	//250511Annie
		//					  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 2  },	//100311Annie					
		.ring_playback_vol_levels 		= { 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff ,0x7fff, 
											0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },	
		.ring_tone_vol_levels 			= { 0x1000, 0x1500, 0x1c00, 0x2500, 0x3000, 0x4700, 0x5b00 ,0x7500, 
											0x7FFF, 0x4900, 0x5500, 0x6500, 0x7200, 0x7fff, 0x5100 },				
		.call_tone_classd_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		.dtmf_tone_classd_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },

		.classd_vout 					= { 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 },	
		.loopgain_rxpl_levels			= { 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000,
											0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x4000},	//230511Annie
		.ng_threshold_levels			= { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
											0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000},									

	},
/********************************** HANDsFREE *****************************************************/
	{
		.use_analog 		= 1,
		.use_rx_path_att 	= 1,
		.use_rx_path_shift	= 1,
		.use_sidetone_att 	= 1,
		.use_ext_spk_att 	= 1,
		.use_shift_ext_spk 	= 1,
		.use_tone_vol 		= 1,
		.use_classd_vout	= 1,
		.use_pcm_gain_pre 	= 1,
		.use_pcm_gain_shift = 1,
		.use_loopgain_rxpl        = 1, 
		.use_ng_threshold	= 1,
		
		.analog_levels 					= { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 },	//160211GF
		.rx_path_att_levels 			= { 0x01A0, 0x0350, 0x0500, 0x0750, 0x0AB0, 0x08CF, 0x0E50,  
											0x1300, 0x1A00, 0x2000, 0x2C00, 0x3B00, 0x4D00, 0x6200, 0x7FFF },  //250511Annie
		//					  { 0x01A0, 0x0230, 0x02EE, 0x0520, 0x0820, 0x0D00, 0x13F0,  
		//									0x1E00, 0x1F00, 0x2000, 0x2E00, 0x2200, 0x3000, 0x4400, 0x6300 },  //220411Annie
		.rx_path_shift_levels 			= { 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 }, 	//250511Annie
		//					  { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2 }, 	//100311Annie
		.sidetone_att_levels 			= { 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300,
											0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300 },
		.call_tone_codec_vol_levels 	= { 0x0250, 0x0350, 0x0500, 0x0650, 0x0800, 0x0b00, 0x1000, 
											0x1600, 0x1b00, 0x2000, 0x2600, 0x2A00, 0x3600, 0x4300, 0x5500 }, 
		.dtmf_tone_codec_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		.pcm_gain_pre 					= { 0x1000, 0x2000, 0x3000, 0x4000, 0x5000, 0x6000, 0x7000, 0x7fff,
											0x5000, 0x6000, 0x7000, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.pcm_gain_shift 				= { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1 },
		.pcm_gain_pre_ringtone 			= { 0x0100, 0x0200, 0x02e0,0x03F0, 0x058A, 0x0800, 0x0BD9, 0x1214,  
											0x171A, 0x1FDB, 0x2AC6, 0x357B, 0x3F0D, 0x47FA, 0x2ADf },
		.pcm_gain_shift_ringtone 		= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1  },
		//                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2  },
		.ext_spk_att_levels 			= { 0x0900, 0x1A00, 0x2B00, 0x3F00, 0x5200 ,0x5500, 0x7200, 0x4E00,  
											0x5600, 0x6000, 0x7E00, 0x7200, 0x4D00 ,0x6F00, 0x6600 },	//250511Annie
		//					  { 0x090f, 0x1200, 0x2000, 0x2E00, 0x4200 ,0x5500, 0x6500, 0x7FFF,  
		//									0x4E00, 0x6000, 0x7E00, 0x4A00, 0x5C00 ,0x7200, 0x4D00 },	//230511Annie	
		.shift_ext_spk_levels 			= { 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 3, 3, 4  },	//250511Annie
		//					  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 2  },	//100311Annie					
		.ring_playback_vol_levels 		= { 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff ,0x7fff, 
											0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },	
		.ring_tone_vol_levels 			= { 0x1000, 0x1500, 0x1c00, 0x2500, 0x3000, 0x4700, 0x5b00 ,0x7500, 
											0x7FFF, 0x4900, 0x5500, 0x6500, 0x7200, 0x7fff, 0x5100 },				
		.call_tone_classd_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7FFF },
	    .dtmf_tone_classd_vol_levels 	={ 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		.classd_vout 					= { 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 },	
		.loopgain_rxpl_levels			= { 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000,
											0x5000, 0x5000, 0x5000, 0x5000, 0x4000, 0x4000, 0x4000},	//230511Annie
		.ng_threshold_levels			= { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
											0x0000, 0x0000, 0x0000, 0x0000, 0x0029, 0x0029, 0x0029},											
	},
/********************************** HEADSET *******************************************************/
	{
		.use_analog 		= 1,
		.use_rx_path_att 	= 1,
		.use_rx_path_shift	= 1,
		.use_sidetone_att 	= 1,
		.use_ext_spk_att 	= 1,
		.use_shift_ext_spk 	= 1,
		.use_tone_vol 		= 1,
		.use_classd_vout	= 1,
		.use_pcm_gain_pre 	= 1,
		.use_pcm_gain_shift = 1,
		.use_loopgain_rxpl        = 1, 
		.use_ng_threshold	= 1,
		
		.analog_levels 					= { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 },	//160211GF
		.rx_path_att_levels 			= { 0x01A0, 0x0350, 0x0500, 0x0750, 0x0AB0, 0x08CF, 0x0E50,  
											0x1300, 0x1A00, 0x2000, 0x2C00, 0x3B00, 0x4D00, 0x6200, 0x7FFF },  //250511Annie
		//					  { 0x01A0, 0x0230, 0x02EE, 0x0520, 0x0820, 0x0D00, 0x13F0,  
		//									0x1E00, 0x1F00, 0x2000, 0x2E00, 0x2200, 0x3000, 0x4400, 0x6300 },  //220411Annie
		.rx_path_shift_levels 			= { 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 }, 	//250511Annie
		//					  { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2 }, 	//100311Annie
		.sidetone_att_levels 			= { 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300,
											0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300 },
		.call_tone_codec_vol_levels 	= { 0x0250, 0x0350, 0x0500, 0x0650, 0x0800, 0x0b00, 0x1000, 
											0x1600, 0x1b00, 0x2000, 0x2600, 0x2A00, 0x3600, 0x4300, 0x5500 }, 
		.dtmf_tone_codec_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		.pcm_gain_pre 					= { 0x1000, 0x2000, 0x3000, 0x4000, 0x5000, 0x6000, 0x7000, 0x7fff,
											0x5000, 0x6000, 0x7000, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.pcm_gain_shift 				= { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1 },
		.pcm_gain_pre_ringtone 			= { 0x0100, 0x0200, 0x02e0,0x03F0, 0x058A, 0x0800, 0x0BD9, 0x1214,  
											0x171A, 0x1FDB, 0x2AC6, 0x357B, 0x3F0D, 0x47FA, 0x2ADf },
		//					    0x005f, 0x060f, 0x0aff, 0x150f, 0x200f, 0x290f, 0x389f, 0x4c0f,
		//									0x5f0f, 0x700f, 0x7fff, 0x4c0f, 0x5a0f, 0x350f, 0x480f },
		.pcm_gain_shift_ringtone 		= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2  },
		.ext_spk_att_levels 			= { 0x0900, 0x1A00, 0x2B00, 0x3F00, 0x5200 ,0x5500, 0x7200, 0x4E00,  
											0x5600, 0x6000, 0x7E00, 0x5C00, 0x7200 ,0x4D00, 0x6F00 },	//250511Annie
		//					  { 0x090f, 0x1200, 0x2000, 0x2E00, 0x4200 ,0x5500, 0x6500, 0x7FFF,  
		//									0x4E00, 0x6000, 0x7E00, 0x4A00, 0x5C00 ,0x7200, 0x4D00 },	//230511Annie
		.shift_ext_spk_levels 			= { 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3  },	//250511Annie
		//					  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 2  },	//100311Annie
		.ring_playback_vol_levels 		= { 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff ,0x7fff, 
											0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },	
		.ring_tone_vol_levels 			= { 0x1000, 0x1500, 0x1c00, 0x2500, 0x3000, 0x4700, 0x5b00 ,0x7500, 
											0x7FFF, 0x4900, 0x5500, 0x6500, 0x7200, 0x7fff, 0x5100 },				
		.call_tone_classd_vol_levels 	= { 0x0400, 0x0b00, 0x0810, 0x0B20, 0x0D80, 0x1010, 0x1320, 0x1530, 
		 		  							0x1B80, 0x2200, 0x2A60, 0x32F0, 0x3880, 0x3D40, 0x4020 }, 
	.dtmf_tone_classd_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		.classd_vout 					= { 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 },	
		.loopgain_rxpl_levels			= { 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000,
											0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x4000},	//230511Annie
		.ng_threshold_levels			= { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
											0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000},											
	},
/********************************** WIRELESS_HANDSET ************************************************/
/* NOTE: This is a dummy mode that disables any volume control since it is handled from the			*/
/* wireless handset device																			*/
	{
		.use_analog 		= 0,
		.use_rx_path_att 	= 0,
		.use_rx_path_shift	= 0,
		.use_sidetone_att 	= 0,
		.use_ext_spk_att 	= 0,
		.use_shift_ext_spk 	= 0,
		.use_tone_vol 		= 1,
		.use_classd_vout	= 0,
		.use_pcm_gain_pre 	= 1,
		.use_pcm_gain_shift = 1,
		.use_ng_threshold	= 0,


		.call_tone_codec_vol_levels 	= {  0x0800, 0x0b00, 0x1000, 0x1600, 0x1b00, 0x2000, 0x2600,
		//		  							0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00 },
											0x2A00, 0x3600, 0x4300, 0x5500, 0x6500, 0x7000, 0x7A00, 0x7fff }, 
		.dtmf_tone_codec_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		.pcm_gain_pre 					= { 0x1000, 0x2000, 0x3000, 0x4000, 0x5000, 0x6000, 0x7000, 0x7fff,
											0x5000, 0x6000, 0x7000, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.pcm_gain_shift 				= { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1 },
		.pcm_gain_pre_ringtone 			= { 0x005f, 0x060f, 0x0aff, 0x150f, 0x200f, 0x290f, 0x389f, 0x4c0f,
											0x5f0f, 0x700f, 0x7fff, 0x4c0f, 0x5a0f, 0x350f, 0x480f },
		.pcm_gain_shift_ringtone 		= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2  },
	},	
} ;

/****************************************************************************************************/
/**************************** WIDEBAND RECEIVE DIRECTION VOLUME LEVELS ******************************/
/****************************************************************************************************/
/* There are five modes of operation available. The proper path of the SC14452 Gen2DSP and the 		*/
/* analog audio circuities are enabled according to the HANDSET, OPEN_LISTINING, HANDsFREE,	HEADSET	*/
/* and WIRELESS_HANDSET. The number of gain steps is programmable for volume control of each mode.	*/
/* Amplification as well as attenuation units from the volume control section in order to offer	the */
/* best tuning flexibility. The following units are controlled through this process:				*/
/* 1) analog		 --> control the receiver analog gain through CODEC_LSR_REG.LSRATT				*/
/* 2) rx_path_att	 --> control the receiver digital attenuation through DSP cmd 0x0008, 0-0x7fff	*/
/* 3) rx_path_shift	 --> control the receiver digital gain through DSP cmd 0x0020, 0-4				*/
/* 4) sidetone_att	 --> control the sidetone attenuation through DSP cmd 0x000A, 0-0x7fff			*/
/* 5) ext_spk_att	 --> control the speaker digital attenuation through DSP cmd 0x0013, 0-0x7fff	*/
/* 6) shift_ext_spk  --> control the speaker digital gain through DSP cmd 0x0017, 0-4				*/
/* 7) tone_vol		 --> dtmf/key/ring tones loudness control (from tonegens)						*/
/* 8) pcm_gain_pre	 --> playback ring/sounds digital attenuation through DSP cmd 0x002A, 0-0x7fff	*/
/* 9) pcm_gain_shift --> playback ring/sounds digital gain through DSP cmd 0x002A, 0-4				*/
/****************************************************************************************************/

static struct vspk_volumes
profile_vspk_levels_wideband[SC1445x_AE_IFACE_MODE_COUNT_IN_AP] = {

/********************************** HANDSET *********************************************************/
	{
		.use_analog 		= 1,
		.use_rx_path_att 	= 1,
		.use_rx_path_shift	= 1,
		.use_sidetone_att 	= 1,
		.use_ext_spk_att 	= 1,
		.use_shift_ext_spk 	= 1,
		.use_tone_vol 		= 1,
		.use_classd_vout	= 1,
		.use_pcm_gain_pre 	= 1,
		.use_pcm_gain_shift = 1,
		.use_ng_threshold	= 1,
		
		.analog_levels 					= { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 },	//160211GF
		.rx_path_att_levels 			=   { 0x06EE, 0x08CF, 0x0AFF, 0x0EFF, 0x1550, 0x1DB0, 0x2C00, 
											0x3500,0x4E80, 0x6420, 0x7FA0, 0x5090, 0x6560, 0x7FA0, 0x63A0 },
		.rx_path_shift_levels 			= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3 }, 	//230211Annie
		.sidetone_att_levels 			= { 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300,
											0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300  },
		.call_tone_codec_vol_levels 	= { 0x0250, 0x0350, 0x0500, 0x0650, 0x0800, 0x0b00, 0x1000, 
											0x1600, 0x1b00, 0x2000, 0x2600, 0x2A00, 0x3600, 0x4300, 0x5500,  }, 
		.dtmf_tone_codec_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		.pcm_gain_pre 					= { 0x1000, 0x2000, 0x3000, 0x4000, 0x5000, 0x6000, 0x7000, 0x7fff,
											0x5000, 0x6000, 0x7000, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.pcm_gain_shift 				= { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1 },
		.pcm_gain_pre_ringtone 			= { 0x005f, 0x060f, 0x0aff, 0x150f, 0x200f, 0x290f, 0x389f, 0x4c0f,
											0x5f0f, 0x700f, 0x7fff, 0x4c0f, 0x5a0f, 0x350f, 0x480f },
		.pcm_gain_shift_ringtone 		= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2  },
		.ext_spk_att_levels 			= { 0x04d0, 0x06d0, 0x0999, 0x0d80, 0x1300 ,0x1b00, 0x2600, 0x3600,  
											0x4c00, 0x6800, 0x4500, 0x5700, 0x5400 ,0x7600, 0x7fff },		
		.shift_ext_spk_levels 			= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 2  },						
		.ring_playback_vol_levels 		= { 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff ,0x7fff, 
											0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },	
		.ring_tone_vol_levels 			= { 0x1000, 0x1500, 0x1c00, 0x2500, 0x3000, 0x4700, 0x5b00 ,0x7500, 
											0x7FFF, 0x4900, 0x5500, 0x6500, 0x7200, 0x7fff, 0x5100 },				
		.call_tone_classd_vol_levels 	= { 0x0400, 0x0b00, 0x0810, 0x0B20, 0x0D80, 0x1010, 0x1320, 0x1530, 
		 		  							0x1B80, 0x2200, 0x2A60, 0x32F0, 0x3880, 0x3D40, 0x4020 },
		//                                  0x0800, 0x0b00, 0x1000, 0x1600, 0x1b00, 0x2000, 0x2600, 0x2A00, 
		//		  							0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00 },
		//									0x3600, 0x4300, 0x5500, 0x6500, 0x7000, 0x7A00, 0x7fff}, 
		.dtmf_tone_classd_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		//	                            0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000,
		//									0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000},
		.classd_vout 					= { 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 },	
		.loopgain_rxpl_levels			= { 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000,
											0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x4000},	
		.ng_threshold_levels			= { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
											0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000},											

	},
/********************************** OPEN_LISTINING **************************************************/
	{
		.use_analog 		= 1,
		.use_rx_path_att 	= 1,
		.use_rx_path_shift	= 1,
		.use_sidetone_att 	= 1,
		.use_ext_spk_att 	= 1,
		.use_shift_ext_spk 	= 1,
		.use_tone_vol 		= 1,
		.use_classd_vout	= 1,
		.use_pcm_gain_pre 	= 1,
		.use_pcm_gain_shift = 1,
		.use_ng_threshold	= 1,
		
		.analog_levels 					= { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 },	//160211GF
		.rx_path_att_levels 			=   { 0x06EE, 0x08CF, 0x0AFF, 0x0EFF, 0x1550, 0x1DB0, 0x2C00, 
											0x3500,0x4E80, 0x6420, 0x7FA0, 0x5090, 0x6560, 0x7FA0, 0x63A0 },
		.rx_path_shift_levels 			= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3 }, 	//230211Annie
		.sidetone_att_levels 			= { 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300,
											0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300  },
		.call_tone_codec_vol_levels 	= { 0x0250, 0x0350, 0x0500, 0x0650, 0x0800, 0x0b00, 0x1000, 
											0x1600, 0x1b00, 0x2000, 0x2600, 0x2A00, 0x3600, 0x4300, 0x5500,  }, 
		.dtmf_tone_codec_vol_levels 	={ 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		.pcm_gain_pre 					= { 0x1000, 0x2000, 0x3000, 0x4000, 0x5000, 0x6000, 0x7000, 0x7fff,
											0x5000, 0x6000, 0x7000, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.pcm_gain_shift 				= { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1 },
		.pcm_gain_pre_ringtone 			= { 0x005f, 0x060f, 0x0aff, 0x150f, 0x200f, 0x290f, 0x389f, 0x4c0f,
											0x5f0f, 0x700f, 0x7fff, 0x4c0f, 0x5a0f, 0x350f, 0x480f },
		.pcm_gain_shift_ringtone 		= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2  },
		.ext_spk_att_levels 			= { 0x04d0, 0x06d0, 0x0999, 0x0d80, 0x1300 ,0x1b00, 0x2600, 0x3600,  
											0x4c00, 0x6800, 0x4500, 0x5700, 0x5400 ,0x7600, 0x7fff },		
		.shift_ext_spk_levels 			= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 2  },						
		.ring_playback_vol_levels 		= { 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff ,0x7fff, 
											0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },	
		.ring_tone_vol_levels 			= { 0x1000, 0x1500, 0x1c00, 0x2500, 0x3000, 0x4700, 0x5b00 ,0x7500, 
											0x7FFF, 0x4900, 0x5500, 0x6500, 0x7200, 0x7fff, 0x5100 },				
		.call_tone_classd_vol_levels 	= { 0x0400, 0x0b00, 0x0810, 0x0B20, 0x0D80, 0x1010, 0x1320, 0x1530, 
		 		  							0x1B80, 0x2200, 0x2A60, 0x32F0, 0x3880, 0x3D40, 0x4020 },
		//                                  0x0800, 0x0b00, 0x1000, 0x1600, 0x1b00, 0x2000, 0x2600, 0x2A00, 
		//		  							0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00 },
		//									0x3600, 0x4300, 0x5500, 0x6500, 0x7000, 0x7A00, 0x7fff}, 
		.dtmf_tone_classd_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		//	                            0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000,
		//									0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000},
		.classd_vout 					= { 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 },	
		.loopgain_rxpl_levels			= { 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000,
											0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x4000},					
		.ng_threshold_levels			= { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
											0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000},
	},
/********************************** HANDsFREE *****************************************************/
	{
		.use_analog 		= 1,
		.use_rx_path_att 	= 1,
		.use_rx_path_shift	= 1,
		.use_sidetone_att 	= 1,
		.use_ext_spk_att 	= 1,
		.use_shift_ext_spk 	= 1,
		.use_tone_vol 		= 1,
		.use_classd_vout	= 1,
		.use_pcm_gain_pre 	= 1,
		.use_pcm_gain_shift = 1,
		.use_ng_threshold	= 1,
		
		.analog_levels 					= { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 },	//160211GF
		.rx_path_att_levels 			=   { 0x06EE, 0x08CF, 0x0AFF, 0x0EFF, 0x1550, 0x1DB0, 0x2C00, 
											0x3500,0x4E80, 0x6420, 0x7FA0, 0x5090, 0x6560, 0x7FA0, 0x63A0 },
		.rx_path_shift_levels 			= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3 }, 	//230211Annie
		.sidetone_att_levels 			= { 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300,
											0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300  },
		.call_tone_codec_vol_levels 	= { 0x0250, 0x0350, 0x0500, 0x0650, 0x0800, 0x0b00, 0x1000, 
											0x1600, 0x1b00, 0x2000, 0x2600, 0x2A00, 0x3600, 0x4300, 0x5500,  }, 
		.dtmf_tone_codec_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		.pcm_gain_pre 					= { 0x1000, 0x2000, 0x3000, 0x4000, 0x5000, 0x6000, 0x7000, 0x7fff,
											0x5000, 0x6000, 0x7000, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.pcm_gain_shift 				= { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1 },
		.pcm_gain_pre_ringtone 			= { 0x005f, 0x060f, 0x0aff, 0x150f, 0x200f, 0x290f, 0x389f, 0x4c0f,
											0x5f0f, 0x700f, 0x7fff, 0x4c0f, 0x5a0f, 0x350f, 0x480f },
		.pcm_gain_shift_ringtone 		= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2  },
		.ext_spk_att_levels 			= { 0x04d0, 0x06d0, 0x0999, 0x0d80, 0x1300 ,0x1b00, 0x2600, 0x3600,  
											0x4c00, 0x6800, 0x4B00, 0x5700, 0x5400 ,0x7600, 0x7fff },		
		.shift_ext_spk_levels 			= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 2  },						
		.ring_playback_vol_levels 		= { 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff ,0x7fff, 
											0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },	
		.ring_tone_vol_levels 			= { 0x1000, 0x1500, 0x1c00, 0x2500, 0x3000, 0x4700, 0x5b00 ,0x7500, 
											0x7FFF, 0x4900, 0x5500, 0x6500, 0x7200, 0x7fff, 0x5100 },				
		.call_tone_classd_vol_levels 	= { 0x0400, 0x0b00, 0x0810, 0x0B20, 0x0D80, 0x1010, 0x1320, 0x1530, 
		 		  							0x1B80, 0x2200, 0x2A60, 0x32F0, 0x3880, 0x3D40, 0x4020 },
		//                                  0x0800, 0x0b00, 0x1000, 0x1600, 0x1b00, 0x2000, 0x2600, 0x2A00, 
		//		  							0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00 },
		//									0x3600, 0x4300, 0x5500, 0x6500, 0x7000, 0x7A00, 0x7fff}, 
		.dtmf_tone_classd_vol_levels 	={ 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		//	                            0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000,
		//									0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000},
		.classd_vout 					= { 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 },	
		.loopgain_rxpl_levels			= { 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000,
											0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x4000},
		.ng_threshold_levels			= { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
											0x0000, 0x0000, 0x0000, 0x0000, 0x0029, 0x0029, 0x0029},											
	},
/********************************** HEADSET *******************************************************/
	{
		.use_analog 		= 1,
		.use_rx_path_att 	= 1,
		.use_rx_path_shift	= 1,
		.use_sidetone_att 	= 1,
		.use_ext_spk_att 	= 1,
		.use_shift_ext_spk 	= 1,
		.use_tone_vol 		= 1,
		.use_classd_vout	= 1,
		.use_pcm_gain_pre 	= 1,
		.use_pcm_gain_shift = 1,
		.use_ng_threshold	= 1,
		
		.analog_levels 					= { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 },	//160211GF
		.rx_path_att_levels 			=   { 0x06EE, 0x08CF, 0x0AFF, 0x0EFF, 0x1550, 0x1DB0, 0x2C00, 
											0x3500,0x4E80, 0x6420, 0x7FA0, 0x5090, 0x6560, 0x7FA0, 0x63A0 },
		.rx_path_shift_levels 			= { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3 }, 	//230211Annie
		.sidetone_att_levels 			= { 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300,
											0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300, 0x1300  },
		.call_tone_codec_vol_levels 	= { 0x0250, 0x0350, 0x0500, 0x0650, 0x0800, 0x0b00, 0x1000, 
											0x1600, 0x1b00, 0x2000, 0x2600, 0x2A00, 0x3600, 0x4300, 0x5500,  }, 
		.dtmf_tone_codec_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		.pcm_gain_pre 					= { 0x1000, 0x2000, 0x3000, 0x4000, 0x5000, 0x6000, 0x7000, 0x7fff,
											0x5000, 0x6000, 0x7000, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.pcm_gain_shift 				= { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1 },
		.pcm_gain_pre_ringtone 			= { 0x005f, 0x060f, 0x0aff, 0x150f, 0x200f, 0x290f, 0x389f, 0x4c0f,
											0x5f0f, 0x700f, 0x7fff, 0x4c0f, 0x5a0f, 0x350f, 0x480f },
		.pcm_gain_shift_ringtone 		= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2  },
		.ext_spk_att_levels 			= { 0x04d0, 0x06d0, 0x0999, 0x0d80, 0x1300 ,0x1b00, 0x2600, 0x3600,  
											0x4c00, 0x6800, 0x4500, 0x5700, 0x5400 ,0x7600, 0x7fff },		
		.shift_ext_spk_levels 			= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 2  },						
		.ring_playback_vol_levels 		= { 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff ,0x7fff, 
											0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },	
		.ring_tone_vol_levels 			= { 0x1000, 0x1500, 0x1c00, 0x2500, 0x3000, 0x4700, 0x5b00 ,0x7500, 
											0x7FFF, 0x4900, 0x5500, 0x6500, 0x7200, 0x7fff, 0x5100 },				
		.call_tone_classd_vol_levels 	= { 0x0400, 0x0b00, 0x0810, 0x0B20, 0x0D80, 0x1010, 0x1320, 0x1530, 
		 		  							0x1B80, 0x2200, 0x2A60, 0x32F0, 0x3880, 0x3D40, 0x4020 },
		//                                  0x0800, 0x0b00, 0x1000, 0x1600, 0x1b00, 0x2000, 0x2600, 0x2A00, 
		//		  							0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00 },
		//									0x3600, 0x4300, 0x5500, 0x6500, 0x7000, 0x7A00, 0x7fff}, 
		.dtmf_tone_classd_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		//	                            0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000,
		//									0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000, 0x3000},
		.classd_vout 					= { 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 },	
		.loopgain_rxpl_levels			= { 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000,
											0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x5000, 0x4000},	
		.ng_threshold_levels			= { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
											0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000},											
	},
/********************************** WIRELESS_HANDSET ************************************************/
/* NOTE: This is a dummy mode that disables any volume control since it is handled from the			*/
/* wireless handset device																			*/
	{
		.use_analog 		= 0,
		.use_rx_path_att 	= 0,
		.use_rx_path_shift	= 0,
		.use_sidetone_att 	= 0,
		.use_ext_spk_att 	= 0,
		.use_shift_ext_spk 	= 0,
		.use_tone_vol 		= 1,
		.use_classd_vout	= 0,
		.use_pcm_gain_pre 	= 1,
		.use_pcm_gain_shift = 1,
		.use_ng_threshold	= 0,


		.call_tone_codec_vol_levels 	= {  0x0800, 0x0b00, 0x1000, 0x1600, 0x1b00, 0x2000, 0x2600,
		//		  							0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00, 0x2A00 },
											0x2A00, 0x3600, 0x4300, 0x5500, 0x6500, 0x7000, 0x7A00, 0x7fff }, 
		.dtmf_tone_codec_vol_levels 	= { 0x0600, 0x0800, 0x0b00, 0x0F00, 0x1300, 0x1900, 0x2000, 0x2600, 
		                                                                        0x3000, 0x3900, 0x4300, 0x5090, 0x5300, 0x6500, 0x7000 },
		.pcm_gain_pre 					= { 0x1000, 0x2000, 0x3000, 0x4000, 0x5000, 0x6000, 0x7000, 0x7fff,
											0x5000, 0x6000, 0x7000, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.pcm_gain_shift 				= { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1 },
		.pcm_gain_pre_ringtone 			= { 0x005f, 0x060f, 0x0aff, 0x150f, 0x200f, 0x290f, 0x389f, 0x4c0f,
											0x5f0f, 0x700f, 0x7fff, 0x4c0f, 0x5a0f, 0x350f, 0x480f },
		.pcm_gain_shift_ringtone 		= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2  },
	},	
} ;

/****************************************************************************************************/
/*************************** NARROWBAND TRANSMIT DIRECTION VOLUME LEVELS *****************************/
/****************************************************************************************************/
/* There are five modes of operation available. The proper path of the SC14452 Gen2DSP and the 		*/
/* analog audio circuities are enabled according to the HANDSET, OPEN_LISTINING, HANDsFREE,	HEADSET	*/
/* and WIRELESS_HANDSET. The number of gain steps is programmable for volume control of each mode.	*/
/* Amplification as well as attenuation units from the volume control section in order to offer	the */
/* 1) analog		 --> control the transmit analog gain through CODEC_MIC_REG.MIC_GAIN			*/
/* 2) shift_paec_out --> control the transmit digital amplification through DSP cmd 0x001A, 0-4		*/
/* 3) paec_tx_att	 --> control the transmit digital attenuation through DSP cmd 0x001B, 0-0x7fff	*/
/* 4) attlimit		 --> echo suppression from PAEC													*/
/* 5) supmin		 --> echo suppression from HFREE												*/
/****************************************************************************************************/

static struct vmic_gains
profile_vmic_levels_narrowband[SC1445x_AE_IFACE_MODE_COUNT_IN_AP] = {

/********************************** HANDSET *********************************************************/
	{
		.use_analog =			1,
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
	#endif
		.use_attlimit		=	1,
		.use_supmin			=	1,
		.use_noiseattlimit	=	1,

		.analog_levels =		{ 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6 },	//130111GF voice quality
								//{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },		//gflamis190609  . 
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  },	//250511Annie
		.paec_tx_att_levels = { 0x6000, 0x6000, 0x6000, 0x6000, 0x6000, 0x6000, 0x6000,   	
									0x6000, 0x6000, 0x6000, 0x6000, 0x6000, 0x6000, 0x6000, 0x6000  }, //250511Annie	
	#endif
		.attlimit				= { 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc,
									0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc },	//160211GF
								//	{ 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1700, 0x1600,
								//	0x1400, 0x1300, 0x1200, 0x1100, 0x500 },
		.supmin					= { 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.noiseattlimit			= { 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 
									0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900 },							
	},
/********************************** OPEN_LISTINING **************************************************/
	{
		.use_analog =			1,
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
	#endif
		.use_attlimit		=	1,
		.use_supmin			=	1,
		.use_noiseattlimit	=	1,

		.analog_levels =		{ 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6 },	//130111GF voice quality
								//{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },		//gflamis190609  . 
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  },	//250511Annie	
		.paec_tx_att_levels =	{ 0x6000, 0x6000, 0x6000, 0x6000, 0x6000, 0x6000, 0x6000,   	
									0x6000, 0x6000, 0x6000, 0x6000, 0x6000, 0x6000, 0x6000, 0x6000  }, //250511Annie		
	#endif
		.attlimit				= { 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc,
									0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc },	//160211GF
								//	{ 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1700, 0x1600,
								//	0x1400, 0x1300, 0x1200, 0x1100, 0x0950 },
		.supmin					= { 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.noiseattlimit			= { 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 
									0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900 },								
	},
/********************************** HANDsFREE *******************************************************/
	{
		.use_analog =			1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
#endif
		.use_attlimit		=	1,
		.use_supmin			=	1,
		.use_noiseattlimit	=	1,

		.analog_levels =		{ 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8 }, //080311GF		
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels = { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 },	 //190311GF 
		.paec_tx_att_levels =	{ 0x6000, 0x6000, 0x6000, 0x6000, 0x6000, 0x6000, 0x6000 ,0x6000, 
									0x6000, 0x6000, 0x6000, 0x6000, 0x6000, 0x6000, 0x6000 },	 //230511GF
		//			{ 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff ,0x7fff, 
		//							0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7000, 0x6666, 0x5aaa },	 //190311GF
		.attlimit				= { 0x0048, 0x0048, 0x0048, 0x0048, 0x0048, 0x0048, 0x0048,
									0x0048, 0x0048, 0x0048, 0x0048, 0x0048, 0x0048, 0x0048, 0x0048 },	 //080311GF
		.supmin					= { 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 
									0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000 },	 //060411GF
								//	{ 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 
								//	0x2000, 0x2000, 0x2000, 0x2000, 0x0ccc, 0x0ccc, 0x0ccc },	 //190311GF
								//{ 0x4333, 0x4333, 0x4333, 0x4333, 0x4333, 0x4333, 0x4333, 0x4333, 
								//	0x4333, 0x4333, 0x4333, 0x4333, 0x4333, 0x4333, 0x4333 },	 //080311GF
		.noiseattlimit			= { 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 
									0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000 },							
#endif
	},
/********************************** HEADSET *******************************************************/
/* NOTE: It has not been tuned */
	{
		.use_analog =			1,
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
	#endif
		.use_attlimit		=	1,
		.use_supmin			=	1,
		.use_noiseattlimit	=	1,

		.analog_levels =		{ 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6 },	//130111GF voice quality
								//{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },		//gflamis190609  . 
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels ={ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },			//gflamis190609  .	
		.paec_tx_att_levels =	{ 0x5500, 0x5500, 0x5500, 0x5500, 0x5500, 0x5500, 0x5500,   	
									0x5500, 0x5500, 0x5500, 0x5500, 0x5500, 0x5500, 0x5500, 0x5500 }, //110311Annie
		//			{ 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff,0x7fff,
		//						0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff,0x7fff,0x7fff,0x7fff,0x7fff },	//gflamis190609  .	
	#endif
		.attlimit				= { 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc,
									0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc },	//160211GF
								//	{ 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1700, 0x1600,
								//	0x1400, 0x1300, 0x1200, 0x1100, 0x1000 },
		.supmin					= { 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.noiseattlimit			= { 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 
									0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900 },							
	},
/********************************** WIRELESS_HANDSET ************************************************/
/* NOTE: This is a dummy mode that disables any volume control since it is handled from the			*/
/* wireless handset device																			*/
	{
		.use_analog =			0,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	0,
		.use_paec_tx_att =		0,
#endif

		.analog_levels =		{ },
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels ={ },
		.paec_tx_att_levels =	{ },
#endif
	}	
} ;

/****************************************************************************************************/
/**************************** WIDEBAND TRANSMIT DIRECTION VOLUME LEVELS *****************************/
/****************************************************************************************************/
/* There are five modes of operation available. The proper path of the SC14452 Gen2DSP and the 		*/
/* analog audio circuities are enabled according to the HANDSET, OPEN_LISTINING, HANDsFREE,	HEADSET	*/
/* and WIRELESS_HANDSET. The number of gain steps is programmable for volume control of each mode.	*/
/* Amplification as well as attenuation units from the volume control section in order to offer	the */
/* 1) analog		 --> control the transmit analog gain through CODEC_MIC_REG.MIC_GAIN			*/
/* 2) shift_paec_out --> control the transmit digital amplification through DSP cmd 0x001A, 0-4		*/
/* 3) paec_tx_att	 --> control the transmit digital attenuation through DSP cmd 0x001B, 0-0x7fff	*/
/* 4) attlimit		 --> echo suppression from PAEC													*/
/* 5) supmin		 --> echo suppression from HFREE												*/
/****************************************************************************************************/

static struct vmic_gains
profile_vmic_levels_wideband[SC1445x_AE_IFACE_MODE_COUNT_IN_AP] = {

/********************************** HANDSET *********************************************************/
	{
		.use_analog =			1,
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
	#endif
		.use_attlimit		=	1,
		.use_supmin			=	1,
		.use_noiseattlimit	=	1,

		.analog_levels =		{ 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6 },	//130111GF voice quality
								//{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },		//gflamis190609  . 
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  },	//160211GF
							//	{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },			//gflamis190609  .	
		.paec_tx_att_levels =	{ 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff,	//160211GF
									0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
							//	{ 0x4000, 0x4000, 0x4000, 0x4000, 0x4000, 0x4000,
							//	0x4000, 0x4000, 0x4000, 0x4000, 0x4000, 0x4000, 0x4000, 0x4000, 0x4000 },	//gflamis190609  .	
	#endif
		.attlimit				= { 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc,
									0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc },	//160211GF
								//	{ 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1700, 0x1600,
								//	0x1400, 0x1300, 0x1200, 0x1100, 0x500 },
		.supmin					= { 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.noiseattlimit			= { 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 
									0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900 },							
	},
/********************************** OPEN_LISTINING **************************************************/
	{
		.use_analog =			1,
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
	#endif
		.use_attlimit		=	1,
		.use_supmin			=	1,
		.use_noiseattlimit	=	1,

		.analog_levels =		{ 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6 },	//130111GF voice quality
								//{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },		//gflamis190609  . 
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  },	//160211GF
							//	{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },			//gflamis190609  .	
		.paec_tx_att_levels =	{ 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff,	//160211GF
									0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
							//	{ 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff,0x7fff,
							//	0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff,0x7fff,0x7fff,0x7fff,0x7fff },	//gflamis190609  .	
	#endif
		.attlimit				= { 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc,
									0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc },	//160211GF
								//	{ 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1700, 0x1600,
								//	0x1400, 0x1300, 0x1200, 0x1100, 0x0950 },
		.supmin					= { 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.noiseattlimit			= { 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 
									0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900 },							
	},
/********************************** HANDsFREE *******************************************************/
	{
		.use_analog =			1,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
#endif
		.use_attlimit		=	1,
		.use_supmin			=	1,
		.use_noiseattlimit	=	1,

		.analog_levels =		{ 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8 }, //080311GF		
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels = { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 },	 //190311GF 
								//{ 	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },	 //080311GF
		.paec_tx_att_levels =	 { 	0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff ,0x7fff, 
									0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x5aaa, 0x5aaa, 0x5aaa },	 //190311GF
		
		.attlimit				= { 0x0048, 0x0048, 0x0048, 0x0048, 0x0048, 0x0048, 0x0048,
									0x0048, 0x0048, 0x0048, 0x0048, 0x0048, 0x0048, 0x0048, 0x0048 },	 //080311GF
		.supmin					= { 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 
									0x2000, 0x2000, 0x2000, 0x2000, 0x0ccc, 0x0ccc, 0x0ccc },	 //190311GF
								//{ 0x4333, 0x4333, 0x4333, 0x4333, 0x4333, 0x4333, 0x4333, 0x4333, 
								//	0x4333, 0x4333, 0x4333, 0x4333, 0x4333, 0x4333, 0x4333 },	 //080311GF
		.noiseattlimit			= { 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 
									0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000, 0x2000 },						
#endif
	},
/********************************** HEADSET *******************************************************/
/* NOTE: It has not been tuned */
	{
		.use_analog =			1,
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	1,
		.use_paec_tx_att =		1,
	#endif
		.use_attlimit		=	1,
		.use_supmin			=	1,
		.use_noiseattlimit	=	1,

		.analog_levels =		{ 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6 },	//130111GF voice quality
								//{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },		//gflamis190609  . 
	#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels ={ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },			//gflamis190609  .	
		.paec_tx_att_levels =	{ 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff,0x7fff,
								0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff,0x7fff,0x7fff,0x7fff,0x7fff },	//gflamis190609  .	
	#endif
		.attlimit				= { 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc,
									0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc, 0x0ccc },	//160211GF
								//	{ 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1800, 0x1700, 0x1600,
								//	0x1400, 0x1300, 0x1200, 0x1100, 0x1000 },
		.supmin					= { 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff,
									0x7fff, 0x7fff, 0x7fff, 0x7fff, 0x7fff },
		.noiseattlimit			= { 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 
									0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900, 0x2900 },							
	},
/********************************** WIRELESS_HANDSET ************************************************/
/* NOTE: This is a dummy mode that disables any volume control since it is handled from the			*/
/* wireless handset device																			*/
	{
		.use_analog =			0,
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.use_shift_paec_out =	0,
		.use_paec_tx_att =		0,
#endif

		.analog_levels =		{ },
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		.shift_paec_out_levels ={ },
		.paec_tx_att_levels =	{ },
#endif
	}	
} ;

/****************************************************************************************************/
/****************************** NARROWBAND FREQUENCY RESPONSE FILTERS *******************************/
/****************************************************************************************************/
/* The available filters are responsible of the frequency mask shape for each direction.			*/
/* They consist of three cascaded IIR filter that construct a bandpass filter. Their coefficients	*/
/* are separated in three categories:	i) "handset" for the handset/headset acoustic components 	*/
/*								  	   ii) "external" for the handsfree acoustic components			*/
/*								  	  iii) "headset" for the corded headset acoustic components		*/
/****************************************************************************************************/

/****************************** TRANSMIT DIRECTION FILTER COEFFICIENTS *******************************/
static const unsigned short profile_narrowband_filters_TX1[3][FILTER_SIZE] = {
	/* handset */
	{ 0x4000, 0x83BA, 0x3C64, 0x6E59, 0xCEC5, 0x3746, 0x5000 },     // 110311Annie
	//{ 0x4000, 0x0C87, 0xEFAE, 0xE92A, 0x0047, 0x4b16, 0x4000 },		//190211GF
	//{ 0x4000, 0x83BA, 0x3C64, 0x6E59, 0xCEC5, 0x3746, 0x5000 },		//gflamis230509  
	/* external */
	{ 0x2D37, 0x16B9, 0x09CB, 0xFD46, 0xF500, 0x7FFF, 0x4000 },	//080311GF
	/* headset */
	{ 0x4000, 0x83BA, 0x3C64, 0x6E59, 0xCEC5, 0x3746, 0x5000 }
} ;
static const unsigned short profile_narrowband_filters_TX2[3][FILTER_SIZE] = {
	/* handset */
	{ 0x4000, 0x7E12, 0x3E64, 0x90A3, 0xCDCE, 0x3746, 0x5000 },	// 110311Annie
	//{ 0x4000, 0xC52E, 0x1C30, 0x2722, 0xF0BE, 0x7333, 0x4000 },		//190211GF
	//{ 0x4000, 0x7E12, 0x3E64, 0x90A3, 0xCDCE, 0x3746, 0x5000 },		//gflamis230509  
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		//230311GF 
	//{ 0x1EE8, 0xF6c5, 0x1C19, 0x093b, 0xE4ff, 0x7FFF, 0x8000 },	//080311GF
	/* headset */
	{ 0x4000, 0x7E12, 0x3E64, 0x90A3, 0xCDCE, 0x3746, 0x5000 }
} ;
static const unsigned short profile_narrowband_filters_TX3[3][FILTER_SIZE] = { //
	/* handset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },	// 110311Annie
	//{ 0x4000, 0x2194, 0x2085, 0xE07F, 0xE1B4, 0x7FFF, 0x5000 },		//190211GF
	//{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x1000 },	//190311GF 	, 0x2000 },	//080311GF
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
};
/****************************** RECEIVE DIRECTION FILTER COEFFICIENTS *******************************/
static const unsigned short profile_narrowband_filters_RX1[3][FILTER_SIZE] = {
	/* handset */
	//{ 0x4000, 0x83BA, 0x3C64, 0x6E59, 0xCEC5, 0x3746, 0x5000 },		//gflamis230509  
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },             //Allen
	/* external */
	{ 0x4000, 0xF44D, 0xE40E, 0xFF69, 0x1543, 0x68F6, 0x0000 },		//230311GF 
	//{ 0x1EE8, 0xF6c5, 0x1C19, 0x093b, 0xE4ff, 0x5999, 0x8000 },	//080311GF
	/* headset */
	{ 0x4000, 0x83BA, 0x3C64, 0x6E59, 0xCEC5, 0x3746, 0x5000 }
} ;
static const unsigned short profile_narrowband_filters_RX2[3][FILTER_SIZE] = {
	/* handset */
	//{ 0x4000, 0x7E12, 0x3E64, 0x90A3, 0xCDCE, 0x3746, 0x5000 },		//gflamis230509 
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }, 
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		//230311GF 
	//{ 0x1EE8, 0xF6c5, 0x1C19, 0x093b, 0xE4ff, 0x7FFF, 0x8000 },	//080311GF
	/* headset */
	{ 0x4000, 0x7E12, 0x3E64, 0x90A3, 0xCDCE, 0x3746, 0x5000 }
} ;
static const unsigned short profile_narrowband_filters_RX3[3][FILTER_SIZE] = { //
	/* handset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x2000 },		//230311GF 
	//{ 0x1EE8, 0xF6c5, 0x1C19, 0x093b, 0xE4ff, 0x7FFF, 0x9000 },	//080311GF
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
};

/****************************************************************************************************/
/******************************* WIDEBAND FREQUENCY RESPONSE FILTERS ********************************/
/****************************************************************************************************/
/* The available filters are responsible of the frequency mask shape for each direction.			*/
/* They consist of three cascaded IIR filter that construct a bandpass filter. Their coefficients	*/
/* are separated in three categories:	i) "handset" for the handset/headset acoustic components 	*/
/*								  	   ii) "external" for the handsfree acoustic components			*/
/*								  	  iii) "headset" for the corded headset acoustic components		*/
/****************************************************************************************************/

/****************************** TRANSMIT DIRECTION FILTER COEFFICIENTS *******************************/
static const unsigned short profile_wideband_filters_TX1[3][FILTER_SIZE] = {
	/* handset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
} ;
static const unsigned short profile_wideband_filters_TX2[3][FILTER_SIZE] = {
	/* handset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
} ;
static const unsigned short profile_wideband_filters_TX3[3][FILTER_SIZE] = { //
	/* handset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
};
/****************************** RECEIVE DIRECTION FILTER COEFFICIENTS *******************************/
static const unsigned short profile_wideband_filters_RX1[3][FILTER_SIZE] = {
	/* handset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
} ;
static const unsigned short profile_wideband_filters_RX2[3][FILTER_SIZE] = {
	/* handset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
} ;
static const unsigned short profile_wideband_filters_RX3[3][FILTER_SIZE] = { //
	/* handset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* external */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 },		
	/* headset */
	{ 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000 }
};

/****************************************************************************************************/
/************************************ Acoustic Echo Canceller ***************************************/
/****************************************************************************************************/
/* The following values are the narrowband / wideband settings of the same parameters as well as	*/
/* the parameters that can be adjusted toward the fine tuning of the PAEC and the SUPPRESSOR		*/
/* algorithms.																						*/
/****************************************************************************************************/

#if defined( CONFIG_SND_SC1445x_USE_PAEC )
/* PAEC parameters setup */
#ifdef PAECv4
	
	static const unsigned short profile_paec_data_loc[] = { 
	23/*npart*/,		30/*init*/,		31/*init2*/,	33/*dtdcount_init*/,	35/*minframe*/,	38/*cctc*/,		44/*dtdsmooth*/,	48/*noiseest1*/,
	49/*noiseest2*/,	53/*fevadsm1*/,	54/*fevadsm2*/,	55/*nevadsm1*/,			56/*nevadsm2*/,	60/*reverbtc*/,	61/*smoothlow*/,	62/*smoothtop*/
} ;
	static const unsigned short profile_paec_data_narrowband[] = { 
	0x000C,		0xFF83,		0xFE0C,		0x0007,		0x007D,		0x00D9,		0x0368,		0x000B,
	0x020C,		0x7FFF,		0x051A,		0x7FFF,		0x051A,		0x1000,		0x0CCC,		0x6666
	} ;
	static const unsigned short profile_paec_data_wideband[] = { 
	0x000F,		0xFF06,		0xFC18,		0x000F,		0x00FA,		0x006C,		0x01B4,		0x0005, 
	0x0106,		0x6666,		0x0146,		0x0A3D,		0x0146,		0x0800,		0x0666,		0x3333
	} ;

#else
static const unsigned short profile_paec_data_loc[] = {
	22, 29, 34, 36, 40, 44, 45, 
	48, 49, 52, 53, 54, 55, 56
} ;
static const unsigned short profile_paec_data_narrowband[] = {
	0x000c, 0xffb0, 0x0106, 0x0040, 0x036A, 0x000d, 0x020c, 
	0x6666, 0x0ea1, 0x1000, 0x3000, 0x2000, 0x0666, 0x7000				
} ;
static const unsigned short profile_paec_data_wideband[] = {		
	0x000f, 0xff60, 0x0083, 0x0040, 0x01b5, 0x0007, 0x0106, 
	0x3333, 0x0750, 0x1000, 0x6000, 0x3000, 0x0333, 0x6000
} ;

#endif

#  if defined( HAVE_PAEC_SUPPRESSOR )
/* Suppressor Setup*/
static unsigned short* const profile_supp_params_addr[] = {
/*	(void*)0x10718, (void*)0x1071a,		//plevdet_data_spk fall / rise time
	(void*)0x11786, (void*)0x11788,		//plevdet_data_spk1 fall / rise time
	(void*)0x107f2, (void*)0x107f4,		//plevdet_data_paec_out fall / rise time 
	(void*)0x11a7e, (void*)0x11a82,		//pnlev_tx_data fall / rise time
	(void*)0x109B4,						//centclip_att
	(void*)0x109b8,						//c2_thresh
	(void*)0x109ba,						//cntrini 
	(void*)0x109bc,						//cntrmin 
	(void*)0x109c4, (void*)0x109be,		//rbetaoff / off 
	(void*)0x109c6, (void*)0x109c0,		//rbetaon / on	*/
#if 0  /* don't use Tx Peak Limiter for now */
/* TxPeakLimit */		
	(void*)0x1056e,						//HagcTx toff_scale
	(void*)0x10574,						//HagcTx toff
	(void*)0x10576,						//HagcTx ton				
	(void*)0x10586, (void*)0x10588,		//PLevDet2TxComp1 fall / rise time
	(void*)0x1058E, (void*)0x10590,		//PLevDet2TxComp2 fall / rise time
#endif
/* RxPeakLimit */	
	(void*)0x105ba,						//HagcRx toff_scale
	(void*)0x105C0,						//HagcRx toff
	(void*)0x105C2,						//HagcRx ton
	(void*)0x10624, (void*)0x10626,		//PLevDet2RxComp1 fall / rise time
	(void*)0x1062C, (void*)0x1062E,		//PLevDet2RxComp2 fall / rise time
/* Hfree */
	(void*)0x11fca, (void*)0x11fcc,		//hfree_pl_rxin fall / rise	
	(void*)0x11fd4, (void*)0x11fd8,		//hfree_pn_rxin fall / rise	
	(void*)0x11fe0, (void*)0x11fe2,		//hfree_pa_txin fall / rise	
	(void*)0x11fea, (void*)0x11fee,		//hfree_pn_txin fall / rise	
	(void*)0x12B72,						//hfree idlecntini
	(void*)0x12B74,						//hfree idlecntini2
//	(void*)0x12B82,						//rx ratio
	(void*)0x12B84,						//hfree rxtxcntini
	(void*)0x12B88,						//hfree suptidleoff
	(void*)0x12B8a,						//hfree suptidleon
	(void*)0x12B8c,						//hfree suptoff
	(void*)0x12B8e,						//hfree supton
	(void*)0x11ff6, (void*)0x11ff8,		//hfree_pl_txin fall / rise	
	(void*)0x11f2c, (void*)0x11f2e,		//hfree_pa_rxin fall / rise
	/* CNG */	
	(void*)0x11fc4,						//comfnoiselevel
	/* PAEC mode */
//	(void*)0x10750,
	/* RxPL emphasized */
	(void*)0x11b1a, (void*)0x11b1c, (void*)0x11b1e, (void*)0x11b20, (void*)0x11b22, (void*)0x11b24, (void*)0x11b26,		//RxPL flt1
	(void*)0x11b34, (void*)0x11b36, (void*)0x11b38, (void*)0x11b3a, (void*)0x11b3c, (void*)0x11b3e				 		//RxPL flt0, outgain controlled from cmd:0x33
	
} ;

static const unsigned short profile_supp_params_data_narrowband[] = {
/*	0x7b81, 0x783d,						//plevdet_data_spk fall / rise time
	0x7dbc, 0x7ce0,						//plevdet_data_spk1 fall / rise time
	0x7b81, 0x783d,						//plevdet_data_paec_out fall / rise time
	0x7c0e, 0x7ff1,						//pnlev_tx_data fall / rise time
	0x7000,								//centclip_att
	0xE400,								//c2_thresh		//270509 GF


	0x0030,								//cntrini		//270509 GF
	0x7000,								//cntrmin		//020609 GF	reduced center_clipper noise suppression 
	0x783d, 0x07c2,						//rbetaoff / off 
	0x70f4, 0x0AAA,						//rbetaon / on	//020609 GF	suppression @ -2.9dB (instead of -9dB)	*/
#if 0  /* don't use Tx Peak Limiter for now */
/* TxPeakLimit */							
	0x0015,								//HagcTx toff_scale
	0x00ee,								//HagcTx toff
	0x7e52,								//HagcTx ton					
	0x4da3, 0x783f,						//PLevDet2TxComp1 fall / rise time
	0x7e69, 0x70f6,						//PLevDet2TxComp2 fall / rise time
#endif
/* RxPeakLimit */	
	0x0015,								//HagcRx toff_scale
	0x00ee,								//HagcRx toff
	0x7e52,								//HagcRx ton					
	0x4da3, 0x783f,						//PLevDet2RxComp1 fall / rise time
	0x7e69, 0x70f6,						//PLevDet2RxComp2 fall / rise time
/* Hfree */
	0x7f3d, 0x7eab,						//hfree_pl_rxin fall / rise	
	0x7c0e, 0x7c0e,						//hfree_pn_rxin fall / rise	
	0x7f3d, 0x7eab,						//hfree_pa_txin fall / rise	
	0x7c0e, 0x7c0e,						//hfree_pn_txin fall / rise	
	0x0050,								//hfree idlecntini
	0x0008,								//hfree idlecntini2
//	0xF800,								//rx ratio
	0x0050,								//hfree rxtxcntini
	0x0076,								//hfree suptidleoff
	0x7f89,								//hfree suptidleon
	0x05a5,								//hfree suptoff
	0x7a96,								//hfree supton
	0x7f3d, 0x7eab,						//hfree_pl_txin fall / rise	
	0x7f3d, 0x7eab,						//hfree_pa_rxin fall / rise
	/* CNG */	
	0x0004,								//comfnoiselevel		//170211GF
	/* PAEC mode */
	//0xcffb,			//190211GF
	/* RxPL emphasized */
	0x4000, 0xBAA6, 0x0B07, 0x5651, 0xE352, 0x51EC, 0x4000,
	0x4000, 0xB63F, 0x286C, 0x5964, 0xD8DC, 0x7fff
} ;


static const unsigned short profile_supp_params_data_wideband[] = {		//020609 GF	--> updated suppressor settings for wideband operation
/*	0x7DBB, 0x7C0F,						//plevdet_data_spk fall / rise time
	0x7EDC, 0x7E03,						//plevdet_data_spk1 fall / rise time
	0x7DBB, 0x7C0F,						//plevdet_data_paec_out fall / rise time
	0x7e03, 0x7fbb,						//pnlev_tx_data fall / rise time
	0x7000,								//centclip_att
	0xE400,								//c2_thresh


	0x0060,								//cntrini
	0x7000,								//cntrmin
	0x7C0F, 0x03F0,						//rbetaoff / off 
	0x783E, 0x0590,						//rbetaon / on	//020609 GF	suppression @ -2.9dB (instead of -9dB)	*/
#if 0  /* don't use Tx Peak Limiter for now */
/* TxPeakLimit */						
	0x0009,								//HagcTx toff_scale
	0x0039,								//HagcTx toff
	0x7f28,								//HagcTx ton					
	0x63b0, 0x7c10,						//PLevDet2TxComp1 fall / rise time
	0x7f34, 0x783f,						//PLevDet2TxComp2 fall / rise time
#endif
/* RxPeakLimit */	
	0x0009,								//HagcRx toff_scale
	0x0039,								//HagcRx toff
	0x7f28,								//HagcRx ton					
	0x63b0, 0x7c10,						//PLevDet2RxComp1 fall / rise time
	0x7f34, 0x783f,						//PLevDet2RxComp2 fall / rise time
/* Hfree */
	0x7f9e, 0x7f55,						//hfree_pl_rxin fall / rise	
	0x7e03, 0x7e03,						//hfree_pn_rxin fall / rise	
	0x7f9e, 0x7f55,						//hfree_pa_txin fall / rise	
	0x7e03, 0x7e03,						//hfree_pn_txin fall / rise	
	0x00A0,								//hfree idlecntini
	0x0010,								//hfree idlecntini2
//	0xEFFF,								//rx ratio
	0x00A0,								//hfree rxtxcntini
	0x003B,								//hfree suptidleoff
	0x7FC5,								//hfree suptidleon
	0x02d2,								//hfree suptoff
	0x7D2E,								//hfree supton
	0x7f9e, 0x7f55,						//hfree_pl_txin fall / rise	
	0x7f9e, 0x7f55,						//hfree_pa_rxin fall / rise
	/* CNG */	
	0x0004,								//comfnoiselevel
	/* PAEC mode */
//	0xcffb			//190211GF
	/* RxPL emphasized */
	0x4000, 0xBAA6, 0x0B07, 0x5651, 0xE352, 0x51EC, 0x4000,
	0x4000, 0xB63F, 0x286C, 0x5964, 0xD8DC, 0x7fff
} ;
#  endif
#endif



#if defined( CONFIG_SND_SC1445x_USE_AEC )
/* AEC setup */
static const unsigned short profile_aec_params_data[] = {
	0x4000, 0x0100, 0x7FFF, 0x0020, 0x0666, 0x1000, 0x0200, 0x2000,
	0x3000, 0x0200, 0x0020, 0x0020, 0x0100, 0x0100, 0x0040, 0x0040,
	0x0020, 0x0020, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x07D0, 0x0069, 0x8000, 0x8000, 0x0100, 0x0200,
	0x7DBC, 0x7C10, 0x7DBC, 0x7C10, 0x7DBC, 0x7C10, 0x7FDF, 0x7FFF,
	0x7DBC, 0x7C10, 0x0014, 0x7C10, 0x0148, 0x7FF2, 0x0014, 0x7C10,
	0x01CF, 0x7FF2, 0x7DBC, 0x7C10, 0x7DBC, 0x7C10, 0x7DBC, 0x7C10,
	0x4000, 0x0000, 0xF333, 0x0200, 0x0000, 0x7FFF, 0x4000, 0x7FFF,
	0x4000, 0x0247, 0x7FFF, 0x7FFF, 0x287A, 0x0247, 0x01DB, 0x0032,
	0x7F15, 0x06BE, 0x7C5E, 0x4000, 0x0000, 0xA562, 0x0000, 0x0000,
	0xFA97, 0xAA74, 0x051E, 0x061E, 0x0278, 0x0298, 0x0000, 0x0000,
	0x0000, 0x0002, 0x0000, 0x0000, 0x0000, 0x0000, 0x101D, 0x101D,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0244, 0x03F0, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0207, 0x0207, 0x0207, 0x0207, 0x0000, 0x0000, 0x0000
} ;
#endif

#endif  /* __AUDIO_PROFILE_SC1445x_F_G2_BOARD_H__ */


