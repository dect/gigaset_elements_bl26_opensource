/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 * tonegen-related stuff used by the audio engine
 */

#if !defined( __AUDIOENGINE_TONES_H__ )
#define __AUDIOENGINE_TONES_H__

#include "sc1445x_audioengine_defs.h"


#define USE_NL_PSTN_TONES
//#define USE_KR_PSTN_TONES


/* sin-cos lookup tables for the available frequencies, for 8kHz sampling */
#define ARGSIN_8KHZ_F0		0x0000
#define ARGSIN_8KHZ_F697	0x42a0
#define ARGSIN_8KHZ_F770	0x48c7
#define ARGSIN_8KHZ_F852	0x4f67
#define ARGSIN_8KHZ_F941	0x5639
#define ARGSIN_8KHZ_F1209	0x6816
#define ARGSIN_8KHZ_F1336	0x6efd
#define ARGSIN_8KHZ_F1477	0x755b
#define ARGSIN_8KHZ_F1633	0x7ab9
#define ARGSIN_8KHZ_F425	0x29f0
#define ARGSIN_8KHZ_F440	0x2b5c
#define ARGSIN_8KHZ_F1000	0x5a83
#define ARGSIN_8KHZ_F400	0x278e
#define ARGSIN_8KHZ_F100	0x0A0B
#define ARGSIN_8KHZ_F250	0x18F9
#define ARGSIN_8KHZ_F350	0x22BF
#define ARGSIN_8KHZ_F480	0x2F1F
#define ARGSIN_8KHZ_F600	0x3A1D
#define ARGSIN_8KHZ_F620	0x3A1D
#define ARGSIN_8KHZ_F680	0x4129
#define ARGSIN_8KHZ_F950	0x56E4
#define ARGSIN_8KHZ_F1200	0x678F
#define ARGSIN_8KHZ_F1400	0x720D
#define ARGSIN_8KHZ_F1800	0x7E6E
#define ARGSIN_8KHZ_F2060	0x7FDD
#define ARGSIN_8KHZ_F2130	0x7F56
#define ARGSIN_8KHZ_F2450	0x7818
#define ARGSIN_8KHZ_F2600	0x720D
#define ARGSIN_8KHZ_F2750	0x6A6F

#define ARGCOS_8KHZ_F0	 	0x8000
#define ARGCOS_8KHZ_F697 	0x6d4c
#define ARGCOS_8KHZ_F770 	0x694d
#define ARGCOS_8KHZ_F852 	0x6466
#define ARGCOS_8KHZ_F941 	0x5e9c
#define ARGCOS_8KHZ_F1209	0x4a81
#define ARGCOS_8KHZ_F1336	0x3fc5
#define ARGCOS_8KHZ_F1477	0x331d
#define ARGCOS_8KHZ_F1633	0x2463
#define ARGCOS_8KHZ_F425 	0x78f0
#define ARGCOS_8KHZ_F440 	0x7870
#define ARGCOS_8KHZ_F1000	0x5a83
#define ARGCOS_8KHZ_F400 	0x79bd
#define ARGCOS_8KHZ_F100 	0x7F9D
#define ARGCOS_8KHZ_F250 	0x7D8B
#define ARGCOS_8KHZ_F350 	0x7B33
#define ARGCOS_8KHZ_F480 	0x7705
#define ARGCOS_8KHZ_F600 	0x720D
#define ARGCOS_8KHZ_F620 	0x720D
#define ARGCOS_8KHZ_F680 	0x6E2E
#define ARGCOS_8KHZ_F950 	0x5DFF
#define ARGCOS_8KHZ_F1200	0x4B3E
#define ARGCOS_8KHZ_F1400	0x3A1D
#define ARGCOS_8KHZ_F1800	0x1406
#define ARGCOS_8KHZ_F2060	0xF9F8
#define ARGCOS_8KHZ_F2130	0xF2F4
#define ARGCOS_8KHZ_F2450	0xD3B2
#define ARGCOS_8KHZ_F2600	0xC5E3
#define ARGCOS_8KHZ_F2750	0xB8E2

static const unsigned short sc1445x_ae_tone_sin_8kHz[SC1445x_AE_TONE_INVALID] =
{
	ARGSIN_8KHZ_F0, ARGSIN_8KHZ_F697, ARGSIN_8KHZ_F770, ARGSIN_8KHZ_F852,
	ARGSIN_8KHZ_F941, ARGSIN_8KHZ_F1209, ARGSIN_8KHZ_F1336, ARGSIN_8KHZ_F1477,
	ARGSIN_8KHZ_F1633, ARGSIN_8KHZ_F425, ARGSIN_8KHZ_F440, ARGSIN_8KHZ_F1000,
	ARGSIN_8KHZ_F400, ARGSIN_8KHZ_F100, ARGSIN_8KHZ_F250, ARGSIN_8KHZ_F350,
	ARGSIN_8KHZ_F480, ARGSIN_8KHZ_F600, ARGSIN_8KHZ_F620, ARGSIN_8KHZ_F680,
	ARGSIN_8KHZ_F950, ARGSIN_8KHZ_F1200, ARGSIN_8KHZ_F1400, ARGSIN_8KHZ_F1800,
	ARGSIN_8KHZ_F2060, ARGSIN_8KHZ_F2130, ARGSIN_8KHZ_F2450, ARGSIN_8KHZ_F2600,
	ARGSIN_8KHZ_F2750
} ;

static const unsigned short sc1445x_ae_tone_cos_8kHz[SC1445x_AE_TONE_INVALID] =
{
	ARGCOS_8KHZ_F0, ARGCOS_8KHZ_F697, ARGCOS_8KHZ_F770, ARGCOS_8KHZ_F852,
	ARGCOS_8KHZ_F941, ARGCOS_8KHZ_F1209, ARGCOS_8KHZ_F1336, ARGCOS_8KHZ_F1477,
	ARGCOS_8KHZ_F1633, ARGCOS_8KHZ_F425, ARGCOS_8KHZ_F440, ARGCOS_8KHZ_F1000,
	ARGCOS_8KHZ_F400, ARGCOS_8KHZ_F100, ARGCOS_8KHZ_F250, ARGCOS_8KHZ_F350,
	ARGCOS_8KHZ_F480, ARGCOS_8KHZ_F600, ARGCOS_8KHZ_F620, ARGCOS_8KHZ_F680,
	ARGCOS_8KHZ_F950, ARGCOS_8KHZ_F1200, ARGCOS_8KHZ_F1400, ARGCOS_8KHZ_F1800,
	ARGCOS_8KHZ_F2060, ARGCOS_8KHZ_F2130, ARGCOS_8KHZ_F2450, ARGCOS_8KHZ_F2600,
	ARGCOS_8KHZ_F2750
} ;


#define ARGSIN_16KHZ_F0		0x0000
#define ARGSIN_16KHZ_F697	0x229a
#define ARGSIN_16KHZ_F770	0x261e
#define ARGSIN_16KHZ_F852	0x2a08
#define ARGSIN_16KHZ_F941	0x2e3b
#define ARGSIN_16KHZ_F1209	0x3a84
#define ARGSIN_16KHZ_F1336	0x401f
#define ARGSIN_16KHZ_F1477	0x4627
#define ARGSIN_16KHZ_F1633	0x4c93
#define ARGSIN_16KHZ_F425	0x1544
#define ARGSIN_16KHZ_F440	0x1602
#define ARGSIN_16KHZ_F1000	0x30fc
#define ARGSIN_16KHZ_F400	0x1406
#define ARGSIN_16KHZ_F100	0x0507
#define ARGSIN_16KHZ_F250	0x0C8C
#define ARGSIN_16KHZ_F350	0x118A
#define ARGSIN_16KHZ_F480	0x17FC
#define ARGSIN_16KHZ_F600	0x1DE2
#define ARGSIN_16KHZ_F620	0x1EDC
#define ARGSIN_16KHZ_F680	0x21C7
#define ARGSIN_16KHZ_F950	0x2EA7
#define ARGSIN_16KHZ_F1200	0x3A1D
#define ARGSIN_16KHZ_F1400	0x42E2
#define ARGSIN_16KHZ_F1800	0x5322
#define ARGSIN_16KHZ_F2060	0x5C9F
#define ARGSIN_16KHZ_F2130	0x5F04
#define ARGSIN_16KHZ_F2450	0x6904
#define ARGSIN_16KHZ_F2600	0x6D24
#define ARGSIN_16KHZ_F2750	0x70E5

#define ARGCOS_16KHZ_F0	 	0x8000
#define ARGCOS_16KHZ_F697 	0x7b3d
#define ARGCOS_16KHZ_F770 	0x7a32
#define ARGCOS_16KHZ_F852 	0x78e8
#define ARGCOS_16KHZ_F941 	0x775d
#define ARGCOS_16KHZ_F1209	0x71d9
#define ARGCOS_16KHZ_F1336	0x6eca
#define ARGCOS_16KHZ_F1477	0x6b12
#define ARGCOS_16KHZ_F1633	0x6693
#define ARGCOS_16KHZ_F425 	0x7e3a
#define ARGCOS_16KHZ_F440 	0x7e19
#define ARGCOS_16KHZ_F1000	0x7643
#define ARGCOS_16KHZ_F400 	0x7e6e
#define ARGCOS_16KHZ_F100 	0x7FE8
#define ARGCOS_16KHZ_F250 	0x7F63
#define ARGCOS_16KHZ_F350 	0x7ECD
#define ARGCOS_16KHZ_F480 	0x7DBD
#define ARGCOS_16KHZ_F600 	0x7C78
#define ARGCOS_16KHZ_F620 	0x7C3B
#define ARGCOS_16KHZ_F680 	0x7B78
#define ARGCOS_16KHZ_F950 	0x7733
#define ARGCOS_16KHZ_F1200	0x720D
#define ARGCOS_16KHZ_F1400	0x6D24
#define ARGCOS_16KHZ_F1800	0x6156
#define ARGCOS_16KHZ_F2060	0x585B
#define ARGCOS_16KHZ_F2130	0x55C7
#define ARGCOS_16KHZ_F2450	0x4931
#define ARGCOS_16KHZ_F2600	0x42E2
#define ARGCOS_16KHZ_F2750	0x3C58

static const unsigned short sc1445x_ae_tone_sin_16kHz[SC1445x_AE_TONE_INVALID] =
{
	ARGSIN_16KHZ_F0, ARGSIN_16KHZ_F697, ARGSIN_16KHZ_F770, ARGSIN_16KHZ_F852,
	ARGSIN_16KHZ_F941, ARGSIN_16KHZ_F1209, ARGSIN_16KHZ_F1336, ARGSIN_16KHZ_F1477,
	ARGSIN_16KHZ_F1633, ARGSIN_16KHZ_F425, ARGSIN_16KHZ_F440, ARGSIN_16KHZ_F1000,
	ARGSIN_16KHZ_F400, ARGSIN_16KHZ_F100, ARGSIN_16KHZ_F250, ARGSIN_16KHZ_F350,
	ARGSIN_16KHZ_F480, ARGSIN_16KHZ_F600, ARGSIN_16KHZ_F620, ARGSIN_16KHZ_F680,
	ARGSIN_16KHZ_F950, ARGSIN_16KHZ_F1200, ARGSIN_16KHZ_F1400, ARGSIN_16KHZ_F1800,
	ARGSIN_16KHZ_F2060, ARGSIN_16KHZ_F2130, ARGSIN_16KHZ_F2450, ARGSIN_16KHZ_F2600,
	ARGSIN_16KHZ_F2750
} ;

static const unsigned short sc1445x_ae_tone_cos_16kHz[SC1445x_AE_TONE_INVALID] =
{
	ARGCOS_16KHZ_F0, ARGCOS_16KHZ_F697, ARGCOS_16KHZ_F770, ARGCOS_16KHZ_F852,
	ARGCOS_16KHZ_F941, ARGCOS_16KHZ_F1209, ARGCOS_16KHZ_F1336, ARGCOS_16KHZ_F1477,
	ARGCOS_16KHZ_F1633, ARGCOS_16KHZ_F425, ARGCOS_16KHZ_F440, ARGCOS_16KHZ_F1000,
	ARGCOS_16KHZ_F400, ARGCOS_16KHZ_F100, ARGCOS_16KHZ_F250, ARGCOS_16KHZ_F350,
	ARGCOS_16KHZ_F480, ARGCOS_16KHZ_F600, ARGCOS_16KHZ_F620, ARGCOS_16KHZ_F680,
	ARGCOS_16KHZ_F950, ARGCOS_16KHZ_F1200, ARGCOS_16KHZ_F1400, ARGCOS_16KHZ_F1800,
	ARGCOS_16KHZ_F2060, ARGCOS_16KHZ_F2130, ARGCOS_16KHZ_F2450, ARGCOS_16KHZ_F2600,
	ARGCOS_16KHZ_F2750
} ;


#define ARGSIN_N( f )	ARGSIN_8KHZ_F##f
#define ARGCOS_N( f )	ARGCOS_8KHZ_F##f
#define ARGSIN_W( f )	ARGSIN_16KHZ_F##f
#define ARGCOS_W( f )	ARGCOS_16KHZ_F##f


/* parameters for the standard tones */
/* the values for DTMF's are pretty much standard everywhere */
/* the values below for the PSTN tones (busy, dial etc) are for NL */
/* (according to http://www.3amsystems.com/wireline/tone-search.htm) */
static const sc1445x_ae_tone dtmf[16][2] = {
	/* digit 0 */
	{ SC1445x_AE_TONE_F941,		SC1445x_AE_TONE_F1336 },
	/* digit 1 */
	{ SC1445x_AE_TONE_F697,		SC1445x_AE_TONE_F1209 },
	/* digit 2 */
	{ SC1445x_AE_TONE_F697,		SC1445x_AE_TONE_F1336 },
	/* digit 3 */
	{ SC1445x_AE_TONE_F697,		SC1445x_AE_TONE_F1477 },
	/* digit 4 */
	{ SC1445x_AE_TONE_F770,		SC1445x_AE_TONE_F1209 },
	/* digit 5 */
	{ SC1445x_AE_TONE_F770,		SC1445x_AE_TONE_F1336 },
	/* digit 6 */
	{ SC1445x_AE_TONE_F770,		SC1445x_AE_TONE_F1477 },
	/* digit 7 */
	{ SC1445x_AE_TONE_F852,		SC1445x_AE_TONE_F1209 },
	/* digit 8 */
	{ SC1445x_AE_TONE_F852,		SC1445x_AE_TONE_F1336 },
	/* digit 9 */
	{ SC1445x_AE_TONE_F852,		SC1445x_AE_TONE_F1477 },
	/* key * */
	{ SC1445x_AE_TONE_F941,		SC1445x_AE_TONE_F1209 },
	/* key # */
	{ SC1445x_AE_TONE_F941,		SC1445x_AE_TONE_F1477 },
	/* key A */
	{ SC1445x_AE_TONE_F697,		SC1445x_AE_TONE_F1633 },
	/* key B */
	{ SC1445x_AE_TONE_F770,		SC1445x_AE_TONE_F1633 },
	/* key C */
	{ SC1445x_AE_TONE_F852,		SC1445x_AE_TONE_F1633 },
	/* key D */
	{ SC1445x_AE_TONE_F941,		SC1445x_AE_TONE_F1633 },
} ;


typedef struct sc1445x_ae_simple_tone_t {
	sc1445x_ae_tone f1 ;
	sc1445x_ae_tone f2 ;
	sc1445x_ae_tone f3 ;
	sc1445x_ae_tone f4 ;
	unsigned short a1 ;
	unsigned short a2 ;
	unsigned short a3 ;
	unsigned short a4 ;
	unsigned short dur_on ;
	unsigned short dur_off ;
	unsigned short play_count ;  /* 0 means play "forever" */
} sc1445x_ae_simple_tone ;

typedef struct sc1445x_ae_complex_tone_t {
	unsigned short seq_len ;
	const sc1445x_ae_tone4_sequence_part seq[4] ;
	sc1445x_ae_tone_seq_repeat repeat_seq ;
	unsigned short play_count ;
} sc1445x_ae_complex_tone ;

/* struct to describe a PSTN tone */
typedef struct sc1445x_ae_pstn_tone_t {
	short is_simple_tone ;

	union {
		sc1445x_ae_simple_tone simple_tone ;

		sc1445x_ae_complex_tone complex_tone ;
	} ;
} sc1445x_ae_pstn_tone ;


#if defined ( USE_NL_PSTN_TONES )

static const sc1445x_ae_pstn_tone pstn_tones[] = {
	/* Busy tone */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F425,
				.f2 = SC1445x_AE_TONE_F0,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 500,
				.dur_off = 500,
				.play_count = 0,  /* play "forever" */
			},
		}
	},
	/* Congestion tone */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F425,
				.f2 = SC1445x_AE_TONE_F0,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 250,
				.dur_off = 250,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* Dial tone */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F425,
				.f2 = SC1445x_AE_TONE_F0,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 500,
				.dur_off = 0,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* Disconnect tone - I */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F425,
				.f2 = SC1445x_AE_TONE_F0,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 500,
				.dur_off = 500,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* Disconnect tone - II */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F425,
				.f2 = SC1445x_AE_TONE_F0,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 250,
				.dur_off = 250,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* Ringing tone */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 2,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 425 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 425 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 425 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 425 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 1000,
						.off_duration = 2000,
						.play_count = 1,
					},
					/* seq[1], silence */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 2000,
						.off_duration = 0,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_REPEAT_ALL,
				.play_count = 0,
			}
		}
	},
	/* Ring back tone */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F440,
				.f2 = SC1445x_AE_TONE_F480,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 1000,
				.dur_off = 2000,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* Special dial tone */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F425,
				.f2 = SC1445x_AE_TONE_F0,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 500,
				.dur_off = 50,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* Waiting tone */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 3,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 425 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 425 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 425 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 425 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 1000,
						.off_duration = 2000,
						.play_count = 1,
					},
					/* seq[1], silence */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 2000,
						.off_duration = 2000,
						.play_count = 1,
					},
					/* seq[2], silence */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 1000,
						.off_duration = 2000,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_REPEAT_ALL,
				.play_count = 0,
			}
		}
	},
	/* Dial tone 2 */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F425,
				.f2 = SC1445x_AE_TONE_F0,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 500,
				.dur_off = 0,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* Dial tone 3 */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F425,
				.f2 = SC1445x_AE_TONE_F0,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 500,
				.dur_off = 0,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* off-on dial tone */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F425,
				.f2 = SC1445x_AE_TONE_F0,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 500,
				.dur_off = 50,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* call waiting 1 */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 1,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 300,
						.off_duration = 300,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* call waiting 2 */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 2,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
					/* seq[1] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* call waiting 3 */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 3,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
					/* seq[1] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
					/* seq[2] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* call waiting 4 */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 3,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
					/* seq[1] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 300,
						.off_duration = 100,
						.play_count = 1,
					},
					/* seq[2] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* CIDCW CAS */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 1,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 2130 ),
								ARGCOS_N( 2750 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 2130 ),
								ARGSIN_N( 2750 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 2130 ),
								ARGCOS_W( 2750 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 2130 ),
								ARGSIN_W( 2750 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 85,
						.off_duration = 0,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* Out of service */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 4,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 950 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 950 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 950 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 950 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 330,
						.off_duration = 0,
						.play_count = 1,
					},
					/* seq[1] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 1400 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 1400 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 1400 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 1400 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 330,
						.off_duration = 0,
						.play_count = 1,
					},
					/* seq[2] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 1800 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 1800 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 1800 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 1800 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 330,
						.off_duration = 2000,
						.play_count = 1,
					},
					/* seq[3], silence */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 2000,
						.off_duration = 1000,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_REPEAT_ALL,
				.play_count = 0,
			}
		}
	},
	/* off-hook warning */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F1400,
				.f2 = SC1445x_AE_TONE_F2060,
				.f3 = SC1445x_AE_TONE_F2450,
				.f4 = SC1445x_AE_TONE_F2600,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 100,
				.dur_off = 100,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* addr_ack */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 2,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 600 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 600 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 600 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 600 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 125,
						.off_duration = 125,
						.play_count = 1,
					},
					/* seq[1] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 600 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 600 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 600 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 600 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 125,
						.off_duration = 125,
						.play_count = 1,
					}
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* keypad echo 1 */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 1,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 250 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 250 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 250 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 250 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* keypad echo 2 */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 1,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 350 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 350 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 350 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 350 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 0,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
} ;

#elif defined( USE_KR_PSTN_TONES )

static const sc1445x_ae_pstn_tone pstn_tones[] = {
	/* Busy tone */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F480,
				.f2 = SC1445x_AE_TONE_F620,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 500,
				.dur_off = 500,
				.play_count = 0,  /* play "forever" */
			},
		}
	},
	/* Congestion tone */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F480,
				.f2 = SC1445x_AE_TONE_F620,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 250,
				.dur_off = 250,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* Dial tone */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F350,
				.f2 = SC1445x_AE_TONE_F440,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 500,
				.dur_off = 0,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* Disconnect tone - I */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 2,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 480 ),
								ARGCOS_N( 620 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 480 ),
								ARGSIN_N( 620 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 480 ),
								ARGCOS_W( 620 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 480 ),
								ARGSIN_W( 620 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 250,
						.off_duration = 250,
						.play_count = 1,
					},
					/* seq[1] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 480 ),
								ARGCOS_N( 620 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 480 ),
								ARGSIN_N( 620 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 480 ),
								ARGCOS_W( 620 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 480 ),
								ARGSIN_W( 620 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 250,
						.off_duration = 250,
						.play_count = 1,
					}
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* Disconnect tone - II */
	/* same as Disconnect tone - I */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 2,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 480 ),
								ARGCOS_N( 620 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 480 ),
								ARGSIN_N( 620 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 480 ),
								ARGCOS_W( 620 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 480 ),
								ARGSIN_W( 620 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 250,
						.off_duration = 250,
						.play_count = 1,
					},
					/* seq[1] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 480 ),
								ARGCOS_N( 620 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 480 ),
								ARGSIN_N( 620 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 480 ),
								ARGCOS_W( 620 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 480 ),
								ARGSIN_W( 620 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 250,
						.off_duration = 250,
						.play_count = 1,
					}
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* Ringing tone */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 2,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 425 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 425 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 425 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 425 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 1000,
						.off_duration = 2000,
						.play_count = 1,
					},
					/* seq[1], silence */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 2000,
						.off_duration = 0,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_REPEAT_ALL,
				.play_count = 0,
			}
		}
	},
	/* Ring back tone */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F440,
				.f2 = SC1445x_AE_TONE_F480,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 1000,
				.dur_off = 2000,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* Special dial tone */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F425,
				.f2 = SC1445x_AE_TONE_F0,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 500,
				.dur_off = 50,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* Waiting tone */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 3,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 425 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 425 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 425 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 425 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 1000,
						.off_duration = 2000,
						.play_count = 1,
					},
					/* seq[1], silence */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 2000,
						.off_duration = 2000,
						.play_count = 1,
					},
					/* seq[2], silence */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 1000,
						.off_duration = 2000,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_REPEAT_ALL,
				.play_count = 0,
			}
		}
	},
	/* Dial tone 2 */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F350,
				.f2 = SC1445x_AE_TONE_F440,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 500,
				.dur_off = 100,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* Dial tone 3 */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 1,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 350 ),
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 350 ),
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 350 ),
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 350 ),
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 1000,
						.off_duration = 250,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* off-on dial tone */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F350,
				.f2 = SC1445x_AE_TONE_F440,
				.f3 = SC1445x_AE_TONE_F0,
				.f4 = SC1445x_AE_TONE_F0,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 100,
				.dur_off = 100,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* call waiting 1 */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 1,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 300,
						.off_duration = 300,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* call waiting 2 */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 2,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
					/* seq[1] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* call waiting 3 */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 3,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
					/* seq[1] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
					/* seq[2] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* call waiting 4 */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 3,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
					/* seq[1] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 300,
						.off_duration = 100,
						.play_count = 1,
					},
					/* seq[2] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 440 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 440 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 440 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 440 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* CIDCW CAS */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 1,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 2130 ),
								ARGCOS_N( 2750 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 2130 ),
								ARGSIN_N( 2750 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 2130 ),
								ARGCOS_W( 2750 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 2130 ),
								ARGSIN_W( 2750 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 85,
						.off_duration = 0,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* Out of service */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 4,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 950 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 950 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 950 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 950 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 330,
						.off_duration = 0,
						.play_count = 1,
					},
					/* seq[1] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 1400 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 1400 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 1400 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 1400 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 330,
						.off_duration = 0,
						.play_count = 1,
					},
					/* seq[2] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 1800 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 1800 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 1800 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 1800 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 330,
						.off_duration = 2000,
						.play_count = 1,
					},
					/* seq[3], silence */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 2000,
						.off_duration = 1000,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_REPEAT_ALL,
				.play_count = 0,
			}
		}
	},
	/* off-hook warning */
	{
		.is_simple_tone = 1,
		{
			.simple_tone =
			{
				.f1 = SC1445x_AE_TONE_F1400,
				.f2 = SC1445x_AE_TONE_F2060,
				.f3 = SC1445x_AE_TONE_F2450,
				.f4 = SC1445x_AE_TONE_F2600,
				.a1 = 0x7000,
				.a2 = 0x7fff,
				.a3 = 0x7fff,
				.a4 = 0x7fff,
				.dur_on = 100,
				.dur_off = 100,
				.play_count = 0,  /* play "forever" */
			}
		}
	},
	/* addr_ack */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 2,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 600 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 600 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 600 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 600 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 125,
						.off_duration = 125,
						.play_count = 1,
					},
					/* seq[1] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 600 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 600 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 600 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 600 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 125,
						.off_duration = 125,
						.play_count = 1,
					}
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* keypad echo 1 */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 1,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 250 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 250 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 250 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 250 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 100,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
	/* keypad echo 2 */
	{
		.is_simple_tone = 0,
		{
			.complex_tone =
			{
				.seq_len = 1,
				.seq =
				{
					/* seq[0] */
					{
						.tone4_params =
						{
							.narrow_argcos =
							{
								ARGCOS_N( 350 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
								ARGCOS_N( 0 ),
							},
							.narrow_argsin =
							{
								ARGSIN_N( 350 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
								ARGSIN_N( 0 ),
							},
							.wide_argcos =
							{
								ARGCOS_W( 350 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
								ARGCOS_W( 0 ),
							},
							.wide_argsin =
							{
								ARGSIN_W( 350 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
								ARGSIN_W( 0 ),
							},
							.amplitude =
							{
								0x7000,
								0x7fff,
								0x7fff,
								0x7fff,
							}
						},
						.on_duration = 100,
						.off_duration = 0,
						.play_count = 1,
					},
				},
				.repeat_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT,
				.play_count = 1,
			}
		}
	},
} ;

#endif


#endif  /* __AUDIOENGINE_TONES_H__ */

