/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 * Implementation of audio engine functions.
 */

#include <linux/delay.h>

#include "sc1445x_audioengine_api.h"
#include "../sc1445x_alsa.h"
#include "sc1445x_audioengine_tones.h"
#if defined( SC1445x_AE_ATA_SUPPORT )
#  include "../sc1445x_tone_det.h"
#endif

//ENABLE PCM LOG AND PLAYBACK
//#define FXO_PCM_LOG
//#define FXO_PLAY_PRERECORDED_PCM

/* the DSP firmware */
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
#  if defined( SC1445x_AE_ATA_FXO_SUPPORT )
#    include "DSP1_ATA/DSP1_ATA_FXO.e.DM"
#    include "DSP1_ATA/DSP1_ATA_FXO.e.PM"
#    ifdef FXO_PLAY_PRERECORDED_PCM
#      include "cas_tone.values"
#    endif
#  else
#    include "DSP1_ATA/DSP1_ATA.e.DM"
#    include "DSP1_ATA/DSP1_ATA.e.PM"
#  endif
#else
#  if defined( CONFIG_SND_SC1445x_USE_PAEC )
#    if defined( CONFIG_SC14452 )
#      if defined( CONFIG_SC14452_C )
#	include "DSP1_Phone/SC14452_DSP1_Phone_ES4.e.DM"
#	include "DSP1_Phone/SC14452_DSP1_Phone_ES4.e.PM"
#      else
#	include "DSP1_Phone/SC14452_DSP1_Phone.e.DM"
#	include "DSP1_Phone/SC14452_DSP1_Phone.e.PM"
#      endif
#    elif defined( CONFIG_SC14450 )
#	include "DSP1_Phone/DSP1_Phone_PAEC.e.DM"
#	include "DSP1_Phone/DSP1_Phone_PAEC.e.PM"
#    endif
#  else
#    include "DSP1_Phone/DSP1_Phone_AEC.e.DM"
#    include "DSP1_Phone/DSP1_Phone_AEC.e.PM"
#  endif
#endif
#if defined( CONFIG_SC14452_C )
#  include "DSP2_Phone/DSP2_Phone_ES4.e.PM"
#  include "DSP2_Phone/DSP2_Phone_ES4.e.DM"
#else
#  include "DSP2_Phone/DSP2_Phone.e.PM"
#  include "DSP2_Phone/DSP2_Phone.e.DM"
#endif

//#define DSP1_16kHz								//psavvop, This definition feeds only the 16kHz to DSP1 for WB operation on either WB or NB calls 

//#define VC_enable 								//psavvop-VC, This definition is needed to ensure that the generated clock to DSP1 is 16kHz!
													// The command "0x2d" can be used
													//When it is not defined the default Rx path of 8kHz/16kHz is used!
													// The command "0x2d" CAN NOT be used
#ifdef VC_enable 
/*** psavvop-VC, Definitions START ***/
	#define bInitOK 0x11f7e							//Driver needs to write vocal_clarity_data entries to initiate VC processing
	#define bFirst 0x11f7c							//Driver needs to write vocal_clarity_data entries to initiate VC processing
	//#define VC_proc 0x10638							//Driver needs to know when VC processing is enabled (1:ON, 0:OFF)
	#define DSP1_codec_en 0x10530					//Driver needs to know if CODEC output is enabled (1:ON, 0:OFF)
	#define DSP1_preset_fxp 0x12f0c					//Driver needs to alter the content of preset_fxp table of DSP acccording to 
													//wether the handset or speaker is enabled on board at the start of each call
	long preset_ear_fxp[] = {
		#include "preset_16KHz_ear.include"
	};
	long preset_spk_fxp[] = {
		#include "preset_16KHz_spk.include"
	};
/*** psavvop-VC, Definitions END ***/
#endif

//#define CHECK_LOST_DSP_INTS

//#define DONT_SEND_TONES

//#define DONT_HEAR_TONES

//#define DEBUG_WITH_GPIO

//#define DSP_DEBUG_WITH_GPIO

//#define DEBUG_iLBC_WITH_GPIO

//#define TEST_WITH_MARKED_PACKET

//#define DUMP_PLAYBACK_PACKETS

//#define DUMP_XMATRIX

//#define DO_REVERSE_LOOPBACK

//#define DONT_POWER_DOWN_LSR

//#define USE_CLASSD_AUTOPOWER_IN_HANDS_FREE

//#define DEBUG_SEND_DSP_CMD_STALL

//#define TRACE_DSP_CMD

//#define TRACE_AUDIOENGINE
#if defined( TRACE_AUDIOENGINE )
#  define DUMP_AUDIOENGINE_TRACE
//#  define TRACE_AUDIOEGINE_ISR
#endif

//#define DUMP_DSP_COMMANDS

/* uncomment to use special WIRELESS mode (DECT mic + external spk) */
//#define USE_SPECIAL_WIRELESS_MODE

/*
 * default amplitude for the first frequency in a DTMF
 * the default amplitude for the second frequency is 0x7fff
 * set to a value less than 0x7fff for some twist
 * set to 0x7fff for no twist
 */
#define TWISTED_DTMF_FIRST_AMPL		0x7fff

/* setup CODEC for a specifig rate */
static void sc1445x_internal_init_codec( const sc1445x_ae_state* ae_state,
					sc1445x_ae_raw_pcm_rate rate ) ;

/* send a command to a DSP */
static
void sc1445x_internal_send_dsp_cmd( unsigned short cmd, unsigned short arg ) ;

#if defined( DEBUG_SEND_DSP_CMD_STALL ) || defined( TRACE_AUDIOENGINE )
static void sc1445x_internal_dump_traces( const char* loc, unsigned short cmd,
							unsigned short arg ) ;
#endif


#if defined( TRACE_AUDIOENGINE )

#  define AUDIOENGINE_TRACE_SIZE	256

static const char* audioengine_loc_trace[AUDIOENGINE_TRACE_SIZE] ;
static unsigned long audioengine_time_trace[AUDIOENGINE_TRACE_SIZE] ;
static unsigned short audioengine_trace_pos = 0 ;

void sc1445x_private_dump_audioengine_trace( void )
{
	int i, j ;

	PRINT( "\naudioengine trace:\n" ) ;
	for( i = 0, j = audioengine_trace_pos ;  i < AUDIOENGINE_TRACE_SIZE ;
									++i ) {
		PRINT( "time=%lu loc='%s'\n", audioengine_time_trace[j],
						audioengine_loc_trace[j] ) ;
		if( ++j == AUDIOENGINE_TRACE_SIZE )
			j = 0 ;
	}
}

#if 1

static void AUDIOENGINE_TRACE_ADD2( const char* loc, unsigned long tme )
{
	unsigned long flags ;

	if( 0 == DSP1_CTRL_REG ) {
		PRINT( "%s: DSP1 is dead! loc='%s'\n", __FUNCTION__, loc ) ;
		sc1445x_internal_dump_traces( __FUNCTION__, 0, 0 ) ;
	} else if(  0 == DSP2_CTRL_REG ) {
		PRINT( "%s: DSP2 is dead! loc='%s'\n", __FUNCTION__, loc ) ;
		sc1445x_internal_dump_traces( __FUNCTION__, 0, 0 ) ;
	}

	local_irq_save( flags ) ;
	audioengine_loc_trace[audioengine_trace_pos] = loc ;
	audioengine_time_trace[audioengine_trace_pos] = tme ;
	if( ++audioengine_trace_pos == AUDIOENGINE_TRACE_SIZE )
		audioengine_trace_pos = 0 ;
	local_irq_restore( flags ) ;

#  if defined( DUMP_AUDIOENGINE_TRACE )
#    if defined( CONFIG_PRINTK_TIME )
	PRINT( PRINT_LEVEL "%s\n", loc ) ;
#    else
	PRINT( PRINT_LEVEL "[%lu] %s\n", tme, loc ) ;
#    endif
#  endif
}

#else

#  define AUDIOENGINE_TRACE_ADD2( loc, tme ) \
	do {								\
		audioengine_loc_trace[audioengine_trace_pos] = loc ;	\
		audioengine_time_trace[audioengine_trace_pos] = tme ;	\
		if( ++audioengine_trace_pos == AUDIOENGINE_TRACE_SIZE )	\
			audioengine_trace_pos = 0 ;			\
	} while( 0 )

#endif

#else

#  define AUDIOENGINE_TRACE_ADD2( loc, tme ) \
	do {  } while( 0 )

#endif  /* TRACE_AUDIOENGINE */

#define AUDIOENGINE_TRACE_ADD( loc ) \
	AUDIOENGINE_TRACE_ADD2( loc, jiffies )

#define AUDIOENGINE_TRACE_FUNC() \
	AUDIOENGINE_TRACE_ADD( __FUNCTION__ )


/* uncomment to keep a log of the PCM data in allocated buffers */
//#define PCM_LOG
#ifdef PCM_LOG
#  define PLAY_PCM_LOG	0x1B802
#  define CAP_PCM_LOG	0x1B8A2
#endif


#ifdef FXO_PCM_LOG
#  define FXO_PLAY_PCM_LOG	0x10140
#  define FXO_CAP_PCM_LOG	0x101e0
static void do_fxo_pcm_log(void);
#endif

#ifdef FXO_PLAY_PRERECORDED_PCM
static void send_recorded_cas(void);
#  define FXO_SEND_CAS_TONE	0x103c0 //0x01e0 on DSP1
#endif

//#define iLBC_LOG
#ifdef iLBC_LOG
#     define iLBC_CAP_LOG1 0x1b980
#     define iLBC_CAP_LOG2 0x1b9b2
#     define iLBC_CAP_LOG3 0x1b9e4
#endif

//#define MEASURE_C_DELAY
#ifdef MEASURE_C_DELAY
#  define MEASURE_C_DELAY_CAP_LOG1	(void*)0x1a400
#  define MEASURE_C_DELAY_CAP_LOG2	(void*)0x1a600
#  define MEASURE_C_DELAY_LOG_SIZE	10	/* in "frames" */
#  define MEASURE_C_DELAY_FRAME_SIZE	160	/* in bytes */
#  define MEASURE_C_DELAY_DSP_TRIGGER	*(unsigned short*)0x131fa
#endif

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT ) && defined( CONFIG_SC14452 )
/* uncomment if DSP's tonegen modules support 4 frequencies */
#  define HAVE_TONEGEN4
/* uncomment if DSPs do smooth transitions for tones and PCM */
#  define SMOOTH_IFACE_TRANSITION
#endif


#if defined( CONFIG_SC14452 )
/* uncomment to keep a log of the encoded audio on the playback path */
//#  define VOIP_PLAYBACK_LOG

/* uncomment to keep a log of the decoded audio on the playback path */
//#  define PCM_PLAYBACK_LOG
#endif

#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_ATA_SUPPORT )
/* uncomment to check dsp irq overflow */
//#  define CHECK_DSP_IRQ_OVERFLOW
#endif


#if defined( SC1445x_AE_SUPPORT_FAX )
static void sc1445x_internal_dsp_hot_download( const sc1445x_ae_state* ae_state );
#endif

/* alternate, internal, way to stop playing a tone */
static void sc1445x_internal_fast_stop_tone( sc1445x_ae_state* ae_state,
						unsigned short tg_mod ) ;
/* select to send output (playback) to ext speaker via class D amp */
static
short sc1445x_internal_enable_output_to_classd_amp(
			const sc1445x_ae_state* ae_state, short enable ) ;
/* select to send output (playback) via the hardware CODEC */
static
short sc1445x_internal_enable_output_to_hw_codec(
			const sc1445x_ae_state* ae_state, short enable ) ;

static void sc1445x_ae_internal_set_vmic( const sc1445x_ae_state* ae_state,
				const sc1445x_ae_iface_mode new_mode,
				short new_vmic ) ;
static void sc1445x_ae_internal_set_vspk( const sc1445x_ae_state* ae_state,
				const sc1445x_ae_iface_mode new_mode,
				short new_vspk ) ;

static void sc1445x_internal_init_channel_buffers( unsigned short ch,
				short is_wide ) ;

#if defined( SC1445x_AE_BT )
static void sc1445x_ae_internal_reattach_to_pcm( sc1445x_ae_state* ae_state,
						unsigned short pcm_dev_id ) ;
#endif

#if defined( DEBUG_WITH_GPIO ) || defined( TEST_WITH_MARKED_PACKET )

//#  define DEBUG_WITH_GPIO_0_06
//#  define DEBUG_WITH_GPIO_0_09
#  define DEBUG_WITH_GPIO_2_03
//#  define DEBUG_WITH_GPIO_2_10

#  if defined( DEBUG_WITH_GPIO_0_06 )

#    define DEBUG_WITH_GPIO_SETUP	do { P0_06_MODE_REG = 0x300 ; } while( 0 )
#    define DEBUG_WITH_GPIO_RAISE	do { P0_SET_DATA_REG = 0x40 ; } while( 0 )
#    define DEBUG_WITH_GPIO_LOWER	do { P0_RESET_DATA_REG = 0x40 ; } while( 0 )

#  elif defined( DEBUG_WITH_GPIO_0_09 )

#    define DEBUG_WITH_GPIO_SETUP	do { P0_09_MODE_REG = 0x300 ; } while( 0 )
#    define DEBUG_WITH_GPIO_RAISE	do { P0_SET_DATA_REG = 0x200 ; } while( 0 )
#    define DEBUG_WITH_GPIO_LOWER	do { P0_RESET_DATA_REG = 0x200 ; } while( 0 )

#  elif defined( DEBUG_WITH_GPIO_2_03 )

#    define DEBUG_WITH_GPIO_SETUP	do { P2_03_MODE_REG = 0x300 ; } while( 0 )
#    define DEBUG_WITH_GPIO_RAISE	do { P2_SET_DATA_REG = 0x8 ; } while( 0 )
#    define DEBUG_WITH_GPIO_LOWER	do { P2_RESET_DATA_REG = 0x8 ; } while( 0 )

#  elif defined( DEBUG_WITH_GPIO_2_10 )

#    define DEBUG_WITH_GPIO_SETUP	do { P2_10_MODE_REG = 0x300 ; } while( 0 )
#    define DEBUG_WITH_GPIO_RAISE	do { P2_SET_DATA_REG = 0x400 ; } while( 0 )
#    define DEBUG_WITH_GPIO_LOWER	do { P2_RESET_DATA_REG = 0x400 ; } while( 0 )

#  endif


#else  /* DEBUG_WITH_GPIO */

#  define DEBUG_WITH_GPIO_SETUP		do { } while( 0 )
#  define DEBUG_WITH_GPIO_RAISE		do { } while( 0 )
#  define DEBUG_WITH_GPIO_LOWER		do { } while( 0 )

#endif  /* DEBUG_WITH_GPIO */


/* turn on/off Packet Loss Concealment */
#define USE_PLC

/* turn on/off mic DC cancellation */
#define DO_MIC_DC_CANCELLATION


/* values for clock-related registers */
#if defined( CONFIG_SC14452 )
static unsigned short clk_spu1_val ;
static unsigned short clk_spu2_val[3] ;    /* for 8kHz, 16kHz, 32kHz */
static unsigned short clk_codec1_val[3] ;  /* for 8kHz, 16kHz, 32kHz */
static unsigned short clk_codec2_val[3] ;  /* for 8kHz, 16kHz, 32kHz */
static unsigned short clk_codec3_val[3] ;  /* for 8kHz, 16kHz, 32kHz */
#endif


/* error messages */
#if !defined( AUDIO_ENGINE_NO_OUTPUT )
const char sc1445x_ae_nomem_error[] =
	PRINT_LEVEL "%s: cannot allocate memory for %s!\n" ;
const char sc1445x_ae_null_ae_struct_error[] =
	PRINT_LEVEL "NULL audio engine struct passed to %s!\n" ;
const char sc1445x_ae_arg_out_of_range_error[] =
	PRINT_LEVEL "%s: argument %s is out of range!\n" ;
const char sc1445x_ae_null_pointer_arg_error[] =
	PRINT_LEVEL "NULL pointer argument passed to %s!\n" ;
const char sc1445x_ae_no_free_channel_for_codec_error[] =
	PRINT_LEVEL "No free channel to activate for codec type %d\n" ;
const char sc1445x_ae_channel_not_active_error[] =
	PRINT_LEVEL "The audio channel %d is not activated!\n" ;
const char sc1445x_ae_codec_not_supported_error[] =
	PRINT_LEVEL "CODEC type %d is not supported!\n" ;
#if 0
const char sc1445x_ae_tonegen_not_idle_error[] =
	PRINT_LEVEL "%s tone generation module already playing a tone!\n" ;
#endif
const char sc1445x_ae_tonegen_is_idle_error[] =
	PRINT_LEVEL "%s: tone generation module is idle!\n" ;
const char sc1445x_ae_cannot_change_mode_error[] =
	PRINT_LEVEL "%s: cannot change mode when there are active channels!\n" ;
const char sc1445x_ae_not_available_error[] =
	PRINT_LEVEL "%s: this feature is not available in this configuration!\n" ;
const char sc1445x_ae_already_fax_error[] =
	PRINT_LEVEL "The audio channel %d is already switched to fax!\n" ;
const char sc1445x_ae_not_fax_error[] =
	PRINT_LEVEL "The audio channel %d is not switched to fax!\n" ;
const char sc1445x_ae_line_not_mapped_error[] =
	PRINT_LEVEL "%s: line %d is not mapped to any audio stream!\n" ;
const char sc1445x_ae_no_free_audio_streams_error[] =
	PRINT_LEVEL "%s: line %d cannot be mapped to any audio stream!\n" ;
const char sc1445x_ae_ilbc_needs_channel4_error[] =
	PRINT_LEVEL "%s: using iLBC requires the 4th channel to be free, but it is being used!\n" ;
const char sc1445x_ae_ilbc_uses_channel4_error[] =
	PRINT_LEVEL "%s: the 4th channel is being used by the iLBC codec!\n" ;
const char sc1445x_ae_pcm_dev_attach_codec_busy_error[] =
	PRINT_LEVEL "%s: cannot attach to PCM device because CODEC/CLASSD is busy!\n" ;
const char sc1445x_ae_iface_mode_not_available_error[] =
	PRINT_LEVEL "%s: interface mode %d is not currently available!\n" ;
const char sc1445x_ae_pcm_dev_attach_impossible_error[] =
	PRINT_LEVEL "%s: cannot attach to any more PCM devices!\n" ;
const char sc1445x_ae_incompatible_audio_profile_error[] =
	PRINT_LEVEL "%s: the passed audio profile is incompatible with the current one!\n" ;
const char sc1445x_ae_cannot_mix_voice_and_raw_pcm_error[] =
	PRINT_LEVEL "%s: cannot mix raw PCM with voice in mode %d!\n" ;
const char sc1445x_ae_no_free_channel_for_raw_pcm_error[] =
	PRINT_LEVEL "%s: no free channel to mix raw PCM\n" ;
const char sc1445x_ae_already_mixing_raw_pcm_error[] =
	PRINT_LEVEL "%s: already mixing raw PCM with voice\n" ;
const char sc1445x_ae_not_mixing_raw_pcm_error[] =
	PRINT_LEVEL "%s: currently not mixing raw PCM with voice\n" ;
const char sc1445x_ae_dual_audio_path_already_active_error[] =
	PRINT_LEVEL "%s: dual audio path is already active\n" ;
const char sc1445x_ae_dual_audio_path_not_active_error[] =
	PRINT_LEVEL "%s: dual audio path is not active\n" ;
#endif


/* a fake packet type, to detect missed packets */
#define MISSED_PACKET_TYPE	0x03


/* the ISR type */
typedef irqreturn_t (* sc1445x_isr_t )( int, void* ) ;



/* an ugly hack to initialize const fields of a struct */
#define INIT_CONST_FIELD( type, s, x, val )	\
	*( type* )&( (s)->x ) = (val)


/* some parameters of the audio engine */
#define SC1445x_AE_SPK_COUNT			1
#define SC1445x_AE_MIC_COUNT			1
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
#  define SC1445x_AE_MAX_AUDIO_CHANNELS		4
#else
#  define SC1445x_AE_MAX_AUDIO_CHANNELS		3
#endif



/* return the mic used for an interface mode */
/* 1: MICp/MICn, 2: MICh, 8: PCM/RAMIO */
static short vmic_from_iface_mode( const sc1445x_ae_iface_mode m,
					const short using_external_codec )
{
	if( SC1445x_AE_IFACE_MODE_IDLE == m )
		return 0 ;

	if( using_external_codec )
		return 8 ;

	switch( m ) {
		case SC1445x_AE_IFACE_MODE_HANDSET:
		case SC1445x_AE_IFACE_MODE_OPEN_LISTENING:
#if defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD ) \
			|| defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD_CNX )
		case SC1445x_AE_IFACE_MODE_HEADSET:
#endif
			return 1 ;

		case SC1445x_AE_IFACE_MODE_WIRELESS:
		case SC1445x_AE_IFACE_MODE_BT_GSM:
		case SC1445x_AE_IFACE_MODE_BT_HEADSET:
			return 8 ;

		default:
			return 2 ;
	} ;
}


/* return the spk(s) used for an interface mode (bit mask) */
/* 1: LSRp/LSRn, 2: LSRp, 4: CLASSD, 8: PCM/RAMIO */
static short vspk_from_iface_mode( const sc1445x_ae_iface_mode m,
					const short using_external_codec )
{
	short mask = 0 ;

	if( SC1445x_AE_IFACE_MODE_IDLE == m )
		return mask ;

	if( using_external_codec )
		mask = 8 ;

	if( SC1445x_AE_IFACE_MODE_HANDSET == m  ||
			SC1445x_AE_IFACE_MODE_OPEN_LISTENING == m )
		mask |= 1 ;
	if( SC1445x_AE_IFACE_MODE_HEADSET == m )
#if !defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD )
		mask |= 2 ;
#else
		mask |= 1 ;
#endif
	if( !using_external_codec  &&
			( SC1445x_AE_IFACE_MODE_OPEN_LISTENING == m  ||
				SC1445x_AE_IFACE_MODE_HANDS_FREE == m ) )
		mask |= 4 ;
	if( SC1445x_AE_IFACE_MODE_WIRELESS == m )
#if !defined( USE_SPECIAL_WIRELESS_MODE )
		mask = 8 ;
#else
		mask = 4 ;
#endif
	else if ( SC1445x_AE_IFACE_MODE_BT_GSM == m  ||
				SC1445x_AE_IFACE_MODE_BT_HEADSET == m )
		mask = 8 ;

	/* we cannot have 1 and 2 at the same time */
	BUG_ON( ( 3 == (mask & 3) ) ) ;

	return mask ;
}


#if defined( CONFIG_SC1445x_CVQ_METRICS ) \
		|| defined( CONFIG_SC1445x_CVQ_METRICS_MODULE )

#include "../cvq_metrics/sc1445x_cvqm_wrapper.h"

static struct sc1445x_cvqm_callbacks cvqm_cbs ;

/*
 * interface with sc1445x-cvqm
 */
void sc1445x_ae_private_register_cvqm_callbacks(
				struct sc1445x_cvqm_callbacks* cbs )
{
	if( !cbs )
		return ;

	cvqm_cbs = *cbs ;
}
EXPORT_SYMBOL( sc1445x_ae_private_register_cvqm_callbacks ) ;

#endif  /* CONFIG_SC1445x_CVQ_METRICS */


#if defined( SC1445x_AE_PHONE_DECT )

/* number of audio streams supported by the DSP */
#  define SC1445x_AE_AUDIO_STREAMS_COUNT		5

/* number of available RAMIO lines */
#  define SC1445x_AE_RAMIO_LINES_COUNT			4

#  define SC1445x_AE_TOTAL_LINES_COUNT      \
		( SC1445x_AE_NATIVE_DECT_LINE_OFFSET + SC1445x_AE_RAMIO_LINES_COUNT )

#elif defined( SC1445x_AE_PCM_LINES_SUPPORT )

/* number of audio streams supported by the DSP */
#  if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
#    define SC1445x_AE_AUDIO_STREAMS_COUNT		4
#  else
#    define SC1445x_AE_AUDIO_STREAMS_COUNT		3
#  endif

/* number of available PCM lines */
#  if defined( SC1445x_AE_ATA_SUPPORT ) || \
				( defined( SC1445x_AE_DECT_SUPPORT ) && \
				 !defined( SC1445x_AE_NATIVE_DECT_SUPPORT ) )
#    define SC1445x_AE_PCM_LINES_COUNT		SC1445x_AE_AUDIO_STREAMS_COUNT
#  else
#    define SC1445x_AE_PCM_LINES_COUNT			0
#  endif

/* number of available RAMIO lines */
#  if defined( SC1445x_AE_NATIVE_DECT_SUPPORT )
#    define SC1445x_AE_RAMIO_LINES_COUNT	SC1445x_AE_AUDIO_STREAMS_COUNT
#  else
#    define SC1445x_AE_RAMIO_LINES_COUNT		0
#  endif

/* total number of lines (PCM+RAMIO) */
#  if defined( SC1445x_AE_NATIVE_DECT_SUPPORT )
#    define SC1445x_AE_TOTAL_LINES_COUNT      \
	( SC1445x_AE_NATIVE_DECT_LINE_OFFSET + SC1445x_AE_RAMIO_LINES_COUNT )
#  else
#    define SC1445x_AE_TOTAL_LINES_COUNT      \
		( SC1445x_AE_PCM_LINES_COUNT + SC1445x_AE_RAMIO_LINES_COUNT )
#  endif

#  if defined( CONFIG_CVM480_DECT_SUPPORT )
/* the first PCM line that is used by a DECT phone */
/* (lower numbers are used by ATA) */
#    if defined( CONFIG_ATA_1_FXS_NO_FXO_1_CVM )
#	define SC1445x_AE_PCM_FIRST_DECT_LINE	1
#    else
#	define SC1445x_AE_PCM_FIRST_DECT_LINE	2
#    endif
#  elif defined( SC1445x_AE_NATIVE_DECT_SUPPORT )
#	define SC1445x_AE_PCM_FIRST_DECT_LINE	0
#  endif


#  if defined( SC1445x_AE_ATA_SUPPORT )
static void sc1445x_internal_start_fsk_decoder( sc1445x_ae_state* ae_state,
						unsigned short stream, short send_cid_type2_ack ) ;
static void sc1445x_internal_cid_read_fsk_char( sc1445x_ae_state* ae_state,
						unsigned short stream ) ;
static void sc1445x_internal_activate_fsk_decoder_wq( void* arg ) ;
#  endif


/* update the interconnection matrix on the DSPs */
static void sc1445x_internal_update_interconnection_matrix(
						sc1445x_ae_state* ae_state ) ;


#define XMATRIX_VOIP_CHANNEL_BIT( ch )		\
	( 1 << ( SC1445x_AE_MAX_AUDIO_CHANNELS - 1 - (ch) + 8 ) )

#define XMATRIX_AUDIO_STREAM_BIT( s )		\
	( 1 << ( SC1445x_AE_AUDIO_STREAMS_COUNT - 1 - s ) )


/* turns on the bit that corresponds to ch */
/* returns -1 if the bit was already set */
static inline
short xmatrix_listen_to_voip_channel( sc1445x_ae_xmatrix_entry* e,
							unsigned short ch )
{
	/* this is an internal function, we don't check args */

	if( *e & XMATRIX_VOIP_CHANNEL_BIT( ch ) )
		return -1 ;

	*e |= XMATRIX_VOIP_CHANNEL_BIT( ch ) ;
	return 0 ;
}

/* turns off the bit that corresponds to ch */
/* returns -1 if the bit was already cleared */
static inline
short xmatrix_dont_listen_to_voip_channel( sc1445x_ae_xmatrix_entry* e,
							unsigned short ch )
{
	/* this is an internal function, we don't check args */

	if( !( *e & XMATRIX_VOIP_CHANNEL_BIT( ch ) ) )
		return -1 ;

	*e &= ~XMATRIX_VOIP_CHANNEL_BIT( ch ) ;
	return 0 ;
}

/* turns on the bit that corresponds to s */
/* returns -1 if the bit was already set */
static inline
short xmatrix_listen_to_audio_stream( sc1445x_ae_xmatrix_entry* e,
							unsigned short s )
{
	/* this is an internal function, we don't check args */

	if( *e & XMATRIX_AUDIO_STREAM_BIT( s ) )
		return -1 ;

	*e |= XMATRIX_AUDIO_STREAM_BIT( s ) ;
	return 0 ;
}

/* turns off the bit that corresponds to s */
/* returns -1 if the bit was already cleared */
static inline
short xmatrix_dont_listen_to_audio_stream( sc1445x_ae_xmatrix_entry* e,
							unsigned short s )
{
	/* this is an internal function, we don't check args */

	if( !( *e & XMATRIX_AUDIO_STREAM_BIT( s ) ) )
		return -1 ;

	*e &= ~XMATRIX_AUDIO_STREAM_BIT( s ) ;
	return 0 ;
}


#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
/* Dynamic initialiazation of dect structures */ /*AK*/
static void sc1445x_internal_init_narrowband_dect_structure(
		const sc1445x_ae_state* ae_state,
		unsigned short local_stream_physical_stream ) ;
static void sc1445x_internal_init_wideband_dect_structure(
		unsigned short local_stream_physical_stream ) ;
#endif
					

/* inform DSP about the line mappings and types */
static
void sc1445x_internal_enforce_line_mappings( const sc1445x_ae_state* ae_state,
			const unsigned short line, const unsigned short s )
{
	/* this is an internal function, we don't check args */

	unsigned short cmd, val1, val2 ;

	DPRINT( PRINT_LEVEL "%s: %s mapping for line %d to audio stream %d\n",
								__FUNCTION__,
		ae_state->line_mappings[line] < 0 ?  "setting" :  "changing",
								line, s ) ;

	if( ae_state->line_types[line] == ae_state->prev_line_types[line]
			&&  s == ae_state->line_mappings[line]  )
		return ;

	/* disconnect line before change */
	cmd = ( s < 3 ) ?  ( 0x0004 + s ) :  0x0026 ;
	sc1445x_internal_send_dsp_cmd( cmd, 0x0000 ) ;

	ae_state->prev_line_types[line] = ae_state->line_types[line] ;
	ae_state->line_mappings[line] = s ;
	val1 = val2 = s << 8 ;
	switch( ae_state->line_types[line] ) {
		case SC1445x_AE_LINE_TYPE_ATA:
			if( ae_state->line2pcm_mappings[line] >= 0 )
				val2 |= ae_state->line2pcm_mappings[line] ;
			else
				val2 |= line /*% ATA_DEV_CNT*/ ;
			break ;

		case SC1445x_AE_LINE_TYPE_CVM_DECT_NARROW:
			val1 |= 1 ;
			val2 |= line /*% SC1445x_AE_PCM_LINES_COUNT*/ ;
			break ;

		case SC1445x_AE_LINE_TYPE_CVM_DECT_WIDE_8KHZ_ALAW:
		case SC1445x_AE_LINE_TYPE_CVM_DECT_WIDE_8KHZ_ULAW:
			//TODO: diffentiate between these two
			val1 |= 2 ;
			val2 |= line /*% SC1445x_AE_PCM_LINES_COUNT*/ ;
			break ;

		case SC1445x_AE_LINE_TYPE_CVM_DECT_WIDE_16KHZ:
			val1 |= 3 ;
			val2 |= line /*% SC1445x_AE_PCM_LINES_COUNT*/ ;
			break ;

		case SC1445x_AE_LINE_TYPE_NATIVE_DECT_NARROW:
			val1 |= 4 ;
			val2 |= 
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
				4
#else
				3
#endif
				+ ( (line - SC1445x_AE_NATIVE_DECT_LINE_OFFSET)
					/*% SC1445x_AE_RAMIO_LINES_COUNT*/ ) ;
			sc1445x_internal_init_narrowband_dect_structure(
							ae_state, val2 ) ;
			break ;

		case SC1445x_AE_LINE_TYPE_NATIVE_DECT_WIDE:
			val1 |= 5 ;
			val2 |= 
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
				4
#else
				3
#endif
				+ ( (line - SC1445x_AE_NATIVE_DECT_LINE_OFFSET)
					/*% SC1445x_AE_RAMIO_LINES_COUNT*/ ) ;
			sc1445x_internal_init_wideband_dect_structure( val2 ) ;
			break ;

		case SC1445x_AE_LINE_TYPE_FXO:
			val1 |= 7 ;
			if( ae_state->line2pcm_mappings[line] >= 0 )
				val2 |= ae_state->line2pcm_mappings[line] ;
			else
				val2 |= line /*% ATA_DEV_CNT*/ ;
			break ;

		case SC1445x_AE_LINE_TYPE_INVALID:
			/* can't happen */
			break ;
	}
	sc1445x_internal_send_dsp_cmd( 0x0011, val1 ) ;
	sc1445x_internal_send_dsp_cmd( 0x0016, val2 ) ;
	DPRINT( PRINT_LEVEL "%s: (0x0011, 0x%04x) (0x0016, 0x%04x)\n",
						__FUNCTION__, val1, val2 ) ;

}

/* return the audio stream number that line is mapped to */
/* if create_mapping != 0, a new mapping will be attempted if none exists */
/* negative return values indicate an error */
short sc1445x_private_line2stream( const sc1445x_ae_state* ae_state,
				unsigned short line, short create_mapping )
{
	/* this is an internal function, we don't check args */

	short s = ae_state->line_mappings[line] ;
#if( SC1445x_AE_AUDIO_STREAMS_COUNT != SC1445x_AE_TOTAL_LINES_COUNT )
	unsigned short i, mask = 0 ;
#endif

	if( s >= 0 ) {
		sc1445x_internal_enforce_line_mappings( ae_state, line, s ) ;
		return s ;
	}

#if( SC1445x_AE_AUDIO_STREAMS_COUNT == SC1445x_AE_TOTAL_LINES_COUNT )

#warning using static line mapping

	/* static mapping is just fine! */
	s = line ;

#else

#warning using dynamic line mapping

	/* try dynamic mapping */

	if( !create_mapping ) {
		PRINT( sc1445x_ae_line_not_mapped_error, __FUNCTION__, line ) ;
		return SC1445x_AE_ERR_LINE_NOT_MAPPED ;
	}

	/* see if there is a free audio stream */
	for( i = 0 ;  i < SC1445x_AE_TOTAL_LINES_COUNT ;  ++i ) {
		s = ae_state->line_mappings[i] ;
		if( s < 0 )
			continue ;
		mask |= 1 << s ;
	}

	if( unlikely( ( 1 << SC1445x_AE_AUDIO_STREAMS_COUNT ) - 1  ==  mask ) )
		return SC1445x_AE_ERR_NO_FREE_AUDIO_STREAMS ;

	/* there is at least one available stream, locate it */
	for( s = 0 ;  s < SC1445x_AE_AUDIO_STREAMS_COUNT ;  ++s, mask >>= 1 ) {
		if( !( mask & 1 ) )
			break ;
	}

#endif

	sc1445x_internal_enforce_line_mappings( ae_state, line, s ) ;

	return s ;
}

#endif


/* define a macro for functions that are not implemented when building for */
/* ATA and DECT */
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
#define NOT_AVAILABLE_WITH_PCM_LINES				\
	PRINT( sc1445x_ae_not_available_error, __FUNCTION__ ) ;	\
	return SC1445x_AE_ERR_NOT_AVAILABLE ;
#else
#define NOT_AVAILABLE_WITH_PCM_LINES
#endif


/* we have 1 tonegen module for each PCM line, but only 3 for Phone */
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
# define SC1445x_AE_TONEGEN_MOD_COUNT	SC1445x_AE_AUDIO_STREAMS_COUNT
#else
# define SC1445x_AE_TONEGEN_MOD_COUNT	3
#endif


static unsigned char*
		sc1445x_ae_playback_bufs[SC1445x_AE_MAX_AUDIO_CHANNELS] = {
	(unsigned char*)0x1888a,
	(unsigned char*)0x1984a,
	(unsigned char*)0x1a80a,
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(unsigned char*)0x1ba8a
#endif
} ;
static volatile unsigned short*
	sc1445x_ae_g711_1_playback_bufs[SC1445x_AE_MAX_AUDIO_CHANNELS][2] =
{
		{ (unsigned short*)0x18002, (unsigned short*)0x18040 },
		{ (unsigned short*)0x18fc2, (unsigned short*)0x19000 },
		{ (unsigned short*)0x19f82, (unsigned short*)0x19fc0 },
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
		{ (unsigned short*)0x1b202, (unsigned short*)0x1b240 }
#endif
} ;

static unsigned char*
		sc1445x_ae_capture_bufs[SC1445x_AE_MAX_AUDIO_CHANNELS] = {
	(unsigned char*)0x18002,
	(unsigned char*)0x18fc2,
	(unsigned char*)0x19f82,
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(unsigned char*)0x1b202
#endif
} ;
static volatile unsigned short*
	sc1445x_ae_g711_1_capture_bufs[SC1445x_AE_MAX_AUDIO_CHANNELS][2] =
{
		{ (unsigned short*)0x18002, (unsigned short*)0x18040 },
		{ (unsigned short*)0x18fc2, (unsigned short*)0x19000 },
		{ (unsigned short*)0x19f82, (unsigned short*)0x19fc0 },
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
		{ (unsigned short*)0x1b202, (unsigned short*)0x1b240 }
#endif
} ;


/* variables for synchronizing with DSPs */
static volatile unsigned short*
		sc1445x_ae_playback_ch_status[SC1445x_AE_MAX_AUDIO_CHANNELS] = {
	(unsigned short*)0x18888,
	(unsigned short*)0x19848,
	(unsigned short*)0x1a808,
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(unsigned short*)0x1ba88
#endif
} ;
static volatile unsigned short*
	sc1445x_ae_g711_1_playback_ch_status[SC1445x_AE_MAX_AUDIO_CHANNELS][2] =
{
		{ (unsigned short*)0x18000, (unsigned short*)0x1803e },
		{ (unsigned short*)0x18fc0, (unsigned short*)0x18ffe },
		{ (unsigned short*)0x19f80, (unsigned short*)0x19fbe },
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
		{ (unsigned short*)0x1b200, (unsigned short*)0x1b23e }
#endif
} ;

static volatile unsigned short*
		sc1445x_ae_capture_ch_header[SC1445x_AE_MAX_AUDIO_CHANNELS] = {
	(unsigned short*)0x18000,
	(unsigned short*)0x18fc0,
	(unsigned short*)0x19f80,
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(unsigned short*)0x1b200
#endif
} ;
static volatile unsigned short*
	sc1445x_ae_g711_1_capture_ch_header[SC1445x_AE_MAX_AUDIO_CHANNELS][2] =
{
		{ (unsigned short*)0x18000, (unsigned short*)0x1803e },
		{ (unsigned short*)0x18fc0, (unsigned short*)0x18ffe },
		{ (unsigned short*)0x19f80, (unsigned short*)0x19fbe },
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
		{ (unsigned short*)0x1b200, (unsigned short*)0x1b23e }
#endif
} ;

static  volatile unsigned short*
		sc1445x_ae_playback_status = (unsigned short*)0x1b000 ;
static  volatile unsigned short*
		sc1445x_ae_capture_status = (unsigned short*)0x1b004 ;



/* some pointers in the shared DSP memory */
static volatile unsigned short* dsp1_prog_mem  = (unsigned short*)0x1030000 ;
#if defined( CONFIG_SC14450 )
static volatile unsigned short* dsp2_prog_mem  = (unsigned short*)0x1034000 ;
#elif defined( CONFIG_SC14452 )
static volatile unsigned short* dsp2_prog_mem  = (unsigned short*)0x1038000 ;
#endif
static volatile unsigned short* dsp1_data_mem  = (unsigned short*)0x0010000 ;
static volatile unsigned short* dsp2_data_mem  = (unsigned short*)0x0018000 ;

/* the DSP variables for command passing */
static volatile unsigned short* dsp1_cmd     = (unsigned short*)0x12A00 ;
static volatile unsigned short* dsp1_cmd_val = (unsigned short*)0x12A02 ;


/* DSP interrupt counters */
unsigned int sc1445x_internal_dsp1_irq_cnt = 0 ;
unsigned int sc1445x_internal_dsp2_irq_cnt = 0 ;
unsigned int sc1445x_internal_write_irq_cnt = 0 ;
unsigned int sc1445x_internal_read_irq_cnt = 0 ;
unsigned int sc1445x_internal_raw_pcm_irq_cnt = 0 ;
#if defined( SMOOTH_IFACE_TRANSITION )
unsigned int sc1445x_internal_smooth_transition_irq_cnt = 0 ;
#endif
#if defined( SC1445x_AE_ATA_SUPPORT )
unsigned int sc1445x_internal_cid_irq_cnt = 0 ;
#endif


/******************/
/* AUDIO PROFILES */
/******************/

/*
 * define values for spk and mic levels presented to vspk/vmic user
 * speaker volume levels should be defined in profile_vspk_levels
 * mic gain levels should be defined in profile_vmic_levels
 * also define wideband/narrowband filters for TX/RX
 * these should be defined in:
 * 	profile_narrowband_filters_TX1
 * 	profile_narrowband_filters_TX2
 * 	profile_narrowband_filters_TX3
 * 	profile_narrowband_filters_RX1
 * 	profile_narrowband_filters_RX2
 * 	profile_narrowband_filters_RX3
 * 	profile_wideband_filters_TX1
 * 	profile_wideband_filters_TX2
 * 	profile_wideband_filters_TX3
 * 	profile_wideband_filters_RX1
 * 	profile_wideband_filters_RX2
 * 	profile_wideband_filters_RX3
 * also define any AEC/PAEC parameters
 */

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )

#if defined( CONFIG_SND_SC1445x_DEMO_V2_DK_PROFILE )

#warning compiling with default audio profile for SC1445x Demo V2 DK
#include "audio_profile_sc1445x_demo_v2_dk.h"

#elif defined( CONFIG_SND_SC1445x_DEMO_V2_DK_SHORT4_PROFILE )

#warning compiling with short4 audio profile for SC1445x Demo V2 DK
#include "audio_profile_sc1445x_demo_v2_dk_short4.h"

#elif defined( CONFIG_SND_SC1445x_THO2022 )

#warning compiling with audio profile for THO2022
#include "audio_profile_SC1445x_tho2022.h"

#elif defined( CONFIG_SND_SC1445x_F_G2_BOARD )

#warning compiling with audio profile for F_G2_BOARD
#include "audio_profile_SC1445x_f_g2_board.h"

#elif defined( CONFIG_SND_SC1445x_D150cam )

#warning compiling with audio profile for D150cam
#include "audio_profile_sc1445x_D150cam.h"

#elif defined( CONFIG_SND_SC1445x_DE900 )

#warning compiling with audio profile for DE900
#include "audio_profile_sc1445x_DE900.h"

#elif defined( CONFIG_SND_SC1445x_L_V1_BOARD )

#warning compiling with audio profile for L_V1_BOARD
#include "audio_profile_sc1445x_l_v1_board.h"

#elif defined( CONFIG_SND_SC1445x_L_V2_BOARD )

#warning compiling with audio profile for L_V2_BOARD
#include "audio_profile_sc1445x_l_v2_board.h"

#elif defined( CONFIG_SND_SC1445x_L_V5_BOARD )

#warning compiling with audio profile for L_V5_BOARD
#include "audio_profile_sc1445x_l_v5_board.h"

#elif defined( CONFIG_SND_SC1445x_VT_V1_BOARD )

#warning compiling with audio profile for VT_V1_BOARD
#include "audio_profile_sc1445x_VT_v1_board.h"

#elif defined( CONFIG_SND_SC1445x_SMG_I_BOARD )

#warning compiling with audio profile for SMG_I_BOARD
#include "audio_profile_sc1445x_SMG_I_board.h"

#elif defined( CONFIG_SND_SC1445x_IN_V1_BOARD )

#warning compiling with audio profile for IN_V1_BOARD
#include "audio_profile_sc1445x_in_v1_board.h"

#else

#error No audio profile defined!

#endif

#endif  /* SC1445x_AE_PCM_LINES_SUPPORT */



/****************/
/* INTERNAL API */
/****************/

/* allocate memory for audio engine state data -- all fields are zeroed */
static short sc1445x_internal_alloc_mem( sc1445x_ae_state* ae_state,
				unsigned short nspks, unsigned short nmics,
				unsigned short naudiochannels ) ;
/* free memory allocated for audio engine state data */
static short sc1445x_internal_free_mem( sc1445x_ae_state* ae_state ) ;

/*
 * set up the codec to use on audio channel ch
 * (dec_ch is valid only for G711.1 codec
 */
static short sc1445x_internal_set_codec( sc1445x_ae_state* ae_state,
				unsigned short ch, unsigned short dec_ch,
				sc1445x_ae_codec_type enc_ctype,
				sc1445x_ae_codec_type dec_ctype ) ;

/* initialize several registers before using the DSPs/CODEC */
static void sc1445x_internal_init_regs( const sc1445x_ae_state* ae_state ) ;
/* initialize DSP memories */
static void sc1445x_internal_init_dsp_mem( void ) ;
/* install the IRQ handler for DSP interrupts */
static int sc1445x_internal_install_isr( sc1445x_isr_t isr1, sc1445x_isr_t isr2,
					sc1445x_ae_state* ae_state ) ;

/* send a command to a DSP */
static
void sc1445x_internal_send_dsp_cmd( unsigned short cmd, unsigned short arg ) ;



/* set the RX/TX filters */
static void sc1445x_ae_set_rx_tx_filters( sc1445x_ae_state* ae_state,
					short is_external, short is_wideband,
					short is_headset ) ;


/* helper function for setting the current vmic gain */
static void do_sc1445x_ae_set_vmic_gain( const sc1445x_ae_state* ae_state ) ;
static void sc1445x_internal_vmic_gain_wq( void* arg ) ;
static void sc1445x_internal_release_channel_wq( void* arg ) ;

/* helper function for setting the current vspk volume */
static void do_sc1445x_ae_set_vspk_volume( const sc1445x_ae_state* ae_state ) ;



static inline unsigned short currently_wideband( void )
{
	unsigned short sync = ( DSP_MAIN_SYNC1_REG & AD_SYNC ) >> 6 ;

	return  1 == sync  ||  2 == sync ;
}

static inline unsigned short currently_using_headset(
					const sc1445x_ae_state* ae_state )
{
	return  SC1445x_AE_IFACE_MODE_HEADSET == ae_state->iface_mode ;
}

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
static inline
unsigned short is_stream_wideband( const sc1445x_ae_state* ae_state,
						const unsigned short s )
{
	unsigned short lt = SC1445x_AE_LINE_TYPE_INVALID ;
	unsigned short line ;

	for( line = 0 ;  line < SC1445x_AE_TOTAL_LINES_COUNT ;  ++line ) {
		if( s == ae_state->line_mappings[line] ) {
			lt = ae_state->line_types[line] ;
			break ;
		}
	}

	DPRINT( "%s: %d:%d\n", __FUNCTION__, line, lt ) ;
	switch( lt ) {
		case SC1445x_AE_LINE_TYPE_ATA:
		case SC1445x_AE_LINE_TYPE_FXO:
		case SC1445x_AE_LINE_TYPE_CVM_DECT_NARROW:
		case SC1445x_AE_LINE_TYPE_CVM_DECT_WIDE_8KHZ_ALAW:  //TODO: check
		case SC1445x_AE_LINE_TYPE_CVM_DECT_WIDE_8KHZ_ULAW:  //TODO: check
		case SC1445x_AE_LINE_TYPE_NATIVE_DECT_NARROW:
			return 0 ;

		case SC1445x_AE_LINE_TYPE_CVM_DECT_WIDE_16KHZ:
		case SC1445x_AE_LINE_TYPE_NATIVE_DECT_WIDE:
			return 1 ;

		default:
			return 0 ;	/* whatever */
	}
}
#endif

#if defined( MEASURE_C_DELAY )
static unsigned short measure_c_delay_pos = 0 ;
#  define MEASURE_C_DELAY_BUF_SIZE \
			MEASURE_C_DELAY_FRAME_SIZE * MEASURE_C_DELAY_LOG_SIZE
static unsigned char measure_c_delay_buf1[MEASURE_C_DELAY_BUF_SIZE] ;
static unsigned char measure_c_delay_buf2[MEASURE_C_DELAY_BUF_SIZE] ;
static unsigned short measure_c_delay_measuring = 0 ;
#endif

#ifdef iLBC_LOG
static unsigned char* iLBC_capture_pos1 ;
static unsigned char* iLBC_capture_pos2 ;
static unsigned char* iLBC_capture_pos3 ;

static unsigned char* iLBC_capture_buf1 ;
static unsigned char* iLBC_capture_buf2 ;
static unsigned char* iLBC_capture_buf3 ;

static unsigned char* iLBC_capture_end1 ;
static unsigned char* iLBC_capture_end2 ;
static unsigned char* iLBC_capture_end3 ;
#endif

#ifdef PCM_LOG
static unsigned char* fax_pcm_playback_pos ;
static unsigned char* fax_pcm_playback_buf ;
static unsigned char* fax_pcm_playback_end ;
static unsigned char* fax_pcm_capture_pos ;
static unsigned char* fax_pcm_capture_buf ;
static unsigned char* fax_pcm_capture_end ;
#endif

#ifdef FXO_PCM_LOG
static unsigned char* fxo_pcm_playback_pos ;
static unsigned char* fxo_pcm_playback_buf ;
static unsigned char* fxo_pcm_playback_end ;
static unsigned char  fxo_pcm_playback_done = 0;
static unsigned char* fxo_pcm_capture_pos ;
static unsigned char* fxo_pcm_capture_buf ;
static unsigned char* fxo_pcm_capture_end ;
static unsigned char  fxo_pcm_capture_done = 0;
#endif

#ifdef FXO_PLAY_PRERECORDED_PCM
//SEND RECORDED CAS TONES
static unsigned char* fxo_cas_pos ;

static unsigned char* fxo_cas_end ;
#endif

#if defined( VOIP_PLAYBACK_LOG )
static unsigned char* voip_playback_log_pos[SC1445x_AE_MAX_AUDIO_CHANNELS] ;
static unsigned char* voip_playback_log_buf[SC1445x_AE_MAX_AUDIO_CHANNELS] ;
static unsigned char* voip_playback_log_end[SC1445x_AE_MAX_AUDIO_CHANNELS] ;
static volatile short* voip_playback_log_enable[SC1445x_AE_MAX_AUDIO_CHANNELS] =
{
	(void*)0x1b060,
	(void*)0x1b062,
	(void*)0x1b064,
#  if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(void*)0x1b066
#  endif
} ;
#endif

#if defined( PCM_PLAYBACK_LOG )
static unsigned char* pcm_playback_log_pos[SC1445x_AE_MAX_AUDIO_CHANNELS] ;
static unsigned char* pcm_playback_log_buf[SC1445x_AE_MAX_AUDIO_CHANNELS] ;
static unsigned char* pcm_playback_log_end[SC1445x_AE_MAX_AUDIO_CHANNELS] ;
static volatile short pcm_playback_log_enable[SC1445x_AE_MAX_AUDIO_CHANNELS] ;
static const unsigned char*
		pcm_playback_src_buf[SC1445x_AE_MAX_AUDIO_CHANNELS] = {
			(void*)0x10140,//0x18f20,
			(void*)0x19ee0,
			(void*)0x1aea0,
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
			(void*)0x1c120
#endif
		} ;
#endif

/* allocate memory for audio engine state data -- all fields are zeroed */
/* if it returns non-zero (indicating an error), the caller can safely invoke */
/* sc1445x_internal_free_mem() to free any allocated memory */
static short sc1445x_internal_alloc_mem( sc1445x_ae_state* ae_state,
				unsigned short nspks, unsigned short nmics,
				unsigned short naudiochannels )
{
	int sz ;
#if defined( VOIP_PLAYBACK_LOG ) || defined( PCM_PLAYBACK_LOG )
	int i ;
#endif

	/* this is an internal function, we don't check args */

	/* fill with zeroes */
	memset( ae_state, 0, sizeof( *ae_state ) ) ;

	/* fill known fields and allocate memory appropriately */
	INIT_CONST_FIELD( unsigned short, ae_state, spk_count, nspks ) ;
	if( nspks ) {
		sz = nspks * sizeof( sc1445x_ae_spk_state ) ;
		ae_state->spks = (sc1445x_ae_spk_state*)MALLOC( sz ) ;
		if( !ae_state->spks ) {
			PRINT( sc1445x_ae_nomem_error, __FUNCTION__,
								"spk states" ) ;
			return SC1445x_AE_ERR_NO_MEM ;
		}
		memset( ae_state->spks, 0, sz ) ;
	}
	INIT_CONST_FIELD( unsigned short, ae_state, mic_count, nmics ) ;
	if( nmics ) {
		sz = nmics * sizeof( sc1445x_ae_mic_state ) ;
		ae_state->mics = (sc1445x_ae_mic_state*)MALLOC( sz ) ;
		if( !ae_state->mics ) {
			PRINT( sc1445x_ae_nomem_error, __FUNCTION__,
								"mic states" ) ;
			return SC1445x_AE_ERR_NO_MEM ;
		}
		memset( ae_state->mics, 0, sz ) ;
	}
	INIT_CONST_FIELD( unsigned short, ae_state, audio_channels_count,
							naudiochannels ) ;
	if( naudiochannels ) {
		sz = naudiochannels * sizeof( sc1445x_ae_channel_state) ;
		ae_state->audio_channels = (sc1445x_ae_channel_state*)
								MALLOC( sz ) ;
		if( !ae_state->audio_channels ) {
			PRINT(  sc1445x_ae_nomem_error, __FUNCTION__,
						"audio channels states" ) ;
			return SC1445x_AE_ERR_NO_MEM ;
		}
		memset( ae_state->audio_channels, 0, sz ) ;
		for( sz = 0 ;  sz < naudiochannels ;  ++sz ) {
			ae_state->audio_channels[sz].my_ae_state = ae_state ;
#if defined( SC1445x_AE_USE_FADE_IN_TIMER )
			init_timer( &ae_state->audio_channels[sz].
							fade_in_timer ) ;
#endif
			init_waitqueue_head( &ae_state->audio_channels[sz].
								releaseq ) ;
		}

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
		sz = naudiochannels * sizeof( sc1445x_ae_xmatrix_entry ) ;
		ae_state->xmatrix.voip_channels = MALLOC( sz ) ;
		if( !ae_state->xmatrix.voip_channels ) {
			PRINT(  sc1445x_ae_nomem_error, __FUNCTION__,
					"xmatrix VOIP channel entries" ) ;
			return SC1445x_AE_ERR_NO_MEM ;
		}
		memset( ae_state->xmatrix.voip_channels, 0, sz ) ;

		sz = SC1445x_AE_AUDIO_STREAMS_COUNT *
					sizeof( sc1445x_ae_xmatrix_entry ) ;
		ae_state->xmatrix.audio_streams = MALLOC( sz ) ;
		if( !ae_state->xmatrix.audio_streams ) {
			PRINT(  sc1445x_ae_nomem_error, __FUNCTION__,
					"xmatrix audio streams entries" ) ;
			return SC1445x_AE_ERR_NO_MEM ;
		}
		memset( ae_state->xmatrix.audio_streams, 0, sz ) ;

		sz = SC1445x_AE_TOTAL_LINES_COUNT *
					sizeof( sc1445x_ae_line_type ) ;
		ae_state->line_types = MALLOC( sz ) ;
		if( !ae_state->line_types ) {
			PRINT(  sc1445x_ae_nomem_error, __FUNCTION__,
					"line types" ) ;
			return SC1445x_AE_ERR_NO_MEM ;
		}
		memset( ae_state->line_types, 0, sz ) ;
		ae_state->prev_line_types = MALLOC( sz ) ;
		if( !ae_state->prev_line_types ) {
			PRINT(  sc1445x_ae_nomem_error, __FUNCTION__,
					"prev line types" ) ;
			return SC1445x_AE_ERR_NO_MEM ;
		}
		memset( ae_state->prev_line_types, -1, sz ) ;

		sz = SC1445x_AE_TOTAL_LINES_COUNT * sizeof( short ) ;
		ae_state->line_mappings = MALLOC( sz ) ;
		if( !ae_state->line_mappings ) {
			PRINT(  sc1445x_ae_nomem_error, __FUNCTION__,
					"line mappings" ) ;
			return SC1445x_AE_ERR_NO_MEM ;
		}
		memset( ae_state->line_mappings, 0xff, sz ) ;

		ae_state->line2pcm_mappings = MALLOC( sz ) ;
		if( !ae_state->line2pcm_mappings ) {
			PRINT(  sc1445x_ae_nomem_error, __FUNCTION__,
					"line-to-pcm mappings" ) ;
			return SC1445x_AE_ERR_NO_MEM ;
		}
		memset( ae_state->line2pcm_mappings, 0xff, sz ) ;
#endif

		sz = SC1445x_AE_TONEGEN_MOD_COUNT *
					sizeof( sc1445x_ae_tonegen_state ) ;
		ae_state->tonegen = MALLOC( sz ) ;
		if( !ae_state->tonegen ) {
			PRINT(  sc1445x_ae_nomem_error, __FUNCTION__,
					"tone generation modules" ) ;
			return SC1445x_AE_ERR_NO_MEM ;
		}
		memset( ae_state->tonegen, 0, sz ) ;
		for( sz = 0 ;  sz < SC1445x_AE_TONEGEN_MOD_COUNT ;  ++sz ) {
			ae_state->tonegen[sz].my_ae_state = ae_state ;
			init_timer( &ae_state->tonegen[sz].timer ) ;
			init_timer( &ae_state->tonegen[sz].auto_stop_timer ) ;
#if defined( SC1445x_AE_USE_FAX_TIMER )
			init_timer( &ae_state->tonegen[sz].fax_timer ) ;
#endif
			init_timer( &ae_state->tonegen[sz].cb_timer ) ;
			ae_state->tonegen[sz].tx_volume = 0x6000 ;
			ae_state->tonegen[sz].rx_volume = 0x6000 ;
		}
	}

#if defined( SC1445x_AE_ATA_SUPPORT )
	sz = SC1445x_AE_AUDIO_STREAMS_COUNT * sizeof( sc1445x_ae_cid_state ) ;
	ae_state->caller_id = MALLOC( sz ) ;
	if( !ae_state->caller_id ) {
		PRINT(  sc1445x_ae_nomem_error, __FUNCTION__,
							"caller id structs" ) ;
		return SC1445x_AE_ERR_NO_MEM ;
	}
	memset( ae_state->caller_id, 0, sz ) ;
	for( sz = 0 ;  sz < SC1445x_AE_AUDIO_STREAMS_COUNT ;  ++sz ) {
		init_timer( &ae_state->caller_id[sz].fsk_init_timer ) ;
		init_timer( &ae_state->caller_id[sz].fsk_char_timer ) ;
		INIT_WORK( &ae_state->caller_id[sz].activate_fsk_decoder_work,
				sc1445x_internal_activate_fsk_decoder_wq,
						&ae_state->caller_id[sz] ) ;
		ae_state->caller_id[sz].my_ae_state = ae_state ;
	}
#endif

#ifdef PCM_LOG
	fax_pcm_playback_buf = MALLOC( 960000 ) ;
	fax_pcm_capture_buf = MALLOC( 960000 ) ;

	if( !fax_pcm_playback_buf  ||  !fax_pcm_capture_buf ) {
		PRINT( "COULD NOT ALLOCATE MEMORY FOR FAX PCM BUFFERS!\n" ) ;
		return SC1445x_AE_ERR_NO_MEM ;
	}

	memset(fax_pcm_playback_buf, 0xCA, 960000);
	memset(fax_pcm_capture_buf, 0xCA, 960000);

	PRINT( PRINT_LEVEL "FAX PCM LOGGING: playback @%p capture @%p\n",
			fax_pcm_playback_buf, fax_pcm_capture_buf ) ;
	fax_pcm_playback_pos = fax_pcm_playback_buf ;
	fax_pcm_playback_end = fax_pcm_playback_buf + 960000 ;
	fax_pcm_capture_pos = fax_pcm_capture_buf ;
	fax_pcm_capture_end = fax_pcm_capture_buf + 960000 ;
#endif

#ifdef FXO_PCM_LOG
	fxo_pcm_playback_buf = MALLOC( 960000 ) ;
	fxo_pcm_capture_buf = MALLOC( 960000 ) ;

	if( !fxo_pcm_playback_buf  ||  !fxo_pcm_capture_buf ) {
		PRINT( "COULD NOT ALLOCATE MEMORY FOR FXO PCM BUFFERS!\n" ) ;
		return SC1445x_AE_ERR_NO_MEM ;
	}

	memset(fxo_pcm_playback_buf, 0xCA, 960000);
	memset(fxo_pcm_capture_buf, 0xCA, 960000);

	PRINT( PRINT_LEVEL "FXO PCM LOGGING: PCM_IN @%p PCM_OUT @%p\n",
			fxo_pcm_playback_buf, fxo_pcm_capture_buf ) ;
	fxo_pcm_playback_pos = fxo_pcm_playback_buf ;
	fxo_pcm_playback_end = fxo_pcm_playback_buf + 960000 ;
	fxo_pcm_capture_pos = fxo_pcm_capture_buf ;
	fxo_pcm_capture_end = fxo_pcm_capture_buf + 960000 ;
#endif	

#ifdef iLBC_LOG
#define CAPTURE_SIZE 300000
	iLBC_capture_buf1 = MALLOC( CAPTURE_SIZE ) ;
	iLBC_capture_buf2 = MALLOC( CAPTURE_SIZE ) ;
	iLBC_capture_buf3 = MALLOC( CAPTURE_SIZE ) ;

	if( !iLBC_capture_buf1  ||  !iLBC_capture_buf2 || !iLBC_capture_buf3 ) {
		PRINT( "COULD NOT ALLOCATE MEMORY FOR iLBC LOGGING!\n" ) ;
		return SC1445x_AE_ERR_NO_MEM ;
	}

	memset(iLBC_capture_buf1, 0xBA, CAPTURE_SIZE);
	memset(iLBC_capture_buf2, 0xCA, CAPTURE_SIZE);
	memset(iLBC_capture_buf3, 0xDA, CAPTURE_SIZE);

	PRINT( PRINT_LEVEL "iLBC LOGGING: stream1: @%p stream2: @%p stream3: @%p\n",
			iLBC_capture_buf1, iLBC_capture_buf2, iLBC_capture_buf3 ) ;
	iLBC_capture_pos1 = iLBC_capture_buf1 ;
	iLBC_capture_pos2 = iLBC_capture_buf2 ;
	iLBC_capture_pos3 = iLBC_capture_buf3 ;
	iLBC_capture_end1 = iLBC_capture_buf1 + CAPTURE_SIZE ;
	iLBC_capture_end2 = iLBC_capture_buf2 + CAPTURE_SIZE ;
	iLBC_capture_end3 = iLBC_capture_buf3 + CAPTURE_SIZE ;
#endif

#if defined( VOIP_PLAYBACK_LOG )

#  define VOIP_PLAYBACK_LOG_BUF_SZ	( 100 * 10 * 82  +  82 )
#  define VOIP_PLAYBACK_LOG_TOTAL_SZ	\
		( SC1445x_AE_MAX_AUDIO_CHANNELS * VOIP_PLAYBACK_LOG_BUF_SZ )

	/* allocate all buffers in one chunk (enough for 10secs) */
	voip_playback_log_buf[0] = MALLOC( VOIP_PLAYBACK_LOG_TOTAL_SZ ) ;
	if( !voip_playback_log_buf[0] ) {
		PRINT( "Could not allocate memory for VoIP logging!\n" ) ;
		return SC1445x_AE_ERR_NO_MEM ;
	}

	memset( voip_playback_log_buf[0], 0xca, VOIP_PLAYBACK_LOG_TOTAL_SZ ) ;

	PRINT( PRINT_LEVEL "\tVoIP playback logging:\n" ) ;
	PRINT( PRINT_LEVEL "\t\tchannel 0 @%p\n", voip_playback_log_buf[0] ) ;
	voip_playback_log_pos[0] = voip_playback_log_buf[0] ;
	voip_playback_log_end[0] =
			voip_playback_log_buf[0] + VOIP_PLAYBACK_LOG_BUF_SZ ;
	for( i = 1 ;  i < SC1445x_AE_MAX_AUDIO_CHANNELS ;  ++i ) {
		voip_playback_log_buf[i] =
			voip_playback_log_buf[i - 1] + VOIP_PLAYBACK_LOG_BUF_SZ;
		voip_playback_log_pos[i] = voip_playback_log_buf[i] ;
		voip_playback_log_end[i] =
			voip_playback_log_buf[i] + VOIP_PLAYBACK_LOG_BUF_SZ ;
		/* leave some space for overruns */
		voip_playback_log_end[i] -= 82 ;

		PRINT( PRINT_LEVEL "\t\tchannel %d @%p\n",
						i, voip_playback_log_buf[i] ) ;
	}

#endif

#if defined( PCM_PLAYBACK_LOG )

#  define PCM_PLAYBACK_LOG_BUF_SZ	( 100 * 10 * 160  +  160 )
#  define PCM_PLAYBACK_LOG_TOTAL_SZ	\
		( SC1445x_AE_MAX_AUDIO_CHANNELS * PCM_PLAYBACK_LOG_BUF_SZ )

	/* allocate all buffers in one chunk (enough for 10secs) */
	pcm_playback_log_buf[0] = MALLOC( PCM_PLAYBACK_LOG_TOTAL_SZ ) ;
	if( !pcm_playback_log_buf[0] ) {
		PRINT( "Could not allocate memory for VoIP logging!\n" ) ;
		return SC1445x_AE_ERR_NO_MEM ;
	}

	memset( pcm_playback_log_buf[0], 0xca, PCM_PLAYBACK_LOG_TOTAL_SZ ) ;

	PRINT( PRINT_LEVEL "\tVoIP playback logging:\n" ) ;
	PRINT( PRINT_LEVEL "\t\tchannel 0 @%p\n", pcm_playback_log_buf[0] ) ;
	pcm_playback_log_pos[0] = pcm_playback_log_buf[0] ;
	pcm_playback_log_end[0] =
			pcm_playback_log_buf[0] + PCM_PLAYBACK_LOG_BUF_SZ ;
	for( i = 1 ;  i < SC1445x_AE_MAX_AUDIO_CHANNELS ;  ++i ) {
		pcm_playback_log_buf[i] =
			pcm_playback_log_buf[i - 1] + PCM_PLAYBACK_LOG_BUF_SZ ;
		pcm_playback_log_pos[i] = pcm_playback_log_buf[i] ;
		pcm_playback_log_end[i] =
			pcm_playback_log_buf[i] + PCM_PLAYBACK_LOG_BUF_SZ ;
		/* leave some space for overruns */
		pcm_playback_log_end[i] -= 160 ;

		PRINT( PRINT_LEVEL "\t\tchannel %d @%p\n",
						i, pcm_playback_log_buf[i] ) ;
	}

#endif

	init_waitqueue_head( &ae_state->fadeoutq ) ;

	INIT_WORK( &ae_state->vmic_gain_work, sc1445x_internal_vmic_gain_wq,
								ae_state ) ;
	INIT_WORK( &ae_state->release_channel_work,
			sc1445x_internal_release_channel_wq, ae_state ) ;

	return SC1445x_AE_OK ;
}


/* free tone sequence struct */
static
void sc1445x_internal_free_tone_seq( sc1445x_ae_tonegen_state* tonegen )
{
	/* this is an internal function, we don't check args */

	DPRINT( "entering %s (%p)\n", __FUNCTION__, tonegen->tone_seq_start ) ;

	if( tonegen->tone_seq_start ) {
		sc1445x_ae_tone4_seq_container* tsc = tonegen->tone_seq_start ;

		tonegen->tone_seq_curr = NULL ;
		while( tsc ) {
			sc1445x_ae_tone4_seq_container* next = tsc->next ;

			/* be safe */
			tonegen->tone_seq_start = next ;

			FREE( tsc ) ;
			tsc = next ;
		}
	}

	/* tonegen->tone_seq_start is NULL here */
	tonegen->repeat_tone_seq = SC1445x_AE_TONE_SEQ_NO_REPEAT ;
}


/* free memory allocated for audio engine state data */
static
short sc1445x_internal_free_mem( sc1445x_ae_state* ae_state )
{
	/* this is an internal function, we don't check args */

	int i ;

	FREE( ae_state->ap.data ) ;

#if defined( PCM_PLAYBACK_LOG )
	FREE( pcm_playback_log_buf[0] ) ;
	memset( pcm_playback_log_buf, 0, sizeof( pcm_playback_log_buf ) ) ;
#endif

#if defined( VOIP_PLAYBACK_LOG )
	FREE( voip_playback_log_buf[0] ) ;
	memset( voip_playback_log_buf, 0, sizeof( voip_playback_log_buf ) ) ;
#endif

#if defined( SC1445x_AE_ATA_SUPPORT )
	if( ae_state->caller_id )
		FREE( ae_state->caller_id ) ;
#endif

	for( i = 0 ;  i < SC1445x_AE_TONEGEN_MOD_COUNT ;  ++i )
		sc1445x_internal_free_tone_seq( &ae_state->tonegen[i] ) ;
	FREE( ae_state->tonegen ) ;

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	if( ae_state->line_mappings )
		FREE( ae_state->line_mappings ) ;
	if( ae_state->line_types )
		FREE( ae_state->line_types ) ;
	if( ae_state->prev_line_types )
		FREE( ae_state->prev_line_types ) ;
	if( ae_state->xmatrix.audio_streams )
		FREE( ae_state->xmatrix.audio_streams ) ;
	if( ae_state->xmatrix.voip_channels )
		FREE( ae_state->xmatrix.voip_channels ) ;
#endif
	if( ae_state->audio_channels )
		FREE( ae_state->audio_channels ) ;
	if( ae_state->mics )
		FREE( ae_state->mics ) ;
	if( ae_state->spks )
		FREE( ae_state->spks ) ;

	/* fill with zeroes */
	memset( ae_state, 0, sizeof( *ae_state ) ) ;

	return SC1445x_AE_OK ;
}


/* check if the DSP is ready to receive another command */
/* (actually checks if the command has been cleared, presumably by the DSP */
static inline
short sc1445x_internal_can_send_dsp_cmd( void )
{
	return  0 == *dsp1_cmd ; 
}


#if defined( TRACE_DSP_CMD )

#  define DSP_CMD_TRACE_SIZE	64

static unsigned short dsp_cmd_trace[DSP_CMD_TRACE_SIZE] ;
static unsigned short dsp_cmd_arg_trace[DSP_CMD_TRACE_SIZE] ;
static unsigned long dsp_cmd_time_trace[DSP_CMD_TRACE_SIZE] ;
static unsigned short dsp_cmd_trace_pos = 0 ;

void sc1445x_private_dump_dsp_cmd_trace( void )
{
	int i, j ;

	PRINT( "\nDSP cmd trace:\n" ) ;
	for( i = 0, j = dsp_cmd_trace_pos ;  i < DSP_CMD_TRACE_SIZE ;  ++i ) {
		PRINT( "time=%lu cmd=%04x arg=%04x\n", dsp_cmd_time_trace[j],
				dsp_cmd_trace[j], dsp_cmd_arg_trace[j] ) ;
		if( ++j == DSP_CMD_TRACE_SIZE )
			j = 0 ;
	}
}

#endif

#if defined( DEBUG_SEND_DSP_CMD_STALL ) || defined( TRACE_AUDIOENGINE )
static void sc1445x_internal_dump_traces( const char* loc, unsigned short cmd,
							unsigned short arg )
{
	extern void sc1445x_private_dump_ioctl_trace( void ) ;

	unsigned short* p = (void*)0x13d90 ;

	PRINT( "\n\nSTUCK IN %s\n", loc ) ;
	PRINT( "prev_cmd=%04x prev_val=%04x\n",
			*dsp1_cmd, *dsp1_cmd_val ) ;
	PRINT( "next_cmd=%04x next_val=%04x\n",
			cmd, arg ) ;
	PRINT( "\n" ) ;
	PRINT( "     DSP1_CTRL_REG=%04x\n", DSP1_CTRL_REG ) ;
	PRINT( "       DSP1_PC_REG=%04x\n", DSP1_PC_REG ) ;
	PRINT( "      DSP1_INT_REG=%04x\n", DSP1_INT_REG ) ;
	PRINT( " DSP1_INT_MASK_REG=%04x\n",
			DSP1_INT_MASK_REG ) ;
	PRINT( "DSP1_INT_PRIO1_REG=%04x\n",
			DSP1_INT_PRIO1_REG ) ;
	PRINT( "DSP1_INT_PRIO2_REG=%04x\n",
			DSP1_INT_PRIO2_REG ) ;
	PRINT( "\n" ) ;
	PRINT( "     DSP2_CTRL_REG=%04x\n", DSP2_CTRL_REG ) ;
	PRINT( "       DSP2_PC_REG=%04x\n", DSP2_PC_REG ) ;
	PRINT( "      DSP2_INT_REG=%04x\n", DSP2_INT_REG ) ;
	PRINT( " DSP2_INT_MASK_REG=%04x\n",
			DSP2_INT_MASK_REG ) ;
	PRINT( "DSP2_INT_PRIO1_REG=%04x\n",
			DSP2_INT_PRIO1_REG ) ;
	PRINT( "DSP2_INT_PRIO2_REG=%04x\n",
			DSP2_INT_PRIO2_REG ) ;
	PRINT( "\n" ) ;
	PRINT( "0x13d90: %04x %04x %04x\n", p[0], p[1], p[2] ) ;
	PRINT( "\n\n" ) ;
#  if defined( TRACE_DSP_CMD )
	sc1445x_private_dump_dsp_cmd_trace() ;
	PRINT( "\n\n" ) ;
#  endif
	sc1445x_private_dump_ioctl_trace() ;
	PRINT( "\n\n" ) ;
	sc1445x_private_dump_audioengine_trace() ;
	PRINT( "\n\n" ) ;
}
#endif

/* send a command to a DSP */
static
void sc1445x_internal_send_dsp_cmd( unsigned short cmd, unsigned short arg )
{
#if defined( DEBUG_SEND_DSP_CMD_STALL )
	extern void sc1445x_private_dump_ioctl_trace( void ) ;
	int i = 0 ;
#endif
#if defined( TRACE_DSP_CMD )
	unsigned long flags ;
#endif
#if !defined( SC1445x_AE_NATIVE_DECT_SUPPORT )
	static int in_sync_with_dip = 0 ;

	if( unlikely( !in_sync_with_dip && !( DIP_CTRL_REG & URST ) ) ) {
		in_sync_with_dip = 1 ;

		/* wait for DIP A_NORM instruction */
		SetWord( DSP_MAIN_CTRL_REG, 0x02AF ) ;
	}
#endif

	/* make sure last command has been read */
	while( !sc1445x_internal_can_send_dsp_cmd() ) {
#if defined( DEBUG_SEND_DSP_CMD_STALL )
		if( ++i == 100000 ) {
			sc1445x_internal_dump_traces( __FUNCTION__, cmd, arg ) ;
		}
#endif
	}

#if defined( DUMP_DSP_COMMANDS )
	PRINT( PRINT_LEVEL "DSP CMD:%04x  VAL:%04x\n", cmd, arg ) ;
#endif
	*dsp1_cmd_val = arg ;
	*dsp1_cmd = cmd ;

#if defined( TRACE_DSP_CMD )
	if( 0 == DSP1_CTRL_REG )
		PRINT( "%s: DSP1 is dead! cmd=%04x arg=%04x\n", __FUNCTION__,
								cmd, arg ) ;

	local_irq_save( flags ) ;
	dsp_cmd_trace[dsp_cmd_trace_pos] = cmd ;
	dsp_cmd_arg_trace[dsp_cmd_trace_pos] = arg ;
	dsp_cmd_time_trace[dsp_cmd_trace_pos] = jiffies ;
	if( DSP_CMD_TRACE_SIZE == ++dsp_cmd_trace_pos )
		dsp_cmd_trace_pos = 0 ;
	local_irq_restore( flags ) ;
#endif
}


/* clear DSP1 data memory; len in 16-bit words */
static inline
void sc1445x_internal_clear_dsp1_dm( unsigned short base, unsigned short len )
{
	unsigned short* p = (unsigned short*)(0x10000  +  2 * base) ;

	memset( p, 0, len << 1 ) ;
}

/* clear DSP2 data memory; len in 16-bit words */
static inline
void sc1445x_internal_clear_dsp2_dm( unsigned short base, unsigned short len )
{
	unsigned short* p = (unsigned short*)(0x18000  +  2 * base) ;

	memset( p, 0, len << 1 ) ;
}


#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
/* copy scarce data to DSP1 memory, channel independent */
static void sc1445x_internal_copy2dsp1_channel_indep( unsigned short dst_base,
					const unsigned short src_offsets[],
					const unsigned short src_values[],
					unsigned short values_cnt )
{
	unsigned short* mem = (unsigned short*)(0x10000  +  2 * dst_base) ;
	unsigned short i ;

	for( i = 0 ;  i < values_cnt ;  ++i )
		mem[ src_offsets[i] ] = src_values[i] ;
}
#endif

/* copy scarce data to DSP2 memory, channel independent */
static void sc1445x_internal_copy2dsp2_channel_indep( unsigned short dst_base,
					const unsigned short src_offsets[],
					const unsigned short src_values[],
					unsigned short values_cnt )
{
	unsigned short* mem = (unsigned short*)(0x18000  +  2 * dst_base) ;
	unsigned short i ;

	for( i = 0 ;  i < values_cnt ;  ++i ) {
		mem[ src_offsets[i] ] = src_values[i] ;
		DPRINT( PRINT_LEVEL "[%p + 2*%04x] <- %04x\n",
				mem, src_offsets[i], src_values[i] ) ;
	}
}


/* copy scarce data to DSP memory, channel dependent */
static void sc1445x_internal_copy2dsp_channel_dep( unsigned short dst_base,
					const unsigned short src_offsets[],
					const unsigned short src_values[],
			unsigned short values_cnt, unsigned short channel )
{
	unsigned short* mem = (unsigned short*)(0x18000  +  2 * dst_base ) ;
	unsigned short i ;

	for( i = 0 ;  i < values_cnt ;  ++i )
		mem[ src_offsets[i] ] = src_values[i  +  channel * values_cnt] ;
}


/* initialize PLC memory */
static void sc1445x_internal_init_plc_mem( unsigned short base )
{
	unsigned short* mem = (unsigned short*)( 0x18000  +  2 * base ) ;

	memset( mem, 0, 683 * 2 ) ;

	mem[5] = base + 332 ; //in plc code: pitchbufend = &pitchbuf[HISTORYLEN];
}


/* return non-zero if the codec is wideband */
static short sc1445x_internal_is_wideband_codec( sc1445x_ae_codec_type ctype )
{
	switch( ctype ) {
		case SC1445x_AE_CODEC_G722:
		case SC1445x_AE_CODEC_G722_MODE2:
		case SC1445x_AE_CODEC_G722_MODE3:
		case SC1445x_AE_CODEC_G711_1_ALAW_WB80:
		case SC1445x_AE_CODEC_G711_1_ALAW_WB96:
		case SC1445x_AE_CODEC_G711_1_ULAW_WB80:
		case SC1445x_AE_CODEC_G711_1_ULAW_WB96:
			return 1 ;

		default:
			return 0 ;
	}
}


/* see if there are any active wideband channels */
static
short sc1445x_internal_have_wide_channels( const sc1445x_ae_state* ae_state )
{
	/* this is an internal function, we don't check args */

	const unsigned short nchannels = ae_state->audio_channels_count ;
	unsigned short i ;

#if defined( SC1445x_AE_PHONE_DECT )
	if( SC1445x_AE_LINE_TYPE_NATIVE_DECT_WIDE ==
						ae_state->dect_headset_type )
		return 1 ;
#endif

	for( i = 0 ;  i < nchannels ;  ++i ) {
		if( ae_state->audio_channels[i].is_active &&
				sc1445x_internal_is_wideband_codec(
					ae_state->audio_channels[i].
							enc_codec.type )
				&&
				sc1445x_internal_is_wideband_codec(
					ae_state->audio_channels[i].
							dec_codec.type ) ) {
			return 1 ;
		}
	}

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	/* check, also, if a wideband tone is playing */
	for( i = 0 ;  i < SC1445x_AE_TONEGEN_MOD_COUNT ;  ++i ) {
		if( SC1445x_AE_TONEGEN_PLAYING == ae_state->tonegen[i].status
					&&  is_stream_wideband( ae_state, i ) ) {
			return 1 ;
		}
	}
#endif

	return 0 ;
}


/* see if there are any active channels */
short sc1445x_private_have_active_channels( const sc1445x_ae_state* ae_state )
{
	/* this is an internal function, we don't check args */

	const unsigned short nchannels = ae_state->audio_channels_count ;
	unsigned short i ;

	for( i = 0 ;  i < nchannels ;  ++i ) {
		if( ae_state->audio_channels[i].is_active ) {
			return 1 ;
		}
	}

	/* check, also, if a tone is playing */
	for( i = 0 ;  i < SC1445x_AE_TONEGEN_MOD_COUNT ;  ++i ) {
		if( SC1445x_AE_TONEGEN_PLAYING == ae_state->tonegen[i].status ) {
			return 1 ;
		}
	}

	return 0 ;
}


#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
inline
static void sc1445x_internal_enforce_phone_op_mode( sc1445x_ae_state* ae_state )
{
	unsigned short i, ramio_id = 0, pcm_id = 0, om ;

	if( ae_state->using_external_codec_mic
					||  ae_state->using_external_codec_spk )
		ramio_id = 0 ;  /* assume external CODEC is at PCM slot #0 */
	else {
		/* we basically can attach only to 1 PCM device for now */
		for( i = 0 ;  i < SC1445x_AE_PCM_DEV_COUNT ;  ++i ) {
			if( ae_state->pcm_dev[i].attached ) {
				ramio_id = ae_state->pcm_dev[i].slot ;
				break ;
			}
		}
	}

	/* we use only RAMIO #0, fow now */


	/* op mode passed to DSP should not be 0, or we won't have tones */
	if( ae_state->op_mode.val )
		om = ae_state->op_mode.val & 0xf ;
	else
		om = 1 ;

	om |= ( (ramio_id & 0xf) << 12 )  |  ( (pcm_id & 0xf) << 8 ) ;

	DPRINT( "%s: setting op mode to %04x\n", __FUNCTION__, om ) ;
	sc1445x_internal_send_dsp_cmd( 0x0026, om ) ;
}
#endif


/* helper function to turn on/off CLASSD
 * CLASSD is turned on/off according to the following expression
 * 	( dep_cond && (iface_mode uses CLASSD) ) ||  force_cond
 */
static inline
void sc1445x_internal_classd_autopower( const sc1445x_ae_state* ae_state,
					const sc1445x_ae_iface_mode iface_mode,
					short dep_cond, short force_cond )
{
	short vspk = vspk_from_iface_mode( iface_mode,
					ae_state->using_external_codec_spk ) ;

	if( ( dep_cond || force_cond ) && (vspk & 4) ) {  //thanks to Mikko
		/* turn on the external spk */
		sc1445x_internal_enable_output_to_classd_amp( ae_state, 1 ) ;
		DPRINT( "%s: CLASSD ON\n", __FUNCTION__ ) ;
	} else {
		/* turn off the external spk */
		sc1445x_internal_enable_output_to_classd_amp( ae_state, 0 ) ;
		DPRINT( "%s: CLASSD OFF\n", __FUNCTION__ ) ;
	}
}


/*
 * set up the codecs to use on audio channel ch
 * (dec_ch is valid only for G711.1 codec
 */
static short sc1445x_internal_set_codec( sc1445x_ae_state* ae_state,
				unsigned short ch, unsigned short dec_ch,
				sc1445x_ae_codec_type enc_ctype,
				sc1445x_ae_codec_type dec_ctype )
{
	/* this is an internal function, we don't check args */

	/*** ENCODING ***/
	const unsigned short encoder_buff_base_addr[4] =
						{ 0, 0x7e0, 0xfc0, 0x1900 } ;

	/////////////////////////G711 ENCODER///////////////////////////////
#define G711_ENC_TOTALLENGTH 3
#define G711_ENC_DATALENGTH 1
	const unsigned short g711_enc_base_addrs[4] =
				{ 0x51, 0x7e0+0x51, 0xfc0+0x51, 0x1900+0x51 } ;
	const unsigned short g711_enc_data[1] = { 0x18f0 } ;
	const unsigned short g711_enc_addr[1] = { 0x0000 } ;
	////////////////////////////////////////////////////////////////////

	/////////////////////////G711+VAD ENCODER///////////////////////////
#define G711VAD_ENC_TOTALLENGTH ( 3 + 1007 )
#define G711VAD_UNR_ENC_DATALENGTH 3
	const unsigned short g711vad_unr_enc_data[3] =
						{ 0x18f0, 0x0001, 0x0001 } ;
	const unsigned short g711vad_unr_enc_addr[3] =
						{ 0x0000, 0x0005, 0x0006 } ;

#define G711VAD_REL_ENC_DATALENGTH 2
	const unsigned short g711vad_rel_enc_data[8] =
			{ 0x4000, 0x0001,  0x40a0, 0x07e1,
			  0x4140, 0x0fc1,  0x41e0, 0x1901 } ;
	const unsigned short g711vad_rel_enc_addr[2] = { 0x0003, 0x0004 } ;
	////////////////////////////////////////////////////////////////////

	/////////////////////////G726 ENCODER///////////////////////////////
#define G726_ENC_TOTALLENGTH ( 29 + 12 )
	const unsigned short g726_enc_base_addrs[4] = 
			{ 0x29, 0x7e0 + 0x29, 0xfc0 + 0x29, 0x1900 + 0x29 } ;

#define G726_UNR_ENC_DATALENGTH 16
	const unsigned short g726vad_unr_enc_data[18] = {
		0x18f0, 0x0002, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020,
		0x0020, 0x0220, 0x0220, 0x0020, 0x82c0, 0x0004, 0x0008, 0x6000,
		0x0001, 0x0001
	} ;
	const unsigned short g726vad_unr_enc_addr[18] = {
		0x0000, 0x0001, 0x0003, 0x0006, 0x0007, 0x0008, 0x0009, 0x000a,
		0x000b, 0x0015, 0x0017, 0x001b, 0x001c, 0x001f, 0x0021, 0x0022,
		0x002d, 0x002e } ;

#define G726_REL_ENC_DATALENGTH 4
	const unsigned short g726_rel_enc_data[16]= {
		0x002b, 0x0001, 0x0001, 0x0028,
		0x7e0 +	0x002b, 0x7e1, 0x7e1, 0x7e1 + 0x0027,
		0xfc0 +	0x002b, 0xfc1, 0xfc1, 0xfc1 + 0x0027,
		0x1900 + 0x002b, 0x1901, 0x1901, 0x1901 + 0x0027
	} ;
	const unsigned short g726_rel_enc_addr[4] =
				{ 0x001d, 0x0025, 0x0026, 0x0027 } ;
	////////////////////////////////////////////////////////////////////

	/////////////////////////G726+VAD ENCODER///////////////////////////
#define G726VAD_ENC_TOTALLENGTH ( 29 + 12 + 1007 )

#define G726VAD_UNR_ENC_DATALENGTH 18
#define G726VAD_REL_ENC_DATALENGTH 3
	const unsigned short g726vad_rel_enc_data[12] = {
		0x002b, 0x4000, 0x0001,
		0x7e0+0x002b, 0x40a0, 0x07e1,
		0xfc0+0x002b, 0x4140, 0x0fc1,
		0x1900+0x002b, 0x41e0, 0x1901
	} ;
	const unsigned short g726vad_rel_enc_addr[3] =
						{ 0x001d, 0x002b, 0x002c } ;
	////////////////////////////////////////////////////////////////////

	/////////////////////////G729 ENCODER///////////////////////////////
#define G729_ENC_TOTALLENGTH 980 //??? 976

	const unsigned short g729_enc_base_addrs[4] =
			{ 0xc, 0x7e0 + 0xc, 0xfc0 + 0xc, 0x1900 + 0xc } ;

#define G729_UNR_ENC_DATALENGTH 1
	const unsigned short g729_unr_enc_data[1] = { 0x0001 } ;
	const unsigned short g729_unr_enc_addr[1] = { 0x0002 } ;

#define G729_REL_ENC_DATALENGTH 2
	const unsigned short g729_rel_enc_addr[2] = { 0x0000, 0x0001 } ;
	const unsigned short g729_rel_enc_data[8] =
		{ 0x4000, 0x0000,  0x40a0, 0x07e0,
		  0x4140, 0x0fc0,  0x41e0, 0x1900 } ;
	////////////////////////////////////////////////////////////////////

	/////////////////////////G722 ENCODER///////////////////////////////
#define G722_ENC_TOTALLENGTH 93+12

#if defined( CONFIG_SC14452 )
	/* use new base addrs  - move encoder structures after the decoder */
	/* ones and put in the encoder`s memory space the plc struct!!!*/
#  define G722_RELOCATION
#endif

#ifdef G722_RELOCATION
	const unsigned short g722_enc_base_addrs[4] =
			{ 0x51+0x4ac, 0x7e0+0x51+0x4ac,
			  0xfc0+0x51+0x4ac, 0x1900+0x51+0x4ac } ;
#else
#  define g722_enc_base_addrs g711_enc_base_addrs
#endif

#define G722_UNR_ENC_DATALENGTH 15

	const unsigned short g722_unr_enc_data[15] = {
		0x18f0, 0x18f1, 0x0020, 0x82f0, 0x830d, 0x834d, 0x835d, 0x8361,
		0x8371, 0x8375, 0x8395, 0x0006, 0x0002, 0x0008, 0x0000
	} ;
	const unsigned short g722_unr_enc_addr[15] = {
		0x0000, 0x0001, 0x0004, 0x0055, 0x0056, 0x0057, 0x0058, 0x0059,
		0x005a, 0x005b, 0x005c, 0x005f, 0x0060, 0x0061, 0x0062
	} ;

#define G722_REL_ENC_DATALENGTH 5
#ifdef G722_RELOCATION
	const unsigned short g722_rel_enc_data[20] = {
		0x51 + 2+0x4ac, 0x51 + 3+0x4ac, 1, 1, 80,
		0x7e0 + 0x51 + 2+0x4ac, 0x7e0 + 0x51 + 3+0x4ac,
						0x7e1, 0x7e1, 0x7e1 + 79,
		0xfc0 + 0x51 + 2+0x4ac, 0xfc0 + 0x51 + 3+0x4ac,
						0xfc1, 0xfc1, 0xfc1 + 79,
		0x1900 + 0x51 + 2+0x4ac, 0x1900 + 0x51 + 3+0x4ac,
						0x1901, 0x1901, 0x1901 + 79
	} ;
	const unsigned short g722_rel_enc_addr[5] =
				{ 0x005d, 0x005e, 0x0065, 0x0066, 0x0067 } ;
#else
	const unsigned short g722_rel_enc_data[20] = {
		0x51 + 2, 0x51 + 3, 1, 1, 80,
		0x7e0 + 0x51 + 2, 0x7e0 + 0x51 + 3, 0x7e1, 0x7e1, 0x7e1 + 79,
		0xfc0 + 0x51 + 2, 0xfc0 + 0x51 + 3, 0xfc1, 0xfc1, 0xfc1 + 79,
		0x1900 + 0x51 + 2, 0x1900 + 0x51 + 3, 0x1901, 0x1901, 0x1901 + 79,
	} ;
	const unsigned short g722_rel_enc_addr[5] =
				{ 0x005d, 0x005e, 0x0065, 0x0066, 0x0067 } ;
#endif

	volatile unsigned short* g722_dsp_mode = (void*)0x1B09A ;
	////////////////////////////////////////////////////////////////////

	/////////////////////////iLBC ENCODER///////////////////////////////
#define iLBC_ENC_TOTALLENGTH 348

	const unsigned short iLBC_enc_base_addrs[4] =
		{ 0x45e, 0x7e0 + 0x45e, 0xfc0 + 0x45e, 0x1900 + 0x45e } ;

#define iLBC_UNR_ENC_DATALENGTH 1
	const unsigned short iLBC_unr_enc_data[1] = { 0x0001 } ;
	const unsigned short iLBC_unr_enc_addr[1] = { 347 } ;

#define iLBC_REL_ENC_DATALENGTH 2
	const unsigned short iLBC_rel_enc_addr[2] = { 0x0000, 0x0001 } ;
	const unsigned short iLBC_rel_enc_data[8] = {
		0x05BA, 0x0001,  0x7e0+0x05BA, 0x7e0+0x0001,
		0xfc0+0x05BA, 0xfc0+0x0001,  0x1900+0x05BA, 0x1900+0x0001
	} ;
	unsigned short* iLBC_capture_cnt = (unsigned short*)(0x1b03a) ;
	unsigned short* iLBC_capture_tmp_header = (unsigned short*)(0x1b04a) ;
        ////////////////////////////////////////////////////////////////////

#if defined( CONFIG_SC14452_C )
	/////////////////////////G711-1 ENCODER/////////////////////////////
#  define G711_1_ENC_TOTALLENGTH 180
#  define G711_1_ENC_DATALENGTH 4
	const unsigned short g711_1_enc_base_addrs[4] =
				{ 0x3E, 0x7e0+0x3E, 0xfc0+0x3E, 0x1900+0x3E } ;
	const unsigned short g711_1_enc_data[4] =
				{ 0x0003, 0x0000, 0x0000, 0x0000 } ;
	const unsigned short g711_1_enc_addr[4] =
				{ 0x0003, 0x00ad, 0x00ae, 0x00af } ;
        ////////////////////////////////////////////////////////////////////
#endif  /* CONFIG_SC14452_C */
	
	
	/*** DECODING ***/
	const unsigned short decoder_buffin_base_addr[4] = 
					{ 0x444, 0xc24, 0x1404, 0x1d44 } ;

	/////////////////////////G711 DECODER///////////////////////////////
#define G711_DEC_TOTALLENGTH 3
#define G711_DEC_DATALENGTH 1
	const unsigned short g711_dec_base_addrs[4] = 
		{ 0x444 + 0x51, 0xc24 + 0x51, 0x1404 + 0x51, 0x1d44 + 0x51 } ;
	const unsigned short g711_dec_data[1] = { 0x18f0 } ;
	const unsigned short g711_dec_addr[1] = { 0x0000 } ;
	////////////////////////////////////////////////////////////////////

	/////////////////////////G711+CNG DECODER///////////////////////////
#define G711CNG_DEC_TOTALLENGTH 3+64

#define G711CNG_UNR_DEC_DATALENGTH 3
	const unsigned short g711cng_unr_dec_data[3] =
						{ 0x18f0, 0x0001, 0x0001 } ;
	const unsigned short g711cng_unr_dec_addr[3] =
						{ 0x0000, 0x0005, 0x0006 } ;

#define G711CNG_REL_DEC_DATALENGTH 2
	const unsigned short g711cng_rel_dec_data[8] =
			{ 0x0445, 0x0790,  0x0c25, 0x0f70,
			  0x1405, 0x1750,  0x1d45, 0x2090 } ;
	const unsigned short g711cng_rel_dec_addr[2] = { 0x0003, 0x0004 } ;
	////////////////////////////////////////////////////////////////////

	/////////////////////////G726 DECODER///////////////////////////////
#define G726_DEC_TOTALLENGTH 29+12

	const unsigned short g726_dec_base_addrs[4] =
		{ 0x444 + 0x29, 0xc24 + 0x29, 0x1404 + 0x29, 0x1d44 + 0x29 } ;

#define G726_UNR_DEC_DATALENGTH 15
	const unsigned short g726cng_unr_dec_data[17] = {
		0x8002, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020,
		0x0220, 0x0220, 0x0020, 0x82c0, 0x0004, 0x0008, 0x6000, 0x0001,
		0x0001
	} ;
	const unsigned short g726cng_unr_dec_addr[17] = {
		0x0001, 0x0003, 0x0006, 0x0007, 0x0008, 0x0009, 0x000a, 0x000b,
		0x0015, 0x0017, 0x001b, 0x001c, 0x001d, 0x001f, 0x0020, 0x002d,
		0x002e
	} ;

#define G726_REL_DEC_DATALENGTH 4
	const unsigned short g726_rel_dec_data[16] = {
		0x444 + 0x50, 0x444 + 1, 0x444 + 1, 0x444 + 0x28,
		0xc24 + 0x50, 0xc24 + 1, 0xc24 + 1, 0xc24 + 0x28,
		0x1404 + 0x50, 0x1404 + 1, 0x1404 + 1, 0x1404 + 0x28,
		0x1d44 + 0x50, 0x1d44 + 1, 0x1d44 + 1, 0x1d44 + 0x28
	} ;
	const unsigned short g726_rel_dec_addr[4]=
					{ 0x0000, 0x0023, 0x0024, 0x0025 } ;
	////////////////////////////////////////////////////////////////////

	/////////////////////////G726+VAD DECODER///////////////////////////
#define G726CNG_DEC_TOTALLENGTH 29+12+64

#define G726CNG_UNR_DEC_DATALENGTH 18
#define G726CNG_REL_DEC_DATALENGTH 3
	const unsigned short g726cng_rel_dec_data[12] = {
		0x444 + 0x50, 0x445, 0x0790,
		0xc24 + 0x50, 0x0c25, 0x0f70,
		0x1404 + 0x50, 0x1405, 0x1750,
		0x1d44 + 0x50, 0x1d45, 0x2090
	} ;
const unsigned short g726cng_rel_dec_addr[3] = { 0x0000, 0x002b, 0x002c } ;
	////////////////////////////////////////////////////////////////////

	/////////////////////////G729 DECODER///////////////////////////////
#define G729_DEC_TOTALLENGTH 832

	const unsigned short g729_dec_base_addrs[4] =
		{ 0x444 + 0xc, 0xc24 + 0xc, 0x1404 + 0xc, 0x1d44 + 0xc } ;

#define G729_UNR_DEC_DATALENGTH 1
	const unsigned short g729_unr_dec_data[1] = { 0x0001 } ;
	const unsigned short g729_unr_dec_addr[1] = { 0x0002 } ;

#define G729_REL_DEC_DATALENGTH 2
	const unsigned short g729_rel_dec_data[8] = 
			{ 0x0444, 0x0790,  0x0c24, 0x0f70,
			  0x1404, 0x1750,  0x1d44, 0x2090 } ;
	const unsigned short g729_rel_dec_addr[2] = { 0x0000, 0x0001 } ;
	////////////////////////////////////////////////////////////////////

	/////////////////////////G722 DECODER///////////////////////////////

#ifndef G722_RELOCATION
#  define G722_DEC_TOTALLENGTH 92+12
	/* use g711 base addrs */

#  define G722_UNR_DEC_DATALENGTH 12
	const unsigned short g722_unr_dec_data[12] = {
		0x0020, 0x834d, 0x835d, 0x8361, 0x8371, 0x8375, 0x83a1, 0x8395,
		0x0006, 0x0002, 0x0008, 0x0000};
	const unsigned short g722_unr_dec_addr[12] = {
		0x0004, 0x0055, 0x0056, 0x0057, 0x0058, 0x0059, 0x005a, 0x005b,
		0x005c, 0x005d, 0x005e, 0x005f
	} ;

#  define G722_REL_DEC_DATALENGTH 5
	const unsigned short g722_rel_dec_data[20] = {
		0x444 + 0xb7, 0x444 + 0xb8, 0x444 + 1, 0x444 + 1, 0x444 + 80,
		0xc24 + 0xb7, 0xc24 + 0xb8, 0xc24 + 1, 0xc24 + 1, 0xc24 + 80,
		0x1404 + 0xb7, 0x1404 + 0xb8, 0x1404 + 1, 0x1404 + 1,
								0x1404 + 80,
		0x1d44 + 0xb7, 0x1d44 + 0xb8, 0x1d44 + 1, 0x1d44 + 1,
								0x1d44 + 80
	} ;
	const unsigned short g722_rel_dec_addr[5] =
		{ 0x0000, 0x0001, 0x0062, 0x0063, 0x0064 } ;
#else
	#  define G722_DEC_TOTALLENGTH 92
	#  define G722_UNR_DEC_DATALENGTH 9
	const unsigned short g722_unr_dec_data[9] = {
		0x0020, 0x8, 0x834d, 0x835d, 0x8361, 0x8371, 0x8375, 0x83a1, 0x8395
		};
	const unsigned short g722_unr_dec_addr[9] = {
		0x0004, 0x20, 0x0055, 0x0056, 0x0057, 0x0058, 0x0059, 0x005a, 0x005b
		} ;
	#  define G722_PLC_TOTALLENGTH 866
	#  define G722_PLC_UNR_DATALENGTH 13
	const unsigned short g722_plc_unr_data[13] = {
		297, 289, 2, 5, 80, 32767, 32767, 32767, 1,
		10, 10, 170, 0x8200
		};
	const unsigned short g722_plc_unr_addr[13] = {
		0x0002, 0x0003, 0x0004, 0x02ef, 0x02f1, 0x0342, 0x034b, 0x034c, 0x0347,
		0x0348, 0x0349, 0x034a, 0x0360
		} ;		
	#  define G722_REL_PLC_DATALENGTH 1
	const unsigned short g722_rel_plc_data[4] = {
		0x444 + 0x51,
		0xc24 + 0x51,
		0x1404 + 0x51,
		0x1d44 + 0x51
	} ;
	const unsigned short g722_rel_plc_addr[1] =
		{865} ;
#endif
	//G722 ADDITIONAL DATA OPERATION MODES
	//Temporary mapping in RAM until next METAL LAYER.
	//Mode 2 g722 audio @56kbps	data  @8kbps - MAP TO DSP ADDRESS 0x055A - CR16 ADDRESS 0x10AB4 
	const unsigned short invqbtbl_mode2[64] = {
		-280,	-280,	-280,	-280,	-23352,	-23352,	-17560,	-17560,	-14120,	-14120,
		-11664,	-11664,	-9752,	-9752,	-8184,	-8184,	-6864,	-6864,	-5712,	-5712,
		-4696,	-4696,	-3784,	-3784,	-2960,	-2960,	-2208,	-2208,	-1520,	-1520,
		-880,	-880,	23352,	23352,	17560,	17560,	14120,	14120,	11664,	11664,
		9752,	9752,	8184,	8184,	6864,	6864,	5712,	5712,	4696,	4696,
		3784,	3784,	2960,	2960,	2208,	2208,	1520,	1520,	880,	880,
		280,	280,	-280,	-280
	};

	//Mode 3 g722 audio @48kbps	data  @16kbps - MAP TO DSP ADDRESS 0x059A - CR16 ADDRESS 0x10B34
	const unsigned short invqbtbl_mode3[64] = {
		0,	0,	0,	0,	-20456,	-20456,	-20456,	-20456,	-12896,	-12896,
		-12896,	-12896,	-8968,	-8968,	-8968,	-8968,	-6288,	-6288,	-6288,	-6288,
		-4240,	-4240,	-4240,	-4240,	-2584,	-2584,	-2584,	-2584,	-1200,	-1200,
		-1200,	-1200,	20456,	20456,	20456,	20456,	12896,	12896,	12896,	12896,
		8968,	8968,	8968,	8968,	6288,	6288,	6288,	6288,	4240,	4240,
		4240,	4240,	2584,	2584,	2584,	2584,	1200,	1200,	1200,	1200,
		0,	0,	0,	0
	};
	////////////////////////////////////////////////////////////////////

	/////////////////////////iLBC DECODER///////////////////////////////
#define iLBC_DEC_TOTALLENGTH 1017
	const unsigned short iLBC_dec_base_addrs[4] =
		{ 0x1a, 0x7e0+0x1a , 0xfc0 + 0x1a, 0x1900 + 0x1a } ;

#define iLBC_UNR_DEC_DATALENGTH 1
	const unsigned short iLBC_unr_dec_data[1] = { 0x0001 } ;
	const unsigned short iLBC_unr_dec_addr[1] = { 1016 } ;

#define iLBC_REL_DEC_DATALENGTH 2
	const unsigned short iLBC_rel_dec_data[8] = {
		0x0445, 0x06AA, 0x7e0+0x0445,0x7e0+0x06AA,
		0xfc0+0x0445,0xfc0+0x06AA,  0x1900+0x0445,0x1900+0x06AA} ;
	const unsigned short iLBC_rel_dec_addr[2] = { 0x0000, 0x0001 } ;
	unsigned short* iLBC_playback_cnt = (unsigned short*)(0x1b03a+8) ;
	unsigned short* iLBC_playback_tmp_status = (unsigned short*)(0x1B052) ;
	////////////////////////////////////////////////////////////////////

#if defined( CONFIG_SC14452_C )
	/////////////////////////G711-1 DECODER/////////////////////////////
#  define G711_1_DEC_TOTALLENGTH 1808
#  define G711_1_DEC_DATALENGTH 6
	const unsigned short g711_1_dec_base_addrs[4] =
			{ 0x3E, 0x7e0+0x3E, 0xfc0+0x3E, 0x1900+0x3E } ;
	const unsigned short g711_1_dec_data[6] =
			{ 0x0000, 0x0000, 0xFFFF, 0x0000, 0x0000, 0x0000 } ;
	const unsigned short g711_1_dec_addr[6] =
			{ 0x0420, 0x0704, 0x0709, 0x070A, 0x070B, 0x070D } ;
	////////////////////////////////////////////////////////////////////
#endif  /* CONFIG_SC14452_C */

	/* download encoding codec */
	sc1445x_ae_codec_settings* codec ;
	unsigned short cmd, cmd_val = 0 ;
	unsigned short dec_cmd, dec_cmd_val = 0, use_dec_cmd = 0 ;
	volatile unsigned short* mem = NULL ;
	sc1445x_ae_channel_state* acs ;

	cmd = ( ch < 3 ) ?  ( ch + 1 ) :  0x0025 ;
	dec_cmd = ( dec_ch < 3 ) ?  ( dec_ch + 1 ) :  0x0025 ;

	/* set up the encoding codec struct and download the appropriate code */
	acs = &ae_state->audio_channels[ch] ;
	codec = &acs->enc_codec ;
	INIT_CONST_FIELD( unsigned short, codec, type, enc_ctype ) ;
	INIT_CONST_FIELD( unsigned short, codec, sample_bits, 8 ) ;
	switch( enc_ctype ) {
		/* G711 flavors */
		case SC1445x_AE_CODEC_G711_ULAW:
		case SC1445x_AE_CODEC_G711_ALAW:
			codec->rate = 80 ;
			sc1445x_internal_clear_dsp2_dm(
					encoder_buff_base_addr[ch], 81 ) ;
			sc1445x_internal_clear_dsp2_dm( g711_enc_base_addrs[ch],
							G711_ENC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g711_enc_base_addrs[ch], g711_enc_addr,
					g711_enc_data, G711_ENC_DATALENGTH ) ;
			if( SC1445x_AE_CODEC_G711_ULAW == enc_ctype ) {
				mem = (unsigned short*)( 0x18000 +
						2 * g711_enc_base_addrs[ch] ) ;
				mem[1] = 0x8000 ;
				cmd_val |= 0x0100 ;
			}
			cmd_val |= 0x0010 ;

			break ;

		case SC1445x_AE_CODEC_G711_ULAW_VAD:
		case SC1445x_AE_CODEC_G711_ALAW_VAD:
			codec->rate = 80 ;
			sc1445x_internal_clear_dsp2_dm(
					encoder_buff_base_addr[ch], 81 ) ;
			sc1445x_internal_clear_dsp2_dm(
						g711_enc_base_addrs[ch],
						G711VAD_ENC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g711_enc_base_addrs[ch],
					g711vad_unr_enc_addr,
					g711vad_unr_enc_data,
					G711VAD_UNR_ENC_DATALENGTH ) ;
			sc1445x_internal_copy2dsp_channel_dep(
					g711_enc_base_addrs[ch],
					g711vad_rel_enc_addr,
					g711vad_rel_enc_data,
					G711VAD_REL_ENC_DATALENGTH, ch ) ;
			if( SC1445x_AE_CODEC_G711_ULAW_VAD == enc_ctype ) {
				mem = (unsigned short*)( 0x18000 +
						2 * g711_enc_base_addrs[ch] ) ;
				mem[1] = 0x8000 ;
				cmd_val |= 0x0100 ;
			}
			cmd_val |= 0x0020 ;

			break ;


		/* G726 flavors */
		case SC1445x_AE_CODEC_G726:
			codec->rate = 40 ;
			sc1445x_internal_clear_dsp2_dm(
					encoder_buff_base_addr[ch], 41 ) ;
			sc1445x_internal_clear_dsp2_dm(
						g726_enc_base_addrs[ch],
						G726_ENC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g726_enc_base_addrs[ch],
					g726vad_unr_enc_addr,
					g726vad_unr_enc_data,
					G726_UNR_ENC_DATALENGTH ) ;
			sc1445x_internal_copy2dsp_channel_dep(
					g726_enc_base_addrs[ch],
					g726_rel_enc_addr,
					g726_rel_enc_data,
					G726_REL_ENC_DATALENGTH, ch ) ;
			cmd_val |= 0x0030 ;

			break ;

		case SC1445x_AE_CODEC_G726_VAD:
			codec->rate = 40 ;
			sc1445x_internal_clear_dsp2_dm(
					encoder_buff_base_addr[ch], 41 ) ;
			sc1445x_internal_clear_dsp2_dm(
						g726_enc_base_addrs[ch],
						G726VAD_ENC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g726_enc_base_addrs[ch],
					g726vad_unr_enc_addr,
					g726vad_unr_enc_data,
					G726VAD_UNR_ENC_DATALENGTH ) ;
			sc1445x_internal_copy2dsp_channel_dep(
					g726_enc_base_addrs[ch],
					g726vad_rel_enc_addr,
					g726vad_rel_enc_data,
					G726VAD_REL_ENC_DATALENGTH, ch ) ;
			cmd_val |= 0x0040 ;

			break ;


		/* G729 flavors */
		case SC1445x_AE_CODEC_G729:
		case SC1445x_AE_CODEC_G729_VAD:
			codec->rate = 10 ;
			sc1445x_internal_clear_dsp2_dm(
					encoder_buff_base_addr[ch], 13 ) ;
			sc1445x_internal_clear_dsp2_dm( g729_enc_base_addrs[ch],
							G729_ENC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g729_enc_base_addrs[ch],
					g729_unr_enc_addr, g729_unr_enc_data,
						G729_UNR_ENC_DATALENGTH ) ;
			sc1445x_internal_copy2dsp_channel_dep(
					g729_enc_base_addrs[ch],
					g729_rel_enc_addr, g729_rel_enc_data,
						G729_REL_ENC_DATALENGTH, ch ) ;
			if( SC1445x_AE_CODEC_G729_VAD == enc_ctype ) {
				mem = (unsigned short*)( 0x18000 +
						2 * g729_enc_base_addrs[ch] ) ;
				mem[3] = 0x0001 ;
				cmd_val |= 0x0060 ;
			}
			else
				cmd_val |= 0x0050 ;

			break ;

		/* G722 (wideband) */
		case SC1445x_AE_CODEC_G722:
		case SC1445x_AE_CODEC_G722_MODE2:
		case SC1445x_AE_CODEC_G722_MODE3:
			*g722_dsp_mode &= ~( 3 << (2 * ch) ) ;
			if( SC1445x_AE_CODEC_G722_MODE3 == enc_ctype ) {
				codec->rate = 60 ;
				*g722_dsp_mode |= 3 << (2 * ch) ;
			} else if( SC1445x_AE_CODEC_G722_MODE2 == enc_ctype ) {
				codec->rate = 70 ;
				*g722_dsp_mode |= 2 << (2 * ch) ;
			} else {
				codec->rate = 80 ;
				*g722_dsp_mode |= 1 << (2 * ch) ;
			}

			/* CLEAR WHOLE ENC MEM */
			sc1445x_internal_clear_dsp2_dm(
					encoder_buff_base_addr[ch], 0x442 ) ;
			sc1445x_internal_clear_dsp2_dm( g722_enc_base_addrs[ch],
							G722_ENC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g722_enc_base_addrs[ch],
					g722_unr_enc_addr,
					g722_unr_enc_data,
					G722_UNR_ENC_DATALENGTH ) ;
			sc1445x_internal_copy2dsp_channel_dep(
					g722_enc_base_addrs[ch],
					g722_rel_enc_addr,
					g722_rel_enc_data,
					G722_REL_ENC_DATALENGTH, ch ) ;
			cmd_val |= 0x0070 ;

			break ;

		/* iLBC flavors */
		case SC1445x_AE_CODEC_iLBC_20ms:
		case SC1445x_AE_CODEC_iLBC_30ms:
			sc1445x_internal_clear_dsp2_dm(
					encoder_buff_base_addr[ch], 1+25 ) ;
			sc1445x_internal_clear_dsp2_dm( iLBC_enc_base_addrs[ch],
						iLBC_ENC_TOTALLENGTH+240+240 ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					iLBC_enc_base_addrs[ch],
					iLBC_unr_enc_addr, iLBC_unr_enc_data,
					iLBC_UNR_ENC_DATALENGTH ) ;
			sc1445x_internal_copy2dsp_channel_dep(
					iLBC_enc_base_addrs[ch],
					iLBC_rel_enc_addr, iLBC_rel_enc_data,
					iLBC_REL_ENC_DATALENGTH, ch ) ;
			mem = (unsigned short*)( 0x18000 +
						2 * iLBC_enc_base_addrs[ch] ) ;
			iLBC_capture_cnt[ch] = 0 ;
			iLBC_capture_tmp_header[ch] = 0 ;

			if( SC1445x_AE_CODEC_iLBC_20ms == enc_ctype ) {
				mem[2] = 20 ;
				codec->rate = 38 ;
				cmd_val |= 0x00b0 ;
				INIT_CONST_FIELD( unsigned short, acs,
							capture_cnt_lim, 2 ) ;
			} else {
				mem[2] = 30 ;
				codec->rate = 50 ;
				cmd_val |= 0x00c0 ;
				INIT_CONST_FIELD( unsigned short, acs,
							capture_cnt_lim, 3 ) ;
			}

#if defined( DEBUG_iLBC_WITH_GPIO )
			P1_15_MODE_REG = 0x300 ;
			//P1_SET_DATA_REG = 0x8000 ;
			//P1_RESET_DATA_REG = 0x8000 ;
			P1_DATA_REG &= ~0x8000 ;

			P1_02_MODE_REG = 0x300 ;
			//P1_SET_DATA_REG = 0x0004 ;
			//P1_RESET_DATA_REG = 0x0004 ;
			P1_DATA_REG &= ~0x0004 ;

			P2_04_MODE_REG = 0x300 ;
			//P2_SET_DATA_REG = 0x0010 ;
			//P2_RESET_DATA_REG = 0x0010 ;
			P2_DATA_REG &= ~0x0010 ;

			P1_13_MODE_REG = 0x300 ;
			//P1_SET_DATA_REG = 0x2000 ;
			//P1_RESET_DATA_REG = 0x2000 ;
			P1_DATA_REG &= ~0x2000 ;
#endif
			break ;

#if defined( CONFIG_SC14452_C )
		/* G711-1 flavors, narrowband (NB) / wideband (WB) */ 
		case SC1445x_AE_CODEC_G711_1_ALAW_NB64:
		case SC1445x_AE_CODEC_G711_1_ULAW_NB64:
			/* 40 Words per 10msec */
			codec->rate = 40 ;
			/* max words for 2 channels (10msec) */
			sc1445x_internal_clear_dsp2_dm(
					encoder_buff_base_addr[ch], 62 ) ;		
			sc1445x_internal_clear_dsp2_dm(
					g711_1_enc_base_addrs[ch],
					G711_1_ENC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g711_1_enc_base_addrs[ch],
					g711_1_enc_addr,
					g711_1_enc_data,
					G711_1_ENC_DATALENGTH ) ;
			mem = (unsigned short*)
				( 0x18000 + 2 * g711_1_enc_base_addrs[ch] ) ;
			if( SC1445x_AE_CODEC_G711_1_ALAW_NB64 == enc_ctype ) {
				/* Mode R1 (W(2-0):0), A-law (W-MSB:0) */
				mem[36] = 2 ;
			} else {
				/* Mode R1 (W(2-0):0), U-law (W-MSB:1) */
				mem[36] = 1 ;
				cmd_val |= 0x8000 ;
			}
			/* mode */
			mem[3] = 0 ;
			/* operating freq */
			mem[4] = 8000 ;
			/* nsamplesIn */
			mem[176] = 40 ;
			/* MODE_R1: bits, bytes number */
			mem[177] = 40 * 8 ;
			mem[178] = 40 ;
			/* Tables */
			mem[179]= 0x8da9 ;
			/* Encoder Select, Mode through Y field :d value */
			cmd_val |= 0x00d0 ;
			break ;

		case SC1445x_AE_CODEC_G711_1_ALAW_NB80:
		case SC1445x_AE_CODEC_G711_1_ALAW_WB80:
		case SC1445x_AE_CODEC_G711_1_ULAW_NB80:
		case SC1445x_AE_CODEC_G711_1_ULAW_WB80:
			/* 50 Words per 10msec */
			codec->rate = 50 ;
			/* max words for 2 channels (10msec) */
			sc1445x_internal_clear_dsp2_dm(
					encoder_buff_base_addr[ch], 62 ) ;
			sc1445x_internal_clear_dsp2_dm(
					g711_1_enc_base_addrs[ch],
					G711_1_ENC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g711_1_enc_base_addrs[ch],
					g711_1_enc_addr,
					g711_1_enc_data,
					G711_1_ENC_DATALENGTH ) ;
			mem = (unsigned short*)
				( 0x18000 + 2 * g711_1_enc_base_addrs[ch] ) ;
			if( SC1445x_AE_CODEC_G711_1_ALAW_NB80 == enc_ctype ) {
				/* Mode R2a (W(2-0):2), A-law (W-MSB:0) */
				mem[36] = 2 ;
				mem[3] = 1 ;
				mem[4] = 8000 ;
				cmd_val |= 0x2000 ;
			} else if( SC1445x_AE_CODEC_G711_1_ALAW_WB80
								== enc_ctype ) {
				/* Mode R2b (W(2-0):3), A-law (W-MSB:0) */
				mem[36] = 2 ;
				mem[3] = 2 ;
				mem[4] = 16000 ;
				cmd_val |= 0x3000 ;
			} else if( SC1445x_AE_CODEC_G711_1_ULAW_NB80
								== enc_ctype ) {
				/* Mode R2a (W(2-0):2), U-law (W-MSB:1) */
				mem[36] = 1 ;
				cmd_val |= 0x8000 ;
				mem[3] = 1 ;
				mem[4] = 8000 ;
				cmd_val |= 0x2000 ;
			} else /* if( SC1445x_AE_CODEC_G711_1_ULAW_WB80
			  				== enc_ctype )*/ {
				/* Mode R2b (W(2-0):3), U-law (W-MSB:1) */
				mem[36] = 1 ;
				cmd_val |= 0x8000 ;
				mem[3] = 2 ;
				mem[4] = 16000 ;
				cmd_val |= 0x3000 ;
			}
			/* nsamplesIn */
			mem[176] = ( 8000 == mem[4] ) ?  40 :  80 ;
			/* MODE_R1: bits, bytes number */
			mem[177] = 50 * 8 ;
			mem[178] = 50 ;
			/* Tables */
			mem[179]= 0x8da9 ;
			/* Encoder Select, Mode through Y field :d value */
			cmd_val |= 0x00d0 ;
			break ;

		case SC1445x_AE_CODEC_G711_1_ALAW_WB96:
		case SC1445x_AE_CODEC_G711_1_ULAW_WB96:
			/* 60 Words per 10msec */
			codec->rate = 60 ;
			sc1445x_internal_clear_dsp2_dm(
					encoder_buff_base_addr[ch], 62 ) ;
			sc1445x_internal_clear_dsp2_dm(
					g711_1_enc_base_addrs[ch],
					G711_1_ENC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g711_1_enc_base_addrs[ch],
					g711_1_enc_addr,
					g711_1_enc_data,
					G711_1_ENC_DATALENGTH ) ;
			mem = (unsigned short*)
				( 0x18000 + 2 * g711_1_enc_base_addrs[ch] ) ;
			if( SC1445x_AE_CODEC_G711_1_ALAW_WB96 == enc_ctype ) {
				/* Mode R3 (W(2-0):4), A-law (W-MSB:0) */
				mem[36] = 2 ;
				cmd_val |= 0x4000 ;
			} else {
				/* Mode R3 (W(2-0):4), U-law (W-MSB:1) */
				mem[36] = 1 ;
				cmd_val |= 0xc000 ;
			}
			mem[4] = 16000 ;
			/* nsamplesIn */
			mem[176] = 80 ;
			/* MODE_R1: bits,bytes number */
			mem[177] = 60 * 8 ;
			mem[178] = 60 ;
			/* Tables */
			mem[179]= 0x8da9 ;
			/* Encoder Select, Mode through Y field :d value */
			cmd_val |= 0x00d0 ;
			break ;
#endif  /* CONFIG_SC14452_C */
			
		default:
			PRINT( sc1445x_ae_codec_not_supported_error,
								enc_ctype ) ;
			return SC1445x_AE_ERR_CODEC_NOT_SUPPORTED ;
	}

	DPRINT( "Downloaded code for channel %d: encoding codec %d\n",
							ch, enc_ctype ) ;

	/* set up the decoding codec struct and download the appropriate code */
	codec = &acs->dec_codec ;
	INIT_CONST_FIELD( unsigned short, codec, type, dec_ctype ) ;
	INIT_CONST_FIELD( unsigned short, codec, sample_bits, 8 ) ;
	switch( dec_ctype ) {
		/* G711 flavors */
		case SC1445x_AE_CODEC_G711_ULAW:
		case SC1445x_AE_CODEC_G711_ALAW:
			codec->rate = 80 ;
			sc1445x_internal_clear_dsp2_dm(
					decoder_buffin_base_addr[ch], 81 ) ;
			sc1445x_internal_clear_dsp2_dm( g711_dec_base_addrs[ch],
							G711_DEC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g711_dec_base_addrs[ch], g711_dec_addr,
					g711_dec_data, G711_DEC_DATALENGTH ) ;
			sc1445x_internal_init_plc_mem(
					decoder_buffin_base_addr[ch] + 148 ) ;
			if( SC1445x_AE_CODEC_G711_ULAW == dec_ctype ) {
				mem = (unsigned short*)( 0x18000 +
						2 * g711_dec_base_addrs[ch] ) ;
				mem[1] = 0x8000 ;
				cmd_val |= 0x0200 ;
			}
			cmd_val |= 0x0001 ;

			break ;

		case SC1445x_AE_CODEC_G711_ULAW_VAD:
		case SC1445x_AE_CODEC_G711_ALAW_VAD:
			codec->rate = 80 ;
			sc1445x_internal_clear_dsp2_dm(
					decoder_buffin_base_addr[ch], 81 ) ;
			sc1445x_internal_clear_dsp2_dm(
						g711_dec_base_addrs[ch],
						G711CNG_DEC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g711_dec_base_addrs[ch],
					g711cng_unr_dec_addr,
					g711cng_unr_dec_data,
					G711CNG_UNR_DEC_DATALENGTH ) ;
			sc1445x_internal_copy2dsp_channel_dep(
					g711_dec_base_addrs[ch],
					g711cng_rel_dec_addr,
					g711cng_rel_dec_data,
					G711CNG_REL_DEC_DATALENGTH, ch ) ;
			sc1445x_internal_init_plc_mem(
					decoder_buffin_base_addr[ch] + 148 ) ;
			if( SC1445x_AE_CODEC_G711_ULAW_VAD == dec_ctype ) {
				mem = (unsigned short*)( 0x18000 +
						2 * g711_dec_base_addrs[ch] ) ;
				mem[1] = 0x8000 ;
				cmd_val |= 0x0200 ;
			}
			cmd_val |= 0x0002 ;

			break ;


		/* G726 flavors */
		case SC1445x_AE_CODEC_G726:
			codec->rate = 40 ;
			sc1445x_internal_clear_dsp2_dm(
					decoder_buffin_base_addr[ch], 41 ) ;
			sc1445x_internal_clear_dsp2_dm( g726_dec_base_addrs[ch],
							G726_DEC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g726_dec_base_addrs[ch],
					g726cng_unr_dec_addr,
					g726cng_unr_dec_data,
					G726_UNR_DEC_DATALENGTH ) ;
			sc1445x_internal_copy2dsp_channel_dep(
					g726_dec_base_addrs[ch],
					g726_rel_dec_addr,
					g726_rel_dec_data,
					G726_REL_DEC_DATALENGTH, ch ) ;
			sc1445x_internal_init_plc_mem(
					decoder_buffin_base_addr[ch] + 148 ) ;
			cmd_val |= 0x0003 ;

			break ;

		case SC1445x_AE_CODEC_G726_VAD:
			codec->rate = 40 ;
			sc1445x_internal_clear_dsp2_dm(
					decoder_buffin_base_addr[ch], 41 ) ;
			sc1445x_internal_clear_dsp2_dm( g726_dec_base_addrs[ch],
						G726CNG_DEC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g726_dec_base_addrs[ch],
					g726cng_unr_dec_addr,
					g726cng_unr_dec_data,
					G726CNG_UNR_DEC_DATALENGTH ) ;
			sc1445x_internal_copy2dsp_channel_dep(
					g726_dec_base_addrs[ch],
					g726cng_rel_dec_addr,
					g726cng_rel_dec_data,
					G726CNG_REL_DEC_DATALENGTH, ch ) ;
			sc1445x_internal_init_plc_mem(
					decoder_buffin_base_addr[ch] + 148 ) ;
			cmd_val |= 0x0004 ;

			break ;


		/* G729 flavors */
		case SC1445x_AE_CODEC_G729:
		case SC1445x_AE_CODEC_G729_VAD:
			codec->rate = 10 ;
			sc1445x_internal_clear_dsp2_dm(
					decoder_buffin_base_addr[ch], 13 ) ;
			sc1445x_internal_clear_dsp2_dm( g729_dec_base_addrs[ch],
							G729_DEC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g729_dec_base_addrs[ch],
					g729_unr_dec_addr, g729_unr_dec_data,
						G729_UNR_DEC_DATALENGTH ) ;
			sc1445x_internal_copy2dsp_channel_dep(
					g729_dec_base_addrs[ch],
					g729_rel_dec_addr, g729_rel_dec_data,
						G729_REL_DEC_DATALENGTH, ch ) ;
			if( SC1445x_AE_CODEC_G729_VAD == dec_ctype ) {
				mem = (unsigned short*)( 0x18000 +
						2 * g729_dec_base_addrs[ch] ) ;
#if 0
				mem[3] = 0x0001 ;
#endif
				cmd_val |= 0x0006 ;
			}
			else
				cmd_val |= 0x0005 ;

			break ;

		/* G722 (wideband) */
		case SC1445x_AE_CODEC_G722:
		case SC1445x_AE_CODEC_G722_MODE2:
		case SC1445x_AE_CODEC_G722_MODE3:
			*g722_dsp_mode &= ~( 3 << (2 * ch  +  8) ) ;
			if( SC1445x_AE_CODEC_G722_MODE3 == dec_ctype ) {
				codec->rate = 60 ;
				*g722_dsp_mode |= 3 << (2 * ch  +  8) ;
			} else if( SC1445x_AE_CODEC_G722_MODE2 == dec_ctype ) {
				codec->rate = 70 ;
				*g722_dsp_mode |= 2 << (2 * ch  +  8) ;
			} else {
				codec->rate = 80 ;
				*g722_dsp_mode |= 1 << (2 * ch  +  8) ;
			}

			sc1445x_internal_clear_dsp2_dm(
					decoder_buffin_base_addr[ch], 81 ) ;
			sc1445x_internal_clear_dsp2_dm( g711_dec_base_addrs[ch],
							G722_DEC_TOTALLENGTH ) ;
#ifdef G722_RELOCATION							
			//CLEAR PLC STRUCT
			//See the note below why g711_enc_base_addrs is correct.
			sc1445x_internal_clear_dsp2_dm( g711_enc_base_addrs[ch],
							G722_PLC_TOTALLENGTH ) ;
#endif							
							
#ifndef G722_RELOCATION
			sc1445x_internal_copy2dsp2_channel_indep(
					g711_dec_base_addrs[ch],
					g722_unr_dec_addr,
					g722_unr_dec_data,
					G722_UNR_DEC_DATALENGTH ) ;
			sc1445x_internal_copy2dsp_channel_dep(
					g711_dec_base_addrs[ch],
					g722_rel_dec_addr,
					g722_rel_dec_data,
					G722_REL_DEC_DATALENGTH, ch ) ;
#else
			//G722 DECODER STRUCT INITIALIZATION
			sc1445x_internal_copy2dsp2_channel_indep(
					g711_dec_base_addrs[ch],
					g722_unr_dec_addr,
					g722_unr_dec_data,
					G722_UNR_DEC_DATALENGTH ) ;
			//PLC STRUCT INITIALIZATION
			//Note that relocation has taken place so
			//g711_enc_base_addrs is correct.	
			sc1445x_internal_copy2dsp2_channel_indep(
					g711_enc_base_addrs[ch],
					g722_plc_unr_addr,
					g722_plc_unr_data,
					G722_PLC_UNR_DATALENGTH ) ;
			
			sc1445x_internal_copy2dsp_channel_dep(
					g711_enc_base_addrs[ch],
					g722_rel_plc_addr,
					g722_rel_plc_data,
					G722_REL_PLC_DATALENGTH, ch ) ;
#endif
			if( SC1445x_AE_CODEC_G722_MODE2 == dec_ctype  ||
				    SC1445x_AE_CODEC_G722_MODE3 == dec_ctype ) {
				/* Indepedently of relocation, copy the two
				 * additional tables at the decoder's part.
				 */
				mem = (unsigned short*)( 0x18000 +
					2 * (g711_dec_base_addrs[ch] + 0x5a) ) ;
				*mem = g711_dec_base_addrs[ch] + 0xD1 ;	

				if( SC1445x_AE_CODEC_G722_MODE2 == dec_ctype ) {
					DPRINT( "%s G722 mode 2\n", __FUNCTION__ ) ;
					mem = (unsigned short*)( 0x18000 +
						2 * (g711_dec_base_addrs[ch] + 0xD1) ) ;
					memcpy( (void*)mem, invqbtbl_mode2, 128 ) ;
				} else if( SC1445x_AE_CODEC_G722_MODE3 ==
								dec_ctype ) {
					DPRINT( "%s G722 mode 3\n", __FUNCTION__ ) ;
					mem = (unsigned short*)( 0x18000 +
						2 * (g711_dec_base_addrs[ch] + 0xD1) ) ;
					memcpy( (void*)mem, invqbtbl_mode3, 128 ) ;
				}
			}
			cmd_val |= 0x0007 ;

			break ;

		/* iLBC flavors */
		case SC1445x_AE_CODEC_iLBC_20ms:
		case SC1445x_AE_CODEC_iLBC_30ms:
			sc1445x_internal_clear_dsp2_dm(
					decoder_buffin_base_addr[ch],1+25) ;
			//NOTE: Encoder is in Decoder's place and v.v.
			sc1445x_internal_clear_dsp2_dm( iLBC_dec_base_addrs[ch],
						iLBC_DEC_TOTALLENGTH) ;

			sc1445x_internal_copy2dsp2_channel_indep(
					iLBC_dec_base_addrs[ch],
					iLBC_unr_dec_addr, iLBC_unr_dec_data,
					iLBC_UNR_DEC_DATALENGTH ) ;
			sc1445x_internal_copy2dsp_channel_dep(
					iLBC_dec_base_addrs[ch],
					iLBC_rel_dec_addr, iLBC_rel_dec_data,
					iLBC_REL_DEC_DATALENGTH, ch ) ;
			mem = (unsigned short*)( 0x18000 +
						2 * iLBC_dec_base_addrs[ch] ) ;
			iLBC_playback_cnt[ch] = 0 ;
			iLBC_playback_tmp_status[ch] = 0 ;

			if( SC1445x_AE_CODEC_iLBC_20ms == dec_ctype ){
				mem[2] = 20 ;
				codec->rate = 38 ;
				cmd_val |= 0x000b ;
				INIT_CONST_FIELD( unsigned short, acs,
							playback_cnt_lim, 2 ) ;
			} else {
				mem[2] = 30 ;
				codec->rate = 50 ;
				cmd_val |= 0x000c ;
				INIT_CONST_FIELD( unsigned short, acs,
							playback_cnt_lim, 3 ) ;
			}

			break ;

#if defined( CONFIG_SC14452_C )
		/* G711-1 flavors, narrowband (NB) / wideband (WB) */ 
		case SC1445x_AE_CODEC_G711_1_ALAW_NB64:
		case SC1445x_AE_CODEC_G711_1_ALAW_NB64POST:
		case SC1445x_AE_CODEC_G711_1_ULAW_NB64:
		case SC1445x_AE_CODEC_G711_1_ULAW_NB64POST:
			use_dec_cmd = 1 ;
			codec->rate = 40 ;
			sc1445x_internal_clear_dsp2_dm(
					encoder_buff_base_addr[dec_ch], 62 ) ;
			sc1445x_internal_clear_dsp2_dm(
					g711_1_dec_base_addrs[dec_ch],
					G711_1_DEC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g711_1_dec_base_addrs[dec_ch],
					g711_1_dec_addr,
					g711_1_dec_data,
					G711_1_DEC_DATALENGTH ) ;
			mem = (unsigned short*)
				( 0x18000 + 2 * g711_1_dec_base_addrs[dec_ch] );
			if( SC1445x_AE_CODEC_G711_1_ALAW_NB64 == dec_ctype ) {
				mem[70] = 2 ;		/* law = 2 */
				mem[1801] = 0 ;		/* mode_dec = 0 */
				/* MODE R1 (X(2-0):0), A-law (X-MSB:0) */
				/* dec_cmd_val |= 0x0000 ; */
			} else if( SC1445x_AE_CODEC_G711_1_ALAW_NB64POST
								== dec_ctype ) {
				mem[70] = 2 ;		/* law = 2 */
				mem[1056] = 1 ;		/* postfilter_option */
				mem[1801] = 10 ;	/* mode_dec = 10 */
				/* MODE R1post (X(2-0):1), A-law (X-MSB:0) */
				dec_cmd_val |= 0x0100 ;
			} else if( SC1445x_AE_CODEC_G711_1_ULAW_NB64
								== dec_ctype ) {
				mem[70] = 1 ;		/* law = 1 */
				mem[1801] = 0 ;		/* mode_dec = 0 */
				/* MODE R1 (X(2-0):0), U-law (X-MSB:1) */
				dec_cmd_val |= 0x0800 ;
			} else /* if( SC1445x_AE_CODEC_G711_1_ULAW_NB64POST
							== dec_ctype )*/ {
				mem[70] = 1 ;		/* law = 1 */
				mem[1056] = 1 ;		/* postfilter_option */
				mem[1801] = 10 ;	/* mode_dec = 10 */
				/* MODE R1post (X(2-0):1), U-law (X-MSB:1) */
				dec_cmd_val |= 0x0900 ;
			}
			/* operating freq */
			mem[69] = 8000 ;
			/* nbytesin */
			mem[1804] = 40 ;
			/* nsamplesOut */
			mem[1806] = 40 ;
			/* Tables */
			mem[1807]= 0x8da9 ;
			/* Decoder Select */
			dec_cmd_val |= 0x000d ;
			break ;

		case SC1445x_AE_CODEC_G711_1_ALAW_NB80:
		case SC1445x_AE_CODEC_G711_1_ALAW_WB80:
		case SC1445x_AE_CODEC_G711_1_ULAW_NB80:
		case SC1445x_AE_CODEC_G711_1_ULAW_WB80:
			use_dec_cmd = 1 ;
			codec->rate = 50 ;
			sc1445x_internal_clear_dsp2_dm(
					decoder_buffin_base_addr[dec_ch], 62 ) ;
			sc1445x_internal_clear_dsp2_dm(
					g711_1_dec_base_addrs[dec_ch],
					G711_1_DEC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g711_1_dec_base_addrs[dec_ch],
					g711_1_dec_addr,
					g711_1_dec_data,
					G711_1_ENC_DATALENGTH ) ;
			mem = (unsigned short*)
				( 0x18000 + 2 * g711_1_dec_base_addrs[dec_ch] );
			if( SC1445x_AE_CODEC_G711_1_ALAW_NB80 == dec_ctype ) {
				mem[70] = 2 ;		/* law = 2 */
				mem[69] = 8000 ;	/* OpFs = 8000 */
				mem[1800] = 1 ;		/* mode_bst=1 */
				mem[1801] = mem[1800] ;	/* mode_dec=1 */
				/* Mode R2a (X(2-0):2), A-law (X-MSB:0) */
				dec_cmd_val |= 0x0200 ;
			} else if( SC1445x_AE_CODEC_G711_1_ALAW_WB80
								== dec_ctype ) {
				mem[70] = 2 ;		/* law = 2 */
				mem[69] = 16000 ;	/* OpFs = 16000 */
				mem[1800] = 2 ;		/* mode_bst=2 */
				mem[1801] = mem[1800] ;	/* mode_dec=2 */
				/* Mode R2b (X(2-0):3), A-law (X-MSB:0) */
				dec_cmd_val |= 0x0300 ;
			} else if( SC1445x_AE_CODEC_G711_1_ULAW_NB80
								== dec_ctype ) {
				mem[70] = 1 ;		/* law = 1 */
				mem[69] = 8000 ;	/* OpFs = 8000 */
				mem[1800] = 1 ;		/* mode_bst=1 */
				mem[1801] = mem[1800] ;	/* mode_dec=1 */
				/* Mode R2a (X(2-0):2), U-law (X-MSB:1) */
				dec_cmd_val |= 0x0a00 ;
			} else /*if( SC1445x_AE_CODEC_G711_1_ULAW_WB80
				 			== dec_ctype )*/ {
				mem[70] = 1 ;		/* law = 1 */
				mem[69] = 16000 ;	/* OpFs = 16000 */
				mem[1800] = 2 ;		/* mode_bst=2 */
				mem[1801] = mem[1800] ;	/* mode_dec=2 */
				/* Mode R2b (X(2-0):3), U-law (X-MSB:1) */
				dec_cmd_val |= 0x0b00 ;
			}
			/* nbytesin */
			mem[1804] = 50 ;
			/* nsamplesOut */
			mem[1806] = ( 8000 == mem[69] ) ?  40 :  80 ;
			/* Tables */
			mem[1807]= 0x8da9 ;
			/* Decoder Select */
			dec_cmd_val |= 0x000d ;
			break ;

		case SC1445x_AE_CODEC_G711_1_ALAW_WB96:
		case SC1445x_AE_CODEC_G711_1_ULAW_WB96:
			use_dec_cmd = 1 ;
			codec->rate = 60 ;
			sc1445x_internal_clear_dsp2_dm(
					decoder_buffin_base_addr[dec_ch], 62 ) ;
			sc1445x_internal_clear_dsp2_dm(
					g711_1_dec_base_addrs[dec_ch],
					G711_1_DEC_TOTALLENGTH ) ;
			sc1445x_internal_copy2dsp2_channel_indep(
					g711_1_dec_base_addrs[dec_ch],
					g711_1_dec_addr,
					g711_1_dec_data,
					G711_1_DEC_DATALENGTH ) ;
			mem = (unsigned short*)
				( 0x18000 + 2 * g711_1_dec_base_addrs[dec_ch] );
			if( SC1445x_AE_CODEC_G711_1_ALAW_WB96 == dec_ctype ) {
				mem[70] = 2 ;		/* law = 2 */
				mem[1800] = 3;		/* mode_bst=3 */
				mem[1801] = mem[1800] ;	/* mode_dec=3 */
				/* Mode R3 (X(2-0):4), A-law (X-MSB:0) */
				dec_cmd_val |= 0x0400 ;
			} else {
				mem[70] = 1 ;		/* law = 1 */
				mem[1800] = 3 ;		/* mode_bst=3 */
				mem[1801] = mem[1800] ;	/* mode_dec=3 */
				/* Mode R3 (X(2-0):4), U-law (X-MSB:1) */
				dec_cmd_val |= 0x0c00 ;
			}
			mem[69] = 16000 ;
			/* nbytesin */
			mem[1804] = 60 ;
			/* nsamplesOut */
			mem[1806] = 80 ;
			/* Tables */
			mem[1807]= 0x8da9 ;
			/* Decoder Select */
			dec_cmd_val |= 0x000d ;
			break ;
#endif  /* CONFIG_SC14452_C */

		default:
			PRINT( sc1445x_ae_codec_not_supported_error,
								dec_ctype ) ;
			return SC1445x_AE_ERR_CODEC_NOT_SUPPORTED ;
	}

	DPRINT( "Downloaded code for channel %d: decoding codec %d\n",
							dec_ch, dec_ctype ) ;

	if( sc1445x_internal_is_wideband_codec( enc_ctype ) ||
			sc1445x_internal_is_wideband_codec( dec_ctype ) ) {
		sc1445x_internal_init_codec( ae_state,
				SC1445x_AE_RAW_PCM_RATE_16000 ) ;
	} else {
		if( !sc1445x_internal_have_wide_channels( ae_state ) ) {
			sc1445x_internal_init_codec( ae_state,
					SC1445x_AE_RAW_PCM_RATE_8000 ) ;
		}
	}

#if 1
#if defined( USE_PLC ) && !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	/* activate PLC */
	sc1445x_internal_send_dsp_cmd( 0x0005, 1 ) ;
#endif
	/* actually activate the channel(s) */
	sc1445x_internal_send_dsp_cmd( cmd, cmd_val ) ;
	if( use_dec_cmd ) {
		/* tell DSP that these 2 channels are related */
		cmd_val = ch << 8 ;
		cmd_val |= dec_ch & 0x0f ;
		sc1445x_internal_send_dsp_cmd( 0x003b, cmd_val ) ;

		sc1445x_internal_send_dsp_cmd( dec_cmd, dec_cmd_val ) ;
	}
#endif

	return SC1445x_AE_OK ;
}


/* deactivate an audio channel */
short sc1445x_ae_deactivate_channel( sc1445x_ae_state* ae_state,
                                        unsigned short channel_index )
{
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	sc1445x_ae_xmatrix_entry* channels ;
	sc1445x_ae_xmatrix_entry* streams ;
	unsigned short i ;
#endif
#if defined( SMOOTH_IFACE_TRANSITION )
	int res ;
#endif
	unsigned short dec_ch, nchannels ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( channel_index, ae_state->audio_channels_count ) ;

	if( !ae_state->audio_channels[channel_index].is_active ) {
		PRINT( sc1445x_ae_channel_not_active_error, channel_index ) ;
		return SC1445x_AE_ERR_CHANNEL_NOT_ACTIVE ;
	}

	dec_ch = nchannels = ae_state->audio_channels_count ;

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	channels = ae_state->xmatrix.voip_channels ;
	streams = ae_state->xmatrix.audio_streams ;

	for( i = 0 ;  i < ae_state->audio_channels_count ;  ++i )
		xmatrix_dont_listen_to_voip_channel( &channels[i],
							channel_index ) ;
	for( i = 0 ;  i < SC1445x_AE_AUDIO_STREAMS_COUNT ;  ++i )
		xmatrix_dont_listen_to_voip_channel( &streams[i],
							channel_index ) ;
	channels[channel_index] = 0 ;

	sc1445x_internal_update_interconnection_matrix( ae_state ) ;
#endif

	if( ae_state->audio_channels[channel_index].is_active ) {
		sc1445x_ae_channel_state* acs =
				&ae_state->audio_channels[channel_index] ;
#if defined( CONFIG_SC14452 )
		unsigned short dec_type = acs->dec_codec.type ;
		unsigned short enc_type = acs->enc_codec.type ;
#endif
		unsigned short cmd =
			( channel_index < 3 )	?  ( channel_index + 1 )
						:  0x0025 ;

		if( acs->is_g711_1_enc )
			dec_ch = acs->g711_1_buddy_dec_ch ;

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
		ae_state->audio_channels[channel_index].is_active = 0 ;
#else
		ae_state->audio_channels[channel_index].do_release = 1 ;
#endif
		/* actually deactivate the channel */
		sc1445x_internal_send_dsp_cmd( cmd, 0 ) ;
#if defined( USE_PLC ) && !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		/* deactivate PLC */
		DPRINT( PRINT_LEVEL "Turn off PLC\n" ) ;
		sc1445x_internal_send_dsp_cmd( 0x0005, 0 ) ;
#endif

#if defined( CONFIG_SC14452 )
		if( SC1445x_AE_CODEC_iLBC_30ms == enc_type  ||
				SC1445x_AE_CODEC_iLBC_20ms == enc_type  ||
				SC1445x_AE_CODEC_iLBC_30ms == dec_type  ||
				SC1445x_AE_CODEC_iLBC_20ms == dec_type ) {
			/* keep track of how many channels use iLBC */
			--ae_state->ilbc_codec_count ;
		}
#endif

#if defined( CONFIG_SC1445x_CVQ_METRICS ) \
				|| defined( CONFIG_SC1445x_CVQ_METRICS_MODULE )
		if( cvqm_cbs.stop_channel )
			cvqm_cbs.stop_channel( channel_index,
					SC1445X_CVQM_CHANNEL_DIR_PLAYBACK |
					SC1445X_CVQM_CHANNEL_DIR_CAPTURE ) ;
#endif
	}
	
	if( !sc1445x_private_have_active_channels( ae_state ) ) {
#if defined( CHECK_DSP_IRQ_OVERFLOW )
		SetWord( DSP1_OVERFLOW_REG, 0x00FF ) ;
		SetWord( DSP2_OVERFLOW_REG, 0x00FF ) ;
#endif

		if(
#if defined( SC1445x_AE_PHONE_DECT )
			ae_state->dect_headset_type !=
				SC1445x_AE_LINE_TYPE_NATIVE_DECT_NARROW
#else
			1
#endif
		  ) {
			DPRINT( "%s: switching to 8K while all channels are "
					"closed\n", __FUNCTION__ ) ;
			sc1445x_internal_init_codec( ae_state,
					SC1445x_AE_RAW_PCM_RATE_8000 ) ;
		}

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		ae_state->op_mode.bits.voip_x_codec_classd = 0 ;
		if( ae_state->iface_mode != SC1445x_AE_IFACE_MODE_BT_HEADSET
				&&  ae_state->iface_mode !=
						SC1445x_AE_IFACE_MODE_WIRELESS )
			ae_state->op_mode.bits.voip_x_ramio_pcm = 0 ;
		sc1445x_internal_enforce_phone_op_mode( ae_state ) ;

		if( !ae_state->do_stop_pcm_channel )
			sc1445x_internal_classd_autopower( ae_state,
						ae_state->iface_mode, 0, 0 ) ;
#endif
	}

#if defined( SMOOTH_IFACE_TRANSITION )
	res = wait_event_interruptible_timeout( ae_state->fadeoutq,
				( 0 == ae_state->do_stop_pcm_channel ), 10 ) ;
	if( res < 0 )
		DPRINT( PRINT_LEVEL "%s: wait_event_interruptible_timeout() "
						"failed\n", __FUNCTION__ ) ;
	else
		DPRINT( "%s: wait_event_interruptible_timeout() returned %d\n",
							__FUNCTION__, res ) ;
#endif

	if( dec_ch < nchannels )
		sc1445x_ae_deactivate_channel( ae_state, dec_ch ) ;
#if 0
	else {
		int res = wait_event_interruptible_timeout(
			ae_state->audio_channels[channel_index].releaseq,
			( 0 == ae_state->audio_channels[channel_index].
			  					do_release ),
			10 ) ;
		if( res < 0 )
			PRINT( PRINT_LEVEL "%s: "
					"wait_event_interruptible_timeout() "
					"failed\n", __FUNCTION__ ) ;
		else
			DPRINT( "%s: wait_event_interruptible_timeout()"
					" returned %d\n", __FUNCTION__, res ) ;
	}
#endif

	return SC1445x_AE_OK ;
}


/* control DSP loopback capability */
short sc1445x_ae_control_dsp_loopback( const sc1445x_ae_state* ae_state,
							unsigned short val )
{
	struct __CODEC_LSR_REG* codec_lsr_reg =
				(struct __CODEC_LSR_REG*)&CODEC_LSR_REG ;
	volatile unsigned short* dsp2_loopback_ctrl = (void*)0x1b082 ;
	unsigned short ch ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	PRINT( PRINT_LEVEL "%s: val=%04x\n", __FUNCTION__, val ) ;

	ch = val / 100 ;

	switch( val ) {
		case 0:
			{
				/*
				 * we should re-apply the settings for the
				 * current iface mode
				 */
				short cur_vmic = vmic_from_iface_mode(
					ae_state->iface_mode,
					ae_state->using_external_codec_mic ) ;
				short cur_vspk = vspk_from_iface_mode(
					ae_state->iface_mode,
				       	ae_state->using_external_codec_spk ) ;

				sc1445x_ae_internal_set_vmic( ae_state,
							ae_state->iface_mode,
							cur_vmic ) ;
				sc1445x_ae_internal_set_vspk( ae_state,
							ae_state->iface_mode,
							cur_vspk ) ;
				break ;
			}

		case 1:
			/* loopback to handset spk */

			/* turn on the handset spk */
			codec_lsr_reg->BITFLD_LSREN_SE = 0 ;
			codec_lsr_reg->BITFLD_LSRN_MODE = 2 ;
			codec_lsr_reg->BITFLD_LSRP_MODE = 2 ;
			codec_lsr_reg->BITFLD_LSRN_PD = 0 ;
			codec_lsr_reg->BITFLD_LSRP_PD = 0 ;

			/* turn off CLASSD */
			sc1445x_internal_classd_autopower( ae_state,
						ae_state->iface_mode, 0, 0 ) ;

			sc1445x_internal_enable_output_to_classd_amp( ae_state,
									0 ) ;
			sc1445x_internal_enable_output_to_hw_codec( ae_state,
									1 ) ;
			break ;

		case 2:
			/* loopback to external spk */

			/* turn off the handset spk */
			codec_lsr_reg->BITFLD_LSRN_PD = 1 ;
			codec_lsr_reg->BITFLD_LSRP_PD = 1 ;

			/* turn on CLASSD */
			sc1445x_internal_classd_autopower( ae_state,
				SC1445x_AE_IFACE_MODE_HANDS_FREE, 1, 0 ) ;

			sc1445x_internal_enable_output_to_classd_amp( ae_state,
									1 ) ;
			sc1445x_internal_enable_output_to_hw_codec( ae_state,
									0 ) ;
			break ;

		case 10:
		case 20:
		case 110:
		case 120:
		case 210:
		case 220:
		case 310:
		case 320:
			/* undo DSP2 loopback */
			dsp2_loopback_ctrl[ch] = 0 ;
			break ;

		case 11:
		case 111:
		case 211:
		case 311:
			/* DSP2 remote loopback before the decoder */
			dsp2_loopback_ctrl[ch] = 1 ;
			break ;

		case 12:
		case 112:
		case 212:
		case 312:
			/* DSP2 remote loopback after the decoder */
			dsp2_loopback_ctrl[ch] = 2 ;
			break ;

		case 21:
		/*case 121:*/
		case 221:
		/*case 321:*/
			/*
			 * DSP2 local cross-loopback
			 * between channels ch and (ch + 1)
			 */
			dsp2_loopback_ctrl[ch] = 3 ;
			break ;

		case 22:
		case 122:
		case 222:
		case 322:
			/* DSP2 local loopback in the same channel */
			dsp2_loopback_ctrl[ch] = 4 ;
			break ;

		default:
			PRINT( PRINT_LEVEL "%s: val %d is not supported!\n",
							__FUNCTION__, val ) ;
			return SC1445x_AE_OK ;
	}

	if( val < 10 )
		sc1445x_internal_send_dsp_cmd( 0x0027, val ) ;

	return SC1445x_AE_OK ;
}


/*
 * enable monitoring mode
 * i.e. there is no conference and all remote parties can only hear us,
 * while we can hear all remote parties
 */
short sc1445x_ae_enable_monitoring( sc1445x_ae_state* ae_state )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	sc1445x_internal_send_dsp_cmd( 0x0038, 0x0001 ) ;

	return SC1445x_AE_OK ;
}


/*
 * disable monitoring mode
 * i.e. all active audio channels are mixed into a conference
 */
short sc1445x_ae_disable_monitoring( sc1445x_ae_state* ae_state )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	sc1445x_internal_send_dsp_cmd( 0x0038, 0x0000 ) ;

	return SC1445x_AE_OK ;
}


/*
 * enable external CODEC mic and/or spk
 * i.e. instead of the built-in CODEC/CLASSD, an external CODEC is used
 * the external CODEC is connected to PCM bus, slot #0
 */
short sc1445x_ae_enable_external_codec( sc1445x_ae_state* ae_state,
				       short enable_mic, short enable_spk )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	if( enable_mic ) {
		sc1445x_internal_send_dsp_cmd( 0x003c, 0x0001 ) ;
		ae_state->using_external_codec_mic = 1 ;
	}
	if( enable_spk ) {
		sc1445x_internal_send_dsp_cmd( 0x003d, 0x0001 ) ;
		ae_state->using_external_codec_spk = 1 ;
	}

	if( ae_state->using_external_codec_mic
				||  ae_state->using_external_codec_spk ) {
		/* enable PCM */
#if defined( CONFIG_SC14450 )
		CLK_CODEC_REG |= 0x4000 ;
#elif defined( CONFIG_SC14452 )
		clk_spu1_val |= SW_PCMCDC_EN ;
		CLK_SPU1_REG = clk_spu1_val ;
#endif
	}

	return SC1445x_AE_OK ;
}

/*
 * disable external CODEC mic and/or spk
 * i.e. return to normal operation, using the built-in CODEC/CLASSD
 */
short sc1445x_ae_disable_external_codec( sc1445x_ae_state* ae_state,
				       short disable_mic, short disable_spk )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	if( disable_mic ) {
		sc1445x_internal_send_dsp_cmd( 0x003c, 0x0000 ) ;
		ae_state->using_external_codec_mic = 0 ;
	}
	if( disable_spk ) {
		sc1445x_internal_send_dsp_cmd( 0x003d, 0x0000 ) ;
		ae_state->using_external_codec_spk = 0 ;
	}

#if 0
	if( !ae_state->using_external_codec_mic
				&&  !ae_state->using_external_codec_spk ) {
		/* disable PCM */
#if defined( CONFIG_SC14450 )
		CLK_CODEC_REG &= ~0x4000 ;
#elif defined( CONFIG_SC14452 )
#  ifndef SC1445x_AE_L_V2_V3_V4_V5_BOARD_CNX
		clk_spu1_val &= ~SW_PCMCDC_EN ;
#  endif
		CLK_SPU1_REG = clk_spu1_val ;
#endif
	}
#endif

	return SC1445x_AE_OK ;
}


static short sc1445x_ae_internal_start_tone4( sc1445x_ae_state* ae_state,
			unsigned short tg_mod,
			sc1445x_ae_tone tone1, unsigned short ampl1,
			sc1445x_ae_tone tone2, unsigned short ampl2,
			sc1445x_ae_tone tone3, unsigned short ampl3,
			sc1445x_ae_tone tone4, unsigned short ampl4,
			unsigned short dur_on, unsigned short dur_off,
			unsigned short play_count ) ;
static void sc1445x_private_update_tonegens( sc1445x_ae_state* ae_state,
							short is_wideband ) ;

/* setup CODEC for a specifig rate */
static void _sc1445x_internal_init_codec( const sc1445x_ae_state* ae_state,
			sc1445x_ae_raw_pcm_rate rate, short update_tonegens )
{
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	short classd_enabled = 
		SC1445x_AE_IFACE_MODE_HANDS_FREE == ae_state->iface_mode
			||
		SC1445x_AE_IFACE_MODE_OPEN_LISTENING == ae_state->iface_mode
			||
#  if defined( CONFIG_SC14450 )
		!!( CLK_CODEC_REG & 0x1000 ) ;
#  elif defined( CONFIG_SC14452 )
		!!( CLK_CLASSD1_REG & SW_CLASSD_EN ) ;
#  endif
#endif
	short was_wideband = !!currently_wideband() ;
	short is_wideband = !!( rate != SC1445x_AE_RAW_PCM_RATE_8000 ) ;

	if( update_tonegens && ( was_wideband ^ is_wideband ) ) {
		unsigned short i ;

		for( i = 0 ;  i < SC1445x_AE_TONEGEN_MOD_COUNT ;  ++i ) {
			if( SC1445x_AE_TONEGEN_IDLE ==
						ae_state->tonegen[i].status )
				continue ;

			/* stop playing the tone */
			sc1445x_internal_send_dsp_cmd( 0x0010, i ) ;
		}
	}

	SetBits( DSP1_CTRL_REG, DSP_CLK_EN, 0 ) ;
	SetBits( DSP2_CTRL_REG, DSP_CLK_EN, 0 ) ;

//psavvop-VC, telis proposal!
//psavvop, WB operation during WB and NB calls
#if !defined (DSP1_16kHz) && !defined (VC_enable) //#ifndef VC_enable
	if( SC1445x_AE_RAW_PCM_RATE_8000 == rate ) {
#  if defined( CONFIG_SC14450 )
		CLK_CODEC_REG = (CLK_CODEC_REG & 0x5000) | 0x2D55 ;
#  elif defined( CONFIG_SC14452 )
		CLK_CODEC1_REG = clk_codec1_val[0] ;
		CLK_CODEC2_REG = clk_codec2_val[0] ;
		CLK_CODEC3_REG = clk_codec3_val[0] ;
		CLK_SPU2_REG = clk_spu2_val[0] ;
		CLK_SPU1_REG = clk_spu1_val ;
#  endif
		// 8KHZ CODEC MIC Samples
		SetBits( DSP_MAIN_SYNC1_REG, AD_SYNC, 0 ) ;
		// 8KHZ CODEC LSR Samples
		SetBits( DSP_MAIN_SYNC1_REG, DA_LSR_SYNC, 0 ) ;
		// 8KHZ strobe to SYNC0
		SetBits( DSP_MAIN_SYNC1_REG, DSP_SYNC0, 0 ) ;
		// 8KHZ CLASSD Samples
		SetBits( DSP_MAIN_SYNC1_REG, DA_CLASSD_SYNC, 0 ) ;
#  if defined( SC1445x_AE_DECT_SUPPORT ) \
			|| defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD_CNX )
		// 8KHZ PCM FSC
		SetBits( DSP_MAIN_SYNC1_REG, PCM_SYNC, 0 ) ;
#  endif
		// DSP_INT1_PRIO & DSP_INT2_PRIO disabled!!!
		// DSP_INT0_PRIO = DSP_SYNC0
		SetWord( DSP1_INT_PRIO1_REG, 0x077 ) ;
		// DSP_INT1_PRIO & DSP_INT2_PRIO disabled!!!
		// DSP_INT0_PRIO = DSP_IRQ
		SetWord( DSP2_INT_PRIO2_REG, 0x377 ) ;
#  if defined( DSP_DEBUG_WITH_GPIO )
		/* enable for delay measurements 4-9! */
		SetWord( DSP2_INT_PRIO1_REG, 0x077 ) ;
#  endif

#  if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		sc1445x_ae_set_rx_tx_filters( (sc1445x_ae_state*)ae_state,
					classd_enabled, 0,
					currently_using_headset( ae_state ) ) ;
#  endif
	} else if( SC1445x_AE_RAW_PCM_RATE_16000 == rate ) {
/********** psavvop-VC **********/
#else
	if( (SC1445x_AE_RAW_PCM_RATE_8000 == rate)
			|| (SC1445x_AE_RAW_PCM_RATE_16000 == rate) ) {
#endif
/********** psavvop-VC **********/
#if defined( CONFIG_SC14450 )
		CLK_CODEC_REG = (CLK_CODEC_REG & 0x5000) | 0x2DA9 ;
#elif defined( CONFIG_SC14452 )
		CLK_CODEC1_REG = clk_codec1_val[1] ;
		CLK_CODEC2_REG = clk_codec2_val[1] ;
		CLK_CODEC3_REG = clk_codec3_val[1] ;
		CLK_SPU2_REG = clk_spu2_val[1] ;
		CLK_SPU1_REG = clk_spu1_val ;
#endif
		// 16KHZ CODEC MIC Samples
		SetBits( DSP_MAIN_SYNC1_REG, AD_SYNC, 1 ) ;
		// 16KHZ CODEC LSR Samples
		SetBits( DSP_MAIN_SYNC1_REG, DA_LSR_SYNC, 1 ) ;
		// 16KHZ strobe to SYNC1
		SetBits( DSP_MAIN_SYNC1_REG, DSP_SYNC1, 1 ) ;
		// 16KHZ CLASSD Samples
		SetBits( DSP_MAIN_SYNC1_REG, DA_CLASSD_SYNC, 1 ) ;
#if defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD_CNX )
		// 16KHZ PCM FSC
		SetBits( DSP_MAIN_SYNC1_REG, PCM_SYNC, 1 ) ;
#elif defined( SC1445x_AE_DECT_SUPPORT )  &&  !defined( SC1445x_AE_ATA_SUPPORT )
		if( SC1445x_AE_IFACE_MODE_BT_HEADSET == ae_state->iface_mode ) {
			// 8KHZ PCM FSC
			SetBits( DSP_MAIN_SYNC1_REG, PCM_SYNC, 0 ) ;
		} else {
			// 16KHZ PCM FSC
			SetBits( DSP_MAIN_SYNC1_REG, PCM_SYNC, 1 ) ;
		}
#endif
		// DSP_INT1_PRIO & DSP_INT2_PRIO disabled!!!
		// DSP_INT0_PRIO = DSP_SYNC1
		SetWord( DSP1_INT_PRIO1_REG, 0x717 ) ;
		// DSP_INT1_PRIO & DSP_INT2_PRIO disabled!!!
		// DSP_INT0_PRIO = DSP_IRQ
		SetWord( DSP2_INT_PRIO2_REG, 0x377 ) ;

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		sc1445x_ae_set_rx_tx_filters( (sc1445x_ae_state*)ae_state,
					classd_enabled, 1,
					currently_using_headset( ae_state ) ) ;
#endif
	} else if( SC1445x_AE_RAW_PCM_RATE_32000 == rate ) {
#if defined( CONFIG_SC14450 )
		CLK_CODEC_REG = (CLK_CODEC_REG & 0x5000) | 0x2DFD ;
#elif defined( CONFIG_SC14452 )
		CLK_CODEC1_REG = clk_codec1_val[2] ;
		CLK_CODEC2_REG = clk_codec2_val[2] ;
		CLK_CODEC3_REG = clk_codec3_val[2] ;
		CLK_SPU2_REG = clk_spu2_val[2] ;
		CLK_SPU1_REG = clk_spu1_val ;
#endif
		// 32KHZ CODEC MIC Samples
		SetBits( DSP_MAIN_SYNC1_REG, AD_SYNC, 2 ) ;
		// 32KHZ CODEC LSR Samples
		SetBits( DSP_MAIN_SYNC1_REG, DA_LSR_SYNC, 2 ) ;
		// 32KHZ strobe to SYNC1
		SetBits( DSP_MAIN_SYNC1_REG, DSP_SYNC2, 2 ) ;
		// 32KHZ CLASSD Samples
		SetBits( DSP_MAIN_SYNC1_REG, DA_CLASSD_SYNC, 2 ) ;
#if defined( SC1445x_AE_DECT_SUPPORT )  &&  !defined( SC1445x_AE_ATA_SUPPORT )
		if( SC1445x_AE_IFACE_MODE_BT_HEADSET == ae_state->iface_mode ) {
			// 8KHZ PCM FSC
			SetBits( DSP_MAIN_SYNC1_REG, PCM_SYNC, 0 ) ;
		} else {
			// 32KHZ PCM FSC
			SetBits( DSP_MAIN_SYNC1_REG, PCM_SYNC, 2 ) ;
		}
#endif
		// DSP_INT1_PRIO & DSP_INT2_PRIO disabled!!!
		// DSP_INT0_PRIO = DSP_SYNC2
		SetWord( DSP1_INT_PRIO1_REG, 0x772 ) ;
		// DSP_INT1_PRIO & DSP_INT2_PRIO disabled!!!
		// DSP_INT0_PRIO = DSP_IRQ
		SetWord( DSP2_INT_PRIO2_REG, 0x377 ) ;

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		sc1445x_ae_set_rx_tx_filters( (sc1445x_ae_state*)ae_state,
					classd_enabled, 1,
					currently_using_headset( ae_state ) ) ;
#endif
	}

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	if( GetBits( DSP1_CTRL_REG, DSP_EN ) ) {
		/* re-set vmic gain and vspk volume, in case we switched */
		/* between narrowband and wideband */
		do_sc1445x_ae_set_vmic_gain( ae_state ) ;
		if( !ae_state->do_stop_pcm_channel )
			do_sc1445x_ae_set_vspk_volume( ae_state ) ;
	}
#endif

	if( update_tonegens && ( was_wideband ^ is_wideband ) )
		sc1445x_private_update_tonegens( (sc1445x_ae_state*)ae_state,
								is_wideband ) ;
}
/* setup CODEC for a specifig rate */
static void sc1445x_internal_init_codec( const sc1445x_ae_state* ae_state,
						sc1445x_ae_raw_pcm_rate rate )
{
	return _sc1445x_internal_init_codec( ae_state, rate, 1 ) ;
}


/* initialize several registers before using the DSPs/CODEC */
static void sc1445x_internal_init_regs( const sc1445x_ae_state* ae_state )
{
	struct __CODEC_LSR_REG* codec_lsr_reg = (void*)&CODEC_LSR_REG ;
#if defined( CONFIG_SC14452 )
	const unsigned char layout = CHIP_TEST1_REG ;
	const short is_fibbed = 1 ;
#endif

#if defined( CONFIG_SC14452 )
	if( 0 == layout  &&  !is_fibbed ) {
		clk_spu1_val = 0x1C21 ;		/* no PCM by default */
		clk_spu2_val[0] = 0x4848 ;	/*  8kHz */
		clk_spu2_val[1] = 0x2448 ;	/* 16kHz */
		clk_spu2_val[2] = 0x1248 ;	/* 32kHz */

		clk_codec1_val[0] = 0x0148 ;	/*  8kHz */
		clk_codec1_val[1] = 0x0124 ;	/* 16kHz */
		clk_codec1_val[2] = 0x0112 ;	/* 32kHz */

		clk_codec2_val[0] = 0x0148 ;	/*  8kHz */
		clk_codec2_val[1] = 0x0124 ;	/* 16kHz */
		clk_codec2_val[2] = 0x0112 ;	/* 32kHz */

		clk_codec3_val[0] = 0x0048 ;	/*  8kHz */
		clk_codec3_val[1] = 0x0024 ;	/* 16kHz */
		clk_codec3_val[2] = 0x0012 ;	/* 32kHz */
	} else {
		clk_spu1_val = 0x1C42 ;		/* no PCM by default */
		clk_spu2_val[0] = 0x9090 ;	/*  8kHz */
		clk_spu2_val[1] = 0x9090 ;	/* 16kHz */
		clk_spu2_val[2] = 0x9090 ;	/* 32kHz */

		clk_codec1_val[0] = 0x0190 ;	/*  8kHz */
		clk_codec1_val[1] = 0x0148 ;	/* 16kHz */
		clk_codec1_val[2] = 0x0124 ;	/* 32kHz */

		clk_codec2_val[0] = 0x0190 ;	/*  8kHz */
		clk_codec2_val[1] = 0x0148 ;	/* 16kHz */
		clk_codec2_val[2] = 0x0124 ;	/* 32kHz */

		clk_codec3_val[0] = 0x0090 ;	/*  8kHz */
		clk_codec3_val[1] = 0x0048 ;	/* 16kHz */
		clk_codec3_val[2] = 0x0024 ;	/* 32kHz */
	}
#endif

#if defined( SC1445x_AE_PCM_LINES_SUPPORT ) || defined( SC1445x_AE_BT )
#  if defined( SC1445x_AE_ATA_SUPPORT )
	/* use this for ATA */
	/* set up PCM clock at 1.536MHz */
#    ifdef CONFIG_SC14452
	clk_spu2_val[0] = 0x6c6c ;	/*  8kHz */
	clk_spu2_val[1] = 0x6c6c ;	/* 16kHz */
	clk_spu2_val[2] = 0x6c6c ;	/* 32kHz */

	CLK_SPU2_REG = clk_spu2_val[0] ;
#    else
	CLK_CODEC_DIV_REG = 0xb6 ;
#    endif
	DSP_MAIN_CNT_REG = 0xbf00 ;
#  else
	/* use this for only DECT */
	/* set up PCM clock at 1.152MHz */
#    ifdef CONFIG_SC14452
	CLK_SPU2_REG = clk_spu2_val[0] ;
#    else
	CLK_CODEC_DIV_REG = 0xc8 ;
#    endif
	DSP_MAIN_CNT_REG = 0x8f00 ;
#  endif
	/* enable PCM clock */
#  ifdef CONFIG_SC14452
	clk_spu1_val |= SW_PCMCDC_EN ;
	CLK_SPU1_REG = clk_spu1_val ;
#  else
	CLK_CODEC_REG |= 0x4000 ;
#  endif
	SetBits( DSP_MAIN_SYNC1_REG, PCM_SYNC, 0 ) ;
#endif

#if defined( SC1445x_AE_BT ) && !defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD ) \
			&& !defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD_CNX )
	/* setup GPIO for PCM */
#  if !defined( CONFIG_SND_SC1445x_DE900 )
	SetPort( P1_14_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_PCM_FSC ) ;
#  else
	SetPort( P2_06_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_PCM_FSC ) ;
#  endif
	SetPort( P2_07_MODE_REG, GPIO_PUPD_IN_NONE, GPIO_PID_PCM_DI ) ;
#  if !defined( CONFIG_SND_SC1445x_DE900 )
	SetPort( P1_00_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_PCM_DO ) ;
#  else
	SetPort( P2_08_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_PCM_DO ) ;
#  endif
	SetPort( P2_09_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_PCM_CLK ) ;
#endif

	// vrefs on
	SetWord( CODEC_VREF_REG, 0x0014 ) ;
	udelay( 100 ) ;
	SetWord( CODEC_VREF_REG, 0x0004 ) ;

	// enable AD/DA
	SetWord( CODEC_ADDA_REG, 0x00E8 ) ;

	/* put speaker in power-down */
	SetWord( CODEC_LSR_REG, 0x24 ) ;
	codec_lsr_reg->BITFLD_LSRN_MODE = 2 ;
	codec_lsr_reg->BITFLD_LSRP_MODE = 2 ;

	/* put MIC in power-down */
#if defined( DO_MIC_DC_CANCELLATION )
	SetBits( CODEC_TEST_CTRL_REG, COR_ON, 1 ) ;
#endif
	SetWord( CODEC_MIC_REG, 0x00A4 ) ;  /* diffential mic on MICp/MICn */

	/* put CLASSD in power-down */
	CLASSD_CTRL_REG = 0xcc0d ;

	// Gen2DSP clock disabled after WTF execution. Wakes up on SYNC event
	SetBits( DSP1_CTRL_REG, DSP_CLK_EN, 0 ) ;

	//aris for dsp2
	// Gen2DSP clock disabled after WTF execution. Wakes up on SYNC event
	SetBits( DSP2_CTRL_REG, DSP_CLK_EN, 0 ) ;

#if !defined( SC1445x_AE_NATIVE_DECT_SUPPORT )
	// DSP main counter out of reset. Free running
	SetWord( DSP_MAIN_CTRL_REG, 0x01AF ) ;
#else
#  if 0
	/* wait for DIP A_NORM instruction */
	SetWord( DSP_MAIN_CTRL_REG, 0x02AF ) ;
#  else
	// DSP main counter out of reset. Free running
	SetWord( DSP_MAIN_CTRL_REG, 0x01AF ) ;
#  endif
#endif

#ifdef SC1445x_AE_WIDEBAND
	sc1445x_internal_init_codec( ae_state,
				SC1445x_AE_RAW_PCM_RATE_16000 ) ;
#else
	sc1445x_internal_init_codec( ae_state,
				SC1445x_AE_RAW_PCM_RATE_8000 ) ;
#endif
}


/* initialize DSP memories */
static void sc1445x_internal_init_dsp_mem( void )
{
	unsigned short i ;
#if defined( SC1445x_AE_SUPPORT_FAX )
	static volatile unsigned short* dsp_hot_dl_sel_addr = (void*)0x1AF40 ;
#endif

	/* initialize program memory of DSP1 */
	if(DSP1_PM_addr[DSP1_PM_size-1]>=DSP1_PROG_MEM_LIMIT)
		panic("DSP MEMORY INITIALIZATION FILE BIGGER THAN MEMORY LIMIT @DSP1 PROGRAM MEMORY");

	for( i = 0 ;  i < DSP1_PROG_MEM_LIMIT ;  ++i )
		dsp1_prog_mem[ i ] = 0xAAAA;

	for( i = 0 ;  i < DSP1_PM_size ;  ++i )
		dsp1_prog_mem[ DSP1_PM_addr[i] ] = DSP1_PM_val[i] ;

	/* initialize data memory of DSP1 */
	if(DSP1_DM_addr[DSP1_DM_size-1]>=DSP1_DATA_MEM_LIMIT)
		panic("DSP MEMORY INITIALIZATION FILE BIGGER THAN MEMORY LIMIT @DSP1 DATA MEMORY");

	for( i = 0 ;  i < DSP1_DATA_MEM_LIMIT ;  ++i )
		dsp1_data_mem[ i ] = 0xBBBB ;

	for( i = 0 ;  i < DSP1_DM_size ;  ++i )
		dsp1_data_mem[ DSP1_DM_addr[i] ] = DSP1_DM_val[i] ;

	/* initialize program memory of DSP2 */
	if(DSP2_PM_addr[DSP2_PM_size-1]>=DSP2_PROG_MEM_LIMIT)
		panic("DSP MEMORY INITIALIZATION FILE BIGGER THAN MEMORY LIMIT @DSP2 PROGRAM MEMORY");

	for( i = 0 ;  i < DSP2_PROG_MEM_LIMIT ;  ++i )
		dsp2_prog_mem[ i ] = 0xCCCC;

	for( i = 0 ;  i < DSP2_PM_size ;  ++i )
		dsp2_prog_mem[ DSP2_PM_addr[i] ] = DSP2_PM_val[i] ;

	/* initialize data memory of DSP2 */
	if(DSP2_DM_addr[DSP2_DM_size-1]>=DSP2_DATA_MEM_LIMIT)
		panic("DSP MEMORY INITIALIZATION FILE BIGGER THAN MEMORY LIMIT @DSP2 DATA MEMORY");

	for( i = 0 ;  i < DSP2_DATA_MEM_LIMIT ;  ++i )
		dsp2_data_mem[ i ] = 0xBBBB ;

	for( i = 0 ;  i < DSP2_DM_size ;  ++i )
		dsp2_data_mem[ DSP2_DM_addr[i] ] = DSP2_DM_val[i] ;
	
#if defined( SC1445x_AE_SUPPORT_FAX )
	*dsp_hot_dl_sel_addr = 0 ;
#endif
}


static inline
void sc1445x_internal_collect_playback_stats( sc1445x_ae_channel_state* acs,
				unsigned short* len, unsigned short type )
{
#if defined( SC1445x_AE_COLLECT_STATISTICS )
	if( unlikely( MISSED_PACKET_TYPE == type ) )
		++acs->stats.missed_from_os ;

	if( unlikely( 0 == *len ) )
		++acs->stats.empty_to_dsp ;
	else {
		if( 1 == type ) {
			++acs->stats.normal_to_dsp ;
		} else if( 2 == type ) {
			++acs->stats.sid_to_dsp ;
		} else {
			++acs->stats.empty_to_dsp ;
			*len = 0 ;
		}
	}
#endif
}


#if defined( TEST_WITH_MARKED_PACKET )

#  define MARKED_PACKET_PERIOD		64

#  define MARKED_PACKET_INJECTOR_SIDE
#  define MARKED_PACKET_DETECTOR_SIDE

#  define DECLARE_PACKET_MARKER( name )					\
	static const unsigned char name[4] = {				\
		0xde, 0xad, 0xbe, 0xef					\
	}

#  if defined( MARKED_PACKET_DETECTOR_SIDE )

#    define DETECT_MARKED_PACKET_START( p, marker )			\
	do {								\
		if( !memcmp( p, marker, sizeof marker ) )		\
			DEBUG_WITH_GPIO_RAISE ;				\
	} while( 0 )

#    define DETECT_MARKED_PACKET_END( p, marker )			\
	do {								\
		if( !memcmp( p, marker, sizeof marker ) )		\
			DEBUG_WITH_GPIO_LOWER ;				\
	} while( 0 )

#  else  /* MARKED_PACKET_DETECTOR_SIDE */

#    define DETECT_MARKED_PACKET_START( p, marker )	do { } while( 0 )
#    define DETECT_MARKED_PACKET_END( p, marker )	do { } while( 0 )

#  endif  /* MARKED_PACKET_DETECTOR_SIDE */

#  if defined( MARKED_PACKET_INJECTOR_SIDE )

#    define INJECT_MARKED_PACKET( p, marker )				\
	do {								\
		if( !( sc1445x_internal_read_irq_cnt			\
					% MARKED_PACKET_PERIOD ) ) {	\
			memcpy( p, marker, sizeof marker ) ;		\
			DEBUG_WITH_GPIO_LOWER ;				\
		}							\
	} while( 0 )

#  else  /* MARKED_PACKET_INJECTOR_SIDE */

#    define INJECT_MARKED_PACKET( p, marker )	do { } while( 0 )

#  endif  /* MARKED_PACKET_INJECTOR_SIDE */

#else  /* TEST_WITH_MARKED_PACKET */

#  define DECLARE_PACKET_MARKER( name )			do { } while( 0 )
#  define DETECT_MARKED_PACKET_START( p, marker )	do { } while( 0 )
#  define DETECT_MARKED_PACKET_END( p, marker )		do { } while( 0 )
#  define INJECT_MARKED_PACKET( p, marker )		do { } while( 0 )

#endif  /* TEST_WITH_MARKED_PACKET */


#if defined( DUMP_PLAYBACK_PACKETS )

#  define DUMP_PACKET_START( p )					\
	do {								\
		static unsigned pkt_cnt = 0 ;				\
		if( !( pkt_cnt++ % 16 ) )				\
			PRINT( PRINT_LEVEL				\
				"* %02X %02X %02X %02X %02X %02X *\n",	\
				p[0], p[1], p[2], p[3], p[4], p[5] );	\
	} while( 0 )

#else  /* DUMP_PLAYBACK_PACKETS */

#  define DUMP_PACKET_START( p )			do { } while( 0 )

#endif  /* DUMP_PLAYBACK_PACKETS */


#if defined( DO_REVERSE_LOOPBACK )
static unsigned short reverse_loopback_buffer[SC1445x_AE_MAX_AUDIO_CHANNELS]
		[( SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER +
					SC1445x_AE_BYTES_PER_AUDIO_PACKET )] ;
#endif

/* copy data to/from DSP memory */
static inline
void sc1445x_internal_dsp_memcpy( volatile void* dst, const volatile void* src,
					unsigned short nbytes )
{
#if 0
	/* copy 16-bit words */
	unsigned short i, len = nbytes >> 1 ;
	volatile unsigned short* _dst = dst ;
	volatile const unsigned short* _src = src ;

	for( i = 0 ;  i < len ;  ++i )
		_dst[i] = _src[i] ;
#else
	/* use plain memcpy */
	memcpy( (void*)dst, (void*)src, nbytes ) ;
#endif
}

/* get the (dynamic) address of dsp1's iLBC playback buffer */
static inline
volatile unsigned short* sc1445x_internal_get_dsp1_ilbc_p_buf(
							unsigned short ch )
{
	static volatile unsigned short* dsp1_addr = (void*)0x131fe ;
	volatile unsigned short* dsp1_ilbc_p_buf =
					(void*)( (*dsp1_addr << 1) + 0x10000 ) ;
	unsigned p = (dsp1_ilbc_p_buf[ch] << 1) + 0x10000 ;

	return (void*)p ;
}

//#define USE_G729_FRAME_TYPE_4

/* copy an audio packet and its header from the "DMA" buffer to a DSP buffer */
static inline void copy_playback_audio_packet( sc1445x_ae_state* ae_state,
					unsigned short ch, unsigned char* src )
{
	sc1445x_ae_channel_state* acs = &ae_state->audio_channels[ch] ;
	unsigned short dsp_status, status = *(unsigned short*)src ;
	unsigned short i, sp, len = status & 0xff, offset ;
	unsigned short type, nchannels = ae_state->audio_channels_count ;
	unsigned short dec_type = acs->dec_codec.type ;
	short is_ilbc = 0 ;
#if defined( MARKED_PACKET_DETECTOR_SIDE )
	DECLARE_PACKET_MARKER( marker ) ;
#endif

	if( unlikely( acs->g711_1_buddy_dec_ch < nchannels ) )
		return ;  /* no decoder for this channel */

	if( SC1445x_AE_CODEC_iLBC_30ms == dec_type  ||
				SC1445x_AE_CODEC_iLBC_20ms == dec_type ) {
		/* stay in sync with the iLBC playback flow */
		const unsigned short ilbc20_status = 0x126 ;
		const unsigned short ilbc30_status = 0x132 ;
		unsigned short valid_status =
				SC1445x_AE_CODEC_iLBC_20ms == dec_type ?
						ilbc20_status :  ilbc30_status ;

		is_ilbc = 1 ;
		/* are we in sync? */
		if( unlikely( !acs->playback_sync ) ) {
			if( status != valid_status ) {
				/* ...not yet */
				*sc1445x_ae_playback_ch_status[ch] = 0 ;
				status = 0 ;
			} else {
				/* ...sync now! */
				acs->playback_sync = 1 ;
				acs->playback_counter = 0 ;
			}
		} else {
			++acs->playback_counter ;

			if( acs->playback_counter  &&
					acs->playback_counter <
						acs->playback_cnt_lim ) {
				if( 0 != status ) {
					//DPRINT( "iLBC P header is %x when "
					//	"expecting 0 -- resync\n",
					//			status ) ;
					acs->playback_counter = 0 ;
					//status = 0; //NEED TO BE DONE?
				}
			} else {
				acs->playback_counter = 0 ;

				if( 0 == status ) {
					//DPRINT( "iLBC P header is 0 when exp"
					//	"ecting %x\n", valid_status ) ;
					acs->playback_sync = 0 ;
					/* set status to trigger PLC */
					status = valid_status & 0x00ff ;
				}
			}
		}
	}

	/* there might be up to 2 sub-packets */
	for( offset = 0, sp = 0 ;  sp < 2 ;  ++sp ) {
		volatile unsigned short* ch_status ;
		unsigned short* dst ;
		unsigned rate ;

		if( !acs->is_g711_1_dec ) {
			dst = (unsigned short*)acs->playback_buffer ;
			rate = acs->dec_codec.rate ;
		} else {
			dst = (unsigned short*)
				sc1445x_ae_g711_1_playback_bufs[ch][sp] ;
			rate = ae_state->audio_channels
				[acs->g711_1_buddy_enc_ch].dec_codec.rate ;
		}

		ch_status = !acs->is_g711_1_dec ?
				sc1445x_ae_playback_ch_status[ch] :
				sc1445x_ae_g711_1_playback_ch_status[ch][sp] ;

		DUMP_PACKET_START( src ) ;

		DETECT_MARKED_PACKET_START(
				src + SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER,
				marker ) ;

		type = status >> 8 ;
		if( unlikely( ( type > 2  &&  type != MISSED_PACKET_TYPE )
					||  len > rate ) ) {
			/* invalid packet */
			PRINT( KERN_DEBUG "%s: invalid header %04x\n",
					__FUNCTION__, status ) ;
			status = 0 ;
			len = 0 ;
			type = 0 ;
		}

#if 1
		if( unlikely( ae_state->updating_filters ) ) {
			DPRINT( PRINT_LEVEL "%s: skipping packet\n",
					__FUNCTION__ ) ;
			status = 0 ;  /* mask out this packet */
		}
#endif

		if( unlikely( MISSED_PACKET_TYPE == type ) ) {
			/* force frame type 0 */
			dsp_status = 0 ;
		} else {
			dsp_status = status ;
		}

#if defined( USE_G729_FRAME_TYPE_4 )
		if( ( SC1445x_AE_CODEC_G729 == dec_type )  &&
				( 0 == ( dsp_status & 0xff00 ) ) ) {
			/* use special frame type 4 instead of 0, for G.729 */
			dsp_status |= 0x400 ;
		}
#endif

		if( acs->is_g711_1_dec ) {
			/* DSP expects length as number of 16-bit words */
			dsp_status &= ~0xff ;
			dsp_status |= (len >> 1) & 0xff ;
		}
		*ch_status = dsp_status ;
#if defined( DO_REVERSE_LOOPBACK )
		reverse_loopback_buffer[ch][offset] = dsp_status ;
#endif

#if defined( CONFIG_SC1445x_CVQ_METRICS ) \
		|| defined( CONFIG_SC1445x_CVQ_METRICS_MODULE )
		if( cvqm_cbs.channel_tick ) {
			unsigned short cvqm_type ;
#  if defined( TEST_WITH_MARKED_PACKET ) \
			&& defined( MARKED_PACKET_DETECTOR_SIDE )
			short mark = !memcmp(
					src + SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER,
					marker, sizeof marker ) ;

			if( mark )
				DEBUG_WITH_GPIO_LOWER ;
#  endif

			if( unlikely( MISSED_PACKET_TYPE == type ) )
				cvqm_type = 0 ;
			else
				cvqm_type = type ;
			cvqm_cbs.channel_tick( ch,
					SC1445X_CVQM_CHANNEL_DIR_PLAYBACK, cvqm_type ) ;

#  if defined( TEST_WITH_MARKED_PACKET ) \
			&& defined( MARKED_PACKET_DETECTOR_SIDE )
			if( mark )
				DEBUG_WITH_GPIO_RAISE ;
#  endif
		}
#endif

		sc1445x_internal_collect_playback_stats( acs, &len, type ) ;

		if( len  >  SC1445x_AE_BYTES_PER_AUDIO_PACKET - offset ) {
			PRINT( KERN_DEBUG "%s: invalid len=%d (%x)\n",
					__FUNCTION__, len, len ) ;
			/* clip */
			len = SC1445x_AE_BYTES_PER_AUDIO_PACKET - offset ;
		}

#if defined( VOIP_PLAYBACK_LOG )
		if( *voip_playback_log_enable[ch] &&
				likely( voip_playback_log_pos[ch] <
					voip_playback_log_end[ch] ) ) {
			int ll = len + SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER ;

			memcpy( voip_playback_log_pos[ch], src, ll ) ;
			if( acs->do_release ) {
				voip_playback_log_pos[ch][1] |= 0x10 ;
			}
			voip_playback_log_pos[ch] += ll ;
		} else if( unlikely( *voip_playback_log_enable[ch] ) ) {
			PRINT( PRINT_LEVEL "VoIP playback log buffer #%d is full "
					"(%p - %p) -- no more logging\n", ch,
					voip_playback_log_buf[ch],
					voip_playback_log_end[ch] ) ;
			*voip_playback_log_enable[ch] = 0 ;  /* denote end of logging */
		}
#endif

		src += SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER ;
		offset += SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER ;
		if( is_ilbc || acs->is_g711_1_dec ) {
			memcpy( dst, src, len ) ;
		} else {
			/* the DSP wants each sample in a 16-bit word */
			for( i = 0 ;  i < len ;  ++i ) {
				unsigned short sample ;

				sample = src[i] ;
				dst[i] = sample ;
#if defined( DO_REVERSE_LOOPBACK )
				reverse_loopback_buffer[ch][offset + i + 1] =
									sample ;
#endif
			}
		}

		DETECT_MARKED_PACKET_END( src, marker ) ;

		/* invalidate packet (make frame type 0) */
		src -= SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER ;
		*(unsigned short*)src = (status & 0x00ff)
			| ( MISSED_PACKET_TYPE << 8 ) ;

		if( !acs->is_g711_1_dec )
			break ;  /* there is only 1 sub-packet */

		/* there is more... */
		src += SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER + len ;
		offset += len ;
		status = *(unsigned short*)src ;
		len = status & 0xff ;
	}

#if defined( PCM_PLAYBACK_LOG )
	if( pcm_playback_log_enable[ch] &&
				likely( pcm_playback_log_pos[ch] <
						pcm_playback_log_end[ch] ) ) {

		memcpy( pcm_playback_log_pos[ch], pcm_playback_src_buf[ch],
					160 << !!currently_wideband() ) ;
		pcm_playback_log_pos[ch] += 160 << !!currently_wideband() ;
	} else if( unlikely( pcm_playback_log_enable[ch] ) ) {
		PRINT( PRINT_LEVEL "PCM playback log buffer #%d is full "
				"(%p - %p) -- no more logging\n", ch,
				pcm_playback_log_buf[ch],
				pcm_playback_log_end[ch] ) ;
		pcm_playback_log_enable[ch] = 0 ;  /* denote end of logging */
	}
#endif
}

#if defined( SC1445x_AE_SUPPORT_FAX )
/* copy a fax packet and its header from the "DMA" buffer to a DSP buffer */
static inline void copy_playback_fax_packet( sc1445x_ae_state* ae_state,
					unsigned short ch, unsigned char* src )
{
	sc1445x_ae_channel_state* acs = &ae_state->audio_channels[ch] ;
	unsigned short status = *(unsigned short*)src ;
	unsigned short len = status & 0xff ;
	unsigned short* dst = (unsigned short*)acs->playback_buffer ;
	unsigned short type ;

	type = status >> 8 ;
	if( unlikely( type != 1  ||  len != 80 ) ) {
		/* invalid packet */
		snd_printd( "%s: invalid header %04x\n",
						__FUNCTION__, status ) ;
		status = 0 ;
		len = 0 ;
		type = 0 ;
	}

	--dst ;		/* start from the header address */

	sc1445x_internal_collect_playback_stats( acs, &len, type ) ;

	if( likely( len > 0 ) )
		memcpy( dst, src + SC1445x_AE_BYTES_PER_FAX_PACKET_HEADER,
					SC1445x_AE_BYTES_PER_FAX_PACKET ) ;

	/* invalidate packet (make frame type 0) */
	*(unsigned short*)src = status & 0x00ff ;
}
#endif

#if 1
static unsigned char fake_ring_buffer[20 * SC1445x_AE_MAX_AUDIO_CHANNELS *
				( SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER +
					SC1445x_AE_BYTES_PER_AUDIO_PACKET )] ;

static snd_pcm_runtime_t fake_runtime = {
	.dma_area =	fake_ring_buffer,
	.buffer_size =	sizeof( fake_ring_buffer ),
} ;
#endif

#define PLAYBACK_INJECT_FAULT_CONDITION					\
			( 2 > (sc1445x_internal_write_irq_cnt % 8) )

#define INJECT_FAULT_DONT_SERVE_WRITE_IRQ( ae_state, nch )		\
	do {								\
		struct snd_sc1445x* alsa_chip =				\
			(struct snd_sc1445x*)ae_state->private_data ;	\
		unsigned short ch ;					\
									\
		for( ch = 0 ;  ch < nch ;  ++ch ) {			\
			*sc1445x_ae_playback_ch_status[ch] = 0 ;	\
									\
			alsa_chip->playback_r_pos +=			\
				SC1445x_AE_BYTES_PER_AUDIO_PACKET +	\
				SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER ;\
			if( alsa_chip->playback_r_pos >=		\
						runtime->buffer_size ){	\
				alsa_chip->playback_r_pos = 0 ;		\
			}						\
		}							\
	} while( 0 )

/* feed audio packets to the DSPs */
static void generic_put_playback_packets( sc1445x_ae_state* ae_state )
{
	static const short inject_fault_dont_serve_write_irq = 0 ;
	struct snd_sc1445x* alsa_chip =
				(struct snd_sc1445x*)ae_state->private_data ;
	snd_pcm_runtime_t* runtime = alsa_chip->playback_substream->runtime ;
	unsigned short ch, nch = ae_state->audio_channels_count ;

#if 0
	runtime = &fake_runtime ;
#endif
	++sc1445x_internal_write_irq_cnt ;

	/* start writing to playback buffers */
	*sc1445x_ae_playback_status = 1 ;

	if( inject_fault_dont_serve_write_irq  &&
					PLAYBACK_INJECT_FAULT_CONDITION ) {
		INJECT_FAULT_DONT_SERVE_WRITE_IRQ( ae_state, nch ) ;

#if defined( SC1445x_AE_COLLECT_STATISTICS )
		for( ch = 0 ;  ch < nch ;  ++ch )
			++ae_state->audio_channels[ch].stats.empty_to_dsp ;
#endif
		*sc1445x_ae_playback_status = 0 ;
		snd_pcm_period_elapsed( alsa_chip->playback_substream ) ;
		return ;
	}


	for( ch = 0 ;  ch < nch ;  ++ch ) {
		sc1445x_ae_channel_state* acs ;
		const unsigned short ch_sz =
				SC1445x_AE_BYTES_PER_AUDIO_PACKET +
				SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER ;

		if( unlikely( NULL == runtime  ||  !runtime->buffer_size ) ) {
			*sc1445x_ae_playback_ch_status[ch] = 0 ;
			continue ;
		}

		acs = &ae_state->audio_channels[ch] ;
		if( acs->is_active ) {
#if defined( SC1445x_AE_SUPPORT_FAX )
			if( acs->is_fax ) {
				/* fax data comes after the audio data */
				const unsigned short fax_offset =
					alsa_chip->playback_r_pos ;

				/* support one fax channel fow now */
				copy_playback_fax_packet( ae_state, ch,
					runtime->dma_area + fax_offset ) ;
			} else
			/* FALL THROUGH */
#endif
			if( !acs->is_raw_pcm ) {
				int offset = alsa_chip->playback_r_pos ;
				unsigned short enc_ch =
						acs->g711_1_buddy_enc_ch ;

				if( enc_ch < nch ) {
					/*
					 * the data for the decoder are
					 * in enc_ch
					 */
					short diff_ch = enc_ch - ch ;

					offset += diff_ch * ch_sz ;
				}

				copy_playback_audio_packet( ae_state, ch,
						runtime->dma_area + offset ) ;
			}
		}
		alsa_chip->playback_r_pos += ch_sz ;
		if( alsa_chip->playback_r_pos >= runtime->buffer_size ) {
			if( alsa_chip->playback_r_pos > runtime->buffer_size ) {
				PRINT( PRINT_LEVEL "%s: ch=%d, "
					"playback_r_pos=%d, buffer_size=%lu\n",
						__FUNCTION__, ch,
						alsa_chip->playback_r_pos,
						runtime->buffer_size ) ;
			}
			alsa_chip->playback_r_pos = 0 ;
		}
	}

	if( 1 == *sc1445x_ae_playback_status ) {
		/* finished writing to playback buffers */
		*sc1445x_ae_playback_status = 0 ;
	} else {
		PRINT( KERN_DEBUG "%s: TOO LATE -- MISSED PACKET\n",
							__FUNCTION__ ) ;
	}

	snd_pcm_period_elapsed( alsa_chip->playback_substream ) ;
}


/* copy an audio packet and its header from a DSP buffer to the "DMA" buffer */
static inline void copy_capture_audio_packet( sc1445x_ae_state* ae_state,
					unsigned short ch, unsigned char* dst )
{
	sc1445x_ae_channel_state* acs = &ae_state->audio_channels[ch] ;
	unsigned short header ;
	unsigned short i, sp, len ;
	const unsigned short* src ;
	unsigned short enc_type = acs->enc_codec.type ;
#if defined( SC1445x_AE_COLLECT_STATISTICS )
	unsigned short type ;
#endif
	unsigned short offset, nchannels = ae_state->audio_channels_count ;
	short is_ilbc = 0 ;

#if defined( MARKED_PACKET_INJECTOR_SIDE )
	DECLARE_PACKET_MARKER( marker ) ;
#endif

	if( unlikely( acs->g711_1_buddy_enc_ch < nchannels ) )
		return ;  /* no encoder for this channel */

	for( offset = 0, sp = 0 ;  sp < 2 ;  ++sp ) {
#if defined( DO_REVERSE_LOOPBACK )
		header = reverse_loopback_buffer[ch][offset] ;
		src = reverse_loopback_buffer[ch] + offset + 1 ;
#else
		if( !acs->is_g711_1_enc ) {
			header = *sc1445x_ae_capture_ch_header[ch] ;
			src = (unsigned short*)acs->capture_buffer ;
		} else {
			/*
			 * DSP provides the length in 16-bit words
			 * change it to bytes
			 */
			header = *sc1445x_ae_g711_1_capture_ch_header[ch][sp] ;
			len = header & 0xff ;
			len <<= 1 ;
			header &= ~0xff ;
			header |= len & 0xff ;
			src = (unsigned short*)
					sc1445x_ae_g711_1_capture_bufs[ch][sp] ;
		}
#endif
		/* frame length, in bytes */
		len = header & 0xff ;
#if defined( SC1445x_AE_COLLECT_STATISTICS )
		type = header >> 8 ;
#endif

		if( SC1445x_AE_CODEC_iLBC_30ms == enc_type  ||
				SC1445x_AE_CODEC_iLBC_20ms == enc_type ) {
			is_ilbc = 1 ;
			/*
			 * stay in sync with the iLBC capture
			 * flow (i.e. from DSP)
			 */

			/* are we in sync? */
			if( unlikely( !acs->capture_sync ) ) {
				if( 0 == header ) {
					/* ...not yet */
					*(unsigned short*)dst = 0 ;
				} else {
					/* ...sync now! */
					acs->capture_sync = 1 ;
					acs->capture_counter = 0 ;
				}
			} else {
				++acs->capture_counter ;

				if( acs->capture_counter  &&
						acs->capture_counter <
						acs->capture_cnt_lim ) {
					if( 0 != header ) {
						DPRINT( PRINT_LEVEL "iLBC "
							"capture header is %x "
							"when expecting 0\n",
							header ) ;
						acs->capture_sync = 0 ;
					}

					header = len = type = 0 ;
				} else {
					acs->capture_counter = 0 ;

					if( 0 == header ) {
						DPRINT( PRINT_LEVEL "iLBC "
							"capture header is 0 "
							"when expecting "
							"!=0\n" ) ;
						acs->capture_sync = 0 ;
						header = len = type = 0 ;
					}
				}
			}
		}

		*(unsigned short*)dst = header ;

		if( len  >  SC1445x_AE_BYTES_PER_AUDIO_PACKET - offset ) {
			PRINT( PRINT_LEVEL "%s: invalid len=%d (%x)\n",
					__FUNCTION__, len, len ) ;
			/* clip */
			len = SC1445x_AE_BYTES_PER_AUDIO_PACKET - offset ;
		}

#if defined( CONFIG_SC1445x_CVQ_METRICS ) \
		|| defined( CONFIG_SC1445x_CVQ_METRICS_MODULE )
		if( cvqm_cbs.channel_tick ) {
#  if defined( TEST_WITH_MARKED_PACKET ) \
			&& defined( MARKED_PACKET_INJECTOR_SIDE )
			short mark = !( sc1445x_internal_read_irq_cnt
					% MARKED_PACKET_PERIOD ) ;

			if( mark )
				DEBUG_WITH_GPIO_LOWER ;
#  endif

			cvqm_cbs.channel_tick( ch,
				SC1445X_CVQM_CHANNEL_DIR_CAPTURE, type ) ;

#  if defined( TEST_WITH_MARKED_PACKET ) \
			&& defined( MARKED_PACKET_INJECTOR_SIDE )
			if( mark )
				DEBUG_WITH_GPIO_RAISE ;
#  endif
		}
#endif

#if defined( SC1445x_AE_COLLECT_STATISTICS )
		if( unlikely( 0 == len ) )
			++acs->stats.empty_from_dsp ;
		else {
			if( 1 == type ) {
				++acs->stats.normal_from_dsp ;
			} else if( 2 == type ) {
				++acs->stats.sid_from_dsp ;
			} else {
				++acs->stats.empty_from_dsp ;
			}
		}
#endif

		dst += SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER ;
		offset += SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER ;
		if( is_ilbc ) {
			memcpy( dst, src, len ) ;
#ifdef iLBC_LOG

			switch(ch){
				case 0:
					if( likely( iLBC_capture_pos1 <
							iLBC_capture_end1 ) ) {
						memcpy( iLBC_capture_pos1, src,
									len ) ;
						iLBC_capture_pos1 += len ;
					} else {
						PRINT( PRINT_LEVEL "iLBC BUFFER"
							" IS FULL (%p - %p) -- "
							"no more logging\n",
							iLBC_capture_buf1,
							iLBC_capture_end1 ) ;
					}
					break;

				case 1:
					if( likely( iLBC_capture_pos2 <
							iLBC_capture_end2 ) ) {
						memcpy( iLBC_capture_pos2, src,
									len ) ;
						iLBC_capture_pos2 += len ;
					} else {
						PRINT( PRINT_LEVEL "iLBC BUFFER"
							" IS FULL (%p - %p) -- "
							"no more logging\n",
							iLBC_capture_buf2,
							iLBC_capture_end2 ) ;
					}
					break;

				case 2:
					if( likely( iLBC_capture_pos3 <
							iLBC_capture_end3 ) ) {
						memcpy( iLBC_capture_pos3, src,
								len ) ;
						iLBC_capture_pos3 += len ;
					} else {
						PRINT( PRINT_LEVEL "iLBC BUFFER"
							" IS FULL (%p - %p) -- "
							"no more logging\n",
							iLBC_capture_buf3,
							iLBC_capture_end3 ) ;
					}
					break;	

				default:
					break;
			}

#endif
		} else if( acs->is_g711_1_enc ) {
			memcpy( dst, src, len ) ;
		} else if( unlikely( SC1445x_AE_CODEC_G729_VAD == enc_type
					&&  2 == type ) ) {
			/* DSP provides the data (16 bits) packed */
			unsigned short sid = src[0] ;

			dst[0] = sid >> 8 ;
			dst[1] = sid & 0xff ;
		} else {
			/* the DSP provides each sample in a 16-bit word */
			for( i = 0 ;  i < len ;  ++i ) {
				unsigned char sample ;

				sample = src[i] ;
				dst[i] = sample ;
			}
		}

		INJECT_MARKED_PACKET( dst, marker ) ;

		if( !acs->is_g711_1_enc )
			break ;  /* there is only 1 sub-packet */

		/* there is more... */
		dst += len ;
		offset += len ;
	}
}


#if defined( SC1445x_AE_SUPPORT_FAX )
/* copy an audio packet and its header from a DSP buffer to the "DMA" buffer */
static inline void copy_capture_fax_packet( sc1445x_ae_state* ae_state,
					unsigned short ch, unsigned char* dst )
{
	unsigned short header = *sc1445x_ae_capture_ch_header[ch] ;
	unsigned short len = header & 0xff ;
	unsigned short* src = (unsigned short*)
				ae_state->audio_channels[ch].capture_buffer ;
#  if defined( SC1445x_AE_COLLECT_STATISTICS )
	unsigned short type = 1 ;
#  endif

	header = 1 ;

	*(unsigned short*)dst = header ;

	if( len > SC1445x_AE_BYTES_PER_FAX_PACKET ) {
		PRINT( PRINT_LEVEL "%s: invalid len=%d (%x)\n",
				__FUNCTION__, len, len ) ;
		/* clip */
		len = SC1445x_AE_BYTES_PER_AUDIO_PACKET ;
	}

#  if defined( SC1445x_AE_COLLECT_STATISTICS )
	if( unlikely( 0 == len ) )
		++ae_state->audio_channels[ch].stats.empty_from_dsp ;
	else {
		if( 1 == type ) {
			++ae_state->audio_channels[ch].stats.normal_from_dsp ;
		} else if( 2 == type ) {
			++ae_state->audio_channels[ch].stats.sid_from_dsp ;
		} else {
			++ae_state->audio_channels[ch].stats.empty_from_dsp ;
		}
	}
#  endif

	--src ;  /* start from the header address */
	memcpy( dst + SC1445x_AE_BYTES_PER_FAX_PACKET_HEADER, src,
					SC1445x_AE_BYTES_PER_FAX_PACKET ) ;
#  ifdef PCM_LOG
	if( likely( fax_pcm_playback_pos < fax_pcm_playback_end ) ) {
		memcpy( fax_pcm_playback_pos, (void*)PLAY_PCM_LOG, 160 ) ;
		fax_pcm_playback_pos += 160 ;
	} else {
		PRINT( PRINT_LEVEL "FAX PCM PLAYBACK BUFFER IS FULL (%p - %p)"
						" -- no more logging\n",
				fax_pcm_playback_buf, fax_pcm_playback_end ) ;
	}

	if( likely( fax_pcm_capture_pos < fax_pcm_capture_end ) ) {
		memcpy( fax_pcm_capture_pos, (void*)CAP_PCM_LOG,
				*( (unsigned short *)CAP_PCM_LOG + 1 ) + 6 ) ;
		fax_pcm_capture_pos +=
				*( (unsigned short *)CAP_PCM_LOG + 1 ) + 6 ;
	} else {
		PRINT( PRINT_LEVEL "FAX PCM CAPTURE BUFFER IS FULL (%p - %p)"
						" -- no more logging\n",
				fax_pcm_capture_buf, fax_pcm_capture_end ) ;
	}
#  endif
}
#endif  /* SC1445x_AE_SUPPORT_FAX */


/* get the audio packets from the DSPs */
static void generic_get_capture_packets( sc1445x_ae_state* ae_state )
{
	struct snd_sc1445x* alsa_chip =
				(struct snd_sc1445x*)ae_state->private_data ;
	snd_pcm_runtime_t* runtime = alsa_chip->capture_substream->runtime ;
	unsigned short ch, nch = ae_state->audio_channels_count ;

#if 0
	runtime = &fake_runtime ;
#endif
	++sc1445x_internal_read_irq_cnt ;

	for( ch = 0 ;  ch < nch ;  ++ch ) {
		if( unlikely( NULL == runtime  ||  !runtime->buffer_size ) ) {
			runtime->dma_area[alsa_chip->capture_w_pos] = 0 ;
			runtime->dma_area[alsa_chip->capture_w_pos + 1] = 0 ;
			continue ;
		}

		if( ae_state->audio_channels[ch].is_active  &&
				!ae_state->audio_channels[ch].is_raw_pcm ) {
#if defined( SC1445x_AE_SUPPORT_FAX )
			if( ae_state->audio_channels[ch].is_fax ) {
				/* fax data comes after the audio data */
				const unsigned short fax_offset =
					alsa_chip->capture_w_pos ;

				/* support one fax channel fow now */
				copy_capture_fax_packet( ae_state, ch,
					runtime->dma_area + fax_offset ) ;
			} else
			/* FALL THROUGH */
#endif
			copy_capture_audio_packet( ae_state, ch,
					runtime->dma_area +
						alsa_chip->capture_w_pos ) ;
		}
		else {
			/* just write the header */
			runtime->dma_area[alsa_chip->capture_w_pos] = 0 ;
			runtime->dma_area[alsa_chip->capture_w_pos + 1] = 0 ;
		}
		alsa_chip->capture_w_pos += SC1445x_AE_BYTES_PER_AUDIO_PACKET +
				SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER ;
		if( alsa_chip->capture_w_pos >= runtime->buffer_size ) {
			if( alsa_chip->capture_w_pos > runtime->buffer_size ) {
				PRINT( PRINT_LEVEL "%s: ch=%d, "
					"capture_w_pos=%d, buffer_size=%lu\n",
						__FUNCTION__, ch,
						alsa_chip->capture_w_pos,
						runtime->buffer_size ) ;
			}
			alsa_chip->capture_w_pos = 0 ;
		}
	}

	*sc1445x_ae_capture_status = 0 ;

	snd_pcm_period_elapsed( alsa_chip->capture_substream ) ;
}


/* start/end pointers for autoplay mode */
/* the format of the stored audio is as follows: */
/*	- 16-bit header (MSByte=type, LSByte=length) */
/*	- audio data (#samples as denoted in the header) */
static unsigned short* autoplay_startp[SC1445x_AE_MAX_AUDIO_CHANNELS] ;
static unsigned short* autoplay_endp[SC1445x_AE_MAX_AUDIO_CHANNELS] ;

/* read pointers for the autoplay mode */
static unsigned short* autoplay_readp[SC1445x_AE_MAX_AUDIO_CHANNELS] ;


/* feed stored audio packets to the DSPs */
static void generic_put_autoplayback_packets( sc1445x_ae_state* ae_state )
{
	unsigned short ch, nch = ae_state->audio_channels_count ;

	++sc1445x_internal_write_irq_cnt ;

	/* start writing to playback buffers */
	*sc1445x_ae_playback_status = 1 ;

	for( ch = 0 ;  ch < nch ;  ++ch ) {
		unsigned short header ;

		if( !ae_state->audio_channels[ch].is_active  ||
							!autoplay_readp[ch] )
			continue ;

		header = *autoplay_readp[ch] ;
		++autoplay_readp[ch] ;
		memcpy( ae_state->audio_channels[ch].playback_buffer,
				autoplay_readp[ch], (header & 0xff) << 1 ) ;
		*sc1445x_ae_playback_ch_status[ch] = header ;

		/* advance read pointer and wrap around if we reached the end */
		autoplay_readp[ch] += header & 0xff ;
		if( autoplay_readp[ch] >= autoplay_endp[ch] )
			autoplay_readp[ch] = autoplay_startp[ch] ;
	}

	/* finished writing to playback buffers */
	*sc1445x_ae_playback_status = 0 ;
}


void sc1445x_private_stop_raw_pcm( void )
{
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	/* clear bank used by the DSP */
	memset( (void*)0x18000, 0, 0x280 ) ;
	/* tell the DSPs to stop playing raw PCM */
	sc1445x_internal_send_dsp_cmd( 0x0004, 0x0000 ) ;
#endif
}


static inline
void detect_lost_dsp_ints( void )
{
#if defined( CHECK_LOST_DSP_INTS )
	/* see if the dsp says we've lost interrupts */
	static unsigned short lost_interrupts = 0 ;
	unsigned short n = *(unsigned short*)0x1b002 ;

	if( unlikely( n != lost_interrupts ) ) {
		PRINT( PRINT_LEVEL "lost DSP ints: %d\n", n ) ;
		lost_interrupts = n ;
	}
#endif
}


#if defined( SC1445x_AE_SUPPORT_FAX )
#  include "DSP2_Phone/t38_Channel0/V21.PM"
#  include "DSP2_Phone/t38_Channel0/V21.DM"
#  include "DSP2_Phone/t38_Channel0/V2x.PM"
#  include "DSP2_Phone/t38_Channel0/V2x.DM"
//V17 IS NOT INCLUDED IN DEMO VERSION

#  include "DSP2_Phone/t38_Channel1/V21.PM"
#  include "DSP2_Phone/t38_Channel1/V21.DM"
#  include "DSP2_Phone/t38_Channel1/V2x.PM"
#  include "DSP2_Phone/t38_Channel1/V2x.DM"


/* hot download DSP firmware */
static
void sc1445x_internal_dsp_hot_download( const sc1445x_ae_state* ae_state )
{
	static volatile unsigned short* dsp_hot_dl_sel_addr = (void*)0x1AF40;
	unsigned short sel = *dsp_hot_dl_sel_addr ;
	const unsigned short* dsp2_pm_addr ;
	const unsigned short* dsp2_pm_val ;
	unsigned short dsp2_pm_size ;
	const unsigned short* dsp2_dm_addr ;
	const unsigned short* dsp2_dm_val ;
	unsigned short dsp2_dm_size ;
	unsigned short i ;
	unsigned short channel;
	/* For FAX struct initialization */
	const unsigned short encoder_buff_base_addr[3] = { 0, 0x7e0, 0xfc0 } ;
	static volatile unsigned short* my_fmdp_handle_p = (void*)0x1b032 ;
	static volatile unsigned short* more_fax_vars_p = (void*)0x1b02e ;
	unsigned short* mem;

	for( i = 0 ;  i < ae_state->audio_channels_count ;  ++i )
		if( ae_state->audio_channels[i].is_fax )
			break ;
	DPRINT(PRINT_LEVEL "FAX IS ON CHANNEL:%d\n",i);
	if( ae_state->audio_channels_count == i )
		return ;

	channel = i;

	switch( sel ) {
		case 1:
			DPRINT( PRINT_LEVEL "DOWNLOADING V21\n") ;
			dsp2_pm_addr =	i ?  V21_PM_addr_ch1 :  V21_PM_addr_ch0;
			dsp2_pm_val =	i ?  V21_PM_val_ch1  :  V21_PM_val_ch0;
			dsp2_pm_size =	i ?  V21_PM_size_ch1 :  V21_PM_size_ch0;
			dsp2_dm_addr =	i ?  V21_DM_addr_ch1 :  V21_DM_addr_ch0;
			dsp2_dm_val =	i ?  V21_DM_val_ch1  :  V21_DM_val_ch0;
			dsp2_dm_size =	i ?  V21_DM_size_ch1 :  V21_DM_size_ch0;
			break ;

		case 2:
			DPRINT( PRINT_LEVEL "DOWNLOADING V2X\n") ;
			dsp2_pm_addr =	i ?  V2x_PM_addr_ch1 :  V2x_PM_addr_ch0;
			dsp2_pm_val =	i ?  V2x_PM_val_ch1  :  V2x_PM_val_ch0;
			dsp2_pm_size =	i ?  V2x_PM_size_ch1 :  V2x_PM_size_ch0;
			dsp2_dm_addr =	i ?  V2x_DM_addr_ch1 :  V2x_DM_addr_ch0;
			dsp2_dm_val =	i ?  V2x_DM_val_ch1  :  V2x_DM_val_ch0;
			dsp2_dm_size =	i ?  V2x_DM_size_ch1 :  V2x_DM_size_ch0;
			break ;
#if 0
		case 3:
		PRINT( PRINT_LEVEL "DOWNLOADING V17\n") ;
			dsp2_pm_addr = V17_PM_addr ;
			dsp2_pm_val = V17_PM_val ;
			dsp2_pm_size = V17_PM_size ;
			dsp2_dm_addr = V17_DM_addr ;
			dsp2_dm_val = V17_DM_val ;
			dsp2_dm_size = V17_DM_size ;
			break ;
#endif
		default:
			PRINT( PRINT_LEVEL "%s: invalid firmware download "
					"selector %d\n", __FUNCTION__, sel ) ;
			return ;
	}

#if 0
	SetWord( DSP2_CTRL_REG, 0x0000 ) ;
#endif

	/* update program memory of DSP2 */
	for( i = 0 ;  i < dsp2_pm_size ;  ++i )
		dsp2_prog_mem[ dsp2_pm_addr[i] ] = dsp2_pm_val[i] ;

	/* update data memory of DSP2 */
	for( i = 0 ;  i < dsp2_dm_size ;  ++i )
		dsp2_data_mem[ dsp2_dm_addr[i] ] = dsp2_dm_val[i] ;

	/************/
	//initalize dsp's more fax variable struct.
	*my_fmdp_handle_p = encoder_buff_base_addr[channel+1];
	*more_fax_vars_p = *my_fmdp_handle_p + 1205;

	mem = (unsigned short*)(0x18000  +  2 * (*more_fax_vars_p) ) ;
	//PRINT("mem start:%x\n",mem);
	for( i = 8 ;  i < 8 + 243 ;  i++ )
		mem[i] = 0;
	//PRINT("mem start:%x\n",&mem[i]);
	/*************/
#if 0
	SetWord( DSP2_IRQ_START_REG, 0x0000 ) ;
	SetWord( DSP2_PC_START_REG, 0x0010 ) ;
	SetWord( DSP2_INT_MASK_REG, 0x0302 ) ;
	SetWord( DSP2_CTRL_REG, 0x0004 ) ;
#endif	
}
#endif  /* SC1445x_AE_SUPPORT_FAX */


#if defined( CHECK_DSP_IRQ_OVERFLOW )
struct dsp_irq_overflow_stats {
	unsigned irq_of ;
	unsigned wtf_of ;
	unsigned int_of[5] ;
} ;

static struct dsp_irq_overflow_stats dsp_irq_overflows[2] ;
#endif


extern void sc1445x_private_tone_det_add_event( int event, unsigned timestamp );


/*
 * helper function for getting the effective iface_mode for accessing the
 * audio profile settings
 */
static inline sc1445x_ae_iface_mode
get_audio_profile_iface_mode( const sc1445x_ae_state* ae_state )
{
	sc1445x_ae_iface_mode im ;

	if( SC1445x_AE_IFACE_MODE_BT_GSM == ae_state->iface_mode ) {
		/* assume SC1445x_AE_PCM_DEV_COUNT is 1 */
		if( !ae_state->pcm_dev[0].attached ) {
			PRINT( PRINT_LEVEL
				"%s: iface_mode is SC1445x_AE_IFACE_MODE_BT_GSM"
				" but no PCM devices are attached!\n",
								__FUNCTION__ ) ;
		}

		switch( ae_state->pcm_dev[0].lep ) {
			case SC1445x_AE_PCM_LOCAL_EP_HANDSET:
				im = SC1445x_AE_IFACE_MODE_HANDSET ;
				break ;

			case SC1445x_AE_PCM_LOCAL_EP_HANDS_FREE:
				im = SC1445x_AE_IFACE_MODE_HANDS_FREE ;
				break ;

			case SC1445x_AE_PCM_LOCAL_EP_OPEN_LISTENING:
				im = SC1445x_AE_IFACE_MODE_OPEN_LISTENING ;
				break ;

			default:
				/* play it safe */
				PRINT( PRINT_LEVEL
					"%s: unknown local endpoint using "
						"handset!\n", __FUNCTION__ ) ;
				im = SC1445x_AE_IFACE_MODE_HANDSET ;
				break ;
		}
	} else if( SC1445x_AE_IFACE_MODE_BT_HEADSET == ae_state->iface_mode ) {
		im = SC1445x_AE_IFACE_MODE_WIRELESS ;
	} else if( SC1445x_AE_IFACE_MODE_IDLE == ae_state->iface_mode ) {
		im = SC1445x_AE_IFACE_MODE_HANDSET ;
	} else
		im = ae_state->iface_mode ;

	return im ;
}

/* interrupt handler for DSP1 interrupts */
static irqreturn_t generic_dsp1_isr_normal( int irq, void* devid )
{
	irqreturn_t ret = IRQ_NONE ;
#if defined( SMOOTH_IFACE_TRANSITION ) || defined( SC1445x_AE_ATA_SUPPORT ) \
					|| defined( CHECK_DSP_IRQ_OVERFLOW )
#  if defined( CHECK_DSP_IRQ_OVERFLOW )
	unsigned short overflow, mask, source ;
#  endif
	unsigned short dsp1_int_vector ;
	unsigned short dsp1_handled_ints = 0 ;
	short ack_dsp1_int = 0 ;
	sc1445x_ae_state* ae_state = devid ;

#  if defined( TRACE_AUDIOEGINE_ISR )
	AUDIOENGINE_TRACE_FUNC() ;
#  endif

	++sc1445x_internal_dsp1_irq_cnt ;

#  if defined( CHECK_DSP_IRQ_OVERFLOW )
	overflow = DSP1_OVERFLOW_REG ;
	mask = overflow & (M_IRQ_OVERFLOW | M_WTF_OVERFLOW | M_INT_OVERFLOW) ;
	source = overflow & (IRQ_OVERFLOW | WTF_OVERFLOW | INT_OVERFLOW) ;
	source &= mask >> 8 ;
	if( source & (unsigned short)IRQ_OVERFLOW ) {
		DPRINT( PRINT_LEVEL "IRQ_OVERFLOW on DSP1\n" ) ;
		++dsp_irq_overflows[0].irq_of ;
	}
	if( source & (unsigned short)WTF_OVERFLOW ) {
		DPRINT( PRINT_LEVEL "WTF_OVERFLOW on DSP1\n" ) ;
		++dsp_irq_overflows[0].wtf_of ;
	}
	if( source & (unsigned short)INT_OVERFLOW ) {
		int i ;
		unsigned short t = source & (unsigned short)INT_OVERFLOW ;

		DPRINT( PRINT_LEVEL "INT_OVERFLOW %02x on DSP1\n",
				source & (unsigned short)INT_OVERFLOW ) ;
		for( i = 0 ;  i < 5 ;  ++i ) {
			if( t & 1 )
				++dsp_irq_overflows[0].int_of[i] ;
			t >>= 1 ;
		}
	}
	DSP1_OVERFLOW_REG |= source ;

	ack_dsp1_int = 1 ;
#  endif  /* CHECK_DSP_IRQ_OVERFLOW */

#  if defined( SMOOTH_IFACE_TRANSITION )
	dsp1_int_vector = GetWord( DSP1_INT_REG ) ;
	if( dsp1_int_vector & 0x001 ) {
		sc1445x_ae_iface_mode im ;
		unsigned short ch ;

		++sc1445x_internal_smooth_transition_irq_cnt ;

		/* fade-out has finished */
		DPRINT( "%s: DSP1 INT\n", __FUNCTION__);
		if( ae_state->do_disable_classd ) {
			do_sc1445x_ae_set_vspk_volume( ae_state ) ;

			DPRINT( "%s: DSP1 INT A\n", __FUNCTION__) ;

			sc1445x_internal_enable_output_to_classd_amp(
							ae_state, 0 ) ;
			ae_state->do_disable_classd = 0 ;
		}

		if( ae_state->do_disable_codec ) {
#if !defined( DONT_POWER_DOWN_LSR )
			struct __CODEC_LSR_REG* codec_lsr_reg =
				(struct __CODEC_LSR_REG*)&CODEC_LSR_REG ;
#endif

			do_sc1445x_ae_set_vspk_volume( ae_state ) ;

			DPRINT( "%s: DSP1 INT B\n", __FUNCTION__) ;

#if !defined( DONT_POWER_DOWN_LSR )
			codec_lsr_reg->BITFLD_LSRN_PD = 1 ;
			codec_lsr_reg->BITFLD_LSRP_PD = 1 ;
#endif
			ae_state->do_disable_codec = 0 ;
		}

		if( ae_state->do_stop_pcm_channel ) {
			short i, have_active_voice_channels = 0 ;
			short have_active_tonegen = 0 ;

			DPRINT( "%s: DSP1 INT C\n", __FUNCTION__) ;

			for( i = 0 ;  i < ae_state->audio_channels_count ;
					++i ) {
				if( ae_state->audio_channels[i].is_active &&
						!ae_state->audio_channels[i].
						is_raw_pcm ) {
					if( ae_state->audio_channels[i].
							do_release )
						continue ;  /* ignore it */
					have_active_voice_channels = 1 ;
					break ;
				}
			}

			for( i = 0 ;  i < SC1445x_AE_TONEGEN_MOD_COUNT ;
									++i ) {
				if( ae_state->tonegen[i].status !=
						SC1445x_AE_TONEGEN_IDLE ) {
					have_active_tonegen = 1 ;
					break ;
				}
			}

			if( !have_active_voice_channels &&
							!have_active_tonegen )
				sc1445x_internal_enable_output_to_classd_amp(
							ae_state, 0 ) ;
			ae_state->do_stop_pcm_channel = 0 ;
		}

		if( ae_state->do_set_vmic ) {
			DPRINT( "%s: DSP1 INT D\n", __FUNCTION__) ;

#if 1
			im = get_audio_profile_iface_mode( ae_state ) ;
			sc1445x_ae_internal_set_vmic( ae_state, im,
				vmic_from_iface_mode( im,
					ae_state->using_external_codec_mic ) ) ;
#endif

			schedule_delayed_work( &ae_state->vmic_gain_work, 10 ) ;

			ae_state->do_set_vmic = 0 ;
		}

		if( ae_state->do_reset_paec ) {
			DPRINT( "%s: DSP1 INT E\n", __FUNCTION__) ;

			/* adjust filters */
			im  = ae_state->iface_mode ;
			sc1445x_ae_set_rx_tx_filters( ae_state,
					SC1445x_AE_IFACE_MODE_HANDS_FREE == im,
					currently_wideband(),
					SC1445x_AE_IFACE_MODE_HEADSET == im ) ;

			/* reset (P)AEC */
#if 0
			sc1445x_ae_set_aec_off( ae_state ) ;
#endif
			/*
			 * restart echo cancellation only if it was already
			 * enabled (thanks to Mikko)
			 */
			if( ae_state->AEC_enabled )
				sc1445x_ae_set_aec_on( ae_state ) ;

			ae_state->do_reset_paec = 0 ;
		}

		for( ch = 0 ;  ch < ae_state->audio_channels_count ;  ++ch ) {
			if( ae_state->audio_channels[ch].do_release ) {
				DPRINT( "%s: DSP1 INT F\n", __FUNCTION__) ;

				ae_state->audio_channels[ch].do_release = 2 ;
				schedule_delayed_work(
					&ae_state->release_channel_work, 1 ) ;
			}
		}

		if( ae_state->do_disable_dap ) {
			DPRINT( "%s: DSP1 INT G\n", __FUNCTION__) ;

			/* tell DSP to disable the dual audio path */
			sc1445x_internal_send_dsp_cmd( 0x0034, 0 ) ;

			ae_state->do_disable_dap = 0 ;
		}

		/* turn on sidetone */
		sc1445x_internal_send_dsp_cmd( 0x0018, 0x3000 ) ;

		dsp1_handled_ints |= 0x0001 ;

		ack_dsp1_int = 1 ;

		wake_up_interruptible_all( &ae_state->fadeoutq ) ;
	}
#  endif  /* SMOOTH_IFACE_TRANSITION */

#  if defined( SC1445x_AE_ATA_SUPPORT )
	dsp1_int_vector = GetWord( DSP1_INT_REG ) ;
	if( dsp1_int_vector & 0x0002 ) {
		volatile unsigned short* dsp1_det_action = (void*)0x12D54 ;
		short i ;

		++sc1445x_internal_cid_irq_cnt ;

		for( i = 0 ;  i < SC1445x_AE_AUDIO_STREAMS_COUNT ;  ++i ) {
			unsigned short action = dsp1_det_action[i] ;
			unsigned long timestamp = jiffies ;
		
			DPRINT( "%s: action %d for stream %d\n", __FUNCTION__,
								action, i ) ;
			switch( action ) {
				case 0:
					/* nothing here */
					break ;

				case 1:
					sc1445x_internal_start_fsk_decoder(
								ae_state, i, 1 ) ;
					break ;

				case 2:
					sc1445x_internal_cid_read_fsk_char(
								ae_state, i ) ;
					break ;

				case 3:
					/*
					 * account for detection latency (50msec)
					 */
					timestamp -= 5 ;
					DPRINT( PRINT_LEVEL "%s: stutter dial "
						"ON started at %lu\n",
						__FUNCTION__, timestamp ) ;
					sc1445x_private_tone_det_add_event(
						SC1445x_TONE_DET_STUTTER_DIAL |
						SC1445x_TONE_DET_EVENT_START,
						timestamp * 10 ) ;
					break ;

				case 4:
					DPRINT( PRINT_LEVEL "%s: stutter dial "
						"ON ended at %lu\n",
						__FUNCTION__, timestamp ) ;
					sc1445x_private_tone_det_add_event(
						SC1445x_TONE_DET_STUTTER_DIAL |
						SC1445x_TONE_DET_EVENT_END,
						timestamp * 10 ) ;
					break ;

				default:
					PRINT( PRINT_LEVEL "%s: unknown CID "
							"action %04x\n",
							__FUNCTION__, action ) ;
			}

			dsp1_det_action[i] = 0 ;
		}

		dsp1_handled_ints |= 2 ;

		ack_dsp1_int = 1 ;
	}
#  endif  /* SC1445x_AE_ATA_SUPPORT */

	if( ack_dsp1_int ) {
		/* write back vector to clear the vector bits */
		SetWord( DSP1_INT_REG, dsp1_handled_ints ) ;
		/* clear pending DSP1 interrupt bit */
		SetWord( RESET_INT_PENDING_REG, DSP1_INT_PEND ) ;

		ret = IRQ_HANDLED ;
	}
#endif

	return ret ;
}

/* interrupt handler for DSP2 interrupts */
static irqreturn_t generic_dsp2_isr_normal( int irq, void* devid )
{
	irqreturn_t ret = IRQ_NONE ;
	unsigned short dsp_int_vector ;
#if defined( CHECK_DSP_IRQ_OVERFLOW )
	unsigned short overflow, mask, source ;
#endif
	short ack_dsp2_int = 0 ;
	sc1445x_ae_state* my_ae_state = devid ;
	struct snd_sc1445x* alsa_chip =
				(struct snd_sc1445x*)my_ae_state->private_data ;

#if defined( TRACE_AUDIOEGINE_ISR )
	//AUDIOENGINE_TRACE_FUNC() ;
	AUDIOENGINE_TRACE_ADD( "dsp2_isr enter" ) ;
#endif

#if !defined( TEST_WITH_MARKED_PACKET ) || defined( DEBUG_WITH_GPIO )
	DEBUG_WITH_GPIO_RAISE ;
#endif

	detect_lost_dsp_ints() ;

	++sc1445x_internal_dsp2_irq_cnt ;

#if defined( TEST_WITH_MARKED_PACKET ) && defined( MARKED_PACKET_INJECTOR_SIDE )
	if( !( (sc1445x_internal_read_irq_cnt + 1) % MARKED_PACKET_PERIOD ) )
		DEBUG_WITH_GPIO_RAISE ;
	else
		DEBUG_WITH_GPIO_LOWER ;
#endif

	dsp_int_vector = GetWord( DSP2_INT_REG ) ;
#if defined( SC1445x_AE_SUPPORT_FAX )
	if( 0x800 & dsp_int_vector ) {
		sc1445x_internal_dsp_hot_download( my_ae_state ) ;
		/* write back vector to clear the vector bits */
		SetWord( DSP2_INT_REG, 0x800 ) ;

		/* inform DSP we're done */
		sc1445x_internal_send_dsp_cmd( 0x0020, 0x0000 ) ;

		ack_dsp2_int = 1 ;
	}
	dsp_int_vector = GetWord( DSP2_INT_REG ) ;
#endif

	if( 0x100 & dsp_int_vector ) {
#ifdef FXO_PLAY_PRERECORDED_PCM
		send_recorded_cas();
#endif	
		if( likely( alsa_chip->playback_substream ) )
			generic_put_playback_packets( my_ae_state ) ;

		/* write back vector to clear the vector bits */
		SetWord( DSP2_INT_REG, 0x100 ) ;

		ack_dsp2_int = 1 ;
	}
	dsp_int_vector = GetWord( DSP2_INT_REG ) ;
	if( 0x200 & dsp_int_vector ) {
#if defined( DSP_DEBUG_WITH_GPIO )
		DSP_ZCROSS1_OUT_REG = 0 ;
#endif
#ifdef FXO_PCM_LOG
		do_fxo_pcm_log();
#endif
		if( likely( alsa_chip->capture_substream ) )
			generic_get_capture_packets( my_ae_state ) ;

		/* write back vector to clear the vector bits */
		SetWord( DSP2_INT_REG, 0x200 ) ;

#if defined( MEASURE_C_DELAY )
		if( MEASURE_C_DELAY_DSP_TRIGGER ) {
			memcpy( measure_c_delay_buf1 + measure_c_delay_pos,
						MEASURE_C_DELAY_CAP_LOG1,
						MEASURE_C_DELAY_FRAME_SIZE ) ;
			memcpy( measure_c_delay_buf2 + measure_c_delay_pos,
						MEASURE_C_DELAY_CAP_LOG2,
						MEASURE_C_DELAY_FRAME_SIZE ) ;
			measure_c_delay_pos += MEASURE_C_DELAY_FRAME_SIZE ;
			measure_c_delay_measuring = 1 ;
			if( measure_c_delay_pos >= MEASURE_C_DELAY_BUF_SIZE ) {
				PRINT( PRINT_LEVEL
					"Logging for delay measurement in the "
					"capture substream finished:\n" ) ;
				PRINT( PRINT_LEVEL
					"Capture log 1 @0x%p (0x%04x bytes)\n",
					measure_c_delay_buf1,
					MEASURE_C_DELAY_BUF_SIZE ) ;
				PRINT( PRINT_LEVEL
					"Capture log 2 @0x%p (0x%04x bytes)\n",
					measure_c_delay_buf2,
					MEASURE_C_DELAY_BUF_SIZE ) ;
				measure_c_delay_pos = 0 ;
			}
		} else if( measure_c_delay_measuring ) {
			measure_c_delay_measuring = 0 ;
			PRINT( PRINT_LEVEL
					"Logging for delay measurement in the "
					"capture substream finished:\n" ) ;
			PRINT( PRINT_LEVEL
				"Capture log 1 @0x%p (0x%04x bytes)\n",
				measure_c_delay_buf1, measure_c_delay_pos ) ;
			PRINT( PRINT_LEVEL
				"Capture log 2 @0x%p (0x%04x bytes)\n",
				measure_c_delay_buf2, measure_c_delay_pos ) ;
			measure_c_delay_pos = 0 ;
		}
#endif

		ack_dsp2_int = 1 ;
	}
	dsp_int_vector = GetWord( DSP2_INT_REG ) ;
	if( dsp_int_vector & 0x002 ) {
		extern void snd_sc1445x_raw_pcm_dsp_irq( void ) ;

		snd_sc1445x_raw_pcm_dsp_irq() ;
		/* write back vector to clear the vector bits */
		SetWord( DSP2_INT_REG, 0x002 ) ;

		ack_dsp2_int = 1 ;
	}
	if( dsp_int_vector & 0x004 ) {
		/* write back vector to clear the vector bits */
		SetWord( DSP2_INT_REG, 0x004 ) ;

		DPRINT( "%s: DSP ran out of PCM data!\n", __FUNCTION__ ) ;
		ack_dsp2_int = 1 ;
	}

#if defined( CHECK_DSP_IRQ_OVERFLOW )
	overflow = DSP2_OVERFLOW_REG ;
	mask = overflow & (M_IRQ_OVERFLOW | M_WTF_OVERFLOW | M_INT_OVERFLOW) ;
	source = overflow & (IRQ_OVERFLOW | WTF_OVERFLOW | INT_OVERFLOW) ;
	source &= mask >> 8 ;
	if( source & (unsigned short)IRQ_OVERFLOW ) {
		DPRINT( PRINT_LEVEL "IRQ_OVERFLOW on DSP2\n" ) ;
		++dsp_irq_overflows[1].irq_of ;
	}
	if( source & (unsigned short)WTF_OVERFLOW ) {
		DPRINT( PRINT_LEVEL "WTF_OVERFLOW on DSP2\n" ) ;
		++dsp_irq_overflows[1].wtf_of ;
	}
	if( source & (unsigned short)INT_OVERFLOW ) {
		int i ;
		unsigned short t = source & (unsigned short)INT_OVERFLOW ;

		DPRINT( PRINT_LEVEL "INT_OVERFLOW %02x on DSP2\n",
				source & (unsigned short)INT_OVERFLOW ) ;
		for( i = 0 ;  i < 5 ;  ++i ) {
			if( t & 1 )
				++dsp_irq_overflows[1].int_of[i] ;
			t >>= 1 ;
		}
	}
	DSP2_OVERFLOW_REG |= source ;

	ack_dsp2_int = 1 ;
#endif

	/* clear pending DSP2 interrupt bit */
	if( ack_dsp2_int ) {
		SetWord( RESET_INT_PENDING_REG, DSP2_INT_PEND ) ;

		ret = IRQ_HANDLED ;
	}

#if !defined( TEST_WITH_MARKED_PACKET ) || defined( DEBUG_WITH_GPIO )
	DEBUG_WITH_GPIO_LOWER ;
#endif

#if defined( TRACE_AUDIOEGINE_ISR )
	AUDIOENGINE_TRACE_ADD( "dsp2_isr exit" ) ;
#endif
	return ret ;
}


static inline void loopback_audio_packets( sc1445x_ae_state* ae_state,
						unsigned capture_w_pos )
{
	struct snd_sc1445x* alsa_chip =
				(struct snd_sc1445x*)ae_state->private_data ;
	snd_pcm_runtime_t* p_runtime = alsa_chip->playback_substream->runtime ;
	snd_pcm_runtime_t* c_runtime = alsa_chip->capture_substream->runtime ;
	const unsigned short nch = ae_state->audio_channels_count ;
	const unsigned short sz = nch * ( SC1445x_AE_BYTES_PER_AUDIO_PACKET +
				SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER ) ;
	unsigned char* dst, * src ;

#if 1
	p_runtime = &fake_runtime ;
	c_runtime = &fake_runtime ;
#endif
	dst = p_runtime->dma_area + alsa_chip->playback_r_pos ;
	src = c_runtime->dma_area + capture_w_pos ;
	memcpy( dst, src, sz ) ;
}


static irqreturn_t generic_dsp1_isr_loopback( int irq, void* devid )
{
	++sc1445x_internal_dsp1_irq_cnt ;

	return IRQ_NONE ;
}

static irqreturn_t generic_dsp2_isr_loopback( int irq, void* devid )
{
	static const short inject_fault_dont_serve_dsp_irq = 0 ;
	sc1445x_ae_state* my_ae_state = devid ;
	struct snd_sc1445x* alsa_chip = my_ae_state->private_data ;
	unsigned short dsp_int_vector ;

	++sc1445x_internal_dsp2_irq_cnt ;

	if( inject_fault_dont_serve_dsp_irq  &&
			!(sc1445x_internal_dsp2_irq_cnt % 16) )
		return IRQ_HANDLED ;

	dsp_int_vector = GetWord( DSP2_INT_REG ) ;
	if( 0x100 == dsp_int_vector ) {
		generic_put_playback_packets( my_ae_state ) ;
	}
	else if( 0x200 == dsp_int_vector ) {
		unsigned capture_old_w_pos = alsa_chip->capture_w_pos ;

		generic_get_capture_packets( my_ae_state ) ;
		/* loopback */
		loopback_audio_packets( my_ae_state, capture_old_w_pos ) ;
	}

	/* write back vector to clear the vector bits */
	SetWord( DSP2_INT_REG, dsp_int_vector ) ;
	/* clear pending DSP2 interrupt bit */
	SetWord( RESET_INT_PENDING_REG, DSP2_INT_PEND ) ;

	return IRQ_HANDLED ;
}


static irqreturn_t generic_dsp1_isr_autoplay( int irq, void* devid  )
{
	++sc1445x_internal_dsp2_irq_cnt ;

	return IRQ_NONE ;
}

static irqreturn_t generic_dsp2_isr_autoplay( int irq, void* devid  )
{
	sc1445x_ae_state* my_ae_state = devid ;
	unsigned short dsp_int_vector ;

	++sc1445x_internal_dsp2_irq_cnt ;

	dsp_int_vector = GetWord( DSP2_INT_REG ) ;
	if( 0x100 == dsp_int_vector ) {
		generic_put_autoplayback_packets( my_ae_state ) ;
	}
	else if( 0x200 == dsp_int_vector ) {
		generic_get_capture_packets( my_ae_state ) ;
#if 0
		/* loopback */
		unsigned short ch, nch = my_ae_state->audio_channels_count ;

		for( ch = 0 ;  ch < nch ;  ++ch ) {
			unsigned short header ;

			if( !my_ae_state->audio_channels[ch].is_active  ||
							!autoplay_readp[ch] )
				continue ;

			header = ( audio_buf_to_os_type[ch] << 8 ) |
						audio_buf_to_os_len[ch] ;
//			autoplay_readp[ch] = autoplay_startp[ch] ;
			*autoplay_readp[ch] = header ;
			memcpy( autoplay_readp[ch] + 1, audio_buf_to_os[ch],
						audio_buf_to_os_len[ch] << 1 ) ;
		}
#endif
	}

	/* write back vector to clear the vector bits */
	SetWord( DSP2_INT_REG, dsp_int_vector ) ;
	/* clear pending DSP2 interrupt bit */
	SetWord( RESET_INT_PENDING_REG, DSP2_INT_PEND ) ;

	return IRQ_HANDLED ;
}


#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )

static irqreturn_t generic_dsp1_isr_raw_pcm( int irq, void* devid )
{
	irqreturn_t ret = IRQ_NONE ;
#if defined( SMOOTH_IFACE_TRANSITION )
	sc1445x_ae_state* my_ae_state = devid ;
	unsigned short dsp1_int_vector ;
	short ack_dsp1_int = 0 ;

	AUDIOENGINE_TRACE_ADD( "generic_dsp1_isr_raw_pcm enter" ) ;

	++sc1445x_internal_dsp1_irq_cnt ;

	dsp1_int_vector = GetWord( DSP1_INT_REG ) ;
	if( dsp1_int_vector & 0x001 ) {
		++sc1445x_internal_smooth_transition_irq_cnt ;

		/* fade-out has finished */
		DPRINT( "%s: DSP1 INT\n", __FUNCTION__);
		if( my_ae_state->do_disable_classd ) {
			DPRINT( "%s: DSP1 INT A\n", __FUNCTION__) ;

			sc1445x_internal_enable_output_to_classd_amp(
							my_ae_state, 0 ) ;
			my_ae_state->do_disable_classd = 0 ;
		}

		if( my_ae_state->do_disable_codec ) {
#if !defined( DONT_POWER_DOWN_LSR )
			struct __CODEC_LSR_REG* codec_lsr_reg =
				(struct __CODEC_LSR_REG*)&CODEC_LSR_REG ;
#endif

			DPRINT( "%s: DSP1 INT B\n", __FUNCTION__) ;

#if !defined( DONT_POWER_DOWN_LSR )
			codec_lsr_reg->BITFLD_LSRN_PD = 1 ;
			codec_lsr_reg->BITFLD_LSRP_PD = 1 ;
#endif
			my_ae_state->do_disable_codec = 0 ;
		}

		/* turn on sidetone */
		sc1445x_internal_send_dsp_cmd( 0x0018, 0x3000 ) ;

		/* write back vector to clear the vector bits */
		SetWord( DSP1_INT_REG, 0x001 ) ;

		ack_dsp1_int = 1 ;
	}

	/* clear pending DSP1 interrupt bit */
	if( ack_dsp1_int ) {
		SetWord( RESET_INT_PENDING_REG, DSP1_INT_PEND ) ;
		ret = IRQ_HANDLED ;
	}
#endif

	AUDIOENGINE_TRACE_ADD( "generic_dsp1_isr_raw_pcm exit" ) ;

	return ret ;
}

static irqreturn_t generic_dsp2_isr_raw_pcm( int irq, void* devid )
{
	sc1445x_ae_state* my_ae_state = devid ;
	struct snd_sc1445x* alsa_chip = my_ae_state->private_data ;
	unsigned short dsp2_int_vector ;
	extern void snd_sc1445x_raw_pcm_dsp_irq( void ) ;
	short ack_dsp2_int = 0 ;

	AUDIOENGINE_TRACE_ADD( "generic_dsp2_isr_raw_pcm enter" ) ;

	++sc1445x_internal_dsp2_irq_cnt ;

	dsp2_int_vector = GetWord( DSP2_INT_REG ) ;
	if( dsp2_int_vector & 0x0002 ) {
		++sc1445x_internal_raw_pcm_irq_cnt ;
		if( likely( alsa_chip->playback_substream ) ) {
			snd_pcm_runtime_t* runtime =
					alsa_chip->playback_substream->runtime ;

			alsa_chip->playback_r_pos +=
					my_ae_state->raw_pcm.block_size ;
			if( alsa_chip->playback_r_pos >=
							runtime->buffer_size )
					alsa_chip->playback_r_pos = 0 ;
			snd_pcm_period_elapsed( alsa_chip->playback_substream );
		}
		else {
			DPRINT( "%s: ignoring interrupt #%d because playback "
				"substream is closed!\n", __FUNCTION__,
				sc1445x_internal_raw_pcm_irq_cnt ) ;
		}
#if 1
		snd_sc1445x_raw_pcm_dsp_irq() ;
#endif

		ack_dsp2_int = 1 ;
	}
	if( dsp2_int_vector & 0x0100 )
		ack_dsp2_int = 1 ;
	if( dsp2_int_vector & 0x0004 ) {
		DPRINT( "%s: DSP ran out of PCM data!\n", __FUNCTION__ ) ;
		ack_dsp2_int = 1 ;
	}

	/* write back vector to clear the vector bits */
	SetWord( DSP2_INT_REG, dsp2_int_vector ) ;
	/* clear pending DSP2 interrupt bit */
	if( ack_dsp2_int )
		SetWord( RESET_INT_PENDING_REG, DSP2_INT_PEND ) ;

	AUDIOENGINE_TRACE_ADD( "generic_dsp2_isr_raw_pcm exit" ) ;

	return IRQ_HANDLED ;
}

#endif


/* install the IRQ handler for DSP interrupts */
static int sc1445x_internal_install_isr( sc1445x_isr_t isr1, sc1445x_isr_t isr2,
						sc1445x_ae_state* ae_state )
{
	int res ;

	res = request_irq( DSP2_INT, isr2, 0, "Audio Engine",
							(void*)ae_state ) ;
	if( res < 0 ) {
		printk( KERN_ERR "Audio engine: unable to register IRQ #%d\n",
								DSP2_INT ) ;
	}

#if defined( CHECK_DSP_IRQ_OVERFLOW ) || defined( SMOOTH_IFACE_TRANSITION ) \
					|| defined( SC1445x_AE_ATA_SUPPORT )
	res = request_irq( DSP1_INT, isr1, SA_SHIRQ, "Audio Engine",
							(void*)ae_state ) ;
	if( res < 0 ) {
		printk( KERN_ERR "Audio engine: unable to register IRQ #%d\n",
								DSP1_INT ) ;
		free_irq( DSP2_INT, (void*)ae_state ) ;
	}
#endif

	return res ;
}



/*******************************/
/* INITIALIZATION/FINALIZATION */
/*******************************/

#define REASSIGN_FIELD_AND_ADVANCE( params, field, p, n, new_params ) 	\
	do {								\
		params->field = p ;					\
		p += n ;						\
		memcpy( params->field, new_params->field, 		\
				n * sizeof( *p ) ) ;			\
	} while( 0 ) ;

#define REASSIGN_FIELD_PTR_AND_ADVANCE( params, field, p, n ) 		\
	do {								\
		params->field = p ;					\
		p += n ;						\
	} while( 0 ) ;

#define REASSIGN_ARRAY_AND_ADVANCE( a, p, sz, pa ) 			\
	do {								\
		a = p ;							\
		memcpy( a, pa, sz ) ;					\
		p = (void*)( (unsigned long)p + sz ) ;			\
	} while( 0 ) ;

#define REASSIGN_ARRAY_PTR_AND_ADVANCE( a, p, sz ) 			\
	do {								\
		a = p ;							\
		p = (void*)( (unsigned long)p + sz ) ;			\
	} while( 0 ) ;

/* reset audio profile to its default settings */
short sc1445x_ae_reset_audio_profile( sc1445x_ae_state* ae_state )
{
	NOT_AVAILABLE_WITH_PCM_LINES

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	unsigned short i, j, n, cpn ;
	unsigned short* p ;
	unsigned short** pp ;
	int sz, total_sz ;
	sc1445x_ae_vspk_volumes* vsvs ;
	struct vspk_volumes* pvsvs ;
	sc1445x_ae_vmic_gains* vmgs ;
	struct vmic_gains* pvmgs ;
	sc1445x_ae_ap_custom_param* cp ;
	struct vspk_custom_param* pvcp ;
	struct vmic_custom_param* pmcp ;

	CHECK_AE_STATE( ae_state ) ;

	/* calculate how much memory we need for the default audio profile */
	total_sz = 0 ;

	/* vspk_lvl_narrow */
	n = SC1445x_AE_VSPK_VOL_LEVEL_COUNT ;
	cpn = ARRAY_SIZE( profile_vspk_levels_narrowband[0].cp.params ) ;
	sz = n * sizeof(unsigned short)
			* ( cpn + sc1445x_ae_vspk_volumes_FIELDS_COUNT ) ;
	sz += cpn * sizeof( sc1445x_ae_ap_custom_param ) ;
		
	sz *= SC1445x_AE_IFACE_MODE_COUNT_IN_AP ;
	/* vspk_lvl_wide */
	sz += sz ;
	total_sz += sz ;

	/* vmic_lvl_narrow */
	n = SC1445x_AE_VMIC_GAIN_LEVEL_COUNT ;
	cpn = ARRAY_SIZE( profile_vmic_levels_narrowband[0].cp.params ) ;
	sz = n * sizeof(unsigned short)
			* ( cpn + sc1445x_ae_vmic_gains_FIELDS_COUNT ) ;
	sz += cpn * sizeof( sc1445x_ae_ap_custom_param ) ;
	sz *= SC1445x_AE_IFACE_MODE_COUNT_IN_AP ;
	/* vmic_lvl_wide */
	sz += sz ;
	total_sz += sz ;

#if defined( CONFIG_SND_SC1445x_USE_PAEC )

#  if AUDIO_PROFILE_MAGIC == AUDIO_PROFILE_MAGIC_AP10
	/* PAEC state array setup */
	n = ARRAY_SIZE( profile_paec_band_loc ) ;
	sz = n * sizeof(unsigned short) ;
	sz += 2 * sz ;
	total_sz += sz ;
#  endif

	/* PAEC parameters setup */
	n = ARRAY_SIZE( profile_paec_data_loc ) ;
	sz = n * sizeof(unsigned short) ;
	sz += 2 * sz ;
	total_sz += sz ;

	/* Suppressor Setup*/
	n = ARRAY_SIZE( profile_supp_params_addr ) ;
	sz = n * sizeof(unsigned short*) ;
	sz += n * sizeof(unsigned short) ;
	sz += n * sizeof(unsigned short) ;
	total_sz += sz ;
#endif

#if defined( CONFIG_SND_SC1445x_USE_AEC )
	/* AEC setup */
	n = ARRAY_SIZE( profile_aec_params_data ) ;
	sz = n * sizeof(unsigned short) ;
	total_sz += sz ;
#endif


	/* allocate the memory */
	PRINT( "%s: will allocate %d bytes for default audio profile\n",
						__FUNCTION__, total_sz ) ;
	p = MALLOC( total_sz ) ;
	if( unlikely( !p ) ) {
		PRINT( sc1445x_ae_nomem_error, __FUNCTION__, "audio profile" ) ;
		return SC1445x_AE_ERR_NO_MEM ;
	}
	if( ae_state->ap.data )
		FREE( ae_state->ap.data ) ;
	memset( p, 0, total_sz ) ;
	ae_state->ap.data = p ;
	ae_state->ap.data_nbytes = total_sz ;


	/* update array pointers and copy data */

	/* vspk_lvl_narrow and vspk_lvl_wide */
	n = SC1445x_AE_VSPK_VOL_LEVEL_COUNT ;
	for( i = 0 ;  i < SC1445x_AE_IFACE_MODE_COUNT_IN_AP ;  ++i ) {
		vsvs = &ae_state->ap.vspk_lvl_narrow[i] ;
		pvsvs = &profile_vspk_levels_narrowband[i] ;

		vsvs->use_analog = pvsvs->use_analog ;
		vsvs->use_rx_path_att = pvsvs->use_rx_path_att ;
		vsvs->use_rx_path_shift	= pvsvs->use_rx_path_shift ;
		vsvs->use_sidetone_att = pvsvs->use_sidetone_att ;
		vsvs->use_ext_spk_att = pvsvs->use_ext_spk_att ;
		vsvs->use_shift_ext_spk = pvsvs->use_shift_ext_spk ;
		vsvs->use_tone_vol = pvsvs->use_tone_vol ;
		vsvs->use_classd_vout = pvsvs->use_classd_vout ;
		vsvs->use_pcm_gain_pre = pvsvs->use_pcm_gain_pre ;
		vsvs->use_pcm_gain_shift = pvsvs->use_pcm_gain_shift ;
		vsvs->use_loopgain_rxpl = pvsvs->use_loopgain_rxpl ;
		vsvs->use_ng_threshold = pvsvs->use_ng_threshold ;

		DPRINT( "%s: narrow vsv #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, analog_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, rx_path_att_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, rx_path_shift_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, sidetone_att_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, ext_spk_att_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, ring_playback_vol_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, shift_ext_spk_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, ring_tone_vol_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, call_tone_codec_vol_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, call_tone_classd_vol_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, dtmf_tone_codec_vol_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, dtmf_tone_classd_vol_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, classd_vout, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, pcm_gain_pre, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, pcm_gain_shift, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, pcm_gain_pre_ringtone, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, pcm_gain_shift_ringtone, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, loopgain_rxpl_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, ng_threshold_levels, p, n, pvsvs ) ;

		/* custom params */
		cpn = ARRAY_SIZE( pvsvs->cp.params ) ;
		DPRINT( "%s: narrow vsv #%2d custom params \t@%p (%d)\n",
						__FUNCTION__, i, p, cpn ) ;
		vsvs->cp.count = cpn ;
		vsvs->cp.params = (void*)p ;
		/* assume sizeof( sc1445x_ae_ap_custom_param ) is even */
		p += ( cpn * sizeof( sc1445x_ae_ap_custom_param ) ) >> 1 ;
		cp = vsvs->cp.params ;
		pvcp = pvsvs->cp.params ;
		for( j = 0 ;  j < cpn ;  ++j, ++cp, ++pvcp ) {
			cp->use = pvcp->use ;
			cp->addr = pvcp->addr ;
			REASSIGN_FIELD_AND_ADVANCE( cp, values, p, n, pvcp ) ;
		}


		vsvs = &ae_state->ap.vspk_lvl_wide[i] ;
		pvsvs = &profile_vspk_levels_wideband[i] ;

		vsvs->use_analog = pvsvs->use_analog ;
		vsvs->use_rx_path_att = pvsvs->use_rx_path_att ;
		vsvs->use_rx_path_shift	= pvsvs->use_rx_path_shift ;
		vsvs->use_sidetone_att = pvsvs->use_sidetone_att ;
		vsvs->use_ext_spk_att = pvsvs->use_ext_spk_att ;
		vsvs->use_shift_ext_spk = pvsvs->use_shift_ext_spk ;
		vsvs->use_tone_vol = pvsvs->use_tone_vol ;
		vsvs->use_classd_vout = pvsvs->use_classd_vout ;
		vsvs->use_pcm_gain_pre = pvsvs->use_pcm_gain_pre ;
		vsvs->use_pcm_gain_shift = pvsvs->use_pcm_gain_shift ;
		vsvs->use_loopgain_rxpl = pvsvs->use_loopgain_rxpl ;
		vsvs->use_ng_threshold = pvsvs->use_ng_threshold ;


		DPRINT( "%s:  wide  vsv #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, analog_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, rx_path_att_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, rx_path_shift_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, sidetone_att_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, ext_spk_att_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, ring_playback_vol_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, shift_ext_spk_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, ring_tone_vol_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, call_tone_codec_vol_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, call_tone_classd_vol_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, dtmf_tone_codec_vol_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, dtmf_tone_classd_vol_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, classd_vout, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, pcm_gain_pre, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, pcm_gain_shift, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, pcm_gain_pre_ringtone, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, pcm_gain_shift_ringtone, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, loopgain_rxpl_levels, p, n, pvsvs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vsvs, ng_threshold_levels, p, n, pvsvs ) ;

		/* custom params */
		cpn = ARRAY_SIZE( pvsvs->cp.params ) ;
		DPRINT( "%s:  wide  vsv #%2d custom params \t@%p (%d)\n",
						__FUNCTION__, i, p, cpn ) ;
		vsvs->cp.count = cpn ;
		vsvs->cp.params = (void*)p ;
		/* assume sizeof( sc1445x_ae_ap_custom_param ) is even */
		p += ( cpn * sizeof( sc1445x_ae_ap_custom_param ) ) >> 1 ;
		cp = vsvs->cp.params ;
		pvcp = pvsvs->cp.params ;
		for( j = 0 ;  j < cpn ;  ++j, ++cp, ++pvcp ) {
			cp->use = pvcp->use ;
			cp->addr = pvcp->addr ;
			REASSIGN_FIELD_AND_ADVANCE( cp, values, p, n, pvcp ) ;
		}
	}
	INIT_CONST_FIELD( unsigned short, &ae_state->ap, vspk_lvl_count, n ) ;

	/* vmic_lvl_narrow and vmic_lvl_wide */
	n = SC1445x_AE_VMIC_GAIN_LEVEL_COUNT ;
	for( i = 0 ;  i < SC1445x_AE_IFACE_MODE_COUNT_IN_AP ;  ++i ) {
		vmgs = &ae_state->ap.vmic_lvl_narrow[i] ;
		pvmgs = &profile_vmic_levels_narrowband[i] ;

		vmgs->use_analog = pvmgs->use_analog ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		vmgs->use_shift_paec_out = pvmgs->use_shift_paec_out ;
		vmgs->use_paec_tx_att = pvmgs->use_paec_tx_att ;
		vmgs->use_attlimit = pvmgs->use_attlimit ;
		vmgs->use_supmin = pvmgs->use_supmin ;
		vmgs->use_noiseattlimit = pvmgs->use_noiseattlimit ;
#endif

		DPRINT( "%s: narrow vmg #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_AND_ADVANCE( vmgs, analog_levels, p, n, pvmgs ) ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		REASSIGN_FIELD_AND_ADVANCE( vmgs, shift_paec_out_levels, p, n, pvmgs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vmgs, shift_paec_out_levels_BT, p, n, pvmgs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vmgs, paec_tx_att_levels, p, n, pvmgs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vmgs, paec_tx_att_levels_BT, p, n, pvmgs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vmgs, attlimit, p, n, pvmgs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vmgs, supmin, p, n, pvmgs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vmgs, noiseattlimit, p, n, pvmgs ) ;

		/* custom params */
		cpn = ARRAY_SIZE( pvmgs->cp.params ) ;
		DPRINT( "%s: narrow vmg #%2d custom params \t@%p (%d)\n",
						__FUNCTION__, i, p, cpn ) ;
		vmgs->cp.count = cpn ;
		vmgs->cp.params = (void*)p ;
		/* assume sizeof( sc1445x_ae_ap_custom_param ) is even */
		p += ( cpn * sizeof( sc1445x_ae_ap_custom_param ) ) >> 1 ;
		cp = vmgs->cp.params ;
		pmcp = pvmgs->cp.params ;
		for( j = 0 ;  j < cpn ;  ++j, ++cp, ++pmcp ) {
			cp->use = pmcp->use ;
			cp->addr = pmcp->addr ;
			REASSIGN_FIELD_AND_ADVANCE( cp, values, p, n, pmcp ) ;
		}
#endif


		vmgs = &ae_state->ap.vmic_lvl_wide[i] ;
		pvmgs = &profile_vmic_levels_wideband[i] ;

		vmgs->use_analog = pvmgs->use_analog ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		vmgs->use_shift_paec_out = pvmgs->use_shift_paec_out ;
		vmgs->use_paec_tx_att = pvmgs->use_paec_tx_att ;
		vmgs->use_attlimit = pvmgs->use_attlimit ;
		vmgs->use_supmin = pvmgs->use_supmin ;
		vmgs->use_noiseattlimit = pvmgs->use_noiseattlimit ;
#endif

		DPRINT( "%s:  wide  vmg #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_AND_ADVANCE( vmgs, analog_levels, p, n, pvmgs ) ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		REASSIGN_FIELD_AND_ADVANCE( vmgs, shift_paec_out_levels, p, n, pvmgs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vmgs, shift_paec_out_levels_BT, p, n, pvmgs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vmgs, paec_tx_att_levels, p, n, pvmgs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vmgs, paec_tx_att_levels_BT, p, n, pvmgs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vmgs, attlimit, p, n, pvmgs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vmgs, supmin, p, n, pvmgs ) ;
		REASSIGN_FIELD_AND_ADVANCE( vmgs, noiseattlimit, p, n, pvmgs ) ;

		/* custom params */
		cpn = ARRAY_SIZE( pvmgs->cp.params ) ;
		DPRINT( "%s:  wide  vmg #%2d custom params \t@%p (%d)\n",
						__FUNCTION__, i, p, cpn ) ;
		vmgs->cp.count = cpn ;
		vmgs->cp.params = (void*)p ;
		/* assume sizeof( sc1445x_ae_ap_custom_param ) is even */
		p += ( cpn * sizeof( sc1445x_ae_ap_custom_param ) ) >> 1 ;
		cp = vmgs->cp.params ;
		pmcp = pvmgs->cp.params ;
		for( j = 0 ;  j < cpn ;  ++j, ++cp, ++pmcp ) {
			cp->use = pmcp->use ;
			cp->addr = pmcp->addr ;
			REASSIGN_FIELD_AND_ADVANCE( cp, values, p, n, pmcp ) ;
		}
#endif
	}
	INIT_CONST_FIELD( unsigned short, &ae_state->ap, vmic_lvl_count, n ) ;

	/* filters */
	for( i = 0 ;  i < 3 ;  ++i ) {
		for( j = 0 ;  j < FILTER_SIZE ;  ++j ) {
			ae_state->ap.narrowband_filters_TX1[i][j] =
					profile_narrowband_filters_TX1[i][j] ;
			ae_state->ap.narrowband_filters_TX2[i][j] =
					profile_narrowband_filters_TX2[i][j] ;
			ae_state->ap.narrowband_filters_TX3[i][j] =
					profile_narrowband_filters_TX3[i][j] ;
			ae_state->ap.wideband_filters_TX1[i][j] =
					profile_wideband_filters_TX1[i][j] ;
			ae_state->ap.wideband_filters_TX2[i][j] =
					profile_wideband_filters_TX2[i][j] ;
			ae_state->ap.wideband_filters_TX3[i][j] =
					profile_wideband_filters_TX3[i][j] ;

			ae_state->ap.narrowband_filters_RX1[i][j] =
					profile_narrowband_filters_RX1[i][j] ;
			ae_state->ap.narrowband_filters_RX2[i][j] =
					profile_narrowband_filters_RX2[i][j] ;
			ae_state->ap.narrowband_filters_RX3[i][j] =
					profile_narrowband_filters_RX3[i][j] ;
			ae_state->ap.wideband_filters_RX1[i][j] =
					profile_wideband_filters_RX1[i][j] ;
			ae_state->ap.wideband_filters_RX2[i][j] =
					profile_wideband_filters_RX2[i][j] ;
			ae_state->ap.wideband_filters_RX3[i][j] =
					profile_wideband_filters_RX3[i][j] ;
		}
	}

#if defined( CONFIG_SND_SC1445x_USE_PAEC )
#  if AUDIO_PROFILE_MAGIC == AUDIO_PROFILE_MAGIC_AP10
	/* PAEC state array setup */
	n = ARRAY_SIZE( profile_paec_band_loc ) ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: paec_band_loc \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_AND_ADVANCE( ae_state->ap.paec_band_loc, p, sz,
						profile_paec_band_loc ) ;
	DPRINT( "%s: paec_band_narrow \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_AND_ADVANCE( ae_state->ap.paec_band_narrow, p, sz,
						profile_paec_band_narrowband ) ;
	DPRINT( "%s: paec_band_wide \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_AND_ADVANCE( ae_state->ap.paec_band_wide, p, sz,
						profile_paec_band_wideband ) ;
	INIT_CONST_FIELD( unsigned short, &ae_state->ap, paec_band_count, n ) ;
#  endif

	/* PAEC parameters setup */
	n = ARRAY_SIZE( profile_paec_data_loc ) ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: paec_data_loc \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_AND_ADVANCE( ae_state->ap.paec_data_loc, p, sz,
						profile_paec_data_loc ) ;
	DPRINT( "%s: paec_data_narrow \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_AND_ADVANCE( ae_state->ap.paec_data_narrow, p, sz,
						profile_paec_data_narrowband ) ;
	DPRINT( "%s: paec_data_wide \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_AND_ADVANCE( ae_state->ap.paec_data_wide, p, sz,
						profile_paec_data_wideband ) ;
	INIT_CONST_FIELD( unsigned short, &ae_state->ap, paec_data_count, n ) ;

	/* Suppressor Setup*/
	n = ARRAY_SIZE( profile_supp_params_addr ) ;
	sz = n * sizeof(unsigned short*) ;
	DPRINT( "%s: supp_params_addr \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	pp = (void*)p ;
	REASSIGN_ARRAY_AND_ADVANCE( ae_state->ap.supp_params_addr, pp, sz,
						profile_supp_params_addr ) ;
	p = (void*)pp ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: supp_params_data_narrow \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_AND_ADVANCE( ae_state->ap.supp_params_data_narrow, p, sz,
					profile_supp_params_data_narrowband ) ;
	DPRINT( "%s: supp_params_data_wide \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_AND_ADVANCE( ae_state->ap.supp_params_data_wide, p, sz,
					profile_supp_params_data_wideband ) ;
	INIT_CONST_FIELD( unsigned short, &ae_state->ap, supp_params_count, n ) ;
#endif

#if defined( CONFIG_SND_SC1445x_USE_AEC )
	/* AEC setup */
	n = ARRAY_SIZE( profile_aec_params_data ) ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: aec_params_data \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_AND_ADVANCE( ae_state->ap.aec_params_data, p, sz,
						profile_aec_params_data ) ;
	INIT_CONST_FIELD( unsigned short, &ae_state->ap, aec_params_count, n ) ;
#endif

	if( p != ae_state->ap.data + ae_state->ap.data_nbytes ) {
		PRINT( "%s: wrote audio profile dynamic data up to %p\n"
			"%s: allocated space up to %p\n",
				__FUNCTION__, p, __FUNCTION__,
				ae_state->ap.data + ae_state->ap.data_nbytes ) ;
	}

	return SC1445x_AE_OK ;
#endif  /* SC1445x_AE_PCM_LINES_SUPPORT */
}

/* initialize audio engine and allocate all required memory */
short sc1445x_ae_init_engine( sc1445x_ae_state* ae_state )
{
	short res ;
	unsigned short i, dsp1_int_mask ;

	CHECK_AE_STATE( ae_state ) ;

	res = sc1445x_internal_alloc_mem( ae_state, SC1445x_AE_SPK_COUNT,
					SC1445x_AE_MIC_COUNT,
					SC1445x_AE_MAX_AUDIO_CHANNELS ) ;
	if( res != SC1445x_AE_OK ) {
		sc1445x_internal_free_mem( ae_state ) ;
		return res ;
	}

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	res = sc1445x_ae_reset_audio_profile( ae_state ) ;
	if( res != SC1445x_AE_OK ) {
		sc1445x_internal_free_mem( ae_state ) ;
		return res ;
	}
#endif

	/* set up pointers to buffers and an invalid codec for each channel */
	for( i = 0 ;  i < SC1445x_AE_MAX_AUDIO_CHANNELS ;  ++i ) {
		/* the buffer pointers never change */
		INIT_CONST_FIELD( unsigned char*,
				&ae_state->audio_channels[i], playback_buffer,
						sc1445x_ae_playback_bufs[i] ) ;
		INIT_CONST_FIELD( unsigned char*,
				&ae_state->audio_channels[i], capture_buffer,
						sc1445x_ae_capture_bufs[i] ) ;
		/* the codec types will change when the channel is activated */
		INIT_CONST_FIELD( unsigned short, 
			&ae_state->audio_channels[i].enc_codec, type,
						SC1445x_AE_CODEC_UNDEFINED ) ;
		INIT_CONST_FIELD( unsigned short, 
			&ae_state->audio_channels[i].dec_codec, type,
						SC1445x_AE_CODEC_UNDEFINED ) ;
	}

	INIT_CONST_FIELD( unsigned short*,
			&ae_state->raw_pcm, buffer, (unsigned short*)0x18280 ) ;
	INIT_CONST_FIELD( unsigned short*,
			&ae_state->raw_pcm, block_status,
						(unsigned short*)0x1aa80 ) ;
	INIT_CONST_FIELD( unsigned short,
			&ae_state->raw_pcm, block_size, 320 ) ;
	INIT_CONST_FIELD( unsigned short,
			&ae_state->raw_pcm, block_count, 16 ) ;

	sc1445x_internal_init_regs( ae_state ) ;

	SetBits( INT0_PRIORITY_REG, DSP2_INT_PRIO, 6 ) ;
	/* Mask all DSP interrupts */
	dsp1_int_mask = GetWord( DSP1_INT_MASK_REG ) ;
	SetWord( DSP1_INT_MASK_REG, 0 ) ;
	SetWord( DSP2_INT_MASK_REG, 0 ) ;
	/* Clear DSP interrupts */
	SetWord( RESET_INT_PENDING_REG, DSP2_INT_PEND ) ;
	SetWord( RESET_INT_PENDING_REG, DSP1_INT_PEND ) ;
	/* Set PCM master */
#ifdef CONFIG_PCM_SLAVE
	SetBits( DSP_PCM_CTRL_REG, PCM_MASTER, 0 ) ;
	SetBits( DSP_PCM_CTRL_REG, DSP_PCM_SYNC, 1 ) ;
#else
	SetBits( DSP_PCM_CTRL_REG, PCM_MASTER, 1 ) ;
#endif	
	SetBits( DSP_PCM_CTRL_REG, PCM_EN, 1 ) ;

	sc1445x_internal_init_dsp_mem() ;
	sc1445x_internal_install_isr( generic_dsp1_isr_normal,
					generic_dsp2_isr_normal, ae_state ) ;

	SetWord( DSP1_IRQ_START_REG, 0x0000 ) ;
	SetWord( DSP2_IRQ_START_REG, 0x0000 ) ;
	SetWord( DSP1_PC_START_REG, 0x0010 ) ;
	SetWord( DSP2_PC_START_REG, 0x0010 ) ;
	SetWord( DSP1_INT_MASK_REG, dsp1_int_mask | 0x6EE3 ) ;
	SetWord( DSP2_INT_MASK_REG, 0x0B06 ) ;
#if 0
#if defined( CHECK_DSP_IRQ_OVERFLOW )
	SetWord( DSP1_OVERFLOW_REG, 0xFFFF ) ;
	SetWord( DSP2_OVERFLOW_REG, 0xFFFF ) ;
#endif
#endif

#if defined( DSP_DEBUG_WITH_GPIO )
	P0_02_MODE_REG = 0x306 ;
	DSP_ZCROSS1_OUT_REG = 0 ;
#endif

	SetWord( DSP2_CTRL_REG, 0x0004 ) ;
	SetWord( DSP1_CTRL_REG, 0x0004 ) ;

#if defined( DEBUG_WITH_GPIO ) || defined( TEST_WITH_MARKED_PACKET )
	DEBUG_WITH_GPIO_SETUP ;
	DEBUG_WITH_GPIO_LOWER ;
#endif

	/* initial values for turning CLASSD off/on */
	ae_state->classd_ctrl_val[0] = 0xcc0d ;  /* OFF */
	ae_state->classd_ctrl_val[1] = 0xcc0c ;  /* ON */

	sc1445x_ae_set_iface_mode( ae_state, SC1445x_AE_IFACE_MODE_IDLE ) ;

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	sc1445x_ae_set_vspk_volume( ae_state,
				ae_state->ap.vspk_lvl_count >> 1 ) ;
	sc1445x_ae_set_vspk_dtmf_level( ae_state,
				ae_state->ap.vspk_lvl_count >> 1 ) ;
	sc1445x_ae_set_vspk_pcm_level( ae_state,
				ae_state->ap.vspk_lvl_count >> 1 ) ;
	sc1445x_ae_set_vmic_gain( ae_state,
				ae_state->ap.vmic_lvl_count >> 1 ) ;

	sc1445x_internal_enforce_phone_op_mode( ae_state ) ;
#endif

#if 0
	/*
	 * 230410GF
	 * reduce classd noise threshold to -54dB (from -48dB) to
	 * prevent noise gap at lower volume 
	 */
	SetWord( CLASSD_NR_REG, 0x0857 ) ;
#else
	/*
	 * noise threshold -72dB
	 * noise reduction hysteresis 18dB
	 */
	CLASSD_NR_REG = 0x0b5f ;
#endif

#if defined( CONFIG_SND_SC1445x_MUX_AUDIO_STREAMS_TO_PCM_3 )
	ae_state->mux_audio_streams_to_pcm_3 = 1 ;
	sc1445x_internal_send_dsp_cmd( 0x002d, 1 ) ;
#else
	ae_state->mux_audio_streams_to_pcm_3 = 0 ;
#endif

#if defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD ) \
		|| defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD_CNX ) \
		|| defined( CONFIG_L_V1_BOARD)
	sc1445x_internal_send_dsp_cmd( 0x003f, 1 ) ;
	sc1445x_internal_send_dsp_cmd( 0x0040, 1 ) ;
	//THIS COMMAND CHANGES THE FADE IN AND FADE OUT PERIOD OF THE INTERFACE
#  define FADE_IN_OUT_PERIOD 0x400
//#  define FADE_IN_OUT_PERIOD 0x20
	//the actual period in samples is (0x8000/FADE_IN_OUT_PERIOD)
	//the minimum value that can be used is 0x20.
	sc1445x_internal_send_dsp_cmd( 0x003a,  FADE_IN_OUT_PERIOD) ;
	//Speech to RX direction will be muted when dtmf is played locally
	sc1445x_internal_send_dsp_cmd( 0x0041, 1) ;
#endif

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	/* set up gains of Line Echo Canceller */
	sc1445x_internal_send_dsp_cmd( 0x002e, 0x4024 ) ;
	sc1445x_internal_send_dsp_cmd( 0x002e, 0x4025 ) ;
	sc1445x_internal_send_dsp_cmd( 0x002e, 0x4026 ) ;
	sc1445x_internal_send_dsp_cmd( 0x002e, 0x4027 ) ;
#endif

	return SC1445x_AE_OK ;
}

/* finalize audio engine and free all allocated memory */
short sc1445x_ae_finalize_engine( sc1445x_ae_state* ae_state )
{
	short res ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	/* disable DSP interrupts */
	SetWord( DSP2_INT_MASK_REG, 0 ) ;
	SetWord( DSP1_INT_MASK_REG, 0 ) ;
	/* clear DSP interrupts */
	SetWord( RESET_INT_PENDING_REG, DSP2_INT_PEND ) ;
	SetWord( RESET_INT_PENDING_REG, DSP1_INT_PEND ) ;
	//SetBits( INT0_PRIORITY_REG, DSP2_INT_PRIO, 0 ) ;
	SetWord( DSP2_INT_REG, 0xffff ) ;
	SetWord( DSP1_INT_REG, 0xffff ) ;
	/* shut down DSPs */
	SetWord( DSP1_CTRL_REG, 0 ) ;
	SetWord( DSP2_CTRL_REG, 0 ) ;
	// TODO: should we also turn off the codec?

	res = sc1445x_internal_free_mem( ae_state ) ;
#if defined( CHECK_DSP_IRQ_OVERFLOW ) || defined( SMOOTH_IFACE_TRANSITION ) \
					|| defined( SC1445x_AE_ATA_SUPPORT )
	free_irq( DSP1_INT, (void*)ae_state ) ;
#endif
	free_irq( DSP2_INT, (void*)ae_state ) ;

	return res ;
}


/************/
/* SPEAKERS */
/************/

static void sc1445x_internal_do_pcm_muting( const sc1445x_ae_state* ae_state )
{
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	unsigned short val_pcm = 0 ;
	short use_pcm ;
	const sc1445x_ae_iface_mode mode = ae_state->iface_mode ;

	if( mode != SC1445x_AE_IFACE_MODE_IDLE ) {
#  if defined( SC1445x_AE_NATIVE_DECT_SUPPORT )

		use_pcm = !!( SC1445x_AE_IFACE_MODE_BT_HEADSET == mode ) ;

#  else

		use_pcm = !!( SC1445x_AE_IFACE_MODE_WIRELESS == mode  ||
				SC1445x_AE_IFACE_MODE_BT_HEADSET == mode ) ;

#  endif
		use_pcm |= ae_state->using_external_codec_mic ;
		use_pcm |= ae_state->using_external_codec_spk ;

		if( use_pcm ) {
			if( !ae_state->spks[ae_state->spk_in_use].is_muted )
				val_pcm |= 0x01 ;
			if( !ae_state->mics[ae_state->mic_in_use].is_muted )
				val_pcm |= 0x10 ;
		}
	}

	if( ae_state->op_mode.val <= 1 ) {
		/* use new commands */

		/* mic */
		sc1445x_internal_send_dsp_cmd( 0x003c, !!(val_pcm & 0x0f) ) ;
		/* spk */
		sc1445x_internal_send_dsp_cmd( 0x003d, !!(val_pcm & 0xf0) ) ;
	} else {
		DPRINT( "%s: sending DSP cmd (001e, %04x)\n", __FUNCTION__,
								val_pcm ) ;
		sc1445x_internal_send_dsp_cmd( 0x001e, val_pcm ) ;
	}
#endif
}

static void sc1445x_internal_do_ramio_muting( const sc1445x_ae_state* ae_state )
{
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	unsigned short val_ramio = 0 ;
#  if defined( SC1445x_AE_NATIVE_DECT_SUPPORT )
	const sc1445x_ae_iface_mode mode = ae_state->iface_mode ;

	if( mode != SC1445x_AE_IFACE_MODE_WIRELESS ) {
		val_ramio = 0 ;
	} else {
#    if !defined( USE_SPECIAL_WIRELESS_MODE )
		/* may use both mic and spk */
		if( !ae_state->spks[ae_state->spk_in_use].is_muted )
			val_ramio |= 0x01 ;
		if( !ae_state->mics[ae_state->mic_in_use].is_muted )
			val_ramio |= 0x10 ;
#    else
		/* may use only mic */
		if( !ae_state->mics[ae_state->mic_in_use].is_muted )
			val_ramio |= 0x10 ;
#    endif
		if( SC1445x_AE_LINE_TYPE_NATIVE_DECT_NARROW ==
				ae_state->dect_headset_type )
			val_ramio |= 0x0400 ;
		else
			val_ramio |= 0x0500 ;
	}
	DPRINT( "%s: dect_headset_type=%d\n", __FUNCTION__,
			ae_state->dect_headset_type ) ;

#  else

	/* let the default, 0 */
	/* val_ramio = 0 ; */

#  endif

	DPRINT( "%s: sending DSP cmd (001f, %04x)\n", __FUNCTION__, val_ramio );
	sc1445x_internal_send_dsp_cmd( 0x001f, val_ramio ) ;
#endif
}

/* mute the active speaker (retaining the current volume) */
short sc1445x_ae_mute_spk( sc1445x_ae_state* ae_state )
{
	struct __CODEC_LSR_REG* codec_lsr_reg = (void*)&CODEC_LSR_REG ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	/* actually mute */
	codec_lsr_reg->BITFLD_LSRN_PD = 1 ;
	codec_lsr_reg->BITFLD_LSRP_PD = 1 ;
	/* turn off the external spk (thanks to Mikko) */
	CLASSD_CTRL_REG = ae_state->classd_ctrl_val[0] ;
	ae_state->spks[ae_state->spk_in_use].is_muted = 1 ;
	sc1445x_internal_do_pcm_muting( ae_state ) ;
	sc1445x_internal_do_ramio_muting( ae_state ) ;
	return SC1445x_AE_OK ;
}

/* unmute the active speaker (retaining the previous volume) */
short sc1445x_ae_unmute_spk( sc1445x_ae_state* ae_state )
{
	struct __CODEC_LSR_REG* codec_lsr_reg = (void*)&CODEC_LSR_REG ;
	short vspk ;
	sc1445x_ae_iface_mode im ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	if( ae_state->iface_mode != SC1445x_AE_IFACE_MODE_IDLE ) {
		im = get_audio_profile_iface_mode( ae_state ) ;
		vspk = vspk_from_iface_mode( im,
				ae_state->using_external_codec_spk ) ;

		/* actually unmute */
		if( vspk & 3 ) {
			codec_lsr_reg->BITFLD_LSRN_PD = 0 ;
			codec_lsr_reg->BITFLD_LSRP_PD = 0 ;
		}
		/* turn on the external spk (thanks to Mikko) */
		if( vspk & 4 )
			CLASSD_CTRL_REG = ae_state->classd_ctrl_val[1] ;
	}
	ae_state->spks[ae_state->spk_in_use].is_muted = 0 ;
	sc1445x_internal_do_pcm_muting( ae_state ) ;
	sc1445x_internal_do_ramio_muting( ae_state ) ;

	return SC1445x_AE_OK ;
}


/***************/
/* MICROPHONES */
/***************/

/* mute the active microphone (retaining the current gain) */
short sc1445x_ae_mute_mic( sc1445x_ae_state* ae_state )
{
	short i ;
	struct __CODEC_MIC_REG* codec_mic_reg = (void*)&CODEC_MIC_REG ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	/* inform DSP about the mute */
	sc1445x_internal_send_dsp_cmd( 0x0042, 0x0001 ) ;

	/* actually mute */
	codec_mic_reg->BITFLD_MIC_PD = 1 ;
	ae_state->mics[ae_state->mic_in_use].is_muted = 1 ;
	sc1445x_internal_do_pcm_muting( ae_state ) ;
	sc1445x_internal_do_ramio_muting( ae_state ) ;

#if defined( DO_MIC_DC_CANCELLATION )
	/* adjust vmic gain, to recalculate DC cancellation */
	do_sc1445x_ae_set_vmic_gain( ae_state ) ;
#endif

	/* mute all vmics */
	for( i = 0 ;  i < ARRAY_SIZE( ae_state->vmics ) ;  ++i )
		ae_state->vmics[i].is_muted = 1 ;

	return SC1445x_AE_OK ;
}

/* unmute the active microphone (retaining the previous gain) */
short sc1445x_ae_unmute_mic( sc1445x_ae_state* ae_state )
{
	short i ;
	short vmic ;
	sc1445x_ae_iface_mode im ;
	struct __CODEC_MIC_REG* codec_mic_reg = (void*)&CODEC_MIC_REG ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	im = get_audio_profile_iface_mode( ae_state ) ;
	vmic = vmic_from_iface_mode( im, ae_state->using_external_codec_mic ) ;

	/* actually unmute */
	if( 1 == vmic ) {
		/* turn on the handset mic */
#if defined( CONFIG_SC14450 )
		codec_mic_reg->BITFLD_MICH_ON = 0 ;
#elif defined( CONFIG_SC14452 )
		codec_mic_reg->BITFLD_MICHN_ON = 0 ;
		codec_mic_reg->BITFLD_MICHP_ON = 0 ;
#endif
		codec_mic_reg->BITFLD_MIC_MODE = 0 ;
	} else {
		/* turn on the external/headset mic */
#if defined( CONFIG_SC14450 )
		codec_mic_reg->BITFLD_MIC_MODE = 2 ;
		codec_mic_reg->BITFLD_MICH_ON = 1 ;
#elif defined( CONFIG_SC14452 )
		//gflamis
		codec_mic_reg->BITFLD_MIC_MODE = 0 ;
		codec_mic_reg->BITFLD_MICHN_ON = 1 ;
		codec_mic_reg->BITFLD_MICHP_ON = 1 ;
#endif
	}
	ae_state->mics[ae_state->mic_in_use].is_muted = 0 ;
	if( ae_state->iface_mode != SC1445x_AE_IFACE_MODE_IDLE )
		codec_mic_reg->BITFLD_MIC_PD = 0 ;
	sc1445x_internal_do_pcm_muting( ae_state ) ;
	sc1445x_internal_do_ramio_muting( ae_state ) ;

#if defined( DO_MIC_DC_CANCELLATION )
	/* adjust vmic gain, to recalculate DC cancellation */
	do_sc1445x_ae_set_vmic_gain( ae_state ) ;
#endif

	/* inform DSP about the unmute */
	sc1445x_internal_send_dsp_cmd( 0x0042, 0x0000 ) ;

	/* unmute all vmics */
	for( i = 0 ;  i < ARRAY_SIZE( ae_state->vmics ) ;  ++i )
		ae_state->vmics[i].is_muted = 0 ;

	return SC1445x_AE_OK ;
}



/*******/
/* AEC */
/*******/

/* turn on Acoustic Echo Cancellation */
short sc1445x_ae_set_aec_on( sc1445x_ae_state* ae_state )
{
	unsigned short i ;
	sc1445x_ae_iface_mode im ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
	const short is_wideband = currently_wideband() ;

	/* we have to set up some values for PAEC before turning it on */
	static unsigned short* const paec_data = (unsigned short*)0x10720 ;
	const unsigned short* pd = is_wideband ?  ae_state->ap.paec_data_wide
					       :  ae_state->ap.paec_data_narrow;
#  if defined( HAVE_PAEC_SUPPRESSOR )
	const unsigned short* supp_params = is_wideband ?
					ae_state->ap.supp_params_data_wide
				:	ae_state->ap.supp_params_data_narrow ;
#  endif
#endif

#if defined( CONFIG_SND_SC1445x_USE_AEC )
	/* we have to set up some values for AEC before turning it on */
	static unsigned short* const aec_params = (unsigned short*)0x10432 ;
	static unsigned short* const aec_coeffs = (unsigned short*)0x10a3c ;
#endif

	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	im = get_audio_profile_iface_mode( ae_state ) ;

#if defined( CONFIG_SND_SC1445x_USE_PAEC )
	/* select between reduced or full PAEC */
	if( SC1445x_AE_IFACE_MODE_HANDSET == im  &&  !is_wideband )
		sc1445x_internal_send_dsp_cmd( 0x0028, 0 ) ;
	else{
		if(!is_wideband)
			sc1445x_internal_send_dsp_cmd( 0x0028, 1 ) ;
		else	
			sc1445x_internal_send_dsp_cmd( 0x0028, 2 ) ;
	}	
#endif

	/* the wireless handset handles echo cancellation itself */
	/* the Bluetooth handset handles echo cancellation itself */
	if( SC1445x_AE_IFACE_MODE_WIRELESS == im  ||
			SC1445x_AE_IFACE_MODE_BT_HEADSET == im ) {
		ae_state->AEC_enabled = 1 ;
		return SC1445x_AE_OK ;
	}

#if defined( CONFIG_SND_SC1445x_USE_PAEC )
	for( i = 0 ;  i < ae_state->ap.paec_data_count ;  ++i )
		paec_data[ae_state->ap.paec_data_loc[i]] = pd[i] ;
	
#  if defined( HAVE_PAEC_SUPPRESSOR )
	for( i = 0 ;  i < ae_state->ap.supp_params_count ;  ++i )
		*ae_state->ap.supp_params_addr[i] = supp_params[i] ;
#  endif

#endif

#if defined( CONFIG_SND_SC1445x_USE_AEC )
	for( i = 0 ;  i < ARRAY_SIZE( profile_aec_params_data ) ;  ++i )
		aec_params[i] = profile_aec_params_data[i] ;

	memset( aec_coeffs, 0, 1024 ) ;
#endif

	ae_state->AEC_enabled = 1 ;
	/* actually enable AEC */
	sc1445x_internal_send_dsp_cmd( 0x0006, 1 ) ;
	DPRINT( PRINT_LEVEL "%s: sent command to turn echo cancellation ON\n",
								__FUNCTION__ ) ;
	return SC1445x_AE_OK ;
}

/* turn off Acoustic Echo Cancellation */
short sc1445x_ae_set_aec_off( sc1445x_ae_state* ae_state )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	ae_state->AEC_enabled = 0 ;
	/* actually disable AEC */
	sc1445x_internal_send_dsp_cmd( 0x0006, 0 ) ;
	DPRINT( PRINT_LEVEL "%s: sent command to turn echo canceallation OFF\n",
								__FUNCTION__ ) ;
	return SC1445x_AE_OK ;
}


/*****************/
/* AUDIO PROFILE */
/*****************/

/* get the total size of the dynamic audio profile arrays, in bytes */
short sc1445x_ae_get_audio_profile_dyn_size( const sc1445x_ae_state* ae_state,
						       unsigned* nbytes )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( nbytes ) ;

	*nbytes = ae_state->ap.data_nbytes ;

	return SC1445x_AE_OK ;
}

static void sc1445x_internal_copy_audio_profile(
					sc1445x_ae_audio_profile* dst_ap,
					const sc1445x_ae_audio_profile* src_ap )
{
	/* this is an internal function, we don't check args */

	unsigned short i, j, n, cpn ;
	const sc1445x_ae_vspk_volumes* src_vsvs ;
	const sc1445x_ae_vmic_gains* src_vmgs ;
	sc1445x_ae_vspk_volumes* dst_vsvs ;
	sc1445x_ae_vmic_gains* dst_vmgs ;
	unsigned short* p ;
	unsigned short** pp ;
	int sz ;
	const sc1445x_ae_ap_custom_param* src_cp ;
	sc1445x_ae_ap_custom_param* dst_cp ;

	memcpy( dst_ap->data, src_ap->data, dst_ap->data_nbytes ) ;

	/* update array pointers and copy data */
	p = dst_ap->data ;

	/* vspk_lvl_narrow and vspk_lvl_wide */
	n = src_ap->vspk_lvl_count ;
	for( i = 0 ;  i < SC1445x_AE_IFACE_MODE_COUNT_IN_AP ;  ++i ) {
		dst_vsvs = &dst_ap->vspk_lvl_narrow[i] ;
		src_vsvs = &src_ap->vspk_lvl_narrow[i] ;

		dst_vsvs->use_analog = src_vsvs->use_analog ;
		dst_vsvs->use_rx_path_att = src_vsvs->use_rx_path_att ;
		dst_vsvs->use_rx_path_shift = src_vsvs->use_rx_path_shift ;
		dst_vsvs->use_sidetone_att = src_vsvs->use_sidetone_att ;
		dst_vsvs->use_ext_spk_att = src_vsvs->use_ext_spk_att ;
		dst_vsvs->use_shift_ext_spk = src_vsvs->use_shift_ext_spk ;
		dst_vsvs->use_tone_vol = src_vsvs->use_tone_vol ;
		dst_vsvs->use_classd_vout = src_vsvs->use_classd_vout ;
		dst_vsvs->use_pcm_gain_pre = src_vsvs->use_pcm_gain_pre ;
		dst_vsvs->use_pcm_gain_shift = src_vsvs->use_pcm_gain_shift ;
		dst_vsvs->use_loopgain_rxpl = src_vsvs->use_loopgain_rxpl ;
		dst_vsvs->use_ng_threshold = src_vsvs->use_ng_threshold ;

		DPRINT( "%s: narrow vsv #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, analog_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, rx_path_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, rx_path_shift_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, sidetone_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ext_spk_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ring_playback_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, shift_ext_spk_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ring_tone_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, call_tone_codec_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, call_tone_classd_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, dtmf_tone_codec_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, dtmf_tone_classd_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, classd_vout, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_pre, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_shift, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_pre_ringtone, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_shift_ringtone, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, loopgain_rxpl_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ng_threshold_levels, p, n ) ;

		/* custom params */
		cpn = src_vsvs->cp.count ;
		DPRINT( "%s: narrow vsv #%2d custom params \t@%p (%d)\n",
						__FUNCTION__, i, p, cpn ) ;
		dst_vsvs->cp.count = cpn ;
		dst_vsvs->cp.params = (void*)p ;
		/* assume sizeof( sc1445x_ae_ap_custom_param ) is even */
		p += ( cpn * sizeof( sc1445x_ae_ap_custom_param ) ) >> 1 ;
		src_cp = src_vsvs->cp.params ;
		dst_cp = dst_vsvs->cp.params ;
		for( j = 0 ;  j < cpn ;  ++j, ++src_cp, ++dst_cp ) {
			dst_cp->use = src_cp->use ;
			dst_cp->addr = src_cp->addr ;
			REASSIGN_FIELD_PTR_AND_ADVANCE( dst_cp, values, p, n ) ;
		}


		dst_vsvs = &dst_ap->vspk_lvl_wide[i] ;
		src_vsvs = &src_ap->vspk_lvl_wide[i] ;

		dst_vsvs->use_analog = src_vsvs->use_analog ;
		dst_vsvs->use_rx_path_att = src_vsvs->use_rx_path_att ;
		dst_vsvs->use_rx_path_shift = src_vsvs->use_rx_path_shift ;
		dst_vsvs->use_sidetone_att = src_vsvs->use_sidetone_att ;
		dst_vsvs->use_ext_spk_att = src_vsvs->use_ext_spk_att ;
		dst_vsvs->use_shift_ext_spk = src_vsvs->use_shift_ext_spk ;
		dst_vsvs->use_tone_vol = src_vsvs->use_tone_vol ;
		dst_vsvs->use_classd_vout = src_vsvs->use_classd_vout ;
		dst_vsvs->use_pcm_gain_pre = src_vsvs->use_pcm_gain_pre ;
		dst_vsvs->use_pcm_gain_shift = src_vsvs->use_pcm_gain_shift ;
		dst_vsvs->use_loopgain_rxpl = src_vsvs->use_loopgain_rxpl ;
		dst_vsvs->use_ng_threshold = src_vsvs->use_ng_threshold ;

		DPRINT( "%s:  wide  vsv #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, analog_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, rx_path_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, rx_path_shift_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, sidetone_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ext_spk_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ring_playback_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, shift_ext_spk_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ring_tone_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, call_tone_codec_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, call_tone_classd_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, dtmf_tone_codec_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, dtmf_tone_classd_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, classd_vout, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_pre, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_shift, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_pre_ringtone, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_shift_ringtone, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, loopgain_rxpl_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ng_threshold_levels, p, n ) ;

		/* custom params */
		cpn = src_vsvs->cp.count ;
		DPRINT( "%s:  wide  vsv #%2d custom params \t@%p (%d)\n",
						__FUNCTION__, i, p, cpn ) ;
		dst_vsvs->cp.count = cpn ;
		dst_vsvs->cp.params = (void*)p ;
		/* assume sizeof( sc1445x_ae_ap_custom_param ) is even */
		p += ( cpn * sizeof( sc1445x_ae_ap_custom_param ) ) >> 1 ;
		src_cp = src_vsvs->cp.params ;
		dst_cp = dst_vsvs->cp.params ;
		for( j = 0 ;  j < cpn ;  ++j, ++src_cp, ++dst_cp ) {
			dst_cp->use = src_cp->use ;
			dst_cp->addr = src_cp->addr ;
			REASSIGN_FIELD_PTR_AND_ADVANCE( dst_cp, values, p, n ) ;
		}
	}
	INIT_CONST_FIELD( unsigned short, dst_ap, vspk_lvl_count, n ) ;

	/* vmic_lvl_narrow and vmic_lvl_wide */
	n = src_ap->vmic_lvl_count ;
	for( i = 0 ;  i < SC1445x_AE_IFACE_MODE_COUNT_IN_AP ;  ++i ) {
		dst_vmgs = &dst_ap->vmic_lvl_narrow[i] ;
		src_vmgs = &src_ap->vmic_lvl_narrow[i] ;

		dst_vmgs->use_analog = src_vmgs->use_analog ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		dst_vmgs->use_shift_paec_out = src_vmgs->use_shift_paec_out ;
		dst_vmgs->use_paec_tx_att = src_vmgs->use_paec_tx_att ;
		dst_vmgs->use_attlimit = src_vmgs->use_attlimit ;
		dst_vmgs->use_supmin = src_vmgs->use_supmin ;
		dst_vmgs->use_noiseattlimit = src_vmgs->use_noiseattlimit ;
#endif

		DPRINT( "%s: narrow vmg #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, analog_levels, p, n ) ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, shift_paec_out_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, shift_paec_out_levels_BT, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, paec_tx_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, paec_tx_att_levels_BT, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, attlimit, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, supmin, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, noiseattlimit, p, n ) ;

		/* custom params */
		cpn = src_vmgs->cp.count ;
		DPRINT( "%s: narrow vmg #%2d custom params \t@%p (%d)\n",
						__FUNCTION__, i, p, cpn ) ;
		dst_vmgs->cp.count = cpn ;
		dst_vmgs->cp.params = (void*)p ;
		/* assume sizeof( sc1445x_ae_ap_custom_param ) is even */
		p += ( cpn * sizeof( sc1445x_ae_ap_custom_param ) ) >> 1 ;
		src_cp = src_vmgs->cp.params ;
		dst_cp = dst_vmgs->cp.params ;
		for( j = 0 ;  j < cpn ;  ++j, ++src_cp, ++dst_cp ) {
			dst_cp->use = src_cp->use ;
			dst_cp->addr = src_cp->addr ;
			REASSIGN_FIELD_PTR_AND_ADVANCE( dst_cp, values, p, n ) ;
		}
#endif


		dst_vmgs = &dst_ap->vmic_lvl_wide[i] ;
		src_vmgs = &src_ap->vmic_lvl_wide[i] ;

		dst_vmgs->use_analog = src_vmgs->use_analog ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		dst_vmgs->use_shift_paec_out = src_vmgs->use_shift_paec_out ;
		dst_vmgs->use_paec_tx_att = src_vmgs->use_paec_tx_att ;
		dst_vmgs->use_attlimit = src_vmgs->use_attlimit ;
		dst_vmgs->use_supmin = src_vmgs->use_supmin ;
		dst_vmgs->use_noiseattlimit = src_vmgs->use_noiseattlimit ;
#endif

		DPRINT( "%s:  wide  vmg #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, analog_levels, p, n ) ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, shift_paec_out_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, shift_paec_out_levels_BT, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, paec_tx_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, paec_tx_att_levels_BT, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, attlimit, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, supmin, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, noiseattlimit, p, n ) ;

		/* custom params */
		cpn = src_vmgs->cp.count ;
		DPRINT( "%s:  wide  vmg #%2d custom params \t@%p (%d)\n",
						__FUNCTION__, i, p, cpn ) ;
		dst_vmgs->cp.count = cpn ;
		dst_vmgs->cp.params = (void*)p ;
		/* assume sizeof( sc1445x_ae_ap_custom_param ) is even */
		p += ( cpn * sizeof( sc1445x_ae_ap_custom_param ) ) >> 1 ;
		src_cp = src_vmgs->cp.params ;
		dst_cp = dst_vmgs->cp.params ;
		for( j = 0 ;  j < cpn ;  ++j, ++src_cp, ++dst_cp ) {
			dst_cp->use = src_cp->use ;
			dst_cp->addr = src_cp->addr ;
			REASSIGN_FIELD_PTR_AND_ADVANCE( dst_cp, values, p, n ) ;
		}
#endif
	}
	INIT_CONST_FIELD( unsigned short, dst_ap, vmic_lvl_count, n ) ;

	/* filters */
	for( i = 0 ;  i < 3 ;  ++i ) {
		for( j = 0 ;  j < FILTER_SIZE ;  ++j ) {
			dst_ap->narrowband_filters_TX1[i][j] =
					src_ap->narrowband_filters_TX1[i][j] ;
			dst_ap->narrowband_filters_TX2[i][j] =
					src_ap->narrowband_filters_TX2[i][j] ;
			dst_ap->narrowband_filters_TX3[i][j] =
					src_ap->narrowband_filters_TX3[i][j] ;
			dst_ap->wideband_filters_TX1[i][j] =
					src_ap->wideband_filters_TX1[i][j] ;
			dst_ap->wideband_filters_TX2[i][j] =
					src_ap->wideband_filters_TX2[i][j] ;
			dst_ap->wideband_filters_TX3[i][j] =
					src_ap->wideband_filters_TX3[i][j] ;

			dst_ap->narrowband_filters_RX1[i][j] =
					src_ap->narrowband_filters_RX1[i][j] ;
			dst_ap->narrowband_filters_RX2[i][j] =
					src_ap->narrowband_filters_RX2[i][j] ;
			dst_ap->narrowband_filters_RX3[i][j] =
					src_ap->narrowband_filters_RX3[i][j] ;
			dst_ap->wideband_filters_RX1[i][j] =
					src_ap->wideband_filters_RX1[i][j] ;
			dst_ap->wideband_filters_RX2[i][j] =
					src_ap->wideband_filters_RX2[i][j] ;
			dst_ap->wideband_filters_RX3[i][j] =
					src_ap->wideband_filters_RX3[i][j] ;
		}
	}

#if defined( CONFIG_SND_SC1445x_USE_PAEC )
#  if AUDIO_PROFILE_MAGIC == AUDIO_PROFILE_MAGIC_AP10
	/* PAEC state array setup */
	n = src_ap->paec_band_count ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: paec_band_loc \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_band_loc, p, sz ) ;
	DPRINT( "%s: paec_band_narrow \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_band_narrow, p, sz ) ;
	DPRINT( "%s: paec_band_wide \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_band_wide, p, sz ) ;
	INIT_CONST_FIELD( unsigned short, dst_ap, paec_band_count, n ) ;
#  endif

	/* PAEC parameters setup */
	n = src_ap->paec_data_count ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: paec_data_loc \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_data_loc, p, sz ) ;
	DPRINT( "%s: paec_date_narrow \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_data_narrow, p, sz ) ;
	DPRINT( "%s: paec_data_wide \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_data_wide, p, sz ) ;
	INIT_CONST_FIELD( unsigned short, dst_ap, paec_data_count, n ) ;

	/* Suppressor Setup*/
	n = src_ap->supp_params_count ;
	sz = n * sizeof(unsigned short*) ;
	DPRINT( "%s: supp_params_addr \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	pp = (void*)p ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->supp_params_addr, pp, sz ) ;
	p = (void*)pp ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: supp_params_data_narrow \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->supp_params_data_narrow, p, sz ) ;
	DPRINT( "%s: supp_params_data_wide \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->supp_params_data_wide, p, sz ) ;
	INIT_CONST_FIELD( unsigned short, dst_ap, supp_params_count, n ) ;
#endif

#if defined( CONFIG_SND_SC1445x_USE_AEC )
	/* AEC setup */
	n = src_ap->aec_params_count ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: aec_params_data \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->aec_params_data, p, sz ) ;
	INIT_CONST_FIELD( unsigned short, dst_ap, aec_params_count, n ) ;
#endif

	if( p != dst_ap->data + dst_ap->data_nbytes ) {
		PRINT( "%s: wrote audio profile dynamic data up to %p\n"
			"%s: allocated space up to %p\n",
				__FUNCTION__, p, __FUNCTION__,
				dst_ap->data + dst_ap->data_nbytes ) ;
	}
}

/*
 * copy the current audio profile settings to the passed struct
 *
 * NOTE	first, call sc1445x_ae_get_audio_profile_dyn_size() to get the
 * 	sizes of the variable-lenth arrays
 * 	then, allocate memory for these arrays
 * 	finally, call sc1445x_ae_get_audio_profile()
 * 	all operations should be made on the same
 * 	sc1445x_ae_audio_profile struct
 */
short sc1445x_ae_get_audio_profile( const sc1445x_ae_state* ae_state,
					       sc1445x_ae_audio_profile* ap )
{
	const sc1445x_ae_audio_profile* ae_ap ;

	NOT_AVAILABLE_WITH_PCM_LINES

	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( ap ) ;

	ae_ap = &ae_state->ap ;
	if( ap->data_nbytes != ae_ap->data_nbytes  ||  !ap->data ) {
		PRINT( sc1445x_ae_incompatible_audio_profile_error,
								__FUNCTION__ ) ;
		return SC1445x_AE_ERR_INCOMPATIBLE_AUDIO_PROFILE ;
	}

	/* there is only so much checking we can do... */
	sc1445x_internal_copy_audio_profile( ap, ae_ap ) ;

	return SC1445x_AE_OK ;
}

/* set the audio profile settings */
short sc1445x_ae_set_audio_profile( sc1445x_ae_state* ae_state,
					const sc1445x_ae_audio_profile* ap )
{
	sc1445x_ae_audio_profile* ae_ap ;
	unsigned nbytes ;
	void* p ;

	NOT_AVAILABLE_WITH_PCM_LINES

	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( ap ) ;

	ae_ap = &ae_state->ap ;
	if( !ap->data ) {
		PRINT( sc1445x_ae_incompatible_audio_profile_error,
								__FUNCTION__ ) ;
		return SC1445x_AE_ERR_INCOMPATIBLE_AUDIO_PROFILE ;
	}

	nbytes = ap->data_nbytes ;
	p = MALLOC( nbytes ) ;
	if( unlikely( !p ) ) {
		PRINT( sc1445x_ae_nomem_error, __FUNCTION__, "audio profile" ) ;
		return SC1445x_AE_ERR_NO_MEM ;
	}
	if( ae_ap->data )
		FREE( ae_ap->data ) ;
	ae_ap->data = p ;
	ae_ap->data_nbytes = nbytes ;

	sc1445x_internal_copy_audio_profile( ae_ap, ap ) ;

	return SC1445x_AE_OK ;
}



/***********/
/* GENERAL */
/***********/

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
/* set the operating mode to SC1445x_AE_MODE_RAW_PCM and do the proper init */
void sc1445x_private_start_raw_pcm_mode( sc1445x_ae_state* ae_state,
					short change_mode, unsigned short rate )
{
	sc1445x_ae_raw_pcm_rate r ;

	sc1445x_internal_classd_autopower( ae_state,
						ae_state->iface_mode, 1, 0 ) ;
	switch( rate ) {
		case 8000:
			r = 0 ;
			break ;

		case 16000:
			r = 1 ;
			break ;

		case 32000:
			r = 2 ;
			break ;

		default:
			r = rate / 8000 / 2 ;
			PRINT( PRINT_LEVEL "Rate %dHz is not supported;"
					" will use %dHz instead\n",
					rate, 8000 * (1 << r) ) ;
	}

	INIT_CONST_FIELD( unsigned short*,
			&ae_state->raw_pcm, buffer, (unsigned short*)0x18280 ) ;
	INIT_CONST_FIELD( unsigned short*,
			&ae_state->raw_pcm, block_status,
						(unsigned short*)0x1aa80 ) ;
	INIT_CONST_FIELD( sc1445x_ae_raw_pcm_rate,
			&ae_state->raw_pcm, rate, r ) ;
	INIT_CONST_FIELD( unsigned short,
			&ae_state->raw_pcm, block_count, 16 ) ;
	memset( ae_state->raw_pcm.block_status, 0,
			sizeof( ae_state->raw_pcm.block_status[0] )
			* ae_state->raw_pcm.block_count ) ;
	/* clear bank used by the DSP */
	memset( (void*)0x18000, 0, 0x280 ) ;
	ae_state->raw_pcm.next_block_index = 0 ;
	ae_state->raw_pcm.partial = 0 ;
	if( change_mode )
		sc1445x_internal_install_isr( generic_dsp1_isr_raw_pcm,
					generic_dsp2_isr_raw_pcm, ae_state ) ;

	/* set up CODEC */
	sc1445x_internal_init_codec( ae_state, r ) ;

	/* do_sc1445x_ae_set_vspk_volume() checks for this */
	ae_state->mode = SC1445x_AE_MODE_RAW_PCM ;
	/* adjust vspk volume */
	do_sc1445x_ae_set_vspk_volume( ae_state ) ;

	/* tell the DSPs to start playing raw PCM */
	sc1445x_internal_send_dsp_cmd( 0x0004, 1 << r ) ;
}
#endif

/* set the operating mode of the audio engine */
/* the mode can change only when there are no active audio channels */
short sc1445x_ae_set_mode( sc1445x_ae_state* ae_state,
					sc1445x_ae_mode new_mode, void* arg )
{
	unsigned short i, a ;
	int change_mode ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( new_mode, SC1445x_AE_MODE_INVALID ) ;

	/* make sure there are no active audio channels */
	for( i = 0 ;  i < ae_state->audio_channels_count ;  ++i ) {
		if( ae_state->audio_channels[i].is_active  &&
				!ae_state->audio_channels[i].do_release) {
			if( &ae_state->audio_channels[i] ==
						ae_state->raw_pcm_channel )
				continue ;

		 	PRINT( sc1445x_ae_cannot_change_mode_error,
							__FUNCTION__ ) ;
			return SC1445x_AE_ERR_CANNOT_CHANGE_MODE ;
		}
	}

	change_mode =  ae_state->mode != new_mode ;
	if( change_mode ) {
#if defined( CHECK_DSP_IRQ_OVERFLOW ) || defined( SMOOTH_IFACE_TRANSITION ) \
					|| defined( SC1445x_AE_ATA_SUPPORT )
		free_irq( DSP1_INT, (void*)ae_state ) ;
#endif
		free_irq( DSP2_INT, (void*)ae_state ) ;
	}

	if( SC1445x_AE_MODE_NORMAL == new_mode ) {
		if( change_mode ) {
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
			sc1445x_internal_classd_autopower( ae_state,
				ae_state->iface_mode,
#  if defined( USE_CLASSD_AUTOPOWER_IN_HANDS_FREE )
				sc1445x_private_have_active_channels(
								ae_state ),
#  else
				1,
#  endif
				0 ) ;
#endif
			sc1445x_internal_install_isr( generic_dsp1_isr_normal,
					generic_dsp2_isr_normal, ae_state ) ;
			sc1445x_internal_init_codec( ae_state,
					SC1445x_AE_RAW_PCM_RATE_8000 ) ;
		}
	}
	else if( SC1445x_AE_MODE_LOOPBACK == new_mode ) {
		sc1445x_internal_install_isr( generic_dsp1_isr_loopback,
					generic_dsp2_isr_loopback, ae_state ) ;
	}
	else if( SC1445x_AE_MODE_AUTOPLAY == new_mode ) {
		/* arg must be an array of pointers, two for each audio */
		/* channel, start/end of the area where the stored audio */
		/* will be read from; the end pointer points to one byte past */
		/* the last sample */
		unsigned int* p ;
		CHECK_POINTER_ARG( arg ) ;

		p = (unsigned int*)arg ;
		for( i = a = 0 ;  i < SC1445x_AE_MAX_AUDIO_CHANNELS ;  ++i ) {
			unsigned int val ;
			
			get_user( val, &p[a] ) ;
			autoplay_startp[i] = (unsigned short*)val ;
			++a ;
			get_user( val, &p[a] ) ;
			autoplay_endp[i] = (unsigned short*)val ;
			++a ;
			autoplay_readp[i] = autoplay_startp[i] ;
		}
		sc1445x_internal_init_codec( ae_state,
					SC1445x_AE_RAW_PCM_RATE_8000 ) ;
		sc1445x_internal_install_isr( generic_dsp1_isr_autoplay,
					generic_dsp2_isr_autoplay, ae_state ) ;
	}
	else if( SC1445x_AE_MODE_RAW_PCM == new_mode ) {
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		struct snd_sc1445x* alsa_chip =
				(struct snd_sc1445x*)ae_state->private_data ;
		snd_pcm_runtime_t* runtime =
					alsa_chip->playback_substream->runtime ;

		sc1445x_private_start_raw_pcm_mode( ae_state, change_mode,
							runtime->rate ) ;
#else
		PRINT( PRINT_LEVEL "%s: SC1445x_AE_MODE_RAW_PCM not implemented"
				" in this build!\n", __FUNCTION__ ) ;
		return SC1445x_AE_MODE_INVALID ;
#endif  /* SC1445x_AE_PCM_LINES_SUPPORT */
	}

	ae_state->mode = new_mode ;

	return SC1445x_AE_OK ;
}

/* set the sidetone volume */
short sc1445x_ae_set_sidetone_volume( sc1445x_ae_state* ae_state,
					unsigned short new_vol )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( new_vol, 0x8000 ) ;

	sc1445x_internal_send_dsp_cmd( 0x000a, new_vol ) ;
	ae_state->sidetone_volume = new_vol ;

	return SC1445x_AE_OK ;
}

/* select to send output (playback) via the hardware CODEC */
static
short sc1445x_internal_enable_output_to_hw_codec(
				const sc1445x_ae_state* ae_state, short enable )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	DPRINT( "%s: enable=%d\n", __FUNCTION__, enable ) ;
	sc1445x_internal_send_dsp_cmd( 0x0011, ( enable != 0 ) ) ;

	return SC1445x_AE_OK ;
}

/* select to send output (playback) to ext speaker via class D amp */
static
short sc1445x_internal_enable_output_to_classd_amp(
				const sc1445x_ae_state* ae_state, short enable )
{
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	short classd_enabled ; 
#endif

	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	DPRINT( "%s: enable=%d\n", __FUNCTION__, enable ) ;
	if( enable ) {
#if defined( CONFIG_SC14450 )
		CLK_CODEC_REG |= 0x1000 ;
#elif defined( CONFIG_SC14452 )
		clk_codec3_val[0] |= SW_DACLASSDCDC_EN ;
		clk_codec3_val[1] |= SW_DACLASSDCDC_EN ;
		clk_codec3_val[2] |= SW_DACLASSDCDC_EN ;
		CLK_CODEC3_REG |= SW_DACLASSDCDC_EN ;

		CLK_CLASSD1_REG |= SW_CLASSD_EN;
		CLASSD_CTRL_REG = ae_state->classd_ctrl_val[1] ;
#endif
#if defined( SMOOTH_IFACE_TRANSITION )
		((sc1445x_ae_state*)ae_state)->do_disable_classd = 0 ;
	} else if( !ae_state->do_disable_classd /* &&
					!ae_state->do_stop_pcm_channel*/ ) {
		if( CLK_CLASSD1_REG & SW_CLASSD_EN ) {  /* CLASSD is ON... */
			/* turn off sidetone */
			DPRINT( "%s: Disable ClassD\n", __FUNCTION__);
			sc1445x_internal_send_dsp_cmd( 0x0018, 0 ) ;

			((sc1445x_ae_state*)ae_state)->do_disable_classd = 1 ;
		}
#endif
	} else {
#if defined( CONFIG_SC14450 )
		CLK_CODEC_REG &= ~0x1000 ;
#elif defined( CONFIG_SC14452 )
		clk_codec3_val[0] &= ~SW_DACLASSDCDC_EN ;
		clk_codec3_val[1] &= ~SW_DACLASSDCDC_EN ;
		clk_codec3_val[2] &= ~SW_DACLASSDCDC_EN ;
		CLK_CODEC3_REG &= ~SW_DACLASSDCDC_EN ;

		CLASSD_CTRL_REG = ae_state->classd_ctrl_val[0] ;
		CLK_CLASSD1_REG &= ~SW_CLASSD_EN;
#endif
	}
	sc1445x_internal_send_dsp_cmd( 0x0012, ( enable != 0 ) ) ;

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
#  if defined( CONFIG_SC14450 )
	classd_enabled = !!( CLK_CODEC_REG & 0x1000 ) ;
#  elif defined( CONFIG_SC14452 )
	classd_enabled = !!( CLK_CLASSD1_REG & SW_CLASSD_EN ) ;
#  endif

#  if !defined( SMOOTH_IFACE_TRANSITION )
	sc1445x_ae_set_rx_tx_filters( (sc1445x_ae_state*)ae_state,
					classd_enabled, currently_wideband(),
					currently_using_headset( ae_state ) ) ;
#  endif
#endif

	return SC1445x_AE_OK ;
}

/* helper function for setting the current vmic gain */
static void do_sc1445x_ae_set_vmic_gain( const sc1445x_ae_state* ae_state )
{
	unsigned short lvl ;
	const short is_wideband = currently_wideband() ;
	const sc1445x_ae_vmic_gains* vmg ;
	const sc1445x_ae_ap_custom_param* cp ;
	unsigned short i, n, val ;
	sc1445x_ae_iface_mode im = get_audio_profile_iface_mode( ae_state ) ;
#if 0  /* don't use Tx Peak Limiter for now */
	volatile unsigned short* Tx_PL_max = (void*)0x1056a ;
	const unsigned short Tx_PL_max_values[4] = {
			0x7fff, 0x1b80, 0xe00, 0x0712
	} ;
	volatile unsigned short* Tx_PL_min = (void*)0x1056c ;
	const unsigned short Tx_PL_min_values[4] = {
			0x5000, 0x1180, 0x08e0, 0x047c
	} ;
	volatile unsigned short* Tx_PL_clip = (void*)0x10572 ;
	const unsigned short Tx_PL_clip_values[4] = {
			0x7fff, 0x1b80, 0xe00, 0x0712
	} ;
#endif
	short use_bt_settings =
		( SC1445x_AE_IFACE_MODE_BT_GSM == ae_state->iface_mode ) ;

	lvl = ae_state->vmics[im].gain_level ;
	if( is_wideband ) {
		vmg = &ae_state->ap.vmic_lvl_wide[im] ;
	} else {
		vmg = &ae_state->ap.vmic_lvl_narrow[im] ;
	}

	if( vmg->use_analog ) {
#if defined( DO_MIC_DC_CANCELLATION )
		unsigned short cor1 ;
#endif
#if defined( DO_MIC_DC_CANCELLATION ) && defined( CONFIG_SC14452 )
		/* make the MIC DC cancellation code below atomic */
		unsigned long flags ;
#endif

		SetBits( CODEC_MIC_REG, MIC_GAIN, vmg->analog_levels[lvl] ) ;

#if defined( DO_MIC_DC_CANCELLATION ) && defined( CONFIG_SC14452 )
		local_irq_save( flags ) ;
#endif

#if defined( CONFIG_SC14452 )
		CLK_CODEC1_REG |= SW_ADCDC_EN ;
#endif 

#if defined( DO_MIC_DC_CANCELLATION )
		SetBits( CODEC_MIC_REG, MIC_MUTE, 1 ) ;

		while( GetBits( CODEC_TEST_CTRL_REG, COR_STAT ) )
			;

#  if defined( CONFIG_SC14452 )
		local_irq_restore( flags ) ;
#  endif

		cor1 = CODEC_OFFSET1_REG ;
		CODEC_OFFSET2_REG = cor1 ;
		SetBits( CODEC_MIC_REG, MIC_MUTE, 0 ) ;
#endif
	}

	if( vmg->use_shift_paec_out ) {
		short i = use_bt_settings ?  vmg->shift_paec_out_levels_BT[lvl]
					: vmg->shift_paec_out_levels[lvl] ;

		sc1445x_internal_send_dsp_cmd( 0x001A, i ) ;
	}

	if( vmg->use_paec_tx_att )
		sc1445x_internal_send_dsp_cmd( 0x001B,
			use_bt_settings ?  vmg->paec_tx_att_levels_BT[lvl]
					:  vmg->paec_tx_att_levels[lvl] ) ;

	if( vmg->use_attlimit ) {
		volatile unsigned short* attlimit = (void*)0x10770 ;

		*attlimit = vmg->attlimit[lvl] ;
	}

	if( vmg->use_supmin ) {
		volatile unsigned short* supmin = (void*)0x12b86 ;

		*supmin = vmg->supmin[lvl] ;
	}

	if( vmg->use_noiseattlimit ) {
		volatile unsigned short* noiseattlimit = (void*)0x10784 ;

		*noiseattlimit = vmg->noiseattlimit[lvl] ;
	}

	/* custom parameters */
	cp = vmg->cp.params ;
	n = vmg->cp.count ;
	for( i = 0 ;  i < n ;  ++i, ++cp ) {
		if( cp->use ) {
			const unsigned dsp_abs_addr_low = 0x10000 ;

			val = cp->values[lvl] ;
			if( cp->addr < dsp_abs_addr_low )
				sc1445x_internal_send_dsp_cmd( cp->addr, val ) ;
			else
				*(unsigned short*)cp->addr = val ;
		}
	}


	/* special handling for G729 */
	for( i = 0 ;  i < ae_state->audio_channels_count ;  ++i ) {
		if( SC1445x_AE_CODEC_G729 ==
				ae_state->audio_channels[i].enc_codec.type ) {
			sc1445x_internal_send_dsp_cmd( 0x0043, 0x0010 ) ;
			sc1445x_internal_send_dsp_cmd( 0x0044, 0x0000 ) ;
			break ;
		}
	}
	if( ae_state->audio_channels_count == i ) {
		/* no G729 encoder */
		sc1445x_internal_send_dsp_cmd( 0x0044, 0x0003 ) ;
		sc1445x_internal_send_dsp_cmd( 0x0043, 0x0000 ) ;
	}
}

static void sc1445x_internal_vmic_gain_wq( void* arg )
{
	sc1445x_ae_state* ae_state = arg ;

	DPRINT( "%s: entering\n", __FUNCTION__ ) ;
	do_sc1445x_ae_set_vmic_gain( ae_state ) ;
}

static void sc1445x_internal_release_channel_wq( void* arg )
{
	sc1445x_ae_state* ae_state = arg ;
	unsigned short ch ;

#if defined( USE_PLC ) && !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	/* reactivate PLC */
	DPRINT( PRINT_LEVEL "Turn on PLC\n" ) ;
	sc1445x_internal_send_dsp_cmd( 0x0005, 1 ) ;
#endif

	DPRINT( "%s: entering\n", __FUNCTION__ ) ;
	for( ch = 0 ;  ch < ae_state->audio_channels_count ;  ++ch ) {
		PRINT( "ch=%d do_release=%d\n", ch, ae_state->audio_channels[ch].do_release ) ;
		if( 2 == ae_state->audio_channels[ch].do_release ) {
			ae_state->audio_channels[ch].do_release = 0 ;
			ae_state->audio_channels[ch].is_active = 0 ;

			wake_up_interruptible_all(
				&ae_state->audio_channels[ch].releaseq ) ;
		}
	}
}

/* helper function for setting the current vspk volume */
static void do_sc1445x_ae_set_vspk_tone_volume(
		const sc1445x_ae_state* ae_state )
{
	unsigned short vol_lvl, pcm_lvl, dtmf_lvl, val1 ;
	const short is_wideband = currently_wideband() ;
	const sc1445x_ae_vspk_volumes* vsv ;
	sc1445x_ae_iface_mode im = get_audio_profile_iface_mode( ae_state ) ;
	unsigned short i ;
	short is_tonegen_ringing = 0 ;
	short is_playing_dtmf = 0 ;
	short have_active_voice_channels = 0 ;
	short on_hook_ringing, DAP ;

	for( i = 0 ;  i < SC1445x_AE_TONEGEN_MOD_COUNT ;  ++i ) {
		if( ae_state->tonegen[i].is_ringing )
			is_tonegen_ringing = 1 ;
		if( ae_state->tonegen[i].is_playing_dtmf )
			is_playing_dtmf = 1 ;
	}

	for( i = 0 ;  i < ae_state->audio_channels_count ;  ++i ) {
		if( ae_state->audio_channels[i].is_active &&
			 	!ae_state->audio_channels[i].is_raw_pcm ) {
			if( ae_state->audio_channels[i].do_release )
				continue ;  /* ignore it */
			have_active_voice_channels = 1 ;
			break ;
		}
	}

	DAP = !!ae_state->activating_dual_audio_path
				||  !!ae_state->dual_audio_path_is_active ;
	DPRINT( "%s: DAP is %s\n", __FUNCTION__, DAP ?  "ON" :  "OFF" ) ;

	on_hook_ringing = ( !have_active_voice_channels
				&&
			!!( vspk_from_iface_mode( ae_state->iface_mode,
								0 ) & 4 )
				&&
			( SC1445x_AE_MODE_RAW_PCM == ae_state->mode
				  ||  is_tonegen_ringing
				  ||  ae_state->raw_pcm_channel != NULL ) )
		||  DAP ;
	DPRINT( "%s: on_hook_ringing=%d\n", __FUNCTION__, on_hook_ringing ) ;

	if( !DAP ) {
		vol_lvl = ae_state->vspks[im].vol_level ;
		pcm_lvl = ae_state->vspks[im].pcm_level ;
		dtmf_lvl = ae_state->vspks[im].dtmf_level ;
	} else {
		vol_lvl = ae_state->vspks[SC1445x_AE_IFACE_MODE_HANDSET].
								vol_level ;
		pcm_lvl = ae_state->vspks[SC1445x_AE_IFACE_MODE_HANDS_FREE].
								pcm_level ;
		dtmf_lvl = ae_state->vspks[SC1445x_AE_IFACE_MODE_HANDSET].
								dtmf_level ;
	}
	DPRINT( "%s: %d %d vol_lvl=%d pcm_lvl=%d\n", __FUNCTION__,
		is_tonegen_ringing, on_hook_ringing, vol_lvl, pcm_lvl ) ;
	if( is_wideband )
		vsv = &ae_state->ap.vspk_lvl_wide[im] ;
	else
		vsv = &ae_state->ap.vspk_lvl_narrow[im] ;

	if( vsv->use_ext_spk_att ) {
		if( on_hook_ringing ) {
			if( DAP )
				val1 = vsv->ring_playback_vol_levels[pcm_lvl] ;
			else
				val1 = vsv->ring_playback_vol_levels[vol_lvl] ;
		} else
			val1 = vsv->ext_spk_att_levels[vol_lvl] ;

		sc1445x_internal_send_dsp_cmd( 0x0013, val1 ) ;
	}

	if( vsv->use_tone_vol ) {
		unsigned short val2 ;

		if( DAP ) {
			if( on_hook_ringing && !is_tonegen_ringing ) {
				/* PCM ringtone */
				DPRINT( "%s: DAP -- PCM ringtone\n",
								__FUNCTION__ ) ;

				if( is_playing_dtmf )
					val1 = vsv->
					  dtmf_tone_codec_vol_levels[dtmf_lvl] ;
				else
					val1 = vsv->
					  call_tone_codec_vol_levels[vol_lvl] ;

				val2 = 0 ;
			} else if( is_tonegen_ringing ) {
				/* tonegen ringtone */
				DPRINT( "%s: DAP -- tonegen ringtone\n",
								__FUNCTION__ ) ;

#if 0
				val1 = 0 ;
				val2 = vsv->ring_tone_vol_levels[vol_lvl] ;
#else
				if( is_playing_dtmf )
					val1 = vsv-> dtmf_tone_codec_vol_levels
								[dtmf_lvl] ;
				else
					val1 = vsv-> call_tone_codec_vol_levels
								[vol_lvl] ;
				val2 = 0 ;
#endif
			} else if( is_playing_dtmf ) {
				val1 = vsv->
					dtmf_tone_codec_vol_levels[dtmf_lvl] ;
				val2 = vsv->
					dtmf_tone_classd_vol_levels[dtmf_lvl] ;
			} else {
				val1 = vsv->
					call_tone_codec_vol_levels[vol_lvl] ;
				val2 = vsv->
					call_tone_classd_vol_levels[vol_lvl] ;
			}
		} else {
			if( is_tonegen_ringing ) {
				val1 = 0 ;  /* whatever */
				val2 = vsv->ring_tone_vol_levels[vol_lvl] ;
			} else if( is_playing_dtmf ) {
				val1 = vsv->
					dtmf_tone_codec_vol_levels[dtmf_lvl] ;
				val2 = vsv->
					dtmf_tone_classd_vol_levels[dtmf_lvl] ;
			} else {
				val1 = vsv->
					call_tone_codec_vol_levels[vol_lvl] ;
				val2 = vsv->
					call_tone_classd_vol_levels[vol_lvl] ;
			}
		}

#if 0
		if( on_hook_ringing )
			val2 = 0x7fff ;  /* "bypass" this block */
#endif

		sc1445x_internal_send_dsp_cmd( 0x0009, val1 ) ;
		sc1445x_internal_send_dsp_cmd( 0x0007, val2 ) ;
	}
}

/* helper function for setting the current vspk volume */
static void do_sc1445x_ae_set_vspk_volume( const sc1445x_ae_state* ae_state )
{
	unsigned short vol_lvl, pcm_lvl ;
	unsigned short val ;
	short i ;
	const short is_wideband = currently_wideband() ;
	const sc1445x_ae_vspk_volumes* vsv ;
	const sc1445x_ae_ap_custom_param* cp ;
	unsigned short n ;
	sc1445x_ae_iface_mode im = get_audio_profile_iface_mode( ae_state ) ;
	volatile unsigned short* Rx_PL_max = (void*)0x105b6 ;
	const unsigned short Rx_PL_max_values[4] = {
			0x7fff, 0x1b80, 0xe00, 0x0712
	} ;
	volatile unsigned short* Rx_PL_min = (void*)0x105b8 ;
	const unsigned short Rx_PL_min_values[4] = {
			0x5000, 0x1180, 0x08e0, 0x047c
	} ;
	volatile unsigned short* Rx_PL_clip = (void*)0x105be ;
	const unsigned short Rx_PL_clip_values[4] = {
			0x7fff, 0x1b80, 0xe00, 0x0712
	} ;
	short is_tonegen_ringing = 0 ;
	short have_active_voice_channels = 0 ;
	short vspk, on_hook_ringing ;
	short HFdap, DAP ;

	for( i = 0 ;  i < SC1445x_AE_TONEGEN_MOD_COUNT ;  ++i ) {
		if( ae_state->tonegen[i].is_ringing )
			is_tonegen_ringing = 1 ;
	}

	for( i = 0 ;  i < ae_state->audio_channels_count ;  ++i ) {
		if( ae_state->audio_channels[i].is_active &&
			 	!ae_state->audio_channels[i].is_raw_pcm ) {
			if( ae_state->audio_channels[i].do_release )
				continue ;  /* ignore it */
			have_active_voice_channels = 1 ;
			break ;
		}
	}

	DAP = !!ae_state->activating_dual_audio_path
		||  !!ae_state->dual_audio_path_is_active ;
	DPRINT( "%s: DAP is %s\n", __FUNCTION__, DAP ?  "ON" :  "OFF" ) ;

	vspk = vspk_from_iface_mode( ae_state->iface_mode, 0 ) ;
	on_hook_ringing = ( !have_active_voice_channels
			&&
			!!( vspk & 4 )
			&&
			( SC1445x_AE_MODE_RAW_PCM == ae_state->mode
			  ||  is_tonegen_ringing
			  ||  ae_state->raw_pcm_channel != NULL ) )
		||  DAP ;
	DPRINT( "%s: on_hook_ringing=%d\n", __FUNCTION__, on_hook_ringing ) ;

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	HFdap = ( SC1445x_AE_IFACE_MODE_HANDS_FREE == ae_state->iface_mode )
		/*&& have_active_voice_channels*/
		&& !DAP
		&& ( ae_state->raw_pcm_channel != NULL ) ;
	if( !( vspk & 4 ) )
		val = 0 ;  /* CLASSD is not used */
	else {
		if( HFdap )
			val = 1 ;  /* HFdap */
		else if( DAP && is_tonegen_ringing )
			val = 2 ;  /* DAP with tonegen ringtone */
		else
			val = 0 ;  /* DAP with PCM ringtone */
	}
	sc1445x_internal_send_dsp_cmd( 0x0035, val ) ;
	DPRINT( "%s: HFdap is %s\n", __FUNCTION__, HFdap ?  "ON" :  "OFF" ) ;
#else
	HFdap = 0 ;
#endif

	if( !DAP ) {
		vol_lvl = ae_state->vspks[im].vol_level ;
		pcm_lvl = ae_state->vspks[im].pcm_level ;
	} else {
		vol_lvl = ae_state->vspks[SC1445x_AE_IFACE_MODE_HANDSET].
								vol_level ;
		pcm_lvl = ae_state->vspks[SC1445x_AE_IFACE_MODE_HANDS_FREE].
								pcm_level ;
	}

	if( is_wideband )
		vsv = &ae_state->ap.vspk_lvl_wide[im] ;
	else
		vsv = &ae_state->ap.vspk_lvl_narrow[im] ;

	if( vsv->use_analog )
		SetBits( CODEC_LSR_REG, LSRATT, vsv->analog_levels[vol_lvl] ) ;

	if( vsv->use_rx_path_att )
		sc1445x_internal_send_dsp_cmd( 0x0008,
					vsv->rx_path_att_levels[vol_lvl] ) ;

	if( vsv->use_rx_path_shift )
		sc1445x_internal_send_dsp_cmd( 0x0020,
					vsv->rx_path_shift_levels[vol_lvl] ) ;

	if( vsv->use_shift_ext_spk )
		sc1445x_internal_send_dsp_cmd( 0x0017,
			( ( on_hook_ringing  &&
			    ae_state->raw_pcm_channel != NULL )
		  		||  DAP )
					?  0  /* "bypass" */
					:  vsv->shift_ext_spk_levels[vol_lvl] );

	if( vsv->use_sidetone_att )
		sc1445x_internal_send_dsp_cmd( 0x000a,
					vsv->sidetone_att_levels[vol_lvl] ) ;

	if( vsv->use_loopgain_rxpl )
		sc1445x_internal_send_dsp_cmd( 0x0033,
					vsv->loopgain_rxpl_levels[vol_lvl] ) ;

	if( vsv->use_ng_threshold )
		sc1445x_internal_send_dsp_cmd( 0x0036,
					vsv->ng_threshold_levels[vol_lvl] ) ;

	/* set PeakLimit parameters */
	if( vsv->use_shift_ext_spk ) {
		if( ( on_hook_ringing  &&  ae_state->raw_pcm_channel != NULL )
				||  DAP )
			i = 0 ;  /* "bypass" */
		else
			i = vsv->shift_ext_spk_levels[vol_lvl] ;
	} else if( vsv->use_rx_path_shift )
		i = vsv->rx_path_shift_levels[vol_lvl] ;
	else
		i = -1 ;

	if( i >= 0 ) {
		if( i > 3 )
			i = 3 ;
		*Rx_PL_max = Rx_PL_max_values[i] ;
		*Rx_PL_min = Rx_PL_min_values[i] ;
		*Rx_PL_clip = Rx_PL_clip_values[i] ;
	}

	if( vsv->use_classd_vout ) {
		val = ae_state->classd_ctrl_val[0] ;
		val &= ~0xc ;
		val |= (vsv->classd_vout[vol_lvl] << 2) ;
		INIT_CONST_FIELD( unsigned short, ae_state,
						classd_ctrl_val[0], val ) ;

		val = ae_state->classd_ctrl_val[1] ;
		val &= ~0xc ;
		val |= (vsv->classd_vout[vol_lvl] << 2) ;
		INIT_CONST_FIELD( unsigned short, ae_state,
						classd_ctrl_val[1], val ) ;

		/* update current value of CLASSD_CTRL_REG */
		if( CLASSD_CTRL_REG & 1 )
			CLASSD_CTRL_REG = ae_state->classd_ctrl_val[0] ;
		else
			CLASSD_CTRL_REG = ae_state->classd_ctrl_val[1] ;
	}

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT ) \
			&& !defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD_CNX )
#if 1
	/* FIXME: this is a quick fix, to decrease high volume on BT headset */
	if( SC1445x_AE_IFACE_MODE_BT_HEADSET == ae_state->iface_mode ) {
		sc1445x_internal_send_dsp_cmd( 0x000c, 0x0800 ) ;
		sc1445x_internal_send_dsp_cmd( 0x000d, 0x0800 ) ;
		sc1445x_internal_send_dsp_cmd( 0x000e, 0x0800 ) ;
		sc1445x_internal_send_dsp_cmd( 0x0021, 0x0800 ) ;
	} else {
		sc1445x_internal_send_dsp_cmd( 0x000c, 0x7777 ) ;
		sc1445x_internal_send_dsp_cmd( 0x000d, 0x7777 ) ;
		sc1445x_internal_send_dsp_cmd( 0x000e, 0x7777 ) ;
		sc1445x_internal_send_dsp_cmd( 0x0021, 0x7777 ) ;
	}
#endif
#endif

	if( vsv->use_pcm_gain_pre ) {
		val = ( on_hook_ringing	| HFdap )
					?  vsv->pcm_gain_pre_ringtone[pcm_lvl]
					:  vsv->pcm_gain_pre[pcm_lvl] ;
		/* use the same value for all channels: it is used only if
		 * it is a "raw pcm channel" and the API supports only
		 * one such channel
		 */
		for( i = 0 ;  i < ae_state->audio_channels_count ;  ++i ) {
			val &= ~3 ;
			val |= i & 3 ;
			sc1445x_internal_send_dsp_cmd( 0x002a, val ) ;
		}
	}
	if( vsv->use_pcm_gain_shift ) {
		val = ( on_hook_ringing | HFdap )
					?  vsv->pcm_gain_shift_ringtone[pcm_lvl]
					:  vsv->pcm_gain_shift[pcm_lvl] ;
		/* use the same value for all channels: it is used only if
		 * it is a "raw pcm channel" and we the API supports only
		 * one such channel
		 */
		for( i = 0 ;  i < ae_state->audio_channels_count ;  ++i ) {
			val &= ~0x0f00 ;
			val |= ( i & 0xf ) << 8 ;
			sc1445x_internal_send_dsp_cmd( 0x002c, val ) ;
		}
	}
#if 0
	if( vsv->use_pcm_gain_post )
		sc1445x_internal_send_dsp_cmd( 0x002b,
						vsv->pcm_gain_post[pcm_lvl] ) ;
#endif

	do_sc1445x_ae_set_vspk_tone_volume( ae_state ) ;

	/* custom parameters */
	cp = vsv->cp.params ;
	n = vsv->cp.count ;
	for( i = 0 ;  i < n ;  ++i, ++cp ) {
		if( cp->use ) {
			const unsigned dsp_abs_addr_low = 0x10000 ;

			val = cp->values[vol_lvl] ;
			if( cp->addr < dsp_abs_addr_low )
				sc1445x_internal_send_dsp_cmd( cp->addr, val ) ;
			else
				*(unsigned short*)cp->addr = val ;
		}
	}
}

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
static void sc1445x_ae_set_lep(sc1445x_ae_state* ae_state,
		unsigned short pcm_dev_id, const sc1445x_ae_iface_mode m)
{
	switch(m)
	{
		case SC1445x_AE_IFACE_MODE_HANDSET:
			ae_state->pcm_dev[pcm_dev_id].lep =
					SC1445x_AE_PCM_LOCAL_EP_HANDSET;
			break;

		case SC1445x_AE_IFACE_MODE_HANDS_FREE:
			ae_state->pcm_dev[pcm_dev_id].lep =
					SC1445x_AE_PCM_LOCAL_EP_HANDS_FREE;
			break;

		case SC1445x_AE_IFACE_MODE_OPEN_LISTENING:
			ae_state->pcm_dev[pcm_dev_id].lep =
					SC1445x_AE_PCM_LOCAL_EP_OPEN_LISTENING;
			break;

		default:
			break;
	}
}
#endif


static void sc1445x_ae_internal_set_vmic( const sc1445x_ae_state* ae_state,
				const sc1445x_ae_iface_mode new_mode,
				short new_vmic )
{
	struct __CODEC_MIC_REG* codec_mic_reg =
				(struct __CODEC_MIC_REG*)&CODEC_MIC_REG ;

	/* this is an internal function, we don't check args */

	/* turn off mic */
#if defined( CONFIG_SC14452 )
	CLK_CODEC1_REG &= ~SW_ADCDC_EN ;
#endif
	codec_mic_reg->BITFLD_MIC_PD = 1 ;
	wmb() ;

	if( 1 == new_vmic ) {
		if( !ae_state->vmics[new_mode].is_muted ) {
			/* turn on the handset mic */
#if defined( CONFIG_SC14450 )
			codec_mic_reg->BITFLD_MICH_ON = 0 ;
#elif defined( CONFIG_SC14452 )
			codec_mic_reg->BITFLD_MICHN_ON = 0 ;
			codec_mic_reg->BITFLD_MICHP_ON = 0 ;
#endif
			codec_mic_reg->BITFLD_MIC_MODE = 0 ;
			wmb() ;
			codec_mic_reg->BITFLD_MIC_PD = 0 ;
		}
	} else {
		if( !ae_state->vmics[new_mode].is_muted ) {
			if( new_vmic != 8  &&  new_vmic != 0 ) {
				/* turn on the external/headset mic */
#if defined( CONFIG_SC14450 )
				codec_mic_reg->BITFLD_MIC_MODE = 2 ;
				codec_mic_reg->BITFLD_MICH_ON = 1 ;
#elif defined( CONFIG_SC14452 )
				//gflamis
				codec_mic_reg->BITFLD_MIC_MODE = 0 ;
				codec_mic_reg->BITFLD_MICHN_ON = 1 ;
				codec_mic_reg->BITFLD_MICHP_ON = 1 ;
#endif
				wmb() ;
				codec_mic_reg->BITFLD_MIC_PD = 0 ;
			} else {
				/* turn off mic; done this already! */
			}
		}
	}

#if 0 && defined( CONFIG_SC14452 )
	/* this delay is necessary to avoid a 'click'
	 * noise on the far end
	 */
	msleep( 100 ) ;
	CLK_CODEC1_REG |= SW_ADCDC_EN ;
#endif
}

static void sc1445x_ae_internal_set_vspk( const sc1445x_ae_state* ae_state,
				const sc1445x_ae_iface_mode new_mode,
				short new_vspk )
{
	struct __CODEC_LSR_REG* codec_lsr_reg =
				(struct __CODEC_LSR_REG*)&CODEC_LSR_REG ;

	AUDIOENGINE_TRACE_FUNC() ;

	if( 8 == new_vspk ) {
		/* turn off speaker(s), we'll use PCM */
		codec_lsr_reg->BITFLD_LSRN_PD = 1 ;
		codec_lsr_reg->BITFLD_LSRP_PD = 1 ;
		/* turn off the external spk */
		sc1445x_internal_enable_output_to_classd_amp( ae_state, 0 ) ;
		sc1445x_internal_enable_output_to_hw_codec( ae_state, 0 ) ;
	} else {
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		short classd_dep_cond ;
#endif

		if( new_vspk & 1 ) {
			/* turn on the handset spk */
			codec_lsr_reg->BITFLD_LSREN_SE = 0 ;
			codec_lsr_reg->BITFLD_LSRN_MODE = 2 ;
			codec_lsr_reg->BITFLD_LSRP_MODE = 2 ;
			codec_lsr_reg->BITFLD_LSRN_PD = 0 ;
			codec_lsr_reg->BITFLD_LSRP_PD = 0 ;
#if defined( SMOOTH_IFACE_TRANSITION )
			((sc1445x_ae_state*)ae_state)->do_disable_codec = 0 ;
#endif
		} else if( new_vspk & 2 ) {
			/* turn on the headset spk */
			codec_lsr_reg->BITFLD_LSREN_SE = 1 ;
			codec_lsr_reg->BITFLD_LSRN_MODE = 3 ;
			codec_lsr_reg->BITFLD_LSRP_MODE = 3 ;
			codec_lsr_reg->BITFLD_LSRN_PD = 1 ;
			codec_lsr_reg->BITFLD_LSRP_PD = 0 ;
#if defined( SMOOTH_IFACE_TRANSITION )
			((sc1445x_ae_state*)ae_state)->do_disable_codec = 0 ;
#endif
		} else {
			/* turn off LSR */
#if defined( SMOOTH_IFACE_TRANSITION )
			if( 0 == codec_lsr_reg->BITFLD_LSRN_PD  &&
					0 == codec_lsr_reg->BITFLD_LSRP_PD ) {
				/* LSR is ON... */
				/* turn off sidetone */
				DPRINT( "%s: Disable Codec\n", __FUNCTION__) ;
				sc1445x_internal_send_dsp_cmd( 0x0018, 0 ) ;

				((sc1445x_ae_state*)ae_state)->
							do_disable_codec = 1 ;
			}
#else
			codec_lsr_reg->BITFLD_LSRN_PD = 1 ;
			codec_lsr_reg->BITFLD_LSRP_PD = 1 ;
#endif
		}
		sc1445x_internal_enable_output_to_hw_codec(
				ae_state, !!( new_vspk & 3 ) ) ;

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		if( !ae_state->dual_audio_path_is_active  &&
				!ae_state->activating_dual_audio_path ) {
			classd_dep_cond = sc1445x_private_have_active_channels(
								ae_state ) ;
		} else {
			unsigned short i ;

			/* enable CLASSD only if there is a ringtone playing */
			classd_dep_cond = 0 ;
			for( i = 0 ;  i < SC1445x_AE_TONEGEN_MOD_COUNT ;  ++i ){
				if( ae_state->tonegen[i].is_ringing ) {
					classd_dep_cond = 1 ;
					break ;
				}
			}

			classd_dep_cond |=  ae_state->raw_pcm_channel != NULL ;
		}

		sc1445x_internal_classd_autopower( ae_state,
				new_mode, classd_dep_cond,
				SC1445x_AE_MODE_RAW_PCM == ae_state->mode ) ;
#endif
	}
}

/* set the interface mode of the audio engine */
/* this will have an effect on which virtual mic and spk(s) are being used */
short sc1445x_ae_set_iface_mode( sc1445x_ae_state* ae_state,
					const sc1445x_ae_iface_mode new_mode )
{
	short res = SC1445x_AE_OK ;
	short cur_vmic, new_vmic, cur_vspk, new_vspk ;
	sc1445x_ae_iface_mode cur_mode ;


	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( new_mode, SC1445x_AE_IFACE_MODE_INVALID ) ;

	if( ae_state->dual_audio_path_is_active ) {
		PRINT( "%s: not changing iface mode while dual audio path"
					" is activated!\n", __FUNCTION__ ) ;
		return res ;
	}

	cur_mode = ae_state->iface_mode ;

	DPRINT( "%s: new_mode=%d\n", __FUNCTION__, new_mode ) ;
	if( unlikely( new_mode == cur_mode ) ) {
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		sc1445x_internal_classd_autopower( ae_state,
				new_mode,
				sc1445x_private_have_active_channels(
								ae_state ),
				SC1445x_AE_MODE_RAW_PCM == ae_state->mode ) ;
#endif

		return SC1445x_AE_OK ;
	}
	#ifdef VC_enable
	else //if(*((unsigned short*)(VC_proc))==0x1)
	{//psavvop-VC
		if (sc1445x_private_have_active_channels(ae_state)==1){
			if ( ( (cur_mode==SC1445x_AE_IFACE_MODE_OPEN_LISTENING) || (cur_mode==SC1445x_AE_IFACE_MODE_HANDS_FREE) ) && (new_mode==SC1445x_AE_IFACE_MODE_HANDSET) ){
				memset( (void*)(bInitOK), 0, 1 ) ;		//vocal_clarity_data.bInitOK = 0;
				memcpy( (void*)(DSP1_preset_fxp), (void*)preset_ear_fxp, 270);
				memset( (void*)(bFirst), 1, 1 ) ;		// vocal_clarity_data.bFirst = 1;

				}
			if ( (cur_mode==SC1445x_AE_IFACE_MODE_HANDSET) && ((new_mode==SC1445x_AE_IFACE_MODE_HANDS_FREE) || (new_mode==SC1445x_AE_IFACE_MODE_OPEN_LISTENING)) ){
				memset( (void*)(bInitOK), 0, 1 ) ;		//vocal_clarity_data.bInitOK = 0;
				memcpy( (void*)(DSP1_preset_fxp), (void*)preset_spk_fxp, 270);
				memset( (void*)(bFirst), 1, 1 ) ;		// vocal_clarity_data.bFirst = 1;
			}
		}
	}
	#endif

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	if( new_mode != SC1445x_AE_IFACE_MODE_BT_GSM
				&& ae_state->op_mode.bits.pcm_x_codec_classd ) {
		/* not a possible combination */
		PRINT( sc1445x_ae_iface_mode_not_available_error,
					__FUNCTION__, ae_state->iface_mode ) ;
		return SC1445x_AE_ERR_IFACE_MODE_NOT_AVAILABLE ;
	}
#endif

#if defined( SC1445x_AE_NATIVE_DECT_SUPPORT ) && \
				!defined( SC1445x_AE_PCM_LINES_SUPPORT )
	if( SC1445x_AE_IFACE_MODE_WIRELESS == new_mode ) {
		DPRINT( "%s: entering DECT headset mode\n", __FUNCTION__ ) ;
		if( SC1445x_AE_LINE_TYPE_NATIVE_DECT_WIDE ==
						ae_state->dect_headset_type ) {
			DPRINT( "%s: wideband headset, switching to 16K\n",
					__FUNCTION__ ) ;
			sc1445x_internal_init_codec( ae_state,
					SC1445x_AE_RAW_PCM_RATE_16000 ) ;
		} else {
			/* DSP takes care of everything when the DECT headset
			 * is narrowband
			 */
			DPRINT( "%s: narrowband headset, leaving it up to "
					"the DSP\n", __FUNCTION__ ) ;
		}
	} else if( SC1445x_AE_IFACE_MODE_WIRELESS == cur_mode ) {
		sc1445x_ae_raw_pcm_rate rate ;

		DPRINT( "%s: leaving DECT headset mode\n", __FUNCTION__ ) ;
		if( sc1445x_internal_have_wide_channels( ae_state ) ) {
		DPRINT( "%s: switching to 16K\n", __FUNCTION__ ) ;
			rate = SC1445x_AE_RAW_PCM_RATE_16000 ;
		} else {
			DPRINT( "%s: switching to 8K\n", __FUNCTION__ ) ;
			rate = SC1445x_AE_RAW_PCM_RATE_8000 ;
		}

		sc1445x_internal_init_codec( ae_state, rate ) ;
	}
#endif

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	if( SC1445x_AE_IFACE_MODE_BT_GSM == new_mode  ||
	 		SC1445x_AE_IFACE_MODE_BT_HEADSET == new_mode ) {
		if( SC1445x_AE_IFACE_MODE_BT_GSM == new_mode ) {
#  if 1
			DPRINT( "%s: entering BT mode, switching to 8K\n",
								__FUNCTION__ ) ;
			sc1445x_internal_init_codec( ae_state,
					SC1445x_AE_RAW_PCM_RATE_8000 ) ;
#  else
			DPRINT( "%s: entering BT mode, switching to 16K\n",
								__FUNCTION__ ) ;
			sc1445x_internal_init_codec( ae_state,
					SC1445x_AE_RAW_PCM_RATE_16000 ) ;
#  endif
		} else {
#  if !defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD_CNX )
			// 8KHZ PCM FSC
			SetBits( DSP_MAIN_SYNC1_REG, PCM_SYNC, 0 ) ;
#  endif
		}

		/*
		 * we assume we have can have only 1 PCM device attached
		 * reattach to have the proper settings again
		 */
		if( ae_state->pcm_dev[0].attached )
		{
			sc1445x_ae_set_lep(ae_state, 0, new_mode);
#  if defined( SC1445x_AE_BT )
			sc1445x_ae_internal_reattach_to_pcm( ae_state, 0 ) ;
#  endif
		}
	}
#endif

	cur_vmic = vmic_from_iface_mode( cur_mode,
				ae_state->using_external_codec_mic ) ;
	new_vmic = vmic_from_iface_mode( new_mode,
				ae_state->using_external_codec_mic ) ;
	cur_vspk = vspk_from_iface_mode( cur_mode,
				ae_state->using_external_codec_spk ) ;
	new_vspk = vspk_from_iface_mode( new_mode,
				ae_state->using_external_codec_spk ) ;

#if defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD )
	if( SC1445x_AE_IFACE_MODE_HEADSET == new_mode
			||
		( SC1445x_AE_IFACE_MODE_OPEN_LISTENING == new_mode
		  		&&
		 	( SC1445x_AE_IFACE_MODE_HEADSET ==
					ae_state->old_iface_mode
				||
			SC1445x_AE_IFACE_MODE_HEADSET == cur_mode ) ) ) {
		DPRINT( "Setting P0_SET_DATA_REG \r\n" ) ;
		P0_SET_DATA_REG = (1<<2) ;
	} else {
		DPRINT( "ReSetting P0_RESET_DATA_REG \r\n" ) ;
		P0_RESET_DATA_REG = (1<<2) ;
	}
#elif defined( CONFIG_L_V3_BOARD_CNX )
#  if 0
	/* TODO */
	if( SC1445x_AE_IFACE_MODE_HEADSET == new_mode ) {
		P2_SET_DATA_REG = (1<<0) ;
	} else {
		P2_RESET_DATA_REG = (1<<0) ;
	}
#  endif
#endif

	if( cur_vmic != new_vmic ) {
#if !defined( SMOOTH_IFACE_TRANSITION )
		sc1445x_ae_internal_set_vmic( ae_state, new_mode,
								new_vmic ) ;
#else
		ae_state->do_set_vmic = 1 ;
#endif
	}

	if( cur_vspk != new_vspk )
		sc1445x_ae_internal_set_vspk( ae_state, new_mode, new_vspk ) ;

	/* turn PCM on/off */
	if( SC1445x_AE_IFACE_MODE_WIRELESS == new_mode  ||
			SC1445x_AE_IFACE_MODE_BT_GSM == new_mode  ||
			SC1445x_AE_IFACE_MODE_BT_HEADSET == new_mode  ||
			ae_state->using_external_codec_mic  ||
			ae_state->using_external_codec_spk ) {
#if defined( CONFIG_SC14450 )
		CLK_CODEC_REG |= 0x4000 ;
#elif defined( CONFIG_SC14452 )
		clk_spu1_val |= SW_PCMCDC_EN ;
		CLK_SPU1_REG = clk_spu1_val ;
#endif
	} else {
#if defined( CONFIG_SC14450 )
		CLK_CODEC_REG &= ~0x4000 ;
#elif defined( CONFIG_SC14452 )
#  ifndef SC1445x_AE_L_V2_V3_V4_V5_BOARD_CNX
		clk_spu1_val &= ~SW_PCMCDC_EN ;
		CLK_SPU1_REG = clk_spu1_val ;
#  endif
#endif
	}

#if !defined( SMOOTH_IFACE_TRANSITION )
	/* adjust filters */
	sc1445x_ae_set_rx_tx_filters( ae_state,
				SC1445x_AE_IFACE_MODE_HANDS_FREE == new_mode,
						currently_wideband(),
				SC1445x_AE_IFACE_MODE_HEADSET == new_mode ) ;
#endif

	ae_state->iface_mode = new_mode ;

#if 1
	sc1445x_internal_do_pcm_muting( ae_state ) ;
	sc1445x_internal_do_ramio_muting( ae_state ) ;
#endif

#if defined( SMOOTH_IFACE_TRANSITION )
	if( cur_vmic != new_vmic )
		ae_state->do_set_vmic = 1 ;
#else
	/* adjust vmic gain */
	do_sc1445x_ae_set_vmic_gain( ae_state ) ;
#endif

#if defined( SMOOTH_IFACE_TRANSITION )
	if( !ae_state->do_disable_codec && !ae_state->do_disable_classd )
	/* FALL THROUGH */
#endif
	/* adjust vspk volume */
	do_sc1445x_ae_set_vspk_volume( ae_state ) ;

#if defined( SMOOTH_IFACE_TRANSITION )
	ae_state->do_reset_paec = 1 ;
#else
	if( ae_state->AEC_enabled ) {
		/* reset (P)AEC */
		sc1445x_ae_set_aec_off( ae_state ) ;
		sc1445x_ae_set_aec_on( ae_state ) ;
	}
#endif

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	if( new_mode != SC1445x_AE_IFACE_MODE_BT_GSM ) {
		if( new_mode != SC1445x_AE_IFACE_MODE_WIRELESS  &&
			 	new_mode != SC1445x_AE_IFACE_MODE_BT_HEADSET ) {
			ae_state->op_mode.bits.voip_x_codec_classd = 1 ;
			ae_state->op_mode.bits.voip_x_ramio_pcm = 0 ;
		} else {
			ae_state->op_mode.bits.voip_x_codec_classd = 0 ;
			ae_state->op_mode.bits.voip_x_ramio_pcm = 1 ;
		}
	} else {
		ae_state->op_mode.bits.voip_x_codec_classd = 0 ;
		ae_state->op_mode.bits.voip_x_ramio_pcm = 0 ;
	}
	sc1445x_internal_enforce_phone_op_mode( ae_state ) ;
#endif

	return res ;
}

/* set the volume of the current virtual speaker */
short sc1445x_ae_set_vspk_volume( sc1445x_ae_state* ae_state,
							unsigned short vol )
{
	sc1445x_ae_iface_mode im ;

	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( vol, ae_state->ap.vspk_lvl_count ) ;

	im = get_audio_profile_iface_mode( ae_state ) ;
	if( ae_state->dual_audio_path_is_active ) {
		/*
		 * im is SC1445x_AE_IFACE_MODE_OPEN_LISTENING
		 * also set vol_level for SC1445x_AE_IFACE_MODE_HANDSET
		 */
		ae_state->vspks[SC1445x_AE_IFACE_MODE_HANDSET].vol_level = vol ;
	}
	ae_state->vspks[im].vol_level = vol ;

	do_sc1445x_ae_set_vspk_volume( ae_state ) ;

	return SC1445x_AE_OK ;
}

/* set the volume of a virtual speaker */
short sc1445x_ae_set_vspk_volume_ex( sc1445x_ae_state* ae_state,
			const sc1445x_ae_iface_mode m, unsigned short vol )
{
	short enforce ;
	sc1445x_ae_iface_mode im ;

	NOT_AVAILABLE_WITH_PCM_LINES

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( m, SC1445x_AE_IFACE_MODE_INVALID ) ;
	CHECK_ARG_CEIL( vol, ae_state->ap.vspk_lvl_count ) ;

	ae_state->vspks[m].vol_level = vol ;

	/*
	 * if m is the current iface_mode, then
	 * make the new volume level effective immediately
	 */
	im = get_audio_profile_iface_mode( ae_state ) ;
	enforce =  im == m ;

	if( ae_state->dual_audio_path_is_active  &&
			 		SC1445x_AE_IFACE_MODE_HANDSET == m ) {
		/*
		 * also set vol_level for SC1445x_AE_IFACE_MODE_OPEN_LISTENING
		 */
		ae_state->vspks[SC1445x_AE_IFACE_MODE_OPEN_LISTENING].
							vol_level = vol ;
		enforce = 1 ;
	} else if( SC1445x_AE_IFACE_MODE_OPEN_LISTENING == im  &&
					SC1445x_AE_IFACE_MODE_HANDS_FREE == m ) {
		/*
		 * also set vol_level for SC1445x_AE_IFACE_MODE_OPEN_LISTENING
		 */
		ae_state->vspks[SC1445x_AE_IFACE_MODE_OPEN_LISTENING].
							vol_level = vol ;
		enforce = 1 ;
	}

	if( enforce )
		do_sc1445x_ae_set_vspk_volume( ae_state ) ;

	return SC1445x_AE_OK ;
}

/* get the volume range of a virtual speaker */
short sc1445x_ae_get_vspk_volume_ex( const sc1445x_ae_state* ae_state,
		const sc1445x_ae_iface_mode m, unsigned short* vol_min,
		unsigned short* vol_curr, unsigned short* vol_max )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( m, SC1445x_AE_IFACE_MODE_INVALID ) ;
	CHECK_POINTER_ARG( vol_min ) ;
	CHECK_POINTER_ARG( vol_curr ) ;
	CHECK_POINTER_ARG( vol_max ) ;

	*vol_min = 0 ;
	*vol_curr = ae_state->vspks[m].vol_level ;
	*vol_max = ae_state->ap.vspk_lvl_count - 1 ;

	return SC1445x_AE_OK ;
}

/* set the volume of the current virtual speaker */
short sc1445x_ae_set_vspk_dtmf_level( sc1445x_ae_state* ae_state,
						unsigned short lvl )
{
	sc1445x_ae_iface_mode im ;

	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( lvl, ae_state->ap.vspk_lvl_count ) ;

	im = get_audio_profile_iface_mode( ae_state ) ;
	ae_state->vspks[im].dtmf_level = lvl ;

	do_sc1445x_ae_set_vspk_tone_volume( ae_state ) ;

	return SC1445x_AE_OK ;
}

/* set the DTMF level of a virtual speaker */
short sc1445x_ae_set_vspk_dtmf_level_ex( sc1445x_ae_state* ae_state,
			const sc1445x_ae_iface_mode m, unsigned short lvl )
{
	short enforce ;

	NOT_AVAILABLE_WITH_PCM_LINES

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( m, SC1445x_AE_IFACE_MODE_INVALID ) ;
	CHECK_ARG_CEIL( lvl, ae_state->ap.vspk_lvl_count ) ;

	ae_state->vspks[m].dtmf_level = lvl ;

	/*
	 * if m is the current iface_mode, then
	 * make the new DTMF level effective immediately
	 */
	enforce =  get_audio_profile_iface_mode( ae_state ) == m ;

	if( ae_state->dual_audio_path_is_active  &&
			 		SC1445x_AE_IFACE_MODE_HANDSET == m ) {
		/*
		 * also set dtmf_level for SC1445x_AE_IFACE_MODE_OPEN_LISTENING
		 */
		ae_state->vspks[SC1445x_AE_IFACE_MODE_OPEN_LISTENING].
							dtmf_level = lvl ;
		enforce = 1 ;
	}

	if( enforce )
		do_sc1445x_ae_set_vspk_tone_volume( ae_state ) ;

	return SC1445x_AE_OK ;
}

/* get the DTMF level range of a virtual speaker */
short sc1445x_ae_get_vspk_dtmf_level_ex( const sc1445x_ae_state* ae_state,
		const sc1445x_ae_iface_mode m, unsigned short* lvl_min,
		unsigned short* lvl_curr, unsigned short* lvl_max )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( m, SC1445x_AE_IFACE_MODE_INVALID ) ;
	CHECK_POINTER_ARG( lvl_min ) ;
	CHECK_POINTER_ARG( lvl_curr ) ;
	CHECK_POINTER_ARG( lvl_max ) ;

	*lvl_min = 0 ;
	*lvl_curr = ae_state->vspks[m].dtmf_level ;
	*lvl_max = ae_state->ap.vspk_lvl_count - 1 ;

	return SC1445x_AE_OK ;
}

/* set the PCM volume of the current virtual speaker */
short sc1445x_ae_set_vspk_pcm_level( sc1445x_ae_state* ae_state,
						unsigned short lvl )
{
	sc1445x_ae_iface_mode im ;

	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( lvl, ae_state->ap.vspk_lvl_count ) ;

	im = get_audio_profile_iface_mode( ae_state ) ;
	if( ae_state->dual_audio_path_is_active ) {
		/*
		 * im is SC1445x_AE_IFACE_MODE_OPEN_LISTENING
		 * also set pcm_level for SC1445x_AE_IFACE_MODE_HANDSFREE
		 */
		ae_state->vspks[SC1445x_AE_IFACE_MODE_HANDS_FREE].pcm_level = lvl ;
	}
	ae_state->vspks[im].pcm_level = lvl ;

	do_sc1445x_ae_set_vspk_volume( ae_state ) ;

	return SC1445x_AE_OK ;
}

/* set the PCM volume of a virtual speaker */
short sc1445x_ae_set_vspk_pcm_lvl_ex( sc1445x_ae_state* ae_state,
			const sc1445x_ae_iface_mode m, unsigned short lvl )
{
	short enforce ;
	sc1445x_ae_iface_mode im ;

	NOT_AVAILABLE_WITH_PCM_LINES

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( m, SC1445x_AE_IFACE_MODE_INVALID ) ;
	CHECK_ARG_CEIL( lvl, ae_state->ap.vspk_lvl_count ) ;

	ae_state->vspks[m].pcm_level = lvl ;

	/*
	 * if m is the current iface_mode, then
	 * make the new PCM volume level effective immediately
	 */
	im = get_audio_profile_iface_mode( ae_state ) ;
	enforce =  im == m ;

	if( ae_state->dual_audio_path_is_active  &&
		 		SC1445x_AE_IFACE_MODE_HANDS_FREE == m ) {
		/*
		 * also set vol_level for SC1445x_AE_IFACE_MODE_OPEN_LISTENING
		 */
		ae_state->vspks[SC1445x_AE_IFACE_MODE_OPEN_LISTENING].
							pcm_level = lvl ;
		enforce = 1 ;
	} else if( SC1445x_AE_IFACE_MODE_OPEN_LISTENING == im  &&
				SC1445x_AE_IFACE_MODE_HANDS_FREE == m ) {
		/*
		 * also set vol_level for SC1445x_AE_IFACE_MODE_OPEN_LISTENING
		 */
		ae_state->vspks[SC1445x_AE_IFACE_MODE_OPEN_LISTENING].
							pcm_level = lvl ;
		enforce = 1 ;
	}

	if( enforce )
		do_sc1445x_ae_set_vspk_volume( ae_state ) ;

	return SC1445x_AE_OK ;
}

/* get the PCM volume range of a virtual speaker */
short sc1445x_ae_get_vspk_pcm_lvl_ex( const sc1445x_ae_state* ae_state,
		const sc1445x_ae_iface_mode m, unsigned short* lvl_min,
		unsigned short* lvl_curr, unsigned short* lvl_max )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( m, SC1445x_AE_IFACE_MODE_INVALID ) ;
	CHECK_POINTER_ARG( lvl_min ) ;
	CHECK_POINTER_ARG( lvl_curr ) ;
	CHECK_POINTER_ARG( lvl_max ) ;

	*lvl_min = 0 ;
	*lvl_curr = ae_state->vspks[m].pcm_level ;
	*lvl_max = ae_state->ap.vspk_lvl_count - 1 ;

	return SC1445x_AE_OK ;
}

/* set the gain of the current virtual microphone */
short sc1445x_ae_set_vmic_gain( sc1445x_ae_state* ae_state,
							unsigned short gain )
{
	sc1445x_ae_iface_mode im ;

	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( gain, ae_state->ap.vmic_lvl_count ) ;

	im = get_audio_profile_iface_mode( ae_state ) ;
	ae_state->vmics[im].gain_level = gain ;

	do_sc1445x_ae_set_vmic_gain( ae_state ) ;

	return SC1445x_AE_OK ;
}

/* set the gain of a virtual microphone */
short sc1445x_ae_set_vmic_gain_ex( sc1445x_ae_state* ae_state,
			const sc1445x_ae_iface_mode m, unsigned short gain )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	CHECK_ARG_CEIL( m, SC1445x_AE_IFACE_MODE_INVALID ) ;
	CHECK_ARG_CEIL( gain, ae_state->ap.vmic_lvl_count ) ;

	ae_state->vmics[m].gain_level = gain ;

	/*
	 * if m is the current iface_mode, then
	 * make the new gain level effective immediately
	 */
	if( get_audio_profile_iface_mode( ae_state ) == m )
		do_sc1445x_ae_set_vmic_gain( ae_state ) ;

	return SC1445x_AE_OK ;
}

/* get the gain range of a virtual microphone */
short sc1445x_ae_get_vmic_gain_ex( const sc1445x_ae_state* ae_state,
		const sc1445x_ae_iface_mode m, unsigned short* gain_min,
		unsigned short* gain_curr, unsigned short* gain_max )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( m, SC1445x_AE_IFACE_MODE_INVALID ) ;
	CHECK_POINTER_ARG( gain_min ) ;
	CHECK_POINTER_ARG( gain_curr ) ;
	CHECK_POINTER_ARG( gain_max ) ;

	*gain_min = 0 ;
	*gain_curr = ae_state->vmics[m].gain_level ;
	*gain_max = ae_state->ap.vmic_lvl_count - 1 ;

	return SC1445x_AE_OK ;
}

/* mute the current virtual microphone */
short sc1445x_ae_mute_vmic( sc1445x_ae_state* ae_state )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	return sc1445x_ae_mute_mic( ae_state ) ;
}

/* unmute the current virtual microphone */
short sc1445x_ae_unmute_vmic( sc1445x_ae_state* ae_state )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	return sc1445x_ae_unmute_mic( ae_state ) ;
}

/* get the number of vspk volume levels */
short sc1445x_ae_get_vspk_vol_level_count( const sc1445x_ae_state* ae_state,
	                                                unsigned short* count )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( count ) ;

	*count = ae_state->ap.vspk_lvl_count ;

	return SC1445x_AE_OK ;
}

/* get the number of vmic gain levels */
short sc1445x_ae_get_vmic_gain_level_count( const sc1445x_ae_state* ae_state,
	                                                unsigned short* count )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( count ) ;

	*count = ae_state->ap.vmic_lvl_count ;

	return SC1445x_AE_OK ;
}



/******************/
/* AUDIO CHANNELS */
/******************/

/* zero-out buffers related to ch, before activating it */
static void sc1445x_internal_init_channel_buffers( unsigned short ch,
								short is_wide )
{
	unsigned short* const dsp2_to_dsp1_buf[SC1445x_AE_MAX_AUDIO_CHANNELS] ={
			(unsigned short*)0x18f20,
			(unsigned short*)0x19ee0,
			(unsigned short*)0x1aea0,
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
			(unsigned short*)0x1c120
#endif
	} ;
	unsigned short* const dsp1_to_dsp2_buf[SC1445x_AE_MAX_AUDIO_CHANNELS] ={
			(unsigned short*)0x10000,
			(unsigned short*)0x10140,
			(unsigned short*)0x10280,
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
			(unsigned short*)0x103c0
#endif
	} ;
	unsigned short* dsp1_bufs_ptr1 = (void*)0x131fc ;
	unsigned short* dsp1_bufs_base1 =
			(void*)( 0x10000 + ( (*dsp1_bufs_ptr1) << 1 ) ) ;
	unsigned short* dsp1_bufs_ptr2 = (void*)0x131fe ;
	unsigned short* dsp1_bufs_base2 =
			(void*)( 0x10000 + ( (*dsp1_bufs_ptr2) << 1 ) ) ;

	*sc1445x_ae_playback_ch_status[ch] = 0 ;
	*sc1445x_ae_capture_ch_header[ch] = 0 ;
	memset( sc1445x_ae_playback_bufs[ch], 0, 160 ) ;
	memset( sc1445x_ae_capture_bufs[ch], 0, 160 ) ;
	if( !is_wide )
		memset( dsp2_to_dsp1_buf[ch], 0, 160 ) ;
	else
		memset( dsp2_to_dsp1_buf[ch] - 80, 0, 320 ) ;
	memset( dsp1_to_dsp2_buf[ch], 0, 320 ) ;

	memset( (void*)( 0x10000 + (dsp1_bufs_base1[ch] << 1) ), 0, 320 ) ;
	memset( (void*)( 0x10000 + (dsp1_bufs_base2[ch] << 1) ), 0, 320 ) ;

#ifdef VC_enable
	//if (*((unsigned short*)(VC_proc))==0x1){
		//psavvop-VC
		memset( (void*)(bInitOK), 0, 1 );
		//vocal_clarity_data.bInitOK = 0;

		/*
		 * Check wether SPEAKER is enabled, and make the right
		 * initialization of the preset_fxp table on DSP1
		 */
		if (*((unsigned short*)(DSP1_codec_en))==0x0)
			memcpy( (void*)(DSP1_preset_fxp), (void*)preset_spk_fxp, 270);
		else
			memcpy( (void*)(DSP1_preset_fxp), (void*)preset_ear_fxp, 270);
			
		memset( (void*)(bFirst), 1, 1 ) ;
		//vocal_clarity_data.bFirst = 1;
		
		//PRINT( PRINT_LEVEL "Number of calls: %d\n", *((unsigned short*)(0x10636)));//psavvop-debug
	//}
#endif
}

#if defined( SC1445x_AE_USE_FADE_IN_TIMER )
static void fade_in_isr( unsigned long arg ) ;
#endif
/*
 * activate a channel (audio stream) with specific codec
 * (in case of a G711.1 variant, 2 channels are used: ch and dec_ch)
 */
static
short sc1445x_ae_internal_activate_channel( sc1445x_ae_state* ae_state,
					const unsigned short ch,
					const unsigned short dec_ch,
					const sc1445x_ae_codec_type enc_codec,
					const sc1445x_ae_codec_type dec_codec )
{
	short res ;
	sc1445x_ae_channel_state* acs = &ae_state->audio_channels[ch] ;
	sc1445x_ae_channel_state* dec_acs = &ae_state->audio_channels[dec_ch] ;
	short is_wide = sc1445x_internal_is_wideband_codec( enc_codec ) || 
			sc1445x_internal_is_wideband_codec( dec_codec ) ;

#if defined( SC1445x_AE_USE_FADE_IN_TIMER )
	sc1445x_ae_mute_spk( ae_state ) ;
#endif

#if defined( CONFIG_SC14452 )
	if( 3 == ch  &&  ae_state->ilbc_codec_count ) {
		PRINT( sc1445x_ae_ilbc_uses_channel4_error, __FUNCTION__ ) ;
		return SC1445x_AE_ERR_ILBC_USES_CHANNEL4 ;
	}

	if( SC1445x_AE_CODEC_iLBC_30ms == enc_codec  ||
				SC1445x_AE_CODEC_iLBC_20ms == enc_codec  ||
				SC1445x_AE_CODEC_iLBC_30ms == dec_codec  ||
				SC1445x_AE_CODEC_iLBC_20ms == dec_codec ) {
		/* check if the 4th channel is free, or already used by
		 * the iLBC codec
		 */
		if( 3 == ch  ||  ae_state->audio_channels[3].is_active ) {
			PRINT( sc1445x_ae_ilbc_needs_channel4_error,
							__FUNCTION__ ) ;
			return SC1445x_AE_ERR_ILBC_NEEDS_CHANNEL4 ;
		}

		/* keep track of how many channels use iLBC */
		++ae_state->ilbc_codec_count ;
	}
#endif

#if defined( CONFIG_SC1445x_CVQ_METRICS ) \
				|| defined( CONFIG_SC1445x_CVQ_METRICS_MODULE )
	if( cvqm_cbs.start_channel )
		cvqm_cbs.start_channel( ch,
					SC1445X_CVQM_CHANNEL_DIR_PLAYBACK |
					SC1445X_CVQM_CHANNEL_DIR_CAPTURE,
					dec_codec, enc_codec ) ;
#endif

	sc1445x_internal_init_channel_buffers( ch, is_wide ) ;

	/* use this audio channel */
	res = sc1445x_internal_set_codec( ae_state, ch, dec_ch,
						enc_codec, dec_codec ) ;
	if( res != SC1445x_AE_OK ) {
		return res ;
	}

	acs->is_active = 1 ;
	if( dec_acs != acs ) {
		dec_acs->is_active = 1 ;
	}
	DPRINT( "Activating audio channels %d, %d (mode=%d)\n",
						ch, dec_ch, ae_state->mode ) ;
	acs->playback_counter = acs->capture_counter = 0 ;
	acs->playback_sync = acs->capture_sync = 0 ;
	memset( acs->playback_tmp_buf, 0, sizeof( acs->playback_tmp_buf ) ) ;
	memset( acs->capture_tmp_buf, 0, sizeof( acs->capture_tmp_buf ) ) ;

#if defined( CHECK_DSP_IRQ_OVERFLOW )
	PRINT( PRINT_LEVEL "DSP overflow stats @%p and @%p\n",
				dsp_irq_overflows, dsp_irq_overflows + 1 ) ;
	SetWord( DSP1_OVERFLOW_REG, 0xBFFF ) ;
	SetWord( DSP2_OVERFLOW_REG, 0xBFFF ) ;
#endif

#if defined( SC1445x_AE_USE_FADE_IN_TIMER )
	acs->fade_in_timer.data = (unsigned long)acs ;
	acs->fade_in_timer.function = fade_in_isr ;
	acs->fade_in_timer.expires = jiffies + 10 ;
	add_timer( &acs->fade_in_timer ) ;
#endif

	return SC1445x_AE_OK ;
}

/* helper function to check if a given codec is a G711.1 variant */
static inline
short sc1445x_ae_internal_is_g711_1_codec( const sc1445x_ae_codec_type c )
{
	return ( c >= SC1445x_AE_CODEC_G711_1_ALAW_NB64  &&
			c <= SC1445x_AE_CODEC_G711_1_ULAW_WB96 ) ;
}

/* select a free audio channel and start using it */
short sc1445x_ae_activate_free_channel( sc1445x_ae_state* ae_state,
                                        sc1445x_ae_codec_type enc_codec,
                                        sc1445x_ae_codec_type dec_codec,
                                        unsigned short* activated_channel )
{
	unsigned short i, ch, dec_ch, nchannels ;
	unsigned short wide = 0, narrow = 0, nfree = 0 ;
	short res, use_dec_ch = 0 ;
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	sc1445x_ae_iface_mode im ;
#endif

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( activated_channel ) ;
	CHECK_ARG_CEIL( enc_codec, SC1445x_AE_CODEC_INVALID ) ;
	CHECK_ARG_CEIL( dec_codec, SC1445x_AE_CODEC_INVALID ) ;

	nchannels = ae_state->audio_channels_count ;

	for( i = 0, ch = nchannels ;  i < nchannels ;  ++i ) {
		if( ae_state->audio_channels[i].do_release ) {
			/*
			 * this channel is about to be released
			 * wait on it
			 */
			res = wait_event_interruptible_timeout(
				ae_state->audio_channels[i].releaseq,
				( 0 == ae_state->audio_channels[i].do_release ),
									10 ) ;
			if( res < 0 )
				DPRINT( PRINT_LEVEL "%s: "
					"wait_event_interruptible_timeout() "
					"failed\n", __FUNCTION__ ) ;
			else
				DPRINT( "%s: wait_event_interruptible_timeout()"
					" returned %d\n", __FUNCTION__, res ) ;
		}

		if( !ae_state->audio_channels[i].is_active ) {
#if defined( SC1445x_AE_SUPPORT_FAX )
			/* effectively, allocate 2 channels for fax */
			if( i && ae_state->audio_channels[i -1].is_fax )
				continue ;
#endif

			++nfree ;
			if( ch == nchannels )
				ch = i ;
			continue ;
		}

		if( sc1445x_internal_is_wideband_codec(
				ae_state->audio_channels[i].enc_codec.type )
			||
				sc1445x_internal_is_wideband_codec(
					ae_state->audio_channels[i].
							dec_codec.type ) )
			++wide ;
		else
			++narrow ;
	}

	if( sc1445x_ae_internal_is_g711_1_codec( enc_codec )  ||
			sc1445x_ae_internal_is_g711_1_codec( dec_codec ) ) {
		use_dec_ch = 1 ;
		for( i = 0, dec_ch = nchannels ;  i < nchannels ;  ++i ) {
			if( ae_state->audio_channels[i].do_release ) {
				/*
				 * this channel is about to be released
				 * wait on it
				 */
				res = wait_event_interruptible_timeout(
					ae_state->audio_channels[i].releaseq,
					( 0 == ae_state->
					  	audio_channels[i].do_release ),
					10 ) ;
				if( res < 0 )
					DPRINT( PRINT_LEVEL "%s: "
						"wait_event_interruptible_"
						"timeout() "
						"failed\n", __FUNCTION__ ) ;
				else
					DPRINT( "%s: wait_event_interruptible_"
						"timeout()"
						" returned %d\n", __FUNCTION__,
						res ) ;
			}

			if( !ae_state->audio_channels[i].is_active ) {
#if defined( SC1445x_AE_SUPPORT_FAX )
				/* effectively, allocate 2 channels for fax */
				if( i && ae_state->audio_channels[i -1].is_fax )
					continue ;
#endif

				if( dec_ch == nchannels  &&  i != ch ) {
					dec_ch = i ;
					break ;
				}
			}
		}
	} else
		dec_ch = ch ;

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	im = ae_state->iface_mode ;

	if( nchannels == nfree ) {
		if( im != SC1445x_AE_IFACE_MODE_BT_GSM
				&& ae_state->op_mode.bits.pcm_x_codec_classd ) {
			/* not a possible combination */
			PRINT( sc1445x_ae_iface_mode_not_available_error,
					__FUNCTION__, im ) ;
			return SC1445x_AE_ERR_IFACE_MODE_NOT_AVAILABLE ;
		}
	}
#endif

	if( sc1445x_internal_is_wideband_codec( enc_codec ) ||
			sc1445x_internal_is_wideband_codec( dec_codec ) )
		++wide ;
	else
		++narrow ;

#if 0
	/* intermixing or narrowband and wideband should be working now! */
	if( narrow > 0  &&  wide > 0 )
		PRINT( PRINT_LEVEL "Intermixing narrowband and wideband codecs "
				"-- this might not work as intended!\n" ) ;
#endif

	if( ( ch < nchannels )  &&  ( !use_dec_ch  ||
					( dec_ch < nchannels ) ) ) {
		res = sc1445x_ae_internal_activate_channel( ae_state, ch,
						dec_ch, enc_codec, dec_codec ) ;
		if( res != SC1445x_AE_OK ) {
			return res ;
		}

		if( use_dec_ch ) {
			/* encoder */
			ae_state->audio_channels[ch].is_g711_1_enc = 1 ;
			ae_state->audio_channels[ch].is_g711_1_dec = 0 ;
			ae_state->audio_channels[ch].g711_1_buddy_dec_ch =
								dec_ch ;
			ae_state->audio_channels[ch].g711_1_buddy_enc_ch =
								nchannels ;

			/* decoder */
			ae_state->audio_channels[dec_ch].is_g711_1_enc = 0 ;
			ae_state->audio_channels[dec_ch].is_g711_1_dec = 1 ;
			ae_state->audio_channels[dec_ch].g711_1_buddy_dec_ch =
								nchannels ;
			ae_state->audio_channels[dec_ch].g711_1_buddy_enc_ch =
								ch ;
		} else {
			ae_state->audio_channels[ch].is_g711_1_enc = 0 ;
			ae_state->audio_channels[ch].is_g711_1_dec = 0 ;
			ae_state->audio_channels[ch].g711_1_buddy_dec_ch =
								nchannels ;
			ae_state->audio_channels[ch].g711_1_buddy_enc_ch =
								nchannels ;
		}

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		sc1445x_internal_classd_autopower( ae_state, im, 1, 0 ) ;
#endif

		if( nchannels == nfree  &&  ae_state->AEC_enabled ) {
			/* there were no active channels and AEC is enabled */
			/* reset (P)AEC */
			sc1445x_ae_set_aec_off( ae_state ) ;
			sc1445x_ae_set_aec_on( ae_state ) ;
		}

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		if( im != SC1445x_AE_IFACE_MODE_BT_GSM ) {
			if( im != SC1445x_AE_IFACE_MODE_WIRELESS  &&
					im != SC1445x_AE_IFACE_MODE_BT_HEADSET ) {
				ae_state->op_mode.bits.voip_x_codec_classd = 1 ;
				ae_state->op_mode.bits.voip_x_ramio_pcm = 0 ;
			} else {
				ae_state->op_mode.bits.voip_x_codec_classd = 0 ;
				ae_state->op_mode.bits.voip_x_ramio_pcm = 1 ;
			}
		}
		sc1445x_internal_enforce_phone_op_mode( ae_state ) ;
#endif

		*activated_channel = ch ;
		return SC1445x_AE_OK ;
	}

	PRINT( sc1445x_ae_no_free_channel_for_codec_error, enc_codec ) ;
	return SC1445x_AE_ERR_NO_FREE_CHANNEL ;
}



/*******************/
/* TONE GENERATION */
/*******************/

/* Timer1 for tonegen interrupt counter */
unsigned int sc1445x_internal_togen_irq_cnt = 0 ;

#define TONEGEN_VAR( base, var_id ) \
	(unsigned short*)( (base) + ( (var_id) << 1 ) )

/* DSP variables for tonegen */
static volatile unsigned short* TIMER1_PRESET[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A00,
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	(unsigned short*)0x10EA0,
	(unsigned short*)0x11340,
#  if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(unsigned short*)0x1088A
#  endif
#else
	TONEGEN_VAR( 0x1319A, 0x00 ),
	TONEGEN_VAR( 0x130F0, 0x00 ),
#endif
} ;
static volatile unsigned short* TIMER2_PRESET[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A02,
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	(unsigned short*)0x10EA2,
	(unsigned short*)0x11342,
#  if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(unsigned short*)0x1088C
#  endif
#else
	TONEGEN_VAR( 0x1319A, 0x01 ),
	TONEGEN_VAR( 0x130F0, 0x01 ),
#endif
} ;
static volatile unsigned short* ARGSIN1[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A04,
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	(unsigned short*)0x10EA4,
	(unsigned short*)0x11344,
#  if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(unsigned short*)0x1088E
#  endif
#else
	TONEGEN_VAR( 0x1319A, 0x02 ),
	TONEGEN_VAR( 0x130F0, 0x02 ),
#endif
} ;
static volatile unsigned short* ARGCOS1[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A06,
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	(unsigned short*)0x10EA6,
	(unsigned short*)0x11346,
#  if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(unsigned short*)0x10890
#  endif
#else
	TONEGEN_VAR( 0x1319A, 0x03 ),
	TONEGEN_VAR( 0x130F0, 0x03 ),
#endif
} ;
static volatile unsigned short* ARGATT1[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A08,
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	(unsigned short*)0x10EA8,
	(unsigned short*)0x11348,
#  if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(unsigned short*)0x10892
#  endif
#else
	TONEGEN_VAR( 0x1319A, 0x04 ),
	TONEGEN_VAR( 0x130F0, 0x04 ),
#endif
} ;
static volatile unsigned short* ARGSIN2[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A0A,
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	(unsigned short*)0x10EAA,
	(unsigned short*)0x1134A,
#  if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(unsigned short*)0x10894
#  endif
#else
	TONEGEN_VAR( 0x1319A, 0x05 ),
	TONEGEN_VAR( 0x130F0, 0x05 ),
#endif
} ;
static volatile unsigned short* ARGCOS2[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A0C,
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	(unsigned short*)0x10EAC,
	(unsigned short*)0x1134C,
#  if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(unsigned short*)0x10896
#  endif
#else
	TONEGEN_VAR( 0x1319A, 0x06 ),
	TONEGEN_VAR( 0x130F0, 0x06 ),
#endif
} ;
static volatile unsigned short* ARGATT2[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A0E,
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	(unsigned short*)0x10EAE,
	(unsigned short*)0x1134E,
#  if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(unsigned short*)0x10898
#  endif
#else
	TONEGEN_VAR( 0x1319A, 0x07 ),
	TONEGEN_VAR( 0x130F0, 0x07 ),
#endif
} ;
static volatile unsigned short* ARGSIN3[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A10,
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	(unsigned short*)0x10EB0,
	(unsigned short*)0x11350,
#  if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(unsigned short*)0x1089A
#  endif
#else
	TONEGEN_VAR( 0x1319A, 0x08 ),
	TONEGEN_VAR( 0x130F0, 0x08 ),
#endif
} ;
static volatile unsigned short* ARGCOS3[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A12,
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	(unsigned short*)0x10EB2,
	(unsigned short*)0x11352,
#  if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(unsigned short*)0x1089C
#  endif
#else
	TONEGEN_VAR( 0x1319A, 0x09 ),
	TONEGEN_VAR( 0x130F0, 0x09 ),
#endif
} ;
static volatile unsigned short* ARGATT3[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A14,
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	(unsigned short*)0x10EB4,
	(unsigned short*)0x11354,
#  if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	(unsigned short*)0x1089E
#  endif
#else
	TONEGEN_VAR( 0x1319A, 0x0a ),
	TONEGEN_VAR( 0x130F0, 0x0a ),
#endif
} ;
#if defined ( HAVE_TONEGEN4 )
static volatile unsigned short* ARGSIN4[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A16,
	TONEGEN_VAR( 0x1319A, 0x0b ),
	TONEGEN_VAR( 0x130F0, 0x0b ),
} ;
static volatile unsigned short* ARGCOS4[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A18,
	TONEGEN_VAR( 0x1319A, 0x0c ),
	TONEGEN_VAR( 0x130F0, 0x0c ),
} ;
static volatile unsigned short* ARGATT4[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A1A,
	TONEGEN_VAR( 0x1319A, 0x0d ),
	TONEGEN_VAR( 0x130F0, 0x0d ),
} ;
static volatile unsigned short* FADEIN_PRESET[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A52,
	TONEGEN_VAR( 0x1319A, 0x29 ),
	TONEGEN_VAR( 0x130F0, 0x29 ),
} ;
static volatile unsigned short* FADEOUT_PRESET[SC1445x_AE_TONEGEN_MOD_COUNT] = {
	(unsigned short*)0x10A54,
	TONEGEN_VAR( 0x1319A, 0x2a ),
	TONEGEN_VAR( 0x130F0, 0x2a ),
} ;
#endif


#if defined( SC1445x_AE_USE_FAX_TIMER )
static void fax_isr( unsigned long arg )
{
	sc1445x_ae_tonegen_state* tg_state = (sc1445x_ae_tonegen_state*)arg ;
	sc1445x_ae_state* my_ae_state = tg_state->my_ae_state ;
	unsigned short tg_mod = tg_state - my_ae_state->tonegen ;

	if( tg_state->is_CED_detection_off ) {
		sc1445x_internal_send_dsp_cmd( 0x0028, (tg_mod << 8) | 1 ) ;
		PRINT( PRINT_LEVEL "%s: enabled CED detector #%d\n",
							__FUNCTION__, tg_mod ) ;
		tg_state->is_CED_detection_off = 0 ;
	}
	if( tg_state->is_CNG_detection_off ) {
		sc1445x_internal_send_dsp_cmd( 0x0029, (tg_mod << 8) | 1 ) ;
		PRINT( PRINT_LEVEL "%s: enabled CNG detector #%d\n",
							__FUNCTION__, tg_mod ) ;
		tg_state->is_CNG_detection_off = 0 ;
	}
}
#endif

/* "interrupt handler" for tone callback */
static void tone_cb_isr( unsigned long arg )
{
	sc1445x_ae_tonegen_state* tg_state = (sc1445x_ae_tonegen_state*)arg ;
	sc1445x_ae_state* ae_state = tg_state->my_ae_state ;
	unsigned short tg_mod = tg_state - ae_state->tonegen ;

	if( !ae_state->tone_cb_pid )
		return ;

	DPRINT( PRINT_LEVEL "%s: sending signal %d to PID %d\n", __FUNCTION__,
			ae_state->tone_cb_signo, ae_state->tone_cb_pid ) ;

	__put_user( (int)tg_mod, ae_state->tone_cb_cmem ) ;

	kill_proc( ae_state->tone_cb_pid, ae_state->tone_cb_signo, 0 ) ;
}

/* "interrupt handler" for tone generation */
static void generic_tone_isr( unsigned long arg )
{
	sc1445x_ae_tone4_seq_container* tsc = NULL ;
	sc1445x_ae_tonegen_state* tg_state = (sc1445x_ae_tonegen_state*)arg ;
	sc1445x_ae_state* my_ae_state = tg_state->my_ae_state ;
	unsigned short tg_mod = tg_state - my_ae_state->tonegen ;
	short stop_timer = 0 ;
	int off_dur = 0 ;
	const short is_wideband = 
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
				currently_wideband() ;
#else
				is_stream_wideband( my_ae_state, tg_mod ) ;
#endif
	const short shift = is_wideband ?  1 :  0 ;
	const unsigned short* const tone_sin =  is_wideband ?
			sc1445x_ae_tone_sin_16kHz :  sc1445x_ae_tone_sin_8kHz ;
	const unsigned short* const tone_cos =  is_wideband ?
			sc1445x_ae_tone_cos_16kHz :  sc1445x_ae_tone_cos_8kHz ;
	sc1445x_ae_tone_seq_repeat rep_seq = tg_state->repeat_tone_seq ;
	short restarting_seq = 0, keep_tone ;
	unsigned short max_dur ;

	++sc1445x_internal_togen_irq_cnt ;

	if( SC1445x_AE_TONEGEN_IDLE == tg_state->status ) {
		if( tg_state->tone_seq_start ) ;
			sc1445x_internal_free_tone_seq( tg_state ) ;
		return ;
	}

	if( tg_state->play_count )
		keep_tone =  ++tg_state->play_counter < tg_state->play_count ;
	else
		keep_tone = 1 ;
	if( tg_state->tone_seq_curr && !keep_tone )
		tsc = tg_state->tone_seq_curr->next ;
	else
		tsc = tg_state->tone_seq_curr ;
	if( !tsc && tg_state->tone_seq_start ) {
		if( ( SC1445x_AE_TONE_SEQ_REPEAT_ALL == rep_seq )
				&& ( (0 == tg_state->tone_seq_play_count)  ||
		 		     ( ++tg_state->tone_seq_play_counter < 
					tg_state->tone_seq_play_count ) ) ) {
			DPRINT( "%s: restarting sequence\n", __FUNCTION__ ) ;
			/* start sequence over */
			tg_state->tone_seq_curr = tg_state->tone_seq_start ;
			tsc = tg_state->tone_seq_curr ;
			restarting_seq = 1 ;
		} else if( SC1445x_AE_TONE_SEQ_REPEAT_LAST == rep_seq ) {
			/*
			 * we've reached the last part of the sequence
			 * "convert" tone sequence to a simple tone and
			 * let it play on
			 */
			sc1445x_internal_free_tone_seq( tg_state ) ;
#if 0
			tg_state->play_count = tg_state->tone_seq_play_count ;
			tg_state->play_counter = 0 ;
#endif
			return ;
		}
	}

	if( ( !tsc && !keep_tone )  ||
			SC1445x_AE_TONEGEN_STOPPING == tg_state->status ) {
		/* stop playing this tone, set up for idle */
		tg_state->tone1 = SC1445x_AE_TONE_F0 ;
		tg_state->tone2 = SC1445x_AE_TONE_F0 ;
		tg_state->tone3 = SC1445x_AE_TONE_F0 ;
		tg_state->tone4 = SC1445x_AE_TONE_F0 ;
		tg_state->on_duration = 0 ;
		off_dur = tg_state->off_duration ;
		tg_state->off_duration = 1 ;
		stop_timer = 1 ;
	}

	if( tsc ) {
		if( restarting_seq || !keep_tone ) {
			/*
			 * schedule next tone in sequence
			 * let's assume that tone #n is currently playing
			 * we'll now program tone #(n + 1)
			 */
			sc1445x_ae_tone4_sequence_part* tsp = &tsc->data ;
			const unsigned short* tone_sin =  is_wideband ?
				tsp->tone4_params.wide_argsin :
				tsp->tone4_params.narrow_argsin ;
			const unsigned short* tone_cos =  is_wideband ?
				tsp->tone4_params.wide_argcos :
				tsp->tone4_params.narrow_argcos ;

			*ARGSIN1[tg_mod] = tone_sin[0] ;
			*ARGCOS1[tg_mod] = tone_cos[0] ;
			*ARGATT1[tg_mod] = tsp->tone4_params.amplitude[0] ;
			*ARGSIN2[tg_mod] = tone_sin[1] ;
			*ARGCOS2[tg_mod] = tone_cos[1] ;
			*ARGATT2[tg_mod] = tsp->tone4_params.amplitude[1] ;
			*ARGSIN3[tg_mod] = tone_sin[2] ;
			*ARGCOS3[tg_mod] = tone_cos[2] ;
			*ARGATT3[tg_mod] = tsp->tone4_params.amplitude[2] ;
#if defined ( HAVE_TONEGEN4 )
			*ARGSIN4[tg_mod] = tone_sin[3] ;
			*ARGCOS4[tg_mod] = tone_cos[3] ;
			*ARGATT4[tg_mod] = tsp->tone4_params.amplitude[3] ;
#endif

			tg_state->on_duration = tsp->on_duration ;
			tg_state->off_duration = tsp->off_duration ;
			tg_state->play_count = tsp->play_count ;
			tg_state->play_counter = 0 ;

			tg_state->tone_seq_curr = tsc ;
		} else {
			/* let the current tone play again */
		}
	} else if( !keep_tone ) {
		*ARGSIN1[tg_mod] = tone_sin[tg_state->tone1] ;
		*ARGCOS1[tg_mod] = tone_cos[tg_state->tone1] ;
		*ARGATT1[tg_mod] = tg_state->ampl1 ;
		*ARGSIN2[tg_mod] = tone_sin[tg_state->tone2] ;
		*ARGCOS2[tg_mod] = tone_cos[tg_state->tone2] ;
		*ARGATT2[tg_mod] = tg_state->ampl2 ;
		*ARGSIN3[tg_mod] = tone_sin[tg_state->tone3] ;
		*ARGCOS3[tg_mod] = tone_cos[tg_state->tone3] ;
		*ARGATT3[tg_mod] = tg_state->ampl3 ;
#if defined ( HAVE_TONEGEN4 )
		*ARGSIN4[tg_mod] = tone_sin[tg_state->tone4] ;
		*ARGCOS4[tg_mod] = tone_cos[tg_state->tone4] ;
		*ARGATT4[tg_mod] = tg_state->ampl4 ;
#endif
	}

#if defined ( HAVE_TONEGEN4 )
	if( tg_state->on_duration  &&  tg_state->off_duration ) {
		/* do fade-out */
		DPRINT( "FADE OUT\n" ) ;
		*FADEOUT_PRESET[tg_mod] = 6 ;	/* 64 samples */
		*FADEIN_PRESET[tg_mod] = 6 ;		/* 64 samples */
	} else {
		/* don't do fade-out */
		if( !stop_timer )
			*FADEOUT_PRESET[tg_mod] = 0 ;
		*FADEIN_PRESET[tg_mod] = 0 ;
	}
#endif

	max_dur = ( 4096 >> shift ) - 1 ;
	if( 0 == tg_state->off_duration )
		*TIMER2_PRESET[tg_mod] = 0xffff ;
	else if( tg_state->off_duration <= max_dur )
		*TIMER2_PRESET[tg_mod] = tg_state->off_duration << (shift + 3) ;
	else {
		*TIMER2_PRESET[tg_mod] = 0x7fff ;  /* the best we can do */
		PRINT( KERN_DEBUG
			"%s: off_duration=%d is greater than %d; clipping it "
			"to %d!\n", __FUNCTION__, tg_state->off_duration,
						max_dur, max_dur ) ;
	}
	if( tg_state->on_duration <= max_dur )
		*TIMER1_PRESET[tg_mod] = tg_state->on_duration << (shift + 3) ;
	else {
		*TIMER1_PRESET[tg_mod] = 0x7fff ;  /* the best we can do */
		PRINT( KERN_DEBUG
			"%s: on_duration=%d is greater than %d; clipping it "
			"to %d!\n", __FUNCTION__, tg_state->on_duration,
						max_dur, max_dur ) ;
	}

	if( stop_timer ) {
		/* stop the timer */
		tg_state->status = SC1445x_AE_TONEGEN_IDLE ;
		if( tg_state->tone_seq_start ) ;
			sc1445x_internal_free_tone_seq( tg_state ) ;

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		if( tg_state->is_ringing || tg_state->is_playing_dtmf ) {
			/* revert tone volume */
			tg_state->is_ringing = 0 ;
			tg_state->is_playing_dtmf = 0 ;
			/*
			 * the correct volumes will be enforced on the
			 * next tone start
			 */
			//do_sc1445x_ae_set_vspk_volume( my_ae_state ) ;
		}
#endif

#if defined( SC1445x_AE_USE_FAX_TIMER )
		if( unlikely( tg_state->is_CED_detection_off ||
					tg_state->is_CNG_detection_off ) ) {
			/* re-enable detectors in 1 sec */
			tg_state->fax_timer.data =
						(unsigned long)tg_state ;
			tg_state->fax_timer.function = fax_isr ;
			tg_state->fax_timer.expires =
						jiffies + ( 1 * HZ ) ;
			DPRINT( "%s: schedule fax_timer for %lu\n",
				__FUNCTION__, tg_state->fax_timer.expires ) ;
			add_timer( &tg_state->fax_timer ) ;
		}
#endif
		if( my_ae_state->tone_cb_pid ) {
			volatile short* TIMER1 = TIMER1_PRESET[tg_mod] + 17 ;
			volatile short* TIMER2 = TIMER1_PRESET[tg_mod] + 18 ;
			int interval ;
			short t1, t2 ;

#if defined ( HAVE_TONEGEN4 )
			/* account for extra variables */
			TIMER1 += 5 ;
			TIMER2 += 5 ;
#endif
			/* get the remaining time for the ON interval */
			t1 = *TIMER1 ;
			if( t1 < 0 )
				t1 = 0 ;
			/* get the remaining time for the OFF interval */
			t2 = *TIMER2 ;
			if( t2 < 0 )
				t2 = 0 ;
			interval = t1 + t2 ;
			/* convert to msec */
			interval >>= (shift + 3) ;
			tg_state->cb_timer.data = (unsigned long)tg_state ;
			tg_state->cb_timer.function = tone_cb_isr ;
			tg_state->cb_timer.expires = jiffies +
						( interval * HZ / 1000 ) ;
			add_timer( &tg_state->cb_timer ) ;
		}
	} else {
		/*
		 * schedule for a little after the end of tone #n
		 * at that point, tone #(n + 1) will have started playing and
		 * we'll program tone #(n + 2)
		 */
		volatile short* TIMER1 = TIMER1_PRESET[tg_mod] + 17 ;
		volatile short* TIMER2 = TIMER1_PRESET[tg_mod] + 18 ;
		int interval ;
		short t1, t2 ;

#if defined ( HAVE_TONEGEN4 )
		/* account for extra variables */
		TIMER1 += 5 ;
		TIMER2 += 5 ;
#endif
		/* get the remaining time for the currently playing tone */
		t1 = *TIMER1 ;
		if( t1 < 0 )
			t1 = 0 ;
	       	t2 = *TIMER2 ;
		if( t2 < 0 )
			t2 = 0 ;
		interval = t1 + t2 ;
		/* convert to msec */
		interval >>= (shift + 3) ;
		/* give it some air */
		interval += 20 ;

		/* reschedule the timer */
		tg_state->timer.expires = jiffies + ( interval * HZ / 1000 ) ;
		add_timer( &tg_state->timer ) ;
		DPRINT( "%s: interval=%d\n", __FUNCTION__, interval ) ;
	}
}


/* setup a kernel timer to expire after x msec and set its ISR to func */
/* x is rounded down to a multiple of 10 */
static void generic_add_timer( unsigned short x, sc1445x_ae_state* ae_state,
							unsigned short tg_mod )
{
	unsigned interval = x ;
	sc1445x_ae_tonegen_state* tg_state = &ae_state->tonegen[tg_mod] ;

	if( timer_pending( &tg_state->timer ) )
		del_timer( &tg_state->timer ) ;
	tg_state->timer.data = (unsigned long)tg_state ;
	tg_state->timer.function = generic_tone_isr ;
	tg_state->timer.expires = jiffies + ( interval * HZ / 1000 ) ;
	add_timer( &tg_state->timer ) ;
}


static short sc1445x_ae_internal_start_tone4( sc1445x_ae_state* ae_state,
			unsigned short tg_mod,
			sc1445x_ae_tone tone1, unsigned short ampl1,
			sc1445x_ae_tone tone2, unsigned short ampl2,
			sc1445x_ae_tone tone3, unsigned short ampl3,
			sc1445x_ae_tone tone4, unsigned short ampl4,
			unsigned short dur_on, unsigned short dur_off,
			unsigned short play_count )
{
	sc1445x_ae_tonegen_state* tonegen ;
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	unsigned short cmd ;
#endif
	short is_wideband ;
	short shift ;
	const unsigned short* tone_sin ;
	const unsigned short* tone_cos ;
#if defined( SMOOTH_IFACE_TRANSITION )
	long res ;
	volatile unsigned short* dsp1_transition = (void*)0x10538 ;
	volatile unsigned short* dsp1_transition_mode = (void*)0x1053a ;
#  define FADEOUT_DONE	( 0 == *dsp1_transition  ||  *dsp1_transition_mode > 1 )
#endif
	unsigned short max_dur ;

	sc1445x_internal_fast_stop_tone( ae_state, tg_mod ) ;

	is_wideband = 
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
				currently_wideband() ;
#else
				is_stream_wideband( ae_state, tg_mod ) ;
#endif

	DPRINT( "%s entering s=%d, w=%d\n", __FUNCTION__, tg_mod, is_wideband );

	shift = is_wideband ?  1 :  0 ;
	tone_sin = is_wideband ?
			sc1445x_ae_tone_sin_16kHz :  sc1445x_ae_tone_sin_8kHz ;
	tone_cos = is_wideband ?
			sc1445x_ae_tone_cos_16kHz :  sc1445x_ae_tone_cos_8kHz ;

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	if( is_wideband )
		_sc1445x_internal_init_codec( ae_state,
					SC1445x_AE_RAW_PCM_RATE_16000, 0 ) ;
#endif

	tonegen = &ae_state->tonegen[tg_mod] ;

#if 0
	if( tonegen->status != SC1445x_AE_TONEGEN_IDLE ) {
		PRINT( sc1445x_ae_tonegen_not_idle_error, __FUNCTION__ ) ;
		return SC1445x_AE_ERR_TONEGEN_NOT_IDLE ;
	}
#endif

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	sc1445x_internal_classd_autopower( ae_state,
						ae_state->iface_mode, 1, 0 ) ;
#endif

#if defined( DONT_SEND_TONES ) && !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	/* don't send the tone to the other side */
	sc1445x_internal_send_dsp_cmd( 0x000f, 0 ) ;
#endif

#if defined( DONT_HEAR_TONES ) && !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	/* don't send the tone to the speaker */
	sc1445x_internal_send_dsp_cmd( 0x0009, 0 ) ;
#endif

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	/* enable line */
	cmd = ( tg_mod < 3 ) ?  ( 0x0004 + tg_mod ) :  0x0026 ;
	sc1445x_internal_send_dsp_cmd( cmd, 0x0011 ) ;

	/* set stream to "off-hook" state (only for ATA) */
	sc1445x_internal_send_dsp_cmd( 0x0013, tg_mod << 8 | 0x0002 ) ;
#endif

	tonegen->is_custom_tone = 0 ;

	/* initialize the tonegen state */
	tonegen->tone1 = tone1 ;
	tonegen->tone2 = tone2 ;
	tonegen->tone3 = tone3 ;
	tonegen->tone4 = tone4 ;
	tonegen->ampl1 = ampl1 ;
	tonegen->ampl2 = ampl2 ;
	tonegen->ampl3 = ampl3 ;
	tonegen->ampl4 = ampl4 ;
	max_dur = ( 4096 >> shift ) - 1 ;
	if( dur_on > max_dur ) {
		PRINT( KERN_DEBUG
			"%s: on_duration=%d is greater than %d; clipping it "
			"to %d!\n", __FUNCTION__, dur_on, max_dur, max_dur ) ;
		dur_on = max_dur ;
	}
	tonegen->on_duration = dur_on ;
	if( dur_off > max_dur ) {
		PRINT( KERN_DEBUG
			"%s: off_duration=%d is greater than %d; clipping it "
			"to %d!\n", __FUNCTION__, dur_off, max_dur, max_dur ) ;
		dur_off = max_dur ;
	}
	tonegen->off_duration = dur_off ;
	tonegen->play_count = play_count ;
	tonegen->play_counter = 0 ;

#if defined( SMOOTH_IFACE_TRANSITION )
	/* make sure last command has been read */
	while( !sc1445x_internal_can_send_dsp_cmd() )
		;
	/* wait for fadeout, if any, to finish */
	res = wait_event_interruptible_timeout( ae_state->fadeoutq,
							FADEOUT_DONE, 10 ) ;
	if( res < 0 )
		DPRINT( PRINT_LEVEL "%s: wait_event_interruptible_timeout() "
						"failed\n", __FUNCTION__ ) ;
	else
		DPRINT( "%s: wait_event_interruptible_timeout() returned %ld\n",
							__FUNCTION__, res ) ;
#endif

	/* initialize the relevant DSP variables */
	*ARGSIN1[tg_mod] = tone_sin[tone1] ;
	*ARGCOS1[tg_mod] = tone_cos[tone1] ;
	*ARGATT1[tg_mod] = ampl1 ;
	*ARGSIN2[tg_mod] = tone_sin[tone2] ;
	*ARGCOS2[tg_mod] = tone_cos[tone2] ;
	*ARGATT2[tg_mod] = ampl2 ;
	*ARGSIN3[tg_mod] = tone_sin[tone3] ;
	*ARGCOS3[tg_mod] = tone_cos[tone3] ;
	*ARGATT3[tg_mod] = ampl3 ;
#if defined ( HAVE_TONEGEN4 )
	*ARGSIN4[tg_mod] = tone_sin[tone4] ;
	*ARGCOS4[tg_mod] = tone_cos[tone4] ;
	*ARGATT4[tg_mod] = ampl4 ;
#endif

	/* add a timer to either put tonegen to idle or repeat the tone */
	tonegen->status = SC1445x_AE_TONEGEN_PLAYING ;
	if( play_count != 0 ) {
		generic_add_timer( 20, ae_state, tg_mod ) ;
	}

#if defined ( HAVE_TONEGEN4 )
	if( dur_on  &&  dur_off ) {
		/* do fading */
		DPRINT( "FADING\n" ) ;
		*FADEOUT_PRESET[tg_mod] = 6 ;	/* 64 samples */
		*FADEIN_PRESET[tg_mod] = 6 ;		/* 64 samples */
	} else {
		/* don't do fading */
		*FADEOUT_PRESET[tg_mod] = 0 ;
		*FADEIN_PRESET[tg_mod] = 0 ;
	}
#endif

	/* convert times from msecs to 125usec units */
	if( 0 == dur_off )
		*TIMER2_PRESET[tg_mod] = 0xffff ;
	else/* if( dur_off <= max_dur )*/
		*TIMER2_PRESET[tg_mod] = dur_off << ( shift + 3 ) ;
#if 0
	/* clipping has been done already */
	else {
		*TIMER2_PRESET[tg_mod] = 0x7fff ;  /* the best we can do */
		PRINT( PRINT_LEVEL
			"%s: off_duration=%d is greater than %d; clipping it "
			"to %d!\n", __FUNCTION__, dur_off,
						4096 >> shift, 4096 >> shift ) ;
	}
#endif
#if 0
	/* clipping has been done already */
	if( dur_on <= max_dur )
#endif
		*TIMER1_PRESET[tg_mod] = dur_on << ( shift + 3 ) ;
#if 0
	else {
		*TIMER1_PRESET[tg_mod] = 0x7fff ;  /* the best we can do */
		PRINT( PRINT_LEVEL
			"%s: on_duration=%d is greater than %d; clipping it "
			"to %d!\n", __FUNCTION__, dur_on,
						4096 >> shift, 4096 >> shift ) ;
	}
#endif

	return SC1445x_AE_OK ;
}

/*
 * on-off times are given in msec
 * play_count:
 * 	== 0 --> play "for ever"
 * 	 > 0 --> play (play_count) times
 */
short sc1445x_ae_start_tone4( sc1445x_ae_state* ae_state, unsigned short tg_mod,
				sc1445x_ae_tone tone1, unsigned short ampl1,
				sc1445x_ae_tone tone2, unsigned short ampl2,
				sc1445x_ae_tone tone3, unsigned short ampl3,
				sc1445x_ae_tone tone4, unsigned short ampl4,
				unsigned short dur_on, unsigned short dur_off,
				unsigned short play_count )
{
	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_ARG_CEIL( tone1, SC1445x_AE_TONE_INVALID ) ;
	CHECK_ARG_CEIL( ampl1, 0x8000 ) ;
	CHECK_ARG_CEIL( tone2, SC1445x_AE_TONE_INVALID ) ;
	CHECK_ARG_CEIL( ampl2, 0x8000 ) ;
	CHECK_ARG_CEIL( tone3, SC1445x_AE_TONE_INVALID ) ;
	CHECK_ARG_CEIL( ampl3, 0x8000 ) ;
	CHECK_ARG_CEIL( tone4, SC1445x_AE_TONE_INVALID ) ;
	CHECK_ARG_CEIL( ampl4, 0x8000 ) ;

	ae_state->tonegen[tg_mod].is_playing_dtmf = 0 ;
	ae_state->tonegen[tg_mod].is_ringing = 0 ;
	do_sc1445x_ae_set_vspk_volume( ae_state ) ;

	return sc1445x_ae_internal_start_tone4( ae_state, tg_mod, tone1, ampl1,
				tone2, ampl2, tone3, ampl3, tone4, ampl4,
				dur_on, dur_off, play_count ) ;
}

/* start playing a tone */
/* on-off times are given in msec */
short sc1445x_ae_start_tone( sc1445x_ae_state* ae_state, unsigned short tg_mod,
				sc1445x_ae_tone tone1, sc1445x_ae_tone tone2,
				sc1445x_ae_tone tone3, unsigned short dur_on,
				unsigned short dur_off, short repeat )
{
	unsigned short play_count =  repeat ?  0 :  1 ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_ARG_CEIL( tone1, SC1445x_AE_TONE_INVALID ) ;
	CHECK_ARG_CEIL( tone2, SC1445x_AE_TONE_INVALID ) ;
	CHECK_ARG_CEIL( tone3, SC1445x_AE_TONE_INVALID ) ;

	ae_state->tonegen[tg_mod].is_playing_dtmf = 0 ;
	ae_state->tonegen[tg_mod].is_ringing = 0 ;
	do_sc1445x_ae_set_vspk_volume( ae_state ) ;

	return sc1445x_ae_internal_start_tone4( ae_state, tg_mod, tone1,
			TWISTED_DTMF_FIRST_AMPL,
			tone2, 0x7fff, tone3, 0x7fff,
			SC1445x_AE_TONE_F0, 0x7fff, dur_on, dur_off,
			play_count ) ;
}

/*
 * start playing a key tone
 * on-off times are given in msec
 * play_count:
 * 	== 0 --> play "for ever"
 * 	 > 0 --> play (play_count) times
 */
short sc1445x_ae_start_key_tone4( sc1445x_ae_state* ae_state,
				unsigned short tg_mod,
				sc1445x_ae_tone tone1, unsigned short ampl1,
				sc1445x_ae_tone tone2, unsigned short ampl2,
				sc1445x_ae_tone tone3, unsigned short ampl3,
				sc1445x_ae_tone tone4, unsigned short ampl4,
				unsigned short dur_on, unsigned short dur_off,
				unsigned short play_count )
{
	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_ARG_CEIL( tone1, SC1445x_AE_TONE_INVALID ) ;
	CHECK_ARG_CEIL( ampl1, 0x8000 ) ;
	CHECK_ARG_CEIL( tone2, SC1445x_AE_TONE_INVALID ) ;
	CHECK_ARG_CEIL( ampl2, 0x8000 ) ;
	CHECK_ARG_CEIL( tone3, SC1445x_AE_TONE_INVALID ) ;
	CHECK_ARG_CEIL( ampl3, 0x8000 ) ;
	CHECK_ARG_CEIL( tone4, SC1445x_AE_TONE_INVALID ) ;
	CHECK_ARG_CEIL( ampl4, 0x8000 ) ;

	ae_state->tonegen[tg_mod].is_playing_dtmf = 1 ;
	ae_state->tonegen[tg_mod].is_ringing = 0 ;
	do_sc1445x_ae_set_vspk_volume( ae_state ) ;

	return sc1445x_ae_internal_start_tone4( ae_state, tg_mod, tone1, ampl1,
				tone2, ampl2, tone3, ampl3, tone4, ampl4,
				dur_on, dur_off, play_count ) ;
}

/* start playing a key tone */
/* on-off times are given in msec */
short sc1445x_ae_start_key_tone( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, sc1445x_ae_tone tone1,
				sc1445x_ae_tone tone2, sc1445x_ae_tone tone3,
				unsigned short dur_on, unsigned short dur_off,
				short repeat )
{
	unsigned short play_count =  repeat ?  0 :  1 ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_ARG_CEIL( tone1, SC1445x_AE_TONE_INVALID ) ;
	CHECK_ARG_CEIL( tone2, SC1445x_AE_TONE_INVALID ) ;
	CHECK_ARG_CEIL( tone3, SC1445x_AE_TONE_INVALID ) ;

	ae_state->tonegen[tg_mod].is_playing_dtmf = 1 ;
	ae_state->tonegen[tg_mod].is_ringing = 0 ;
	do_sc1445x_ae_set_vspk_volume( ae_state ) ;

	return sc1445x_ae_internal_start_tone4( ae_state, tg_mod, tone1,
			TWISTED_DTMF_FIRST_AMPL,
			tone2, 0x7fff, tone3, 0x7fff,
			SC1445x_AE_TONE_F0, 0x7fff, dur_on, dur_off,
			play_count ) ;
}

/* stop playing tone */
short sc1445x_ae_stop_tone( sc1445x_ae_state* ae_state, unsigned short tg_mod )
{
	sc1445x_ae_tonegen_state* tg_state ;
	short is_wideband ;
	short shift ;
	const unsigned short* tone_sin ;
	const unsigned short* tone_cos ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;

	is_wideband = 
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
				currently_wideband() ;
#else
				is_stream_wideband( ae_state, tg_mod ) ;
#endif

	shift = is_wideband ?  1 :  0 ;
	tone_sin =  is_wideband ?
			sc1445x_ae_tone_sin_16kHz :  sc1445x_ae_tone_sin_8kHz ;
	tone_cos =  is_wideband ?
			sc1445x_ae_tone_cos_16kHz :  sc1445x_ae_tone_cos_8kHz ;

	tg_state = &ae_state->tonegen[tg_mod] ;
	if( SC1445x_AE_TONEGEN_IDLE == tg_state->status ) {
		PRINT( sc1445x_ae_tonegen_is_idle_error, __FUNCTION__ ) ;
		return SC1445x_AE_ERR_TONEGEN_IS_IDLE ;
	}

	if( timer_pending( &tg_state->timer ) )
		del_timer( &tg_state->timer ) ;

	tg_state->play_count = 1 ;
	tg_state->play_counter = 0 ;
	tg_state->status = SC1445x_AE_TONEGEN_STOPPING ;

	/* stop playing this tone, set up for idle */
	sc1445x_internal_send_dsp_cmd( 0x0010, tg_mod ) ;
	tg_state->tone1 = SC1445x_AE_TONE_F0 ;
	tg_state->tone2 = SC1445x_AE_TONE_F0 ;
	tg_state->tone3 = SC1445x_AE_TONE_F0 ;
	tg_state->tone4 = SC1445x_AE_TONE_F0 ;
	tg_state->ampl1 = TWISTED_DTMF_FIRST_AMPL ;
	tg_state->ampl2 = 0x7fff ;
	tg_state->ampl3 = 0x7fff ;
	tg_state->ampl4 = 0x7fff ;
	tg_state->on_duration = 0 ;
	tg_state->off_duration = 1 ;
	*ARGSIN1[tg_mod] = tone_sin[tg_state->tone1] ;
	*ARGCOS1[tg_mod] = tone_cos[tg_state->tone1] ;
	*ARGATT1[tg_mod] = tg_state->ampl1 ;
	*ARGSIN2[tg_mod] = tone_sin[tg_state->tone2] ;
	*ARGCOS2[tg_mod] = tone_cos[tg_state->tone2] ;
	*ARGATT2[tg_mod] = tg_state->ampl2 ;
	*ARGSIN3[tg_mod] = tone_sin[tg_state->tone3] ;
	*ARGCOS3[tg_mod] = tone_cos[tg_state->tone3] ;
	*ARGATT3[tg_mod] = tg_state->ampl3 ;
#if defined ( HAVE_TONEGEN4 )
	*ARGSIN4[tg_mod] = tone_sin[tg_state->tone4] ;
	*ARGCOS4[tg_mod] = tone_cos[tg_state->tone4] ;
	*ARGATT4[tg_mod] = tg_state->ampl4 ;
#endif
	*TIMER2_PRESET[tg_mod] = tg_state->off_duration << ( shift + 3 ) ;
	*TIMER1_PRESET[tg_mod] = tg_state->on_duration << ( shift + 3 ) ;
	tg_state->status = SC1445x_AE_TONEGEN_IDLE ;
	if( tg_state->tone_seq_start ) ;
		sc1445x_internal_free_tone_seq( tg_state ) ;

	/* should we disable the line now? */

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	sc1445x_internal_classd_autopower( ae_state,
			ae_state->iface_mode,
#  if defined( USE_CLASSD_AUTOPOWER_IN_HANDS_FREE )
			sc1445x_private_have_active_channels( ae_state ),
#  else
			1,
#  endif
			0 ) ;
#endif

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	if( tg_state->is_ringing || tg_state->is_playing_dtmf ) {
		/* revert tone volume */
		tg_state->is_ringing = 0 ;
		tg_state->is_playing_dtmf = 0 ;
		do_sc1445x_ae_set_vspk_volume( ae_state ) ;
	}
#endif

#if defined( SC1445x_AE_USE_FAX_TIMER )
	 if( unlikely( tg_state->is_CED_detection_off ||
					tg_state->is_CNG_detection_off ) ) {
		/* re-enable detectors in 2 secs */
		tg_state->fax_timer.data = (unsigned long)tg_state ;
		tg_state->fax_timer.function = fax_isr ;
		tg_state->fax_timer.expires = jiffies + ( 2 * HZ ) ;
		DPRINT( "%s: schedule fax_timer for %lu\n", __FUNCTION__,
						tg_state->fax_timer.expires ) ;
		add_timer( &tg_state->fax_timer ) ;
	}
#endif

	return SC1445x_AE_OK ;
}

#define NEW_STOP_TONE_DSP_CMD
/* alternate, internal, way to stop playing a tone */
static void sc1445x_internal_fast_stop_tone( sc1445x_ae_state* ae_state,
							unsigned short tg_mod )
{
	sc1445x_ae_tonegen_state* tg_state ;
#if !defined( NEW_STOP_TONE_DSP_CMD )
	volatile unsigned short* TIMER1 = TIMER1_PRESET[tg_mod] + 17 ;
	volatile unsigned short* TIMER2 = TIMER1_PRESET[tg_mod] + 18 ;
	const short is_wideband = 
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
				currently_wideband() ;
#else
				is_stream_wideband( ae_state, tg_mod ) ;
#endif
	const short shift = is_wideband ?  1 :  0 ;
	const unsigned short* const tone_sin =  is_wideband ?
			sc1445x_ae_tone_sin_16kHz :  sc1445x_ae_tone_sin_8kHz ;
	const unsigned short* const tone_cos =  is_wideband ?
			sc1445x_ae_tone_cos_16kHz :  sc1445x_ae_tone_cos_8kHz ;
#endif  /* !NEW_STOP_TONE_DSP_CMD */

	tg_state = &ae_state->tonegen[tg_mod] ;

	if( unlikely( tg_state->is_being_reprogrammed ) )
		return ;  /* skip stopping */

	if( timer_pending( &tg_state->timer ) )
		del_timer( &tg_state->timer ) ;
	if( timer_pending( &tg_state->auto_stop_timer ) )
		del_timer( &tg_state->auto_stop_timer ) ;
#if defined( SC1445x_AE_USE_FAX_TIMER )
	if( timer_pending( &tg_state->fax_timer ) )
		del_timer( &tg_state->fax_timer ) ;
#endif
	if( timer_pending( &tg_state->cb_timer ) )
		del_timer( &tg_state->cb_timer ) ;

	tg_state->play_count = 1 ;
	tg_state->play_counter = 0 ;
	tg_state->status = SC1445x_AE_TONEGEN_STOPPING ;

	/* stop playing this tone, set up for idle */
#if defined( NEW_STOP_TONE_DSP_CMD )
	sc1445x_internal_send_dsp_cmd( 0x0010, tg_mod ) ;
#endif
	tg_state->tone1 = SC1445x_AE_TONE_F0 ;
	tg_state->tone2 = SC1445x_AE_TONE_F0 ;
	tg_state->tone3 = SC1445x_AE_TONE_F0 ;
	tg_state->tone4 = SC1445x_AE_TONE_F0 ;
	tg_state->ampl1 = TWISTED_DTMF_FIRST_AMPL ;
	tg_state->ampl2 = 0x7fff ;
	tg_state->ampl3 = 0x7fff ;
	tg_state->ampl4 = 0x7fff ;
	tg_state->on_duration = 0 ;
	tg_state->off_duration = 1 ;
#if !defined( NEW_STOP_TONE_DSP_CMD )
	*ARGSIN1[tg_mod] = tone_sin[tg_state->tone1] ;
	*ARGCOS1[tg_mod] = tone_cos[tg_state->tone1] ;
	*ARGATT1[tg_mod] = tg_state->ampl1 ;
	*ARGSIN2[tg_mod] = tone_sin[tg_state->tone2] ;
	*ARGCOS2[tg_mod] = tone_cos[tg_state->tone2] ;
	*ARGATT2[tg_mod] = tg_state->ampl2 ;
	*ARGSIN3[tg_mod] = tone_sin[tg_state->tone3] ;
	*ARGCOS3[tg_mod] = tone_cos[tg_state->tone3] ;
	*ARGATT3[tg_mod] = tg_state->ampl3 ;
#if defined ( HAVE_TONEGEN4 )
	*ARGSIN4[tg_mod] = tone_sin[tg_state->tone4] ;
	*ARGCOS4[tg_mod] = tone_cos[tg_state->tone4] ;
	*ARGATT4[tg_mod] = tg_state->ampl4 ;
	/* account for extra variables */
	TIMER1 += 5 ;
	TIMER2 += 5 ;
#endif
	*TIMER2_PRESET[tg_mod] = tg_state->off_duration << ( shift + 3 ) ;
	*TIMER1_PRESET[tg_mod] = tg_state->on_duration << ( shift + 3 ) ;
#endif  /* !NEW_STOP_TONE_DSP_CMD */
#if !defined( NEW_STOP_TONE_DSP_CMD )
	//*TIMER1 = 0xffff ;
	*TIMER1 = 0x0000 ;
	*TIMER2 = 0xffff ;
#endif
	tg_state->status = SC1445x_AE_TONEGEN_IDLE ;
	if( tg_state->tone_seq_start ) ;
		sc1445x_internal_free_tone_seq( tg_state ) ;

#if 0 && !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	ae_state->tonegen[tg_mod].is_ringing = 0 ;
	ae_state->tonegen[tg_mod].is_playing_dtmf = 0 ;
	do_sc1445x_ae_set_vspk_volume( ae_state ) ;
#endif

#if defined( SC1445x_AE_USE_FAX_TIMER )
	if( unlikely( tg_state->is_CED_detection_off ||
					tg_state->is_CNG_detection_off ) ) {
		DPRINT( "%s: calling fax_isr()\n", __FUNCTION__ ) ;
		fax_isr( (unsigned long)tg_state ) ;
	}
#endif
}

/* "interrupt handler" for tone generation */
static void auto_stop_tone_isr( unsigned long arg )
{
	sc1445x_ae_tonegen_state* tg_state = (sc1445x_ae_tonegen_state*)arg ;
	sc1445x_ae_state* my_ae_state = tg_state->my_ae_state ;
	unsigned short tg_mod = tg_state - my_ae_state->tonegen ;

	/* cover case of auto-stopping a finishing tone */
	tg_state->status = SC1445x_AE_TONEGEN_PLAYING ;
	sc1445x_ae_stop_tone( my_ae_state, tg_mod ) ;
}

#if defined( SC1445x_AE_USE_FADE_IN_TIMER )
static void fade_in_isr( unsigned long arg )
{
	sc1445x_ae_channel_state* acs = (sc1445x_ae_channel_state*)arg ;
	sc1445x_ae_state* my_ae_state = acs->my_ae_state ;

#  if 0
	sc1445x_ae_set_vspk_volume( my_ae_state, acs->vol_level ) ;
	DPRINT( "%s: reverted volume to %d\n", __FUNCTION__, acs->vol_level ) ;
#  else
	sc1445x_ae_unmute_spk( my_ae_state ) ;
#  endif
}
#endif

/* automatically stop playing the current tone after x msec */
short sc1445x_ae_auto_stop_tone( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned long x )
{
	sc1445x_ae_tonegen_state* tg_state ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;

	tg_state = &ae_state->tonegen[tg_mod] ;

	if( timer_pending( &tg_state->auto_stop_timer ) )
		del_timer( &tg_state->auto_stop_timer ) ;
	tg_state->auto_stop_timer.data = (unsigned long)tg_state ;
	tg_state->auto_stop_timer.function = auto_stop_tone_isr ;
	tg_state->auto_stop_timer.expires = jiffies + ( x * HZ / 1000 ) ;
	add_timer( &tg_state->auto_stop_timer ) ;

	return SC1445x_AE_OK ;
}

/* test if the tone generation module is idle */
short sc1445x_ae_is_tonegen_idle( sc1445x_ae_state* ae_state,
			unsigned short tg_mod, unsigned short* is_tonegen_idle )
{
	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_POINTER_ARG( is_tonegen_idle ) ;

	*is_tonegen_idle =
		SC1445x_AE_TONEGEN_IDLE == ae_state->tonegen[tg_mod].status ;
	return SC1445x_AE_OK ;
}

/* set tone volume */
short sc1445x_ae_set_tone_volume( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short vol )
{
	unsigned short cmd ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_ARG_CEIL( vol, 0x8000 ) ;

	cmd = ( tg_mod < 3 ) ?  ( 0x0009 + tg_mod ) :  0x0027 ;
	sc1445x_internal_send_dsp_cmd( cmd, vol ) ;
	ae_state->tonegen[tg_mod].volume = vol ;

	return SC1445x_AE_OK ;
}

/* get tone volume */
short sc1445x_ae_get_tone_volume( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short* vol )
{
	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_POINTER_ARG( vol ) ;

	*vol = ae_state->tonegen[tg_mod].volume ;
	return SC1445x_AE_OK ;
}

/* set tone volume for TX path */
short sc1445x_ae_set_tone_volume_tx( sc1445x_ae_state* ae_state,
                                                        unsigned short vol )
{
	NOT_AVAILABLE_WITH_PCM_LINES

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( vol, 0x8000 ) ;

	sc1445x_internal_send_dsp_cmd( 0x000f, vol ) ;

	return SC1445x_AE_OK ;
}

/* set tone volume for TX path, with parameter for tg_mod */
short sc1445x_ae_set_tone_volume_tx_ex( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short vol )
{
	short res = SC1445x_AE_OK ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_ARG_CEIL( vol, 0x8000 ) ;

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	sc1445x_internal_send_dsp_cmd( 0x0021 + tg_mod, vol ) ;
#else
	sc1445x_internal_send_dsp_cmd( 0x000f,
					SC1445x_AE_TONEGEN_DEFAULT_VOLUME_TX ) ;
	res = sc1445x_ae_set_tonegen_tx_volume( ae_state, tg_mod, vol ) ;
#endif

	return res ;
}

/* play a predefined standard tone */
short sc1445x_ae_play_standard_tone( sc1445x_ae_state* ae_state,
			unsigned short tg_mod, sc1445x_ae_std_tone std_tone )
{
	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	if( std_tone > SC1445x_AE_STD_TONE_D )
		CHECK_ARG_CEIL( std_tone, SC1445x_AE_STD_TONE_INVALID ) ;

	sc1445x_internal_fast_stop_tone( ae_state, tg_mod ) ;

	if( std_tone <= SC1445x_AE_STD_TONE_D ) {
		/* play a DTMF */
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		ae_state->tonegen[tg_mod].is_playing_dtmf = 1 ;
		ae_state->tonegen[tg_mod].is_ringing = 0 ;
		do_sc1445x_ae_set_vspk_volume( ae_state ) ;
#endif
		sc1445x_ae_internal_start_tone4( ae_state, tg_mod,
					dtmf[std_tone][0],
					TWISTED_DTMF_FIRST_AMPL,
					dtmf[std_tone][1], 0x7fff,
					SC1445x_AE_TONE_F0, 0x7fff,
					SC1445x_AE_TONE_F0, 0x7fff,
					200, 0,
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
					0 /* play "forever" */
#else
					1 /* play once */
#endif
				) ;
	}
	else {
		/* play a PSTN tone */

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		/* don't let the tone be heard by the other party */
		if( std_tone < SC1445x_AE_STD_TONE_BUSY_LR )
			sc1445x_ae_set_tone_volume_tx( ae_state, 0x0000 ) ;
#endif

		if( std_tone >= SC1445x_AE_STD_TONE_BUSY_LR )
			std_tone -= SC1445x_AE_CP_TONE_LR_OFFSET ;

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		/* use special volume for "ringing" tone */
		if( SC1445x_AE_STD_TONE_RINGING == std_tone ) {
			ae_state->tonegen[tg_mod].is_ringing = 1 ;
			ae_state->tonegen[tg_mod].is_playing_dtmf = 0 ;
		} else if( SC1445x_AE_STD_TONE_KEYPAD_ECHO1 == std_tone ||
				SC1445x_AE_STD_TONE_KEYPAD_ECHO1 == std_tone ) {
			ae_state->tonegen[tg_mod].is_playing_dtmf = 1 ;
			ae_state->tonegen[tg_mod].is_ringing = 0 ;
		} else {
			ae_state->tonegen[tg_mod].is_playing_dtmf = 0 ;
			ae_state->tonegen[tg_mod].is_ringing = 0 ;
		}
		do_sc1445x_ae_set_vspk_volume( ae_state ) ;
#endif

		std_tone -= SC1445x_AE_STD_TONE_BUSY ;
		if( pstn_tones[std_tone].is_simple_tone ) {
			sc1445x_ae_internal_start_tone4( ae_state, tg_mod,
				pstn_tones[std_tone].simple_tone.f1,
				pstn_tones[std_tone].simple_tone.a1,
				pstn_tones[std_tone].simple_tone.f2,
				pstn_tones[std_tone].simple_tone.a2,
				pstn_tones[std_tone].simple_tone.f3,
				pstn_tones[std_tone].simple_tone.a3,
				pstn_tones[std_tone].simple_tone.f4,
				pstn_tones[std_tone].simple_tone.a4,
				pstn_tones[std_tone].simple_tone.dur_on,
				pstn_tones[std_tone].simple_tone.dur_off,
				pstn_tones[std_tone].simple_tone.play_count ) ;
		} else {
			sc1445x_ae_start_tone4_sequence( ae_state, tg_mod,
				pstn_tones[std_tone].complex_tone.seq_len,
				pstn_tones[std_tone].complex_tone.seq,
				pstn_tones[std_tone].complex_tone.repeat_seq,
				pstn_tones[std_tone].complex_tone.play_count ) ;
		}

	}

	return SC1445x_AE_OK ;
}

static short do_start_custom_tone4( sc1445x_ae_state* ae_state,
				unsigned short tg_mod,
				const sc1445x_ae_custom_tone4_params* params,
				unsigned short dur_on, unsigned short dur_off,
				unsigned short play_count )
{
	/* this is an internal function, we don't check args */

	sc1445x_ae_tonegen_state* tonegen ;
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	unsigned short cmd ;
#endif
	short is_wideband ;
	short shift ;
	const unsigned short* tone_sin ;
	const unsigned short* tone_cos ;
#if defined( SMOOTH_IFACE_TRANSITION )
	long res ;
	volatile unsigned short* dsp1_transition = (void*)0x10538 ;
	volatile unsigned short* dsp1_transition_mode = (void*)0x1053a ;
#  define FADEOUT_DONE	( 0 == *dsp1_transition  ||  *dsp1_transition_mode > 1 )
#endif
	unsigned short max_dur ;

	is_wideband = 
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
				currently_wideband() ;
#else
				is_stream_wideband( ae_state, tg_mod ) ;
#endif

	DPRINT( "%s entering s=%d, w=%d\n", __FUNCTION__, tg_mod, is_wideband );

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	sc1445x_internal_classd_autopower( ae_state,
						ae_state->iface_mode, 1, 0 ) ;
#endif

	shift = is_wideband ?  1 :  0 ;

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	if( is_wideband )
		_sc1445x_internal_init_codec( ae_state,
					SC1445x_AE_RAW_PCM_RATE_16000, 0 ) ;
#endif

	tonegen = &ae_state->tonegen[tg_mod] ;

	if( tonegen->status != SC1445x_AE_TONEGEN_IDLE ) {
		/* stop currently playing tone */
		sc1445x_internal_fast_stop_tone( ae_state, tg_mod ) ;
		if( timer_pending( &tonegen->timer ) )
			del_timer( &tonegen->timer ) ;
		if( timer_pending( &tonegen->auto_stop_timer ) )
			del_timer( &tonegen->auto_stop_timer ) ;
	}

	tone_sin =  is_wideband ?  params->wide_argsin :  params->narrow_argsin ;
	tone_cos =  is_wideband ?  params->wide_argcos :  params->narrow_argcos ;

#if defined( SC1445x_AE_USE_FAX_TIMER )
	/*
	 * check if we are abput to start
	 * - a 2100Hz tone (i.e. CED); if so, disable CED detection
	 * - a 1100Hz tone (i.e. CNG); if so, disable CNG detection
	 */
	if( is_wideband ) {
		if( unlikely( 0x5DFF == tone_sin[0]  &&  0x56E4 == tone_cos[0]
					&&  !tone_sin[1]  &&  !tone_sin[2] ) ) {
			/* 2100Hz tone */
			tonegen->is_CED_detection_off = 1 ;
		} else if( unlikely( 0x3597 == tone_sin[0]
						&&  0x743F == tone_cos[0]
					&&  !tone_sin[1]  &&  !tone_sin[2] ) ) {
			/* 1100Hz tone */
			tonegen->is_CNG_detection_off = 1 ;
		}
	} else {
		if( unlikely( 0x7F9D == tone_sin[0]  &&  0xF5F5 == tone_cos[0]
				&&  !tone_sin[1]  &&  !tone_sin[2] ) ) {
			/* 2100Hz tone */
			tonegen->is_CED_detection_off = 1 ;
		} else if( unlikely( 0x6156 == tone_sin[0]
						&&  0x5322 == tone_cos[0]
					&&  !tone_sin[1]  &&  !tone_sin[2] ) ) {
			/* 1100Hz tone */
			tonegen->is_CNG_detection_off = 1 ;
		}
	}

	if( unlikely( tonegen->is_CED_detection_off ) ) {
		sc1445x_internal_send_dsp_cmd( 0x0028, (tg_mod << 8) | 0 ) ;
		PRINT( PRINT_LEVEL "%s: disabled CED detector #%d\n",
							__FUNCTION__, tg_mod ) ;
	} else if( unlikely( tonegen->is_CNG_detection_off ) ) {
		sc1445x_internal_send_dsp_cmd( 0x0029, (tg_mod << 8) | 0 ) ;
		PRINT( PRINT_LEVEL "%s: disabled CNG detector #%d\n",
							__FUNCTION__, tg_mod ) ;
	}
#endif

#if defined( DONT_SEND_TONES ) && !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	/* don't send the tone to the other side */
	sc1445x_internal_send_dsp_cmd( 0x000f, 0 ) ;
#endif

#if defined( DONT_HEAR_TONES ) && !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	/* don't send the tone to the speaker */
	sc1445x_internal_send_dsp_cmd( 0x0009, 0 ) ;
#endif

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	/* enable line */
	cmd = ( tg_mod < 3 ) ?  ( 0x0004 + tg_mod ) :  0x0026 ;
	sc1445x_internal_send_dsp_cmd( cmd, 0x0011 ) ;
#endif

	/* initialize the tonegen state */
	tonegen->tone1 = SC1445x_AE_STD_TONE_INVALID ;
	tonegen->tone2 = SC1445x_AE_STD_TONE_INVALID ;
	tonegen->tone3 = SC1445x_AE_STD_TONE_INVALID ;
#if defined ( HAVE_TONEGEN4 )
	tonegen->tone4 = SC1445x_AE_STD_TONE_INVALID ;
#endif
	tonegen->ampl1 = 0 ;
	tonegen->ampl2 = 0 ;
	tonegen->ampl3 = 0 ;
#if defined ( HAVE_TONEGEN4 )
	tonegen->ampl4 = 0 ;
#endif
	max_dur = ( 4096 >> shift ) - 1 ;
	if( dur_on > max_dur ) {
		PRINT( KERN_DEBUG
			"%s: on_duration=%d is greater than %d; clipping it "
			"to %d!\n", __FUNCTION__, dur_on, max_dur, max_dur ) ;
		dur_on = max_dur ;
	}
	tonegen->on_duration = dur_on ;
	if( dur_off > max_dur ) {
		PRINT( KERN_DEBUG
			"%s: off_duration=%d is greater than %d; clipping it "
			"to %d!\n", __FUNCTION__, dur_off, max_dur, max_dur ) ;
		dur_off = max_dur ;
	}
	tonegen->off_duration = dur_off ;
	tonegen->play_count = play_count ;
	tonegen->play_counter = 0 ;

	tonegen->is_custom_tone = 1 ;
	tonegen->custom_tone = *params ;

#if defined( SMOOTH_IFACE_TRANSITION )
	/* make sure last command has been read */
	while( !sc1445x_internal_can_send_dsp_cmd() )
		;
	/* wait for fadeout, if any, to finish */
	res = wait_event_interruptible_timeout( ae_state->fadeoutq,
							FADEOUT_DONE, 10 ) ;
	if( res < 0 )
		DPRINT( PRINT_LEVEL "%s: wait_event_interruptible_timeout() "
						"failed\n", __FUNCTION__ ) ;
	else
		DPRINT( "%s: wait_event_interruptible_timeout() returned %ld\n",
							__FUNCTION__, res ) ;
#endif

	/* initialize the relevant DSP variables */
	*ARGSIN1[tg_mod] = tone_sin[0] ;
	*ARGCOS1[tg_mod] = tone_cos[0] ;
	*ARGATT1[tg_mod] = params->amplitude[0] ;
	*ARGSIN2[tg_mod] = tone_sin[1] ;
	*ARGCOS2[tg_mod] = tone_cos[1] ;
	*ARGATT2[tg_mod] = params->amplitude[1] ;
	*ARGSIN3[tg_mod] = tone_sin[2] ;
	*ARGCOS3[tg_mod] = tone_cos[2] ;
	*ARGATT3[tg_mod] = params->amplitude[2] ;
#if defined ( HAVE_TONEGEN4 )
	*ARGSIN4[tg_mod] = tone_sin[3] ;
	*ARGCOS4[tg_mod] = tone_cos[3] ;
	*ARGATT4[tg_mod] = params->amplitude[3] ;
#endif

	/* add a timer to either put tonegen to idle or repeat the tone */
	tonegen->status = SC1445x_AE_TONEGEN_PLAYING ;
	if( play_count != 0 ) {
		generic_add_timer( 20, ae_state, tg_mod ) ;
	}

#if defined ( HAVE_TONEGEN4 )
	if( dur_on  &&  dur_off ) {
		/* do fade-out */
		DPRINT( "FADE OUT\n" ) ;
		*FADEOUT_PRESET[tg_mod] = 6 ;	/* 64 samples */
		*FADEIN_PRESET[tg_mod] = 6 ;	/* 64 samples */
	} else {
		/* don't do fade-out */
		*FADEOUT_PRESET[tg_mod] = 0 ;
		*FADEIN_PRESET[tg_mod] = 0 ;
	}
#endif

	/* convert times from msecs to 125usec units */
	if( 0 == dur_off )
		*TIMER2_PRESET[tg_mod] = 0xffff ;
	else/* if( dur_off  <=  ( 4096 >> shift ) )*/
		*TIMER2_PRESET[tg_mod] = dur_off << ( shift + 3 ) ;
#if 0
	/* clipping has been done already */
	else {
		*TIMER2_PRESET[tg_mod] = 0x7fff ;  /* the best we can do */
		PRINT( PRINT_LEVEL
			"%s: off_duration=%d is greater than %d; clipping it "
			"to %d!\n", __FUNCTION__, dur_off,
						4096 >> shift, 4096 >> shift ) ;
	}
#endif
#if 0
	/* clipping has been done already */
	if( dur_on <= max_dur )
#endif
		*TIMER1_PRESET[tg_mod] = dur_on << ( shift + 3 ) ;
#if 0
	else {
		*TIMER1_PRESET[tg_mod] = 0x7fff ;  /* the best we can do */
		PRINT( PRINT_LEVEL
			"%s: on_duration=%d is greater than %d; clipping it "
			"to %d!\n", __FUNCTION__, dur_on,
						4096 >> shift, 4096 >> shift ) ;
	}
#endif

	return SC1445x_AE_OK ;
}

/*
 * start playing a custom tone
 * on-off times are given in msec
 * play_count:
 * 	== 0 --> play "for ever"
 * 	 > 0 --> play (play_count) times
 */
short sc1445x_ae_start_custom_tone4( sc1445x_ae_state* ae_state,
				unsigned short tg_mod,
				const sc1445x_ae_custom_tone4_params* params,
				unsigned short dur_on, unsigned short dur_off,
				unsigned short play_count )
{
	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_POINTER_ARG( params ) ;

	return do_start_custom_tone4( ae_state, tg_mod, params,
						dur_on, dur_off, play_count ) ;
}

/* start playing a custom tone */
/* on-off times are given in msec */
short sc1445x_ae_start_custom_tone( sc1445x_ae_state* ae_state,
				unsigned short tg_mod,
				const sc1445x_ae_custom_tone_params* params,
				unsigned short dur_on, unsigned short dur_off,
				short repeat )
{
	sc1445x_ae_custom_tone4_params tp ;
	unsigned short f, play_count =  repeat ?  0 :  1 ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_POINTER_ARG( params ) ;

	for( f = 0 ;  f < 3 ;  ++f ) {
		tp.narrow_argcos[f] = params->narrow_argcos[f] ;
		tp.narrow_argsin[f] = params->narrow_argsin[f] ;
		tp.wide_argcos[f] = params->wide_argcos[f] ;
		tp.wide_argsin[f] = params->wide_argsin[f] ;
		tp.amplitude[f] = params->amplitude[f] ;
	}
	tp.narrow_argcos[f] = ARGCOS_N( 0 ) ;
	tp.narrow_argsin[f] = ARGSIN_N( 0 ) ;
	tp.wide_argcos[f] = ARGCOS_W( 0 ) ;
	tp.wide_argsin[f] = ARGSIN_W( 0 ) ;
	tp.amplitude[f] = 0x7fff ;

	return do_start_custom_tone4( ae_state, tg_mod, &tp,
						dur_on, dur_off, play_count ) ;
}

/* start playing a tone sequence */
short sc1445x_ae_start_tone4_sequence( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short seq_len,
				const sc1445x_ae_tone4_sequence_part* seq,
				sc1445x_ae_tone_seq_repeat repeat_seq,
				unsigned short play_count )
{
	sc1445x_ae_tonegen_state* tonegen ;
	unsigned short i ;
	short res ;
	sc1445x_ae_tone4_seq_container* tsc = NULL ;	/* to make gcc happy */
	sc1445x_ae_tone4_seq_container* prev = NULL ;	/* to make gcc happy */


	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_POINTER_ARG( seq ) ;
	CHECK_ARG_RANGE( seq_len, 1, 1000 ) ;

	sc1445x_internal_fast_stop_tone( ae_state, tg_mod ) ;

	tonegen = &ae_state->tonegen[tg_mod] ;

	/* allocate memory for tone sequence and copy params */
	if( tonegen->tone_seq_start )
		sc1445x_internal_free_tone_seq( tonegen ) ;
	for( i = 0 ;  i < seq_len ;  ++i ) {
		tsc = MALLOC( seq_len * sizeof( *tsc ) ) ;
		if( unlikely( !tsc ) ) {
			PRINT( sc1445x_ae_nomem_error, __FUNCTION__,
							"tone sequence" ) ;
			sc1445x_internal_free_tone_seq( tonegen ) ;
			return SC1445x_AE_ERR_NO_MEM ;
		}

		memcpy( &tsc->data, &seq[i], sizeof( tsc->data ) ) ;

		if( unlikely( 0 == i ) )
			tonegen->tone_seq_start = tsc ;
		else
			prev->next = tsc ;

		prev = tsc ;
	}
	tsc->next = NULL ;  /* end of sequence */
	tonegen->tone_seq_curr = tonegen->tone_seq_start ;
	tonegen->repeat_tone_seq = repeat_seq ;
	tonegen->tone_seq_play_count = play_count ;
	tonegen->tone_seq_play_counter = 0 ;

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	sc1445x_internal_classd_autopower( ae_state,
						ae_state->iface_mode, 1, 0 ) ;
#endif

	tsc = tonegen->tone_seq_curr ;
	res = sc1445x_ae_start_custom_tone4( ae_state, tg_mod,
				&tsc->data.tone4_params,
				tsc->data.on_duration,
				tsc->data.off_duration, tsc->data.play_count ) ;

	if( res != SC1445x_AE_OK )
		sc1445x_internal_free_tone_seq( tonegen ) ;

	return res ;
}

/* start playing a tone sequence */
short sc1445x_ae_start_tone_sequence( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short seq_len,
				const sc1445x_ae_tone_sequence_part* seq,
				sc1445x_ae_tone_seq_repeat repeat_seq )
{
	sc1445x_ae_tonegen_state* tonegen ;
	unsigned short i, f ;
	short res ;
	sc1445x_ae_tone4_seq_container* tsc = NULL ;	/* to make gcc happy */
	sc1445x_ae_tone4_seq_container* prev = NULL ;	/* to make gcc happy */


	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_POINTER_ARG( seq ) ;
	CHECK_ARG_RANGE( seq_len, 1, 1000 ) ;

	sc1445x_internal_fast_stop_tone( ae_state, tg_mod ) ;

	tonegen = &ae_state->tonegen[tg_mod] ;

	/* allocate memory for tone sequence and copy params */
	if( tonegen->tone_seq_start )
		sc1445x_internal_free_tone_seq( tonegen ) ;
	for( i = 0 ;  i < seq_len ;  ++i ) {
		tsc = MALLOC( seq_len * sizeof( *tsc ) ) ;
		if( unlikely( !tsc ) ) {
			PRINT( sc1445x_ae_nomem_error, __FUNCTION__,
							"tone sequence" ) ;
			sc1445x_internal_free_tone_seq( tonegen ) ;
			return SC1445x_AE_ERR_NO_MEM ;
		}

		/* convert from 3-frequency to 4-frequency definitions */
		tsc->data.on_duration = seq[i].on_duration ;
		tsc->data.off_duration = seq[i].off_duration ;

		for( f = 0 ;  f < 3 ;  ++f ) {
			tsc->data.tone4_params.narrow_argcos[f] =
					seq[i].tone_params.narrow_argcos[f] ;
			tsc->data.tone4_params.narrow_argsin[f] =
					seq[i].tone_params.narrow_argsin[f] ;
			tsc->data.tone4_params.wide_argcos[f] =
					seq[i].tone_params.wide_argcos[f] ;
			tsc->data.tone4_params.wide_argsin[f] =
					seq[i].tone_params.wide_argsin[f] ;
			tsc->data.tone4_params.amplitude[f] =
					seq[i].tone_params.amplitude[f] ;
		}
		tsc->data.tone4_params.narrow_argcos[f] = ARGCOS_N( 0 ) ;
		tsc->data.tone4_params.narrow_argsin[f] = ARGSIN_N( 0 ) ;
		tsc->data.tone4_params.wide_argcos[f] = ARGCOS_W( 0 ) ;
		tsc->data.tone4_params.wide_argsin[f] = ARGSIN_W( 0 ) ;
		tsc->data.tone4_params.amplitude[f] = 0x7fff ;
		tsc->data.play_count = 1 ;

		if( unlikely( 0 == i ) )
			tonegen->tone_seq_start = tsc ;
		else
			prev->next = tsc ;

		prev = tsc ;
	}
	tsc->next = NULL ;  /* end of sequence */
	tonegen->tone_seq_curr = tonegen->tone_seq_start ;
	tonegen->repeat_tone_seq = repeat_seq ;
	tonegen->tone_seq_play_count = 1 ;
	tonegen->tone_seq_play_counter = 0 ;

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	sc1445x_internal_classd_autopower( ae_state,
						ae_state->iface_mode, 1, 0 ) ;
#endif

	tsc = tonegen->tone_seq_curr ;
	res = sc1445x_ae_start_custom_tone4( ae_state, tg_mod,
				&tsc->data.tone4_params,
				tsc->data.on_duration,
				tsc->data.off_duration, tsc->data.play_count ) ;

	if( res != SC1445x_AE_OK )
		sc1445x_internal_free_tone_seq( tonegen ) ;

	return res ;
}

/* start playing a ringtone sequence */
short sc1445x_ae_start_ringtone4_sequence( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short seq_len,
				const sc1445x_ae_tone4_sequence_part* seq,
				sc1445x_ae_tone_seq_repeat repeat_seq,
				unsigned short play_count )
{
	short res ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_POINTER_ARG( seq ) ;
	CHECK_ARG_RANGE( seq_len, 1, 1000 ) ;

	res = sc1445x_ae_start_tone4_sequence( ae_state, tg_mod, seq_len, seq,
						repeat_seq, play_count ) ;
	if( SC1445x_AE_OK == res ) {
		ae_state->tonegen[tg_mod].is_ringing = 1 ;
		ae_state->tonegen[tg_mod].is_playing_dtmf = 0 ;
		do_sc1445x_ae_set_vspk_volume( ae_state ) ;
	}

	return res ;
}

/* start playing a key (DTMF) tone sequence */
short sc1445x_ae_start_keytone4_sequence( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short seq_len,
				const sc1445x_ae_tone4_sequence_part* seq,
				sc1445x_ae_tone_seq_repeat repeat_seq,
	       			unsigned short play_count )
{
	short res ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_POINTER_ARG( seq ) ;
	CHECK_ARG_RANGE( seq_len, 1, 1000 ) ;

	res = sc1445x_ae_start_tone4_sequence( ae_state, tg_mod, seq_len, seq,
						repeat_seq, play_count ) ;
	if( SC1445x_AE_OK == res ) {
		ae_state->tonegen[tg_mod].is_ringing = 0 ;
		ae_state->tonegen[tg_mod].is_playing_dtmf = 1 ;
		do_sc1445x_ae_set_vspk_volume( ae_state ) ;
	}

	return res ;
}

/* start playing a ringtone sequence */
short sc1445x_ae_start_ringtone_sequence( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short seq_len,
				const sc1445x_ae_tone_sequence_part* seq,
				sc1445x_ae_tone_seq_repeat repeat_seq )
{
	short res ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_POINTER_ARG( seq ) ;
	CHECK_ARG_RANGE( seq_len, 1, 1000 ) ;

	res = sc1445x_ae_start_tone_sequence( ae_state, tg_mod, seq_len, seq,
								repeat_seq ) ;
	if( SC1445x_AE_OK == res ) {
		ae_state->tonegen[tg_mod].is_ringing = 1 ;
		ae_state->tonegen[tg_mod].is_playing_dtmf = 0 ;
		do_sc1445x_ae_set_vspk_volume( ae_state ) ;
	}

	return res ;
}

/* expand existing tone sequence (or start a new one if none exists) */
/* expansion is made by adding tones at the end of the sequence */
short sc1445x_ae_expand_tone4_sequence( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short seq_len,
				const sc1445x_ae_tone4_sequence_part* seq )
{
	sc1445x_ae_tonegen_state* tonegen ;
	unsigned short i ;
	short res = SC1445x_AE_OK ;
	sc1445x_ae_tone4_seq_container* tsc = NULL ;	/* to make gcc happy */
	sc1445x_ae_tone4_seq_container* prev = NULL ;	/* to make gcc happy */
	sc1445x_ae_tone4_seq_container* start = NULL ;	/* to make gcc happy */

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_POINTER_ARG( seq ) ;
	CHECK_ARG_RANGE( seq_len, 1, 1000 ) ;

	tonegen = &ae_state->tonegen[tg_mod] ;
	if( !tonegen->tone_seq_start )
		return sc1445x_ae_start_tone4_sequence( ae_state, tg_mod,
			seq_len, seq, SC1445x_AE_TONE_SEQ_NO_REPEAT, 1 ) ;

	/* allocate memory for tone sequence and copy params */
	for( i = 0 ;  i < seq_len ;  ++i ) {
		tsc = MALLOC( seq_len * sizeof( *tsc ) ) ;
		if( unlikely( !tsc ) ) {
			PRINT( sc1445x_ae_nomem_error, __FUNCTION__,
							"tone sequence" ) ;

			if( prev )
				prev->next = NULL ;
			tsc = start ;
			while( tsc ) {
				prev = tsc ;
				tsc = tsc->next ;
				FREE( prev ) ;
			}

			return SC1445x_AE_ERR_NO_MEM ;
		}

		memcpy( &tsc->data, &seq[i], sizeof( tsc->data ) ) ;

		if( unlikely( 0 == i ) )
			start = tsc ;
		else
			prev->next = tsc ;

		prev = tsc ;
	}
	tsc->next = NULL ;  /* end of sequence */

	/* find the last part of the currently playing sequence */
	/* and append the new parts */
	prev = NULL ;
	tsc = tonegen->tone_seq_start ;
	while( tsc ) {
		prev = tsc ;
		tsc = tsc->next ;
	}

	if( likely( prev ) )
		prev->next = start ;
	else {
		tonegen->tone_seq_start = tonegen->tone_seq_curr = start ;
		res = sc1445x_ae_start_custom_tone4( ae_state, tg_mod,
					&start->data.tone4_params,
					start->data.on_duration,
					start->data.off_duration,
					start->data.play_count ) ;

		if( res != SC1445x_AE_OK )
			sc1445x_internal_free_tone_seq( tonegen ) ;
	}

	return res ;
}

/* expand existing tone sequence (or start a new one if none exists) */
/* expansion is made by adding tones at the end of the sequence */
short sc1445x_ae_expand_tone_sequence( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short seq_len,
				const sc1445x_ae_tone_sequence_part* seq )
{
	sc1445x_ae_tonegen_state* tonegen ;
	unsigned short i, f ;
	short res = SC1445x_AE_OK ;
	sc1445x_ae_tone4_seq_container* tsc = NULL ;	/* to make gcc happy */
	sc1445x_ae_tone4_seq_container* prev = NULL ;	/* to make gcc happy */
	sc1445x_ae_tone4_seq_container* start = NULL ;	/* to make gcc happy */

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_POINTER_ARG( seq ) ;
	CHECK_ARG_RANGE( seq_len, 1, 1000 ) ;

	tonegen = &ae_state->tonegen[tg_mod] ;
	if( !tonegen->tone_seq_start )
		return sc1445x_ae_start_tone_sequence( ae_state, tg_mod,
				seq_len, seq, SC1445x_AE_TONE_SEQ_NO_REPEAT ) ;

	/* allocate memory for tone sequence and copy params */
	for( i = 0 ;  i < seq_len ;  ++i ) {
		tsc = MALLOC( seq_len * sizeof( *tsc ) ) ;
		if( unlikely( !tsc ) ) {
			PRINT( sc1445x_ae_nomem_error, __FUNCTION__,
							"tone sequence" ) ;

			if( prev )
				prev->next = NULL ;
			tsc = start ;
			while( tsc ) {
				prev = tsc ;
				tsc = tsc->next ;
				FREE( prev ) ;
			}

			return SC1445x_AE_ERR_NO_MEM ;
		}

		/* convert from 3-frequency to 4-frequency definitions */
		tsc->data.on_duration = seq[i].on_duration ;
		tsc->data.off_duration = seq[i].off_duration ;

		for( f = 0 ;  f < 3 ;  ++f ) {
			tsc->data.tone4_params.narrow_argcos[f] =
					seq[i].tone_params.narrow_argcos[f] ;
			tsc->data.tone4_params.narrow_argsin[f] =
					seq[i].tone_params.narrow_argsin[f] ;
			tsc->data.tone4_params.wide_argcos[f] =
					seq[i].tone_params.wide_argcos[f] ;
			tsc->data.tone4_params.wide_argsin[f] =
					seq[i].tone_params.wide_argsin[f] ;
			tsc->data.tone4_params.amplitude[f] =
					seq[i].tone_params.amplitude[f] ;
		}
		tsc->data.tone4_params.narrow_argcos[f] = ARGCOS_N( 0 ) ;
		tsc->data.tone4_params.narrow_argsin[f] = ARGSIN_N( 0 ) ;
		tsc->data.tone4_params.wide_argcos[f] = ARGCOS_W( 0 ) ;
		tsc->data.tone4_params.wide_argsin[f] = ARGSIN_W( 0 ) ;
		tsc->data.tone4_params.amplitude[f] = 0x7fff ;
		tsc->data.play_count = 1 ;

		if( unlikely( 0 == i ) )
			start = tsc ;
		else
			prev->next = tsc ;

		prev = tsc ;
	}
	tsc->next = NULL ;  /* end of sequence */

	/* find the last part of the currently playing sequence */
	/* and append the new parts */
	prev = NULL ;
	tsc = tonegen->tone_seq_start ;
	while( tsc ) {
		prev = tsc ;
		tsc = tsc->next ;
	}

	if( likely( prev ) )
		prev->next = start ;
	else {
		tonegen->tone_seq_start = tonegen->tone_seq_curr = start ;
		res = sc1445x_ae_start_custom_tone4( ae_state, tg_mod,
					&start->data.tone4_params,
					start->data.on_duration,
					start->data.off_duration,
					tsc->data.play_count ) ;	

		if( res != SC1445x_AE_OK )
			sc1445x_internal_free_tone_seq( tonegen ) ;
	}

	return res ;
}

/*
 * set the RX (playback) volume for a tonegen module
 */
short sc1445x_ae_set_tonegen_rx_volume( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short volume )
{
	NOT_AVAILABLE_WITH_PCM_LINES ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_ARG_CEIL( volume, 0x8000 ) ;

	ae_state->tonegen[tg_mod].rx_volume = volume ;

	/* bits 2:0 have special meaning */
	volume &= ~7 ;
	volume |= (3 + tg_mod) & 7 ;

	sc1445x_internal_send_dsp_cmd( 0x0022, volume ) ;

	return SC1445x_AE_OK ;
}

/*
 * set the TX (capture) volume for a tonegen module
 */
short sc1445x_ae_set_tonegen_tx_volume( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short volume )
{
	NOT_AVAILABLE_WITH_PCM_LINES ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_ARG_CEIL( volume, 0x8000 ) ;

	ae_state->tonegen[tg_mod].tx_volume = volume ;

	/* bits 2:0 have special meaning */
	volume &= ~7 ;
	volume |= (0 + tg_mod) & 7 ;

	sc1445x_internal_send_dsp_cmd( 0x0022, volume ) ;

	return SC1445x_AE_OK ;
}

/*
 * get the RX (playback) volume for a tonegen module
 */
short sc1445x_ae_get_tonegen_rx_volume( sc1445x_ae_state* ae_state,
			unsigned short tg_mod, unsigned short* volume )
{
	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_POINTER_ARG( volume ) ;

	*volume = ae_state->tonegen[tg_mod].rx_volume ;

	return SC1445x_AE_OK ;
}

/*
 * get the TX (capture) volume for a tonegen module
 */
short sc1445x_ae_get_tonegen_tx_volume( sc1445x_ae_state* ae_state,
			unsigned short tg_mod, unsigned short* volume )
{
	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( tg_mod, SC1445x_AE_TONEGEN_MOD_COUNT ) ;
	CHECK_POINTER_ARG( volume ) ;

	*volume = ae_state->tonegen[tg_mod].tx_volume ;

	return SC1445x_AE_OK ;
}

/*
 * register a PID to be notified with signo when a tone ends normally
 */
short sc1445x_ae_register_tone_callback( sc1445x_ae_state* ae_state,
					pid_t pid, int signo, int* __user cmem )
{
	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	ae_state->tone_cb_pid = pid ;
	ae_state->tone_cb_signo = signo ;
	ae_state->tone_cb_cmem = cmem ;

	DPRINT( PRINT_LEVEL "%s: PID %d will get signal %d and read at position"
		" %p when a tone ends\n", __FUNCTION__, pid, signo, cmem ) ;

	return SC1445x_AE_OK ;
}

/* update tonegens after frequency switch */
static void sc1445x_private_update_tonegens( sc1445x_ae_state* ae_state,
							short is_wideband )
{
	unsigned short i ;

	for( i = 0 ;  i < SC1445x_AE_TONEGEN_MOD_COUNT ;  ++i ) {
		sc1445x_ae_tonegen_state* tonegen ;
		short t1, t2 ;
#if 1
		/*
		 * This approach has a small problem:
		 * A single repeating tone must be replaced by a 2-tone
		 * sequence, which consists of the remaining part of the
		 * tone and the "normal" tone.
		 * A non-repeating tone is OK.
		 */
		volatile short* TIMER1 = TIMER1_PRESET[i] + 17 ;
		volatile short* TIMER2 = TIMER1_PRESET[i] + 18 ;
		const short shift = is_wideband ?  4 :  3 ;

#if defined ( HAVE_TONEGEN4 )
		/* account for extra variables */
		TIMER1 += 5 ;
		TIMER2 += 5 ;
#endif
		tonegen = ae_state->tonegen + i ;
		if( SC1445x_AE_TONEGEN_IDLE == tonegen->status )
			continue ;

		DPRINT( "%s: reprogramming tonegen %d...\n",
				__FUNCTION__, i ) ;

		if( 1 == tonegen->play_count ) {
			/*
			 * get the remaining time for the
			 * currently playing tone
			 */
			t1 = *TIMER1 ;
			if( t1 < 0 )
				t1 = 0 ;
			t2 = *TIMER2 ;
			if( t2 < 0 )
				t2 = 0 ;
			/* convert to msec */
			t1 >>= shift ;
			t2 >>= shift ;
		} else {
			/* not an easy case, restart tone */
			t1 = tonegen->on_duration ;
			t2 = tonegen->off_duration ;
		}
		DPRINT( "%s: T1=%d T2=%d t1=%d t2=%d\n", __FUNCTION__,
				tonegen->on_duration, tonegen->off_duration,
				t1, t2 ) ;
#else
		/*
		 * This approach repeats the current tone from the beginning
		 */
		tonegen = ae_state->tonegen + i ;
		t1 = tonegen->on_duration ;
		t2 = tonegen->off_duration ;
#endif

		tonegen->is_being_reprogrammed = 1 ;
		if( tonegen->is_custom_tone ) {
			sc1445x_ae_custom_tone4_params ctp =
				tonegen->custom_tone ;

			sc1445x_ae_start_custom_tone4(
					ae_state, i, &ctp, t1, t2,
					tonegen->play_count -
						tonegen->play_counter ) ;
		} else
			sc1445x_ae_internal_start_tone4(
					ae_state, i,
					tonegen->tone1, tonegen->ampl1,
					tonegen->tone2, tonegen->ampl2,
					tonegen->tone3, tonegen->ampl3,
					tonegen->tone4, tonegen->ampl4,
					t1, t2,
					tonegen->play_count -
						tonegen->play_counter ) ;
		tonegen->is_being_reprogrammed = 0 ;
	}
}



/*******************/
/* RAW PCM SUPPORT */
/*******************/

static unsigned short* raw_pcm_buffers_base[4] = {
	(unsigned short*)0x18000,
	(unsigned short*)0x18fc0,
	(unsigned short*)0x19f80,
	(unsigned short*)0x1b200
} ;

/*
 * start playing raw PCM concurrently with voice
 */
short sc1445x_ae_start_mixing_raw_pcm_with_voice( sc1445x_ae_state* ae_state,
				sc1445x_ae_raw_pcm_rate rate,
				sc1445x_ae_raw_pcm_sample_size sample_bits )
{
	unsigned short i, nchannels ;
	sc1445x_ae_channel_state* acs ;
	unsigned short cmd, cmd_val = 0 ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( rate, SC1445x_AE_RAW_PCM_RATE_INVALID ) ;
	CHECK_ARG_CEIL( sample_bits, SC1445x_AE_RAW_PCM_SAMPLE_SIZE_INVALID ) ;

	if( ae_state->raw_pcm_channel ) {
		PRINT( sc1445x_ae_already_mixing_raw_pcm_error, __FUNCTION__ ) ;
		return SC1445x_AE_ERR_ALREADY_MIXING_RAW_PCM ;
	}

	if( ae_state->mode != SC1445x_AE_MODE_NORMAL ) {
		PRINT( sc1445x_ae_cannot_mix_voice_and_raw_pcm_error,
						__FUNCTION__, ae_state->mode ) ;
		return SC1445x_AE_ERR_CANNOT_MIX_VOICE_AND_RAW_PCM ;
	}

	nchannels = ae_state->audio_channels_count ;

	for( i = 0 ;  i < nchannels ;  ++i ) {
		if( ae_state->audio_channels[i].do_release ) {
			/*
			 * this channel is about to be released
			 * wait on it
			 */
			short res = wait_event_interruptible_timeout(
				ae_state->audio_channels[i].releaseq,
				( 0 == ae_state->audio_channels[i].do_release ),
									10 ) ;
			if( res < 0 )
				DPRINT( PRINT_LEVEL "%s: "
					"wait_event_interruptible_timeout() "
					"failed\n", __FUNCTION__ ) ;
			else
				DPRINT( "%s: wait_event_interruptible_timeout()"
					" returned %d\n", __FUNCTION__, res ) ;
		}

		if( !ae_state->audio_channels[i].is_active )
			break ;
	}
	if( nchannels == i ) {
		PRINT( sc1445x_ae_no_free_channel_for_raw_pcm_error,
								__FUNCTION__ ) ;
		return SC1445x_AE_ERR_NO_FREE_CHANNEL_FOR_RAW_PCM ;
	}

	acs = &ae_state->audio_channels[i] ;
	acs->is_raw_pcm = 1 ;
	acs->is_active = 1 ;
	acs->is_g711_1_enc = 0 ;
	acs->is_g711_1_dec = 0 ;
	acs->g711_1_buddy_dec_ch = nchannels ;
	acs->g711_1_buddy_enc_ch = nchannels ;
	ae_state->raw_pcm_channel = acs ;

	INIT_CONST_FIELD( unsigned short*, &ae_state->raw_pcm, buffer,
					raw_pcm_buffers_base[i] + 0x140 ) ;
	INIT_CONST_FIELD( unsigned short*,
			&ae_state->raw_pcm, block_status,
					raw_pcm_buffers_base[i] + 0x780 ) ;
	INIT_CONST_FIELD( sc1445x_ae_raw_pcm_rate,
					&ae_state->raw_pcm, rate, rate ) ;
	INIT_CONST_FIELD( unsigned short,
			&ae_state->raw_pcm, block_count, 5 ) ;
	memset( ae_state->raw_pcm.block_status, 0,
			sizeof( ae_state->raw_pcm.block_status[0] )
			* ae_state->raw_pcm.block_count ) ;
	/* clear bank used by the DSP */
	memset( (void*)raw_pcm_buffers_base[i], 0, 0x280 ) ;
	ae_state->raw_pcm.next_block_index = 0 ;
	ae_state->raw_pcm.partial = 0 ;
	ae_state->raw_pcm.sample_bits = sample_bits ;

	sc1445x_internal_init_channel_buffers( i, 1 ) ;

	switch( rate ) {
		case SC1445x_AE_RAW_PCM_RATE_8000:
			/* stay at 16kHz is there is a wideband codec in use */
			if( !sc1445x_internal_have_wide_channels( ae_state ) )
				sc1445x_internal_init_codec( ae_state, rate ) ;
			break ;

		case SC1445x_AE_RAW_PCM_RATE_16000:
			sc1445x_internal_init_codec( ae_state, rate ) ;
			break ;

		case SC1445x_AE_RAW_PCM_RATE_32000:
			sc1445x_internal_init_codec( ae_state, rate ) ;
			break ;

		default:
			/* shouldn't happen */
			break ;
	}

	DPRINT( "%s: raw_pcm buffer=%p block_status=%p rate=%d\n", __FUNCTION__,
		ae_state->raw_pcm.buffer, ae_state->raw_pcm.block_status,
		ae_state->raw_pcm.rate ) ;
	/* inform DSP */
	cmd = ( i < 3 ) ?  ( i + 1 ) :  0x0025 ;
	cmd_val = ( (1 << (short)rate) << 8 ) | 0x08 ;
	DPRINT( "%s: sending DSP cmd (%04x, %04x)\n", __FUNCTION__, cmd,
								cmd_val ) ;
	sc1445x_internal_send_dsp_cmd( cmd, cmd_val ) ;

	sc1445x_internal_classd_autopower( ae_state,
						ae_state->iface_mode, 1, 0 ) ;

	return SC1445x_AE_OK ;
}

/*
 * stop playing raw PCM concurrently with voice
 * (stop only the raw PCM playback)
 */
short sc1445x_ae_stop_mixing_raw_pcm_with_voice( sc1445x_ae_state* ae_state )
{
	sc1445x_ae_channel_state* acs ;
	unsigned short ch, nchannels ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	if( !ae_state->raw_pcm_channel ) {
		PRINT( sc1445x_ae_not_mixing_raw_pcm_error, __FUNCTION__ ) ;
		return SC1445x_AE_ERR_NOT_MIXING_RAW_PCM ;
	}

	acs = ae_state->raw_pcm_channel ;
	ae_state->raw_pcm_channel = NULL ;

	nchannels = ae_state->audio_channels_count ;
	for( ch = 0 ;  ch < nchannels ;  ++ch ) {
		if( &ae_state->audio_channels[ch] == acs )
			break ;
	}
	if( unlikely( nchannels == ch ) ) {
		/* this shouldn't happen */
		PRINT( "%s: unknown raw PCM channel %p\n", __FUNCTION__, acs ) ;
	} else {
		ae_state->do_stop_pcm_channel = 1 ;
		sc1445x_ae_deactivate_channel( ae_state, ch ) ;

		acs->is_raw_pcm = 0 ;

		/* clear bank used by the DSP */
		memset( (void*)raw_pcm_buffers_base[ch], 0, 0x280 ) ;
	}

	return SC1445x_AE_OK ;
}

/*
 * enable dual audio path
 * side effect: switch to SC1445x_AE_IFACE_MODE_OPEN_LISTENING iface mode
 */
short sc1445x_ae_enable_dual_audio_path( sc1445x_ae_state* ae_state )
{
	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	if( unlikely( ae_state->dual_audio_path_is_active ) ) {
		PRINT( sc1445x_ae_dual_audio_path_already_active_error,
								__FUNCTION__ ) ;
		return SC1445x_AE_ERR_DUAL_AUDIO_PATH_IS_ACTIVE ;
	}

	/* switch to open listening */
	ae_state->activating_dual_audio_path = 1 ;
	ae_state->old_iface_mode = ae_state->iface_mode ;
	sc1445x_ae_set_iface_mode( ae_state,
					SC1445x_AE_IFACE_MODE_OPEN_LISTENING ) ;

	/* tell DSP to enable the dual audio path */
	sc1445x_internal_send_dsp_cmd( 0x0034, 1 ) ;

	ae_state->dual_audio_path_is_active = 1 ;
	ae_state->activating_dual_audio_path = 0 ;

	do_sc1445x_ae_set_vspk_volume( ae_state ) ;

	return SC1445x_AE_OK ;
}

/*
 * disable dual audio path
 * side effect: switch to the iface mode that was active
 * 		when sc1445x_ae_enable_dual_audio_path() was called
 */
short sc1445x_ae_disable_dual_audio_path( sc1445x_ae_state* ae_state )
{
	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;

	if( unlikely( !ae_state->dual_audio_path_is_active ) ) {
		PRINT( sc1445x_ae_dual_audio_path_not_active_error,
								__FUNCTION__ ) ;
		return SC1445x_AE_ERR_DUAL_AUDIO_PATH_IS_NOT_ACTIVE ;
	}

	ae_state->dual_audio_path_is_active = 0 ;

	/* switch to previous iface mode */
	sc1445x_ae_set_iface_mode( ae_state, ae_state->old_iface_mode ) ;

	ae_state->do_disable_dap = 1 ;
	ae_state->old_iface_mode = SC1445x_AE_IFACE_MODE_INVALID ;

	return SC1445x_AE_OK ;
}



/*****************/
/* RX/TX FILTERS */
/*****************/

/* set the RX/TX filters */
void sc1445x_ae_set_rx_tx_filters( sc1445x_ae_state* ae_state,
					short is_external, short is_wideband,
					short is_headset )
{
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	return ;
#else
	static unsigned short* filter_tx1 = (unsigned short*)0x10802 ;
	static unsigned short* filter_tx2 = (unsigned short*)0x1081E ;
	static unsigned short* filter_tx3 = (unsigned short*)0x11EA0 ;
	static unsigned short* filter_rx1 = (unsigned short*)0x1083A ;
	static unsigned short* filter_rx2 = (unsigned short*)0x10856 ;
	static unsigned short* filter_rx3 = (unsigned short*)0x11F48 ;
	static unsigned short* filter_rx1_ext = (unsigned short*)0x10872 ;
	static unsigned short* filter_rx2_ext = (unsigned short*)0x1088e ;
	static unsigned short* filter_rx3_ext = (unsigned short*)0x11F82 ;

	short tx_filter_sel ;
	short rx_filter_sel ;

	const unsigned short* ftx1 ;
	const unsigned short* ftx2 ;
	const unsigned short* ftx3 ;
	const unsigned short* frx1 ;
	const unsigned short* frx2 ;
	const unsigned short* frx3 ;
	const unsigned short* frx1_ext ;
	const unsigned short* frx2_ext ;
	const unsigned short* frx3_ext ;

	is_external = !!is_external ;
	tx_filter_sel = is_external ? 1 :  ( is_headset ?  2 :  0 ) ;
	rx_filter_sel = is_headset ?  2 :  0 ;

	if( is_wideband ) {
		ftx1 = &ae_state->ap.wideband_filters_TX1[tx_filter_sel][0] ;
		ftx2 = &ae_state->ap.wideband_filters_TX2[tx_filter_sel][0] ;
		ftx3 = &ae_state->ap.wideband_filters_TX3[tx_filter_sel][0] ;
		frx1 = &ae_state->ap.wideband_filters_RX1[rx_filter_sel][0] ;
		frx2 = &ae_state->ap.wideband_filters_RX2[rx_filter_sel][0] ;
		frx3 = &ae_state->ap.wideband_filters_RX3[rx_filter_sel][0] ;
		frx1_ext = &ae_state->ap.wideband_filters_RX1[1][0] ;
		frx2_ext = &ae_state->ap.wideband_filters_RX2[1][0] ;
		frx3_ext = &ae_state->ap.wideband_filters_RX3[1][0] ;
	} else {
		ftx1 = &ae_state->ap.narrowband_filters_TX1[tx_filter_sel][0] ;
		ftx2 = &ae_state->ap.narrowband_filters_TX2[tx_filter_sel][0] ;
		ftx3 = &ae_state->ap.narrowband_filters_TX3[tx_filter_sel][0] ;
		frx1 = &ae_state->ap.narrowband_filters_RX1[rx_filter_sel][0] ;
		frx2 = &ae_state->ap.narrowband_filters_RX2[rx_filter_sel][0] ;
		frx3 = &ae_state->ap.narrowband_filters_RX3[rx_filter_sel][0] ;
		frx1_ext = &ae_state->ap.narrowband_filters_RX1[1][0] ;
		frx2_ext = &ae_state->ap.narrowband_filters_RX2[1][0] ;
		frx3_ext = &ae_state->ap.narrowband_filters_RX3[1][0] ;
	}
	ae_state->updating_filters = 1 ;
	memcpy( filter_tx1, ftx1, FILTER_SIZE << 1 ) ;
	memcpy( filter_tx2, ftx2, FILTER_SIZE << 1 ) ;
	memcpy( filter_tx3, ftx3, FILTER_SIZE << 1 ) ;
	memcpy( filter_rx1, frx1, FILTER_SIZE << 1 ) ;
	memcpy( filter_rx2, frx2, FILTER_SIZE << 1 ) ;
	memcpy( filter_rx3, frx3, FILTER_SIZE << 1 ) ;
	memcpy( filter_rx1_ext, frx1_ext, FILTER_SIZE << 1 ) ;
	memcpy( filter_rx2_ext, frx2_ext, FILTER_SIZE << 1 ) ;
	memcpy( filter_rx3_ext, frx3_ext, FILTER_SIZE << 1 ) ;
	ae_state->updating_filters = 0 ;
#endif
}



#if defined( SC1445x_AE_PCM_LINES_SUPPORT )

/********************/
/* ATA/DECT SUPPORT */
/********************/

#define COMPRESS_XMATRIX_ENTRY( x ) \
	( ( (x) & 0xff00 ) >> ( 8 - SC1445x_AE_AUDIO_STREAMS_COUNT ) ) |  \
					( (x) & 0xff )


static inline
void sc1445x_internal_dump_xmatrix( sc1445x_ae_state* ae_state )
{
#if defined( DUMP_XMATRIX )
	unsigned short i ;

	sc1445x_ae_xmatrix_entry* channels = ae_state->xmatrix.voip_channels ;
	sc1445x_ae_xmatrix_entry* streams = ae_state->xmatrix.audio_streams ;

	/* dump new interconnection matrix */
	PRINT( PRINT_LEVEL "New interconnection matrix:\n" ) ;
	for( i = 0 ;  i < SC1445x_AE_MAX_AUDIO_CHANNELS ;  ++i ) {
		unsigned short b ;

		PRINT( PRINT_LEVEL "  " ) ;
		for( b = 0 ;  b < SC1445x_AE_MAX_AUDIO_CHANNELS ; ++b ) {
			if( channels[i] & XMATRIX_VOIP_CHANNEL_BIT( b ) )
				PRINT( "1 " ) ;
			else
				PRINT( "0 " ) ;
		}
		for( b = 0 ;  b < SC1445x_AE_AUDIO_STREAMS_COUNT ; ++b ) {
			if( channels[i] & XMATRIX_AUDIO_STREAM_BIT( b ) )
				PRINT( "1 " ) ;
			else
				PRINT( "0 " ) ;
		}
		PRINT( "\n" ) ;
	}
	for( i = 0 ;  i < SC1445x_AE_AUDIO_STREAMS_COUNT ;  ++i ) {
		unsigned short b ;

		PRINT( PRINT_LEVEL "  " ) ;
		for( b = 0 ;  b < SC1445x_AE_MAX_AUDIO_CHANNELS ; ++b ) {
			if( streams[i] & XMATRIX_VOIP_CHANNEL_BIT( b ) )
				PRINT( "1 " ) ;
			else
				PRINT( "0 " ) ;
		}
		for( b = 0 ;  b < SC1445x_AE_AUDIO_STREAMS_COUNT ; ++b ) {
			if( streams[i] & XMATRIX_AUDIO_STREAM_BIT( b ) )
				PRINT( "1 " ) ;
			else
				PRINT( "0 " ) ;
		}
		PRINT( "\n" ) ;
	}
#endif
}

/* update the interconnection matrix on the DSPs */
static
void sc1445x_internal_update_interconnection_matrix( sc1445x_ae_state* ae_state )
{
#if !defined( CONFIG_SC14452 )
	unsigned short val ;
#endif
	unsigned short i, l ;
#if defined( CONFIG_SC14452 )
	unsigned short ii ;
	volatile unsigned short* dsp_xmatrix_coeff = (void*)0x11ec0 ;
#endif

	for( i = 0 ;  i < ae_state->audio_channels_count ;  ++i ) {
#if defined( CONFIG_SC14452 )
		for( ii = 0 ;  ii < ae_state->audio_channels_count ;  ++ii ) {
			if( ae_state->xmatrix.voip_channels[i] &
						XMATRIX_VOIP_CHANNEL_BIT( ii ) )
				*dsp_xmatrix_coeff = 0x7fff ;
			else
				*dsp_xmatrix_coeff = 0 ;

			++dsp_xmatrix_coeff ;
		}

		for( ii = 0 ;  ii < SC1445x_AE_AUDIO_STREAMS_COUNT ;  ++ii ) {
			if( ae_state->xmatrix.voip_channels[i] &
						XMATRIX_AUDIO_STREAM_BIT( ii ) )
				*dsp_xmatrix_coeff = 0x7fff ;
			else
				*dsp_xmatrix_coeff = 0 ;

			++dsp_xmatrix_coeff ;
		}
#else
		val = COMPRESS_XMATRIX_ENTRY(
				ae_state->xmatrix.voip_channels[i] ) ;
		val |= i << 8 ;
		sc1445x_internal_send_dsp_cmd( 0x0007, val ) ;
#endif
	}

	for( l = 0 ;  l < SC1445x_AE_AUDIO_STREAMS_COUNT ;  ++l, ++i ) {
#if defined( CONFIG_SC14452 )
		for( ii = 0 ;  ii < ae_state->audio_channels_count ;  ++ii ) {
			if( ae_state->xmatrix.audio_streams[l] &
						XMATRIX_VOIP_CHANNEL_BIT( ii ) )
				*dsp_xmatrix_coeff = 0x7fff ;
			else
				*dsp_xmatrix_coeff = 0 ;

			++dsp_xmatrix_coeff ;
		}

		for( ii = 0 ;  ii < SC1445x_AE_AUDIO_STREAMS_COUNT ;  ++ii ) {
			if( ae_state->xmatrix.audio_streams[l] &
						XMATRIX_AUDIO_STREAM_BIT( ii ) )
				*dsp_xmatrix_coeff = 0x7fff ;
			else
				*dsp_xmatrix_coeff = 0 ;

			++dsp_xmatrix_coeff ;
		}
#else
		val = COMPRESS_XMATRIX_ENTRY(
				ae_state->xmatrix.audio_streams[l] ) ;
		val |= i << 8 ;
		sc1445x_internal_send_dsp_cmd( 0x0007, val ) ;

		val = l << 8 ;
#endif
	}

	sc1445x_internal_dump_xmatrix( ae_state ) ;
}


/* helper function for sc1445x_ae_connect_to_pcm_line() and */
/* sc1445x_ae_connect_pcm_lines() */
static
void massage_xmatrix( sc1445x_ae_state* ae_state,
					sc1445x_ae_xmatrix_entry mask )
{
	sc1445x_ae_xmatrix_entry* channels ;
	sc1445x_ae_xmatrix_entry* streams ;
	sc1445x_ae_xmatrix_entry val ;
	unsigned short i ;

	channels = ae_state->xmatrix.voip_channels ;
	streams = ae_state->xmatrix.audio_streams ;

	val = mask ;

	for( i = 0 ;  i < ae_state->audio_channels_count ;  ++i )
		if( channels[i] & mask )
			val |= channels[i] ;
	for( i = 0 ;  i < SC1445x_AE_AUDIO_STREAMS_COUNT ;  ++i )
		if( streams[i] & mask )
			val |= streams[i] ;

	for( i = 0 ;  i < ae_state->audio_channels_count ;  ++i ) {
		if( channels[i] & mask ) {
			channels[i] = val ;
			xmatrix_dont_listen_to_voip_channel( &channels[i], i ) ;
		}
	}
	for( i = 0 ;  i < SC1445x_AE_AUDIO_STREAMS_COUNT ;  ++i ) {
		if( streams[i] & mask ) {
			streams[i] = val ;
			xmatrix_dont_listen_to_audio_stream( &streams[i], i ) ;
		}
	}
}

/* connect an active audio channel to a line */
short sc1445x_ae_connect_to_line( sc1445x_ae_state* ae_state,
				unsigned short channel, unsigned short line )
{
	unsigned short cmd ;
	sc1445x_ae_xmatrix_entry* channels ;
	sc1445x_ae_xmatrix_entry* streams ;
	sc1445x_ae_xmatrix_entry mask ;
	short s ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( channel, ae_state->audio_channels_count ) ;
	CHECK_ARG_CEIL( line, SC1445x_AE_TOTAL_LINES_COUNT ) ;

	if( !ae_state->audio_channels[channel].is_active ) {
		PRINT( sc1445x_ae_channel_not_active_error, channel ) ;
		return SC1445x_AE_ERR_CHANNEL_NOT_ACTIVE ;
	}

	channels = ae_state->xmatrix.voip_channels ;
	streams = ae_state->xmatrix.audio_streams ;
	s = sc1445x_private_line2stream( ae_state, line, 1 ) ;
	if( s < 0 )
		return s ;

	/* these checks are just for debugging, but the bits must be set */
	if( xmatrix_listen_to_voip_channel( &streams[s], channel ) < 0 ) {
		PRINT( PRINT_LEVEL "line %d was already listening to "
					"audio channel %d\n", line, channel ) ;
	}
	if( xmatrix_listen_to_audio_stream( &channels[channel], s ) < 0 ) {
		PRINT( PRINT_LEVEL "Audio channel %d was already listening to "
					"line %d\n", channel, line ) ;
	}

	/* make all other required interconnections and make sure that */
	/* xmatrix stays symmetric */
	mask = 0 ;
	xmatrix_listen_to_audio_stream( &mask, s ) ;
	xmatrix_listen_to_voip_channel( &mask, channel ) ;
	massage_xmatrix( ae_state, mask ) ;

	sc1445x_internal_update_interconnection_matrix( ae_state ) ;
	cmd = ( s < 3 ) ?  ( 0x0004 + s ) :  0x0026 ;
	sc1445x_internal_send_dsp_cmd( cmd, 0x0011 ) ;

	return SC1445x_AE_OK ;
}

/* connect two lines with each other */
short sc1445x_ae_connect_lines( sc1445x_ae_state* ae_state,
				unsigned short line1, unsigned short line2 )
{
	unsigned short cmd ;
	sc1445x_ae_xmatrix_entry* streams ;
	sc1445x_ae_xmatrix_entry mask ;
	short s1, s2 ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( line1, SC1445x_AE_TOTAL_LINES_COUNT ) ;
	CHECK_ARG_CEIL( line2, SC1445x_AE_TOTAL_LINES_COUNT ) ;

	streams = ae_state->xmatrix.audio_streams ;
	s1 = sc1445x_private_line2stream( ae_state, line1, 1 ) ;
	if( s1 < 0 )
		return s1 ;
	s2 = sc1445x_private_line2stream( ae_state, line2, 1 ) ;
	if( s2 < 0 )
		return s2 ;

	/* these checks are just for debugging, but the bits must be set */
	if( xmatrix_listen_to_audio_stream( &streams[s1], s2 ) < 0 ) {
		PRINT( PRINT_LEVEL "line %d was already listening to "
					"line %d\n", line1, line2 ) ;
	}
	if( line1 != line2  &&
			xmatrix_listen_to_audio_stream( &streams[s2],
								s1 ) < 0 ) {
		PRINT( PRINT_LEVEL "line %d was already listening to "
					"line %d\n", line2, line1 ) ;
	}

	/* make all other required interconnections and make sure that */
	/* xmatrix stays symmetric */
	mask = 0 ;
	xmatrix_listen_to_audio_stream( &mask, s1 ) ;
	xmatrix_listen_to_audio_stream( &mask, s2 ) ;
	massage_xmatrix( ae_state, mask ) ;

	sc1445x_internal_update_interconnection_matrix( ae_state ) ;
	cmd = ( s1 < 3 ) ?  ( 0x0004 + s1 ) :  0x0026 ;
	sc1445x_internal_send_dsp_cmd( cmd, 0x0011 ) ;
	cmd = ( s2 < 3 ) ?  ( 0x0004 + s2 ) :  0x0026 ;
	sc1445x_internal_send_dsp_cmd( cmd, 0x0011 ) ;

	return SC1445x_AE_OK ;
}

/* disconnect a line */
short  sc1445x_ae_disconnect_line( sc1445x_ae_state* ae_state,
							unsigned short line )
{
	sc1445x_ae_xmatrix_entry* channels ;
	sc1445x_ae_xmatrix_entry* streams ;
	sc1445x_ae_xmatrix_entry temp ;
	unsigned short i ;
	short s ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( line, SC1445x_AE_TOTAL_LINES_COUNT ) ;

	channels = ae_state->xmatrix.voip_channels ;
	streams = ae_state->xmatrix.audio_streams ;
	s = sc1445x_private_line2stream( ae_state, line, 0 ) ;
	if( s < 0 )
		return s ;

	temp = streams[s] ;
	for( i = 0 ;  i < ae_state->audio_channels_count ;  ++i ) {
		if( xmatrix_listen_to_voip_channel( &temp, i ) < 0 ) {
			PRINT( PRINT_LEVEL "line %d (s %d) is listening to "
				"audio channel %d -- silently refusing to "
					"disconnect\n", line, s, i ) ;
			return SC1445x_AE_OK ;
		}
	}

	for( i = 0 ;  i < ae_state->audio_channels_count ;  ++i )
		xmatrix_dont_listen_to_audio_stream( &channels[i], s ) ;
	for( i = 0 ;  i < SC1445x_AE_AUDIO_STREAMS_COUNT ;  ++i )
		xmatrix_dont_listen_to_audio_stream( &streams[i], s ) ;
	streams[s] = 0 ;

#if 1
	/* remove line mapping */
	PRINT( PRINT_LEVEL "%s: removing mapping of line %d!\n",
							__FUNCTION__, line ) ;
	ae_state->line_mappings[line] = -1 ;
#endif

	sc1445x_internal_update_interconnection_matrix( ae_state ) ;
	if( ae_state->tonegen[line].status != SC1445x_AE_TONEGEN_PLAYING ) {
		unsigned short cmd = ( s < 3 ) ?  ( 0x0004 + s ) :  0x0026 ;

		sc1445x_internal_send_dsp_cmd( cmd, 0x0000 ) ;

		/* return stream to "on-hook" state (only for ATA) */
		sc1445x_internal_send_dsp_cmd( 0x0013, s << 8 | 0x0003 ) ;
	}

	return SC1445x_AE_OK ;
}


/* set the CID information for a line */
short sc1445x_ae_set_cid_info( sc1445x_ae_state* ae_state, unsigned short line,
				unsigned char month, unsigned char day,
				unsigned char hour, unsigned char minutes,
				const char* number, const char* name )
{
	static unsigned short*
			cid_info_for_stream[SC1445x_AE_AUDIO_STREAMS_COUNT] = {
		(unsigned short*)0x103E8,
		(unsigned short*)0x10614,
		(unsigned short*)0x10C1C
	} ;
	/* aseemble message locally; byte access in DSP memory is troublesome */
	unsigned char cid_info[256] ;
	unsigned char* p = cid_info ;
	unsigned short* buf ;
	unsigned char csum ;
	int i, n ;
	short s ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( line, SC1445x_AE_TOTAL_LINES_COUNT ) ;
	CHECK_ARG_CEIL( month - 1, 12 ) ;
	CHECK_ARG_CEIL( day - 1, 31 ) ;
	CHECK_ARG_CEIL( hour, 24 ) ;
	CHECK_ARG_CEIL( minutes, 60 ) ;
	CHECK_POINTER_ARG( number ) ;
	CHECK_ARG_CEIL( strlen( number ), 81 ) ;
	CHECK_POINTER_ARG( name ) ;
	CHECK_ARG_CEIL( strlen( name ), 101 ) ;

	s = sc1445x_private_line2stream( ae_state, line, 0 ) ;
	if( s < 0 )
		return s ;

	/* assemble CID message */
	*p++ = 128 ;			/* we use MDMF */
	++p ;				/* we'll fill length later */

	*p++ = 1 ;			/* Parameter Type (Date/Time) */
	*p++ = 8 ;			/* Parameter Length */
	*p++ = '0'  +  ( month > 9 ) ;	/* Month */
	*p++ = '0'  +  ( month < 10 ?  month :  month - 10 ) ;
	*p++ = '0'  +  day / 10 ;	/* Day */
	*p++ = '0'  +  day % 10 ;
	*p++ = '0'  +  hour / 10 ;	/* Hour */
	*p++ = '0'  +  hour % 10 ;
	*p++ = '0'  +  minutes / 10 ;	/* Minutes */
	*p++ = '0'  +  minutes % 10 ;

	n = strlen( number ) ;
	if( 0 == n ) {
		/* the number is not available, put P (private) */
		*p++ = 4 ;		/* Parameter Type (Number N/A) */
		*p++ = 1 ;		/* Parameter Length */
		*p++ = 'P' ;
	} else {
		*p++ = 2 ;		/* Parameter Type (Number) */
		*p++ = n ;		/* Parameter Length */
		memcpy( p, number, n ) ;
		p += n ;
	}

	n = strlen( name ) ;
	if( 0 == n ) {
		/* the name is not available, put P (private) */
		*p++ = 8 ;		/* Parameter Type (Number N/A) */
		*p++ = 1 ;		/* Parameter Length */
		*p++ = 'P' ;
	} else {
		*p++ = 7 ;		/* Parameter Type (Number) */
		*p++ = n ;		/* Parameter Length */
		memcpy( p, name, n ) ;
		p += n ;
	}

	/* calculate message length */
	n = p - cid_info ;
	cid_info[1] = n - 2 ;	/* exclude msg type and msg length fields */

	/* calculate checksum */
	for( i = 0, csum = 0 ;  i < n ;  ++i )
		csum += cid_info[i] ;
	*p = (unsigned char)-(char)csum ;	/* 2's complement */

	/* copy message to dsp memory */
	buf = cid_info_for_stream[s] ;
	for( i = 0 ;  i < n + 1 ;  ++i ) {
		*buf++ = cid_info[i] ;
	}

	/* inform dsp */
	sc1445x_internal_send_dsp_cmd( 0x0014, s << 8 | ( n & 0xff ) ) ;

	return SC1445x_AE_OK ;
}

/* pass CID-related indication for first ring event */
short sc1445x_ae_cid_ind_first_ring( sc1445x_ae_state* ae_state,
							unsigned short line )
{
	unsigned short cmd ;
	short s ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( line, SC1445x_AE_TOTAL_LINES_COUNT ) ;

	s = sc1445x_private_line2stream( ae_state, line, 1 ) ;
	if( s < 0 )
		return s ;

	/* open PCM line (in case it was closed) */
	cmd = ( s < 3 ) ?  ( 0x0004 + s ) :  0x0026 ;
	sc1445x_internal_send_dsp_cmd( cmd, 0x0011 ) ;

	/* inform dsp */
	sc1445x_internal_send_dsp_cmd( 0x0013, s << 8 | 0x0001 ) ;

	return SC1445x_AE_OK ;
}

/* pass CID-related indication for first ring event */
short sc1445x_ae_cid_ind_off_hook( sc1445x_ae_state* ae_state,
							unsigned short line )
{
	short s ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( line, SC1445x_AE_TOTAL_LINES_COUNT ) ;

	s = sc1445x_private_line2stream( ae_state, line, 1 ) ;
	if( s < 0 )
		return s ;

	/* inform dsp */
	sc1445x_internal_send_dsp_cmd( 0x0013, s << 8 | 0x0002 ) ;

	return SC1445x_AE_OK ;
}

/* pass CID-related indication for first ring event */
short sc1445x_ae_cid_ind_on_hook( sc1445x_ae_state* ae_state,
							unsigned short line )
{
	short s ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( line, SC1445x_AE_TOTAL_LINES_COUNT ) ;

	s = sc1445x_private_line2stream( ae_state, line, 0 ) ;
	if( s < 0 )
		return s ;

	/* inform dsp */
	sc1445x_internal_send_dsp_cmd( 0x0013, s << 8 | 0x0003 ) ;

	return SC1445x_AE_OK ;
}

/* pass CID-related indication for line reversal event */
short sc1445x_ae_cid_line_reversal( sc1445x_ae_state* ae_state,
							unsigned short line )
{
#  if defined( SC1445x_AE_ATA_SUPPORT )
	short s ;
#  endif

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( line, SC1445x_AE_TOTAL_LINES_COUNT ) ;

#  if defined( SC1445x_AE_ATA_SUPPORT )
	s = sc1445x_private_line2stream( ae_state, line, 0 ) ;
	if( s < 0 )
		return s ;

	sc1445x_internal_start_fsk_decoder( ae_state, s, 0 ) ;
#  endif

	return SC1445x_AE_OK ;
}

/* enable DTMF pass-through */
short sc1445x_ae_enable_dtmf_passthrough( sc1445x_ae_state* ae_state,
						unsigned short line )
{
#  if defined( SC1445x_AE_ATA_SUPPORT )
	short s ;
#  endif

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( line, SC1445x_AE_TOTAL_LINES_COUNT ) ;

#  if defined( SC1445x_AE_ATA_SUPPORT )
	s = sc1445x_private_line2stream( ae_state, line, 0 ) ;
	if( s < 0 )
		return s ;

	sc1445x_internal_send_dsp_cmd( 0x002b, (s << 4) | 1 ) ;
#  endif

	return SC1445x_AE_OK ;
}

/* disable DTMF pass-through */
short sc1445x_ae_disable_dtmf_passthrough( sc1445x_ae_state* ae_state,
						unsigned short line )
{
#  if defined( SC1445x_AE_ATA_SUPPORT )
	short s ;
#  endif

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( line, SC1445x_AE_TOTAL_LINES_COUNT ) ;

#  if defined( SC1445x_AE_ATA_SUPPORT )
	s = sc1445x_private_line2stream( ae_state, line, 0 ) ;
	if( s < 0 )
		return s ;

	sc1445x_internal_send_dsp_cmd( 0x002b, (s << 4) | 0 ) ;
#  endif

	return SC1445x_AE_OK ;
}

/* enable detection of specific tones */
short sc1445x_ae_enable_tone_detection( sc1445x_ae_state* ae_state,
					unsigned short line, int tones )
{
#  if defined( SC1445x_AE_ATA_SUPPORT )
	short s ;
#  endif

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( line, SC1445x_AE_TOTAL_LINES_COUNT ) ;

#  if defined( SC1445x_AE_ATA_SUPPORT )
	s = sc1445x_private_line2stream( ae_state, line, 0 ) ;
	if( s < 0 )
		return s ;

	if( tones & SC1445x_TONE_DET_STUTTER_DIAL ) {
		sc1445x_internal_send_dsp_cmd( 0x002c, (s << 4) | 1 ) ;
	}
#  endif
	
	return SC1445x_AE_OK ;
}

/* disable detection of specific tones */
short sc1445x_ae_disable_tone_detection( sc1445x_ae_state* ae_state,
					unsigned short line, int tones )
{
#  if defined( SC1445x_AE_ATA_SUPPORT )
	short s ;
#  endif

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( line, SC1445x_AE_TOTAL_LINES_COUNT ) ;

#  if defined( SC1445x_AE_ATA_SUPPORT )
	s = sc1445x_private_line2stream( ae_state, line, 0 ) ;
	if( s < 0 )
		return s ;

	if( tones & SC1445x_TONE_DET_STUTTER_DIAL ) {
		sc1445x_internal_send_dsp_cmd( 0x002c, (s << 4) | 0 ) ;
	}
#  endif

	return SC1445x_AE_OK ;
}

#endif  /* SC1445x_AE_PCM_LINES_SUPPORT */


#if defined( SC1445x_AE_PCM_LINES_SUPPORT ) || defined( SC1445x_AE_PHONE_DECT )

/* set the type of a line (ATA/CVM DECT/native DECT -- narrow/wide)
 * this API is also provided in phone+DECT_headset configuration, only
 * to set up if we are using a narrowband or wideband DECT headset
 */
short sc1445x_ae_set_line_type( sc1445x_ae_state* ae_state, unsigned short line,
						sc1445x_ae_line_type type )
{
	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	CHECK_ARG_CEIL( line, SC1445x_AE_TOTAL_LINES_COUNT ) ;
#else
	CHECK_ARG_CEIL( line, 1 ) ;
#endif
	CHECK_ARG_CEIL( type, SC1445x_AE_LINE_TYPE_INVALID ) ;

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	ae_state->line_types[line] = type ;
	if( SC1445x_AE_LINE_TYPE_ATA == type ||
					SC1445x_AE_LINE_TYPE_FXO == type ) {
		short s = sc1445x_private_line2stream( ae_state, line, 1 ) ;

		if( s < 0 )
			PRINT( PRINT_LEVEL "%s: cannot activate ATA/FXO line\n",
								__FUNCTION__ ) ;
		else {
			if( SC1445x_AE_LINE_TYPE_FXO == type ) {
				/*
				 * activate playback path
				 * to let the detectors work
				 */
				unsigned short cmd = ( s < 3 ) ?
						( 0x0004 + s ) :  0x0026 ;

				sc1445x_internal_send_dsp_cmd( cmd, 0x0001 ) ;
			}

			ae_state->line2pcm_mappings[line] = s ;
			PRINT( PRINT_LEVEL "%s: line %d mapped to PCM slot "
					"%d\n", __FUNCTION__, line, s ) ;
		}
	}
#else
	if( type != SC1445x_AE_LINE_TYPE_NATIVE_DECT_NARROW  &&
			type != SC1445x_AE_LINE_TYPE_NATIVE_DECT_WIDE ) {
		PRINT( PRINT_LEVEL "%s: line type %d is not supported in this "
				"configuration\n", __FUNCTION__, type ) ;
		return SC1445x_AE_ERR_ARG_OUT_OF_RANGE ;
	}

	ae_state->dect_headset_type = type ;
#endif

	return SC1445x_AE_OK ;
}

/* get the type of a line (ATA/CVM DECT/native DECT -- narrow/wide) */
short sc1445x_ae_get_line_type( sc1445x_ae_state* ae_state, unsigned short line,
						sc1445x_ae_line_type* type )
{
	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	CHECK_ARG_CEIL( line, SC1445x_AE_TOTAL_LINES_COUNT ) ;
#else
	CHECK_ARG_CEIL( line, 1 ) ;
#endif
	CHECK_POINTER_ARG( type ) ;

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	*type = ae_state->line_types[line] ;
#else
	*type = ae_state->dect_headset_type ;
#endif

	return SC1445x_AE_OK ;
}


#  if defined( SC1445x_AE_ATA_SUPPORT )

static void sc1445x_internal_init_cid_structs( sc1445x_ae_cid_state* cid )
{
	memset( cid->buf, 0, CID_BUF_LEN ) ;
	cid->buf_pos = 0 ;
	cid->csum = 0 ;
	cid->msg_len = 0 ;
	cid->muted_on = 0 ;
}

/* CID helper functions (called from ISR) */
extern void sc1445x_private_fill_cid_buffer( const unsigned char* buf,
							unsigned short len ) ;
static void sc1445x_internal_init_cid( unsigned long arg )
{
	sc1445x_ae_cid_state* cid = (void*)arg ;
	sc1445x_ae_state* ae_state = cid->my_ae_state ;
	unsigned short stream = cid - ae_state->caller_id ;
//#  define USE_ETSI
	volatile unsigned short* dsp1_mem = (void*)0x10000 ;
	volatile unsigned short* p ;
	short i ;
	unsigned short CALLER_ID_RESERVED_SPACE_addr = 0x1816 ;
	static const unsigned short CALLER_ID_RESERVED_SPACE_data[96+6+24+39+4*13] = {
		//FSKDECOD//FIR FILTER//CASDET2
		//(CASDET2 WILL NOT BE ACTIVE CONCURRENTLY WITH FSKDECODER PROBABLY SO SAVE RESOURCES)
		0x1816+96+3, 0x0200, 0x0e00, 0x7fff, 0x2000, 0x2000, 0x000d, 0x001e, 0xff5c, 0x0052,
		0x004f, 0x7fff, 0x0100, 0x0100, 0x3333, 0x2666, 0x04b0, 0x0190, 0x0320, 0x0018,
		0x0041, 0x0023, 0x7fff, 0x0018, 0x0004, 
#    ifdef USE_ETSI
		//ETSI
		/* MARK */
		0x0DA4, 0x0E42, 0x0141, 0xF30E,
		0xF138, 0xFD7F, 0x0C2B, 0x0F38,
		0x03BC, 0xF4B0, 0xF071, 0xFB0E,
		0x0A64, 0x0FCE, 0x061F, 0xF698,
		/* SPACE */
		0x0FF3, 0xFD7F, 0xF071, 0x04F2,
		0x0EC8, 0xF8BC, 0xF25C, 0x0968,
		0x0C2B, 0xF4B0, 0xF59C, 0x0CF2,
		0x085C, 0xF1BE, 0xF9E1, 0x0F38,
#    else		
		//BELLCORE
		/* MARK */
		0x0CF2, 0x0F38, 0x04F2, 0xF698,
		0xF000, 0xF698, 0x04F2, 0x0F38,
		0x0CF2, 0x0000, 0xF30E, 0xF0C8,
		0xFB0E, 0x0968, 0x1000, 0x0968,
		/* SPACE */
		0x0FCE, 0xFB0E, 0xF1BE, 0x0968,
		0x0B50, 0xF30E, 0xF8BC, 0x0F38,
		0x0281, 0xF000, 0x0281, 0x0F38,
		0xF8BC, 0xF30E, 0x0B50, 0x0968,
#    endif
		0,0,0,0,//60
		0,0,0,0,0,0,0,0,0x7fff,0,//70
		0,0,0,0,0,0,0,0,0,0,//80
		0,0,0,0,0,0,0,0,0,0,//90
		0,0,0,0,0,//96
		0x169f, 0x1816+102, 0x16BE, 0, 24, 0, //PFIR PARAMETERS //102 0x169f is the output of dscrtgain,0x1816+102 is the pointer to the delay line,0x16BE is the location of fsk_prefilter_coefs
		0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,//126

		0x1816+126+39+25,0x1816+126+39+26+25,0x7F57,0xF2F4,0x6A6F,0xB8E2,0x0555,0x0800,0x0800,0x2000,0x2000,//CASDET2  0=output of biq low, 1=output of biquad high
		0x00AB,0x3333,0x3999,0x01E0,0x0400,0x0080,0x0046,0x2B05,0x8000,0x0000,
		0x8000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,
		0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,//164
		0x0000,0x2000,0x4000,0x2000,0xF645,0xE7C7,0x0EB1,0x8000,0,0,0,0,0,//BIQUAD LOW  1 FOR CASDET2
		0x1816+126+39+12,0x2000,0x4000,0x2000,0x1145,0xF766,0x1746,0xA000,0,0,0,0,0,//BIQUAD LOW  2 FOR CASDET2
		0x0000,0x2000,0xC000,0x2000,0xE347,0xE6EE,0x0651 ,0x8000,0,0,0,0,0,//BIQUAD HIGH 1 FOR CASDET2
		0x1816+126+39+26+12,0x2000,0xC000,0x2000,0xD8E5,0xF0D2,0x0813,0xA000,0,0,0,0,0 //BIQUAD HIGH 2 FOR CASDET2
	} ;
	unsigned short n = ae_state->audio_channels_count ;
	sc1445x_ae_xmatrix_entry* channels ;
	sc1445x_ae_xmatrix_entry* streams ;
	DPRINT( "%s: cid timer expired\n", __FUNCTION__) ;
	if( cid->buf_pos > 0  &&  cid->buf_pos < ( 3 + cid->msg_len ) ) {
		/* we are receiving a message */
		DPRINT( "%s: ignoring 3.6-sec timer because we are in the middle "
				"of receiving a message\n", __FUNCTION__ ) ;

		/*
		 * expiration of the char timer will cause this function
		 * to be called again and turn off FSK decoder
		 */
		cid->do_disable_fdk_decoder = 1 ;
		return ;
	}

	/* unmute stream */
	DPRINT( "%s: muted on %04x\n", __FUNCTION__, cid->muted_on ) ;
	channels = ae_state->xmatrix.voip_channels ;
	streams = ae_state->xmatrix.audio_streams ;
	for( i = 0 ;  i < n ;  ++i ) {
		if( cid->muted_on & ( 1 << i ) ) {
			xmatrix_listen_to_audio_stream( &channels[i], stream ) ;
		}
	}
	for( i = 0 ;  i < SC1445x_AE_AUDIO_STREAMS_COUNT ;  ++i ) {
		if( cid->muted_on & ( 1 << ( i + n ) ) ) {
			xmatrix_listen_to_audio_stream( &streams[i], stream ) ;
		}
	}
	sc1445x_internal_update_interconnection_matrix( ae_state ) ;
	sc1445x_internal_dump_xmatrix( ae_state ) ;

	/* deactivate FSK decoder */
	sc1445x_internal_send_dsp_cmd( 0x002a, (stream << 4 ) | 0 ) ;

	/* initialize FSK decoder structs */
	for( p = dsp1_mem + CALLER_ID_RESERVED_SPACE_addr, i = 0 ;
			i < ARRAY_SIZE( CALLER_ID_RESERVED_SPACE_data ) ;  ++i )
		p[i] = CALLER_ID_RESERVED_SPACE_data[i] ;

	sc1445x_internal_init_cid_structs( cid ) ;
	cid->do_disable_fdk_decoder = 0 ;
}


static void sc1445x_cid_char_timeout( unsigned long arg )
{
	sc1445x_ae_cid_state* cid = (void*)arg ;
	sc1445x_ae_state* ae_state = cid->my_ae_state ;
	unsigned short stream = cid - ae_state->caller_id ;

	PRINT( "%s: timeout while waiting FSK character on stream %d\n",
							__FUNCTION__, stream ) ;

	if( cid->do_disable_fdk_decoder ) {
		/* full reset */
		sc1445x_internal_init_cid_structs( cid ) ;
		sc1445x_internal_init_cid( (unsigned long)cid ) ;
	} else {
		/* just re-init cid struct */
		sc1445x_internal_init_cid_structs( cid ) ;
	}
}

static void sc1445x_internal_activate_fsk_decoder_wq( void* arg )
{
	sc1445x_ae_cid_state* cid = arg ;
	sc1445x_ae_state* ae_state = cid->my_ae_state ;
	unsigned short stream = cid - ae_state->caller_id ;

	PRINT( "%s: activating FSK decoder for stream %d\n", __FUNCTION__,
								stream ) ;

	/* activate FSK decoder */
	sc1445x_internal_send_dsp_cmd( 0x002a, (stream << 4 ) | 1 ) ;

	if( 1 || !timer_pending( &cid->fsk_init_timer ) ) {
		/*
		 * re-init FSK decoder when a 3.9-sec timeout expires
		 */
#define FSK_INIT_TIMEOUT		(3900 * HZ / 1000)
		cid->fsk_init_timer.data = (unsigned long)cid ;
		cid->fsk_init_timer.function = sc1445x_internal_init_cid ;
		mod_timer( &cid->fsk_init_timer, jiffies + FSK_INIT_TIMEOUT  ) ;

		PRINT( "%s: setting timer to deactivate FSK decoder at %lu\n",
				__FUNCTION__, cid->fsk_init_timer.expires ) ;

		/*
		 * prepare the char timer
		 */
		cid->fsk_char_timer.data = (unsigned long)cid ;
		cid->fsk_char_timer.function = sc1445x_cid_char_timeout ;
	}
}

static void sc1445x_internal_start_fsk_decoder( sc1445x_ae_state* ae_state,
							unsigned short stream, short send_cid_type2_ack )
{
	sc1445x_ae_cid_state* cid = ae_state->caller_id + stream ;
	unsigned short i, n = ae_state->audio_channels_count ;
	sc1445x_ae_xmatrix_entry* channels ;
	sc1445x_ae_xmatrix_entry* streams ;

	/* mute stream */
	channels = ae_state->xmatrix.voip_channels ;
	streams = ae_state->xmatrix.audio_streams ;
	for( i = 0 ;  i < n ;  ++i ) {
		if( channels[i] & XMATRIX_AUDIO_STREAM_BIT( stream ) ) {
			cid->muted_on |= 1 << i ;
			xmatrix_dont_listen_to_audio_stream( &channels[i],
								stream ) ;
		}
	}
	for( i = 0 ;  i < SC1445x_AE_AUDIO_STREAMS_COUNT ;  ++i ) {
		if( streams[i] & XMATRIX_AUDIO_STREAM_BIT( stream ) ) {
			cid->muted_on |= 1 << ( i + n ) ;
			xmatrix_dont_listen_to_audio_stream( &streams[i],
								stream ) ;
		}
	}
	sc1445x_internal_update_interconnection_matrix( ae_state ) ;
	sc1445x_internal_dump_xmatrix( ae_state ) ;
	if( send_cid_type2_ack ) {
		/* play DTMF D */
		sc1445x_ae_play_standard_tone( ae_state, stream,
						SC1445x_AE_STD_TONE_D ) ;

		/* activate the FSK decoder after the tone has finished (in 200 msec) */
		schedule_delayed_work( &ae_state->
			caller_id[stream].activate_fsk_decoder_work, 20 ) ;
	} else {
		schedule_work( &ae_state->
			caller_id[stream].activate_fsk_decoder_work ) ;
	}

}

static void sc1445x_internal_cid_read_fsk_char( sc1445x_ae_state* ae_state,
							unsigned short stream )
{
	volatile unsigned short* dsp1_fsk_char = (void*)0x12D60 ;
	sc1445x_ae_cid_state* cid = ae_state->caller_id + stream ;
	unsigned char fsk_char ;

	fsk_char = *dsp1_fsk_char ;

	PRINT( PRINT_LEVEL "%s: detected %02X ('%c')\n", __FUNCTION__, fsk_char,
		( fsk_char > 32  &&  fsk_char < 127 ) ?  fsk_char :  '.' ) ;

	if( likely( cid->buf_pos < CID_BUF_LEN ) ) {
		if( 1 == cid->buf_pos )
			cid->msg_len = fsk_char ;
		cid->buf[cid->buf_pos++] = fsk_char ;
		cid->csum += fsk_char ;
	} else {
		PRINT( PRINT_LEVEL "%s: CID buffer is full!\n", __FUNCTION__ ) ;
	}

	if( cid->buf_pos == ( 3 + cid->msg_len ) ) {
		/* we just read the checksum byte */
		if( 0 == ( cid->csum & 0xff ) ) {
			PRINT( PRINT_LEVEL "%s: end of CID -- checksum OK\n",
								__FUNCTION__ ) ;

			if( timer_pending( &cid->fsk_char_timer ) )
				del_timer( &cid->fsk_char_timer ) ;
			/* reschedule init timer to fire in 50msec */
			mod_timer( &cid->fsk_init_timer,
						jiffies + 50 * HZ / 1000 ) ;

			/* fill CID buffer only when the checksum is OK */
			sc1445x_private_fill_cid_buffer( cid->buf,
								cid->buf_pos ) ;

			return ;
		} else {
			PRINT( PRINT_LEVEL "%s: CID checksum FAIL\n",
								__FUNCTION__ ) ;
			sc1445x_internal_init_cid_structs( cid ) ;
		}
	}

	/* reschedule the char timer to fire in 20msec */
	mod_timer( &cid->fsk_char_timer, jiffies + 20 * HZ / 1000 ) ;
}

#  endif  /* SC1445x_AE_ATA_SUPPORT */


#  if !defined( SC1445x_AE_PHONE_DECT )
/*
 * DSP1 ADDRESSES, IF THESE CHANGE IN DSP1 MAPPING THEN
 * THIS SECTION SHOULD BE ALSO UPDATED
 */
 
#define DECT_CHANNEL0 0x1436
#define DECT_CHANNEL1 0x151A
#define DECT_CHANNEL2 0x15D3
#define DECT_CHANNEL3 0x173F

static void sc1445x_internal_init_wideband_dect_structure(
				unsigned short local_stream_physical_stream )
{
	unsigned short physical_stream;
	const unsigned short dsp_dect_enc_structs[4] = {
		DECT_CHANNEL0,
		DECT_CHANNEL1,
		DECT_CHANNEL2,
		DECT_CHANNEL3
	} ;
	const unsigned short dsp_dect_dec_structs[4] = {
		DECT_CHANNEL0+93, 
		DECT_CHANNEL1+93,
		DECT_CHANNEL2+93,
		DECT_CHANNEL3+93
	} ;

#define WIDE_DECT_UNR_ENC_DATALENGTH 8
	const unsigned short
		wide_dect_unr_enc_addr[WIDE_DECT_UNR_ENC_DATALENGTH] = {
			0x55, 0x56, 0x57, 0x58,
			0x59, 0x5a, 0x5b, 0x5c
		} ;	
	const unsigned short
		wide_dect_unr_enc_data[WIDE_DECT_UNR_ENC_DATALENGTH] = {
			0x82F0, 0x830D, 0x834D, 0x835D,
			0x8361, 0x8371, 0x8375, 0x8395
		} ;

#define WIDE_DECT_UNR_DEC_DATALENGTH 7
	const unsigned short
		wide_dect_unr_dec_addr[WIDE_DECT_UNR_DEC_DATALENGTH] = {
			0x55, 0x56, 0x57, 0x58,
			0x59, 0x5a, 0x5b
		} ;
	const unsigned short
		wide_dect_unr_dec_data[WIDE_DECT_UNR_DEC_DATALENGTH] = {
			0x834D, 0x835D, 0x8361, 0x8371,
			0x8375, 0x83A1, 0x8395
		} ;

	physical_stream = local_stream_physical_stream & 0x0003 ;
	
	PRINT( PRINT_LEVEL "initializing wide channel %d\n", physical_stream ) ;
	sc1445x_internal_clear_dsp1_dm(
				dsp_dect_enc_structs[physical_stream], 93+92 ) ;
	
	sc1445x_internal_copy2dsp1_channel_indep(
					dsp_dect_enc_structs[physical_stream],
					wide_dect_unr_enc_addr,
					wide_dect_unr_enc_data,
					WIDE_DECT_UNR_ENC_DATALENGTH ) ;
					
	sc1445x_internal_copy2dsp1_channel_indep(
					dsp_dect_dec_structs[physical_stream],
					wide_dect_unr_dec_addr,
					wide_dect_unr_dec_data,
					WIDE_DECT_UNR_DEC_DATALENGTH ) ;				
}

static void sc1445x_internal_init_narrowband_dect_structure(
				const sc1445x_ae_state* ae_state,
				unsigned short local_stream_physical_stream )
{
	unsigned short physical_stream, local_stream, struct_to_assign ;
	const unsigned short dsp_dect_enc_structs[4] = {
		DECT_CHANNEL0,
		DECT_CHANNEL1,
		DECT_CHANNEL2,
		DECT_CHANNEL3
	} ;
	const unsigned short dsp_dect_dec_structs[4] = {
		DECT_CHANNEL0+93, 
		DECT_CHANNEL1+93,
		DECT_CHANNEL2+93,
		DECT_CHANNEL3+93
	} ;

#define NARROW_DECT_UNR_ENC_DATALENGTH 15
	const unsigned short
	narrow_dect_unr_enc_addr[NARROW_DECT_UNR_ENC_DATALENGTH] = {
		0x1, 0x3, 0x6, 0x7, 0x8, 0x9, 0xa, 0xb,
		0x15, 0x17, 0x1b, 0x1c,//g726 encoder
  		0x1f, 0x21, 0x22 //buf_pack
	} ;
	const unsigned short
	narrow_dect_unr_enc_data[NARROW_DECT_UNR_ENC_DATALENGTH] = {
		0x0002, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020,
		0x0220, 0x0220, 0x0020, 0x82C0,
		0x0004, 0x0008, 0x6000
	} ;

#define NARROW_DECT_UNR_DEC_DATALENGTH 15
	const unsigned short
	narrow_dect_unr_dec_addr[NARROW_DECT_UNR_DEC_DATALENGTH] = {
		0x1, 0x3, 0x6, 0x7, 0x8, 0x9, 0xa, 0xb,
		0x15, 0x17, 0x1b, 0x1c,//g726 decoder
  		0x1d, 0x1f, 0x20 //buf_unpack
	};
	const unsigned short
	narrow_dect_unr_dec_data[NARROW_DECT_UNR_DEC_DATALENGTH] = {
		0x8002, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020,
		0x0220, 0x0220, 0x0020, 0x82C0,
		0x0004, 0x0008, 0x6000
	} ;					

	local_stream	= (local_stream_physical_stream & 0x0300) >> 8 ;
	physical_stream = local_stream_physical_stream & 0x0003 ;

	if( ae_state->mux_audio_streams_to_pcm_3 ) {
		/*
		 * if this mode is enabled then each audio stream is assigned
		 * with a g726 structure so the MSByte of val2will be taken
		 * into account and not the LSByte.
		 */
		struct_to_assign = local_stream ;
	} else
		struct_to_assign = physical_stream ;

	PRINT( PRINT_LEVEL "initializing narrow struct %d\n",
							struct_to_assign ) ;
	sc1445x_internal_clear_dsp1_dm( dsp_dect_enc_structs[struct_to_assign],
								93+92 ) ;

	sc1445x_internal_copy2dsp1_channel_indep(
					dsp_dect_enc_structs[struct_to_assign],
					narrow_dect_unr_enc_addr,
					narrow_dect_unr_enc_data,
					NARROW_DECT_UNR_ENC_DATALENGTH ) ;

	sc1445x_internal_copy2dsp1_channel_indep(
					dsp_dect_dec_structs[struct_to_assign],
					narrow_dect_unr_dec_addr,
					narrow_dect_unr_dec_data,
					NARROW_DECT_UNR_DEC_DATALENGTH ) ;				
}
#  endif  /* !SC1445x_AE_PHONE_DECT */

#endif



/**********************/
/* PCM DEVICE SUPPORT */
/**********************/

#if defined( SC1445x_AE_BT )

static void sc1445x_ae_internal_reattach_to_pcm( sc1445x_ae_state* ae_state,
		unsigned short pcm_dev_id )
{
	/* this is an internal function, we don't check args */

	sc1445x_ae_pcm_local_endpoint lep = ae_state->pcm_dev[pcm_dev_id].lep ;
	struct __CODEC_LSR_REG* codec_lsr_reg = (void*)&CODEC_LSR_REG ;
	struct __CODEC_MIC_REG* codec_mic_reg = (void*)&CODEC_MIC_REG ;

	ae_state->op_mode.bits.pcm_x_codec_classd = 1 ;
	sc1445x_internal_enforce_phone_op_mode( ae_state ) ;

	if( SC1445x_AE_PCM_LOCAL_EP_HANDSET == lep ) {
		/* turn on the handset mic */
#  if defined( CONFIG_SC14450 )
		codec_mic_reg->BITFLD_MICH_ON = 0 ;
#  elif defined( CONFIG_SC14452 )
		codec_mic_reg->BITFLD_MICHN_ON = 0 ;
		codec_mic_reg->BITFLD_MICHP_ON = 0 ;
#  endif
		codec_mic_reg->BITFLD_MIC_MODE = 0 ;
		wmb() ;
		codec_mic_reg->BITFLD_MIC_PD = 0 ;

		/* turn on the handset spk */
		codec_lsr_reg->BITFLD_LSREN_SE = 0 ;
		codec_lsr_reg->BITFLD_LSRN_MODE = 2 ;
		codec_lsr_reg->BITFLD_LSRP_MODE = 2 ;
		codec_lsr_reg->BITFLD_LSRN_PD = 0 ;
		codec_lsr_reg->BITFLD_LSRP_PD = 0 ;

		sc1445x_internal_enable_output_to_hw_codec( ae_state, 1 ) ;

		/* turn off the external spk */
		CLASSD_CTRL_REG = ae_state->classd_ctrl_val[0] ;
		sc1445x_internal_enable_output_to_classd_amp( ae_state, 0 ) ;
	} else if( SC1445x_AE_PCM_LOCAL_EP_HANDS_FREE == lep ) {
		/* turn on the external/headset mic */
#  if defined( CONFIG_SC14450 )
		codec_mic_reg->BITFLD_MIC_MODE = 2 ;
		codec_mic_reg->BITFLD_MICH_ON = 1 ;
#  elif defined( CONFIG_SC14452 )
		//gflamis
		codec_mic_reg->BITFLD_MIC_MODE = 0 ;
		codec_mic_reg->BITFLD_MICHN_ON = 1 ;
		codec_mic_reg->BITFLD_MICHP_ON = 1 ;
#  endif
		wmb() ;
		codec_mic_reg->BITFLD_MIC_PD = 0 ;

		codec_lsr_reg->BITFLD_LSRN_PD = 1 ;
		codec_lsr_reg->BITFLD_LSRP_PD = 1 ;

		sc1445x_internal_enable_output_to_hw_codec( ae_state, 0 ) ;

		/* turn on the external spk */
		CLASSD_CTRL_REG = ae_state->classd_ctrl_val[1] ;
		sc1445x_internal_enable_output_to_classd_amp( ae_state, 1 ) ;
	} else if( SC1445x_AE_PCM_LOCAL_EP_OPEN_LISTENING == lep ) {
		/* turn on the handset mic */
#  if defined( CONFIG_SC14450 )
		codec_mic_reg->BITFLD_MICH_ON = 0 ;
#  elif defined( CONFIG_SC14452 )
		codec_mic_reg->BITFLD_MICHN_ON = 0 ;
		codec_mic_reg->BITFLD_MICHP_ON = 0 ;
#  endif
		codec_mic_reg->BITFLD_MIC_MODE = 0 ;
		wmb() ;
		codec_mic_reg->BITFLD_MIC_PD = 0 ;

		/* turn on the handset spk */
		codec_lsr_reg->BITFLD_LSREN_SE = 0 ;
		codec_lsr_reg->BITFLD_LSRN_MODE = 2 ;
		codec_lsr_reg->BITFLD_LSRP_MODE = 2 ;
		codec_lsr_reg->BITFLD_LSRN_PD = 0 ;
		codec_lsr_reg->BITFLD_LSRP_PD = 0 ;

		sc1445x_internal_enable_output_to_hw_codec( ae_state, 1 ) ;

		/* turn on the external spk */
		CLASSD_CTRL_REG = ae_state->classd_ctrl_val[1] ;
		sc1445x_internal_enable_output_to_classd_amp( ae_state, 1 ) ;
	} else
		PRINT( PRINT_LEVEL "%s: local endpoint %d not supported!\n",
							__FUNCTION__, lep ) ;

	/* re-adjust vmic gain */
	do_sc1445x_ae_set_vmic_gain( ae_state ) ;

	/* re-adjust vspk volume */
	do_sc1445x_ae_set_vspk_volume( ae_state ) ;
}

/* directly attach to a PCM device (no VoIP channels involved) */
short sc1445x_ae_attach_to_pcm( sc1445x_ae_state* ae_state,
		sc1445x_ae_pcm_slot_id slot, sc1445x_ae_pcm_local_endpoint lep,
		sc1445x_ae_pcm_freq freq, sc1445x_ae_pcm_sample_width width )
{
	short i, avail = -1 ;
	unsigned short val ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( slot, SC1445x_AE_PCM_SLOT_ID_INVALID ) ;
	CHECK_ARG_CEIL( lep, SC1445x_AE_PCM_LOCAL_EP_INVALID ) ;
	CHECK_ARG_CEIL( freq, SC1445x_AE_PCM_FREQ_INVALID ) ;
	CHECK_ARG_CEIL( width, SC1445x_AE_PCM_SAMPLE_WIDTH_INVALID ) ;

	if( ae_state->op_mode.bits.voip_x_codec_classd ) {
		PRINT( sc1445x_ae_pcm_dev_attach_codec_busy_error,
						       	__FUNCTION__ ) ;
		return SC1445x_AE_ERR_PCM_DEV_ATTACH_CODEC_BUSY ;
	}

	/* find an enrty to use */
	for( i = 0 ;  i < SC1445x_AE_PCM_DEV_COUNT ;  ++i ) {
		if( !ae_state->pcm_dev[i].attached ) {
			if( -1 == avail )
				avail = i ;
		} else if( ae_state->pcm_dev[i].slot == slot ) {
			avail = i ;
		}
	}
	if( -1 == avail ) {
		PRINT( sc1445x_ae_pcm_dev_attach_impossible_error,
						       	__FUNCTION__ ) ;
		return SC1445x_AE_ERR_PCM_DEV_ATTACH_IMPOSSIBLE ;
	}

	ae_state->pcm_dev[avail].slot = slot ;
	ae_state->pcm_dev[avail].lep = lep ;
	ae_state->pcm_dev[avail].freq = freq ;
	ae_state->pcm_dev[avail].width = width ;
	ae_state->pcm_dev[avail].attached = 1 ;

	switch( width ) {
		case SC1445x_AE_PCM_SAMPLE_WIDTH_16BITS:
			val = 0 ;
			break ;

		case SC1445x_AE_PCM_SAMPLE_WIDTH_8BITS_ALAW:
			val = 0x101 ;
			break ;

		case SC1445x_AE_PCM_SAMPLE_WIDTH_8BITS_ULAW:
			val = 0x001 ;
			break ;

		default:
			val = 0 ;
			PRINT( "%s: unknown width code %d\n",
							__FUNCTION__, width ) ;
	}

	sc1445x_internal_send_dsp_cmd( 0x0037, val ) ;

	sc1445x_ae_internal_reattach_to_pcm( ae_state, avail ) ;

	return SC1445x_AE_OK ;
}

/* tear a direct attachment to a PCM device */
short sc1445x_ae_detach_from_pcm( sc1445x_ae_state* ae_state,
						sc1445x_ae_pcm_slot_id slot )
{
	short i ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( slot, SC1445x_AE_PCM_SLOT_ID_INVALID ) ;

	for( i = 0 ;  i < SC1445x_AE_PCM_DEV_COUNT ;  ++i ) {
		if( ae_state->pcm_dev[i].attached  &&
					ae_state->pcm_dev[i].slot == slot )
			break ;
	}
	if( SC1445x_AE_PCM_DEV_COUNT == i )
		/* silently accept that we weren't attached to this PCM dev */
		return SC1445x_AE_OK ;

	ae_state->pcm_dev[i].attached = 0 ;

	ae_state->op_mode.bits.pcm_x_codec_classd = 0 ;
	sc1445x_internal_enforce_phone_op_mode( ae_state ) ;

	sc1445x_internal_send_dsp_cmd( 0x0037, 0 ) ;

	return SC1445x_AE_OK ;
}

#endif



#if defined( SC1445x_AE_SUPPORT_FAX )

/***************/
/* FAX SUPPORT */
/***************/

/* switch an active audio channel to fax */
short sc1445x_ae_switch_to_fax( sc1445x_ae_state* ae_state,
						unsigned short channel )
{
	const unsigned short encoder_buff_base_addr[3] = { 0, 0x7e0, 0xfc0 } ;
	const unsigned short decoder_buffin_base_addr[3] = 
						{ 0x444, 0xc24, 0x1404 } ;
	static volatile unsigned short* dsp_hot_dl_sel_addr = (void*)0x1af40 ;
	static volatile unsigned short* my_fmdp_handle_p = (void*)0x1b032 ;
	static volatile unsigned short* more_fax_vars_p = (void*)0x1b02e ;
	unsigned short* mem;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( channel, ae_state->audio_channels_count ) ;

	if( !ae_state->audio_channels[channel].is_active ) {
		PRINT( sc1445x_ae_channel_not_active_error, channel ) ;
		return SC1445x_AE_ERR_CHANNEL_NOT_ACTIVE ;
	}

	if( ae_state->audio_channels[channel].is_fax ) {
		PRINT( sc1445x_ae_already_fax_error, channel ) ;
		return SC1445x_AE_ERR_ALREADY_FAX ;
	}

	*dsp_hot_dl_sel_addr = 0 ;

	sc1445x_internal_clear_dsp2_dm(
				encoder_buff_base_addr[channel], 0x442 ) ;
	sc1445x_internal_clear_dsp2_dm(
				encoder_buff_base_addr[channel+1], 0x442 ) ;
	sc1445x_internal_clear_dsp2_dm(
				decoder_buffin_base_addr[channel], 0x39C ) ;
	sc1445x_internal_clear_dsp2_dm(
				decoder_buffin_base_addr[channel+1], 0x39C ) ;

	//initalize dsp structs
	*my_fmdp_handle_p = encoder_buff_base_addr[channel+1];
	*more_fax_vars_p = *my_fmdp_handle_p + 1205;

	mem = (unsigned short*)(0x18000  +  2 * (*my_fmdp_handle_p) ) ;
	mem[2] = *more_fax_vars_p + 0x15;//src
	mem[3] = *more_fax_vars_p + 0x65;//data
	mem[4] = encoder_buff_base_addr[channel] + 0x790;//dst

	ae_state->audio_channels[channel].is_fax = 1 ;

	/* switch to fax "codec" */
	sc1445x_internal_send_dsp_cmd( channel + 1, 0xaa ) ;
	sc1445x_internal_send_dsp_cmd( 0x0011, channel << 8  |  6 ) ;

	PRINT( PRINT_LEVEL "Activating fax %d\n", channel ) ;
	return SC1445x_AE_OK ;
}

/* switch an active channel from fax to audio, keeping the previous */
/* codec settings */
short sc1445x_ae_switch_to_audio( sc1445x_ae_state* ae_state,
						unsigned short channel )
{
	int res ;
	short is_wide ;

	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( channel, ae_state->audio_channels_count ) ;

	if( !ae_state->audio_channels[channel].is_active ) {
		PRINT( sc1445x_ae_channel_not_active_error, channel ) ;
		return SC1445x_AE_ERR_CHANNEL_NOT_ACTIVE ;
	}

	if( !ae_state->audio_channels[channel].is_fax ) {
		PRINT( sc1445x_ae_not_fax_error, channel ) ;
		return SC1445x_AE_ERR_NOT_FAX ;
	}

	ae_state->audio_channels[channel].is_fax = 0 ;

	/* restart audio codec */
	is_wide = sc1445x_internal_is_wideband_codec(
			ae_state->audio_channels[channel].enc_codec.type ) || 
		sc1445x_internal_is_wideband_codec(
			ae_state->audio_channels[channel].dec_codec.type) ;
	sc1445x_internal_init_channel_buffers( channel, is_wide ) ;
	/* use this audio channel */
	/* TODO: fix this for G711.1 */
	res = sc1445x_internal_set_codec( ae_state, channel, channel,
			ae_state->audio_channels[channel].enc_codec.type,
			ae_state->audio_channels[channel].dec_codec.type ) ;
	if( res != SC1445x_AE_OK ) {
		return res ;
	}

	PRINT( PRINT_LEVEL "Reactivating audio channel %d (mode=%d)\n",
						channel, ae_state->mode ) ;

	return SC1445x_AE_OK ;
}

/* initialize a fax channel */
short sc1445x_ae_fax_init( sc1445x_ae_state* ae_state, unsigned short channel,
                        unsigned short p0DBIN, unsigned short p0DBOUT,
                        unsigned short pCEDLength, unsigned short pMDMCmd )
{
	AUDIOENGINE_TRACE_FUNC() ;

	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( channel, ae_state->audio_channels_count ) ;

	if( !ae_state->audio_channels[channel].is_active ) {
		PRINT( sc1445x_ae_channel_not_active_error, channel ) ;
		return SC1445x_AE_ERR_CHANNEL_NOT_ACTIVE ;
	}

	if( !ae_state->audio_channels[channel].is_fax ) {
		PRINT( sc1445x_ae_not_fax_error, channel ) ;
		return SC1445x_AE_ERR_NOT_FAX ;
	}

	/* pass parameters to dsp */
	sc1445x_internal_send_dsp_cmd( 0x001b, pMDMCmd ) ;
	sc1445x_internal_send_dsp_cmd( 0x0018, p0DBIN ) ;
	sc1445x_internal_send_dsp_cmd( 0x0019, p0DBOUT ) ;
	sc1445x_internal_send_dsp_cmd( 0x001a, pCEDLength ) ;
	/* tell dsp to start fax processing on the next tick */
	sc1445x_internal_send_dsp_cmd( 0x0017, channel ) ;
	sc1445x_internal_send_dsp_cmd( 0x0020, 0x0000 ) ;

	return SC1445x_AE_OK ;
}

#endif

#ifdef FXO_PCM_LOG
static void do_fxo_pcm_log(){
	unsigned short n = *(unsigned short*)0x1b0a0 ;
	if(n==1){
		if( likely( fxo_pcm_playback_pos < fxo_pcm_playback_end ) ) {
			memcpy( fxo_pcm_playback_pos, (void*)FXO_PLAY_PCM_LOG, 160 ) ;
			fxo_pcm_playback_pos += 160 ;
		} else {
			if(!fxo_pcm_playback_done){
				PRINT( PRINT_LEVEL "FXO PCM_IN BUFFER IS FULL (%p - %p)"
						" -- no more logging\n",
						fxo_pcm_playback_buf, fxo_pcm_playback_end ) ;
				fxo_pcm_playback_done = 1;	
			}
		}

		if( likely( fxo_pcm_capture_pos < fxo_pcm_capture_end ) ) {
			memcpy( fxo_pcm_capture_pos, (void*)FXO_CAP_PCM_LOG, 160 ) ;
			fxo_pcm_capture_pos += 160 ;
		} else {
			if(!fxo_pcm_capture_done){
				PRINT( PRINT_LEVEL "FXO PCM_OUT BUFFER IS FULL (%p - %p)"
						" -- no more logging\n",
						fxo_pcm_capture_buf, fxo_pcm_capture_end ) ;
				fxo_pcm_capture_done = 1;	
			}
		}
	}
}
#endif

#ifdef FXO_PLAY_PRERECORDED_PCM
static void send_recorded_cas(){
	unsigned short *n = (unsigned short*)0x10460 ; //0x1e0+0x50
	if(*n==10){
		fxo_cas_pos = (unsigned char*)cas_values_wrong;
		fxo_cas_end = (unsigned char*)(&cas_values_wrong[200000]);
		*n=21;
	}
	if(*n==15){
		fxo_cas_pos = (unsigned char*)cas_values_correct;
		fxo_cas_end = (unsigned char*)(&cas_values_correct[3360]);
		*n=21;
	}
	if(*n>20){
		if( likely( fxo_cas_pos < fxo_cas_end ) ) {
			memcpy( (void*)FXO_SEND_CAS_TONE, fxo_cas_pos, 160 ) ;
			fxo_cas_pos += 160 ;
		} else {
			PRINT( PRINT_LEVEL "FXO cas tone send -- no more sending\n") ;
			*n=0;
		}
	}
}
#endif

