/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 * The API of the audio engine
 */
#if !defined( __AUDIO_ENGINE_API_H__ )
#define __AUDIO_ENGINE_API_H__

#include <asm/regs.h>
#if defined( CONFIG_SC14450 )
#  include "sitel_io.h"
#endif
#include "sc1445x_audioengine_defs.h"


/* error messages */
#if !defined( AUDIO_ENGINE_NO_OUTPUT )
/* "%s: cannot allocate memory for %s!\n" */
extern const char sc1445x_ae_nomem_error[] ;

/* "NULL audio engine struct passed to %s!\n" */
extern const char sc1445x_ae_null_ae_struct_error[] ;

/* "%s: argument %s is out of range!\n" */
extern const char sc1445x_ae_arg_out_of_range_error[] ;

/* "NULL pointer argument passed to %s!\n" */
extern const char sc1445x_ae_null_pointer_arg_error[] ;

/* "No free channel to activate for codec type %d\n" */
extern const char sc1445x_ae_no_free_channel_for_codec_error[] ;

/* "The audio channel %d is not activated!\n" */
extern const char sc1445x_ae_channel_not_active_error[] ;

/* "CODEC type %d is not supported!\n" */
extern const char sc1445x_ae_codec_not_supported_error[] ;

#if 0
/* "%s: tone generation module already playing a tone!\n" */
extern const char sc1445x_ae_tonegen_not_idle_error[] ;
#endif

/* "%s: tone generation module is idle!\n" */
extern const char sc1445x_ae_tonegen_is_idle_error[] ;

/* "%s: cannot change mode when there are active channels!\n" */
extern const char sc1445x_ae_cannot_change_mode_error[] ;

/* "%s: this feature is not available in this configuration!\n" */
extern const char sc1445x_ae_not_available_error[] ;

/* "The audio channel %d is already switched to fax!\n" */
extern const char sc1445x_ae_already_fax_error[] ;

/* "The audio channel %d is not switched to fax!\n" */
extern const char sc1445x_ae_not_fax_error[] ;

/* "%s: line %d is not mapped to any audio stream!\n" */
extern const char sc1445x_ae_line_not_mapped_error[] ;

/* "%s: line %d cannot be mapped to any audio stream!\n" */
extern const char sc1445x_ae_no_free_audio_streams_error[] ;

/* "%s: using iLBC requires the 4th channel to be free, but it is being used!\n" */
extern const char sc1445x_ae_ilbc_needs_channel4_error[] ;

/* "%s: the 4th channel is being used by the iLBC codec!\n" */
extern const char sc1445x_ae_ilbc_uses_channel4_error[] ;

/* "%s: cannot attach to PCM device because CODEC/CLASSD is busy!\n" */
extern const char sc1445x_ae_pcm_dev_attach_codec_busy_error[] ;

/* "%s: interface mode %d is not currently available!\n" */
extern const char sc1445x_ae_iface_mode_not_available_error[] ;

/* "%s: cannot attach to any more PCM devices!\n" */
extern const char sc1445x_ae_pcm_dev_attach_impossible_error[] ;

/* "%s: the passed audio profile is incompatible with the current one!\n" */
extern const char sc1445x_ae_incompatible_audio_profile_error[] ;

/* "%s: cannot mix raw PCM with voice in mode %d!\n" */
extern const char sc1445x_ae_cannot_mix_voice_and_raw_pcm_error[] ;

/* "%s: No free channel to mix raw PCM\n" */
extern const char sc1445x_ae_no_free_channel_for_raw_pcm_error[] ;

/* "%s: already mixing raw PCM with voice\n" */
extern const char sc1445x_ae_already_mixing_raw_pcm_error[] ;

/* "%s: currently not mixing raw PCM with voice\n" */
extern const char sc1445x_ae_not_mixing_raw_pcm_error[] ;

/* "%s: dual audio path is already active\n" */
extern const char sc1445x_ae_dual_audio_path_already_active_error[] ;

/* "%s: dual audio path is not active\n" */
extern const char sc1445x_ae_dual_audio_path_not_active_error[] ;
#endif


/* error codes */
#define SC1445x_AE_OK					(short) 0
#define SC1445x_AE_ERR_NO_MEM				(short)-1
#define SC1445x_AE_ERR_NULL_AE_STRUCT			(short)-2
#define SC1445x_AE_ERR_ARG_OUT_OF_RANGE			(short)-3
#define SC1445x_AE_ERR_NULL_POINTER_ARG			(short)-4
#define SC1445x_AE_ERR_NO_FREE_CHANNEL			(short)-5
#define SC1445x_AE_ERR_CHANNEL_NOT_ACTIVE		(short)-6
#define SC1445x_AE_ERR_CODEC_NOT_SUPPORTED		(short)-7
#if 0
#define SC1445x_AE_ERR_TONEGEN_NOT_IDLE			(short)-8
#endif
#define SC1445x_AE_ERR_TONEGEN_IS_IDLE			(short)-9
#define SC1445x_AE_ERR_CANNOT_CHANGE_MODE		(short)-10
#define SC1445x_AE_ERR_NOT_AVAILABLE			(short)-11
#define SC1445x_AE_ERR_ALREADY_FAX			(short)-12
#define SC1445x_AE_ERR_NOT_FAX				(short)-13
#define SC1445x_AE_ERR_LINE_NOT_MAPPED			(short)-14
#define SC1445x_AE_ERR_NO_FREE_AUDIO_STREAMS		(short)-15
#define SC1445x_AE_ERR_ILBC_NEEDS_CHANNEL4		(short)-16
#define SC1445x_AE_ERR_ILBC_USES_CHANNEL4		(short)-17
#define SC1445x_AE_ERR_PCM_DEV_ATTACH_CODEC_BUSY	(short)-18
#define SC1445x_AE_ERR_IFACE_MODE_NOT_AVAILABLE		(short)-19
#define SC1445x_AE_ERR_PCM_DEV_ATTACH_IMPOSSIBLE	(short)-20
#define SC1445x_AE_ERR_CANNOT_MIX_VOICE_AND_RAW_PCM	(short)-21
#define SC1445x_AE_ERR_NO_FREE_CHANNEL_FOR_RAW_PCM	(short)-22
#define SC1445x_AE_ERR_ALREADY_MIXING_RAW_PCM		(short)-23
#define SC1445x_AE_ERR_NOT_MIXING_RAW_PCM		(short)-24
#if 0
#define SC1445x_AE_ERR_NO_MORE_CONVERSATIONS		(short)-25
#define SC1445x_AE_ERR_CONVERSATION_NOT_STARTED		(short)-26
#define SC1445x_AE_ERR_CONVERSATION_HAS_MEMBERS		(short)-27
#endif
#define SC1445x_AE_ERR_INCOMPATIBLE_AUDIO_PROFILE	(short)-28
#define SC1445x_AE_ERR_DUAL_AUDIO_PATH_IS_ACTIVE	(short)-29
#define SC1445x_AE_ERR_DUAL_AUDIO_PATH_IS_NOT_ACTIVE	(short)-30



/* some macros to check the validity of the arguments passed to API functions */

/* check if x (supposedly a pointer to sc1445x_ae_state) is NULL */
#define CHECK_AE_STATE( x )						\
do {									\
	if( !(x) ) {							\
		PRINT( sc1445x_ae_null_ae_struct_error, __FUNCTION__ ) ;  \
		return SC1445x_AE_ERR_NULL_AE_STRUCT ;			\
	}								\
} while( 0 )

/* check if x (supposedly a pointer argument) is NULL */
#define CHECK_POINTER_ARG( x )						\
do {									\
	if( !(x) ) {							\
		PRINT( sc1445x_ae_null_pointer_arg_error, __FUNCTION__ ) ;  \
		return SC1445x_AE_ERR_NULL_POINTER_ARG ;		\
	}								\
} while( 0 )

/* check if integer arg is not less than ceil_val */
#define CHECK_ARG_CEIL( arg, ceil_val )					\
do {									\
	if( (arg) >= (ceil_val) ) {					\
		PRINT( sc1445x_ae_arg_out_of_range_error,		\
			__FUNCTION__, #arg ) ;				\
		return SC1445x_AE_ERR_ARG_OUT_OF_RANGE ;		\
	}								\
} while( 0 )

/* check if integer arg is in the range [min_val, max_val] */
#define CHECK_ARG_RANGE( arg, min_val, max_val )			\
do {									\
	if( (arg) < (min_val)  ||  (arg) > (max_val) ) {		\
		PRINT( sc1445x_ae_arg_out_of_range_error,		\
			__FUNCTION__, #arg ) ;				\
		return SC1445x_AE_ERR_ARG_OUT_OF_RANGE ;		\
	}								\
} while( 0 )



/*******************************/
/* INITIALIZATION/FINALIZATION */
/*******************************/

/* initialize audio engine and allocate all required memory */
short sc1445x_ae_init_engine( sc1445x_ae_state* ae_state ) ;

/* finalize audio engine and free all allocated memory */
short sc1445x_ae_finalize_engine( sc1445x_ae_state* ae_state ) ;


/************/
/* SPEAKERS */
/************/

/* mute the active speaker (retaining the current volume) */
short sc1445x_ae_mute_spk( sc1445x_ae_state* ae_state ) ;
/* unmute the active speaker (retaining the previous volume) */
short sc1445x_ae_unmute_spk( sc1445x_ae_state* ae_state ) ;
/* get the mute status of the active speaker */
static inline
short sc1445x_ae_is_spk_muted( const sc1445x_ae_state* ae_state,
						unsigned short* is_muted )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( is_muted ) ;

	*is_muted = ae_state->spks[ae_state->spk_in_use].is_muted ;
	return SC1445x_AE_OK ;
}


/***************/
/* MICROPHONES */
/***************/

/* mute the active microphone (retaining the current gain) */
short sc1445x_ae_mute_mic( sc1445x_ae_state* ae_state ) ;
/* unmute the active microphone (retaining the previous gain) */
short sc1445x_ae_unmute_mic( sc1445x_ae_state* ae_state ) ;
/* get the mute status of the active microphone */
static inline
short sc1445x_ae_is_mic_muted( const sc1445x_ae_state* ae_state,
						unsigned short* is_muted )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( is_muted ) ;

	*is_muted = ae_state->mics[ae_state->mic_in_use].is_muted ;
	return SC1445x_AE_OK ;
}


/*******/
/* AEC */
/*******/

/* turn on Acoustic Echo Cancellation */
short sc1445x_ae_set_aec_on( sc1445x_ae_state* ae_state ) ;
/* turn off Acoustic Echo Cancellation */
short sc1445x_ae_set_aec_off( sc1445x_ae_state* ae_state ) ;
/* get the status of the AEC */
static inline
short sc1445x_ae_is_aec_on( const sc1445x_ae_state* ae_state,
					unsigned short* is_aec_enabled )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( is_aec_enabled ) ;

	*is_aec_enabled = ae_state->AEC_enabled ;
	return SC1445x_AE_OK ;
}


/*****************/
/* AUDIO PROFILE */
/*****************/

/* reset audio profile to its default settings */
short sc1445x_ae_reset_audio_profile( sc1445x_ae_state* ae_state ) ;
/* get the total size of the dynamic audio profile arrays, in bytes */
short sc1445x_ae_get_audio_profile_dyn_size( const sc1445x_ae_state* ae_state,
						       unsigned* nbytes ) ;
/*
 * get the current audio profile settings
 *
 * NOTE	first, call sc1445x_ae_get_audio_profile_sizes() to get the
 * 	sizes of the variable-lenth arrays
 * 	then, allocate memory for these arrays
 * 	finally, call sc1445x_ae_get_audio_profile()
 * 	all operations should be made on the same
 * 	sc1445x_ae_audio_profile struct
 */
short sc1445x_ae_get_audio_profile( const sc1445x_ae_state* ae_state,
					       sc1445x_ae_audio_profile* ap ) ;
/* set the audio profile settings */
short sc1445x_ae_set_audio_profile( sc1445x_ae_state* ae_state,
				       const sc1445x_ae_audio_profile* ap ) ;


/***********/
/* GENERAL */
/***********/

/* set the operating mode of the audio engine */
/* the mode can change only when there are no active audio channels */
short sc1445x_ae_set_mode( sc1445x_ae_state* ae_state,
					sc1445x_ae_mode new_mode, void* arg ) ;
/* get the operating mode of the audio engine */
static inline
short sc1445x_ae_get_mode( const sc1445x_ae_state* ae_state,
							sc1445x_ae_mode* mode )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( mode ) ;

	*mode = ae_state->mode ;
	return SC1445x_AE_OK ;
}
/* set the sidetone volume */
short sc1445x_ae_set_sidetone_volume( sc1445x_ae_state* ae_state,
						unsigned short new_vol ) ;
/* get the sidetone volume */
static inline
short sc1445x_ae_get_sidetone_volume( const sc1445x_ae_state* ae_state,
							unsigned short* vol )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( vol ) ;

	*vol = ae_state->sidetone_volume ;
	return SC1445x_AE_OK ;
}


/*****************/
/* AUDIO PROFILE */
/*****************/

/* set the interface mode of the audio engine */
/* this will have an effect on which virtual mic and spk(s) are being used */
short sc1445x_ae_set_iface_mode( sc1445x_ae_state* ae_state,
					const sc1445x_ae_iface_mode new_mode ) ;
/* get the interface mode of the audio engine */
static inline
short sc1445x_ae_get_iface_mode( const sc1445x_ae_state* ae_state,
							sc1445x_ae_iface_mode* mode )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( mode ) ;

	*mode = ae_state->iface_mode ;
	return SC1445x_AE_OK ;
}

/* set the volume of the current virtual speaker */
short sc1445x_ae_set_vspk_volume( sc1445x_ae_state* ae_state,
						unsigned short vol ) ;
/* get the volume of the current virtual speaker */
static inline
short sc1445x_ae_get_vspk_volume( const sc1445x_ae_state* ae_state,
						unsigned short* vol )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( vol ) ;

	*vol = ae_state->vspks[ae_state->iface_mode].vol_level ;
	return SC1445x_AE_OK ;
}
/* set the volume of a virtual speaker */
short sc1445x_ae_set_vspk_volume_ex( sc1445x_ae_state* ae_state,
			const sc1445x_ae_iface_mode m, unsigned short vol ) ;
/* get the volume range of a virtual speaker */
short sc1445x_ae_get_vspk_volume_ex( const sc1445x_ae_state* ae_state,
		const sc1445x_ae_iface_mode m, unsigned short* vol_min,
		unsigned short* vol_curr, unsigned short* vol_max ) ;

/* set the DTMF level of the current virtual speaker */
short sc1445x_ae_set_vspk_dtmf_level( sc1445x_ae_state* ae_state,
						unsigned short lvl ) ;
/* get the DTMF level of the current virtual speaker */
static inline
short sc1445x_ae_get_vspk_dtmf_level( const sc1445x_ae_state* ae_state,
						unsigned short* lvl )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( lvl ) ;

	*lvl = ae_state->vspks[ae_state->iface_mode].dtmf_level ;
	return SC1445x_AE_OK ;
}
/* set the DTMF level of a virtual speaker */
short sc1445x_ae_set_vspk_dtmf_level_ex( sc1445x_ae_state* ae_state,
			const sc1445x_ae_iface_mode m, unsigned short lvl ) ;
/* get the DTMF level range of a virtual speaker */
short sc1445x_ae_get_vspk_dtmf_level_ex( const sc1445x_ae_state* ae_state,
		const sc1445x_ae_iface_mode m, unsigned short* lvl_min,
		unsigned short* lvl_curr, unsigned short* lvl_max ) ;
/* set the PCM volume of the current virtual speaker */
short sc1445x_ae_set_vspk_pcm_level( sc1445x_ae_state* ae_state,
						unsigned short lvl ) ;
/* get the PCM volume of the current virtual speaker */
static inline
short sc1445x_ae_get_vspk_pcm_level( const sc1445x_ae_state* ae_state,
						unsigned short* lvl )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( lvl ) ;

	*lvl = ae_state->vspks[ae_state->iface_mode].pcm_level ;
	return SC1445x_AE_OK ;
}
/* set the PCM volume of a virtual speaker */
short sc1445x_ae_set_vspk_pcm_lvl_ex( sc1445x_ae_state* ae_state,
			const sc1445x_ae_iface_mode m, unsigned short lvl ) ;
/* get the PCM volume range of a virtual speaker */
short sc1445x_ae_get_vspk_pcm_lvl_ex( const sc1445x_ae_state* ae_state,
		const sc1445x_ae_iface_mode m, unsigned short* lvl_min,
		unsigned short* lvl_curr, unsigned short* lvl_max ) ;

/* set the gain of the current virtual microphone */
short sc1445x_ae_set_vmic_gain( sc1445x_ae_state* ae_state,
						unsigned short gain ) ;
/* get the gain of the current virtual microphone */
static inline
short sc1445x_ae_get_vmic_gain( const sc1445x_ae_state* ae_state,
						unsigned short* gain )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( gain ) ;

	*gain = ae_state->vmics[ae_state->iface_mode].gain_level ;
	return SC1445x_AE_OK ;
}
/* set the gain of a virtual microphone */
short sc1445x_ae_set_vmic_gain_ex( sc1445x_ae_state* ae_state,
			const sc1445x_ae_iface_mode m, unsigned short gain ) ;
/* get the gain range of a virtual microphone */
short sc1445x_ae_get_vmic_gain_ex( const sc1445x_ae_state* ae_state,
		const sc1445x_ae_iface_mode m, unsigned short* gain_min,
		unsigned short* gain_curr, unsigned short* gain_max ) ;
/* mute the current virtual microphone */
short sc1445x_ae_mute_vmic( sc1445x_ae_state* ae_state ) ;
/* unmute the current virtual microphone */
short sc1445x_ae_unmute_vmic( sc1445x_ae_state* ae_state ) ;
/* get the mute status of the current virtual microphone */
static inline
short sc1445x_ae_is_vmic_muted( const sc1445x_ae_state* ae_state,
						unsigned short* is_muted )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( is_muted ) ;

	*is_muted = ae_state->vmics[ae_state->iface_mode].is_muted ;
	return SC1445x_AE_OK ;
}

/* get the number of vspk volume levels */
short sc1445x_ae_get_vspk_vol_level_count( const sc1445x_ae_state* ae_state,
						unsigned short* count ) ;
/* get the number of vmic gain levels */
short sc1445x_ae_get_vmic_gain_level_count( const sc1445x_ae_state* ae_state,
						unsigned short* count ) ;


/******************/
/* AUDIO CHANNELS */
/******************/

/* get the total number of supported audio channels */
static inline
short sc1445x_ae_get_channels_count( const sc1445x_ae_state* ae_state,
					unsigned short* nchannels )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( nchannels ) ;

	*nchannels = ae_state->audio_channels_count ;
	return SC1445x_AE_OK ;
}
/* get the number of free audio channels */
static inline
short sc1445x_ae_get_free_channels_count( const sc1445x_ae_state* ae_state,
					unsigned short* nfree_channels )
{
	unsigned short i, cnt ;
	CHECK_AE_STATE( ae_state ) ;
	CHECK_POINTER_ARG( nfree_channels ) ;

	for( i = 0, cnt = 0 ;  i < ae_state->audio_channels_count ;  ++i ) {
		if( ae_state->audio_channels[i].is_active )
			++cnt ;
	}

	*nfree_channels = cnt ;
	return SC1445x_AE_OK ;
}

/* select a free audio channel and start using it */
short sc1445x_ae_activate_free_channel( sc1445x_ae_state* ae_state,
					sc1445x_ae_codec_type enc_codec,
					sc1445x_ae_codec_type dec_codec,
					unsigned short* activated_channel ) ;
/* deactivate an audio channel */
short sc1445x_ae_deactivate_channel( sc1445x_ae_state* ae_state,
					unsigned short channel_index ) ;

/* get the address of the playback buffer of an audio channel */
static inline
short sc1445x_ae_get_playback_buffer( const sc1445x_ae_state* ae_state,
					unsigned short channel_index,
					const unsigned char** pbuf )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( channel_index, ae_state->audio_channels_count ) ;
	CHECK_POINTER_ARG( pbuf ) ;

	*pbuf = ae_state->audio_channels[channel_index].playback_buffer ;
	return SC1445x_AE_OK ;
}
/* get the address of the capture buffer of an audio channel */
static inline
short sc1445x_ae_get_capture_buffer( const sc1445x_ae_state* ae_state,
					unsigned short channel_index,
					const unsigned char** cbuf )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( channel_index, ae_state->audio_channels_count ) ;
	CHECK_POINTER_ARG( cbuf ) ;

	*cbuf = ae_state->audio_channels[channel_index].capture_buffer ;
	return SC1445x_AE_OK ;
}

/* get a copy of the current codec settings of an active audio channel */
static inline
short sc1445x_ae_copy_codec( const sc1445x_ae_state* ae_state,
				unsigned short channel_index,
				sc1445x_ae_codec_settings* enc_codec,
				sc1445x_ae_codec_settings* dec_codec )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( channel_index, ae_state->audio_channels_count ) ;
	CHECK_POINTER_ARG( enc_codec ) ;
	CHECK_POINTER_ARG( dec_codec ) ;

	if( !ae_state->audio_channels[channel_index].is_active ) {
		PRINT( sc1445x_ae_channel_not_active_error, channel_index ) ;
		return SC1445x_AE_ERR_CHANNEL_NOT_ACTIVE ;
	}

	memcpy( enc_codec, &ae_state->audio_channels[channel_index].enc_codec,
							sizeof( *enc_codec ) ) ;
	memcpy( dec_codec, &ae_state->audio_channels[channel_index].dec_codec,
							sizeof( *dec_codec ) ) ;
	return SC1445x_AE_OK ;
}
/* get the codec types of an active audio channel */
static inline
short sc1445x_ae_get_codec_type( const sc1445x_ae_state* ae_state,
			unsigned short channel_index, unsigned short* enc_type,
						unsigned short* dec_type )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( channel_index, ae_state->audio_channels_count ) ;
	CHECK_POINTER_ARG( enc_type ) ;
	CHECK_POINTER_ARG( dec_type ) ;

	if( !ae_state->audio_channels[channel_index].is_active ) {
		PRINT( sc1445x_ae_channel_not_active_error, channel_index ) ;
		return SC1445x_AE_ERR_CHANNEL_NOT_ACTIVE ;
	}

	*enc_type = ae_state->audio_channels[channel_index].enc_codec.type ;
	*dec_type = ae_state->audio_channels[channel_index].dec_codec.type ;
	return SC1445x_AE_OK ;
}
/* set the rates of the codecs of an active audio channel */
static inline
short sc1445x_ae_set_codec_rate( sc1445x_ae_state* ae_state,
			unsigned short channel_index, unsigned int new_enc_rate,
						unsigned int new_dec_rate )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( channel_index, ae_state->audio_channels_count ) ;

	if( !ae_state->audio_channels[channel_index].is_active ) {
		PRINT( sc1445x_ae_channel_not_active_error, channel_index ) ;
		return SC1445x_AE_ERR_CHANNEL_NOT_ACTIVE ;
	}

	ae_state->audio_channels[channel_index].enc_codec.rate = new_enc_rate ;
	ae_state->audio_channels[channel_index].dec_codec.rate = new_dec_rate ;
	// TODO: actually change the rate
	return SC1445x_AE_OK ;
}
/* get the current rates of the codecs of an active audio channel */
static inline
short sc1445x_ae_get_codec_rate( const sc1445x_ae_state* ae_state,
			unsigned short channel_index, unsigned int* enc_rate,
							unsigned int* dec_rate )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( channel_index, ae_state->audio_channels_count ) ;
	CHECK_POINTER_ARG( enc_rate ) ;
	CHECK_POINTER_ARG( dec_rate ) ;

	if( !ae_state->audio_channels[channel_index].is_active ) {
		PRINT( sc1445x_ae_channel_not_active_error, channel_index ) ;
		return SC1445x_AE_ERR_CHANNEL_NOT_ACTIVE ;
	}

	*enc_rate = ae_state->audio_channels[channel_index].enc_codec.rate ;
	*dec_rate = ae_state->audio_channels[channel_index].dec_codec.rate ;
	return SC1445x_AE_OK ;
}
/* get the sample width (in bits) of the codecs of an active audio channel */
static inline
short sc1445x_ae_get_codec_sample_width( const sc1445x_ae_state* ae_state,
			unsigned short channel_index, unsigned int* enc_nbits,
						unsigned int* dec_nbits )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( channel_index, ae_state->audio_channels_count ) ;
	CHECK_POINTER_ARG( enc_nbits ) ;
	CHECK_POINTER_ARG( dec_nbits ) ;

	if( !ae_state->audio_channels[channel_index].is_active ) {
		PRINT( sc1445x_ae_channel_not_active_error, channel_index ) ;
		return SC1445x_AE_ERR_CHANNEL_NOT_ACTIVE ;
	}

	*enc_nbits =
		ae_state->audio_channels[channel_index].enc_codec.sample_bits ;
	*dec_nbits =
		ae_state->audio_channels[channel_index].dec_codec.sample_bits ;
	return SC1445x_AE_OK ;
}
#if defined( SC1445x_AE_COLLECT_STATISTICS )
/* get the current statistics of an audio channel */
static inline
short sc1445x_ae_get_channel_stats( const sc1445x_ae_state* ae_state,
					unsigned short channel_index,
						sc1445x_ae_audio_stats* stats )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( channel_index, ae_state->audio_channels_count ) ;
	CHECK_POINTER_ARG( stats ) ;

	memcpy( stats, &ae_state->audio_channels[channel_index].stats,
							sizeof( *stats ) ) ;

	return SC1445x_AE_OK ;
}
/* reset the statistics of an audio channel */
static inline
short sc1445x_ae_reset_channel_stats( const sc1445x_ae_state* ae_state,
					unsigned short channel_index )
{
	CHECK_AE_STATE( ae_state ) ;
	CHECK_ARG_CEIL( channel_index, ae_state->audio_channels_count ) ;

	memset( &ae_state->audio_channels[channel_index].stats, 0,
					sizeof( sc1445x_ae_audio_stats ) ) ;

	return SC1445x_AE_OK ;
}
#endif
/* control DSP loopback capability */
short sc1445x_ae_control_dsp_loopback( const sc1445x_ae_state* ae_state,
							unsigned short val ) ;
/*
 * enable monitoring mode
 * i.e. there is no conference and all remote parties can only hear us,
 * while we can hear all remote parties
 */
short sc1445x_ae_enable_monitoring( sc1445x_ae_state* ae_state ) ;
/*
 * disable monitoring mode
 * i.e. all active audio channels are mixed into a conference
 */
short sc1445x_ae_disable_monitoring( sc1445x_ae_state* ae_state ) ;
/*
 * enable external CODEC mic and/or spk
 * i.e. instead of the built-in CODEC/CLASSD, an external CODEC is used
 * the external CODEC is connected to PCM bus, slot #0
 */
short sc1445x_ae_enable_external_codec( sc1445x_ae_state* ae_state,
				       short enable_mic, short enable_spk ) ;
/*
 * disable external CODEC mic and/or spk
 * i.e. return to normal operation, using the built-in CODEC/CLASSD
 */
short sc1445x_ae_disable_external_codec( sc1445x_ae_state* ae_state,
	       				short disable_mic, short disable_spk ) ;


/*******************/
/* TONE GENERATION */
/*******************/

/* start playing a tone */
/* on-off times are given in msec */
short sc1445x_ae_start_tone( sc1445x_ae_state* ae_state, unsigned short tg_mod,
				sc1445x_ae_tone tone1, sc1445x_ae_tone tone2,
				sc1445x_ae_tone tone3, unsigned short dur_on,
				unsigned short dur_off, short repeat ) ;
/*
 * on-off times are given in msec
 * play_count:
 * 	== 0 --> play "for ever"
 * 	 > 0 --> play (play_count) times
 */
short sc1445x_ae_start_tone4( sc1445x_ae_state* ae_state, unsigned short tg_mod,
				sc1445x_ae_tone tone1, unsigned short ampl1,
				sc1445x_ae_tone tone2, unsigned short ampl2,
				sc1445x_ae_tone tone3, unsigned short ampl3,
				sc1445x_ae_tone tone4, unsigned short ampl4,
				unsigned short dur_on, unsigned short dur_off,
				unsigned short play_count ) ;
/* stop playing the current tone */
short sc1445x_ae_stop_tone( sc1445x_ae_state* ae_state,
						unsigned short tg_mod ) ;
/* automatically stop playing the current tone after x msec */
short sc1445x_ae_auto_stop_tone( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned long x ) ;
/* test if the tone generation module is idle */
short sc1445x_ae_is_tonegen_idle( sc1445x_ae_state* ae_state,
					unsigned short tg_mod,
					unsigned short* is_tonegen_idle ) ;
/* set tone volume */
short sc1445x_ae_set_tone_volume( sc1445x_ae_state* ae_state,
							unsigned short tg_mod,
							unsigned short vol ) ;
/* get tone volume */
short sc1445x_ae_get_tone_volume( sc1445x_ae_state* ae_state,
							unsigned short tg_mod,
							unsigned short* vol ) ;
/* play a predefined standard tone */
short sc1445x_ae_play_standard_tone( sc1445x_ae_state* ae_state,
			unsigned short tg_mod, sc1445x_ae_std_tone std_tone ) ;
/* set tone volume for TX path */
short sc1445x_ae_set_tone_volume_tx( sc1445x_ae_state* ae_state,
							unsigned short vol ) ;
/* set tone volume for TX path, with parameter for tg_mod */
short sc1445x_ae_set_tone_volume_tx_ex( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short vol ) ;
/* start playing a custom tone */
/* on-off times are given in msec */
short sc1445x_ae_start_custom_tone( sc1445x_ae_state* ae_state,
				unsigned short tg_mod,
				const sc1445x_ae_custom_tone_params* params,
				unsigned short dur_on, unsigned short dur_off,
				short repeat ) ;
/*
 * start playing a custom tone
 * on-off times are given in msec
 * play_count:
 * 	== 0 --> play "for ever"
 * 	 > 0 --> play (play_count) times
 */
short sc1445x_ae_start_custom_tone4( sc1445x_ae_state* ae_state,
				unsigned short tg_mod,
				const sc1445x_ae_custom_tone4_params* params,
				unsigned short dur_on, unsigned short dur_off,
				unsigned short play_count ) ;
/* start playing a tone sequence */
short sc1445x_ae_start_tone_sequence( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short seq_len,
				const sc1445x_ae_tone_sequence_part* seq,
				sc1445x_ae_tone_seq_repeat repeat_seq ) ;
/*
 * start playing a tone sequence
 * play_count:
 * 	== 0 --> play "for ever"
 * 	 > 0 --> play (play_count) times
 */
short sc1445x_ae_start_tone4_sequence( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short seq_len,
				const sc1445x_ae_tone4_sequence_part* seq,
				sc1445x_ae_tone_seq_repeat repeat_seq,
				unsigned short play_count ) ;
/* start playing a ringtone sequence */
short sc1445x_ae_start_ringtone_sequence( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short seq_len,
				const sc1445x_ae_tone_sequence_part* seq,
				sc1445x_ae_tone_seq_repeat repeat_seq ) ;
/* start playing a ringtone sequence
 * play_count:
 * 	== 0 --> play "for ever"
 * 	 > 0 --> play (play_count) times
 */
short sc1445x_ae_start_ringtone4_sequence( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short seq_len,
				const sc1445x_ae_tone4_sequence_part* seq,
				sc1445x_ae_tone_seq_repeat repeat_seq,
	       			unsigned short play_count ) ;
/*
 * start playing a key (DTMF) tone sequence
 * play_count:
 * 	== 0 --> play "for ever"
 * 	 > 0 --> play (play_count) times
 */
short sc1445x_ae_start_keytone4_sequence( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short seq_len,
				const sc1445x_ae_tone4_sequence_part* seq,
				sc1445x_ae_tone_seq_repeat repeat_seq,
	       			unsigned short play_count ) ;
/* expand existing tone sequence (or start a new one if none exists) */
/* expansion is made by adding tones at the end of the sequence */
short sc1445x_ae_expand_tone_sequence( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short seq_len,
				const sc1445x_ae_tone_sequence_part* seq ) ;
/*
 * expand existing tone sequence (or start a new one if none exists)
 * expansion is made by adding tones at the end of the sequence
 */
short sc1445x_ae_expand_tone4_sequence( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short seq_len,
				const sc1445x_ae_tone4_sequence_part* seq ) ;
/*
 * start playing a key tone
 * on-off times are given in msec
 */
short sc1445x_ae_start_key_tone( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, sc1445x_ae_tone tone1,
				sc1445x_ae_tone tone2, sc1445x_ae_tone tone3,
				unsigned short dur_on, unsigned short dur_off,
				short repeat ) ;
/*
 * start playing a key tone
 * on-off times are given in msec
 * play_count:
 * 	== 0 --> play "for ever"
 * 	 > 0 --> play (play_count) times
 */
short sc1445x_ae_start_key_tone4( sc1445x_ae_state* ae_state,
				unsigned short tg_mod,
				sc1445x_ae_tone tone1, unsigned short ampl1,
				sc1445x_ae_tone tone2, unsigned short ampl2,
				sc1445x_ae_tone tone3, unsigned short ampl3,
				sc1445x_ae_tone tone4, unsigned short ampl4,
				unsigned short dur_on, unsigned short dur_off,
				unsigned short play_count ) ;
/*
 * set the RX (playback) volume for a tonegen module
 */
short sc1445x_ae_set_tonegen_rx_volume( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short volume ) ;
/*
 * set the TX (capture) volume for a tonegen module
 */
short sc1445x_ae_set_tonegen_tx_volume( sc1445x_ae_state* ae_state,
				unsigned short tg_mod, unsigned short volume ) ;
/*
 * get the RX (playback) volume for a tonegen module
 */
short sc1445x_ae_get_tonegen_rx_volume( sc1445x_ae_state* ae_state,
			unsigned short tg_mod, unsigned short* volume ) ;
/*
 * get the TX (capture) volume for a tonegen module
 */
short sc1445x_ae_get_tonegen_tx_volume( sc1445x_ae_state* ae_state,
			unsigned short tg_mod, unsigned short* volume ) ;
/*
 * register a PID to be notified with signo when a tone ends normally
 */
short sc1445x_ae_register_tone_callback( sc1445x_ae_state* ae_state,
					pid_t pid, int signo, int* cmem ) ;

/*******************/
/* RAW PCM SUPPORT */
/*******************/

/*
 * start playing raw PCM concurrently with voice
 */
short sc1445x_ae_start_mixing_raw_pcm_with_voice( sc1445x_ae_state* ae_state,
				sc1445x_ae_raw_pcm_rate rate,
				sc1445x_ae_raw_pcm_sample_size sample_bits ) ;
/*
 * stop playing raw PCM concurrently with voice
 * (stop only the raw PCM playback)
 */
short sc1445x_ae_stop_mixing_raw_pcm_with_voice( sc1445x_ae_state* ae_state ) ;
/*
 * enable dual audio path
 * side effect: switch to SC1445x_AE_IFACE_MODE_OPEN_LISTENING iface mode
 */
short sc1445x_ae_enable_dual_audio_path( sc1445x_ae_state* ae_state ) ;
/*
 * disable dual audio path
 * side effect: switch to the iface mode that was active
 * 		when sc1445x_ae_enable_dual_audio_path() was called
 */
short sc1445x_ae_disable_dual_audio_path( sc1445x_ae_state* ae_state ) ;



#if defined( SC1445x_AE_PCM_LINES_SUPPORT )

/********************/
/* ATA/DECT SUPPORT */
/********************/

/* connect an active audio channel to a line */
short sc1445x_ae_connect_to_line( sc1445x_ae_state* ae_state,
				unsigned short channel, unsigned short line ) ;
/* connect two lines with each other */
short sc1445x_ae_connect_lines( sc1445x_ae_state* ae_state,
				unsigned short line1, unsigned short line2 ) ;
/* disconnect a line */
short sc1445x_ae_disconnect_line( sc1445x_ae_state* ae_state,
							unsigned short line ) ;
/* set the CID information for a line */
short sc1445x_ae_set_cid_info( sc1445x_ae_state* ae_state, unsigned short line,
				unsigned char month, unsigned char day,
				unsigned char hour, unsigned char minutes,
				const char* number, const char* name ) ;
/* pass CID-related indication for first ring event */
short sc1445x_ae_cid_ind_first_ring( sc1445x_ae_state* ae_state,
						unsigned short line ) ;
/* pass CID-related indication for first ring event */
short sc1445x_ae_cid_ind_off_hook( sc1445x_ae_state* ae_state,
						unsigned short line ) ;
/* pass CID-related indication for first ring event */
short sc1445x_ae_cid_ind_on_hook( sc1445x_ae_state* ae_state,
						unsigned short line ) ;
/* pass CID-related indication for line reversal event */
short sc1445x_ae_cid_line_reversal( sc1445x_ae_state* ae_state,
						unsigned short line ) ;
/* enable DTMF pass-through */
short sc1445x_ae_enable_dtmf_passthrough( sc1445x_ae_state* ae_state,
						unsigned short line ) ;
/* disable DTMF pass-through */
short sc1445x_ae_disable_dtmf_passthrough( sc1445x_ae_state* ae_state,
						unsigned short line ) ;
/* enable detection of specific tones */
short sc1445x_ae_enable_tone_detection( sc1445x_ae_state* ae_state,
					unsigned short line, int tones ) ;
/* disable detection of specific tones */
short sc1445x_ae_disable_tone_detection( sc1445x_ae_state* ae_state,
					unsigned short line, int tones ) ;

#endif

#if defined( SC1445x_AE_PCM_LINES_SUPPORT ) || defined( SC1445x_AE_PHONE_DECT )

/* set the type of a line (ATA/CVM DECT/native DECT -- narrow/wide) */
short sc1445x_ae_set_line_type( sc1445x_ae_state* ae_state, unsigned short line,
						sc1445x_ae_line_type type ) ;
/* get the type of a line (ATA/CVM DECT/native DECT -- narrow/wide) */
short sc1445x_ae_get_line_type( sc1445x_ae_state* ae_state, unsigned short line,
						sc1445x_ae_line_type* type ) ;

#endif




/**********************/
/* PCM DEVICE SUPPORT */
/**********************/

#if defined( SC1445x_AE_BT )

/* directly attach to a PCM device (no VoIP channels involved) */
short sc1445x_ae_attach_to_pcm( sc1445x_ae_state* ae_state,
	       	sc1445x_ae_pcm_slot_id slot, sc1445x_ae_pcm_local_endpoint lep,
		sc1445x_ae_pcm_freq freq, sc1445x_ae_pcm_sample_width width ) ;
/* tear a direct attachment to a PCM device */
short sc1445x_ae_detach_from_pcm( sc1445x_ae_state* ae_state,
					       	sc1445x_ae_pcm_slot_id slot ) ;

#endif


#if defined( SC1445x_AE_SUPPORT_FAX )

/***************/
/* FAX SUPPORT */
/***************/

/* switch an active audio channel to fax */
short sc1445x_ae_switch_to_fax( sc1445x_ae_state* ae_state,
						unsigned short channel ) ;
/* switch an active channel from fax to audio, keeping the previous */
/* codec settings */
short sc1445x_ae_switch_to_audio( sc1445x_ae_state* ae_state,
						unsigned short channel ) ;
/* initialize a fax channel */
short sc1445x_ae_fax_init( sc1445x_ae_state* ae_state, unsigned short channel,
			unsigned short p0DBIN, unsigned short p0DBOUT,
			unsigned short pCEDLength, unsigned short pMDMCmd ) ;

#endif


#endif  /* __AUDIO_ENGINE_API_H__ */

