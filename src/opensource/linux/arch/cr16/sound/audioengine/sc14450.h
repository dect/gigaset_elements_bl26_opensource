#ifndef _IO14450_INCLUDED
#define _IO14450_INCLUDED
/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * GW and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

#define MODEL               4

/*========================== USER DEFINED CONSTANTS =========================*/

#define MAX_DOG_TIME        0x00FF

/*==============================================================
 * SC14450 Programmable Peripheral Assigment PID values
 * Datasheet V0.43 October 11
 * 17-nov-2006 PORT_PULL_UP, PULL_DOWN corrected
 ===============================================================*/

#if !defined( SetPort )
#define SetPort(PORT, IO_CONFIG, PID) SetWord(PORT, IO_CONFIG <<8 | PID); 
#endif

/*----------------------------*/
/*  IO configurations         */
/*----------------------------*/
#define PORT_INPUT          0
#define PORT_PULL_UP        1
#define PORT_PULL_DOWN      2
#define PORT_OUTPUT         3

/*----------------------------*/
/*  PIDs                      */
/*----------------------------*/
#define PID_port            0	
#define PID_CLK100          1	
#define PID_PID_TDOD        2
#define PID_BXTAL           3	
#define PID_PWM0            4	
#define PID_PWM1            5	
#define PID_ECZ1            6	
#define PID_ECZ2            7	
#define PID_PLL2_CLK        8	
#define PID_WTF_IN1         9	
#define PID_WTF_IN2         10	
#define PID_UTX             11	
#define PID_URX             12	
#define PID_SDA1            13	
#define PID_SCL1            14	
#define PID_SDA2            15	
#define PID_SCL2            16	
#define PID_SPI1_DOUT       17	
#define PID_SPI1_DIN        18	
#define PID_SPI1_CLK        19	
#define PID_SPI1_EN         20	
#define PID_SPI2_DOUT       21	
#define PID_SPI2_DIN        22	
#define PID_SPI2_CLK        23	
#define PID_PCM_DO          24	
#define PID_PCM_CLK         25	
#define PID_PCM_FSC         26	
#define PID_PCM_DI          27	
#define PID_AD13            28	
#define PID_AD14            28	
#define PID_AD15            28	
#define PID_AD16            28	
#define PID_AD17            28	
#define PID_AD18            28	
#define PID_AD19            28	
#define PID_AD20            28	
#define PID_AD21            28	
#define PID_AD22            28	
#define PID_AD23            28	
#define PID_SDCKE           29
#define PID_SDCLK           30	
#define PID_SF_ADV          31	
#define PID_BE0n            32	
#define PID_BE1n            33	
#define PID_READY           34	
#define PID_INT0n           35	
#define PID_INT1n           36	
#define PID_INT2n           37	
#define PID_INT3n           38	
#define PID_INT4n           39	
#define PID_INT5n           40	
#define PID_INT6n           41	
#define PID_INT7n           42	
#define PID_INT8n           43	
#define PID_ACS0            44	
#define PID_ACS1            45	
#define PID_ACS2            46	
#define PID_ACS3            47	
#define PID_ACS4            48	
#define PID_PD1             49	
#define PID_PD2             50	
#define PID_PD3             51	
#define PID_PD4             52	
#define PID_PD5             53	
#define PID_PAOUTp          63	
#define PID_PAOUTn          63	
#define PID_ADC0            63	
#define PID_ADC1            63	
#define PID_ADC2            63	

/*======================= Start of automatic generated code =================*/

/*
 * This file is generated from Meta file "Output\SC14450.meta"
 * With version number: 
 * On Sep 25 2006 15:30:17
 */




#define RAM_START                               (0x000000)  /*  */
#define RAM_END                                 (0x005FFF)  /*  */
#define RAM_ADM_START                           (0x008000)  /*  */
#define RAM_ADM_END                             (0x009FFF)  /*  */
#define SHARED_RAM1_START                       (0x010000)  /*  */
#define SHARED_RAM1_END                         (0x012FFF)  /*  */
#define SHARED_RAM2_START                       (0x018000)  /*  */
#define SHARED_RAM2_END                         (0x01BFFF)  /*  */
#define PROGRAM_START                           (0x0F0000)  /*  */
#define PROGRAM_END                             (0xFEEFFF)  /*  */
#define BOOT_ROM_START                          (0xFEF000)  /*  */
#define BOOT_ROM_END                            (0xFEF7FF)  /*  */



/*====================================================*/
struct __ACCESS1_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_EN_ACCESS_BUS                      : 1;
    WORD BITFLD_ACKn                               : 1;
    WORD BITFLD_SCL_VAL                            : 1;
    WORD BITFLD_SDA_VAL                            : 1;
    WORD BITFLD_ACCESS_INT                         : 1;
    WORD BITFLD_EN_ACCESS_INT                      : 1;
    WORD BITFLD_SCL_OD                             : 1;
    WORD BITFLD_SDA_OD                             : 1;
    WORD BITFLD_SCK_NUM                            : 1;
    WORD BITFLD_SCK_SEL                            : 2;
};

#define EN_ACCESS_BUS                      (0x0001)
#define ACKn                               (0x0002)
#define SCL_VAL                            (0x0004)
#define SDA_VAL                            (0x0008)
#define ACCESS_INT                         (0x0010)
#define EN_ACCESS_INT                      (0x0020)
#define SCL_OD                             (0x0040)
#define SDA_OD                             (0x0080)
#define SCK_NUM                            (0x0100)
#define SCK_SEL                            (0x0600)


/*====================================================*/
struct __ACCESS2_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_EN_ACCESS_BUS                      : 1;
    WORD BITFLD_ACKn                               : 1;
    WORD BITFLD_SCL_VAL                            : 1;
    WORD BITFLD_SDA_VAL                            : 1;
    WORD BITFLD_ACCESS_INT                         : 1;
    WORD BITFLD_EN_ACCESS_INT                      : 1;
    WORD BITFLD_SCL_OD                             : 1;
    WORD BITFLD_SDA_OD                             : 1;
    WORD BITFLD_SCK_NUM                            : 1;
    WORD BITFLD_SCK_SEL                            : 2;
};

#define EN_ACCESS_BUS                      (0x0001)
#define ACKn                               (0x0002)
#define SCL_VAL                            (0x0004)
#define SDA_VAL                            (0x0008)
#define ACCESS_INT                         (0x0010)
#define EN_ACCESS_INT                      (0x0020)
#define SCL_OD                             (0x0040)
#define SDA_OD                             (0x0080)
#define SCK_NUM                            (0x0100)
#define SCK_SEL                            (0x0600)


/*====================================================*/
struct __AD_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_ADC_START                          : 1;
    WORD BITFLD_ADC_AUTO                           : 1;
    WORD BITFLD_ADC_ALT                            : 1;
    WORD BITFLD_ADC_IN_3_0                         : 4;
    WORD BITFLD_ADC_TEST                           : 4;
    WORD BITFLD_ADC_INT                            : 1;
    WORD BITFLD_ADC_MINT                           : 1;
    WORD                                           : 1;
    WORD BITFLD_ADC0_PR_DIS                        : 1;
    WORD BITFLD_ADC1_PR_DIS                        : 1;
};

#define ADC_START                          (0x0001)
#define ADC_AUTO                           (0x0002)
#define ADC_ALT                            (0x0004)
#define ADC_IN_3_0                         (0x0078)
#define ADC_TEST                           (0x0780)
#define ADC_INT                            (0x0800)
#define ADC_MINT                           (0x1000)
#define ADC0_PR_DIS                        (0x4000)
#define ADC1_PR_DIS                        (0x8000)


/*====================================================*/
struct __AD_CTRL1_REG
/*====================================================*/
{
    WORD BITFLD_ADC_IN_3_0_1                       : 4;
};

#define ADC_IN_3_0_1                       (0x000F)


/*====================================================*/
struct __ADC0_REG
/*====================================================*/
{
    WORD BITFLD_ADC0_VAL                           : 10;
};

#define ADC0_VAL                           (0x03FF)


/*====================================================*/
struct __ADC1_REG
/*====================================================*/
{
    WORD BITFLD_ADC1_VAL                           : 10;
};

#define ADC1_VAL                           (0x03FF)


/*====================================================*/
struct __BANDGAP_REG
/*====================================================*/
{
    WORD BITFLD_BANDGAP_VI                         : 4;
    WORD BITFLD_BANDGAP_VIT                        : 2;
    WORD BITFLD_BANDGAP_I                          : 3;
};

#define BANDGAP_VI                         (0x000F)
#define BANDGAP_VIT                        (0x0030)
#define BANDGAP_I                          (0x01C0)


#if 0
/*====================================================*/
struct __BAT_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_REG_ON                             : 1;
    WORD BITFLD_LDO1_ON                            : 1;
    WORD BITFLD_DC_ON                              : 1;
    WORD BITFLD_LDO1_LEVEL                         : 3;
    WORD BITFLD_DC_MODE                            : 2;
    WORD BITFLD_DC_VOUT                            : 3;
    WORD BITFLD_DC_HYST                            : 1;
    WORD BITFLD_DC_IMAX                            : 1;
    WORD BITFLD_DC_FREQ                            : 2;
    WORD BITFLD_DC_CLK_SEL                         : 1;
};

#define REG_ON                             (0x0001)
#define LDO1_ON                            (0x0002)
#define DC_ON                              (0x0004)
#define LDO1_LEVEL                         (0x0038)
#define DC_MODE                            (0x00C0)
#define DC_VOUT                            (0x0700)
#define DC_HYST                            (0x0800)
#define DC_IMAX                            (0x1000)
#define DC_FREQ                            (0x6000)
#define DC_CLK_SEL                         (0x8000)


/*====================================================*/
struct __BAT_CTRL2_REG
/*====================================================*/
{
    WORD BITFLD_CHARGE_LEVEL                       : 5;
    WORD BITFLD_CHARGE_ON                          : 1;
    WORD BITFLD_NTC_DISABLE                        : 1;
    WORD                                           : 3;
    WORD BITFLD_CHARGE_CUR                         : 3;
    WORD BITFLD_SOC_ON                             : 1;
    WORD BITFLD_SOC_CAL                            : 1;
    WORD BITFLD_SOC_TEST                           : 1;
};

#define CHARGE_LEVEL                       (0x001F)
#define CHARGE_ON                          (0x0020)
#define NTC_DISABLE                        (0x0040)
#define CHARGE_CUR                         (0x1C00)
#define SOC_ON                             (0x2000)
#define SOC_CAL                            (0x4000)
#define SOC_TEST                           (0x8000)


/*====================================================*/
struct __BAT_STATUS_REG
/*====================================================*/
{
    WORD BITFLD_VBAT3_STS                          : 1;
    WORD BITFLD_PON_STS                            : 1;
    WORD BITFLD_CHARGE_STS                         : 1;
    WORD BITFLD_CHARGE_I_LIMIT                     : 1;
    WORD BITFLD_TWO_CELL_STS                       : 1;
    WORD BITFLD_VBUS_OK                            : 1;
};

#define VBAT3_STS                          (0x0001)
#define PON_STS                            (0x0002)
#define CHARGE_STS                         (0x0004)
#define CHARGE_I_LIMIT                     (0x0008)
#define TWO_CELL_STS                       (0x0010)
#define VBUS_OK                            (0x0020)
#endif


/*====================================================*/
struct __BMC_CTRL_REG
/*====================================================*/
{
    WORD                                           : 4;
    WORD BITFLD_GAUSS_REF                          : 1;
    WORD BITFLD_RSSI_RANGE                         : 1;
    WORD BITFLD_RSSI_TDO                           : 1;
    WORD                                           : 1;
    WORD BITFLD_SIO_PD                             : 1;
    WORD                                           : 4;
    WORD BITFLD_BCNT_INH                           : 1;
};

#define GAUSS_REF                          (0x0010)
#define RSSI_RANGE                         (0x0020)
#define RSSI_TDO                           (0x0040)
#define SIO_PD                             (0x0100)
#define BCNT_INH                           (0x2000)


/*====================================================*/
struct __BMC_CTRL2_REG
/*====================================================*/
{
    WORD BITFLD_DAC_TEST                           : 3;
    WORD BITFLD_RCV_CTL                            : 5;
};

#define DAC_TEST                           (0x0007)
#define RCV_CTL                            (0x00F8)


/*====================================================*/
struct __CACHE_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_CACHE_SIZE                         : 3;
    WORD BITFLD_CACHE_TEST                         : 1;
    WORD BITFLD_CACHE_MODE                         : 2;
    WORD BITFLD_CACHE_LOCK                         : 1;
    WORD BITFLD_CACHE_PAR                          : 1;
    WORD BITFLD_ICACHE_B_SIZE                      : 1;
    WORD BITFLD_ICACHE_PF_EN                       : 1;
    WORD BITFLD_DCACHE_B_SIZE                      : 1;
    WORD BITFLD_DCACHE_PF_EN                       : 1;
    WORD BITFLD_TRACE_SIZE                         : 2;
    WORD BITFLD_TRACE_CTRL                         : 1;
    WORD BITFLD_TRACE_MODE                         : 1;
};

#define CACHE_SIZE                         (0x0007)
#define CACHE_TEST                         (0x0008)
#define CACHE_MODE                         (0x0030)
#define CACHE_LOCK                         (0x0040)
#define CACHE_PAR                          (0x0080)
#define ICACHE_B_SIZE                      (0x0100)
#define ICACHE_PF_EN                       (0x0200)
#define DCACHE_B_SIZE                      (0x0400)
#define DCACHE_PF_EN                       (0x0800)
#define TRACE_SIZE                         (0x3000)
#define TRACE_CTRL                         (0x4000)
#define TRACE_MODE                         (0x8000)


/*====================================================*/
struct __CACHE_LEN0_REG
/*====================================================*/
{
    WORD BITFLD_CACHE_LEN0                         : 9;
};

#define CACHE_LEN0                         (0x01FF)


/*====================================================*/
struct __CACHE_START0_REG
/*====================================================*/
{
    WORD BITFLD_CACHE_START0                       : 9;
};

#define CACHE_START0                       (0x01FF)


/*====================================================*/
struct __CACHE_LEN1_REG
/*====================================================*/
{
    WORD BITFLD_CACHE_LEN1                         : 9;
};

#define CACHE_LEN1                         (0x01FF)


/*====================================================*/
struct __CACHE_START1_REG
/*====================================================*/
{
    WORD BITFLD_CACHE_START1                       : 9;
};

#define CACHE_START1                       (0x01FF)


/*====================================================*/
struct __CACHE_STATUS_REG
/*====================================================*/
{
    WORD BITFLD_TRACE_IDX                          : 10;
    WORD BITFLD_ICACHE_HIT                         : 1;
    WORD BITFLD_DCACHE_HIT                         : 1;
    WORD BITFLD_CACHE_TOUCH                        : 1;
    WORD BITFLD_TRACE_TOUCH                        : 1;
    WORD BITFLD_reserved                           : 2;
};

#define TRACE_IDX                          (0x03FF)
#define ICACHE_HIT                         (0x0400)
#define DCACHE_HIT                         (0x0800)
#define CACHE_TOUCH                        (0x1000)
#define TRACE_TOUCH                        (0x2000)


/*====================================================*/
struct __CCU_MODE_REG
/*====================================================*/
{
    WORD BITFLD_CCU_MODE                           : 2;
    WORD BITFLD_CCU_BIT_SWAP                       : 1;
    WORD BITFLD_CCU_BYTE_SWAP                      : 1;
};

#define CCU_MODE                           (0x0003)
#define CCU_BIT_SWAP                       (0x0004)
#define CCU_BYTE_SWAP                      (0x0008)


/*====================================================*/
struct __CHIP_REVISION_REG
/*====================================================*/
{
    WORD                                           : 4;
    WORD BITFLD_REVISION_ID                        : 4;
};

#define REVISION_ID                        (0x00F0)


/*====================================================*/
struct __CLASSD_BUZZER_REG
/*====================================================*/
{
    WORD BITFLD_CLASSD_BUZ_GAIN                    : 4;
    WORD BITFLD_CLASSD_BUZ_MODE                    : 1;
};

#define CLASSD_BUZ_GAIN                    (0x000F)
#define CLASSD_BUZ_MODE                    (0x0010)


/*====================================================*/
struct __CLASSD_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_CLASSD_PD                          : 1;
    WORD BITFLD_CLASSD_PROT                        : 1;
    WORD BITFLD_CLASSD_VOUT                        : 2;
    WORD BITFLD_CLASSD_CLIP                        : 3;
    WORD BITFLD_CLASSD_INT_BIT                     : 1;
    WORD BITFLD_CLASSD_DITH_D                      : 2;
    WORD BITFLD_CLASSD_DITH_A                      : 2;
    WORD BITFLD_CLASSD_MODE                        : 1;
    WORD BITFLD_CLASSD_MINT                        : 1;
    WORD BITFLD_CLASSD_MOPEN                       : 1;
    WORD BITFLD_CLASSD_POPEN                       : 1;
};

#define CLASSD_PD                          (0x0001)
#define CLASSD_PROT                        (0x0002)
#define CLASSD_VOUT                        (0x000C)
#define CLASSD_CLIP                        (0x0070)
#define CLASSD_INT_BIT                     (0x0080)
#define CLASSD_DITH_D                      (0x0300)
#define CLASSD_DITH_A                      (0x0C00)
#define CLASSD_MODE                        (0x1000)
#define CLASSD_MINT                        (0x2000)
#define CLASSD_MOPEN                       (0x4000)
#define CLASSD_POPEN                       (0x8000)


/*====================================================*/
struct __CLASSD_TEST_REG
/*====================================================*/
{
    WORD BITFLD_CLASSD_SWITCH                      : 1;
    WORD BITFLD_CLASSD_RST_D                       : 1;
    WORD BITFLD_CLASSD_RST_A                       : 1;
    WORD BITFLD_CLASSD_FORCE                       : 1;
    WORD BITFLD_CLASSD_DIG_MUTE                    : 1;
    WORD BITFLD_CLASSD_ANA_TEST                    : 3;
};

#define CLASSD_SWITCH                      (0x0001)
#define CLASSD_RST_D                       (0x0002)
#define CLASSD_RST_A                       (0x0004)
#define CLASSD_FORCE                       (0x0008)
#define CLASSD_DIG_MUTE                    (0x0010)
#define CLASSD_ANA_TEST                    (0x00E0)


/*====================================================*/
struct __CLASSD_NR_REG
/*====================================================*/
{
    WORD BITFLD_CLASSD_NR_ACTIVE                   : 1;
    WORD BITFLD_CLASSD_NR_LVL                      : 3;
    WORD BITFLD_CLASSD_NR_TON                      : 4;
    WORD BITFLD_CLASSD_NR_HYST                     : 2;
    WORD BITFLD_CLASSD_NR_ZERO                     : 2;
};

#define CLASSD_NR_ACTIVE                   (0x0001)
#define CLASSD_NR_LVL                      (0x000E)
#define CLASSD_NR_TON                      (0x00F0)
#define CLASSD_NR_HYST                     (0x0300)
#define CLASSD_NR_ZERO                     (0x0C00)


/*====================================================*/
struct __CLK_AMBA_REG
/*====================================================*/
{
    WORD BITFLD_HCLK_DIV                           : 3;
    WORD BITFLD_PCLK_DIV                           : 2;
    WORD BITFLD_SRAM1_EN                           : 1;
    WORD BITFLD_SRAM2_EN                           : 1;
    WORD BITFLD_MCRAM1_EN                          : 1;
    WORD BITFLD_MCRAM2_EN                          : 1;
    WORD BITFLD_HCLK_PRE                           : 1;
};

#define HCLK_DIV                           (0x0007)
#define PCLK_DIV                           (0x0018)
#define SRAM1_EN                           (0x0020)
#define SRAM2_EN                           (0x0040)
#define MCRAM1_EN                          (0x0080)
#define MCRAM2_EN                          (0x0100)
#define HCLK_PRE                           (0x0200)


/*====================================================*/
struct __CLK_AUX_REG
/*====================================================*/
{
    WORD BITFLD_BXTAL_DIV                          : 3;
    WORD BITFLD_BXTAL_EN                           : 2;
    WORD BITFLD_OWI_DIV                            : 2;
    WORD BITFLD_OWI_SEL                            : 1;
    WORD BITFLD_XDIV                               : 1;
    WORD BITFLD_USB_TST_CLK                        : 1;
};

#define BXTAL_DIV                          (0x0007)
#define BXTAL_EN                           (0x0018)
#define OWI_DIV                            (0x0060)
#define OWI_SEL                            (0x0080)
#define XDIV                               (0x0100)
#define USB_TST_CLK                        (0x0200)


/*====================================================*/
struct __CLK_CODEC_DIV_REG
/*====================================================*/
{
    WORD BITFLD_CODEC_DIV                          : 7;
    WORD BITFLD_CODEC_CLK_SEL                      : 2;
};

#define CODEC_DIV                          (0x007F)
#define CODEC_CLK_SEL                      (0x0180)


/*====================================================*/
struct __CLK_CODEC_REG
/*====================================================*/
{
    WORD BITFLD_CLK_MAIN_SEL                       : 2;
    WORD BITFLD_CLK_AD_SEL                         : 2;
    WORD BITFLD_CLK_DA_CLASSD_SEL                  : 2;
    WORD BITFLD_CLK_DA_LSR_SEL                     : 2;
    WORD BITFLD_CLK_PCM_SEL                        : 2;
};

#define CLK_MAIN_SEL                       (0x0003)
#define CLK_AD_SEL                         (0x000C)
#define CLK_DA_CLASSD_SEL                  (0x0030)
#define CLK_DA_LSR_SEL                     (0x00C0)
#define CLK_PCM_SEL                        (0x0300)


/*====================================================*/
struct __CLK_DSP_REG
/*====================================================*/
{
    WORD BITFLD_CLK_DSP1_DIV                       : 3;
    WORD BITFLD_CLK_DSP1_EN                        : 1;
    WORD BITFLD_CLK_DSP2_DIV                       : 3;
    WORD BITFLD_CLK_DSP2_EN                        : 1;
};

#define CLK_DSP1_DIV                       (0x0007)
#define CLK_DSP1_EN                        (0x0008)
#define CLK_DSP2_DIV                       (0x0070)
#define CLK_DSP2_EN                        (0x0080)


/*====================================================*/
struct __CLK_FREQ_TRIM_REG
/*====================================================*/
{
    WORD BITFLD_FINE_ADJ                           : 5;
    WORD BITFLD_COARSE_ADJ                         : 3;
    WORD BITFLD_CL_SEL                             : 1;
    WORD BITFLD_OSC_OK                             : 1;
};

#define FINE_ADJ                           (0x001F)
#define COARSE_ADJ                         (0x00E0)
#define CL_SEL                             (0x0100)
#define OSC_OK                             (0x0200)


/*====================================================*/
struct __CLK_PER_DIV_REG
/*====================================================*/
{
    WORD BITFLD_PER_DIV                            : 7;
    WORD BITFLD_PER_CLK_SEL                        : 2;
};

#define PER_DIV                            (0x007F)
#define PER_CLK_SEL                        (0x0180)


/*====================================================*/
struct __CLK_PER10_DIV_REG
/*====================================================*/
{
    WORD BITFLD_PER10_DIV                          : 3;
    WORD BITFLD_PER10_CLK_SEL                      : 2;
    WORD BITFLD_PER20_DIV                          : 3;
};

#define PER10_DIV                          (0x0007)
#define PER10_CLK_SEL                      (0x0018)
#define PER20_DIV                          (0x00E0)


/*====================================================*/
struct __CLK_PLL1_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_TESTMODE_SEL                       : 1;
    WORD BITFLD_CP_ON                              : 1;
    WORD BITFLD_PLL1_OUT_DIV                       : 1;
    WORD BITFLD_PLL_CLK_SEL                        : 1;
    WORD BITFLD_VCO_ON                             : 1;
    WORD                                           : 1;
    WORD BITFLD_HF_SEL                             : 1;
    WORD BITFLD_PLL_DIP_DIV                        : 4;
    WORD BITFLD_DYN_SW                             : 1;
};

#define TESTMODE_SEL                       (0x0001)
#define CP_ON                              (0x0002)
#define PLL1_OUT_DIV                       (0x0004)
#define PLL_CLK_SEL                        (0x0008)
#define VCO_ON                             (0x0010)
#define HF_SEL                             (0x0040)
#define PLL_DIP_DIV                        (0x0780)
#define DYN_SW                             (0x0800)


/*====================================================*/
struct __CLK_PLL1_DIV_REG
/*====================================================*/
{
    WORD BITFLD_XD1                                : 2;
    WORD BITFLD_VD1                                : 3;
};

#define XD1                                (0x0003)
#define VD1                                (0x001C)


/*====================================================*/
struct __CLK_PLL2_DIV_REG
/*====================================================*/
{
    WORD BITFLD_XD                                 : 7;
    WORD BITFLD_DIV2                               : 1;
    WORD BITFLD_DIV1                               : 1;
    WORD BITFLD_VD                                 : 4;
    WORD BITFLD_DIV4                               : 1;
    WORD BITFLD_DIV3                               : 1;
    WORD BITFLD_DIV5                               : 1;
};

#define XD                                 (0x007F)
#define DIV2                               (0x0080)
#define DIV1                               (0x0100)
#define VD                                 (0x1E00)
#define DIV4                               (0x2000)
#define DIV3                               (0x4000)
#define DIV5                               (0x8000)


/*====================================================*/
struct __CLK_XTAL_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_LDO_XTAL_ON                        : 1;
    WORD BITFLD_XTAL_SUPPLY                        : 1;
    WORD BITFLD_AVD_XTAL_OK                        : 1;
    WORD BITFLD_LDO_RFCLK_ON                       : 2;
    WORD BITFLD_RFCLK_SUPPLY                       : 1;
    WORD BITFLD_LDO_RFCLK_OK                       : 1;
    WORD BITFLD_XTAL_EXTRA_CV                      : 1;
};

#define LDO_XTAL_ON                        (0x0001)
#define XTAL_SUPPLY                        (0x0002)
#define AVD_XTAL_OK                        (0x0004)
#define LDO_RFCLK_ON                       (0x0018)
#define RFCLK_SUPPLY                       (0x0020)
#define LDO_RFCLK_OK                       (0x0040)
#define XTAL_EXTRA_CV                      (0x0080)


/*====================================================*/
struct __CODEC_ADDA_REG
/*====================================================*/
{
    WORD BITFLD_DA_PD                              : 1;
    WORD BITFLD_AD_PD                              : 1;
    WORD BITFLD_DA_CADJ                            : 2;
    WORD BITFLD_AD_CADJ                            : 2;
    WORD BITFLD_DA_DITH_OFF                        : 1;
    WORD BITFLD_AD_DITH_OFF                        : 1;
    WORD BITFLD_DA_HBW                             : 1;
    WORD BITFLD_LPF_PD                             : 1;
    WORD BITFLD_LPF_BW                             : 3;
    WORD BITFLD_ADC_VREF_LSR                       : 2;
    WORD BITFLD_AUTO_SYNC                          : 1;
};

#define DA_PD                              (0x0001)
#define AD_PD                              (0x0002)
#define DA_CADJ                            (0x000C)
#define AD_CADJ                            (0x0030)
#define DA_DITH_OFF                        (0x0040)
#define AD_DITH_OFF                        (0x0080)
#define DA_HBW                             (0x0100)
#define LPF_PD                             (0x0200)
#define LPF_BW                             (0x1C00)
#define ADC_VREF_LSR                       (0x6000)
#define AUTO_SYNC                          (0x8000)


/*====================================================*/
struct __CODEC_LSR_REG
/*====================================================*/
{
    WORD BITFLD_LSRP_MODE                          : 2;
    WORD BITFLD_LSRP_PD                            : 1;
    WORD BITFLD_LSRN_MODE                          : 2;
    WORD BITFLD_LSRN_PD                            : 1;
    WORD BITFLD_LSRATT                             : 3;
    WORD BITFLD_LSREN_SE                           : 1;
};

#define LSRP_MODE                          (0x0003)
#define LSRP_PD                            (0x0004)
#define LSRN_MODE                          (0x0018)
#define LSRN_PD                            (0x0020)
#define LSRATT                             (0x01C0)
#define LSREN_SE                           (0x0200)


/*====================================================*/
struct __CODEC_MIC_REG
/*====================================================*/
{
    WORD BITFLD_MIC_MODE                           : 2;
    WORD BITFLD_MIC_PD                             : 1;
    WORD BITFLD_MIC_MUTE                           : 1;
    WORD BITFLD_MIC_GAIN                           : 4;
    WORD BITFLD_MIC_OFFCOM_SG                      : 1;
    WORD BITFLD_MIC_OFFCOM_ON                      : 1;
    WORD BITFLD_DSP_CTRL                           : 1;
    WORD BITFLD_MICH_ON                            : 1;
    WORD BITFLD_MIC_CADJ                           : 2;
};

#define MIC_MODE                           (0x0003)
#define MIC_PD                             (0x0004)
#define MIC_MUTE                           (0x0008)
#define MIC_GAIN                           (0x00F0)
#define MIC_OFFCOM_SG                      (0x0100)
#define MIC_OFFCOM_ON                      (0x0200)
#define DSP_CTRL                           (0x0400)
#define MICH_ON                            (0x0800)
#define MIC_CADJ                           (0x3000)


/*====================================================*/
struct __CODEC_TEST_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_TCR                                : 12;
    WORD BITFLD_COR_ON                             : 1;
    WORD BITFLD_COR_STAT                           : 1;
};

#define TCR                                (0x0FFF)
#define COR_ON                             (0x1000)
#define COR_STAT                           (0x2000)


/*====================================================*/
struct __CODEC_TONE_REG
/*====================================================*/
{
    WORD BITFLD_CID_PD                             : 1;
    WORD BITFLD_CID_PR_DIS                         : 1;
    WORD BITFLD_RNG_CMP_PD                         : 1;
};

#define CID_PD                             (0x0001)
#define CID_PR_DIS                         (0x0002)
#define RNG_CMP_PD                         (0x0004)


/*====================================================*/
struct __CODEC_VREF_REG
/*====================================================*/
{
    WORD BITFLD_VREF_PD                            : 1;
    WORD                                           : 1;
    WORD BITFLD_VREF_FILT_CADJ                     : 16;
    WORD BITFLD_VREF_INIT                          : 1;
    WORD BITFLD_AMP1V5_PD                          : 1;
    WORD BITFLD_VREF_BG_PD                         : 1;
    WORD BITFLD_BIAS_PD                            : 1;
    WORD BITFLD_AGND_LSR_PD                        : 1;
    WORD BITFLD_REFINT_PD                          : 1;
};

#define VREF_PD                            (0x0001)
#define VREF_FILT_CADJ                     (0x80000000)
#define VREF_INIT                          (0x0010)
#define AMP1V5_PD                          (0x0020)
#define VREF_BG_PD                         (0x0040)
#define BIAS_PD                            (0x0080)
#define AGND_LSR_PD                        (0x0100)
#define REFINT_PD                          (0x0200)


/*====================================================*/
struct __CP_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_CP_EN                              : 2;
    WORD BITFLD_CP_LEVEL                           : 2;
    WORD BITFLD_CP_PWM                             : 1;
    WORD BITFLD_CP_FREQ                            : 1;
    WORD BITFLD_CP_PARALLEL                        : 1;
    WORD BITFLD_CP_EXTRA                           : 1;
    WORD BITFLD_CP_MODE                            : 1;
    WORD                                           : 3;
    WORD BITFLD_CP_TEST_ADC1                       : 1;
    WORD BITFLD_CP_TEST_ADC2                       : 1;
};

#define CP_EN                              (0x0003)
#define CP_LEVEL                           (0x000C)
#define CP_PWM                             (0x0010)
#define CP_FREQ                            (0x0020)
#define CP_PARALLEL                        (0x0040)
#define CP_EXTRA                           (0x0080)
#define CP_MODE                            (0x0100)
#define CP_TEST_ADC1                       (0x1000)
#define CP_TEST_ADC2                       (0x2000)


/*====================================================*/
struct __DEBUG_REG
/*====================================================*/
{
    WORD BITFLD_CLK100_EDGE                        : 1;
    WORD BITFLD_CLK100_NEG                         : 1;
    WORD BITFLD_CLK100_POS                         : 1;
    WORD BITFLD_CLK100_SRC                         : 1;
    WORD BITFLD_ENV_B01                            : 1;
    WORD                                           : 2;
    WORD BITFLD_SW_RESET                           : 1;
};

#define CLK100_EDGE                        (0x0001)
#define CLK100_NEG                         (0x0002)
#define CLK100_POS                         (0x0004)
#define CLK100_SRC                         (0x0008)
#define ENV_B01                            (0x0010)
#define SW_RESET                           (0x0080)


/*====================================================*/
struct __DIP_PC_REG
/*====================================================*/
{
    WORD BITFLD_DIP_PC                             : 8;
    WORD BITFLD_DIP_BANK                           : 1;
};

#define DIP_PC                             (0x00FF)
#define DIP_BANK                           (0x0100)


/*====================================================*/
struct __DIP_STATUS_REG
/*====================================================*/
{
    WORD BITFLD_DIP_INT_VEC                        : 4;
    WORD BITFLD_PD1_INT                            : 1;
    WORD BITFLD_DIP_BRK_INT                        : 1;
    WORD BITFLD_PRESCALER                          : 1;
    WORD BITFLD_URST                               : 1;
};

#define DIP_INT_VEC                        (0x000F)
#define PD1_INT                            (0x0010)
#define DIP_BRK_INT                        (0x0020)
#define PRESCALER                          (0x0040)
#define URST                               (0x0080)


/*====================================================*/
struct __DIP_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_DIP_INT_VEC                        : 4;
    WORD BITFLD_PD1_INT                            : 1;
    WORD BITFLD_DIP_BRK_INT                        : 1;
    WORD BITFLD_PRESCALER                          : 1;
    WORD BITFLD_URST                               : 1;
};

#define DIP_INT_VEC                        (0x000F)
#define PD1_INT                            (0x0010)
#define DIP_BRK_INT                        (0x0020)
#define PRESCALER                          (0x0040)
#define URST                               (0x0080)


/*====================================================*/
struct __DIP_CTRL1_REG
/*====================================================*/
{
    WORD BITFLD_DIP_VNMI_VEC                       : 4;
};

#define DIP_VNMI_VEC                       (0x000F)


/*====================================================*/
struct __DIP_STATUS1_REG
/*====================================================*/
{
    WORD BITFLD_DIP_VNMI_VEC                       : 4;
};

#define DIP_VNMI_VEC                       (0x000F)


/*====================================================*/
struct __DIP_CTRL2_REG
/*====================================================*/
{
    WORD BITFLD_DBUF                               : 1;
    WORD BITFLD_PRE_ACT                            : 1;
    WORD BITFLD_BRK_PRE_OVR                        : 1;
    WORD                                           : 1;
    WORD BITFLD_PD1_INT                            : 1;
    WORD BITFLD_DIP_BRK                            : 1;
    WORD                                           : 1;
    WORD BITFLD_EN_8DIV9                           : 1;
    WORD BITFLD_SLOTCNT_RES                        : 1;
};

#define DBUF                               (0x0001)
#define PRE_ACT                            (0x0002)
#define BRK_PRE_OVR                        (0x0004)
#define PD1_INT                            (0x0010)
#define DIP_BRK                            (0x0020)
#define EN_8DIV9                           (0x0080)
#define SLOTCNT_RES                        (0x0100)


/*====================================================*/
struct __DIP_MOD_SEL_REG
/*====================================================*/
{
    WORD BITFLD_ARMOD0                             : 1;
    WORD BITFLD_ARMOD1                             : 1;
    WORD BITFLD_ARMOD2                             : 1;
    WORD BITFLD_ARMOD3                             : 1;
    WORD BITFLD_AWMOD0                             : 1;
    WORD BITFLD_AWMOD1                             : 1;
    WORD BITFLD_AWMOD2                             : 1;
    WORD BITFLD_AWMOD3                             : 1;
};

#define ARMOD0                             (0x0001)
#define ARMOD1                             (0x0002)
#define ARMOD2                             (0x0004)
#define ARMOD3                             (0x0008)
#define AWMOD0                             (0x0010)
#define AWMOD1                             (0x0020)
#define AWMOD2                             (0x0040)
#define AWMOD3                             (0x0080)


/*====================================================*/
struct __DIP_SLOT_NUMBER_REG
/*====================================================*/
{
    WORD BITFLD_SLOT_CNTER                         : 5;
};

#define SLOT_CNTER                         (0x001F)


/*====================================================*/
struct __DIP_USB_PHASE_REG
/*====================================================*/
{
    WORD BITFLD_DIP_USB_PHASE                      : 13;
};

#define DIP_USB_PHASE                      (0x1FFF)


/*====================================================*/
struct __DMA0_A_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_A_STARTH                      : 9;
};

#define DMAx_A_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA1_A_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_A_STARTH                      : 9;
};

#define DMAx_A_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA2_A_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_A_STARTH                      : 9;
};

#define DMAx_A_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA3_A_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_A_STARTH                      : 9;
};

#define DMAx_A_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA0_B_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_B_STARTH                      : 9;
};

#define DMAx_B_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA1_B_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_B_STARTH                      : 9;
};

#define DMAx_B_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA2_B_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_B_STARTH                      : 9;
};

#define DMAx_B_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA3_B_STARTH_REG
/*====================================================*/
{
    WORD BITFLD_DMAx_B_STARTH                      : 9;
};

#define DMAx_B_STARTH                      (0x01FF)


/*====================================================*/
struct __DMA0_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_DMA_ON                             : 1;
    WORD BITFLD_BW                                 : 2;
    WORD BITFLD_DINT_MODE                          : 1;
    WORD BITFLD_DREQ_MODE                          : 1;
    WORD BITFLD_BINC                               : 1;
    WORD BITFLD_AINC                               : 1;
    WORD BITFLD_CIRCULAR                           : 1;
    WORD BITFLD_DMA_PRIO                           : 2;
};

#define DMA_ON                             (0x0001)
#define BW                                 (0x0006)
#define DINT_MODE                          (0x0008)
#define DREQ_MODE                          (0x0010)
#define BINC                               (0x0020)
#define AINC                               (0x0040)
#define CIRCULAR                           (0x0080)
#define DMA_PRIO                           (0x0300)


/*====================================================*/
struct __DMA1_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_DMA_ON                             : 1;
    WORD BITFLD_BW                                 : 2;
    WORD BITFLD_DINT_MODE                          : 1;
    WORD BITFLD_DREQ_MODE                          : 1;
    WORD BITFLD_BINC                               : 1;
    WORD BITFLD_AINC                               : 1;
    WORD BITFLD_CIRCULAR                           : 1;
    WORD BITFLD_DMA_PRIO                           : 2;
};

#define DMA_ON                             (0x0001)
#define BW                                 (0x0006)
#define DINT_MODE                          (0x0008)
#define DREQ_MODE                          (0x0010)
#define BINC                               (0x0020)
#define AINC                               (0x0040)
#define CIRCULAR                           (0x0080)
#define DMA_PRIO                           (0x0300)


/*====================================================*/
struct __DMA2_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_DMA_ON                             : 1;
    WORD BITFLD_BW                                 : 2;
    WORD BITFLD_DINT_MODE                          : 1;
    WORD BITFLD_DREQ_MODE                          : 1;
    WORD BITFLD_BINC                               : 1;
    WORD BITFLD_AINC                               : 1;
    WORD BITFLD_CIRCULAR                           : 1;
    WORD BITFLD_DMA_PRIO                           : 2;
};

#define DMA_ON                             (0x0001)
#define BW                                 (0x0006)
#define DINT_MODE                          (0x0008)
#define DREQ_MODE                          (0x0010)
#define BINC                               (0x0020)
#define AINC                               (0x0040)
#define CIRCULAR                           (0x0080)
#define DMA_PRIO                           (0x0300)


/*====================================================*/
struct __DMA3_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_DMA_ON                             : 1;
    WORD BITFLD_BW                                 : 2;
    WORD BITFLD_DINT_MODE                          : 1;
    WORD BITFLD_DREQ_MODE                          : 1;
    WORD BITFLD_BINC                               : 1;
    WORD BITFLD_AINC                               : 1;
    WORD BITFLD_CIRCULAR                           : 1;
    WORD BITFLD_DMA_PRIO                           : 2;
};

#define DMA_ON                             (0x0001)
#define BW                                 (0x0006)
#define DINT_MODE                          (0x0008)
#define DREQ_MODE                          (0x0010)
#define BINC                               (0x0020)
#define AINC                               (0x0040)
#define CIRCULAR                           (0x0080)
#define DMA_PRIO                           (0x0300)


/*====================================================*/
struct __DSP_CLASSD_BUZZOFF_REG
/*====================================================*/
{
    WORD                                           : 15;
    WORD BITFLD_BUZZOFF                            : 1;
};

#define BUZZOFF                            (0x8000)


/*====================================================*/
struct __DSP_CODEC_MIC_GAIN_REG
/*====================================================*/
{
    WORD                                           : 12;
    WORD BITFLD_DSP_MIC_GAIN                       : 4;
};

#define DSP_MIC_GAIN                       (0xF000)


/*====================================================*/
struct __DSP1_CTRL_REG
/*====================================================*/
{
    WORD                                           : 2;
    WORD BITFLD_DSP_EN                             : 1;
    WORD BITFLD_DSP_CLK_EN                         : 1;
    WORD BITFLD_DSP_CR16_INT                       : 1;
    WORD                                           : 3;
    WORD BITFLD_DBG_EN                             : 1;
};

#define DSP_EN                             (0x0004)
#define DSP_CLK_EN                         (0x0008)
#define DSP_CR16_INT                       (0x0010)
#define DBG_EN                             (0x0100)


/*====================================================*/
struct __DSP2_CTRL_REG
/*====================================================*/
{
    WORD                                           : 2;
    WORD BITFLD_DSP_EN                             : 1;
    WORD BITFLD_DSP_CLK_EN                         : 1;
    WORD BITFLD_DSP_CR16_INT                       : 1;
    WORD                                           : 3;
    WORD BITFLD_DBG_EN                             : 1;
};

#define DSP_EN                             (0x0004)
#define DSP_CLK_EN                         (0x0008)
#define DSP_CR16_INT                       (0x0010)
#define DBG_EN                             (0x0100)


/*====================================================*/
struct __DSP1_INT_PRIO1_REG
/*====================================================*/
{
    WORD BITFLD_DSP_INT2_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT1_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT0_PRIO                      : 3;
};

#define DSP_INT2_PRIO                      (0x0007)
#define DSP_INT1_PRIO                      (0x0070)
#define DSP_INT0_PRIO                      (0x0700)


/*====================================================*/
struct __DSP2_INT_PRIO1_REG
/*====================================================*/
{
    WORD BITFLD_DSP_INT2_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT1_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT0_PRIO                      : 3;
};

#define DSP_INT2_PRIO                      (0x0007)
#define DSP_INT1_PRIO                      (0x0070)
#define DSP_INT0_PRIO                      (0x0700)


/*====================================================*/
struct __DSP1_INT_PRIO2_REG
/*====================================================*/
{
    WORD BITFLD_DSP_INT5_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT4_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT3_PRIO                      : 3;
};

#define DSP_INT5_PRIO                      (0x0007)
#define DSP_INT4_PRIO                      (0x0070)
#define DSP_INT3_PRIO                      (0x0700)


/*====================================================*/
struct __DSP2_INT_PRIO2_REG
/*====================================================*/
{
    WORD BITFLD_DSP_INT5_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT4_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP_INT3_PRIO                      : 3;
};

#define DSP_INT5_PRIO                      (0x0007)
#define DSP_INT4_PRIO                      (0x0070)
#define DSP_INT3_PRIO                      (0x0700)


/*====================================================*/
struct __DSP1_IRQ_START_REG
/*====================================================*/
{
    WORD                                           : 4;
    WORD BITFLD_DSPx_IRQ_START                     : 12;
};

#define DSPx_IRQ_START                     (0xFFF0)


/*====================================================*/
struct __DSP2_IRQ_START_REG
/*====================================================*/
{
    WORD                                           : 4;
    WORD BITFLD_DSPx_IRQ_START                     : 12;
};

#define DSPx_IRQ_START                     (0xFFF0)


/*====================================================*/
struct __DSP_MAIN_SYNC0_REG
/*====================================================*/
{
    WORD BITFLD_RAMIN0_SYNC                        : 2;
    WORD BITFLD_RAMIN1_SYNC                        : 2;
    WORD BITFLD_RAMIN2_SYNC                        : 2;
    WORD BITFLD_RAMIN3_SYNC                        : 2;
    WORD BITFLD_RAMOUT0_SYNC                       : 2;
    WORD BITFLD_RAMOUT1_SYNC                       : 2;
    WORD BITFLD_RAMOUT2_SYNC                       : 2;
    WORD BITFLD_RAMOUT3_SYNC                       : 2;
};

#define RAMIN0_SYNC                        (0x0003)
#define RAMIN1_SYNC                        (0x000C)
#define RAMIN2_SYNC                        (0x0030)
#define RAMIN3_SYNC                        (0x00C0)
#define RAMOUT0_SYNC                       (0x0300)
#define RAMOUT1_SYNC                       (0x0C00)
#define RAMOUT2_SYNC                       (0x3000)
#define RAMOUT3_SYNC                       (0xC000)


/*====================================================*/
struct __DSP_MAIN_SYNC1_REG
/*====================================================*/
{
    WORD BITFLD_DSP_SYNC0                          : 2;
    WORD BITFLD_DSP_SYNC1                          : 2;
    WORD BITFLD_DSP_SYNC2                          : 2;
    WORD BITFLD_AD_SYNC                            : 2;
    WORD BITFLD_DA_CLASSD_SYNC                     : 2;
    WORD BITFLD_DA_LSR_SYNC                        : 2;
    WORD BITFLD_ADC_SYNC                           : 2;
    WORD BITFLD_PCM_SYNC                           : 2;
};

#define DSP_SYNC0                          (0x0003)
#define DSP_SYNC1                          (0x000C)
#define DSP_SYNC2                          (0x0030)
#define AD_SYNC                            (0x00C0)
#define DA_CLASSD_SYNC                     (0x0300)
#define DA_LSR_SYNC                        (0x0C00)
#define ADC_SYNC                           (0x3000)
#define PCM_SYNC                           (0xC000)


/*====================================================*/
struct __DSP_MAIN_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_DSP_MAIN_PRESET                    : 8;
    WORD BITFLD_DSP_MAIN_CTRL                      : 2;
};

#define DSP_MAIN_PRESET                    (0x00FF)
#define DSP_MAIN_CTRL                      (0x0300)


/*====================================================*/
struct __DSP1_OVERFLOW_REG
/*====================================================*/
{
    WORD BITFLD_INT_OVERFLOW                       : 6;
    WORD BITFLD_WTF_OVERFLOW                       : 1;
    WORD BITFLD_IRQ_OVERFLOW                       : 1;
    WORD BITFLD_M_INT_OVERFLOW                     : 6;
    WORD BITFLD_M_WTF_OVERFLOW                     : 1;
    WORD BITFLD_M_IRQ_OVERFLOW                     : 1;
};

#define INT_OVERFLOW                       (0x003F)
#define WTF_OVERFLOW                       (0x0040)
#define IRQ_OVERFLOW                       (0x0080)
#define M_INT_OVERFLOW                     (0x3F00)
#define M_WTF_OVERFLOW                     (0x4000)
#define M_IRQ_OVERFLOW                     (0x8000)


/*====================================================*/
struct __DSP2_OVERFLOW_REG
/*====================================================*/
{
    WORD BITFLD_INT_OVERFLOW                       : 6;
    WORD BITFLD_WTF_OVERFLOW                       : 1;
    WORD BITFLD_IRQ_OVERFLOW                       : 1;
    WORD BITFLD_M_INT_OVERFLOW                     : 6;
    WORD BITFLD_M_WTF_OVERFLOW                     : 1;
    WORD BITFLD_M_IRQ_OVERFLOW                     : 1;
};

#define INT_OVERFLOW                       (0x003F)
#define WTF_OVERFLOW                       (0x0040)
#define IRQ_OVERFLOW                       (0x0080)
#define M_INT_OVERFLOW                     (0x3F00)
#define M_WTF_OVERFLOW                     (0x4000)
#define M_IRQ_OVERFLOW                     (0x8000)


/*====================================================*/
struct __DSP_PCM_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_PCM_EN                             : 1;
    WORD BITFLD_PCM_MASTER                         : 1;
    WORD BITFLD_DSP_PCM_SYNC                       : 1;
    WORD BITFLD_PCM_FSC0LEN                        : 2;
    WORD BITFLD_PCM_FSC0DEL                        : 1;
    WORD BITFLD_PCM_PPOD                           : 1;
};

#define PCM_EN                             (0x0001)
#define PCM_MASTER                         (0x0002)
#define DSP_PCM_SYNC                       (0x0004)
#define PCM_FSC0LEN                        (0x0018)
#define PCM_FSC0DEL                        (0x0020)
#define PCM_PPOD                           (0x0040)


/*====================================================*/
struct __DSP_VQI_REG
/*====================================================*/
{
    WORD BITFLD_BVQI_ON                            : 4;
};

#define BVQI_ON                            (0x000F)


/*====================================================*/
struct __DSP_ZCROSS1_OUT_REG
/*====================================================*/
{
    WORD BITFLD_DSP_ZCROSSx                        : 1;
};

#define DSP_ZCROSSx                        (0x8000)


/*====================================================*/
struct __DSP_ZCROSS2_OUT_REG
/*====================================================*/
{
    WORD BITFLD_DSP_ZCROSSx                        : 1;
};

#define DSP_ZCROSSx                        (0x8000)


/*====================================================*/
struct __EBI_SDCONR_REG
/*====================================================*/
{
    WORD BITFLD_Unused                             : 3;
    WORD BITFLD_S_BANK_ADDR_WIDTH                  : 2;
    WORD BITFLD_S_ROW_ADDR_WIDTH                   : 4;
    WORD BITFLD_S_COL_ADDR_WIDTH                   : 4;
    WORD BITFLD_S_DATA_WIDTH                       : 2;
};

#define Unused                             (0x0007)
#define S_BANK_ADDR_WIDTH                  (0x0018)
#define S_ROW_ADDR_WIDTH                   (0x01E0)
#define S_COL_ADDR_WIDTH                   (0x1E00)
#define S_DATA_WIDTH                       (0x6000)


/*====================================================*/
struct __EBI_SDTMG0R_REG
/*====================================================*/
{
    WORD BITFLD_CAS_LATENCY                        : 2;
    WORD BITFLD_T_RAS_MIN                          : 4;
    WORD BITFLD_T_RCD                              : 3;
    WORD BITFLD_T_RP                               : 3;
    WORD BITFLD_T_WRSD                             : 2;
    WORD BITFLD_T_RCAR                             : 4;
    WORD BITFLD_T_XSR                              : 4;
    WORD BITFLD_T_RCSD                             : 4;
    WORD BITFLD_CAS_LATENCY_H                      : 1;
    WORD BITFLD_T_XSR_H                            : 5;
};

#define CAS_LATENCY                        (0x0003)
#define T_RAS_MIN                          (0x003C)
#define T_RCD                              (0x01C0)
#define T_RP                               (0x0E00)
#define T_WRSD                             (0x3000)
#define T_RCAR                             (0x3C000)
#define T_XSR                              (0x3C0000)
#define T_RCSD                             (0x3C00000)
#define CAS_LATENCY_H                      (0x4000000)
#define T_XSR_H                            (0xF8000000)


/*====================================================*/
struct __EBI_SDTMG1R_REG
/*====================================================*/
{
    WORD BITFLD_T_INIT                             : 16;
    WORD BITFLD_NUM_INIT_REF                       : 4;
};

#define T_INIT                             (0xFFFF)
#define NUM_INIT_REF                       (0xF0000)


/*====================================================*/
struct __EBI_SDCTLR_REG
/*====================================================*/
{
    WORD BITFLD_INITIALIZE                         : 1;
    WORD BITFLD_SR_OR_DP_MODE                      : 1;
    WORD BITFLD_POWER_DOWN_MODE                    : 1;
    WORD BITFLD_PRECHARGE_ALGO                     : 1;
    WORD BITFLD_FULL_REFRESH_BEFORE_SR             : 1;
    WORD BITFLD_FULL_REFRESH_AFTER_SR              : 1;
    WORD BITFLD_READ_PIPE                          : 3;
    WORD BITFLD_SET_MODE_REG                       : 1;
    WORD                                           : 1;
    WORD BITFLD_SELF_REFRESH_STATUS                : 1;
    WORD BITFLD_NUM_OPEN_BANKS                     : 5;
    WORD BITFLD_S_RD_READY_MODE                    : 1;
};

#define INITIALIZE                         (0x0001)
#define SR_OR_DP_MODE                      (0x0002)
#define POWER_DOWN_MODE                    (0x0004)
#define PRECHARGE_ALGO                     (0x0008)
#define FULL_REFRESH_BEFORE_SR             (0x0010)
#define FULL_REFRESH_AFTER_SR              (0x0020)
#define READ_PIPE                          (0x01C0)
#define SET_MODE_REG                       (0x0200)
#define SELF_REFRESH_STATUS                (0x0800)
#define NUM_OPEN_BANKS                     (0x1F000)
#define S_RD_READY_MODE                    (0x20000)


/*====================================================*/
struct __EBI_SDREFR_REG
/*====================================================*/
{
    WORD BITFLD_T_REF                              : 16;
    WORD BITFLD_READ_PIPE_CLK                      : 3;
    WORD BITFLD_ACS3_IOEXP                         : 1;
    WORD BITFLD_READ_PIPE_MUX                      : 3;
    WORD BITFLD_GPO                                : 1;
    WORD BITFLD_GPI                                : 8;
};

#define T_REF                              (0xFFFF)
#define READ_PIPE_CLK                      (0x70000)
#define ACS3_IOEXP                         (0x80000)
#define READ_PIPE_MUX                      (0x700000)
#define GPO                                (0x800000)
#define GPI                                (0xFF000000)


/*====================================================*/
struct __EBI_ACS0_LOW_REG
/*====================================================*/
{
    WORD                                           : 11;
    WORD BITFLD_EBI_RES                            : 5;
    WORD BITFLD_CS_BASE                            : 16;
};

#define EBI_RES                            (0xF800)
#define CS_BASE                            (0xFFFF0000)


/*====================================================*/
struct __EBI_ACS1_LOW_REG
/*====================================================*/
{
    WORD                                           : 11;
    WORD BITFLD_EBI_RES                            : 5;
    WORD BITFLD_CS_BASE                            : 16;
};

#define EBI_RES                            (0xF800)
#define CS_BASE                            (0xFFFF0000)


/*====================================================*/
struct __EBI_ACS2_LOW_REG
/*====================================================*/
{
    WORD                                           : 11;
    WORD BITFLD_EBI_RES                            : 5;
    WORD BITFLD_CS_BASE                            : 16;
};

#define EBI_RES                            (0xF800)
#define CS_BASE                            (0xFFFF0000)


/*====================================================*/
struct __EBI_ACS3_LOW_REG
/*====================================================*/
{
    WORD                                           : 11;
    WORD BITFLD_EBI_RES                            : 5;
    WORD BITFLD_CS_BASE                            : 16;
};

#define EBI_RES                            (0xF800)
#define CS_BASE                            (0xFFFF0000)


/*====================================================*/
struct __EBI_ACS4_LOW_REG
/*====================================================*/
{
    WORD                                           : 11;
    WORD BITFLD_EBI_RES                            : 5;
    WORD BITFLD_CS_BASE                            : 16;
};

#define EBI_RES                            (0xF800)
#define CS_BASE                            (0xFFFF0000)


/*====================================================*/
struct __EBI_ACS0_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_MEM_SIZE                           : 5;
    WORD BITFLD_MEM_TYPE                           : 3;
    WORD BITFLD_REG_SELECT                         : 3;
};

#define MEM_SIZE                           (0x001F)
#define MEM_TYPE                           (0x00E0)
#define REG_SELECT                         (0x0700)


/*====================================================*/
struct __EBI_ACS1_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_MEM_SIZE                           : 5;
    WORD BITFLD_MEM_TYPE                           : 3;
    WORD BITFLD_REG_SELECT                         : 3;
};

#define MEM_SIZE                           (0x001F)
#define MEM_TYPE                           (0x00E0)
#define REG_SELECT                         (0x0700)


/*====================================================*/
struct __EBI_ACS2_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_MEM_SIZE                           : 5;
    WORD BITFLD_MEM_TYPE                           : 3;
    WORD BITFLD_REG_SELECT                         : 3;
};

#define MEM_SIZE                           (0x001F)
#define MEM_TYPE                           (0x00E0)
#define REG_SELECT                         (0x0700)


/*====================================================*/
struct __EBI_ACS3_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_MEM_SIZE                           : 5;
    WORD BITFLD_MEM_TYPE                           : 3;
    WORD BITFLD_REG_SELECT                         : 3;
};

#define MEM_SIZE                           (0x001F)
#define MEM_TYPE                           (0x00E0)
#define REG_SELECT                         (0x0700)


/*====================================================*/
struct __EBI_ACS4_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_MEM_SIZE                           : 5;
    WORD BITFLD_MEM_TYPE                           : 3;
    WORD BITFLD_REG_SELECT                         : 3;
};

#define MEM_SIZE                           (0x001F)
#define MEM_TYPE                           (0x00E0)
#define REG_SELECT                         (0x0700)

/*====================================================*/
struct __EBI_FLASH_TRPDR_REG
/*====================================================*/
{
    WORD BITFLD_T_RPD                              : 12;
};

#define T_RPD                              (0x0FFF)


/*====================================================*/
struct __EBI_SMCTLR_REG
/*====================================================*/
{
    WORD BITFLD_sSM_RP_N                           : 1;
    WORD BITFLD_WP_N                               : 3;
    WORD                                           : 3;
    WORD BITFLD_SM_DATA_WIDTH_SET0                 : 3;
    WORD BITFLD_SM_DATA_WIDTH_SET1                 : 3;
    WORD BITFLD_SM_DATA_WIDTH_SET2                 : 3;
};

#define sSM_RP_N                           (0x0001)
#define WP_N                               (0x000E)
#define SM_DATA_WIDTH_SET0                 (0x0380)
#define SM_DATA_WIDTH_SET1                 (0x1C00)
#define SM_DATA_WIDTH_SET2                 (0xE000)


/*====================================================*/
struct __INT0_PRIORITY_REG
/*====================================================*/
{
    WORD BITFLD_SPI2_INT_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP1_INT_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_DSP2_INT_PRIO                      : 3;
};

#define SPI2_INT_PRIO                      (0x0007)
#define DSP1_INT_PRIO                      (0x0070)
#define DSP2_INT_PRIO                      (0x0700)


/*====================================================*/
struct __INT1_PRIORITY_REG
/*====================================================*/
{
    WORD BITFLD_TIM1_INT_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_CLK100_INT_PRIO                    : 3;
    WORD                                           : 1;
    WORD BITFLD_DIP_INT_PRIO                       : 3;
    WORD                                           : 1;
    WORD BITFLD_USB_INT_PRIO                       : 3;
};

#define TIM1_INT_PRIO                      (0x0007)
#define CLK100_INT_PRIO                    (0x0070)
#define DIP_INT_PRIO                       (0x0700)
#define USB_INT_PRIO                       (0x7000)


/*====================================================*/
struct __INT2_PRIORITY_REG
/*====================================================*/
{
    WORD BITFLD_UART_RI_INT_PRIO                   : 3;
    WORD                                           : 1;
    WORD BITFLD_UART_TI_INT_PRIO                   : 3;
    WORD                                           : 1;
    WORD BITFLD_SPI1_AD_INT_PRIO                   : 3;
    WORD                                           : 1;
    WORD BITFLD_TIM0_INT_PRIO                      : 3;
};

#define UART_RI_INT_PRIO                   (0x0007)
#define UART_TI_INT_PRIO                   (0x0070)
#define SPI1_AD_INT_PRIO                   (0x0700)
#define TIM0_INT_PRIO                      (0x7000)


/*====================================================*/
struct __INT3_PRIORITY_REG
/*====================================================*/
{
    WORD BITFLD_ACCESS_INT_PRIO                    : 3;
    WORD                                           : 1;
    WORD BITFLD_KEYB_INT_PRIO                      : 3;
    WORD                                           : 1;
    WORD BITFLD_RESERVED_INT_PRIO                  : 3;
    WORD                                           : 1;
    WORD BITFLD_CT_CLASSD_INT_PRIO                 : 3;
};

#define ACCESS_INT_PRIO                    (0x0007)
#define KEYB_INT_PRIO                      (0x0070)
#define RESERVED_INT_PRIO                  (0x0700)
#define CT_CLASSD_INT_PRIO                 (0x7000)


/*====================================================*/
struct __KEY_GP_INT_REG
/*====================================================*/
{
    WORD BITFLD_INT6_CTRL                          : 3;
    WORD BITFLD_INT7_CTRL                          : 3;
    WORD BITFLD_INT8_CTRL                          : 3;
    WORD BITFLD_DREQ1_CTRL                         : 1;
};

#define INT6_CTRL                          (0x0007)
#define INT7_CTRL                          (0x0038)
#define INT8_CTRL                          (0x01C0)
#define DREQ1_CTRL                         (0x0200)


/*====================================================*/
struct __KEY_BOARD_INT_REG
/*====================================================*/
{
    WORD BITFLD_INT0_EN                            : 1;
    WORD BITFLD_INT1_EN                            : 1;
    WORD BITFLD_INT2_EN                            : 1;
    WORD BITFLD_INT3_EN                            : 1;
    WORD BITFLD_INT4_EN                            : 1;
    WORD BITFLD_INT5_EN                            : 1;
    WORD BITFLD_INT_PON_EN                         : 1;
    WORD BITFLD_INT_CHARGE_EN                      : 1;
    WORD BITFLD_CHARGE_CTRL                        : 1;
    WORD BITFLD_PON_CTRL                           : 1;
    WORD BITFLD_KEY_LEVEL                          : 1;
    WORD BITFLD_KEY_REL                            : 1;
};

#if 0
#define INT0_EN                            (0x0001)
#define INT1_EN                            (0x0002)
#define INT2_EN                            (0x0004)
#define INT3_EN                            (0x0008)
#define INT4_EN                            (0x0010)
#define INT5_EN                            (0x0020)
#define INT_PON_EN                         (0x0040)
#define INT_CHARGE_EN                      (0x0080)
#define CHARGE_CTRL                        (0x0100)
#define PON_CTRL                           (0x0200)
#define KEY_LEVEL                          (0x0400)
#define KEY_REL                            (0x0800)
#endif


/*====================================================*/
struct __KEY_DEBOUNCE_REG
/*====================================================*/
{
    WORD BITFLD_DEBOUNCE                           : 6;
    WORD BITFLD_KEY_REPEAT                         : 6;
};

#define DEBOUNCE                           (0x003F)
#define KEY_REPEAT                         (0x0FC0)


/*====================================================*/
struct __KEY_STATUS_REG
/*====================================================*/
{
    WORD BITFLD_KEY_STATUS                         : 4;
};

#define KEY_STATUS                         (0x000F)


/*====================================================*/
struct __PC_START_REG
/*====================================================*/
{
    WORD BITFLD_PC_START10                         : 2;
    WORD BITFLD_PC_START                           : 14;
};

#define PC_START10                         (0x0003)
#define PC_START                           (0xFFFC)


/*====================================================*/
struct __P0_00_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_01_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_02_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_03_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_04_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_05_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_06_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_07_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_08_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_09_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_10_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_11_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_12_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_13_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_14_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P0_15_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_DATA_REG
/*====================================================*/
{
    WORD BITFLD_P1_DATA                            : 14;
};

#define P1_DATA                            (0x3FFF)


/*====================================================*/
struct __P1_SET_DATA_REG
/*====================================================*/
{
    WORD BITFLD_P1_SET                             : 14;
};

#define P1_SET                             (0x3FFF)


/*====================================================*/
struct __P1_RESET_DATA_REG
/*====================================================*/
{
    WORD BITFLD_P1_SET                             : 14;
};

#define P1_SET                             (0x3FFF)


/*====================================================*/
struct __P1_00_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_01_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_02_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_03_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_04_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_05_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_06_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_07_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_08_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_09_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_10_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_11_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_12_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_13_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_14_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P1_15_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_DATA_REG
/*====================================================*/
{
    WORD BITFLD_P2_DATA                            : 12;
};

#define P2_DATA                            (0x0FFF)


/*====================================================*/
struct __P2_SET_DATA_REG
/*====================================================*/
{
    WORD BITFLD_P2_SET                             : 12;
};

#define P2_SET                             (0x0FFF)


/*====================================================*/
struct __P2_RESET_DATA_REG
/*====================================================*/
{
    WORD BITFLD_P2_SET                             : 12;
};

#define P2_SET                             (0x0FFF)


/*====================================================*/
struct __P2_00_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_01_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_04_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_05_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_06_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_07_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_08_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_09_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __P2_10_MODE_REG
/*====================================================*/
{
    WORD BITFLD_PID                                : 6;
    WORD                                           : 2;
    WORD BITFLD_PUPD                               : 2;
};

#define PID                                (0x003F)
#define PUPD                               (0x0300)


/*====================================================*/
struct __RESET_FREEZE_REG
/*====================================================*/
{
    WORD BITFLD_FRZ_DIP                            : 1;
    WORD BITFLD_FRZ_TIM0                           : 1;
    WORD BITFLD_FRZ_TIM1                           : 1;
    WORD BITFLD_FRZ_WDOG                           : 1;
    WORD BITFLD_FRZ_DMA0                           : 1;
    WORD BITFLD_FRZ_DMA1                           : 1;
    WORD BITFLD_FRZ_DMA2                           : 1;
    WORD BITFLD_FRZ_DMA3                           : 1;
    WORD BITFLD_FRZ_USB                            : 1;
};

#define FRZ_DIP                            (0x0001)
#define FRZ_TIM0                           (0x0002)
#define FRZ_TIM1                           (0x0004)
#define FRZ_WDOG                           (0x0008)
#define FRZ_DMA0                           (0x0010)
#define FRZ_DMA1                           (0x0020)
#define FRZ_DMA2                           (0x0040)
#define FRZ_DMA3                           (0x0080)
#define FRZ_USB                            (0x0100)


/*====================================================*/
struct __RESET_INT_PENDING_REG
/*====================================================*/
{
    WORD BITFLD_ACCESS_INT_PEND                    : 1;
    WORD BITFLD_KEYB_INT_PEND                      : 1;
    WORD BITFLD_RESERVED_INT_PEND                  : 1;
    WORD BITFLD_CT_CLASSD_INT_PEND                 : 1;
    WORD BITFLD_UART_RI_INT_PEND                   : 1;
    WORD BITFLD_UART_TI_INT_PEND                   : 1;
    WORD BITFLD_SPI1_AD_INT_PEND                   : 1;
    WORD BITFLD_TIM0_INT_PEND                      : 1;
    WORD BITFLD_TIM1_INT_PEND                      : 1;
    WORD BITFLD_CLK100_INT_PEND                    : 1;
    WORD BITFLD_DIP_INT_PEND                       : 1;
    WORD BITFLD_USB_INT_PEND                       : 1;
    WORD BITFLD_SPI2_INT_PEND                      : 1;
    WORD BITFLD_DSP1_INT_PEND                      : 1;
    WORD BITFLD_DSP2_INT_PEND                      : 1;
};

#if 0
#define ACCESS_INT_PEND                    (0x0001)
#define KEYB_INT_PEND                      (0x0002)
#define RESERVED_INT_PEND                  (0x0004)
#define CT_CLASSD_INT_PEND                 (0x0008)
#define UART_RI_INT_PEND                   (0x0010)
#define UART_TI_INT_PEND                   (0x0020)
#define SPI1_AD_INT_PEND                   (0x0040)
#define TIM0_INT_PEND                      (0x0080)
#define TIM1_INT_PEND                      (0x0100)
#define CLK100_INT_PEND                    (0x0200)
#define DIP_INT_PEND                       (0x0400)
#define USB_INT_PEND                       (0x0800)
#define SPI2_INT_PEND                      (0x1000)
#define DSP1_INT_PEND                      (0x2000)
#define DSP2_INT_PEND                      (0x4000)
#endif


/*====================================================*/
struct __SET_FREEZE_REG
/*====================================================*/
{
    WORD BITFLD_FRZ_DIP                            : 1;
    WORD BITFLD_FRZ_TIM0                           : 1;
    WORD BITFLD_FRZ_TIM1                           : 1;
    WORD BITFLD_FRZ_WDOG                           : 1;
    WORD BITFLD_FRZ_DMA0                           : 1;
    WORD BITFLD_FRZ_DMA1                           : 1;
    WORD BITFLD_FRZ_DMA2                           : 1;
    WORD BITFLD_FRZ_DMA3                           : 1;
    WORD BITFLD_FRZ_USB                            : 1;
};

#define FRZ_DIP                            (0x0001)
#define FRZ_TIM0                           (0x0002)
#define FRZ_TIM1                           (0x0004)
#define FRZ_WDOG                           (0x0008)
#define FRZ_DMA0                           (0x0010)
#define FRZ_DMA1                           (0x0020)
#define FRZ_DMA2                           (0x0040)
#define FRZ_DMA3                           (0x0080)
#define FRZ_USB                            (0x0100)


/*====================================================*/
struct __SET_INT_PENDING_REG
/*====================================================*/
{
    WORD BITFLD_ACCESS_INT_PEND                    : 1;
    WORD BITFLD_KEYB_INT_PEND                      : 1;
    WORD BITFLD_RESERVED_INT_PEND                  : 1;
    WORD BITFLD_CT_CLASSD_INT_PEND                 : 1;
    WORD BITFLD_UART_RI_INT_PEND                   : 1;
    WORD BITFLD_UART_TI_INT_PEND                   : 1;
    WORD BITFLD_SPI1_AD_INT_PEND                   : 1;
    WORD BITFLD_TIM0_INT_PEND                      : 1;
    WORD BITFLD_TIM1_INT_PEND                      : 1;
    WORD BITFLD_CLK100_INT_PEND                    : 1;
    WORD BITFLD_DIP_INT_PEND                       : 1;
    WORD BITFLD_USB_INT_PEND                       : 1;
    WORD BITFLD_SPI2_INT_PEND                      : 1;
    WORD BITFLD_DSP1_INT_PEND                      : 1;
    WORD BITFLD_DSP2_INT_PEND                      : 1;
};

#if 0
#define ACCESS_INT_PEND                    (0x0001)
#define KEYB_INT_PEND                      (0x0002)
#define RESERVED_INT_PEND                  (0x0004)
#define CT_CLASSD_INT_PEND                 (0x0008)
#define UART_RI_INT_PEND                   (0x0010)
#define UART_TI_INT_PEND                   (0x0020)
#define SPI1_AD_INT_PEND                   (0x0040)
#define TIM0_INT_PEND                      (0x0080)
#define TIM1_INT_PEND                      (0x0100)
#define CLK100_INT_PEND                    (0x0200)
#define DIP_INT_PEND                       (0x0400)
#define USB_INT_PEND                       (0x0800)
#define SPI2_INT_PEND                      (0x1000)
#define DSP1_INT_PEND                      (0x2000)
#define DSP2_INT_PEND                      (0x4000)
#endif


/*====================================================*/
struct __SPI1_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_SPI_ON                             : 1;
    WORD BITFLD_SPI_PHA                            : 1;
    WORD BITFLD_SPI_POL                            : 1;
    WORD BITFLD_SPI_CLK                            : 2;
    WORD BITFLD_SPI_DO                             : 1;
    WORD BITFLD_SPI_SMN                            : 1;
    WORD BITFLD_SPI_WORD                           : 2;
    WORD BITFLD_SPI_RST                            : 1;
    WORD BITFLD_SPI_FORCE_DO                       : 1;
    WORD BITFLD_SPI_TXH                            : 1;
    WORD BITFLD_SPI_DI                             : 1;
    WORD BITFLD_SPI_INT_BIT                        : 1;
    WORD BITFLD_SPI_MINT                           : 1;
    WORD BITFLD_SPI_EN_CTRL                        : 1;
};

#define SPI_ON                             (0x0001)
#define SPI_PHA                            (0x0002)
#define SPI_POL                            (0x0004)
#define SPI_CLK                            (0x0018)
#define SPI_DO                             (0x0020)
#define SPI_SMN                            (0x0040)
#define SPI_WORD                           (0x0180)
#define SPI_RST                            (0x0200)
#define SPI_FORCE_DO                       (0x0400)
#define SPI_TXH                            (0x0800)
#define SPI_DI                             (0x1000)
#define SPI_INT_BIT                        (0x2000)
#define SPI_MINT                           (0x4000)
#define SPI_EN_CTRL                        (0x8000)


/*====================================================*/
struct __SPI2_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_SPI_ON                             : 1;
    WORD BITFLD_SPI_PHA                            : 1;
    WORD BITFLD_SPI_POL                            : 1;
    WORD BITFLD_SPI_CLK                            : 2;
    WORD BITFLD_SPI_DO                             : 1;
    WORD BITFLD_SPI_SMN                            : 1;
    WORD BITFLD_SPI_WORD                           : 2;
    WORD BITFLD_SPI_RST                            : 1;
    WORD BITFLD_SPI_FORCE_DO                       : 1;
    WORD BITFLD_SPI_TXH                            : 1;
    WORD BITFLD_SPI_DI                             : 1;
    WORD BITFLD_SPI_INT_BIT                        : 1;
    WORD BITFLD_SPI_MINT                           : 1;
    WORD BITFLD_SPI_EN_CTRL                        : 1;
};

#define SPI_ON                             (0x0001)
#define SPI_PHA                            (0x0002)
#define SPI_POL                            (0x0004)
#define SPI_CLK                            (0x0018)
#define SPI_DO                             (0x0020)
#define SPI_SMN                            (0x0040)
#define SPI_WORD                           (0x0180)
#define SPI_RST                            (0x0200)
#define SPI_FORCE_DO                       (0x0400)
#define SPI_TXH                            (0x0800)
#define SPI_DI                             (0x1000)
#define SPI_INT_BIT                        (0x2000)
#define SPI_MINT                           (0x4000)
#define SPI_EN_CTRL                        (0x8000)


/*====================================================*/
struct __TIMER_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_TIM0_CTRL                          : 1;
    WORD BITFLD_TIM1_CTRL                          : 1;
    WORD BITFLD_CLK_CTRL0                          : 1;
    WORD BITFLD_CLK_CTRL1                          : 1;
    WORD BITFLD_WDOG_CTRL                          : 1;
    WORD BITFLD_TIM1_MODE                          : 1;
    WORD BITFLD_CLK_DIV8                           : 1;
    WORD BITFLD_TIM2_CTRL                          : 1;
};

#define TIM0_CTRL                          (0x0001)
#define TIM1_CTRL                          (0x0002)
#define CLK_CTRL0                          (0x0004)
#define CLK_CTRL1                          (0x0008)
#define WDOG_CTRL                          (0x0010)
#define TIM1_MODE                          (0x0020)
#define CLK_DIV8                           (0x0040)
#define TIM2_CTRL                          (0x0080)


/*====================================================*/
struct __TEST_ENV_REG
/*====================================================*/
{
    WORD BITFLD_BOOT                               : 1;
    WORD BITFLD_AD3_1                              : 3;
    WORD BITFLD_ENV_SDI                            : 1;
    WORD BITFLD_ENV_REG7_5                         : 3;
};

#define BOOT                               (0x0001)
#define AD3_1                              (0x000E)
#define ENV_SDI                            (0x0010)
#define ENV_REG7_5                         (0x00E0)


/*====================================================*/
struct __TONE_CTRL1_REG
/*====================================================*/
{
    WORD BITFLD_GATESRC1                           : 2;
    WORD BITFLD_CLKSRC1                            : 2;
    WORD BITFLD_TIMER_RELOAD1                      : 4;
    WORD BITFLD_CT1_INT                            : 1;
    WORD BITFLD_MCT1_INT                           : 1;
    WORD BITFLD_GATE_EDGE1                         : 1;
};

#define GATESRC1                           (0x0003)
#define CLKSRC1                            (0x000C)
#define TIMER_RELOAD1                      (0x00F0)
#define CT1_INT                            (0x0100)
#define MCT1_INT                           (0x0200)
#define GATE_EDGE1                         (0x0400)


/*====================================================*/
struct __TONE_CTRL2_REG
/*====================================================*/
{
    WORD BITFLD_GATESRC2                           : 2;
    WORD BITFLD_CLKSRC2                            : 2;
    WORD BITFLD_TIMER_RELOAD2                      : 4;
    WORD BITFLD_CT2_INT                            : 1;
    WORD BITFLD_MCT2_INT                           : 1;
    WORD BITFLD_GATE_EDGE2                         : 1;
};

#define GATESRC2                           (0x0003)
#define CLKSRC2                            (0x000C)
#define TIMER_RELOAD2                      (0x00F0)
#define CT2_INT                            (0x0100)
#define MCT2_INT                           (0x0200)
#define GATE_EDGE2                         (0x0400)


/*====================================================*/
struct __UART_CTRL_REG
/*====================================================*/
{
    WORD BITFLD_UART_REN                           : 1;
    WORD BITFLD_UART_TEN                           : 1;
    WORD BITFLD_BAUDRATE                           : 3;
    WORD BITFLD_TI                                 : 1;
    WORD BITFLD_RI                                 : 1;
    WORD BITFLD_UART_MODE                          : 1;
    WORD BITFLD_IRDA_EN                            : 1;
    WORD BITFLD_INV_URX                            : 1;
    WORD BITFLD_INV_UTX                            : 1;
};

#define UART_REN                           (0x0001)
#define UART_TEN                           (0x0002)
#define BAUDRATE                           (0x001C)
#define TI                                 (0x0020)
#define RI                                 (0x0040)
#define UART_MODE                          (0x0080)
#define IRDA_EN                            (0x0100)
#define INV_URX                            (0x0200)
#define INV_UTX                            (0x0400)


/*====================================================*/
struct __UART_ERROR_REG
/*====================================================*/
{
    WORD BITFLD_PAR_STATUS                         : 1;
    WORD BITFLD_DMA_PARITY_ERROR                   : 1;
};

#define PAR_STATUS                         (0x0001)
#define DMA_PARITY_ERROR                   (0x0002)


#if 0
/*====================================================*/
struct __USB_ALTEV_REG
/*====================================================*/
{
    WORD                                           : 3;
    WORD BITFLD_USB_EOP                            : 1;
    WORD BITFLD_USB_SD3                            : 1;
    WORD BITFLD_USB_SD5                            : 1;
    WORD BITFLD_USB_RESET                          : 1;
    WORD BITFLD_USB_RESUME                         : 1;
};

#define USB_EOP                            (0x0008)
#define USB_SD3                            (0x0010)
#define USB_SD5                            (0x0020)
#define USB_RESET                          (0x0040)
#define USB_RESUME                         (0x0080)


/*====================================================*/
struct __USB_ALTMSK_REG
/*====================================================*/
{
    WORD                                           : 3;
    WORD BITFLD_USB_M_EOP                          : 1;
    WORD BITFLD_USB_M_SD3                          : 1;
    WORD BITFLD_USB_M_SD5                          : 1;
    WORD BITFLD_USB_M_RESET                        : 1;
    WORD BITFLD_USB_M_RESUME                       : 1;
};

#define USB_M_EOP                          (0x0008)
#define USB_M_SD3                          (0x0010)
#define USB_M_SD5                          (0x0020)
#define USB_M_RESET                        (0x0040)
#define USB_M_RESUME                       (0x0080)


/*====================================================*/
struct __USB_EP0_NAK_REG
/*====================================================*/
{
    WORD BITFLD_USB_EP0_INNAK                      : 1;
    WORD BITFLD_USB_EP0_OUTNAK                     : 1;
};

#define USB_EP0_INNAK                      (0x0001)
#define USB_EP0_OUTNAK                     (0x0002)


/*====================================================*/
struct __USB_EPC0_REG
/*====================================================*/
{
    WORD BITFLD_USB_EP                             : 4;
    WORD                                           : 2;
    WORD BITFLD_USB_DEF                            : 1;
    WORD BITFLD_USB_STALL                          : 1;
};

#define USB_EP                             (0x000F)
#define USB_DEF                            (0x0040)
#define USB_STALL                          (0x0080)


/*====================================================*/
struct __USB_EPC1_REG
/*====================================================*/
{
    WORD BITFLD_USB_EP                             : 4;
    WORD BITFLD_USB_EP_EN                          : 1;
    WORD BITFLD_USB_ISO                            : 1;
    WORD                                           : 1;
    WORD BITFLD_USB_STALL                          : 1;
};

#define USB_EP                             (0x000F)
#define USB_EP_EN                          (0x0010)
#define USB_ISO                            (0x0020)
#define USB_STALL                          (0x0080)


/*====================================================*/
struct __USB_EPC2_REG
/*====================================================*/
{
    WORD BITFLD_USB_EP                             : 4;
    WORD BITFLD_USB_EP_EN                          : 1;
    WORD BITFLD_USB_ISO                            : 1;
    WORD                                           : 1;
    WORD BITFLD_USB_STALL                          : 1;
};

#define USB_EP                             (0x000F)
#define USB_EP_EN                          (0x0010)
#define USB_ISO                            (0x0020)
#define USB_STALL                          (0x0080)


/*====================================================*/
struct __USB_EPC3_REG
/*====================================================*/
{
    WORD BITFLD_USB_EP                             : 4;
    WORD BITFLD_USB_EP_EN                          : 1;
    WORD BITFLD_USB_ISO                            : 1;
    WORD                                           : 1;
    WORD BITFLD_USB_STALL                          : 1;
};

#define USB_EP                             (0x000F)
#define USB_EP_EN                          (0x0010)
#define USB_ISO                            (0x0020)
#define USB_STALL                          (0x0080)


/*====================================================*/
struct __USB_EPC4_REG
/*====================================================*/
{
    WORD BITFLD_USB_EP                             : 4;
    WORD BITFLD_USB_EP_EN                          : 1;
    WORD BITFLD_USB_ISO                            : 1;
    WORD                                           : 1;
    WORD BITFLD_USB_STALL                          : 1;
};

#define USB_EP                             (0x000F)
#define USB_EP_EN                          (0x0010)
#define USB_ISO                            (0x0020)
#define USB_STALL                          (0x0080)


/*====================================================*/
struct __USB_EPC5_REG
/*====================================================*/
{
    WORD BITFLD_USB_EP                             : 4;
    WORD BITFLD_USB_EP_EN                          : 1;
    WORD BITFLD_USB_ISO                            : 1;
    WORD                                           : 1;
    WORD BITFLD_USB_STALL                          : 1;
};

#define USB_EP                             (0x000F)
#define USB_EP_EN                          (0x0010)
#define USB_ISO                            (0x0020)
#define USB_STALL                          (0x0080)


/*====================================================*/
struct __USB_EPC6_REG
/*====================================================*/
{
    WORD BITFLD_USB_EP                             : 4;
    WORD BITFLD_USB_EP_EN                          : 1;
    WORD BITFLD_USB_ISO                            : 1;
    WORD                                           : 1;
    WORD BITFLD_USB_STALL                          : 1;
};

#define USB_EP                             (0x000F)
#define USB_EP_EN                          (0x0010)
#define USB_ISO                            (0x0020)
#define USB_STALL                          (0x0080)


/*====================================================*/
struct __USB_FAR_REG
/*====================================================*/
{
    WORD BITFLD_USB_AD                             : 7;
    WORD BITFLD_USB_AD_EN                          : 1;
};

#define USB_AD                             (0x007F)
#define USB_AD_EN                          (0x0080)


/*====================================================*/
struct __USB_FNH_REG
/*====================================================*/
{
    WORD BITFLD_USB_FN_10_8                        : 3;
    WORD                                           : 2;
    WORD BITFLD_USB_RFC                            : 1;
    WORD BITFLD_USB_UL                             : 1;
    WORD BITFLD_USB_MF                             : 1;
};

#define USB_FN_10_8                        (0x0007)
#define USB_RFC                            (0x0020)
#define USB_UL                             (0x0040)
#define USB_MF                             (0x0080)


/*====================================================*/
struct __USB_FWEV_REG
/*====================================================*/
{
    WORD BITFLD_USB_TXWARN31                       : 3;
    WORD                                           : 1;
    WORD BITFLD_USB_RXWARN31                       : 3;
};

#define USB_TXWARN31                       (0x0007)
#define USB_RXWARN31                       (0x0070)


/*====================================================*/
struct __USB_FWMSK_REG
/*====================================================*/
{
    WORD BITFLD_USB_M_TXWARN31                     : 4;
    WORD                                           : 1;
    WORD BITFLD_USB_M_RXWARN31                     : 3;
};

#define USB_M_TXWARN31                     (0x000F)
#define USB_M_RXWARN31                     (0x0070)


/*====================================================*/
struct __USB_MCTRL_REG
/*====================================================*/
{
    WORD BITFLD_USBEN                              : 1;
    WORD BITFLD_USB_DBG                            : 1;
    WORD                                           : 1;
    WORD BITFLD_USB_NAT                            : 1;
    WORD BITFLD_LSMODE                             : 1;
};

#define USBEN                              (0x0001)
#define USB_DBG                            (0x0002)
#define USB_NAT                            (0x0008)
#define LSMODE                             (0x0010)


/*====================================================*/
struct __USB_MAEV_REG
/*====================================================*/
{
    WORD BITFLD_USB_WARN                           : 1;
    WORD BITFLD_USB_ALT                            : 1;
    WORD BITFLD_USB_TX_EV                          : 1;
    WORD BITFLD_USB_FRAME                          : 1;
    WORD BITFLD_USB_NAK                            : 1;
    WORD BITFLD_USB_ULD                            : 1;
    WORD BITFLD_USB_RX_EV                          : 1;
    WORD BITFLD_USB_INTR                           : 1;
    WORD BITFLD_USB_EP0_TX                         : 1;
    WORD BITFLD_USB_EP0_RX                         : 1;
    WORD BITFLD_USB_EP0_NAK                        : 1;
};

#define USB_WARN                           (0x0001)
#define USB_ALT                            (0x0002)
#define USB_TX_EV                          (0x0004)
#define USB_FRAME                          (0x0008)
#define USB_NAK                            (0x0010)
#define USB_ULD                            (0x0020)
#define USB_RX_EV                          (0x0040)
#define USB_INTR                           (0x0080)
#define USB_EP0_TX                         (0x0100)
#define USB_EP0_RX                         (0x0200)
#define USB_EP0_NAK                        (0x0400)


/*====================================================*/
struct __USB_MAMSK_REG
/*====================================================*/
{
    WORD BITFLD_USB_M_WARN                         : 1;
    WORD BITFLD_USB_M_ALT                          : 1;
    WORD BITFLD_USB_M_TX_EV                        : 1;
    WORD BITFLD_USB_M_FRAME                        : 1;
    WORD BITFLD_USB_M_NAK                          : 1;
    WORD BITFLD_USB_M_ULD                          : 1;
    WORD BITFLD_USB_M_RX_EV                        : 1;
    WORD BITFLD_USB_M_INTR                         : 1;
    WORD BITFLD_USB_M_EP0_TX                       : 1;
    WORD BITFLD_USB_M_EP0_RX                       : 1;
    WORD BITFLD_USB_M_EP0_NAK                      : 1;
};

#define USB_M_WARN                         (0x0001)
#define USB_M_ALT                          (0x0002)
#define USB_M_TX_EV                        (0x0004)
#define USB_M_FRAME                        (0x0008)
#define USB_M_NAK                          (0x0010)
#define USB_M_ULD                          (0x0020)
#define USB_M_RX_EV                        (0x0040)
#define USB_M_INTR                         (0x0080)
#define USB_M_EP0_TX                       (0x0100)
#define USB_M_EP0_RX                       (0x0200)
#define USB_M_EP0_NAK                      (0x0400)


/*====================================================*/
struct __USB_NFSR_REG
/*====================================================*/
{
    WORD BITFLD_USB_NFS                            : 2;
};

#define USB_NFS                            (0x0003)


/*====================================================*/
struct __USB_NAKEV_REG
/*====================================================*/
{
    WORD BITFLD_USB_IN31                           : 3;
    WORD                                           : 1;
    WORD BITFLD_USB_OUT31                          : 3;
};

#define USB_IN31                           (0x0007)
#define USB_OUT31                          (0x0070)


/*====================================================*/
struct __USB_NAKMSK_REG
/*====================================================*/
{
    WORD BITFLD_USB_M_IN31                         : 3;
    WORD                                           : 1;
    WORD BITFLD_USB_M_OUT31                        : 3;
};

#define USB_M_IN31                         (0x0007)
#define USB_M_OUT31                        (0x0070)


/*====================================================*/
struct __USB_RXC0_REG
/*====================================================*/
{
    WORD BITFLD_USB_RX_EN                          : 1;
    WORD BITFLD_USB_IGN_OUT                        : 1;
    WORD BITFLD_USB_IGN_SETUP                      : 1;
    WORD BITFLD_USB_FLUSH                          : 1;
};

#define USB_RX_EN                          (0x0001)
#define USB_IGN_OUT                        (0x0002)
#define USB_IGN_SETUP                      (0x0004)
#define USB_FLUSH                          (0x0008)


/*====================================================*/
struct __USB_RXC1_REG
/*====================================================*/
{
    WORD BITFLD_USB_RX_EN                          : 1;
    WORD                                           : 1;
    WORD BITFLD_USB_IGN_SETUP                      : 1;
    WORD BITFLD_USB_FLUSH                          : 1;
    WORD                                           : 1;
    WORD BITFLD_USB_RFWL                           : 2;
};

#define USB_RX_EN                          (0x0001)
#define USB_IGN_SETUP                      (0x0004)
#define USB_FLUSH                          (0x0008)
#define USB_RFWL                           (0x0060)


/*====================================================*/
struct __USB_RXC2_REG
/*====================================================*/
{
    WORD BITFLD_USB_RX_EN                          : 1;
    WORD                                           : 1;
    WORD BITFLD_USB_IGN_SETUP                      : 1;
    WORD BITFLD_USB_FLUSH                          : 1;
    WORD                                           : 1;
    WORD BITFLD_USB_RFWL                           : 2;
};

#define USB_RX_EN                          (0x0001)
#define USB_IGN_SETUP                      (0x0004)
#define USB_FLUSH                          (0x0008)
#define USB_RFWL                           (0x0060)


/*====================================================*/
struct __USB_RXC3_REG
/*====================================================*/
{
    WORD BITFLD_USB_RX_EN                          : 1;
    WORD                                           : 1;
    WORD BITFLD_USB_IGN_SETUP                      : 1;
    WORD BITFLD_USB_FLUSH                          : 1;
    WORD                                           : 1;
    WORD BITFLD_USB_RFWL                           : 2;
};

#define USB_RX_EN                          (0x0001)
#define USB_IGN_SETUP                      (0x0004)
#define USB_FLUSH                          (0x0008)
#define USB_RFWL                           (0x0060)


/*====================================================*/
struct __USB_RXS0_REG
/*====================================================*/
{
    WORD BITFLD_USB_RCOUNT                         : 4;
    WORD BITFLD_USB_RX_LAST                        : 1;
    WORD BITFLD_USB_TOGGLE_RX0                     : 1;
    WORD BITFLD_USB_SETUP                          : 1;
};

#define USB_RCOUNT                         (0x000F)
#define USB_RX_LAST                        (0x0010)
#define USB_TOGGLE_RX0                     (0x0020)
#define USB_SETUP                          (0x0040)


/*====================================================*/
struct __USB_RXS1_REG
/*====================================================*/
{
    WORD BITFLD_USB_RCOUNT                         : 4;
    WORD BITFLD_USB_RX_LAST                        : 1;
    WORD BITFLD_USB_TOGGLE_RX                      : 1;
    WORD BITFLD_USB_SETUP                          : 1;
    WORD BITFLD_USB_RX_ERR                         : 1;
};

#define USB_RCOUNT                         (0x000F)
#define USB_RX_LAST                        (0x0010)
#define USB_TOGGLE_RX                      (0x0020)
#define USB_SETUP                          (0x0040)
#define USB_RX_ERR                         (0x0080)


/*====================================================*/
struct __USB_RXS2_REG
/*====================================================*/
{
    WORD BITFLD_USB_RCOUNT                         : 4;
    WORD BITFLD_USB_RX_LAST                        : 1;
    WORD BITFLD_USB_TOGGLE_RX                      : 1;
    WORD BITFLD_USB_SETUP                          : 1;
    WORD BITFLD_USB_RX_ERR                         : 1;
};

#define USB_RCOUNT                         (0x000F)
#define USB_RX_LAST                        (0x0010)
#define USB_TOGGLE_RX                      (0x0020)
#define USB_SETUP                          (0x0040)
#define USB_RX_ERR                         (0x0080)


/*====================================================*/
struct __USB_RXS3_REG
/*====================================================*/
{
    WORD BITFLD_USB_RCOUNT                         : 4;
    WORD BITFLD_USB_RX_LAST                        : 1;
    WORD BITFLD_USB_TOGGLE_RX                      : 1;
    WORD BITFLD_USB_SETUP                          : 1;
    WORD BITFLD_USB_RX_ERR                         : 1;
};

#define USB_RCOUNT                         (0x000F)
#define USB_RX_LAST                        (0x0010)
#define USB_TOGGLE_RX                      (0x0020)
#define USB_SETUP                          (0x0040)
#define USB_RX_ERR                         (0x0080)


/*====================================================*/
struct __USB_RXEV_REG
/*====================================================*/
{
    WORD BITFLD_USB_RXFIFO31                       : 3;
    WORD                                           : 1;
    WORD BITFLD_USB_RXOVRRN31                      : 3;
};

#define USB_RXFIFO31                       (0x0007)
#define USB_RXOVRRN31                      (0x0070)


/*====================================================*/
struct __USB_RXMSK_REG
/*====================================================*/
{
    WORD BITFLD_USB_M_RXFIFO31                     : 3;
    WORD                                           : 1;
    WORD BITFLD_USB_M_RXOVRRN31                    : 3;
};

#define USB_M_RXFIFO31                     (0x0007)
#define USB_M_RXOVRRN31                    (0x0070)


/*====================================================*/
struct __USB_TCR_REG
/*====================================================*/
{
    WORD BITFLD_USB_CADJ                           : 5;
    WORD BITFLD_USB_VADJ                           : 3;
};

#define USB_CADJ                           (0x001F)
#define USB_VADJ                           (0x00E0)


/*====================================================*/
struct __USB_TXC0_REG
/*====================================================*/
{
    WORD BITFLD_USB_TX_EN                          : 1;
    WORD                                           : 1;
    WORD BITFLD_USB_TOGGLE_TX0                     : 1;
    WORD BITFLD_USB_FLUSH                          : 1;
    WORD BITFLD_USB_IGN_IN                         : 1;
};

#define USB_TX_EN                          (0x0001)
#define USB_TOGGLE_TX0                     (0x0004)
#define USB_FLUSH                          (0x0008)
#define USB_IGN_IN                         (0x0010)


/*====================================================*/
struct __USB_TXEV_REG
/*====================================================*/
{
    WORD BITFLD_USB_TXFIFO31                       : 3;
    WORD                                           : 1;
    WORD BITFLD_USB_TXUDRRN31                      : 3;
};

#define USB_TXFIFO31                       (0x0007)
#define USB_TXUDRRN31                      (0x0070)


/*====================================================*/
struct __USB_TXMSK_REG
/*====================================================*/
{
    WORD BITFLD_USB_M_TXFIFO31                     : 3;
    WORD                                           : 1;
    WORD BITFLD_USB_M_TXUDRRN31                    : 3;
};

#define USB_M_TXFIFO31                     (0x0007)
#define USB_M_TXUDRRN31                    (0x0070)


/*====================================================*/
struct __USB_TXC1_REG
/*====================================================*/
{
    WORD BITFLD_USB_TX_EN                          : 1;
    WORD BITFLD_USB_LAST                           : 1;
    WORD BITFLD_USB_TOGGLE_TX                      : 1;
    WORD BITFLD_USB_FLUSH                          : 1;
    WORD BITFLD_USB_RFF                            : 1;
    WORD BITFLD_USB_TFWL                           : 2;
    WORD BITFLD_USB_IGN_ISOMSK                     : 1;
};

#define USB_TX_EN                          (0x0001)
#define USB_LAST                           (0x0002)
#define USB_TOGGLE_TX                      (0x0004)
#define USB_FLUSH                          (0x0008)
#define USB_RFF                            (0x0010)
#define USB_TFWL                           (0x0060)
#define USB_IGN_ISOMSK                     (0x0080)


/*====================================================*/
struct __USB_TXC2_REG
/*====================================================*/
{
    WORD BITFLD_USB_TX_EN                          : 1;
    WORD BITFLD_USB_LAST                           : 1;
    WORD BITFLD_USB_TOGGLE_TX                      : 1;
    WORD BITFLD_USB_FLUSH                          : 1;
    WORD BITFLD_USB_RFF                            : 1;
    WORD BITFLD_USB_TFWL                           : 2;
    WORD BITFLD_USB_IGN_ISOMSK                     : 1;
};

#define USB_TX_EN                          (0x0001)
#define USB_LAST                           (0x0002)
#define USB_TOGGLE_TX                      (0x0004)
#define USB_FLUSH                          (0x0008)
#define USB_RFF                            (0x0010)
#define USB_TFWL                           (0x0060)
#define USB_IGN_ISOMSK                     (0x0080)


/*====================================================*/
struct __USB_TXC3_REG
/*====================================================*/
{
    WORD BITFLD_USB_TX_EN                          : 1;
    WORD BITFLD_USB_LAST                           : 1;
    WORD BITFLD_USB_TOGGLE_TX                      : 1;
    WORD BITFLD_USB_FLUSH                          : 1;
    WORD BITFLD_USB_RFF                            : 1;
    WORD BITFLD_USB_TFWL                           : 2;
    WORD BITFLD_USB_IGN_ISOMSK                     : 1;
};

#define USB_TX_EN                          (0x0001)
#define USB_LAST                           (0x0002)
#define USB_TOGGLE_TX                      (0x0004)
#define USB_FLUSH                          (0x0008)
#define USB_RFF                            (0x0010)
#define USB_TFWL                           (0x0060)
#define USB_IGN_ISOMSK                     (0x0080)


/*====================================================*/
struct __USB_TXS0_REG
/*====================================================*/
{
    WORD BITFLD_USB_TCOUNT                         : 5;
    WORD BITFLD_USB_TX_DONE                        : 1;
    WORD BITFLD_USB_ACK_STAT                       : 1;
};

#define USB_TCOUNT                         (0x001F)
#define USB_TX_DONE                        (0x0020)
#define USB_ACK_STAT                       (0x0040)


/*====================================================*/
struct __USB_TXS1_REG
/*====================================================*/
{
    WORD BITFLD_USB_TCOUNT                         : 5;
    WORD BITFLD_USB_TX_DONE                        : 1;
    WORD BITFLD_USB_ACK_STAT                       : 1;
    WORD BITFLD_USB_TX_URUN                        : 1;
};

#define USB_TCOUNT                         (0x001F)
#define USB_TX_DONE                        (0x0020)
#define USB_ACK_STAT                       (0x0040)
#define USB_TX_URUN                        (0x0080)


/*====================================================*/
struct __USB_TXS2_REG
/*====================================================*/
{
    WORD BITFLD_USB_TCOUNT                         : 5;
    WORD BITFLD_USB_TX_DONE                        : 1;
    WORD BITFLD_USB_ACK_STAT                       : 1;
    WORD BITFLD_USB_TX_URUN                        : 1;
};

#define USB_TCOUNT                         (0x001F)
#define USB_TX_DONE                        (0x0020)
#define USB_ACK_STAT                       (0x0040)
#define USB_TX_URUN                        (0x0080)


/*====================================================*/
struct __USB_TXS3_REG
/*====================================================*/
{
    WORD BITFLD_USB_TCOUNT                         : 5;
    WORD BITFLD_USB_TX_DONE                        : 1;
    WORD BITFLD_USB_ACK_STAT                       : 1;
    WORD BITFLD_USB_TX_URUN                        : 1;
};

#define USB_TCOUNT                         (0x001F)
#define USB_TX_DONE                        (0x0020)
#define USB_ACK_STAT                       (0x0040)
#define USB_TX_URUN                        (0x0080)


/*====================================================*/
struct __USB_UTR_REG
/*====================================================*/
{
    WORD BITFLD_USB_UTR_RES                        : 5;
    WORD BITFLD_USB_SF                             : 1;
    WORD BITFLD_USB_NCRC                           : 1;
    WORD BITFLD_USB_DIAG                           : 1;
};

#define USB_UTR_RES                        (0x001F)
#define USB_SF                             (0x0020)
#define USB_NCRC                           (0x0040)
#define USB_DIAG                           (0x0080)


/*====================================================*/
struct __USB_UX20CDR_REG
/*====================================================*/
{
    WORD BITFLD_RPU_SSPROTEN                       : 1;
    WORD BITFLD_RPU_RCDELAY                        : 1;
    WORD BITFLD_RPU_TEST_SW1DM                     : 1;
    WORD                                           : 1;
    WORD BITFLD_RPU_TEST_EN                        : 1;
    WORD BITFLD_RPU_TEST_SW1                       : 1;
    WORD BITFLD_RPU_TEST_SW2                       : 1;
    WORD BITFLD_RPU_TEST7                          : 1;
};

#define RPU_SSPROTEN                       (0x0001)
#define RPU_RCDELAY                        (0x0002)
#define RPU_TEST_SW1DM                     (0x0004)
#define RPU_TEST_EN                        (0x0010)
#define RPU_TEST_SW1                       (0x0020)
#define RPU_TEST_SW2                       (0x0040)
#define RPU_TEST7                          (0x0080)


/*====================================================*/
struct __USB_XCVDIAG_REG
/*====================================================*/
{
    WORD BITFLD_USB_XCV_TEST                       : 1;
    WORD BITFLD_USB_XCV_TXp                        : 1;
    WORD BITFLD_USB_XCV_TXn                        : 1;
    WORD BITFLD_USB_XCV_TXEN                       : 1;
    WORD                                           : 1;
    WORD BITFLD_USB_RCV                            : 1;
    WORD BITFLD_USB_VMIN                           : 1;
    WORD BITFLD_USB_VPIN                           : 1;
};

#define USB_XCV_TEST                       (0x0001)
#define USB_XCV_TXp                        (0x0002)
#define USB_XCV_TXn                        (0x0004)
#define USB_XCV_TXEN                       (0x0008)
#define USB_RCV                            (0x0020)
#define USB_VMIN                           (0x0040)
#define USB_VPIN                           (0x0080)

#endif


#if 0
/*
 * Extern declarations of all peripheral registers above 16 Mbyte
 * Note that such a pointer has to be declared somewhere,
 * before it can be used.in the application.
 */

extern const __data24 uint16* dsp_main_sync0_reg;
extern const __data24 uint16* dsp_main_sync1_reg; 
extern const __data24 uint16* dsp_main_cnt_reg;
extern const __data24 uint16* dsp_adcos;
extern const __data24 uint16* dsp_adcis;
extern const __data24 uint16* dsp_classd_reg;
extern const __data24 uint16* dsp_codec_mic_gain_reg;
extern const __data24 uint16* dsp_codec_out_reg;
extern const __data24 uint16* dsp_codec_in_reg;
extern const __data24 uint16* dsp_ram_out0_reg;
extern const __data24 uint16* dsp_ram_out1_reg;
extern const __data24 uint16* dsp_ram_out2_reg;
extern const __data24 uint16* dsp_ram_out3_reg;
extern const __data24 uint16* dsp_ram_in0_reg;
extern const __data24 uint16* dsp_ram_in1_reg;
extern const __data24 uint16* dsp_ram_in2_reg;
extern const __data24 uint16* dsp_ram_in3_reg;
extern const __data24 uint16* dsp_zcross1_out_reg;
extern const __data24 uint16* dsp_zcross2_out_reg;
extern const __data24 uint16* dsp_pcm_out_reg;     
extern const __data24 uint16* dsp_pcm_out_reg;    
extern const __data24 uint16* dsp_pcm_out_reg;    
extern const __data24 uint16* dsp_pcm_out_reg;    
extern const __data24 uint16* dsp_pcm_in0_reg;     
extern const __data24 uint16* dsp_pcm_in1_reg;    
extern const __data24 uint16* dsp_pcm_in2_reg;    
extern const __data24 uint16* dsp_pcm_in3_reg;    
extern const __data24 uint16* dsp_pcm_ctrl_reg;    
extern const __data24 uint16* dsp_phase_info_reg;    
extern const __data24 uint16* dsp_vqi_reg;    
extern const __data24 uint16* dsp_main_ctrl_reg;
extern const __data24 uint16* dsp_classd_buzzoff_reg;       
extern const __data24 uint16* dsp_ctrl_reg;       
extern const __data24 uint16* dsp_pc_reg;    
extern const __data24 uint16* dsp_pc_start_reg;    
extern const __data24 uint16* dsp_irq_start_reg;  
extern const __data24 uint16* dsp_int_reg;        
extern const __data24 uint16* dsp_int_mask_reg;   
extern const __data24 uint16* dsp_int_prio1_reg;   
extern const __data24 uint16* dsp_int_prio2_reg;   
extern const __data24 uint16* dsp_overflow_reg;
extern const __data24 uint16* dsp2_ctrl_reg;
extern const __data24 uint16* dsp2_pc_reg;    
extern const __data24 uint16* dsp2_pc_start_reg;    
extern const __data24 uint16* dsp2_irq_start_reg;  //aris
extern const __data24 uint16* dsp2_int_reg;        
extern const __data24 uint16* dsp2_int_mask_reg;   
extern const __data24 uint16* dsp2_int_prio1_reg;   
extern const __data24 uint16* dsp2_int_prio2_reg;   
extern const __data24 uint16* dsp2_overflow_reg;   
extern const __data24 uint16* dbg1_ireg;   
extern const __data24 uint16* dbg1_inout_reg_lsw;   
extern const __data24 uint16* dbg1_inout_reg_msw;   
extern const __data24 uint16* dbg2_ireg;   
extern const __data24 uint16* dbg2_inout_reg_lsw;   
extern const __data24 uint16* dbg2_inout_reg_msw;  
#endif



/*
 * Missing bit field definitions for GPIO registers
 * This is not done in the datasheet to avoid very long tables
 * that contain no extra information.
 */


#define __GPIO_DATA_REG        __GPIO_BITFIELD_REG
#define __GPIO_DIR_REG         __GPIO_BITFIELD_REG
#define __GPIO_PUPD_REG        __GPIO_BITFIELD_REG
#define __GPIO_INT_EN_REG      __GPIO_BITFIELD_REG

/*====================================================*/
struct __GPIO_BITFIELD_REG
/*====================================================*/
{
    WORD BITFLD_GPIO_0                        : 1;
    WORD BITFLD_GPIO_1                        : 1;
    WORD BITFLD_GPIO_2                        : 1;
    WORD BITFLD_GPIO_3                        : 1;
    WORD BITFLD_GPIO_4                        : 1;
    WORD BITFLD_GPIO_5                        : 1;
    WORD BITFLD_GPIO_6                        : 1;
    WORD BITFLD_GPIO_7                        : 1;
    WORD BITFLD_GPIO_8                        : 1;
    WORD BITFLD_GPIO_9                        : 1;
    WORD BITFLD_GPIO_10                       : 1;
    WORD BITFLD_GPIO_11                       : 1;
    WORD BITFLD_GPIO_12                       : 1;
    WORD BITFLD_GPIO_13                       : 1;
    WORD BITFLD_GPIO_14                       : 1;
    WORD BITFLD_GPIO_15                       : 1;
};

#define GPIO_0                        (0x0001)
#define GPIO_1                        (0x0002)
#define GPIO_2                        (0x0004)
#define GPIO_3                        (0x0008)
#define GPIO_4                        (0x0010)
#define GPIO_5                        (0x0020)
#define GPIO_6                        (0x0040)
#define GPIO_7                        (0x0080)
#define GPIO_8                        (0x0100)
#define GPIO_9                        (0x0200)
#define GPIO_10                       (0x0400)
#define GPIO_11                       (0x0800)
#define GPIO_12                       (0x1000)
#define GPIO_13                       (0x2000)
#define GPIO_14                       (0x4000)
#define GPIO_15                       (0x8000)

#define SHIF(a) ((a)&0x0001?0: (a)&0x0002?1: (a)&0x0004?2: (a)&0x0008?3:\
                 (a)&0x0010?4: (a)&0x0020?5: (a)&0x0040?6: (a)&0x0080?7:\
                 (a)&0x0100?8: (a)&0x0200?9: (a)&0x0400?10:(a)&0x0800?11:\
                 (a)&0x1000?12:(a)&0x2000?13:(a)&0x4000?14: 15)

#define DSHIF(a)((a)&0x00000001?0: (a)&0x00000002?1: (a)&0x00000004?2: (a)&0x00000008?3:\
                 (a)&0x00000010?4: (a)&0x00000020?5: (a)&0x00000040?6: (a)&0x00000080?7:\
                 (a)&0x00000100?8: (a)&0x00000200?9: (a)&0x00000400?10:(a)&0x00000800?11:\
                 (a)&0x00001000?12:(a)&0x00002000?13:(a)&0x00004000?14:(a)&0x00008000?15:\
                 (a)&0x00010000?16:(a)&0x00020000?17:(a)&0x00040000?18:(a)&0x00080000?19:\
                 (a)&0x00100000?20:(a)&0x00200000?21:(a)&0x00400000?22:(a)&0x00800000?23:\
                 (a)&0x01000000?24:(a)&0x02000000?25:(a)&0x04000000?26:(a)&0x08000000?27:\
                 (a)&0x10000000?28:(a)&0x20000000?29:(a)&0x40000000?30: 31)

    #define GetByte(a)  	(* ( __far volatile BYTE*)(&a) )
    #define GetWord(a)  	(* ( __far volatile WORD*)(&a) )
    #define GetDword(a)  	(* ( __far volatile DWORD*)(&a) )
    #define GetBits(a,f)	(( ( __far volatile struct __##a *)(&a))->BITFLD_##f )
    #define GetDbits(a,f)	(( ( __far volatile struct __##a *)(&a))->BITFLD_##f ) 
    #define SetByte(a,d)	(* ( __far volatile BYTE*)(&a)=(d) )
    #define SetWord(a,d)	(* ( __far volatile WORD*)(&a)=(d) )
    #define SetDword(a,d)	(* ( __far volatile DWORD*)(&a)=(d) )
# if !defined( SetBits )
    #define SetBits(a,f,d)	( SetWord( a, (GetWord(a)&((WORD)~f)) | ((d)<<SHIF(f)) ))
# endif
    #define SetDbits(a,f,d)	( SetDword(a, (GetDword(a)&((DWORD)~f)) | (((DWORD)d)<<DSHIF(f)) ))
#endif
