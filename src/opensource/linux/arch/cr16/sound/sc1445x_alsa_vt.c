/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 * Driver for ALSA on SC1445x
 */

#include <sound/driver.h>
#include <linux/init.h>
#include <linux/err.h>
#include <linux/platform_device.h>
#include <linux/moduleparam.h>
#include <linux/delay.h>
#include <sound/core.h>
#include <sound/initval.h>
#include <sound/hwdep.h>
#include "sc1445x_alsa_vt.h"
#include "sc1445x_alsa_ioctl_vt.h"
#include "audioengine/sc1445x_audioengine_api_vt.h"
#include "sc1445x_voip_version.h"


MODULE_LICENSE( "GPL" ) ;
MODULE_DESCRIPTION( "SC1445x ALSA driver" ) ;


//#define USE_FORCED_PACKET

//#define TEST_WITH_MARKED_PACKET

/* private API from audioengine */
void sc1445x_private_stop_raw_pcm( void ) ;
short sc1445x_private_line2stream( const sc1445x_ae_state* ae_state,
				unsigned short line, short create_mapping ) ;

static struct platform_device* snd_sc1445x_device ;

static const char SND_SC1445x_DRIVER[] = "snd_sc1445x" ;

static const unsigned short* dsp1_fw_ver = (unsigned short*)0x12a04 ;
static const unsigned short* dsp1_fw_eng_ver = (unsigned short*)0x12a06 ;
static const unsigned short* dsp2_fw_ver = (unsigned short*)0x1b1fe ;
static const unsigned short* dsp2_fw_eng_ver = (unsigned short*)0x1b15a ;


/* free resources allocated for ALSA "chip" */
int snd_sc1445x_free( struct snd_sc1445x* chip )
{

	sc1445x_ae_finalize_engine( &chip->ae_state ) ;

	kfree( chip ) ;

	return 0 ;
}


/*
 * free the device
 */
static int snd_sc1445x_dev_free( struct snd_device *device )
{
	struct snd_sc1445x* chip = device->device_data ;
	return snd_sc1445x_free( chip ) ;
}


#if 1
/* save a pointer to the ae_state, for debugging */
sc1445x_ae_state* the_ae_state = NULL ;
#endif

/* create a new "chip" for ALSA */
static int __init snd_sc1445x_new( struct snd_card* card,
					struct snd_sc1445x** ret_chip )
{
	int res ;
	struct snd_sc1445x* chip ;
	char dsp_fw_ver_major, dsp_fw_ver_minor ;
	static snd_device_ops_t ops = {
		.dev_free =	snd_sc1445x_dev_free,
	} ;

	snd_assert( ret_chip != NULL, return -EINVAL ) ;
	*ret_chip = NULL ;

	chip = kzalloc( sizeof( *chip), GFP_KERNEL ) ;
	if( NULL == chip )
		return -ENOMEM ;
	chip->card = card ;

	if( sc1445x_ae_init_engine( &chip->ae_state ) != SC1445x_AE_OK ) {
		snd_sc1445x_free( chip ) ;
		return -ENOMEM ;
	}

	/* check DSP1 firmware version */
	dsp_fw_ver_major = *dsp1_fw_ver >> 8 ;
	dsp_fw_ver_minor = *dsp1_fw_ver ;
	/* the check below is only valid for one-digit major version numbers */
	if( ( SC1445x_AUDIO_DRIVER_VERSION_MAJOR[0] - '0' ) !=
						dsp_fw_ver_major ) {
		snd_printk( "ALSA audio driver (version %s) is not compatible "
			"with DSP1 firmware (version %d.%d) -- bailing out!\n",
						SC1445x_AUDIO_DRIVER_VERSION,
					dsp_fw_ver_major, dsp_fw_ver_minor ) ;
		snd_sc1445x_free( chip ) ;
		return -ENODEV ;
	}

	/* check DSP2 firmware version */
	dsp_fw_ver_major = *dsp2_fw_ver >> 8 ;
	dsp_fw_ver_minor = *dsp2_fw_ver ;
	/* the check below is only valid for one-digit major version numbers */
	if( ( SC1445x_AUDIO_DRIVER_VERSION_MAJOR[0] - '0' ) !=
						dsp_fw_ver_major ) {
		snd_printk( "ALSA audio driver (version %s) is not compatible "
			"with DSP2 firmware (version %d.%d) -- bailing out!\n",
						SC1445x_AUDIO_DRIVER_VERSION,
					dsp_fw_ver_major, dsp_fw_ver_minor ) ;
		snd_sc1445x_free( chip ) ;
		return -ENODEV ;
	}

#if 1
	the_ae_state = &chip->ae_state ;
#endif
	chip->ae_state.private_data = chip ;

	chip->irq = DSP2_INT ;

	res = snd_device_new( card, SNDRV_DEV_LOWLEVEL, chip, &ops ) ;
	if( res < 0 ) {
		snd_sc1445x_free( chip ) ;
		return res ;
	}

	*ret_chip = chip ;
	return 0 ;
}


/* hw info */
static struct snd_pcm_hardware snd_sc1445x_playback =
{
	.info =			SNDRV_PCM_INFO_INTERLEAVED |
				SNDRV_PCM_INFO_PAUSE,
	.formats =		SNDRV_PCM_FMTBIT_SPECIAL |
				SNDRV_PCM_FMTBIT_S16 |
				SNDRV_PCM_FMTBIT_S8 | SNDRV_PCM_FMTBIT_U8 |
				SNDRV_PCM_FMTBIT_MU_LAW |
				SNDRV_PCM_FMTBIT_A_LAW,
	.rates =		SNDRV_PCM_RATE_CONTINUOUS |
				SNDRV_PCM_RATE_8000 |
				SNDRV_PCM_RATE_16000 | SNDRV_PCM_RATE_32000,
	.rate_min =		1000,
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	.rate_max =		32800,
#else
	.rate_max =		32000,
#endif
	.channels_min =		1,
	.channels_max =		1,
	.buffer_bytes_max =	2560,
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	.period_bytes_min =	328,
#else
	.period_bytes_min =	246,
#endif
	.period_bytes_max =	640,
	.periods_min =          1,
	.periods_max =          4,
	.fifo_size =		0,
} ;

static struct snd_pcm_hardware snd_sc1445x_capture =
{
	.info =			SNDRV_PCM_INFO_INTERLEAVED,
	.formats =		SNDRV_PCM_FMTBIT_SPECIAL |
				SNDRV_PCM_FMTBIT_S8 | SNDRV_PCM_FMTBIT_U8 |
				SNDRV_PCM_FMTBIT_MU_LAW |
				SNDRV_PCM_FMTBIT_A_LAW,
	.rates =		SNDRV_PCM_RATE_CONTINUOUS |
				SNDRV_PCM_RATE_8000 |
				SNDRV_PCM_RATE_16000 | SNDRV_PCM_RATE_32000,
	.rate_min =		1000,
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	.rate_max =		32800,
#else
	.rate_max =		32000,
#endif
	.channels_min =		1,
	.channels_max =		1,
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	.buffer_bytes_max =	1312,
	.period_bytes_min =	328,
	.period_bytes_max =	328,
#else
	.buffer_bytes_max =	1024,
	.period_bytes_min =	246,
	.period_bytes_max =	246,
#endif
	.periods_min =          1,
	.periods_max =          4,
	.fifo_size =		0,
} ;


/* playback PCM ops */
static int snd_sc1445x_playback_open( snd_pcm_substream_t* substream ) 
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	substream->runtime->hw = snd_sc1445x_playback ;

	if( chip->playback_substream )
		chip->started = 0 ;
	chip->playback_substream = substream ;
	/* invalidate pointers for playback */
	chip->playback_r_pos = -1 ;

	snd_printd( "%s: chip @%p, runtime->hw @%p\n",
			__FUNCTION__, chip, &substream->runtime->hw ) ;
	return 0 ;
}


static int snd_sc1445x_playback_close( snd_pcm_substream_t* substream )
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	chip->playback_substream = NULL ;
	//TODO: stop audio engine?
	if( !chip->capture_substream )
		chip->started = 0 ;

	if( SC1445x_AE_MODE_RAW_PCM == chip->mode ) {
		sc1445x_private_stop_raw_pcm() ;
	}

	return 0 ;
}


static int snd_sc1445x_playback_prepare( snd_pcm_substream_t* substream )
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	if( !chip->started ) {
		//TODO: pass arg according to mode
		if( sc1445x_ae_set_mode( &chip->ae_state, chip->mode, NULL ) ) {
			return -EAGAIN ;
		}
		chip->started = 1 ;
	}

	chip->playback_r_pos = 0 ;

	return 0 ;
}


static int snd_sc1445x_playback_trigger( snd_pcm_substream_t* substream,
								int cmd )
{
	int res = 0 ;
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	switch( cmd ) {
		case SNDRV_PCM_TRIGGER_START:
			snd_printd( "%s: START\n", __FUNCTION__ ) ;
			break ;

		case SNDRV_PCM_TRIGGER_STOP:
			snd_printd( "%s: STOP\n", __FUNCTION__ ) ;
			break ;

		case SNDRV_PCM_TRIGGER_PAUSE_PUSH:
			snd_printd( "%s: PAUSE_PUSH\n", __FUNCTION__ ) ;
			sc1445x_private_stop_raw_pcm() ;
			break ;

		case SNDRV_PCM_TRIGGER_PAUSE_RELEASE:
			snd_printd( "%s: PAUSE_RELEASE\n", __FUNCTION__ ) ;
			sc1445x_ae_set_mode( &chip->ae_state, chip->mode,
				       		NULL ) ;
			break ;

		default:
			res = -EINVAL ;
			break ;
	}

	return res ;
}


static snd_pcm_uframes_t
snd_sc1445x_playback_pointer( snd_pcm_substream_t* substream )
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	return bytes_to_frames( substream->runtime, chip->playback_r_pos ) ;
}


/* get frames from ALSA and put them into the "DMA" buffer allocated by ALSA */
/* we assume that the application feeds ALSA with enough frames for the */
/* maximum number (3) of audio channels -- if any channel(s) is(are) not */
/* being used, the corresponding frames are ignored */
/* frames for each audio packet are not interleaved */
static int snd_sc1445x_playback_copy( snd_pcm_substream_t* substream,
					int channel, snd_pcm_uframes_t pos,
					void __user * src,
					snd_pcm_uframes_t count )
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;
	snd_pcm_runtime_t* runtime = substream->runtime ;
	unsigned nbytes = frames_to_bytes( runtime, count ) ;
#if defined( SC1445x_AE_COLLECT_STATISTICS )
	sc1445x_ae_state* s = &chip->ae_state ;
	const unsigned short nch = s->audio_channels_count ;
	const unsigned short block_size = nch *
				( SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER +
					SC1445x_AE_BYTES_PER_AUDIO_PACKET )
#if 0
//#  if defined( SC1445x_AE_SUPPORT_FAX )
				+ ( SC1445x_AE_BYTES_PER_FAX_PACKET_HEADER
					+ SC1445x_AE_BYTES_PER_FAX_PACKET ) ;
#  else
							+ 0 ;
#  endif
#endif
#if defined( TEST_WITH_MARKED_PACKET )
	static const unsigned char marker[4] = {
		0xde, 0xad, 0xbe, 0xef
	} ;
#endif

	if( SC1445x_AE_MODE_NORMAL == chip->mode ) {
		unsigned char* p = runtime->dma_area +
					frames_to_bytes( runtime, pos ) ;

		if( copy_from_user( p, src, nbytes ) )
			return -EFAULT ;

#if defined( TEST_WITH_MARKED_PACKET )
		if( !memcmp( p + SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER,
						marker, sizeof marker ) )
			P2_RESET_DATA_REG = 0x400 ;
#endif

#if defined( SC1445x_AE_COLLECT_STATISTICS )
		if( block_size == nbytes ) {
			unsigned short ch ;
			unsigned short type ;
			sc1445x_ae_audio_stats* stats ;

			for( ch = 0 ;  ch < nch ;  ++ch ) {
				type = ( *(unsigned short*)p ) >> 8 ;
				stats = &s->audio_channels[ch].stats ;

				if( 1 == type ) {
					++stats->normal_from_os ;
				} else if( 2 == type ) {
					++stats->sid_from_os ;
				}
				else {
					++stats->empty_from_os ;
				}

				p += SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER +
					SC1445x_AE_BYTES_PER_AUDIO_PACKET ;
			}
		}
		else {
			snd_printk( PRINT_LEVEL "%s: can't keep stats; block "
				"size is %d bytes -- wanted %d bytes\n",
					__FUNCTION__, nbytes, block_size ) ;
		}
#endif
	} else if( SC1445x_AE_MODE_RAW_PCM == chip->mode) {
		sc1445x_ae_raw_pcm_state* raw_pcm = &chip->ae_state.raw_pcm ;
		unsigned char* p = (unsigned char*)( raw_pcm->buffer +
			raw_pcm->next_block_index * raw_pcm->block_size ) ;

		p += raw_pcm->partial ;

		while( !raw_pcm->block_status[raw_pcm->next_block_index]  &&
								nbytes > 0 ) {
#if 0
			const unsigned bs = 2 * raw_pcm->block_size ;
#else
			const unsigned bs = ( runtime->sample_bits / 8 ) *
							raw_pcm->block_size ;
#endif
			unsigned len =  nbytes > bs ?  bs :  nbytes ;
			unsigned slen = len ;

			if( len  >  bs - raw_pcm->partial )
				slen = len = bs - raw_pcm->partial ;

#if 1
			if( 8 == runtime->sample_bits ) {
				unsigned char* samples = src ;
				unsigned short* pp = (void*)p ;
				int i ;

				for( i = 0 ;  i < len ;  ++i ) {
					unsigned char s ;

					get_user( s, samples + i ) ;
					pp[i] = (unsigned short)s << 5 ;
				}
				len <<= 1 ;
				/* now len is 2 times the value of slen */
			} else
#endif
			if( copy_from_user( p, src, len ) )
				return -EFAULT ;
			if( len + raw_pcm->partial  <  bs ) {
				/* pad with zeroes */
				memset( p + len, 0,
						bs - len - raw_pcm->partial ) ;
				raw_pcm->partial += len ;
			} else
				raw_pcm->partial = 0 ;

			/* advance pointers for next block */
			p += len ;
			src = (void __user *)( (unsigned char*)src + slen ) ;
			nbytes -= slen ;

			if( !raw_pcm->partial ) {
		                /* set the block status to 1; DSP will */
				/* clear it */
		                raw_pcm->block_status
					[raw_pcm->next_block_index] = 1 ;
				/* we've filled this buffer */
		                if( raw_pcm->block_count ==
						++raw_pcm->next_block_index ) {
		                        raw_pcm->next_block_index = 0 ;
        		                p = (unsigned char*)raw_pcm->buffer ;
	                	}
			}
		}
		if( unlikely( nbytes > 0 ) ) {
			PRINT( "%s: exiting with %d bytes left!\n",
						__FUNCTION__, nbytes ) ;
		}
	}
	//TODO: need to do something for the other modes?

	return 0 ;
}


/* capture PCM ops */
static int snd_sc1445x_capture_open( snd_pcm_substream_t* substream ) 
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	substream->runtime->hw = snd_sc1445x_capture ;

	chip->capture_substream = substream ;
	/* invalidate pointer for capture */
	chip->capture_w_pos = -1 ;

	snd_printd( "%s: chip @%p, runtime->hw @%p\n",
			__FUNCTION__, chip, &substream->runtime->hw ) ;
	return 0 ;
}


static int snd_sc1445x_capture_close( snd_pcm_substream_t* substream )
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	chip->capture_substream = NULL ;
	//TODO: stop audio engine?
	if( !chip->playback_substream )
		chip->started = 0 ;

	return 0 ;
}


static int snd_sc1445x_capture_prepare( snd_pcm_substream_t* substream )
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	if( !chip->started ) {
		//TODO: pass arg according to mode
		if( sc1445x_ae_set_mode( &chip->ae_state, chip->mode, NULL ) ) {
			return -EAGAIN ;
		}
		chip->started = 1 ;
	}

	chip->capture_w_pos = 0 ;

	return 0 ;
}


static int snd_sc1445x_capture_trigger( snd_pcm_substream_t* substream,
								int cmd )
{
	int res = 0 ;

	switch( cmd ) {
		case SNDRV_PCM_TRIGGER_START:
			snd_printd( "%s: START\n", __FUNCTION__ ) ;
			break ;

		case SNDRV_PCM_TRIGGER_STOP:
			snd_printd( "%s: STOP\n", __FUNCTION__ ) ;
			break ;

		default:
			res = -EINVAL ;
			break ;
	}

	return res ;
}


static snd_pcm_uframes_t
snd_sc1445x_capture_pointer( snd_pcm_substream_t* substream )
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	return bytes_to_frames( substream->runtime, chip->capture_w_pos ) ;
}


/* common PCM ops */
static int snd_sc1445x_hw_params( snd_pcm_substream_t* substream,
					snd_pcm_hw_params_t* hw_params )
{
	return snd_pcm_lib_malloc_pages( substream,
					params_buffer_bytes( hw_params ) ) ;
}


static int snd_sc1445x_hw_free( snd_pcm_substream_t* substream )
{
        snd_pcm_lib_free_pages( substream ) ;
        return 0 ;
}


/* get frames from the "DMA" buffer allocated by ALSA and give them to ALSA */
/* we assume that the application asks ALSA for enough frames for the */
/* maximum number (3) of audio channels -- if any channel(s) is(are) not */
/* being used, the corresponding frames are ignored */
/* frames for each audio packet are not interleaved */
static int snd_sc1445x_capture_copy( snd_pcm_substream_t* substream,
					int channel, snd_pcm_uframes_t pos,
					void __user * dst,
					snd_pcm_uframes_t count )
{
#if defined( USE_FORCED_PACKET )
	const unsigned char forced_pkt[ 3 * ( SC1445x_AE_BYTES_PER_AUDIO_PACKET
			+ SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER ) ] = {
		0x50, 0x01,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	} ;
#endif
#if defined( TEST_WITH_MARKED_PACKET )
	static const unsigned char marker[4] = {
		0xde, 0xad, 0xbe, 0xef
	} ;
#endif
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;
	snd_pcm_runtime_t* runtime = substream->runtime ;
	unsigned nbytes = frames_to_bytes( runtime, count ) ;
#if defined( SC1445x_AE_COLLECT_STATISTICS )
	sc1445x_ae_state* s = &chip->ae_state ;
	const unsigned short nch = s->audio_channels_count ;
	const unsigned short block_size = nch *
				( SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER +
					SC1445x_AE_BYTES_PER_AUDIO_PACKET )
#if 0
//#  if defined( SC1445x_AE_SUPPORT_FAX )
				+ ( SC1445x_AE_BYTES_PER_FAX_PACKET_HEADER
					+ SC1445x_AE_BYTES_PER_FAX_PACKET ) ;
#  else
							+ 0 ;
#  endif
#endif

	if( SC1445x_AE_MODE_NORMAL == chip->mode ) {
#if !defined( USE_FORCED_PACKET )

		unsigned char* p = runtime->dma_area +
					frames_to_bytes( runtime, pos ) ;

		if( copy_to_user( dst, p, nbytes ) )
			return -EFAULT ;

#if defined( TEST_WITH_MARKED_PACKET )
		if( !memcmp( p + SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER,
						marker, sizeof marker ) )
			P2_RESET_DATA_REG = 0x400 ;
#endif

#if defined( SC1445x_AE_COLLECT_STATISTICS )
		if( block_size == nbytes ) {
			unsigned short ch ;
			unsigned short type ;
			sc1445x_ae_audio_stats* stats ;

			for( ch = 0 ;  ch < nch ;  ++ch ) {
				type = ( *(unsigned short*)p ) >> 8 ;
				stats = &s->audio_channels[ch].stats ;

				if( 1 == type ) {
					++stats->normal_to_os ;
				} else if( 2 == type ) {
					++stats->sid_to_os ;
				}
				else {
					++stats->empty_to_os ;
				}

				p += SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER +
					SC1445x_AE_BYTES_PER_AUDIO_PACKET ;
			}
		}
		else {
			snd_printk( PRINT_LEVEL "%s: can't keep stats; block "
				"size is %d bytes -- wanted %d bytes\n",
					__FUNCTION__, nbytes, block_size ) ;
		}
#endif

#else
		if( sizeof( forced_pkt ) != nbytes )
			printk( "%s: count=%d (we expected %d)\n",
				__FUNCTION__, nbytes, sizeof( forced_pkt ) ) ;
		else {
			if( copy_to_user( dst, forced_pkt, nbytes ) )
				return -EFAULT ;
		}
#endif
	}
	//TODO: need to do something for the other modes?

	return 0 ;
}



/* PCM ops */
static struct snd_pcm_ops snd_sc1445x_playback_ops = {
	.open =		snd_sc1445x_playback_open,
	.close =	snd_sc1445x_playback_close,
	.ioctl =	snd_pcm_lib_ioctl,
	.hw_params =	snd_sc1445x_hw_params,
	.hw_free =	snd_sc1445x_hw_free,
	.prepare =	snd_sc1445x_playback_prepare,
	.trigger =	snd_sc1445x_playback_trigger,
	.pointer =	snd_sc1445x_playback_pointer,
	.copy =		snd_sc1445x_playback_copy,
} ;

static struct snd_pcm_ops snd_sc1445x_capture_ops = {
	.open =		snd_sc1445x_capture_open,
	.close =	snd_sc1445x_capture_close,
	.ioctl =	snd_pcm_lib_ioctl,
	.hw_params =	snd_sc1445x_hw_params,
	.hw_free =	snd_sc1445x_hw_free,
	.prepare =	snd_sc1445x_capture_prepare,
	.trigger =	snd_sc1445x_capture_trigger,
	.pointer =	snd_sc1445x_capture_pointer,
	.copy =		snd_sc1445x_capture_copy,
} ;


static int __init snd_sc1445x_pcm_new( struct snd_sc1445x* chip )
{
	struct snd_pcm* pcm ;
	int res ;

	res = snd_pcm_new( chip->card, chip->card->driver, 0, 1, 1, &pcm ) ;
	if( res < 0 )
		return res ;

	snd_pcm_set_ops( pcm, SNDRV_PCM_STREAM_PLAYBACK,
						&snd_sc1445x_playback_ops ) ;
	snd_pcm_set_ops( pcm, SNDRV_PCM_STREAM_CAPTURE,
						&snd_sc1445x_capture_ops ) ;
	pcm->private_data = chip ;
	strncpy( pcm->name, chip->card->shortname, sizeof( pcm->name ) - 1 ) ;
	chip->pcm = pcm ;

	snd_pcm_lib_preallocate_pages_for_all( pcm, SNDRV_DMA_TYPE_CONTINUOUS,
					snd_dma_continuous_data(GFP_KERNEL),
								2560, 2560 ) ;

	return 0 ;
}


/* open hwdep */
static int snd_sc1445x_hwdep_open( struct snd_hwdep* hw, struct file* file )
{
	return 0 ;
}


/* close hwdep */
static int snd_sc1445x_hwdep_release( struct snd_hwdep* hw, struct file* file )
{
	return 0 ;
}


/* an ugly hack to initialize const fields of a struct */
#define INIT_CONST_FIELD( type, s, x, val )	\
	*( type* )&( (s)->x ) = (val)

#define REASSIGN_FIELD_PTR_AND_ADVANCE( params, field, p, n ) 		\
	do {								\
		params->field = p ;					\
		p += n ;						\
	} while( 0 ) ;

#define REASSIGN_ARRAY_PTR_AND_ADVANCE( a, p, sz ) 			\
	do {								\
		a = p ;							\
		p = (void*)( (unsigned long)p + sz ) ;			\
	} while( 0 ) ;

static short copy_ap_to_user( unsigned long arg,
					const sc1445x_ae_audio_profile* src_ap )
{
	sc1445x_ae_audio_profile* __user dst_ap = (void __user*)arg ;
	unsigned short i, j, n ;
	const sc1445x_ae_vspk_volumes* src_vsvs ;
	const sc1445x_ae_vmic_gains* src_vmgs ;
	sc1445x_ae_vspk_volumes* dst_vsvs ;
	sc1445x_ae_vmic_gains* dst_vmgs ;
	unsigned short __user* p ;
	unsigned short __user** pp ;
	int sz ;

	if( copy_to_user( dst_ap->data, src_ap->data, dst_ap->data_nbytes ) )
		return -EFAULT ;

	//TODO: copy stuff with _user macros

	/* update array pointers and copy data */
	p = dst_ap->data ;

	/* vspk_lvl_narrow and vspk_lvl_wide */
	n = src_ap->vspk_lvl_count ;
	for( i = 0 ;  i < SC1445x_AE_VMIC_VSPK_COUNT ;  ++i ) {
		dst_vsvs = &dst_ap->vspk_lvl_narrow[i] ;
		src_vsvs = &src_ap->vspk_lvl_narrow[i] ;

		dst_vsvs->use_analog = src_vsvs->use_analog ;
		dst_vsvs->use_rx_path_att = src_vsvs->use_rx_path_att ;
		dst_vsvs->use_rx_path_shift = src_vsvs->use_rx_path_shift ;
		dst_vsvs->use_sidetone_att = src_vsvs->use_sidetone_att ;
		dst_vsvs->use_ext_spk_att = src_vsvs->use_ext_spk_att ;
		dst_vsvs->use_shift_ext_spk = src_vsvs->use_shift_ext_spk ;
		dst_vsvs->use_tone_vol = src_vsvs->use_tone_vol ;
		dst_vsvs->use_classd_vout = src_vsvs->use_classd_vout ;

		DPRINT( "%s: narrow vsv #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, analog_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, rx_path_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, rx_path_shift_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, sidetone_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ext_spk_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ring_playback_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, shift_ext_spk_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ring_tone_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, call_tone_codec_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, call_tone_classd_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, dtmf_tone_codec_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, dtmf_tone_classd_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, classd_vout, p, n ) ;

		dst_vsvs = &dst_ap->vspk_lvl_wide[i] ;
		src_vsvs = &src_ap->vspk_lvl_wide[i] ;

		dst_vsvs->use_analog = src_vsvs->use_analog ;
		dst_vsvs->use_rx_path_att = src_vsvs->use_rx_path_att ;
		dst_vsvs->use_rx_path_shift = src_vsvs->use_rx_path_shift ;
		dst_vsvs->use_sidetone_att = src_vsvs->use_sidetone_att ;
		dst_vsvs->use_ext_spk_att = src_vsvs->use_ext_spk_att ;
		dst_vsvs->use_shift_ext_spk = src_vsvs->use_shift_ext_spk ;
		dst_vsvs->use_tone_vol = src_vsvs->use_tone_vol ;
		dst_vsvs->use_classd_vout = src_vsvs->use_classd_vout ;


		DPRINT( "%s: wide vsv #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, analog_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, rx_path_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, rx_path_shift_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, sidetone_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ext_spk_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ring_playback_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, shift_ext_spk_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ring_tone_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, call_tone_codec_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, call_tone_classd_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, dtmf_tone_codec_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, dtmf_tone_classd_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, classd_vout, p, n ) ;
	}
	INIT_CONST_FIELD( unsigned short, dst_ap, vspk_lvl_count, n ) ;

	/* vmic_lvl_narrow and vmic_lvl_wide */
	n = src_ap->vmic_lvl_count ;
	for( i = 0 ;  i < SC1445x_AE_VMIC_VSPK_COUNT ;  ++i ) {
		dst_vmgs = &dst_ap->vmic_lvl_narrow[i] ;
		src_vmgs = &src_ap->vmic_lvl_narrow[i] ;

		dst_vmgs->use_analog = src_vmgs->use_analog ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		dst_vmgs->use_shift_paec_out = src_vmgs->use_shift_paec_out ;
		dst_vmgs->use_paec_tx_att = src_vmgs->use_paec_tx_att ;
		dst_vmgs->use_attlimit = src_vmgs->use_attlimit ;
		dst_vmgs->use_supmin = src_vmgs->use_supmin ;
#endif

		DPRINT( "%s: narrow vmg #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, analog_levels, p, n ) ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, shift_paec_out_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, paec_tx_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, paec_tx_att_levels_BT, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, attlimit, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, supmin, p, n ) ;
#endif

		dst_vmgs = &dst_ap->vmic_lvl_wide[i] ;
		src_vmgs = &src_ap->vmic_lvl_wide[i] ;

		dst_vmgs->use_analog = src_vmgs->use_analog ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		dst_vmgs->use_shift_paec_out = src_vmgs->use_shift_paec_out ;
		dst_vmgs->use_paec_tx_att = src_vmgs->use_paec_tx_att ;
		dst_vmgs->use_attlimit = src_vmgs->use_attlimit ;
		dst_vmgs->use_supmin = src_vmgs->use_supmin ;
#endif

		DPRINT( "%s: wide vmg #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, analog_levels, p, n ) ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, shift_paec_out_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, paec_tx_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, paec_tx_att_levels_BT, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, attlimit, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, supmin, p, n ) ;
#endif
	}
	INIT_CONST_FIELD( unsigned short, dst_ap, vmic_lvl_count, n ) ;

	/* filters */
	for( i = 0 ;  i < 3 ;  ++i ) {
		for( j = 0 ;  j < FILTER_SIZE ;  ++j ) {
			dst_ap->narrowband_filters_TX1[i][j] =
					src_ap->narrowband_filters_TX1[i][j] ;
			dst_ap->narrowband_filters_TX2[i][j] =
					src_ap->narrowband_filters_TX2[i][j] ;
			dst_ap->narrowband_filters_TX3[i][j] =
					src_ap->narrowband_filters_TX3[i][j] ;
			dst_ap->wideband_filters_TX1[i][j] =
					src_ap->wideband_filters_TX1[i][j] ;
			dst_ap->wideband_filters_TX2[i][j] =
					src_ap->wideband_filters_TX2[i][j] ;
			dst_ap->wideband_filters_TX3[i][j] =
					src_ap->wideband_filters_TX3[i][j] ;

			dst_ap->narrowband_filters_RX1[i][j] =
					src_ap->narrowband_filters_RX1[i][j] ;
			dst_ap->narrowband_filters_RX2[i][j] =
					src_ap->narrowband_filters_RX2[i][j] ;
			dst_ap->narrowband_filters_RX3[i][j] =
					src_ap->narrowband_filters_RX3[i][j] ;
			dst_ap->wideband_filters_RX1[i][j] =
					src_ap->wideband_filters_RX1[i][j] ;
			dst_ap->wideband_filters_RX2[i][j] =
					src_ap->wideband_filters_RX2[i][j] ;
			dst_ap->wideband_filters_RX3[i][j] =
					src_ap->wideband_filters_RX3[i][j] ;
		}
	}

#if defined( CONFIG_SND_SC1445x_USE_PAEC )
	/* PAEC state array setup */
	n = src_ap->paec_band_count ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: paec_band_loc \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_band_loc, p, sz ) ;
	DPRINT( "%s: paec_band_narrow \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_band_narrow, p, sz ) ;
	DPRINT( "%s: paec_band_wide \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_band_wide, p, sz ) ;
	INIT_CONST_FIELD( unsigned short, dst_ap, paec_band_count, n ) ;

	/* PAEC parameters setup */
	n = src_ap->paec_data_count ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: paec_data_loc \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_data_loc, p, sz ) ;
	DPRINT( "%s: paec_date_narrow \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_data_narrow, p, sz ) ;
	DPRINT( "%s: paec_data_wide \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_data_wide, p, sz ) ;
	INIT_CONST_FIELD( unsigned short, dst_ap, paec_data_count, n ) ;

	/* Suppressor Setup*/
	n = src_ap->supp_params_count ;
	sz = n * sizeof(unsigned short*) ;
	DPRINT( "%s: supp_params_addr \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	pp = (void*)p ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->supp_params_addr, pp, sz ) ;
	p = (void*)pp ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: supp_params_data_narrow \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->supp_params_data_narrow, p, sz ) ;
	DPRINT( "%s: supp_params_data_wide \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->supp_params_data_wide, p, sz ) ;
	INIT_CONST_FIELD( unsigned short, dst_ap, supp_params_count, n ) ;
#endif

#if defined( CONFIG_SND_SC1445x_USE_AEC )
	/* AEC setup */
	n = src_ap->aec_params_count ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: aec_params_data \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->aec_params_data, p, sz ) ;
	INIT_CONST_FIELD( unsigned short, dst_ap, aec_params_count, n ) ;
#endif

	return 0 ;
}

static short copy_ap_from_user( sc1445x_ae_audio_profile* dst_ap,
							unsigned  long arg )
{
	//TODO: do it properly
	const sc1445x_ae_audio_profile* src_ap = (void*)arg ;

	dst_ap->data = MALLOC( src_ap->data_nbytes ) ;
	if( !dst_ap->data ) {
		PRINT( "%s: could not allocate memory for dynamic data\n",
								__FUNCTION__ ) ;
		return -ENOMEM ;
	}
	dst_ap->data_nbytes = src_ap->data_nbytes ;

	return copy_ap_to_user( (unsigned long)dst_ap, src_ap ) ;
}


#define READ_USER_ARG( dst, arg ) \
	copy_from_user( &(dst), (void __user*)(arg), sizeof( dst ) )

#define WRITE_USER_ARG( arg, src ) \
	copy_to_user( (void __user*)(arg), &(src), sizeof( src ) )

/* hwdep ioctls */
static int snd_sc1445x_hwdep_ioctl( struct snd_hwdep* hw, struct file* file,
					unsigned int cmd, unsigned long arg )
{
	struct snd_sc1445x* chip = hw->private_data ;
	sc1445x_audio_mode_t mode_t ;
	sc1445x_start_audio_channel_t start_t ;
	sc1445x_stop_audio_channel_t stop_t ;
	sc1445x_tone_t tone_t ;
	sc1445x_stop_tone_t stop_tone_t ;
	sc1445x_auto_stop_tone_t auto_stop_tone_t ;
	sc1445x_std_tone_t std_tone_t ;
	sc1445x_tone_vol_t tone_vol_t ;
	sc1445x_custom_tone_t custom_tone_t ;
	sc1445x_tone_seq_t tone_seq_t ;
	sc1445x_send_tone_ex_t send_tone_ex_t ;
	sc1445x_sidetone_vol_t sidetone_vol_t ;
#if defined( SC1445x_AE_COLLECT_STATISTICS )
	sc1445x_audio_stats_t audio_stats_t ;
#endif
	sc1445x_ae_version_t ae_version_t ;
	sc1445x_platform_info_t platform_info_t ;
	sc1445x_iface_mode_t if_mode_t ;
	sc1445x_ae_vspk_vol_t vspk_vol_t ;
	sc1445x_ae_vmic_gain_t vmic_gain_t ;
	sc1445x_vol_gain_count_t vol_gain_count_t ;
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	sc1445x_connect_to_line_t conn_to_line_t ;
	sc1445x_connect_lines_t conn_lines_t ;
	sc1445x_disconnect_line_t disconn_line_t ;
	sc1445x_set_cid_info_t set_cid_info_t ;
	sc1445x_cid_ind_t cid_ind_t ;
#endif
#if defined( SC1445x_AE_PCM_LINES_SUPPORT ) || defined( SC1445x_AE_PHONE_DECT )
	sc1445x_line_type_t line_type_t ;
#endif
#if defined( SC1445x_AE_BT )
	sc1445x_attach_to_pcm_dev_t attach_to_pcm_dev_t ;
	sc1445x_detach_from_pcm_dev_t detach_from_pcm_dev_t ;
#endif
#if defined( SC1445x_AE_SUPPORT_FAX )
	sc1445x_audio_to_fax_t audio2fax_t ;
	sc1445x_fax_to_audio_t fax2audio_t ;
	sc1445x_fax_init_params_t fax_init_params_t ;
#endif
	sc1445x_ae_ctrl_dsp_loopback_t dsp_lb ;
#if defined( SC1445x_AE_USE_CONVERSATIONS )
	sc1445x_conversation_t conv ;
	sc1445x_conversation_add_t conv_add ;
	sc1445x_conversation_rem_t conv_rem ;
#endif
	sc1445x_ae_audio_profile ap ;

	int res = 0 ;

	snd_assert( chip != NULL, return -EINVAL ) ;

	DPRINT( PRINT_LEVEL "%s: cmd=%02x\n", __FUNCTION__, cmd & 0xff ) ;
	switch( cmd ) {
		case SNDRV_SC1445x_SET_MODE:
			if( copy_from_user( &mode_t, (void __user*)arg,
							sizeof( mode_t ) ) )
				res = -EFAULT ;
			else {
				void* sm_arg = NULL ;
				sc1445x_ae_state* ae_state = &chip->ae_state ;

				if( SC1445x_AE_MODE_AUTOPLAY == mode_t.mode ) {
					if( mode_t.auto_play.nchannels !=
					     ae_state->audio_channels_count ) {
						/* # channels must match */
						res = -EINVAL ;
						break ;
					}

					sm_arg = mode_t.auto_play.pointers ;
				}

				if( SC1445x_AE_MODE_RAW_PCM == mode_t.mode ) {
					/* don't actually change the mode now */
					/* it will be set by playback_open() */
					chip->mode = SC1445x_AE_MODE_RAW_PCM ;
				}
				else {
					res = sc1445x_ae_set_mode( ae_state,
							mode_t.mode, sm_arg ) ;
					chip->mode = ae_state->mode ;
				}
			}
			break ;

		case SNDRV_SC1445x_GET_MODE:
			memset( &mode_t, 0, sizeof( mode_t ) ) ;
			res = sc1445x_ae_get_mode( &chip->ae_state,
							&mode_t.mode ) ;
			if( res )
				break ;
			if( copy_to_user( (void __user*)arg, &mode_t,
							sizeof( mode_t ) ) )
				res = -EFAULT ;
			break ;

		case SNDRV_SC1445x_START_AUDIO_CHANNEL:
			if( copy_from_user( &start_t, (void __user*)arg,
							sizeof( start_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_activate_free_channel(
						&chip->ae_state,
						start_t.enc_codec,
						start_t.dec_codec,
						&start_t.activated_channel ) ;
				if( res )
					break ;  /* error */
				if( copy_to_user( (void __user*)arg, &start_t,
							sizeof( start_t ) ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_STOP_AUDIO_CHANNEL:
			if( copy_from_user( &stop_t, (void __user*)arg,
							sizeof( stop_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_deactivate_channel(
						&chip->ae_state,
						stop_t.channel_index ) ;
			}
			break ;

		case SNDRV_SC1445x_START_TONE:
			tone_t.is_key_tone = 0 ;
			if( copy_from_user( &tone_t, (void __user*)arg,
							sizeof( tone_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				tone_t.tg_mod = sc1445x_private_line2stream(
					&chip->ae_state, tone_t.tg_mod, 1 ) ;
#endif
				if( tone_t.is_key_tone )
					res = sc1445x_ae_start_key_tone(
						&chip->ae_state,
						tone_t.tg_mod, tone_t.tone1,
						tone_t.tone2, tone_t.tone3,
						tone_t.dur_on, tone_t.dur_off,
							tone_t.repeat ) ;
				else
					res = sc1445x_ae_start_tone(
						&chip->ae_state,
						tone_t.tg_mod, tone_t.tone1,
						tone_t.tone2, tone_t.tone3,
						tone_t.dur_on, tone_t.dur_off,
							tone_t.repeat ) ;
			}
			break ;

		case SNDRV_SC1445x_START_CUSTOM_TONE:
			if( copy_from_user( &custom_tone_t, (void __user*)arg,
						sizeof( custom_tone_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				custom_tone_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						custom_tone_t.tg_mod, 1 ) ;
#endif
				res = sc1445x_ae_start_custom_tone(
						&chip->ae_state,
						custom_tone_t.tg_mod,
						&custom_tone_t.params,
						custom_tone_t.dur_on,
						custom_tone_t.dur_off,
						custom_tone_t.repeat ) ;
			}
			break ;

		case SNDRV_SC1445x_START_TONE_SEQUENCE:
			tone_seq_t.is_ringtone = 0 ;
			if( copy_from_user( &tone_seq_t, (void __user*)arg,
						sizeof( tone_seq_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				tone_seq_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						tone_seq_t.tg_mod, 1 ) ;
#endif
				if( !tone_seq_t.is_ringtone )
					res = sc1445x_ae_start_tone_sequence(
						&chip->ae_state,
						tone_seq_t.tg_mod,
						tone_seq_t.seq_len,
						tone_seq_t.seq,
						tone_seq_t.repeat_seq ) ;
				else
					res = sc1445x_ae_start_ringtone_sequence(
						&chip->ae_state,
						tone_seq_t.tg_mod,
						tone_seq_t.seq_len,
						tone_seq_t.seq,
						tone_seq_t.repeat_seq ) ;
			}
			break ;

		case SNDRV_SC1445x_EXPAND_TONE_SEQUENCE:
			if( copy_from_user( &tone_seq_t, (void __user*)arg,
						sizeof( tone_seq_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				tone_seq_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						tone_seq_t.tg_mod, 1 ) ;
#endif
				res = sc1445x_ae_expand_tone_sequence(
						&chip->ae_state,
						tone_seq_t.tg_mod,
						tone_seq_t.seq_len,
						tone_seq_t.seq ) ;
			}
			break ;

		case SNDRV_SC1445x_STOP_TONE:
			stop_tone_t.tg_mod = 0 ;
			if( arg && copy_from_user( &stop_tone_t,
						(void __user*)arg,
						sizeof( stop_tone_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				stop_tone_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						stop_tone_t.tg_mod, 0 ) ;
#endif
				res = sc1445x_ae_stop_tone( &chip->ae_state,
							stop_tone_t.tg_mod ) ;
			}
			break ;

		case SNDRV_SC1445x_AUTO_STOP_TONE:
			if( arg && copy_from_user( &auto_stop_tone_t,
						(void __user*)arg,
						sizeof( auto_stop_tone_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				auto_stop_tone_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						auto_stop_tone_t.tg_mod, 0 ) ;
#endif
				res = sc1445x_ae_auto_stop_tone( &chip->ae_state,
					auto_stop_tone_t.tg_mod,
					auto_stop_tone_t.stop_interval ) ;
			}
			break ;

		case SNDRV_SC1445x_PLAY_STD_TONE:
			if( copy_from_user( &std_tone_t, (void __user*)arg,
							sizeof( std_tone_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				std_tone_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						std_tone_t.tg_mod, 1 ) ;
#endif
				res = sc1445x_ae_play_standard_tone(
							&chip->ae_state,
							std_tone_t.tg_mod,
							std_tone_t.tone ) ;
			}
			break ;

		case SNDRV_SC1445x_SET_TONE_VOL:
			if( copy_from_user( &tone_vol_t, (void __user*)arg,
							sizeof( tone_vol_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				tone_vol_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						tone_vol_t.tg_mod, 1 ) ;
#endif
				res = sc1445x_ae_set_tone_volume(
							&chip->ae_state,
							tone_vol_t.tg_mod,
							tone_vol_t.vol ) ;
			}
			break ;

		case SNDRV_SC1445x_GET_TONE_VOL:
			if( copy_from_user( &tone_vol_t, (void __user*)arg,
						sizeof( tone_vol_t ) ) ) {
				res = -EFAULT ;
				break ;
			}

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
			tone_vol_t.tg_mod = sc1445x_private_line2stream(
							&chip->ae_state,
							tone_vol_t.tg_mod, 1 ) ;
#endif
			res = sc1445x_ae_get_tone_volume( &chip->ae_state,
							tone_vol_t.tg_mod,
							&tone_vol_t.vol ) ;
			if( res )
				break ;  /* error */
			if( copy_to_user( (void __user*)arg, &tone_vol_t,
						sizeof( tone_vol_t ) ) )
				res = -EFAULT ;
			break ;

		case SNDRV_SC1445x_MUTE_SPK:
			res = sc1445x_ae_mute_spk( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_UNMUTE_SPK:
			res = sc1445x_ae_unmute_spk( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_MUTE_MIC:
			res = sc1445x_ae_mute_mic( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_UNMUTE_MIC:
			res = sc1445x_ae_unmute_mic( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_SEND_TONES:
			res = sc1445x_ae_set_tone_volume_tx( &chip->ae_state,
					SC1445x_AE_TONEGEN_DEFAULT_VOLUME_TX ) ;
			break ;

		case SNDRV_SC1445x_DONT_SEND_TONES:
			res = sc1445x_ae_set_tone_volume_tx( &chip->ae_state,
								0x0000 ) ;
			break ;

		case SNDRV_SC1445x_SEND_TONES_EX:
			if( copy_from_user( &send_tone_ex_t, (void __user*)arg,
						sizeof( send_tone_ex_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_tone_volume_tx_ex(
					&chip->ae_state, send_tone_ex_t.tg_mod,
					SC1445x_AE_TONEGEN_DEFAULT_VOLUME_TX ) ;
			}
			break ;

		case SNDRV_SC1445x_DONT_SEND_TONES_EX:
			if( copy_from_user( &send_tone_ex_t, (void __user*)arg,
						sizeof( send_tone_ex_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_tone_volume_tx_ex(
					&chip->ae_state, send_tone_ex_t.tg_mod,
								0x0000 ) ;
			}
			break ;

		case SNDRV_SC1445x_SET_SIDETONE_VOL:
			if( copy_from_user( &sidetone_vol_t, (void __user*)arg,
						sizeof( sidetone_vol_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_sidetone_volume(
							&chip->ae_state,
							sidetone_vol_t.vol ) ;
			}
			break ;

		case SNDRV_SC1445x_GET_SIDETONE_VOL:
			res = sc1445x_ae_get_sidetone_volume( &chip->ae_state,
							&sidetone_vol_t.vol ) ;
			if( res )
				break ;  /* error */
			if( copy_to_user( (void __user*)arg, &sidetone_vol_t,
						sizeof( sidetone_vol_t ) ) )
				res = -EFAULT ;
			break ;

		case SNDRV_SC1445x_TURN_AEC_ON:
			res = sc1445x_ae_set_aec_on( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_TURN_AEC_OFF:
			res = sc1445x_ae_set_aec_off( &chip->ae_state ) ;
			break ;

#if defined( SC1445x_AE_COLLECT_STATISTICS )
		case SNDRV_SC1445x_GET_AUDIO_CHANNEL_STATS:
			if( copy_from_user( &audio_stats_t, (void __user*)arg,
						sizeof( audio_stats_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_channel_stats(
					&chip->ae_state,
					audio_stats_t.channel_index,
					&audio_stats_t.stats ) ;
				if( res )
					break ;  /* error */
				if( copy_to_user( (void __user*)arg,
						&audio_stats_t,
						sizeof( audio_stats_t ) ) )
					res = -EFAULT ;
			}
			
			break ;

		case SNDRV_SC1445x_RESET_AUDIO_STATS:
			{
				const unsigned short nch =
					chip->ae_state.audio_channels_count ;
				unsigned short ch ;

				for( ch = 0 ;  ch < nch ;  ++ch )
					sc1445x_ae_reset_channel_stats(
						&chip->ae_state, ch ) ;
			}
			break ;
#endif

		case SNDRV_SC1445x_GET_AUDIO_DRIVER_VERSION:
			strncpy( ae_version_t.id, SC1445x_AUDIO_DRIVER_VERSION,
					sizeof( ae_version_t.id ) - 1 ) ;
			ae_version_t.id[sizeof( ae_version_t.id ) - 1] = '\0' ;
			if( copy_to_user( (void __user*)arg,
						&ae_version_t,
						sizeof( ae_version_t ) ) )
				res = -EFAULT ;

			break ;

		case SNDRV_SC1445x_GET_PLATFORM_INFO:
			platform_info_t.type = 
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
					SC14452_4_CHANNELS ;
#else
					SC14450_3_CHANNELS ;
#endif
			if( copy_to_user( (void __user*)arg,
						&platform_info_t,
						sizeof( platform_info_t ) ) )
				res = -EFAULT ;

			break ;

		case SNDRV_SC1445x_GET_DSP_FIRMWARE_VERSION:
			{
				char dsp_fw_ver_major = *dsp1_fw_ver >> 8 ;
				char dsp_fw_ver_minor = *dsp1_fw_ver ;

				snprintf( ae_version_t.id,
					sizeof ae_version_t.id, "%d.%d-%s",
					dsp_fw_ver_major, dsp_fw_ver_minor,
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
					"A/D"	/* ATA/DECT */
#else
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
					"PP"	/* Phone, PAEC */
#else
					"PA"	/* Phone, AEC */
#endif
#endif
					 ) ;
				if( copy_to_user( (void __user*)arg,
							&ae_version_t,
						sizeof( ae_version_t ) ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_GET_DSP2_FIRMWARE_VERSION:
			{
				char dsp_fw_ver_major = *dsp2_fw_ver >> 8 ;
				char dsp_fw_ver_minor = *dsp2_fw_ver ;

				snprintf( ae_version_t.id,
					sizeof ae_version_t.id, "%d.%d",
					dsp_fw_ver_major, dsp_fw_ver_minor ) ;
				if( copy_to_user( (void __user*)arg,
							&ae_version_t,
						sizeof( ae_version_t ) ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_SET_IFACE_MODE:
			if( copy_from_user( &if_mode_t, (void __user*)arg,
							sizeof( if_mode_t ) ) )
				res = -EFAULT ;
			else
				res = sc1445x_ae_set_iface_mode(
					&chip->ae_state, if_mode_t.mode ) ;
			break ;

		case SNDRV_SC1445x_GET_IFACE_MODE:
			res = sc1445x_ae_get_iface_mode( &chip->ae_state,
							&if_mode_t.mode ) ;
			if( res )
				break ;
			if( copy_to_user( (void __user*)arg, &if_mode_t,
							sizeof( if_mode_t ) ) )
				res = -EFAULT ;
			break ;

		case SNDRV_SC1445x_SET_VIRTUAL_SPK_VOL:
			if( copy_from_user( &vspk_vol_t, (void __user*)arg,
							sizeof( vspk_vol_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_vspk_volume(
					&chip->ae_state, vspk_vol_t.level ) ;
			}
			break ;

		case SNDRV_SC1445x_SET_VIRTUAL_SPK_DTMF_LVL:
			if( copy_from_user( &vspk_vol_t, (void __user*)arg,
							sizeof( vspk_vol_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_vspk_dtmf_level(
					&chip->ae_state, vspk_vol_t.level ) ;
			}
			break ;

		case SNDRV_SC1445x_GET_VIRTUAL_SPK_VOL:
			if( copy_from_user( &vspk_vol_t, (void __user*)arg,
							sizeof( vspk_vol_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_vspk_volume(
					&chip->ae_state, &vspk_vol_t.level ) ;
				if( res )
					break ;  /* error */
				if( copy_to_user( (void __user*)arg,
							&vspk_vol_t,
							sizeof( vspk_vol_t ) ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_GET_VIRTUAL_SPK_DTMF_LVL:
			if( copy_from_user( &vspk_vol_t, (void __user*)arg,
							sizeof( vspk_vol_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_vspk_dtmf_level(
					&chip->ae_state, &vspk_vol_t.level ) ;
				if( res )
					break ;  /* error */
				if( copy_to_user( (void __user*)arg,
							&vspk_vol_t,
							sizeof( vspk_vol_t ) ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_SET_VIRTUAL_MIC_GAIN:
			if( copy_from_user( &vmic_gain_t, (void __user*)arg,
						sizeof( vmic_gain_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_vmic_gain(
					&chip->ae_state, vmic_gain_t.level ) ;
			}
			break ;

		case SNDRV_SC1445x_GET_VIRTUAL_MIC_GAIN:
			if( copy_from_user( &vmic_gain_t, (void __user*)arg,
						sizeof( vmic_gain_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_vmic_gain(
					&chip->ae_state, &vmic_gain_t.level ) ;
				if( res )
					break ;  /* error */
				if( copy_to_user( (void __user*)arg,
						&vmic_gain_t,
						sizeof( vmic_gain_t ) ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_MUTE_VIRTUAL_MIC:
			res = sc1445x_ae_mute_vmic( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_UNMUTE_VIRTUAL_MIC:
			res = sc1445x_ae_unmute_vmic( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_GET_VOL_GAIN_LEVEL_COUNT:
			if( copy_from_user( &vol_gain_count_t,
						(void __user*)arg,
						sizeof( vol_gain_count_t ) ) )
				res = -EFAULT ;
			else {
				sc1445x_ae_get_vspk_vol_level_count(
					&chip->ae_state,
					&vol_gain_count_t.vol_level_count ) ;
				sc1445x_ae_get_vmic_gain_level_count(
					&chip->ae_state,
					&vol_gain_count_t.gain_level_count ) ;
				if( copy_to_user( (void __user*)arg,
						&vol_gain_count_t,
						sizeof( vol_gain_count_t ) ) )
					res = -EFAULT ;
			}
			break ;

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )

		case SNDRV_SC1445x_CONNECT_TO_LINE:
			if( copy_from_user( &conn_to_line_t, (void __user*)arg,
						sizeof( conn_to_line_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_connect_to_line(
					&chip->ae_state,
					conn_to_line_t.channel_index,
					conn_to_line_t.line ) ;
			}
			break ;

		case SNDRV_SC1445x_CONNECT_LINES:
			if( copy_from_user( &conn_lines_t, (void __user*)arg,
						sizeof( conn_lines_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_connect_lines(
					&chip->ae_state,
					conn_lines_t.line1,
					conn_lines_t.line2 ) ;
			}
			break ;

		case SNDRV_SC1445x_DISCONNECT_LINE:
			if( copy_from_user( &disconn_line_t, (void __user*)arg,
						sizeof( disconn_line_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_disconnect_line(
					&chip->ae_state,
					disconn_line_t.line ) ;
			}
			break ;

		case SNDRV_SC1445x_SET_CID_INFO:
			if( copy_from_user( &set_cid_info_t,
						(void __user*)arg,
						sizeof( set_cid_info_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_cid_info( &chip->ae_state,
					set_cid_info_t.line,
					set_cid_info_t.month,
					set_cid_info_t.day, set_cid_info_t.hour,
					set_cid_info_t.minutes,
					set_cid_info_t.number,
					set_cid_info_t.name ) ;
			}
			break ;

		case SNDRV_SC1445x_CID_IND_FIRST_RING:
			if( copy_from_user( &cid_ind_t, (void __user*)arg,
						sizeof( cid_ind_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_cid_ind_first_ring(
					&chip->ae_state, cid_ind_t.line ) ;
			}
			break ;

		case SNDRV_SC1445x_CID_IND_OFF_HOOK:
			if( copy_from_user( &cid_ind_t, (void __user*)arg,
						sizeof( cid_ind_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_cid_ind_off_hook(
					&chip->ae_state, cid_ind_t.line ) ;
			}
			break ;

		case SNDRV_SC1445x_CID_IND_ON_HOOK:
			if( copy_from_user( &cid_ind_t, (void __user*)arg,
						sizeof( cid_ind_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_cid_ind_on_hook(
					&chip->ae_state, cid_ind_t.line ) ;
			}
			break ;

#endif  /* SC1445x_AE_PCM_LINES_SUPPORT */

#if defined( SC1445x_AE_PCM_LINES_SUPPORT ) || defined( SC1445x_AE_PHONE_DECT )
		case SNDRV_SC1445x_SET_LINE_TYPE:
			if( copy_from_user( &line_type_t, (void __user*)arg,
						sizeof( line_type_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_line_type(
					&chip->ae_state, line_type_t.line,
							line_type_t.type ) ;
			}
			break ;

		case SNDRV_SC1445x_GET_LINE_TYPE:
			if( copy_from_user( &line_type_t, (void __user*)arg,
						sizeof( line_type_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_line_type(
					&chip->ae_state, line_type_t.line,
							&line_type_t.type ) ;
				if( res )
					break ;  /* error */
				if( copy_to_user( (void __user*)arg,
						&line_type_t,
						sizeof( line_type_t ) ) )
					res = -EFAULT ;
			}
			break ;
#endif

#if defined( SC1445x_AE_BT )
		case SNDRV_SC1445x_ATTACH_TO_PCM_DEV:
			if( copy_from_user( &attach_to_pcm_dev_t,
					(void __user*)arg,
					sizeof( attach_to_pcm_dev_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_attach_to_pcm( &chip->ae_state,
						attach_to_pcm_dev_t.slot,
						attach_to_pcm_dev_t.lep,
						attach_to_pcm_dev_t.freq,
						attach_to_pcm_dev_t.width ) ;
			}
			break ;

		case SNDRV_SC1445x_DETACH_FROM_PCM_DEV:
			if( copy_from_user( &detach_from_pcm_dev_t,
					(void __user*)arg,
					sizeof( detach_from_pcm_dev_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_detach_from_pcm(
							&chip->ae_state,
						detach_from_pcm_dev_t.slot ) ;
			}
			break ;
#endif

#if defined( SC1445x_AE_SUPPORT_FAX )

		case SNDRV_SC1445x_SWITCH_AUDIO_TO_FAX:
			if( copy_from_user( &audio2fax_t,
						(void __user*)arg,
						sizeof( audio2fax_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_switch_to_fax( &chip->ae_state,
						audio2fax_t.channel_index ) ;
			}
			break ;

		case SNDRV_SC1445x_SWITCH_FAX_TO_AUDIO:
			if( copy_from_user( &fax2audio_t,
						(void __user*)arg,
						sizeof( fax2audio_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_switch_to_audio(
						&chip->ae_state,
						fax2audio_t.channel_index ) ;
			}
			break ;

		case SNDRV_SC1445x_FAX_INIT:
			if( copy_from_user( &fax_init_params_t,
						(void __user*)arg,
						sizeof( fax_init_params_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_fax_init( &chip->ae_state,
						fax_init_params_t.line,
						fax_init_params_t.p0DBIN,
						fax_init_params_t.p0DBOUT,
						fax_init_params_t.pCEDLength,
						fax_init_params_t.pMDMCmd ) ;
			}
			break ;

#endif  /* SC1445x_AE_SUPPORT_FAX */

		case SNDRV_SC1445x_CONTROL_DSP_LOOPBACK:
			if( copy_from_user( &dsp_lb, (void __user*)arg,
							sizeof( dsp_lb ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_control_dsp_loopback(
						&chip->ae_state,
						dsp_lb.val ) ;
			}
			break ;

#if defined( SC1445x_AE_USE_CONVERSATIONS )

		case SNDRV_SC1445x_START_CONVERSATION:
			if( copy_from_user( &conv, (void __user*)arg,
							sizeof( conv ) ) ) {
				res = -EFAULT ;
				break ;
			}

			res = sc1445x_ae_start_conversation(
						&chip->ae_state,
						&conv.conversation_id ) ;
			if( res )
				break ;  /* error */

			if( copy_to_user( (void __user*)arg,
							&conv,
							sizeof( conv ) ) )
				res = -EFAULT ;
			break ;

		case SNDRV_SC1445x_STOP_CONVERSATION:
			if( copy_from_user( &conv, (void __user*)arg,
							sizeof( conv ) ) ) {
				res = -EFAULT ;
				break ;
			}

			res = sc1445x_ae_stop_conversation(
						&chip->ae_state,
						conv.conversation_id ) ;
			break ;

		case SNDRV_SC1445x_CONVERSATION_ADD_CHANNEL:
			if( copy_from_user( &conv_add, (void __user*)arg,
							sizeof( conv_add ) ) ) {
				res = -EFAULT ;
				break ;
			}

			res = sc1445x_ae_add_channel_to_conversation(
						&chip->ae_state,
						conv_add.conversation_id,
						conv_add.member_id ) ;
			break ;

		case SNDRV_SC1445x_CONVERSATION_REM_CHANNEL:
			if( copy_from_user( &conv_rem, (void __user*)arg,
							sizeof( conv_rem ) ) ) {
				res = -EFAULT ;
				break ;
			}

			res = sc1445x_ae_remove_channel_from_conversation(
						&chip->ae_state,
						conv_rem.conversation_id,
						conv_rem.member_id ) ;
			break ;

		case SNDRV_SC1445x_CONVERSATION_ADD_LOCAL_IFACE:
			if( copy_from_user( &conv_add, (void __user*)arg,
							sizeof( conv_add ) ) ) {
				res = -EFAULT ;
				break ;
			}

			res = sc1445x_ae_add_local_iface_to_conversation(
						&chip->ae_state,
						conv_add.conversation_id ) ;
			break ;

		case SNDRV_SC1445x_CONVERSATION_REM_LOCAL_IFACE:
			if( copy_from_user( &conv_rem, (void __user*)arg,
							sizeof( conv_rem ) ) ) {
				res = -EFAULT ;
				break ;
			}

			res = sc1445x_ae_remove_local_iface_from_conversation(
						&chip->ae_state,
						conv_rem.conversation_id ) ;
			break ;

#  if defined( SC1445x_AE_PHONE_DECT )
		case SNDRV_SC1445x_CONVERSATION_ADD_DECT:
			if( copy_from_user( &conv_add, (void __user*)arg,
							sizeof( conv_add ) ) ) {
				res = -EFAULT ;
				break ;
			}

			res = sc1445x_ae_add_dect_to_conversation(
						&chip->ae_state,
						conv_add.conversation_id,
						conv_add.member_id ) ;
			break ;

		case SNDRV_SC1445x_CONVERSATION_REM_DECT:
			if( copy_from_user( &conv_rem, (void __user*)arg,
							sizeof( conv_rem ) ) ) {
				res = -EFAULT ;
				break ;
			}

			res = sc1445x_ae_remove_dect_from_conversation(
						&chip->ae_state,
						conv_rem.conversation_id,
						conv_rem.member_id ) ;
			break ;
#  endif  /* SC1445x_AE_PHONE_DECT */

#endif  /* SC1445x_AE_USE_CONVERSATIONS */

		case SNDRV_SC1445x_RESTART_AUDIO_ENGINE:
			PRINT( "Shutting down audio engine...\n" ) ;
			res = sc1445x_ae_finalize_engine( &chip->ae_state ) ;
			if( res ) {
				PRINT( "...FAILED -- need to reboot\n" ) ;
				break ;
			}

			PRINT( "Initializing audio engine...\n" ) ;
			res = sc1445x_ae_init_engine( &chip->ae_state ) ;
			if( res ) {
				PRINT( "...FAILED -- need to reboot\n" ) ;
				break ;
			}
			chip->ae_state.private_data = chip ;

			PRINT( "DONE!\n" ) ;
			break ;

		case SNDRV_SC1445x_RESET_AUDIO_PROFILE:
			res = sc1445x_ae_reset_audio_profile(
							&chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_GET_AUDIO_PROFILE_DYN_SZ:
			memset( &ap, 0, sizeof( ap ) ) ;
			res = sc1445x_ae_get_audio_profile_dyn_size(
					&chip->ae_state, &ap.data_nbytes ) ;
			if( res )
				break ;  /* error */
			if( WRITE_USER_ARG( arg, ap ) )
				res = -EFAULT ;

			break ;

		case SNDRV_SC1445x_GET_AUDIO_PROFILE:
			if( READ_USER_ARG( ap, arg ) ) {
				res = -EFAULT ;
				break ;
			}
			res = sc1445x_ae_get_audio_profile(
							&chip->ae_state, &ap ) ;
			if( res )
				break ;  /* error */
			res = copy_ap_to_user( arg, &ap ) ;

			break ;

		case SNDRV_SC1445x_SET_AUDIO_PROFILE:
#if 0
			if( READ_USER_ARG( ap, arg ) ) {
				res = -EFAULT ;
				break ;
			}
#else
			if( copy_ap_from_user( &ap, arg ) ) {
				res = -EFAULT ;
				break ;
			}
#endif
			res = sc1445x_ae_set_audio_profile(
							&chip->ae_state, &ap ) ;

			break ;

		default:
			snd_printk( "%s: Unknown ioctl %08x (arg=%08lx)\n",
						__FUNCTION__, cmd, arg ) ;
			res = -ENOTTY ;
			break ;
	}

	return res ;
}


static int __init snd_sc1445x_probe( struct platform_device* devptr )
{
	int res ;
	snd_card_t* card ;
	struct snd_sc1445x* chip ;
	struct snd_hwdep* hw ;
	char dsp1_eng_ver[5], dsp2_eng_ver[5] ;

	card = snd_card_new( SNDRV_DEFAULT_IDX1, SNDRV_DEFAULT_STR1,
							THIS_MODULE, 0 ) ;
	if( NULL == card )
		return -ENOMEM ;

	res = snd_sc1445x_new( card, &chip ) ;
	if( res < 0 ) {
		snd_card_free( card ) ;
		return res ;
	}
	card->private_data = chip ;

	strncpy( card->driver, "SC1445x ALSA", sizeof( card->driver ) - 1 ) ;
	strncpy( card->shortname,
			"SC1445x Audio Engine " SC1445x_AUDIO_DRIVER_VERSION,
					sizeof( card->shortname ) - 1 ) ;
	if( *dsp1_fw_ver >= 0x130 )
		snprintf( dsp1_eng_ver, sizeof( dsp1_eng_ver ), ".%u",
						*dsp1_fw_eng_ver & 0xff ) ;
	else
		dsp1_eng_ver[0] = '\0' ;
	if( *dsp2_fw_ver >= 0x10a )
		snprintf( dsp2_eng_ver, sizeof( dsp2_eng_ver ), ".%u",
						*dsp2_fw_eng_ver & 0xff ) ;
	else
		dsp2_eng_ver[0] = '\0' ;
	snprintf( card->longname, sizeof( card->longname ),
			"%s (priv @%06x, DSP1 %u.%u%s-%s "
			"DSP2 %u.%u%s)\n",
			card->shortname, (unsigned)chip,
			*dsp1_fw_ver >> 8, *dsp1_fw_ver & 0xff,
			dsp1_eng_ver,
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
					"A/D",	/* ATA/DECT */
#else
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
					"PP",	/* Phone, PAEC */
#else
					"PA",	/* Phone, AEC */
#endif
#endif
			*dsp2_fw_ver >> 8, *dsp2_fw_ver & 0xff,
			dsp2_eng_ver ) ;

	res = snd_hwdep_new( card, "SC1445x Audio Engine", 0, &chip->hw ) ;
	if( res < 0 ) {
		snd_card_free( card ) ;
		return res ;
	}
	hw = chip->hw ;
	strncpy( hw->name, "SC1445x ALSA hwdep", sizeof( hw->name ) - 1 ) ;
	hw->iface = SNDRV_HWDEP_IFACE_SC1445x ;
	hw->private_data = chip ;
	/* hwdep operators */
	hw->ops.open = snd_sc1445x_hwdep_open ;
	hw->ops.ioctl = snd_sc1445x_hwdep_ioctl ;
	hw->ops.release = snd_sc1445x_hwdep_release ;

	res = snd_sc1445x_pcm_new( chip ) ;
	if( res < 0 ) {
		snd_card_free( card ) ;
		return res ;
	}

	snd_card_set_dev( card, &devptr->dev ) ;
	res = snd_card_register( card ) ;
	if( res < 0 ) {
		snd_card_free( card ) ;
		return res ;
	}
	platform_set_drvdata( devptr, card ) ;

	return 0 ;
}


static int __devexit snd_sc1445x_remove( struct platform_device* devptr )
{
	snd_card_free( platform_get_drvdata( devptr ) ) ;
	platform_set_drvdata( devptr, NULL ) ;
	return 0 ;
}


static struct platform_driver snd_sc1445x_driver = {
	.probe		= snd_sc1445x_probe,
	.remove		= __devexit_p( snd_sc1445x_remove ),
	.driver		= {
		.name	= SND_SC1445x_DRIVER
	},
} ;


static int __init sc1445x_alsa_init( void )
{
	int res = platform_driver_register( &snd_sc1445x_driver ) ;
	if( res < 0 )
		return res ;
	snd_sc1445x_device =
			platform_device_register_simple( SND_SC1445x_DRIVER, -1,
								NULL, 0 ) ;

	snd_printd( "SC1445x ALSA driver initialized\n" ) ;

#ifdef CONFIG_L_V2_BOARD
	P0_02_MODE_REG=0x300;
	P0_RESET_DATA_REG=(1<<2);
#endif

	return 0 ;
}


static void __exit sc1445x_alsa_exit( void )
{
	if( !IS_ERR( snd_sc1445x_device ) )
		platform_device_unregister( snd_sc1445x_device ) ;
	platform_driver_unregister( &snd_sc1445x_driver ) ;

	snd_printd( "SC1445x ALSA driver exiting\n" ) ;
}


module_init( sc1445x_alsa_init )
module_exit( sc1445x_alsa_exit )

