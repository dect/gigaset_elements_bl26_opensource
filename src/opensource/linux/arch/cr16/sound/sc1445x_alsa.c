/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 *
 * Driver for ALSA on SC1445x
 */

#include <sound/driver.h>
#include <linux/init.h>
#include <linux/err.h>
#include <linux/platform_device.h>
#include <linux/moduleparam.h>
#include <linux/delay.h>
#include <sound/core.h>
#include <sound/initval.h>
#include <sound/hwdep.h>
#include <linux/cdev.h>
#include <linux/list.h>
#include "sc1445x_alsa.h"
#include "sc1445x_alsa_ioctl.h"
#include "audioengine/sc1445x_audioengine_api.h"
#include "sc1445x_voip_version.h"


MODULE_LICENSE( "GPL" ) ;
MODULE_DESCRIPTION( "SC1445x ALSA driver" ) ;


//#define USE_FORCED_PACKET

//#define TEST_WITH_MARKED_PACKET

//#define DUMP_IOCTL

//#define TRACE_IOCTL

/* define to use PCM character device */
#define USE_PCM_CHAR_DEVICE
#if defined( USE_PCM_CHAR_DEVICE )

/* define to use a write buffer for the PCM char device */
#  define USE_PCM_WRITE_BUFFER
#  define PCM_WRITE_BUFFER_SIZE		8192

#endif

#if defined( SC1445x_AE_ATA_SUPPORT )

/* define to use CID character device */
#  define USE_CID_CHAR_DEVICE
#  if defined( USE_CID_CHAR_DEVICE )
#  endif

/* define to use tone detection character device */
#  define USE_TONE_DET_CHAR_DEVICE
#  if defined( USE_TONE_DET_CHAR_DEVICE )
#    include "sc1445x_tone_det.h"
#  endif

#endif

#if defined( USE_PCM_WRITE_BUFFER )
static int snd_sc1445x_playback_copy_raw_pcm_from_buf( sc1445x_ae_state* s,
	const void* src, unsigned nbytes, unsigned short sample_bits ) ;
#else
static int snd_sc1445x_playback_copy_raw_pcm( sc1445x_ae_state* s,
	const void __user* src, unsigned nbytes, unsigned short sample_bits ) ;
#endif

/* private API from audioengine */
void sc1445x_private_stop_raw_pcm( void ) ;
short sc1445x_private_line2stream( const sc1445x_ae_state* ae_state,
				unsigned short line, short create_mapping ) ;
void sc1445x_private_start_raw_pcm_mode( sc1445x_ae_state* ae_state,
				short change_mode, unsigned short rate ) ;


static struct platform_device* snd_sc1445x_device ;

static const char SND_SC1445x_DRIVER[] = "snd_sc1445x" ;

static const unsigned short* dsp1_fw_ver = (unsigned short*)0x12a04 ;
static const unsigned short* dsp1_fw_eng_ver = (unsigned short*)0x12a06 ;
static const unsigned short* dsp2_fw_ver = (unsigned short*)0x1b1fe ;
static const unsigned short* dsp2_fw_eng_ver = (unsigned short*)0x1b15a ;
static const unsigned short* dsp2_fw_eng_ver_1_27 = (unsigned short*)0x1b012 ;


/* free resources allocated for ALSA "chip" */
int snd_sc1445x_free( struct snd_sc1445x* chip )
{

	sc1445x_ae_finalize_engine( &chip->ae_state ) ;

	kfree( chip ) ;

	return 0 ;
}


/*
 * free the device
 */
static int snd_sc1445x_dev_free( struct snd_device *device )
{
	struct snd_sc1445x* chip = device->device_data ;
	return snd_sc1445x_free( chip ) ;
}


/* save a pointer to the ae_state */
sc1445x_ae_state* the_ae_state = NULL ;


/* check is there any channels active, using the default ae_state */
short sc1445x_private_have_active_channels( const sc1445x_ae_state* ae_state ) ;
short snd_sc1445x_have_active_channels( void )
{
	if( !the_ae_state )
		return 0 ;

	if( sc1445x_private_have_active_channels( the_ae_state ) )
		return 1 ;

	/* check if we are in raw PCM mode (i.e. playing a ringtone) */
	if( SC1445x_AE_MODE_RAW_PCM == the_ae_state->mode )
		return 1 ;

	return 0 ;
}


static inline
short have_space_for_raw_pcm( const sc1445x_ae_raw_pcm_state* raw_pcm )
{
	return !raw_pcm->block_status[raw_pcm->next_block_index] ;
}


#if defined( USE_PCM_CHAR_DEVICE )

/*
 * create a char device, to receive raw PCM samples when VoIP channels
 * are active, bypassing ALSA
 */
struct sc1445x_pcm_dev {
	atomic_t is_avail ;
	atomic_t is_stopped ;
	wait_queue_head_t writeq ;
	sc1445x_ae_state* ae_state ;
	sc1445x_ae_raw_pcm_rate rate ;
	unsigned short sample_bits ;
#  if defined( USE_PCM_WRITE_BUFFER )
	wait_queue_head_t writebufq ;
	struct work_struct read_work ;
	unsigned char buf[PCM_WRITE_BUFFER_SIZE] ;
	struct semaphore buf_sem ;
	unsigned short r_pos ;
	unsigned short w_pos ;
	unsigned short nfree ;
#  endif

	unsigned long start_time ;
	unsigned long total_count ;
	
	struct cdev cdev ;
} ;

static int sc1445x_pcm_major = 249 ;
static int sc1445x_pcm_minor = 0 ;

static struct sc1445x_pcm_dev the_sc1445x_pcm_dev =
{
	.is_avail = ATOMIC_INIT( 1 ),
	.is_stopped = ATOMIC_INIT( 1 ),
#  if defined( USE_PCM_WRITE_BUFFER )
	.r_pos = 0,
	.w_pos = 0,
	.nfree = PCM_WRITE_BUFFER_SIZE,
#  endif
} ;


#  if defined( USE_PCM_WRITE_BUFFER )

/* return != 0 if there is space to write to the PCM buffer */
static inline
short have_space_in_pcm_buf( struct sc1445x_pcm_dev* dev )
{
	return  dev->nfree != 0 ;
}

/*
 * return the size (in bytes) of contiguous free space for writing to
 * the PCM buffer
 */
static inline
unsigned short contig_w_space_in_pcm_buf( struct sc1445x_pcm_dev* dev )
{
	unsigned short sz ;

	down( &dev->buf_sem ) ;

	if( dev->w_pos == dev->r_pos ) {
		if( 0 == dev->nfree )
			sz = 0 ;
		else
			sz = PCM_WRITE_BUFFER_SIZE - dev->w_pos ;
	} else if( dev->w_pos > dev->r_pos )
		sz = PCM_WRITE_BUFFER_SIZE - dev->w_pos ;
	else
		sz = dev->r_pos - dev->w_pos ;

	up( &dev->buf_sem ) ;

	return sz ;
}

/*
 * return the size (in bytes) of contiguous data for reading from
 * the PCM buffer
 */
static inline
unsigned short contig_r_data_in_pcm_buf( struct sc1445x_pcm_dev* dev )
{
	unsigned short sz ;

	down( &dev->buf_sem ) ;

	if( dev->w_pos == dev->r_pos ) {
		if( 0 == dev->nfree )
			sz = PCM_WRITE_BUFFER_SIZE - dev->r_pos ;
		else
			sz = 0 ;
	} else if( dev->r_pos > dev->w_pos )
		sz = PCM_WRITE_BUFFER_SIZE - dev->r_pos ;
	else
		sz = dev->w_pos - dev->r_pos ;

	up( &dev->buf_sem ) ;

	return sz ;
}

/* return negative on error; or else, how many bytes could NOT be written */
static int pcm_buf_write( struct sc1445x_pcm_dev* dev, const void __user* src,
				unsigned nbytes, unsigned short sample_bits )
{
	unsigned short count = contig_w_space_in_pcm_buf( dev ) ;
	short i ;

	if( unlikely( 0 == count ) )
		return nbytes ;  /* buffer is full */

	if( count > nbytes )
		count = nbytes ;

	down( &dev->buf_sem ) ;

	/* loop twice, in case of wrap-around */
	for( i = 0 ;  nbytes > 0  &&  i < 2 ;  ++i ) {
		if( copy_from_user( dev->buf + dev->w_pos, src, count ) ) {
			up( &dev->buf_sem ) ;
			return -EFAULT ;
		}

		nbytes -= count ;
		dev->nfree -= count ;
		dev->w_pos += count ;
		if( PCM_WRITE_BUFFER_SIZE == dev->w_pos ) {
			/*
			 * try a little more, in case there is free space
			 * at the beginning of the ring buffer
			 */
			dev->w_pos = 0 ;
			up( &dev->buf_sem ) ;
			count = contig_w_space_in_pcm_buf( dev ) ;
			if( count > nbytes )
				count = nbytes ;
			down( &dev->buf_sem ) ;
		} else
			break ;
	}

	up( &dev->buf_sem ) ;

	return nbytes ;
}

/* read from the PCM buffer (and write to the DSP's PCM buffer) */
static void pcm_buf_read( void* data )
{
	struct sc1445x_pcm_dev* dev = data ;
	unsigned count = contig_r_data_in_pcm_buf( dev ) ;
	unsigned not_consumed ;

	if( 0 == count ) {
		return ;  /* buffer is empty */
	}

	not_consumed =
		snd_sc1445x_playback_copy_raw_pcm_from_buf( dev->ae_state,
			dev->buf + dev->r_pos, count, dev->sample_bits ) ;

	DPRINT( "%s: count=%d not_consumed=%d\n", __FUNCTION__, count,
								not_consumed ) ;
	count -= not_consumed ;

	down( &dev->buf_sem ) ;

	dev->nfree += count ;
	dev->r_pos += count ;
	if( PCM_WRITE_BUFFER_SIZE == dev->r_pos )
		dev->r_pos = 0 ;

	up( &dev->buf_sem ) ;
}

#  endif  /* USE_PCM_WRITE_BUFFER */

static int
sc1445x_pcm_open( struct inode* inode, struct file* filp )
{
	/* enforce single open */
	if( !atomic_dec_and_test( &the_sc1445x_pcm_dev.is_avail ) ) {
		atomic_inc( &the_sc1445x_pcm_dev.is_avail ) ;
		return -EBUSY ;
	}

	filp->private_data = &the_sc1445x_pcm_dev ;
#  if defined( USE_PCM_WRITE_BUFFER )
	the_sc1445x_pcm_dev.r_pos = 0 ;
	the_sc1445x_pcm_dev.w_pos = 0 ;
	the_sc1445x_pcm_dev.nfree = PCM_WRITE_BUFFER_SIZE ;

	the_sc1445x_pcm_dev.total_count = 0 ;
#  endif

	return 0 ;
}

static short
sc1445x_pcm_setup( sc1445x_ae_raw_pcm_rate rate,
				sc1445x_ae_raw_pcm_sample_size sample_bits )
{
#if 0
	unsigned short r ;
#endif

	if( atomic_read( &the_sc1445x_pcm_dev.is_avail ) != 0 ) {
		PRINT( PRINT_LEVEL "%s: PCM char device has not been opened "
						"yet\n", __FUNCTION__ ) ;
		return -EBUSY ;
	}

	if( !atomic_dec_and_test( &the_sc1445x_pcm_dev.is_stopped ) ) {
		PRINT( PRINT_LEVEL "%s: PCM char device is already active\n",
								__FUNCTION__ ) ;
		atomic_inc( &the_sc1445x_pcm_dev.is_stopped ) ;
		return -EBUSY ;
	}

	if( ( sample_bits >= SC1445x_AE_RAW_PCM_SAMPLE_SIZE_INVALID ) ||
				( rate >= SC1445x_AE_RAW_PCM_RATE_INVALID ) ) {
		PRINT( PRINT_LEVEL "%s: INVALID rate=%d and/or "
			"sample_bits=%d\n", __FUNCTION__, rate, sample_bits ) ;
		atomic_inc( &the_sc1445x_pcm_dev.is_stopped ) ;
		return -EINVAL ;
	}

	the_sc1445x_pcm_dev.rate = rate ;
	switch( sample_bits ) {
		case SC1445x_AE_RAW_PCM_SAMPLE_SIZE_8:
			the_sc1445x_pcm_dev.sample_bits = 8 ;
			break ;

		case SC1445x_AE_RAW_PCM_SAMPLE_SIZE_16:
			the_sc1445x_pcm_dev.sample_bits = 16 ;
			break ;

		default:
			the_sc1445x_pcm_dev.sample_bits = 0 ;
			break ;
	}

#if 0
	r = 8000 << (unsigned short)rate ; 
	free_irq( DSP2_INT, (void*)the_sc1445x_pcm_dev.ae_state ) ;
	sc1445x_private_start_raw_pcm_mode( the_sc1445x_pcm_dev.ae_state,
								1, r ) ;

	return 0 ;
#else
	return sc1445x_ae_start_mixing_raw_pcm_with_voice(
			the_sc1445x_pcm_dev.ae_state, rate, sample_bits ) ;
#endif
}

static void sc1445x_pcm_flush_dsp_buf( sc1445x_ae_raw_pcm_state* raw_pcm )
{
	unsigned short i, cnt ;
	//volatile unsigned short* start_pcm_fadeout = (void*)0x10538 ;

	do {
		cnt = 0 ;
		for( i = 0 ;  i < raw_pcm->block_count ;  ++i ) {
			if( raw_pcm->block_status[i] ) {
				++cnt ;
			}
		}
		//if( cnt <= 2 )
		//	*start_pcm_fadeout = 1 ;

		if( cnt )
			msleep( 10 ) ;
	} while( cnt > 0 ) ;
}

static short sc1445x_pcm_stop( void )
{
#if 0
	sc1445x_ae_raw_pcm_state* raw_pcm =
					&the_sc1445x_pcm_dev.ae_state->raw_pcm ;
#else
	//volatile unsigned short* start_pcm_fadeout = (void*)0x10538 ;
#endif

	if( atomic_read( &the_sc1445x_pcm_dev.is_avail ) != 0 )
		return -ENOTTY ;

	if( atomic_read( &the_sc1445x_pcm_dev.is_stopped ) == 1 )
		return -EINVAL ;

#if 0
	sc1445x_pcm_flush_dsp_buf( raw_pcm ) ;
#else
	//*start_pcm_fadeout = 2 ;
	//if( the_sc1445x_pcm_dev.nfree != PCM_WRITE_BUFFER_SIZE ) {
	//	schedule_work( &the_sc1445x_pcm_dev.read_work ) ;
	//}
#endif

	sc1445x_ae_stop_mixing_raw_pcm_with_voice( 
						the_sc1445x_pcm_dev.ae_state ) ;
	atomic_inc( &the_sc1445x_pcm_dev.is_stopped ) ;

	return 0 ;
}


#  if defined( USE_PCM_WRITE_BUFFER )

#    define HAVE_SPACE_TO_WRITE( dev ) \
			have_space_in_pcm_buf( dev )

#    define WRITE_WAITQ( dev )		(dev)->writebufq

#    define WRITE_PCM( dev, buf, len, w ) \
			pcm_buf_write( dev, buf, len, w )

#  else

#    define HAVE_SPACE_TO_WRITE( dev ) \
			have_space_for_raw_pcm( &dev->ae_state->raw_pcm )

#    define WRITE_WAITQ( dev )		(dev)->writeq

#    define WRITE_PCM( dev, buf, len, w ) \
		snd_sc1445x_playback_copy_raw_pcm( dev->ae_state, buf, len, w )

#  endif

static ssize_t
sc1445x_pcm_write( struct file* filp, const char __user* buf, size_t count,
								loff_t* ppos )
{
	int res ;
	struct sc1445x_pcm_dev* dev = filp->private_data ;

	snd_printd( "buf=%p count=%u pos=%lld\n", buf, count, *ppos ) ;

	while( !HAVE_SPACE_TO_WRITE( dev ) ) {
		if( filp->f_flags & O_NONBLOCK )
			return -EAGAIN ;

		DPRINT( "%s: going to wait\n", __FUNCTION__ ) ;
#  if defined( USE_PCM_WRITE_BUFFER )
		schedule_work( &dev->read_work ) ;
#  endif
		if( wait_event_interruptible_timeout( WRITE_WAITQ( dev ),
				(HAVE_SPACE_TO_WRITE( dev )), 1 ) < 0 )
			return -ERESTARTSYS ;
	}
	DPRINT( "%s: continuing\n", __FUNCTION__ ) ;

	res = WRITE_PCM( dev, buf, count, dev->sample_bits ) ;
	if( res < 0 )
		return res ;  /* error */

	/* res shows how many bytes were NOT written */
	count = count - res ;

	if( 0 == dev->total_count ) {
		dev->start_time = jiffies ;
		dev->total_count = count ;
	} else {
		unsigned long rate ;

		dev->total_count += count ;
		rate = dev->total_count / ( jiffies - dev->start_time ) ;
		DPRINT( "%s: rate is %lu\n", __FUNCTION__, rate ) ;
	}

	DPRINT( "%s: exiting\n", __FUNCTION__ ) ;
	*ppos += count ;
	return count ;
}

static unsigned int sc1445x_pcm_poll( struct file* filp, poll_table* wait )
{
	struct sc1445x_pcm_dev* dev = filp->private_data ;
	unsigned int mask = 0 ;

	poll_wait( filp, &WRITE_WAITQ( dev ), wait ) ;
	if( HAVE_SPACE_TO_WRITE( dev ) )
		mask |= POLLOUT | POLLWRNORM ;  /* writeable */

	return mask ;
}

static int 
sc1445x_pcm_release( struct inode* inode, struct file* filp )
{
	struct sc1445x_pcm_dev* dev = filp->private_data ;
	sc1445x_ae_raw_pcm_state* raw_pcm = &dev->ae_state->raw_pcm ;
	unsigned char silence[640] ;

	if( atomic_read( &the_sc1445x_pcm_dev.is_stopped ) == 0 ) {
#if 1
#  if defined( USE_PCM_WRITE_BUFFER )
		/* wait for buffer to be flushed */
		while( dev->nfree != PCM_WRITE_BUFFER_SIZE ) {
			schedule_work( &dev->read_work ) ;
			msleep( 10 ) ;
		}
#  endif

		/* write a block with zeroes to flush out half-filled block */
		memset( silence, 0, sizeof( silence ) ) ;
		snd_sc1445x_playback_copy_raw_pcm_from_buf( dev->ae_state,
				silence, sizeof( silence ), dev->sample_bits ) ;

		sc1445x_pcm_flush_dsp_buf( raw_pcm ) ;

#  if 0
		sc1445x_private_stop_raw_pcm() ;
#  endif
		sc1445x_ae_stop_mixing_raw_pcm_with_voice( dev->ae_state ) ;

		atomic_inc( &the_sc1445x_pcm_dev.is_stopped ) ;
#else
		sc1445x_pcm_stop() ;
#endif
	}

	atomic_inc( &the_sc1445x_pcm_dev.is_avail ) ;

	return 0 ;
}

static struct file_operations sc1445x_pcm_fops = {
	.owner =	THIS_MODULE,
	.open =		sc1445x_pcm_open,
	.write =	sc1445x_pcm_write,
	.poll =		sc1445x_pcm_poll,
	.release =	sc1445x_pcm_release,
};

static int __init snd_sc1445x_register_pcm_cdev( sc1445x_ae_state* aes )
{
	dev_t dev ;
	int res ;

	snd_printd( "Registering PCM char device\n" ) ;

	dev = MKDEV( sc1445x_pcm_major, sc1445x_pcm_minor ) ;
	res = register_chrdev_region( dev, 1, "sc1445x-pcm" ) ;
	if( res < 0 ) {
		snd_printk( "%s: could not register char device with major "
				"%d\n", __FUNCTION__, sc1445x_pcm_major ) ;
		return res ;
	}

	cdev_init( &the_sc1445x_pcm_dev.cdev, &sc1445x_pcm_fops ) ;
	the_sc1445x_pcm_dev.ae_state = aes ;
	init_waitqueue_head( &the_sc1445x_pcm_dev.writeq ) ;
#  if defined( USE_PCM_WRITE_BUFFER )
	init_waitqueue_head( &the_sc1445x_pcm_dev.writebufq ) ;
	init_MUTEX( &the_sc1445x_pcm_dev.buf_sem ) ;
	INIT_WORK( &the_sc1445x_pcm_dev.read_work, pcm_buf_read,
						&the_sc1445x_pcm_dev ) ;
#  endif

	res = cdev_add( &the_sc1445x_pcm_dev.cdev, dev, 1 ) ;
	if( res < 0 ) {
		snd_printk( "%s: could not add raw PCM char device\n",
								__FUNCTION__ ) ;
		unregister_chrdev_region( dev, 1 ) ;
		return res ;
	}

	return 0 ;
}

static void __exit snd_sc1445x_unregister_pcm_cdev( void )
{
	snd_printd( "Unregistering PCM char device\n" ) ;

	cdev_del( &the_sc1445x_pcm_dev.cdev ) ;
	unregister_chrdev_region(
			MKDEV( sc1445x_pcm_major, sc1445x_pcm_minor ), 1 ) ;
}

#endif  /* USE_PCM_CHAR_DEVICE */


#if defined( USE_CID_CHAR_DEVICE )

/*
 * create a char device, to provide CID information
 */
struct sc1445x_cid_dev {
	atomic_t is_avail ;
	wait_queue_head_t readq ;
	sc1445x_ae_state* ae_state ;

	unsigned char cid_buf[CID_BUF_LEN] ;
	unsigned short cid_buf_len ;

	struct cdev cdev ;
} ;

static int sc1445x_cid_major = 249 ;
static int sc1445x_cid_minor = 1 ;

static struct sc1445x_cid_dev the_sc1445x_cid_dev =
{
	.is_avail = ATOMIC_INIT( 1 ),
	.cid_buf_len = 0,
} ;


static int
sc1445x_cid_open( struct inode* inode, struct file* filp )
{
	/* enforce single open */
	if( !atomic_dec_and_test( &the_sc1445x_cid_dev.is_avail ) ) {
		atomic_inc( &the_sc1445x_cid_dev.is_avail ) ;
		return -EBUSY ;
	}

	filp->private_data = &the_sc1445x_cid_dev ;

	return 0 ;
}

static ssize_t sc1445x_cid_read( struct file* filp, char __user* buf,
						size_t count, loff_t* f_pos )
{
	ssize_t ret = 0 ;
	struct sc1445x_cid_dev* dev = filp->private_data ;

	if( 0 == dev->cid_buf_len  ||  *f_pos >= dev->cid_buf_len )
		return ret ;

	if( *f_pos + count  >  dev->cid_buf_len )
		count = dev->cid_buf_len - *f_pos ;

	if( copy_to_user( buf, dev->cid_buf + *f_pos, count ) )
		ret = -EFAULT ;
	else {
		*f_pos = count ;
		ret = count ;

		if( count == dev->cid_buf_len )
			dev->cid_buf_len = 0 ;  /* no more to read  */
	}

	return ret ;
}

void sc1445x_private_fill_cid_buffer( const unsigned char* buf,
							unsigned short len )
{
	memcpy( the_sc1445x_cid_dev.cid_buf, buf, len ) ;
	the_sc1445x_cid_dev.cid_buf_len = len ;
	wake_up_interruptible( &the_sc1445x_cid_dev.readq ) ;
}

static unsigned int sc1445x_cid_poll( struct file* filp, poll_table* wait )
{
	struct sc1445x_cid_dev* dev = filp->private_data ;
	unsigned int mask = 0 ;

	poll_wait( filp, &dev->readq, wait ) ;
	if( dev->cid_buf_len > 0 )
		mask |= POLLIN | POLLRDNORM ;  /* readable */

	return mask ;
}

static int 
sc1445x_cid_release( struct inode* inode, struct file* filp )
{
	atomic_inc( &the_sc1445x_cid_dev.is_avail ) ;

	return 0 ;
}

static struct file_operations sc1445x_cid_fops = {
	.owner =	THIS_MODULE,
	.open =		sc1445x_cid_open,
	.read =		sc1445x_cid_read,
	.poll =		sc1445x_cid_poll,
	.release =	sc1445x_cid_release,
};

static int __init snd_sc1445x_register_cid_cdev( sc1445x_ae_state* aes )
{
	dev_t dev ;
	int res ;

	snd_printd( "Registering CID char device\n" ) ;

	dev = MKDEV( sc1445x_cid_major, sc1445x_cid_minor ) ;
	res = register_chrdev_region( dev, 1, "sc1445x-cid" ) ;
	if( res < 0 ) {
		snd_printk( "%s: could not register char device with major "
				"%d\n", __FUNCTION__, sc1445x_cid_major ) ;
		return res ;
	}

	cdev_init( &the_sc1445x_cid_dev.cdev, &sc1445x_cid_fops ) ;
	the_sc1445x_cid_dev.ae_state = aes ;
	init_waitqueue_head( &the_sc1445x_cid_dev.readq ) ;

	res = cdev_add( &the_sc1445x_cid_dev.cdev, dev, 1 ) ;
	if( res < 0 ) {
		snd_printk( "%s: could not add CID char device\n",
								__FUNCTION__ ) ;
		unregister_chrdev_region( dev, 1 ) ;
		return res ;
	}

	return 0 ;
}

static void __exit snd_sc1445x_unregister_cid_cdev( void )
{
	snd_printd( "Unregistering CID char device\n" ) ;

	cdev_del( &the_sc1445x_cid_dev.cdev ) ;
	unregister_chrdev_region(
			MKDEV( sc1445x_cid_major, sc1445x_cid_minor ), 1 ) ;
}

#endif  /* USE_CID_CHAR_DEVICE */


#if defined( USE_TONE_DET_CHAR_DEVICE )

/*
 * create a char device, to provide information about tone detection
 */
struct sc1445x_tone_det_dev {
	atomic_t is_avail ;
	wait_queue_head_t readq ;
	sc1445x_ae_state* ae_state ;

	struct list_head events ;
	struct semaphore events_sem ;

	struct cdev cdev ;
} ;

struct sc1445x_tone_det_event_node {
	struct list_head list ;

	struct sc1445x_tone_det_event e ;
} ;


static int sc1445x_tone_det_major = 249 ;
static int sc1445x_tone_det_minor = 2 ;

static struct sc1445x_tone_det_dev the_sc1445x_tone_det_dev =
{
	.is_avail = ATOMIC_INIT( 1 ),
} ;


void sc1445x_private_tone_det_add_event( int event, unsigned timestamp )
{
	struct sc1445x_tone_det_event_node* n =
			kmalloc( sizeof( struct sc1445x_tone_det_event_node ),
								GFP_KERNEL ) ;

	BUG_ON( NULL == n ) ;

	n->e.event = event ;
	n->e.timestamp = timestamp ;
	INIT_LIST_HEAD( &n->list ) ;

	down( &the_sc1445x_tone_det_dev.events_sem ) ;
	list_add_tail( &n->list, &the_sc1445x_tone_det_dev.events ) ;
	up( &the_sc1445x_tone_det_dev.events_sem ) ;

	wake_up_interruptible( &the_sc1445x_tone_det_dev.readq ) ;

}

static int
sc1445x_tone_det_open( struct inode* inode, struct file* filp )
{
	/* enforce single open */
	if( !atomic_dec_and_test( &the_sc1445x_tone_det_dev.is_avail ) ) {
		atomic_inc( &the_sc1445x_tone_det_dev.is_avail ) ;
		return -EBUSY ;
	}

	filp->private_data = &the_sc1445x_tone_det_dev ;

	return 0 ;
}

static ssize_t sc1445x_tone_det_read( struct file* filp, char __user* buf,
						size_t count, loff_t* f_pos )
{
	ssize_t ret = 0 ;
	struct sc1445x_tone_det_dev* dev = filp->private_data ;
	struct sc1445x_tone_det_event_node* p ;
	struct sc1445x_tone_det_event_node* next ;
	struct sc1445x_tone_det_event* e ;

	if( list_empty( &dev->events ) )
		return ret ;

	/* enforce reads of multiples of struct sc1445x_tone_det_event */
	ret = count % sizeof( struct sc1445x_tone_det_event ) ;
	count -= ret ;
	if( 0 == count )
		return -EINVAL ;

	down( &dev->events_sem ) ;

	ret = 0 ;
	list_for_each_entry_safe( p, next, &dev->events, list ) {
		e = &p->e ;
		if( copy_to_user( buf, e,
				sizeof( struct sc1445x_tone_det_event ) ) ) {
			if( 0 == ret )
				ret = -EFAULT ;
			break ;
		}

		list_del( &p->list ) ;
		kfree( p ) ;

		ret += sizeof( struct sc1445x_tone_det_event ) ;
		if( count == ret )
			break ;
	}

	up( &dev->events_sem ) ;

	if( ret > 0 )
		*f_pos += ret ;

	return ret ;
}

static unsigned int sc1445x_tone_det_poll( struct file* filp, poll_table* wait )
{
	struct sc1445x_tone_det_dev* dev = filp->private_data ;
	unsigned int mask = 0 ;

	poll_wait( filp, &dev->readq, wait ) ;
	if( !list_empty( &dev->events ) )
		mask |= POLLIN | POLLRDNORM ;  /* readable */

	return mask ;
}

static int 
sc1445x_tone_det_release( struct inode* inode, struct file* filp )
{
	struct sc1445x_tone_det_dev* dev = filp->private_data ;

	down( &dev->events_sem ) ;

	if( !list_empty( &dev->events ) ) {
		struct sc1445x_tone_det_event_node* p ;
		struct sc1445x_tone_det_event_node* next ;

		/* free nodes */
		list_for_each_entry_safe( p, next, &dev->events, list ) {
			list_del( &p->list ) ;
			kfree( p ) ;
		}
	}

	up( &dev->events_sem ) ;

	atomic_inc( &dev->is_avail ) ;

	return 0 ;
}

static struct file_operations sc1445x_tone_det_fops = {
	.owner =	THIS_MODULE,
	.open =		sc1445x_tone_det_open,
	.read =		sc1445x_tone_det_read,
	.poll =		sc1445x_tone_det_poll,
	.release =	sc1445x_tone_det_release,
};

static int __init snd_sc1445x_register_tone_det_cdev( sc1445x_ae_state* aes )
{
	dev_t dev ;
	int res ;

	snd_printd( "Registering tone-detection char device\n" ) ;

	dev = MKDEV( sc1445x_tone_det_major, sc1445x_tone_det_minor ) ;
	res = register_chrdev_region( dev, 1, "sc1445x-tone_det" ) ;
	if( res < 0 ) {
		snd_printk( "%s: could not register char device with "
			"(major, minor)=(%d, %d)\n", __FUNCTION__,
			sc1445x_tone_det_major, sc1445x_tone_det_minor ) ;
		return res ;
	}

	cdev_init( &the_sc1445x_tone_det_dev.cdev, &sc1445x_tone_det_fops ) ;
	the_sc1445x_tone_det_dev.ae_state = aes ;
	init_waitqueue_head( &the_sc1445x_tone_det_dev.readq ) ;
	init_MUTEX( &the_sc1445x_tone_det_dev.events_sem ) ;
	INIT_LIST_HEAD( &the_sc1445x_tone_det_dev.events ) ;

	res = cdev_add( &the_sc1445x_tone_det_dev.cdev, dev, 1 ) ;
	if( res < 0 ) {
		snd_printk( "%s: could not add tone-detection char device\n",
								__FUNCTION__ ) ;
		unregister_chrdev_region( dev, 1 ) ;
		return res ;
	}

	return 0 ;
}

static void __exit snd_sc1445x_unregister_tone_det_cdev( void )
{
	snd_printd( "Unregistering tone-detection char device\n" ) ;

	cdev_del( &the_sc1445x_tone_det_dev.cdev ) ;
	unregister_chrdev_region(
		MKDEV( sc1445x_tone_det_major, sc1445x_tone_det_minor ), 1 ) ;
}

#endif  /* USE_TONE_DET_CHAR_DEVICE */


/* callback to be notified when one block of raw PCM has been played */
void snd_sc1445x_raw_pcm_dsp_irq( void )
{
	DPRINT( "%s entering\n", __FUNCTION__ ) ;
#if defined( USE_PCM_CHAR_DEVICE )
#  if defined( USE_PCM_WRITE_BUFFER )
	wake_up_interruptible( &the_sc1445x_pcm_dev.writebufq ) ;
	schedule_work( &the_sc1445x_pcm_dev.read_work ) ;
#  else
	wake_up_interruptible( &the_sc1445x_pcm_dev.writeq ) ;
#  endif
#endif
}


/* create a new "chip" for ALSA */
static int __init snd_sc1445x_new( struct snd_card* card,
					struct snd_sc1445x** ret_chip )
{
	int res ;
	struct snd_sc1445x* chip ;
	char dsp_fw_ver_major, dsp_fw_ver_minor ;
	static struct snd_device_ops ops = {
		.dev_free =	snd_sc1445x_dev_free,
	} ;

	snd_assert( ret_chip != NULL, return -EINVAL ) ;
	*ret_chip = NULL ;

	chip = kzalloc( sizeof( *chip), GFP_KERNEL ) ;
	if( NULL == chip )
		return -ENOMEM ;
	chip->card = card ;

	if( sc1445x_ae_init_engine( &chip->ae_state ) != SC1445x_AE_OK ) {
		snd_sc1445x_free( chip ) ;
		return -ENOMEM ;
	}

	/* check DSP1 firmware version */
	dsp_fw_ver_major = *dsp1_fw_ver >> 8 ;
	dsp_fw_ver_minor = *dsp1_fw_ver ;
	/* the check below is only valid for one-digit major version numbers */
	if( ( SC1445x_AUDIO_DRIVER_VERSION_MAJOR[0] - '0' ) !=
						dsp_fw_ver_major ) {
		snd_printk( "ALSA audio driver (version %s) is not compatible "
			"with DSP1 firmware (version %d.%d) -- bailing out!\n",
						SC1445x_AUDIO_DRIVER_VERSION,
					dsp_fw_ver_major, dsp_fw_ver_minor ) ;
		snd_sc1445x_free( chip ) ;
		return -ENODEV ;
	}

	/* check DSP2 firmware version */
	dsp_fw_ver_major = *dsp2_fw_ver >> 8 ;
	dsp_fw_ver_minor = *dsp2_fw_ver ;
	/* the check below is only valid for one-digit major version numbers */
	if( ( SC1445x_AUDIO_DRIVER_VERSION_MAJOR[0] - '0' ) !=
						dsp_fw_ver_major ) {
		snd_printk( "ALSA audio driver (version %s) is not compatible "
			"with DSP2 firmware (version %d.%d) -- bailing out!\n",
						SC1445x_AUDIO_DRIVER_VERSION,
					dsp_fw_ver_major, dsp_fw_ver_minor ) ;
		snd_sc1445x_free( chip ) ;
		return -ENODEV ;
	}

#if 1
	the_ae_state = &chip->ae_state ;
#endif
	chip->ae_state.private_data = chip ;

	chip->irq = DSP2_INT ;

#if defined( USE_PCM_CHAR_DEVICE )
	res = snd_sc1445x_register_pcm_cdev( &chip->ae_state ) ;
	if( res < 0 ) {
		snd_sc1445x_free( chip ) ;
		return res ;
	}
#endif

#if defined( USE_CID_CHAR_DEVICE )
	res = snd_sc1445x_register_cid_cdev( &chip->ae_state ) ;
	if( res < 0 ) {
#  if defined( USE_PCM_CHAR_DEVICE )
		snd_sc1445x_unregister_pcm_cdev() ;
#  endif
		snd_sc1445x_free( chip ) ;
		return res ;
	}
#endif

#if defined( USE_TONE_DET_CHAR_DEVICE )
	res = snd_sc1445x_register_tone_det_cdev( &chip->ae_state ) ;
	if( res < 0 ) {
#  if defined( USE_CID_CHAR_DEVICE )
		snd_sc1445x_unregister_cid_cdev() ;
#  endif
#  if defined( USE_PCM_CHAR_DEVICE )
		snd_sc1445x_unregister_pcm_cdev() ;
#  endif
		snd_sc1445x_free( chip ) ;
		return res ;
	}
#endif

	res = snd_device_new( card, SNDRV_DEV_LOWLEVEL, chip, &ops ) ;
	if( res < 0 ) {
#if defined( USE_TONE_DET_CHAR_DEVICE )
		snd_sc1445x_unregister_tone_det_cdev() ;
#endif
#if defined( USE_CID_CHAR_DEVICE )
		snd_sc1445x_unregister_cid_cdev() ;
#endif
#if defined( USE_PCM_CHAR_DEVICE )
		snd_sc1445x_unregister_pcm_cdev() ;
#endif
		snd_sc1445x_free( chip ) ;
		return res ;
	}

	*ret_chip = chip ;
	return 0 ;
}


/* hw info */
static struct snd_pcm_hardware snd_sc1445x_playback =
{
	.info =			SNDRV_PCM_INFO_INTERLEAVED |
				SNDRV_PCM_INFO_PAUSE,
	.formats =		SNDRV_PCM_FMTBIT_SPECIAL |
				SNDRV_PCM_FMTBIT_S16 |
				SNDRV_PCM_FMTBIT_S8 | SNDRV_PCM_FMTBIT_U8 |
				SNDRV_PCM_FMTBIT_MU_LAW |
				SNDRV_PCM_FMTBIT_A_LAW,
	.rates =		SNDRV_PCM_RATE_CONTINUOUS |
				SNDRV_PCM_RATE_8000 |
				SNDRV_PCM_RATE_16000 | SNDRV_PCM_RATE_32000,
	.rate_min =		1000,
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	.rate_max =		49600,
#else
	.rate_max =		37200,
#endif
	.channels_min =		1,
	.channels_max =		1,
	.buffer_bytes_max =	2560,
#if defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD ) \
			|| defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD_CNX ) \
			|| defined( CONFIG_L_V1_BOARD )
	.period_bytes_min =	328,
#elif defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	.period_bytes_min =	496,
#else
	.period_bytes_min =	372,
#endif
	.period_bytes_max =	640,
	.periods_min =          1,
	.periods_max =          4,
	.fifo_size =		0,
} ;

static struct snd_pcm_hardware snd_sc1445x_capture =
{
	.info =			SNDRV_PCM_INFO_INTERLEAVED,
	.formats =		SNDRV_PCM_FMTBIT_SPECIAL |
				SNDRV_PCM_FMTBIT_S8 | SNDRV_PCM_FMTBIT_U8 |
				SNDRV_PCM_FMTBIT_MU_LAW |
				SNDRV_PCM_FMTBIT_A_LAW,
	.rates =		SNDRV_PCM_RATE_CONTINUOUS |
				SNDRV_PCM_RATE_8000 |
				SNDRV_PCM_RATE_16000 | SNDRV_PCM_RATE_32000,
	.rate_min =		1000,
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	.rate_max =		49600,
#else
	.rate_max =		37200,
#endif
	.channels_min =		1,
	.channels_max =		1,
#if defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD ) \
			|| defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD_CNX ) \
			|| defined( CONFIG_L_V1_BOARD )
	.buffer_bytes_max =	1312,
	.period_bytes_min =	328,
	.period_bytes_max =	328,
#elif defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
	.buffer_bytes_max =	1984,
	.period_bytes_min =	496,
	.period_bytes_max =	496,
#else
	.buffer_bytes_max =	1116,
	.period_bytes_min =	372,
	.period_bytes_max =	372,
#endif
	.periods_min =          1,
	.periods_max =          4,
	.fifo_size =		0,
} ;


/* playback PCM ops */
static int snd_sc1445x_playback_open( struct snd_pcm_substream* substream ) 
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	substream->runtime->hw = snd_sc1445x_playback ;

	if( chip->playback_substream )
		chip->started = 0 ;
	chip->playback_substream = substream ;
	/* invalidate pointers for playback */
	chip->playback_r_pos = -1 ;

	snd_printd( "%s: chip @%p, runtime->hw @%p\n",
			__FUNCTION__, chip, &substream->runtime->hw ) ;
	return 0 ;
}


static int snd_sc1445x_playback_close( struct snd_pcm_substream* substream )
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	chip->playback_substream = NULL ;
	//TODO: stop audio engine?
	if( !chip->capture_substream )
		chip->started = 0 ;

	if( SC1445x_AE_MODE_RAW_PCM == chip->mode ) {
		sc1445x_private_stop_raw_pcm() ;
	}

	return 0 ;
}


static int snd_sc1445x_playback_prepare( struct snd_pcm_substream* substream )
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	if( !chip->started ) {
		//TODO: pass arg according to mode
		if( sc1445x_ae_set_mode( &chip->ae_state, chip->mode, NULL ) ) {
			return -EAGAIN ;
		}
		chip->started = 1 ;
	}

	memset( substream->runtime->dma_area, 0,
					substream->runtime->buffer_size ) ;
	chip->playback_r_pos = 0 ;

	return 0 ;
}


static int snd_sc1445x_playback_trigger( struct snd_pcm_substream* substream,
								int cmd )
{
	int res = 0 ;
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	switch( cmd ) {
		case SNDRV_PCM_TRIGGER_START:
			snd_printd( "%s: START\n", __FUNCTION__ ) ;
			break ;

		case SNDRV_PCM_TRIGGER_STOP:
			snd_printd( "%s: STOP\n", __FUNCTION__ ) ;
			break ;

		case SNDRV_PCM_TRIGGER_PAUSE_PUSH:
			snd_printd( "%s: PAUSE_PUSH\n", __FUNCTION__ ) ;
			sc1445x_private_stop_raw_pcm() ;
			break ;

		case SNDRV_PCM_TRIGGER_PAUSE_RELEASE:
			snd_printd( "%s: PAUSE_RELEASE\n", __FUNCTION__ ) ;
			sc1445x_ae_set_mode( &chip->ae_state, chip->mode,
				       		NULL ) ;
			break ;

		default:
			res = -EINVAL ;
			break ;
	}

	return res ;
}


static snd_pcm_uframes_t
snd_sc1445x_playback_pointer( struct snd_pcm_substream* substream )
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	return bytes_to_frames( substream->runtime, chip->playback_r_pos ) ;
}


/*
 * pass raw PCM frames to DSP for playback
 *
 * sample_bits should be either 8 or 16
 *
 * returns the number of bytes NOT written, or negative on error
 */
#if defined( USE_PCM_WRITE_BUFFER )
static int snd_sc1445x_playback_copy_raw_pcm_from_buf( sc1445x_ae_state* s,
	const void* src, unsigned nbytes, unsigned short sample_bits )
{
	sc1445x_ae_raw_pcm_state* raw_pcm = &s->raw_pcm ;
	unsigned char* p = (unsigned char*)( raw_pcm->buffer +
			raw_pcm->next_block_index * raw_pcm->block_size ) ;

	p += raw_pcm->partial ;

	while( have_space_for_raw_pcm( raw_pcm )  &&  nbytes > 0 ) {
		const unsigned bs = (sample_bits >> 3) * raw_pcm->block_size ;
		unsigned len =  nbytes > bs ?  bs :  nbytes ;
		unsigned slen = len ;

		if( len  >  bs - raw_pcm->partial )
			slen = len = bs - raw_pcm->partial ;

		if( 8 == sample_bits ) {
			/* convert 8-bit samples to 16-bit samples */
			const unsigned char* samples = src ;
			unsigned short* pp = (void*)p ;
			int i ;

			for( i = 0 ;  i < len ;  ++i ) {
				pp[i] = (unsigned short)samples[i] << 5 ;
			}
			len <<= 1 ;
			/* now len is 2 times the value of slen */
		} else 
			memcpy( p, src, len ) ;
		if( len + raw_pcm->partial  <  bs ) {
			/* pad with zeroes */
			memset( p + len, 0, bs - len - raw_pcm->partial ) ;
			raw_pcm->partial += len ;
		} else
			raw_pcm->partial = 0 ;

		/* advance pointers for next block */
		p += len ;
		src += slen ;
		nbytes -= slen ;

		if( !raw_pcm->partial ) {
			/* set the block status to 1; DSP will */
			/* clear it */
			raw_pcm->block_status [raw_pcm->next_block_index] = 1 ;
			DPRINT( "%s: filled block %d\n", __FUNCTION__,
						raw_pcm->next_block_index ) ;
			/* we've filled this buffer */
			if( raw_pcm->block_count ==
						++raw_pcm->next_block_index ) {
				raw_pcm->next_block_index = 0 ;
				p = (unsigned char*)raw_pcm->buffer ;
			}
		}
	}

	return nbytes ;
}
#else
static int snd_sc1445x_playback_copy_raw_pcm( sc1445x_ae_state* s,
	const void __user* src, unsigned nbytes, unsigned short sample_bits )
{
	sc1445x_ae_raw_pcm_state* raw_pcm = &s->raw_pcm ;
	unsigned char* p = (unsigned char*)( raw_pcm->buffer +
			raw_pcm->next_block_index * raw_pcm->block_size ) ;

	p += raw_pcm->partial ;

	while( have_space_for_raw_pcm( raw_pcm )  &&  nbytes > 0 ) {
		const unsigned bs = (sample_bits >> 3) * raw_pcm->block_size ;
		unsigned len =  nbytes > bs ?  bs :  nbytes ;
		unsigned slen = len ;

		if( len  >  bs - raw_pcm->partial )
			slen = len = bs - raw_pcm->partial ;

		if( 8 == sample_bits ) {
			/* convert 8-bit samples to 16-bit samples */
			const unsigned char __user* samples = src ;
			unsigned short* pp = (void*)p ;
			int i ;

			for( i = 0 ;  i < len ;  ++i ) {
				unsigned char s ;

				get_user( s, samples + i ) ;
				pp[i] = (unsigned short)s << 5 ;
			}
			len <<= 1 ;
			/* now len is 2 times the value of slen */
		} else if( copy_from_user( p, src, len ) )
				return -EFAULT ;
		if( len + raw_pcm->partial  <  bs ) {
			/* pad with zeroes */
			memset( p + len, 0, bs - len - raw_pcm->partial ) ;
			raw_pcm->partial += len ;
		} else
			raw_pcm->partial = 0 ;

		/* advance pointers for next block */
		p += len ;
		src = (void __user *)( (unsigned char*)src + slen ) ;
		nbytes -= slen ;

		if( !raw_pcm->partial ) {
			/* set the block status to 1; DSP will */
			/* clear it */
			raw_pcm->block_status[raw_pcm->next_block_index] = 1 ;
			/* we've filled this buffer */
			if( raw_pcm->block_count ==
						++raw_pcm->next_block_index ) {
				raw_pcm->next_block_index = 0 ;
				p = (unsigned char*)raw_pcm->buffer ;
			}
		}
	}

	return nbytes ;
}
#endif  /* USE_PCM_WRITE_BUFFER */

/* get frames from ALSA and put them into the "DMA" buffer allocated by ALSA */
/* we assume that the application feeds ALSA with enough frames for the */
/* maximum number (3) of audio channels -- if any channel(s) is(are) not */
/* being used, the corresponding frames are ignored */
/* frames for each audio packet are not interleaved */
#if defined( USE_PCM_WRITE_BUFFER )
#  define	P_COPY_RAW_PCM	snd_sc1445x_playback_copy_raw_pcm_from_buf
#else
#  define	P_COPY_RAW_PCM	snd_sc1445x_playback_copy_raw_pcm
#endif
static int snd_sc1445x_playback_copy( struct snd_pcm_substream* substream,
					int channel, snd_pcm_uframes_t pos,
					void __user * src,
					snd_pcm_uframes_t count )
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;
	struct snd_pcm_runtime* runtime = substream->runtime ;
	unsigned nbytes = frames_to_bytes( runtime, count ) ;
#if defined( SC1445x_AE_COLLECT_STATISTICS )
	sc1445x_ae_state* s = &chip->ae_state ;
	const unsigned short nch = s->audio_channels_count ;
	const unsigned short block_size = nch *
				( SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER +
					SC1445x_AE_BYTES_PER_AUDIO_PACKET )
#if 0
//#  if defined( SC1445x_AE_SUPPORT_FAX )
				+ ( SC1445x_AE_BYTES_PER_FAX_PACKET_HEADER
					+ SC1445x_AE_BYTES_PER_FAX_PACKET ) ;
#  else
							+ 0 ;
#  endif
#endif
#if defined( TEST_WITH_MARKED_PACKET )
	static const unsigned char marker[4] = {
		0xde, 0xad, 0xbe, 0xef
	} ;
#endif

	if( SC1445x_AE_MODE_NORMAL == chip->mode ) {
		unsigned char* p = runtime->dma_area +
					frames_to_bytes( runtime, pos ) ;

		if( copy_from_user( p, src, nbytes ) )
			return -EFAULT ;

#if defined( TEST_WITH_MARKED_PACKET )
		if( !memcmp( p + SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER,
						marker, sizeof marker ) ) {
			P2_SET_DATA_REG = 0x8 ;
			//P0_SET_DATA_REG = 0x200 ;
			udelay( 500 ) ;  /* just for signal to go up */
			P2_RESET_DATA_REG = 0x8 ;
			//P0_RESET_DATA_REG = 0x200 ;
		}
#endif

#if defined( SC1445x_AE_COLLECT_STATISTICS )
		if( block_size == nbytes ) {
			unsigned short ch ;
			unsigned short type ;
			unsigned short len ;
			sc1445x_ae_audio_stats* stats ;

			for( ch = 0 ;  ch < nch ;  ++ch ) {
				type = ( *(unsigned short*)p ) >> 8 ;
				len = ( *(unsigned short*)p ) & 0xff ;
				stats = &s->audio_channels[ch].stats ;

				if( unlikely( 0 == len ) ) {
					++stats->empty_from_os ;
				} else if( 1 == type ) {
					++stats->normal_from_os ;
				} else if( 2 == type ) {
					++stats->sid_from_os ;
				}
				else {
					++stats->empty_from_os ;
				}

				p += SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER +
					SC1445x_AE_BYTES_PER_AUDIO_PACKET ;
			}
		}
		else {
			snd_printk( PRINT_LEVEL "%s: can't keep stats; block "
				"size is %d bytes -- wanted %d bytes\n",
					__FUNCTION__, nbytes, block_size ) ;
		}
#endif
	} else if( SC1445x_AE_MODE_RAW_PCM == chip->mode) {
		int res =
			P_COPY_RAW_PCM( s, src, nbytes, runtime->sample_bits ) ;
		if( res < 0 )
			return res ;  /* error */

		if( unlikely( res > 0 ) ) {
			PRINT( PRINT_LEVEL "%s: exiting with %d bytes left!\n",
					__FUNCTION__, nbytes ) ;
		}
	}
	//TODO: need to do something for the other modes?

	return 0 ;
}


/* capture PCM ops */
static int snd_sc1445x_capture_open( struct snd_pcm_substream* substream ) 
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	substream->runtime->hw = snd_sc1445x_capture ;

	chip->capture_substream = substream ;
	/* invalidate pointer for capture */
	chip->capture_w_pos = -1 ;

	snd_printd( "%s: chip @%p, runtime->hw @%p\n",
			__FUNCTION__, chip, &substream->runtime->hw ) ;
	return 0 ;
}


static int snd_sc1445x_capture_close( struct snd_pcm_substream* substream )
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	chip->capture_substream = NULL ;
	//TODO: stop audio engine?
	if( !chip->playback_substream )
		chip->started = 0 ;

	return 0 ;
}


static int snd_sc1445x_capture_prepare( struct snd_pcm_substream* substream )
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	if( !chip->started ) {
		//TODO: pass arg according to mode
		if( sc1445x_ae_set_mode( &chip->ae_state, chip->mode, NULL ) ) {
			return -EAGAIN ;
		}
		chip->started = 1 ;
	}

	memset( substream->runtime->dma_area, 0,
					substream->runtime->buffer_size ) ;
	chip->capture_w_pos = 0 ;

	return 0 ;
}


static int snd_sc1445x_capture_trigger( struct snd_pcm_substream* substream,
								int cmd )
{
	int res = 0 ;

	switch( cmd ) {
		case SNDRV_PCM_TRIGGER_START:
			snd_printd( "%s: START\n", __FUNCTION__ ) ;
			break ;

		case SNDRV_PCM_TRIGGER_STOP:
			snd_printd( "%s: STOP\n", __FUNCTION__ ) ;
			break ;

		default:
			res = -EINVAL ;
			break ;
	}

	return res ;
}


static snd_pcm_uframes_t
snd_sc1445x_capture_pointer( struct snd_pcm_substream* substream )
{
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;

	return bytes_to_frames( substream->runtime, chip->capture_w_pos ) ;
}


/* common PCM ops */
static int snd_sc1445x_hw_params( struct snd_pcm_substream* substream,
					struct snd_pcm_hw_params* hw_params )
{
	return snd_pcm_lib_malloc_pages( substream,
					params_buffer_bytes( hw_params ) ) ;
}


static int snd_sc1445x_hw_free( struct snd_pcm_substream* substream )
{
        snd_pcm_lib_free_pages( substream ) ;
        return 0 ;
}


/* get frames from the "DMA" buffer allocated by ALSA and give them to ALSA */
/* we assume that the application asks ALSA for enough frames for the */
/* maximum number (3) of audio channels -- if any channel(s) is(are) not */
/* being used, the corresponding frames are ignored */
/* frames for each audio packet are not interleaved */
static int snd_sc1445x_capture_copy( struct snd_pcm_substream* substream,
					int channel, snd_pcm_uframes_t pos,
					void __user * dst,
					snd_pcm_uframes_t count )
{
#if defined( USE_FORCED_PACKET )
	const unsigned char forced_pkt[ 3 * ( SC1445x_AE_BYTES_PER_AUDIO_PACKET
			+ SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER ) ] = {
		0x50, 0x01,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0xd5, 0x25, 0x22, 0x25, 0xd5, 0xa5, 0xa2, 0xa5,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00,

		0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00,

		0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00
	} ;
#endif
#if defined( TEST_WITH_MARKED_PACKET )
	static const unsigned char marker[4] = {
		0xde, 0xad, 0xbe, 0xef
	} ;
	short mark = 0 ;
#endif
	struct snd_sc1445x* chip = snd_pcm_substream_chip( substream ) ;
	struct snd_pcm_runtime* runtime = substream->runtime ;
	unsigned nbytes = frames_to_bytes( runtime, count ) ;
#if defined( SC1445x_AE_COLLECT_STATISTICS ) && !defined( USE_FORCED_PACKET )
	sc1445x_ae_state* s = &chip->ae_state ;
	const unsigned short nch = s->audio_channels_count ;
	const unsigned short block_size = nch *
				( SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER +
					SC1445x_AE_BYTES_PER_AUDIO_PACKET ) ;
#endif

	if( SC1445x_AE_MODE_NORMAL == chip->mode ) {
#if !defined( USE_FORCED_PACKET )

		unsigned char* p = runtime->dma_area +
					frames_to_bytes( runtime, pos ) ;

		if( copy_to_user( dst, p, nbytes ) )
			return -EFAULT ;

#  if defined( TEST_WITH_MARKED_PACKET )
		if( !memcmp( p + SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER,
						marker, sizeof marker ) ) {
			P2_SET_DATA_REG = 0x8 ;
			//P0_SET_DATA_REG = 0x200 ;
			mark = 1 ;
		}
#  endif

#  if defined( SC1445x_AE_COLLECT_STATISTICS )
		if( block_size == nbytes ) {
			unsigned short ch ;
			unsigned short type ;
			sc1445x_ae_audio_stats* stats ;

			for( ch = 0 ;  ch < nch ;  ++ch ) {
				type = ( *(unsigned short*)p ) >> 8 ;
				stats = &s->audio_channels[ch].stats ;

				if( 1 == type ) {
					++stats->normal_to_os ;
				} else if( 2 == type ) {
					++stats->sid_to_os ;
				}
				else {
					++stats->empty_to_os ;
				}

				p += SC1445x_AE_BYTES_PER_AUDIO_PACKET_HEADER +
					SC1445x_AE_BYTES_PER_AUDIO_PACKET ;
			}
		}
		else {
			snd_printk( PRINT_LEVEL "%s: can't keep stats; block "
				"size is %d bytes -- wanted %d bytes\n",
					__FUNCTION__, nbytes, block_size ) ;
		}
#  endif

#  if defined( TEST_WITH_MARKED_PACKET )
		if( mark ) {
			P2_RESET_DATA_REG = 0x8 ;
			//P0_RESET_DATA_REG = 0x200 ;
		}
#  endif

#else
		if( sizeof( forced_pkt ) != nbytes )
			PRINT( PRINT_LEVEL "%s: count=%d (we expected %lu)\n",
				__FUNCTION__, nbytes, sizeof( forced_pkt ) ) ;
		else {
			if( copy_to_user( dst, forced_pkt, nbytes ) )
				return -EFAULT ;
		}
#endif
	}
	//TODO: need to do something for the other modes?

	return 0 ;
}



/* PCM ops */
static struct snd_pcm_ops snd_sc1445x_playback_ops = {
	.open =		snd_sc1445x_playback_open,
	.close =	snd_sc1445x_playback_close,
	.ioctl =	snd_pcm_lib_ioctl,
	.hw_params =	snd_sc1445x_hw_params,
	.hw_free =	snd_sc1445x_hw_free,
	.prepare =	snd_sc1445x_playback_prepare,
	.trigger =	snd_sc1445x_playback_trigger,
	.pointer =	snd_sc1445x_playback_pointer,
	.copy =		snd_sc1445x_playback_copy,
} ;

static struct snd_pcm_ops snd_sc1445x_capture_ops = {
	.open =		snd_sc1445x_capture_open,
	.close =	snd_sc1445x_capture_close,
	.ioctl =	snd_pcm_lib_ioctl,
	.hw_params =	snd_sc1445x_hw_params,
	.hw_free =	snd_sc1445x_hw_free,
	.prepare =	snd_sc1445x_capture_prepare,
	.trigger =	snd_sc1445x_capture_trigger,
	.pointer =	snd_sc1445x_capture_pointer,
	.copy =		snd_sc1445x_capture_copy,
} ;


static int __init snd_sc1445x_pcm_new( struct snd_sc1445x* chip )
{
	struct snd_pcm* pcm ;
	int res ;

	res = snd_pcm_new( chip->card, chip->card->driver, 0, 1, 1, &pcm ) ;
	if( res < 0 )
		return res ;

	snd_pcm_set_ops( pcm, SNDRV_PCM_STREAM_PLAYBACK,
						&snd_sc1445x_playback_ops ) ;
	snd_pcm_set_ops( pcm, SNDRV_PCM_STREAM_CAPTURE,
						&snd_sc1445x_capture_ops ) ;
	pcm->private_data = chip ;
	strncpy( pcm->name, chip->card->shortname, sizeof( pcm->name ) - 1 ) ;
	chip->pcm = pcm ;

	snd_pcm_lib_preallocate_pages_for_all( pcm, SNDRV_DMA_TYPE_CONTINUOUS,
					snd_dma_continuous_data(GFP_KERNEL),
								2560, 2560 ) ;

	return 0 ;
}


/* open hwdep */
static int snd_sc1445x_hwdep_open( struct snd_hwdep* hw, struct file* file )
{
	return 0 ;
}


/* close hwdep */
static int snd_sc1445x_hwdep_release( struct snd_hwdep* hw, struct file* file )
{
	return 0 ;
}


/* an ugly hack to initialize const fields of a struct */
#define INIT_CONST_FIELD( type, s, x, val )	\
	*( type* )&( (s)->x ) = (val)

#define REASSIGN_FIELD_PTR_AND_ADVANCE( params, field, p, n ) 		\
	do {								\
		params->field = p ;					\
		p += n ;						\
	} while( 0 ) ;

#define REASSIGN_ARRAY_PTR_AND_ADVANCE( a, p, sz ) 			\
	do {								\
		a = p ;							\
		p = (void*)( (unsigned long)p + sz ) ;			\
	} while( 0 ) ;

static short copy_ap_to_user( unsigned long arg,
					const sc1445x_ae_audio_profile* src_ap )
{
	sc1445x_ae_audio_profile* __user dst_ap = (void __user*)arg ;
	unsigned short i, j, n, cpn ;
	const sc1445x_ae_vspk_volumes* src_vsvs ;
	const sc1445x_ae_vmic_gains* src_vmgs ;
	sc1445x_ae_vspk_volumes* dst_vsvs ;
	sc1445x_ae_vmic_gains* dst_vmgs ;
	unsigned short __user* p ;
	unsigned short __user** pp ;
	int sz ;
	const sc1445x_ae_ap_custom_param* src_cp ;
	sc1445x_ae_ap_custom_param* dst_cp ;

	if( copy_to_user( dst_ap->data, src_ap->data, dst_ap->data_nbytes ) )
		return -EFAULT ;

	//TODO: copy stuff with _user macros

	/* update array pointers and copy data */
	p = dst_ap->data ;

	/* vspk_lvl_narrow and vspk_lvl_wide */
	n = src_ap->vspk_lvl_count ;
	for( i = 0 ;  i < SC1445x_AE_IFACE_MODE_COUNT_IN_AP ;  ++i ) {
		dst_vsvs = &dst_ap->vspk_lvl_narrow[i] ;
		src_vsvs = &src_ap->vspk_lvl_narrow[i] ;

		dst_vsvs->use_analog = src_vsvs->use_analog ;
		dst_vsvs->use_rx_path_att = src_vsvs->use_rx_path_att ;
		dst_vsvs->use_rx_path_shift = src_vsvs->use_rx_path_shift ;
		dst_vsvs->use_sidetone_att = src_vsvs->use_sidetone_att ;
		dst_vsvs->use_ext_spk_att = src_vsvs->use_ext_spk_att ;
		dst_vsvs->use_shift_ext_spk = src_vsvs->use_shift_ext_spk ;
		dst_vsvs->use_tone_vol = src_vsvs->use_tone_vol ;
		dst_vsvs->use_classd_vout = src_vsvs->use_classd_vout ;
		dst_vsvs->use_pcm_gain_pre = src_vsvs->use_pcm_gain_pre ;
		dst_vsvs->use_pcm_gain_shift = src_vsvs->use_pcm_gain_shift ;
		dst_vsvs->use_loopgain_rxpl = src_vsvs->use_loopgain_rxpl ;
		dst_vsvs->use_ng_threshold = src_vsvs->use_ng_threshold ;

		DPRINT( "%s: narrow vsv #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, analog_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, rx_path_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, rx_path_shift_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, sidetone_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ext_spk_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ring_playback_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, shift_ext_spk_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ring_tone_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, call_tone_codec_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, call_tone_classd_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, dtmf_tone_codec_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, dtmf_tone_classd_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, classd_vout, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_pre, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_shift, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_pre_ringtone, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_shift_ringtone, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, loopgain_rxpl_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ng_threshold_levels, p, n ) ;

		/* custom params */
		cpn = src_vsvs->cp.count ;
		DPRINT( "%s: narrow vsv #%2d custom params \t@%p (%d)\n",
						__FUNCTION__, i, p, cpn ) ;
		dst_vsvs->cp.count = cpn ;
		dst_vsvs->cp.params = (void*)p ;
		/* assume sizeof( sc1445x_ae_ap_custom_param ) is even */
		p += ( cpn * sizeof( sc1445x_ae_ap_custom_param ) ) >> 1 ;
		src_cp = src_vsvs->cp.params ;
		dst_cp = dst_vsvs->cp.params ;
		for( j = 0 ;  j < cpn ;  ++j, ++src_cp, ++dst_cp ) {
			dst_cp->use = src_cp->use ;
			dst_cp->addr = src_cp->addr ;
			REASSIGN_FIELD_PTR_AND_ADVANCE( dst_cp, values, p, n ) ;
		}


		dst_vsvs = &dst_ap->vspk_lvl_wide[i] ;
		src_vsvs = &src_ap->vspk_lvl_wide[i] ;

		dst_vsvs->use_analog = src_vsvs->use_analog ;
		dst_vsvs->use_rx_path_att = src_vsvs->use_rx_path_att ;
		dst_vsvs->use_rx_path_shift = src_vsvs->use_rx_path_shift ;
		dst_vsvs->use_sidetone_att = src_vsvs->use_sidetone_att ;
		dst_vsvs->use_ext_spk_att = src_vsvs->use_ext_spk_att ;
		dst_vsvs->use_shift_ext_spk = src_vsvs->use_shift_ext_spk ;
		dst_vsvs->use_tone_vol = src_vsvs->use_tone_vol ;
		dst_vsvs->use_classd_vout = src_vsvs->use_classd_vout ;
		dst_vsvs->use_pcm_gain_pre = src_vsvs->use_pcm_gain_pre ;
		dst_vsvs->use_pcm_gain_shift = src_vsvs->use_pcm_gain_shift ;
		dst_vsvs->use_loopgain_rxpl = src_vsvs->use_loopgain_rxpl ;
		dst_vsvs->use_ng_threshold = src_vsvs->use_ng_threshold ;


		DPRINT( "%s: wide vsv #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, analog_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, rx_path_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, rx_path_shift_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, sidetone_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ext_spk_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ring_playback_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, shift_ext_spk_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ring_tone_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, call_tone_codec_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, call_tone_classd_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, dtmf_tone_codec_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, dtmf_tone_classd_vol_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, classd_vout, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_pre, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_shift, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_pre_ringtone, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, pcm_gain_shift_ringtone, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, loopgain_rxpl_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vsvs, ng_threshold_levels, p, n ) ;

		/* custom params */
		cpn = src_vsvs->cp.count ;
		DPRINT( "%s: wide vsv #%2d custom params \t@%p (%d)\n",
						__FUNCTION__, i, p, cpn ) ;
		dst_vsvs->cp.count = cpn ;
		dst_vsvs->cp.params = (void*)p ;
		/* assume sizeof( sc1445x_ae_ap_custom_param ) is even */
		p += ( cpn * sizeof( sc1445x_ae_ap_custom_param ) ) >> 1 ;
		src_cp = src_vsvs->cp.params ;
		dst_cp = dst_vsvs->cp.params ;
		for( j = 0 ;  j < cpn ;  ++j, ++src_cp, ++dst_cp ) {
			dst_cp->use = src_cp->use ;
			dst_cp->addr = src_cp->addr ;
			REASSIGN_FIELD_PTR_AND_ADVANCE( dst_cp, values, p, n ) ;
		}
	}
	INIT_CONST_FIELD( unsigned short, dst_ap, vspk_lvl_count, n ) ;

	/* vmic_lvl_narrow and vmic_lvl_wide */
	n = src_ap->vmic_lvl_count ;
	for( i = 0 ;  i < SC1445x_AE_IFACE_MODE_COUNT_IN_AP ;  ++i ) {
		dst_vmgs = &dst_ap->vmic_lvl_narrow[i] ;
		src_vmgs = &src_ap->vmic_lvl_narrow[i] ;

		dst_vmgs->use_analog = src_vmgs->use_analog ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		dst_vmgs->use_shift_paec_out = src_vmgs->use_shift_paec_out ;
		dst_vmgs->use_paec_tx_att = src_vmgs->use_paec_tx_att ;
		dst_vmgs->use_attlimit = src_vmgs->use_attlimit ;
		dst_vmgs->use_supmin = src_vmgs->use_supmin ;
		dst_vmgs->use_noiseattlimit = src_vmgs->use_noiseattlimit ;
#endif

		DPRINT( "%s: narrow vmg #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, analog_levels, p, n ) ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, shift_paec_out_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, shift_paec_out_levels_BT, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, paec_tx_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, paec_tx_att_levels_BT, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, attlimit, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, supmin, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, noiseattlimit, p, n ) ;

		/* custom params */
		cpn = src_vmgs->cp.count ;
		DPRINT( "%s: narrow vmg #%2d custom params \t@%p (%d)\n",
						__FUNCTION__, i, p, cpn ) ;
		dst_vmgs->cp.count = cpn ;
		dst_vmgs->cp.params = (void*)p ;
		/* assume sizeof( sc1445x_ae_ap_custom_param ) is even */
		p += ( cpn * sizeof( sc1445x_ae_ap_custom_param ) ) >> 1 ;
		src_cp = src_vmgs->cp.params ;
		dst_cp = dst_vmgs->cp.params ;
		for( j = 0 ;  j < cpn ;  ++j, ++src_cp, ++dst_cp ) {
			dst_cp->use = src_cp->use ;
			dst_cp->addr = src_cp->addr ;
			REASSIGN_FIELD_PTR_AND_ADVANCE( dst_cp, values, p, n ) ;
		}
#endif


		dst_vmgs = &dst_ap->vmic_lvl_wide[i] ;
		src_vmgs = &src_ap->vmic_lvl_wide[i] ;

		dst_vmgs->use_analog = src_vmgs->use_analog ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		dst_vmgs->use_shift_paec_out = src_vmgs->use_shift_paec_out ;
		dst_vmgs->use_paec_tx_att = src_vmgs->use_paec_tx_att ;
		dst_vmgs->use_attlimit = src_vmgs->use_attlimit ;
		dst_vmgs->use_supmin = src_vmgs->use_supmin ;
		dst_vmgs->use_noiseattlimit = src_vmgs->use_noiseattlimit ;
#endif


		DPRINT( "%s: wide vmg #%2d \t@%p (%d)\n", __FUNCTION__, i, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, analog_levels, p, n ) ;
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, shift_paec_out_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, shift_paec_out_levels_BT, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, paec_tx_att_levels, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, paec_tx_att_levels_BT, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, attlimit, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, supmin, p, n ) ;
		REASSIGN_FIELD_PTR_AND_ADVANCE( dst_vmgs, noiseattlimit, p, n ) ;

		/* custom params */
		cpn = src_vmgs->cp.count ;
		DPRINT( "%s: wide vmg #%2d custom params \t@%p (%d)\n",
						__FUNCTION__, i, p, cpn ) ;
		dst_vmgs->cp.count = cpn ;
		dst_vmgs->cp.params = (void*)p ;
		/* assume sizeof( sc1445x_ae_ap_custom_param ) is even */
		p += ( cpn * sizeof( sc1445x_ae_ap_custom_param ) ) >> 1 ;
		src_cp = src_vmgs->cp.params ;
		dst_cp = dst_vmgs->cp.params ;
		for( j = 0 ;  j < cpn ;  ++j, ++src_cp, ++dst_cp ) {
			dst_cp->use = src_cp->use ;
			dst_cp->addr = src_cp->addr ;
			REASSIGN_FIELD_PTR_AND_ADVANCE( dst_cp, values, p, n ) ;
		}
#endif
	}
	INIT_CONST_FIELD( unsigned short, dst_ap, vmic_lvl_count, n ) ;

	/* filters */
	for( i = 0 ;  i < 3 ;  ++i ) {
		for( j = 0 ;  j < FILTER_SIZE ;  ++j ) {
			dst_ap->narrowband_filters_TX1[i][j] =
					src_ap->narrowband_filters_TX1[i][j] ;
			dst_ap->narrowband_filters_TX2[i][j] =
					src_ap->narrowband_filters_TX2[i][j] ;
			dst_ap->narrowband_filters_TX3[i][j] =
					src_ap->narrowband_filters_TX3[i][j] ;
			dst_ap->wideband_filters_TX1[i][j] =
					src_ap->wideband_filters_TX1[i][j] ;
			dst_ap->wideband_filters_TX2[i][j] =
					src_ap->wideband_filters_TX2[i][j] ;
			dst_ap->wideband_filters_TX3[i][j] =
					src_ap->wideband_filters_TX3[i][j] ;

			dst_ap->narrowband_filters_RX1[i][j] =
					src_ap->narrowband_filters_RX1[i][j] ;
			dst_ap->narrowband_filters_RX2[i][j] =
					src_ap->narrowband_filters_RX2[i][j] ;
			dst_ap->narrowband_filters_RX3[i][j] =
					src_ap->narrowband_filters_RX3[i][j] ;
			dst_ap->wideband_filters_RX1[i][j] =
					src_ap->wideband_filters_RX1[i][j] ;
			dst_ap->wideband_filters_RX2[i][j] =
					src_ap->wideband_filters_RX2[i][j] ;
			dst_ap->wideband_filters_RX3[i][j] =
					src_ap->wideband_filters_RX3[i][j] ;
		}
	}

#if defined( CONFIG_SND_SC1445x_USE_PAEC )
#  if AUDIO_PROFILE_MAGIC == AUDIO_PROFILE_MAGIC_AP10
	/* PAEC state array setup */
	n = src_ap->paec_band_count ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: paec_band_loc \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_band_loc, p, sz ) ;
	DPRINT( "%s: paec_band_narrow \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_band_narrow, p, sz ) ;
	DPRINT( "%s: paec_band_wide \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_band_wide, p, sz ) ;
	INIT_CONST_FIELD( unsigned short, dst_ap, paec_band_count, n ) ;
#  endif

	/* PAEC parameters setup */
	n = src_ap->paec_data_count ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: paec_data_loc \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_data_loc, p, sz ) ;
	DPRINT( "%s: paec_date_narrow \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_data_narrow, p, sz ) ;
	DPRINT( "%s: paec_data_wide \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->paec_data_wide, p, sz ) ;
	INIT_CONST_FIELD( unsigned short, dst_ap, paec_data_count, n ) ;

	/* Suppressor Setup*/
	n = src_ap->supp_params_count ;
	sz = n * sizeof(unsigned short*) ;
	DPRINT( "%s: supp_params_addr \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	pp = (void*)p ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->supp_params_addr, pp, sz ) ;
	p = (void*)pp ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: supp_params_data_narrow \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->supp_params_data_narrow, p, sz ) ;
	DPRINT( "%s: supp_params_data_wide \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->supp_params_data_wide, p, sz ) ;
	INIT_CONST_FIELD( unsigned short, dst_ap, supp_params_count, n ) ;
#endif

#if defined( CONFIG_SND_SC1445x_USE_AEC )
	/* AEC setup */
	n = src_ap->aec_params_count ;
	sz = n * sizeof(unsigned short) ;
	DPRINT( "%s: aec_params_data \t@%p (%d)\n", __FUNCTION__, p, n ) ;
	REASSIGN_ARRAY_PTR_AND_ADVANCE( dst_ap->aec_params_data, p, sz ) ;
	INIT_CONST_FIELD( unsigned short, dst_ap, aec_params_count, n ) ;
#endif

	if( p != dst_ap->data + dst_ap->data_nbytes ) {
		PRINT( "%s: wrote audio profile dynamic data up to %p\n"
			"%s: allocated space up to %p\n",
				__FUNCTION__, p, __FUNCTION__,
				dst_ap->data + dst_ap->data_nbytes ) ;
	}

	return 0 ;
}

static short copy_ap_from_user( sc1445x_ae_audio_profile* dst_ap,
							unsigned  long arg )
{
	//TODO: do it properly
	const sc1445x_ae_audio_profile* src_ap = (void*)arg ;

	dst_ap->data = MALLOC( src_ap->data_nbytes ) ;
	if( !dst_ap->data ) {
		PRINT( "%s: could not allocate memory for dynamic data\n",
								__FUNCTION__ ) ;
		return -ENOMEM ;
	}
	dst_ap->data_nbytes = src_ap->data_nbytes ;

	return copy_ap_to_user( (unsigned long)dst_ap, src_ap ) ;
}


#define READ_USER_ARG( dst, arg ) \
	copy_from_user( &(dst), (void __user*)(arg), sizeof( dst ) )

#define WRITE_USER_ARG( arg, src ) \
	copy_to_user( (void __user*)(arg), &(src), sizeof( src ) )



#if defined( TRACE_IOCTL ) || defined( DUMP_IOCTL )
static void dump_ioctl_name( unsigned int cmd )
{
	do {
		const char* ioctl_name[] = {
			"reserved"			/* 0x00 */,
			"reserved"			/* 0x01 */,
			"reserved"			/* 0x02 */,
			"reserved"			/* 0x03 */,
			"reserved"			/* 0x04 */,
			"reserved"			/* 0x05 */,
			"reserved"			/* 0x06 */,
			"reserved"			/* 0x07 */,
			"reserved"			/* 0x08 */,
			"reserved"			/* 0x09 */,
			"reserved"			/* 0x0a */,
			"reserved"			/* 0x0b */,
			"reserved"			/* 0x0c */,
			"reserved"			/* 0x0d */,
			"reserved"			/* 0x0e */,
			"reserved"			/* 0x0f */,
			"SET_MODE"			/* 0x10 */,
			"GET_MODE"			/* 0x11 */,
			"START_AUDIO_CHANNEL"		/* 0x12 */,
			"STOP_AUDIO_CHANNEL"		/* 0x13 */,
			"SET_SPK_VOL"			/* 0x14 */,
			"GET_SPK_VOL"			/* 0x15 */,
			"SET_MIC_GAIN"			/* 0x16 */,
			"GET_MIC_GAIN"			/* 0x17 */,
			"START_TONE"			/* 0x18 */,
			"STOP_TONE"			/* 0x19 */,
			"PLAY_STD_TONE"			/* 0x1a */,
			"SET_TONE_VOL"			/* 0x1b */,
			"GET_TONE_VOL"			/* 0x1c */,
			"MUTE_SPK"			/* 0x1d */,
			"UNMUTE_SPK"			/* 0x1e */,
			"MUTE_MIC"			/* 0x1f */,
			"UNMUTE_MIC"			/* 0x20 */,
			"SELECT_HANDSET"		/* 0x21 */,
			"SELECT_HEADSET"		/* 0x22 */,
			"SELECT_EXTERNAL"		/* 0x23 */,
			"SEND_TONES"			/* 0x24 */,
			"DONT_SEND_TONES"		/* 0x25 */,
			"SET_DIGITAL_VOL"		/* 0x26 */,
			"SET_SIDETONE_VOL"		/* 0x27 */,
			"GET_SIDETONE_VOL"		/* 0x28 */,
			"SEL_OUTPUT_TO_CODEC"		/* 0x29 */,
			"SEL_OUTPUT_TO_EXT_SPK"		/* 0x2a */,
			"SET_EXT_SPK_VOL"		/* 0x2b */,
			"TURN_AEC_ON"			/* 0x2c */,
			"TURN_AEC_OFF"			/* 0x2d */,
			"GET_AUDIO_CHANNEL_STATS"	/* 0x2e */,
			"RESET_AUDIO_STATS"		/* 0x2f */,
			"GET_AUDIO_DRIVER_VERSION"	/* 0x30 */,
			"GET_DSP_FIRMWARE_VERSION"	/* 0x31 */,
			"SET_IFACE_MODE"		/* 0x32 */,
			"GET_IFACE_MODE"		/* 0x33 */,
			"SET_VIRTUAL_SPK_VOL"		/* 0x34 */,
			"GET_VIRTUAL_SPK_VOL"		/* 0x35 */,
			"SET_VIRTUAL_MIC_GAIN"		/* 0x36 */,
			"GET_VIRTUAL_MIC_GAIN"		/* 0x37 */,
			"MUTE_VIRTUAL_MIC"		/* 0x38 */,
			"UNMUTE_VIRTUAL_MIC"		/* 0x39 */,
			"CONNECT_TO_LINE"		/* 0x3a */,
			"CONNECT_LINES"			/* 0x3b */,
			"DISCONNECT_LINE"		/* 0x3c */,
			"GET_DSP2_FIRMWARE_VERSION"	/* 0x3d */,
			"SWITCH_AUDIO_TO_FAX"		/* 0x3e */,
			"SWITCH_FAX_TO_AUDIO"		/* 0x3f */,
			"SET_CID_INFO"			/* 0x40 */,
			"CID_IND_FIRST_RING"		/* 0x41 */,
			"CID_IND_OFF_HOOK"		/* 0x42 */,
			"CID_IND_ON_HOOK"		/* 0x43 */,
			"SET_LINE_TYPE"			/* 0x44 */,
			"GET_LINE_TYPE"			/* 0x45 */,
			"GET_VOL_GAIN_LEVEL_COUNT"	/* 0x46 */,
			"FAX_INIT"			/* 0x47 */,
			"START_CUSTOM_TONE"		/* 0x48 */,
			"START_TONE_SEQUENCE"		/* 0x49 */,
			"EXPAND_TONE_SEQUENCE"		/* 0x4a */,
			"SEND_TONES_EX"			/* 0x4b */,
			"DONT_SEND_TONES_EX"		/* 0x4c */,
			"GET_PLATFORM_INFO"		/* 0x4d */,
			"ATTACH_TO_PCM_DEV"		/* 0x4e */,
			"DETACH_FROM_PCM_DEV"		/* 0x4f */,
			"AUTO_STOP_TONE"		/* 0x50 */,
			"SET_VIRTUAL_SPK_DTMF_LVL"	/* 0x51 */,
			"GET_VIRTUAL_SPK_DTMF_LVL"	/* 0x52 */,
			"CONTROL_DSP_LOOPBACK"		/* 0x53 */,
			"START_CONVERSATION"		/* 0x54 */,
			"STOP_CONVERSATION"		/* 0x55 */,
			"CONVERSATION_ADD_CHANNEL"	/* 0x56 */,
			"CONVERSATION_REM_CHANNEL"	/* 0x57 */,
			"CONVERSATION_ADD_LOCAL_IFACE"	/* 0x58 */,
			"CONVERSATION_REM_LOCAL_IFACE"	/* 0x59 */,
			"CONVERSATION_ADD_DECT"		/* 0x5a */,
			"CONVERSATION_REM_DECT"		/* 0x5b */,
			"SET_VIRTUAL_SPK_VOL_EX"	/* 0x5c */,
			"GET_VIRTUAL_SPK_VOL_EX"	/* 0x5d */,
			"SET_VIRTUAL_MIC_GAIN_EX"	/* 0x5e */,
			"GET_VIRTUAL_MIC_GAIN_EX"	/* 0x5f */,
			"SET_RAW_PCM_PARAMS"		/* 0x60 */,
			"START_TONE4"			/* 0x61 */,
			"START_CUSTOM_TONE4"		/* 0x62 */,
			"START_TONE4_SEQUENCE"		/* 0x63 */,
			"EXPAND_TONE4_SEQUENCE"		/* 0x64 */,
			"SET_TONEGEN_TX_VOL"		/* 0x65 */,
			"SET_TONEGEN_RX_VOL"		/* 0x66 */,
			"GET_TONEGEN_TX_VOL"		/* 0x67 */,
			"GET_TONEGEN_RX_VOL"		/* 0x68 */,
			"RESTART_AUDIO_ENGINE"		/* 0x69 */,
			"RESET_AUDIO_PROFILE"		/* 0x6a */,
			"GET_AUDIO_PROFILE_DYN_SZ"	/* 0x6b */,
			"GET_AUDIO_PROFILE"		/* 0x6c */,
			"SET_AUDIO_PROFILE"		/* 0x6d */,
			"REGISTER_TONE_CB_PID"		/* 0x6e */,
			"SET_VSPK_PCM_LVL"		/* 0x6f */,
			"GET_VSPK_PCM_LVL"		/* 0x70 */,
			"SET_VSPK_PCM_LVL_EX"		/* 0x71 */,
			"GET_VSPK_PCM_LVL_EX"		/* 0x72 */,
			"CID_LINE_REVERSAL"		/* 0x73 */,
			"STOP_RAW_PCM"			/* 0x74 */,
			"ENABLE_DTMF_PASSTHROUGH"	/* 0x75 */,
			"DISABLE_DTMF_PASSTHROUGH"	/* 0x76 */,
			"ENABLE_TONE_DET"		/* 0x77 */,
			"DISABLE_TONE_DET"		/* 0x78 */,
			"ENABLE_DUAL_AUDIO_PATH"	/* 0x79 */,
			"DISABLE_DUAL_AUDIO_PATH"	/* 0x7a */,
			"ENABLE_MONITORING"		/* 0x7b */,
			"DISABLE_MONITORING"		/* 0x7c */,
			"SNDRV_SC1445x_SET_VIRTUAL_SPK_DTMF_LVL_EX"  /* 0x7d */,
			"SNDRV_SC1445x_GET_VIRTUAL_SPK_DTMF_LVL_EX"  /* 0x7e */,
			"SNDRV_SC1445x_ENABLE_EXTERNAL_CODEC"	     /* 0x7f */,
			"SNDRV_SC1445x_DISABLE_EXTERNAL_CODEC"	     /* 0x80 */,
			"SNDRV_SC1445x_ENABLE_EXTERNAL_CODEC_SPK"    /* 0x81 */,
			"SNDRV_SC1445x_DISABLE_EXTERNAL_CODEC_SPK"   /* 0x82 */
		} ;
		unsigned short c = cmd & 0xff ;

		PRINT( PRINT_LEVEL "%s: cmd='%s'\n", __FUNCTION__,
				( c <= ARRAY_SIZE( ioctl_name ) ) ?
					ioctl_name[c] :  "invalid" ) ;
	} while( 0 ) ;
}
#endif


#if defined( TRACE_IOCTL )

#  define IOCTL_TRACE_SIZE	256

static unsigned char ioctl_trace[IOCTL_TRACE_SIZE] ;
static unsigned short ioctl_trace_pos = 0 ;

void sc1445x_private_dump_ioctl_trace( void )
{
	int i, j ;

	PRINT( "\nioctl trace:\n" ) ;
	for( i = 0, j = ioctl_trace_pos ;  i < IOCTL_TRACE_SIZE ;  ++i ) {
		dump_ioctl_name( ioctl_trace[j] ) ;
		if( ++j == IOCTL_TRACE_SIZE )
			j = 0 ;
	}
}

#endif


/* hwdep ioctls */
static int snd_sc1445x_hwdep_ioctl( struct snd_hwdep* hw, struct file* file,
					unsigned int cmd, unsigned long arg )
{
	struct snd_sc1445x* chip = hw->private_data ;
	sc1445x_audio_mode_t mode_t ;
	sc1445x_start_audio_channel_t start_t ;
	sc1445x_stop_audio_channel_t stop_t ;
	sc1445x_tone_t tone_t ;
	sc1445x_stop_tone_t stop_tone_t ;
	sc1445x_auto_stop_tone_t auto_stop_tone_t ;
	sc1445x_std_tone_t std_tone_t ;
	sc1445x_tone_vol_t tone_vol_t ;
	sc1445x_custom_tone_t custom_tone_t ;
	sc1445x_tone_seq_t tone_seq_t ;
	sc1445x_send_tone_ex_t send_tone_ex_t ;
	sc1445x_sidetone_vol_t sidetone_vol_t ;
#if defined( SC1445x_AE_COLLECT_STATISTICS )
	sc1445x_audio_stats_t audio_stats_t ;
#endif
	sc1445x_ae_version_t ae_version_t ;
	sc1445x_platform_info_t platform_info_t ;
	sc1445x_iface_mode_t if_mode_t ;
	sc1445x_ae_vspk_vol_t vspk_vol_t ;
	sc1445x_ae_vmic_gain_t vmic_gain_t ;
	sc1445x_vol_gain_count_t vol_gain_count_t ;
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	sc1445x_connect_to_line_t conn_to_line_t ;
	sc1445x_connect_lines_t conn_lines_t ;
	sc1445x_disconnect_line_t disconn_line_t ;
	sc1445x_set_cid_info_t set_cid_info_t ;
	sc1445x_cid_ind_t cid_ind_t ;
#endif
#if defined( SC1445x_AE_PCM_LINES_SUPPORT ) || defined( SC1445x_AE_PHONE_DECT )
	sc1445x_line_type_t line_type_t ;
#endif
#if defined( SC1445x_AE_BT )
	sc1445x_attach_to_pcm_dev_t attach_to_pcm_dev_t ;
	sc1445x_detach_from_pcm_dev_t detach_from_pcm_dev_t ;
#endif
#if defined( SC1445x_AE_SUPPORT_FAX )
	sc1445x_audio_to_fax_t audio2fax_t ;
	sc1445x_fax_to_audio_t fax2audio_t ;
	sc1445x_fax_init_params_t fax_init_params_t ;
#endif
	sc1445x_ae_ctrl_dsp_loopback_t dsp_lb ;
	sc1445x_ae_set_vspk_vol_ex_t set_vspk_vol_ex ;
	sc1445x_ae_get_vspk_vol_ex_t get_vspk_vol_ex ;
	sc1445x_ae_set_vmic_gain_ex_t set_vmic_gain_ex ;
	sc1445x_ae_get_vmic_gain_ex_t get_vmic_gain_ex ;
#if defined( USE_PCM_CHAR_DEVICE )
	sc1445x_ae_set_raw_pcm_params_t set_raw_pcm_params ;
#endif
	sc1445x_tone4_t tone4_t ;
	sc1445x_custom_tone4_t custom_tone4_t ;
	sc1445x_tone4_seq_t tone4_seq_t ;
#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
	sc1445x_ae_set_tonegen_tx_vol_t set_tg_tx_vol ;
	sc1445x_ae_set_tonegen_rx_vol_t set_tg_rx_vol ;
	sc1445x_ae_get_tonegen_tx_vol_t get_tg_tx_vol ;
	sc1445x_ae_get_tonegen_rx_vol_t get_tg_rx_vol ;
#endif
	sc1445x_register_tone_cb_pid rtcbp ;
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
	sc1445x_line l ;
#endif
#if defined( SC1445x_AE_ATA_SUPPORT )
	sc1445x_tone_det td ;
#endif
#if defined( TRACE_IOCTL )
	unsigned long flags ;
#endif
	sc1445x_ae_audio_profile ap ;

	int res = 0 ;

	snd_assert( chip != NULL, return -EINVAL ) ;

#if defined( TRACE_IOCTL )
	local_irq_save( flags ) ;
	ioctl_trace[ioctl_trace_pos++] = (unsigned char)cmd ;
	if( IOCTL_TRACE_SIZE == ioctl_trace_pos )
		ioctl_trace_pos = 0 ;
	local_irq_restore( flags ) ;
#endif

#if defined( DUMP_IOCTL )
	dump_ioctl_name( cmd ) ;
#endif

	switch( cmd ) {
		case SNDRV_SC1445x_SET_MODE:
			if( copy_from_user( &mode_t, (void __user*)arg,
							sizeof( mode_t ) ) )
				res = -EFAULT ;
			else {
				void* sm_arg = NULL ;
				sc1445x_ae_state* ae_state = &chip->ae_state ;

				if( SC1445x_AE_MODE_AUTOPLAY == mode_t.mode ) {
					if( mode_t.auto_play.nchannels !=
					     ae_state->audio_channels_count ) {
						/* # channels must match */
						res = -EINVAL ;
						break ;
					}

					sm_arg = mode_t.auto_play.pointers ;
				}

				if( SC1445x_AE_MODE_RAW_PCM == mode_t.mode ) {
					/* don't actually change the mode now */
					/* it will be set by playback_open() */
					chip->mode = SC1445x_AE_MODE_RAW_PCM ;
				}
				else {
					if( SC1445x_AE_MODE_RAW_PCM
							== chip->mode ) {
						sc1445x_private_stop_raw_pcm() ;
					}
					res = sc1445x_ae_set_mode( ae_state,
							mode_t.mode, sm_arg ) ;
					chip->mode = ae_state->mode ;
				}
			}
			break ;

		case SNDRV_SC1445x_GET_MODE:
			memset( &mode_t, 0, sizeof( mode_t ) ) ;
			res = sc1445x_ae_get_mode( &chip->ae_state,
							&mode_t.mode ) ;
			if( res )
				break ;
			if( copy_to_user( (void __user*)arg, &mode_t,
							sizeof( mode_t ) ) )
				res = -EFAULT ;
			break ;

		case SNDRV_SC1445x_START_AUDIO_CHANNEL:
			if( copy_from_user( &start_t, (void __user*)arg,
							sizeof( start_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_activate_free_channel(
						&chip->ae_state,
						start_t.enc_codec,
						start_t.dec_codec,
						&start_t.activated_channel ) ;
				if( res )
					break ;  /* error */
				if( copy_to_user( (void __user*)arg, &start_t,
							sizeof( start_t ) ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_STOP_AUDIO_CHANNEL:
			if( copy_from_user( &stop_t, (void __user*)arg,
							sizeof( stop_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_deactivate_channel(
						&chip->ae_state,
						stop_t.channel_index ) ;
			}
			break ;

		case SNDRV_SC1445x_START_TONE:
			tone_t.is_key_tone = 0 ;
			if( copy_from_user( &tone_t, (void __user*)arg,
							sizeof( tone_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				tone_t.tg_mod = sc1445x_private_line2stream(
					&chip->ae_state, tone_t.tg_mod, 1 ) ;
#endif
				if( tone_t.is_key_tone )
					res = sc1445x_ae_start_key_tone(
						&chip->ae_state,
						tone_t.tg_mod, tone_t.tone1,
						tone_t.tone2, tone_t.tone3,
						tone_t.dur_on, tone_t.dur_off,
							tone_t.repeat ) ;
				else
					res = sc1445x_ae_start_tone(
						&chip->ae_state,
						tone_t.tg_mod, tone_t.tone1,
						tone_t.tone2, tone_t.tone3,
						tone_t.dur_on, tone_t.dur_off,
							tone_t.repeat ) ;
			}
			break ;

		case SNDRV_SC1445x_START_CUSTOM_TONE:
			if( copy_from_user( &custom_tone_t, (void __user*)arg,
						sizeof( custom_tone_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				custom_tone_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						custom_tone_t.tg_mod, 1 ) ;
#endif
				res = sc1445x_ae_start_custom_tone(
						&chip->ae_state,
						custom_tone_t.tg_mod,
						&custom_tone_t.params,
						custom_tone_t.dur_on,
						custom_tone_t.dur_off,
						custom_tone_t.repeat ) ;
			}
			break ;

		case SNDRV_SC1445x_START_TONE_SEQUENCE:
			tone_seq_t.is_ringtone = 0 ;
			if( copy_from_user( &tone_seq_t, (void __user*)arg,
						sizeof( tone_seq_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				tone_seq_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						tone_seq_t.tg_mod, 1 ) ;
#endif
				if( !tone_seq_t.is_ringtone )
					res = sc1445x_ae_start_tone_sequence(
						&chip->ae_state,
						tone_seq_t.tg_mod,
						tone_seq_t.seq_len,
						tone_seq_t.seq,
						tone_seq_t.repeat_seq ) ;
				else
					res = sc1445x_ae_start_ringtone_sequence(
						&chip->ae_state,
						tone_seq_t.tg_mod,
						tone_seq_t.seq_len,
						tone_seq_t.seq,
						tone_seq_t.repeat_seq ) ;
			}
			break ;

		case SNDRV_SC1445x_EXPAND_TONE_SEQUENCE:
			if( copy_from_user( &tone_seq_t, (void __user*)arg,
						sizeof( tone_seq_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				tone_seq_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						tone_seq_t.tg_mod, 1 ) ;
#endif
				res = sc1445x_ae_expand_tone_sequence(
						&chip->ae_state,
						tone_seq_t.tg_mod,
						tone_seq_t.seq_len,
						tone_seq_t.seq ) ;
			}
			break ;

		case SNDRV_SC1445x_STOP_TONE:
			stop_tone_t.tg_mod = 0 ;
			if( arg && copy_from_user( &stop_tone_t,
						(void __user*)arg,
						sizeof( stop_tone_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				stop_tone_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						stop_tone_t.tg_mod, 0 ) ;
#endif
				res = sc1445x_ae_stop_tone( &chip->ae_state,
							stop_tone_t.tg_mod ) ;
			}
			break ;

		case SNDRV_SC1445x_AUTO_STOP_TONE:
			if( arg && copy_from_user( &auto_stop_tone_t,
						(void __user*)arg,
						sizeof( auto_stop_tone_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				auto_stop_tone_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						auto_stop_tone_t.tg_mod, 0 ) ;
#endif
				res = sc1445x_ae_auto_stop_tone( &chip->ae_state,
					auto_stop_tone_t.tg_mod,
					auto_stop_tone_t.stop_interval ) ;
			}
			break ;

		case SNDRV_SC1445x_PLAY_STD_TONE:
			if( copy_from_user( &std_tone_t, (void __user*)arg,
							sizeof( std_tone_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				std_tone_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						std_tone_t.tg_mod, 1 ) ;
#endif
				res = sc1445x_ae_play_standard_tone(
							&chip->ae_state,
							std_tone_t.tg_mod,
							std_tone_t.tone ) ;
			}
			break ;

		case SNDRV_SC1445x_SET_TONE_VOL:
			if( copy_from_user( &tone_vol_t, (void __user*)arg,
							sizeof( tone_vol_t ) ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				tone_vol_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						tone_vol_t.tg_mod, 1 ) ;
#endif
				res = sc1445x_ae_set_tone_volume(
							&chip->ae_state,
							tone_vol_t.tg_mod,
							tone_vol_t.vol ) ;
			}
			break ;

		case SNDRV_SC1445x_GET_TONE_VOL:
			if( copy_from_user( &tone_vol_t, (void __user*)arg,
						sizeof( tone_vol_t ) ) ) {
				res = -EFAULT ;
				break ;
			}

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
			tone_vol_t.tg_mod = sc1445x_private_line2stream(
							&chip->ae_state,
							tone_vol_t.tg_mod, 1 ) ;
#endif
			res = sc1445x_ae_get_tone_volume( &chip->ae_state,
							tone_vol_t.tg_mod,
							&tone_vol_t.vol ) ;
			if( res )
				break ;  /* error */
			if( copy_to_user( (void __user*)arg, &tone_vol_t,
						sizeof( tone_vol_t ) ) )
				res = -EFAULT ;
			break ;

		case SNDRV_SC1445x_MUTE_SPK:
			res = sc1445x_ae_mute_spk( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_UNMUTE_SPK:
			res = sc1445x_ae_unmute_spk( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_MUTE_MIC:
			res = sc1445x_ae_mute_mic( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_UNMUTE_MIC:
			res = sc1445x_ae_unmute_mic( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_SEND_TONES:
			res = sc1445x_ae_set_tone_volume_tx( &chip->ae_state,
					SC1445x_AE_TONEGEN_DEFAULT_VOLUME_TX ) ;
			break ;

		case SNDRV_SC1445x_DONT_SEND_TONES:
			res = sc1445x_ae_set_tone_volume_tx( &chip->ae_state,
								0x0000 ) ;
			break ;

		case SNDRV_SC1445x_SEND_TONES_EX:
			if( copy_from_user( &send_tone_ex_t, (void __user*)arg,
						sizeof( send_tone_ex_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_tone_volume_tx_ex(
					&chip->ae_state, send_tone_ex_t.tg_mod,
					SC1445x_AE_TONEGEN_DEFAULT_VOLUME_TX ) ;
			}
			break ;

		case SNDRV_SC1445x_DONT_SEND_TONES_EX:
			if( copy_from_user( &send_tone_ex_t, (void __user*)arg,
						sizeof( send_tone_ex_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_tone_volume_tx_ex(
					&chip->ae_state, send_tone_ex_t.tg_mod,
								0x0000 ) ;
			}
			break ;

		case SNDRV_SC1445x_SET_SIDETONE_VOL:
			if( copy_from_user( &sidetone_vol_t, (void __user*)arg,
						sizeof( sidetone_vol_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_sidetone_volume(
							&chip->ae_state,
							sidetone_vol_t.vol ) ;
			}
			break ;

		case SNDRV_SC1445x_GET_SIDETONE_VOL:
			res = sc1445x_ae_get_sidetone_volume( &chip->ae_state,
							&sidetone_vol_t.vol ) ;
			if( res )
				break ;  /* error */
			if( copy_to_user( (void __user*)arg, &sidetone_vol_t,
						sizeof( sidetone_vol_t ) ) )
				res = -EFAULT ;
			break ;

		case SNDRV_SC1445x_TURN_AEC_ON:
			res = sc1445x_ae_set_aec_on( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_TURN_AEC_OFF:
			res = sc1445x_ae_set_aec_off( &chip->ae_state ) ;
			break ;

#if defined( SC1445x_AE_COLLECT_STATISTICS )
		case SNDRV_SC1445x_GET_AUDIO_CHANNEL_STATS:
			if( copy_from_user( &audio_stats_t, (void __user*)arg,
						sizeof( audio_stats_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_channel_stats(
					&chip->ae_state,
					audio_stats_t.channel_index,
					&audio_stats_t.stats ) ;
				if( res )
					break ;  /* error */
				if( copy_to_user( (void __user*)arg,
						&audio_stats_t,
						sizeof( audio_stats_t ) ) )
					res = -EFAULT ;
			}
			
			break ;

		case SNDRV_SC1445x_RESET_AUDIO_STATS:
			{
				const unsigned short nch =
					chip->ae_state.audio_channels_count ;
				unsigned short ch ;

				for( ch = 0 ;  ch < nch ;  ++ch )
					sc1445x_ae_reset_channel_stats(
						&chip->ae_state, ch ) ;
			}
			break ;
#endif

		case SNDRV_SC1445x_GET_AUDIO_DRIVER_VERSION:
			strncpy( ae_version_t.id, SC1445x_AUDIO_DRIVER_VERSION,
					sizeof( ae_version_t.id ) - 1 ) ;
			ae_version_t.id[sizeof( ae_version_t.id ) - 1] = '\0' ;
			if( copy_to_user( (void __user*)arg,
						&ae_version_t,
						sizeof( ae_version_t ) ) )
				res = -EFAULT ;

			break ;

		case SNDRV_SC1445x_GET_PLATFORM_INFO:
			platform_info_t.type = 
#if defined( CONFIG_SC14452 ) && !defined( SC1445x_AE_USE_3_CHANNELS )
					SC14452_4_CHANNELS ;
#else
					SC14450_3_CHANNELS ;
#endif
			if( copy_to_user( (void __user*)arg,
						&platform_info_t,
						sizeof( platform_info_t ) ) )
				res = -EFAULT ;

			break ;

		case SNDRV_SC1445x_GET_DSP_FIRMWARE_VERSION:
			{
				char dsp_fw_ver_major = *dsp1_fw_ver >> 8 ;
				char dsp_fw_ver_minor = *dsp1_fw_ver ;

				snprintf( ae_version_t.id,
					sizeof ae_version_t.id, "%d.%d-%s",
					dsp_fw_ver_major, dsp_fw_ver_minor,
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
					"A/D"	/* ATA/DECT */
#else
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
					"PP"	/* Phone, PAEC */
#else
					"PA"	/* Phone, AEC */
#endif
#endif
					 ) ;
				if( copy_to_user( (void __user*)arg,
							&ae_version_t,
						sizeof( ae_version_t ) ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_GET_DSP2_FIRMWARE_VERSION:
			{
				char dsp_fw_ver_major = *dsp2_fw_ver >> 8 ;
				char dsp_fw_ver_minor = *dsp2_fw_ver ;

				snprintf( ae_version_t.id,
					sizeof ae_version_t.id, "%d.%d",
					dsp_fw_ver_major, dsp_fw_ver_minor ) ;
				if( copy_to_user( (void __user*)arg,
							&ae_version_t,
						sizeof( ae_version_t ) ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_SET_IFACE_MODE:
			if( copy_from_user( &if_mode_t, (void __user*)arg,
							sizeof( if_mode_t ) ) )
				res = -EFAULT ;
			else
				res = sc1445x_ae_set_iface_mode(
					&chip->ae_state, if_mode_t.mode ) ;
			break ;

		case SNDRV_SC1445x_GET_IFACE_MODE:
			res = sc1445x_ae_get_iface_mode( &chip->ae_state,
							&if_mode_t.mode ) ;
			if( res )
				break ;
			if( copy_to_user( (void __user*)arg, &if_mode_t,
							sizeof( if_mode_t ) ) )
				res = -EFAULT ;
			break ;

		case SNDRV_SC1445x_SET_VIRTUAL_SPK_VOL:
			if( copy_from_user( &vspk_vol_t, (void __user*)arg,
							sizeof( vspk_vol_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_vspk_volume(
					&chip->ae_state, vspk_vol_t.level ) ;
			}
			break ;

		case SNDRV_SC1445x_SET_VIRTUAL_SPK_DTMF_LVL:
			if( copy_from_user( &vspk_vol_t, (void __user*)arg,
							sizeof( vspk_vol_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_vspk_dtmf_level(
					&chip->ae_state, vspk_vol_t.level ) ;
			}
			break ;

		case SNDRV_SC1445x_GET_VIRTUAL_SPK_VOL:
			if( copy_from_user( &vspk_vol_t, (void __user*)arg,
							sizeof( vspk_vol_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_vspk_volume(
					&chip->ae_state, &vspk_vol_t.level ) ;
				if( res )
					break ;  /* error */
				if( copy_to_user( (void __user*)arg,
							&vspk_vol_t,
							sizeof( vspk_vol_t ) ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_GET_VIRTUAL_SPK_DTMF_LVL:
			if( copy_from_user( &vspk_vol_t, (void __user*)arg,
							sizeof( vspk_vol_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_vspk_dtmf_level(
					&chip->ae_state, &vspk_vol_t.level ) ;
				if( res )
					break ;  /* error */
				if( copy_to_user( (void __user*)arg,
							&vspk_vol_t,
							sizeof( vspk_vol_t ) ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_SET_VIRTUAL_MIC_GAIN:
			if( copy_from_user( &vmic_gain_t, (void __user*)arg,
						sizeof( vmic_gain_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_vmic_gain(
					&chip->ae_state, vmic_gain_t.level ) ;
			}
			break ;

		case SNDRV_SC1445x_GET_VIRTUAL_MIC_GAIN:
			if( copy_from_user( &vmic_gain_t, (void __user*)arg,
						sizeof( vmic_gain_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_vmic_gain(
					&chip->ae_state, &vmic_gain_t.level ) ;
				if( res )
					break ;  /* error */
				if( copy_to_user( (void __user*)arg,
						&vmic_gain_t,
						sizeof( vmic_gain_t ) ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_MUTE_VIRTUAL_MIC:
			res = sc1445x_ae_mute_vmic( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_UNMUTE_VIRTUAL_MIC:
			res = sc1445x_ae_unmute_vmic( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_GET_VOL_GAIN_LEVEL_COUNT:
			if( copy_from_user( &vol_gain_count_t,
						(void __user*)arg,
						sizeof( vol_gain_count_t ) ) )
				res = -EFAULT ;
			else {
				sc1445x_ae_get_vspk_vol_level_count(
					&chip->ae_state,
					&vol_gain_count_t.vol_level_count ) ;
				sc1445x_ae_get_vmic_gain_level_count(
					&chip->ae_state,
					&vol_gain_count_t.gain_level_count ) ;
				if( copy_to_user( (void __user*)arg,
						&vol_gain_count_t,
						sizeof( vol_gain_count_t ) ) )
					res = -EFAULT ;
			}
			break ;

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )

		case SNDRV_SC1445x_CONNECT_TO_LINE:
			if( copy_from_user( &conn_to_line_t, (void __user*)arg,
						sizeof( conn_to_line_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_connect_to_line(
					&chip->ae_state,
					conn_to_line_t.channel_index,
					conn_to_line_t.line ) ;
			}
			break ;

		case SNDRV_SC1445x_CONNECT_LINES:
			if( copy_from_user( &conn_lines_t, (void __user*)arg,
						sizeof( conn_lines_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_connect_lines(
					&chip->ae_state,
					conn_lines_t.line1,
					conn_lines_t.line2 ) ;
			}
			break ;

		case SNDRV_SC1445x_DISCONNECT_LINE:
			if( copy_from_user( &disconn_line_t, (void __user*)arg,
						sizeof( disconn_line_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_disconnect_line(
					&chip->ae_state,
					disconn_line_t.line ) ;
			}
			break ;

		case SNDRV_SC1445x_SET_CID_INFO:
			if( copy_from_user( &set_cid_info_t,
						(void __user*)arg,
						sizeof( set_cid_info_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_cid_info( &chip->ae_state,
					set_cid_info_t.line,
					set_cid_info_t.month,
					set_cid_info_t.day, set_cid_info_t.hour,
					set_cid_info_t.minutes,
					set_cid_info_t.number,
					set_cid_info_t.name ) ;
			}
			break ;

		case SNDRV_SC1445x_CID_IND_FIRST_RING:
			if( copy_from_user( &cid_ind_t, (void __user*)arg,
						sizeof( cid_ind_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_cid_ind_first_ring(
					&chip->ae_state, cid_ind_t.line ) ;
			}
			break ;

		case SNDRV_SC1445x_CID_IND_OFF_HOOK:
			if( copy_from_user( &cid_ind_t, (void __user*)arg,
						sizeof( cid_ind_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_cid_ind_off_hook(
					&chip->ae_state, cid_ind_t.line ) ;
			}
			break ;

		case SNDRV_SC1445x_CID_IND_ON_HOOK:
			if( copy_from_user( &cid_ind_t, (void __user*)arg,
						sizeof( cid_ind_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_cid_ind_on_hook(
					&chip->ae_state, cid_ind_t.line ) ;
			}
			break ;

		case SNDRV_SC1445x_CID_LINE_REVERSAL:
			if( copy_from_user( &cid_ind_t, (void __user*)arg,
						sizeof( cid_ind_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_cid_line_reversal(
					&chip->ae_state, cid_ind_t.line ) ;
			}
			break ;

#endif  /* SC1445x_AE_PCM_LINES_SUPPORT */

#if defined( SC1445x_AE_PCM_LINES_SUPPORT ) || defined( SC1445x_AE_PHONE_DECT )
		case SNDRV_SC1445x_SET_LINE_TYPE:
			if( copy_from_user( &line_type_t, (void __user*)arg,
						sizeof( line_type_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_line_type(
					&chip->ae_state, line_type_t.line,
							line_type_t.type ) ;
			}
			break ;

		case SNDRV_SC1445x_GET_LINE_TYPE:
			if( copy_from_user( &line_type_t, (void __user*)arg,
						sizeof( line_type_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_line_type(
					&chip->ae_state, line_type_t.line,
							&line_type_t.type ) ;
				if( res )
					break ;  /* error */
				if( copy_to_user( (void __user*)arg,
						&line_type_t,
						sizeof( line_type_t ) ) )
					res = -EFAULT ;
			}
			break ;
#endif

#if defined( SC1445x_AE_BT )
		case SNDRV_SC1445x_ATTACH_TO_PCM_DEV:
			if( copy_from_user( &attach_to_pcm_dev_t,
					(void __user*)arg,
					sizeof( attach_to_pcm_dev_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_attach_to_pcm( &chip->ae_state,
						attach_to_pcm_dev_t.slot,
						attach_to_pcm_dev_t.lep,
						attach_to_pcm_dev_t.freq,
						attach_to_pcm_dev_t.width ) ;
			}
			break ;

		case SNDRV_SC1445x_DETACH_FROM_PCM_DEV:
			if( copy_from_user( &detach_from_pcm_dev_t,
					(void __user*)arg,
					sizeof( detach_from_pcm_dev_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_detach_from_pcm(
							&chip->ae_state,
						detach_from_pcm_dev_t.slot ) ;
			}
			break ;
#endif

#if defined( SC1445x_AE_SUPPORT_FAX )

		case SNDRV_SC1445x_SWITCH_AUDIO_TO_FAX:
			if( copy_from_user( &audio2fax_t,
						(void __user*)arg,
						sizeof( audio2fax_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_switch_to_fax( &chip->ae_state,
						audio2fax_t.channel_index ) ;
			}
			break ;

		case SNDRV_SC1445x_SWITCH_FAX_TO_AUDIO:
			if( copy_from_user( &fax2audio_t,
						(void __user*)arg,
						sizeof( fax2audio_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_switch_to_audio(
						&chip->ae_state,
						fax2audio_t.channel_index ) ;
			}
			break ;

		case SNDRV_SC1445x_FAX_INIT:
			if( copy_from_user( &fax_init_params_t,
						(void __user*)arg,
						sizeof( fax_init_params_t ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_fax_init( &chip->ae_state,
						fax_init_params_t.line,
						fax_init_params_t.p0DBIN,
						fax_init_params_t.p0DBOUT,
						fax_init_params_t.pCEDLength,
						fax_init_params_t.pMDMCmd ) ;
			}
			break ;

#endif  /* SC1445x_AE_SUPPORT_FAX */

		case SNDRV_SC1445x_CONTROL_DSP_LOOPBACK:
			if( copy_from_user( &dsp_lb, (void __user*)arg,
							sizeof( dsp_lb ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_control_dsp_loopback(
						&chip->ae_state,
						dsp_lb.val ) ;
			}
			break ;

		case SNDRV_SC1445x_SET_VIRTUAL_SPK_VOL_EX:
			if( copy_from_user( &set_vspk_vol_ex, (void __user*)arg,
						sizeof( set_vspk_vol_ex ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_vspk_volume_ex(
					&chip->ae_state, set_vspk_vol_ex.mode,
					set_vspk_vol_ex.level ) ;
			}
			break ;

		case SNDRV_SC1445x_GET_VIRTUAL_SPK_VOL_EX:
			if( copy_from_user( &get_vspk_vol_ex, (void __user*)arg,
						sizeof( get_vspk_vol_ex ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_vspk_volume_ex(
					&chip->ae_state, get_vspk_vol_ex.mode,
					&get_vspk_vol_ex.min_level,
					&get_vspk_vol_ex.curr_level,
					&get_vspk_vol_ex.max_level ) ;
				if( res )
					break ;  /* error */
				if( copy_to_user( (void __user*)arg,
						&get_vspk_vol_ex,
						sizeof( get_vspk_vol_ex ) ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_SET_VIRTUAL_SPK_DTMF_LVL_EX:
			if( copy_from_user( &set_vspk_vol_ex, (void __user*)arg,
						sizeof( set_vspk_vol_ex ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_vspk_dtmf_level_ex(
					&chip->ae_state, set_vspk_vol_ex.mode,
					set_vspk_vol_ex.level ) ;
			}
			break ;

		case SNDRV_SC1445x_GET_VIRTUAL_SPK_DTMF_LVL_EX:
			if( copy_from_user( &get_vspk_vol_ex, (void __user*)arg,
						sizeof( get_vspk_vol_ex ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_vspk_dtmf_level_ex(
					&chip->ae_state, get_vspk_vol_ex.mode,
					&get_vspk_vol_ex.min_level,
					&get_vspk_vol_ex.curr_level,
					&get_vspk_vol_ex.max_level ) ;
				if( res )
					break ;  /* error */
				if( copy_to_user( (void __user*)arg,
						&get_vspk_vol_ex,
						sizeof( get_vspk_vol_ex ) ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_SET_VIRTUAL_MIC_GAIN_EX:
			if( copy_from_user( &set_vmic_gain_ex, (void __user*)arg,
						sizeof( set_vmic_gain_ex ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_set_vmic_gain_ex(
					&chip->ae_state, set_vmic_gain_ex.mode,
					set_vmic_gain_ex.level ) ;
			}
			break ;

		case SNDRV_SC1445x_GET_VIRTUAL_MIC_GAIN_EX:
			if( copy_from_user( &get_vmic_gain_ex, (void __user*)arg,
						sizeof( get_vmic_gain_ex ) ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_vmic_gain_ex(
					&chip->ae_state, get_vmic_gain_ex.mode,
					&get_vmic_gain_ex.min_level,
					&get_vmic_gain_ex.curr_level,
					&get_vmic_gain_ex.max_level ) ;
				if( res )
					break ;  /* error */
				if( copy_to_user( (void __user*)arg,
						&get_vmic_gain_ex,
						sizeof( get_vmic_gain_ex ) ) )
					res = -EFAULT ;
			}
			break ;

#if defined( USE_PCM_CHAR_DEVICE )
		case SNDRV_SC1445x_SET_RAW_PCM_PARAMS:
			if( READ_USER_ARG( set_raw_pcm_params, arg ) ) {
				res = -EFAULT ;
				break ;
			}

			res = sc1445x_pcm_setup( set_raw_pcm_params.rate,
					set_raw_pcm_params.sample_bits ) ;
			break ;

		case SNDRV_SC1445x_STOP_RAW_PCM:
			res = sc1445x_pcm_stop() ;
			break ;

		case SNDRV_SC1445x_ENABLE_DUAL_AUDIO_PATH:
			res = sc1445x_ae_enable_dual_audio_path(
							&chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_DISABLE_DUAL_AUDIO_PATH:
			res = sc1445x_ae_disable_dual_audio_path(
							&chip->ae_state ) ;
			break ;
#endif

		case SNDRV_SC1445x_START_TONE4:
			//tone4_t.is_key_tone = 0 ;
			if( READ_USER_ARG( tone4_t, arg ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				tone4_t.tg_mod = sc1445x_private_line2stream(
					&chip->ae_state, tone4_t.tg_mod, 1 ) ;
#endif
				if( tone4_t.is_key_tone )
					res = sc1445x_ae_start_key_tone4(
						&chip->ae_state,
						tone4_t.tg_mod,
						tone4_t.tone1, tone4_t.ampl1,
						tone4_t.tone2, tone4_t.ampl2,
						tone4_t.tone3, tone4_t.ampl3,
						tone4_t.tone4, tone4_t.ampl4,
						tone4_t.dur_on, tone4_t.dur_off,
						tone4_t.play_count ) ;
				else
					res = sc1445x_ae_start_tone4(
						&chip->ae_state,
						tone4_t.tg_mod,
						tone4_t.tone1, tone4_t.ampl1,
						tone4_t.tone2, tone4_t.ampl2,
						tone4_t.tone3, tone4_t.ampl3,
						tone4_t.tone4, tone4_t.ampl4,
						tone4_t.dur_on, tone4_t.dur_off,
						tone4_t.play_count ) ;
			}
			break ;

		case SNDRV_SC1445x_START_CUSTOM_TONE4:
			if( READ_USER_ARG( custom_tone4_t, arg ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				custom_tone4_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						custom_tone4_t.tg_mod, 1 ) ;
#endif
				res = sc1445x_ae_start_custom_tone4(
						&chip->ae_state,
						custom_tone4_t.tg_mod,
						&custom_tone4_t.params,
						custom_tone4_t.dur_on,
						custom_tone4_t.dur_off,
						custom_tone4_t.play_count ) ;
			}
			break ;

		case SNDRV_SC1445x_START_TONE4_SEQUENCE:
			//tone4_seq_t.is_ringtone = 0 ;
			if( READ_USER_ARG( tone4_seq_t, arg ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				tone4_seq_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						tone4_seq_t.tg_mod, 1 ) ;
#endif
				if( tone4_seq_t.is_ringtone )
					res =
					  sc1445x_ae_start_ringtone4_sequence(
						&chip->ae_state,
						tone4_seq_t.tg_mod,
						tone4_seq_t.seq_len,
						tone4_seq_t.seq,
						tone4_seq_t.repeat_seq,
						tone4_seq_t.play_count ) ;
				else if( tone4_seq_t.is_keytone )
					res =
					  sc1445x_ae_start_keytone4_sequence(
						&chip->ae_state,
						tone4_seq_t.tg_mod,
						tone4_seq_t.seq_len,
						tone4_seq_t.seq,
						tone4_seq_t.repeat_seq,
						tone4_seq_t.play_count ) ;
				else
					res = sc1445x_ae_start_tone4_sequence(
						&chip->ae_state,
						tone4_seq_t.tg_mod,
						tone4_seq_t.seq_len,
						tone4_seq_t.seq,
						tone4_seq_t.repeat_seq,
						tone4_seq_t.play_count ) ;
			}
			break ;

		case SNDRV_SC1445x_EXPAND_TONE4_SEQUENCE:
			if( READ_USER_ARG( tone4_seq_t, arg ) )
				res = -EFAULT ;
			else {
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
				tone4_seq_t.tg_mod =
					sc1445x_private_line2stream(
						&chip->ae_state,
						tone4_seq_t.tg_mod, 1 ) ;
#endif
				res = sc1445x_ae_expand_tone4_sequence(
						&chip->ae_state,
						tone4_seq_t.tg_mod,
						tone4_seq_t.seq_len,
						tone4_seq_t.seq ) ;
			}
			break ;

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		case SNDRV_SC1445x_SET_TONEGEN_TX_VOL:
			if( READ_USER_ARG( set_tg_tx_vol, arg ) )
				res = -EFAULT ;
			else
				res = sc1445x_ae_set_tonegen_tx_volume(
						&chip->ae_state,
						set_tg_tx_vol.tg_mod,
						set_tg_tx_vol.vol ) ;
			break ;

		case SNDRV_SC1445x_SET_TONEGEN_RX_VOL:
			if( READ_USER_ARG( set_tg_rx_vol, arg ) )
				res = -EFAULT ;
			else
				res = sc1445x_ae_set_tonegen_rx_volume(
						&chip->ae_state,
						set_tg_rx_vol.tg_mod,
						set_tg_rx_vol.vol ) ;
			break ;

		case SNDRV_SC1445x_GET_TONEGEN_TX_VOL:
			if( READ_USER_ARG( get_tg_tx_vol, arg ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_tonegen_tx_volume(
						&chip->ae_state,
						get_tg_tx_vol.tg_mod,
						&get_tg_tx_vol.vol ) ;
				if( res )
					break ;  /* error */
				if( WRITE_USER_ARG( arg, get_tg_tx_vol ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_GET_TONEGEN_RX_VOL:
			if( READ_USER_ARG( get_tg_rx_vol, arg ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_tonegen_rx_volume(
						&chip->ae_state,
						get_tg_rx_vol.tg_mod,
						&get_tg_rx_vol.vol ) ;
				if( res )
					break ;  /* error */
				if( WRITE_USER_ARG( arg, get_tg_rx_vol ) )
					res = -EFAULT ;
			}
			break ;
#endif

		case SNDRV_SC1445x_RESTART_AUDIO_ENGINE:
			PRINT( "Shutting down audio engine...\n" ) ;
			res = sc1445x_ae_finalize_engine( &chip->ae_state ) ;
			if( res ) {
				PRINT( "...FAILED -- need to reboot\n" ) ;
				break ;
			}

			PRINT( "Initializing audio engine...\n" ) ;
			res = sc1445x_ae_init_engine( &chip->ae_state ) ;
			if( res ) {
				PRINT( "...FAILED -- need to reboot\n" ) ;
				break ;
			}
			chip->ae_state.private_data = chip ;

			PRINT( "DONE!\n" ) ;
			break ;

		case SNDRV_SC1445x_REGISTER_TONE_CB_PID:
			if( READ_USER_ARG( rtcbp, arg ) )
				res = -EFAULT ;
			else
				res = sc1445x_ae_register_tone_callback(
						&chip->ae_state, rtcbp.cb_pid,
						rtcbp.cb_signo, rtcbp.cb_cmem ) ;
			break ;

		case SNDRV_SC1445x_SET_VSPK_PCM_LVL:
			if( READ_USER_ARG( vspk_vol_t, arg ) )
				res = -EFAULT ;
			else
				res = sc1445x_ae_set_vspk_pcm_level(
					&chip->ae_state, vspk_vol_t.level ) ;
			break ;

		case SNDRV_SC1445x_GET_VSPK_PCM_LVL:
			if( READ_USER_ARG( vspk_vol_t, arg ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_vspk_pcm_level(
					&chip->ae_state, &vspk_vol_t.level ) ;
				if( res )
					break ;  /* error */
				if( WRITE_USER_ARG( arg, vspk_vol_t ) )
					res = -EFAULT ;
			}
			break ;

		case SNDRV_SC1445x_SET_VSPK_PCM_LVL_EX:
			if( READ_USER_ARG( set_vspk_vol_ex, arg ) )
				res = -EFAULT ;
			else
				res = sc1445x_ae_set_vspk_pcm_lvl_ex(
					&chip->ae_state, set_vspk_vol_ex.mode,
					set_vspk_vol_ex.level ) ;
			break ;

		case SNDRV_SC1445x_GET_VSPK_PCM_LVL_EX:
			if( READ_USER_ARG( get_vspk_vol_ex, arg ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_get_vspk_pcm_lvl_ex(
					&chip->ae_state, get_vspk_vol_ex.mode,
					&get_vspk_vol_ex.min_level,
					&get_vspk_vol_ex.curr_level,
					&get_vspk_vol_ex.max_level ) ;
				if( res )
					break ;  /* error */
				if( WRITE_USER_ARG( arg, get_vspk_vol_ex ) )
					res = -EFAULT ;
			}
			break ;

#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
		case SNDRV_SC1445x_ENABLE_DTMF_PASSTHROUGH:
			if( READ_USER_ARG( l, arg ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_enable_dtmf_passthrough(
						&chip->ae_state, l.line ) ;
			}
			break ;

		case SNDRV_SC1445x_DISABLE_DTMF_PASSTHROUGH:
			if( READ_USER_ARG( l, arg ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_disable_dtmf_passthrough(
						&chip->ae_state, l.line ) ;
			}
			break ;
#endif

#if defined( SC1445x_AE_ATA_SUPPORT )
		case SNDRV_SC1445x_ENABLE_TONE_DET:
			if( READ_USER_ARG( td, arg ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_enable_tone_detection(
					&chip->ae_state, td.line, td.tones ) ;
			}
			break ;

		case SNDRV_SC1445x_DISABLE_TONE_DET:
			if( READ_USER_ARG( td, arg ) )
				res = -EFAULT ;
			else {
				res = sc1445x_ae_disable_tone_detection(
					&chip->ae_state, td.line, td.tones ) ;
			}
			break ;
#endif

		case SNDRV_SC1445x_RESET_AUDIO_PROFILE:
			res = sc1445x_ae_reset_audio_profile(
							&chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_GET_AUDIO_PROFILE_DYN_SZ:
			memset( &ap, 0, sizeof( ap ) ) ;
			res = sc1445x_ae_get_audio_profile_dyn_size(
					&chip->ae_state, &ap.data_nbytes ) ;
			if( res )
				break ;  /* error */
			if( WRITE_USER_ARG( arg, ap ) )
				res = -EFAULT ;

			break ;

		case SNDRV_SC1445x_GET_AUDIO_PROFILE:
			if( READ_USER_ARG( ap, arg ) ) {
				res = -EFAULT ;
				break ;
			}
			res = sc1445x_ae_get_audio_profile(
							&chip->ae_state, &ap ) ;
			if( res )
				break ;  /* error */
			res = copy_ap_to_user( arg, &ap ) ;

			break ;

		case SNDRV_SC1445x_SET_AUDIO_PROFILE:
#if 0
			if( READ_USER_ARG( ap, arg ) ) {
				res = -EFAULT ;
				break ;
			}
#else
			if( copy_ap_from_user( &ap, arg ) ) {
				res = -EFAULT ;
				break ;
			}
#endif
			res = sc1445x_ae_set_audio_profile(
							&chip->ae_state, &ap ) ;

			break ;

#if !defined( SC1445x_AE_PCM_LINES_SUPPORT )
		case SNDRV_SC1445x_ENABLE_MONITORING:
			res = sc1445x_ae_enable_monitoring( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_DISABLE_MONITORING:
			res = sc1445x_ae_disable_monitoring( &chip->ae_state ) ;
			break ;

		case SNDRV_SC1445x_ENABLE_EXTERNAL_CODEC:
			res = sc1445x_ae_enable_external_codec(
						&chip->ae_state, 1, 1 ) ;
			break ;

		case SNDRV_SC1445x_DISABLE_EXTERNAL_CODEC:
			res = sc1445x_ae_disable_external_codec(
						&chip->ae_state, 1, 1 ) ;
			break ;

		case SNDRV_SC1445x_ENABLE_EXTERNAL_CODEC_SPK:
			res = sc1445x_ae_enable_external_codec(
						&chip->ae_state, 0, 1 ) ;
			break ;

		case SNDRV_SC1445x_DISABLE_EXTERNAL_CODEC_SPK:
			res = sc1445x_ae_disable_external_codec(
						&chip->ae_state, 0, 1 ) ;
			break ;
#endif

		default:
			snd_printk( "%s: Unknown ioctl %08x (arg=%08lx)\n",
						__FUNCTION__, cmd, arg ) ;
			res = -ENOTTY ;
			break ;
	}

	return res ;
}


static int __init snd_sc1445x_probe( struct platform_device* devptr )
{
	int res ;
	struct snd_card* card ;
	struct snd_sc1445x* chip ;
	struct snd_hwdep* hw ;
	char dsp1_eng_ver[5], dsp2_eng_ver[5] ;

	card = snd_card_new( SNDRV_DEFAULT_IDX1, SNDRV_DEFAULT_STR1,
							THIS_MODULE, 0 ) ;
	if( NULL == card )
		return -ENOMEM ;

	res = snd_sc1445x_new( card, &chip ) ;
	if( res < 0 ) {
		snd_card_free( card ) ;
		return res ;
	}
	card->private_data = chip ;

	strncpy( card->driver, "SC1445x ALSA", sizeof( card->driver ) - 1 ) ;
	strncpy( card->shortname,
			"SC1445x Audio Engine " SC1445x_AUDIO_DRIVER_VERSION,
					sizeof( card->shortname ) - 1 ) ;
	if( *dsp1_fw_ver >= 0x135 )
		snprintf( dsp1_eng_ver, sizeof( dsp1_eng_ver ), ".%u",
						*dsp1_fw_eng_ver & 0xff ) ;
	else
		dsp1_eng_ver[0] = '\0' ;
	if( *dsp2_fw_ver >= 0x11b )
		snprintf( dsp2_eng_ver, sizeof( dsp2_eng_ver ), ".%u",
						*dsp2_fw_eng_ver_1_27 & 0xff ) ;
	else if( *dsp2_fw_ver >= 0x10a )
		snprintf( dsp2_eng_ver, sizeof( dsp2_eng_ver ), ".%u",
						*dsp2_fw_eng_ver & 0xff ) ;
	else
		dsp2_eng_ver[0] = '\0' ;
	snprintf( card->longname, sizeof( card->longname ),
			"%s (priv @%06x, DSP1 %u.%u%s-%s "
			"DSP2 %u.%u%s)\n",
			card->shortname, (unsigned)chip,
			*dsp1_fw_ver >> 8, *dsp1_fw_ver & 0xff,
			dsp1_eng_ver,
#if defined( SC1445x_AE_PCM_LINES_SUPPORT )
					"A/D",	/* ATA/DECT */
#else
#if defined( CONFIG_SND_SC1445x_USE_PAEC )
					"PP",	/* Phone, PAEC */
#else
					"PA",	/* Phone, AEC */
#endif
#endif
			*dsp2_fw_ver >> 8, *dsp2_fw_ver & 0xff,
			dsp2_eng_ver ) ;

	res = snd_hwdep_new( card, "SC1445x Audio Engine", 0, &chip->hw ) ;
	if( res < 0 ) {
		snd_card_free( card ) ;
		return res ;
	}
	hw = chip->hw ;
	strncpy( hw->name, "SC1445x ALSA hwdep", sizeof( hw->name ) - 1 ) ;
	hw->iface = SNDRV_HWDEP_IFACE_SC1445x ;
	hw->private_data = chip ;
	/* hwdep operators */
	hw->ops.open = snd_sc1445x_hwdep_open ;
	hw->ops.ioctl = snd_sc1445x_hwdep_ioctl ;
	hw->ops.release = snd_sc1445x_hwdep_release ;

	res = snd_sc1445x_pcm_new( chip ) ;
	if( res < 0 ) {
		snd_card_free( card ) ;
		return res ;
	}

	snd_card_set_dev( card, &devptr->dev ) ;
	res = snd_card_register( card ) ;
	if( res < 0 ) {
		snd_card_free( card ) ;
		return res ;
	}
	platform_set_drvdata( devptr, card ) ;

	return 0 ;
}


static int __devexit snd_sc1445x_remove( struct platform_device* devptr )
{
	snd_card_free( platform_get_drvdata( devptr ) ) ;
	platform_set_drvdata( devptr, NULL ) ;
	return 0 ;
}


static struct platform_driver snd_sc1445x_driver = {
	.probe		= snd_sc1445x_probe,
	.remove		= __devexit_p( snd_sc1445x_remove ),
	.driver		= {
		.name	= SND_SC1445x_DRIVER
	},
} ;


static int __init sc1445x_alsa_init( void )
{
	int res = platform_driver_register( &snd_sc1445x_driver ) ;
	if( res < 0 )
		return res ;
	snd_sc1445x_device =
			platform_device_register_simple( SND_SC1445x_DRIVER, -1,
								NULL, 0 ) ;

	snd_printd( "SC1445x ALSA driver initialized\n" ) ;

#if defined( SC1445x_AE_L_V2_V3_V4_V5_BOARD )
	P0_02_MODE_REG=0x300;
	P0_RESET_DATA_REG=(1<<2);
#endif

	return 0 ;
}


static void __exit sc1445x_alsa_exit( void )
{
#if defined( USE_PCM_CHAR_DEVICE )
	snd_sc1445x_unregister_pcm_cdev() ;
#endif
	if( !IS_ERR( snd_sc1445x_device ) )
		platform_device_unregister( snd_sc1445x_device ) ;
	platform_driver_unregister( &snd_sc1445x_driver ) ;

	snd_printd( "SC1445x ALSA driver exiting\n" ) ;
}


module_init( sc1445x_alsa_init )
module_exit( sc1445x_alsa_exit )

