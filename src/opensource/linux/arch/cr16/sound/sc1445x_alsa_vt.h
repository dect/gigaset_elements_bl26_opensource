/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

#if !defined( __SC1445x_ALSA_H )
#define __SC1445x_ALSA_H

#include <sound/driver.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include "audioengine/sc1445x_audioengine_defs_vt.h"


/*
 * SC1445x's private data for the ALSA card.
 */
struct snd_sc1445x {
	/* ALSA stuff */
	struct snd_card* card ;
	struct snd_pcm* pcm ;
	struct snd_pcm_substream* playback_substream ;
	struct snd_pcm_substream* capture_substream ;
	struct snd_hwdep* hw ;

	/* implementation stuff */
	short irq ;
	unsigned short started ;
	int playback_r_pos ;	/* read pos in playback buffer */
	int capture_w_pos ;	/* write pos in capture buffer */

	/* audio engine stuff */
	sc1445x_ae_state ae_state ;	/* the audioengine state */
	sc1445x_ae_mode mode ;		/* mode to use in the next prepare */
} ;


#endif  /* __SC1445x_ALSA_H */

