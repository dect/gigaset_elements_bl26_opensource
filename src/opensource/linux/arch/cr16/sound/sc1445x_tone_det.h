/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

#if !defined __SC1445x_TONE_DET_H
#define __SC1445x_TONE_DET_H


struct sc1445x_tone_det_event {
	/* event type */
	int event ;

	/*
	 * timestamp of tone-detection event, in msec
	 * but with jiffy (10msec) accuracy
	 */
	unsigned timestamp ;
} ;


/* MSB of event signifies start (0) or end (1) */
#define SC1445x_TONE_DET_EVENT_START		0x00000000
#define SC1445x_TONE_DET_EVENT_END		0x80000000

/* detectable tones */
#define SC1445x_TONE_DET_STUTTER_DIAL		0x00000001


#endif  /* __SC1445x_TONE_DET_H */

