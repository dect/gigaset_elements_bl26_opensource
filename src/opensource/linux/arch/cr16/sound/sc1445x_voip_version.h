/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

#if !defined( SC1445x_AUDIO_DRIVER_VERSION )

#  define SC1445x_AUDIO_DRIVER_VERSION_MAJOR	"1"
#  define SC1445x_AUDIO_DRIVER_VERSION_MINOR	"130"
#  define SC1445x_AUDIO_DRIVER_VERSION \
		SC1445x_AUDIO_DRIVER_VERSION_MAJOR "." \
			SC1445x_AUDIO_DRIVER_VERSION_MINOR

#endif
