/*
 *  linux/arch/cr16/kernel/time.c
 *
 * CR16 support by Peter Griffin <pgriffin@mpc-data.co.uk>
 * 
 * Based on
 *  Yoshinori Sato <ysato@users.sourceforge.jp>
 *
 *  Copied/hacked from:
 *
 *  linux/arch/m68k/kernel/time.c
 *
 *  Copyright (C) 1991, 1992, 1995  Linus Torvalds
 *
 * This file contains the cr16-specific time handling details.
 *
 * 1997-09-10	Updated NTP code according to technical memorandum Jan '96
 *		"A Kernel Model for Precision Timekeeping" by Dave Mills
 */

#include <linux/errno.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/param.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/timex.h>
#include <linux/profile.h>
#include <linux/jiffies.h>

#include <asm/io.h>
#include <asm/target_time.h>

#define	TICK_SIZE (tick_nsec / 1000)



/*
 * timer_interrupt() needs to keep up the real-time clock,
 * as well as call the "do_timer()" routine every clocktick
 */
irqreturn_t timer_interrupt(int irq, void *dummy)
{


	/* may need to kick the hardware timer */
	platform_timer_eoi();
	
	do_timer(1);


#ifdef CONFIG_SC14452_ES2
	rmii_patch();
#endif
	update_process_times(user_mode(get_irq_regs()));

#ifdef CONFIG_SC14452_ES2
	rmii_patch();
#endif

	profile_tick(CPU_PROFILING);

#ifdef CONFIG_SC14452_ES2
rmii_patch();
#endif

	return IRQ_HANDLED;
}

void time_init(void)
{
	unsigned int year, mon, day, hour, min, sec;

	platform_gettod (&year, &mon, &day, &hour, &min, &sec);
	xtime.tv_sec = 0;
	xtime.tv_nsec = 0;

	/*
	 * Initialize wall_to_monotonic such that adding it to xtime will yield zero, the
	 * tv_nsec field must be normalized (i.e., 0 <= nsec < NSEC_PER_SEC).
	 */
	set_normalized_timespec(&wall_to_monotonic, -xtime.tv_sec, -xtime.tv_nsec);

	platform_timer_setup(timer_interrupt);
}

#if !defined( CONFIG_GENERIC_TIME )

/*
 * This version of gettimeofday has near microsecond resolution.
 */
void do_gettimeofday(struct timeval *tv)
{
	unsigned long flags;
	unsigned long seq;
	unsigned long usec, sec;

#if 0
	read_lock_irqsave(&xtime_lock, flags);
	usec = 0;
	sec = xtime.tv_sec;
	usec += (xtime.tv_nsec / 1000);
	read_unlock_irqrestore(&xtime_lock, flags); 
#else
	do {
		seq = read_seqbegin_irqsave(&xtime_lock, flags);
		//usec = mach_gettimeoffset ? mach_gettimeoffset() : 0;
		usec = 0 ;
		sec = xtime.tv_sec;
		usec += (xtime.tv_nsec / 1000);
	} while (read_seqretry_irqrestore(&xtime_lock, seq, flags));
#endif

#if 1
	do {
#  define TEN_mSECS ( (unsigned short)0x2CFF )  /* Ten milliseconds */

		/* how much time has passed since the timer's last tick */
		unsigned short x = TEN_mSECS - TIMER1_RELOAD_N_REG ;
		unsigned short usec1, usec2 ;

		/*
		 * we need to calculate  (x / 0x2CFF) * 10000
		 * instead, we calculate the approximation  (7 * x / 8)
		 * which can be split as  ((3 * x) / 8)  +  (x / 2)
		 */
		usec1 = ( 3 * x ) >> 3 ;
		usec2 = x >> 2 ;
		usec += usec1 + usec2 ;
	} while( 0 ) ;
#endif

	while (usec >= 1000000) {
		usec -= 1000000;
		sec++;
	}

	tv->tv_sec = sec;
	tv->tv_usec = usec;
}

EXPORT_SYMBOL(do_gettimeofday);

int do_settimeofday(struct timespec *tv)
{
	time_t wtm_sec, sec = tv->tv_sec;
	long wtm_nsec, nsec = tv->tv_nsec;

	if ((unsigned long)tv->tv_nsec >= NSEC_PER_SEC)
		return -EINVAL;

	write_seqlock_irq(&xtime_lock);
	/* This is revolting. We need to set the xtime.tv_usec
	 * correctly. However, the value in this location is
	 * is value at the last tick.
	 * Discover what correction gettimeofday
	 * would have done, and then undo it!
	 */
#if 0
	while (tv->tv_nsec < 0) {
		tv->tv_nsec += NSEC_PER_SEC;
		tv->tv_sec--;
	}

	xtime.tv_sec = tv->tv_sec;
	xtime.tv_nsec = tv->tv_nsec;
#else
	wtm_sec  = wall_to_monotonic.tv_sec + (xtime.tv_sec - sec);
	wtm_nsec = wall_to_monotonic.tv_nsec + (xtime.tv_nsec - nsec);

	set_normalized_timespec(&xtime, sec, nsec);
	set_normalized_timespec(&wall_to_monotonic, wtm_sec, wtm_nsec);
#endif
	ntp_clear();
	write_sequnlock_irq(&xtime_lock);
	clock_was_set();
	return 0;
}

EXPORT_SYMBOL(do_settimeofday);

#endif  /* CONFIG_GENERIC_TIME */

unsigned long long sched_clock(void)
{
	return (unsigned long long)jiffies * (1000000000 / HZ);

}
