#include <linux/moduleloader.h>
#include <linux/elf.h>
#include <linux/vmalloc.h>
#include <linux/fs.h>
#include <linux/string.h>
#include <linux/kernel.h>

#if 0
#define DEBUGP printk
#else
#define DEBUGP(fmt...)
#endif

void *module_alloc(unsigned long size)
{
	if (size == 0)
		return NULL;
#if defined( CONFIG_RAM_SIZE_32MB )
	return kmalloc( size, GFP_KERNEL | GFP_DMA ) ;
#else
	return vmalloc(size);
#endif
}


/* Free memory returned from module_alloc */
void module_free(struct module *mod, void *module_region)
{
#if defined( CONFIG_RAM_SIZE_32MB )
	kfree( module_region ) ;
#else
	vfree(module_region);
#endif
	/* FIXME: If module_region == mod->init_region, trim exception
           table entries. */
}

/* We don't need anything special. */
int module_frob_arch_sections( Elf_Ehdr* hdr, Elf_Shdr* sechdrs,
					char* secstrings, struct module *mod )
{
	Elf32_Shdr* s, * sechdrs_end = sechdrs + hdr->e_shnum ;

	for( s = sechdrs ;  s < sechdrs_end ;  ++s ) {
		char* sec_name = secstrings + s->sh_name ;

		DEBUGP( "section '%s' has %sSHF_ALLOC set\n",
			sec_name, s->sh_flags & SHF_ALLOC ?  "" :  "not " ) ;

#if defined( CONFIG_LMX418x_DECT_SUPPORT ) \
				|| defined( CONFIG_LMX418x_DECT_SUPPORT_MODULE )
		/* don't allocate memory for certain sections */
		if( strcmp( sec_name, ".SC144xx_REGISTERS" ) == 0 ) {
			s->sh_flags &= ~SHF_ALLOC ;
			s->sh_addr = (unsigned)0xFF0000 ;
		} else if( strcmp( sec_name, ".BMCDATARAMSEG" ) == 0 ) {
			s->sh_flags &= ~SHF_ALLOC ;
			s->sh_addr = (unsigned)0x12000 ;
		}
#endif
	}

	return 0;
}

int apply_relocate(Elf32_Shdr *sechdrs,
		   const char *strtab,
		   unsigned int symindex,
		   unsigned int relsec,
		   struct module *me)
{
	printk(KERN_ERR "module %s: RELOCATION unsupported\n",
	       me->name);
	return -ENOEXEC;
}

int apply_relocate_add(Elf32_Shdr *sechdrs,
		       const char *strtab,
		       unsigned int symindex,
		       unsigned int relsec,
		       struct module *me)
{
	unsigned int i;
	Elf32_Rela *rela = (void *)sechdrs[relsec].sh_addr;
	extern char _start_rodata ;

	DEBUGP("Applying relocate section %u to %u\n", relsec,
						sechdrs[relsec].sh_info);
	for (i = 0; i < sechdrs[relsec].sh_size / sizeof(*rela); i++) {
		/* This is where to make the change */
		uint32_t *loc = (uint32_t *)(sechdrs[sechdrs[relsec].sh_info].sh_addr
					     + rela[i].r_offset);
		/* This is the symbol it is referring to.  Note that all
		   undefined symbols have been resolved.  */
		Elf32_Sym *sym = (Elf32_Sym *)sechdrs[symindex].sh_addr
			+ ELF32_R_SYM(rela[i].r_info);
		uint32_t v = sym->st_value + rela[i].r_addend;
		unsigned char* pp = (void*)loc ;
		uint32_t tmp, cur ;

		switch (ELF32_R_TYPE(rela[i].r_info)) {
		case R_CR16_IMM32a:
			if( sym->st_shndx )
				v >>= 1 ;	/* it's a local func pointer */
			loc = (void*)( (unsigned long)loc + 2 ) ;
#if 1
			v = ( v << 16 ) | ( v >> 16 ) ;
#else
			tmp = v << 16 ;
			tmp |= v >> 16 ;
			v = tmp ;
#endif
			DEBUGP( "R_CR16_IMM32a: loc=%p, v=%08x\n", loc, v ) ;
			*loc = v ;
			break ;

		case R_CR16_IMM32:
			loc = (void*)( (unsigned long)loc + 2 ) ;
			cur = *loc ;
			tmp = cur << 16 ;
			tmp |= cur >> 16 ;
			v += tmp ;
#if 1
			v = ( v << 16 ) | ( v >> 16 ) ;
#else
			tmp = v << 16 ;
			tmp |= v >> 16 ;
			v = tmp ;
#endif
			DEBUGP( "R_CR16_IMM32: loc=%p, v=%08x\n", loc, v ) ;
			*loc = v ;
			break ;

		case R_CR16_DISP24a:
			if( v < (uint32_t)&_start_rodata  ||  !sym->st_shndx )
				v <<= 1 ;	/* it's a function pointer */
			DEBUGP( "R_CR16_DISP24a: v=%08x\n", v ) ;
			v -= (unsigned long)loc ;
			DEBUGP( "R_CR16_DISP24a: loc=%p, v=%08x\n", loc, v ) ;

			pp[0] = ( (v & 0xff0000) >> 16 ) & 0xff ;
			pp[3] = (v >> 8) & 0xff ;
			pp[2] = v & 0xff ;
			break ;

		case R_CR16_ABS24:
			DEBUGP( "R_CR16_ABS24: loc=%p, v=%08x\n", loc, v ) ;
			{
				unsigned char* pp = (void*)loc ;

				tmp =	( (pp[2] & 0xf) << 20 ) |
					( (pp[3] & 0xf) << 16 ) |
					(  pp[5]        <<  8 ) |
					(  pp[4]	      ) ;

				v += tmp ;
				DEBUGP( "R_CR16_ABS24: v=%08x\n", v ) ;

				/* second word */
				pp[3] |= (v & 0xf0000) >> 16 ;

				pp[2] |= (v & 0xf00000) >> 20 ;

				pp[5] = (v >> 8) & 0xff ;
				pp[4] = v & 0xff ;
			}
			break ;

		case R_CR16_NUM32:
			DEBUGP( "R_CR16_NUM32: loc=%p, v=%08x\n", loc, v ) ;
			*loc = v ;
			break ;

		case R_CR16_NUM32a:
			if( ELF32_ST_TYPE( sym->st_info ) == STT_FUNC ) {
				DEBUGP( "R_CR16_NUM32a: STT_FUNC symbol\n" ) ;
				v >>= 1 ;	/* it's a function pointer */
			} else if( sechdrs[sym->st_shndx].sh_flags &
							SHF_EXECINSTR ) {
				DEBUGP( "R_CR16_NUM32a: reference to section "
						"with SHF_EXECINSTR\n" ) ;
				v >>= 1 ;	/* it refers to code */
			}
			DEBUGP( "R_CR16_NUM32a: loc=%p, v=%08x\n", loc, v ) ;
			*loc = v ;
			break ;

		case R_CR16_DISP8:
			DEBUGP( "R_CR16_DISP8: v=%08x\n", v ) ;
			v -= (unsigned long)loc ;
			DEBUGP( "R_CR16_DISP8: loc=%p, v=%08x\n", loc, v ) ;

			v >>= 1 ;

			pp[0] |= ( v &  0xf ) ;
			pp[1] |= ( v & 0xf0 ) >> 4 ;
			break ;

		case R_CR16_DISP16:
			DEBUGP( "R_CR16_DISP16: v=%08x\n", v ) ;
			v -= (unsigned long)loc ;
			DEBUGP( "R_CR16_DISP16: loc=%p, v=%08x\n", loc, v ) ;

			pp[3] = ( v >> 8 ) & 0xff;
			/*
			  Bit 0 needs to be set when branching backwards
			*/

			if( (int)v < 0 )
				pp[2] = ( v & 0xff ) | 0x1 ;
			else
				pp[2] = ( v & 0xff ) ;
			break ;

		case R_CR16_DISP24:
			DEBUGP( "R_CR16_DISP24: v=%08x\n", v ) ;
			v -= (unsigned long)loc ;
			DEBUGP( "R_CR16_DISP24: loc=%p, v=%08x\n", loc, v ) ;

			/* 19:16 */
			pp[3] |= ( (v &  0xf0000) >> 16 ) & 0xf ;
			/* 23:20 */
			pp[2] |= ( (v & 0xf00000) >> 20 ) & 0xf ;

			/* third word */
			pp[5] = (v >> 8) & 0xff ;
			pp[4] =  v       & 0xff ;
			break ;

		case R_CR16_NONE:
		case R_CR16_NUM8:
		case R_CR16_NUM16:
		case R_CR16_ABS20:
		case R_CR16_IMM4:
		case R_CR16_IMM8:
		case R_CR16_IMM16:
		case R_CR16_IMM20:
		case R_CR16_IMM24:
		case R_CR16_DISP4:
		case R_CR16_SWITCH8:
		case R_CR16_SWITCH16:
		case R_CR16_SWITCH32:
		case R_CR16_GLOB_DAT:
		case R_CR16_GOTC_REGREL20:
		case R_CR16_GOT_REGREL20:
			printk(KERN_ERR "module %s: Unexpected CR16 relocation:"
				" %u\n", me->name, ELF32_R_TYPE(rela[i].r_info));
			return -ENOEXEC;

		default:
			printk(KERN_ERR "module %s: Unknown relocation: %u\n",
			       me->name, ELF32_R_TYPE(rela[i].r_info));
			return -ENOEXEC;
		}
	}
	return 0;
}

int module_finalize(const Elf_Ehdr *hdr,
		    const Elf_Shdr *sechdrs,
		    struct module *me)
{
	return 0;
}

void module_arch_cleanup(struct module *mod)
{
}
