/*
 * linux/arch/cr16/boot/traps.c -- general exception handling code
 * CR16 support by Peter Griffin <pgriffin@mpc-data.co.uk>
 * 
 * Based on
 * H8/300 support Yoshinori Sato <ysato@users.sourceforge.jp>
 * 
 * Cloned from Linux/m68k.
 *
 * No original Copyright holder listed,
 * Probabily original (C) Roman Zippel (assigned DJD, 1999)
 *
 * Copyright 1999-2000 D. Jeff Dionne, <jeff@rt-control.com>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive
 * for more details.
 */

#include <linux/types.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/module.h>

#include <asm/system.h>
#include <asm/irq.h>
#include <asm/traps.h>
#include <asm/page.h>
#include <asm/gpio.h>

/*
 * this must be called very early as the kernel might
 * use some instruction that are emulated on the 060
 */

void __init base_trap_init(void)
{
}

void __init trap_init (void)
{
}

asmlinkage void set_esp0 (unsigned long ssp)
{
	current->thread.esp0 = ssp;
}

/*
 *	Generic dumping code. Used for panic and debug.
 */

static void dump(struct pt_regs *fp)
{
	unsigned long	*sp;
	unsigned char	*tp;
	int		i;

	printk("\nCURRENT PROCESS:\n\n");
	printk("COMM=%s PID=%d\n", current->comm, current->pid);
	if (current->mm) {
		printk("TEXT=%08x-%08x DATA=%08x-%08x BSS=%08x-%08x\n",
			(int) current->mm->start_code,
			(int) current->mm->end_code,
			(int) current->mm->start_data,
			(int) current->mm->end_data,
			(int) current->mm->end_data,
			(int) current->mm->brk);
		printk("USER-STACK=%08x  KERNEL-STACK=%08lx\n\n",
			(int) current->mm->start_stack,
			(int) PAGE_SIZE+(unsigned long)current);
	}

	show_regs(fp);
	printk("\nCODE:");
	tp = ((unsigned char *) fp->pc) - 0x20;
	for (sp = (unsigned long *) tp, i = 0; (i < 0x40);  i += 4) {
		if ((i % 0x10) == 0)
			printk("\n%08x: ", (int) (tp + i));
		printk("%08x ", (int) *sp++);
	}
	printk("\n");

	printk("\nKERNEL STACK:");
	tp = ((unsigned char *) fp) - 0x40;
	for (sp = (unsigned long *) tp, i = 0; (i < 0xc0); i += 4) {
		if ((i % 0x10) == 0)
			printk("\n%08x: ", (int) (tp + i));
		printk("%08x ", (int) *sp++);
	}
	printk("\n");
	if (STACK_MAGIC != *(unsigned long *)((unsigned long)current+PAGE_SIZE))
                printk("(Possibly corrupted stack page??)\n");

	printk("\n\n");
}

void die_if_kernel (char *str, struct pt_regs *fp, int nr)
{
	extern int console_loglevel;

	if (!(fp->psr & PS_S))
		return;

	console_loglevel = 15;
	dump(fp);

	do_exit(SIGSEGV);
}

extern char _start, _etext;
#define check_kernel_text(addr) \
        ((addr >= (unsigned long)(&_start)) && \
         (addr <  (unsigned long)(&_etext))) 

#define check_user_text(task, addr) \
        ((addr >= (unsigned long)(task->mm->start_code)) && \
         (addr <  (unsigned long)(task->mm->end_code))) 

static int kstack_depth_to_print = 24;

void show_stack(struct task_struct *task, unsigned long *esp)
{
	unsigned long *stack,  addr, lim_stack;
	int i;

	if (esp == NULL)
		esp = (unsigned long *) &esp;

	stack = esp;
	lim_stack = ( (unsigned long)stack & ~(THREAD_SIZE - 1) ) + THREAD_SIZE;
	printk("Stack from %08lx:", (unsigned long)stack);
	for (i = 0; i < kstack_depth_to_print; i++) {
		if (((unsigned long)stack > lim_stack))
			break;
		if (i % 8 == 0)
			printk("\n       ");
		printk(" %08lx", *stack++);
	}

	printk("\nCall Trace (start=%p, etext=%p):", &_start, &_etext);
	i = 0;
	stack = esp;
	while( (unsigned long)stack < lim_stack ) {
		addr = *stack++;
		addr <<= 1 ;	/* func ptrs are stored right-shifted by 1 */
		/*
		 * If the address is either in the text segment of the
		 * kernel, or in the region which contains vmalloc'ed
		 * memory, it *may* be the address of a calling
		 * routine; if so, print it so that someone tracing
		 * down the cause of the crash will be able to figure
		 * out the call path that was taken.
		 */
		if (check_kernel_text(addr)) {
			if (i % 4 == 0)
				printk("\n       ");
			printk(" [<%08lx>]", addr);
			i++;
		}
	}
	printk("\n");

	/* Repeat for USP */
	printk( "\n" ) ;
	stack = (unsigned long*)rdusp() ;
	lim_stack = ( (unsigned long)stack & ~(THREAD_SIZE - 1) ) + THREAD_SIZE;
	printk("User Stack from %08lx:", (unsigned long)stack);
	for (i = 0; i < kstack_depth_to_print; i++) {
		if (((unsigned long)stack > lim_stack))
			break;
		if (i % 8 == 0)
			printk("\n       ");
		printk(" %08lx", *stack++);
	}

	printk( "\nCall Trace (code start=%lx, code end=%lx):",
		task->mm->start_code, task->mm->end_code ) ;
	i = 0;
	stack = (unsigned long*)rdusp();
	while( (unsigned long)stack < lim_stack ) {
		addr = *stack++;
		addr <<= 1 ;	/* func ptrs are stored right-shifted by 1 */
		/*
		 * If the address is either in the text segment of the
		 * kernel, or in the region which contains vmalloc'ed
		 * memory, it *may* be the address of a calling
		 * routine; if so, print it so that someone tracing
		 * down the cause of the crash will be able to figure
		 * out the call path that was taken.
		 */
		if (check_user_text(task, addr)) {
			if (i % 4 == 0)
				printk("\n       ");
			printk(" [<%08lx>]", addr);
			i++;
		}
	}
	printk("\n");

	/* show information about current process */
	printk( "\nCurrent process: task @%p  thread_info @%p\n"
		"PID %d (%s)\n", task, task->thread_info,
						task->pid, task->comm ) ;
	if( task->parent )
		printk( "Parent PID %d (%s)\n",
				task->parent->pid, task->parent->comm ) ;
	else
		printk( "No parent\n" ) ;
	printk( "\n" ) ;

	printk( "Code:  start=%08lx  end=%08lx\n",
				task->mm->start_code, task->mm->end_code ) ;
	printk( "Data:  start=%08lx  end=%08lx\n",
				task->mm->start_data, task->mm->end_data ) ;
	printk( "Stack: start=%08lx\n"
		"         USP=%p\n",
			task->mm->start_stack, (unsigned long*)rdusp() ) ;
	printk( "brk:   start=%08lx\n",
				task->mm->start_brk ) ;
	printk( "\n" ) ;

	printk( "     Blocked signal mask: " ) ;
	for( i = _NSIG_WORDS - 1 ;  i >= 0 ;  --i )
		printk( "%08lx ", task->blocked.sig[i] ) ;
	printk( "\n" ) ;
	printk( "Blocked real signal mask: " ) ;
	for( i = _NSIG_WORDS - 1 ;  i >= 0 ;  --i )
		printk( "%08lx ", task->real_blocked.sig[i] ) ;
	printk( "\n" ) ;
	printk( "    Pending signals mask: " ) ;
	for( i = _NSIG_WORDS - 1 ;  i >= 0 ;  --i )
		printk( "%08lx ", task->pending.signal.sig[i] ) ;
	printk( "\n" ) ;
}

void show_trace_task(struct task_struct *tsk)
{
	/* copy SP to esp0 */
	__asm( "push $2, r2\n" ) ;
	__asm( "movd (sp), (r3, r2)\n" ) ;
	__asm( "bal (ra), _set_esp0\n" ) ;
	__asm( "pop $2, r2\n" ) ;
	show_stack(tsk,(unsigned long *)tsk->thread.esp0);
}

void dump_stack(void)
{
	unsigned long stack ;

	show_stack( current, &stack ) ;
}

EXPORT_SYMBOL(dump_stack);
