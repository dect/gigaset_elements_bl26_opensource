/*
 *  early_printk.c
 *
 *  Early printk console support using SC14450 on-chip UART
 *
 *  Author:     Bennie Affleck
 *  Created:    16th October 2007
 *  Tabs:       4 spaces
 *  Copyright:  MPC Data LTD
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 */

#include <linux/console.h>
#include <linux/tty.h>
#include <linux/init.h>
#include <asm/io.h>
#include <linux/serial_core.h>
#include <asm/regs.h>



static void sitel_sercon_putc(int c)
{
	/* always transmit a CR before each NL */
	if (c == '\n')
		sitel_sercon_putc('\r');

	/* clear down interrupt condition (empty buffer) */
	while ( UART_CTRL_REG & UART_CTRL_TI ) {
		UART_CLEAR_TX_INT_REG = 0;
	}

	/* send character to TX register */
	UART_RX_TX_REG = c;

	/* wait until byte has been transmitted */
	while ( !(UART_CTRL_REG & UART_CTRL_TI) );
}

static void sitel_sercon_write(struct console *con, const char *s,
			      unsigned count)
{
	while (count-- > 0)
		sitel_sercon_putc(*s++);
}

static int __init sitel_sercon_setup(struct console *con, char *options)
{
	con->cflag = CREAD | HUPCL | CLOCAL | B115200 | CS8;

	return 0;
}

static struct console sitel_console = {
	.name		= "sercon",
	.write		= sitel_sercon_write,
	.setup		= sitel_sercon_setup,
	.flags		= CON_PRINTBUFFER,
	.index		= -1,
};

static void sitel_sercon_init(int baud)
{
	/* Connect RS232 pins to UART */
/* UART RX/TX pins are already configured by boot-loader */
#if !defined(CONFIG_SC14452_REEF_BS_BOARD)
#if defined( CONFIG_G_MERKUR_BOARDS )
	P2_08_MODE_REG
#elif defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	P2_10_MODE_REG
#else
	P0_00_MODE_REG
#endif
		= GPIO_PID_UTX | GPIO_PUPD_OUT_NONE;

#if defined( CONFIG_G_MERKUR_BOARDS )
	P2_09_MODE_REG
#elif defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
	P2_09_MODE_REG
#else
	P0_01_MODE_REG
#endif
#if (defined(CONFIG_L_V2_BOARD)||defined(CONFIG_L_V1_BOARD)||defined(CONFIG_L_V4_BOARD)||defined(CONFIG_L_V3_BOARD)||defined(CONFIG_L_V5_BOARD) \
	||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX))
		= GPIO_PID_URX | GPIO_PUPD_IN_PULLUP; /* 2010.11.09 by jkkim */
#else
		= GPIO_PID_URX | GPIO_PUPD_IN_PULLUP;
#endif
#endif /*#if !defined(CONFIG_SC14452_REEF_BS_BOARD)*/	
	/* Configure the serial port.  We can only change the baud rate - all
	   other values are fixed: 1 start bit, 8 data bits, 1 stop bit. */
	UART_CTRL_REG = UART_CTRL_BAUDRATE_115K2 | UART_CTRL_TEN | UART_CTRL_REN;
}

/*
 * Setup a default console, if more than one is compiled in, rely on the
 * earlyprintk= parsing to give priority.
 */
static struct console *early_console = &sitel_console;

static int __initdata keep_early;

int __init setup_early_printk(char *opt)
{
	char *space;
	char buf[256];

	strlcpy(buf, opt, sizeof(buf));
	space = strchr(buf, ' ');
	if (space)
		*space = 0;

	if (strstr(buf, "keep"))
		keep_early = 1;

	if (!strncmp(buf, "serial", 6)) {
		early_console = &sitel_console;
		sitel_sercon_init(115200);
	}

	if (likely(early_console))
		register_console(early_console);

	return 1;
}

__setup("earlyprintk=", setup_early_printk);

void __init disable_early_printk(void)
{
	if (!keep_early) {
		printk("disabling early console\n");
		unregister_console(early_console);
	} else
		printk("keeping early console\n");
}
