#include <linux/module.h>
#include <linux/linkage.h>
#include <linux/sched.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/user.h>
#include <linux/elfcore.h>
#include <linux/in6.h>
#include <linux/interrupt.h>

#include <asm/setup.h>
#include <asm/pgalloc.h>
#include <asm/irq.h>
#include <asm/io.h>
#include <asm/semaphore.h>
#include <asm/checksum.h>
#include <asm/current.h>
#include <asm/gpio.h>


extern char cr16_debug_device[];

extern unsigned long dram_end ;
EXPORT_SYMBOL( dram_end ) ;

#if defined(CONFIG_RAM_SIZE_32MB)
extern unsigned long ram_hole_start ;
extern unsigned long ram_hole_len ;
EXPORT_SYMBOL( ram_hole_start ) ;
EXPORT_SYMBOL( ram_hole_len ) ;
#endif



/* platform dependent support */

EXPORT_SYMBOL(strnlen);
EXPORT_SYMBOL(strrchr);
EXPORT_SYMBOL(strstr);
EXPORT_SYMBOL(strchr);
EXPORT_SYMBOL(strcat);
EXPORT_SYMBOL(strlen);
EXPORT_SYMBOL(strcmp);
EXPORT_SYMBOL(strncmp);

EXPORT_SYMBOL(ip_fast_csum);

EXPORT_SYMBOL(kernel_thread);
EXPORT_SYMBOL(enable_irq);
EXPORT_SYMBOL(disable_irq);

/* Networking helper routines. */
EXPORT_SYMBOL(csum_partial_copy);

/* The following are special because they're not called
   explicitly (the C compiler generates them).  Fortunately,
   their interface isn't gonna change any time soon now, so
   it's OK to leave it out of version control.  */
EXPORT_SYMBOL(memcpy);
EXPORT_SYMBOL(memset);
EXPORT_SYMBOL(memcmp);
EXPORT_SYMBOL(memscan);
EXPORT_SYMBOL(memmove);

/*
 * libgcc functions - functions that are used internally by the
 * compiler...  (prototypes are not correct though, but that
 * doesn't really matter since they're not versioned).
 */
extern void __gcc_bcmp(void);
extern void __ashldi3(void);
extern void __ashrdi3(void);
extern void __cmpdi2(void);
extern void __divdi3(void);
extern void __divsi3(void);
extern void __lshrdi3(void);
extern void __moddi3(void);
extern void __modsi3(void);
extern void __muldi3(void);
extern void __mulsi3(void);
extern void __negdi2(void);
extern void __ucmpdi2(void);
extern void __udivdi3(void);
extern void __udivmoddi4(void);
extern void __udivsi3(void);
extern void __umoddi3(void);
extern void __umodsi3(void);
extern void __umodhi3(void);
extern void __udivhi3(void);
extern void __fixunssfsi(void);
extern void __divsf3(void);
extern void __floatsisf(void);
extern void __mulsf3(void);
#if 0
extern void __muldf3(void);
extern void __truncdfsf2(void);
extern void __extendsfdf2(void);
extern void __divdf3(void);
#endif

        /* gcc lib functions */
EXPORT_SYMBOL(__gcc_bcmp);
EXPORT_SYMBOL(__ashldi3);
EXPORT_SYMBOL(__ashrdi3);
EXPORT_SYMBOL(__cmpdi2);
EXPORT_SYMBOL(__divdi3);
EXPORT_SYMBOL(__divsi3);
EXPORT_SYMBOL(__lshrdi3);
EXPORT_SYMBOL(__moddi3);
EXPORT_SYMBOL(__modsi3);
EXPORT_SYMBOL(__muldi3);
EXPORT_SYMBOL(__mulsi3);
EXPORT_SYMBOL(__negdi2);
EXPORT_SYMBOL(__ucmpdi2);
EXPORT_SYMBOL(__udivdi3);
EXPORT_SYMBOL(__udivmoddi4);
EXPORT_SYMBOL(__udivsi3);
EXPORT_SYMBOL(__umoddi3);
EXPORT_SYMBOL(__umodsi3);
EXPORT_SYMBOL(__umodhi3);
EXPORT_SYMBOL(__udivhi3);
EXPORT_SYMBOL(__fixunssfsi);
EXPORT_SYMBOL(__divsf3);
EXPORT_SYMBOL(__floatsisf);
EXPORT_SYMBOL(__mulsf3);
#if 0
EXPORT_SYMBOL(__muldf3);
EXPORT_SYMBOL(__truncdfsf2);
EXPORT_SYMBOL(__extendsfdf2);
EXPORT_SYMBOL(__divdf3);
#endif


#if defined(CONFIG_SC1445x_DHRYSTONE_MODULE)
/* system call routines used by Dhrystone 'driver' */
extern void sys_gettimeofday( void ) ;
extern void sys_time( void ) ;

EXPORT_SYMBOL( sys_gettimeofday ) ;
EXPORT_SYMBOL( sys_time ) ;
#endif

