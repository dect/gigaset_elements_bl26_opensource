/*
 * linux/arch/cr16/kernel/signal.c
 *
 * Copyright (C) 1991, 1992  Linus Torvalds
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive
 * for more details.
 *
 * CR16 support by Peter Griffin <pgriffin@mpc-data.co.uk>
 * 
 * Based on
 * Linux/H8300 By Yoshinori Sato and David McCullough
 *
 * (Which was)
 *
 * Based on
 * Linux/m68k by Hamish Macdonald
 */

/*
 * ++roman (07/09/96): implemented signal stacks (specially for tosemu on
 * Atari :-) Current limitation: Only one sigstack can be active at one time.
 * If a second signal with SA_ONSTACK set arrives while working on a sigstack,
 * SA_ONSTACK is ignored. This behaviour avoids lots of trouble with nested
 * signal handlers!
 */

#include <linux/sched.h>
#include <linux/mm.h>
#include <linux/kernel.h>
#include <linux/signal.h>
#include <linux/syscalls.h>
#include <linux/errno.h>
#include <linux/wait.h>
#include <linux/ptrace.h>
#include <linux/unistd.h>
#include <linux/stddef.h>
#include <linux/highuid.h>
#include <linux/personality.h>
#include <linux/tty.h>
#include <linux/binfmts.h>
#include <linux/suspend.h>

#include <asm/setup.h>
#include <asm/uaccess.h>
#include <asm/pgtable.h>
#include <asm/traps.h>
#include <asm/ucontext.h>
#include <asm/thread_info.h>
#include <asm/cacheflush.h>



#define _BLOCKABLE (~(sigmask(SIGKILL) | sigmask(SIGSTOP)))

asmlinkage int do_signal(struct pt_regs *regs, sigset_t *oldset);

#define DEBUG_SIG 0
/*
 * Atomically swap in the new signal mask, and wait for a signal.
 */
asmlinkage int do_sigsuspend(struct pt_regs *regs)
{
	old_sigset_t mask = regs->r6and7;
	sigset_t saveset;
#if DEBUG_SIG
	printk("do_sigsuspend()+\n");
#endif
	mask &= _BLOCKABLE;
	spin_lock_irq(&current->sighand->siglock);
	saveset = current->blocked;
	siginitset(&current->blocked, mask);
	recalc_sigpending();
	spin_unlock_irq(&current->sighand->siglock);

	regs->r0and1 = -EINTR;
	while (1) {
		current->state = TASK_INTERRUPTIBLE;
		schedule();
		if (do_signal(regs, &saveset))
			return -EINTR;
	}
}

asmlinkage int
do_rt_sigsuspend(struct pt_regs *regs)
{
	sigset_t *unewset = (sigset_t *)regs->r2and3;
	size_t sigsetsize = (size_t)regs->r4and5;
	sigset_t saveset, newset;
#if DEBUG_SIG
	printk("do_rt_sigsuspend()+\n");
#endif
	/* XXX: Don't preclude handling different sized sigset_t's.  */
	if (sigsetsize != sizeof(sigset_t))
		return -EINVAL;

	if (copy_from_user(&newset, unewset, sizeof(newset)))
		return -EFAULT;
	sigdelsetmask(&newset, ~_BLOCKABLE);

	spin_lock_irq(&current->sighand->siglock);
	saveset = current->blocked;
	current->blocked = newset;
	recalc_sigpending();
	spin_unlock_irq(&current->sighand->siglock);

	regs->r0and1 = -EINTR;
	while (1) {
		current->state = TASK_INTERRUPTIBLE;
		schedule();
		if (do_signal(regs, &saveset))
			return -EINTR;
	}
}

asmlinkage int 
sys_sigaction(int sig, const struct old_sigaction *act,
	      struct old_sigaction *oact)
{
	struct k_sigaction new_ka, old_ka;
	int ret;
#if DEBUG_SIG
	printk("sys_sigaction()+\n");
#endif

	if (act) {
		old_sigset_t mask;
		if (!access_ok(VERIFY_READ, act, sizeof(*act)) ||
		    __get_user(new_ka.sa.sa_handler, &act->sa_handler) ||
		    __get_user(new_ka.sa.sa_restorer, &act->sa_restorer))
			return -EFAULT;
		__get_user(new_ka.sa.sa_flags, &act->sa_flags);
		__get_user(mask, &act->sa_mask);
		siginitset(&new_ka.sa.sa_mask, mask);
	}

	ret = do_sigaction(sig, act ? &new_ka : NULL, oact ? &old_ka : NULL);

	if (!ret && oact) {
		if (!access_ok(VERIFY_WRITE, oact, sizeof(*oact)) ||
		    __put_user(old_ka.sa.sa_handler, &oact->sa_handler) ||
		    __put_user(old_ka.sa.sa_restorer, &oact->sa_restorer))
			return -EFAULT;
		__put_user(old_ka.sa.sa_flags, &oact->sa_flags);
		__put_user(old_ka.sa.sa_mask.sig[0], &oact->sa_mask);
	}

	return ret;
}

asmlinkage int sys_sigaltstack(const stack_t *uss, stack_t *uoss)
{
	return do_sigaltstack(uss, uoss, rdusp());
}

/*
 * Do a signal return; undo the signal stack.
 *
 * Keep the return code on the stack quadword aligned!
 * That makes the cache flush below easier.
 */

struct sigframe
{
	char *pretcode;
	unsigned char retcode[8];
	unsigned long extramask[_NSIG_WORDS-1];
	struct sigcontext sc;
	int sig;
} __attribute__((aligned(2),packed));

struct rt_sigframe
{
        /*3rd param to sig handler*/
        unsigned int paramthree;
	char *pretcode;
	struct siginfo *pinfo;
	void *puc;
	unsigned char retcode[8];
	struct siginfo info;
	struct ucontext uc;
	int sig;
} __attribute__((aligned(2),packed));

static inline int
restore_sigcontext(struct pt_regs *regs, struct sigcontext *usc,
		   int *pd0)
{
	int err = 0;
	unsigned int psr;
	unsigned int usp;
	unsigned int r0and1;
#if DEBUG_SIG
	printk("restore_sigcontext()+\n");
#endif

	/* Always make any pending restarted system calls return -EINTR */
	current_thread_info()->restart_block.fn = do_no_restart_syscall;

#define COPY(r) err |= __get_user(regs->r, &usc->sc_##r)    /* restore passed registers */
	COPY(r0and1);
	COPY(r2and3);
	COPY(r4and5);
	COPY(r6and7);
	COPY(r8and9);
	COPY(r10and11);
	COPY(r12);
	COPY(r13);
	COPY(pc);
	COPY(ra);

	psr = regs->psr & 0x08;
	COPY(psr);
#undef COPY
#if DEBUG_SIG
	printk("\n\n restore sigcontext\n");
	show_regs(regs);
	printk("\n\n");
	printk("disable syscall checks\n");
#endif
	regs->orig_r0and1 = -1;		/* disable syscall checks */

	err |= __get_user(usp, &usc->sc_usp);
#if DEBUG_SIG
	printk("new usp = 0x%x\n",usp);
#endif
	wrusp(usp);

	err |= __get_user(r0and1, &usc->sc_r0and1);
#if DEBUG_SIG
	printk("r0and1 = 0x%x\n",r0and1);
#endif
	*pd0 = r0and1;
	return err;
}
asmlinkage int do_sigreturn(struct pt_regs *regs)
{
	unsigned long usp = rdusp();
	struct sigframe *frame = (struct sigframe *)(usp);
	sigset_t set;
	int r0and1;
#if DEBUG_SIG
	printk("\n\ndo_sigreturn()+ - frame = [0x%x]\n",frame);
#endif

	if (!access_ok(VERIFY_READ, frame, sizeof(*frame)))
		goto badframe;
	if (__get_user(set.sig[0], &frame->sc.sc_mask) ||
	    (_NSIG_WORDS > 1 &&
	     __copy_from_user(&set.sig[1], &frame->extramask,
			      sizeof(frame->extramask))))
		goto badframe;

	sigdelsetmask(&set, ~_BLOCKABLE);
	spin_lock_irq(&current->sighand->siglock);
	current->blocked = set;
	recalc_sigpending();
	spin_unlock_irq(&current->sighand->siglock);
#if DEBUG_SIG	
	printk("regs = 0x%x, frame->sc = 0x%x, r0and1 = 0x%x\n",
	       regs, &frame->sc, r0and1);
#endif

	if (restore_sigcontext(regs, &frame->sc, &r0and1))
		goto badframe;
	return r0and1;

badframe:
	force_sig(SIGSEGV, current);
	return 0;
}

asmlinkage int do_rt_sigreturn(struct pt_regs *regs)
{
	unsigned long usp = rdusp();
	struct rt_sigframe *frame = (struct rt_sigframe *)(usp);
	sigset_t set;
	int r0and1;

#if DEBUG_SIG
	printk("do_rt_sigreturn()+\n");
#endif

	if (!access_ok(VERIFY_READ, frame, sizeof(*frame)))
		goto badframe;
	if (__copy_from_user(&set, &frame->uc.uc_sigmask, sizeof(set)))
		goto badframe;

	sigdelsetmask(&set, ~_BLOCKABLE);
	spin_unlock_irq(&current->sighand->siglock);
	current->blocked = set;
	recalc_sigpending();
	spin_lock_irq(&current->sighand->siglock);
	
	if (restore_sigcontext(regs, &frame->uc.uc_mcontext, &r0and1))
		goto badframe;

	if (do_sigaltstack(&frame->uc.uc_stack, NULL, usp) == -EFAULT)
		goto badframe;

	return r0and1;

badframe:
	force_sig(SIGSEGV, current);
	return 0;
}

static int setup_sigcontext(struct sigcontext __user *sc, struct pt_regs *regs,
			     unsigned long mask)
{
	int err = 0;
#if DEBUG_SIG
	printk("setup_sigcontext()+\n");
#endif

	err |= __put_user(regs->r0and1,   &sc->sc_r0and1);
	err |= __put_user(regs->r2and3,   &sc->sc_r2and3);
	err |= __put_user(regs->r4and5,   &sc->sc_r4and5);
	err |= __put_user(regs->r6and7,   &sc->sc_r6and7);
	err |= __put_user(regs->r8and9,   &sc->sc_r8and9);
	err |= __put_user(regs->r10and11, &sc->sc_r10and11);
	err |= __put_user(regs->r12,      &sc->sc_r12);
	err |= __put_user(regs->r13,      &sc->sc_r13);
	err |= __put_user(rdusp(),        &sc->sc_usp);
	err |= __put_user(regs->pc,       &sc->sc_pc);
	err |= __put_user(regs->ra,      &sc->sc_ra);
	err |= __put_user(regs->psr,      &sc->sc_psr);
	err |= __put_user(mask,           &sc->sc_mask);
#if DEBUG_SIG
	printk("\n\n save sigcontext\n");
	show_regs(regs);
	printk("\n\n");
#endif

	return err;
}

static inline void *
get_sigframe(struct k_sigaction *ka, struct pt_regs *regs, size_t frame_size)
{
	unsigned long usp;
#if DEBUG_SIG
	printk("get_sigframe()+\n");
#endif

	/* Default to using normal stack.  */
	usp = rdusp();

	/* This is the X/Open sanctioned signal stack switching.  */
	if (ka->sa.sa_flags & SA_ONSTACK) {
		if (!sas_ss_flags(usp))
			usp = current->sas_ss_sp + current->sas_ss_size;
	}
	return (void *)((usp - frame_size) & -8UL);
}

#if defined( CONFIG_RAM_SIZE_32MB )
static asmlinkage void cr16_sigret_stub( void )
{
	/*
	 * NOTE: __NR_sigreturn is hardwired below
	 */
	__asm__( \
		"movd	$0, (r1,r0)\n\t" \
		"movd	$0x77,(r1,r0)\n\t" \
		"excp	svc\n\t" \
	) ;
}

static asmlinkage void cr16_rt_sigret_stub( void )
{
	/*
	 * NOTE: __NR_rt_sigreturn is hardwired below
	 */
	__asm__( \
		"movd	$0, (r1,r0)\n\t" \
		"movd	$0xad,(r1,r0)\n\t" \
		"excp	svc\n\t" \
	) ;
}
#endif

static void setup_frame (int sig, struct k_sigaction *ka,
			 sigset_t *set, struct pt_regs *regs)
{
	struct sigframe *frame;
	int err = 0;
	int usig;
	unsigned char *ret;
#if DEBUG_SIG
	printk("setup_frame()+\n");
#endif

	frame = get_sigframe(ka, regs, sizeof(*frame));
#if DEBUG_SIG
	printk("&frame = [0X%x]\n",frame);
#endif
	if (!access_ok(VERIFY_WRITE, frame, sizeof(*frame)))
		goto give_sigsegv;

	usig = current_thread_info()->exec_domain
		&& current_thread_info()->exec_domain->signal_invmap
		&& sig < 32
		? current_thread_info()->exec_domain->signal_invmap[sig]
		: sig;



	err |= __put_user(usig, &frame->sig);
	if (err)
		goto give_sigsegv;
#if DEBUG_SIG
	printk("usig = 0x%x, frame->sig = [0X%x]\n",usig, frame->sig);
	printk("setup_sigcontext - &frame->sc, regs = 0x%x",&frame->sc, regs);
#endif

	err |= setup_sigcontext(&frame->sc, regs, set->sig[0]);
	if (err)
		goto give_sigsegv;

	if (_NSIG_WORDS > 1) {
		err |= copy_to_user(frame->extramask, &set->sig[1],
				    sizeof(frame->extramask));
		if (err)
			goto give_sigsegv;
	}

	ret = frame->retcode;
	if (ka->sa.sa_flags & SA_RESTORER)
		ret = (unsigned char *)(ka->sa.sa_restorer);
	else {

#if !defined( CONFIG_RAM_SIZE_32MB )
	  /*
	    movd	$0x0, (r1,r0)
	    movd	__NR_sigreturn,(r1,r0)
	    excp	svc
	  */
	  err |= __put_user(0x5400,(unsigned long *)(frame->retcode + 0));
	  err |= __put_user(0x54b0,
			    (unsigned long *)(frame->retcode + 2));

	  err |= __put_user((__NR_sigreturn & 0xff),
			    (unsigned long *)(frame->retcode + 4));

	  err |= __put_user(0x00c5,(unsigned long *)(frame->retcode + 6));
#else
		ret = (void*)cr16_sigret_stub ;
#endif

	}

	/* Set up to return from userspace.  */
	err |= __put_user(ret, &frame->pretcode);

	flush_cache_all();

	if (err)
		goto give_sigsegv;
#if DEBUG_SIG
	printk("frame->pretcode = [0X%x\n",frame->pretcode);

	/* Set up registers for signal handler */
	printk("wrusp = frame = 0x%x\n",frame);
#endif
	wrusp ((unsigned long) frame);
	regs->pc = (unsigned long) ka->sa.sa_handler;
	regs->r2and3 = (current_thread_info()->exec_domain
			   && current_thread_info()->exec_domain->signal_invmap
			   && sig < 32
			   ? current_thread_info()->exec_domain->signal_invmap[sig]
		          : sig);

	regs->ra = frame->pretcode;
#if !defined( CONFIG_RAM_SIZE_32MB )
	regs->ra = regs->ra / 2;
#endif

	return;

give_sigsegv:
	force_sigsegv(sig, current);
}


static void setup_rt_frame (int sig, struct k_sigaction *ka, siginfo_t *info,
			    sigset_t *set, struct pt_regs *regs)
{
	struct rt_sigframe *frame;
	int err = 0;
	int usig;
	unsigned char *ret;
#if DEBUG_SIG
	printk("setup_rt_frame()+\n");
#endif

	frame = get_sigframe(ka, regs, sizeof(*frame));

	if (!access_ok(VERIFY_WRITE, frame, sizeof(*frame)))
		goto give_sigsegv;

	usig = current_thread_info()->exec_domain
		&& current_thread_info()->exec_domain->signal_invmap
		&& sig < 32
		? current_thread_info()->exec_domain->signal_invmap[sig]
		: sig;

	err |= __put_user(usig, &frame->sig);
	if (err)
		goto give_sigsegv;

	err |= __put_user(&frame->info, &frame->pinfo);
	err |= __put_user(&frame->uc, &frame->puc);
	err |= copy_siginfo_to_user(&frame->info, info);
	if (err)
		goto give_sigsegv;

	/* Create the ucontext.  */
	err |= __put_user(0, &frame->uc.uc_flags);
	err |= __put_user(0, &frame->uc.uc_link);
	err |= __put_user((void *)current->sas_ss_sp,
			  &frame->uc.uc_stack.ss_sp);
	err |= __put_user(sas_ss_flags(rdusp()),
			  &frame->uc.uc_stack.ss_flags);
	err |= __put_user(current->sas_ss_size, &frame->uc.uc_stack.ss_size);
	err |= setup_sigcontext(&frame->uc.uc_mcontext, regs, set->sig[0]);
	err |= copy_to_user (&frame->uc.uc_sigmask, set, sizeof(*set));
	if (err)
		goto give_sigsegv;

	/* Set up to return from userspace.  */

	ret = frame->retcode;
	if (ka->sa.sa_flags & SA_RESTORER)
		ret = (unsigned char *)(ka->sa.sa_restorer);
	else {

#if !defined( CONFIG_RAM_SIZE_32MB )
	  /*
	    movd	$0x0, (r1,r0)
	    movd	__NR_sigreturn,(r1,r0)
	    excp	svc
	  */
	  err |= __put_user(0x5400,(unsigned long *)(frame->retcode + 0));
	  err |= __put_user(0x54b0,
			    (unsigned long *)(frame->retcode + 2));

	  err |= __put_user((__NR_rt_sigreturn & 0xff),
			    (unsigned long *)(frame->retcode + 4));

	  err |= __put_user(0x00c5,(unsigned long *)(frame->retcode + 6));
#else
		ret = (void*)cr16_rt_sigret_stub ;
#endif

	}
	err |= __put_user(ret, &frame->pretcode);

	if (err)
		goto give_sigsegv;

	/* Set up registers for signal handler */
	wrusp ((unsigned long) frame);
	regs->pc  = (unsigned long) ka->sa.sa_handler;
	regs->r2and3 = (current_thread_info()->exec_domain
		     && current_thread_info()->exec_domain->signal_invmap
		     && sig < 32
		     ? current_thread_info()->exec_domain->signal_invmap[sig]
		     : sig);

	regs->r4and5 = &frame->info;
	/*we can only pass 2 arguments in registers
	  so 3rd gets passed on the stack*/
	frame->paramthree = &frame->uc;
	regs->ra = frame->pretcode;
#if !defined( CONFIG_RAM_SIZE_32MB )
	regs->ra = regs->ra / 2;
#endif

	flush_cache_all();

#if DEBUG_SIG
	printk("SIG deliver (%s:%d): sp=0x%p pc=0x%lx ra=0x%p\n",
	       current->comm, current->pid,
	       frame, regs->pc, regs->ra);
#endif
	return;

give_sigsegv:
	force_sigsegv(sig, current);
}

/*
 * OK, we're invoking a handler
 */
static void
handle_signal(unsigned long sig, siginfo_t *info, struct k_sigaction *ka,
	      sigset_t *oldset,	struct pt_regs * regs)
{
#if DEBUG_SIG
  	printk("handle_signal()+\n");
#endif

	/* are we from a system call? */
	if (regs->orig_r0and1 >= 0) {

		switch (regs->r0and1) {
		        case -ERESTART_RESTARTBLOCK:
			case -ERESTARTNOHAND:
				regs->r0and1 = -EINTR;
				break;

			case -ERESTARTSYS:
				if (!(ka->sa.sa_flags & SA_RESTART)) {
					regs->r0and1 = -EINTR;
					break;
				}
			/* fallthrough */
			case -ERESTARTNOINTR:
				regs->r0and1 = regs->orig_r0and1;
				regs->pc = regs->pc - 1;
		}
	}

	/* set up the stack frame */
	if (ka->sa.sa_flags & SA_SIGINFO)
		setup_rt_frame(sig, ka, info, oldset, regs);
	else
		setup_frame(sig, ka, oldset, regs);

	spin_lock_irq(&current->sighand->siglock);
	sigorsets(&current->blocked,&current->blocked,&ka->sa.sa_mask);
	if (!(ka->sa.sa_flags & SA_NODEFER))
		sigaddset(&current->blocked,sig);
	recalc_sigpending();
	spin_unlock_irq(&current->sighand->siglock);
}

/*
 * Note that 'init' is a special process: it doesn't get signals it doesn't
 * want to handle. Thus you cannot kill init even with a SIGKILL even by
 * mistake.
 */
asmlinkage int do_signal(struct pt_regs *regs, sigset_t *oldset)
{
	siginfo_t info;
	int signr;
	struct k_sigaction ka;
#if DEBUG_SIG
	printk("\n\n\ndo_signal()+\n");
#endif
	/*
	 * We want the common case to go fast, which
	 * is why we may in certain cases get here from
	 * kernel mode. Just return without doing anything
	 * if so.
	 */

	if (!user_mode(regs))
		return 1;

	if (try_to_freeze())
		goto no_signal;

	current->thread.esp0 = (unsigned long) regs;

	if (!oldset)
		oldset = &current->blocked;

	signr = get_signal_to_deliver(&info, &ka, regs, NULL);
	if (signr > 0) {
		/* Whee!  Actually deliver the signal.  */
		handle_signal(signr, &info, &ka, oldset, regs);
		return 1;
	}
#if DEBUG_SIG
	else
	  printk("no signal to deliver!\n");
#endif
 no_signal:

	/* Did we come from a system call? */
	if (regs->orig_r0and1 >= 0) {
#if DEBUG_SIG
	        printk("we came from a syscall!\n");
	        printk("\ndo_signal  regs->pc = 0x%x, regs->orig_r0and1 = 0x%x\n"
		       ,regs->pc, regs->orig_r0and1);
#endif

	  if (regs->orig_r0and1 == 0xabcd1234)
	    {

	      while(1)
		{
		  printk("cr16_signal,,hit flag hack!\n");
		}
	    }

		/* Restart the system call - no handlers present */
		if (regs->r0and1 == -ERESTARTNOHAND ||
		    regs->r0and1 == -ERESTARTSYS ||
		    regs->r0and1 == -ERESTARTNOINTR) {
			regs->r0and1 = regs->orig_r0and1;
			regs->pc = regs->pc - 1;
		}
		if (regs->r0and1 == -ERESTART_RESTARTBLOCK){
			regs->r0and1 = __NR_restart_syscall;
			regs->pc = regs->pc - 1;
		}
#if DEBUG_SIG
		printk("\ndo_signal after regs->pc = 0x%x\n",regs->pc);
#endif

	}

	return 0;
}


asmlinkage void do_notify_resume(struct pt_regs *regs, u32 thread_info_flags)
 {
         if (thread_info_flags & (_TIF_SIGPENDING | _TIF_RESTORE_SIGMASK))
                 do_signal(regs, NULL);
 }
