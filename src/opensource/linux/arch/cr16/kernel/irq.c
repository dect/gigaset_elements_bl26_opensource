/* $Id: irq.c,v 1.4 2005/01/04 12:22:28 starvik Exp $
 *
 *	linux/arch/cr16/kernel/irq.c
 *
 *      Copyright (c) 2000-2002 Axis Communications AB
 *      Copyright (c) 2007 MPC Data Limited (CR16 support)
 *
 *      Authors: Bjorn Wesen (bjornw@axis.com)
 *               Lee Jones (ljones@mpc-data.co.uk)
 *
 *      This file contains the interrupt vectors and some 
 *      helper functions
 *
 */

#include <asm/regs.h>
#include <linux/module.h>
#include <linux/ptrace.h>
#include <linux/irq.h>

#include <linux/kernel_stat.h>
#include <linux/signal.h>
#include <linux/sched.h>
#include <linux/ioport.h>
#include <linux/interrupt.h>
#include <linux/timex.h>
#include <linux/slab.h>
#include <linux/random.h>
#include <linux/init.h>
#include <linux/seq_file.h>
#include <linux/errno.h>
#include <linux/spinlock.h>
#include <linux/reboot.h>

#include <asm/io.h>

#define ENABLE_NESTED_IRQS

#if defined( CONFIG_SC1445x_POWER_DRIVER )

#include <asm-cr16/sc14450_power.h>

#endif

#if defined( CONFIG_SC1445x_POWER_DRIVER )
#   define CR16_IRV_ADDR		0x8000
#elif defined( CONFIG_SDRAM_BASE_F0000 )
#   define CR16_IRV_ADDR		0xF0000
#elif defined( CONFIG_SDRAM_BASE_22000 )
#   define CR16_IRV_ADDR		0x22000
#elif defined( CONFIG_SDRAM_BASE_20000 )
#   define CR16_IRV_ADDR		0x20000
#else
#  error SDRAM base not defined
#endif


extern void show_trace_task(struct task_struct *tsk);
extern void show_regs(struct pt_regs * regs);

extern int console_loglevel;

extern int nmi_counter;


#define save_regs() \
__asm__ ( \
          "push $2, r0\n\t" \
          "push $8, r8, ra\n\t" \
          "push $8, r0\n\t" \
          "sprd isp, (r1,r0)\n\t" \
          "loadd 0x0(r1,r0), (r3,r2)\n\t" \
          "push $2, r2\n\t" \
          "loadw 0x4(r1,r0), r2\n\t" \
          "push $1, r2\n\t" \
          "subd $2, (sp)\n\t" \
          "movd (sp), (r3,r2)\n\t" /* r3,r2 is struct pt_regs* */ \
	  "push $2, r2\n" \
)

#define dump_regs() \
__asm__ ( \
	  "pop $2, r2\n\t" \
	  "bal (ra), _show_regs\n" \
)


extern int reboot_on_crash ;

#define TRAP_HANDLED					\
	do {						\
		if( reboot_on_crash )			\
			emergency_restart();		\
		else					\
			while( 1 ) ;			\
	} while( 0 )


void nmi(void) __attribute__((interrupt));

void nmi(void)
{
    local_irq_disable();

    // here we are analyzing exception from watchdog
    // can't ignore NMI this time
    if( nmi_counter )
    {
        WATCHDOG_REG = 0xFF;
        --nmi_counter;

        local_irq_enable();
    }
    else
    {
        local_irq_enable();

        save_regs() ;
        oops_in_progress = 1;

        printk("NMI trap - from WATCHDOG\n");
        printk("WATCHDOG_REG is set to 0x%02x\n", WATCHDOG_REG);

        __asm("pop $2, r0\n");
        dump_regs();
        show_trace_task(current);
        TRAP_HANDLED;
    }
}

void divide_zero(void)
{
	//local_irq_disable();
	save_regs() ;
	oops_in_progress = 1;
	printk("Divide by Zero trap\n");
	__asm( "pop $2, r0\n" ) ;
	//console_loglevel = 15;
	dump_regs() ;
	show_trace_task( current ) ;
	TRAP_HANDLED ;
}

void flag(void)
{
	 force_sig(SIGTRAP,current); // simulate a breakpoint for the inferior
	      asm("nop");
	      asm("nop");
	       asm("retx");
}

void breakpoint(void)
{
	//local_irq_disable();
	save_regs() ;
	oops_in_progress = 1;
	printk("BREAKPOINT trap\n");
	__asm( "pop $2, r0\n" ) ;
	//console_loglevel = 15;
	dump_regs() ;
	show_trace_task( current ) ;
	TRAP_HANDLED ;
}

void trace(void)
{
	//local_irq_disable();
	save_regs() ;
	oops_in_progress = 1;
	printk("TRACE trap\n");
	__asm( "pop $2, r0\n" ) ;
	//console_loglevel = 15;
	dump_regs() ;
	show_trace_task( current ) ;
	TRAP_HANDLED ;
}

void undefined_instr(void)
{
	//local_irq_disable();
	save_regs() ;
	oops_in_progress = 1;
	printk("Undefined Instruction trap\n");
	__asm( "pop $2, r0\n" ) ;
	//console_loglevel = 15;
	dump_regs() ;
	show_trace_task( current ) ;
	TRAP_HANDLED ;
}

void illegal_addr(void)
{
	//local_irq_disable();
	save_regs() ;
	oops_in_progress = 1;
	printk("Illegal Address trap\n");
	__asm( "pop $2, r0\n" ) ;
	//console_loglevel = 15;
	dump_regs() ;
	show_trace_task( current ) ;
	TRAP_HANDLED ;
}

static unsigned short irq_priorities[NR_IRQS] = {
	4,	/* 00 ACCESS12_INT */
	6,	/* 01 KEYB_INT */
	5,	/* 02 RESERVED_INT */ //452 emac
	4,	/* 03 CT_CLASSD_INT */
	4,	/* 04 UART_RI_INT */
	4,	/* 05 UART_TI_INT */
	4,	/* 06 SPI1_AD_INT */
	4,	/* 07 TIM0_INT */
	4,	/* 08 TIM1_INT */
	4,	/* 09 CLK100_INT */
	7,	/* 10 DIP_INT */
	4,	/* 11 USB_INT */ //452 crypto
	4,	/* 12 SPI2_INT */
	6,	/* 13 DSP1_INT */
	6,	/* 14 DSP2_INT */
} ;

void mask_irq (unsigned long int_number)
{
	const short shift = ( int_number % 4 ) * 4 ;
	unsigned short mask = 0x7 << shift ;
	unsigned short * reg = ((unsigned short*)0xff540a) - (int_number/4);

	irq_priorities[int_number] = ( readw(reg) & mask ) >> shift ;
	writew(readw(reg)&~mask,reg);
}

void unmask_irq (unsigned long int_number)
{
	const short shift = ( int_number % 4 ) * 4 ;
	unsigned short mask = 0x7 << shift ;
	unsigned short val = irq_priorities[int_number] << shift ;
	unsigned short * reg = ((unsigned short*)0xff540a) - (int_number/4);
    
	writew( (readw( reg ) & ~mask) | val, reg ) ;
}

struct cr16_interrupt_vector *cr16_irv =
				(struct cr16_interrupt_vector *)CR16_IRV_ADDR;

void
set_int_vector(int n, irqvectptr addr)
{
	cr16_irv->v[n + 0x10] = (irqvectptr)addr;
}

/* the breakpoint vector is obviously not made just like the normal irq handlers
 * but needs to contain _code_ to jump to addr.
 *
 * the BREAK n instruction jumps to IBR + n * 8
 */

void
set_break_vector(int n, irqvectptr addr)
{
	cr16_irv->v[n] = (irqvectptr)addr;
}

/*
 * This builds up the IRQ handler stubs using some ugly macros in irq.h
 *
 * These macros create the low-level assembly IRQ routines that do all
 * the operations that are needed. They are also written to be fast - and to
 * disable interrupts as little as humanly possible.
 *
 */


void hwbreakpoint(void);
BUILD_IRQ(0) 
BUILD_IRQ(1)
BUILD_IRQ(2)
BUILD_IRQ(3)
BUILD_IRQ(4)
BUILD_IRQ(5)
BUILD_IRQ(6)
BUILD_IRQ(7)
BUILD_IRQ(8)
BUILD_IRQ(9)
BUILD_IRQ(10)
BUILD_IRQ(11)
BUILD_IRQ(12)
BUILD_IRQ(13)
BUILD_IRQ(14)


 
/*
 * Pointers to the low-level handlers 
 */

static void (*interrupt[NR_IRQS])(void) = {
	IRQ0_interrupt, IRQ1_interrupt, IRQ2_interrupt, IRQ3_interrupt,
	IRQ4_interrupt, IRQ5_interrupt, IRQ6_interrupt, IRQ7_interrupt,
	IRQ8_interrupt, IRQ9_interrupt, IRQ10_interrupt, IRQ11_interrupt,
	IRQ12_interrupt, IRQ13_interrupt, IRQ14_interrupt
};



static void enable_cr16_irq(unsigned int irq);

static unsigned int startup_cr16_irq(unsigned int irq)
{
	enable_cr16_irq(irq);
	return 0;
}

#define shutdown_cr16_irq	disable_cr16_irq

static void enable_cr16_irq(unsigned int irq)
{
	unmask_irq(irq);
}

static void disable_cr16_irq(unsigned int irq)
{
	mask_irq(irq);
}

static void ack_cr16_irq(unsigned int irq)
{
}

static void end_cr16_irq(unsigned int irq)
{
}

static struct hw_interrupt_type cr16_irq_type = {
	.typename =    "CR16",
	.startup =     startup_cr16_irq,
	.shutdown =    shutdown_cr16_irq,
	.enable =      enable_cr16_irq,
	.disable =     disable_cr16_irq,
	.ack =         ack_cr16_irq,
	.end =         end_cr16_irq,
	.set_affinity = NULL
};

void weird_irq(void);
void system_call(void);  /* from entry.S */
void do_sigtrap(void); /* from entry.S */
void gdb_handle_breakpoint(void); /* from entry.S */

/* init_IRQ() is called by start_kernel and is responsible for fixing IRQ masks and
   setting the irq vector table.
*/

void __init
init_IRQ(void)
{
	int i;

	/* clear all interrupt masks */

	RESET_INT_PENDING_REG = 0xffff;

	for (i = 0; i < 32; i++)
		cr16_irv->v[i] = weird_irq;
	
	/* Initialize IRQ handler descriptiors. */
	for(i = 0; i < NR_IRQS; i++) {
		irq_desc[i].chip = &cr16_irq_type;
		set_int_vector(i, interrupt[i]);
	}

        /* the entries in the break vector contain actual code to be
           executed by the associated break handler, rather than just a jump
           address. therefore we need to setup a default breakpoint handler
           for all breakpoints */

	for (i = 0; i < 16; i++)
		set_break_vector(i, do_sigtrap);
        
	set_break_vector(1, nmi);
	set_break_vector(5, system_call);
	set_break_vector(6, divide_zero);
	set_break_vector(7, flag);
	set_break_vector(8, breakpoint);
	set_break_vector(9, trace);
	set_break_vector(10, undefined_instr);
	set_break_vector(12, illegal_addr);

}


// generic file copied here

void ack_bad_irq(unsigned int irq)
{
	printk(KERN_EMERG "unexpected IRQ trap at vector %02x\n", irq);
}

int show_interrupts(struct seq_file *p, void *v)
{

	int i = *(loff_t *) v, j;
	struct irqaction * action;
	unsigned long flags;

	if (i == 0) {
		seq_printf(p, "           ");
		for_each_online_cpu(j)
			seq_printf(p, "CPU%d       ",j);
		seq_putc(p, '\n');
	}

	if (i < NR_IRQS) {
		spin_lock_irqsave(&irq_desc[i].lock, flags);
		action = irq_desc[i].action;
		if (!action)
			goto skip;
		seq_printf(p, "%3d: ",i);

		seq_printf(p, "%10u ", kstat_irqs(i));

		seq_printf(p, " %14s", irq_desc[i].chip->typename);
		seq_printf(p, "  %s", action->name);

		for (action=action->next; action; action = action->next)
			seq_printf(p, ", %s", action->name);

		seq_putc(p, '\n');
skip:
		spin_unlock_irqrestore(&irq_desc[i].lock, flags);
	}
	return 0;

}


/* called by the assembler IRQ entry functions defined in irq.h
 * to dispatch the interrupts to registred handlers
 * interrupts are disabled upon entry - depending on if the
 * interrupt was registred with IRQF_DISABLED or not, interrupts
 * are re-enabled or not.
 */
#define _lpr_(procreg, src) __asm__("lpr\t%0," procreg : \
                                  /* no output */ : "r" (src) : "cc")

#define _spr_(procreg, dest) __asm__("spr\t" procreg ",%0" : \
                                  "=r" (dest) : /* no input */ "0" (dest) : "cc")


asmlinkage void do_IRQ(int irq, struct pt_regs * regs)
{
	struct pt_regs* old_regs ;
	unsigned short temp1;

	old_regs = set_irq_regs( regs ) ;

#if defined( CONFIG_SC1445x_POWER_DRIVER )
    sc1445x_power_ioctl_set_state(0);
    // sc1445x_power_ioctl_set_state2(0);
#endif

	irq_enter();
#ifdef ENABLE_NESTED_IRQS
	_spr_("psr",temp1);
        temp1 |= 0x800;
        _lpr_("psr",temp1);
#endif
	__do_IRQ(irq);

    irq_exit();

	set_irq_regs( old_regs ) ;
}

void weird_irq(void)
{
	//local_irq_disable();
	printk(KERN_EMERG "weird irq\n");
	TRAP_HANDLED; //	while(1);
}


