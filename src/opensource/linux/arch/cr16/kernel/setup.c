/*
 *
 *  linux/arch/cr16/kernel/setup.c
 *
 *  Copyright (C) 1995  Linus Torvalds
 *  Copyright (c) 2001  Axis Communications AB
 *  Copyright (c) 2007  MPC Data LTD - pgriffin@mpcdata.co.uk
 *  Copyright (c) 2007  MPC Data LTD - ljones@mpcdata.co.uk
 */

/*
 * This file handles the architecture-dependent parts of initialization
 */

#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/fs.h>
#include <linux/fb.h>
#include <linux/console.h>
#include <linux/genhd.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/major.h>
#include <linux/bootmem.h>
#include <linux/seq_file.h>
#include <linux/init.h>

#include <asm/setup.h>
#include <asm/irq.h>


#include <linux/init.h>
#include <linux/mm.h>
#include <linux/bootmem.h>
#include <asm/pgtable.h>
#include <linux/seq_file.h>
#include <linux/screen_info.h>
#include <linux/utsname.h>
#include <linux/pfn.h>
#include <linux/initrd.h>

#include <asm/setup.h>


#if defined(CONFIG_SC14450)
#  define CPU "Sitel SC14450"
#  include <asm/regs_sc14450.h>
#elif defined(CONFIG_SC14452)
#  define CPU "Sitel SC14452"
#  include <asm/regs_sc14452.h>
#endif

/*
 * Setup options
 */
struct screen_info screen_info;

extern int root_mountflags;
extern char _etext, _edata, _end;

char cr16_command_line[COMMAND_LINE_SIZE] = { 0, };

extern const unsigned long text_start, edata; /* set by the linker script */
extern unsigned long dram_start, dram_end;
extern unsigned long ram_hole_start, ram_hole_len, stack_end, _irq_stack_size;


/* This mainly sets up the memory area, and can be really confusing.
 *
 * The physical DRAM starts at dram_start to dram_end
 * (usually 0xf0000 to 0xf0000 + DRAM size). 
 *
 * In this DRAM, the kernel code and data is loaded, in the beginning.
 * It really starts at f4000 to make room for some special pages - 
 * the start address is text_start. The kernel data ends at _end. After
 * this the ROM filesystem is appended (if there is any).
 * 
 * Between this address and dram_end, we have RAM pages usable to the
 * boot code and the system.
 *
 */
extern void *__initramfs_start;
extern void *__initramfs_end;

unsigned long memory_start, memory_end;

void __init 
setup_arch(char **cmdline_p)
{
	unsigned long bootmap_size;
	unsigned long start_pfn, max_pfn;
	unsigned long ram_size;

 	/* register an initial console printing routine for printk's */

	//	setup_early_printk("");

	/* we should really poll for DRAM size! */

	high_memory = &dram_end;

#if 0
	if(romfs_in_flash || !romfs_length) {
		/* if we have the romfs in flash, or if there is no rom filesystem,
		 * our free area starts directly after the BSS
		 */
		memory_start = (unsigned long) &_end;
	} else {

		/* otherwise the free area starts after the ROM filesystem */
		printk("ROM fs in RAM, size %lu bytes\n", romfs_length);
		memory_start = romfs_start + romfs_length;
	}
#endif

	memory_start = (unsigned long) &_end;
	ram_size = ((unsigned long)&dram_end - (unsigned long)&dram_start);	

	memory_end = (unsigned long)&dram_end;

	printk("RAM size: 0x%lx, memory_end = 0x%lx\n", ram_size, memory_end);
	printk("IRQ stack size 0x%lx at %p\n", (long unsigned int)&_irq_stack_size, (void*)&stack_end);
	/* process 1's initial memory region is the kernel code/data */

	init_mm.start_code = (unsigned long) &text_start;
	init_mm.end_code =   (unsigned long) &_etext;
	init_mm.end_data =   (unsigned long) &_edata;
	init_mm.brk =        (unsigned long) &_end;

	/* min_low_pfn points to the start of DRAM, start_pfn points
	 * to the first DRAM pages after the kernel, and max_low_pfn
	 * to the end of DRAM.
	 */
	
	/*
	 * partially used pages are not usable - thus
	 * we are rounding upwards:
	 */
	
	start_pfn = PFN_UP(memory_start);  /* usually f0000 + kernel + romfs */
	max_pfn =   PFN_DOWN((unsigned long)high_memory); /* usually f0000 + dram size */
	
	/*
         * Initialize the boot-time allocator (start, end)
		 *
		 * We give it access to all our DRAM, but we could as well just have
		 * given it a small slice. No point in doing that though, unless we
		 * have non-contiguous memory and want the boot-stuff to be in, say,
		 * the smallest area.
		 *
		 * It will put a bitmap of the allocated pages in the beginning
		 * of the range we give it, but it won't mark the bitmaps pages
		 * as reserved. We have to do that ourselves below.
		 *
		 * We need to use init_bootmem_node instead of init_bootmem
		 * because our map starts at a quite high address (min_low_pfn).
         */

	max_low_pfn = max_pfn;
	min_low_pfn = PAGE_OFFSET >> PAGE_SHIFT;

#if 0
	printk("init bootmem node start_pfn=%p min_low_pfn=%p max_low_pfn=%p\n",(void*)start_pfn,
	        (void*)min_low_pfn,
	        (void*)max_low_pfn);
#endif
	bootmap_size = init_bootmem_node(NODE_DATA(0), start_pfn,
					 min_low_pfn, 
					 max_low_pfn);

	/* And free all memory not belonging to the kernel (addr, size) */

	printk("freed bootmem %lx %lx\n",PFN_PHYS(start_pfn), PFN_PHYS(max_pfn - start_pfn));

	free_bootmem(PFN_PHYS(start_pfn), PFN_PHYS(max_pfn - start_pfn));

        /*
         * Reserve the bootmem bitmap itself as well. We do this in two
         * steps (first step was init_bootmem()) because this catches
         * the (very unlikely) case of us accidentally initializing the
         * bootmem allocator with an invalid RAM area.
	 *
	 * Arguments are start, size
         */

	printk("reserve bootmem %lx %lx\n",PFN_PHYS(start_pfn), bootmap_size);

        reserve_bootmem(PFN_PHYS(start_pfn), bootmap_size);

#if defined( CONFIG_SC1445x_ROMFS_XIP ) && ( CONFIG_MTDRAM_ABS_POS > 0 )
	//reserve romfs root filesystem
	reserve_bootmem(CONFIG_MTDRAM_ABS_POS, CONFIG_MTDRAM_TOTAL_SIZE * 1024);
#endif

#if defined( CONFIG_RAM_SIZE_32MB )
	/* put a hole in the available RAM */
	//printk( "%s: memory_end=%08lx\n", __FUNCTION__, memory_end ) ;
	reserve_bootmem( (unsigned long)&ram_hole_start, (unsigned long)&ram_hole_len) ;
	printk( "IRQ stack + mem hole: reserved memory range %p-%p\n",
	        (void*)&ram_hole_start,
	        (void*)((unsigned long)&ram_hole_start + (unsigned long)&ram_hole_len - 1) ) ;

#endif

	/* paging_init() sets up the MMU and marks all pages as reserved */
	paging_init();

	*cmdline_p = cr16_command_line;

#ifdef CONFIG_DEFAULT_CMDLINE
	/* set from default command line */
	if (*cr16_command_line == '\0') {
		strncpy( cr16_command_line,
#if defined( CONFIG_SC1445x_ROMFS_XIP )
			"root=/dev/mtdblock0 rootfstype=romfs ro ",
#else
			"noinitrd root=/dev/ram0 rw ",
#endif
			sizeof( cr16_command_line ) ) ;
		strncat( cr16_command_line, CONFIG_KERNEL_COMMAND,
						sizeof( cr16_command_line ) ) ;
	}
#endif
	strncat( cr16_command_line, " ", sizeof( cr16_command_line ) ) ;
#if defined( CONFIG_SDRAM_BASE_F0000 )
	strncat( cr16_command_line, (char*)0xF0000,
						sizeof( cr16_command_line ) ) ;
#elif defined( CONFIG_SDRAM_BASE_22000 )
	/* NOTE: we are passing the command line at 0x20000, but we set */
	/*       the SDRAM base address at 0x22000 -- that's OK */
	strncat( cr16_command_line, (char*)0x20000,
						sizeof( cr16_command_line ) ) ;
#elif defined( CONFIG_SDRAM_BASE_20000 )
	strncat( cr16_command_line, (char*)0x20000,
						sizeof( cr16_command_line ) ) ;
#else
#  error SDRAM base not defined
#endif
	printk( "commandline = '%s'\n", cr16_command_line) ;

	/* Save command line for future references. */
	memcpy(saved_command_line, cr16_command_line, COMMAND_LINE_SIZE);
	saved_command_line[COMMAND_LINE_SIZE - 1] = '\0';

}

static void *c_start(struct seq_file *m, loff_t *pos)
{
	return *pos < NR_CPUS ? (void *)(int)(*pos + 1): NULL;
}

static void *c_next(struct seq_file *m, void *v, loff_t *pos)
{
	++*pos;
	return c_start(m, pos);
}

static void c_stop(struct seq_file *m, void *v)
{
}


static int show_cpuinfo(struct seq_file *m, void *v)
{
  char *cpu;

  char revision, id1, id2, id3, layout;

  cpu = CPU;
  revision = CHIP_REVISION_REG >> 4;
  id1 = CHIP_ID1_REG;
  id2 = CHIP_ID2_REG;
  id3 = CHIP_ID3_REG;
  layout = CHIP_TEST1_REG;
      

  seq_printf(m, "CPU:\t\t%s \tDevice ID: %c%c%c (revision:%c, layout=%d)\n"
	     "BogoMips:\t%lu.%02lu\n"
	     "Calibration:\t%lu loops\n",
	     cpu, id1, id2, id3, revision - 1 + 'A', layout,
	     (loops_per_jiffy*HZ)/500000,((loops_per_jiffy*HZ)/5000)%100,
	     (loops_per_jiffy*HZ));
  

  return 0;
}

struct seq_operations cpuinfo_op = {
	.start = c_start,
	.next  = c_next,
	.stop  = c_stop,
	.show  = show_cpuinfo,
};


