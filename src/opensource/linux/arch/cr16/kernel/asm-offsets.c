/*
 * This program is used to generate definitions needed by
 * assembly language modules.
 *
 * We use the technique used in the OSF Mach kernel code:
 * generate asm statements containing #defines,
 * compile this file to assembler, and then extract the
 * #defines from the assembly-language output.
 */

#include <linux/stddef.h>
#include <linux/sched.h>
#include <linux/kernel_stat.h>
#include <linux/ptrace.h>
#include <linux/hardirq.h>
#include <asm/bootinfo.h>
#include <asm/irq.h>
#include <asm/ptrace.h>

#define DEFINE(sym, val) \
        asm volatile("\n->" #sym " %0 " #val : : "i" (val))

#define BLANK() asm volatile("\n->" : : )

int main(void)
{
	/* offsets into the task struct */
	DEFINE(TASK_STATE, offsetof(struct task_struct, state));
	DEFINE(TASK_FLAGS, offsetof(struct task_struct, flags));
	DEFINE(TASK_PTRACE, offsetof(struct task_struct, ptrace));
	DEFINE(TASK_BLOCKED, offsetof(struct task_struct, blocked));
	DEFINE(TASK_THREAD, offsetof(struct task_struct, thread));
	DEFINE(TASK_THREAD_INFO, offsetof(struct task_struct, thread_info));
	DEFINE(TASK_MM, offsetof(struct task_struct, mm));
	DEFINE(TASK_ACTIVE_MM, offsetof(struct task_struct, active_mm));

	/* offsets into the irq_cpustat_t struct */
	DEFINE(CPUSTAT_SOFTIRQ_PENDING, offsetof(irq_cpustat_t, __softirq_pending));

	/* offsets into the thread struct */
	DEFINE(THREAD_KSP, offsetof(struct thread_struct, ksp));
	DEFINE(THREAD_USP, offsetof(struct thread_struct, usp)); 
	DEFINE(THREAD_PSR, offsetof(struct thread_struct, psr));

	/* offsets into the pt_regs struct */
	DEFINE(LR0AND1,  offsetof(struct pt_regs, r0and1)      );
	DEFINE(LR2AND3,  offsetof(struct pt_regs, r2and3)      );
	DEFINE(LR4AND5,  offsetof(struct pt_regs, r4and5)      );
	DEFINE(LR6AND7,  offsetof(struct pt_regs, r6and7)      );
	DEFINE(LR8AND9,   offsetof(struct pt_regs, r8and9)      );
	DEFINE(LR10AND11, offsetof(struct pt_regs, r10and11)    );
	DEFINE(LR12,      offsetof(struct pt_regs, r12)         );
	DEFINE(LR13,      offsetof(struct pt_regs, r13)         );
	DEFINE(LORIG,    offsetof(struct pt_regs, orig_r0and1) );
	DEFINE(LPSR,     offsetof(struct pt_regs, psr)         );
	DEFINE(LRA,     offsetof(struct pt_regs, ra)          );
//	DEFINE(LVEC,     offsetof(struct pt_regs, vector)      );


	DEFINE(PT_PTRACED, PT_PTRACED);
	DEFINE(PT_DTRACE, PT_DTRACE);

	return 0;
}
