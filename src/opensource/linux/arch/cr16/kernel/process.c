/*
 *  linux/arch/cr16/kernel/process.c
 *
 *  Peter Griffin pgriffin@mpc-data.co.uk
 *
 *  Based on:
 *
 *  linux/arch/h8300/kernel/process.c
 *  Yoshinori Sato <ysato@users.sourceforge.jp>
 *
 *  Based on:
 *
 *  linux/arch/m68knommu/kernel/process.c
 *
 *  Copyright (C) 1998  D. Jeff Dionne <jeff@ryeham.ee.ryerson.ca>,
 *                      Kenneth Albanowski <kjahds@kjahds.com>,
 *                      The Silver Hammer Group, Ltd.
 *
 *  linux/arch/m68k/kernel/process.c
 *
 *  Copyright (C) 1995  Hamish Macdonald
 *
 *  68060 fixes by Jesper Skov
 */

/*
 * This file handles the architecture-dependent parts of process handling..
 */

#include <linux/errno.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/smp.h>
#include <linux/smp_lock.h>
#include <linux/stddef.h>
#include <linux/unistd.h>
#include <linux/ptrace.h>
#include <linux/slab.h>
#include <linux/user.h>
#include <linux/a.out.h>
#include <linux/interrupt.h>
#include <linux/reboot.h>

#include <asm/uaccess.h>
#include <asm/system.h>
#include <asm/traps.h>
#include <asm/setup.h>
#include <asm/pgtable.h>

#if defined( CONFIG_SC1445x_POWER_DRIVER )
#include <asm-cr16/sc14450_power.h>
#endif

//#define CR16_PROCESS_DEBUG 1

void (*pm_power_off)(void) = NULL;
EXPORT_SYMBOL(pm_power_off);

asmlinkage void ret_from_fork(void);

/*
 * The idle loop on CR16..
 */
 
static void default_idle(void)
{
	local_irq_disable();
	if (!need_resched()) {
        
#if defined( CONFIG_SC1445x_POWER_DRIVER )
        *(volatile u16 *) POS_POWER_FLAG = 1;
       sc1445x_power_ioctl_set_state(3);
       if ( (*(unsigned short *) POS_POWER_FLAG != 3) )
#endif
            __asm__("eiwait\t\n"
        	    "nop\t\n");
#if defined( CONFIG_SC1445x_POWER_DRIVER )
        *(volatile u16 *) POS_POWER_FLAG = 2;
#endif
	} 
    else 
		local_irq_enable();
    
}

void (*idle)(void) = default_idle;

/*
 * The idle thread. There's no useful work to be
 * done, so just try to conserve power and have a
 * low exit latency (ie sit in a loop waiting for
 * somebody to say that they'd like to reschedule)
 */
void cpu_idle(void)
{
	while (1) {
		while (!need_resched())
		  idle();
		preempt_enable_no_resched();
		schedule();
		preempt_disable();
	}
}

void machine_restart(char * __unused)
{
	local_irq_disable();
#if 0
	__asm__("1:\n\t"
		"br 1b\n"); 
#else
	/* program watchdog timer for ~100 msec and set it to issue a reset */
	TIMER_CTRL_REG |= 0x10 ;
	WATCHDOG_REG = 0xFF ; // Allow ~3sec for any printk to get printed before doing a reset
	while( 1 )
		;	/* shouldn't take long... */
#endif
}

void machine_halt(void)
{
	local_irq_disable();
		__asm__("wait");

	for (;;);
}

void machine_power_off(void)
{
	local_irq_disable();
		__asm__("wait");

	for (;;);
}

void show_regs(struct pt_regs * regs)
{
	printk( "\nPC: %08lx RA: %08lx"
		"\neffective code addresses PC: %08lx RA: %08lx",
			regs->pc, regs->ra, regs->pc << 1, regs->ra << 1 ) ;
	printk( "\nORIG_R0AND1: %08lx R0AND1: %08lx R2AND3: %08lx",
			regs->orig_r0and1, regs->r0and1, regs->r2and3 ) ;
	printk( "\nR4AND5: %08lx R6AND7: %08lx R8AND9: %08lx R10AND11: %08lx",
		regs->r4and5, regs->r6and7, regs->r8and9, regs->r10and11 ) ;
	printk( "\nR12: %08lx",regs->r12 ) ;
	printk( "\nR13: %08lx",regs->r13 ) ;
	printk( "\nPSR: %04x", regs->psr ) ;
	if( user_mode( regs ) )
		printk( "\nUSP: %08lx\n", rdusp() ) ;
	else
		printk( "\n" ) ;
}



void kernel_thread_helper(int (*fn)(void *), void * arg)
{
#if defined CR16_PROCESS_DEBUG
  printk("\n\n kernel_thread_helper()fn = 0x%x, arg = 0x%x\n\n", fn, arg);
#endif
  fn(arg);
  do_exit(-1); /* Should never be called, return bad exit value */
}

/*
 * Create a kernel thread
 */
int kernel_thread(int (*fn)(void *), void * arg, unsigned long flags)
{
	struct pt_regs regs;

	memset(&regs, 0, sizeof(regs));
#if defined CR16_PROCESS_DEBUG
	printk("\n\nkernel_thread() fn = 0x%x, arg = 0x%x\n\n", fn, arg);
#endif

        /* Don't use r1,r0 since that is set to 0 in copy_thread */
	regs.r2and3 = (unsigned long)fn;
	regs.r4and5 = (unsigned long)arg;

	/* Ok, create the new process.. */
        return do_fork(flags | CLONE_VM | CLONE_UNTRACED, 0, &regs, 0, NULL, NULL);
}


void flush_thread(void)
{
}

asmlinkage int cr16_fork(struct pt_regs *regs)
{
	return -EINVAL;
}

asmlinkage int cr16_vfork(struct pt_regs *regs)
{
	return do_fork(CLONE_VFORK | CLONE_VM | SIGCHLD, rdusp(), regs, 0, NULL, NULL);
}

asmlinkage int cr16_clone(struct pt_regs *regs)
{
	unsigned long clone_flags;
	unsigned long newsp;
#if defined CR16_PROCESS_DEBUG
	printk("cr16_clone()+ regs = 0x%x\n", regs);
	
	show_regs(regs);
#endif

        /* syscall2 puts clone_flags in (r2,r3) and usp in (r4,r5) */
        clone_flags = regs->r2and3;
        newsp = regs->r4and5;
        if (!newsp)
                newsp  = rdusp();
#if defined CR16_PROCESS_DEBUG
        printk("clone %X %X %X\n",clone_flags, newsp, regs);
#endif

        return do_fork(clone_flags, newsp, regs, 0, NULL, NULL);

}



int copy_thread(int nr, unsigned long clone_flags,
                unsigned long usp, unsigned long topstk,
		 struct task_struct * p, struct pt_regs * regs)
{
	struct pt_regs * childregs;
	struct pt_regs * childregs2;

	childregs = (struct pt_regs *) (THREAD_SIZE + task_stack_page(p)) - 1;

	childregs2 = (struct pt_regs *) childregs - 1;

	/* struct copy of pt_regs */
	*childregs = *regs;
	*childregs2 = *regs;

	childregs2->ra = (unsigned long) ret_from_fork;
	/* child returns 0 after a fork/clone */
	childregs2->r0and1 = 0;
	childregs->r0and1 = 0;

	/* fix the user-mode stackpointer */
	p->thread.usp = usp;
	/* and the kernel-mode one */
	p->thread.ksp = (unsigned long)childregs2;

#if defined CR16_PROCESS_DEBUG
	printk("copy_thread()\n");
	show_regs(regs);
	printk("\n\n");
	show_regs(childregs);
	printk("\n\n");
	show_regs(childregs2);
	printk("nksp = 0x%x\n",p->thread.ksp);
	printk("childregs = 0x%x\n",childregs);
#endif

	return 0;
}

/*
 * sys_execve() executes a new program.
 */
asmlinkage int cr16_execve(struct pt_regs *regs)
{
        int  error;
	char * filename;

	char *name = (char *)regs->r2and3;
	char **argv = (char **)regs->r4and5;
	char **envp = (char **)regs->r6and7;

#if defined CR16_PROCESS_DEBUG
        printk("regs = 0x%x",regs);
        show_regs(regs);
	printk("sys_execve() - name = 0x%x, argv = 0x%x, envp = 0x%x, regs = 0x%x\n"
	       , name, argv, envp, regs);
#endif
	lock_kernel();
	filename = getname(name);
	error = PTR_ERR(filename);
	if (IS_ERR(filename))
		goto out;
	error = do_execve(filename, argv, envp, regs);
	putname(filename);
out:

	unlock_kernel();

	return error;
}

unsigned long thread_saved_pc(struct task_struct *tsk)
{
	return ((struct pt_regs *)tsk->thread.esp0)->pc;
}

unsigned long get_wchan(struct task_struct *p)
{
	unsigned long fp, pc;
	unsigned long stack_page;
	int count = 0;
	if (!p || p == current || p->state == TASK_RUNNING)
		return 0;

	stack_page = (unsigned long)p;
	fp = ((struct pt_regs *)p->thread.ksp)->r10and11;
	do {
		if (fp < stack_page+sizeof(struct thread_info) ||
		    fp >= 8184+stack_page)
			return 0;
		pc = ((unsigned long *)fp)[1];
		if (!in_sched_functions(pc))
			return pc;
		fp = *(unsigned long *) fp;
	} while (count++ < 16);
	return 0;
}
