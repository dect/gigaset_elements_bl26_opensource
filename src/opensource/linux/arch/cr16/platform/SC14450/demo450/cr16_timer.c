/*
 *  linux/arch/cr16/platform/SC14450/demo450/timer.c
 *
 *  Yoshinori Sato <ysato@users.sourcefoge.jp>
 *  Lee Jones <ljones@mpc-data.co.uk>
 *
 *  Platform depend Timer Handler
 *
 */

#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/param.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/interrupt.h>
#include <linux/init.h>
#include <linux/timex.h>

#include <asm/segment.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/regs_sc14450.h>

#define CTRL_SETUP 0x2A              /* Timers 0 and 2 off          */
                                     /* Timer  1 on (Running at 115.2KHz in mode 2) */
#define CLEAR_INT_PEND 0x0100
                       
#define CR16_TIMER_FREQ 0x1CCCC      /* Timer input freq - 115.2 KHz */
#define ONE_mSECS 0x47F               /* One millisecond              */
#define TEN_mSECS 0x2CFF              /* Ten milliseconds             */

//#define JIFFY_ONE_mSECOND

#ifdef  JIFFY_ONE_mSECOND
#define TICK_DELAY ONE_mSECS
#else 
#define TICK_DELAY TEN_mSECS
#endif 

extern irqreturn_t timer_interrupt(int irq, void *dummy);

static struct irqaction irq2 = { 
	timer_interrupt, 
	IRQF_SHARED | IRQF_DISABLED,
	CPU_MASK_NONE, 
	"timer", 
	NULL, 
	NULL };

void __init platform_timer_setup(irqreturn_t (*timer_int)(int, void *))
{
	
	printk("platform_timer_setup () timer_int = 0x%p\n", timer_int); //PAG
	
	/* Request an interupt */
	setup_irq(TIM1_INT, &irq2);
	
	/* Setup inital timer values */
	TIMER1_RELOAD_N_REG = TICK_DELAY;
	TIMER1_RELOAD_M_REG = 0x0;
	
	/* Set interupt priority */
	INT1_PRIORITY_REG = 0x4;
	
	/* Clear timer1 interrupt */
	RESET_INT_PENDING_REG = 0x100;
	
	/* Turn on the timer */
	TIMER_CTRL_REG = 0x22; 
	
}

void platform_timer_eoi(void)
{
	RESET_INT_PENDING_REG = CLEAR_INT_PEND;
}

void platform_gettod(int *year, int *mon, int *day, int *hour,
		 int *min, int *sec)
{
	*year = *mon = *day = *hour = *min = *sec = 0;
}

