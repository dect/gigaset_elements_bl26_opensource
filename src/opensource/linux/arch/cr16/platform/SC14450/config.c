/* do some system initialization */

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/param.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/physmap.h>
#include <linux/spi/spi.h>
#include <linux/spi/flash.h>
#include <asm/spi.h>
#include <asm/config.h>


#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT )
#  include <../drivers/char/sc1445x_ata/arch/cr16/vp_api_types.h>
#  include <../drivers/char/sc1445x_ata/api_lib/includes/vp_api_dev_term.h>
#endif

#if defined( CONFIG_SC14450_VoIP_RevB )

	//#define HAVE_2MB_FLASH

	#  define FLASH_NAME		"m25p80"
	#  define FLASH_NAME_ALT	"sst25vf"
	#if defined( HAVE_2MB_FLASH )
		#  define FLASH_TYPE		"w25x16"
	#else
		#  define FLASH_TYPE		"w25x32"
		#  define FLASH_TYPE_ALT	"sst25vf032b"
	#endif
	#  define PERIPH_SPI_BUS	1
	#  define PERIPH_SPI_INT	SPI1_AD_INT
	
#elif 	defined( CONFIG_RPS_BOARD )
	#define HAVE_8MB_FLASH

	#  define FLASH_NAME		"m25p80"
	#  define FLASH_TYPE		"w25x64"

	#  define PERIPH_SPI_BUS	1
	#  define PERIPH_SPI_INT	SPI1_AD_INT

#elif 	defined( CONFIG_F_G2_P1_BOARD )

	#  define FLASH_NAME		"m25p80"
	#  define FLASH_TYPE		"MX25L3205D"
	#  define PERIPH_SPI_BUS	1
	#  define PERIPH_SPI_INT	SPI1_AD_INT
	
#else
	#  define FLASH_NAME		"sst25vf"
	#  define FLASH_TYPE		"sst25vf032b"
	#  define PERIPH_SPI_BUS	0
	#  define PERIPH_SPI_INT	SPI2_INT
#endif

#define BOOTLOADER_ENV_VARS_PART	"Enviroment Variables"
#define BOOTLOADER_ENV_VARS_SIZE	0x2000

int reboot_on_crash = 1 ;


#if defined( HAVE_2MB_FLASH )
static struct mtd_partition sc1445x_spi_flash_partitions_2MB[] = {
	{
		name:		"Loader",
		size:		0x001000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Bootloader",
		size:		0x01F000,
		offset:		0x001000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Filler",
		size:		0x000000,
		offset:		0x020000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"File System",
		size:		0x1DE000,
		offset:		0x020000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		BOOTLOADER_ENV_VARS_PART,
		size:		BOOTLOADER_ENV_VARS_SIZE,
		offset:		0x1FE000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"All Flash Contents",
		size:		0x200000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
} ;
#elif 	defined( HAVE_8MB_FLASH )
static struct mtd_partition sc1445x_spi_flash_partitions_8MB[] = {
	{
		name:		"Loader",
		size:		0x001000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Bootloader",
		size:		0x01F000,
		offset:		0x001000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"uClinux Image",
		size:		0x700000,
		offset:		0x020000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"File System",
		size:		0x0DC000,
		offset:		0x720000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Manufacture Data",
		size:		0x002000,
		offset:		0x7FC000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		BOOTLOADER_ENV_VARS_PART,
		size:		BOOTLOADER_ENV_VARS_SIZE,
		offset:		0x7FE000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"All Flash Contents",
		size:		0x800000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
} ;
#else //4MB_FLASH
static struct mtd_partition sc1445x_spi_flash_partitions_4MB[] = {
	{
		name:		"Loader",
		size:		0x001000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Bootloader",
		size:		0x01F000,
		offset:		0x001000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"uClinux Image",
		size:		0x300000,
		offset:		0x020000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"File System",
		size:		0x0DC000,
		offset:		0x320000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Manufacture Data",
		size:		0x002000,
		offset:		0x3FC000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		BOOTLOADER_ENV_VARS_PART,
		size:		BOOTLOADER_ENV_VARS_SIZE,
		offset:		0x3FE000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"All Flash Contents",
		size:		0x400000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
} ;
#endif


static struct flash_platform_data sc1445x_spi_flash_data = {
	.name = FLASH_NAME,
#if defined( HAVE_2MB_FLASH )
	.parts = sc1445x_spi_flash_partitions_2MB,
	.nr_parts = ARRAY_SIZE( sc1445x_spi_flash_partitions_2MB ),
#elif defined( HAVE_8MB_FLASH )
	.parts = sc1445x_spi_flash_partitions_8MB,
	.nr_parts = ARRAY_SIZE( sc1445x_spi_flash_partitions_8MB ),	
#else
	.parts = sc1445x_spi_flash_partitions_4MB,
	.nr_parts = ARRAY_SIZE( sc1445x_spi_flash_partitions_4MB ),
#endif
	.type = FLASH_TYPE,
} ;

#if defined( CONFIG_SC14450_VoIP_RevB ) && !defined( HAVE_2MB_FLASH )
static struct flash_platform_data sc1445x_spi_flash_data_alt = {
	.name = FLASH_NAME_ALT,
	.parts = sc1445x_spi_flash_partitions_4MB,
	.nr_parts = ARRAY_SIZE( sc1445x_spi_flash_partitions_4MB ),
	.type = FLASH_TYPE_ALT,
} ;
#endif

#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT )
	// #if (defined(CONFIG_ATA_1_FXS_NO_FXO) || defined(CONFIG_ATA_1_FXS_NO_FXO_1_CVM))
		// #define ATA_DEV_CNT 1
	// #elif 	(defined(CONFIG_ATA_2_FXS_NO_FXO) || defined(CONFIG_ATA_1_FXS_1_FXO) || defined(CONFIG_ATA_2_FXS_NO_FXO_1_CVM))
		// #define ATA_DEV_CNT 2
	// #elif 	(defined(CONFIG_ATA_3_FXS_NO_FXO) || defined(CONFIG_ATA_2_FXS_1_FXO))
		// #define ATA_DEV_CNT 3	
	// #elif 	(defined(CONFIG_ATA_4_FXS_NO_FXO) || defined(CONFIG_ATA_3_FXS_1_FXO) || defined(CONFIG_ATA_2_FXS_2_FXO))	
		// #define ATA_DEV_CNT 4
	// #else	
		// #define ATA_DEV_CNT 0
	// #endif

	/* majornum, deviceType, linecount, devId */
	#  if (ATA_DEV_CNT>0)
		static u8 sc1445x_spi_ata1_data[4] = { 240, VP_DEV_890_SERIES, ATA_DEV_LINES(0), 0 } ;
	#  endif
	#  if (ATA_DEV_CNT>1)
		static u8 sc1445x_spi_ata2_data[4] = { 241, VP_DEV_890_SERIES, ATA_DEV_LINES(1), 1 } ;
	#  endif
	#  if (ATA_DEV_CNT>2)
		static u8 sc1445x_spi_ata3_data[4] = { 242, VP_DEV_890_SERIES, ATA_DEV_LINES(2), 2 } ;
	#  endif
	#  if (ATA_DEV_CNT>3)
		static u8 sc1445x_spi_ata4_data[4] = { 243, VP_DEV_890_SERIES, ATA_DEV_LINES(3), 3 } ;
	#  endif
#endif

/* SPI device parameters */
static struct sc1445x_spi_chip sc1445x_spi_flash_chip_info = {
	.use_dma = 0,
	.word_width = 0,
	.cs_per_word = 0,
} ;

#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT ) 
static struct sc1445x_spi_chip sc1445x_spi_ata_chip_info = {
	.use_dma = 0,
	.word_width = 0,
	.cs_per_word = 1,
} ;
#endif

#if defined( CONFIG_SPI_REDPINE_RS9110_LI )
static struct sc1445x_spi_chip sc1445x_spi_wifi_chip_info = {
	.use_dma = 0,
	.word_width = 0,
	.cs_per_word = 0,
} ;
#endif

#if defined( CONFIG_CVM480_SPI_DECT_SUPPORT )
static struct sc1445x_spi_chip cvm480_spi_dect_chip_info = {
	.use_dma = 0,
#ifdef CONFIG_CATIQ_V2_SUPPORT	
	.word_width = 1,
#else
	.word_width = 0,
#endif	
	.cs_per_word = 1,
} ;
#endif //CONFIG_CVM480_SPI_DECT_SUPPORT

#if defined (CONFIG_SC1445x_ST7565P_LCD_SERIAL_SUPPORT)
static struct sc1445x_spi_chip lcd_spi_chip_info = {
	.use_dma = 0,
	.word_width = 0,
	.cs_per_word = 1,
} ;
#endif//CONFIG_SC1445x_ST7565P_LCD_SERIAL_SUPPORT

static struct spi_board_info spi_board_info[] __initdata = {
	{
		.modalias = FLASH_NAME,
		.max_speed_hz = 25000000,
		.bus_num = 0,
		.chip_select = 0,
		.irq = SPI2_INT,
		.platform_data = &sc1445x_spi_flash_data,
		.controller_data = &sc1445x_spi_flash_chip_info,
		.mode = SPI_MODE_3,
	},

#if defined( CONFIG_SC14450_VoIP_RevB ) && !defined( HAVE_2MB_FLASH )
	{
		.modalias = FLASH_NAME_ALT,
		.max_speed_hz = 25000000,
		.bus_num = 0,
		.chip_select = 7,
		.irq = SPI2_INT,
		.platform_data = &sc1445x_spi_flash_data_alt,
		.controller_data = &sc1445x_spi_flash_chip_info,
		.mode = SPI_MODE_3,
	},
#endif

#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT ) 
	#  if (ATA_DEV_CNT>0)
	{
		.modalias = "legerity_890",
		.max_speed_hz = 8192000,
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = 1,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = &sc1445x_spi_ata1_data,
		.controller_data = &sc1445x_spi_ata_chip_info,
		.mode = SPI_MODE_0,
	},
	#  endif
	#  if (ATA_DEV_CNT>1)
	{
		.modalias = "legerity_890",
		.max_speed_hz = 8192000,
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = ATA_DEV_LINES(0)+1,//2,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = &sc1445x_spi_ata2_data,
		.controller_data = &sc1445x_spi_ata_chip_info,
		.mode = SPI_MODE_0,
	},
	#  endif
	#  if (ATA_DEV_CNT>2)
	{
		.modalias = "legerity_890",
		.max_speed_hz = 8192000,
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = ATA_DEV_LINES(0)+ATA_DEV_LINES(1)+1,//3,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = &sc1445x_spi_ata3_data,
		.controller_data = &sc1445x_spi_ata_chip_info,
		.mode = SPI_MODE_0,
	},
	#  endif
	#  if (ATA_DEV_CNT>3)
	{
		.modalias = "legerity_890",
		.max_speed_hz = 8192000,
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = ATA_DEV_LINES(0)+ATA_DEV_LINES(1)+ATA_DEV_LINES(2)+1,//4,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = &sc1445x_spi_ata4_data,
		.controller_data = &sc1445x_spi_ata_chip_info,
		.mode = SPI_MODE_0,
	},
	#  endif
#endif  // CONFIG_SC1445x_LEGERITY_890_SUPPORT

#if defined( CONFIG_SPI_REDPINE_RS9110_LI )
	{
		.modalias = "rsisitel",
		.max_speed_hz = 15000000,
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = 5,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = NULL,
		.controller_data = &sc1445x_spi_wifi_chip_info,
		.mode = SPI_MODE_3,
	},
#endif

#if defined( CONFIG_CVM480_SPI_DECT_SUPPORT )
	{
		.modalias = "dect_cvm480_spi",
		.max_speed_hz = 4000000,//this is the maximum tested frequency
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = 6,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = NULL,
		.controller_data = &cvm480_spi_dect_chip_info,
		.mode = SPI_MODE_2,
	},
#endif //CONFIG_CVM480_SPI_DECT_SUPPORT

#if defined (CONFIG_SC1445x_ST7565P_LCD_SERIAL_SUPPORT)
	{
		.modalias = "lcd_sc14450",
		.max_speed_hz = 4000000,//this is the maximum tested frequency
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = 8,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = NULL,
		.controller_data = &lcd_spi_chip_info,
		.mode = SPI_MODE_3,
	},
#endif //CONFIG_SC1445x_ST7565P_LCD_SERIAL_SUPPORT
} ;

static struct resource sc1445x_spi0_resources[] = {
	{
		/* 4 registers */
		.start = (resource_size_t)&SPI2_CTRL_REG,
		.end   = (resource_size_t)(&SPI2_CTRL_REG + 4),
		.flags = IORESOURCE_MEM
	},
} ;

#if defined( CONFIG_SC14450_VoIP_RevB ) || defined (CONFIG_F_G2_P1_BOARD) || \
						defined (CONFIG_RPS_BOARD)
static struct resource sc1445x_spi1_resources[] = {
	{
		/* 4 registers */
		.start = (resource_size_t)&SPI1_CTRL_REG,
		.end   = (resource_size_t)(&SPI1_CTRL_REG + 4),
		.flags = IORESOURCE_MEM
	},
} ;
#endif

/* SPI bus 0 */
static struct platform_device sc1445x_spi0 = {
	.name = "sc1445x-spi",
	.id = 0,
	.resource = sc1445x_spi0_resources,
	.num_resources = ARRAY_SIZE( sc1445x_spi0_resources ),
	.dev = {
		.platform_data = NULL,	/* passed to driver */
	}
} ;

#if defined( CONFIG_SC14450_VoIP_RevB ) || defined (CONFIG_F_G2_P1_BOARD) || \
						defined (CONFIG_RPS_BOARD)
/* SPI bus 1 */
static struct platform_device sc1445x_spi1 = {
	.name = "sc1445x-spi",
	.id = 1,
	.resource = sc1445x_spi1_resources,
	.num_resources = ARRAY_SIZE( sc1445x_spi1_resources ),
	.dev = {
		.platform_data = NULL,	/* passed to driver */
	}
} ;
#endif

static struct platform_device* sc1445x_spi[] = {
	&sc1445x_spi0,
#if defined( CONFIG_SC14450_VoIP_RevB ) || defined (CONFIG_F_G2_P1_BOARD) || \
						defined (CONFIG_RPS_BOARD)
	&sc1445x_spi1,
#endif
} ;

static void sc1445x_port_setup(void){
#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT ) || defined (CONFIG_CVM480_DECT_SUPPORT) 
#ifdef ONFIG_PCM_MASTER
	SetPort(P0_02_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_FSC);       /* P0_02 is PCM_FSC */
	SetPort(P0_03_MODE_REG, GPIO_PUPD_IN_NONE,  GPIO_PID_PCM_DI);         /* P0_03 is PCM_DI */
	SetPort(P0_04_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_DO);         /* P0_04 is PCM_DO */
	SetPort(P0_05_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_CLK);        /* P0_05 is PCM_CLK */
#else
	SetPort(P0_02_MODE_REG, GPIO_PUPD_IN_NONE,  GPIO_PID_PCM_FSC);       /* P0_02 is PCM_FSC */
	SetPort(P0_03_MODE_REG, GPIO_PUPD_IN_NONE,  GPIO_PID_PCM_DI);         /* P0_03 is PCM_DI */
	SetPort(P0_04_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_DO);         /* P0_04 is PCM_DO */
	SetPort(P0_05_MODE_REG, GPIO_PUPD_IN_NONE,  GPIO_PID_PCM_CLK);        /* P0_05 is PCM_CLK */
#endif	
#endif	//CONFIG_SC1445x_LEGERITY_890_SUPPORT
}


void sc1445x_mtd_notify_add( struct mtd_info* mtd )
{
	size_t retlen = 0 ;
	int ret ;
	int len = BOOTLOADER_ENV_VARS_SIZE ;
	char* buf ;
	int pos = 0 ;
	char* var = "reboot_on_crash=" ;
	int n ;
	char* p ;
	char* b ;

	if( strcmp( mtd->name, BOOTLOADER_ENV_VARS_PART ) )
		return ;

	buf = kmalloc( BOOTLOADER_ENV_VARS_SIZE, GFP_KERNEL ) ;
	if( !buf ) {
		printk( "%s: cannot allocate memory\n", __FUNCTION__ ) ;
		return ;
	}

	do {
		ret = mtd->read( mtd, pos, len, &retlen, buf + pos ) ;
		if( ret ) {
			printk( "%s: MTD read() returned %d\n",
							__FUNCTION__, ret ) ;
			kfree( buf ) ;
			return ;
		}

		pos += retlen ;
		len -= retlen ;
	} while( len ) ;

	b = buf + 4 ;  /* skip CRC */
	n = BOOTLOADER_ENV_VARS_SIZE - 4 ;
	len = strlen( var ) ;
	ret = 1 ;  /* will reboot on crash */

	/* look for var inside buf, which is an array of strings */
	do {
		p = strstr( b, var ) ;
		if( NULL == p ) {
			p = memchr( b, 0, n ) ;
			if( NULL == p )
				break ;
			if( 0 == p[1] )
				break ;  /* 2 consecutive 0's -> end of array */
			n -= ++p - b ;
			b = p ;
		} else {
			p += len ;
			break ;
		}
	} while( n > 0 ) ;

	if( p  &&  ( 'n' == *p  ||  'N' == *p ) )
		ret = 0 ;

	reboot_on_crash = ret ;
	printk( "will %sreboot on crash\n", reboot_on_crash ?  "" :  "NOT " ) ;

	kfree( buf ) ;
}

void sc1445x_mtd_notify_remove( struct mtd_info* mtd )
{
	/* don't care */
}

static struct mtd_notifier sc1445x_mtd_notifier = {
	.add = sc1445x_mtd_notify_add,
	.remove = sc1445x_mtd_notify_remove,
};

static int __init sc1445x_dev_init( void )
{
	int res = 0 ;

	res = platform_add_devices( sc1445x_spi, ARRAY_SIZE( sc1445x_spi ) ) ;
	if( res < 0 ) {
		printk( "could not register SPI device(s) (%d)\n", res ) ;
		return res ;
	}

	if( ARRAY_SIZE( spi_board_info ) ) {
		res = spi_register_board_info( spi_board_info,
					ARRAY_SIZE( spi_board_info ) ) ;
	}
	sc1445x_port_setup();

	register_mtd_user( &sc1445x_mtd_notifier ) ;

	return res ;
}

arch_initcall( sc1445x_dev_init ) ;

