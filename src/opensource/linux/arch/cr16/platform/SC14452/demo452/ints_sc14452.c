/*
 * linux/arch/cr16/platform/SC14452/demo450/ints_sc14452.c
 * Interrupt handling CPU variants
 *
 *  Peter Griffin pgriffin@mpc-data.co.uk
 * based on:
 * Yoshinori Sato <ysato@users.sourceforge.jp>
 *
 */

#include <linux/init.h>
#include <linux/errno.h>
#include <linux/kernel.h>

#include <asm/ptrace.h>
#include <asm/traps.h>
#include <asm/irq.h>
#include <asm/io.h>
#include <asm/gpio.h>
#include <asm/regs.h>

/* trap entry table */
const unsigned long __initdata cr16_trap_table[NR_TRAPS]={
	0,0,0,0,0,
	(unsigned long)trace_break,  /* TRACE */
	0,0,
	(unsigned long)system_call,  /* TRAPA #0 */
	0,0,0,0,0,0
};

