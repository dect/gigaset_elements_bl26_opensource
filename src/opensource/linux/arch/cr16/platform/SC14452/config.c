/* do some system initialization */

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/param.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/physmap.h>
#include <linux/spi/spi.h>
#include <linux/spi/flash.h>
#include <linux/leds.h>
#include <asm/spi.h>
#include <asm/config.h>
#include <asm/sc14452_led.h>
#include <linux/gpio_keyboard.h>
#include <linux/input.h>
#include <stddef.h>

#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT )
#  include <../drivers/char/sc1445x_ata/arch/cr16/vp_api_types.h>
#  include <../drivers/char/sc1445x_ata/api_lib/includes/vp_api_dev_term.h>
#endif


#if defined( CONFIG_SC14452_DK_REV_A ) || defined (CONFIG_SC14452_DK_REV_C) \
	|| defined (CONFIG_L_BOARDS) || defined( CONFIG_VT_BOARDS) || defined( CONFIG_IN_BOARDS)\
	|| defined( CONFIG_SMG_BOARDS ) || defined( CONFIG_JW_BOARDS ) \
	|| defined( CONFIG_SC14452_ULE_DK_BOARDS )

#  define FLASH_NAME		"m25p80"

#  if (defined (CONFIG_L_BOARDS) || defined (CONFIG_VT_V1_BOARD) || defined (CONFIG_VT_DT_BOARD) || defined (CONFIG_VT_MS20_CNSL_BOARD) || defined (CONFIG_IN_V1_BOARD) || defined (CONFIG_JW_V1_BOARD))

/* flash type will be found using jedec probe */
#    define FLASH_TYPE		NULL  /* "e25q32" */

#  else

#    ifdef CONFIG_SC1445x_8MB_FLASH
#	define FLASH_TYPE	"MX25L6405D/MX25L6445E"
//#	define FLASH_TYPE	"w25q64"
#    else
#	define FLASH_TYPE	"w25q32"
#    endif

#  endif

#  define PERIPH_SPI_BUS	1
#  define PERIPH_SPI_INT	SPI1_AD_INT

#elif defined( CONFIG_F_G2_P2_2PORT_BOARD ) \
	||  defined( CONFIG_F_G2_P2_1PORT_BOARD ) \
	||  defined( CONFIG_F_G2_ST7567_2PORT_BOARD)

#  define FLASH_NAME		"m25p80"
#  define FLASH_TYPE		"MX25L3205D"
#  define PERIPH_SPI_BUS	1
#  define PERIPH_SPI_INT	SPI1_AD_INT

#elif defined( CONFIG_G_MERKUR_BOARDS )

#  define FLASH_NAME		"m25p80"
#  define FLASH_TYPE		"m25px64"
#  define PERIPH_SPI_BUS	1
#  define PERIPH_SPI_INT	SPI1_AD_INT

#else

	#error "Unknown configuration!"

#endif

#define BOOTLOADER_ENV_VARS_PART	"Env"
#define BOOTLOADER_ENV_VARS_SIZE	0x2000

int reboot_on_crash = 1 ;


#if defined( CONFIG_G_MERKUR_BOARDS )

static struct mtd_partition sc1445x_spi_flash_partitions_8MB[] = {
	{
		name:		"Loader",
		size:		0x001000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Bootloader",
		size:		0x01F000,
		offset:		0x001000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"uClinux Image",
		size:		0x6E0000,
		offset:		0x020000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"File System",
		size:		0x0F0000,
		offset:		0x700000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Manufacture Data & Enviroment Variables",
		size:		0x010000,
		offset:		0x7F0000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"All Flash Contents",
		size:		0x800000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
} ;

#elif ((defined (CONFIG_SC1445x_8MB_FLASH)) && (defined (CONFIG_VT_V1_BOARD) || defined (CONFIG_VT_DT_BOARD) || defined (CONFIG_VT_MS20_CNSL_BOARD)))

static struct mtd_partition sc1445x_spi_flash_partitions_8MB[] = {
	{
		name:			"Loader",
		size:			0x001000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:			"Bootloader",
		size:			0x01F000,
		offset:		0x001000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:			"uClinux Image",
		size:			0x380000,
		offset:		0x020000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:			"File System",
		size:			0x0DC000,
		offset:		0x3A0000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:			"Manufacture Data",
		size:			0x002000,
		offset:		0x47C000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:			BOOTLOADER_ENV_VARS_PART,
		size:			BOOTLOADER_ENV_VARS_SIZE,
		offset:		0x7FE000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:			"All Flash Contents",
		size:			0x800000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:			"Secondary uClinux Image",
		size:			0x380000,
		offset:		0x47E000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
};

#elif ((defined( CONFIG_SC1445x_8MB_FLASH )) && (defined (CONFIG_IN_V1_BOARD )))

static struct mtd_partition sc1445x_spi_flash_partitions_8MB[] = {
	{
		name:		"Loader",
		size:		0x001000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Bootloader",
		size:		0x01F000,
		offset:		0x001000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"uClinux Image",
		size:		0x380000,
		offset:		0x020000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"File System",
		size:		0x0DC000,
		offset:		0x3A0000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Manufacture Data",
		size:		0x002000,
		offset:		0x47C000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		BOOTLOADER_ENV_VARS_PART,
		size:		BOOTLOADER_ENV_VARS_SIZE,
		offset:		0x7FE000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"All Flash Contents",
		size:		0x800000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Secondary uClinux Image",
		size:		0x380000,
		offset:		0x47E000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
};

#elif (defined( CONFIG_SC1445x_8MB_FLASH ) &&  defined (CONFIG_L_BOARDS))

static struct mtd_partition sc1445x_spi_flash_partitions_8MB[] = {
	{
		name:		"Loader",
		size:		0x001000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Bootloader",
		size:		0x01F000,
		offset:		0x001000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"uClinux Image",
#if defined (CONFIG_L_V2_BOARD)||defined (CONFIG_L_V3_BOARD)||defined (CONFIG_L_V4_BOARD)||defined (CONFIG_L_V5_BOARD)\
	||defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)
		size:		0x360000,
#else
  		size:		0x1B0000,
#endif
		offset:		0x020000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"File System",
		size:		0x47C000,
		offset:		0x380000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Manufacture Data",
		size:		0x002000,
		offset:		0x7FC000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		BOOTLOADER_ENV_VARS_PART,
		size:		BOOTLOADER_ENV_VARS_SIZE,
		offset:		0x7FE000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */ //IP8802_Porting_lmh_20100514_setmac
	},
	{
		name:		"All Flash Contents",
		size:		0x800000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Loader-Bootloader Upgrade",
		size:		0x020000,
		offset:		0x000000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
#if defined (CONFIG_L_V1_BOARD)
	{
		name:		"uClinux Backup Image",
  		size:		0x1B0000,
		offset:		0x1D0000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
#endif
} ;
#elif defined ( CONFIG_SC1445x_8MB_FLASH )

static struct mtd_partition sc1445x_spi_flash_partitions_8MB[] = {
	{
		name:		"Loader",
		size:		0x001000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Bootloader",
		size:		0x01F000,
		offset:		0x001000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Recovery",
		size:		0x0C8000,
		offset:		0x020000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Linux1",
		size:		0x271000,
		offset:		0x0e8000,
		//RW mask_flags:	MTD_WRITEABLE,
	},
	{
		name:		"FS1",
		size:		0x116000,
		offset:		0x359000,
		//RW mask_flags:	MTD_WRITEABLE,
	},
	{
		name:		"Linux2",
		size:		0x271000,
		offset:		0x46F000,
		//RW mask_flags:	MTD_WRITEABLE,
	},
	{
		name:		"FS2",
		size:		0x116000,
		offset:		0x6E0000,
		//RW mask_flags:	MTD_WRITEABLE,
	},
	{
		name:		"Factory",
		size:		0x008000,
		offset:		0x7F6000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		BOOTLOADER_ENV_VARS_PART,
		size:		BOOTLOADER_ENV_VARS_SIZE,
		offset:		0x7FE000,
		//RW mask_flags:	MTD_WRITEABLE,	/* RW to change env from userspace */
	},
	{
		name:		"All Flash Contents",
		size:		0x800000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,	/* mask MTD_WRITEABLE = read only */
	},
	{
		name:		"Firmware1",
		size:		0x387000,
		offset:		0x0E8000,
	},
	{
		name:		"Firmware2",
		size:		0x387000,
		offset:		0x46F000,
	},
	{
		name:		"Crashdump",
		size:		0x001000,
		offset:		0x7FE000,
	},
};

#else //4MB_FLASH

static struct mtd_partition sc1445x_spi_flash_partitions_4MB[] = {
	{
		name:		"Loader",
		size:		0x001000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Bootloader",
		size:		0x01F000,
		offset:		0x001000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"uClinux Image",
		size:		0x25C000,
		offset:		0x020000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"File System",
		size:		0x180000,
		offset:		0x27C000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"Manufacture Data",
		size:		0x002000,
		offset:		0x3FC000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		BOOTLOADER_ENV_VARS_PART,
		size:		BOOTLOADER_ENV_VARS_SIZE,
		offset:		0x3FE000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
	{
		name:		"All Flash Contents",
		size:		0x400000,
		offset:		0x000000,
		mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
#if defined (CONFIG_L_V1_BOARD)
	{
		name:		"Loader-Bootloader Upgrade",
		size:		0x020000,
		offset:		0x000000,
		//mask_flags:	MTD_WRITEABLE,  /* force read-only */
	},
#endif
} ;

#endif

static struct flash_platform_data sc1445x_spi_flash_data = {
	.name = FLASH_NAME,
#if defined( CONFIG_SC1445x_8MB_FLASH ) || defined( CONFIG_G_MERKUR_BOARDS )
	.parts = sc1445x_spi_flash_partitions_8MB,
	.nr_parts = ARRAY_SIZE( sc1445x_spi_flash_partitions_8MB ),
#else
	.parts = sc1445x_spi_flash_partitions_4MB,
	.nr_parts = ARRAY_SIZE( sc1445x_spi_flash_partitions_4MB ),
#endif
	.type = FLASH_TYPE,
} ;



#ifdef CONFIG_MTD_W25QXX
static struct flash_platform_data sc14452_qspi_flash_data = {
	.name = "w25q32",
#if defined( CONFIG_SC1445x_8MB_FLASH ) || defined( CONFIG_G_MERKUR_BOARDS )
	.parts = sc1445x_spi_flash_partitions_8MB,
	.nr_parts = ARRAY_SIZE( sc1445x_spi_flash_partitions_8MB ),
#else
	.parts = sc1445x_spi_flash_partitions_4MB,
	.nr_parts = ARRAY_SIZE( sc1445x_spi_flash_partitions_4MB ),
#endif
	.type = FLASH_TYPE,
} ;
#endif


#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT )
	/* majornum, deviceType, linecount, devId */
	#  if (ATA_DEV_CNT>0)
		static u8 sc1445x_spi_ata1_data[4] = { 240, VP_DEV_890_SERIES, ATA_DEV_LINES(0), 0 } ;
	#  endif
	#  if (ATA_DEV_CNT>1)
		static u8 sc1445x_spi_ata2_data[4] = { 241, VP_DEV_890_SERIES, ATA_DEV_LINES(1), 1 } ;
	#  endif
	#  if (ATA_DEV_CNT>2)
		static u8 sc1445x_spi_ata3_data[4] = { 242, VP_DEV_890_SERIES, ATA_DEV_LINES(2), 2 } ;
	#  endif
	#  if (ATA_DEV_CNT>3)
		static u8 sc1445x_spi_ata4_data[4] = { 243, VP_DEV_890_SERIES, ATA_DEV_LINES(3), 3 } ;
	#  endif
#endif

/* SPI device parameters */
static struct sc1445x_spi_chip sc1445x_spi_flash_chip_info = {
	.use_dma = 0,
	.word_width = 0,
	.cs_per_word = 0,
} ;

#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT )
static struct sc1445x_spi_chip sc1445x_spi_ata_chip_info = {
	.use_dma = 0,
	.word_width = 0,
	.cs_per_word = 1,
} ;
#endif

#if defined( CONFIG_CVM480_SPI_DECT_SUPPORT )
static struct sc1445x_spi_chip cvm480_spi_dect_chip_info = {
	.use_dma = 0,
#ifdef CONFIG_CATIQ_V2_SUPPORT
	.word_width = 1,
#else
	.word_width = 0,
#endif
	.cs_per_word = 1,
} ;
#endif //CONFIG_CVM480_SPI_DECT_SUPPORT

#if defined (CONFIG_SC1445x_ST7565P_LCD_SERIAL_SUPPORT) || defined (CONFIG_SC1445x_NT75451_LCD_SERIAL_SUPPORT) || defined (CONFIG_SC1445x_ST7567_LCD_SERIAL_SUPPORT)|| defined(CONFIG_SC1445x_ST7567_LCD_SERIAL_SUPPORT_OPTIMIZED)
static struct sc1445x_spi_chip lcd_spi_chip_info = {
	.use_dma = 0,
	.word_width = 0,
	.cs_per_word = 1,
} ;
#endif//CONFIG_SC1445x_ST7565P_LCD_SERIAL_SUPPORT
#if defined (CONFIG_SC1445x_LED_SPI_SUPPORT)
static struct sc1445x_spi_chip led_spi_chip_info = {
	.use_dma = 0,
	.word_width = 0,
	.cs_per_word = 0,
} ;
#endif
#if defined (CONFIG_SC1445x_CNXT_SPI_SUPPORT)
static struct sc1445x_spi_chip cnxt_spi_chip_info = {
	.use_dma = 0,
	.word_width = 0,
	.cs_per_word = 0,
} ;
#endif
#if defined (CONFIG_L_V6_BOARD)
static struct sc1445x_spi_chip vega_spi_chip_info = {
	.use_dma = 0,
	.word_width = 0,
	.cs_per_word = 0,
} ;
#endif

static struct spi_board_info spi_board_info[] __initdata = {
	{
		.modalias = FLASH_NAME,
		.max_speed_hz = 25000000,
		.bus_num = 0,
		.chip_select = 0,
		.irq = SPI2_INT,
		.platform_data = &sc1445x_spi_flash_data,
		.controller_data = &sc1445x_spi_flash_chip_info,
		.mode = SPI_MODE_3,
	},

#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT )
	#  if (ATA_DEV_CNT>0)
	{
		.modalias = "legerity_890",
		.max_speed_hz = 8192000,
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = 1,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = &sc1445x_spi_ata1_data,
		.controller_data = &sc1445x_spi_ata_chip_info,
		.mode = SPI_MODE_0,
	},
	#  endif
	#  if (ATA_DEV_CNT>1)
	{
		.modalias = "legerity_890",
		.max_speed_hz = 8192000,
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = ATA_DEV_LINES(0)+1,//2,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = &sc1445x_spi_ata2_data,
		.controller_data = &sc1445x_spi_ata_chip_info,
		.mode = SPI_MODE_0,
	},
	#  endif
	#  if (ATA_DEV_CNT>2)
	{
		.modalias = "legerity_890",
		.max_speed_hz = 8192000,
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = ATA_DEV_LINES(0)+ATA_DEV_LINES(1)+1,//3,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = &sc1445x_spi_ata3_data,
		.controller_data = &sc1445x_spi_ata_chip_info,
		.mode = SPI_MODE_0,
	},
	#  endif
	#  if (ATA_DEV_CNT>3)
	{
		.modalias = "legerity_890",
		.max_speed_hz = 8192000,
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = ATA_DEV_LINES(0)+ATA_DEV_LINES(1)+ATA_DEV_LINES(2)+1,//4,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = &sc1445x_spi_ata4_data,
		.controller_data = &sc1445x_spi_ata_chip_info,
		.mode = SPI_MODE_0,
	},
	#  endif
#endif  // CONFIG_SC1445x_LEGERITY_890_SUPPORT

#if defined( CONFIG_CVM480_SPI_DECT_SUPPORT )
	{
		.modalias = "dect_cvm480_spi",
		.max_speed_hz = 4000000,//this is the maximum tested frequency
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = 6,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = NULL,
		.controller_data = &cvm480_spi_dect_chip_info,
		.mode = SPI_MODE_2,
	},
#endif //CONFIG_CVM480_SPI_DECT_SUPPORT

#if defined (CONFIG_SC1445x_ST7565P_LCD_SERIAL_SUPPORT) || defined (CONFIG_SC1445x_NT75451_LCD_SERIAL_SUPPORT) || defined (CONFIG_SC1445x_ST7567_LCD_SERIAL_SUPPORT)|| defined(CONFIG_SC1445x_ST7567_LCD_SERIAL_SUPPORT_OPTIMIZED)
	{
		.modalias = "lcd_sc14450",
		.max_speed_hz = 4000000,//this is the maximum tested frequency
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = 8,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = NULL,
		.controller_data = &lcd_spi_chip_info,
		.mode = SPI_MODE_3,
	},
#endif //CONFIG_SC1445x_ST7565P_LCD_SERIAL_SUPPORT
#if defined (CONFIG_SC1445x_LED_SPI_SUPPORT)
	{
		.modalias = "led_spi",
		.max_speed_hz = 4000000,//this is the maximum tested frequency
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = 9,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = NULL,
		.controller_data = &led_spi_chip_info,
		.mode = SPI_MODE_0,
	},
#endif //CONFIG_SC1445x_LED_SPI_SUPPORT
#if defined (CONFIG_SC1445x_CNXT_SPI_SUPPORT)
	{
		.modalias = "cnxt_spi",
		.max_speed_hz = 4000000,//this is the maximum tested frequency
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = 10,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = NULL,
		.controller_data = &cnxt_spi_chip_info,
		.mode = SPI_MODE_0,
	},
#endif //CONFIG_SC1445x_cnxt_SPI_SUPPORT
#if defined (CONFIG_L_V6_BOARD)
	{
		.modalias = "vega_spi",
		.max_speed_hz = 4000000,//this is the maximum tested frequency
		.bus_num = PERIPH_SPI_BUS,
		.chip_select = 11,
		.irq = PERIPH_SPI_INT,//SPI2_INT,
		.platform_data = NULL,
		.controller_data = &vega_spi_chip_info,
		.mode = SPI_MODE_0,
	},
#endif //CONFIG_SC1445x_cnxt_SPI_SUPPORT


} ;

static struct resource sc1445x_spi0_resources[] = {
	{
		/* 5 registers */
		.start = (resource_size_t)&SPI2_CTRL_REG0,
		.end   = (resource_size_t)(&SPI2_CTRL_REG0 + 5),
		.flags = IORESOURCE_MEM
	},
} ;

static struct resource sc1445x_spi1_resources[] = {
	{
		/* 5 registers */
		.start = (resource_size_t)&SPI1_CTRL_REG0,
		.end   = (resource_size_t)(&SPI1_CTRL_REG0 + 5),
		.flags = IORESOURCE_MEM
	},
} ;

/* SPI bus 0 */
static struct platform_device sc1445x_spi0 = {
	.name = "sc1445x-spi",
	.id = 0,
	.resource = sc1445x_spi0_resources,
	.num_resources = ARRAY_SIZE( sc1445x_spi0_resources ),
	.dev = {
		.platform_data = NULL,	/* passed to driver */
	}
} ;

/* SPI bus 1 */
static struct platform_device sc1445x_spi1 = {
	.name = "sc1445x-spi",
	.id = 1,
	.resource = sc1445x_spi1_resources,
	.num_resources = ARRAY_SIZE( sc1445x_spi1_resources ),
	.dev = {
		.platform_data = NULL,	/* passed to driver */
	}
} ;




#ifdef CONFIG_MTD_W25QXX
/* QSPI flash */
static struct platform_device w25qspi = {
	.name = "w25q_qspi",
	.id = 0,
	.resource = NULL,
	.num_resources = 0,
	.dev = {
		.platform_data = &sc14452_qspi_flash_data,	/* passed to driver */
	}
} ;
#endif


static struct platform_device* sc1445x_spi[] = {
#ifdef CONFIG_MTD_W25QXX
	&sc1445x_spi1,
	&w25qspi,
#else
	&sc1445x_spi0,
	&sc1445x_spi1,
#endif

} ;

static void sc1445x_port_setup(void){
#if defined(CONFIG_L_V2_BOARD_CNX)||defined(CONFIG_L_V3_BOARD_CNX)||defined(CONFIG_L_V4_BOARD_CNX)||defined(CONFIG_L_V5_BOARD_CNX)

	SetPort(P1_14_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_FSC);       /* P1_14 is PCM_FSC */
	SetPort(P1_15_MODE_REG, GPIO_PUPD_IN_NONE,  GPIO_PID_PCM_DI);         /* P1_15 is PCM_DI */
	SetPort(P1_00_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_DO);         /* P1_00 is PCM_DO */
	SetPort(P1_01_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_CLK);        /* P1_01 is PCM_CLK */

	P2_RESET_DATA_REG = (1<<0);
	SetPort(P2_00_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_port);        /* CLOSE THE CLASSD FUNCTION */\
	P2_RESET_DATA_REG = (1<<1);
	SetPort(P2_01_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_port);    /* CLOSE THE CLASSD FUNCTION */

#endif

#if defined(CONFIG_L_V6_BOARD)

	SetPort(P2_10_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_FSC);       /* P1_14 is PCM_FSC */
	SetPort(P1_15_MODE_REG, GPIO_PUPD_IN_NONE,  GPIO_PID_PCM_DI);         /* P1_15 is PCM_DI */
	SetPort(P1_00_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_DO);         /* P1_00 is PCM_DO */
	SetPort(P2_09_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_CLK);        /* P1_01 is PCM_CLK */

#endif


#if defined( CONFIG_SC1445x_LEGERITY_890_SUPPORT ) || defined (CONFIG_CVM480_DECT_SUPPORT)
#ifdef CONFIG_PCM_MASTER
	SetPort(P1_14_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_FSC);       /* P1_14 is PCM_FSC */
	#  ifdef CONFIG_RAM_SIZE_32MB
		SetPort(P0_07_MODE_REG, GPIO_PUPD_IN_NONE,  GPIO_PID_PCM_DI);         /* P0_07 is PCM_DI */
		EBI_ACS1_LOW_REG = 0x1080000;  /* override bootloader setting */
	#  elif defined (CONFIG_SC14452_ULE_DK_BOARDS)
		SetPort(P0_07_MODE_REG, GPIO_PUPD_IN_NONE,  GPIO_PID_PCM_DI);         /* P0_07 is PCM_DI */
	#  else
		SetPort(P2_07_MODE_REG, GPIO_PUPD_IN_NONE,  GPIO_PID_PCM_DI);         /* P2_07 is PCM_DI */
	#  endif
	SetPort(P2_09_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_CLK);        /* P2_09 is PCM_CLK */

    #ifdef CONFIG_VT_DT_BOARD
    /* GPIO for SP100v Vtech */
	   SetPort(P2_08_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_PCM_DO);       /* For SP100Vtech P2_08 is PCM_DO */
	   SetPort(P2_10_MODE_REG, GPIO_PUPD_IN_NONE, GPIO_PID_INT5n);        /* P2_10 is INT5n from FXS to sc1445x*/
	   SetPort(P2_05_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_port );         /* P2_05 is RST from sc1445x to FXS */
	   SetPort(P2_06_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_port );         /* P2_06 is SPI CS0  from sc1445x to FXS */
	   SetPort(P2_02_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_port );         /* P2_02 is SPI CS1  from sc1445x to FXS */
	   //SetPort(P1_01_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_port ); /* P1_01 is LMX4180 reset line. No need to configure it is done in Bootloader */
	   P2_SET_DATA_REG = 0x20;  // Enable P2_05, RST
    #elif defined (CONFIG_VT_MS20_CNSL_BOARD)
       //MS20_4FXO
       SetPort(P2_02_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_port );         /* P2_02 is SPI CS0  from sc1445x to FXS */
       P2_SET_DATA_REG = 0x4; // Set P2_02
       SetPort(P1_02_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_port );         /* P1_02 is SPI CS1  from sc1445x to FXS */
       P1_SET_DATA_REG = 0x4; // Set P1_02
       SetPort(P0_09_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_port );         /* P0_09 is SPI CS2  from sc1445x to FXS */
       P0_SET_DATA_REG = 0x200; // Set P0_09
       SetPort(P0_10_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_port );         /* P0_10 is SPI CS3  from sc1445x to FXS */
       P0_SET_DATA_REG = 0x400; // Set P0_10

       SetPort(P2_12_MODE_REG, GPIO_PUPD_OUT_NONE, GPIO_PID_port );         /* P2_12 is RF_RSTN  */
       SetPort(P2_04_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_DO);         /* P1_00 is PCM_DO */
       SetPort(P2_05_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_CLK);        /* P1_01 is PCM_CLK */
       SetPort(P0_07_MODE_REG, GPIO_PUPD_IN_NONE,  GPIO_PID_PCM_DI);         /* P2_07 is PCM_DI */
    #else
	   SetPort(P1_00_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_DO);         /* P1_00 is PCM_DO */
    #endif
#else//PCM slave
	SetPort(P1_14_MODE_REG, GPIO_PUPD_IN_NONE,  GPIO_PID_PCM_FSC);       /* P1_14 is PCM_FSC */
	#  ifdef CONFIG_RAM_SIZE_32MB
		SetPort(P0_07_MODE_REG, GPIO_PUPD_IN_NONE,  GPIO_PID_PCM_DI);         /* P0_07 is PCM_DI */
		EBI_ACS1_LOW_REG = 0x1080000;  /* override bootloader setting */
	#  elif defined (CONFIG_SC14452_ULE_DK_BOARDS)
		SetPort(P0_07_MODE_REG, GPIO_PUPD_IN_NONE,  GPIO_PID_PCM_DI);         /* P0_07 is PCM_DI */
	#  else
		SetPort(P2_07_MODE_REG, GPIO_PUPD_IN_NONE,  GPIO_PID_PCM_DI);         /* P2_07 is PCM_DI */
	#  endif
	SetPort(P1_00_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_PCM_DO);         /* P1_00 is PCM_DO */
	SetPort(P2_09_MODE_REG, GPIO_PUPD_IN_NONE,  GPIO_PID_PCM_CLK);        /* P2_09 is PCM_CLK */
#endif
#endif	//CONFIG_SC1445x_LEGERITY_890_SUPPORT

#if defined( CONFIG_SC14452_DIALOG_BOARD_LED )
	SetPort(P0_10_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_port);
	SetPort(P2_08_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_port);
	SetPort(P2_11_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_port);
	SetPort(P2_12_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_port);
//	SetPort(P0_10_MODE_REG, GPIO_PUPD_OUT_NONE,  GPIO_PID_port);


#endif  //CONFIG_SC14452_DIALOG_BOARD_LED

}


/*
 * look for var inside buf, which is an array of strings
 * n is the max pos to search
 */
static const char* find_var( const char* buf, const char* var, int n )
{
	const char* p ;
	int len = strlen( var ) ;

	do {
		p = strstr( buf, var ) ;
		if( NULL == p ) {
			p = memchr( buf, 0, n ) ;
			if( NULL == p )
				break ;
			if( 0 == p[1] )
				break ;  /* 2 consecutive 0's -> end of array */
			n -= ++p - buf ;
			buf = p ;
		} else {
			p += len ;
			break ;
		}
	} while( n > 0 ) ;

	return p ;
}

#if defined( CONFIG_SC1445x_CONSOLE )
extern void sc1445x_uart_cons_fini( void ) ;
#endif
void sc1445x_mtd_notify_add( struct mtd_info* mtd )
{
	size_t retlen = 0 ;
	int ret ;
	int len = BOOTLOADER_ENV_VARS_SIZE ;
	char* buf ;
	int pos = 0 ;
	int n ;
	const char* p ;
	char* b ;

	if( strcmp( mtd->name, BOOTLOADER_ENV_VARS_PART ) )
		return ;

	buf = kmalloc( BOOTLOADER_ENV_VARS_SIZE, GFP_KERNEL ) ;
	if( !buf ) {
		printk( "%s: cannot allocate memory\n", __FUNCTION__ ) ;
		return ;
	}

	do {
		ret = mtd->read( mtd, pos, len, &retlen, buf + pos ) ;
		if( ret ) {
			printk( "%s: MTD read() returned %d\n",
							__FUNCTION__, ret ) ;
			kfree( buf ) ;
			return ;
		}

		pos += retlen ;
		len -= retlen ;
	} while( len ) ;

	b = buf + 4 ;  /* skip CRC */
	n = BOOTLOADER_ENV_VARS_SIZE - 4 ;
	p = find_var( b, "reboot_on_crash=", n ) ;

	if( p  &&  ( 'n' == *p  ||  'N' == *p ) )
		reboot_on_crash = 0 ;
	else
		reboot_on_crash = 1 ;
	printk( "will %sreboot on crash\n", reboot_on_crash ?  "" :  "NOT " ) ;

#if defined( CONFIG_SC1445x_CONSOLE )
	p = find_var( b, "op_mode=", n ) ;
	if( p  && ( '1' == *p ) )
		sc1445x_uart_cons_fini() ;
#endif

	kfree( buf ) ;
}


void sc1445x_mtd_notify_remove( struct mtd_info* mtd )
{
	/* don't care */
}

static struct mtd_notifier sc1445x_mtd_notifier = {
	.add = sc1445x_mtd_notify_add,
	.remove = sc1445x_mtd_notify_remove,
};




#if defined( CONFIG_SC14452_REEF_BOARD_LED )
// power LED (green)
static struct sc14452_led power_led_cfg = {
        .name = "power",
        .set_reg = &P2_SET_DATA_REG,
        .reset_reg = &P2_RESET_DATA_REG,
        .port = GPIO_11,
        .port_mode_reg = &P2_11_MODE_REG,
        .default_trigger = 0
};

// network LED (blue)
static struct sc14452_led network_led_cfg = {
        .name = "network",
        .set_reg = &P2_SET_DATA_REG,
        .reset_reg = &P2_RESET_DATA_REG,
        .port = GPIO_8,
        .port_mode_reg = &P2_08_MODE_REG,
        .default_trigger = 0
};

// cloud LED (yellow)
static struct sc14452_led cloud_led_cfg = {
        .name = "cloud",
        .set_reg = &P2_SET_DATA_REG,
        .reset_reg = &P2_RESET_DATA_REG,
        .port = GPIO_12,
        .port_mode_reg = &P2_12_MODE_REG,
        .default_trigger = 0
};
#endif






#if defined( CONFIG_SC14452_DIALOG_BOARD_LED )
static struct platform_device reg_led =
{
        .name = "sc14452-led",
        .id = 0,
        .resource = 0,
        .num_resources = 0,
        .dev = {
                .platform_data = &reg_led_cfg,
            }
};

static struct platform_device led0 =
{
        .name = "sc14452-led",
        .id = 1,
        .resource = 0,
        .num_resources = 0,
        .dev = {
                .platform_data = &led0_cfg,
            }
};

static struct platform_device led1 =
{
        .name = "sc14452-led",
        .id = 2,
        .resource = 0,
        .num_resources = 0,
        .dev = {
                .platform_data = &led1_cfg,
            }
};

static struct platform_device led2 =
{
        .name = "sc14452-led",
        .id = 3,
        .resource = 0,
        .num_resources = 0,
        .dev = {
                .platform_data = &led2_cfg,
            }
};

static struct platform_device led3 =
{
        .name = "sc14452-led",
        .id = 4,
        .resource = 0,
        .num_resources = 0,
        .dev = {
                .platform_data = &led3_cfg,
            }
};
#endif
#if defined( CONFIG_SC14452_REEF_BS_BOARD )
static struct platform_device power_led =
{
        .name = "sc14452-led",
        .id = 0,
        .resource = 0,
        .num_resources = 0,
        .dev = {
                .platform_data = &power_led_cfg,
            }
};

static struct platform_device network_led =
{
        .name = "sc14452-led",
        .id = 1,
        .resource = 0,
        .num_resources = 0,
        .dev = {
                .platform_data = &network_led_cfg,
            }
};

static struct platform_device cloud_led =
{
    .name = "sc14452-led",
    .id = 2,
    .resource = 0,
    .num_resources = 0,
    .dev =
    {
        .platform_data = &cloud_led_cfg,
    }
};

#endif

#if defined( CONFIG_SC14452_DIALOG_BOARD_LED )
static struct platform_device *sc14452_leds[] =
{
    &reg_led,
    &led0,
    &led1,
    &led2,
    &led3,
};
#endif

#if defined( CONFIG_SC14452_REEF_BS_BOARD )
static struct platform_device *sc14452_leds[] =
{
    &power_led,
    &network_led,
    &cloud_led,
};
#endif

#if defined( CONFIG_SC14452_REEF_BS_BOARD )
struct port_data
{
    volatile unsigned short *input_reg;
    unsigned short gpio;
    volatile unsigned short *mode_reg;
    int button;
};

static struct port_data page_button_data =
{
    .input_reg = &P2_DATA_REG,
    .gpio = GPIO_9,
    .mode_reg = &P2_09_MODE_REG,
    .button = BTN_0,
};

static int ReadGpio(void *data)
{
    struct port_data *cfg = (struct port_data*) data;

    return (*cfg->input_reg & cfg->gpio) ? 0 : 1; // this is pull up configuration, so 0 means that button is
                                                  // pressed
};

static int InitGpioToIn(void *data)
{
    struct port_data *cfg = (struct port_data*) data;

    SetPort(*cfg->mode_reg, GPIO_PUPD_IN_PULLUP, GPIO_PID_port);
    return 0;
};

static struct gpio_key page_key_cfg =
{
    .name = "paging",
    .private_data = &page_button_data,
    .read = ReadGpio,
    .init = InitGpioToIn
};

static struct platform_device page_key =
{
    .name = "gpio-key",
    .id = 0,
    .resource = 0,
    .num_resources = 0,
    .dev =
    {
        .platform_data = &page_key_cfg,
    }
};

static struct platform_device *buttons[] =
{
    &page_key,
};
#else
static struct platform_device *buttons[] =
{ };
#endif

static int __init sc1445x_dev_init(void)
{
    int res = 0;

    res = platform_add_devices(sc1445x_spi, ARRAY_SIZE(sc1445x_spi));
    if (res < 0)
    {
        printk("could not register SPI device(s) (%d)\n", res);
        return res;
    }

    if (ARRAY_SIZE(spi_board_info))
    {
        res = spi_register_board_info(spi_board_info,
                ARRAY_SIZE(spi_board_info));
    }
    sc1445x_port_setup();

    register_mtd_user(&sc1445x_mtd_notifier);

    res = platform_add_devices(sc14452_leds, ARRAY_SIZE(sc14452_leds));
    if (res < 0)
    {
        printk("could not register LED device(s), result %d\n", res);
        return res;
    }

    res = platform_add_devices(buttons, ARRAY_SIZE(buttons));
    if (res < 0)
    {
        printk("could not register BUTTON device(s), result %d\n", res);
        return res;
    }

    return res;
}
arch_initcall( sc1445x_dev_init );

