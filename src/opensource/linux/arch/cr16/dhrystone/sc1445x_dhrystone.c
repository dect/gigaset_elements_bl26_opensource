#include <linux/init.h>
#include <linux/module.h>
MODULE_LICENSE( "GPL" ) ;


extern void sc1445x_dhrystone_main( void ) ;

static int sc1445x_dhrystone_init( void )
{
	printk( "Starting SC1445x Dhrystone 'driver'\n" ) ;

	sc1445x_dhrystone_main() ;

	return 0 ;
}


static void sc1445x_dhrystone_exit( void )
{
	printk( "Finishing SC1445x Dhrystone 'driver'\n" ) ;
}


module_init( sc1445x_dhrystone_init ) ;
module_exit( sc1445x_dhrystone_exit ) ;
