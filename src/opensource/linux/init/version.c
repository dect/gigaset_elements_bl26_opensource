/*
 *  linux/init/version.c
 *
 *  Copyright (C) 1992  Theodore Ts'o
 *
 *  May be freely distributed as part of Linux.
 */

#define CONFIG_REEF_BS 1    //tmp solution todo: add to autoconfig

#include <linux/compile.h>
#include <linux/module.h>
#include <linux/uts.h>
#include <linux/utsname.h>
#include <linux/utsrelease.h>
#include <linux/version.h>
#include <linux/sched.h>
#ifdef CONFIG_SC1445x
#include <../sc14450_voip_version.h>
    #ifdef CONFIG_REEF_BS
    #include <../reef_version.h>
    #endif
#endif


#define version(a) Version_ ## a
#define version_string(a) version(a)

int version_string(LINUX_VERSION_CODE);

struct uts_namespace init_uts_ns = {
	.kref = {
		.refcount	= ATOMIC_INIT(2),
	},
	.name = {
		.sysname	= UTS_SYSNAME,
		.nodename	= UTS_NODENAME,
		.release	= UTS_RELEASE,
		.version	= UTS_VERSION,
		.machine	= UTS_MACHINE,
		.domainname	= UTS_DOMAINNAME,
	},
};
EXPORT_SYMBOL_GPL(init_uts_ns);

#if !defined( CONFIG_SC14450) && !defined (CONFIG_SC14452)
const char linux_banner[] =
	"Linux version " UTS_RELEASE " (" LINUX_COMPILE_BY "@"
	LINUX_COMPILE_HOST ") (" LINUX_COMPILER ") " UTS_VERSION "\n";
#else 
const char linux_banner[] =
	"Linux version " UTS_RELEASE " (" LINUX_COMPILE_BY "@"
	LINUX_COMPILE_HOST ") (" LINUX_COMPILER ") " UTS_VERSION "\n"
	"SC14450_VoIP version: " SC14450_VERSION " Tagged at: " SC14450_VERSION_DATE "\n"
	"Version Status: " SC14450_VERSION_STATUS " " SC14450_VERSION_LOCAL "\n"
#ifdef SC14450_LOCAL_COMPILATION
	"Local compilation\n"
#else
	"Repository compilation\n"
#endif
#ifdef CONFIG_REEF_BS
	"Reef BS version '" REEF_BAS_VERSION "' tagged at: '" REEF_BAS_VERSION_DATE "' version status: '" REEF_BAS_VERSION_STATUS "'\n"
	"Reef BS hash '" REEF_BAS_HASH "'\n"
	"based on build version '" REEF_BUILD_VERSION "' tagged at: '" REEF_BUILD_VERSION_DATE "' version status: '" REEF_BUILD_VERSION_STATUS "'\n"
#ifdef REEF_LOCAL_COMPILATION
	"Local compilation, no .git found\n"
#else
    "Repository compilation, .git found\n"
#endif
#endif

;
#endif
//
//Changes introduced by Gigaset Elements GmbH:
//Modification date:  2013-10-31 10:53:46.395897586
//@@ -6,6 +6,8 @@
//  *  May be freely distributed as part of Linux.
//  */
// 
//+#define CONFIG_REEF_BS 1    //tmp solution todo: add to autoconfig
//+
// #include <linux/compile.h>
// #include <linux/module.h>
// #include <linux/uts.h>
//@@ -13,6 +15,13 @@
// #include <linux/utsrelease.h>
// #include <linux/version.h>
// #include <linux/sched.h>
//+#ifdef CONFIG_SC1445x
//+#include <../sc14450_voip_version.h>
//+    #ifdef CONFIG_REEF_BS
//+    #include <../reef_version.h>
//+    #endif
//+#endif
//+
// 
// #define version(a) Version_ ## a
// #define version_string(a) version(a)
//@@ -34,6 +43,31 @@
// };
// EXPORT_SYMBOL_GPL(init_uts_ns);
// 
//+#if !defined( CONFIG_SC14450) && !defined (CONFIG_SC14452)
// const char linux_banner[] =
// 	"Linux version " UTS_RELEASE " (" LINUX_COMPILE_BY "@"
// 	LINUX_COMPILE_HOST ") (" LINUX_COMPILER ") " UTS_VERSION "\n";
//+#else 
//+const char linux_banner[] =
//+	"Linux version " UTS_RELEASE " (" LINUX_COMPILE_BY "@"
//+	LINUX_COMPILE_HOST ") (" LINUX_COMPILER ") " UTS_VERSION "\n"
//+	"SC14450_VoIP version: " SC14450_VERSION " Tagged at: " SC14450_VERSION_DATE "\n"
//+	"Version Status: " SC14450_VERSION_STATUS " " SC14450_VERSION_LOCAL "\n"
//+#ifdef SC14450_LOCAL_COMPILATION
//+	"Local compilation\n"
//+#else
//+	"Repository compilation\n"
//+#endif
//+#ifdef CONFIG_REEF_BS
//+	"Reef BS version '" REEF_BAS_VERSION "' tagged at: '" REEF_BAS_VERSION_DATE "' version status: '" REEF_BAS_VERSION_STATUS "'\n"
//+	"Reef BS hash '" REEF_BAS_HASH "'\n"
//+	"based on build version '" REEF_BUILD_VERSION "' tagged at: '" REEF_BUILD_VERSION_DATE "' version status: '" REEF_BUILD_VERSION_STATUS "'\n"
//+#ifdef REEF_LOCAL_COMPILATION
//+	"Local compilation, no .git found\n"
//+#else
//+    "Repository compilation, .git found\n"
//+#endif
//+#endif
//+
//+;
//+#endif
