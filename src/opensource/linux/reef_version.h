// File created by Gigaset Elements GmbH
// All rights reserved.
#define REEF_BUILD_VERSION  "build-483"
#define REEF_BUILD_VERSION_DATE  "2013-10-01 11:19:13 +0200"
#define REEF_BUILD_VERSION_STATUS  "NOT REPOSITORY VERSION"

#define REEF_BAS_VERSION  "bas-001.000.026"
#define REEF_BAS_VERSION_DATE  "unknown"
#define REEF_BAS_VERSION_STATUS  "NOT REPOSITORY VERSION"
#define REEF_BAS_HASH  "bd35bcc4d561e2d4e268c01eb21aa615bb45dff8"
