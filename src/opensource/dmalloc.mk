DMALLOC_DIR := dmalloc

##############################
# Build configuration for HW #
##############################


ifeq ($(BUILD),hw)

CFLAGS += $(CFLAGS_DYNAMIC)
LDFLAGS += $(LDFLAGS_DYNAMIC)

CFLAGS += -I$(PWD)/linux/ -DL_ENDIAN -DTERMIO  -DEMBED -D__UC_LIBC__ -D__uClinux__ -Wall -std=gnu99 -Wfatal-errors
LDLIBS += -Wl,-R,$(PTHREAD_GDB) 

#LDFLAGS+= -Wl,--verbose
LDFLAGS += -Wl,-Map,dmalloc.map
#LDFLAGS += -Wl,-M

#SHARED_CFLAGS += -fPIC -nostartfiles $(SHARED_LIB_START) 
#SHARED_LDFLAGS += -shared -Wl,-shared-lib-id,7 -Wl,-R,$(PTHREAD_GDB) 

export CFLAGS
export LDLIBS
export LDFLAGS
export SHARED_CFLAGS
export SHARED_LDFLAGS


all:
	cd $(DMALLOC_DIR) && \
	make all

install: all
	cd $(DMALLOC_DIR) && \
	make install
ifeq ($(DMALLOC_ENABLE),true)
	$(call install_on_root,$(TMP_ROOTFS)/bin/dmalloc,/bin)
endif


configure: 
	make clean && \
	cd $(DMALLOC_DIR) && \
	./configure_sc14452.sh $(TMP_ROOTFS)

endif	# HW configuration






##############################
# Build configuration for PC #
##############################

ifeq ($(BUILD),pc)

install: 
	cd $(DMALLOC_DIR) && \
	make -j $(JOBS) && \
	make install_sw

configure: 
	cd $(DMALLOC_DIR) && \
	./config_pc.sh $(TMP_ROOTFS) && \
	make clean
	

endif	# PC configuration



###################
# Commont targets #
###################
clean:
	@cd $(DMALLOC_DIR) && \
	if [ -e Makefile ]; then make clean; fi

distclean:
	@cd $(DMALLOC_DIR) && \
	if [ -e Makefile ]; then make distclean; fi

%:
	$(error Target "$@" is not supported)
	
	
.PHONY: clean configure %
