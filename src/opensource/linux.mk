

export CFLAGS := 
export LDFLAGS	:=


configure:
	cd linux && \
	make	ARCH=$(ARCH) \
			CROSS_COMPILE=$(CC_BARE) \
			CROSS=$(CC_BARE) \
			ROMFS_NODES= \
			XIP_ROMFS_DIR=$(ROOTFS) \
			oldconfig


vmlinux: $(OUT)
	cd linux && \
	make	ARCH=$(ARCH) \
			CROSS_COMPILE=$(CC_BARE) \
			CROSS=$(CC_BARE) \
			ROMFS_NODES= \
			XIP_ROMFS_DIR=$(ROOTFS) \
			-j $(JOBS) \
			vmlinux && \
	./mk_vmlinuz
	cp linux/vmlinuz $(OUT)

modules: $(OUT)
	cd linux && \
	make	ARCH=$(ARCH) \
			CROSS_COMPILE=$(CC_BARE) \
			CROSS=$(CC_BARE) \
			ROMFS_NODES= \
			XIP_ROMFS_DIR=$(ROOTFS) \
			-j $(JOBS) \
			modules

modules_install: $(OUT)
	rm -rf $(TMP_ROOTFS)/lib/modules &>/dev/zero
	cd linux && \
	make	ARCH=$(ARCH) \
			CROSS_COMPILE=$(CC_BARE) \
			CROSS=$(CC_BARE) \
			ROMFS_NODES= \
			XIP_ROMFS_DIR=$(ROOTFS) \
			INSTALL_MOD_PATH=$(TMP_ROOTFS) \
			ROMFS_NODES_SRC_DIR=$(ROOTFS) \
			-j $(JOBS) \
			modules_install
	cp linux/vmlinuz $(OUT)

move_modules_to_jffs:
	rm -rf $(DATAFS)/lib/modules
	mkdir -p $(DATAFS)/lib/modules
	mv -f $(ROOTFS)/lib/modules $(DATAFS)/lib && \
	ln -s /mnt/data/lib/modules $(ROOTFS)/lib/modules 


#configuration
menuconfig:
	cd linux && \
	make 	ARCH=$(ARCH) \
			ROMFS_NODES= \
			XIP_ROMFS_DIR=$(ROOTFS) \
			menuconfig 






# clean kernel
clean:
	cd linux && \
	make    ARCH=$(ARCH) \
        	CROSS_COMPILE=$(CC_BARE) \
        	ROMFS_NODES= \
			XIP_ROMFS_DIR=$(ROOTFS) \
			clean
			


mrproper:
	cd linux && \
	make    ARCH=$(ARCH) \
        	CROSS_COMPILE=$(CC_BARE) \
        	ROMFS_NODES= \
			XIP_ROMFS_DIR=$(ROOTFS) \
			mrproper
			



$(OUT):
	mkdir -p $(OUT)
	

%:
	$(error Target "$@" is not supported)
	
	
.PHONY: % vmlinux menuconfig clean mrproper
		 