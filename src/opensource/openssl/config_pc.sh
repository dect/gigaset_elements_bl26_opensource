#!/bin/bash
#./Configure uclinux-cr16-noopt --with-cryptodev no-shared --prefix=$PWD/release
#./Configure uclinux-cr16 --with-cryptodev no-shared --prefix=$PWD/release
#-DCR16_OPT
#
#PREFIX=$PWD/release
if [ -z "$1" ]; then
    echo "Please call $0 one argument (with target directory)"
    exit 1;
fi
PREFIX=$1
setarch i386 ./config no-shared no-dso -m32 \
			threads \
			no-hw \
			no-bf no-cast no-md2 no-mdc2 no-rc2 no-rc5 no-krb5 no-sse2 no-ec no-ecdh no-ecdsa no-idea \
			--prefix=$PREFIX
