#!/bin/bash
#./Configure uclinux-cr16-noopt --with-cryptodev no-shared --prefix=$PWD/release
#./Configure uclinux-cr16 --with-cryptodev no-shared --prefix=$PWD/release
#-DCR16_OPT
SC1445x_SYS_CONFIG=$PWD/../linux/
CFLAGS="-mcr16cplus -mdata-model=far -mint32 -DSC14450 -mid-shared-library  -fPIC   -DEMBED -D__UC_LIBC__ -D__uClinux__ -Wl,--static,-elf2flt=-z -Wl,--traditional-format" 	
LDFLAGS="-mint32 -mcr16cplus -mdata-model=far -DEMBED -mid-shared-library  -D__UC_LIBC__ -D__uClinux__  -fPIC -Wl,-static,-elf2flt -elf2flt=-s65535  " 
if [ -e $SC1445x_SYS_CONFIG ]
then
	. ../linux/.config
	
	if [ -n "$CONFIG_SC14452" ] 
	then
		if [ -n "$CONFIG_CRYPTO_SC14452" ] 
		then
				echo  "Crypto Driver is set"
				./Configure uclinux-cr16 --with-cryptodev shared --prefix=$PWD/release -DCR16_OPT
				echo "With Crypto Device"
			else
				echo  "	WARNING!! Using 452 without Crypto Driver"
				./Configure uclinux-cr16  no-shared --prefix=$PWD/release -DCR16_OPT
		fi
	else
				./Configure uclinux-cr16  no-shared --prefix=$PWD/release -DCR16_OPT
				echo "No Crypto Device"
	fi
	

fi




