mkdir -p temp
cd temp
rm -rf *
mkdir -p data
mkdir -p system/lib
cp ../libssl.so* system/lib
cp ../libcrypto.so* system/lib
cp ../apps/openssl data
#rm openssl_native.tar
tar cvf openssl_native.tar *

