#!/bin/bash
if [ -z "$1" ]; then
    echo "Please call $0 one argument (with target directory)"
    exit 1;
fi
PREFIX=$1
#PREFIX=/home/niziak/xbase/xbase_http_client/src/opensource/openssl/release
SC1445x_SYS_CONFIG=$PWD/../linux/

if [ -e $SC1445x_SYS_CONFIG ]
then
	. ../linux/.config
	
	echo "Checking for CONFIG_SC14452 in kernel tree... "
	if [ -n "$CONFIG_SC14452" ] 
	then
		if [ -n "$CONFIG_CRYPTO_SC14452" ] 
		then
		
				# Disabled switches:
				# 	no-hw		don't build support for hardware devices (using cryptodev is still possible)
				# 	no-err		don't include strings with error descriptions (only error codes will be displayed)

				echo  "Crypto Driver is set"
				echo # uClinux
				./Configure uclinux-cr16-static1 \
							no-shared \
							no-dso \
							threads \
							no-hw \
							no-bf no-cast no-md2 no-mdc2 no-rc2 no-rc5 no-krb5 no-sse2 no-ec no-ecdh no-ecdsa no-idea \
							-DSC14450 -DLITTLE_ENDIAN -DSC1445x_OPT \
							-DCR16_OPT --prefix=$PREFIX
#							-DCR16_PROF \
                                echo "With Crypto Dev"
				echo "With Crypto Device"
		else
				echo  "	WARNING!! Using 452 without Crypto Driver"
				exit
				./Configure uclinux-cr16  no-shared --prefix=$PWD/release -DCR16_OPT
		fi
	else
				echo "No Crypto Device"	
				exit
				./Configure uclinux-cr16  no-shared --prefix=$PWD/release -DCR16_OPT
				echo "No Crypto Device"
	fi
	

fi
