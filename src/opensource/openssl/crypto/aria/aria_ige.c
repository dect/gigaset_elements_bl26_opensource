/* crypto/aria/aria_ige.c -*- mode:C; c-file-style: "eay" -*- */
/* ====================================================================
 * Copyright (c) 2006 The OpenSSL Project.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. All advertising materials mentioning features or use of this
 *    software must display the following acknowledgment:
 *    "This product includes software developed by the OpenSSL Project
 *    for use in the OpenSSL Toolkit. (http://www.openssl.org/)"
 *
 * 4. The names "OpenSSL Toolkit" and "OpenSSL Project" must not be used to
 *    endorse or promote products derived from this software without
 *    prior written permission. For written permission, please contact
 *    openssl-core@openssl.org.
 *
 * 5. Products derived from this software may not be called "OpenSSL"
 *    nor may "OpenSSL" appear in their names without prior written
 *    permission of the OpenSSL Project.
 *
 * 6. Redistributions of any form whatsoever must retain the following
 *    acknowledgment:
 *    "This product includes software developed by the OpenSSL Project
 *    for use in the OpenSSL Toolkit (http://www.openssl.org/)"
 *
 * THIS SOFTWARE IS PROVIDED BY THE OpenSSL PROJECT ``AS IS'' AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE OpenSSL PROJECT OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * ====================================================================
 *
 */

#include "cryptlib.h"

#include <openssl/aria.h>
#include "aria_locl.h"

#define N_WORDS (ARIA_BLOCK_SIZE / sizeof(unsigned long))
typedef struct {
        unsigned long data[N_WORDS];
} aria_block_t;

/* XXX: probably some better way to do this */
#if defined(__i386__) || defined(__x86_64__)
#define UNALIGNED_MEMOPS_ARE_FAST 1
#else
#define UNALIGNED_MEMOPS_ARE_FAST 0
#endif

#if UNALIGNED_MEMOPS_ARE_FAST
#define load_block(d, s)        (d) = *(const aria_block_t *)(s)
#define store_block(d, s)       *(aria_block_t *)(d) = (s)
#else
#define load_block(d, s)        memcpy((d).data, (s), ARIA_BLOCK_SIZE)
#define store_block(d, s)       memcpy((d), (s).data, ARIA_BLOCK_SIZE)
#endif

/* N.B. The IV for this mode is _twice_ the block size */

void ARIA_ige_encrypt(const unsigned char *in, unsigned char *out,
					 size_t length, const ARIA_KEY *key,
					 unsigned char *ivec, const int enc)
	{
	size_t n;
	size_t len = length;

	OPENSSL_assert(in && out && key && ivec);
	OPENSSL_assert((ARIA_ENCRYPT == enc)||(ARIA_DECRYPT == enc));
	OPENSSL_assert((length%ARIA_BLOCK_SIZE) == 0);

	len = length / ARIA_BLOCK_SIZE;

	if (ARIA_ENCRYPT == enc)
		{
		if (in != out &&
		    (UNALIGNED_MEMOPS_ARE_FAST || ((size_t)in|(size_t)out|(size_t)ivec)%sizeof(long)==0))
			{
			aria_block_t *ivp = (aria_block_t *)ivec;
			aria_block_t *iv2p = (aria_block_t *)(ivec + ARIA_BLOCK_SIZE);

			while (len)
				{
				aria_block_t *inp = (aria_block_t *)in;
				aria_block_t *outp = (aria_block_t *)out;

				for(n=0 ; n < N_WORDS; ++n)
					outp->data[n] = inp->data[n] ^ ivp->data[n];
				ARIA_encrypt((unsigned char *)outp->data, (unsigned char *)outp->data, key);
				for(n=0 ; n < N_WORDS; ++n)
					outp->data[n] ^= iv2p->data[n];
				ivp = outp;
				iv2p = inp;
				--len;
				in += ARIA_BLOCK_SIZE;
				out += ARIA_BLOCK_SIZE;
				}
			memcpy(ivec, ivp->data, ARIA_BLOCK_SIZE);
			memcpy(ivec + ARIA_BLOCK_SIZE, iv2p->data, ARIA_BLOCK_SIZE);
			}
		else
			{
			aria_block_t tmp, tmp2;
			aria_block_t iv;
			aria_block_t iv2;

			load_block(iv, ivec);
			load_block(iv2, ivec + ARIA_BLOCK_SIZE);

			while (len)
				{
				load_block(tmp, in);
				for(n=0 ; n < N_WORDS; ++n)
					tmp2.data[n] = tmp.data[n] ^ iv.data[n];
				ARIA_encrypt((unsigned char *)tmp2.data, (unsigned char *)tmp2.data, key);
				for(n=0 ; n < N_WORDS; ++n)
					tmp2.data[n] ^= iv2.data[n];
				store_block(out, tmp2);
				iv = tmp2;
				iv2 = tmp;
				--len;
				in += ARIA_BLOCK_SIZE;
				out += ARIA_BLOCK_SIZE;
				}
			memcpy(ivec, iv.data, ARIA_BLOCK_SIZE);
			memcpy(ivec + ARIA_BLOCK_SIZE, iv2.data, ARIA_BLOCK_SIZE);
			}
		}
	else
		{
		if (in != out &&
		    (UNALIGNED_MEMOPS_ARE_FAST || ((size_t)in|(size_t)out|(size_t)ivec)%sizeof(long)==0))
			{
			aria_block_t *ivp = (aria_block_t *)ivec;
			aria_block_t *iv2p = (aria_block_t *)(ivec + ARIA_BLOCK_SIZE);

			while (len)
				{
				aria_block_t tmp;
				aria_block_t *inp = (aria_block_t *)in;
				aria_block_t *outp = (aria_block_t *)out;

				for(n=0 ; n < N_WORDS; ++n)
					tmp.data[n] = inp->data[n] ^ iv2p->data[n];
				ARIA_decrypt((unsigned char *)tmp.data, (unsigned char *)outp->data, key);
				for(n=0 ; n < N_WORDS; ++n)
					outp->data[n] ^= ivp->data[n];
				ivp = inp;
				iv2p = outp;
				--len;
				in += ARIA_BLOCK_SIZE;
				out += ARIA_BLOCK_SIZE;
				}
			memcpy(ivec, ivp->data, ARIA_BLOCK_SIZE);
			memcpy(ivec + ARIA_BLOCK_SIZE, iv2p->data, ARIA_BLOCK_SIZE);
			}
		else
			{
			aria_block_t tmp, tmp2;
			aria_block_t iv;
			aria_block_t iv2;

			load_block(iv, ivec);
			load_block(iv2, ivec + ARIA_BLOCK_SIZE);

			while (len)
				{
				load_block(tmp, in);
				tmp2 = tmp;
				for(n=0 ; n < N_WORDS; ++n)
					tmp.data[n] ^= iv2.data[n];
				ARIA_decrypt((unsigned char *)tmp.data, (unsigned char *)tmp.data, key);
				for(n=0 ; n < N_WORDS; ++n)
					tmp.data[n] ^= iv.data[n];
				store_block(out, tmp);
				iv = tmp2;
				iv2 = tmp;
				--len;
				in += ARIA_BLOCK_SIZE;
				out += ARIA_BLOCK_SIZE;
				}
			memcpy(ivec, iv.data, ARIA_BLOCK_SIZE);
			memcpy(ivec + ARIA_BLOCK_SIZE, iv2.data, ARIA_BLOCK_SIZE);
			}
		}
	}

/*
 * Note that its effectively impossible to do biIGE in anything other
 * than a single pass, so no provision is made for chaining.
 */

/* N.B. The IV for this mode is _four times_ the block size */

void ARIA_bi_ige_encrypt(const unsigned char *in, unsigned char *out,
						size_t length, const ARIA_KEY *key,
						const ARIA_KEY *key2, const unsigned char *ivec,
						const int enc)
	{
	size_t n;
	size_t len = length;
	unsigned char tmp[ARIA_BLOCK_SIZE];
	unsigned char tmp2[ARIA_BLOCK_SIZE];
	unsigned char tmp3[ARIA_BLOCK_SIZE];
	unsigned char prev[ARIA_BLOCK_SIZE];
	const unsigned char *iv;
	const unsigned char *iv2;

	OPENSSL_assert(in && out && key && ivec);
	OPENSSL_assert((ARIA_ENCRYPT == enc)||(ARIA_DECRYPT == enc));
	OPENSSL_assert((length%ARIA_BLOCK_SIZE) == 0);

	if (ARIA_ENCRYPT == enc)
		{
		/* XXX: Do a separate case for when in != out (strictly should
		   check for overlap, too) */

		/* First the forward pass */ 
		iv = ivec;
		iv2 = ivec + ARIA_BLOCK_SIZE;
		while (len >= ARIA_BLOCK_SIZE)
			{
			for(n=0 ; n < ARIA_BLOCK_SIZE ; ++n)
				out[n] = in[n] ^ iv[n];
			ARIA_encrypt(out, out, key);
			for(n=0 ; n < ARIA_BLOCK_SIZE ; ++n)
				out[n] ^= iv2[n];
			iv = out;
			memcpy(prev, in, ARIA_BLOCK_SIZE);
			iv2 = prev;
			len -= ARIA_BLOCK_SIZE;
			in += ARIA_BLOCK_SIZE;
			out += ARIA_BLOCK_SIZE;
			}

		/* And now backwards */
		iv = ivec + ARIA_BLOCK_SIZE*2;
		iv2 = ivec + ARIA_BLOCK_SIZE*3;
		len = length;
		while(len >= ARIA_BLOCK_SIZE)
			{
			out -= ARIA_BLOCK_SIZE;
			/* XXX: reduce copies by alternating between buffers */
			memcpy(tmp, out, ARIA_BLOCK_SIZE);
			for(n=0 ; n < ARIA_BLOCK_SIZE ; ++n)
				out[n] ^= iv[n];
			/*			hexdump(stdout, "out ^ iv", out, ARIA_BLOCK_SIZE); */
			ARIA_encrypt(out, out, key);
			/*			hexdump(stdout,"enc", out, ARIA_BLOCK_SIZE); */
			/*			hexdump(stdout,"iv2", iv2, ARIA_BLOCK_SIZE); */
			for(n=0 ; n < ARIA_BLOCK_SIZE ; ++n)
				out[n] ^= iv2[n];
			/*			hexdump(stdout,"out", out, ARIA_BLOCK_SIZE); */
			iv = out;
			memcpy(prev, tmp, ARIA_BLOCK_SIZE);
			iv2 = prev;
			len -= ARIA_BLOCK_SIZE;
			}
		}
	else
		{
		/* First backwards */
		iv = ivec + ARIA_BLOCK_SIZE*2;
		iv2 = ivec + ARIA_BLOCK_SIZE*3;
		in += length;
		out += length;
		while (len >= ARIA_BLOCK_SIZE)
			{
			in -= ARIA_BLOCK_SIZE;
			out -= ARIA_BLOCK_SIZE;
			memcpy(tmp, in, ARIA_BLOCK_SIZE);
			memcpy(tmp2, in, ARIA_BLOCK_SIZE);
			for(n=0 ; n < ARIA_BLOCK_SIZE ; ++n)
				tmp[n] ^= iv2[n];
			ARIA_decrypt(tmp, out, key);
			for(n=0 ; n < ARIA_BLOCK_SIZE ; ++n)
				out[n] ^= iv[n];
			memcpy(tmp3, tmp2, ARIA_BLOCK_SIZE);
			iv = tmp3;
			iv2 = out;
			len -= ARIA_BLOCK_SIZE;
			}

		/* And now forwards */
		iv = ivec;
		iv2 = ivec + ARIA_BLOCK_SIZE;
		len = length;
		while (len >= ARIA_BLOCK_SIZE)
			{
			memcpy(tmp, out, ARIA_BLOCK_SIZE);
			memcpy(tmp2, out, ARIA_BLOCK_SIZE);
			for(n=0 ; n < ARIA_BLOCK_SIZE ; ++n)
				tmp[n] ^= iv2[n];
			ARIA_decrypt(tmp, out, key);
			for(n=0 ; n < ARIA_BLOCK_SIZE ; ++n)
				out[n] ^= iv[n];
			memcpy(tmp3, tmp2, ARIA_BLOCK_SIZE);
			iv = tmp3;
			iv2 = out;
			len -= ARIA_BLOCK_SIZE;
			in += ARIA_BLOCK_SIZE;
			out += ARIA_BLOCK_SIZE;
			}
		}
	}
