#
# crypto/aria/Makefile
#

DIR=	aria
TOP=	../..
CC=	cc
CPP=	$(CC) -E
INCLUDES=
CFLAG=-g
MAKEFILE=	Makefile
AR=		ar r

ARIA_ENC=aria_core.o aria_cbc.o

CFLAGS= $(INCLUDES) $(CFLAG)
ASFLAGS= $(INCLUDES) $(ASFLAG)
AFLAGS= $(ASFLAGS)

GENERAL=Makefile
#TEST=ariatest.c
TEST=
APPS=

LIB=$(TOP)/libcrypto.a
LIBSRC=aria_core.c aria_misc.c aria_ecb.c aria_cbc.c aria_cfb.c aria_ofb.c \
       aria_ctr.c aria_ige.c aria_wrap.c
LIBOBJ=aria_misc.o aria_ecb.o aria_cfb.o aria_ofb.o aria_ctr.o aria_ige.o aria_wrap.o \
       $(ARIA_ENC)

SRC= $(LIBSRC)

EXHEADER= aria.h
HEADER= aria_locl.h $(EXHEADER)

ALL=    $(GENERAL) $(SRC) $(HEADER)

top:
	(cd ../..; $(MAKE) DIRS=crypto SDIRS=$(DIR) sub_all)

all:	lib

lib:	$(LIBOBJ)
	$(AR) $(LIB) $(LIBOBJ)
	$(RANLIB) $(LIB) || echo Never mind.
	@touch lib

aria-ia64.s: asm/aria-ia64.S
	$(CC) $(CFLAGS) -E asm/aria-ia64.S > $@

aria-586.s:	asm/aria-586.pl ../perlasm/x86asm.pl
	$(PERL) asm/aria-586.pl $(PERLASM_SCHEME) $(CFLAGS) $(PROCESSOR) > $@

aria-x86_64.s: asm/aria-x86_64.pl
	$(PERL) asm/aria-x86_64.pl $(PERLASM_SCHEME) > $@

aria-sparcv9.s: asm/aria-sparcv9.pl
	$(PERL) asm/aria-sparcv9.pl $(CFLAGS) > $@

aria-ppc.s:	asm/aria-ppc.pl
	$(PERL) asm/aria-ppc.pl $(PERLASM_SCHEME) $@

# GNU make "catch all"
aria-%.s:	asm/aria-%.pl;	$(PERL) $< $(CFLAGS) > $@

files:
	$(PERL) $(TOP)/util/files.pl Makefile >> $(TOP)/MINFO

links:
	@$(PERL) $(TOP)/util/mklink.pl ../../include/openssl $(EXHEADER)
	@$(PERL) $(TOP)/util/mklink.pl ../../test $(TEST)
	@$(PERL) $(TOP)/util/mklink.pl ../../apps $(APPS)

install:
	@[ -n "$(INSTALLTOP)" ] # should be set by top Makefile...
	@headerlist="$(EXHEADER)"; for i in $$headerlist ; \
	do  \
	(cp $$i $(INSTALL_PREFIX)$(INSTALLTOP)/include/openssl/$$i; \
	chmod 644 $(INSTALL_PREFIX)$(INSTALLTOP)/include/openssl/$$i ); \
	done;

tags:
	ctags $(SRC)

tests:

lint:
	lint -DLINT $(INCLUDES) $(SRC)>fluff

depend:
	@[ -n "$(MAKEDEPEND)" ] # should be set by upper Makefile...
	$(MAKEDEPEND) -- $(CFLAG) $(INCLUDES) $(DEPFLAG) -- $(PROGS) $(LIBSRC)

dclean:
	$(PERL) -pe 'if (/^# DO NOT DELETE THIS LINE/) {print; exit(0);}' $(MAKEFILE) >Makefile.new
	mv -f Makefile.new $(MAKEFILE)

clean:
	rm -f *.s *.o *.obj lib tags core .pure .nfs* *.old *.bak fluff

# DO NOT DELETE THIS LINE -- make depend depends on it.
