/* crypto/aria/aria_core.c -*- mode:C; c-file-style: "eay" -*- */
/*
 * A 32-bit implementation for ARIA
 *
 * follows the specifications given in
 * the ARIA specification at
 * http://www.nsri.re.kr/ARIA/
 *
 * Note:
 *    - Main body optimized for speed for 32 bit platforms
 *       * Utilizes 32-bit optimization techniques presented in ICISC 2003
 *       * Only four 32-bit tables are used
 *
 *    - Implemented some ideas for optimization from the creators of ARIA,
 *         and adopted some ideas from works submitted to ARIA implementation contest on Aug. 2004.
 *
 *    - Handles endian problem pretty well.
 *       * For optimization, for little endian architecture key setup functions return
 *         endian-reversed round keys; Crypt() function handles this correctly.
 *
 * 17, January 2005
 * Aaram Yun
 * National Security Research Institute, KOREA
 *
 * Substantial portion of the code originally written by Jin Hong.
 *
 */

#ifndef ARIA_DEBUG
#ifndef NDEBUG
#define NDEBUG
#endif
#endif
#include <assert.h>

#include <stdlib.h>
#include "aria.h"
#include "aria_locl.h"




/* »η�?λ ΗΓ·§Ζϋ�?Η endian Ζ―�?�?�?΅ µϋ¶σ LITTLE_ENDIAN �?¤�?�?
 * BIG_ENDIAN µΡ Αί Η�?³��?¦ Α¤�?ΗΗΨ�?ί ΔΔΖΔ�?�? µΛ΄�?΄Ω.
 * Windows+Intel ΗΓ·§Ζϋ�?Η °ζ�?μ�?΅΄Β LITTLE_ENDIAN�?�?°ν,
 * ±Χ �?ά�?΅΄Β �?Ή�?�? °ζ�?μ BIG_ENDIAN�?Τ΄�?΄Ω.  �?ί �?π�?£°�?�?�?�?ι
 * �?ΖΉ« Β�?�?�?³� �?±ΕΓΗΨ�?­ ΔΔΖΔ�?�? �?Δ ½ΗΗΰΗ�?½�?½Γ�?�?.  ARIA_test()
 * ΗΤ�?φ�?΅�?­ ENDIAN �?®�?�?�?» Η�?±β ¶§Ή®�?΅ �?ΓΉΩ�?¥ �?±ΕΓ�?�?�?�?΄ΒΑφ�?¦
 * Α΅°ΛΗ� �?φ �?Φ½�?΄�?΄Ω. */
#ifdef CR16_OPT_ARIA
#define SC14450
#endif
#ifdef CR16_OPT_ARIA_FULLASM
#define ARIAFULLASM
#endif
 #ifdef SC14450
	#define SC1445x_OPT
	#undef LITTLE_ENDIAN
	#define LITTLE_ENDIAN

	//TODO extra optimization can be done if the three arrays S1,S2,S3 use the same pointer
	// for read giving more registers for code
#endif

#ifndef SC14450
#if defined (L_ENDIAN)
#define LITTLE_ENDIAN
#elif defined (B_ENDIAN)
#define BIG_ENDIAN
#else
#define LITTLE_ENDIAN
#endif


#ifdef BIG_ENDIAN
#undef LITTLE_ENDIAN
#else
#ifndef LITTLE_ENDIAN
#error In order to compile this, you have to	define either LITTLE_ENDIAN or BIG_ENDIAN. If unsure, try define either of one and run checkEndian() function to see if your guess	is correct.
#endif
#endif
#endif

#undef BIG_ENDIAN
//#define SC14450
#define LITTLE_ENDIAN
//#define SC1445x_OPT

typedef unsigned char 			Byte;
typedef unsigned int  			Word;

/* S-boxµι�?» Α¤�?ΗΗ�?±β �?§ΗΡ �?¶Ε©·�?. */

#define AAA(V) 					0x ## 00 ## V ## V ## V
#define BBB(V) 					0x ## V ## 00 ## V ## V
#define CCC(V) 					0x ## V ## V ## 00 ## V
#define DDD(V) 					0x ## V ## V ## V ## 00
#define XX(NNN,x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,xa,xb,xc,xd,xe,xf)		NNN(x0),NNN(x1),NNN(x2),NNN(x3),NNN(x4),NNN(x5),NNN(x6),NNN(x7),NNN(x8),NNN(x9),NNN(xa),NNN(xb),NNN(xc),NNN(xd),NNN(xe),NNN(xf)

/* BY(X, Y)΄Β Word X�?Η YΉ�?Β° ΉΩ�?�?Ζ®
 * BRF(T,R)�?�? T>>R�?Η Η�?�?§ 1ΉΩ�?�?Ζ®
 * WO(X, Y)΄Β Byte array X�?¦ Word array·�? °£ΑΦΗ� ¶§ YΉ�?Β° Word
 */

#define BY(X,Y) 					(((Byte*)(&X))[Y])
#define BRF(T,R) 					((Byte)((T)>>(R)))
#define WO(X,Y) 					(((Word*)(X))[Y])



/* Byte array�?¦ Word�?΅ ½Ζ΄Β ΗΤ�?φ.  LITTLE_ENDIAN�?Η °ζ�?μ �?£µπ�?�? �?―�?― °�?Α¤�?» °ΕΔ£΄Ω. */
#ifdef LITTLE_ENDIAN

#ifdef SC1445x_OPT
#define SWAP_INT32b(X)\
({ \
		register long __r2 __asm__ ("r2") = (long)(X); \
		asm volatile(\
						"movw r3,r1 \n\t"\
						"movw r2,r3 \n\t"\
						"ashud $8,(r3,r2)\n\t"\
						"movw r1,r2 \n\t"\
						"ashud $8,(r2,r1)\n\t"\
						:"=r" (__r2) \
						: "0" (__r2) \
						: "r1");\
						__r2; \
	})

#define WordLoad(ORIG, DEST) DEST = SWAP_INT32b(ORIG)
#else
#define WordLoad(ORIG, DEST) \
{	\
	Word ___t;							\
	BY(___t, 0) = BY(ORIG, 3);		\
	BY(___t, 1) = BY(ORIG, 2);		\
	BY(___t, 2) = BY(ORIG, 1);		\
	BY(___t, 3) = BY(ORIG, 0);		\
	DEST = ___t;						\
	}
#endif

/* abcd�?Η 4 Byte·�? µ�? Word�?¦ dcba·�? �?―�?―Η�?΄Β ΗΤ�?φ  */
#if defined(_MSC_VER)
/* MSC »η�?λ �?―°ζ�?Η °ζ�?μ�?΅΄Β _lrotr() ΗΤ�?φ�?¦ �?�?�?λΗ� �?φ �?Φ�?�?ΉΗ·�? �?ΰ°£�?Η �?Σµµ Ηβ»σ�?�? °΅΄ΙΗ�?΄Ω. */
#define ReverseWord(W) 			{	(W) = (0xff00ff00 & _lrotr((W), 8)) ^ (0x00ff00ff & _lrotl((W), 8));	}
#else
#ifdef SC1445x_OPT

#define  ReverseWord(W) { (W)=SWAP_INT32b(W); }
#else
#define ReverseWord(W) 			{	(W) = (W) << 24 ^ (W) >> 24 ^ ((W) & 0x0000ff00) << 8 ^ ((W) & 0x00ff0000) >> 8;	}
#endif
#endif


#else
#define WordLoad(ORIG, DEST) 	{	DEST = ORIG;	}
#endif

#if defined(_MSC_VER)
#undef WordLoad
#define WordLoad(ORIG, DEST) 	{	(DEST) = (0xff00ff00 & _lrotr((ORIG), 8)) ^ (0x00ff00ff & _lrotl((ORIG), 8));	}
#endif

/* Key XOR Layer */
#ifndef SC1445x_OPT
#define KXL 						{														\
	t0 ^= WO(rk, 0); t1 ^= WO(rk, 1); t2 ^= WO(rk, 2); t3 ^= WO(rk, 3);	\
	rk += 16;																				\
	}
#else
#define KXL {\
		register long t0v  __asm__ ("r0") = (long)(t0);\
		register long t1v  __asm__ ("r2") = (long)(t1);\
		register long t2v  __asm__ ("r4") = (long)(t2);\
		register long t3v  __asm__ ("r6") = (long)(t3);\
		register long rkv  __asm__ ("r8") = (long)(rk);\
			asm volatile(\
						"loadd	0(r9,r8),(r11,r10) \n\t"\
						"loadd	4(r9,r8),(ra) \n\t"\
						"xord (r11,r10),(r1,r0) \n\t"\
						"loadd	8(r9,r8),(r11,r10) \n\t"\
						"xord (ra),(r3,r2) \n\t"\
						"loadd	0xc(r9,r8),(ra) \n\t"\
						"addd $0x10,(r9,r8) \n\t"\
						"xord (r11,r10),(r5,r4) \n\t"\
						"xord (ra),(r7,r6) \n\t"\
			:"=r" (t0v),"=r" (t1v),"=r" (t2v),"=r" (t3v),"=r" (rkv) \
			:"0" (t0v),"1" (t1v), "2" (t2v), "3" (t3v),"4" (rkv) \
			: "r10","r11","ra");\
			t0=t0v;\
			t1=t1v;\
			t2=t2v;\
			t3=t3v;\
			rk=rkv;\
		}
#endif
/* S-Box Layer 1 + M �?―�?― */
#ifdef SC1445x_OPT
#define BRFOP(T,R) 		(((T)>>(R-2))&(0x000000FF<<2))


#define BRFOPAMM(X)\
({ \
 	register long __r0 __asm__ ("r0") = (long)(X); \
 		asm volatile(\
						"lshd $-24, (r1,r0) \n\t"\
						"ashud $2, (r1,r0) \n\t"\
						:"=r" (__r0) \
						: "0" (__r0) \
							);\
							__r0; \
})


#define BRFOPAML(X)\
({ \
	register long __r0 __asm__ ("r0") = (long)(X); \
 		asm volatile(\
						"ashud $8, (r1,r0) \n\t"\
						"lshd $-24, (r1,r0) \n\t"\
						"ashud $2, (r1,r0) \n\t"\
						:"=r" (__r0) \
						: "0" (__r0) \
							);\
							__r0; \
})


#define BRFOPALM(X)\
({ \
	register long __r0 __asm__ ("r0") = (long)(X); \
 		asm volatile(\
						"ashud $16, (r1,r0) \n\t"\
						"lshd $-24,(r1,r0) \n\t"\
						"ashud $2,(r1,r0) \n\t"\
						:"=r" (__r0) \
						: "0" (__r0) \
							);\
							__r0; \
})

#define BRFOPALL(X)\
({ \
	register long __r0 __asm__ ("r0") = (long)(X); \
 		asm volatile(\
						"ashud $24, (r1,r0) \n\t"\
						"lshd $-24,(r1,r0) \n\t"\
						"ashud $2,(r1,r0) \n\t"\
						:"=r" (__r0) \
						: "0" (__r0) \
							);\
							__r0; \
})



#define BRFOPL(T,R) 	 (((T)<<(2))&(0x000000FF<<2))

#define SBL1_M(T0,T1,T2,T3) {																	\
	T0 = ((Word *)(((Word)S1)+ BRFOPAMM(T0)))[0] ^ ((Word *)(((Word)S2)+ BRFOPAML(T0)))[0] ^ ((Word *)(((Word)X1)+ BRFOPALM(T0)))[0] ^ ((Word *)(((Word)X2)+ BRFOPALL(T0)))[0];	\
	T1 = ((Word *)(((Word)S1)+ BRFOPAMM(T1)))[0] ^ ((Word *)(((Word)S2)+ BRFOPAML(T1)))[0] ^ ((Word *)(((Word)X1)+ BRFOPALM(T1)))[0] ^ ((Word *)(((Word)X2)+ BRFOPALL(T1)))[0];	\
	T2 = ((Word *)(((Word)S1)+ BRFOPAMM(T2)))[0] ^ ((Word *)(((Word)S2)+ BRFOPAML(T2)))[0] ^ ((Word *)(((Word)X1)+ BRFOPALM(T2)))[0] ^ ((Word *)(((Word)X2)+ BRFOPALL(T2)))[0];	\
	T3 = ((Word *)(((Word)S1)+ BRFOPAMM(T3)))[0] ^ ((Word *)(((Word)S2)+ BRFOPAML(T3)))[0] ^ ((Word *)(((Word)X1)+ BRFOPALM(T3)))[0] ^ ((Word *)(((Word)X2)+ BRFOPALL(T3)))[0];	\
	}

/* S-Box Layer 2 + M �?―�?― */
#define SBL2_M(T0,T1,T2,T3) {																	\
	T0 = ((Word *)(((Word)X1)+ BRFOPAMM(T0)))[0] ^ ((Word *)(((Word)X2)+ BRFOPAML(T0)))[0] ^ ((Word *)(((Word)S1)+ BRFOPALM(T0)))[0] ^ ((Word *)(((Word)S2)+ BRFOPALL(T0)))[0];	\
	T1 = ((Word *)(((Word)X1)+ BRFOPAMM(T1)))[0] ^ ((Word *)(((Word)X2)+ BRFOPAML(T1)))[0] ^ ((Word *)(((Word)S1)+ BRFOPALM(T1)))[0] ^ ((Word *)(((Word)S2)+ BRFOPALL(T1)))[0];	\
	T2 = ((Word *)(((Word)X1)+ BRFOPAMM(T2)))[0] ^ ((Word *)(((Word)X2)+ BRFOPAML(T2)))[0] ^ ((Word *)(((Word)S1)+ BRFOPALM(T2)))[0] ^ ((Word *)(((Word)S2)+ BRFOPALL(T2)))[0];	\
	T3 = ((Word *)(((Word)X1)+ BRFOPAMM(T3)))[0] ^ ((Word *)(((Word)X2)+ BRFOPAML(T3)))[0] ^ ((Word *)(((Word)S1)+ BRFOPALM(T3)))[0] ^ ((Word *)(((Word)S2)+ BRFOPALL(T3)))[0];	\
	}

#else
#define SBL1_M(T0,T1,T2,T3) {																	\
	T0 = S1[BRF(T0, 24)] ^ S2[BRF(T0, 16)] ^ X1[BRF(T0, 8)] ^ X2[BRF(T0, 0)];	\
	T1 = S1[BRF(T1, 24)] ^ S2[BRF(T1, 16)] ^ X1[BRF(T1, 8)] ^ X2[BRF(T1, 0)];	\
	T2 = S1[BRF(T2, 24)] ^ S2[BRF(T2, 16)] ^ X1[BRF(T2, 8)] ^ X2[BRF(T2, 0)];	\
	T3 = S1[BRF(T3, 24)] ^ S2[BRF(T3, 16)] ^ X1[BRF(T3, 8)] ^ X2[BRF(T3, 0)];	\
	}

/* S-Box Layer 2 + M �?―�?― */
#define SBL2_M(T0,T1,T2,T3) {																	\
	T0 = X1[BRF(T0, 24)] ^ X2[BRF(T0, 16)] ^ S1[BRF(T0, 8)] ^ S2[BRF(T0, 0)];	\
	T1 = X1[BRF(T1, 24)] ^ X2[BRF(T1, 16)] ^ S1[BRF(T1, 8)] ^ S2[BRF(T1, 0)];	\
	T2 = X1[BRF(T2, 24)] ^ X2[BRF(T2, 16)] ^ S1[BRF(T2, 8)] ^ S2[BRF(T2, 0)];	\
	T3 = X1[BRF(T3, 24)] ^ X2[BRF(T3, 16)] ^ S1[BRF(T3, 8)] ^ S2[BRF(T3, 0)];	\
	}


#endif



/* �?φµε ΄ά�?§�?Η �?―�?― */


#ifdef SC1445x_OPT

#define MM(T0,T1,T2,T3)\
 		asm volatile(\
						"xord %2,%1 \n\t"\
						"xord %3,%2 \n\t"\
						"xord %1,%0 \n\t"\
						"xord %1,%3 \n\t"\
						"xord %0,%2 \n\t"\
						"xord %2,%1 \n\t"\
						: "=r" (T0), "=r" (T1), "=r" (T2), "=r" (T3) \
						:  "0" (T0), "1" (T1), "2" (T2), "3" (T3));\


#else
#define MM(T0,T1,T2,T3) 		{					\
	(T1) ^= (T2); (T2) ^= (T3); (T0) ^= (T1);		\
	(T3) ^= (T1); (T2) ^= (T0); (T1) ^= (T2);		\
	}
#endif
/* P �?―�?―.  �?®»κ °θΓ�?�?Η Αί°£�?΅ µι�?ξ°΅΄Β ΉΩ�?�?Ζ® ΄ά�?§ �?―�?―�?�?΄Ω.
 * �?�? �?�?�?Π�?�? endian°�? Ή«°�?Η�?΄Ω.  */
#if defined(_MSC_VER)
#define P(T0,T1,T2,T3) {															\
	(T1) = (((T1) << 8) & 0xff00ff00) ^ (((T1) >> 8) & 0x00ff00ff);	\
	(T2) = _lrotr((T2), 16);														\
	ReverseWord((T3));																\
	}
#else
#ifdef SC1445x_OPT

#define PT1(X)\
	({ \
	 	register long __r0 __asm__ ("r0") = (long)(X); \
	 		asm volatile(\
							"movw r1,r3 \n\t"\
							"movw r0,r1 \n\t"\
							"movw r3,r2 \n\t"\
							"ashud $8,(r3,r2)\n\t"\
							"ashud $-8,(r1,r0)\n\t"\
							"movw r3,r1 \n\t"\
							:"=r" (__r0) \
							: "0" (__r0) \
							:  "r2","r3" );\
							__r0; \
	})

#define PT2(X)\
	({ \
	 	register long __r0 __asm__ ("r0") = (long)(X); \
	 		asm volatile(\
							"movw r1,r2 \n\t"\
							"movw r0,r1 \n\t"\
							"movw r2,r0 \n\t"\
							:"=r" (__r0) \
							: "0" (__r0) \
							:  "r2" );\
							__r0; \
	})
#define P(T0,T1,T2,T3) {															\
	(T1) = PT1(T1);	\
	(T2) = PT2(T2);	\
	ReverseWord((T3)); \
	}



#else
#define P(T0,T1,T2,T3) {															\
	(T1) = (((T1) <<  8) & 0xff00ff00) ^ (((T1) >>  8) & 0x00ff00ff);	\
	(T2) = (((T2) << 16) & 0xffff0000) ^ (((T2) >> 16) & 0x0000ffff);	\
	ReverseWord((T3));																\
	}
#endif
#endif

 /* FO: �?¦�?φΉ�?Β° ¶σ�?ξµε�?Η F ΗΤ�?φ
  * FE: Β¦�?φΉ�?Β° ¶σ�?ξµε�?Η F ΗΤ�?φ
  * MM°�? P΄Β ΉΩ�?�?Ζ® ΄ά�?§�?΅�?­ endian�?΅ Ή«°�?Η�?°Τ µ�?�?�?ΗΡ °α°�?�?¦ ΑΦ�?η,
  * ¶ΗΗΡ endian �?―�?―°�? °΅�?―�?�?΄Ω.  ¶ΗΗΡ, SBLi_M�?�? LITTLE_ENDIAN�?΅�?­
  * °α°�?�?ϋ�?�?·�? Word ΄ά�?§·�? endian�?» µ�?Α�?�?�? °α°�?�?¦ ΑΨ΄Ω.
  * Αο, FO, FE΄Β BIG_ENDIAN �?―°ζ�?΅�?­΄Β ARIA spec°�? µ�?�?�?ΗΡ °α°�?�?¦,
  * LITTLE_ENDIAN �?―°ζ�?΅�?­΄Β ARIA spec�?΅�?­ Α¤�?ΗΗΡ �?―�?―+endian �?―�?―�?»
  * ΑΨ΄Ω. */
    #ifndef SC1445x_OPT
#define FO 							{	SBL1_M(t0, t1, t2, t3) MM(t0, t1, t2, t3) P(t0, t1, t2, t3) MM(t0, t1, t2, t3)	}
#define FE 							{	SBL2_M(t0, t1, t2, t3) MM(t0, t1, t2, t3) P(t2, t3, t0, t1) MM(t0, t1, t2, t3)	}

#else


/*ASSEMBLER BUG NEED. IF INDEXED ADDRESSING IS USE
 * DO THE FOLLOWING BINARY REPLACEMENTS
 * 1000 0110 10xx xxxx loadd (prp) disp14 4 src (prp) 4 dest rp 14 src disp 17 2


p3 5:4 p1_4 p3_14 p2_4 p3_14   ->

p3 5:4 p1_4 7,6,13-8 p2_4 3:0

pt1=src=5			=	      0101

pt2=dest = 8		=	      1000

pt3=disp=0x400		=00 0100 0000 0000


1000 0110 10 00 0101 0000 0100 1000 0000

   8       6      8       5     0      4        8      0






2a4285860004-->2a4285868004
2a4285860008-->2a4285868008
2242a186000c-->22428186200c
2442c286000c-->24428286400c
2642e386000c-->26428386600c
2a4285860008-->2a4285868008
2a428586000c-->2a428586800c
2242a1860004-->224281862004
2442c2860004-->244282864004
2642e3860004-->264283866004
 *
 *
 *
 *
 *
 */

#define FO {\
		register long t0v  __asm__ ("r0") = (long)(t0);\
		register long t1v  __asm__ ("r2") = (long)(t1);\
		register long t2v  __asm__ ("r4") = (long)(t2);\
		register long t3v  __asm__ ("r6") = (long)(t3);\
		register long S1v  __asm__ ("r8") = (long)(S1);\
			asm volatile(\
							"push $2,r13 \n\t"\
							"movd (r9,r8),(r13) \n\t"\
							/*T0 = ((Word *)(((Word)S1)+ BRFOPAMM(T0)))[0] ^
							 * 	   ((Word *)(((Word)S2)+ BRFOPAML(T0)))[0] ^
							 * 	   ((Word *)(((Word)X1)+ BRFOPALM(T0)))[0] ^
							 * 	   ((Word *)(((Word)X2)+ BRFOPALL(T0)))[0];	*/\
							"movzw r1,(r11,r10) \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd  0(r11,r10),(ra)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movzb r1, r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 	0x400(r11,r10),(r9,r8)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movw r0,r10 \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0x800(r11,r10),(r11,r10)\n\t"\
							\
							/*"andd	$0x0ff,(r1,r0) \n\t"*/\
							"movzb r0,r0 \n\t"\
							"movzw r0,(r1,r0) \n\t"\
							"ashuw	$2:s,r0 \n\t"\
							"addd	(r13),(r1,r0) \n\t"\
							"loadd 0xC00(r1,r0),(r1,r0)\n\t"\
							\
							"xord	(r11,r10),(r9,r8) \n\t"\
							"xord	(ra), (r9,r8) \n\t"\
							\
							/*T1 = ((Word *)(((Word)S1)+ BRFOPAMM(T1)))[0] ^
							 * 	   ((Word *)(((Word)S2)+ BRFOPAML(T1)))[0] ^
							 * 	   ((Word *)(((Word)X1)+ BRFOPALM(T1)))[0] ^
							 * 	   ((Word *)(((Word)X2)+ BRFOPALL(T1)))[0];	*/\
							"movzw r3,(r11,r10) \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"xord	(r9,r8),(r1,r0) \n\t"\
							"loadd 0(r11,r10),(r9,r8)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movzb r3, r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0x400(r11,r10),(ra)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movw r2,r10 \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0x800(r11,r10),(r11,r10)\n\t"\
							\
							/*"andd	$0x0ff,(r3,r2) \n\t"*/\
							"movzb r2,r2 \n\t"\
							"movzw r2,(r3,r2) \n\t"\
							"ashuw	$2:s,r2 \n\t"\
							"addd	(r13),(r3,r2) \n\t"\
							"loadd 0xC00(r3,r2),(r3,r2)\n\t"\
							\
							"xord	(r11,r10),(r9,r8) \n\t"\
							"xord	(ra), (r9,r8) \n\t"\
							\
							/*T2 = ((Word *)(((Word)S1)+ BRFOPAMM(T2)))[0] ^
							 * 	   ((Word *)(((Word)S2)+ BRFOPAML(T2)))[0] ^
							 * 	   ((Word *)(((Word)X1)+ BRFOPALM(T2)))[0] ^
							 * 	   ((Word *)(((Word)X2)+ BRFOPALL(T2)))[0];	*/\
							\
							"movzw r5,(r11,r10) \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"xord	(r9,r8),(r3,r2) \n\t"\
							"loadd 0(r11,r10),(r9,r8)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movzb r5, r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0x400(r11,r10),(ra)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movw r4,r10 \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0x800(r11,r10),(r11,r10)\n\t"\
							\
							/*"andd	$0x0ff,(r5,r4) \n\t"*/\
							"movzb r4,r4 \n\t"\
							"movzw r4,(r5,r4) \n\t"\
							"ashuw	$2:s,r4 \n\t"\
							"addd	(r13),(r5,r4) \n\t"\
							"loadd 0xC00(r5,r4),(r5,r4)\n\t"\
							\
							"xord	(r11,r10),(r9,r8) \n\t"\
							"xord	(ra), (r9,r8) \n\t"\
							\
							\
							/*T3 = ((Word *)(((Word)S1)+ BRFOPAMM(T3)))[0] ^
							 * 	   ((Word *)(((Word)S2)+ BRFOPAML(T3)))[0] ^
							 * 	   ((Word *)(((Word)X1)+ BRFOPALM(T3)))[0] ^
							 * 	   ((Word *)(((Word)X2)+ BRFOPALL(T3)))[0];	*/\
							\
							"movzw r7,(r11,r10) \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"xord	(r9,r8),(r5,r4) \n\t"\
							"loadd 0(r11,r10),(r9,r8)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movzb r7, r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0x400(r11,r10),(ra)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movw r6,r10 \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0x800(r11,r10),(r11,r10)\n\t"\
							\
							/*"andd	$0x0ff,(r7,r6) \n\t"*/\
							"movzb r6,r6 \n\t"\
							"movzw r6,(r7,r6) \n\t"\
							"ashuw	$2:s,r6 \n\t"\
							"addd	(r13),(r7,r6) \n\t"\
							"loadd 0x0C00(r7,r6),(r7,r6)\n\t"\
							\
							"xord	(r11,r10),(r9,r8) \n\t"\
							"xord	(ra), (r9,r8) \n\t"\
							\
							/* MM(T0,T1,T2,T3) 		{					\
								  (T1) ^= (T2); (T2) ^= (T3); (T0) ^= (T1);		\
								  (T3) ^= (T1); (T2) ^= (T0); (T1) ^= (T2);		\
								}*/\
							\
							"xord	(r5,r4),(r3,r2) \n\t"\
							"xord	(r3,r2),(r1,r0) \n\t"\
							"xord	(r9,r8),(r7,r6) \n\t"\
							"xord	(r7,r6),(r5,r4) \n\t"\
							\
							"xord	(r3,r2),(r7,r6) \n\t"\
							"xord	(r1,r0),(r5,r4) \n\t"\
							"xord	(r5,r4),(r3,r2) \n\t"\
							\
							/*#define P(T0,T1,T2,T3) {															\
										(T1) = PT1(T1);	\
										(T2) = PT2(T2);	\
										ReverseWord((T3)); \
							*/\
							\
							/*(T1) = PT1(T1);*/\
							\
							"movd	(r3,r2),(r10,r9) \n\t"\
							"movw	r3,r11 \n\t"\
							"movw	r2,r8 \n\t"\
							"ashud	$-8:s,(r11,r10) \n\t"\
							"ashud	$8:s,(r9,r8) \n\t"\
							"movd	(r10,r9),(r3,r2) \n\t"\
							\
							/*(T2) = (((T2) << 16) & 0xffff0000) ^ (((T2) >> 16) & 0x0000ffff);*/\
							\
							"movw r4,r8 \n\t"\
							"movw r5,r4 \n\t"\
							"movw r8,r5 \n\t"\
							\
							/*ReverseWord((T3));*/\
							\
							"movw r6,r8 \n\t"\
							"movw r7,r6 \n\t"\
							"lshd $-8,(r7,r6)\n\t"\
							"movw r8,r7 \n\t"\
							"ashud $-8,(r8,r7)\n\t"\
							/* MM(T0,T1,T2,T3) 		{					\
							 (T1) ^= (T2); (T2) ^= (T3); (T0) ^= (T1);		\
							 (T3) ^= (T1); (T2) ^= (T0); (T1) ^= (T2);		\
							}*/\
							\
							"xord	(r5,r4),(r3,r2) \n\t"\
							"xord	(r7,r6),(r5,r4) \n\t"\
							"xord	(r3,r2),(r1,r0) \n\t"\
							\
							"xord	(r3,r2),(r7,r6) \n\t"\
							"xord	(r1,r0),(r5,r4) \n\t"\
							"xord	(r5,r4),(r3,r2) \n\t"\
							"movd (r13),(r9,r8)\n\t"\
							"pop $2,r13 \n\t"\
							\
					:"=r" (t0v),"=r" (t1v),"=r" (t2v),"=r" (t3v) \
					:"0" (t0v),"1" (t1v), "2" (t2v), "3" (t3v),"r" (S1v) \
					: "r10","r11","ra");\
			t0=t0v;\
			t1=t1v;\
			t2=t2v;\
			t3=t3v;\
			}
#define FE {\
		register long t0v  __asm__ ("r0") = (long)(t0);\
		register long t1v  __asm__ ("r2") = (long)(t1);\
		register long t2v  __asm__ ("r4") = (long)(t2);\
		register long t3v  __asm__ ("r6") = (long)(t3);\
		register long S1v  __asm__ ("r8") = (long)(S1);\
			asm volatile(\
							"push $2,r13 \n\t"\
							"movd (r9,r8),(r13) \n\t"\
							/*T0 = ((Word *)(((Word)S1)+ BRFOPAMM(T0)))[0] ^
							 * 	   ((Word *)(((Word)S2)+ BRFOPAML(T0)))[0] ^
							 * 	   ((Word *)(((Word)X1)+ BRFOPALM(T0)))[0] ^
							 * 	   ((Word *)(((Word)X2)+ BRFOPALL(T0)))[0];	*/\
							"movzw r1,(r11,r10) \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd  0x800(r11,r10),(ra)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movzb r1, r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 	0xC00(r11,r10),(r9,r8)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movw r0,r10 \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0x000(r11,r10),(r11,r10)\n\t"\
							\
							/*"andd	$0x0ff,(r1,r0) \n\t"*/\
							"movzb r0,r0 \n\t"\
							"movzw r0,(r1,r0) \n\t"\
							"ashuw	$2:s,r0 \n\t"\
							"addd	(r13),(r1,r0) \n\t"\
							"loadd 0x400(r1,r0),(r1,r0)\n\t"\
							\
							"xord	(ra), (r9,r8) \n\t"\
							"xord	(r11,r10),(r9,r8) \n\t"\
							\
							/*T1 = ((Word *)(((Word)S1)+ BRFOPAMM(T1)))[0] ^
							 * 	   ((Word *)(((Word)S2)+ BRFOPAML(T1)))[0] ^
							 * 	   ((Word *)(((Word)X1)+ BRFOPALM(T1)))[0] ^
							 * 	   ((Word *)(((Word)X2)+ BRFOPALL(T1)))[0];	*/\
							"movzw r3,(r11,r10) \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"xord	(r9,r8),(r1,r0) \n\t"\
							"loadd 0x800(r11,r10),(r9,r8)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movzb r3, r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0xC00(r11,r10),(ra)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movw r2,r10 \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0x000(r11,r10),(r11,r10)\n\t"\
							\
							/*"andd	$0x0ff,(r3,r2)\n\t"*/\
							"movzb r2,r2 \n\t"\
							"movzw r2,(r3,r2) \n\t"\
							"ashuw	$2:s,r2 \n\t"\
							"addd	(r13),(r3,r2) \n\t"\
							"loadd 0x400(r3,r2),(r3,r2)\n\t"\
							\
							"xord	(ra), (r9,r8) \n\t"\
							"xord	(r11,r10),(r9,r8) \n\t"\
							\
							/*T2 = ((Word *)(((Word)S1)+ BRFOPAMM(T2)))[0] ^
							 * 	   ((Word *)(((Word)S2)+ BRFOPAML(T2)))[0] ^
							 * 	   ((Word *)(((Word)X1)+ BRFOPALM(T2)))[0] ^
							 * 	   ((Word *)(((Word)X2)+ BRFOPALL(T2)))[0];	*/\
							\
							"movzw r5,(r11,r10) \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"xord	(r9,r8),(r3,r2) \n\t"\
							"loadd 0x800(r11,r10),(r9,r8)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movzb r5, r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0xC00(r11,r10),(ra)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movw r4,r10 \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0x000(r11,r10),(r11,r10)\n\t"\
							\
							/*"andd	$0x0ff,(r5,r4)\n\t"*/\
							"movzb r4,r4 \n\t"\
							"movzw r4,(r5,r4) \n\t"\
							"ashuw	$2:s,r4 \n\t"\
							"addd	(r13),(r5,r4) \n\t"\
							"loadd 0x400(r5,r4),(r5,r4)\n\t"\
							\
							"xord	(ra), (r9,r8) \n\t"\
							"xord	(r11,r10),(r9,r8) \n\t"\
							\
							\
							/*T3 = ((Word *)(((Word)S1)+ BRFOPAMM(T3)))[0] ^
							 * 	   ((Word *)(((Word)S2)+ BRFOPAML(T3)))[0] ^
							 * 	   ((Word *)(((Word)X1)+ BRFOPALM(T3)))[0] ^
							 * 	   ((Word *)(((Word)X2)+ BRFOPALL(T3)))[0];	*/\
							\
							"movzw r7,(r11,r10) \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"xord	(r9,r8),(r5,r4) \n\t"\
							"loadd 0x800(r11,r10),(r9,r8)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movzb r7, r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0xC00(r11,r10),(ra)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movw r6,r10 \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0x000(r11,r10),(r11,r10)\n\t"\
							\
							/*"andd	$0x0ff,(r7,r6)\n\t"*/\
							"movzb r6,r6 \n\t"\
							"movzw r6,(r7,r6) \n\t"\
							"ashuw	$2:s,r6 \n\t"\
							"addd	(r13),(r7,r6) \n\t"\
							"loadd 0x0400(r7,r6),(r7,r6)\n\t"\
							\
							"xord	(ra), (r9,r8) \n\t"\
							"xord	(r11,r10),(r9,r8) \n\t"\
							\
							/* MM(T0,T1,T2,T3) 		{					\
								  (T1) ^= (T2); (T2) ^= (T3); (T0) ^= (T1);		\
								  (T3) ^= (T1); (T2) ^= (T0); (T1) ^= (T2);		\
								}*/\
							\
							"xord	(r5,r4),(r3,r2) \n\t"\
							"xord	(r3,r2),(r1,r0) \n\t"\
							"xord	(r9,r8),(r7,r6) \n\t"\
							"xord	(r7,r6),(r5,r4) \n\t"\
							\
							"xord	(r3,r2),(r7,r6) \n\t"\
							"xord	(r1,r0),(r5,r4) \n\t"\
							"xord	(r5,r4),(r3,r2) \n\t"\
							\
							/*#define P(T0,T1,T2,T3) {
							 * 			...>(T2,T3,T0,T1)												\
										(T1) = PT1(T1);	\
										(T2) = PT2(T2);	\
										ReverseWord((T3)); \
							*/\
							\
							/*(T1) = PT1(T1);*/\
							\
							"movd	(r7,r6),(r10,r9) \n\t"\
							"movw	r7,r11 \n\t"\
							"movw	r6,r8 \n\t"\
							"ashud	$-8:s,(r11,r10) \n\t"\
							"ashud	$8:s,(r9,r8) \n\t"\
							"movd	(r10,r9),(r7,r6) \n\t"\
							\
							/*(T2) = (((T2) << 16) & 0xffff0000) ^ (((T2) >> 16) & 0x0000ffff);*/\
							\
							"movw r0,r8 \n\t"\
							"movw r1,r0 \n\t"\
							"movw r8,r1 \n\t"\
							\
							/*ReverseWord((T3));*/\
							\
							"movd (r7,r6),(r11,r10) \n\t"\
							"movd (r3,r2),(r7,r6) \n\t"\
							"movw r6,r8 \n\t"\
							"movw r7,r6 \n\t"\
							"lshd $-8,(r7,r6)\n\t"\
							"movw r8,r7 \n\t"\
							"ashud $-8,(r8,r7)\n\t"\
							"movd (r7,r6),(r3,r2) \n\t"\
							"movd (r11,r10),(r7,r6) \n\t"\
							/* MM(T0,T1,T2,T3) 		{					\
							 (T1) ^= (T2); (T2) ^= (T3); (T0) ^= (T1);		\
							 (T3) ^= (T1); (T2) ^= (T0); (T1) ^= (T2);		\
							}*/\
							\
							"xord	(r5,r4),(r3,r2) \n\t"\
							"xord	(r7,r6),(r5,r4) \n\t"\
							"xord	(r3,r2),(r1,r0) \n\t"\
							\
							"xord	(r3,r2),(r7,r6) \n\t"\
							"xord	(r1,r0),(r5,r4) \n\t"\
							"xord	(r5,r4),(r3,r2) \n\t"\
							"movd (r13),(r9,r8)\n\t"\
							"pop $2,r13 \n\t"\
							\
					:"=r" (t0v),"=r" (t1v),"=r" (t2v),"=r" (t3v) \
					:"0" (t0v),"1" (t1v), "2" (t2v), "3" (t3v),"r" (S1v) \
					: "r10","r11","ra");\
			t0=t0v;\
			t1=t1v;\
			t2=t2v;\
			t3=t3v;\
			}

 #endif
#ifdef SC1445x_OPT

/* n-bit right shift of Y XORed to X */
/* Word ΄ά�?§·�? Α¤�?Ηµ�? �?ν·�?�?΅�?­�?Η �?�?�?�? + XOR�?�?΄Ω. */
#define q19   	4;
#define r19		19;

#define GSRK19p1(X,Y,W)\
	({ \
		register long __r0 __asm__ ("r0") ; \
		register long __r2 __asm__ ("r2") = (long)(X); \
	 	register long __r4 __asm__ ("r4") = (long)(Y); \
	 	register long __r6 __asm__ ("r6") ; \
	 	register long __r8 __asm__ ("r8") = (long)(W); \
	 	asm volatile(\
							"loadd 0x0:(r3,r2),(r1,r0) \n\t"\
							"loadd 0x0:(r5,r4),(r7,r6) \n\t"\
							"lshd $-19,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x0c:(r5,r4),(r7,r6) \n\t"\
							"lshd $13,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0x0:(r9,r8) \n\t"\
							"\n\t"\
							"loadd 0x4:(r3,r2),(r1,r0) \n\t"\
							"loadd 0x4:(r5,r4),(r7,r6) \n\t"\
							"lshd $-19,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x0:(r5,r4),(r7,r6) \n\t"\
							"lshd $13,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0x4:(r9,r8) \n\t"\
							"\n\t"\
							"loadd 0x8:(r3,r2),(r1,r0) \n\t"\
							"loadd 0x8:(r5,r4),(r7,r6) \n\t"\
							"lshd $-19,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x4:(r5,r4),(r7,r6) \n\t"\
							"lshd $13,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0x8:(r9,r8) \n\t"\
							"\n\t"\
							"loadd 0xc:(r3,r2),(r1,r0) \n\t"\
							"loadd 0xc:(r5,r4),(r7,r6) \n\t"\
							"lshd $-19,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x8:(r5,r4),(r7,r6) \n\t"\
							"lshd $13,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0xc:(r9,r8) \n\t"\
							"\n\t"\
							:"=r" (__r0), "=r" (__r6) \
							: "0" (__r0), "r" (__r2), "r" (__r4), "1" (__r6), "r" (__r8) \
							: "memory" \
							);\
							__r0; \
	})

#define q31   	4;
#define r31		31;
#define GSRK31p1(X,Y,W)\
	({ \
		register long __r0 __asm__ ("r0") ; \
		register long __r2 __asm__ ("r2") = (long)(X); \
	 	register long __r4 __asm__ ("r4") = (long)(Y); \
	 	register long __r6 __asm__ ("r6") ; \
	 	register long __r8 __asm__ ("r8") = (long)(W); \
	 	asm volatile(\
							"loadd 0x0:(r3,r2),(r1,r0) \n\t"\
							"loadd 0x0:(r5,r4),(r7,r6) \n\t"\
							"lshd $-31,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x0c:(r5,r4),(r7,r6) \n\t"\
							"lshd $1,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0x0:(r9,r8) \n\t"\
							"\n\t"\
							"loadd 0x4:(r3,r2),(r1,r0) \n\t"\
							"loadd 0x4:(r5,r4),(r7,r6) \n\t"\
							"lshd $-31,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x0:(r5,r4),(r7,r6) \n\t"\
							"lshd $1,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0x4:(r9,r8) \n\t"\
							"\n\t"\
							"loadd 0x8:(r3,r2),(r1,r0) \n\t"\
							"loadd 0x8:(r5,r4),(r7,r6) \n\t"\
							"lshd $-31,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x4:(r5,r4),(r7,r6) \n\t"\
							"lshd $1,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0x8:(r9,r8) \n\t"\
							"\n\t"\
							"loadd 0xc:(r3,r2),(r1,r0) \n\t"\
							"loadd 0xc:(r5,r4),(r7,r6) \n\t"\
							"lshd $-31,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x8:(r5,r4),(r7,r6) \n\t"\
							"lshd $1,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0xc:(r9,r8) \n\t"\
							"\n\t"\
							:"=r" (__r0), "=r" (__r6) \
							: "0" (__r0), "r" (__r2), "r" (__r4), "1" (__r6), "r" (__r8) \
							: "memory" \
							);\
							__r0; \
	})

#define q67   	2;
#define r67		3;
#define GSRK67p1(X,Y,W)\
	({ \
		register long __r0 __asm__ ("r0") ; \
		register long __r2 __asm__ ("r2") = (long)(X); \
	 	register long __r4 __asm__ ("r4") = (long)(Y); \
	 	register long __r6 __asm__ ("r6") ; \
	 	register long __r8 __asm__ ("r8") = (long)(W); \
	 	asm volatile(\
							"loadd 0x0:(r3,r2),(r1,r0) \n\t"\
							"loadd 0x8:(r5,r4),(r7,r6) \n\t"\
							"lshd $-3,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x04:(r5,r4),(r7,r6) \n\t"\
							"lshd $29,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0x0:(r9,r8) \n\t"\
							"\n\t"\
							"loadd 0x4:(r3,r2),(r1,r0) \n\t"\
							"loadd 0xc:(r5,r4),(r7,r6) \n\t"\
							"lshd $-3,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x8:(r5,r4),(r7,r6) \n\t"\
							"lshd $29,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0x4:(r9,r8) \n\t"\
							"\n\t"\
							"loadd 0x8:(r3,r2),(r1,r0) \n\t"\
							"loadd 0x0:(r5,r4),(r7,r6) \n\t"\
							"lshd $-3,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0xc:(r5,r4),(r7,r6) \n\t"\
							"lshd $29,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0x8:(r9,r8) \n\t"\
							"\n\t"\
							"loadd 0xc:(r3,r2),(r1,r0) \n\t"\
							"loadd 0x4:(r5,r4),(r7,r6) \n\t"\
							"lshd $-3,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x0:(r5,r4),(r7,r6) \n\t"\
							"lshd $29,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0xc:(r9,r8) \n\t"\
							"\n\t"\
							:"=r" (__r0), "=r" (__r6) \
							: "0" (__r0), "r" (__r2), "r" (__r4), "1" (__r6), "r" (__r8) \
							: "memory" \
							);\
							__r0; \
	})

#define q97   	1;
#define r97		1;
#define GSRK97p1(X,Y,W)\
	({ \
		register long __r0 __asm__ ("r0") ; \
		register long __r2 __asm__ ("r2") = (long)(X); \
	 	register long __r4 __asm__ ("r4") = (long)(Y); \
	 	register long __r6 __asm__ ("r6") ; \
	 	register long __r8 __asm__ ("r8") = (long)(W); \
	 	asm volatile(\
							"loadd 0x0:(r3,r2),(r1,r0) \n\t"\
							"loadd 0x4:(r5,r4),(r7,r6) \n\t"\
							"lshd $-1,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x00:(r5,r4),(r7,r6) \n\t"\
							"lshd $31,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0x0:(r9,r8) \n\t"\
							"\n\t"\
							"loadd 0x4:(r3,r2),(r1,r0) \n\t"\
							"loadd 0x8:(r5,r4),(r7,r6) \n\t"\
							"lshd $-1,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x4:(r5,r4),(r7,r6) \n\t"\
							"lshd $31,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0x4:(r9,r8) \n\t"\
							"\n\t"\
							"loadd 0x8:(r3,r2),(r1,r0) \n\t"\
							"loadd 0xC:(r5,r4),(r7,r6) \n\t"\
							"lshd $-1,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x8:(r5,r4),(r7,r6) \n\t"\
							"lshd $31,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0x8:(r9,r8) \n\t"\
							"\n\t"\
							"loadd 0xc:(r3,r2),(r1,r0) \n\t"\
							"loadd 0x0:(r5,r4),(r7,r6) \n\t"\
							"lshd $-1,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0xC:(r5,r4),(r7,r6) \n\t"\
							"lshd $31,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0xc:(r9,r8) \n\t"\
							"\n\t"\
							:"=r" (__r0), "=r" (__r6) \
							: "0" (__r0), "r" (__r2), "r" (__r4), "1" (__r6), "r" (__r8) \
							: "memory" \
							);\
							__r0; \
	})



#define q109   	1;
#define r109	13;
#define GSRK109p1(X,Y,W)\
	({ \
		register long __r0 __asm__ ("r0") ; \
		register long __r2 __asm__ ("r2") = (long)(X); \
	 	register long __r4 __asm__ ("r4") = (long)(Y); \
	 	register long __r6 __asm__ ("r6") ; \
	 	register long __r8 __asm__ ("r8") = (long)(W); \
	 	asm volatile(\
							"loadd 0x0:(r3,r2),(r1,r0) \n\t"\
							"loadd 0x4:(r5,r4),(r7,r6) \n\t"\
							"lshd $-13,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x00:(r5,r4),(r7,r6) \n\t"\
							"lshd $19,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0x0:(r9,r8) \n\t"\
							"\n\t"\
							"loadd 0x4:(r3,r2),(r1,r0) \n\t"\
							"loadd 0x8:(r5,r4),(r7,r6) \n\t"\
							"lshd $-13,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x4:(r5,r4),(r7,r6) \n\t"\
							"lshd $19,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0x4:(r9,r8) \n\t"\
							"\n\t"\
							"loadd 0x8:(r3,r2),(r1,r0) \n\t"\
							"loadd 0xC:(r5,r4),(r7,r6) \n\t"\
							"lshd $-13,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0x8:(r5,r4),(r7,r6) \n\t"\
							"lshd $19,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0x8:(r9,r8) \n\t"\
							"\n\t"\
							"loadd 0xc:(r3,r2),(r1,r0) \n\t"\
							"loadd 0x0:(r5,r4),(r7,r6) \n\t"\
							"lshd $-13,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"loadd 0xC:(r5,r4),(r7,r6) \n\t"\
							"lshd $19,(r7,r6) \n\t"\
							"xord (r7,r6),(r1,r0) \n\t"\
							"stord (r1,r0),0xc:(r9,r8) \n\t"\
							"\n\t"\
							:"=r" (__r0), "=r" (__r6) \
							: "0" (__r0), "r" (__r2), "r" (__r4), "1" (__r6), "r" (__r8) \
							: "memory" \
							);\
							__r0; \
	})

#define GSRK19(X, Y, n) 			{																	\
	q = 4 - ((n) / 32);																				\
	r = (n) % 32;		\
	GSRK19p1(X,Y, &WO(rk, 0)); \
	rk += 16;																							\
	}
#define GSRK31(X, Y, n) 			{																	\
	q = 4 - ((n) / 32);																				\
	r = (n) % 32;		\
	GSRK31p1(X,Y, &WO(rk, 0)); \
	rk += 16;																							\
	}

#define GSRK67(X, Y, n) 			{																	\
	q = 4 - ((n) / 32);																				\
	r = (n) % 32;		\
	GSRK67p1(X,Y, &WO(rk, 0)); \
	rk += 16;																							\
	}

#define GSRK97(X, Y, n) 			{																	\
	q = 4 - ((n) / 32);																				\
	r = (n) % 32;		\
	GSRK97p1(X,Y, &WO(rk, 0)); \
	rk += 16;																							\
	}

#define GSRK109(X, Y, n) 			{																	\
	q = 4 - ((n) / 32);																				\
	r = (n) % 32;		\
	GSRK109p1(X,Y, &WO(rk, 0)); \
	rk += 16;																							\
	}



#else

#define GSRK(X, Y, n) 			{																	\
	q = 4 - ((n) / 32);																				\
	r = (n) % 32;		\
	WO(rk, 0) = ((X)[0]) ^ (((Y)[(q  )%4]) >> r) ^ (((Y)[(q+3)%4]) << (32-r)); 	\
	WO(rk, 1) = ((X)[1]) ^ (((Y)[(q+1)%4]) >> r) ^ (((Y)[(q  )%4]) << (32-r)); 	\
	WO(rk, 2) = ((X)[2]) ^ (((Y)[(q+2)%4]) >> r) ^ (((Y)[(q+1)%4]) << (32-r)); 	\
	WO(rk, 3) = ((X)[3]) ^ (((Y)[(q+3)%4]) >> r) ^ (((Y)[(q+2)%4]) << (32-r)); 	\
	rk += 16;																							\
	}
#endif

/* DecKeySetup()�?΅�?­ »η�?λΗ�?΄Β �?¶Ε©·�? */
#if defined(_MSC_VER)
#define WordM1(X,Y) 				{		\
	w =_lrotr((X), 8);					\
	(Y) = w ^ _lrotr((X)^w, 16);		\
	}
#else
#define WordM1(X,Y) 				{																\
	Y = (X) << 8 ^ (X) >> 8 ^ (X) << 16 ^ (X) >> 16 ^ (X) << 24 ^ (X) >> 24;	\
	}

#define WordM1p(X)\
({ \
		register long __t0l __asm__ ("r0") ; \
		register long __t1l __asm__ ("r2") ; \
		register long __t2l __asm__ ("r4") ; \
		register long __t3l __asm__ ("r6") ; \
		register long __Xl __asm__ ("r10") = (long)(X); \
	 	asm volatile(\
							"loadd 0x0:(r11,r10),(r1,r0) \n\t"\
							"\n\t"\
							"movd (r1,r0),(r9,r8) \n\t"\
							"xorw r9,r8 \n\t"\
							"movzb r8,r9 \n\t"\
							"lshw $-8,r8 \n\t"\
							"xorb r9,r8 \n\t"\
							"movzb r8,r9 \n\t"\
							"ashuw $0x8,r9 \n\t"\
							"xorb r8,r9 \n\t"\
							"movw r9,r8 \n\t"\
							"xord (r9,r8),(r1,r0) \n\t"\
							\
							\
							"loadd 0x4:(r11,r10),(r3,r2) \n\t"\
							"\n\t"\
							"movd (r3,r2),(r9,r8) \n\t"\
							"xorw r9,r8 \n\t"\
							"movzb r8,r9 \n\t"\
							"lshw $-8,r8 \n\t"\
							"xorb r9,r8 \n\t"\
							"movzb r8,r9 \n\t"\
							"ashuw $0x8,r9 \n\t"\
							"xorb r8,r9 \n\t"\
							"movw r9,r8 \n\t"\
							"xord (r9,r8),(r3,r2) \n\t"\
							\
							\
							"loadd 0x8:(r11,r10),(r5,r4) \n\t"\
							"\n\t"\
							"movd (r5,r4),(r9,r8) \n\t"\
							"xorw r9,r8 \n\t"\
							"movzb r8,r9 \n\t"\
							"lshw $-8,r8 \n\t"\
							"xorb r9,r8 \n\t"\
							"movzb r8,r9 \n\t"\
							"ashuw $0x8,r9 \n\t"\
							"xorb r8,r9 \n\t"\
							"movw r9,r8 \n\t"\
							"xord (r9,r8),(r5,r4) \n\t"\
							\
							\
							"loadd 0xc:(r11,r10),(r7,r6) \n\t"\
							"\n\t"\
							"movd (r7,r6),(r9,r8) \n\t"\
							"xorw r9,r8 \n\t"\
							"movzb r8,r9 \n\t"\
							"lshw $-8,r8 \n\t"\
							"xorb r9,r8 \n\t"\
							"movzb r8,r9 \n\t"\
							"ashuw $0x8,r9 \n\t"\
							"xorb r8,r9 \n\t"\
							"movw r9,r8 \n\t"\
							"xord (r9,r8),(r7,r6) \n\t"\
							\
							:"=r" (__t0l),"=r" (__t1l),"=r" (__t2l),"=r" (__t3l)  \
							: "0" (__t0l), "1" (__t1l), "2" (__t2l), "3" (__t3l), "r" (__Xl)  \
							: "r8","r9"\
							);\
							t0=__t0l; \
							t1=__t1l; \
							t2=__t2l; \
							t3=__t3l; \
	})


#define MMPMM\
	({ \
			register long __t0l __asm__ ("r0") =t0; \
			register long __t1l __asm__ ("r2") =t1; \
			register long __t2l __asm__ ("r4") =t2; \
			register long __t3l __asm__ ("r6") =t3; \
		 	asm volatile(\
					"xord (r5,r4),(r3,r2) \n\t"\
					"xord (r7,r6),(r5,r4) \n\t"\
					"xord (r3,r2),(r1,r0) \n\t"\
					"xord (r3,r2),(r7,r6) \n\t"\
					"xord (r1,r0), (r5,r4) \n\t"\
					"xord (r5,r4), (r3,r2) \n\t"\
		 			\
					"movw r1,r8 \n\t"\
					"movw r2,r1 \n\t"\
					"movw r3,r2 \n\t"\
					"ashud $8,(r3,r2)\n\t"\
					"movw r1,r2 \n\t"\
					"ashud $8,(r2,r1)\n\t"\
					"movw r8,r1 \n\t"\
					\
					"movw r5,r8 \n\t"\
					"movw r4,r5 \n\t"\
					"movw r8,r5 \n\t"\
					\
					"movw r6,r8 \n\t"\
					"movw r7,r6 \n\t"\
					"ashud $-8,(r7,r6)\n\t"\
					"movw r8,r7 \n\t"\
					"ashud $-8,(r8,r7)\n\t"\
					\
					\
					:"=r" (__t0l),"=r" (__t1l),"=r" (__t2l),"=r" (__t3l)  \
					: "0" (__t0l), "1" (__t1l), "2" (__t2l), "3" (__t3l)  \
					: "r8"\
					);\
					t0=__t0l; \
					t1=__t1l; \
					t2=__t2l; \
					t3=__t3l; \
		})

#endif

const Word KRK[3][4] = {
	{0x517cc1b7, 0x27220a94, 0xfe13abe8, 0xfa9a6ee0},
	{0x6db14acc, 0x9e21c820, 0xff28b1d5, 0xef5de2b0},
	{0xdb92371d, 0x2126e970, 0x03249775, 0x04e8c90e}
};
#ifdef SC1445x_OPT


const Word S1[]={
	XX(AAA,63,7c,77,7b,f2,6b,6f,c5,30,01,67,2b,fe,d7,ab,76),
	XX(AAA,ca,82,c9,7d,fa,59,47,f0,ad,d4,a2,af,9c,a4,72,c0),
	XX(AAA,b7,fd,93,26,36,3f,f7,cc,34,a5,e5,f1,71,d8,31,15),
	XX(AAA,04,c7,23,c3,18,96,05,9a,07,12,80,e2,eb,27,b2,75),
	XX(AAA,09,83,2c,1a,1b,6e,5a,a0,52,3b,d6,b3,29,e3,2f,84),
	XX(AAA,53,d1,00,ed,20,fc,b1,5b,6a,cb,be,39,4a,4c,58,cf),
	XX(AAA,d0,ef,aa,fb,43,4d,33,85,45,f9,02,7f,50,3c,9f,a8),
	XX(AAA,51,a3,40,8f,92,9d,38,f5,bc,b6,da,21,10,ff,f3,d2),
	XX(AAA,cd,0c,13,ec,5f,97,44,17,c4,a7,7e,3d,64,5d,19,73),
	XX(AAA,60,81,4f,dc,22,2a,90,88,46,ee,b8,14,de,5e,0b,db),
	XX(AAA,e0,32,3a,0a,49,06,24,5c,c2,d3,ac,62,91,95,e4,79),
	XX(AAA,e7,c8,37,6d,8d,d5,4e,a9,6c,56,f4,ea,65,7a,ae,08),
	XX(AAA,ba,78,25,2e,1c,a6,b4,c6,e8,dd,74,1f,4b,bd,8b,8a),
	XX(AAA,70,3e,b5,66,48,03,f6,0e,61,35,57,b9,86,c1,1d,9e),
	XX(AAA,e1,f8,98,11,69,d9,8e,94,9b,1e,87,e9,ce,55,28,df),
	XX(AAA,8c,a1,89,0d,bf,e6,42,68,41,99,2d,0f,b0,54,bb,16),
	XX(BBB,e2,4e,54,fc,94,c2,4a,cc,62,0d,6a,46,3c,4d,8b,d1),
	XX(BBB,5e,fa,64,cb,b4,97,be,2b,bc,77,2e,03,d3,19,59,c1),
	XX(BBB,1d,06,41,6b,55,f0,99,69,ea,9c,18,ae,63,df,e7,bb),
	XX(BBB,00,73,66,fb,96,4c,85,e4,3a,09,45,aa,0f,ee,10,eb),
	XX(BBB,2d,7f,f4,29,ac,cf,ad,91,8d,78,c8,95,f9,2f,ce,cd),
	XX(BBB,08,7a,88,38,5c,83,2a,28,47,db,b8,c7,93,a4,12,53),
	XX(BBB,ff,87,0e,31,36,21,58,48,01,8e,37,74,32,ca,e9,b1),
	XX(BBB,b7,ab,0c,d7,c4,56,42,26,07,98,60,d9,b6,b9,11,40),
	XX(BBB,ec,20,8c,bd,a0,c9,84,04,49,23,f1,4f,50,1f,13,dc),
	XX(BBB,d8,c0,9e,57,e3,c3,7b,65,3b,02,8f,3e,e8,25,92,e5),
	XX(BBB,15,dd,fd,17,a9,bf,d4,9a,7e,c5,39,67,fe,76,9d,43),
	XX(BBB,a7,e1,d0,f5,68,f2,1b,34,70,05,a3,8a,d5,79,86,a8),
	XX(BBB,30,c6,51,4b,1e,a6,27,f6,35,d2,6e,24,16,82,5f,da),
	XX(BBB,e6,75,a2,ef,2c,b2,1c,9f,5d,6f,80,0a,72,44,9b,6c),
	XX(BBB,90,0b,5b,33,7d,5a,52,f3,61,a1,f7,b0,d6,3f,7c,6d),
	XX(BBB,ed,14,e0,a5,3d,22,b3,f8,89,de,71,1a,af,ba,b5,81),
	XX(CCC,52,09,6a,d5,30,36,a5,38,bf,40,a3,9e,81,f3,d7,fb),
	XX(CCC,7c,e3,39,82,9b,2f,ff,87,34,8e,43,44,c4,de,e9,cb),
	XX(CCC,54,7b,94,32,a6,c2,23,3d,ee,4c,95,0b,42,fa,c3,4e),
	XX(CCC,08,2e,a1,66,28,d9,24,b2,76,5b,a2,49,6d,8b,d1,25),
	XX(CCC,72,f8,f6,64,86,68,98,16,d4,a4,5c,cc,5d,65,b6,92),
	XX(CCC,6c,70,48,50,fd,ed,b9,da,5e,15,46,57,a7,8d,9d,84),
	XX(CCC,90,d8,ab,00,8c,bc,d3,0a,f7,e4,58,05,b8,b3,45,06),
	XX(CCC,d0,2c,1e,8f,ca,3f,0f,02,c1,af,bd,03,01,13,8a,6b),
	XX(CCC,3a,91,11,41,4f,67,dc,ea,97,f2,cf,ce,f0,b4,e6,73),
	XX(CCC,96,ac,74,22,e7,ad,35,85,e2,f9,37,e8,1c,75,df,6e),
	XX(CCC,47,f1,1a,71,1d,29,c5,89,6f,b7,62,0e,aa,18,be,1b),
	XX(CCC,fc,56,3e,4b,c6,d2,79,20,9a,db,c0,fe,78,cd,5a,f4),
	XX(CCC,1f,dd,a8,33,88,07,c7,31,b1,12,10,59,27,80,ec,5f),
	XX(CCC,60,51,7f,a9,19,b5,4a,0d,2d,e5,7a,9f,93,c9,9c,ef),
	XX(CCC,a0,e0,3b,4d,ae,2a,f5,b0,c8,eb,bb,3c,83,53,99,61),
	XX(CCC,17,2b,04,7e,ba,77,d6,26,e1,69,14,63,55,21,0c,7d),
	XX(DDD,30,68,99,1b,87,b9,21,78,50,39,db,e1,72,09,62,3c),
	XX(DDD,3e,7e,5e,8e,f1,a0,cc,a3,2a,1d,fb,b6,d6,20,c4,8d),
	XX(DDD,81,65,f5,89,cb,9d,77,c6,57,43,56,17,d4,40,1a,4d),
	XX(DDD,c0,63,6c,e3,b7,c8,64,6a,53,aa,38,98,0c,f4,9b,ed),
	XX(DDD,7f,22,76,af,dd,3a,0b,58,67,88,06,c3,35,0d,01,8b),
	XX(DDD,8c,c2,e6,5f,02,24,75,93,66,1e,e5,e2,54,d8,10,ce),
	XX(DDD,7a,e8,08,2c,12,97,32,ab,b4,27,0a,23,df,ef,ca,d9),
	XX(DDD,b8,fa,dc,31,6b,d1,ad,19,49,bd,51,96,ee,e4,a8,41),
	XX(DDD,da,ff,cd,55,86,36,be,61,52,f8,bb,0e,82,48,69,9a),
	XX(DDD,e0,47,9e,5c,04,4b,34,15,79,26,a7,de,29,ae,92,d7),
	XX(DDD,84,e9,d2,ba,5d,f3,c5,b0,bf,a4,3b,71,44,46,2b,fc),
	XX(DDD,eb,6f,d5,f6,14,fe,7c,70,5a,7d,fd,2f,18,83,16,a5),
	XX(DDD,91,1f,05,95,74,a9,c1,5b,4a,85,6d,13,07,4f,4e,45),
	XX(DDD,b2,0f,c9,1c,a6,bc,ec,73,90,7b,cf,59,8f,a1,f9,2d),
	XX(DDD,f2,b1,00,94,37,9f,d0,2e,9c,6e,28,3f,80,f0,3d,d3),
	XX(DDD,25,8a,b5,e7,42,b3,c7,ea,f7,4c,11,33,03,a2,ac,60)
};
const Word *S2= (const Word*)((const Word)S1+0x400);
const Word *X1= (const Word*)((const Word)S1+0x800);
const Word *X2= (const Word*)((const Word)S1+0xC00);

#else
const Word S1[256]={
	XX(AAA,63,7c,77,7b,f2,6b,6f,c5,30,01,67,2b,fe,d7,ab,76),
	XX(AAA,ca,82,c9,7d,fa,59,47,f0,ad,d4,a2,af,9c,a4,72,c0),
	XX(AAA,b7,fd,93,26,36,3f,f7,cc,34,a5,e5,f1,71,d8,31,15),
	XX(AAA,04,c7,23,c3,18,96,05,9a,07,12,80,e2,eb,27,b2,75),
	XX(AAA,09,83,2c,1a,1b,6e,5a,a0,52,3b,d6,b3,29,e3,2f,84),
	XX(AAA,53,d1,00,ed,20,fc,b1,5b,6a,cb,be,39,4a,4c,58,cf),
	XX(AAA,d0,ef,aa,fb,43,4d,33,85,45,f9,02,7f,50,3c,9f,a8),
	XX(AAA,51,a3,40,8f,92,9d,38,f5,bc,b6,da,21,10,ff,f3,d2),
	XX(AAA,cd,0c,13,ec,5f,97,44,17,c4,a7,7e,3d,64,5d,19,73),
	XX(AAA,60,81,4f,dc,22,2a,90,88,46,ee,b8,14,de,5e,0b,db),
	XX(AAA,e0,32,3a,0a,49,06,24,5c,c2,d3,ac,62,91,95,e4,79),
	XX(AAA,e7,c8,37,6d,8d,d5,4e,a9,6c,56,f4,ea,65,7a,ae,08),
	XX(AAA,ba,78,25,2e,1c,a6,b4,c6,e8,dd,74,1f,4b,bd,8b,8a),
	XX(AAA,70,3e,b5,66,48,03,f6,0e,61,35,57,b9,86,c1,1d,9e),
	XX(AAA,e1,f8,98,11,69,d9,8e,94,9b,1e,87,e9,ce,55,28,df),
	XX(AAA,8c,a1,89,0d,bf,e6,42,68,41,99,2d,0f,b0,54,bb,16)
};

const Word S2[256]={
	XX(BBB,e2,4e,54,fc,94,c2,4a,cc,62,0d,6a,46,3c,4d,8b,d1),
	XX(BBB,5e,fa,64,cb,b4,97,be,2b,bc,77,2e,03,d3,19,59,c1),
	XX(BBB,1d,06,41,6b,55,f0,99,69,ea,9c,18,ae,63,df,e7,bb),
	XX(BBB,00,73,66,fb,96,4c,85,e4,3a,09,45,aa,0f,ee,10,eb),
	XX(BBB,2d,7f,f4,29,ac,cf,ad,91,8d,78,c8,95,f9,2f,ce,cd),
	XX(BBB,08,7a,88,38,5c,83,2a,28,47,db,b8,c7,93,a4,12,53),
	XX(BBB,ff,87,0e,31,36,21,58,48,01,8e,37,74,32,ca,e9,b1),
	XX(BBB,b7,ab,0c,d7,c4,56,42,26,07,98,60,d9,b6,b9,11,40),
	XX(BBB,ec,20,8c,bd,a0,c9,84,04,49,23,f1,4f,50,1f,13,dc),
	XX(BBB,d8,c0,9e,57,e3,c3,7b,65,3b,02,8f,3e,e8,25,92,e5),
	XX(BBB,15,dd,fd,17,a9,bf,d4,9a,7e,c5,39,67,fe,76,9d,43),
	XX(BBB,a7,e1,d0,f5,68,f2,1b,34,70,05,a3,8a,d5,79,86,a8),
	XX(BBB,30,c6,51,4b,1e,a6,27,f6,35,d2,6e,24,16,82,5f,da),
	XX(BBB,e6,75,a2,ef,2c,b2,1c,9f,5d,6f,80,0a,72,44,9b,6c),
	XX(BBB,90,0b,5b,33,7d,5a,52,f3,61,a1,f7,b0,d6,3f,7c,6d),
	XX(BBB,ed,14,e0,a5,3d,22,b3,f8,89,de,71,1a,af,ba,b5,81)
};

const Word X1[256]={
	XX(CCC,52,09,6a,d5,30,36,a5,38,bf,40,a3,9e,81,f3,d7,fb),
	XX(CCC,7c,e3,39,82,9b,2f,ff,87,34,8e,43,44,c4,de,e9,cb),
	XX(CCC,54,7b,94,32,a6,c2,23,3d,ee,4c,95,0b,42,fa,c3,4e),
	XX(CCC,08,2e,a1,66,28,d9,24,b2,76,5b,a2,49,6d,8b,d1,25),
	XX(CCC,72,f8,f6,64,86,68,98,16,d4,a4,5c,cc,5d,65,b6,92),
	XX(CCC,6c,70,48,50,fd,ed,b9,da,5e,15,46,57,a7,8d,9d,84),
	XX(CCC,90,d8,ab,00,8c,bc,d3,0a,f7,e4,58,05,b8,b3,45,06),
	XX(CCC,d0,2c,1e,8f,ca,3f,0f,02,c1,af,bd,03,01,13,8a,6b),
	XX(CCC,3a,91,11,41,4f,67,dc,ea,97,f2,cf,ce,f0,b4,e6,73),
	XX(CCC,96,ac,74,22,e7,ad,35,85,e2,f9,37,e8,1c,75,df,6e),
	XX(CCC,47,f1,1a,71,1d,29,c5,89,6f,b7,62,0e,aa,18,be,1b),
	XX(CCC,fc,56,3e,4b,c6,d2,79,20,9a,db,c0,fe,78,cd,5a,f4),
	XX(CCC,1f,dd,a8,33,88,07,c7,31,b1,12,10,59,27,80,ec,5f),
	XX(CCC,60,51,7f,a9,19,b5,4a,0d,2d,e5,7a,9f,93,c9,9c,ef),
	XX(CCC,a0,e0,3b,4d,ae,2a,f5,b0,c8,eb,bb,3c,83,53,99,61),
	XX(CCC,17,2b,04,7e,ba,77,d6,26,e1,69,14,63,55,21,0c,7d)
};

const Word X2[256]={
	XX(DDD,30,68,99,1b,87,b9,21,78,50,39,db,e1,72,09,62,3c),
	XX(DDD,3e,7e,5e,8e,f1,a0,cc,a3,2a,1d,fb,b6,d6,20,c4,8d),
	XX(DDD,81,65,f5,89,cb,9d,77,c6,57,43,56,17,d4,40,1a,4d),
	XX(DDD,c0,63,6c,e3,b7,c8,64,6a,53,aa,38,98,0c,f4,9b,ed),
	XX(DDD,7f,22,76,af,dd,3a,0b,58,67,88,06,c3,35,0d,01,8b),
	XX(DDD,8c,c2,e6,5f,02,24,75,93,66,1e,e5,e2,54,d8,10,ce),
	XX(DDD,7a,e8,08,2c,12,97,32,ab,b4,27,0a,23,df,ef,ca,d9),
	XX(DDD,b8,fa,dc,31,6b,d1,ad,19,49,bd,51,96,ee,e4,a8,41),
	XX(DDD,da,ff,cd,55,86,36,be,61,52,f8,bb,0e,82,48,69,9a),
	XX(DDD,e0,47,9e,5c,04,4b,34,15,79,26,a7,de,29,ae,92,d7),
	XX(DDD,84,e9,d2,ba,5d,f3,c5,b0,bf,a4,3b,71,44,46,2b,fc),
	XX(DDD,eb,6f,d5,f6,14,fe,7c,70,5a,7d,fd,2f,18,83,16,a5),
	XX(DDD,91,1f,05,95,74,a9,c1,5b,4a,85,6d,13,07,4f,4e,45),
	XX(DDD,b2,0f,c9,1c,a6,bc,ec,73,90,7b,cf,59,8f,a1,f9,2d),
	XX(DDD,f2,b1,00,94,37,9f,d0,2e,9c,6e,28,3f,80,f0,3d,d3),
	XX(DDD,25,8a,b5,e7,42,b3,c7,ea,f7,4c,11,33,03,a2,ac,60)
};
#endif
/**
 * Expand the cipher key into the encryption key schedule.
 */
int ARIA_set_encrypt_key(const unsigned char *userKey, const int bits, ARIA_KEY *key)
{
	register Word t0, t1, t2, t3;
	Word w0[4], w1[4], w2[4], w3[4];
	int q, r;

	Byte *mk;
	Byte *rk;
	short int keyBits;


	if (!userKey || !key) {
		return (-1);
	}
	if ((bits != 128) && (bits != 192) && (bits != 256)) {
		return (-2);
	}

	mk = (Byte*) userKey;
	rk = (Byte*) key->rd_key;
	keyBits = (short int) bits;

	key->rounds = (keyBits + 256) / 32;

	WordLoad(WO(mk,0), w0[0]);
	WordLoad(WO(mk,1), w0[1]);
	WordLoad(WO(mk,2), w0[2]);
	WordLoad(WO(mk,3), w0[3]);

	//printf ("\r\n W00: %8.8X - %8.8X - %8.8X -%8.8X   \r\n: ",w0[0],w0[1],w0[2],w0[3]);

	q = (keyBits - 128) / 64;

#ifdef SC1445x_OPT

	Word * KRKrow=(Word *)KRK[q];
	t0 = w0[0]^KRKrow[0];
	KRKrow++;
	t1 = w0[1]^KRKrow[0];
	KRKrow++;
	t2 = w0[2]^KRKrow[0];
	KRKrow++;
	t3 = w0[3]^KRKrow[0];
#else
	t0 = w0[0] ^ KRK[q][0];
	t1 = w0[1] ^ KRK[q][1];
	t2 = w0[2] ^ KRK[q][2];
	t3 = w0[3] ^ KRK[q][3];
#endif

	//printf ("\r\n T00: %8.8X - %8.8X - %8.8X -%8.8X   \r\n: ",t0,t1,t2 ,t3 );

	FO;

	if (keyBits > 128) {
		WordLoad(WO(mk, 4), w1[0]);
		WordLoad(WO(mk, 5), w1[1]);
		if (keyBits > 192) {
			WordLoad(WO(mk, 6), w1[2]);
			WordLoad(WO(mk, 7), w1[3]);
		}
		else {
			w1[2] = w1[3] = 0;
		}
	}
	else {
		w1[0] = w1[1] = w1[2] = w1[3] = 0;
	}

	w1[0] ^= t0;
	w1[1] ^= t1;
	w1[2] ^= t2;
	w1[3] ^= t3;
	t0 = w1[0];
	t1 = w1[1];
	t2 = w1[2];
	t3 = w1[3];

	q = (q == 2)? 0 : (q + 1);

#ifdef SC1445x_OPT

	KRKrow=(Word *)KRK[q];

	t0 = t0^KRKrow[0];
	KRKrow++;
	t1 = t1^KRKrow[0];
	KRKrow++;
	t2 = t2^KRKrow[0];
	KRKrow++;
	t3 = t3^KRKrow[0];

#else
	t0 ^= KRK[q][0];
	t1 ^= KRK[q][1];
	t2 ^= KRK[q][2];
	t3 ^= KRK[q][3];
#endif
	FE;

	t0 ^= w0[0];
	t1 ^= w0[1];
	t2 ^= w0[2];
	t3 ^= w0[3];
	w2[0] = t0;
	w2[1] = t1;
	w2[2] = t2;
	w2[3] = t3;

	q = (q == 2)? 0 : (q + 1);

#ifdef SC1445x_OPT

	KRKrow=(Word *)KRK[q];

	t0 = t0^KRKrow[0];
	KRKrow++;
	t1 = t1^KRKrow[0];
	KRKrow++;
	t2 = t2^KRKrow[0];
	KRKrow++;
	t3 = t3^KRKrow[0];
#else
	t0 ^= KRK[q][0];
	t1 ^= KRK[q][1];
	t2 ^= KRK[q][2];
	t3 ^= KRK[q][3];
#endif

	FO;

	w3[0] = t0 ^ w1[0];
	w3[1] = t1 ^ w1[1];
	w3[2] = t2 ^ w1[2];
	w3[3] = t3 ^ w1[3];
#ifdef SC1445x_OPT

	GSRK19(w0, w1, 19);
	GSRK19(w1, w2, 19);
	GSRK19(w2, w3, 19);
	GSRK19(w3, w0, 19);

	GSRK31(w0, w1, 31);
	GSRK31(w1, w2, 31);
	GSRK31(w2, w3, 31);
	GSRK31(w3, w0, 31);

	GSRK67(w0, w1, 67);
	GSRK67(w1, w2, 67);
	GSRK67(w2, w3, 67);
	GSRK67(w3, w0, 67);

	GSRK97(w0, w1, 97);

	if (keyBits > 128) {
		GSRK97(w1, w2, 97);
		GSRK97(w2, w3, 97);
	}
	if (keyBits > 192) {
		GSRK97(w3, w0,  97);
		GSRK109(w0, w1, 109);
	}
#else
	GSRK(w0, w1, 19);
	GSRK(w1, w2, 19);
	GSRK(w2, w3, 19);
	GSRK(w3, w0, 19);

	GSRK(w0, w1, 31);
	GSRK(w1, w2, 31);
	GSRK(w2, w3, 31);
	GSRK(w3, w0, 31);

	GSRK(w0, w1, 67);
	GSRK(w1, w2, 67);
	GSRK(w2, w3, 67);
	GSRK(w3, w0, 67);

	GSRK(w0, w1, 97);

	if (keyBits > 128) {
		GSRK(w1, w2, 97);
		GSRK(w2, w3, 97);
	}
	if (keyBits > 192) {
		GSRK(w3, w0,  97);
		GSRK(w0, w1, 109);
	}

#endif
	return (0);
}
#ifdef ARIAFULLASM
int ARIA_set_decrypt_key(const unsigned char *userKey, const int bits, ARIA_KEY *key)
{

	Word *a, *z;
	int rValue;
	register Byte sum;
	register Word t0, t1, t2, t3;
	Word s0, s1, s2, s3;

	Byte *mk;
	Byte *rk;
	int keyBits;

	mk = (Byte*) userKey;
	rk = (Byte*) key->rd_key;
	keyBits = (int) bits;

	ARIA_set_encrypt_key(userKey, bits, key);
	rValue = (keyBits + 256) / 32;
	a = (Word*)(rk);
	z = a + rValue * 4;

	t0 = a[0];
	t1 = a[1];
	t2 = a[2];
	t3 = a[3];

	a[0] = z[0];
	a[1] = z[1];
	a[2] = z[2];
	a[3] = z[3];

	z[0] = t0;
	z[1] = t1;
	z[2] = t2;
	z[3] = t3;

	a += 4;
	z -= 4;

	for (; a < z; a += 4, z -= 4) {
		WordM1p (&a[0]);

		MM(t0, t1, t2, t3) P(t0, t1, t2, t3) MM(t0, t1, t2, t3)
		s0 = t0;
		s1 = t1;
		s2 = t2;
		s3 = t3;

		WordM1p (&z[0]);

		MM(t0, t1, t2, t3) P(t0, t1, t2, t3) MM(t0, t1, t2, t3)
		a[0] = t0;
		a[1] = t1;
		a[2] = t2;
		a[3] = t3;
		z[0] = s0;
		z[1] = s1;
		z[2] = s2;
		z[3] = s3;
	}

		WordM1p (&a[0]);
	MM(t0, t1, t2, t3) P(t0, t1, t2, t3) MM(t0, t1, t2, t3)
	z[0] = t0;
	z[1] = t1;
	z[2] = t2;
	z[3] = t3;

	return (0);
}
#else
/**
 * Expand the cipher key into the decryption key schedule.
 */
int ARIA_set_decrypt_key(const unsigned char *userKey, const int bits, ARIA_KEY *key)
{

	Word *a, *z;
	int rValue;
#if defined(_MSC_VER)
	register Word w;
#else
	register Byte sum;
#endif
	register Word t0, t1, t2, t3;
	Word s0, s1, s2, s3;

	Byte *mk;
	Byte *rk;
	int keyBits;

	mk = (Byte*) userKey;
	rk = (Byte*) key->rd_key;
	keyBits = (int) bits;

#if 1
	ARIA_set_encrypt_key(userKey, bits, key);
	rValue = (keyBits + 256) / 32;
#else
	rValue = EncKeySetup(mk, rk, keyBits);
#endif
	a = (Word*)(rk);
	z = a + rValue * 4;

	t0 = a[0];
	t1 = a[1];
	t2 = a[2];
	t3 = a[3];

	a[0] = z[0];
	a[1] = z[1];
	a[2] = z[2];
	a[3] = z[3];

	z[0] = t0;
	z[1] = t1;
	z[2] = t2;
	z[3] = t3;

	a += 4;
	z -= 4;

	for (; a < z; a += 4, z -= 4) {
		WordM1(a[0], t0);
		WordM1(a[1], t1);
		WordM1(a[2], t2);
		WordM1(a[3], t3);
		MM(t0, t1, t2, t3) P(t0, t1, t2, t3) MM(t0, t1, t2, t3)
		s0 = t0;
		s1 = t1;
		s2 = t2;
		s3 = t3;

		WordM1(z[0], t0);
		WordM1(z[1], t1);
		WordM1(z[2], t2);
		WordM1(z[3], t3);
		MM(t0, t1, t2, t3) P(t0, t1, t2, t3) MM(t0, t1, t2, t3)
		a[0] = t0;
		a[1] = t1;
		a[2] = t2;
		a[3] = t3;
		z[0] = s0;
		z[1] = s1;
		z[2] = s2;
		z[3] = s3;
	}

	WordM1(a[0], t0);
	WordM1(a[1], t1);
	WordM1(a[2], t2);
	WordM1(a[3], t3);
	MM(t0, t1, t2, t3) P(t0, t1, t2, t3) MM(t0, t1, t2, t3)
	z[0] = t0;
	z[1] = t1;
	z[2] = t2;
	z[3] = t3;

	return (0);
}
#endif
#ifdef SC1445x_OPT

#define STXO1(o,rk,t0,S1)\
	({ \
		register long __r0 __asm__ ("r0") ; \
		register long __r2 __asm__ ("r2") ;  \
	 	register long __r4 __asm__ ("r4") = (long)(t0); \
	 	register long __r6 __asm__ ("r6") = (long)(rk); \
	 	register long __r8 __asm__ ("r8") = (long)(o); \
	 	register long __r10 __asm__ ("r10") = (long)(S1); \
	 	asm volatile(\
							"loadd 0x0:(r7,r6),(r1,r0) \n\t"\
							"\n\t"\
							"movw r4,r2 \n\t"\
							"movw $0,r3 \n\t"\
							"andw $0xff,r2 \n\t"\
							"lshw $2,r2 \n\t"\
							"addd (r11,r10),(r3,r2) \n\t"\
							"\n\t"\
							"loadb 0x400:(r3,r2),r2 \n\t"\
							"xorb r2,r0 \n\t"\
							"\n\t"\
							"movw r4,r2 \n\t"\
							"movw $0,r3 \n\t"\
							"lshw $-6,r2 \n\t"\
							"andw $0x3fc,r2 \n\t"\
							"addd (r11,r10),(r3,r2) \n\t"\
							"\n\t"\
							"loadb 0x0:(r3,r2),r2 \n\t"\
							"lshw $8,r2 \n\t"\
							"xorw r2,r0 \n\t"\
							"\n\t"\
							"movw r5,r2 \n\t"\
							"movw $0,r3 \n\t"\
							"andw $0xff,r2 \n\t"\
							"lshd $2,(r3,r2) \n\t"\
							"addd (r11,r10),(r3,r2) \n\t"\
							"\n\t"\
							"loadw 0xC00:(r3,r2),r2 \n\t"\
							"lshw $-8,r2 \n\t"\
							"andw $0xff,r2 \n\t"\
							"xorb r2,r1 \n\t"\
							"\n\t"\
							"movw r5,r2 \n\t"\
							"movw $0,r3 \n\t"\
							"lshd $-6,(r3,r2) \n\t"\
							"andw $0x3fc,r2 \n\t"\
							"addd (r11,r10),(r3,r2) \n\t"\
							"\n\t"\
							"loadb 0x800:(r3,r2),r2 \n\t"\
							"lshw $8,r2 \n\t"\
							"xorw r2,r1 \n\t"\
							"\n\t"\
							"movw r1,r3 \n\t"\
							"movw r0,r1 \n\t"\
							"movw r3,r2 \n\t"\
							"ashud $8,(r3,r2)\n\t"\
							"ashud $8,(r1,r0)\n\t"\
							"movw r3,r0 \n\t"\
							"\n\t"\
							"stord (r1,r0),0:(r9,r8)\n\t"\
							"\n\t"\
							"\n\t"\
							:"=r" (__r0), "=r" (__r2) \
							: "0" (__r0), "1" (__r2), "r" (__r4), "r" (__r6), "r" (__r8), "r" (__r10) \
							: "memory" \
							);\
							__r0; \
	})

#endif


/*	WordLoad(WO(i,0), t0);
	WordLoad(WO(i,1), t1);
	WordLoad(WO(i,2), t2);
	WordLoad(WO(i,3), t3); */


#define WORDL {\
		register long t0v  __asm__ ("r0") = (long)(t0);\
		register long t1v  __asm__ ("r2") = (long)(t1);\
		register long t2v  __asm__ ("r4") = (long)(t2);\
		register long t3v  __asm__ ("r6") = (long)(t3);\
		register long iv  __asm__ ("r10") = (long)(i);\
			asm volatile(\
						"loadd	0(r11,r10),(r1,r0) \n\t"\
						"loadd	4(r11,r10),(r5,r4) \n\t"\
						"movw r0,r2 \n\t"\
						"movw r1,r0 \n\t"\
						"ashud $-8,(r1,r0)\n\t"\
						"movw r2,r1 \n\t"\
						"ashud $-8,(r2,r1)\n\t"\
						\
						"loadd	8(r11,r10),(r7,r6) \n\t"\
						"movw r5,r2 \n\t"\
						"movw r5,r3 \n\t"\
						"ashud $-8,(r3,r2)\n\t"\
						"movw r4,r3 \n\t"\
						"ashud $-8,(r4,r3)\n\t"\
						\
						"loadd	0xC(r11,r10),(r9,r8) \n\t"\
						"movw r7,r5 \n\t"\
						"movw r7,r4 \n\t"\
						"ashud $-8,(r5,r4)\n\t"\
						"movw r6,r5 \n\t"\
						"ashud $-8,(r6,r5)\n\t"\
						\
						"movw r9,r7 \n\t"\
						"movw r9,r6 \n\t"\
						"ashud $-8,(r7,r6)\n\t"\
						"movw r8,r7 \n\t"\
						"ashud $-8,(r8,r7)\n\t"\
			:"=r" (t0v),"=r" (t1v),"=r" (t2v),"=r" (t3v) \
			:"0" (t0v),"1" (t1v), "2" (t2v), "3" (t3v),"r" (iv) \
			: "r8","r9");\
			t0=t0v;\
			t1=t1v;\
			t2=t2v;\
			t3=t3v;\
		}

#ifdef ARIAFULLASM
#define KXLFOKXLFE \
		{\
		register long t0v  __asm__ ("r0") = (long)(t0);\
		register long t1v  __asm__ ("r2") = (long)(t1);\
		register long t2v  __asm__ ("r4") = (long)(t2);\
		register long t3v  __asm__ ("r6") = (long)(t3);\
		register long rkv  __asm__ ("r8") = (long)(rk);\
		register long S1v  __asm__ ("r13") = (long)(S1);\
			asm volatile(\
						"loadd	0(r9,r8),(r11,r10) \n\t"\
						"loadd	4(r9,r8),(ra) \n\t"\
						"xord (r11,r10),(r1,r0) \n\t"\
						"xord (ra),(r3,r2) \n\t"\
						"loadd	8(r9,r8),(r11,r10) \n\t"\
						"loadd	0xc(r9,r8),(ra) \n\t"\
						"addd $0x10,(r9,r8) \n\t"\
						"xord (r11,r10),(r5,r4) \n\t"\
						"xord (ra),(r7,r6) \n\t"\
						/*FO*/\
						/*T0 = ((Word *)(((Word)S1)+ BRFOPAMM(T0)))[0] ^
						 * 	   ((Word *)(((Word)S2)+ BRFOPAML(T0)))[0] ^
						 * 	   ((Word *)(((Word)X1)+ BRFOPALM(T0)))[0] ^
						 * 	   ((Word *)(((Word)X2)+ BRFOPALL(T0)))[0];	*/\
						 "movzw r1,(r11,r10) \n\t"\
						 "lshw $-8:s,r10 \n\t"\
						 "ashuw	$2:s,r10 \n\t"\
						 "addd	(r13),(r11,r10) \n\t"\
						 "loadd  0(r11,r10),(ra)\n\t"\
						 "xorw  r11, r11 \n\t"\
						 \
						 "movzb r1, r10 \n\t"\
						 "ashuw	$2:s,r10 \n\t"\
						 "addd	(r13),(r11,r10) \n\t"\
						 "loadd  0x400(r11,r10),(r11,r10)\n\t"\
						 "xord (r11,r10),(ra) \n\t"\
						 "xorw  r11, r11 \n\t"\
						 \
						 "movw r0,r10 \n\t"\
						 "lshw $-8:s,r10 \n\t"\
						 "ashuw	$2:s,r10 \n\t"\
						 "addd	(r13),(r11,r10) \n\t"\
						 "loadd 0x800(r11,r10),(r11,r10)\n\t"\
						 \
						 "movzb r0,r0 \n\t"\
						 "movzw r0,(r1,r0) \n\t"\
						 "ashuw	$2:s,r0 \n\t"\
						 "addd	(r13),(r1,r0) \n\t"\
						 "loadd 0xC00(r1,r0),(r1,r0)\n\t"\
						 \
						 "xord (r11,r10),(ra) \n\t"\
						 "xord	(ra),(r1,r0) \n\t"\
						 \
						 /*T1 = ((Word *)(((Word)S1)+ BRFOPAMM(T1)))[0] ^
						  * 	   ((Word *)(((Word)S2)+ BRFOPAML(T1)))[0] ^
						  * 	   ((Word *)(((Word)X1)+ BRFOPALM(T1)))[0] ^
						  * 	   ((Word *)(((Word)X2)+ BRFOPALL(T1)))[0];	*/\
						 "movzw r3,(r11,r10) \n\t"\
						 "lshw $-8:s,r10 \n\t"\
						 "ashuw	$2:s,r10 \n\t"\
						 "addd	(r13),(r11,r10) \n\t"\
						 "loadd 0(r11,r10),(ra)\n\t"\
						 "xorw  r11, r11 \n\t"\
						 \
						 "movzb r3, r10 \n\t"\
						 "ashuw	$2:s,r10 \n\t"\
						 "addd	(r13),(r11,r10) \n\t"\
						 "loadd 0x400(r11,r10),(r11,r10)\n\t"\
						 "xord (r11,r10),(ra) \n\t"\
						 "xorw  r11, r11 \n\t"\
						 \
						 "movw r2,r10 \n\t"\
						 "lshw $-8:s,r10 \n\t"\
						 "ashuw	$2:s,r10 \n\t"\
						 "addd	(r13),(r11,r10) \n\t"\
						 "loadd 0x800(r11,r10),(r11,r10)\n\t"\
						 "xord (r11,r10),(ra) \n\t"\
						 \
						 "movzb r2,r2 \n\t"\
						 "movzw r2,(r3,r2) \n\t"\
						 "ashuw	$2:s,r2 \n\t"\
						 "addd	(r13),(r3,r2) \n\t"\
						 "loadd 0xC00(r3,r2),(r3,r2)\n\t"\
						 \
						 "xord	(ra),(r3,r2) \n\t"\
						 \
						 \
						 /*T2 = ((Word *)(((Word)S1)+ BRFOPAMM(T2)))[0] ^
						  * 	   ((Word *)(((Word)S2)+ BRFOPAML(T2)))[0] ^
						  * 	   ((Word *)(((Word)X1)+ BRFOPALM(T2)))[0] ^
						  * 	   ((Word *)(((Word)X2)+ BRFOPALL(T2)))[0];	*/\
						 \
						 "movzw r5,(r11,r10) \n\t"\
						 "lshw $-8:s,r10 \n\t"\
						 "ashuw	$2:s,r10 \n\t"\
						 "addd	(r13),(r11,r10) \n\t"\
						 "loadd 0(r11,r10),(ra)\n\t"\
						 \
						 "xorw  r11, r11 \n\t"\
						 "movzb r5, r10 \n\t"\
						 "ashuw	$2:s,r10 \n\t"\
						 "addd	(r13),(r11,r10) \n\t"\
						 "loadd 0x400(r11,r10),(r11,r10)\n\t"\
						 "xord (r11,r10),(ra) \n\t"\
						 \
						 "xorw  r11, r11 \n\t"\
						 "movw r4,r10 \n\t"\
						 "lshw $-8:s,r10 \n\t"\
						 "ashuw	$2:s,r10 \n\t"\
						 "addd	(r13),(r11,r10) \n\t"\
						 "loadd 0x800(r11,r10),(r11,r10)\n\t"\
						 "xord (r11,r10),(ra) \n\t"\
						 \
						/*"andd	$0x0ff,(r5,r4) \n\t"*/\
						 "movzb r4,r4 \n\t"\
						 "movzw r4,(r5,r4) \n\t"\
						 "ashuw	$2:s,r4 \n\t"\
						 "addd	(r13),(r5,r4) \n\t"\
						 "loadd 0xC00(r5,r4),(r5,r4)\n\t"\
						 \
						 "xord	(ra),(r5,r4) \n\t"\
						 \
						 \
						 /*T3 = ((Word *)(((Word)S1)+ BRFOPAMM(T3)))[0] ^
						  * 	   ((Word *)(((Word)S2)+ BRFOPAML(T3)))[0] ^
						  * 	   ((Word *)(((Word)X1)+ BRFOPALM(T3)))[0] ^
						  * 	   ((Word *)(((Word)X2)+ BRFOPALL(T3)))[0];	*/\
						 \
						 "movzw r7,(r11,r10) \n\t"\
						 "lshw $-8:s,r10 \n\t"\
						 "ashuw	$2:s,r10 \n\t"\
						 "addd	(r13),(r11,r10) \n\t"\
						 "loadd 0(r11,r10),(ra)\n\t"\
						 \
						 "xorw  r11, r11 \n\t"\
						 "movzb r7, r10 \n\t"\
						 "ashuw	$2:s,r10 \n\t"\
						 "addd	(r13),(r11,r10) \n\t"\
						 "loadd 0x400(r11,r10),(r11,r10)\n\t"\
						 "xord (r11,r10),(ra) \n\t"\
						 \
						 "xorw  r11, r11 \n\t"\
						 "movw r6,r10 \n\t"\
						 "lshw $-8:s,r10 \n\t"\
						 "ashuw	$2:s,r10 \n\t"\
						 "addd	(r13),(r11,r10) \n\t"\
						 "loadd 0x800(r11,r10),(r11,r10)\n\t"\
						 "xord (r11,r10),(ra) \n\t"\
						 \
						 /*"andd	$0x0ff,(r7,r6) \n\t"*/\
						 "movzb r6,r6 \n\t"\
						 "movzw r6,(r7,r6) \n\t"\
						 "ashuw	$2:s,r6 \n\t"\
						 "addd	(r13),(r7,r6) \n\t"\
						 "loadd 0x0C00(r7,r6),(r7,r6)\n\t"\
						 \
						 "xord	(ra),(r7,r6) \n\t"\
						 \
						 /* MM(T0,T1,T2,T3) 		{					\
						 (T1) ^= (T2); (T2) ^= (T3); (T0) ^= (T1);		\
						  (T3) ^= (T1); (T2) ^= (T0); (T1) ^= (T2);		\
						}*/\
						\
						"xord	(r5,r4),(r3,r2) \n\t"\
						"xord	(r7,r6),(r5,r4) \n\t"\
						"xord	(r3,r2),(r1,r0) \n\t"\
						\
						"xord	(r3,r2),(r7,r6) \n\t"\
						"xord	(r1,r0),(r5,r4) \n\t"\
						"xord	(r5,r4),(r3,r2) \n\t"\
						\
						/*#define P(T0,T1,T2,T3) {															\
							(T1) = PT1(T1);	\
							(T2) = PT2(T2);	\
							ReverseWord((T3)); \
						*/\
						\
						/*(T1) = PT1(T1);*/\
						\
						"movw	r1,r10 \n\t"\
						"movw	r2,r1 \n\t"\
						"movw	r3,r2 \n\t"\
						"ashud	$8:s,(r3,r2) \n\t"\
						"movw	r1,r2 \n\t"\
						"ashud	$8:s,(r2,r1) \n\t"\
						"movw	r10,r1 \n\t"\
						\
						/*(T2) = (((T2) << 16) & 0xffff0000) ^ (((T2) >> 16) & 0x0000ffff);*/\
						\
						"movw r4,r10 \n\t"\
						"movw r5,r4 \n\t"\
						"movw r10,r5 \n\t"\
						\
						/*ReverseWord((T3));*/\
						\
						"movw r8,r10 \n\t"\
						"movw r6,r8 \n\t"\
						"movw r7,r6 \n\t"\
						"lshd $-8,(r7,r6)\n\t"\
						"movw r8,r7 \n\t"\
						"ashud $-8,(r8,r7)\n\t"\
						"movw r10,r8 \n\t"\
						/* MM(T0,T1,T2,T3) 		{					\
						 (T1) ^= (T2); (T2) ^= (T3); (T0) ^= (T1);		\
						 (T3) ^= (T1); (T2) ^= (T0); (T1) ^= (T2);		\
						}*/\
						\
						"xord	(r5,r4),(r3,r2) \n\t"\
						"xord	(r7,r6),(r5,r4) \n\t"\
						"xord	(r3,r2),(r1,r0) \n\t"\
						\
						"xord	(r3,r2),(r7,r6) \n\t"\
						"xord	(r1,r0),(r5,r4) \n\t"\
						"xord	(r5,r4),(r3,r2) \n\t"\
						\
						/*KXL*/\
						"loadd	0(r9,r8),(r11,r10) \n\t"\
						"loadd	4(r9,r8),(ra) \n\t"\
						"xord (r11,r10),(r1,r0) \n\t"\
						"xord (ra),(r3,r2) \n\t"\
						"loadd	8(r9,r8),(r11,r10) \n\t"\
						"loadd	0xc(r9,r8),(ra) \n\t"\
						"addd $0x10,(r9,r8) \n\t"\
						"xord (r11,r10),(r5,r4) \n\t"\
						"xord (ra),(r7,r6) \n\t"\
						\
						\
						/*FE*/\
						/*T0 = ((Word *)(((Word)S1)+ BRFOPAMM(T0)))[0] ^
						 * 	   ((Word *)(((Word)S2)+ BRFOPAML(T0)))[0] ^
						 * 	   ((Word *)(((Word)X1)+ BRFOPALM(T0)))[0] ^
						 * 	   ((Word *)(((Word)X2)+ BRFOPALL(T0)))[0];	*/\
							"movzw r1,(r11,r10) \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd  0x800(r11,r10),(ra)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movzb r1, r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 	0xC00(r11,r10),(r11,r10)\n\t"\
							"xord	(r11,r10), (ra) \n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movw r0,r10 \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0x000(r11,r10),(r11,r10)\n\t"\
							"xord	(r11,r10), (ra) \n\t"\
							\
							/*"andd	$0x0ff,(r1,r0) \n\t"*/\
							"movzb r0,r0 \n\t"\
							"movzw r0,(r1,r0) \n\t"\
							"ashuw	$2:s,r0 \n\t"\
							"addd	(r13),(r1,r0) \n\t"\
							"loadd 0x400(r1,r0),(r1,r0)\n\t"\
							\
							"xord	(ra),(r1,r0) \n\t"\
							\
							/*T1 = ((Word *)(((Word)S1)+ BRFOPAMM(T1)))[0] ^
							 * 	   ((Word *)(((Word)S2)+ BRFOPAML(T1)))[0] ^
							 * 	   ((Word *)(((Word)X1)+ BRFOPALM(T1)))[0] ^
							 * 	   ((Word *)(((Word)X2)+ BRFOPALL(T1)))[0];	*/\
							"movzw r3,(r11,r10) \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0x800(r11,r10),(ra)\n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movzb r3, r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0xC00(r11,r10),(r11,r10)\n\t"\
							"xord	(r11,r10), (ra) \n\t"\
							\
							"xorw  r11, r11 \n\t"\
							"movw r2,r10 \n\t"\
							"lshw $-8:s,r10 \n\t"\
							"ashuw	$2:s,r10 \n\t"\
							"addd	(r13),(r11,r10) \n\t"\
							"loadd 0x000(r11,r10),(r11,r10)\n\t"\
							"xord	(r11,r10), (ra) \n\t"\
							\
							/*"andd	$0x0ff,(r3,r2)\n\t"*/\
								"movzb r2,r2 \n\t"\
									"movzw r2,(r3,r2) \n\t"\
															"ashuw	$2:s,r2 \n\t"\
															"addd	(r13),(r3,r2) \n\t"\
															"loadd 0x400(r3,r2),(r3,r2)\n\t"\
															\
															"xord	(ra),(r3,r2) \n\t"\
															\
															/*T2 = ((Word *)(((Word)S1)+ BRFOPAMM(T2)))[0] ^
															 * 	   ((Word *)(((Word)S2)+ BRFOPAML(T2)))[0] ^
															 * 	   ((Word *)(((Word)X1)+ BRFOPALM(T2)))[0] ^
															 * 	   ((Word *)(((Word)X2)+ BRFOPALL(T2)))[0];	*/\
															\
															"movzw r5,(r11,r10) \n\t"\
															"lshw $-8:s,r10 \n\t"\
															"ashuw	$2:s,r10 \n\t"\
															"addd	(r13),(r11,r10) \n\t"\
															"loadd 0x800(r11,r10),(ra)\n\t"\
															\
															"xorw  r11, r11 \n\t"\
															"movzb r5, r10 \n\t"\
															"ashuw	$2:s,r10 \n\t"\
															"addd	(r13),(r11,r10) \n\t"\
															"loadd 0xC00(r11,r10),(r11,r10)\n\t"\
															"xord	(r11,r10), (ra) \n\t"\
															\
															"xorw  r11, r11 \n\t"\
															"movw r4,r10 \n\t"\
															"lshw $-8:s,r10 \n\t"\
															"ashuw	$2:s,r10 \n\t"\
															"addd	(r13),(r11,r10) \n\t"\
															"loadd 0x000(r11,r10),(r11,r10)\n\t"\
															"xord	(r11,r10), (ra) \n\t"\
															\
															/*"andd	$0x0ff,(r5,r4)\n\t"*/\
															"movzb r4,r4 \n\t"\
															"movzw r4,(r5,r4) \n\t"\
															"ashuw	$2:s,r4 \n\t"\
															"addd	(r13),(r5,r4) \n\t"\
															"loadd 0x400(r5,r4),(r5,r4)\n\t"\
															\
															"xord	(ra), (r5,r4) \n\t"\
															\
															\
															/*T3 = ((Word *)(((Word)S1)+ BRFOPAMM(T3)))[0] ^
															 * 	   ((Word *)(((Word)S2)+ BRFOPAML(T3)))[0] ^
															 * 	   ((Word *)(((Word)X1)+ BRFOPALM(T3)))[0] ^
															 * 	   ((Word *)(((Word)X2)+ BRFOPALL(T3)))[0];	*/\
															\
															"movzw r7,(r11,r10) \n\t"\
															"lshw $-8:s,r10 \n\t"\
															"ashuw	$2:s,r10 \n\t"\
															"addd	(r13),(r11,r10) \n\t"\
															"loadd 0x800(r11,r10),(ra)\n\t"\
															\
															"xorw  r11, r11 \n\t"\
															"movzb r7, r10 \n\t"\
															"ashuw	$2:s,r10 \n\t"\
															"addd	(r13),(r11,r10) \n\t"\
															"loadd 0xC00(r11,r10),(r11,r10)\n\t"\
															"xord	(r11,r10), (ra) \n\t"\
															\
															"xorw  r11, r11 \n\t"\
															"movw r6,r10 \n\t"\
															"lshw $-8:s,r10 \n\t"\
															"ashuw	$2:s,r10 \n\t"\
															"addd	(r13),(r11,r10) \n\t"\
															"loadd 0x000(r11,r10),(r11,r10)\n\t"\
															"xord	(r11,r10), (ra) \n\t"\
															\
															/*"andd	$0x0ff,(r7,r6)\n\t"*/\
															"movzb r6,r6 \n\t"\
															"movzw r6,(r7,r6) \n\t"\
															"ashuw	$2:s,r6 \n\t"\
															"addd	(r13),(r7,r6) \n\t"\
															"loadd 0x0400(r7,r6),(r7,r6)\n\t"\
															\
															"xord	(ra),(r7,r6) \n\t"\
			\
				/* MM(T0,T1,T2,T3) 		{					\
				(T1) ^= (T2); (T2) ^= (T3); (T0) ^= (T1);		\
				(T3) ^= (T1); (T2) ^= (T0); (T1) ^= (T2);		\
			}*/\
			\
			"xord	(r5,r4),(r3,r2) \n\t"\
			"xord	(r3,r2),(r1,r0) \n\t"\
			"xord	(r7,r6),(r5,r4) \n\t"\
			\
			"xord	(r3,r2),(r7,r6) \n\t"\
			"xord	(r1,r0),(r5,r4) \n\t"\
			"xord	(r5,r4),(r3,r2) \n\t"\
			\
			/*#define P(T0,T1,T2,T3) {
			* 			...>(T2,T3,T0,T1)												\
				(T1) = PT1(T1);	\
				(T2) = PT2(T2);	\
				ReverseWord((T3)); \
			*/\
			\
			/*(T1) = PT1(T1);*/\
			\
			"movw   r5,r10 \n\t"\
			"movw	r6,r5 \n\t"\
			"movw	r7,r6 \n\t"\
			"ashud	$8:s,(r7,r6) \n\t"\
			"movw	r5,r6 \n\t"\
			"ashud	$8:s,(r6,r5) \n\t"\
			"movw	r10,r5 \n\t"\
			\
			/*(T2) = (((T2) << 16) & 0xffff0000) ^ (((T2) >> 16) & 0x0000ffff);*/\
			\
			"movw r0,r10 \n\t"\
			"movw r1,r0 \n\t"\
			"movw r10,r1 \n\t"\
			\
			/*ReverseWord((T3));*/\
			\
			"movd (r1,r0),(r11,r10) \n\t"\
			"movw r3,r1 \n\t"\
			"movw r2,r3 \n\t"\
			"ashud $8,(r3,r2)\n\t"\
			"movw r1,r2 \n\t"\
			"ashud $8,(r2,r1)\n\t"\
			"movd (r11,r10),(r1,r0) \n\t"\
			/* MM(T0,T1,T2,T3) 		{					\
			(T1) ^= (T2); (T2) ^= (T3); (T0) ^= (T1);		\
			(T3) ^= (T1); (T2) ^= (T0); (T1) ^= (T2);		\
			}*/\
			\
			"xord	(r5,r4),(r3,r2) \n\t"\
			"xord	(r7,r6),(r5,r4) \n\t"\
			"xord	(r3,r2),(r1,r0) \n\t"\
			\
			"xord	(r3,r2),(r7,r6) \n\t"\
			"xord	(r1,r0),(r5,r4) \n\t"\
			"xord	(r5,r4),(r3,r2) \n\t"\
			\
			:"=r" (t0v),"=r" (t1v),"=r" (t2v),"=r" (t3v),"=r" (rkv) \
			:"0" (t0v),"1" (t1v), "2" (t2v), "3" (t3v),"4" (rkv),"r" (S1v) \
			: "r10","r11","ra");\
			t0=t0v;\
			t1=t1v;\
			t2=t2v;\
			t3=t3v;\
			rk=(Byte*)rkv;\
}
#define KXLFOKXL \
	{\
			register long t0v  __asm__ ("r0") = (long)(t0);\
			register long t1v  __asm__ ("r2") = (long)(t1);\
			register long t2v  __asm__ ("r4") = (long)(t2);\
			register long t3v  __asm__ ("r6") = (long)(t3);\
			register long rkv  __asm__ ("r8") = (long)(rk);\
			register long S1v  __asm__ ("r13") = (long)(S1);\
				asm volatile(\
							"loadd	0(r9,r8),(r11,r10) \n\t"\
							"loadd	4(r9,r8),(ra) \n\t"\
							"xord (r11,r10),(r1,r0) \n\t"\
							"xord (ra),(r3,r2) \n\t"\
							"loadd	8(r9,r8),(r11,r10) \n\t"\
							"loadd	0xc(r9,r8),(ra) \n\t"\
							"addd $0x10,(r9,r8) \n\t"\
							"xord (r11,r10),(r5,r4) \n\t"\
							"xord (ra),(r7,r6) \n\t"\
							/*FO*/\
							/*T0 = ((Word *)(((Word)S1)+ BRFOPAMM(T0)))[0] ^
							 * 	   ((Word *)(((Word)S2)+ BRFOPAML(T0)))[0] ^
							 * 	   ((Word *)(((Word)X1)+ BRFOPALM(T0)))[0] ^
							 * 	   ((Word *)(((Word)X2)+ BRFOPALL(T0)))[0];	*/\
							 "movzw r1,(r11,r10) \n\t"\
							 "lshw $-8:s,r10 \n\t"\
							 "ashuw	$2:s,r10 \n\t"\
							 "addd	(r13),(r11,r10) \n\t"\
							 "loadd  0(r11,r10),(ra)\n\t"\
							 "xorw  r11, r11 \n\t"\
							 \
							 "movzb r1, r10 \n\t"\
							 "ashuw	$2:s,r10 \n\t"\
							 "addd	(r13),(r11,r10) \n\t"\
							 "loadd  0x400(r11,r10),(r11,r10)\n\t"\
							 "xord (r11,r10),(ra) \n\t"\
							 "xorw  r11, r11 \n\t"\
							 \
							 "movw r0,r10 \n\t"\
							 "lshw $-8:s,r10 \n\t"\
							 "ashuw	$2:s,r10 \n\t"\
							 "addd	(r13),(r11,r10) \n\t"\
							 "loadd 0x800(r11,r10),(r11,r10)\n\t"\
							 \
							 "movzb r0,r0 \n\t"\
							 "movzw r0,(r1,r0) \n\t"\
							 "ashuw	$2:s,r0 \n\t"\
							 "addd	(r13),(r1,r0) \n\t"\
							 "loadd 0xC00(r1,r0),(r1,r0)\n\t"\
							 \
							 "xord (r11,r10),(ra) \n\t"\
							 "xord	(ra),(r1,r0) \n\t"\
							 \
							 /*T1 = ((Word *)(((Word)S1)+ BRFOPAMM(T1)))[0] ^
							  * 	   ((Word *)(((Word)S2)+ BRFOPAML(T1)))[0] ^
							  * 	   ((Word *)(((Word)X1)+ BRFOPALM(T1)))[0] ^
							  * 	   ((Word *)(((Word)X2)+ BRFOPALL(T1)))[0];	*/\
							 "movzw r3,(r11,r10) \n\t"\
							 "lshw $-8:s,r10 \n\t"\
							 "ashuw	$2:s,r10 \n\t"\
							 "addd	(r13),(r11,r10) \n\t"\
							 "loadd 0(r11,r10),(ra)\n\t"\
							 "xorw  r11, r11 \n\t"\
							 \
							 "movzb r3, r10 \n\t"\
							 "ashuw	$2:s,r10 \n\t"\
							 "addd	(r13),(r11,r10) \n\t"\
							 "loadd 0x400(r11,r10),(r11,r10)\n\t"\
							 "xord (r11,r10),(ra) \n\t"\
							 "xorw  r11, r11 \n\t"\
							 \
							 "movw r2,r10 \n\t"\
							 "lshw $-8:s,r10 \n\t"\
							 "ashuw	$2:s,r10 \n\t"\
							 "addd	(r13),(r11,r10) \n\t"\
							 "loadd 0x800(r11,r10),(r11,r10)\n\t"\
							 "xord (r11,r10),(ra) \n\t"\
							 \
							 "movzb r2,r2 \n\t"\
							 "movzw r2,(r3,r2) \n\t"\
							 "ashuw	$2:s,r2 \n\t"\
							 "addd	(r13),(r3,r2) \n\t"\
							 "loadd 0xC00(r3,r2),(r3,r2)\n\t"\
							 \
							 "xord	(ra),(r3,r2) \n\t"\
							 \
							 \
							 /*T2 = ((Word *)(((Word)S1)+ BRFOPAMM(T2)))[0] ^
							  * 	   ((Word *)(((Word)S2)+ BRFOPAML(T2)))[0] ^
							  * 	   ((Word *)(((Word)X1)+ BRFOPALM(T2)))[0] ^
							  * 	   ((Word *)(((Word)X2)+ BRFOPALL(T2)))[0];	*/\
							 \
							 "movzw r5,(r11,r10) \n\t"\
							 "lshw $-8:s,r10 \n\t"\
							 "ashuw	$2:s,r10 \n\t"\
							 "addd	(r13),(r11,r10) \n\t"\
							 "loadd 0(r11,r10),(ra)\n\t"\
							 \
							 "xorw  r11, r11 \n\t"\
							 "movzb r5, r10 \n\t"\
							 "ashuw	$2:s,r10 \n\t"\
							 "addd	(r13),(r11,r10) \n\t"\
							 "loadd 0x400(r11,r10),(r11,r10)\n\t"\
							 "xord (r11,r10),(ra) \n\t"\
							 \
							 "xorw  r11, r11 \n\t"\
							 "movw r4,r10 \n\t"\
							 "lshw $-8:s,r10 \n\t"\
							 "ashuw	$2:s,r10 \n\t"\
							 "addd	(r13),(r11,r10) \n\t"\
							 "loadd 0x800(r11,r10),(r11,r10)\n\t"\
							 "xord (r11,r10),(ra) \n\t"\
							 \
							/*"andd	$0x0ff,(r5,r4) \n\t"*/\
							 "movzb r4,r4 \n\t"\
							 "movzw r4,(r5,r4) \n\t"\
							 "ashuw	$2:s,r4 \n\t"\
							 "addd	(r13),(r5,r4) \n\t"\
							 "loadd 0xC00(r5,r4),(r5,r4)\n\t"\
							 \
							 "xord	(ra),(r5,r4) \n\t"\
							 \
							 \
							 /*T3 = ((Word *)(((Word)S1)+ BRFOPAMM(T3)))[0] ^
							  * 	   ((Word *)(((Word)S2)+ BRFOPAML(T3)))[0] ^
							  * 	   ((Word *)(((Word)X1)+ BRFOPALM(T3)))[0] ^
							  * 	   ((Word *)(((Word)X2)+ BRFOPALL(T3)))[0];	*/\
							 \
							 "movzw r7,(r11,r10) \n\t"\
							 "lshw $-8:s,r10 \n\t"\
							 "ashuw	$2:s,r10 \n\t"\
							 "addd	(r13),(r11,r10) \n\t"\
							 "loadd 0(r11,r10),(ra)\n\t"\
							 \
							 "xorw  r11, r11 \n\t"\
							 "movzb r7, r10 \n\t"\
							 "ashuw	$2:s,r10 \n\t"\
							 "addd	(r13),(r11,r10) \n\t"\
							 "loadd 0x400(r11,r10),(r11,r10)\n\t"\
							 "xord (r11,r10),(ra) \n\t"\
							 \
							 "xorw  r11, r11 \n\t"\
							 "movw r6,r10 \n\t"\
							 "lshw $-8:s,r10 \n\t"\
							 "ashuw	$2:s,r10 \n\t"\
							 "addd	(r13),(r11,r10) \n\t"\
							 "loadd 0x800(r11,r10),(r11,r10)\n\t"\
							 "xord (r11,r10),(ra) \n\t"\
							 \
							 /*"andd	$0x0ff,(r7,r6) \n\t"*/\
							 "movzb r6,r6 \n\t"\
							 "movzw r6,(r7,r6) \n\t"\
							 "ashuw	$2:s,r6 \n\t"\
							 "addd	(r13),(r7,r6) \n\t"\
							 "loadd 0x0C00(r7,r6),(r7,r6)\n\t"\
							 \
							 "xord	(ra),(r7,r6) \n\t"\
							 \
							 /* MM(T0,T1,T2,T3) 		{					\
							 (T1) ^= (T2); (T2) ^= (T3); (T0) ^= (T1);		\
							  (T3) ^= (T1); (T2) ^= (T0); (T1) ^= (T2);		\
							}*/\
							\
							"xord	(r5,r4),(r3,r2) \n\t"\
							"xord	(r7,r6),(r5,r4) \n\t"\
							"xord	(r3,r2),(r1,r0) \n\t"\
							\
							"xord	(r3,r2),(r7,r6) \n\t"\
							"xord	(r1,r0),(r5,r4) \n\t"\
							"xord	(r5,r4),(r3,r2) \n\t"\
							\
							/*#define P(T0,T1,T2,T3) {															\
								(T1) = PT1(T1);	\
								(T2) = PT2(T2);	\
								ReverseWord((T3)); \
							*/\
							\
							/*(T1) = PT1(T1);*/\
							\
							"movw	r1,r10 \n\t"\
							"movw	r2,r1 \n\t"\
							"movw	r3,r2 \n\t"\
							"ashud	$8:s,(r3,r2) \n\t"\
							"movw	r1,r2 \n\t"\
							"ashud	$8:s,(r2,r1) \n\t"\
							"movw	r10,r1 \n\t"\
							\
							/*(T2) = (((T2) << 16) & 0xffff0000) ^ (((T2) >> 16) & 0x0000ffff);*/\
							\
							"movw r4,r10 \n\t"\
							"movw r5,r4 \n\t"\
							"movw r10,r5 \n\t"\
							\
							/*ReverseWord((T3));*/\
							\
							"movw r8,r10 \n\t"\
							"movw r6,r8 \n\t"\
							"movw r7,r6 \n\t"\
							"lshd $-8,(r7,r6)\n\t"\
							"movw r8,r7 \n\t"\
							"ashud $-8,(r8,r7)\n\t"\
							"movw r10,r8 \n\t"\
							/* MM(T0,T1,T2,T3) 		{					\
							 (T1) ^= (T2); (T2) ^= (T3); (T0) ^= (T1);		\
							 (T3) ^= (T1); (T2) ^= (T0); (T1) ^= (T2);		\
							}*/\
							\
							"xord	(r5,r4),(r3,r2) \n\t"\
							"xord	(r7,r6),(r5,r4) \n\t"\
							"xord	(r3,r2),(r1,r0) \n\t"\
							\
							"xord	(r3,r2),(r7,r6) \n\t"\
							"xord	(r1,r0),(r5,r4) \n\t"\
							"xord	(r5,r4),(r3,r2) \n\t"\
							\
							/*KXL*/\
							"loadd	0(r9,r8),(r11,r10) \n\t"\
							"loadd	4(r9,r8),(ra) \n\t"\
							"xord (r11,r10),(r1,r0) \n\t"\
							"xord (ra),(r3,r2) \n\t"\
							"loadd	8(r9,r8),(r11,r10) \n\t"\
							"loadd	0xc(r9,r8),(ra) \n\t"\
							"addd $0x10,(r9,r8) \n\t"\
							"xord (r11,r10),(r5,r4) \n\t"\
							"xord (ra),(r7,r6) \n\t"\
							\
							\
				:"=r" (t0v),"=r" (t1v),"=r" (t2v),"=r" (t3v),"=r" (rkv) \
				:"0" (t0v),"1" (t1v), "2" (t2v), "3" (t3v),"4" (rkv),"r" (S1v) \
				: "r10","r11","ra");\
				t0=t0v;\
				t1=t1v;\
				t2=t2v;\
				t3=t3v;\
				rk=(Byte*)rkv;\
	}

#endif

#ifdef ARIAFULLASM
//Compiler error
// this definition works correctly
//register long S1v __asm__ ("ra") = (long)(S1);
//register long ov __asm__ ("r13") = (long)(o);
// this definition DOES NOT work
//register long ov __asm__ ("r13") = (long)(o);
//register long S1v __asm__ ("ra") = (long)(S1);
//the reason is that the r13->ra that needs to be done (check KXLFOKXL)
//should be done first and then the load of (o)->r13.
//the compiler seems not to do this check.
#define STXO1A {\
		register long t0v  __asm__ ("r0") = (long)(t0);\
		register long t1v  __asm__ ("r2") = (long)(t1);\
		register long t2v  __asm__ ("r4") = (long)(t2);\
		register long t3v  __asm__ ("r6") = (long)(t3);\
		register long rkv __asm__ ("r10") = (long)(rk); \
		register long S1v __asm__ ("r13") = (long)(S1); \
		register long ov __asm__ ("ra") = (long)(o); \
		\
			asm volatile(\
							"push  $4,r4 \n\t"\
							"movd $0x03FC03FC,(r5,r4) \n\t"\
							\
							"movd (r1,r0),(r9,r8) \n\t"\
							"lshd $-6,(r9,r8) \n\t"\
							"andd (r5,r4),(r9,r8) \n\t"\
							\
							/*o[ 2] = 	(Byte)(S1[BRF(t0,  8)]     ) 	^ rk[ 1];*/\
							\
							"movzw r8,(r7,r6) \n\t"\
							"addd (r13),(r7,r6) \n\t"\
							"loadb 0x0(r7,r6),r8 \n\t"\
							\
							/*o[ 0] =	(Byte)(X1[BRF(t0, 24)]     ) 	^ rk[ 3]*/\
							\
							"movzw r9,(r7,r6) \n\t"\
							"addd (r13),(r7,r6) \n\t"\
							"loadb 0x800(r7,r6),r9 \n\t"\
							\
							"ashud $2,(r1,r0) \n\t"\
							"andd (r5,r4),(r1,r0) \n\t"\
							"lshd $8,(r9,r8) \n\t"\
							\
							/*(Byte)(S2[BRF(t0,  0)]) */ \
							"movzw r0,(r7,r6)\n\t"\
							"addd (r13),(r7,r6) \n\t"\
							"loadb 0x400(r7,r6),r8 \n\t"\
							\
							/*(Byte)(X2[BRF(t0, 16)] >> 8)  */ \
							"movzw r1,(r7,r6) \n\t"\
							"loadd 0x0(r11,r10),(r1,r0) \n\t"\
							"addd (r13),(r7,r6) \n\t"\
							"loadb 0xC01(r7,r6),r9 \n\t"\
							\
							"xorw r0,r8  \n\t"\
							"xorw r1,r9  \n\t"\
							"movw r9,r7  \n\t"\
							"movw r8,r9  \n\t"\
							"lshd $8,(r9,r8)  \n\t"\
							"movw r7,r8  \n\t"\
							"lshd $8,(r8,r7)  \n\t"\
							"stord (r9,r8),0(ra)\n\t"\
							/*	o[ 4] = (Byte)(X1[BRF(t1, 24)]     ) ^ rk[ 7];
								o[ 5] = (Byte)(X2[BRF(t1, 16)] >> 8) ^ rk[ 6];
								o[ 6] = (Byte)(S1[BRF(t1,  8)]     ) ^ rk[ 5];
								o[ 7] = (Byte)(S2[BRF(t1,  0)]     ) ^ rk[ 4];
							 */\
							 "movd (r3,r2),(r9,r8) \n\t"\
							 "lshd $-6,(r9,r8) \n\t"\
							 "andd (r5,r4),(r9,r8) \n\t"\
							 \
							 /*	(Byte)(S1[BRF(t0,  8)]     );*/\
							 \
							 "movzw r8,(r7,r6) \n\t"\
							 "addd (r13),(r7,r6) \n\t"\
							 "loadb 0x0(r7,r6),r8 \n\t"\
							 \
							 /*	(Byte)(X1[BRF(t0, 24)]     ) */\
							 \
							 "movzw r9,(r7,r6) \n\t"\
							 "addd (r13),(r7,r6) \n\t"\
							 "loadb 0x800(r7,r6),r9 \n\t"\
							 \
							 "lshd $8,(r9,r8) \n\t"\
							 "ashud $2,(r3,r2) \n\t"\
							 "andd (r5,r4),(r3,r2) \n\t"\
							 \
							 /*(Byte)(S2[BRF(t0,  0)]) */ \
							 "movzw r2,(r7,r6) \n\t"\
							 "addd (r13),(r7,r6) \n\t"\
							 "loadb 0x400(r7,r6),r8 \n\t"\
							 \
							 /*(Byte)(X2[BRF(t0, 16)] >> 8)  */ \
							 "movzw r3,(r7,r6) \n\t"\
							 "loadd 0x4(r11,r10),(r3,r2) \n\t"\
							 "addd (r13),(r7,r6) \n\t"\
							 "loadb 0xC01(r7,r6),r9 \n\t"\
							 \
							 "xorw r2,r8  \n\t"\
							 "xorw r3,r9  \n\t"\
							 "movw r9,r7  \n\t"\
							 "movw r8,r9  \n\t"\
							 "lshd $8,(r9,r8)  \n\t"\
							 "movw r7,r8  \n\t"\
							 "lshd $8,(r8,r7)  \n\t"\
							 "stord (r9,r8),4(ra)\n\t"\
							\
							\
							/* o[ 8] = (Byte)(X1[BRF(t2, 24)]     ) ^ rk[11];
							o[ 9] = (Byte)(X2[BRF(t2, 16)] >> 8) ^ rk[10];
							o[10] = (Byte)(S1[BRF(t2,  8)]     ) ^ rk[ 9];
							o[11] = (Byte)(S2[BRF(t2,  0)]     ) ^ rk[ 8];*/\
							\
							 "movd (r5,r4),(r1,r0) \n\t"\
							 "pop  $4,r4 \n\t"\
							 \
							 "movd (r5,r4),(r9,r8) \n\t"\
							 "lshd $-6,(r9,r8) \n\t"\
							 "andd (r1,r0),(r9,r8) \n\t"\
							 \
							 /*	(Byte)(S1[BRF(t0,  8)]     );*/\
							 \
							 "movzw r8,(r3,r2) \n\t"\
							 "addd (r13),(r3,r2) \n\t"\
							 "loadb 0x0(r3,r2),r8 \n\t"\
							 \
							 /*	(Byte)(X1[BRF(t0, 24)]     ) */\
							 \
							 "movzw r9,(r3,r2) \n\t"\
							 "addd (r13),(r3,r2) \n\t"\
							 "loadb 0x800(r3,r2),r9 \n\t"\
							 \
							 "lshd $8,(r9,r8) \n\t"\
							 "ashud $2,(r5,r4) \n\t"\
							 "andd (r1,r0),(r5,r4) \n\t"\
							 \
							 /*(Byte)(S2[BRF(t0,  0)]) */ \
							 "movzw r4,(r3,r2) \n\t"\
							 "addd (r13),(r3,r2) \n\t"\
							 "loadb 0x400(r3,r2),r8 \n\t"\
							 \
							 /*(Byte)(X2[BRF(t0, 16)] >> 8)  */ \
							 "movzw r5,(r3,r2) \n\t"\
							 "loadd 0x8(r11,r10),(r5,r4) \n\t"\
							 "addd (r13),(r3,r2) \n\t"\
							 "loadb 0xC01(r3,r2),r9 \n\t"\
							 \
							 "xorw r8,r4  \n\t"\
							 "xorw r9,r5  \n\t"\
							 "movw r5,r3  \n\t"\
							 "movw r4,r5  \n\t"\
							 "lshd $8,(r5,r4)  \n\t"\
							 "movw r3,r4  \n\t"\
							 "lshd $8,(r4,r3)  \n\t"\
							 "stord (r5,r4),8(ra)\n\t"\
							\
							/*o[12] = (Byte)(X1[BRF(t3, 24)]     ) ^ rk[15];
							o[13] = (Byte)(X2[BRF(t3, 16)] >> 8) ^ rk[14];
							o[14] = (Byte)(S1[BRF(t3,  8)]     ) ^ rk[13];
							o[15] = (Byte)(S2[BRF(t3,  0)]     ) ^ rk[12];*/\
							\
							\
							 "movd (r7,r6),(r9,r8) \n\t"\
							 "lshd $-6,(r9,r8) \n\t"\
							 "andd (r1,r0),(r9,r8) \n\t"\
							 \
							 /*	(Byte)(S1[BRF(t0,  8)]     );*/\
							 \
							 "movzw r8,(r3,r2) \n\t"\
							 "addd (r13),(r3,r2) \n\t"\
							 "loadb 0x0(r3,r2),r8 \n\t"\
							 \
							 /*	(Byte)(X1[BRF(t0, 24)]     ) */\
							 \
							 "movzw r9,(r3,r2) \n\t"\
							 "addd (r13),(r3,r2) \n\t"\
							 "loadb 0x800(r3,r2),r9 \n\t"\
							 \
							 "lshd $8,(r9,r8) \n\t"\
							 "ashud $2,(r7,r6) \n\t"\
							 "andd (r1,r0),(r7,r6) \n\t"\
							 \
							 /*(Byte)(S2[BRF(t0,  0)]) */ \
							 "movzw r6,(r3,r2) \n\t"\
							 "addd (r13),(r3,r2) \n\t"\
							 "loadb 0x400(r3,r2),r8 \n\t"\
							 \
							 /*(Byte)(X2[BRF(t0, 16)] >> 8)  */ \
							 "movzw r7,(r3,r2)\n\t"\
							 "loadd 0xC(r11,r10),(r7,r6) \n\t"\
							 "addd (r13),(r3,r2) \n\t"\
							 "loadb 0xC01(r3,r2),r9 \n\t"\
							 \
							 "xorw r6,r8  \n\t"\
							 "xorw r7,r9  \n\t"\
							 "movw r9,r7  \n\t"\
							 "movw r8,r9  \n\t"\
							 "lshd $8,(r9,r8)  \n\t"\
							 "movw r7,r8  \n\t"\
							 "lshd $8,(r8,r7)  \n\t"\
							 "stord (r9,r8),0xC(ra)\n\t"\
							\
							\
							: \
							:"r" (t0v),"r" (t1v), "r" (t2v), "r" (t3v),"r" (rkv),"r" (ov),"r" (S1v) \
							: "r8","r9","memory");\
							}
#endif
/*
 * Encrypt a single block
 * in and out can overlap
 */
 #ifdef ARIAFULLASM
void ARIA_encrypt(const unsigned char *in, unsigned char *out, const ARIA_KEY *key)
{
	register Word t0, t1, t2, t3;

	Byte* i;
	Word* iw;
	Byte iba[16], oba[16];
	int *ii;
	Byte* o;
	Byte* rk;
	short int Nr;
	unsigned int im;

	memcpy(iba, in, 16);
	i = (Byte*) iba;
	o = (Byte*) oba;
	rk = (Byte*) key->rd_key;
	Nr = (short int) key->rounds;

WORDL
	if (Nr > 12) {KXLFOKXLFE}
	if (Nr > 14) {KXLFOKXLFE}

	KXLFOKXLFE
	KXLFOKXLFE
	KXLFOKXLFE
	KXLFOKXLFE
	KXLFOKXLFE
	KXLFOKXL
	STXO1A
	memcpy(out, oba, 16);
}

#else
void ARIA_encrypt(const unsigned char *in, unsigned char *out, const ARIA_KEY *key)
{
		register Word t0, t1, t2, t3;

	Byte* i;
	Word* iw;
	Byte iba[16], oba[16];
	int *ii;
	Byte* o;
	Byte* rk;
	short int Nr;
	unsigned int im;

	memcpy(iba, in, 16);
	i = (Byte*) iba;
	o = (Byte*) oba;
	rk = (Byte*) key->rd_key;
	Nr = (int) key->rounds;

	WordLoad(WO(i,0), t0);
	WordLoad(WO(i,1), t1);
	WordLoad(WO(i,2), t2);
	WordLoad(WO(i,3), t3);

	if (Nr > 12) {KXL FO KXL FE}

	if (Nr > 14) {KXL FO KXL FE}

		KXL FO KXL FE KXL FO KXL FE KXL FO KXL FE
	KXL FO KXL FE KXL FO KXL FE KXL FO KXL

	/* ΓΦΑ�? ¶σ�?ξµε΄Β Ζ―�?°ΗΤ */

#ifdef LITTLE_ENDIAN
#ifdef SC1445x_OPT


	//assume that S1,S2,X1,X2 are put consecutive in memory and that each one is 0x400 bytes long



	STXO1(&o[0],&rk[0],t0,&S1[0]);
	STXO1(&o[4],&rk[4],t1,&S1[0]);
	STXO1(&o[8],&rk[8],t2,&S1[0]);
	STXO1(&o[0xc],&rk[0xc],t3,&S1[0]);





#else
	o[ 0] = (Byte)(X1[BRF(t0, 24)]     ) ^ rk[ 3];
	o[ 1] = (Byte)(X2[BRF(t0, 16)] >> 8) ^ rk[ 2];
	o[ 2] = (Byte)(S1[BRF(t0,  8)]     ) ^ rk[ 1];
	o[ 3] = (Byte)(S2[BRF(t0,  0)]     ) ^ rk[ 0];
	o[ 4] = (Byte)(X1[BRF(t1, 24)]     ) ^ rk[ 7];
	o[ 5] = (Byte)(X2[BRF(t1, 16)] >> 8) ^ rk[ 6];
	o[ 6] = (Byte)(S1[BRF(t1,  8)]     ) ^ rk[ 5];
	o[ 7] = (Byte)(S2[BRF(t1,  0)]     ) ^ rk[ 4];
	o[ 8] = (Byte)(X1[BRF(t2, 24)]     ) ^ rk[11];
	o[ 9] = (Byte)(X2[BRF(t2, 16)] >> 8) ^ rk[10];
	o[10] = (Byte)(S1[BRF(t2,  8)]     ) ^ rk[ 9];
	o[11] = (Byte)(S2[BRF(t2,  0)]     ) ^ rk[ 8];
	o[12] = (Byte)(X1[BRF(t3, 24)]     ) ^ rk[15];
	o[13] = (Byte)(X2[BRF(t3, 16)] >> 8) ^ rk[14];
	o[14] = (Byte)(S1[BRF(t3,  8)]     ) ^ rk[13];
	o[15] = (Byte)(S2[BRF(t3,  0)]     ) ^ rk[12];
#endif
#else
	o[ 0] = (Byte)(X1[BRF(t0, 24)]     );
	o[ 1] = (Byte)(X2[BRF(t0, 16)] >> 8);
	o[ 2] = (Byte)(S1[BRF(t0,  8)]     );
	o[ 3] = (Byte)(S2[BRF(t0,  0)]     );
	o[ 4] = (Byte)(X1[BRF(t1, 24)]     );
	o[ 5] = (Byte)(X2[BRF(t1, 16)] >> 8);
	o[ 6] = (Byte)(S1[BRF(t1,  8)]     );
	o[ 7] = (Byte)(S2[BRF(t1,  0)]     );
	o[ 8] = (Byte)(X1[BRF(t2, 24)]     );
	o[ 9] = (Byte)(X2[BRF(t2, 16)] >> 8);
	o[10] = (Byte)(S1[BRF(t2,  8)]     );
	o[11] = (Byte)(S2[BRF(t2,  0)]     );
	o[12] = (Byte)(X1[BRF(t3, 24)]     );
	o[13] = (Byte)(X2[BRF(t3, 16)] >> 8);
	o[14] = (Byte)(S1[BRF(t3,  8)]     );
	o[15] = (Byte)(S2[BRF(t3,  0)]     );

	WO(o, 0) ^= WO(rk, 0);
	WO(o, 1) ^= WO(rk, 1);
	WO(o, 2) ^= WO(rk, 2);
	WO(o, 3) ^= WO(rk, 3);
#endif
    memcpy(out, oba, 16);
}
#endif

/*
 * Decrypt a single block
 * in and out can overlap
 */
 #ifdef ARIAFULLASM
void ARIA_decrypt(const unsigned char *in, unsigned char *out, const ARIA_KEY *key)
{
	register Word t0, t1, t2, t3;

	Byte* i;
	Byte* o;
	Byte* rk;
	Byte iba[16], oba[16];
	short int Nr;

	memcpy(iba, in, 16);
	i = (Byte*) iba;
	o = (Byte*) oba;
	rk = (Byte*) key->rd_key;
	Nr = (short int) key->rounds;

	WORDL
	if (Nr > 12) {KXLFOKXLFE}
	if (Nr > 14) {KXLFOKXLFE}
	KXLFOKXLFE
	KXLFOKXLFE
	KXLFOKXLFE
	KXLFOKXLFE
	KXLFOKXLFE
	KXLFOKXL
	STXO1A
	memcpy(out, oba, 16);
}

#else
void ARIA_decrypt(const unsigned char *in, unsigned char *out, const ARIA_KEY *key)
{
	register Word t0, t1, t2, t3;

	Byte* i;
	Byte* o;
	Byte* rk;
	Byte iba[16], oba[16];
	short int Nr;

	memcpy(iba, in, 16);
	i = (Byte*) iba;
	o = (Byte*) oba;
	rk = (Byte*) key->rd_key;
	Nr = (short int) key->rounds;

	WordLoad(WO(i,0), t0);
	WordLoad(WO(i,1), t1);
	WordLoad(WO(i,2), t2);
	WordLoad(WO(i,3), t3);

	if (Nr > 12) {KXL FO KXL FE}
	if (Nr > 14) {KXL FO KXL FE}
	KXL FO KXL FE KXL FO KXL FE KXL FO KXL FE
	KXL FO KXL FE KXL FO KXL FE KXL FO KXL

	/* ΓΦΑ�? ¶σ�?ξµε΄Β Ζ―�?°ΗΤ */
#ifdef LITTLE_ENDIAN
#ifdef SC1445x_OPT
	STXO1(&o[0],&rk[0],t0,&S1[0]);
	STXO1(&o[4],&rk[4],t1,&S1[0]);
	STXO1(&o[8],&rk[8],t2,&S1[0]);
	STXO1(&o[0xc],&rk[0xc],t3,&S1[0]);
#else

	o[ 0] = (Byte)(X1[BRF(t0, 24)]     ) ^ rk[ 3];
	o[ 1] = (Byte)(X2[BRF(t0, 16)] >> 8) ^ rk[ 2];
	o[ 2] = (Byte)(S1[BRF(t0,  8)]     ) ^ rk[ 1];
	o[ 3] = (Byte)(S2[BRF(t0,  0)]     ) ^ rk[ 0];
	o[ 4] = (Byte)(X1[BRF(t1, 24)]     ) ^ rk[ 7];
	o[ 5] = (Byte)(X2[BRF(t1, 16)] >> 8) ^ rk[ 6];
	o[ 6] = (Byte)(S1[BRF(t1,  8)]     ) ^ rk[ 5];
	o[ 7] = (Byte)(S2[BRF(t1,  0)]     ) ^ rk[ 4];
	o[ 8] = (Byte)(X1[BRF(t2, 24)]     ) ^ rk[11];
	o[ 9] = (Byte)(X2[BRF(t2, 16)] >> 8) ^ rk[10];
	o[10] = (Byte)(S1[BRF(t2,  8)]     ) ^ rk[ 9];
	o[11] = (Byte)(S2[BRF(t2,  0)]     ) ^ rk[ 8];
	o[12] = (Byte)(X1[BRF(t3, 24)]     ) ^ rk[15];
	o[13] = (Byte)(X2[BRF(t3, 16)] >> 8) ^ rk[14];
	o[14] = (Byte)(S1[BRF(t3,  8)]     ) ^ rk[13];
	o[15] = (Byte)(S2[BRF(t3,  0)]     ) ^ rk[12];
#endif
#else
	o[ 0] = (Byte)(X1[BRF(t0, 24)]     );
	o[ 1] = (Byte)(X2[BRF(t0, 16)] >> 8);
	o[ 2] = (Byte)(S1[BRF(t0,  8)]     );
	o[ 3] = (Byte)(S2[BRF(t0,  0)]     );
	o[ 4] = (Byte)(X1[BRF(t1, 24)]     );
	o[ 5] = (Byte)(X2[BRF(t1, 16)] >> 8);
	o[ 6] = (Byte)(S1[BRF(t1,  8)]     );
	o[ 7] = (Byte)(S2[BRF(t1,  0)]     );
	o[ 8] = (Byte)(X1[BRF(t2, 24)]     );
	o[ 9] = (Byte)(X2[BRF(t2, 16)] >> 8);
	o[10] = (Byte)(S1[BRF(t2,  8)]     );
	o[11] = (Byte)(S2[BRF(t2,  0)]     );
	o[12] = (Byte)(X1[BRF(t3, 24)]     );
	o[13] = (Byte)(X2[BRF(t3, 16)] >> 8);
	o[14] = (Byte)(S1[BRF(t3,  8)]     );
	o[15] = (Byte)(S2[BRF(t3,  0)]     );

	WO(o, 0) ^= WO(rk, 0);
	WO(o, 1) ^= WO(rk, 1);
	WO(o, 2) ^= WO(rk, 2);
	WO(o, 3) ^= WO(rk, 3);
#endif
	memcpy(out, oba, 16);
}
#endif
