/* crypto/sha/sha_locl.h */
/* Copyright (C) 1995-1998 Eric Young (eay@cryptsoft.com)
 * All rights reserved.
 *
 * This package is an SSL implementation written
 * by Eric Young (eay@cryptsoft.com).
 * The implementation was written so as to conform with Netscapes SSL.
 * 
 * This library is free for commercial and non-commercial use as long as
 * the following conditions are aheared to.  The following conditions
 * apply to all code found in this distribution, be it the RC4, RSA,
 * lhash, DES, etc., code; not just the SSL code.  The SSL documentation
 * included with this distribution is covered by the same copyright terms
 * except that the holder is Tim Hudson (tjh@cryptsoft.com).
 * 
 * Copyright remains Eric Young's, and as such any Copyright notices in
 * the code are not to be removed.
 * If this package is used in a product, Eric Young should be given attribution
 * as the author of the parts of the library used.
 * This can be in the form of a textual message at program startup or
 * in documentation (online or textual) provided with the package.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    "This product includes cryptographic software written by
 *     Eric Young (eay@cryptsoft.com)"
 *    The word 'cryptographic' can be left out if the rouines from the library
 *    being used are not cryptographic related :-).
 * 4. If you include any Windows specific code (or a derivative thereof) from 
 *    the apps directory (application code) you must include an acknowledgement:
 *    "This product includes software written by Tim Hudson (tjh@cryptsoft.com)"
 * 
 * THIS SOFTWARE IS PROVIDED BY ERIC YOUNG ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 */

#include <stdlib.h>
#include <string.h>

#include <openssl/opensslconf.h>
#include <openssl/sha.h>

#define DATA_ORDER_IS_BIG_ENDIAN

#define HASH_LONG               SHA_LONG
#define HASH_CTX                SHA_CTX
#define HASH_CBLOCK             SHA_CBLOCK
#define HASH_MAKE_STRING(c,s)   do {	\
	unsigned long ll;		\
	ll=(c)->h0; HOST_l2c(ll,(s));	\
	ll=(c)->h1; HOST_l2c(ll,(s));	\
	ll=(c)->h2; HOST_l2c(ll,(s));	\
	ll=(c)->h3; HOST_l2c(ll,(s));	\
	ll=(c)->h4; HOST_l2c(ll,(s));	\
	} while (0)

#if defined(SHA_0)

# define HASH_UPDATE             	SHA_Update
# define HASH_TRANSFORM          	SHA_Transform
# define HASH_FINAL              	SHA_Final
# define HASH_INIT			SHA_Init
# define HASH_BLOCK_DATA_ORDER   	sha_block_data_order
# define Xupdate(a,ix,ia,ib,ic,id)	(ix=(a)=(ia^ib^ic^id))

static void sha_block_data_order (SHA_CTX *c, const void *p,size_t num);

#elif defined(SHA_1)

# define HASH_UPDATE             	SHA1_Update
# define HASH_TRANSFORM          	SHA1_Transform
# define HASH_FINAL              	SHA1_Final
# define HASH_INIT			SHA1_Init
# define HASH_BLOCK_DATA_ORDER   	sha1_block_data_order
# if defined(__MWERKS__) && defined(__MC68K__)
   /* Metrowerks for Motorola fails otherwise:-( <appro@fy.chalmers.se> */
#  define Xupdate(a,ix,ia,ib,ic,id)	do { (a)=(ia^ib^ic^id);		\
					     ix=(a)=ROTATE((a),1);	\
					} while (0)
# else
#  define Xupdate(a,ix,ia,ib,ic,id)	( (a)=(ia^ib^ic^id),	\
					  ix=(a)=ROTATE((a),1)	\
					)
# endif

#ifndef SHA1_ASM
static
#endif
void sha1_block_data_order (SHA_CTX *c, const void *p,size_t num);

#else
# error "Either SHA_0 or SHA_1 must be defined."
#endif

#include "md32_common.h"

#define INIT_DATA_h0 0x67452301UL
#define INIT_DATA_h1 0xefcdab89UL
#define INIT_DATA_h2 0x98badcfeUL
#define INIT_DATA_h3 0x10325476UL
#define INIT_DATA_h4 0xc3d2e1f0UL

#ifdef SHA_0
fips_md_init(SHA)
#else
fips_md_init_ctx(SHA1, SHA)
#endif
	{
	memset (c,0,sizeof(*c));
	c->h0=INIT_DATA_h0;
	c->h1=INIT_DATA_h1;
	c->h2=INIT_DATA_h2;
	c->h3=INIT_DATA_h3;
	c->h4=INIT_DATA_h4;
	return 1;
	}

#define K_00_19	0x5a827999UL
#define K_20_39 0x6ed9eba1UL
#define K_40_59 0x8f1bbcdcUL
#define K_60_79 0xca62c1d6UL

/* As  pointed out by Wei Dai <weidai@eskimo.com>, F() below can be
 * simplified to the code in F_00_19.  Wei attributes these optimisations
 * to Peter Gutmann's SHS code, and he attributes it to Rich Schroeppel.
 * #define F(x,y,z) (((x) & (y))  |  ((~(x)) & (z)))
 * I've just become aware of another tweak to be made, again from Wei Dai,
 * in F_40_59, (x&a)|(y&a) -> (x|y)&a
 */
#define	F_00_19(b,c,d)	((((c) ^ (d)) & (b)) ^ (d)) 
#define	F_20_39(b,c,d)	((b) ^ (c) ^ (d))
#define F_40_59(b,c,d)	(((b) & (c)) | (((b)|(c)) & (d))) 
#define	F_60_79(b,c,d)	F_20_39(b,c,d)

#ifndef OPENSSL_SMALL_FOOTPRINT

#define BODY_00_15(i,a,b,c,d,e,f,xi) \
	(f)=xi+(e)+K_00_19+ROTATE((a),5)+F_00_19((b),(c),(d)); \
	(b)=ROTATE((b),30);

#define BODY_16_19(i,a,b,c,d,e,f,xi,xa,xb,xc,xd) \
	Xupdate(f,xi,xa,xb,xc,xd); \
	(f)+=(e)+K_00_19+ROTATE((a),5)+F_00_19((b),(c),(d)); \
	(b)=ROTATE((b),30);

#define BODY_20_31(i,a,b,c,d,e,f,xi,xa,xb,xc,xd) \
	Xupdate(f,xi,xa,xb,xc,xd); \
	(f)+=(e)+K_20_39+ROTATE((a),5)+F_20_39((b),(c),(d)); \
	(b)=ROTATE((b),30);

#define BODY_32_39(i,a,b,c,d,e,f,xa,xb,xc,xd) \
	Xupdate(f,xa,xa,xb,xc,xd); \
	(f)+=(e)+K_20_39+ROTATE((a),5)+F_20_39((b),(c),(d)); \
	(b)=ROTATE((b),30);

#define BODY_40_59(i,a,b,c,d,e,f,xa,xb,xc,xd) \
	Xupdate(f,xa,xa,xb,xc,xd); \
	(f)+=(e)+K_40_59+ROTATE((a),5)+F_40_59((b),(c),(d)); \
	(b)=ROTATE((b),30);

#define BODY_60_79(i,a,b,c,d,e,f,xa,xb,xc,xd) \
	Xupdate(f,xa,xa,xb,xc,xd); \
	(f)=xa+(e)+K_60_79+ROTATE((a),5)+F_60_79((b),(c),(d)); \
	(b)=ROTATE((b),30);

#ifdef X
#undef X
#endif
#ifndef MD32_XARRAY
  /*
   * Originally X was an array. As it's automatic it's natural
   * to expect RISC compiler to accomodate at least part of it in
   * the register bank, isn't it? Unfortunately not all compilers
   * "find" this expectation reasonable:-( On order to make such
   * compilers generate better code I replace X[] with a bunch of
   * X0, X1, etc. See the function body below...
   *					<appro@fy.chalmers.se>
   */
# define X(i)	XX##i
#else
  /*
   * However! Some compilers (most notably HP C) get overwhelmed by
   * that many local variables so that we have to have the way to
   * fall down to the original behavior.
   */
# define X(i)	XX[i]
#endif


#if !defined(SHA_1) || !defined(SHA1_ASM) || defined (CR16_OPT)
#ifdef CR16_OPT

#define extendWord2(W,M,H)\
({\
	register long Wrpt  __asm__ ("r2") = (long)(W);\
	register long Hptr  __asm__ ("r4") = (long)(H);\
	register long Mptr  __asm__ ("r6") = (long)(M);\
        		asm volatile(\
						"push 	$2, r13, ra \n\t" \
						"push	$0x6,r2 \n\t"\
						\
						 /*-------------------------------*/\
						 /*-------------------------------*/\
						 /*-------------------------------*/\
						 /*-------------------------------*/\
						 /*-------------------------------*/\
						 /*-------------------------------*/\
						 /*-------------------------------*/\
						 /*-------------------------------*/\
						 /*-------------------------------*/\
						 "loadd 0:(r7,r6),(r1,r0) \n\t"\
						 \
						"movd (r3,r2),(r13)\n\t"\
						"movd (r3,r2),(r11,r10)\n\t"\
						\
						 "loadd 4:(r7,r6),(r4,r3) \n\t"\
						 \
						 "movw $0x1,r5 \n\t" \
						 "movd $0x10,(r9,r8) \n\t" \
						 \
						 "1: \n\t"\
						 \
						 "movw r0,r2	\n\t \n\t" \
						 "movw r1,r0	\n\t \n\t" \
						 "ashud $-8,(r1,r0)\n\t"\
						 "movw r2,r1	\n\t \n\t" \
						 "ashud $-8,(r2,r1)\n\t"\
						 "stord (r1,r0),0:(r11,r10) \n\t"\
						 "loadd 8:(r7,r6),(r1,r0) \n\t"\
						 \
						 "movw r4,r2	\n\t \n\t" \
						 "movw r3,r4	\n\t \n\t" \
						 "ashud $8,(r4,r3)\n\t"\
						 "movw r2,r3	\n\t \n\t" \
						 "ashud $8,(r3,r2)\n\t"\
						 "stord (r4,r3),4:(r11,r10) \n\t"\
						 "loadd 0xc:(r7,r6),(r4,r3) \n\t"\
						 \
						 "movw r0,r2	\n\t \n\t" \
						 "movw r1,r0	\n\t \n\t" \
						 "ashud $-8,(r1,r0)\n\t"\
						 "movw r2,r1	\n\t \n\t" \
						 "ashud $-8,(r2,r1)\n\t"\
						 "stord (r1,r0),8:(r11,r10) \n\t"\
						 \
						 "addd (r9,r8),(r7,r6) \n\t"\
						 "loadd 0:(r7,r6),(r1,r0) \n\t"\
						 \
						 "movw r4,r2	\n\t \n\t" \
						 "movw r3,r4	\n\t \n\t" \
						 "ashud $8,(r4,r3)\n\t"\
						 "movw r2,r3	\n\t \n\t" \
						 "ashud $8,(r3,r2)\n\t"\
						 "stord (r4,r3),0xc:(r11,r10) \n\t"\
						 \
						 "addd (r9,r8),(r11,r10) \n\t"\
						 "loadd 0x4:(r7,r6),(r4,r3) \n\t"\
						 \
						 /*-------------------------------*/\
						 \
						 "movw r0,r2	\n\t \n\t" \
						 "movw r1,r0	\n\t \n\t" \
						 "ashud $-8,(r1,r0)\n\t"\
						 "movw r2,r1	\n\t \n\t" \
						 "ashud $-8,(r2,r1)\n\t"\
						 "stord (r1,r0),0:(r11,r10) \n\t"\
						 "loadd 8:(r7,r6),(r1,r0) \n\t"\
						 \
						 "movw r4,r2	\n\t \n\t" \
						 "movw r3,r4	\n\t \n\t" \
						 "ashud $8,(r4,r3)\n\t"\
						 "movw r2,r3	\n\t \n\t" \
						 "ashud $8,(r3,r2)\n\t"\
						 "stord (r4,r3),4:(r11,r10) \n\t"\
						 "loadd 0xc:(r7,r6),(r4,r3) \n\t"\
						 \
						 "movw r0,r2	\n\t \n\t" \
						 "movw r1,r0	\n\t \n\t" \
						 "ashud $-8,(r1,r0)\n\t"\
						 "movw r2,r1	\n\t \n\t" \
						 "ashud $-8,(r2,r1)\n\t"\
						 "stord (r1,r0),8:(r11,r10) \n\t"\
						 \
						 "addd (r9,r8),(r7,r6) \n\t"\
						 "loadd 0:(r7,r6),(r1,r0) \n\t"\
						 \
						 "movw r4,r2	\n\t \n\t" \
						 "movw r3,r4	\n\t \n\t" \
						 "ashud $8,(r4,r3)\n\t"\
						 "movw r2,r3	\n\t \n\t" \
						 "ashud $8,(r3,r2)\n\t"\
						 "stord (r4,r3),0xc:(r11,r10) \n\t"\
						 \
						 "addd (r9,r8),(r11,r10) \n\t"\
						 "loadd 0x4:(r7,r6),(r4,r3) \n\t"\
						 \
						 /*-------------------------------*/\
						/*-------------------------------*/\
						/*---*/\
						"subw $1,r5 \n\t"\
						"bcc 1b \n\t"\
						\
						\
           				/*-------------------------------*/\
						/*-------------------------------*/\
						/*-------------------------------*/\
						"movd (r13),(r1,r0) \n\t"\
						\
						"loadmp $6  \n\t"\
						"movd (r3,r2), (r11,r10) \n\t"\
						"movd (r5,r4), (r13) \n\t"\
						"movd (r9,r8), (ra) \n\t"\
						"loadmp $4  \n\t"\
						"xord (r9,r8), (r11,r10) \n\t"\
						"xord (r3,r2), (r13) \n\t"\
						"xord (r5,r4), (ra) \n\t"\
						"addd $0xC,(r1,r0) \n\t"\
						"loadmp $6  \n\t"\
						"xord (r3,r2), (r11,r10) \n\t"\
						"xord (r5,r4), (r13) \n\t"\
						"xord (r9,r8), (ra) \n\t"\
						"addd $0x8,(r1,r0) \n\t"\
						"loadmp $6  \n\t"\
						"xord (r11,r10),(r3,r2) \n\t"\
						"xord (r13), (r5,r4)  \n\t"\
						"xord (ra),(r9,r8) \n\t"\
						"movd (r9,r8),(r11,r10) \n\t"\
						"ashud $1,(r9,r8)\n\t"\
						"lshw  $-15,r11 \n\t"\
						"orw  r11,r8 \n\t"\
						"movd (r5,r4),(r11,r10) \n\t"\
						"ashud $1,(r5,r4)\n\t"\
						"lshw  $-15,r11 \n\t"\
						"orw  r11,r4 \n\t"\
						"movd (r3,r2),(r11,r10) \n\t"\
						"ashud $1,(r3,r2)\n\t"\
						"lshw  $-15,r11 \n\t"\
						"orw  r11,r2 \n\t"\
						"movd (r1,r0),(r7,r6) \n\t"\
						"stormp $6  \n\t"\
						"subd $0x34, (r1,r0) \n\t"\
						/*19,20,21*/\
						"movw $0x13, r10 \n\t"\
						"1: \n\t"\
						"movd (r3,r2), (r7,r6) \n\t"\
						"movd (r5,r4), (r13) \n\t"\
						"movd (r9,r8), (ra) \n\t"\
						"loadmp $6  \n\t"\
						"xord (r3,r2), (r7,r6) \n\t"\
						"xord (r5,r4), (r13) \n\t"\
						"xord (r9,r8), (ra) \n\t"\
						"loadmp $4  \n\t"\
						"xord (r9,r8), (r7,r6) \n\t"\
						"xord (r3,r2), (r13) \n\t"\
						"xord (r5,r4), (ra) \n\t"\
						"addd $0xC,(r1,r0) \n\t"\
						"loadmp $6  \n\t"\
						"xord (r7,r6),(r3,r2) \n\t"\
						"xord (r13), (r5,r4)  \n\t"\
						"xord (ra),(r9,r8) \n\t"\
						"addd $0x14,(r1,r0) \n\t"\
						"movd (r9,r8),(r7,r6) \n\t"\
						"ashud $1,(r9,r8)\n\t"\
						"lshw  $-15,r7 \n\t"\
						"orw  r7,r8 \n\t"\
						"movd (r5,r4),(r7,r6) \n\t"\
						"ashud $1,(r5,r4)\n\t"\
						"lshw  $-15,r7 \n\t"\
						"orw  r7,r4 \n\t"\
						"movd (r3,r2),(r7,r6) \n\t"\
						"ashud $1,(r3,r2)\n\t"\
						"lshw  $-15,r7 \n\t"\
						"orw  r7,r2 \n\t"\
						"movd (r1,r0),(r7,r6) \n\t"\
						"stormp $6  \n\t"\
						"subd $0x34, (r1,r0) \n\t"\
						\
						/*---*/\
						"subw $1, r10 \n\t"\
						"bcc 1b \n\t"\
						/*---*/\
						/*22,23,24 1*/\
						/*25,26,27 2*/\
						/*28,29,30 3*/\
						/*31,32,33 4*/\
						/*34,35,36 5*/\
						/*37,38,39 6*/\
						/*40,41,42 7*/\
						/*43,44,45 8*/\
						/*46,47,48 9*/\
						/*49,50,51 10*/\
						/*52,53,54 11*/\
						/*55,56,57 12*/\
						/*58,59,60 13*/\
						/*61,62,63 14*/\
						/*64,65,66 15*/\
						/*67,68,69 16*/\
						/*70,71,72 17*/\
						/*73,74,75 18*/\
						/*76,77,78 19*/\
						/*rest*/\
						/*-------------------------------*/\
						/*-------------------------------*/\
						/*---*/\
						"1: \n\t"\
						"loadd 0x0(r1,r0),(r11,r10)  \n\t"\
						"loadd 0x8(r1,r0),(r9,r8)  \n\t"\
						"xord (r9,r8), (r11,r10) \n\t"\
						"loadd 0x20(r1,r0),(r9,r8)  \n\t"\
						"xord (r9,r8), (r11,r10) \n\t"\
						"loadd 0x34(r1,r0),(r9,r8)  \n\t"\
						"xord (r9,r8), (r11,r10) \n\t"\
						"movd (r11,r10),(r9,r8) \n\t"\
						"ashud $1,(r9,r8)\n\t"\
						"lshw  $-15,r11 \n\t"\
						"orw  r11,r8 \n\t"\
						"stord (r9,r8), 0x40(r1,r0)  \n\t"\
						\
						"loadd 0x0:(sp),(r13) \n\t"\
						"loadd 0x4:(sp),(r5,r4) \n\t"\
						\
						"loadd	0x0:s(r5,r4),(r1,r0)\n\t"\
						"loadd	0x8:s(r5,r4),(r7,r6)\n\t"\
						"loadd	0xc:s(r5,r4),(r9,r8)\n\t"\
						"loadd	0x10:s(r5,r4),(r11,r10)\n\t"\
						"loadd	0x4:s(r5,r4),(r5,r4)\n\t"\
						"movd $0xFFFF0003,(r3,r2) \n\t"\
						/*--------------------------------------------------------*/\
						/* (r11,r10) E, (r9,r8) D, (r7,r6) C, (r5,r4) B, (r1,r0) A*/\
						"1:  \n\t"\
						"movd	(r1,r0),(ra) \n\t"\
						"movw	r1,r3 \n\t"\
						"ashud	$5:s,(r1,r0) \n\t"\
						"lshw	$-11:s,r3 \n\t"\
						"orw	r3,r0 \n\t"\
						 \
						 "addd 	(r1,r0),(r11,r10) \n\t" /* E=S5(A)+E*/\
						 "movd   (r9,r8),(r1,r0) \n\t" /* R0=d xor (b and (c xor d))*/\
						 "xord   (r7,r6),(r1,r0) \n\t"\
						 "andd   (r5,r4),(r1,r0) \n\t"\
						 "xord   (r9,r8),(r1,r0) \n\t"\
						 "addd 	$0x5A827999,(r1,r0) \n\t" /* E+=f0(B,C,D)*/\
						 "addd 	(r11,r10),(r1,r0) \n\t" /* E+=f0(B,C,D)*/\
						 \
						 "loadd  0:(r13),(r11,r10)\n\t"\
						 \
						 "movw   r4,r3 \n\t" /**/\
						 "ashuw 	$14,r3 \n\t"\
						 "lshd  	$-2,(r5,r4) \n\t"\
						 "orw   r3,r5 \n\t" /*C=S30(B)*/\
						 \
						 "addd 	(r11,r10),(r1,r0) \n\t" /* E+=f0(B,C,D)*/\
						 /* (r9,r8) E, (r7,r6) D, (r5,r4) C, (ra) B, (r1,r0) A*/\
						 \
						 "movd	(r1,r0),(r11,r10) \n\t"/*(r11,r10) A */\
						 "movw	r1,r3 \n\t"\
						 "ashud	$5:s,(r1,r0) \n\t"\
						 "lshw	$-11:s,r3 \n\t"\
						 "orw	r3,r0 \n\t"\
						 \
						 "addd 	(r1,r0),(r9,r8) \n\t" /* E=S5(A)+E*/\
						 \
						 "movd   (r7,r6),(r1,r0) \n\t" /* R0=d xor (b and (c xor d))*/\
						 "xord   (r5,r4),(r1,r0) \n\t"\
						 "andd   (ra),(r1,r0) \n\t"\
						 "xord   (r7,r6),(r1,r0) \n\t"\
						 "addd 	(r9,r8),(r1,r0) \n\t" /* E+=f0(B,C,D)*/\
						 \
						 \
						 "movd   (ra),(r9,r8) \n\t" /**/\
						 "ashud 	$30,(r9,r8) \n\t"\
						 "lshd  	$-2,(ra) \n\t"\
						 "ord   (r9,r8),(ra) \n\t" /*C=S30(B)*/\
						 \
						 "loadd  4:(r13),(r9,r8)\n\t"\
						 "addd 	$0x5A827999,(r1,r0) \n\t" /* E+=f0(B,C,D)*/\
						 "addd 	(r9,r8),(r1,r0) \n\t" /* E+=W(t)*/\
						 \
						 /* (r7,r6) E, (r5,r4) D, (ra) C, (r11,r10) B, (r1,r0) A*/\
						 \
						 "movd	(r1,r0),(r9,r8) \n\t"/*(r9,r8) A */\
						 "movw	r1,r3 \n\t"\
						 "ashud	$5:s,(r1,r0) \n\t"\
						 "lshw	$-11:s,r3 \n\t"\
						 "orw	r3,r0 \n\t"\
						 \
					  	 \
						 "addd 	(r1,r0),(r7,r6) \n\t" /* E=S5(A)+E*/\
						 "movd   (r5,r4),(r1,r0) \n\t" /* R0=d xor (b and (c xor d))*/\
						 "xord   (ra),(r1,r0) \n\t"\
						 "andd   (r11,r10),(r1,r0) \n\t"\
						 "xord   (r5,r4),(r1,r0) \n\t"\
						 "addd 	$0x5A827999,(r1,r0) \n\t" /* E+=f0(B,C,D)*/\
						 "addd 	(r7,r6),(r1,r0) \n\t" /* E+=f0(B,C,D)*/\
						 \
						 \
						 \
						 "loadd  8:(r13),(r7,r6)\n\t"\
						 \
						 "movw   r10,r3 \n\t" /**/\
						 "ashuw 	$14,r3 \n\t"\
						 "lshd  	$-2,(r11,r10) \n\t"\
						 "orw    r3,r11 \n\t" /*C=S30(B)*/\
						 "addd 	(r7,r6),(r1,r0) \n\t" /*+W(t)*/\
						 \
						 /* (r5,r4) E, (ra) D, (r11,r10) C, (r9,r8) B, (r1,r0) A*/\
						 \
						 "movd	(r1,r0),(r7,r6) \n\t"/*(r7,r6) A */\
						 "movw	r1,r3 \n\t"\
						 "ashud	$5:s,(r1,r0) \n\t"\
						 "lshw	$-11:s,r3 \n\t"\
						 "orw	r3,r0 \n\t"\
						 \
						 \
						 "addd 	(r1,r0),(r5,r4) \n\t" /* E=S5(A)+E*/\
						 "movd   (ra),(r1,r0) \n\t" /* R0=d xor (b and (c xor d))*/\
						 "xord   (r11,r10),(r1,r0) \n\t"\
						 "andd   (r9,r8),(r1,r0) \n\t"\
						 "xord   (ra),(r1,r0) \n\t"\
						 "addd 	$0x5A827999,(r1,r0) \n\t" /* E+=f0(B,C,D)*/\
						 "addd 	(r5,r4),(r1,r0) \n\t" /* E+=f0(B,C,D)*/\
						 \
						 \
						 "loadd  12:(r13),(r5,r4)\n\t"\
						 \
						 \
						 "movw   r8,r3 \n\t" /**/\
						 "ashuw 	$14,r3 \n\t"\
						 "lshd  	$-2,(r9,r8) \n\t"\
						 "orw    r3,r9 \n\t" /*C=S30(B)*/\
						 \
						 "addd 	(r5,r4),(r1,r0) \n\t" /* +W(t)*/\
						 \
						 /* (ra) E, (r11,r10) D, (r9,r8) C, (r7,r6) B, (r1,r0) A*/\
						 \
						 "movd	(r1,r0),(r5,r4) \n\t"/*(r7,r6) A */\
						 "movw	r1,r3 \n\t"\
						 "ashud	$5:s,(r1,r0) \n\t"\
						 "lshw	$-11:s,r3 \n\t"\
						 "orw	r3,r0 \n\t"\
						 \
						 \
						 "addd 	(r1,r0),(ra) \n\t" /* E=S5(A)+E*/\
						 "movd   (r11,r10),(r1,r0) \n\t" /* R0=d xor (b and (c xor d))*/\
						 "xord   (r9,r8),(r1,r0) \n\t"\
						 "andd   (r7,r6),(r1,r0) \n\t"\
						 "xord   (r11,r10),(r1,r0) \n\t"\
						 "addd 	$0x5A827999,(r1,r0) \n\t" /* E+=f0(B,C,D)*/\
						 "addd 	(ra),(r1,r0) \n\t" /* E+=f0(B,C,D)*/\
						 \
						 \
						 "loadd  16:(r13),(ra)\n\t"\
						 \
						 "movw   r6,r3 \n\t" /**/\
						 "ashuw 	$14,r3 \n\t"\
						 "lshd  	$-2,(r7,r6) \n\t"\
						 "orw    r3,r7 \n\t" /*C=S30(B)*/\
						 \
						 "addd 	(ra),(r1,r0) \n\t" /* +W(t)*/\
						 \
						 "addd $0x14,(r13) \n\t"\
						 "subw $1,r2 \n\t"\
						 "bcc 1b \n\t"\
						 "movw $0x3,r2 \n\t"\
						 "1: \n\t"\
						 /*--------------------------------------------------------*/\
						 /*--------------------------------------------------------*/\
						 /*--------------------------------------------------------*/\
						 /*--------------------------------------------------------*/\
						 /* (r11,r10) E, (r9,r8) D, (r7,r6) C, (r5,r4) B, (r1,r0) A*/\
						 "movd	(r1,r0),(ra) \n\t"\
						 "movw	r1,r3 \n\t"\
						 "ashud	$5:s,(r1,r0) \n\t"\
						 "lshw	$-11:s,r3 \n\t"\
						 "orw	r3,r0 \n\t"\
						 "addd 	(r1,r0),(r11,r10) \n\t" /* E=S5(A)+E*/\
						 "movd   (r5,r4),(r1,r0) \n\t" /* R0=B*/\
						 "xord	(r9,r8),(r1,r0) \n\t"/* R0=R0^D*/\
						 "xord   (r7,r6),(r1,r0) \n\t" /* R2=R0^C*/\
						 "addd 	(r11,r10),(r1,r0) \n\t" /* E+=f1(B,C,D)*/\
						 "addd    $0x6ED9EBA1,(r1,r0) \n\t"/* E+=SHA_K0*/\
						 \
						 "loadd  0:(r13),(r11,r10)\n\t"\
						 \
						 "movw   r4,r3 \n\t" /**/\
						 "ashuw 	$14,r3 \n\t"\
						 "lshd  	$-2,(r5,r4) \n\t"\
						 "orw   r3,r5 \n\t" /*C=S30(B)*/\
						 \
						 "addd   (r11,r10),(r1,r0) \n\t"/*E=E+W(t)*/\
						 \
						 /* (r9,r8) E, (r7,r6) D, (r5,r4) C, (ra) B, (r1,r0) A*/\
						 \
						 "movd	(r1,r0),(r11,r10) \n\t"/*(r11,r10) A */\
						 "movw	r1,r3 \n\t"\
						 "ashud	$5:s,(r1,r0) \n\t"\
						 "lshw	$-11:s,r3 \n\t"\
						 "orw	r3,r0 \n\t"\
						 "addd 	(r1,r0),(r9,r8) \n\t" /* E=S5(A)+E*/\
						 \
						 "movd   (ra),(r1,r0) \n\t" /* R0=B*/\
						 "xord	(r7,r6),(r1,r0) \n\t"/* R0=R0^D*/\
						 "xord   (r5,r4),(r1,r0) \n\t" /* R2=R0^C*/\
						 "addd 	(r9,r8),(r1,r0) \n\t" /* E+=f1(B,C,D)*/\
						 \
						 "movd   (ra),(r9,r8) \n\t" /**/\
						 "ashud 	$30,(r9,r8) \n\t"\
						 "lshd  	$-2,(ra) \n\t"\
						 "ord   (r9,r8),(ra) \n\t" /*C=S30(B)*/\
						 \
						 "loadd  4:(r13),(r9,r8)\n\t"\
						 \
						 "addd    $0x6ED9EBA1,(r1,r0) \n\t"/* E+=SHA_K0*/\
						 "addd   (r9,r8),(r1,r0) \n\t"/*E=E+W(t)*/\
						 \
						 /* (r7,r6) E, (r5,r4) D, (ra) C, (r11,r10) B, (r1,r0) A*/\
						 \
						 "movd	(r1,r0),(r9,r8) \n\t"/*(r9,r8) A */\
						 "movw	r1,r3 \n\t"\
						 "ashud	$5:s,(r1,r0) \n\t"\
						 "lshw	$-11:s,r3 \n\t"\
						 "orw	r3,r0 \n\t"\
						 "addd 	(r1,r0),(r7,r6) \n\t" /* E=S5(A)+E*/\
						 \
						 "movd   (r11,r10),(r1,r0) \n\t" /* R0=B*/\
						 "xord	(r5,r4),(r1,r0) \n\t"/* R0=R0^D*/\
						 "xord   (ra),(r1,r0) \n\t" /* R2=R0^C*/\
						 "addd 	(r7,r6),(r1,r0) \n\t" /* E+=f1(B,C,D)*/\
						 "addd    $0x6ED9EBA1,(r1,r0) \n\t"/* E+=SHA_K0*/\
						 \
						 "loadd  8:(r13),(r7,r6)\n\t"\
						 \
						 "movw   r10,r3 \n\t" /**/\
						 "ashuw 	$14,r3 \n\t"\
						 "lshd  	$-2,(r11,r10) \n\t"\
						 "orw    r3,r11 \n\t" /*C=S30(B)*/\
						 \
						 "addd   (r7,r6),(r1,r0) \n\t"/*E=E+W(t)*/\
						 \
						 /* (r5,r4) E, (ra) D, (r11,r10) C, (r9,r8) B, (r1,r0) A*/\
						 \
						 "movd	(r1,r0),(r7,r6) \n\t"/*(r7,r6) A */\
						 "movw	r1,r3 \n\t"\
						 "ashud	$5:s,(r1,r0) \n\t"\
						 "lshw	$-11:s,r3 \n\t"\
						 "orw	r3,r0 \n\t"\
						 "addd 	(r1,r0),(r5,r4) \n\t" /* E=S5(A)+E*/\
						 \
						 "movd   (r9,r8),(r1,r0) \n\t" /* R0=B*/\
						 "xord	(ra),(r1,r0) \n\t"/* R0=R0^D*/\
						 "xord   (r11,r10),(r1,r0) \n\t" /* R2=R0^C*/\
						 "addd 	(r5,r4),(r1,r0) \n\t" /* E+=f1(B,C,D)*/\
						 "addd    $0x6ED9EBA1,(r1,r0) \n\t"/* E+=SHA_K0*/\
						 \
						 "loadd  12:(r13),(r5,r4)\n\t"\
						 \
						 "movw   r8,r3 \n\t" /**/\
						 "ashuw 	$14,r3 \n\t"\
						 "lshd  	$-2,(r9,r8) \n\t"\
						 "orw    r3,r9 \n\t" /*C=S30(B)*/\
						 \
						 "addd   (r5,r4),(r1,r0) \n\t"/*E=E+W(t)*/\
						 \
						 /* (ra) E, (r11,r10) D, (r9,r8) C, (r7,r6) B, (r1,r0) A*/\
						 \
						 "movd	(r1,r0),(r5,r4) \n\t"/*(r7,r6) A */\
						 "movw	r1,r3 \n\t"\
						 "ashud	$5:s,(r1,r0) \n\t"\
						 "lshw	$-11:s,r3 \n\t"\
						 "orw	r3,r0 \n\t"\
						 "addd 	(r1,r0),(ra) \n\t" /* E=S5(A)+E*/\
						 \
						 "movd   (r7,r6),(r1,r0) \n\t" /* R0=B*/\
						 "xord	(r11,r10),(r1,r0) \n\t"/* R0=R0^D*/\
						 "xord   (r9,r8),(r1,r0) \n\t" /* R2=R0^C*/\
						 "addd 	(ra),(r1,r0) \n\t" /* E+=f1(B,C,D)*/\
						 "addd   $0x6ED9EBA1,(r1,r0) \n\t"/* E+=SHA_K0*/\
						\
						"loadd  16:(r13),(ra)\n\t"\
						\
						"movw   r6,r3 \n\t" /**/\
						"ashuw 	$14,r3 \n\t"\
						"lshd  	$-2,(r7,r6) \n\t"\
						"orw    r3,r7 \n\t" /*C=S30(B)*/\
					  	\
						"addd   (ra),(r1,r0) \n\t"/*E=E+W(t)*/\
						\
						"addd $0x14,(r13) \n\t"\
						"subw $1,r2 \n\t"\
						"bcc 1b \n\t"\
						"movw $0x3,r2 \n\t"\
						"1: \n\t"\
						\
						/*--------------------------------------------------------*/\
						/*--------------------------------------------------------*/\
						/*--------------------------------------------------------*/\
						/*--------------------------------------------------------*/\
						/* (r11,r10) E, (r9,r8) D, (r7,r6) C, (r5,r4) B, (r1,r0) A*/\
						"movd	(r1,r0),(ra) \n\t"\
						"movw	r1,r3 \n\t"\
						"ashud	$5:s,(r1,r0) \n\t"\
						"lshw	$-11:s,r3 \n\t"\
						"orw	r3,r0 \n\t"\
						"addd 	(r1,r0),(r11,r10) \n\t" /* E=S5(A)+E*/\
						\
						/*(b and c) + (d and (b xor c))*/\
						"movd   (r7,r6),(r1,r0) \n\t" /* R2=C*/\
						"andd   (r5,r4),(r1,r0) \n\t"/* R2=B&C*/\
						"addd 	(r1,r0),(r11,r10) \n\t" \
						"movd   (r7,r6),(r1,r0) \n\t" /* R2=C*/\
						"xord   (r5,r4),(r1,r0) \n\t"/* R2=BxC*/\
						"andd   (r9,r8),(r1,r0) \n\t"/* R2&=D*/\
						"addd 	(r11,r10),(r1,r0) \n\t" /* E+=f2(B,C,D)*/\
						"addd    $0x8F1BBCDC,(r1,r0) \n\t"/* E+=SHA_K0*/\
						\
						"loadd  0:(r13),(r11,r10)\n\t"\
						\
						"movw   r4,r3 \n\t" /**/\
						"ashuw 	$14,r3 \n\t"\
						"lshd  	$-2,(r5,r4) \n\t"\
						"orw   r3,r5 \n\t" /*C=S30(B)*/\
						\
						"addd   (r11,r10),(r1,r0) \n\t"/*E=E+W(t)*/\
						\
						/* (r9,r8) E, (r7,r6) D, (r5,r4) C, (ra) B, (r1,r0) A*/\
						\
						"movd	(r1,r0),(r11,r10) \n\t"/*(r11,r10) A */\
						"movw	r1,r3 \n\t"\
						"ashud	$5:s,(r1,r0) \n\t"\
						"lshw	$-11:s,r3 \n\t"\
						"orw	r3,r0 \n\t"\
						"addd 	(r1,r0),(r9,r8) \n\t" /* E=S5(A)+E*/\
						\
						/*(b and c) + (d and (b xor c))*/\
						"movd   (r5,r4),(r1,r0) \n\t" /* R2=C*/\
						"andd   (ra),(r1,r0) \n\t"/* R2=B&C*/\
						"addd 	(r1,r0),(r9,r8) \n\t" \
						"movd   (r5,r4),(r1,r0) \n\t" /* R2=C*/\
						"xord   (ra),(r1,r0) \n\t"/* R2=BxC*/\
						"andd   (r7,r6),(r1,r0) \n\t"/* R2&=D*/\
						"addd 	(r9,r8),(r1,r0) \n\t" /* E+=f2(B,C,D)*/\
						\
						"movd   (ra),(r9,r8) \n\t" /**/\
						"ashud 	$30,(r9,r8) \n\t"\
						"lshd  	$-2,(ra) \n\t"\
						"ord   (r9,r8),(ra) \n\t" /*C=S30(B)*/\
						\
						"loadd  4:(r13),(r9,r8)\n\t"\
						"addd   $0x8F1BBCDC,(r1,r0) \n\t"/* E+=SHA_K0*/\
						"addd   (r9,r8),(r1,r0) \n\t"/*E=E+W(t)*/\
						/* (r7,r6) E, (r5,r4) D, (ra) C, (r11,r10) B, (r1,r0) A*/\
						\
						"movd	(r1,r0),(r9,r8) \n\t"/*(r9,r8) A */\
						"movw	r1,r3 \n\t"\
						"ashud	$5:s,(r1,r0) \n\t"\
						"lshw	$-11:s,r3 \n\t"\
						"orw	r3,r0 \n\t"\
						"addd 	(r1,r0),(r7,r6) \n\t" /* E=S5(A)+E*/\
						\
						/*(b and c) + (d and (b xor c))*/\
						"movd   (ra),(r1,r0) \n\t" /* R2=C*/\
						"andd   (r11,r10),(r1,r0) \n\t"/* R2=B&C*/\
						"addd 	(r1,r0),(r7,r6) \n\t" \
						"movd   (ra),(r1,r0) \n\t" /* R2=C*/\
						"xord   (r11,r10),(r1,r0) \n\t"/* R2=BxC*/\
						"andd   (r5,r4),(r1,r0) \n\t"/* R2&=D*/\
						"addd 	(r7,r6),(r1,r0) \n\t" /* E+=f2(B,C,D)*/\
						"addd   $0x8F1BBCDC,(r1,r0) \n\t"/* E+=SHA_K0*/\
						\
						"loadd  8:(r13),(r7,r6)\n\t"\
						\
						"movw   r10,r3 \n\t" /**/\
						"ashuw 	$14,r3 \n\t"\
						"lshd  	$-2,(r11,r10) \n\t"\
						"orw    r3,r11 \n\t" /*C=S30(B)*/\
						\
						"addd   (r7,r6),(r1,r0) \n\t"/* E+=W(t)*/\
						/* (r5,r4) E, (ra) D, (r11,r10) C, (r9,r8) B, (r1,r0) A*/\
						\
						"movd	(r1,r0),(r7,r6) \n\t"/*(r7,r6) A */\
						"movw	r1,r3 \n\t"\
						"ashud	$5:s,(r1,r0) \n\t"\
						"lshw	$-11:s,r3 \n\t"\
						"orw	r3,r0 \n\t"\
						"addd 	(r1,r0),(r5,r4) \n\t" /* E=S5(A)+E*/\
						/*(b and c) + (d and (b xor c))*/\
						"movd   (r11,r10),(r1,r0) \n\t" /* R2=C*/\
						"andd   (r9,r8),(r1,r0) \n\t"/* R2=B&C*/\
						"addd 	(r1,r0),(r5,r4) \n\t" \
						"movd   (r11,r10),(r1,r0) \n\t" /* R2=C*/\
						"xord   (r9,r8),(r1,r0) \n\t"/* R2=BxC*/\
						"andd   (ra),(r1,r0) \n\t"/* R2&=D*/\
						"addd 	(r5,r4),(r1,r0) \n\t" /* E+=f2(B,C,D)*/\
						"addd   $0x8F1BBCDC,(r1,r0) \n\t"/* E+=SHA_K0*/\
						\
						"loadd  12:(r13),(r5,r4)\n\t"\
						\
						"movw   r8,r3 \n\t" /**/\
						"ashuw 	$14,r3 \n\t"\
						"lshd  	$-2,(r9,r8) \n\t"\
						"orw    r3,r9 \n\t" /*C=S30(B)*/\
						\
						"addd   (r5,r4),(r1,r0) \n\t"/*E=E+W(t)*/\
						/* (ra) E, (r11,r10) D, (r9,r8) C, (r7,r6) B, (r1,r0) A*/\
						\
						"movd	(r1,r0),(r5,r4) \n\t"/*(r7,r6) A */\
						"movw	r1,r3 \n\t"\
						"ashud	$5:s,(r1,r0) \n\t"\
						"lshw	$-11:s,r3 \n\t"\
						"orw	r3,r0 \n\t"\
						\
						"addd 	(r1,r0),(ra) \n\t" /* E=S5(A)+E*/\
						\
						/*(b and c) + (d and (b xor c))*/\
						"movd   (r9,r8),(r1,r0) \n\t" /* R2=C*/\
						"andd   (r7,r6),(r1,r0) \n\t"/* R2=B&C*/\
						"addd 	(r1,r0),(ra) \n\t" \
						"movd   (r9,r8),(r1,r0) \n\t" /* R2=C*/\
						"xord   (r7,r6),(r1,r0) \n\t"/* R2=BxC*/\
						"andd   (r11,r10),(r1,r0) \n\t"/* R2&=D*/\
						"addd 	(ra),(r1,r0) \n\t" /* E+=f2(B,C,D)*/\
						"addd   $0x8F1BBCDC,(r1,r0) \n\t"/* E+=SHA_K0*/\
						\
						"loadd  16:(r13),(ra)\n\t"\
						\
						"movw   r6,r3 \n\t" /**/\
						"ashuw 	$14,r3 \n\t"\
						"lshd  	$-2,(r7,r6) \n\t"\
						"orw    r3,r7 \n\t" /*C=S30(B)*/\
						\
						"addd   (ra),(r1,r0) \n\t"/*E=E+W(t)*/\
						\
						"addd $0x14,(r13) \n\t"\
						"subw $1,r2 \n\t"\
						"bcc 1b \n\t"\
						"movw $0x3,r2 \n\t"\
						"1: \n\t"\
						\
						/*--------------------------------------------------------*/\
						/*--------------------------------------------------------*/\
						/*--------------------------------------------------------*/\
						/*--------------------------------------------------------*/\
						/* (r11,r10) E, (r9,r8) D, (r7,r6) C, (r5,r4) B, (r1,r0) A*/\
						"movd	(r1,r0),(ra) \n\t"\
						"movw	r1,r3 \n\t"\
						"ashud	$5:s,(r1,r0) \n\t"\
						"lshw	$-11:s,r3 \n\t"\
						"orw	r3,r0 \n\t"\
						"addd 	(r1,r0),(r11,r10) \n\t" /* E=S5(A)+E*/\
						\
						"movd   (r5,r4),(r1,r0) \n\t" /* R0=B*/\
						"xord	(r9,r8),(r1,r0) \n\t"/* R0=R0^D*/\
						"xord   (r7,r6),(r1,r0) \n\t" /* R2=R0^C*/\
						"addd 	(r11,r10),(r1,r0) \n\t" /* E+=f1(B,C,D)*/\
						"addd    $0xCA62C1D6,(r1,r0) \n\t"/* E+=SHA_K0*/\
						\
						"loadd  0:(r13),(r11,r10)\n\t"\
						\
						"movw   r4,r3 \n\t" /**/\
						"ashuw 	$14,r3 \n\t"\
						"lshd  	$-2,(r5,r4) \n\t"\
						"orw   r3,r5 \n\t" /*C=S30(B)*/\
						\
						"addd   (r11,r10),(r1,r0) \n\t"/*E=E+W(t)*/\
						/* (r9,r8) E, (r7,r6) D, (r5,r4) C, (ra) B, (r1,r0) A*/\
						\
						"movd	(r1,r0),(r11,r10) \n\t"/*(r11,r10) A */\
						"movw	r1,r3 \n\t"\
						"ashud	$5:s,(r1,r0) \n\t"\
						"lshw	$-11:s,r3 \n\t"\
						"orw	r3,r0 \n\t"\
						"addd 	(r1,r0),(r9,r8) \n\t" /* E=S5(A)+E*/\
						\
						"movd   (ra),(r1,r0) \n\t" /* R0=B*/\
						"xord	(r7,r6),(r1,r0) \n\t"/* R0=R0^D*/\
						"xord   (r5,r4),(r1,r0) \n\t" /* R2=R0^C*/\
						"addd 	(r9,r8),(r1,r0) \n\t" /* E+=f1(B,C,D)*/\
						\
						"movd   (ra),(r9,r8) \n\t" /**/\
						"ashud 	$30,(r9,r8) \n\t"\
						"lshd  	$-2,(ra) \n\t"\
						"ord   (r9,r8),(ra) \n\t" /*C=S30(B)*/\
						"loadd  4:(r13),(r9,r8)\n\t"\
						"addd   $0xCA62C1D6,(r1,r0) \n\t"/* E+=SHA_K0*/\
						"addd   (r9,r8),(r1,r0) \n\t"/*E=E+W(t)*/\
						\
						/* (r7,r6) E, (r5,r4) D, (ra) C, (r11,r10) B, (r1,r0) A*/\
						\
						"movd	(r1,r0),(r9,r8) \n\t"/*(r9,r8) A */\
						"movw	r1,r3 \n\t"\
						"ashud	$5:s,(r1,r0) \n\t"\
						"lshw	$-11:s,r3 \n\t"\
						"orw	r3,r0 \n\t"\
						\
						"addd 	(r1,r0),(r7,r6) \n\t" /* E=S5(A)+E*/\
						\
						"movd   (r11,r10),(r1,r0) \n\t" /* R0=B*/\
						"xord	(r5,r4),(r1,r0) \n\t"/* R0=R0^D*/\
						"xord   (ra),(r1,r0) \n\t" /* R2=R0^C*/\
						"addd 	(r7,r6),(r1,r0) \n\t" /* E+=f1(B,C,D)*/\
						"addd   $0xCA62C1D6,(r1,r0) \n\t"/* E+=SHA_K0*/\
						\
						"loadd  8:(r13),(r7,r6)\n\t"\
						\
						"movw   r10,r3 \n\t" /**/\
						"ashuw 	$14,r3 \n\t"\
						"lshd  	$-2,(r11,r10) \n\t"\
						"orw    r3,r11 \n\t" /*C=S30(B)*/\
						"addd   (r7,r6),(r1,r0) \n\t"/*E=E+W(t)*/\
						\
						/* (r5,r4) E, (ra) D, (r11,r10) C, (r9,r8) B, (r1,r0) A*/\
						\
						"movd	(r1,r0),(r7,r6) \n\t"/*(r7,r6) A */\
						"movw	r1,r3 \n\t"\
						"ashud	$5:s,(r1,r0) \n\t"\
						"lshw	$-11:s,r3 \n\t"\
						"orw	r3,r0 \n\t"\
						"addd 	(r1,r0),(r5,r4) \n\t" /* E=S5(A)+E*/\
						\
						"movd   (r9,r8),(r1,r0) \n\t" /* R0=B*/\
						"xord	(ra),(r1,r0) \n\t"/* R0=R0^D*/\
						"xord   (r11,r10),(r1,r0) \n\t" /* R2=R0^C*/\
						"addd 	(r5,r4),(r1,r0) \n\t" /* E+=f1(B,C,D)*/\
						"addd    $0xCA62C1D6,(r1,r0) \n\t"/* E+=SHA_K0*/\
						\
						"loadd  12:(r13),(r5,r4)\n\t"\
						\
						"movw   r8,r3 \n\t" /**/\
						"ashuw 	$14,r3 \n\t"\
						"lshd  	$-2,(r9,r8) \n\t"\
						"orw    r3,r9 \n\t" /*C=S30(B)*/\
						\
						"addd   (r5,r4),(r1,r0) \n\t"/*E=E+W(t)*/\
						\
						/* (ra) E, (r11,r10) D, (r9,r8) C, (r7,r6) B, (r1,r0) A*/\
						\
						"movd	(r1,r0),(r5,r4) \n\t"/*(r7,r6) A */\
						"movw	r1,r3 \n\t"\
						"ashud	$5:s,(r1,r0) \n\t"\
						"lshw	$-11:s,r3 \n\t"\
						"orw	r3,r0 \n\t"\
						"addd 	(r1,r0),(ra) \n\t" /* E=S5(A)+E*/\
						\
						"movd   (r7,r6),(r1,r0) \n\t" /* R0=B*/\
						"xord	(r11,r10),(r1,r0) \n\t"/* R0=R0^D*/\
						"xord   (r9,r8),(r1,r0) \n\t" /* R2=R0^C*/\
						"addd 	(ra),(r1,r0) \n\t" /* E+=f1(B,C,D)*/\
						"addd   $0xCA62C1D6,(r1,r0) \n\t"/* E+=SHA_K0*/\
						\
						"loadd  16:(r13),(ra)\n\t"\
						\
						"movw   r6,r3 \n\t" /**/\
						"ashuw 	$14,r3 \n\t"\
						"lshd  	$-2,(r7,r6) \n\t"\
						"orw    r3,r7 \n\t" /*C=S30(B)*/\
					  	\
						"addd   (ra),(r1,r0) \n\t"/*E=E+W(t)*/\
						\
						"addd $0x14,(r13) \n\t"\
						"subw $1,r2 \n\t"\
						"bcc 1b \n\t"\
				  		\
	  					/* (r11,r10) E, (r9,r8) D, (r7,r6) C, (r5,r4) B, (r1,r0) A*/\
						"loadd 0x4:(sp), (r13) \n\t"\
						"loadd	0x0:s(r13),(r3,r2) \n\t" /**/\
						"loadd	0x4:s(r13),(ra) \n\t" /**/\
						"addd (r1,r0),(r3,r2) \n\t" /**/\
						"stord (r3,r2),0x0:s(r13)\n\t" /**/\
		  			    "loadd	0x8:s(r13),(r3,r2) \n\t" /**/\
		  			    "addd (r5,r4),(ra) \n\t" /**/\
		  			    "stord (ra),0x4:s(r13)\n\t" /**/\
		  			    "loadd	0xc:s(r13),(ra) \n\t" /**/\
		  			    "addd (r7,r6),(r3,r2) \n\t" /**/\
						"stord (r3,r2),0x8:s(r13)\n\t" /**/\
						"loadd	0x10:s(r13),(r3,r2) \n\t" /**/\
						"addd (r9,r8),(ra) \n\t" /**/\
		  			    "stord (ra),0xc:s(r13)\n\t" /**/\
		  			    "addd (r11,r10),(r3,r2) \n\t" /**/\
						"stord (r3,r2),0x10:s(r13)\n\t" /**/\
						\
						"pop	$0x6,r2 \n\t"\
						"pop 	$2, r13,ra \n\t" \
						\
						: \
						:"r" (Wrpt), "r" (Mptr),"r" (Hptr) \
						: "r0","r1","r8","r9","r10","r11","memory");\
						})



static void HASH_BLOCK_DATA_ORDER (SHA_CTX *c, const void *p, size_t num)
{
	volatile unsigned long W[80];
	volatile unsigned long H[5];
	unsigned long *M=p;


	H[0]=c->h0;
	H[1]=c->h1;
	H[2]=c->h2;
	H[3]=c->h3;
	H[4]=c->h4;

		while (num>0)
		{
			 extendWord2(W,M,H);
			num--;
			M+=16;
		}
		c->h0=H[0];
		c->h1=H[1];
		c->h2=H[2];
		c->h3=H[3];
		c->h4=H[4];


}



#else
static void HASH_BLOCK_DATA_ORDER (SHA_CTX *c, const void *p, size_t num)
	{
	const unsigned char *data=p;
	register unsigned MD32_REG_T A,B,C,D,E,T,l;
#ifndef MD32_XARRAY
	unsigned MD32_REG_T	XX0, XX1, XX2, XX3, XX4, XX5, XX6, XX7,
				XX8, XX9,XX10,XX11,XX12,XX13,XX14,XX15;
#else
	SHA_LONG	XX[16];
#endif

	A=c->h0;
	B=c->h1;
	C=c->h2;
	D=c->h3;
	E=c->h4;

	for (;;)
			{
	const union { long one; char little; } is_endian = {1};

	if (!is_endian.little && sizeof(SHA_LONG)==4 && ((size_t)p%4)==0)
		{
		const SHA_LONG *W=(const SHA_LONG *)data;

		X( 0) = W[0];				X( 1) = W[ 1];
		BODY_00_15( 0,A,B,C,D,E,T,X( 0));	X( 2) = W[ 2];
		BODY_00_15( 1,T,A,B,C,D,E,X( 1));	X( 3) = W[ 3];
		BODY_00_15( 2,E,T,A,B,C,D,X( 2));	X( 4) = W[ 4];
		BODY_00_15( 3,D,E,T,A,B,C,X( 3));	X( 5) = W[ 5];
		BODY_00_15( 4,C,D,E,T,A,B,X( 4));	X( 6) = W[ 6];
		BODY_00_15( 5,B,C,D,E,T,A,X( 5));	X( 7) = W[ 7];
		BODY_00_15( 6,A,B,C,D,E,T,X( 6));	X( 8) = W[ 8];
		BODY_00_15( 7,T,A,B,C,D,E,X( 7));	X( 9) = W[ 9];
		BODY_00_15( 8,E,T,A,B,C,D,X( 8));	X(10) = W[10];
		BODY_00_15( 9,D,E,T,A,B,C,X( 9));	X(11) = W[11];
		BODY_00_15(10,C,D,E,T,A,B,X(10));	X(12) = W[12];
		BODY_00_15(11,B,C,D,E,T,A,X(11));	X(13) = W[13];
		BODY_00_15(12,A,B,C,D,E,T,X(12));	X(14) = W[14];
		BODY_00_15(13,T,A,B,C,D,E,X(13));	X(15) = W[15];
		BODY_00_15(14,E,T,A,B,C,D,X(14));
		BODY_00_15(15,D,E,T,A,B,C,X(15));

		data += SHA_CBLOCK;
		}
	else
		{
		HOST_c2l(data,l); X( 0)=l;		HOST_c2l(data,l); X( 1)=l;
		BODY_00_15( 0,A,B,C,D,E,T,X( 0));	HOST_c2l(data,l); X( 2)=l;
		BODY_00_15( 1,T,A,B,C,D,E,X( 1));	HOST_c2l(data,l); X( 3)=l;
		BODY_00_15( 2,E,T,A,B,C,D,X( 2));	HOST_c2l(data,l); X( 4)=l;
		BODY_00_15( 3,D,E,T,A,B,C,X( 3));	HOST_c2l(data,l); X( 5)=l;
		BODY_00_15( 4,C,D,E,T,A,B,X( 4));	HOST_c2l(data,l); X( 6)=l;
		BODY_00_15( 5,B,C,D,E,T,A,X( 5));	HOST_c2l(data,l); X( 7)=l;
		BODY_00_15( 6,A,B,C,D,E,T,X( 6));	HOST_c2l(data,l); X( 8)=l;
		BODY_00_15( 7,T,A,B,C,D,E,X( 7));	HOST_c2l(data,l); X( 9)=l;
		BODY_00_15( 8,E,T,A,B,C,D,X( 8));	HOST_c2l(data,l); X(10)=l;
		BODY_00_15( 9,D,E,T,A,B,C,X( 9));	HOST_c2l(data,l); X(11)=l;
		BODY_00_15(10,C,D,E,T,A,B,X(10));	HOST_c2l(data,l); X(12)=l;
		BODY_00_15(11,B,C,D,E,T,A,X(11));	HOST_c2l(data,l); X(13)=l;
		BODY_00_15(12,A,B,C,D,E,T,X(12));	HOST_c2l(data,l); X(14)=l;
		BODY_00_15(13,T,A,B,C,D,E,X(13));	HOST_c2l(data,l); X(15)=l;
		BODY_00_15(14,E,T,A,B,C,D,X(14));
		BODY_00_15(15,D,E,T,A,B,C,X(15));
		}

	BODY_16_19(16,C,D,E,T,A,B,X( 0),X( 0),X( 2),X( 8),X(13));
	BODY_16_19(17,B,C,D,E,T,A,X( 1),X( 1),X( 3),X( 9),X(14));
	BODY_16_19(18,A,B,C,D,E,T,X( 2),X( 2),X( 4),X(10),X(15));
	BODY_16_19(19,T,A,B,C,D,E,X( 3),X( 3),X( 5),X(11),X( 0));

	BODY_20_31(20,E,T,A,B,C,D,X( 4),X( 4),X( 6),X(12),X( 1));
	BODY_20_31(21,D,E,T,A,B,C,X( 5),X( 5),X( 7),X(13),X( 2));
	BODY_20_31(22,C,D,E,T,A,B,X( 6),X( 6),X( 8),X(14),X( 3));
	BODY_20_31(23,B,C,D,E,T,A,X( 7),X( 7),X( 9),X(15),X( 4));
	BODY_20_31(24,A,B,C,D,E,T,X( 8),X( 8),X(10),X( 0),X( 5));
	BODY_20_31(25,T,A,B,C,D,E,X( 9),X( 9),X(11),X( 1),X( 6));
	BODY_20_31(26,E,T,A,B,C,D,X(10),X(10),X(12),X( 2),X( 7));
	BODY_20_31(27,D,E,T,A,B,C,X(11),X(11),X(13),X( 3),X( 8));
	BODY_20_31(28,C,D,E,T,A,B,X(12),X(12),X(14),X( 4),X( 9));
	BODY_20_31(29,B,C,D,E,T,A,X(13),X(13),X(15),X( 5),X(10));
	BODY_20_31(30,A,B,C,D,E,T,X(14),X(14),X( 0),X( 6),X(11));
	BODY_20_31(31,T,A,B,C,D,E,X(15),X(15),X( 1),X( 7),X(12));

	BODY_32_39(32,E,T,A,B,C,D,X( 0),X( 2),X( 8),X(13));
	BODY_32_39(33,D,E,T,A,B,C,X( 1),X( 3),X( 9),X(14));
	BODY_32_39(34,C,D,E,T,A,B,X( 2),X( 4),X(10),X(15));
	BODY_32_39(35,B,C,D,E,T,A,X( 3),X( 5),X(11),X( 0));
	BODY_32_39(36,A,B,C,D,E,T,X( 4),X( 6),X(12),X( 1));
	BODY_32_39(37,T,A,B,C,D,E,X( 5),X( 7),X(13),X( 2));
	BODY_32_39(38,E,T,A,B,C,D,X( 6),X( 8),X(14),X( 3));
	BODY_32_39(39,D,E,T,A,B,C,X( 7),X( 9),X(15),X( 4));

	BODY_40_59(40,C,D,E,T,A,B,X( 8),X(10),X( 0),X( 5));
	BODY_40_59(41,B,C,D,E,T,A,X( 9),X(11),X( 1),X( 6));
	BODY_40_59(42,A,B,C,D,E,T,X(10),X(12),X( 2),X( 7));
	BODY_40_59(43,T,A,B,C,D,E,X(11),X(13),X( 3),X( 8));
	BODY_40_59(44,E,T,A,B,C,D,X(12),X(14),X( 4),X( 9));
	BODY_40_59(45,D,E,T,A,B,C,X(13),X(15),X( 5),X(10));
	BODY_40_59(46,C,D,E,T,A,B,X(14),X( 0),X( 6),X(11));
	BODY_40_59(47,B,C,D,E,T,A,X(15),X( 1),X( 7),X(12));
	BODY_40_59(48,A,B,C,D,E,T,X( 0),X( 2),X( 8),X(13));
	BODY_40_59(49,T,A,B,C,D,E,X( 1),X( 3),X( 9),X(14));
	BODY_40_59(50,E,T,A,B,C,D,X( 2),X( 4),X(10),X(15));
	BODY_40_59(51,D,E,T,A,B,C,X( 3),X( 5),X(11),X( 0));
	BODY_40_59(52,C,D,E,T,A,B,X( 4),X( 6),X(12),X( 1));
	BODY_40_59(53,B,C,D,E,T,A,X( 5),X( 7),X(13),X( 2));
	BODY_40_59(54,A,B,C,D,E,T,X( 6),X( 8),X(14),X( 3));
	BODY_40_59(55,T,A,B,C,D,E,X( 7),X( 9),X(15),X( 4));
	BODY_40_59(56,E,T,A,B,C,D,X( 8),X(10),X( 0),X( 5));
	BODY_40_59(57,D,E,T,A,B,C,X( 9),X(11),X( 1),X( 6));
	BODY_40_59(58,C,D,E,T,A,B,X(10),X(12),X( 2),X( 7));
	BODY_40_59(59,B,C,D,E,T,A,X(11),X(13),X( 3),X( 8));

	BODY_60_79(60,A,B,C,D,E,T,X(12),X(14),X( 4),X( 9));
	BODY_60_79(61,T,A,B,C,D,E,X(13),X(15),X( 5),X(10));
	BODY_60_79(62,E,T,A,B,C,D,X(14),X( 0),X( 6),X(11));
	BODY_60_79(63,D,E,T,A,B,C,X(15),X( 1),X( 7),X(12));
	BODY_60_79(64,C,D,E,T,A,B,X( 0),X( 2),X( 8),X(13));
	BODY_60_79(65,B,C,D,E,T,A,X( 1),X( 3),X( 9),X(14));
	BODY_60_79(66,A,B,C,D,E,T,X( 2),X( 4),X(10),X(15));
	BODY_60_79(67,T,A,B,C,D,E,X( 3),X( 5),X(11),X( 0));
	BODY_60_79(68,E,T,A,B,C,D,X( 4),X( 6),X(12),X( 1));
	BODY_60_79(69,D,E,T,A,B,C,X( 5),X( 7),X(13),X( 2));
	BODY_60_79(70,C,D,E,T,A,B,X( 6),X( 8),X(14),X( 3));
	BODY_60_79(71,B,C,D,E,T,A,X( 7),X( 9),X(15),X( 4));
	BODY_60_79(72,A,B,C,D,E,T,X( 8),X(10),X( 0),X( 5));
	BODY_60_79(73,T,A,B,C,D,E,X( 9),X(11),X( 1),X( 6));
	BODY_60_79(74,E,T,A,B,C,D,X(10),X(12),X( 2),X( 7));
	BODY_60_79(75,D,E,T,A,B,C,X(11),X(13),X( 3),X( 8));
	BODY_60_79(76,C,D,E,T,A,B,X(12),X(14),X( 4),X( 9));
	BODY_60_79(77,B,C,D,E,T,A,X(13),X(15),X( 5),X(10));
	BODY_60_79(78,A,B,C,D,E,T,X(14),X( 0),X( 6),X(11));
	BODY_60_79(79,T,A,B,C,D,E,X(15),X( 1),X( 7),X(12));
	
	c->h0=(c->h0+E)&0xffffffffL; 
	c->h1=(c->h1+T)&0xffffffffL;
	c->h2=(c->h2+A)&0xffffffffL;
	c->h3=(c->h3+B)&0xffffffffL;
	c->h4=(c->h4+C)&0xffffffffL;

	if (--num == 0) break;

	A=c->h0;
	B=c->h1;
	C=c->h2;
	D=c->h3;
	E=c->h4;

			}
	}
#endif
#endif

#else	/* OPENSSL_SMALL_FOOTPRINT */

#define BODY_00_15(xi)		 do {	\
	T=E+K_00_19+F_00_19(B,C,D);	\
	E=D, D=C, C=ROTATE(B,30), B=A;	\
	A=ROTATE(A,5)+T+xi;	    } while(0)

#define BODY_16_19(xa,xb,xc,xd)	 do {	\
	Xupdate(T,xa,xa,xb,xc,xd);	\
	T+=E+K_00_19+F_00_19(B,C,D);	\
	E=D, D=C, C=ROTATE(B,30), B=A;	\
	A=ROTATE(A,5)+T;	    } while(0)

#define BODY_20_39(xa,xb,xc,xd)	 do {	\
	Xupdate(T,xa,xa,xb,xc,xd);	\
	T+=E+K_20_39+F_20_39(B,C,D);	\
	E=D, D=C, C=ROTATE(B,30), B=A;	\
	A=ROTATE(A,5)+T;	    } while(0)

#define BODY_40_59(xa,xb,xc,xd)	 do {	\
	Xupdate(T,xa,xa,xb,xc,xd);	\
	T+=E+K_40_59+F_40_59(B,C,D);	\
	E=D, D=C, C=ROTATE(B,30), B=A;	\
	A=ROTATE(A,5)+T;	    } while(0)

#define BODY_60_79(xa,xb,xc,xd)	 do {	\
	Xupdate(T,xa,xa,xb,xc,xd);	\
	T=E+K_60_79+F_60_79(B,C,D);	\
	E=D, D=C, C=ROTATE(B,30), B=A;	\
	A=ROTATE(A,5)+T+xa;	    } while(0)

#if !defined(SHA_1) || !defined(SHA1_ASM)
static void HASH_BLOCK_DATA_ORDER (SHA_CTX *c, const void *p, size_t num)
	{
	const unsigned char *data=p;
	register unsigned MD32_REG_T A,B,C,D,E,T,l;
	int i;
	SHA_LONG	X[16];

	A=c->h0;
	B=c->h1;
	C=c->h2;
	D=c->h3;
	E=c->h4;

	for (;;)
		{
	for (i=0;i<16;i++)
	{ HOST_c2l(data,l); X[i]=l; BODY_00_15(X[i]); }
	for (i=0;i<4;i++)
	{ BODY_16_19(X[i],       X[i+2],      X[i+8],     X[(i+13)&15]); }
	for (;i<24;i++)
	{ BODY_20_39(X[i&15],    X[(i+2)&15], X[(i+8)&15],X[(i+13)&15]); }
	for (i=0;i<20;i++)
	{ BODY_40_59(X[(i+8)&15],X[(i+10)&15],X[i&15],    X[(i+5)&15]);  }
	for (i=4;i<24;i++)
	{ BODY_60_79(X[(i+8)&15],X[(i+10)&15],X[i&15],    X[(i+5)&15]);  }

	c->h0=(c->h0+A)&0xffffffffL; 
	c->h1=(c->h1+B)&0xffffffffL;
	c->h2=(c->h2+C)&0xffffffffL;
	c->h3=(c->h3+D)&0xffffffffL;
	c->h4=(c->h4+E)&0xffffffffL;

	if (--num == 0) break;

	A=c->h0;
	B=c->h1;
	C=c->h2;
	D=c->h3;
	E=c->h4;

		}
	}
#endif

#endif
