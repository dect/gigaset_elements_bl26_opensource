install:
	$(call install_on_root,$(TOP)/opensource/certs/ca-bundle.pem.src,/etc/ssl/certs)

%:
	$(error Target "$@" is not supported)
	
	
.PHONY: install %
