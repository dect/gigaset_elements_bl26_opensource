ifeq ($(BUILD),hw)
    BUILD_THIS := 1
else ifeq ($(BUILD),hw_recovery)
    BUILD_THIS := 1
else
    BUILD_THIS := 0
endif

ifeq ($(BUILD_THIS),1)

install: 
	cd u-boot-env-tools && \
	make all && \
	make install

clean:
	cd u-boot-env-tools && \
	make clean

%:
	$(error Target "$@" is not supported)
	
	
.PHONY: install clean %

endif
