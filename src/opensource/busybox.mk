

export CFLAGS := 
export LDFLAGS	:=

configure:
	cd busybox && \
	make	ARCH=$(ARCH) \
			CROSS_COMPILE=$(CC_PREFIX) \
			CONFIG_PREFIX=$(ROOTFS) \
			oldconfig
	
build:
	cd busybox && \
	make	ARCH=$(ARCH) \
			CROSS_COMPILE=$(CC_PREFIX) \
			CONFIG_PREFIX=$(ROOTFS) \
			SKIP_STRIP=y \
			-j $(JOBS)


install:
	cd busybox && \
	make	oldconfig && \
	make	ARCH=$(ARCH) \
			CROSS_COMPILE=$(CC_PREFIX) \
			CONFIG_PREFIX=$(ROOTFS) \
			SKIP_STRIP=y \
			-j $(JOBS) \
			install			

checkstack:
	cd busybox && \
	make	ARCH=$(ARCH) \
			CROSS_COMPILE=$(CC_PREFIX) \
			CONFIG_PREFIX=$(ROOTFS) \
			SKIP_STRIP=y \
			checkstack


menuconfig:
	cd busybox && \
	make 	ARCH=$(ARCH) \
			CROSS_COMPILE=$(CC_PREFIX) \
			CONFIG_PREFIX=$(ROOTFS) \
			menuconfig



clean:
	cd busybox && \
	make    ARCH=$(ARCH) \
        	CROSS_COMPILE=$(CC_PREFIX) \
        	CONFIG_PREFIX=$(ROOTFS) \
			clean



	

%:
	$(error Target "$@" is not supported)
	
	
.PHONY: install menuconfig clean build %
		 