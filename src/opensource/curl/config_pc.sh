#! /bin/sh
#PREFIX=$PWD/release
if [ -z "$1" ]; then
    echo "Please call $0 one argument (with target directory)"
    exit 1;
fi
PREFIX=$1
        
./configure \
			--with-ssl=$PREFIX				\
			--enable-dependency-tracking				\
			--enable-smtp						\
			--enable-static						\
			--disable-shared					\
			--enable-http 						\
			--disable-ftp 						\
			--disable-ldap 						\
			--disable-ldaps						\
			--disable-dict 						\
			--disable-telnet 					\
			--disable-pop3 						\
			--disable-imap						\
			--disable-tftp 						\
			--disable-rtsp						\
			--disable-file						\
			--disable-gopher					\
			--disable-smtp						\
			--disable-manual 					\
			--enable-nonblocking					\
			--disable-verbose 					\
			--disable-debug 					\
			--disable-crypto-auth 					\
			--disable-tls-srp					\
			--disable-threaded-resolver				\
			--with-pic 						\
			--with-gnu-ld 						\
			--with-random=/dev/urandom				\
			--without-libssh2 					\
			--without-librtmp					\
			--without-libidn					\
			--without-zlib						\
			--enable-threads					\
			--enable-verbose 					\
			--prefix=$PREFIX					\
			--exec-prefix=$PREFIX					\
			--with-ca-bundle=$PREFIX/etc/ssl/certs/ca-bundle.pem	
#			--with-ca-path=/etc/ssl/certs/				
#			--without-ca-bundle								



