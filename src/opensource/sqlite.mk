

LIB_NO := $(LIB_SQLITE_NO)
LIB_STATIC := $(TMP_ROOTFS)/lib/libsqlite3.a
LIB_SHARED := $(TMP_ROOTFS)/lib/lib$(LIB_NO).so


CFLAGS += -DSQLITE_DISABLE_LFS -DSQLITE_OMIT_LOAD_EXTENSION


##############################
# Build configuration for HW #
##############################

ifeq ($(BUILD),hw)


CFLAGS += $(CFLAGS_DYNAMIC) 
LDFLAGS += $(LDFLAGS_DYNAMIC) 

LDLIBS = -Wl,-R,$(PTHREAD_GDB)


configure: clean
	cd sqlite && \
	autoreconf -fiv && \
	./configure --disable-largefile --enable-dynamic-extensions=no --host=cr16-uclinux --target =cr16-uclinux --prefix=$(TMP_ROOTFS)  
	



install: make
	cd sqlite && \
	make install
	$(CC) $(LDFLAGS) -o $(LIB_SHARED) -nostartfiles $(SHARED_LIB_START) -Wl,-shared-lib-id,$(LIB_NO) -Wl,--whole-archive,$(LIB_STATIC)  -Wl,--no-whole-archive $(LDLIBS) 
	$(call install_on_root,$(LIB_SHARED),/lib)
	cp $(LIB_SHARED).gdb $(GDB_DIR)
	
	
endif	# HW configuration














##############################
# Build configuration for PC #
##############################

ifeq ($(BUILD),pc)

configure: clean
	cd sqlite && \
	autoreconf -fiv && \
	./configure --prefix=$(TMP_ROOTFS) && \
	make clean
	


install: make
	cd sqlite && \
	make install
	
endif	# PC configuration










########################
# Common build targets #
########################


make:
	cd sqlite && \
	make -j $(JOBS)  	
	

clean:
	cd sqlite && \
	make clean	
	
	

%:
	$(error Target "$@" is not supported)
	
	
.PHONY: %
		 