
##############################
# Build configuration for HW #
##############################

ifeq ($(BUILD),hw)

CFLAGS += $(CFLAGS_DYNAMIC)
LDFLAGS += $(LDFLAGS_DYNAMIC)

CFLAGS += -I$(PWD)/linux/ -I$(TMP_ROOTFS)/include/ -DL_ENDIAN -DTERMIO  -DEMBED -D__UC_LIBC__ -D__uClinux__ -Wall -std=gnu99 -DOPENSSL_SMALL_FOOTPRINT
LDLIBS += -Wl,-R,$(PTHREAD_GDB)

#stop on first error
CFLAGS += -Wfatal-errors


#LDFLAGS+= -Wl,--verbose
LDFLAGS += -Wl,-Map,openssl.map
#LDFLAGS += -Wl,-M

# DMALLOC:
ifeq ($(DMALLOC_ENABLE),true)
CFLAGS += $(DMALLOC_CFLAGS)
LDLIBS += $(DMALLOC_LDLIBS)
#LDFLAGS += -L$(TMP_ROOTFS)/lib
#LDFLAGS += -Wl,-Bstatic -ldmallocth
endif


SHARED_CFLAGS += -fPIC -nostartfiles $(SHARED_LIB_START) 
SHARED_LDFLAGS += -shared -Wl,-shared-lib-id,7 -Wl,-R,$(PTHREAD_GDB) 

export CFLAGS
export LDLIBS
export LDFLAGS
export SHARED_CFLAGS
export SHARED_LDFLAGS


all:
	cd openssl && \
	$(MAKE) all

install: all
	cd openssl && \
	$(MAKE) install_sw
	$(call install_on_nfs,$(TMP_ROOTFS)/bin/openssl,/bin)
#	$(call install_on_root,$(TMP_ROOTFS)/bin/openssl,/bin)
#	rm -f $(DATAFS)/openssl
	rm -f $(ROOTFS)/bin/openssl
#	cp $(TMP_ROOTFS)/bin/openssl $(DATAFS)/openssl
#	ln -s /mnt/data/openssl $(ROOTFS)/bin/openssl
	ln -s /tmp/openssl $(ROOTFS)/bin/openssl


configure: 
	cd openssl && \
	./config_sc1445x.sh $(TMP_ROOTFS) && \
	$(MAKE) clean 
	
	

endif	# HW configuration






##############################
# Build configuration for PC #
##############################

ifeq ($(BUILD),pc)

install: 
	cd openssl && \
	$(MAKE) && \
	$(MAKE) install_sw

configure: 
	cd openssl && \
	./config_pc.sh $(TMP_ROOTFS) && \
	$(MAKE) clean
	

endif	# PC configuration



###################
# Commont targets #
###################
clean:
	cd openssl && \
	if [ -e Makefile ]; then make clean; fi

%:
	$(error Target "$@" is not supported)
	
	
.PHONY: install clean %
		 