This software is provided to you under the following conditions:

(1) You may use the open source software components only in compliance with the respective
    license conditions (see files LICENSE, COPYRIGHT, COPYING, LEGAL, etc.).

(2) To the extent proprietary object and/or binary code is provided by Gigaset, such
    object and/or binary code may only be used in conjunction with the respective,
    designated Gigaset product. You may not (i) modify, reverse engineer or reverse
    compile such proprietary object and/or binary code provided by Gigaset or (ii) extract
    portions thereof; except as may be permitted by applicable mandatory law under certain
    circumstances. All rights reserved.

(3) Gigaset shall pay damages - irrespective on which legal ground (e.g. default or tort)
    - only within the following scope:

    a) In the event of willful intent Gigaset shall be fully liable.

    b) In the event of gross negligence and in the event of default regarding a guarantee
       undertaking Gigaset shall be liable up to the amount of the foreseeable damage
       which would have been prevented by the relevant duty of care or the guarantee
       undertaking.

    c) In the event of simple negligence Gigaset shall only be liable when defaulting with
       an obligation which is so material that the attainment of the purpose of the
       agreement is jeopardized. In this case Gigaset shall be liable for the damage which
       is typical and was foreseeable.

    d) Beyond lit. a) to c) Gigaset shall not be liable.

    e) Gigaset shall compensate frustrated expenses - irrespective on which legal ground
       (e.g. default or tort) - only within the following scope:

        (a) In the event of willful intent Gigaset shall be fully liable.

        (b) In the event of gross negligence and in the event of default regarding a
            guarantee undertaking Gigaset shall be liable up to the amount of the
            foreseeable frustrated expenses which would have been prevented by the
            relevant duty of care or the guarantee undertaking.

        (c) In the event of simple negligence Gigaset shall only be liable when defaulting
            with an obligation which is so material that the attainment of the purpose of
            this Agreement is jeopardized. In this case Gigaset shall be liable for the
            frustrated expenses which are typical and were foreseeable.

        (d) Beyond lit. (a) to (c) Gigaset shall not be liable.

    f) the statutory liability for the injury of life, body and health as well as under
       the Product Liability Act (Produkthaftungsgesetz) shall remain unaffected.

    g) The objection of contributory negligence shall remain unaffected.

(4) Any modification of Gigaset's software or the firmware of Gigaset's products voids
    Gigaset's warranty and liability as far as the damage is attributable to the
    modification.

