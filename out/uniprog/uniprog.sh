#!/bin/bash
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.
# 
OUTDIR=".."
VMLINUZ="reef_vmlinuz"
RECOVERY="reef_vmlinuz-recovery"
#
#
#
. ./func.sh
echo "Current directory: $PWD"


while getopts "hp:ea:" OPTION
do
  case $OPTION in
    p)
      PROGRAM_PORT=$OPTARG
      ;;
    a)
      REPLY=$OPTARG
      ;;
  esac
done



START_PROG=0

####################
  check_vmlinuz
  check_recovery
  OUTIMAGE="reef_boot_rec_sys_fs.bin"
  MNT_DATA_FS="../../datafs"
  MNT_DATA_JFFS2_FILE="${OUTDIR}/reef_fs.bin"
  init_image 0
  insert_bootloader
  insert_recovery
  insert_vmlinuz
  insert_jffs2
  START_PROG=1
create_sum

echo "----------------------------------------------------------"
echo " Image: / (size `${FILESIZE} ${OUTDIR}/${OUTIMAGE}`) created."
echo "----------------------------------------------------------"


if [ $START_PROG -eq 1 ]; then
#
# Program device
#
set_serial_port
echo "Programming HW via ${PORT}"

echo
echo "Please do following things:"
echo "1. Push \"Program\" button,"
echo "2. Push \"Reset\" button,"
echo "3. Pop \"Reset\" button,"
echo "4. Pop \"Program\" button."
echo ""
echo "Be patient"
echo ""

time ./uartfp 452fp.bin ${OUTDIR}/${OUTIMAGE} ${PORT}
echo ""
echo "Done."
fi
