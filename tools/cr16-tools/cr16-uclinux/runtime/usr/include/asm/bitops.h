#ifndef _CR16_BITOPS_H
#define _CR16_BITOPS_H

/*
 * Copyright 1992, Linus Torvalds.
 * Copyright 2002, Yoshinori Sato
 * Copyright 2007, Lee Jones - CR16 version
 */

/*For now just include the generic bitops*/
#ifdef __KERNEL__
#include <asm/system.h>
#include <asm-generic/bitops.h>
#define smp_mb__before_clear_bit()     barrier()
#define smp_mb__after_clear_bit()      barrier()
#endif /* __KERNEL__ */

#if 0 /* Lee - Using the generic stuff for now */

#include <linux/compiler.h>
#include <asm/system.h>

#ifdef __KERNEL__
/*
 * Function prototypes to keep gcc -Wall happy
 */

/*
 * ffz = Find First Zero in word. Undefined if no zero exists,
 * so code should check against ~0UL first..
 */
static __inline__ unsigned long ffz(unsigned long word)
{
	unsigned long result;
	unsigned long one;

	result = -1;

	__asm__("1:\n\t"                         
                "addd $1, %0\n\t"
		"movd $1, %3\n\t"
		"andd %2, %3\n\t"
		"cmpd $1, %3\n\t"
		"lshd $-1, %2\n\t"
		"beq 1b\n\t"
		: "=&r" (result)
		: "0"  (result), "r" (word), "r" (one));

	return result;
}

#define CR16_GEN_BITOP_CONST(OP,BIT,ISNOT)		                     \
	case BIT:					                     \
        /* The 'ISNOT' tells us if the 'change_bit' instruction was called */\
        if (!ISNOT) {                                                        \
	        __asm__(OP " $" #BIT ", 0x0%0"::"r"(b_addr):"memory");       \
        }                                                                    \
        /* There isn't a 'not' instruction - Simulate it */                  \
        else {                                                               \
	        __asm__("movd $1, %1\n\t"                                    \
                        "lshd $" #BIT ", %1\n\t"                             \
                        "loadd 0x0%0, %2\n\t"                                \
		        "andd %2, %1\n\t"                                    \
                        "cmpd $0, %1\n\t"                                    \
                        "blt 1f\n\t"                                         \
		        "sbitb $" #BIT ",0x0%0\n\t"                          \
                        "br 2f\n"                                            \
                        "1:\n\tcbitb $" #BIT ", 0x0%0\n"                     \
                        "2:\n\t"                                             \
			:                                                    \
			:"r"(b_addr), "r"(one), "r"(val)                     \
			:"memory");                                          \
        }                                                                    \
        break;

#define CR16_GEN_BITOP(FNAME,OP,ISNOT)                                       \
static  __inline__ void FNAME(int nr, volatile unsigned long* addr)          \
{								             \
        unsigned long one, val;                                              \
        volatile unsigned char *b_addr;				             \
        b_addr = (volatile unsigned char *)addr + (nr >> 3);                 \
	switch(nr & 7) {				                     \
	        CR16_GEN_BITOP_CONST(OP,0,ISNOT)	                     \
		CR16_GEN_BITOP_CONST(OP,1,ISNOT)	                     \
		CR16_GEN_BITOP_CONST(OP,2,ISNOT)	                     \
		CR16_GEN_BITOP_CONST(OP,3,ISNOT)	                     \
		CR16_GEN_BITOP_CONST(OP,4,ISNOT)	                     \
		CR16_GEN_BITOP_CONST(OP,5,ISNOT)	                     \
		CR16_GEN_BITOP_CONST(OP,6,ISNOT)	                     \
		CR16_GEN_BITOP_CONST(OP,7,ISNOT)	                     \
        }                		                                     \
}							              

/* Lee - I don't know who commented this out?
   b_addr = (volatile unsigned char *)addr;                                  \
*/

/*
 * clear_bit() doesn't provide any barrier for the compiler.
 */
#define smp_mb__before_clear_bit()	barrier()
#define smp_mb__after_clear_bit()	barrier()

CR16_GEN_BITOP(set_bit,"sbitb",0)            /* As we don't have a 'not' insturction, an extra */
CR16_GEN_BITOP(clear_bit,"cbitb",0)          /* 'is this a not' argument had to be added. This */
CR16_GEN_BITOP(change_bit,"",1)              /* can either be '1' (yes) OR '0' (no)            */
#define __set_bit(nr,addr)    set_bit((nr),(addr))
#define __clear_bit(nr,addr)  clear_bit((nr),(addr))
#define __change_bit(nr,addr) change_bit((nr),(addr))

#undef CR16_GEN_BITOP
#undef CR16_GEN_BITOP_CONST

static __inline__ int test_bit(int nr, const unsigned long* addr)
{
	return (*((volatile unsigned char *)addr + 
               (nr >> 3)) & (1UL << (nr & 7))) != 0;
}

#define __test_bit(nr, addr) test_bit(nr, addr)

#define CR16_GEN_TEST_BITOP_CONST_INT(OP,BIT,ISNOT)                  \
	case BIT:						     \
        if (!ISNOT) {                                                \
	__asm__("sprd psr, %6\n\t"                                   \
                "di \n\t"                                             \
                "nop \n\t"                                            \
                "push $8, r2\n\t"                                    \
                "push $2, r10\n\t"                                   \
                "movd $1, %4\n\t"                                    \
                "lshd $" #BIT ", %4\n\t"                             \
                "loadd 0x0%3, %5\n\t"                                \
	        "andd %5, %4\n\t"                                    \
                "lshd $-" #BIT ", %4\n\t"                            \
		"movd %4, %0\n\t"                                    \
		OP " $" #BIT ", 0x0%3\n\t"                           \
                "pop $2, r10\n\t"                                    \
                "pop $8, r2\n\t"                                     \
                "lprd %6, psr\n\t"                                   \
		: "=&r"(retval),"=m"(*b_addr)	                     \
		: "0" (retval),"r" (b_addr), "r" (one), "r" (val), "r" (psr) \
		: "memory");                                         \
	} else {                                                     \
	__asm__("sprd psr, %6\n\t"                                   \
		"di \n\t"                                             \
		"nop \n\t"                                            \
                "push $8, r2\n\t"                                    \
                "push $2, r10\n\t"                                   \
                "movd $1, %4\n\t"                                    \
                "lshd $" #BIT ", %4\n\t"                             \
                "loadd 0x0%3, %5\n\t"                                \
		"andd %5, %4\n\t"                                    \
                "lshd $-" #BIT ", %4\n\t"                            \
                "cmpd $0, %4\n\t"                                    \
                "blt 1f\n\t"                                         \
		"sbitb $" #BIT ", 0x0%3\n\t"                         \
                "br 2f\n"                                            \
                "1:\n\tcbitb $" #BIT ", 0x0%3\n"                     \
                "2:\n\t"                                             \
                "movd %4, %0\n\t"                                    \
                "pop $2, r10\n\t"                                    \
                "pop $8, r2\n\t"                                     \
		"lprd %6, psr\n\t"                                   \
	        : "=&r"(retval),"=m"(*b_addr)	                     \
		: "0" (retval),"r" (b_addr), "r" (one), "r" (val), "r" (psr) \
		: "memory");                                         \
	}                                                            \
        break;

#define CR16_GEN_TEST_BITOP_CONST(OP,BIT,ISNOT)		             \
	case BIT:						     \
        if (!ISNOT) {                                                \
	__asm__("push $8, r2\n\t"                                    \
                "push $2, r10\n\t"                                   \
                "movd $1, %4\n\t"                                    \
                "lshd $" #BIT ", %4\n\t"                             \
                "loadd 0x0%3, %5\n\t"                                \
		"andd %5, %4\n\t"                                    \
                "lshd $-" #BIT ", %4\n\t"                            \
		"movd %4, %0\n\t"                                    \
		OP " $" #BIT ", 0x0%3\n\t"                           \
                "pop $2, r10\n\t"                                    \
                "pop $8, r2\n\t"                                     \
                : "=&r"(retval),"=m"(*b_addr)	                     \
		: "0" (retval),"r" (b_addr), "r" (one), "r" (val)    \
		: "memory");                                         \
	} else {                                                     \
        __asm__("push $8, r2\n\t"                                    \
                "push $2, r10\n\t"                                   \
                "movd $1, %4\n\t"                                    \
                "lshd $" #BIT ", %4\n\t"                             \
                "loadd 0x0%3, %5\n\t"                                \
	        "andd %5, %4\n\t"                                    \
                "lshd $-" #BIT ", %4\n\t"                            \
                "cmpd $0, %4\n\t"                                    \
                "blt 1f\n\t"                                         \
		"sbitb $" #BIT ", 0x0%3\n\t"                         \
                "br 2f\n"                                            \
                "1:\n\tcbitb $" #BIT ", 0x0%3\n"                     \
                "2:\n\t"                                             \
                "movd %4, %0\n\t"                                    \
                "pop $2, r10\n\t"                                    \
                "pop $8, r2\n\t"                                     \
		: "=&r"(retval),"=m"(*b_addr)	                     \
		: "0" (retval),"r" (b_addr), "r" (one), "r" (val)    \
		: "memory");                                         \
        }                                                            \
        break;

#define CR16_GEN_TEST_BITOP(FNNAME,OP,ISNOT)		             \
static __inline__ int FNNAME(int nr, volatile void * addr)	     \
{								     \
	int psr, retval = 0;						     \
        unsigned long one = 0, val;                                  \
	volatile unsigned char *b_addr;				     \
	b_addr = (volatile unsigned char *)addr + (nr >> 3);         \
	switch(nr & 7) {				             \
		CR16_GEN_TEST_BITOP_CONST_INT(OP,0,ISNOT)            \
		CR16_GEN_TEST_BITOP_CONST_INT(OP,1,ISNOT)            \
		CR16_GEN_TEST_BITOP_CONST_INT(OP,2,ISNOT)            \
		CR16_GEN_TEST_BITOP_CONST_INT(OP,3,ISNOT)            \
		CR16_GEN_TEST_BITOP_CONST_INT(OP,4,ISNOT)            \
		CR16_GEN_TEST_BITOP_CONST_INT(OP,5,ISNOT)            \
		CR16_GEN_TEST_BITOP_CONST_INT(OP,6,ISNOT)            \
		CR16_GEN_TEST_BITOP_CONST_INT(OP,7,ISNOT)            \
	}						             \
        return retval;						     \
}								     \
								     \
static __inline__ int __ ## FNNAME(int nr, volatile void * addr)     \
{								     \
	int retval = 0;						     \
        unsigned long one, val;                                      \
	volatile unsigned char *b_addr;				     \
	b_addr = (volatile unsigned char *)addr + (nr >> 3);         \
	switch(nr & 7) {				             \
		CR16_GEN_TEST_BITOP_CONST(OP,0,ISNOT) 	             \
		CR16_GEN_TEST_BITOP_CONST(OP,1,ISNOT) 	             \
		CR16_GEN_TEST_BITOP_CONST(OP,2,ISNOT) 	             \
		CR16_GEN_TEST_BITOP_CONST(OP,3,ISNOT) 	             \
		CR16_GEN_TEST_BITOP_CONST(OP,4,ISNOT) 	             \
		CR16_GEN_TEST_BITOP_CONST(OP,5,ISNOT) 	             \
		CR16_GEN_TEST_BITOP_CONST(OP,6,ISNOT) 	             \
		CR16_GEN_TEST_BITOP_CONST(OP,7,ISNOT) 	             \
	}						             \
	return retval;						     \
}

CR16_GEN_TEST_BITOP(test_and_set_bit,"sbitb",0)
CR16_GEN_TEST_BITOP(test_and_clear_bit,"cbitb",0)
CR16_GEN_TEST_BITOP(test_and_change_bit,"",1)

#undef CR16_GEN_TEST_BITOP_CONST
#undef CR16_GEN_TEST_BITOP_CONST_INT
#undef CR16_GEN_TEST_BITOP

#include <asm-generic/bitops/ffs.h>

static unsigned long __ffs(unsigned long word)
{
	unsigned long result, one;

	result = -1;

	__asm__("1:\n\t"                         
                "addd $1, %0\n\t"
		"movd $1, %3\n\t"
		"andd %2, %3\n\t"
		"cmpd $1, %3\n\t"
		"lshd $-1, %2\n\t"
		"bne 1b\n\t"
		: "=&r" (result)
		: "0"  (result), "r" (word), "r" (one));
	return result;
}

#include <asm-generic/bitops/find.h>
#include <asm-generic/bitops/sched.h>
#include <asm-generic/bitops/hweight.h>
#include <asm-generic/bitops/ext2-non-atomic.h>
#include <asm-generic/bitops/ext2-atomic.h>
#include <asm-generic/bitops/minix.h>

#endif /* __KERNEL__ */

#include <asm-generic/bitops/fls.h>
#include <asm-generic/bitops/fls64.h>

#endif /* Lee - End of my bitops */

#endif /* _CR16_BITOPS_H */

