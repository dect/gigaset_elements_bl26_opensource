/*
 * Copyright (c) 2012, Gigaset Elements GmbH
 *
 * Wojceich Nizinski <niziak@spox.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

#ifndef _SC14452_CACHE_H_
	#define _SC14452_CACHE_H_

#define ASM_CACHE_CTRL_REG      0xFF5006  /* Cache control register */
#define ASM_CACHE_LEN0_REG      0xFF5008  /* Cache length register 0 */
#define ASM_CACHE_START0_REG    0xFF500A  /* Cache start register 0, can be used for iCache and dCache */
#define ASM_CACHE_LEN1_REG      0xFF500C  /* Cache length register 1 */
#define ASM_CACHE_START1_REG    0xFF500E  /* Cache start register 1, can be used for iCache and dCache */
#define ASM_CACHE_STATUS_REG    0xFF5010  /* Cache status register */

#define CFG_DC					0x004	/* [0-9]:2 Data cache enable */
#define CFG_LDC					0x008	/* [0-9]:3 Lock Data Cache */
#define CFG_IC					0x010	/* [0-9]:4 Instruction Cache enable */
#define CFG_LIC					0x020	/* [0-9]:5 Lock Instruction Cache */
#define CFG_ED					0x100	/* [0-9]:8 Extended Dispatch bit */
#define CFG_SR					0x200	/* [0-9]:9 Short register */





#if defined (CONFIG_CR16_CACHE_ENABLE)

	#if defined (CONFIG_CR16_CACHE_UNIFIED)

		#if defined (CONFIG_CR16_CACHE_SIZE_16)
				#define CACHE_CTRL_REG_VALUE	0x6
		#elif defined (CONFIG_CR16_CACHE_SIZE_8)
				#define CACHE_CTRL_REG_VALUE	0x5
		#elif defined (CONFIG_CR16_CACHE_SIZE_4)
				#define CACHE_CTRL_REG_VALUE	0x4
		#endif

	#else

		#if defined (CONFIG_CR16_CACHE_SIZE_16)
				#define CACHE_CTRL_REG_VALUE	0x16
		#elif defined (CONFIG_CR16_CACHE_SIZE_8)
				#define CACHE_CTRL_REG_VALUE	0x15
		#elif defined (CONFIG_CR16_CACHE_SIZE_4)
				#define CACHE_CTRL_REG_VALUE	0x14
		#endif

	#endif

	#define CACHE_RAM_ADM_END 			0xA000

	#if defined (CONFIG_CR16_CACHE_SIZE_16)
			#define CACHE_RAM_ADM_START 0x9800
			#define CACHE_RAM_ADM_LEN	0x800

	#elif defined (CONFIG_CR16_CACHE_SIZE_8)
			#define CACHE_RAM_ADM_START 0x9C00
			#define CACHE_RAM_ADM_LEN	0x400

	#elif defined (CONFIG_CR16_CACHE_SIZE_4)
			#define CACHE_RAM_ADM_START 0x9E00
			#define CACHE_RAM_ADM_LEN	0x200

	#endif
#else

	#define CACHE_CTRL_REG_VALUE	0x0

#endif

#endif
