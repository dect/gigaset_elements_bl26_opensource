/*
 * Copyright (c) 2011, Dialog Semiconductor BV
 *
 * <aristotelis.iordanidis@diasemi.com> and contributors.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation. See linux-2.6.x/COPYING for more details.
 */

#if !defined( _ASM_REGS_H )
#define _ASM_REGS_H

#define __HAVE_INCLUDED_ASM_REGS_H

#if defined(CONFIG_SC14450)
#	include <asm/regs_sc14450.h>
#elif defined( CONFIG_SC14452 )
#	include <asm/regs_sc14452.h>
#	include <asm/regs_sc14452_addendum.h>
#else
#	error UNKNOWN CPU TYPE
#endif

#undef __HAVE_INCLUDED_ASM_REGS_H

#endif  /* _ASM_REGS_H */

