#ifndef _ASM_CR16_SIGCONTEXT_H
#define _ASM_CR16_SIGCONTEXT_H

struct sigcontext {
	unsigned long  sc_mask; 	/* old sigmask */
	unsigned long  sc_usp;		/* old user stack pointer */
	unsigned long  sc_r0and1;
	unsigned long  sc_r2and3;
	unsigned long  sc_r4and5;
	unsigned long  sc_r6and7;
	unsigned long  sc_r8and9;
	unsigned long  sc_r10and11;
	unsigned long  sc_r12;
	unsigned long  sc_r13;
	unsigned long  sc_pc;
	unsigned long  sc_ra;
        unsigned short sc_psr;
};

#endif
