#ifndef _CR16_SEMAPHORE_H
#define _CR16_SEMAPHORE_H

#define RW_LOCK_BIAS		 0x01000000

#ifndef __ASSEMBLY__

#include <linux/linkage.h>
#include <linux/wait.h>
#include <linux/spinlock.h>
#include <linux/rwsem.h>

#include <asm/system.h>
#include <asm/atomic.h>

/*
 * Interrupt-safe semaphores..
 *
 * (C) Copyright 1996 Linus Torvalds
 *
 * H8/300 version by Yoshinori Sato
 * CR16 version by Lee Jones - 2007
 *
 */


struct semaphore {
	atomic_t count;
	int nopers;
	wait_queue_head_t wait;
};

#define __SEMAPHORE_INITIALIZER(name, n)				\
{									\
	.count		= ATOMIC_INIT(n),				\
	.nopers	= 0,						\
	.wait		= __WAIT_QUEUE_HEAD_INITIALIZER((name).wait)	\
}

#define __DECLARE_SEMAPHORE_GENERIC(name,count) \
	struct semaphore name = __SEMAPHORE_INITIALIZER(name,count)

#define DECLARE_MUTEX(name) __DECLARE_SEMAPHORE_GENERIC(name,1)
#define DECLARE_MUTEX_LOCKED(name) __DECLARE_SEMAPHORE_GENERIC(name,0)

static inline void sema_init (struct semaphore *sem, int val)
{
	*sem = (struct semaphore)__SEMAPHORE_INITIALIZER(*sem, val);
}

static inline void init_MUTEX (struct semaphore *sem)
{
	sema_init(sem, 1);
}

static inline void init_MUTEX_LOCKED (struct semaphore *sem)
{
	sema_init(sem, 0);
}

asmlinkage void __down_failed(void /* special register calling convention */);
asmlinkage int  __down_failed_interruptible(void  /* params in registers */);
asmlinkage int  __down_failed_trylock(void  /* params in registers */);
asmlinkage void __up_wakeup(void /* special register calling convention */);

asmlinkage void __down(struct semaphore * sem);
asmlinkage int  __down_interruptible(struct semaphore * sem);
asmlinkage int  __down_trylock(struct semaphore * sem);
asmlinkage void __up(struct semaphore * sem);

extern spinlock_t semaphore_wake_lock;

static inline void down(struct semaphore * sem)
{
  might_sleep();

  /*
   * Try to get the semaphore, take the slow path if we fail.
   */
  if (unlikely(atomic_dec_return(&sem->count) < 0))
    __down(sem);
}

static inline int down_interruptible(struct semaphore * sem)
{
  int ret = 0;

  might_sleep();

  if (unlikely(atomic_dec_return(&sem->count) < 0))
    ret = __down_interruptible(sem);
  return ret;
}

static inline int down_trylock(struct semaphore * sem)
{
 	int ret = 0; 

 	if (unlikely(atomic_dec_return(&sem->count) < 0)) 
 		ret = __down_trylock(sem); 
 	return ret; 
}

static inline void up(struct semaphore * sem)
{
  if (unlikely(atomic_inc_return(&sem->count) <= 0))
    __up(sem);
}

#endif /* __ASSEMBLY__ */

#endif
