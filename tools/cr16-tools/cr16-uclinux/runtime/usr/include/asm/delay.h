#ifndef _CR16_DELAY_H
#define _CR16_DELAY_H

#include <asm/param.h>

/*
 * Copyright (C) 2002 Yoshinori Sato <ysato@sourceforge.jp>
 * Copyright (C) 2007 Lee Jones <ljones@mpc-data.co.uk> - CR16 version
 *
 * Delay routines, using a pre-computed "loops_per_second" value.
 */

extern __inline__ void __delay(unsigned long loops)
{
	__asm__ __volatile__ ("subd $1, %0\n\t"
			      "1:\n\t"
			      "addd $-1, %0\n\t"
			      "bcs 1b\n\t"
			      :"=r" (loops):"0"(loops));
	
}

/*
 * Use only for very small delays ( < 1 msec).  Should probably use a
 * lookup table, really, as the multiplications take much too long with
 * short delays.  This is a "reasonable" implementation, though (and the
 * first constant multiplications gets optimized away if the delay is
 * a constant)  
 */

extern unsigned long loops_per_jiffy;

extern __inline__ void udelay(unsigned long usecs)
{
	usecs = ( usecs * loops_per_jiffy ) / ( 1000000 / HZ ) ;
	if (usecs)
		__delay(usecs);
	
}

#endif /* _CR16_DELAY_H */
