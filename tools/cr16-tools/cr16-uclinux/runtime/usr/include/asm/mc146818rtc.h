/*
 * Machine dependent access functions for RTC registers.
 */
#ifndef _CR16_MC146818RTC_H
#define _CR16_MC146818RTC_H

/* empty include file to satisfy the include in genrtc.c/ide-geometry.c */

#endif /* _CR16_MC146818RTC_H */
