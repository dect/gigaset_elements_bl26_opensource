/*
 * include/asm-cr16/flat.h -- uClinux bFLT relocations
 *
 *  Copyright (C) 2004,05  Microtronix Datacom Ltd
 *
 * This file is subject to the terms and conditions of the GNU General
 * Public License.  See the file COPYING in the main directory of this
 * archive for more details.
 *
 * 
 */

#ifndef __CR16_FLAT_H__
#define __CR16_FLAT_H__

#define flat_stack_align(sp)                    /* nothing needed */
#define flat_argvp_envp_on_stack()              1
#define flat_old_ram_flag(flags)                1
#define flat_reloc_valid(reloc, size)           ((reloc) <= (size))



/* We store the type of relocation in the top 4 bits of the `relval.' */

/* Convert a relocation entry into an address.  */
static inline unsigned long
flat_get_relocate_addr (unsigned long relval)
{
	return relval & 0x0fffffff; /* Mask out top 4-bits */
}

#define FLAT_CR16_RELOC_TYPE(relval)	\
	( ((relval) >> 28) & ~( 1 << (FLAT_CR16_NEW_RELOC - 28) ) )


#define FLAT_CR16_R_32			0 /* Normal 32-bit reloc */
#define FLAT_CR16_R_32a			4 /* Normal 32-bit reloc */
#define FLAT_CR16_R_ABS24		1 /* High 16-bits + low 16-bits field */
#define FLAT_CR16_R_IMM32  	        2 /* High 16-bits adjust + low 16-bits field */
#define FLAT_CR16_R_IMM32a		3 /* Call imm26 */
#define FLAT_CR16_R_IMM20		5 /* imm20 */
#define FLAT_CR16_R_SWITCH32		6
#define FLAT_CR16_R_SYM_DIF		7


#define FLAT_CR16_NEW_RELOC		31 /* flag to denote new elf2flt */

static inline int
flat_cr16_is_new_reloc( unsigned long relval )
{
	return !!( relval & (1 << FLAT_CR16_NEW_RELOC ) ) ;
}


/* Extract the address to be relocated from the symbol reference at rp;
 * relval is the raw relocation-table entry from which RP is derived.
 * rp shall always be 32-bit aligned
 */
static inline unsigned long flat_get_addr_from_rp (unsigned long *rpl,
						   unsigned long relval,
						   unsigned long flags)
{
	unsigned char * rp = (unsigned char *) rpl;
	switch (FLAT_CR16_RELOC_TYPE(relval))
	{
	case FLAT_CR16_R_IMM32a:  
	case FLAT_CR16_R_IMM32:
	case FLAT_CR16_R_32:
	case FLAT_CR16_R_32a:
	case FLAT_CR16_R_SYM_DIF:
		/* Simple 32-bit address. */
		return *rpl;

	case FLAT_CR16_R_ABS24:
		/* get the 24bit address from words 2 and 3 of the instruction
		 * construct a 32-bit value.
		 */
	  return /*htonl*/(((rp[0] & 0xf) << 20) | ((rp[1] & 0xf) << 16)
						| (rp[3] << 8) | (rp[2]));

	case FLAT_CR16_R_IMM20:
		return ( (rp[0] & 0xf) << 16 ) | (rp[3] << 8) | rp[2] ;

	case FLAT_CR16_R_SWITCH32:
		return *rpl ;

	default:
		return ~0;	/* bogus value */
	}
}

/* Insert the address addr into the symbol reference at rp;
 * relval is the raw relocation-table entry from which rp is derived.
 * rp shall always be 32-bit aligned
 */
static inline void flat_put_addr_at_rp (unsigned long *rpl, unsigned long addr,
					unsigned long relval)
{
	unsigned short * rp = (unsigned short *) rpl;
	unsigned char * rpb = (unsigned char *) rpl;
	unsigned short exist_val;
	switch (FLAT_CR16_RELOC_TYPE (relval)) {


	case FLAT_CR16_R_32a:
		/* Simple 32-bit address.  */
		if( flat_cr16_is_new_reloc( relval ) )
			*rpl = addr ;
		else
			*rpl = addr >> 1 ;
		break;

	case FLAT_CR16_R_32:
		/* Simple 32-bit address.  */
		*rpl = addr;
		break;

	case FLAT_CR16_R_ABS24:
		exist_val = rp[0];
		rp[0] = ((exist_val & 0xf0f0) | ((((addr >> 16) & 0xf) << 8) | (((addr >> 16) & 0xf0) >> 4) ));
		rp[1] = (addr & 0xFFFF);
		break;

	case FLAT_CR16_R_IMM32:
		rp[1] = (addr & 0xFFFF);
		rp[0] = (addr >> 16);
		break;

	case FLAT_CR16_R_IMM32a:
	        addr = addr / 2;
		rp[1] = (addr & 0xFFFF);
		rp[0] = (addr >> 16);
		break;

	case FLAT_CR16_R_IMM20:
		rpb[2] = (unsigned char)addr ;
		addr >>= 8 ;
		rpb[3] = (unsigned char)addr ;
		addr >>= 8 ;
		rpb[0] = ( rpb[2] & 0xf0 ) | (unsigned char)( addr & 0xf ) ;
		break ;

	case FLAT_CR16_R_SWITCH32:
	case FLAT_CR16_R_SYM_DIF:
		break;
	}
}

#endif /* __CR16_FLAT_H__ */
