
/* We need to store r0-r13, SP and RA for CR16*/
/* Define the machine-dependent type `jmp_buf'.  CR16 version.  */

#ifndef _SETJMP_H
# error "Never include <bits/setjmp.h> directly; use <setjmp.h> instead."
#endif

typedef struct
  {

    /* Return address  */
    unsigned long __ra;

    /*Registers*/
    unsigned long __r0and1;
    unsigned long __r2and3;
    unsigned long __r4and5;

    unsigned long __r6and7;
    unsigned long __r8and9;
    unsigned long __r10and11;
    unsigned long __r12;
    unsigned long __r13;

    /* Stack pointer.  */
    unsigned long __sp;

  } __jmp_buf[1];


#ifdef __USE_MISC
/* Offset to the program counter in `jmp_buf'.  */
# define JB_PC  0
#endif

/* Test if longjmp to JMPBUF would unwind the frame
   containing a local variable at ADDRESS.  */
#define _JMPBUF_UNWINDS(jmpbuf, address) \
  ((void *) (address) < (jmpbuf)[0].__sp)



