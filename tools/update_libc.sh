#!/bin/bash
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

LIBC_SRC_DIR="./cr16-tools/cr16-uclinux/runtime/usr/lib/far-pic/int32/mid-shared-library"
LIBC_DEST_DIR="../src/init_rootfs/lib"
mkdir -p ${LIBC_DEST_DIR}
cp -f ${LIBC_SRC_DIR}/libc		${LIBC_DEST_DIR}/lib1.so
cp -f ${LIBC_SRC_DIR}/libpthread	${LIBC_DEST_DIR}/lib2.so

