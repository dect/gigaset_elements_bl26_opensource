#!/bin/bash
# This file is copyrighted by Gigaset Elements GmbH
# All rights reserved.

REQUIRED_COMMANDS="grep 7z cat find sha256sum git doxygen cp rm mkdir echo java ln killall autoreconf aclocal expr printf true sed tail cut head"

for CMD in ${REQUIRED_COMMANDS}; do
    if ! command -v ${CMD} &>/dev/zero; then
	echo 
	echo "Command '${CMD}' not found!"
	echo "Please install!"
	echo
	exit 1
    fi
done


